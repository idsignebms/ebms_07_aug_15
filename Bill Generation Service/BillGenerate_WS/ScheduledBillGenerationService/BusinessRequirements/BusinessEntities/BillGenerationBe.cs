﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class BillGenerationBe
    {
        public int BillProcessTypeId { get; set; }

        public string BillProcessType { get; set; }

        public int BillMonthID { get; set; }

        public int Year { get; set; }

        public int Month { get; set; }

        public string MonthName { get; set; }

        public string YearName { get; set; }

        public int OpenStatusId { get; set; }

        public int BillingQueueScheduleId { get; set; }

        public int BillingMonth { get; set; }

        public int BillingYear { get; set; }

        public int BillSendingMode { get; set; }

        public int TotalCustomersInQueue { get; set; }

        public DateTime ServiceStartDate { get; set; }

        public string FeederId { get; set; }

        public string CycleId { get; set; }

        public string CycleName { get; set; }

        public int BillGenarationStatusId { get; set; }

        public bool IsExists { get; set; }

        public bool IsActive { get; set; }

        public bool IsExistingProcessType { get; set; }

        public string TariffName { get; set; }

        public int TariffId { get; set; }

        public int TotalRecords { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        //public string BU_ID { get; set; }

        public string SU_ID { get; set; }

        public string FeederIds { get; set; }

        //public string CycleIds { get; set; }

        public string TariffIds { get; set; }

        public int CustomerBillId { get; set; }
        public string BillNo { get; set; }
        public string AccountNo { get; set; }
        public string CustomerId { get; set; }
        public decimal TotalBillAmount { get; set; }
        public string ServiceAddress { get; set; }
        public string MeterNo { get; set; }
        public int Dials { get; set; }
        public decimal NetArrears { get; set; }
        public decimal NetEnergyCharges { get; set; }
        public decimal NetFixedCharges { get; set; }
        public decimal VAT { get; set; }
        public decimal VATPercentage { get; set; }
        public string Messages { get; set; }
        public string BillGeneratedBy { get; set; }
        public DateTime BillGeneratedDate { get; set; }
        public DateTime LastDateOfBill { get; set; }
        public int BillYear { get; set; }
        public int BillMonth { get; set; }
        public decimal TotalBillAmountWithArrears { get; set; }
        public string PreviousReading { get; set; }
        public string PresentReading { get; set; }
        public decimal Usage { get; set; }
        public decimal TotalBillAmountWithTax { get; set; }
        public string Name { get; set; }
        public string BusinessUnitName { get; set; }
        public string ServiceUnitName { get; set; }
        public string ServiceCenterName { get; set; }
        public string ServiceHouseNo { get; set; }
        public string ServiceStreet { get; set; }
        public string ServiceCity { get; set; }
        public string ServiceZipCode { get; set; }
        public string PostalHouseNo { get; set; }
        public string PostalStreet { get; set; }
        public string PostalZipCode { get; set; }
        public string ReadDate { get; set; }
        public int Multiplier { get; set; }
        public string LastPaymentDate { get; set; }
        public string LastPaidAmount { get; set; }
        public decimal PreviousBalance { get; set; }
        public string RouteSequenceNo { get; set; }
        public string RouteNo { get; set; }
        public string RouteName { get; set; }
        public string AverageReading { get; set; }
        public bool IsSuccess { get; set; }

        public string ReadCode { get; set; }

        public int ReadCodeId { get; set; }

        public int PageNo { get; set; }

        public int PageSize { get; set; }

        public int RowNumber { get; set; }

        public int CustomerReadingId { get; set; }

        public bool IsBilled { get; set; }

        public string FilePath { get; set; }

        public int MeterTypeId { get; set; }

        public string OldAccountNo { get; set; }

        public string PrevMonth { get; set; }

        public string BookNo { get; set; }
        public string AdjustmentAmmount { get; set; }
        public string OpenBatches { get; set; }

        public string BillingQueueScheduleIdList { get; set; }
        public int RowsEffected { get; set; }
        public decimal UsageAdjusted { get; set; }
        public string ServiceCenterID { get; set; }
        public string CycleEmailList { get; set; }
    }
}
