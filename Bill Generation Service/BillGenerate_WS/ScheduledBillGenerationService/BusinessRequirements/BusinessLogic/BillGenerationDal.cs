﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Data;
using UMS_NigeriaBE;

namespace UMS_NigriaDAL
{
    public class BillGenerationDal
    {
        iDsignDAL objiDsignDAL = new iDsignDAL();
        SqlConnection SqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        CommonDAL objCommonDal = new CommonDAL();
        BillGenerationListBe _objBillGenerationListBe = new BillGenerationListBe();
        #region ProcedureNames

        public string PROC_UPS_CheckScheduledBillTime = "UPS_CheckScheduledBillTime";
        public string PROC_UPS_GenerateScheduledBill = "USP_InsertBillGenerationDetails_New_ServicePurpose";
        public string PROC_USP_GetServiceCenterList = "UPS_GetServiceCentersForBillPrint";
        public string PROC_UPS_GetGenerateBillDetails = "GetCustomerBillForFileCreation";
        public string PROC_UPS_UpdateBillingStatusAsBillGenarted = "UPS_UpdateBillingStatusAsBillGenarted";
        public string PROC_UPS_UpdateBillingStatusAsBillFileCreated = "UPS_UpdateBillingStatusAsBillFileCreated";
        public string PROC_UPS_GetCycleEmailListByServiceCenterID = "UPS_GetEmailList";
        public string PROC_UPS_UpdateBillingStatusAsEmailSent = "UPS_UpdateBillingStatusAsEmailSent";
        public string PROC_UPS_UpdateBillingStatusAsComplete = "UPS_UpdateBillingStatusAsComplete";

        #endregion

        #region Methods

        public XmlDocument ScheduleBillGenerationDALXML(XmlDocument entityXml, BillReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case BillReturnType.CheckExecutionTime:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_UPS_CheckScheduledBillTime, SqlCon);
                        break;
                    case BillReturnType.GetServiceCenterList:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GetServiceCenterList, SqlCon);
                        break;
                    case BillReturnType.UpdateBillStatusToGenerated:
                        xml = objiDsignDAL.ExecuteXmlReader(entityXml, PROC_UPS_UpdateBillingStatusAsBillGenarted, SqlCon);
                        break;
                    case BillReturnType.UpdateBillStatusToFileGenrated:
                        xml = objiDsignDAL.ExecuteXmlReader(entityXml, PROC_UPS_UpdateBillingStatusAsBillFileCreated, SqlCon);
                        break;
                    case BillReturnType.UpdateBillStatusToEmailSent:
                        xml = objiDsignDAL.ExecuteXmlReader(entityXml, PROC_UPS_UpdateBillingStatusAsEmailSent, SqlCon);
                        break;
                    case BillReturnType.UpdateBillStatusToComplete:
                        xml = objiDsignDAL.ExecuteXmlReader(entityXml, PROC_UPS_UpdateBillingStatusAsComplete, SqlCon);
                        break;
                    case BillReturnType.GetCycleEmailListByServiceCenterID:
                        xml = objiDsignDAL.ExecuteXmlReader(entityXml, PROC_UPS_GetCycleEmailListByServiceCenterID, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }
        public DataSet ScheduleBillGenerationDALDATASET(XmlDocument entityXml, BillReturnType value)
        {
            DataSet ds = new DataSet();
            try
            {

                switch (value)
                {
                    case BillReturnType.GetGeneratedBillDetails:
                        ds = objiDsignDAL.ExecuteReader_Get(entityXml, PROC_UPS_GetGenerateBillDetails, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }
        public void ScheduleBillGenerationDALDATASET(BillReturnType value, int TimeOut)
        {
            try
            {
                switch (value)
                {

                    case BillReturnType.GenerateScheduledBill:
                        objiDsignDAL.ExecuteReader_Get(PROC_UPS_GenerateScheduledBill, SqlCon, TimeOut);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

    }
}
