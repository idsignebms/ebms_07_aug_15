﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace UMS_NigriaDAL
{
    public class CommonDAL
    {
    }
    public enum BillReturnType
    {
        CheckExecutionTime,
        GenerateScheduledBill,
        GetGeneratedBillDetails,
        GetServiceCenterList,
        UpdateBillStatusToGenerated,
        UpdateBillStatusToFileGenrated,
        UpdateBillStatusToEmailSent,
        UpdateBillStatusToComplete,
        GetCycleEmailListByServiceCenterID,
        IsBillInSchedule,
        InsertCloseMonthData,
        GetBillDetailsPdfGen
    }
}
