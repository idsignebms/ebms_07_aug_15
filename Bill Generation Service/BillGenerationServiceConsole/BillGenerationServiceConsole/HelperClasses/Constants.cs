﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScheduledBillGenerationService.HelperClasses
{
    public class Constants
    {
        public const int GenerateEBillsFiles = 2;

        public const string trnDateFormateStr = "dd/MM/yyyy";
        public const string LogFileName = "Service2_EBillFileGenLogs.txt";
        public const string LogFileDateFormate = "dd_MM_yyyy_hh_mm_ss";
        public const string batchTotalFormate = "00000000000.00";
        public const string paydirectPayment = "PAYDIRECT PAYMENT";
        public const string TrnAmountFormate = "00000000.00";
        public const string xlsFileDateFormate = "ddMMyyyyhhmmss";
        public const string trnDateFormate = "ddMMyyyyhhmmss";
        public const string txtFileDateFormate = "yyyyMMdd";
        public const string logFileDateFormate = "dd_MM_yyyy_hh_mm_ss";

        public const string operationDrive = @"C:\";
        public const string logFolder = "EbillLogs\\";
        public const string EBillPaymentFolder = "EBillPayFiles\\";
        public const string payDateFormat = "dd_MM_yyyy";

        #region CurrencyFormate
        public const int INR_Format = 1;
        public const int MILLION_Format = 2;
        #endregion
    }
}
