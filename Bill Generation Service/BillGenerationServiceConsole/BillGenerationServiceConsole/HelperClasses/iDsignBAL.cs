﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : This Class is Used Common Business Layer.it is Having CommonMethods
 *                  
 Developer        : <031 -  Suresh kumar>
 Creation Date    : <28/02/2012>
 
 Revision History
 
 Modified By    | Reviewer      | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 Vineela-ID017  | Satyanarayana |    10-01-2013                  |                    
 Reason  :     added add to cart feature
 * ---------------------------------------------------------------------------------------
 Vineela-ID017  | Satyanarayana |    11-01-2013                  |                    
 
Reason  :     added ccavenue feature
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Security.Cryptography;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Management;
using System.Reflection;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.Web;

namespace iDSignHelper
{
    public class iDsignBAL
    {
        /// <summary>
        /// serializes the Class Object into Xml Document.
        /// </summary>
        /// <typeparam name="T">Class</typeparam>
        /// <param name="xml">The Class Object</param>
        /// <returns> XmlDocument </returns>
        public XmlDocument DoSerialize<T>(T classObject)
        {
            XmlDocument XmlObject = new XmlDocument();

            XmlSerializer serializer = new XmlSerializer(typeof(T));
            StringWriter sw = new StringWriter();
            serializer.Serialize(sw, classObject);
            string xml = sw.ToString();
            xml = xml.Replace("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "");
            xml = xml.Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "");
            xml = xml.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "").Trim();
            XmlObject.LoadXml(xml);
            return XmlObject;
        }

        /// <summary>
        /// serializes the Dataset Or DataTable Object into Multi Xml Document.
        /// </summary>
        /// <param name="transformObject">The DataSet OR DataTable Object</param>
        /// <returns> XmlDocument </returns>
        public XmlDocument DoSerialize(object transformObject)
        {
            XmlDocument serializedElement = new XmlDocument();
            try
            {
                MemoryStream memStream = new MemoryStream();
                XmlSerializer serializer = new XmlSerializer(transformObject.GetType());
                StringWriter sw = new StringWriter();
                serializer.Serialize(sw, transformObject);
                string xml = sw.ToString();
                xml = xml.Replace("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "");
                xml = xml.Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "");
                xml = xml.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "").Trim();
                serializedElement.LoadXml(xml);

                return serializedElement;
            }
            catch (Exception SerializeException)
            {

            }
            return serializedElement;
        }

        /// <summary>
        /// Deserializes the XmlDocumenst.
        /// </summary>
        /// <typeparam name="T">Class</typeparam>
        /// <param name="xml">The Xml Document</param>
        /// <returns> Class Object </returns>
        public T DeserializeFromXml<T>(XmlDocument xml)
        {
            T myObj = default(T);
            if (!string.IsNullOrEmpty(xml.InnerXml))
            {
                //Assuming doc is an XML document containing a serialized object and objType is a System.Type set to the type of the object.
                XmlNodeReader reader = new XmlNodeReader(xml.DocumentElement);
                XmlSerializer ser = new XmlSerializer(typeof(T));
                object obj = ser.Deserialize(reader);
                // Then you just need to cast obj into whatever type it is eg:
                myObj = (T)obj;
            }
            return myObj;
        }

    }
}
