﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : This Class is Used Common Data Layer.it is Having ExecuteXml Reader methods
 *                  
 Developer        : <031 -  Suresh kumar>
 Creation Date    : <28/02/2012>
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
                       
 
Reason  :     
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace iDSignHelper
{
    public class iDsignDAL
    {

        /// <summary>
        /// ExecuteXmlReader without Parametrs..
        /// </summary>
        /// <param name="ProcedureName">string</param>
        /// <returns> XmlDocument </returns>
        public XmlDocument ExecuteXmlReader(string procedureName, SqlConnection sqlCon)
        {
            XmlDocument xml = new XmlDocument();

            if (sqlCon.State == ConnectionState.Closed)
            {
                sqlCon.Open();
            }
            SqlCommand sqlCmd = new SqlCommand(procedureName, sqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandTimeout = 600;

            using (XmlReader xmlReader = sqlCmd.ExecuteXmlReader())
            {
                while (xmlReader.Read())
                {
                    xml.Load(xmlReader);
                }
            }
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return xml;
        }

        public XmlDocument ExecuteXmlReader(string procedureName, SqlConnection sqlCon,int TimeOut)
        {
            XmlDocument xml = new XmlDocument();

            if (sqlCon.State == ConnectionState.Closed)
            {
                sqlCon.Open();
            }
            SqlCommand sqlCmd = new SqlCommand(procedureName, sqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandTimeout = TimeOut;

            using (XmlReader xmlReader = sqlCmd.ExecuteXmlReader())
            {
                while (xmlReader.Read())
                {
                    xml.Load(xmlReader);
                }
            }
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return xml;
        }
        /// <summary>
        /// ExecuteXmlReader with Parametrs.
        /// </summary>
        /// <param name="ProcedureName">string</param>
        /// <param name="xmlDoc">Input XmlDocument</param>
        /// <returns> XmlDocument </returns>
        public XmlDocument ExecuteXmlReader(XmlDocument xmlDoc, string procedureName, SqlConnection sqlCon)
        {
            XmlDocument xml = new XmlDocument();

            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlCommand sqlCmd = new SqlCommand(procedureName, sqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = new SqlParameter("@XmlDoc", SqlDbType.Xml);
            param1.Value = xmlDoc.InnerXml;
            sqlCmd.Parameters.Add(param1);

            using (XmlReader xmlReader = sqlCmd.ExecuteXmlReader())
            {
                while (xmlReader.Read())
                {
                    xml.Load(xmlReader);
                }
            }
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return xml;
        }

        /// <summary>
        /// ExecuteXmlReader with Parametrs.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="procedureName"></param>
        /// <param name="sqlCon"></param>
        /// <param name="CommandTimeOut"></param>
        /// <returns></returns>
        public XmlDocument ExecuteXmlReader(XmlDocument xmlDoc, string procedureName, SqlConnection sqlCon, int CommandTimeOut)
        {
            XmlDocument xml = new XmlDocument();

            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlCommand sqlCmd = new SqlCommand(procedureName, sqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;

            sqlCmd.CommandTimeout = CommandTimeOut;

            SqlParameter param1 = new SqlParameter("@XmlDoc", SqlDbType.Xml);
            param1.Value = xmlDoc.InnerXml;
            sqlCmd.Parameters.Add(param1);

            using (XmlReader xmlReader = sqlCmd.ExecuteXmlReader())
            {
                while (xmlReader.Read())
                {
                    xml.Load(xmlReader);
                }
            }
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return xml;
        }

        /// <summary>
        /// ExecuteXmlReader with Parametrs.
        /// </summary>
        /// <param name="ProcedureName">string</param>
        /// <param name="xmlDoc">Input XmlDocument</param>
        /// <returns> XmlDocument </returns>
        public XmlDocument ExecuteXmlReader(string procedureName, XmlDocument MultiXmlDoc, SqlConnection sqlCon)
        {
            XmlDocument xml = new XmlDocument();

            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlCommand sqlCmd = new SqlCommand(procedureName, sqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = new SqlParameter("@MultiXmlDoc", SqlDbType.Xml);
            param1.Value = MultiXmlDoc.InnerXml;
            sqlCmd.Parameters.Add(param1);

            using (XmlReader xmlReader = sqlCmd.ExecuteXmlReader())
            {
                while (xmlReader.Read())
                {
                    xml.Load(xmlReader);
                }
            }
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return xml;
        }

        /// <summary>
        /// ExecuteXmlReader with Parametrs.
        /// </summary>
        /// <param name="ProcedureName">string</param>
        /// <param name="xmlDoc">Input XmlDocument</param>
        /// <param name="MultiXmlDoc">Input XmlDocument</param>
        /// <returns> XmlDocument </returns>
        public XmlDocument ExecuteXmlReader(XmlDocument xmlDoc, XmlDocument MultiXmlDoc, string procedureName, SqlConnection sqlCon)
        {
            XmlDocument xml = new XmlDocument();

            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlCommand sqlCmd = new SqlCommand(procedureName, sqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = new SqlParameter("@XmlDoc", SqlDbType.Xml);
            param1.Value = xmlDoc.InnerXml;
            sqlCmd.Parameters.Add(param1);

            SqlParameter param2 = new SqlParameter("@MultiXmlDoc", SqlDbType.Xml);
            param2.Value = MultiXmlDoc.InnerXml;
            sqlCmd.Parameters.Add(param2);

            using (XmlReader xmlReader = sqlCmd.ExecuteXmlReader())
            {
                while (xmlReader.Read())
                {
                    xml.Load(xmlReader);
                }
            }
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return xml;
        }

        public DataSet ExecuteReader_Get(XmlDocument xmlDoc, string procedureName, SqlConnection sqlCon)
        {
            DataSet ds = new DataSet();
            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlDataAdapter sqlDap = new SqlDataAdapter(procedureName, sqlCon);
            sqlDap.SelectCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter param1 = new SqlParameter("@XmlDoc", SqlDbType.Xml);
            param1.Value = xmlDoc.InnerXml;
            sqlDap.SelectCommand.Parameters.Add(param1);
            sqlDap.Fill(ds);
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return ds;
        }

        public DataSet ExecuteReader_Get(string procedureName, SqlConnection sqlCon)
        {
            DataSet ds = new DataSet();
            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlDataAdapter sqlDap = new SqlDataAdapter(procedureName, sqlCon);
            sqlDap.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlDap.Fill(ds);
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return ds;
        }

        public SqlDataReader ExecuteDataReader_Get(XmlDocument xmlDoc, string procedureName, SqlConnection sqlCon)
        {
            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlCommand cmd = new SqlCommand(procedureName, sqlCon);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter param1 = new SqlParameter("@XmlDoc", SqlDbType.Xml);
            param1.Value = xmlDoc.InnerXml;
            cmd.Parameters.Add(param1);
            SqlDataReader dr = cmd.ExecuteReader();
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return dr;
        }

        public SqlDataReader ExecuteDataReader_Get(string procedureName, SqlConnection sqlCon)
        {
            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlCommand cmd = new SqlCommand(procedureName, sqlCon);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return dr;
        }

        public DataSet ExecuteReader_Get(XmlDocument xmlDoc, string procedureName, SqlConnection sqlCon, int CommandTimeout)
        {
            DataSet ds = new DataSet();
            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlDataAdapter sqlDap = new SqlDataAdapter(procedureName, sqlCon);
            sqlDap.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlDap.SelectCommand.CommandTimeout = CommandTimeout;
            SqlParameter param1 = new SqlParameter("@XmlDoc", SqlDbType.Xml);
            param1.Value = xmlDoc.InnerXml;
            sqlDap.SelectCommand.Parameters.Add(param1);
            sqlDap.Fill(ds);
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return ds;
        }

        public DataSet ExecuteReader_Get(string procedureName, SqlConnection sqlCon, int CommandTimeout)
        {
            DataSet ds = new DataSet();
            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlDataAdapter sqlDap = new SqlDataAdapter(procedureName, sqlCon);
            sqlDap.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlDap.SelectCommand.CommandTimeout = CommandTimeout;
            sqlDap.Fill(ds);
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return ds;
        }

        public SqlDataReader ExecuteDataReader_Get(XmlDocument xmlDoc, string procedureName, SqlConnection sqlCon, int CommandTimeout)
        {
            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlCommand cmd = new SqlCommand(procedureName, sqlCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = CommandTimeout;
            SqlParameter param1 = new SqlParameter("@XmlDoc", SqlDbType.Xml);
            param1.Value = xmlDoc.InnerXml;
            cmd.Parameters.Add(param1);
            SqlDataReader dr = cmd.ExecuteReader();
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return dr;
        }

        public SqlDataReader ExecuteDataReader_Get(string procedureName, SqlConnection sqlCon, int CommandTimeout)
        {
            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlCommand cmd = new SqlCommand(procedureName, sqlCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = CommandTimeout;
            SqlDataReader dr = cmd.ExecuteReader();
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return dr;
        }
    }
}
