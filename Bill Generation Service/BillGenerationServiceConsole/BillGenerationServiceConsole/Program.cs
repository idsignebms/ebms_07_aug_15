﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
//using System.ServiceProcess;
using System.Text;
using System.Timers;
using System.Xml;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.IO;
using System.Configuration;
using iDSignHelper;
using System.Globalization;
using System.Net;
using System.Net.Mail;

namespace BillGenerationServiceConsole
{
    public class Program
    {
        #region Main Method
        public static void Main()
        {
            BusinessLogic obj = new BusinessLogic();
            obj.ExecuteBillGenerationProcess();
            //Console.WriteLine("Bill Generation Service Executed at: "+DateTime.Now.ToString());
            //Console.ReadLine();
        }
        #endregion
    }
    public class BusinessLogic
    {
        #region deligate
        private delegate void Delegate_Insert_ClosedMonthData(string year, string month);
        #endregion

        #region Constructor
        public BusinessLogic()
        {
            var location = System.Reflection.Assembly.GetEntryAssembly().Location;
            var directoryPath = Path.GetDirectoryName(location);
            AutoGenBillLogsFileName = directoryPath + ConfigurationManager.AppSettings["AutoGenBillLogsFileName"].ToString();
        }
        #endregion

        #region Members
        iDsignBAL _objIdsignBal = new iDsignBAL();
        BillGenerationDal _ObjBillGenerationDal = new BillGenerationDal();
        BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
        BillGenerationListBe _ObjBillGenerationListBe = new BillGenerationListBe();
        string path = string.Empty;
        string AutoGenBillLogsFileName = string.Empty;
        XmlDocument xml = null;
        bool IsExists = false;
        string FilePathToUpdate = string.Empty;
        int TimeOut = 600;
        string BillMonthList = string.Empty;
        string BillYearList = string.Empty;
        #endregion

        #region Business Requirement

        public void ExecuteBillGenerationProcess()
        {
            CreateOrWriteServiceLogFile("/==---------------Bill Generation Starts---------------==/");
            CreateOrWriteServiceLogFile("Check for time execution.");
            if (!GetServiceTime())
            {
                CreateOrWriteServiceLogFile("Execution Matches.");
                GenerateScheduledBill();
                GetServiceCenterList();
                Insert_ClosedMonthData(BillYearList, BillMonthList);
                //Delegate_Insert_ClosedMonthData _objDelegate_Insert_ClosedMonthData = new Delegate_Insert_ClosedMonthData(Insert_ClosedMonthData);
            }
            else
            {
                CreateOrWriteServiceLogFile("Execution not match.");
            }
            CreateOrWriteServiceLogFile("/==---------------Bill Generation End---------------==/");
        }
        public bool GetServiceTime()
        {
            bool IsExists = false;

            xml = _ObjBillGenerationDal.ScheduleBillGenerationDALXML(_objIdsignBal.DoSerialize<BillGenerationBe>(_objBillGenerationBe), BillReturnType.CheckExecutionTime);
            if (xml != null)
            {
                _objBillGenerationBe = _objIdsignBal.DeserializeFromXml<BillGenerationBe>(xml);
                IsExists = _objBillGenerationBe.IsExists;
            }
            return IsExists;
        }
        public void GenerateScheduledBill()
        {
            CreateOrWriteServiceLogFile("Bill Generation Started.");
            try
            {
                TimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["SqlTimeOut"].ToString());
                _ObjBillGenerationDal.ScheduleBillGenerationDALDATASET(BillReturnType.GenerateScheduledBill, TimeOut);
                CreateOrWriteServiceLogFile("Bill Generated Successfully");
            }
            catch (Exception ex)
            {
                CreateOrWriteServiceLogFile("Bill Not Generated");
                CreateOrWriteServiceLogFile("Found Exception in Bill Generation: " + ex.Message.ToString());
            }
            CreateOrWriteServiceLogFile("Bill Generation Ended.");
        }
        public void GetServiceCenterList()
        {
            CreateOrWriteServiceLogFile("Get Service Center List Started.");
            //int RowNumber = 0;
            string feederId = string.Empty;
            string cycleId = string.Empty;
            string CycleList = string.Empty;
            int billingQueueScheduleId = 0;
            int EmilCount = 0;
            string fileName = null;
            string filePath = null;
            bool IsFileGenValid = true;
            //string fileNameList = null;
            //string filePathList = null;
            DataSet dsGeneratedBill = new DataSet();
            //string CurrentCycleId = null;
            //string CurrentLoopCycleId = null;
            XmlDocument SCList = new XmlDocument();
            BillGenerationListBe _ObjBillGenerationListBeList = new BillGenerationListBe();
            xml = _ObjBillGenerationDal.ScheduleBillGenerationDALXML(_objIdsignBal.DoSerialize<BillGenerationBe>(_objBillGenerationBe), BillReturnType.GetServiceCenterList);
            if (xml != null)
            {
                CreateOrWriteServiceLogFile("Fetched Service Center List Successfully.");
                _ObjBillGenerationListBe = _objIdsignBal.DeserializeFromXml<BillGenerationListBe>(xml);
                if (_ObjBillGenerationListBe.Items.Count > 0)
                {
                    CreateOrWriteServiceLogFile("In loop of Service Center.");
                    foreach (BillGenerationBe item in _ObjBillGenerationListBe.Items) //SERVICE CENTER FOREACH
                    {
                        UpdateBillStatusToGenerated(item.ServiceCenterID, item.Month, item.Year);

                        CreateOrWriteServiceLogFile("Current Service Center: " + item.ServiceCenterName);
                        if (item.CycleId != null)
                            cycleId = item.CycleId;
                        if (item.FeederId != null)
                            feederId = item.FeederId;

                        billingQueueScheduleId = item.BillingQueueScheduleId;
                        CycleList += item.CycleId;
                        BillMonthList += item.Month + "|";
                        BillYearList += item.Year + "|";

                        //================Finding the Directory from where we are executing the WS.exe==========//
                        var location = System.Reflection.Assembly.GetEntryAssembly().Location;
                        var directoryPath = Path.GetDirectoryName(location);
                        var BillGenerationFolderPath = directoryPath + "\\GeneratedBills";
                        //=====================================================================================//


                        //string BUPath = path + "\\" + item.BusinessUnitName + "\\";
                        string BUPath = BillGenerationFolderPath + "\\" + item.BusinessUnitName + "\\";
                        //FilePathToUpdate = "~\\" + item.BusinessUnitName + "\\";
                        FilePathToUpdate = item.BusinessUnitName + "\\";
                        string SUPath = BUPath + item.ServiceUnitName + "\\";
                        FilePathToUpdate += item.ServiceUnitName + "\\";
                        string SCPath = SUPath + item.ServiceCenterName + "\\";
                        FilePathToUpdate += item.ServiceCenterName + "\\";
                        string MonthPath = SCPath + item.MonthName + item.Year.ToString() + "\\";
                        FilePathToUpdate += item.MonthName + item.Year.ToString() + "\\";
                        try
                        {
                            CreateOrWriteServiceLogFile("Directory Creation Started.");

                            if (!string.IsNullOrEmpty(item.BusinessUnitName))
                            {
                                CreateDirectory(BUPath);// Create BU Directory
                            }
                            if (!string.IsNullOrEmpty(item.ServiceUnitName))
                            {
                                CreateDirectory(SUPath);// Create SU Directory
                            }
                            if (!string.IsNullOrEmpty(item.ServiceCenterName))
                            {
                                CreateDirectory(SCPath);// Create SC Directory
                            }
                            if (!string.IsNullOrEmpty(item.MonthName))
                            {
                                CreateDirectory(MonthPath);// Create Month Directory
                            }
                            CreateOrWriteServiceLogFile("Directries Created Successfully.");
                        }
                        catch (Exception ex)
                        {
                            CreateOrWriteServiceLogFile("Directries Not Created.");
                            CreateOrWriteServiceLogFile("Found Exception in Bill Directory Creation: " + ex.Message.ToString());
                        }
                        //fileName = item.ServiceCenterName + ".txt"; //+"_" + DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".txt"; //Modification Req
                        //File name will be SCName-BUName --Faiz-ID103 - 05-May-2015
                        fileName = item.ServiceCenterName + "-" + item.BusinessUnitName + ".txt";

                        //set up a filestream
                        //string filePath = Server.MapPath(ConfigurationManager.AppSettings["BillGeneration"].ToString()) + Path.GetFileName(fileName);
                        filePath = MonthPath + fileName;
                        FilePathToUpdate += fileName;
                        CreateOrWriteServiceLogFile("File existence check.");
                        CreateOrWriteServiceLogFile(filePath);
                        if (File.Exists(filePath))
                        {
                            try
                            {
                                CreateOrWriteServiceLogFile("File is already exists.");
                                File.Delete(filePath);
                                CreateOrWriteServiceLogFile("Deleted file at :" + filePath);
                            }
                            catch (Exception ex)
                            {
                                CreateOrWriteServiceLogFile("Found Exception in File Deletion: " + ex.Message.ToString());
                            }
                        }
                        else
                        {
                            CreateOrWriteServiceLogFile("File is not exists.");
                        }

                        CreateOrWriteServiceLogFile("File generate starts.");

                        try
                        {
                            // Modification Get Customers Bills By Passing Service Center.
                            dsGeneratedBill = GetGeneratedBillDetails(item.ServiceCenterID, item.Month, item.Year);
                            if (dsGeneratedBill.Tables.Count > 0)
                            {
                                CreateOrWriteServiceLogFile("Bill table found.");
                                if (dsGeneratedBill.Tables[0].Rows.Count > 0)
                                {
                                    CreateOrWriteServiceLogFile("Total records for file generation: " + dsGeneratedBill.Tables[0].Rows.Count);

                                    using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write))
                                    {
                                        //set up a streamwriter for adding text
                                        using (StreamWriter sw = new StreamWriter(fs))
                                        {
                                            //find the end of the underlying filestream
                                            sw.BaseStream.Seek(0, SeekOrigin.End);
                                            //string accountNumbers = string.Empty;
                                            int rowCount = 0;
                                            string billDetails = string.Empty;
                                            EmilCount++;
                                            CreateOrWriteServiceLogFile("Bill loop started.");
                                            foreach (DataRow dr in dsGeneratedBill.Tables[0].Rows)
                                            {
                                                rowCount = rowCount + 1;
                                                decimal previouseBalanceAmount = Convert.ToDecimal(dr["PreviousBalance"]);
                                                string previouseBalnce = GetCurrencyFormat(previouseBalanceAmount, 2, 2);//string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", previouseBalanceAmount);
                                                string str = null;
                                                string[] strArr = null;
                                                str = dr["AdjustmentAmmount"].ToString().TrimEnd();
                                                char[] splitchar = { ' ' };
                                                strArr = str.Split(splitchar);
                                                string adjustment = "0.00";
                                                decimal adjustmentAmount = Convert.ToDecimal(strArr[0]);
                                                if (adjustmentAmount != 0)
                                                    adjustment = GetCurrencyFormat(adjustmentAmount, 2, 2);//string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", adjustmentAmount) + " " + strArr[1];

                                                //if (strArr[1] == "DR")
                                                //{
                                                //    adjustmentAmount = adjustmentAmount * -1;
                                                //}


                                                string totalPayment = "0.00";
                                                decimal totalPaymentAmount = 0;
                                                if (!string.IsNullOrWhiteSpace(dr["TotalPayment"].ToString()))
                                                {
                                                    totalPaymentAmount = Convert.ToDecimal(dr["TotalPayment"]);
                                                    totalPayment = GetCurrencyFormat(totalPaymentAmount, 2, 2);//string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", totalPaymentAmount);
                                                }

                                                decimal netFixedChargesAmount = Convert.ToDecimal(dr["NetFixedCharges"]);
                                                string netFixedCharges = GetCurrencyFormat(netFixedChargesAmount, 2, 2);//string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", netFixedChargesAmount);

                                                decimal netArrearsAmount = previouseBalanceAmount - adjustmentAmount - totalPaymentAmount;
                                                string netArrears = GetCurrencyFormat(netArrearsAmount, 2, 2);//string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", netArrearsAmount);

                                                decimal netEnergyChargesAmount = Convert.ToDecimal(dr["NetEnergyCharges"]);
                                                string netEnergyCharges = GetCurrencyFormat(netEnergyChargesAmount, 2, 2);//string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", netEnergyChargesAmount);

                                                //double lastPaidAmount = Convert.ToDouble(dr["LastPaidAmount"]);
                                                //string lastPaidAmountValue = string.Format(CultureInfo.InvariantCulture, "{0:#,#}", netEnergyChargesAmount);

                                                decimal vatAmount = Convert.ToDecimal(dr["VAT"]);
                                                string vat = GetCurrencyFormat(vatAmount, 2, 2);//string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", vatAmount);

                                                decimal totalBillAmount = Convert.ToDecimal(dr["TotalBillAmount"]);
                                                string totalBill = GetCurrencyFormat(totalBillAmount, 2, 2);//string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", totalBillAmount);

                                                decimal totalBillAmountWithTaxAmount = totalBillAmount + netArrearsAmount + vatAmount;
                                                string totalBillAmountWithTax = GetCurrencyFormat(totalBillAmountWithTaxAmount, 2, 2);//string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", totalBillAmountWithTaxAmount);


                                                //accountNumbers = string.IsNullOrEmpty(accountNumbers) ? dr["AccountNo"].ToString() : accountNumbers + "," + dr["AccountNo"].ToString();
                                                billDetails += string.Format("{0,-6}#{1,-107}#{2,-11}", string.Empty, rowCount, rowCount).Replace('#', ' ') +
                                                            Environment.NewLine + string.Format("{0,-15}#{1,-74}#{2,-11}#{3,-6}", "Route: 0", "Seq.", "Route: 0", "Seq.").Replace('#', ' ') +
                                                            Environment.NewLine + Environment.NewLine +
                                                            Environment.NewLine + string.Format("{0,-34}#{1,-63}#{2,-25}", string.Empty, dr["BusinessUnitName"].ToString() + " BUSINESS UNIT", dr["BusinessUnitName"].ToString() + " Business Unit").Replace('#', ' ') +
                                                            Environment.NewLine + string.Format("{0,-23}#{1,-74}#{2,-9}", string.Empty, "PLS PAY AT ANY BEDC OFFICE OR DESIGNATED BANKS", dr["MonthName"].ToString() + dr["BillYear"].ToString()).Replace('#', ' ') +
                                                            Environment.NewLine + string.Format("{0,-31}#{1,-27}#{2,-36}#{3,-22}", string.Empty, dr["ClassName"] + " Account", dr["MonthName"].ToString() + dr["BillYear"].ToString(), dr["ClassName"] + " Account", dr["MonthName"].ToString() + dr["BillYear"].ToString()).Replace('#', ' ') +
                                                             Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", "Bill No ", dr["BillNo"].ToString(), string.Empty, "Bill No", dr["BillNo"].ToString()).Replace('#', ' ') +
                                                            Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, dr["AccountNo"].ToString(), string.Empty, string.Empty, dr["AccountNo"].ToString()).Replace('#', ' ') +
                                                            Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, dr["Name"].ToString(), dr["LastDueDate"].ToString(), previouseBalnce, dr["Name"].ToString()).Replace('#', ' ') +
                                                            Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, dr["ServiceAddress"].ToString() + " ", dr["MeterNo"].ToString(), totalPayment, dr["ServiceAddress"].ToString()).Replace('#', ' ') +
                                                    //Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, dr["PostalZipCode"].ToString(), netFixedCharges, adjustment, dr["ServiceHouseNo"].ToString() + " " + dr["ServiceStreet"].ToString()).Replace('#', ' ') +
                                                    //Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, dr["PostalZipCode"].ToString(), dr["ADC"], adjustment, dr["ServiceHouseNo"].ToString() + " " + dr["ServiceStreet"].ToString()).Replace('#', ' ') +
                                                            Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, dr["PostalZipCode"].ToString(), dr["ADC"], adjustment, string.Empty + " " + string.Empty).Replace('#', ' ') +
                                                            Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, string.Empty, dr["Dials"].ToString(), netArrears, dr["ServiceZipCode"].ToString()).Replace('#', ' ') +
                                                            Environment.NewLine + string.Format("{0,8}#{1,-90}#{2,-15}", string.Empty, string.Format("THIS BILLING IS FOR {0} CONSUMPTION.", dr["BillingMonthName"]), dr["MeterNo"].ToString()).Replace('#', ' ') +
                                                            Environment.NewLine + Environment.NewLine + Environment.NewLine +
                                                            Environment.NewLine + string.Format("{0,-13}#{1,-7}#{2,-16}#{3,-12}#{4,-7}#{5,-7}#{6,7}#{7,11}", "ENERGY", dr["TariffName"].ToString(), dr["ReadDate"].ToString(), dr["PresentReading"].ToString(), dr["PreviousReading"].ToString(), dr["Multiplier"].ToString(), dr["Usage"].ToString(), netEnergyCharges).Replace('#', ' ') +
                                                            Environment.NewLine +
                                                            Environment.NewLine + string.Format("{0,-13}#{1,-7}#{2,-16}#{3,-12}#{4,-7}#{5,-7}#{6,7}#{7,11}", "FIXED", dr["TariffName"].ToString(), "----", "----", "----", "----", "----", netFixedCharges).Replace('#', ' ') +
                                                            Environment.NewLine + string.Format("{0,-22}#{1,-19}", string.Empty, "Billing Periods: 01").Replace('#', ' ') +
                                                            Environment.NewLine +
                                                            Environment.NewLine + string.Format("{0,-33}#{1,-22}", "LAST PAYMENT DATE  " + dr["LastPaymentDate"].ToString(), "AMOUNT      " + dr["LastPaidAmount"].ToString()).Replace('#', ' ') +
                                                            Environment.NewLine + string.Format("{0,-100}#{1,-11}", "RECONNECTION FEE IS NOW =N=5000", dr["LastDueDate"].ToString()).Replace('#', ' ') +
                                                            Environment.NewLine + string.Format("{0,-70}#{1,17}#{2,27}", "PAY WITHIN " + dr["LastDueDays"].ToString() + " DAYS TO AVOID DISCONNECTION", netArrears, netArrears).Replace('#', ' ') +
                                                            Environment.NewLine + string.Format("{0,-70}#{1,17}#{2,27}", string.Empty, totalBill, totalBill).Replace('#', ' ') +
                                                            Environment.NewLine + string.Format("{0,-70}#{1,17}#{2,27}", string.Empty, vat, vat).Replace('#', ' ') +
                                                            Environment.NewLine +
                                                            Environment.NewLine + string.Format("{0,-70}#{1,17}#{2,27}", string.Empty, totalBillAmountWithTax, totalBillAmountWithTax).Replace('#', ' ') +
                                                            Environment.NewLine +
                                                    //Environment.NewLine + string.Format("{0,8}#{1,-19}#{2,-77}#{3,-16}", string.Empty, dr["OldAccountNo"].ToString(), "ENR2S- Rate:=N= 11.37", dr["OldAccountNo"].ToString()).Replace('#', ' ') +
                                                    Environment.NewLine + string.Format("{0,8}#{1,-19}#{2,-77}#{3,-16}", string.Empty, dr["OldAccountNo"].ToString(), "EN" + dr["TariffName"].ToString() + "- Rate:=N= " + (!string.IsNullOrEmpty(Convert.ToString(dr["EnergyCharges"])) ? dr["EnergyCharges"].ToString() : "--") + "", dr["OldAccountNo"].ToString()).Replace('#', ' ') +
                                                            Environment.NewLine + string.Format("{0,-2}#{1,-60}", string.Empty, "THANKS FOR YOUR PATRONAGE").Replace('#', ' ') +
                                                            Environment.NewLine + string.Format("{0,-5}#{1,-26}#{2,-61}", string.Empty, "", " ").Replace('#', ' ') +
                                                            Environment.NewLine;
                                                //+ Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine +
                                                // "-------------------------------------------------------------------------------------------------------------------" + Environment.NewLine;
                                                //}
                                            }
                                            CreateOrWriteServiceLogFile("Bill loop end.");
                                            //add the text
                                            sw.WriteLine(billDetails.ToUpper());
                                        }
                                    }
                                    //Sream Writing End
                                    UpdateBillStatusToFileGenrated(item.ServiceCenterID, item.Month, item.Year, FilePathToUpdate, filePath);

                                    //Checking bill in schedule
                                    CreateOrWriteServiceLogFile("Check for next Service Center bill generation.");
                                    SCList = _ObjBillGenerationDal.ScheduleBillGenerationDALXML(_objIdsignBal.DoSerialize<BillGenerationBe>(_objBillGenerationBe), BillReturnType.IsBillInSchedule);
                                    if (SCList != null)
                                    {
                                        _ObjBillGenerationListBeList = _objIdsignBal.DeserializeFromXml<BillGenerationListBe>(SCList);
                                        if (_ObjBillGenerationListBeList.Items[0].IsExists)
                                        {
                                            CreateOrWriteServiceLogFile("Found Service Center for bill generation.");
                                            ExecuteBillGenerationProcess();
                                        }
                                        else
                                            CreateOrWriteServiceLogFile("No Service Center found for bill generation.");
                                    }
                                    else
                                        CreateOrWriteServiceLogFile("No Service Center found for bill generation.");
                                }
                                else
                                {
                                    CreateOrWriteServiceLogFile("No records found in table.");
                                }
                            }
                            else
                            {
                                CreateOrWriteServiceLogFile("No table found.");
                            }
                        }
                        catch (Exception ex)
                        {
                            IsFileGenValid = false;
                            CreateOrWriteServiceLogFile("Found Exception in File Generation: " + ex.Message.ToString());
                        }
                        CreateOrWriteServiceLogFile("File generate end.");
                    }
                }
                else
                {
                    CreateOrWriteServiceLogFile("No Service Center List Found.");
                }
            }

            CreateOrWriteServiceLogFile("Get Service Center List Ended.");
        }
        public DataSet GetGeneratedBillDetails(string ServiceCenterID, int Month, int Year)
        {
            DataSet ds = new DataSet();
            _objBillGenerationBe.ServiceCenterID = ServiceCenterID;
            _objBillGenerationBe.Month = Month;
            _objBillGenerationBe.Year = Year;
            ds = _ObjBillGenerationDal.ScheduleBillGenerationDALDATASET(_objIdsignBal.DoSerialize<BillGenerationBe>(_objBillGenerationBe), BillReturnType.GetGeneratedBillDetails);
            return ds;
        }
        private void CreateDirectory(string path)
        {
            bool exists = System.IO.Directory.Exists(path);

            if (!exists)
                System.IO.Directory.CreateDirectory(path);
        }
        public void CreateOrWriteServiceLogFile(string text)
        {
            //set up a filestream
            using (FileStream fs = new FileStream(AutoGenBillLogsFileName, FileMode.OpenOrCreate, FileAccess.Write))
            {

                //set up a streamwriter for adding text
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    //find the end of the underlying filestream
                    sw.BaseStream.Seek(0, SeekOrigin.End);

                    //add the text
                    sw.WriteLine(DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ":" + text);
                    //add the text to the underlying filestream

                    //sw.Flush();
                    ////close the writer
                    //sw.Close();
                }
            }
        }
        public int UpdateBillStatusToGenerated(string ServiceCenterID, int Month, int Year)
        {
            try
            {
                CreateOrWriteServiceLogFile("Generated bills status updating to GENERATED starts.");
                _objBillGenerationBe.ServiceCenterID = ServiceCenterID;
                _objBillGenerationBe.Month = Month;
                _objBillGenerationBe.Year = Year;
                xml = _ObjBillGenerationDal.ScheduleBillGenerationDALXML(_objIdsignBal.DoSerialize<BillGenerationBe>(_objBillGenerationBe), BillReturnType.UpdateBillStatusToGenerated);
                if (xml != null)
                {
                    _objBillGenerationBe = _objIdsignBal.DeserializeFromXml<BillGenerationBe>(xml);
                    if (_objBillGenerationBe.RowsEffected > 0) CreateOrWriteServiceLogFile("Generated bills status updated to GENERATED successfully.");
                    else CreateOrWriteServiceLogFile("Generated bills status not updated to GENERATED.");
                }
            }
            catch (Exception ex)
            {
                CreateOrWriteServiceLogFile("Found exception in status updating to GENERATED.");
                CreateOrWriteServiceLogFile("Following are the error: " + ex.Message.ToString());
            }
            CreateOrWriteServiceLogFile("Generated bills status updating to GENERATED end.");
            return _objBillGenerationBe.RowsEffected;
        }
        public int UpdateBillStatusToFileGenrated(string ServiceCenterID, int Month, int Year, string FilePath, string AttachmentPath)
        {
            try
            {
                CreateOrWriteServiceLogFile("Generated bills status updating to FILE GENERATED starts.");
                _objBillGenerationBe.ServiceCenterID = ServiceCenterID;
                _objBillGenerationBe.Month = Month;
                _objBillGenerationBe.Year = Year;
                _objBillGenerationBe.FilePath = FilePath;
                xml = _ObjBillGenerationDal.ScheduleBillGenerationDALXML(_objIdsignBal.DoSerialize<BillGenerationBe>(_objBillGenerationBe), BillReturnType.UpdateBillStatusToFileGenrated);
                if (xml != null) _objBillGenerationBe = _objIdsignBal.DeserializeFromXml<BillGenerationBe>(xml);
                {
                    _objBillGenerationBe = _objIdsignBal.DeserializeFromXml<BillGenerationBe>(xml);
                    if (_objBillGenerationBe.RowsEffected > 0)
                    {
                        CreateOrWriteServiceLogFile("Generated bills status updated to FILE GENERATED successfully.");
                        _objBillGenerationBe = new BillGenerationBe();
                        _objBillGenerationBe.ServiceCenterID = ServiceCenterID;
                        xml = _ObjBillGenerationDal.ScheduleBillGenerationDALXML(_objIdsignBal.DoSerialize<BillGenerationBe>(_objBillGenerationBe), BillReturnType.GetCycleEmailListByServiceCenterID);
                        if (xml != null) _objBillGenerationBe = _objIdsignBal.DeserializeFromXml<BillGenerationBe>(xml);
                        {
                            string emailID = Convert.ToString(ConfigurationManager.AppSettings["AutoGenBillLogsFileName"]);
                            if (!string.IsNullOrEmpty(emailID))
                                SendMailToBookGroup(ServiceCenterID, emailID, Month, Year, AttachmentPath);
                            else CreateOrWriteServiceLogFile("Configuration email Id not available.");
                        }
                    }
                    else CreateOrWriteServiceLogFile("Generated bills status not updated to FILE GENERATED.");
                }
            }
            catch (Exception ex)
            {
                CreateOrWriteServiceLogFile("Found exception in status updating to FILE GENERATED.");
                CreateOrWriteServiceLogFile("Following are the error: " + ex.Message.ToString());
            }
            CreateOrWriteServiceLogFile("Generated bills status updating to FILE GENERATED end.");
            return _objBillGenerationBe.RowsEffected;
        }
        public int UpdateBillStatusToEmailSent(string ServiceCenterID, int Month, int Year)
        {
            try
            {
                CreateOrWriteServiceLogFile("Generated bills status updating to EMAIL SENT starts.");
                _objBillGenerationBe.ServiceCenterID = ServiceCenterID;
                _objBillGenerationBe.Month = Month;
                _objBillGenerationBe.Year = Year;
                xml = _ObjBillGenerationDal.ScheduleBillGenerationDALXML(_objIdsignBal.DoSerialize<BillGenerationBe>(_objBillGenerationBe), BillReturnType.UpdateBillStatusToEmailSent);
                if (xml != null) _objBillGenerationBe = _objIdsignBal.DeserializeFromXml<BillGenerationBe>(xml);
                {
                    _objBillGenerationBe = _objIdsignBal.DeserializeFromXml<BillGenerationBe>(xml);
                    if (_objBillGenerationBe.RowsEffected > 0)
                    {
                        CreateOrWriteServiceLogFile("Generated bills status updated to FILE EMAIL SENT successfully.");
                        UpdateBillStatusToComplete(ServiceCenterID, Month, Year);
                    }
                    else CreateOrWriteServiceLogFile("Generated bills status not updated to EMAIL SENT.");
                }
            }
            catch (Exception ex)
            {
                CreateOrWriteServiceLogFile("Found exception in status updating to EMAIL SENT.");
                CreateOrWriteServiceLogFile("Following are the error: " + ex.Message.ToString());
            }
            CreateOrWriteServiceLogFile("Generated bills status updating to EMAIL SENT end.");
            return _objBillGenerationBe.RowsEffected;
        }
        public int UpdateBillStatusToComplete(string ServiceCenterID, int Month, int Year)
        {
            try
            {
                CreateOrWriteServiceLogFile("Generated bills status updating to COMPLETE starts.");
                _objBillGenerationBe.ServiceCenterID = ServiceCenterID;
                _objBillGenerationBe.Month = Month;
                _objBillGenerationBe.Year = Year;
                xml = _ObjBillGenerationDal.ScheduleBillGenerationDALXML(_objIdsignBal.DoSerialize<BillGenerationBe>(_objBillGenerationBe), BillReturnType.UpdateBillStatusToComplete);
                if (xml != null) _objBillGenerationBe = _objIdsignBal.DeserializeFromXml<BillGenerationBe>(xml);
                {
                    _objBillGenerationBe = _objIdsignBal.DeserializeFromXml<BillGenerationBe>(xml);
                    if (_objBillGenerationBe.RowsEffected > 0) CreateOrWriteServiceLogFile("Generated bills status updated to FILE COMPLETE successfully.");
                    else CreateOrWriteServiceLogFile("Generated bills status not updated to COMPLETE.");
                }
            }
            catch (Exception ex)
            {
                CreateOrWriteServiceLogFile("Found exception in status updating to COMPLETE.");
                CreateOrWriteServiceLogFile("Following are the error: " + ex.Message.ToString());
            }
            CreateOrWriteServiceLogFile("Generated bills status updating to COMPLETE end.");
            return _objBillGenerationBe.RowsEffected;
        }
        public DataSet GetCycleEmailListByServiceCenterID(string ServiceCenterID)
        {
            DataSet ds = new DataSet();
            _objBillGenerationBe.ServiceCenterID = ServiceCenterID;
            ds = _ObjBillGenerationDal.ScheduleBillGenerationDALDATASET(_objIdsignBal.DoSerialize<BillGenerationBe>(_objBillGenerationBe), BillReturnType.GetCycleEmailListByServiceCenterID);
            return ds;
        }
        public void SendMailToBookGroup(string ServiceCenterID, string BookGroupEmailList, int Month, int Year, string filePath)
        {
            try
            {
                CreateOrWriteServiceLogFile("Email send starts.");
                string smtpUserName = ConfigurationManager.AppSettings["SMTPUsername"].ToString();
                string smtpPassword = ConfigurationManager.AppSettings["SMTPPassword"].ToString();
                string smtpServer = ConfigurationManager.AppSettings["SMTPServer"].ToString();
                string smtpPort = ConfigurationManager.AppSettings["SMTPPort_TLS"].ToString();
                var EmailSubject = Convert.ToString(ConfigurationManager.AppSettings["EmailSubject"]);
                NetworkCredential cred = new NetworkCredential(smtpUserName, smtpPassword);
                MailMessage msg = new MailMessage();
                msg.To.Add(BookGroupEmailList);
                msg.Subject = (!string.IsNullOrEmpty(EmailSubject) ? EmailSubject : "BEDC Bill GENERATED");
                msg.Attachments.Add(new Attachment(filePath));
                msg.Body = "Bills are successfully generated. <br />Billing Month: " + Month + "-" + Year.ToString() + "<br />Generated file has been attached with this mail.<br /><br />Thank you!<br />EBMS TEAM";
                msg.From = new MailAddress(smtpUserName, "BEDC Department Of Bill Generation"); // Your Email Id
                msg.Bcc.Add("Jeevan Amunuri <jeevan.a@idsigntechnologies.com>,neeraj.k@idsigntechnologies.com");
                SmtpClient client = new SmtpClient(smtpServer, Convert.ToInt32(smtpPort));
                //SmtpClient client1 = new SmtpClient("smtp.mail.yahoo.com", 465);
                client.Credentials = cred;
                client.EnableSsl = true;
                msg.IsBodyHtml = true;
                client.Send(msg);
                CreateOrWriteServiceLogFile("Email sent to " + ServiceCenterID + " and Email IDs: " + BookGroupEmailList + " successfully.");
                CreateOrWriteServiceLogFile("Email send ends.");
            }
            catch (Exception ex)
            {
                CreateOrWriteServiceLogFile("Email couldn't send.");
                CreateOrWriteServiceLogFile("Found exception in email send for Service Center ID: ." + ServiceCenterID);
                CreateOrWriteServiceLogFile("Following are the error: " + ex.Message.ToString());
                CreateOrWriteServiceLogFile("Email send ends.");
            }
            finally
            {
                CreateOrWriteServiceLogFile("Email send status update statrs.");
                UpdateBillStatusToEmailSent(ServiceCenterID, Month, Year);
                CreateOrWriteServiceLogFile("Email send status update ends.");
            }
        }
        public string GetCurrencyFormat(decimal value, int decimalPlaces, int Value)
        {
            value = Math.Round(value, decimalPlaces);
            bool isNegative = false;
            if (value < 0)
            {
                isNegative = true;
                value = value * -1;
            }
            string result = string.Empty;
            NumberFormatInfo nfo = new NumberFormatInfo();
            nfo.CurrencyGroupSeparator = ",";
            if
            (Value == 1)
            {
                nfo.CurrencyGroupSizes = new int[] { 3, 2 };
            }
            else if (Value == 2)
            {
                nfo.CurrencyGroupSizes = new int[] { 3, 3 };
            }
            nfo.CurrencySymbol = "";
            if (value.ToString().Contains('.'))
            {
                string[] list = value.ToString().Split('.');
                //if (Convert.ToDecimal(list[1]) > 0)
                //{
                result = value.ToString("c" + decimalPlaces.ToString(), nfo);// TO get decimals if the value is having '.'
                //}
                //else
                //{
                //    result = value.ToString("c0", nfo);
                //}
            }
            else
            {
                //To show the decimals if there is decimal places greater than 0
                if (decimalPlaces > 0)
                {
                    result = value.ToString("c" + decimalPlaces.ToString(), nfo);
                }
                else
                {
                    result = value.ToString("c0", nfo);
                }
                //result = value.ToString("c0", nfo);
            }
            if (isNegative)
            {
                result = "-" + result;
            }
            return result;
        }
        public void Insert_ClosedMonthData(string year, string month)
        {
            CreateOrWriteServiceLogFile("Insert close month data starts.");
            try
            {
                BillGenerationBe objBE = new BillGenerationBe();
                objBE.BillMonthList = month;
                objBE.BillYearList = year;
                objBE.ModifiedBy = "Console-Service";
                _ObjBillGenerationDal.ScheduleBillGenerationDALXML(_objIdsignBal.DoSerialize<BillGenerationBe>(objBE), BillReturnType.InsertCloseMonthData);
                CreateOrWriteServiceLogFile("Insert close month data completed.");
            }
            catch (Exception ex)
            {
                CreateOrWriteServiceLogFile("Found following exception: ." + ex.Message.ToString());
            }
            CreateOrWriteServiceLogFile("Insert close month data starts.");
        }
        public DataSet GetBillDetailsPdfGen(int BillMonth, int BillYear)
        {
            DataSet ds = new DataSet();
            BillGenerationBe objBillGenerationBe = new BillGenerationBe();
            objBillGenerationBe.BillMonth = BillMonth;
            objBillGenerationBe.BillYear = BillYear;
            ds = _ObjBillGenerationDal.ScheduleBillGenerationDALDATASET(_objIdsignBal.DoSerialize<BillGenerationBe>(objBillGenerationBe), BillReturnType.GetCycleEmailListByServiceCenterID);

            return ds;
        }
        #endregion
    }
}
