﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Business logic for Emai Send System
                     
 Developer        : Id077-Neeraj Kanojiya
 Creation Date    : 22-July-2015
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Configuration;
using System.IO;
using Communication.EmailInput;

namespace Communication.Mail
{
    public class Email
    {
        #region Memeters

        string AutoGenBillLogsFileName = string.Empty;
        private string SuccessfulMail = string.Empty;

        #endregion

        #region Constructor

        public Email()
        {
            var location = System.IO.Path.GetDirectoryName(new System.Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath);
            //var location = System.Reflection.Assembly.GetEntryAssembly().Location;
            var directoryPath = Path.GetDirectoryName(location);
            AutoGenBillLogsFileName = directoryPath + ConfigurationManager.AppSettings["EmailSystemLog"].ToString();
        }

        #endregion

        #region Methods


        public bool SendMail(EmailInput.EmailInput objEmailBE)
        {
            bool IsMailSent = false;
            try
            {
                using (MailMessage mailMessage = new MailMessage())
                {
                    mailMessage.From = new MailAddress(objEmailBE.FromEmail);
                    mailMessage.Subject = objEmailBE.Subject;
                    mailMessage.Body = objEmailBE.EmailBody;
                    mailMessage.IsBodyHtml = true;
                    if (!string.IsNullOrEmpty(objEmailBE.To))
                    {
                        string ToMail = objEmailBE.To.ToString();
                        string[] Mails = ToMail.Split(';');
                        if (Mails.Length > 1)
                        {
                            for (int i = 0; i < Mails.Length; i++)
                            {
                                mailMessage.To.Add(new MailAddress(Mails[i]));
                            }
                        }
                        else
                            mailMessage.To.Add(new MailAddress(objEmailBE.To));
                    }
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = objEmailBE.SMTPServer;
                    smtp.EnableSsl = objEmailBE.EnableSsl;
                    System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                    NetworkCred.UserName = objEmailBE.SMTPUsername;
                    NetworkCred.Password = objEmailBE.SMTPPassword;
                    try
                    {
                        if (!string.IsNullOrEmpty(objEmailBE.Attachment))
                            mailMessage.Attachments.Add(new Attachment(objEmailBE.Attachment));
                    }
                    catch (Exception ex)
                    {
                        CreateOrWriteServiceLogFile("Email attachement could not sent to :" + objEmailBE.To + ".\nDue to following exception: " + ex.Message.ToString());
                    }
                    smtp.UseDefaultCredentials = objEmailBE.UserDefaultCredentials;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = objEmailBE.SMTPPort;
                    smtp.Send(mailMessage);
                    mailMessage.To.Clear();
                    CreateOrWriteServiceLogFile("Email sent to: " + objEmailBE.To);
                    IsMailSent = true;
                }
            }
            catch (Exception ex)
            {
                IsMailSent = false;
                CreateOrWriteServiceLogFile("Email could not sent to :" + objEmailBE.To + ".\nDue to following exception: " + ex.Message.ToString());
            }
            finally
            {
                objEmailBE = null;
            }
            return IsMailSent;
        }

        private void CreateOrWriteServiceLogFile(string text)
        {
            //set up a filestream
            using (FileStream fs = new FileStream(AutoGenBillLogsFileName, FileMode.OpenOrCreate, FileAccess.Write))
            {

                //set up a streamwriter for adding text
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    //find the end of the underlying filestream
                    sw.BaseStream.Seek(0, SeekOrigin.End);

                    //add the text
                    sw.WriteLine(DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ":" + text);
                    //add the text to the underlying filestream

                    //sw.Flush();
                    ////close the writer
                    //sw.Close();
                }
            }
        }

        #endregion
    }
}
