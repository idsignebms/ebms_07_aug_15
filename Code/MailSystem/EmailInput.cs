﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Entities for Emai Send System
                     
 Developer        : Id077-Neeraj Kanojiya
 Creation Date    : 22-July-2015
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Communication.EmailInput
{
    public class EmailInput
    {
        #region int
        public int SMTPPort { get; set; }
        #endregion

        #region string

        public string FromEmail { get; set; }
        public string Subject { get; set; }
        public string EmailBody { get; set; }
        public string To { get; set; }
        public string SMTPServer { get; set; }
        public string SMTPUsername { get; set; }
        public string SMTPPassword { get; set; }
        public string Attachment { get; set; }
        #endregion

        #region bool
        public bool IsBodyHtml { get; set; }
        public bool UserDefaultCredentials { get; set; }
        public bool EnableSsl { get; set; }
        #endregion
    }
}
