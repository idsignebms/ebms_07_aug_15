﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

namespace Communication
{
   public class GeneratePDF
    {
        public void ConvertHTMLToPDF(string HTMLCode, string FilePath)
        {
            HttpContext context = HttpContext.Current;
            //Render PlaceHolder to temporary stream
            System.IO.StringWriter stringWrite = new StringWriter();
            
            /********************************************************************************/
            //Try adding source strings for each image in content
            //string tempPostContent = getImage(HTMLCode);
            /*********************************************************************************/
            //StringReader reader = new StringReader(tempPostContent);
            StringReader reader = new StringReader(HTMLCode);

            //Create PDF document
            Document pdfDocumnet = new Document(PageSize.A4);
            HTMLWorker htmlParser = new HTMLWorker(pdfDocumnet);
            PdfWriter.GetInstance(pdfDocumnet, new FileStream(FilePath, FileMode.Create));
            pdfDocumnet.Open();
            try
            {
                //Parse Html and dump the result in PDF file
                htmlParser.Parse(reader);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                pdfDocumnet.Close();
            }
        }

        public string getImage(string input)
        {
            if (input == null)
                return string.Empty;
            string tempInput = input;
            string pattern = @"<img(.|\n)+?>";
            string src = string.Empty;
            HttpContext context = HttpContext.Current;

            //Change the relative URL's to absolute URL's for an image, if any in the HTML code.
            foreach (Match m in Regex.Matches(input, pattern, RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.RightToLeft))
            {
                if (m.Success)
                {
                    string tempM = m.Value;
                    string pattern1 = "src=[\'|\"](.+?)[\'|\"]";
                    Regex reImg = new Regex(pattern1, RegexOptions.IgnoreCase | RegexOptions.Multiline);
                    Match mImg = reImg.Match(m.Value);

                    if (mImg.Success)
                    {
                        src = mImg.Value.ToLower().Replace("src=", "").Replace("\"", "");

                        if (src.ToLower().Contains("http://") == false)
                        {
                            //Insert new URL in img tag
                            src = "src=\'" + GetWebUrl() + "/" + src.Substring(1, src.Length - 2) + "\'";
                            try
                            {
                                tempM = tempM.Remove(mImg.Index, mImg.Length);
                                tempM = tempM.Insert(mImg.Index, src);

                                //insert new url img tag in whole html code
                                tempInput = tempInput.Remove(m.Index, m.Length);
                                tempInput = tempInput.Insert(m.Index, tempM);
                            }
                            catch (Exception e)
                            {

                            }
                        }
                    }
                }
            }
            return tempInput;
        }

        string getSrc(string input)
        {
            string pattern = "src=[\'|\"](.+?)[\'|\"]";
            System.Text.RegularExpressions.Regex reImg = new System.Text.RegularExpressions.Regex(pattern,
                System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Multiline);
            System.Text.RegularExpressions.Match mImg = reImg.Match(input);
            if (mImg.Success)
            {
                return mImg.Value.Replace("src=", "").Replace("\"", ""); ;
            }

            return string.Empty;
        }

        public string GetWebUrl()
        {
            return HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpRuntime.AppDomainAppVirtualPath;
        }
    }
}
