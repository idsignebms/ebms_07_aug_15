﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Communication.SmsInput;
using System.IO;
using System.Configuration;

namespace Communication.Sms
{
    public class Sms
    {

        #region Members
        string AutoGenBillLogsFileName = string.Empty;
        #endregion

        #region Constructor

        public Sms()
        {
            var location = System.IO.Path.GetDirectoryName(new System.Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath);
            //var location = System.Reflection.Assembly.GetEntryAssembly().Location;
            var directoryPath = Path.GetDirectoryName(location);
            AutoGenBillLogsFileName = directoryPath + ConfigurationManager.AppSettings["SmsSystemLog"].ToString();
        }

        #endregion

        #region Methods

        public bool SendSMS(SmsInput.SmsInput _objSMSServiceInput)
        {
            bool IsSMSSent = false;
            try
            {
                CreateOrWriteServiceLogFile("SMS Configuration Started");
                //Service URL : http://www.smslive247.com/api/webservice/smssiteadmin.asmx
                EBMSSMSService.SMSSiteAdminProxySoapClient SMS = new EBMSSMSService.SMSSiteAdminProxySoapClient();
                SMS.Login(_objSMSServiceInput.SMSLoginId, _objSMSServiceInput.SMSLoginPassword); //site_id, password  Configuration setting

                EBMSSMSService.MessageInfo NewSMS = new EBMSSMSService.MessageInfo();
                NewSMS.CallBack = _objSMSServiceInput.SMSCallBack; //Configuration setting
                NewSMS.Destination = new EBMSSMSService.ArrayOfString();
                NewSMS.Destination.Add(_objSMSServiceInput.SMSMobileNo); // customer mobile number
                NewSMS.DeliveryEmail = _objSMSServiceInput.SMSDeliveryEmailId; //Configuration Setting
                //NewSMS.Message = "Your Electicity Bill of acc no 0000010845 is generated for June 2015. Amount payable is 294.00 =N= and the bill due date is 28-07-2015."; // Message should be in template
                NewSMS.Message = _objSMSServiceInput.SMSMessage; // Message should be in template
                NewSMS.MessageType = EBMSSMSService.SMSTypeEnum.TEXT;

                EBMSSMSService.ResponseInfo response = new EBMSSMSService.ResponseInfo();
                CreateOrWriteServiceLogFile("SMS Configuration End");
                response = SMS.SendSMS(_objSMSServiceInput.SMSSiteTokenId, NewSMS); //site_token  Configuration Setting

                if (response.ErrorCode == 0)
                {
                    CreateOrWriteServiceLogFile("SMS Sent Successfully to :" + _objSMSServiceInput.SMSMobileNo + response.ExtraMessage);
                    //Response.Write("Message sent. The message id is: " + response.ExtraMessage);
                    IsSMSSent = true;
                }
                else
                {
                    CreateOrWriteServiceLogFile("Error! Message not sent. Reason:  " + response.ErrorMessage);
                    //Response.Write("Error! Message not sent. Reason:  " + response.ErrorMessage);
                    IsSMSSent = false;
                }
            }
            catch (Exception ex)
            {
                CreateOrWriteServiceLogFile("Message sent failed. Due to: "+ex.Message.ToString());
                //Should write logs here for failure messages.
                //Response.Write("Message sent failed");
                IsSMSSent = false;
            }
            return IsSMSSent;
        }

        private void CreateOrWriteServiceLogFile(string text)
        {
            //set up a filestream
            using (FileStream fs = new FileStream(AutoGenBillLogsFileName, FileMode.OpenOrCreate, FileAccess.Write))
            {

                //set up a streamwriter for adding text
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    //find the end of the underlying filestream
                    sw.BaseStream.Seek(0, SeekOrigin.End);

                    //add the text
                    sw.WriteLine(DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ":" + text);
                    //add the text to the underlying filestream

                    //sw.Flush();
                    ////close the writer
                    //sw.Close();
                }
            }
        }
        #endregion
    }
}
