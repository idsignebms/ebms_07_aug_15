﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Communication.SmsInput
{
    public class SmsInput
    {
        public string SMSLoginId { get; set; }
        public string SMSLoginPassword { get; set; }
        public string SMSCallBack { get; set; }
        public string SMSMobileNo { get; set; }
        public string SMSDeliveryEmailId { get; set; }
        public string SMSMessage { get; set; }
        public string SMSSiteTokenId { get; set; }
        
    }
}
