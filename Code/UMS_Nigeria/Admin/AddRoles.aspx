﻿<%@ Page Title=":: Add Roles ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="AddRoles.aspx.cs" Inherits="UMS_Nigeria.Admin.AddRoles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server" AssociatedUpdatePanelID="upAddRoles">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <div class="inner-sec">
            <asp:UpdatePanel ID="upAddRoles" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddRoles" runat="server" Text="<%$ Resources:Resource, ADD_ROLE%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litRoleName" runat="server" Text="<%$ Resources:Resource, ROLE_NAME%>"></asp:Literal>
                                    <span class="span_star">*</span></label><div class="space">
                                    </div>
                                <asp:TextBox CssClass="text-box" ID="txtRoleName" TabIndex="1" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanRoleName','Role Name')"
                                    MaxLength="50" placeholder="Enter Role Name"></asp:TextBox>
                                <span id="spanRoleName" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litAccessLevels" runat="server" Text="Access Levels"></asp:Literal>
                                    <span class="span_star">*</span></label><div class="space">
                                    </div>
                                <asp:DropDownList ID="ddlAccessLevels" TabIndex="2" AutoPostBack="True" 
                                OnSelectedIndexChanged="ddlAccessLevels_OnSelectedIndexChanged"
                                onchange="DropDownlistOnChangelbl(this,'spanAccessLevels',' Access Levels')"
                                    runat="server" CssClass="text-box text-select">
                                </asp:DropDownList>
                                <span class="text-heading_2">
                                    <br />
                                </span><span id="spanAccessLevels" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litCopyRole" runat="server" Text="<%$ Resources:Resource, COPY_ROLE%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:DropDownList ID="ddlCopyRole" TabIndex="3" AutoPostBack="True" Enabled="false"
                                 OnSelectedIndexChanged="ddlCopyRole_OnSelectedIndexChanged"
                                    runat="server" CssClass="text-box text-select">
                                    <asp:ListItem Text="--Select--" value=""/>
                                </asp:DropDownList>
                                <span class="text-heading_2">
                                    <br />
                                </span><span id="spanddlCopyRole" class="span_color"></span>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <asp:Repeater ID="rptrPagePermisions" runat="server" OnItemDataBound="rptrPagePermisions_OnItemDataBound">
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div id="chblDiv" runat="server">
                                    <div class="check_baaTwo" style="width: 96%; margin-top: 16px;">
                                        <div class="text-inner_a">
                                            <asp:Literal ID="litMainMenuName" runat="server" Text='<% #Eval("Name") %>'></asp:Literal>
                                            <asp:Label ID="lblMenuId" runat="server" Visible="False" Text='<% #Eval("MenuId") %>'></asp:Label>
                                        </div>
                                        <div class="check_ba">
                                            <div class="text-inner mster-inputTwo">
                                                <asp:CheckBox runat="server" ID="ChkAll" class="text-inner-b" Text="All" />
                                                <asp:CheckBoxList ID="cblChildPages" class="text-inner-b" RepeatDirection="Horizontal"
                                                    RepeatColumns="3" runat="server">
                                                </asp:CheckBoxList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:Repeater>
                        <div class="clr">
                        </div>
                    </div>
                    <div class="box_total">
                        <div class="box_totalTwi_c" style="margin-left: 0">
                            <asp:Button ID="btnSave" TabIndex="4" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                CssClass="box_s" runat="server" OnClick="btnSave_OnClick" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
            TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
        </asp:ModalPopupExtender>
        <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
        <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" DefaultButton="btngridalertok"
        Style="display: none;">
            <div class="popheader popheaderlblred">
                <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
            </div>
            <div class="footer popfooterbtnrt">
                <div class="fltr popfooterbtn">
                    <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                        CssClass="ok_btn" />
                </div>
                <div class="clear pad_5">
                </div>
            </div>
        </asp:Panel>
    </div>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/AddRoles.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
