﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using iDSignHelper;
using Resources;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;

namespace UMS_Nigeria.Admin
{
    public partial class AddRoles : System.Web.UI.Page
    {
        #region Members
        iDsignBAL objIdsignBal = new iDsignBAL();
        AdminBAL objAdminBal = new AdminBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        XmlDocument xml = null;
        AdminListBE_1 _objAdminListBe_1 = new AdminListBE_1();
        AdminListBE _objAdminListBe = new AdminListBE();
        public string Key = "AddRoles";
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.Visible = false;
            if (!IsPostBack)
            {
                objCommonMethods.BindAccessLevelsList(ddlAccessLevels, true);
            }
        }

        protected void rptrPagePermisions_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBoxList cblChildPages = (CheckBoxList)e.Item.FindControl("cblChildPages");
                Label lblMenuId = (Label)e.Item.FindControl("lblMenuId");
                CheckBox ChkAll = (CheckBox)e.Item.FindControl("ChkAll");
                Literal litMainMenuName = (Literal)e.Item.FindControl("litMainMenuName");

                System.Web.UI.HtmlControls.HtmlGenericControl chblDiv = (System.Web.UI.HtmlControls.HtmlGenericControl)(e.Item.FindControl("chblDiv"));
                
                ChkAll.Attributes.Add("onclick", "CheckAll('" + ChkAll.ClientID + "','" + cblChildPages.ClientID + "')");
                cblChildPages.Attributes.Add("onclick", "UnCheckAll('" + ChkAll.ClientID + "','" + cblChildPages.ClientID + "')");
                
                    var drSubMenuDetails = from x in _objAdminListBe_1.Items.AsEnumerable()
                                           where x.ReferenceMenuId == Convert.ToInt32(lblMenuId.Text)
                                           select new
                                           {
                                               x.MenuId,
                                               x.Name,
                                               x.IsAssigned,
                                               x.IsActive
                                           };
                
                
                //foreach (var cblListItems in drSubMenuDetails)
                //{
                //    ListItem listItem = new ListItem();
                //    listItem.Value = cblListItems.MenuId.ToString();
                //    listItem.Text = cblListItems.Name;
                    
                //}

                if (lblMenuId.Text != "7")
                {
                    drSubMenuDetails = drSubMenuDetails.Where(x => x.IsActive);
                }
                else
                {
                    drSubMenuDetails = drSubMenuDetails.Where(x => x.MenuId != 122);
                }
                var SubMenuItemCount = drSubMenuDetails.Count();
                if (SubMenuItemCount > 0)
                {
                    chblDiv.Visible = true;
                    cblChildPages.DataSource = drSubMenuDetails;
                    cblChildPages.DataTextField = "Name";
                    cblChildPages.DataValueField = "MenuId";
                    cblChildPages.DataBind();
                }
                else
                {
                    chblDiv.Visible = false;
                }
            }
        }

        //protected void ChkAll_OnCheckedChanged(object sender, EventArgs e)
        //{
        //    CheckBox ChkAll = (CheckBox)sender;
        //    RepeaterItem rptrItem = (RepeaterItem)ChkAll.NamingContainer;
        //    if (rptrItem != null)
        //    {
        //        try
        //        {
        //            CheckBoxList cblChildPages = (CheckBoxList) rptrItem.FindControl("cblChildPages");
        //            foreach (ListItem li in cblChildPages.Items)
        //            {
        //                li.Selected = ChkAll.Checked;
        //            } 
        //        }
        //        catch (Exception ex)
        //        {

        //        }
        //    }
        //}

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            try
            {
                AdminBE _ObjAdminBe = new AdminBE();
                _ObjAdminBe.RoleName = txtRoleName.Text.Trim();
                _ObjAdminBe.AccessLevelID = Convert.ToInt32(ddlAccessLevels.SelectedValue);
                _ObjAdminBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _ObjAdminBe.Permissions = string.Empty;
                int itemsCount = rptrPagePermisions.Items.Count;
                for (int i = 0; i < itemsCount; i++)
                {

                    CheckBox ChkAll = (CheckBox)rptrPagePermisions.Items[i].FindControl("ChkAll");
                    Label lblMenuId = (Label)rptrPagePermisions.Items[i].FindControl("lblMenuId");
                    CheckBoxList cblChildPages = (CheckBoxList)rptrPagePermisions.Items[i].FindControl("cblChildPages");


                    //7 is the id for settings and 122 is the id of configuration settings
                    if (lblMenuId.Text=="7")
                    {
                        if (ChkAll.Checked || cblChildPages.Items.Cast<ListItem>().Any(li => li.Selected))
                            _ObjAdminBe.Permissions = _ObjAdminBe.Permissions + (_ObjAdminBe.Permissions == string.Empty ? "122," : ",122,") + lblMenuId.Text;
                        for (int j = 0; j < cblChildPages.Items.Count; j++)
                        {
                            if (cblChildPages.Items[j].Selected)
                            {
                                _ObjAdminBe.Permissions = _ObjAdminBe.Permissions + "," + cblChildPages.Items[j].Value;
                            }
                        } 
                    }
                    else
                    {
                        if (ChkAll.Checked || cblChildPages.Items.Cast<ListItem>().Any(li => li.Selected))
                            _ObjAdminBe.Permissions = _ObjAdminBe.Permissions + (_ObjAdminBe.Permissions == string.Empty ? "" : ",") + lblMenuId.Text;
                        for (int j = 0; j < cblChildPages.Items.Count; j++)
                        {
                            if (cblChildPages.Items[j].Selected)
                            {
                                _ObjAdminBe.Permissions = _ObjAdminBe.Permissions + "," + cblChildPages.Items[j].Value;
                            }
                        } 
                    }
                }
                xml = objAdminBal.Admin(_ObjAdminBe, Statement.Insert);
                _ObjAdminBe = objIdsignBal.DeserializeFromXml<AdminBE>(xml);

                if (_ObjAdminBe.RowsEffected > 0)
                {
                    Message("Role Saved Successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                    ClearAll();
                    txtRoleName.Text = string.Empty;
                }
                else if (_ObjAdminBe.RowsEffected < 0)
                {
                    Message("Role " + _ObjAdminBe.ExistingRoleName + " with the same permission already exists.",
                        UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                {
                    Message("Role with name " + txtRoleName.Text + " already exists.",
                        UMS_Resource.MESSAGETYPE_ERROR);
                    txtRoleName.Focus();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlAccessLevels_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlAccessLevels.SelectedValue))
            {
                BindPagePermisions();
                AdminBE _objAdminBE = new AdminBE();
                _objAdminBE.AccessLevelID = Convert.ToInt32(ddlAccessLevels.SelectedValue);
                objCommonMethods.BindRolesList(ddlCopyRole, _objAdminBE, true);
                ddlCopyRole.SelectedIndex = 0;
                ddlCopyRole.Enabled = true;
            }
            else
            {
                rptrPagePermisions.DataSource = null;
                rptrPagePermisions.DataBind();
                ddlCopyRole.SelectedIndex = 0;
                ddlCopyRole.Enabled = false;
            }
        }

        protected void ddlCopyRole_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            AdminBE _objAdminBe=new AdminBE();
            if (!string.IsNullOrEmpty(ddlCopyRole.SelectedValue))
            {
                _objAdminBe.RoleId = Convert.ToInt32(ddlCopyRole.SelectedValue);
                xml = objAdminBal.GetRolePermissions(_objAdminBe);

                AdminListBE_1 objAdminListBe = objIdsignBal.DeserializeFromXml<AdminListBE_1>(xml);

                for (int i = 0; i < rptrPagePermisions.Items.Count; i++)
                {
                    bool IsAllChecked = true;
                    CheckBoxList cblChildPages = (CheckBoxList) rptrPagePermisions.Items[i].FindControl("cblChildPages");
                    CheckBox ChkAll = (CheckBox) rptrPagePermisions.Items[i].FindControl("ChkAll");
                    for (int j = 0; j < objAdminListBe.Items.Count; j++)
                    {
                        if (cblChildPages.Items.FindByValue(objAdminListBe.Items[j].MenuId.ToString()) != null)
                        {
                            cblChildPages.Items.FindByValue(objAdminListBe.Items[j].MenuId.ToString()).Selected =
                                Convert.ToBoolean(objAdminListBe.Items[j].IsAssigned);
                            if (!objAdminListBe.Items[j].IsAssigned)
                                IsAllChecked = false;
                        }
                    }
                    ChkAll.Checked = IsAllChecked;
                }
            }
            else
            {
                ClearAll();
            }
        }

        #endregion

        #region Methods

        public void BindPagePermisions()
        {
            if(ddlAccessLevels.SelectedIndex==0)
                return;
            AdminBE _objAdminBE = new AdminBE();
            _objAdminBE.RoleId = Session[UMS_Resource.SESSION_ROLEID] != null ? 0 : Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString());
            _objAdminBE.AccessLevelID = Convert.ToInt32(ddlAccessLevels.SelectedValue);
            XmlDocument xmlResult = objAdminBal.Admin(_objAdminBE,ReturnType.Get);

            _objAdminListBe = objIdsignBal.DeserializeFromXml<AdminListBE>(xmlResult);

            _objAdminListBe_1 = objIdsignBal.DeserializeFromXml<AdminListBE_1>(xmlResult);
            rptrPagePermisions.DataSource = _objAdminListBe.Items;
            rptrPagePermisions.DataBind();
            //string[] MenuItems = new string[4]; { };
            //foreach (var Master in _objAdminListBe.Items)
            //{
                //MenuItems[0] = Master.MenuId.ToString();
                //MenuItems[1] = Master.Name;
                //var drSubMenuDetails = from x in _objAdminListBe.Items.AsEnumerable()
                //                       where x.MenuId == Master.MenuId
                //                       select new
                //                       {
                //                           x.MenuId,
                //                           x.Name
                //                       };
                //foreach (var Child in drSubMenuDetails)
                //{
                //    MenuItems[2] = Child.MenuId.ToString();
                //    MenuItems[3] = Child.Name;

                //}
            //}
        }

        public void BindPagePermisionsByRole( int RoleId)
        {

            AdminBE _objAdminBe = new AdminBE();
            _objAdminBe.RoleId = RoleId;
            xml = objAdminBal.GetRolePermissions(_objAdminBe);

            AdminListBE objAdminListBe = objIdsignBal.DeserializeFromXml<AdminListBE>(xml);

            _objAdminListBe_1 = objIdsignBal.DeserializeFromXml<AdminListBE_1>(xml);
            rptrPagePermisions.DataSource = _objAdminListBe.Items;
            rptrPagePermisions.DataBind();
            
        }

        public void ClearAll()
        {
            for (int i = 0; i < rptrPagePermisions.Items.Count; i++)
            {
                CheckBox chkAll = (CheckBox)rptrPagePermisions.Items[i].FindControl("chkAll");
                chkAll.Checked = false;
                CheckBoxList cblChildPages = (CheckBoxList)rptrPagePermisions.Items[i].FindControl("cblChildPages");
                foreach (ListItem li in cblChildPages.Items)
                {
                    li.Selected = false;
                }
            }
            AdminBE _objAdminBE = new AdminBE();
            _objAdminBE.AccessLevelID = 0;
            _objAdminBE.RoleId = Session[UMS_Resource.SESSION_ROLEID] != null ? 0 : Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString());
            objCommonMethods.BindRolesList(ddlCopyRole, _objAdminBE, true);
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion
    }
}