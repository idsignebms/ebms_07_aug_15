﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UMS.Master" AutoEventWireup="true" CodeBehind="AddTariffs.aspx.cs" Inherits="UMS_Nigeria.Admin.AddTariffs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>    
    <link href="../Styles/reset.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.2.1.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
 <asp:UpdateProgress ID="UpdateProgress" runat="server" AssociatedUpdatePanelID="upSignUp">
        <ProgressTemplate>
            <asp:Panel ID="pnlLoading" runat="server" CssClass="popbg">
            </asp:Panel>
            <div class="popbg1">
                <center style="margin-top: 20% 0 0 100px;">
                    <img src="../images/uploading.gif" id="imgload" runat="server" alt="Loading..." width="80" />
                </center>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upSignUp" runat="server">
        <ContentTemplate>
            <div>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
            </div>
            <div>
                <table width="80%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td colspan="2" align="left">
                            <h3 style="color: #2E64FE;">
                                New Tariff Details</h3>
                        </td>
                        <td colspan="4">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 100px;">
                            Tariff Code <span class="spancolor">*</span>
                        </td>
                        <td align="left" style="padding-right: 0px; width: 30px;">
                            <asp:TextBox CssClass="text-box"  ID="txtTariffCode" runat="server" onblur="return ValidateCountryCode()"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtTariffCode"
                                ValidChars="_-" FilterType="Custom,UppercaseLetters">
                            </asp:FilteredTextBoxExtender>
                        </td>
                        <td colspan="4">
                            &nbsp;
                        </td>
                        <td colspan="6" align="left">
                            &nbsp;
                        </td>
                        <td align="left" style="width: 96px;">
                            Tariff Name <span class="spancolor">*</span>
                        </td>
                        <td style="width: 96px;">
                            <asp:TextBox CssClass="text-box"  ID="txtTariffName" runat="server" onblur="return ValidateCountryName()"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtTariffName"
                                ValidChars="_-" FilterType="LowercaseLetters,UppercaseLetters,Custom">
                            </asp:FilteredTextBoxExtender>
                        </td>
                        <td align="left">
                            &nbsp;
                        </td>
                        <td colspan="6">
                            &nbsp;
                        </td>
                        <td align="left" style="width: 63px;">
                             <span class="spancolor">*</span>
                        </td>
                        <td>
                            <%--<asp:TextBox CssClass="text-box"  ID="txtCurrency" runat="server"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtCurrency"
                                ValidChars=" " FilterType="LowercaseLetters,UppercaseLetters,Custom">
                            </asp:FilteredTextBoxExtender>--%>
                        </td>
                        <td align="left">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <table style="margin: 0px;">
                    <tr>
                        <td align="right" colspan="6" style="padding-right: 196PX;">
                            <asp:Button CssClass="addsubmit" ID="btnSave" runat="server" Text="Save" OnClientClick="return CountryValidate()"
                                OnClick="btnSave_Click" />
                            <asp:Button CssClass="resetbutton" ID="btnReset" runat="server" Text="Reset" OnClientClick="return ClearControls()" />
                        </td>
                    </tr>
                </table>
            </div>           
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
