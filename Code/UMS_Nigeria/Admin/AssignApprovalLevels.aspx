﻿<%@ Page Title=":: Assign Approval Levels ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    AutoEventWireup="true" CodeBehind="AssignApprovalLevels.aspx.cs" Inherits="UMS_Nigeria.Admin.AssignApprovalLevels"
    Theme="Green" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" AssociatedUpdatePanelID="upnlCustomerReorder">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdatePanel ID="upnlCustomerReorder" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddCycle" runat="server" Text="Assign Approval Levels"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="litBusinessUnit" runat="server" Text="Function"></asp:Literal>
                                <span class="span_star">*</span>
                                <div class="space">
                                </div>
                                <asp:DropDownList ID="ddlFunctions" TabIndex="1" runat="server" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlFunctions_SelectedIndexChanged" CssClass="text-box text-select">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <div class="space">
                                </div>
                                <span id="spanFunction" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <asp:Literal ID="Literal1" runat="server" Text="No.of Levels"></asp:Literal>
                                <span class="span_star">*</span>
                                <div class="space">
                                </div>
                                <asp:DropDownList ID="ddlLevels" TabIndex="2" runat="server" Width="100" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlLevels_SelectedIndexChanged" CssClass="text-box text-select">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <div class="space">
                                </div>
                                <span id="spanLevels" class="span_color"></span>
                            </div>
                        </div>
                        <div class="consumer_feild">
                            <%--<div class="consume_name">
                        <asp:Literal ID="litBusinessUnit" runat="server" Text="Function"></asp:Literal>
                        <span>*</span>
                    </div>
                    <div class="consume_input">
                        <asp:DropDownList ID="ddlFunctions" runat="server">
                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                        </asp:DropDownList>
                        <span id="spanFunction" class="spancolor"></span>
                    </div>--%>
                        </div>
                        <%--<div class="clear pad_5">
                </div>--%>
                        <div class="consumer_feild">
                            <%--<div class="consume_name">
                        <asp:Literal ID="Literal1" runat="server" Text="No.of Levels"></asp:Literal>
                        <span>*</span>
                    </div>
                    <div class="consume_input">
                        <asp:DropDownList ID="ddlLevels" runat="server" Width="100" AutoPostBack="true" OnSelectedIndexChanged="ddlLevels_SelectedIndexChanged">
                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                        </asp:DropDownList>
                        <br />
                        <span id="spanLevels" class="spancolor"></span>
                    </div>--%>
                        </div>
                        <%--<div class="clear pad_5">
                </div>--%>
                        <div class="clr">
                        </div>
                        <div class="inner-box1" id="divBusinessUnit" runat="server" visible="false">
                            <div class="text-inner">
                                <asp:Literal ID="litBU" runat="server" Text="Business Unit"></asp:Literal>
                                <span class="span_star">*</span>
                                <div class="space">
                                </div>
                                <asp:DropDownList ID="ddlBusinessUnit" CssClass="text-box text-select" Width="100"
                                    runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlBusinessUnit_SelectedIndexChanged">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <div class="space">
                                </div>
                                <span id="spanBusinessUnit" class="spancolor"></span>
                            </div>
                        </div>
                        <div class="inner-box2" id="divFinalApprovalLevel" runat="server" visible="false">
                            <div class="text-inner">
                                <asp:Literal ID="Literal3" runat="server" Text="Final Approval Level"></asp:Literal>
                                <span class="span_star">*</span>
                                <div class="space">
                                </div>
                                <asp:DropDownList ID="ddlFinalApprovalLevel" CssClass="text-box text-select" Width="100"
                                    runat="server">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <div class="space">
                                </div>
                                <span id="spanFina" class="spancolor"></span>
                            </div>
                        </div>
                    </div>
                    <div class="clear pad_5">
                    </div>
                    <div id="divgrid" class="grid_tb" align="center">
                        <asp:GridView ID="gvLevelsSettings" runat="server" AutoGenerateColumns="False" 
                            HeaderStyle-CssClass="grid_tb_hd">
                            <%-- <EmptyDataRowStyle HorizontalAlign="Left" CssClass="color" />
                        <EmptyDataTemplate>
                            There is no data.
                        </EmptyDataTemplate>--%>
                            <Columns>
                                <%--<asp:BoundField HeaderText="<%$ Resources:Resource, GRID_SNO%>" DataField="RowNumber" />--%>
                                <asp:BoundField HeaderText="Level" DataField="Level" ItemStyle-Width="10%" />
                                <asp:TemplateField HeaderText="Role" ItemStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlRoles" class="ddlRoles" runat="server" CssClass="text-box text-select ddlRoles"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlRoles_SelectedIndexChanged">
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Users" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="assgn_input"
                                    ItemStyle-Width="20%">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlAllUsers" class="ddlAllUsers" CssClass="text-box text-select ddlAllUsers"
                                            runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlAllUsers_SelectedIndexChanged">
                                            <asp:ListItem Text="All Users"></asp:ListItem>
                                            <asp:ListItem Text="Select Users"></asp:ListItem>
                                        </asp:DropDownList>
                                        <br />
                                        <div class="divUsersCheckBoxes">
                                            <asp:CheckBox runat="server" ID="ChkAll" Text="All" Visible="false" onclick="CheckAll(this)" />
                                            <asp:CheckBoxList ID="cblUsers" runat="server" RepeatColumns="4" CssClass="assin_border">
                                            </asp:CheckBoxList>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle HorizontalAlign="center"></RowStyle>
                        </asp:GridView>
                    </div>
                    <div class="clear">
                        &nbsp;</div>
                    <div class="box_total">
                        <div class="box_totalTwi_c">
                            <asp:Button ID="btnSubmit" Text="Save" CssClass="box_s" OnClientClick="return Validate();"
                                runat="server" OnClick="btnSubmit_Click" />
                        </div>
                        <div style="clear: both;">
                        </div>
                    </div>
                    <div class="clear pad_5">
                        <div style="margin-left: 480px;">
                            <%--<asp:Button ID="btnSubmit" Text="Save" CssClass="calculate_but" OnClientClick="return Validate()"
                            runat="server" OnClick="btnSubmit_Click" />--%>
                        </div>
                    </div>
                    <div class="clr pad_10"></div>
                    <div class="grid_boxes">
                        <div class="grid_pagingTwo_top">
                            <div class="paging_top_titleTwo">
                                <asp:Literal ID="litCountriesList" runat="server" Text="Approval Settings"></asp:Literal>
                            </div>
                            <div class="paging_top_rightTwo_content" id="divpaging" runat="server">
                                
                            </div>
                        </div>
                    </div>
                    <div class="clr pad_10"></div>
                    <div class="grid " align="center" style="float: left; margin-left: 10px; overflow-x: auto;
                        width: 98%;">
                        <asp:GridView ID="gvApprovalSettings" runat="server" AutoGenerateColumns="False"
                            AlternatingRowStyle-CssClass="color" OnRowDataBound="gvApprovalSettings_RowDataBound"
                            OnRowCommand="gvApprovalSettings_RowCommand">
                            <EmptyDataRowStyle HorizontalAlign="Left" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Function" ItemStyle-HorizontalAlign="left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFunction" runat="server" Text='<%# Eval("Function") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total Levels" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="assgn_input">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAccessLevels" runat="server" Text='<%# Eval("AccessLevels") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Created Date" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="assgn_input">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreatedDate" runat="server" Text='<%# Eval("CreatedDate") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Created By" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="assgn_input">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("CreatedBy") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="BU" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="assgn_input">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBU_ID" Visible="false" runat="server" Text='<%# Eval("BU_ID") %>'></asp:Label>
                                        <asp:Label ID="lblBusinessUnitName" runat="server" Text='<%# Eval("BusinessUnitName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Level" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="assgn_input">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLevel" runat="server" Text='<%# Eval("Levels") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Users" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="assgn_input">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUsers" runat="server" Text='<%# Eval("Users") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Role" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="assgn_input">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRoles" runat="server" Text='<%# Eval("Roles") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="assgn_input">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnActivate" runat="server" ToolTip="Activate" CommandArgument='<%# Eval("FunctionId") %>'
                                            CommandName='ACTIVE' Visible="false">
                                    <img src="../images/Activate.gif"   alt="Active" /></asp:LinkButton>
                                        <asp:LinkButton ID="lbtnDeActivate" Visible="false" runat="server" ToolTip="DeActivate"
                                            CommandArgument='<%# Eval("FunctionId") %>' CommandName='DEACTIVE'>
                                    <img src="../images/Deactivate.gif"   alt="DeActive" /></asp:LinkButton>
                                        <asp:Label ID="lblIsActive" runat="server" Text='<%# Eval("IsActive") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle HorizontalAlign="center"></RowStyle>
                        </asp:GridView>
                        <asp:HiddenField ID="hfBU_ID" runat="server" Value="" />
                    </div>
                    <asp:ModalPopupExtender ID="mpeChangeActive" runat="server" PopupControlID="PanelChangeActivate"
                        TargetControlID="btnChangeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnChangeActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelChangeActivate" runat="server"
                        CssClass="modalPopup" Style="display: none; border-color: #639B00;">
                        <div class="popheader" style="background-color: red;" id="popChangeActive" runat="server">
                            <asp:Label ID="lblChangeActiveMessage" runat="server"></asp:Label>
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnChangeActive" OnClick="btnChangeActive_Click" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
        TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
    </asp:ModalPopupExtender>
    <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
    <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
        <div class="popheader popheaderlblred">
            <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
        </div>
        <div class="footer popfooterbtnrt">
            <div class="fltr popfooterbtn">
                <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                    CssClass="ok_btn" />
            </div>
            <div class="clear pad_5">
            </div>
        </div>
    </asp:Panel>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
    </script>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/PageValidations/AssignApprovalLevels.js?v=1" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
