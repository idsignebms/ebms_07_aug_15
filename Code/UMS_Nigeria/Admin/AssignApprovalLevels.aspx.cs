﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddCountries
                     
 Developer        : id027-T.Karthik
 Creation Date    : 27-Nov-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using Resources;
using System.Data;

namespace UMS_Nigeria.Admin
{
    public partial class AssignApprovalLevels : System.Web.UI.Page
    {
        #region Members
        iDsignBAL objIdsignBal = new iDsignBAL();
        ApprovalBal objApprovalBal = new ApprovalBal();
        UserManagementBAL objUserManagementBAL = new UserManagementBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        public string Key = "AssignApprovalLevels";
        #endregion

        #region Properties
        public int Function
        {
            get { return string.IsNullOrEmpty(ddlFunctions.SelectedValue) ? 0 : Convert.ToInt32(ddlFunctions.SelectedValue); }
            set { ddlFunctions.SelectedValue = value.ToString(); }
        }

        public int NoOfLevels
        {
            get { return string.IsNullOrEmpty(ddlLevels.SelectedValue) ? 0 : Convert.ToInt32(ddlLevels.SelectedValue); }
            set { ddlLevels.SelectedValue = value.ToString(); }
        }

        public string BusinessUnit
        {
            get { return string.IsNullOrEmpty(ddlBusinessUnit.SelectedValue) ? string.Empty : ddlBusinessUnit.SelectedValue; }
            set { ddlBusinessUnit.SelectedValue = value.ToString(); }
        }
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.CssClass = lblMessage.Text = string.Empty;
            try
            {
                if (!IsPostBack)
                {
                    string path = string.Empty;
                    path = objCommonMethods.GetPagePath(this.Request.Url);
                    if (objCommonMethods.IsAccess(path))
                    {
                        BindDropDowns();
                        BindApprovalSettings();
                    }
                    else
                    {
                        Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlFunctions_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Function > 0)
                {
                    ApprovalBe objApprovalBe = new ApprovalBe();
                    objApprovalBe.FunctionId = Function;
                    objApprovalBe = objIdsignBal.DeserializeFromXml<ApprovalBe>(objApprovalBal.Get(objApprovalBe, ReturnType.Retrieve));
                    if (objApprovalBe.AccessLevels > 0)
                    {
                        ddlLevels.SelectedValue = objApprovalBe.AccessLevels.ToString();
                        ddlLevels.Enabled = false;
                        ddlLevels_SelectedIndexChanged(sender, e);
                    }
                    else
                    {
                        ddlLevels.SelectedIndex = 0;
                        ddlLevels.Enabled = true;
                    }
                }
                else
                {
                    divBusinessUnit.Visible = divFinalApprovalLevel.Visible = false;
                    ddlLevels.SelectedIndex = 0;
                    ddlLevels.Enabled = true;
                    gvLevelsSettings.DataSource = new DataTable();
                    gvLevelsSettings.DataBind();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlLevels_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlLevels.SelectedIndex > 0)
                {
                    objCommonMethods.BindUserBusinessUnits(ddlBusinessUnit, string.Empty, false, UMS_Resource.DROPDOWN_SELECT);
                    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    {
                        ddlBusinessUnit.SelectedIndex = ddlBusinessUnit.Items.IndexOf(ddlBusinessUnit.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                        ddlBusinessUnit.Enabled = false;
                        ddlBusinessUnit_SelectedIndexChanged(ddlBusinessUnit, new EventArgs());
                    }
                    divBusinessUnit.Visible = true;
                }
                else
                {
                    gvLevelsSettings.DataSource = new DataTable();
                    gvLevelsSettings.DataBind();
                    divFinalApprovalLevel.Visible = divBusinessUnit.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlBusinessUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divFinalApprovalLevel.Visible = true;
                DataTable dtLevels = new DataTable();
                dtLevels.Columns.Add("Level", typeof(string));
                int NoOfLevels = string.IsNullOrEmpty(ddlLevels.SelectedValue) ? 0 : Convert.ToInt32(ddlLevels.SelectedValue);
                for (int i = 1; i <= NoOfLevels; i++)
                {
                    DataRow dr = dtLevels.NewRow();
                    string LevelName = "Level-" + i.ToString();
                    dr["Level"] = LevelName;
                    dtLevels.Rows.Add(dr);
                    if (i == NoOfLevels)
                    {
                        ddlFinalApprovalLevel.Items.Clear();
                        ListItem item = new ListItem(LevelName, NoOfLevels.ToString());
                        item.Selected = true;
                        ddlFinalApprovalLevel.Items.Add(item);
                        ddlFinalApprovalLevel.Enabled = false;
                    }
                }

                if (dtLevels.Rows.Count > 0)
                {
                    gvLevelsSettings.DataSource = dtLevels;
                    gvLevelsSettings.DataBind();

                    UserManagementBE objUserManagementBE = new UserManagementBE();
                    UserManagementListBE objUserManagementListBE = new UserManagementListBE();
                    objUserManagementBE.RoleId = Constants.Zero;
                    objUserManagementBE.BU_ID = BusinessUnit;
                    objUserManagementListBE = objIdsignBal.DeserializeFromXml<UserManagementListBE>(objUserManagementBAL.Get(objUserManagementBE, ReturnType.Retrieve));

                    foreach (GridViewRow row in gvLevelsSettings.Rows)
                    {
                        DropDownList ddlRoles = (DropDownList)row.FindControl("ddlRoles");
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<UserManagementBE>(objUserManagementListBE.Items), ddlRoles, "RoleName", "RoleId", UMS_Resource.DROPDOWN_SELECT, false);
                    }
                }
            }
            catch (Exception ex)
            {
                Message("Error Occured in Binding the access level grid.", UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        protected void ddlAllUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlGridAllUsers = (DropDownList)sender;
                GridViewRow row = (GridViewRow)ddlGridAllUsers.Parent.Parent;
                DropDownList ddlRoles = (DropDownList)row.FindControl("ddlRoles");
                CheckBoxList cblUsers = (CheckBoxList)row.FindControl("cblUsers");
                CheckBox ChkAll = (CheckBox)row.FindControl("ChkAll");
                if (ddlGridAllUsers.SelectedIndex == 1 && ddlRoles.SelectedIndex > 0)
                {
                    ApprovalBe objApprovalBe = new ApprovalBe();
                    objApprovalBe.RoleId = Convert.ToInt32(ddlRoles.SelectedValue);
                    //ApprovalListBe objApprovalList = objIdsignBal.DeserializeFromXml<ApprovalListBe>(objApprovalBal.Get(objApprovalBe, ReturnType.List));
                    objApprovalBe.BU_ID = BusinessUnit;
                    ApprovalListBe objApprovalList = objIdsignBal.DeserializeFromXml<ApprovalListBe>(objApprovalBal.Get(objApprovalBe, ReturnType.Multiple));
                    if (objApprovalList.items.Count > 0)
                    {
                        objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<ApprovalBe>(objApprovalList.items).Tables[0], cblUsers, "UserId", "UserDetailsId");
                        ChkAll.Checked = false;
                        ChkAll.Visible = true;
                        cblUsers.Attributes.Add("onclick", "UnCheckAll('" + ChkAll.ClientID + "','" + cblUsers.ClientID + "')");
                    }
                }
                else
                {
                    ChkAll.Visible = false;
                    cblUsers.DataSource = new DataTable();
                    cblUsers.DataBind();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlRoles = (DropDownList)sender;
                GridViewRow row = (GridViewRow)ddlRoles.Parent.Parent;
                DropDownList ddlGridAllUsers = (DropDownList)row.FindControl("ddlAllUsers");
                CheckBoxList cblUsers = (CheckBoxList)row.FindControl("cblUsers");
                CheckBox ChkAll = (CheckBox)row.FindControl("ChkAll");
                cblUsers.DataSource = new DataTable();
                cblUsers.DataBind();
                ddlGridAllUsers.SelectedIndex = Constants.Zero;
                ChkAll.Visible = false;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                ApprovalBe objApprovalBe = new ApprovalBe();
                objApprovalBe.FunctionId = this.Function;
                objApprovalBe.BU_ID = ddlBusinessUnit.SelectedValue.ToString();

                objApprovalBe = objIdsignBal.DeserializeFromXml<ApprovalBe>(objApprovalBal.Get(objApprovalBe, ReturnType.Single));
                if (objApprovalBe.IsSuccess)
                {
                    SaveSettings();
                }
                else
                {
                    Message(Resource.REQUEST_EXISTS_APPROVAL_SETTINGS, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnChangeActive_Click(object sender, EventArgs e)
        {
            try
            {
                ApprovalBe objApprovalBe = new ApprovalBe();
                objApprovalBe.FunctionId = Convert.ToInt32(btnChangeActive.CommandName);
                objApprovalBe.IsActive = Convert.ToBoolean(btnChangeActive.CommandArgument);
                objApprovalBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objApprovalBe.BU_ID = hfBU_ID.Value;
                objApprovalBe = objIdsignBal.DeserializeFromXml<ApprovalBe>(objApprovalBal.Insert(objApprovalBe, Statement.Update));
                if (objApprovalBe.IsSuccess)
                {
                    if (Convert.ToBoolean(btnChangeActive.CommandArgument))
                        Message(Resource.APPROVAL_ACTIVATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    else
                        Message(Resource.APPROVAL_DEACTIVATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    BindApprovalSettings();
                    BindDropDowns();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvApprovalSettings_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblIsActive = (Label)e.Row.FindControl("lblIsActive");
                LinkButton lbtnActivate = (LinkButton)e.Row.FindControl("lbtnActivate");
                LinkButton lbtnDeActivate = (LinkButton)e.Row.FindControl("lbtnDeActivate");
                if (Convert.ToBoolean(lblIsActive.Text))
                    lbtnDeActivate.Visible = true;
                else
                    lbtnActivate.Visible = true;
            }
        }

        protected void gvApprovalSettings_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblBU_ID = (Label)row.FindControl("lblBU_ID");
            hfBU_ID.Value = lblBU_ID.Text;
            switch (e.CommandName.ToUpper())
            {
                case "ACTIVE":
                    LinkButton lbtnActivate = (LinkButton)row.FindControl("lbtnActivate");
                    btnChangeActive.CommandArgument = "true";
                    popChangeActive.Attributes.Add("class", "popheader popheaderlblgreen");
                    btnChangeActive.CommandName = lbtnActivate.CommandArgument.ToString();
                    lblChangeActiveMessage.Text = Resource.ACTIVATE_APPROVAL_CONFIRM_MSG;
                    mpeChangeActive.Show();
                    btnChangeActive.Focus();
                    break;
                case "DEACTIVE":
                    LinkButton lbtnDeActivate = (LinkButton)row.FindControl("lbtnDeActivate");
                    ApprovalBe objApprovalBe = new ApprovalBe();
                    objApprovalBe.FunctionId = Convert.ToInt32(lbtnDeActivate.CommandArgument);
                    objApprovalBe.BU_ID = lblBU_ID.Text;
                    objApprovalBe = objIdsignBal.DeserializeFromXml<ApprovalBe>(objApprovalBal.Get(objApprovalBe, ReturnType.Single));
                    if (objApprovalBe.IsSuccess)
                    {
                        popChangeActive.Attributes.Add("class", "popheader popheaderlblred");
                        btnChangeActive.Visible = true;
                        btnChangeActive.Text = Resource.OK;
                        btnDeActiveCancel.Text = Resource.CANCEL;
                        btnChangeActive.CommandArgument = "false";
                        btnChangeActive.CommandName = lbtnDeActivate.CommandArgument.ToString();
                        lblChangeActiveMessage.Text = Resource.DEACTIVATE_APPROVAL_CONFIRM_MSG;
                        mpeChangeActive.Show();
                        btnChangeActive.Focus();
                    }
                    else
                    {
                        popChangeActive.Attributes.Add("class", "popheader popheaderlblred");
                        btnChangeActive.Visible = false;
                        btnDeActiveCancel.Text = Resource.OK;
                        lblChangeActiveMessage.Text = Resource.REQUEST_EXISTS_APPROVAL_SETTINGS;
                        mpeChangeActive.Show();
                        btnDeActiveCancel.Focus();
                    }
                    break;
            }
        }
        #endregion

        #region Methods
        private void BindDropDowns()
        {
            try
            {
                ApprovalListBe objApprovalList = objIdsignBal.DeserializeFromXml<ApprovalListBe>(objApprovalBal.Get(ReturnType.Get));
                objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ApprovalBe>(objApprovalList.items), ddlFunctions, "Function", "FunctionId", UMS_Resource.DROPDOWN_SELECT, false);
                ddlLevels.Items.Clear();
                ListItem listItem = new ListItem("--Select--", "");
                ddlLevels.Items.Add(listItem);
                for (int i = 1; i <= 10; i++)
                {
                    ddlLevels.Items.Add(i.ToString());
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindApprovalSettings()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objApprovalBal.GetApprovalSettings();
                gvApprovalSettings.DataSource = ds.Tables[0];
                gvApprovalSettings.DataBind();
                MergeRows(gvApprovalSettings);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void SaveSettings()
        {
            try
            {
                ApprovalBe objApprovalBe = new ApprovalBe();
                objApprovalBe.FunctionId = this.Function;
                objApprovalBe.ApprovalRoleId = this.NoOfLevels;
                objApprovalBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                string Levels = string.Empty;
                string Roles = string.Empty;
                string Users = string.Empty;
                foreach (GridViewRow row in gvLevelsSettings.Rows)
                {
                    Levels += (row.RowIndex + 1).ToString() + "|";
                    DropDownList ddlRoles = (DropDownList)row.FindControl("ddlRoles");
                    DropDownList ddlAllUsers = (DropDownList)row.FindControl("ddlAllUsers");
                    Roles += ddlRoles.SelectedValue + "|";
                    if (ddlAllUsers.SelectedIndex == 1)
                    {
                        CheckBox ChkAll = (CheckBox)row.FindControl("ChkAll");
                        if (ChkAll.Checked)
                            Users += " |";
                        else
                        {
                            CheckBoxList cblUsers = (CheckBoxList)row.FindControl("cblUsers");
                            Users += objCommonMethods.CollectSelectedItemsValues(cblUsers, ",") + "|";
                        }
                    }
                    else
                        Users += " |";
                }
                objApprovalBe.Levels = Levels.TrimEnd('|');
                objApprovalBe.Roles = Roles.TrimEnd('|');
                objApprovalBe.UserDetailIds = Users.TrimEnd('|');
                objApprovalBe.BU_ID = BusinessUnit;
                objApprovalBe = objIdsignBal.DeserializeFromXml<ApprovalBe>(objApprovalBal.Insert(objApprovalBe, Statement.Insert));
                if (objApprovalBe.IsSuccess)
                {
                    ddlFunctions.SelectedIndex = ddlLevels.SelectedIndex = Constants.Zero;
                    divFinalApprovalLevel.Visible = false;
                    gvLevelsSettings.DataSource = new DataTable();
                    gvLevelsSettings.DataBind();
                    BindApprovalSettings();
                    Message(Resource.APPROVAL_SETTINGS_SAVED, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                    Message(Resource.APPROVAL_SETTINGS_SAVE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        public void MergeRows(GridView gridView)
        {
            try
            {
                for (int rowIndex = gridView.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow row = gridView.Rows[rowIndex];
                    GridViewRow previousRow = gridView.Rows[rowIndex + 1];

                    Label lblFunction = (Label)row.FindControl("lblFunction");
                    Label lblPreviousFunction = (Label)previousRow.FindControl("lblFunction");
                    Label lblAccessLevels = (Label)previousRow.FindControl("lblAccessLevels");
                    Label lblCreatedDate = (Label)row.FindControl("lblCreatedDate");
                    Label lblCreatedBy = (Label)previousRow.FindControl("lblCreatedBy");
                    Label lblBusinessUnitName = (Label)previousRow.FindControl("lblBusinessUnitName");
                    //Label lblPreviousBusinessUnitName = (Label)previousRow.FindControl("lblBusinessUnitName");
                    for (int i = 0; i < row.Cells.Count; i++)
                    {
                        if (i == 0 || i == 1 || i == 2 || i == 3)
                        {
                            if (lblFunction.Text == lblPreviousFunction.Text)
                            {
                                row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                                previousRow.Cells[i].Visible = false;
                            }
                        }
                        if (i == 4 || i == 8)
                        {
                            if (lblFunction.Text == lblPreviousFunction.Text)
                            {
                                if (((Label)row.FindControl("lblBusinessUnitName")).Text == lblBusinessUnitName.Text)
                                {
                                    row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                                    previousRow.Cells[i].Visible = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

    }
}