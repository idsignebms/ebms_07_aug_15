﻿<%@ Page Title=":: Assign Functional Permissions ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="AssignFunctionalPermissions.aspx.cs"
    Inherits="UMS_Nigeria.Admin.AssignFunctionalPermissions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" AssociatedUpdatePanelID="upnlCustomerReorder">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdatePanel ID="upnlCustomerReorder" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddCycle" runat="server" Text="Assign Functional Permissions"></asp:Literal>
                        </div>
                        <div class="star_text">
                            &nbsp
                            <%--<asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>--%>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="clr pad_5">
                    </div>
                    <div class="grid " align="center" style="float: left; margin-left: 10px; overflow-x: auto;
                        width: 98%;">
                        <asp:GridView ID="gvApprovalFunctions" runat="server" AutoGenerateColumns="False"
                            AlternatingRowStyle-CssClass="color" OnRowDataBound="gvApprovalFunctions_RowDataBound"
                            OnRowCommand="gvApprovalFunctions_RowCommand">
                            <EmptyDataRowStyle HorizontalAlign="center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="S.No" ItemStyle-HorizontalAlign="left">
                                    <ItemTemplate>
                                        <%#(Container.DataItemIndex+1)%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Function Name" ItemStyle-HorizontalAlign="left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFunctionId" Visible="false" runat="server" Text='<%# Eval("FunctionId") %>'></asp:Label>
                                        <asp:Label ID="lblFunction" runat="server" Text='<%# Eval("Function") %>'></asp:Label>
                                        <asp:Label ID="lblIsActive" runat="server" Text='<%# Eval("IsActive") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="assgn_input">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnActivate" runat="server" ToolTip="Activate" CommandArgument='<%# Eval("FunctionId") %>'
                                            CommandName='ACTIVE' Visible="false">
                                    <img src="../images/Activate.gif"   alt="Active" /></asp:LinkButton>
                                        <asp:LinkButton ID="lbtnDeActivate" Visible="false" runat="server" ToolTip="DeActivate"
                                            CommandArgument='<%# Eval("FunctionId") %>' CommandName='DEACTIVE'>
                                    <img src="../images/Deactivate.gif"   alt="DeActive" /></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle HorizontalAlign="center"></RowStyle>
                        </asp:GridView>
                    </div>
                    <asp:ModalPopupExtender ID="mpeChangeActive" runat="server" PopupControlID="PanelChangeActivate"
                        TargetControlID="btnChangeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnChangeActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelChangeActivate" runat="server" CssClass="modalPopup" Style="display: none;
                        border-color: #639B00;">
                        <div class="popheader" id="popChangeActive" runat="server">
                            <asp:Label ID="lblChangeActiveMessage" runat="server"></asp:Label>
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnChangeActive" OnClick="btnChangeActive_Click" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
    </script>
    <script src="../JavaScript/PageValidations/AssignApprovalLevels.js?v=1" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        $(document).ready(function () { assignDiv('rptrMainMasterMenu_115_6', '205') });
    </script>
</asp:Content>
