﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AssignFunctionalPermissions
                     
 Developer        : id065-V.Bhimaraju
 Creation Date    : 26-June-2015
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using System.Data;
using iDSignHelper;
using Resources;

namespace UMS_Nigeria.Admin
{
    public partial class AssignFunctionalPermissions : System.Web.UI.Page
    {
        #region Members
        iDsignBAL objIdsignBal = new iDsignBAL();
        ApprovalBal objApprovalBal = new ApprovalBal();
        UserManagementBAL objUserManagementBAL = new UserManagementBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        public string Key = "AssignFunctionalPermissions";
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.CssClass = lblMessage.Text = string.Empty;
            try
            {
                if (!IsPostBack)
                {
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvApprovalFunctions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIsActive = (Label)e.Row.FindControl("lblIsActive");
                    LinkButton lbtnActivate = (LinkButton)e.Row.FindControl("lbtnActivate");
                    LinkButton lbtnDeActivate = (LinkButton)e.Row.FindControl("lbtnDeActivate");
                    if (Convert.ToBoolean(lblIsActive.Text))
                        lbtnDeActivate.Visible = true;
                    else
                        lbtnActivate.Visible = true;
                }
            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvApprovalFunctions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                switch (e.CommandName.ToUpper())
                {

                    case "ACTIVE":
                        LinkButton lbtnActivate = (LinkButton)row.FindControl("lbtnActivate");
                        btnChangeActive.CommandArgument = "true";
                        popChangeActive.Attributes.Add("class", "popheader popheaderlblgreen");
                        btnChangeActive.CommandName = lbtnActivate.CommandArgument.ToString();
                        lblChangeActiveMessage.Text = "Are you sure do you want to Activate this approval Function?";
                        btnChangeActive.Visible = true;
                        btnChangeActive.Text = Resource.OK;
                        btnDeActiveCancel.Text = Resource.CANCEL;
                        mpeChangeActive.Show();
                        btnChangeActive.Focus();
                        break;
                    case "DEACTIVE":
                        LinkButton lbtnDeActivate = (LinkButton)row.FindControl("lbtnDeActivate");
                        ApprovalBe objApprovalBe = new ApprovalBe();
                        objApprovalBe.FunctionId = Convert.ToInt32(lbtnDeActivate.CommandArgument);
                        objApprovalBe = objIdsignBal.DeserializeFromXml<ApprovalBe>(objApprovalBal.Get(objApprovalBe, ReturnType.Fetch));
                        if (objApprovalBe.IsSuccess)
                        {
                            popChangeActive.Attributes.Add("class", "popheader popheaderlblred");
                            btnChangeActive.Visible = true;
                            btnChangeActive.Text = Resource.OK;
                            btnDeActiveCancel.Text = Resource.CANCEL;
                            btnChangeActive.CommandArgument = "false";
                            btnChangeActive.CommandName = lbtnDeActivate.CommandArgument.ToString();
                            lblChangeActiveMessage.Text = "Are you sure do you want to De-activate this approval Function?";
                            mpeChangeActive.Show();
                            btnChangeActive.Focus();
                        }
                        else
                        {
                            popChangeActive.Attributes.Add("class", "popheader popheaderlblred");
                            btnChangeActive.Visible = false;
                            btnDeActiveCancel.Text = Resource.OK;
                            lblChangeActiveMessage.Text = "Requests are still in pending for this Approval Function.";
                            mpeChangeActive.Show();
                            btnDeActiveCancel.Focus();
                        }
                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnChangeActive_Click(object sender, EventArgs e)
        {
            try
            {
                ApprovalBe objApprovalBe = new ApprovalBe();
                objApprovalBe.FunctionId = Convert.ToInt32(btnChangeActive.CommandName);
                objApprovalBe.IsActive = Convert.ToBoolean(btnChangeActive.CommandArgument);
                objApprovalBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objApprovalBe = objIdsignBal.DeserializeFromXml<ApprovalBe>(objApprovalBal.Insert(objApprovalBe, Statement.Change));
                if (objApprovalBe.IsSuccess)
                {
                    if (Convert.ToBoolean(btnChangeActive.CommandArgument))
                        Message(Resource.APPROVAL_ACTIVATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    else
                        Message(Resource.APPROVAL_DEACTIVATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        private void BindGrid()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = objApprovalBal.GetFunctionAccessSettings(ReturnType.Get);

                if (ds != null && ds.Tables.Count > 0)
                {
                    gvApprovalFunctions.DataSource = ds.Tables[0];
                    gvApprovalFunctions.DataBind();
                }
                else
                {
                    gvApprovalFunctions.DataSource = new DataTable();
                    gvApprovalFunctions.DataBind();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion
    }
}