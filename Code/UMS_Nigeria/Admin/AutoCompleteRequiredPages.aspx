﻿<%@ Page Title=":: Auto Complete Required Pages ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="AutoCompleteRequiredPages.aspx.cs"
    Inherits="UMS_Nigeria.Admin.AutoCompleteRequiredPages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server" AssociatedUpdatePanelID="upCustomerMandatoryFeilds">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <div class="inner-sec">
            <asp:UpdatePanel ID="upCustomerMandatoryFeilds" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litCustomerMandatoryFeilds" runat="server" Text="Auto Complete Required Pages"></asp:Literal>
                        </div>
                        <div class="star_text">
                            &nbsp<%--<asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>--%>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box" style="font-size: 92%;">
                        <asp:Repeater ID="rptrMandatoryFelids" runat="server" OnItemDataBound="rptrMandatoryFelids_OnItemDataBound">
                            <HeaderTemplate>
                                <div id="chblDiv" runat="server">
                                    <div class="customerMf">
                                        <div style="float: left; width: 50px; text-align: left;">
                                            <asp:Literal ID="lblRowNumber" runat="server" Text='S.No'></asp:Literal>
                                        </div>
                                        <div style="float: left; width: 300px; text-align: left">
                                            <asp:Literal ID="SpanText" runat="server" Text='Page Names'></asp:Literal>
                                        </div>
                                        <div style="float: left; width: 200px;">
                                            <asp:Literal ID="litIsMandatory" runat="server" Text='Is Mandatory'></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div id="chblDiv" runat="server">
                                    <div style="width: 96%; margin-top: 16px; padding-left: 20px;">
                                        <div style="float: left; width: 50px; text-align: left;">
                                            <asp:Literal ID="lblRowNumber" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString()%>'></asp:Literal>
                                        </div>
                                        <div style="float: left; width: 300px;">
                                            <asp:Literal ID="SpanText" runat="server" Text='<% #Eval("SpanText") %>'></asp:Literal>
                                        </div>
                                        <div style="float: left; width: 200px;">
                                            <asp:Literal ID="SpanId" runat="server" Visible="False" Text=''></asp:Literal>
                                            <asp:Label ID="lblPageId" runat="server" Visible="False" Text='<% #Eval("PageId") %>'></asp:Label>
                                            <asp:Label ID="lblIsActive" runat="server" Visible="False" Text='<% #Eval("IsActive") %>'></asp:Label>
                                            <asp:RadioButtonList ID="rblIsMandatory" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:Repeater>
                        <div class="clr">
                        </div>
                    </div>
                    <div class="box_total">
                        <div class="box_totalTwi_c" style="margin-left: 0">
                            <br />
                            <asp:Button ID="btnSave" TabIndex="2" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                CssClass="box_s" runat="server" OnClick="btnSave_OnClick" />
                            <asp:Button ID="btnCancel" TabIndex="3" Text="<%$ Resources:Resource, RESET%>" CssClass="box_s"
                                runat="server" OnClick="btnCancel_OnClick" />
                        </div>
                    </div>
                    <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                        TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" DefaultButton="btngridalertok"
                        Style="display: none;">
                        <div class="popheader popheaderlblred">
                            <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Edit Confirmation popup Start--%>
                    <asp:ModalPopupExtender ID="mpeEditConfirmation" runat="server" PopupControlID="PanelEditConfirmationOk"
                        TargetControlID="btnEditConfirmationOk" BackgroundCssClass="modalBackground"
                        CancelControlID="btnConfirmCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnEditConfirmationOk" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelEditConfirmationOk" runat="server" CssClass="modalPopup" Style="display: none;">
                        <div class="popheader popheaderlblred">
                            <asp:Label ID="lblEditConfirmationMsg" runat="server"></asp:Label>
                        </div>
                        <div class="space">
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnConfirmOk" OnClick="btnConfirmOk_OnClick" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnConfirmCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Edit Confirmation popup Closed--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/AddRoles.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
