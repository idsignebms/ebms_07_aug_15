﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using Resources;

namespace UMS_Nigeria.Admin
{
    public partial class AutoCompleteRequiredPages : System.Web.UI.Page
    {
        #region Members
        CommonMethods _objCommonMethods = new CommonMethods();
        private string Key = "AutoCompleteRequiredPages";
        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                BindMandatory();
            }
        }

        private void BindMandatory()
        {
            DataSet ds = new DataSet();
            ds.ReadXml(Server.MapPath("~/Xmls/AutoCompleteFiledPages.xml"));
            rptrMandatoryFelids.DataSource = ds;
            rptrMandatoryFelids.DataBind();
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            lblEditConfirmationMsg.Text = "Are you sure to edit Auto Complete required pages?";
            mpeEditConfirmation.Show();
            btnEditConfirmationOk.Focus();
        }

        protected void rptrMandatoryFelids_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                RadioButtonList rblIsMandatory = (RadioButtonList)e.Item.FindControl("rblIsMandatory");
                Label lblIsActive = (Label)e.Item.FindControl("lblIsActive");
                Label lblPageId = (Label)e.Item.FindControl("lblPageId");
                Literal lblRowNumber = (Literal)e.Item.FindControl("lblRowNumber");
                Literal SpanText = (Literal)e.Item.FindControl("SpanText");
                ListItem Yes = rblIsMandatory.Items.FindByValue("Yes");
                ListItem No = rblIsMandatory.Items.FindByValue("No");

                if (Convert.ToBoolean(lblIsActive.Text))
                    Yes.Selected = true;
                else
                    No.Selected = true;
            }
        }

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            BindMandatory();
        }

        protected void btnConfirmOk_OnClick(object sender, EventArgs e)
        {
            try
            {

                XmlDocument x = new XmlDocument();
                x.Load(Server.MapPath("~/Xmls/AutoCompleteFiledPages.xml"));
                XmlNodeList xl = x.GetElementsByTagName("Page");
                for (int j = 0; j < rptrMandatoryFelids.Items.Count; j++)
                {
                    Label lblPageId = (Label)rptrMandatoryFelids.Items[j].FindControl("lblPageId");
                    RadioButtonList rblIsMandatory = (RadioButtonList)rptrMandatoryFelids.Items[j].FindControl("rblIsMandatory");
                    bool IsMandatory = rblIsMandatory.Items.FindByValue("Yes").Selected;
                    for (int i = 0; i < xl.Count; i++)
                    {
                        if (lblPageId.Text == xl[i].Attributes["PageId"].Value)
                        {
                            xl[i].Attributes["IsActive"].Value = IsMandatory.ToString();
                        }
                    }
                }
                x.Save(Server.MapPath("~/Xmls/AutoCompleteFiledPages.xml"));
                Message("Successfully edited Auto Complete required fields.", UMS_Resource.MESSAGETYPE_SUCCESS);
                BindMandatory();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
    }
}