﻿<%@ Page Title=":: Change Meter BusinessUnit ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="ChangeMeterBusinessUnit.aspx.cs"
    Inherits="UMS_Nigeria.Admin.ChangeMeterBusinessUnit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <div class="out-bor">
        <asp:UpdatePanel ID="upStates" runat="server">
            <ContentTemplate>
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text-heading">
                        <asp:Literal ID="litReadMethod" runat="server" Text="Change Meter BusinessUnit"></asp:Literal>
                    </div>
                    <div class="star_text">
                        &nbsp;
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="litMeterNo" runat="server" Text="<%$ Resources:Resource, METERNO%>"></asp:Literal>
                                <div class="space"></div>
                                <asp:TextBox CssClass="text-box" ID="txtMeterNo" TabIndex="1" MaxLength="50" placeholder="Enter Meter No"
                                    runat="server"></asp:TextBox>
                                <span id="spanMeterNo" class="spancolor"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <asp:Literal ID="litMeterSerialNo" runat="server" Text="<%$ Resources:Resource, METER_SERIAL_NO%>"></asp:Literal>
                                <div class="space"></div>
                                <asp:TextBox CssClass="text-box" ID="txtSerialNo" TabIndex="2" MaxLength="50" placeholder="Enter Meter Serial Number"
                                    runat="server"></asp:TextBox>
                                <span id="spanSerialNo" class="spancolor"></span>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <asp:Literal ID="litBrand" runat="server" Text="<%$ Resources:Resource, BRAND%>"></asp:Literal>
                                <div class="space"></div>
                                <asp:TextBox CssClass="text-box" ID="txtBrand" TabIndex="3" MaxLength="50" runat="server"
                                    placeholder="Enter Meter Brand"></asp:TextBox>
                                <span id="spanBrand" class="spancolor"></span>
                            </div>
                        </div>
                        <div class="box_total">
                            <div class="box_total_a">
                                <asp:Button ID="btnSearch" TabIndex="4" OnClick="btnSearch_Click"
                                    CssClass="box_s" Text="<%$ Resources:Resource, SEARCH%>" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    <br /><br />
                    </div>
                    <div class="grid_boxes">
                        <div>
                            <div class="grid_pagingTwo_top">
                                <div class="paging_top_title">
                                    <asp:Literal ID="litGridStatesList" runat="server" Text="Meter Information"></asp:Literal>
                                </div>
                                <div class="paging_top_rightTwo_content" id="divPagin1" runat="server">
                                    <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvMetersList" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                            HeaderStyle-CssClass="grid_tb_hd" OnRowCommand="gvMetersList_RowCommand" OnRowDataBound="gvMetersList_RowDataBound">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no meter information.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:BoundField HeaderText="<%$ Resources:Resource, GRID_SNO%>" DataField="RowNumber"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, METERNO%>"
                                    DataField="MeterNo" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField HeaderText="<%$ Resources:Resource, METER_SERIAL_NO%>" DataField="MeterSerialNo"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, METER_TYPE%>"
                                    DataField="MeterType" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, SIZE%>"
                                    DataField="MeterSize" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField HeaderText="<%$ Resources:Resource, MULTIPLIER%>" DataField="MeterMultiplier"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, BRAND%>"
                                    DataField="MeterBrand" />
                                <asp:BoundField HeaderText="<%$ Resources:Resource, METER_RATING%>" DataField="MeterRating"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField HeaderText="<%$ Resources:Resource, NEXT_CALIBRATION_DATE%>" DataField="NextCalibrationDate"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <%--<asp:BoundField HeaderText="<%$ Resources:Resource, SERVICE_HOURS_BEFORE_CALIBRATION%>"
                                    DataField="ServiceHoursBeforeCalibration" ItemStyle-CssClass="grid_tb_td" />--%>
                                <asp:BoundField HeaderText="<%$ Resources:Resource, METER_DIALS%>" DataField="MeterDials"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <%--<asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, DETAILS%>"
                                    DataField="Details" />--%>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, BUSINESS_UNIT%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBU_ID" Visible="false" runat="server" Text='<%#Eval("BU_ID") %>'></asp:Label>
                                        <asp:Label ID="lblMeterId" Visible="false" runat="server" Text='<%#Eval("MeterId") %>'></asp:Label>
                                        <asp:DropDownList ID="ddlGridBU" runat="server" Visible="false">
                                            <asp:ListItem Text="--Select--"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:Label ID="lblBUName" runat="server" Text='<%#Eval("BusinessUnitName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Is CAPMI Meter?" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIsCAPMI" runat="server" Text='<%#Eval("IsCAPMIMeter") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CAPMI Amount" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCAPMIAmount" runat="server" Text='<%#Eval("CAPMIAmount") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lkbtnEdit" runat="server" ToolTip="Edit" Text="Edit" CommandName="EditMeter">
                                        <img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnUpdate" Visible="false" runat="server" ToolTip="Update"
                                            CommandName="UpdateMeter"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnCancel" Visible="false" runat="server" ToolTip="Cancel"
                                            CommandName="CancelMeter"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                        <%--<asp:LinkButton ID="lkbtnActive" Visible="false" runat="server" ToolTip="Activate Meter Information"
                                            CommandName="ActiveMeter"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnDeActive" runat="server" ToolTip="DeActivate Meter Information"
                                            CommandName="DeActiveMeter"><img src="../images/Deactivate.gif" alt="DeActive" /></asp:LinkButton>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="grid_boxes">
                        <div id="divPagin2" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                <%--Variables decleration starts--%>
                <asp:HiddenField ID="hfPageSize" runat="server" />
                <asp:HiddenField ID="hfPageNo" runat="server" />
                <asp:HiddenField ID="hfLastPage" runat="server" />
                <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                <div id="hidepopup">
                    <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                        TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                        <div class="popheader popheaderlblred">
                            <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--Variables decleration ends--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/ChangeMeterBusinessUnit.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>

    <script language="javascript" type="text/javascript">
        $(document).ready(function () { assignDiv('rptrMainMasterMenu_93_2', '118') });
    </script>
    <%--==============Ajax loading script ends==============--%>
</asp:Content>
