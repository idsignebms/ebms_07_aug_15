﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;

namespace UMS_Nigeria.Admin
{
    public partial class ChangeMeterBusinessUnit : System.Web.UI.Page
    {
        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        ReportsBal _objRerpotsBal = new ReportsBal();
        MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        string Key = "ChangeMeterBusinessUnit";

        private bool IsSearchClick = false;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                IsSearchClick = true;
                BindGrid();
                UCPaging1.GeneratePaging();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvMetersList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    
                    DropDownList ddlGridBU = (DropDownList)e.Row.FindControl("ddlGridBU");

                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + ddlGridBU.ClientID+ "')");
                    
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvMetersList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);                
                DropDownList ddlGridBU = (DropDownList)row.FindControl("ddlGridBU");                

                Label lblBUName = (Label)row.FindControl("lblBUName");
                Label lblBU_ID = (Label)row.FindControl("lblBU_ID");
                Label lblMeterId = (Label)row.FindControl("lblMeterId");
                

                switch (e.CommandName.ToUpper())
                {
                    case "EDITMETER": //
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnEdit").Visible = false;
                        lblBUName.Visible =  false;
                        ddlGridBU.Visible =  true;                        
                        
                        objCommonMethods.BindBusinessUnits(ddlGridBU,string.Empty, true);
                        ddlGridBU.SelectedIndex = ddlGridBU.Items.IndexOf(ddlGridBU.Items.FindByValue(lblBU_ID.Text));
                        
                        break;
                    case "CANCELMETER":
                        row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                        lblBUName.Visible = true;
                        ddlGridBU.Visible = false;
                        break;
                    case "UPDATEMETER":
                        objMastersBE.Flag = 1;
                        objMastersBE.MeterId = Convert.ToInt32(lblMeterId.Text);
                        objMastersBE.BU_ID = ddlGridBU.SelectedValue;
                        objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.MeterType(objMastersBE, Statement.Check));

                        if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        {
                            BindGrid();                            
                            Message(Resource.METER_INFO_UPDATED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        else if (objMastersBE.IsMeterNoExists)
                        {
                            Message(UMS_Resource.METERNO_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else if (objMastersBE.IsMeterSerialNoExists)
                        {
                            Message(UMS_Resource.METER_SERIALNO_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else
                            Message(UMS_Resource.METER_INFO_UPDATE_FAILED, UMS_Resource.MESSAGETYPE_ERROR);

                        break;                    
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }
        #endregion

        #region Methods
        public void BindGrid()
        {
            try
            {
                //Removing spaces at end and start
                txtMeterNo.Text = txtMeterNo.Text.TrimStart();
                txtMeterNo.Text = txtMeterNo.Text.Trim();

                txtBrand.Text = txtBrand.Text.TrimStart();
                txtBrand.Text = txtBrand.Text.Trim();

                txtSerialNo.Text = txtSerialNo.Text.TrimStart();
                txtSerialNo.Text = txtSerialNo.Text.Trim();

                MasterSearchBE _objSearchBe = new MasterSearchBE();
                _objSearchBe.PageNo = IsSearchClick ? Convert.ToInt32(Constants.pageNoValue) : Convert.ToInt32(hfPageNo.Value);
                _objSearchBe.PageSize = PageSize;
                _objSearchBe.Flag = Convert.ToInt32(SearchFlags.ChangeMeterBusinessUnit);
                _objSearchBe.Brand = txtBrand.Text.Trim();
                _objSearchBe.MeterNo = txtMeterNo.Text.Trim();
                _objSearchBe.MeterSerialNo = txtSerialNo.Text.Trim();
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                _objSearchBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    _objSearchBe.BU_ID = string.Empty;
                DataSet ds = new DataSet();
                ds = _objRerpotsBal.SearchMasterBAL(_objSearchBe);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    hfTotalRecords.Value = ds.Tables[0].Rows[0]["TotalRecords"].ToString();
                    gvMetersList.DataSource = ds.Tables[0];
                    gvMetersList.DataBind();
                    divPagin1.Visible = divPagin2.Visible = true;
                }
                else
                {
                    hfTotalRecords.Value = 0.ToString();
                    gvMetersList.DataSource = new DataTable();
                    gvMetersList.DataBind();
                    divPagin1.Visible = divPagin2.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}