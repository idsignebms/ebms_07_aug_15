﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using Resources;
using System.Data;
using UMS_NigeriaBAL;
using System.Xml;
using iDSignHelper;

namespace UMS_Nigeria.Admin
{
    public partial class CommunicationFeature : System.Web.UI.Page
    {
        #region Members
        CommonMethods objCommonMethods = new CommonMethods();
        CommunicationFeaturesBAL objCommunicationFeaturesBAL = new CommunicationFeaturesBAL();
        iDsignBAL objIdsignBal = new iDsignBAL();
        private string Key = "CommunicationFeature";
        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                BindFeatures();
            }

            CommunicationFeaturesBE objCommunicationFeaturesBE = new CommunicationFeaturesBE();
            objCommunicationFeaturesBE.CommunicationFeaturesID = (int)CommunicationFeatures.Adjustments;
            objCommonMethods.IsEmailFeatureEnabled(objCommunicationFeaturesBE);
            objCommonMethods.IsSMSFeatureEnabled(objCommunicationFeaturesBE);

        }

        private void BindFeatures()
        {
            DataSet ds = new DataSet();
            ds = objCommunicationFeaturesBAL.GetCommunicatinFeatures();
            rptrCommunicationFeature.DataSource = ds;
            rptrCommunicationFeature.DataBind();
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            lblEditConfirmationMsg.Text = "Are you sure to edit Communication features?";
            mpeEditConfirmation.Show();
            btnEditConfirmationOk.Focus();
        }

        protected void rptrCommunicationFeature_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                RadioButtonList rblIsEmailEnabled = (RadioButtonList)e.Item.FindControl("rblIsEmailEnabled");
                RadioButtonList rblIsSMSEnabled = (RadioButtonList)e.Item.FindControl("rblIsSMSEnabled");
                //Label lblRowNumber = (Label)e.Item.FindControl("lblRowNumber");
                Label lblIsEmailEnabled = (Label)e.Item.FindControl("lblIsEmailEnabled");
                Label lblIsSMSEnabled = (Label)e.Item.FindControl("lblIsSMSEnabled");
                Label CommunicationFeaturesID = (Label)e.Item.FindControl("CommunicationFeaturesID");
                Literal lblRowNumber = (Literal)e.Item.FindControl("lblRowNumber");
                Literal FeatureName = (Literal)e.Item.FindControl("FeatureName");
                ListItem EmailYes = rblIsEmailEnabled.Items.FindByValue("Yes");
                ListItem EmailNo = rblIsEmailEnabled.Items.FindByValue("No");
                ListItem SMSYes = rblIsSMSEnabled.Items.FindByValue("Yes");
                ListItem SMSNo = rblIsSMSEnabled.Items.FindByValue("No");
                //lblRowNumber.Text = rptrMandatoryFelids.ite


                if (Convert.ToBoolean(lblIsEmailEnabled.Text))
                    EmailYes.Selected = true;
                else
                    EmailNo.Selected = true;

                if (Convert.ToBoolean(lblIsSMSEnabled.Text))
                    SMSYes.Selected = true;
                else
                    SMSNo.Selected = true;

            }
        }

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            BindFeatures();
        }

        protected void btnConfirmOk_OnClick(object sender, EventArgs e)
        {
            try
            {
                CommunicationFeaturesBE objCommunicationFeaturesBE = new CommunicationFeaturesBE();
                XmlDocument xml = new XmlDocument();
                //x.Load(Server.MapPath("~/Xmls/NewCustomer_Mandatory.xml"));
                //XmlNodeList xl = x.GetElementsByTagName("Control");
                for (int j = 0; j < rptrCommunicationFeature.Items.Count; j++)
                {
                    Label CommunicationFeaturesID = (Label)rptrCommunicationFeature.Items[j].FindControl("lblCommunicationFeaturesID");
                    RadioButtonList rblIsEmailEnabled = (RadioButtonList)rptrCommunicationFeature.Items[j].FindControl("rblIsEmailEnabled");
                    RadioButtonList rblIsSMSEnabled = (RadioButtonList)rptrCommunicationFeature.Items[j].FindControl("rblIsSMSEnabled");
                    bool IsEmailEnabled = rblIsEmailEnabled.Items.FindByValue("Yes").Selected;
                    bool IsSMSEnabled = rblIsSMSEnabled.Items.FindByValue("Yes").Selected;
                    if (Convert.ToInt16(CommunicationFeaturesID.Text) == (int)CommunicationFeatures.BillGeneration)
                    {
                        objCommunicationFeaturesBE.IsEmailEnabledBillGeneration = IsEmailEnabled;
                        objCommunicationFeaturesBE.IsSMSEnabledBillGeneration = IsSMSEnabled;
                    }
                    else if (Convert.ToInt16(CommunicationFeaturesID.Text) == (int)CommunicationFeatures.Payments)
                    {
                        objCommunicationFeaturesBE.IsEmailEnabledPayments = IsEmailEnabled;
                        objCommunicationFeaturesBE.IsSMSEnabledPayments = IsSMSEnabled;
                    }
                    else if (Convert.ToInt16(CommunicationFeaturesID.Text) == (int)CommunicationFeatures.Adjustments)
                    {
                        objCommunicationFeaturesBE.IsEmailEnabledAdjustments = IsEmailEnabled;
                        objCommunicationFeaturesBE.IsSMSEnabledAdjustments = IsSMSEnabled;
                    }

                }
                xml = objCommunicationFeaturesBAL.UpdateCommunicationFeatures(objCommunicationFeaturesBE);
                objCommunicationFeaturesBE = objIdsignBal.DeserializeFromXml<CommunicationFeaturesBE>(xml);
                if (objCommunicationFeaturesBE.IsSuccess)
                    Message("Successfully edited Communication features.", UMS_Resource.MESSAGETYPE_SUCCESS);
                else
                    Message("Error in updating Communication features.", UMS_Resource.MESSAGETYPE_ERROR);

                BindFeatures();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
    }
}