﻿<%@ Page Title=":: Edit Menu ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    AutoEventWireup="true" Theme="Green" CodeBehind="EditMenu.aspx.cs" Inherits="UMS_Nigeria.Admin.EditMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server" AssociatedUpdatePanelID="upEditMenu">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <div class="inner-sec">
            <asp:UpdatePanel ID="upEditMenu" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litEditMenus" runat="server" Text="Edit Menu"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">                            
                        </div>
                        <div class="clr">
                        </div>
                        <div style="width: 700px; margin-top: 62px; padding:20px;">
                            <div class="inner-box3" style="width: 100%; float: left; height: auto; margin-top: -83px;
                                top: -140px; right: 310px; }">
                                <div id="divMenu" runat="server" class="text-inner">
                                    <label for="name" style="color: #0078c5">
                                        <asp:Literal ID="litFullPath" runat="server"></asp:Literal>
                                    </label>
                                    <div class="space" style="margin-bottom: 10px;">
                                    </div>
                                    <label for="name">
                                        <asp:Literal ID="litMenuName" runat="server" Text="Menu Name"></asp:Literal><span
                                            class="span_star">*</span>
                                    </label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox CssClass="text-box" Width="24%" ID="txtMenuName" TabIndex="3" runat="server"
                                        onblur="return TextBoxBlurValidationlbl(this,'spanMenuName','Menu Name')" MaxLength="50"
                                        placeholder="Enter Menu Name"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="ftbTxtMenuName" runat="server" TargetControlID="txtMenuName"
                                        FilterType="LowercaseLetters, UppercaseLetters, Custom" ValidChars=" ,-/&">
                                    </asp:FilteredTextBoxExtender>
                                    <span id="spanMenuName" class="span_color"></span>
                                </div>
                                <%--<asp:Button ID="btnSave" Visible="False" TabIndex="3" OnClientClick="return Validate();"
                                    Text="<%$ Resources:Resource, SAVE%>" CssClass="box_s" runat="server" OnClick="btnSave_OnClick" />--%>
                                <br />
                                <asp:Button ID="btnUpdate" TabIndex="3" OnClientClick="return Validate();" Text="<%$ Resources:Resource, UPDATE%>"
                                    CssClass="box_s" runat="server" OnClick="btnUpdate_OnClick" />
                                <asp:Button ID="btnCancel" TabIndex="4" Text="<%$ Resources:Resource, CANCEL%>" CssClass="box_s"
                                    runat="server" OnClick="btnCancel_Click" />
                                <asp:Button ID="btnDeactivate" Visible="False" TabIndex="5" Text="DEACTIVATE" CssClass="box_s"
                                    runat="server" />
                            </div>
                        </div>
                        <div class="inner-box1" style="margin-top:10px;">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litEditMenu" runat="server" Text="Main Menu"></asp:Literal>
                                </label>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div style="width: 150px; float: left; margin-bottom: 20px; margin-top:5px;">
                            <div class="inner-box1" style="width: 0px;">
                                <asp:TreeView ID="tvMenu" ExpandDepth="0" runat="server" OnSelectedNodeChanged="tvMenu_SelectedNodeChanged"
                                    OnTreeNodePopulate="tvMenu_TreeNodePopulate">
                                    <RootNodeStyle Font-Bold="True" />
                                    <SelectedNodeStyle BackColor="#7FBF4D" Font-Bold="True" Font-Italic="True" ForeColor="White" />
                                </asp:TreeView>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                    </div>
                    <!--Dynamic start -->
                    <asp:HiddenField ID="hfRefMenuIDString" runat="server" Value="" />
                    <div id="divGrandParent" runat="server" class="inner-box">
                        <%--<div id="divParent" class="inner-box1">
                            <div ID="divChield" class="text-inner">
                            </div>
                        </div>--%>
                    </div>
                    <!--Dynamic end -->
                    <br />
                    <br />
                    <div class="box_total">
                        <div class="box_totalTwi_c">
                            <div class="space">
                            </div>
                            <div class="space">
                            </div>
                            <%--<asp:Button ID="btnAdd" TabIndex="2" Text="<%$ Resources:Resource, ADD%>" CssClass="box_s"
                                runat="server" Visible="False" OnClick="btnAdd_Click" />
                            <asp:Button ID="btnEdit" TabIndex="4" Text="<%$ Resources:Resource, EDIT%>" CssClass="box_s"
                                runat="server" Visible="False" OnClick="btnEdit_Click" />--%>
                            <%--<asp:Button ID="btnDelete" Visible="False" TabIndex="6" Text="<%$ Resources:Resource, DELETE%>"
                                CssClass="box_s" runat="server" />--%>
                        </div>
                    </div>
                    <div id="hidepopup">
                        <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                            TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;" DefaultButton="btngridalertok">
                            <div class="popheader popheaderlblred">
                                <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                            </div>
                            <div class="footer popfooterbtnrt">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/PageValidations/EditMenu.js" type="text/javascript"></script>

     <script language="javascript" type="text/javascript">
         $(document).ready(function () { assignDiv('rptrMainMasterMenu_171_8', '171') });
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
