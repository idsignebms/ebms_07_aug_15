﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Services.Description;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using iDSignHelper;
using Resources;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;

namespace UMS_Nigeria.Admin
{
    public partial class EditMenu : System.Web.UI.Page
    {
        #region Members
        iDsignBAL objIdsignBal = new iDsignBAL();
        MenuBAL objMenuBAL = new MenuBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        XmlDocument xml = null;
        MenuListBE objMenuListBE = new MenuListBE();
        MenuBE _objMenuBE = new MenuBE();
        public string Key = "EditMenu";

        public delegate void SelectedIndexChange();
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE, false);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.Visible = false;
            if (!IsPostBack)
            {
                PopulateRootLevel();
                TreeNode tn = tvMenu.Nodes[0];
                tn.Selected = litFullPath.Visible = true;
                litFullPath.Text = getFullNodepath();
                txtMenuName.Text = tvMenu.SelectedNode.Text;
            }
        }

        protected void tvMenu_TreeNodePopulate(object sender, TreeNodeEventArgs e)
        {

            objMenuListBE =
                objIdsignBal.DeserializeFromXml<MenuListBE>(
                    objMenuBAL.GetMenuList());
            PopulateSubLevel(int.Parse(e.Node.Value), e.Node);
        }

        protected void tvMenu_SelectedNodeChanged(object sender, EventArgs e)
        {
            litFullPath.Text = getFullNodepath();
            txtMenuName.Text = tvMenu.SelectedNode.Text;
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            //if (!tvMenu.Nodes[0].Selected)
            //{
                _objMenuBE.MenuId = Convert.ToInt32(tvMenu.SelectedValue);
                _objMenuBE.Name = txtMenuName.Text.Trim();
                _objMenuBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                xml = objMenuBAL.Menu(_objMenuBE, Statement.Update);
                _objMenuBE = objIdsignBal.DeserializeFromXml<MenuBE>(xml);


                if (_objMenuBE.RowsEffected > 0)
                {
                    tvMenu.SelectedNode.Text = txtMenuName.Text.Trim();
                    litFullPath.Text = getFullNodepath();
                    Message("Menu name updated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                {
                    Message("Menu name already Exists in current context.", UMS_Resource.MESSAGETYPE_ERROR);
                }
            //}
            //else
            //{
            //    Message("Main Menu can not be edited.", UMS_Resource.MESSAGETYPE_ERROR);
            //}
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            tvMenu.Nodes[0].Selected = true;
            litFullPath.Text = getFullNodepath();
            txtMenuName.Text = tvMenu.SelectedNode.Text;
            tvMenu.CollapseAll();
            //divActType.Visible = btnSave.Visible = btnUpdate.Visible = btnCancel.Visible = false;
            //btnAdd.Visible = tvMenu.Enabled= true;
            //btnEdit.Visible = btnDelete.Visible = true;
        }

        #endregion

        #region Methods

        private void PopulateRootLevel()
        {
            objMenuListBE =
                    objIdsignBal.DeserializeFromXml<MenuListBE>(
                        objMenuBAL.GetMenuList());
            var _objMenuListBE =
                objMenuListBE.Items.Where(x => x.ReferenceMenuId == 0 && x.IsActive).OrderBy(x => x.Menu_Order).ToList();
            PopulateNodes(_objMenuListBE, tvMenu.Nodes);
        }

        private void PopulateNodes(List<MenuBE> objMenuBE, TreeNodeCollection nodes)
        {

            foreach (var Item in objMenuBE)
            {
                TreeNode tn = new TreeNode();
                tn.Text = Item.Name;
                tn.Value = Item.MenuId.ToString();
                tn.SelectAction = TreeNodeSelectAction.SelectExpand;
                nodes.Add(tn);
                // If node has child nodes, then enable on-demand populating
                tn.PopulateOnDemand = objMenuListBE.Items.Exists(x => x.ReferenceMenuId == Item.MenuId);
            }
        }

        private void PopulateSubLevel(int parentid, TreeNode parentNode)
        {
            var _objgoMenuBE =
                objMenuListBE.Items.Where(x => x.ReferenceMenuId == parentid && x.IsActive).OrderBy(x => x.Page_Order).ToList();
            PopulateNodes(_objgoMenuBE, parentNode.ChildNodes);
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public string getFullNodepath()
        {
            string FullPath = string.Empty;
            int depth = tvMenu.SelectedNode.Depth;
            List<string> s = new List<string>();
            TreeNode node = tvMenu.SelectedNode;
            for (int i = depth; i > -1; i--)
            {
                s.Add(node.Text);
               
                node = node.Parent;
            }
            for (int i = s.Count; i > 0; i--)
            {
                if (!string.IsNullOrEmpty(FullPath))
                    FullPath += " --> " + s[i - 1];
                else
                    FullPath = s[i - 1];
            }
            return FullPath;
        }

        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}