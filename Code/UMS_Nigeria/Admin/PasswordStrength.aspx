﻿<%@ Page Title=":: Password Strength Management ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    EnableEventValidation="true" Theme="Green" AutoEventWireup="true" CodeBehind="PasswordStrength.aspx.cs"
    Inherits="UMS_Nigeria.Admin.PasswordStrength" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdatePanel ID="upAddRegion" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddState" runat="server" Text="Password Strength Management"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1" style="width: 700px;">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litMinNoOfCapitalLetters" runat="server" Text="Min Capital Letters"></asp:Literal>
                                </label>
                                <asp:TextBox ID="txtMinNoOfCapitalLetters" TabIndex="1" runat="server" MaxLength="1"
                                    onblur="return TextBoxBlurValidationlbl(this,'spanMinPasswordLength','Min Password Length')"
                                    placeholder="Enter Min no of Capital Letters" CssClass="text-box" Style="width: 100px;"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbTxtMinNoOfCapitalLetters" runat="server" TargetControlID="txtMinNoOfCapitalLetters"
                                    FilterType="Numbers" ValidChars=" ">
                                </asp:FilteredTextBoxExtender>
                                <%-- <asp:RangeValidator ID="rngMinCap" runat="server" ErrorMessage="Enter Min Capital Letter between 1 to 6"
                                    class="span_color" Style="display: inline;" ControlToValidate="txtMinNoOfCapitalLetters"
                                    MinimumValue="1" MaximumValue="6" Type="Integer"></asp:RangeValidator>--%>
                                <%--<asp:TextBox ID="txtMaxNoOfCapitalLetters" TabIndex="1" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanMaxNoOfCapitalLetters','Max No. of Capital Letters')"
                                    MaxLength="50" placeholder="Enter Max no of Capital Letters" CssClass="text-box"
                                    Style="width: 100px;"></asp:TextBox>--%>
                                <br />
                                <span id="spanMinNoOfCapitalLetters" class="span_color"></span>
                                <%--<span id="spanMaxNoOfCapitalLetters" class="span_color"></span>--%>
                            </div>
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litMinNoOfSmallLetters" runat="server" Text="Min Small Letters"></asp:Literal>
                                </label>
                                <asp:TextBox ID="txtMinNoOfSmallLetters" TabIndex="1" runat="server" MaxLength="1"
                                    placeholder="Enter Min no of Small Letters" CssClass="text-box" Style="width: 100px;"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbTxtMinNoOfSmallLetters" runat="server" TargetControlID="txtMinNoOfSmallLetters"
                                    FilterType="Numbers" ValidChars=" ">
                                </asp:FilteredTextBoxExtender>
                                <%--  <asp:RangeValidator ID="rngSmall" runat="server" ErrorMessage="Enter Min Small Letter between 1 to 6"
                                    class="span_color" Style="display: inline;" ControlToValidate="txtMinNoOfSmallLetters"
                                    MinimumValue="1" Type="Integer" MaximumValue="6"></asp:RangeValidator>--%>
                                <%--<asp:TextBox ID="txtMaxNoOfSmallLetters" TabIndex="1" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanMaxNoOfSmallLetters','Max No. of Small Letters')"
                                    MaxLength="50" placeholder="Enter Max no of Capital Letters" CssClass="text-box"
                                    Style="width: 100px;"></asp:TextBox>--%>
                                <br />
                                <span id="spanMinNoOfSmallLetters" class="span_color"></span>
                                <%--<span id="spanMaxNoOfSmallLetters" class="span_color"></span>--%>
                            </div>
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litSpecialCharecters" runat="server" Text="Min Special characters [!@#$%^&']"></asp:Literal>
                                </label>
                                <asp:TextBox ID="txtMinSpecialCharecters" TabIndex="1" runat="server" MaxLength="1"
                                    placeholder="Enter Min no of Special characters" CssClass="text-box" Style="width: 100px;"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbTxtMinSpecialCharecters" runat="server" TargetControlID="txtMinSpecialCharecters"
                                    FilterType="Numbers" ValidChars=" ">
                                </asp:FilteredTextBoxExtender>
                                <%-- <asp:RangeValidator ID="rngSpecial" runat="server" ErrorMessage="Enter Min Special Character between 1 to 6"
                                    class="span_color" Style="display: inline;" ControlToValidate="txtMinSpecialCharecters"
                                    MinimumValue="1" Type="Integer" MaximumValue="6"></asp:RangeValidator>--%>
                                <%--<asp:TextBox ID="txtMaxSpecialCharecters" TabIndex="1" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanMaxNoOfSpecialCharecters','Special Max No. of Charecters')"
                                    MaxLength="50" placeholder="Enter Max no of Capital Letters" CssClass="text-box"
                                    Style="width: 100px;"></asp:TextBox>--%>
                                <br />
                                <span id="spanMinNoOfSpecialCharecters" class="span_color"></span>
                                <%--<span id="spanMaxNoOfSpecialCharecters" class="span_color"></span>--%>
                            </div>
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litMinNoOfNumericValue" runat="server" Text="Min Numeric Letters"></asp:Literal>
                                </label>
                                <asp:TextBox ID="txtMinNoOfNumericValue" TabIndex="1" runat="server" MaxLength="1"
                                    placeholder="Enter Min No. Of Numeric Value" CssClass="text-box" Style="width: 100px;"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbTxtMinNoOfNumericValue" runat="server" TargetControlID="txtMinNoOfNumericValue"
                                    FilterType="Numbers" ValidChars=" ">
                                </asp:FilteredTextBoxExtender>
                                <%--  <asp:RangeValidator ID="rngNumeric" runat="server" ErrorMessage="Enter Min Numeric Letter between 1 to 6"
                                    class="span_color" Style="display: inline;" ControlToValidate="txtMinNoOfNumericValue"
                                    MinimumValue="1" Type="Integer" MaximumValue="6"></asp:RangeValidator>--%>
                                <%--<asp:TextBox ID="txtMaxNoOfNumericValue" TabIndex="1" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanMaxNoOfNumericValue','Max No. Of Numeric Value')"
                                    MaxLength="50" placeholder="Enter Max No. Of Numeric Value" CssClass="text-box"
                                    Style="width: 100px;"></asp:TextBox>--%>
                                <br />
                                <span id="spanMinNoOfNumericValue" class="span_color"></span>
                                <%--<span id="spanMaxNoOfNumericValue" class="span_color"></span>--%>
                            </div>
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litMinPasswordLength" runat="server" Text="Min Password Length"></asp:Literal><span
                                        class="span_star">*</span>
                                </label>
                                <asp:TextBox ID="txtMinPasswordLength" TabIndex="1" runat="server" MaxLength="2"
                                    placeholder="Enter Min Password Length" CssClass="text-box" Style="width: 100px;"></asp:TextBox>
                                <%--onblur="return TextBoxBlurValidationlbl(this,'spanMinPasswordLength','Min Password Length')"--%>
                                <asp:FilteredTextBoxExtender ID="ftbTxtMinPasswordLength" runat="server" TargetControlID="txtMinPasswordLength"
                                    FilterType="Numbers" ValidChars=" ">
                                </asp:FilteredTextBoxExtender>
                                <br />
                                <%--<asp:TextBox ID="txtMaxPasswordLength" TabIndex="1" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanMaxPasswordLength','Max Password Length')"
                                    MaxLength="50" placeholder="Enter Max Password Length" CssClass="text-box" Style="width: 100px;"></asp:TextBox>--%>
                                <span id="spanMinPasswordLength" class="span_color"></span>
                                <%-- <asp:RangeValidator ID="rngpwdstrength" runat="server" ErrorMessage="Enter Min Password Length Between 6 to 20"
                                    class="span_color" Style="display: inline;" ControlToValidate="txtMinPasswordLength" Display="Dynamic"
                                    MinimumValue="6" MaximumValue="20" SetFocusOnError="true" Type="Integer"></asp:RangeValidator>--%>
                                <%--<span id="spanMaxPasswordLength" class="span_color"></span>--%>
                            </div>
                            <%--<div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litPassword" runat="server" Text="Password"></asp:Literal>
                                    <span class="span_star">*</span></label><br />
                                <asp:TextBox ID="txtPassword" TabIndex="1" runat="server" MaxLength="50" placeholder="Enter Password"
                                    CssClass="text-box"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revPassword" runat="server" ControlToValidate="txtPassword"
                                    ValidationExpression="" ForeColor="red" ErrorMessage="Input password is not is correct format">
                                </asp:RegularExpressionValidator>
                                <br />
                                <span id="span1" class="span_color"></span>
                            </div>--%>
                        </div>
                        <div class="box_total">
                            <div class="box_total_a">
                                <br />
                                <asp:Button ID="btnSave" TabIndex="4" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                    CssClass="box_s" runat="server" OnClick="btnSave_OnClick" />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/PasswordStrength.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        $(document).ready(function () { assignDiv('rptrMainMasterMenu_162_4', '175') });
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
