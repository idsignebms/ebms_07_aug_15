﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.AccessControl;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using DocumentFormat.OpenXml.Drawing.Charts;
using iDSignHelper;
using Resources;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;

namespace UMS_Nigeria.Admin
{
    public partial class PasswordStrength : System.Web.UI.Page
    {

        #region Members
        iDsignBAL objIdsignBal = new iDsignBAL();
        private PasswordStrengthBAL objPasswordStrengthBAL = new PasswordStrengthBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        XmlDocument xml = null;

        public string Key = "AddRoles";
        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                setPasswordStrength();
            }
        }

        private void setPasswordStrength()
        {
            PasswordStrengthBE objPasswordStrengthBE = new PasswordStrengthBE();
            PasswordStrengthListBE objPasswordStrengthListBe = new PasswordStrengthListBE();

            XmlDocument xmlResult = new XmlDocument();

            xmlResult = objPasswordStrengthBAL.GetPasswordStrenth();
            objPasswordStrengthListBe = objIdsignBal.DeserializeFromXml<PasswordStrengthListBE>(xmlResult);
            int MinCaps = 0;
            int MinSmall = 0;
            int MinSpecial = 0;
            int MinNumeric = 0;
            int MinPassLength = 0;
            if (objPasswordStrengthListBe.Items.Count > 0)
            {
                for (int i = 0; i < objPasswordStrengthListBe.Items.Count; i++)
                {
                    if (objPasswordStrengthListBe.Items[i].StrengthTypeId == 1)
                    {
                        if (!(string.IsNullOrEmpty(objPasswordStrengthListBe.Items[i].MinLength)))
                        {
                            MinCaps = Convert.ToInt32(objPasswordStrengthListBe.Items[i].MinLength);
                            txtMinNoOfCapitalLetters.Text = objPasswordStrengthListBe.Items[i].MinLength;
                        }
                        else
                        {
                            txtMinNoOfCapitalLetters.Text = "1";
                        }
                    }
                    else if (objPasswordStrengthListBe.Items[i].StrengthTypeId == 2)
                    {
                        if (!(string.IsNullOrEmpty(objPasswordStrengthListBe.Items[i].MinLength)))
                        {
                            MinSmall = Convert.ToInt32(objPasswordStrengthListBe.Items[i].MinLength);
                            txtMinNoOfSmallLetters.Text = objPasswordStrengthListBe.Items[i].MinLength;
                        }
                        else
                        {
                            txtMinNoOfSmallLetters.Text = "1";
                        }
                    }
                    else if (objPasswordStrengthListBe.Items[i].StrengthTypeId == 4)
                    {
                        if (!(string.IsNullOrEmpty(objPasswordStrengthListBe.Items[i].MinLength)))
                        {
                            MinNumeric = Convert.ToInt32(objPasswordStrengthListBe.Items[i].MinLength);
                            txtMinNoOfNumericValue.Text = objPasswordStrengthListBe.Items[i].MinLength;
                        }
                        else
                        {
                            txtMinNoOfNumericValue.Text = "1";
                        }
                    }
                    else if (objPasswordStrengthListBe.Items[i].StrengthTypeId == 3)
                    {
                        if (!(string.IsNullOrEmpty(objPasswordStrengthListBe.Items[i].MinLength)))
                        {
                            MinSpecial = Convert.ToInt32(objPasswordStrengthListBe.Items[i].MinLength);
                            txtMinSpecialCharecters.Text = objPasswordStrengthListBe.Items[i].MinLength;
                        }
                        else
                        {
                            txtMinSpecialCharecters.Text = "1";
                        }
                    }
                    else if (objPasswordStrengthListBe.Items[i].StrengthTypeId == 5)
                    {
                        if (!(string.IsNullOrEmpty(objPasswordStrengthListBe.Items[i].MinLength)))
                        {
                            MinPassLength = Convert.ToInt32(objPasswordStrengthListBe.Items[i].MinLength);
                            txtMinPasswordLength.Text = objPasswordStrengthListBe.Items[i].MinLength;
                        }
                        else
                        {
                            txtMinPasswordLength.Text = "6";
                        }

                    }
                }
            }
            //^(?=.*?[a-z].*?[a-z])
            //(?=.*?[A-Z].*?[A-Z].*?[A-Z])
            //(?=.*?[0-9])
            //(?=.*[!@#$%^&'])
            //[^ ]{8,}$

            string regExp = "^";
            string CapsExp = string.Empty;
            string SmallExp = string.Empty;
            string NumericExp = string.Empty;
            string SpecialExp = string.Empty;
            string PassLengthExp = string.Empty;


            string validationMsg = string.Empty;
            //CAPS
            if (MinCaps > 0)
            {
                CapsExp = "(?=";
                for (int i = 0; i < MinCaps; i++)
                    CapsExp += ".*?[A-Z]";
                CapsExp += ")";
                validationMsg += "- Min " + MinCaps + " Capital Letters";
            }

            //SMALL
            if (MinSmall > 0)
            {
                SmallExp = "(?=";
                for (int i = 0; i < MinSmall; i++)
                    SmallExp += ".*?[a-z]";
                SmallExp += ")";
                validationMsg += "- Min " + MinSmall + " Small Letters";
            }

            //Numeric
            if (MinNumeric > 0)
            {
                NumericExp = "(?=";
                for (int i = 0; i < MinNumeric; i++)
                    NumericExp += ".*?[0-9]";
                NumericExp += ")";
                validationMsg += "- Min " + MinNumeric + " Digits";
            }

            //Special
            if (MinSpecial > 0)
            {
                SpecialExp = "(?=";
                for (int i = 0; i < MinSpecial; i++)
                    SpecialExp += ".*[!@#$%^&']";
                SpecialExp += ")";
                validationMsg += "- Min " + MinSpecial + " Special characters (!@#$%^&')";
            }

            //PassLength
            if (MinPassLength > 0)
            {
                PassLengthExp = "[^ ]{" + MinPassLength + ",}$";
                validationMsg += "- Min " + MinPassLength + " characters";
            }

            regExp = regExp + CapsExp + SmallExp + NumericExp + SpecialExp + PassLengthExp;

            //Response.Write(regExp);
            //revPassword.ValidationExpression = regExp;
            //revPassword.ErrorMessage = validationMsg;

        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            int minCaps, minSmall, minNumeric, minSpecial, minPassLength;
            minCaps = Convert.ToInt32(string.IsNullOrEmpty(txtMinNoOfCapitalLetters.Text) ? "1" : txtMinNoOfCapitalLetters.Text);
            minSmall = Convert.ToInt32(string.IsNullOrEmpty(txtMinNoOfSmallLetters.Text) ? "1" : txtMinNoOfSmallLetters.Text);
            minNumeric = Convert.ToInt32(string.IsNullOrEmpty(txtMinNoOfNumericValue.Text) ? "1" : txtMinNoOfNumericValue.Text);
            minSpecial = Convert.ToInt32(string.IsNullOrEmpty(txtMinSpecialCharecters.Text) ? "1" : txtMinSpecialCharecters.Text);
            minPassLength = Convert.ToInt32(string.IsNullOrEmpty(txtMinPasswordLength.Text) ? "0" : txtMinPasswordLength.Text);
            if (minCaps <= 0 || minSmall <= 0 || minNumeric <= 0 || minSpecial <= 0 || minPassLength < 6)
            {
                Message("Please enter minimum six for password strength and One for Remaining.", Resources.UMS_Resource.MESSAGETYPE_ERROR);
            }
            else if ((minCaps + minSmall + minNumeric + minSpecial + minPassLength) < 6)
            {
                Message("Please enter atleast six password strength criteria.", Resources.UMS_Resource.MESSAGETYPE_ERROR);
            }
            else if (minPassLength < (minCaps + minSmall + minNumeric + minSpecial) || minPassLength < 6)
            {
                Message("Password Length should not be less then sum of other minimum lengths.", Resources.UMS_Resource.MESSAGETYPE_ERROR);
            }
            else
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt.Columns.Add("StrengthTypeId", typeof(int));
                dt.Columns.Add("MinLength", typeof(int));

                dt.Rows.Add(1, minCaps);
                dt.Rows.Add(2, minSmall);
                dt.Rows.Add(3, minSpecial);
                dt.Rows.Add(4, minNumeric);
                dt.Rows.Add(5, minPassLength);

                DataSet ds = new DataSet("PasswordStrength");
                ds.Tables.Add(dt);
                PasswordStrengthBE objPasswordStrengthBe = new PasswordStrengthBE();
                objPasswordStrengthBe =
                    objIdsignBal.DeserializeFromXml<PasswordStrengthBE>(objPasswordStrengthBAL.UpdatePasswordStrength(ds));
                if (objPasswordStrengthBe.RowCount > 0)
                {
                    Message("Password Strength updated successfully.", Resources.UMS_Resource.MESSAGETYPE_SUCCESS);
                    setPasswordStrength();
                }
                else
                {
                    Message("Password Strength updation failed.", Resources.UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {

                throw Ex;
            }
        }
    }
}