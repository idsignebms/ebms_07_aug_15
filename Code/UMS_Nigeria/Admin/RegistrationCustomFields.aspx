﻿<%@ Page Title=":: User Defined Fields ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="RegistrationCustomFields.aspx.cs"
    Inherits="UMS_Nigeria.Admin.RegistrationCustomFields" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdatePanel ID="upAddRegion" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddState" runat="server" Text="User Defined Fields"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litControlType" runat="server" Text="ControlType"></asp:Literal>
                                    <span class="span_star">*</span></label><br />
                                <asp:DropDownList ID="ddlControlType" TabIndex="1" onchange="DropDownlistOnChangelbl(this,'spanddlControlType',' Control Type')"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlControlType_OnSelectedIndexChanged"
                                    runat="server" CssClass="text-box text-select">
                                </asp:DropDownList>
                                <br />
                                <span id="spanddlControlType" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="LitFieldName" runat="server" Text="FieldName"></asp:Literal>
                                    <span class="span_star">*</span></label><div class="space">
                                    </div>
                                <asp:TextBox ID="txtFieldName" TabIndex="2" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanFieldName','Field Name')"
                                    MaxLength="50" placeholder="Enter Field Name" CssClass="text-box"></asp:TextBox>
                                <span id="spanFieldName" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="LitIsMandatory" runat="server" Text="Is Mandatory"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:CheckBox ID="chkIsMandatory" runat="server" />
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div id="DivDdlItems" runat="server" Visible="False" class="inner-box1 out-bor" style="padding-left:10px;">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Item1" runat="server" Text="Item1"></asp:Literal>
                                </label>
                                <br />
                                <asp:TextBox ID="txtItem1" CssClass="text-box" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="box_total_b" style="float: inherit;">
                            <div class="search_div">
                                <asp:Button ID="btnAddItem" TabIndex="5" OnClientClick="return AddItems();" Text="Add Item"
                                    CssClass="addButton" Visible="False" runat="server" />
                            </div>
                        </div>
                        <div class="box_total">
                            <div class="box_total_a">
                                <br />
                                <asp:Button ID="btnSave" TabIndex="4" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                    CssClass="box_s" runat="server" OnClick="btnSave_OnClick" />
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                        <br />
                        <br />
                    </div>
                    <div class="grid_boxes">
                        <div class="grid_pagingTwo_top">
                            <div class="paging_top_title">
                                <asp:Literal ID="litRegion" runat="server" Text="User Defined Fields List"></asp:Literal>
                            </div>
                            <div class="paging_top_rightTwo_content" id="divpaging" runat="server">
                                <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvRegistrationCustomFieldList" runat="server" AutoGenerateColumns="False"
                            HeaderStyle-CssClass="grid_tb_hd" OnRowDataBound="gvRegistrationCustomFieldList_OnRowDataBound"
                            OnRowCommand="gvRegistrationCustomFieldList_OnRowCommand">
                            <EmptyDataRowStyle HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                    HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        <asp:Label ID="lblIsActive" Visible="false" runat="server" Text='<%#Eval("IsActive")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Field Name" ItemStyle-Width="8%" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFieldName" runat="server" Text='<%#Eval("FieldName") %>'></asp:Label>
                                        <asp:TextBox ID="txtGridFieldName" CssClass="text-box" MaxLength="50" placeholder="Enter Field Name"
                                            Visible="false" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Control Type" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblControlTypeId" Visible="False" runat="server" Text='<%#Eval("ControlTypeId") %>'></asp:Label>
                                        <asp:Label ID="lblControlTypeName" runat="server" Text='<%#Eval("ControlTypeName") %>'></asp:Label>
                                        <asp:Label ID="lblIsHavingRef" runat="server" Visible="False" Text='<%#Eval("IsHavingRef") %>'></asp:Label>
                                        <asp:DropDownList ID="ddlGridControlTypeName" CssClass="text-box" Visible="false"
                                            runat="server">
                                        </asp:DropDownList>
                                        <asp:LinkButton ID="lkbtnAddItems" Visible="false" runat="server" ToolTip="Add Items"
                                            CommandName="AddItems"><img src="../images/drag.png" alt="Cancel" /></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="Values/Text" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblVText" runat="server" Text='<%#Eval("VText") %>'></asp:Label>
                                        <asp:TextBox ID="txtVText" CssClass="text-box" MaxLength="50" placeholder="Enter Value/Text"
                                            Visible="false" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="Is Mandatory" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIsMandatory" runat="server" Visible="False" Text='<%#Eval("IsMandatory") %>'></asp:Label>
                                        <asp:CheckBox ID="chkGridIsMandatory" runat="server" Enabled="False" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUDCId" runat="server" Visible="False" Text='<%#Eval("UDCId") %>'></asp:Label>
                                        <asp:LinkButton ID="lkbtnEdit" runat="server" ToolTip="Edit" Text="Edit" CommandName="EditCustomField"><img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnUpdate" Visible="false" runat="server" ToolTip="Update"
                                            CommandName="UpdateCustomField"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnCancel" Visible="false" runat="server" ToolTip="Cancel"
                                            CommandName="CancelCustomField"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnActive" Visible="false" runat="server" ToolTip="Activate Region"
                                            CommandName="ActiveCustomField"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnDeActive" runat="server" ToolTip="DeActivate" CommandName="DeActiveCustomField">
                                    <img src="../images/Deactivate.gif"   alt="DeActive" /></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:HiddenField ID="hfUDCId" runat="server" />
                        <asp:HiddenField ID="hfControlTypeName" runat="server" />
                        <asp:HiddenField ID="hfPageNo" runat="server" />
                        <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                        <asp:HiddenField ID="hfLastPage" runat="server" />
                        <asp:HiddenField ID="hfRecognize" runat="server" />
                        <asp:HiddenField ID="hfPageSize" runat="server" />
                        <asp:HiddenField ID="hfItemIdList" runat="server" />
                        <asp:HiddenField ID="hfIsHavingRef" runat="server" Value="false" />
                        <asp:HiddenField ID="hfIsValue" runat="server" />
                    </div>
                    <div class="grid_boxes">
                        <div id="divdownpaging" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div id="hidepopup">
                        <%-- SAVE & NEXT Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeRegionpopup" runat="server" TargetControlID="btnpopup"
                            PopupControlID="pnlConfirmation" BackgroundCssClass="popbg">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnpopup" runat="server" Text="" Style="display: none;" />
                        <asp:Panel runat="server" ID="pnlConfirmation" DefaultButton="btnOk" CssClass="editpopup_two"
                            Style="display: none;">
                            <div class="panelMessage">
                                <div>
                                    <asp:Panel ID="pnlMsgPopup" runat="server" align="center">
                                        <asp:Label ID="lblMsgPopup" runat="server" Text=""></asp:Label>
                                    </asp:Panel>
                                </div>
                                <div class="clear pad_10">
                                </div>
                                <div class="buttondiv">
                                    <asp:Button ID="btnOk" runat="server" Text="OK" CssClass="btn_ok" Style="margin-left: 105px;" />
                                    <div class="space">
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- SAVE & NEXT Btn popup Closed--%>
                        <%-- Edit Items popup Start--%>
                        <asp:ModalPopupExtender ID="mpeEditItems" runat="server" PopupControlID="pnlEditItems"
                            TargetControlID="btnEditItems" BackgroundCssClass="modalBackground" CancelControlID="btnEditItemsCancel">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnEditItems" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="pnlEditItems" runat="server" CssClass="modalPopup" Style="display: none;">
                            <div>
                                <h4 class="editfieldHeading">
                                    Edit Field
                                    <asp:Literal ID="litEditItemFieldName" runat="server" Text=""></asp:Literal></h4>
                                <asp:Label ID="lblEditItemsUDCId" Visible="False" runat="server"></asp:Label>
                                <div class="inner-box">
                                    <div class="inner-box1">
                                        <div class="text-inner" style="margin-top:8px;">
                                            <label for="name">
                                                <asp:Literal ID="litEditItemName" runat="server" Text="Item Name"></asp:Literal>
                                                <span class="span_star">*</span></label><div class="space">
                                                </div>
                                            <asp:TextBox ID="txtEditItemName" TabIndex="2" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanEditItemName','Item Name')"
                                                MaxLength="50" placeholder="Enter Item Name" CssClass="text-box text-boxWidth"></asp:TextBox>
                                            <span id="spanEditItemName" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="box_total">
                                        <div class="box_total_a" style="margin-left:0px;margin-bottom:10px;">
                                            <asp:Button ID="btnEditItemAdd" TabIndex="4" OnClientClick="return ValidateEditItems();"
                                                Text="<%$ Resources:Resource, ADD%>" CssClass="box_s" runat="server" OnClick="btnEditItemAdd_OnClick" />
                                        </div>
                                    </div>
                                    <div class="clear">
                                    </div>
                                    <div class="grid_tb" style="max-height: 300px; overflow: auto;">
                                        <asp:GridView ID="grdEditItems" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                            HeaderStyle-CssClass="grid_tb_hd" HeaderStyle-BackColor="#558D00" OnRowDataBound="grdEditItems_OnRowDataBound"
                                            OnRowCommand="grdEditItems_OnRowCommand">
                                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                            <EmptyDataTemplate>
                                                There is no data.
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, GRID_SNO%>"
                                                    runat="server">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Item" runat="server">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGridEditItemName" runat="server" Text='<%# Eval("VText") %>'></asp:Label>
                                                        <asp:TextBox ID="txtGridEditItemName" CssClass="text-box" MaxLength="50" placeholder="Enter Item Name"
                                                            Visible="false" runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="20%"
                                                    ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMRefId" runat="server" Visible="False" Text='<%#Eval("MRefId") %>'></asp:Label>
                                                        <asp:Label ID="lblIsActive" runat="server" Visible="False" Text='<%#Eval("IsActive") %>'></asp:Label>
                                                        <asp:LinkButton ID="lkbtnEdit" runat="server" ToolTip="Edit" Text="Edit" CommandName="EditItems"><img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                                                        <asp:LinkButton ID="lkbtnUpdate" Visible="false" runat="server" ToolTip="Update"
                                                            CommandName="UpdateItems"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                                        <asp:LinkButton ID="lkbtnCancel" Visible="false" runat="server" ToolTip="Cancel"
                                                            CommandName="CancelItems"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                                        <asp:LinkButton ID="lkbtnActive" Visible="false" runat="server" ToolTip="Activate Region"
                                                            CommandName="ActiveItems"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                                        <asp:LinkButton ID="lkbtnDeActive" runat="server" ToolTip="Delete" CommandName="DeactiveItems"><img src="../images/Deactivate.gif" alt="DeActive" /></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <asp:HiddenField ID="hfMRefId" runat="server" Value="" />
                                    </div>
                                </div>
                            </div>
                            <div class="space">
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn" style="margin-top:0px;padding-right:0px;padding-top: 5px;">
                                    <asp:Button ID="BtnEditItemsSave" OnClick="BtnEditSave_OnClick" runat="server" Text="<%$ Resources:Resource, SAVE%>"
                                        CssClass="ok_btn" />
                                    <asp:Button ID="btnEditItemsCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Edit Item popup Closed--%>
                        <%-- Active Country Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                            TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="PanelActivate" runat="server" CssClass="modalPopup"
                            Style="display: none;">
                            <div class="popheader popheaderlblgreen">
                                <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                            </div>
                            <div class="space">
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btnActiveOk" OnClick="btnActiveOk_OnClick" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                    <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Active Country Btn popup Closed--%>
                        <%-- DeActive Country Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                            TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnDeActivate" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="PanelDeActivate" runat="server" CssClass="modalPopup"
                            Style="display: none; border-color: #639B00;">
                            <div class="popheader" style="background-color: red;">
                                <asp:Label ID="lblDeActive" runat="server"></asp:Label>
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <div class="space">
                                    </div>
                                    <asp:Button ID="btnDeActiveOk" OnClick="btnDeActiveOk_OnClick" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                    <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- DeActive Country popup Closed--%>
                        <%-- Shift GovtCustomer Confirmation popup Start--%>
                        <asp:ModalPopupExtender ID="mpeConfirmationEditItems" runat="server" PopupControlID="PanelConfirmationEditItems"
                            TargetControlID="btnConfirmationEditItems" BackgroundCssClass="modalBackground"
                            CancelControlID="btnConfirmationEditItemsNo">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnConfirmationEditItems" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="PanelConfirmationEditItems" runat="server" CssClass="modalPopup" Style="display: none;">
                            <div class="popheader popheaderlblred">
                                <asp:Label ID="lblConfirmationEditItemsMsg" runat="server"></asp:Label>
                            </div>
                            <div class="space">
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btnConfirmationEditItemsYes" runat="server" OnClick="btnConfirmationEditItemsYes_OnClick"
                                        Text="<%$ Resources:Resource, YES%>" CssClass="ok_btn" />
                                    <asp:Button ID="btnConfirmationEditItemsNo" runat="server" Text="<%$ Resources:Resource, NO%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Shift GovtCustomer Confirmation popup Closed--%>
                        <%---------------------------------------------------------------------------------- %>
                        
                        <%-- Active Edit Item Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeEditItemsActive" runat="server" PopupControlID="pnlEditItemsActive"
                            TargetControlID="btnEditItemActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveEditItemCancel">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnEditItemActivate" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="pnlEditItemsActive" runat="server" CssClass="modalPopup" Style="display: none;">
                            <div class="popheader popheaderlblgreen">
                                <asp:Label ID="lblActiveEditItem" runat="server"></asp:Label>
                            </div>
                            <div class="space">
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btnActiveEditiItemOk" OnClick="btnActiveEditiItemOk_OnClick" runat="server"
                                        Text="<%$ Resources:Resource, OK%>" CssClass="ok_btn" />
                                    <asp:Button ID="btnActiveEditItemCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Active Edit Item Btn popup Closed--%>
                        <%-- DeActive Edit Item Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeEditItemsDeactive" runat="server" PopupControlID="pnlEditItemsDeactive"
                            TargetControlID="btnEditItemsDeActivate" BackgroundCssClass="modalBackground"
                            CancelControlID="btnDeActiveEditItemsCancel">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnEditItemsDeActivate" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="pnlEditItemsDeactive" runat="server"
                            CssClass="modalPopup" Style="display: none; border-color: #639B00;">
                            <div class="popheader" style="background-color: red;">
                                <asp:Label ID="lblDeActiveEditItems" runat="server"></asp:Label>
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <div class="space">
                                    </div>
                                    <asp:Button ID="btnEditItemsDeActiveOk" OnClick="btnEditItemsDeActiveOk_OnClick"
                                        runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="ok_btn" />
                                    <asp:Button ID="btnDeActiveEditItemsCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- DeActive Edit Item popup Closed--%>
                        <%---------------------------------------------------------------------------------- %>
                        

                        <%-- Grid Alert popup Start--%>
                        <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                            TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;" DefaultButton="btngridalertok">
                            <div class="popheader popheaderlblred">
                                <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                            </div>
                            <div class="footer popfooterbtnrt">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Grid Alert popup Closed--%>
                        
                        <%--Popus Message Starts--%>
                    <asp:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="hfMessage"
                        PopupControlID="pnlMessageValidation" BackgroundCssClass="popbg" CancelControlID="btnMessageOk">
                    </asp:ModalPopupExtender>
                    <asp:HiddenField ID="hfMessage" runat="server" />
                    <asp:Panel ID="pnlMessageValidation" runat="server" DefaultButton="btnMessageOk" CssClass="modalPopup"
                        Style="display: none; border-color: #639B00;">
                        <div class="popheader" style="background-color: red;">
                            <asp:Label ID="lblMessageValidation" runat="server"></asp:Label>
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnMessageOk" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%--Popus Pole Ends--%>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/PageValidations/RegistrationCustomFields.js" type="text/javascript"></script>

     <script language="javascript" type="text/javascript">
         $(document).ready(function () { assignDiv('rptrMainMasterMenu_172_9', '173') });
    </script>
    <%--<script src="../JavaScript/PageValidations/AddCountries.js" type="text/javascript"></script>--%>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
