﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using iDSignHelper;
using Resources;
using UMS_Nigeria.UserControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using System.Data;
using UMS_NigriaDAL;

namespace UMS_Nigeria.Admin
{
    public partial class RegistrationCustomFields : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        // MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        string Key = "RegistrationCustomFields";
        UserDefinedControlsBAL objUserDefinedControlsBal = new UserDefinedControlsBAL();
        UserDefinedControlsBE objUserDefinedControlsBEupdate = new UserDefinedControlsBE();
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                _ObjCommonMethods.BindControlTypes(ddlControlType, true);
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
            }

        }

        private void BindGrid()
        {

            UserDefinedControlsBE objUserDefinedControlsBE = new UserDefinedControlsBE();
            UserDefinedControlsListBE_1 objUserDefinedControlsListBE_1 = new UserDefinedControlsListBE_1();
            XmlDocument resultedXml = new XmlDocument();

            objUserDefinedControlsBE.PageNo = Convert.ToInt32(hfPageNo.Value);
            objUserDefinedControlsBE.PageSize = PageSize;
            resultedXml = objUserDefinedControlsBal.GetUserDefinedControlsDetails(objUserDefinedControlsBE);
            objUserDefinedControlsListBE_1 = _ObjiDsignBAL.DeserializeFromXml<UserDefinedControlsListBE_1>(resultedXml);
            if (objUserDefinedControlsListBE_1.items.Count > 0)
            {
                divdownpaging.Visible = divpaging.Visible = true;
                hfTotalRecords.Value = objUserDefinedControlsListBE_1.items[0].TotalRecords.ToString();
                gvRegistrationCustomFieldList.DataSource = objUserDefinedControlsListBE_1.items;
                gvRegistrationCustomFieldList.DataBind();
            }
            else
            {
                hfTotalRecords.Value = objUserDefinedControlsListBE_1.items.Count.ToString();
                divdownpaging.Visible = divpaging.Visible = false;
                gvRegistrationCustomFieldList.DataSource = new DataTable();
                gvRegistrationCustomFieldList.DataBind();
            }
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            try
            {
                UserDefinedControlsBE objUserDefinedControlsBe = new UserDefinedControlsBE();
                objUserDefinedControlsBe.ControlTypeId = Convert.ToInt32(ddlControlType.SelectedValue);
                objUserDefinedControlsBe.FieldName = txtFieldName.Text.Trim();
                objUserDefinedControlsBe.IsMandatory = chkIsMandatory.Checked;
                objUserDefinedControlsBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objUserDefinedControlsBe.IsHavingRef = Convert.ToBoolean(hfIsHavingRef.Value);
                objUserDefinedControlsBe.ItemsString = Convert.ToString(hfItemIdList.Value);
                xml = objUserDefinedControlsBal.UserDefinedControlsDetails(objUserDefinedControlsBe, Statement.Insert);
                objUserDefinedControlsBe = _ObjiDsignBAL.DeserializeFromXml<UserDefinedControlsBE>(xml);
                if (objUserDefinedControlsBe.RowsEffected > 0)
                {
                    Message("User defined control added successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                    BindGrid();
                    ddlControlType.SelectedIndex = 0;
                    txtFieldName.Text = txtItem1.Text = string.Empty;
                    DivDdlItems.Visible = btnAddItem.Visible = false;
                    chkIsMandatory.Checked = false;
                }
                else if (objUserDefinedControlsBe.IsFieldNameExists)
                {
                    Message("Field name already exits.", UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                {
                    Message("Failed to save User defined control.", UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlControlType_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlControlType.SelectedIndex > 0)
            {
                UserDefinedControlsBE objUserDefinedControlsBe = new UserDefinedControlsBE();
                UserDefinedControlsListBE_1 objUserDefinedControlsListBE_1 = new UserDefinedControlsListBE_1();
                objUserDefinedControlsBe.ControlTypeId = Convert.ToInt32(ddlControlType.SelectedValue);
                xml = objUserDefinedControlsBal.GetUserDefinedControlsList();
                objUserDefinedControlsListBE_1 = _ObjiDsignBAL.DeserializeFromXml<UserDefinedControlsListBE_1>(xml);
                objUserDefinedControlsBe =
                    objUserDefinedControlsListBE_1.items.Find(
                        x => x.ControlTypeId == Convert.ToInt32(ddlControlType.SelectedValue));
                if (objUserDefinedControlsBe != null)
                {
                    DivDdlItems.Visible = btnAddItem.Visible = objUserDefinedControlsBe.IsHavingRef;
                    hfIsHavingRef.Value = objUserDefinedControlsBe.IsHavingRef.ToString();
                }
            }
            else
            {
                DivDdlItems.Visible = btnAddItem.Visible = false;
                hfIsHavingRef.Value = "false";
            }
        }

        protected void gvRegistrationCustomFieldList_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    Label lblIsMandatory = (Label)e.Row.FindControl("lblIsMandatory");
                    TextBox txtGridFieldName = (TextBox)e.Row.FindControl("txtGridFieldName");
                    DropDownList ddlGridControlTypeName = (DropDownList)e.Row.FindControl("ddlGridControlTypeName");
                    CheckBox chkGridIsMandatory = (CheckBox)e.Row.FindControl("chkGridIsMandatory");
                    chkGridIsMandatory.Checked = Convert.ToBoolean(lblIsMandatory.Text);
                    lkbtnUpdate.Attributes.Add("onclick",
                        "return UpdateValidation('" + txtGridFieldName.ClientID + "','" +
                        ddlGridControlTypeName.ClientID + "')");

                    Label lblIsActive = (Label)e.Row.FindControl("lblIsActive");
                    if (!(Convert.ToBoolean(lblIsActive.Text)))
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvRegistrationCustomFieldList_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            UserDefinedControlsListBE_1 objUserDefinedControlsListBE_1 = new UserDefinedControlsListBE_1();
            UserDefinedControlsBE objUserDefinedControlsBE = new UserDefinedControlsBE();

            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

            TextBox txtGridFieldName = (TextBox)row.FindControl("txtGridFieldName");
            DropDownList ddlGridControlTypeName = (DropDownList)row.FindControl("ddlGridControlTypeName");
            CheckBox chkGridIsMandatory = (CheckBox)row.FindControl("chkGridIsMandatory");

            Label lblControlTypeId = (Label)row.FindControl("lblControlTypeId");
            Label lblIsActive = (Label)row.FindControl("lblIsActive");
            Label lblFieldName = (Label)row.FindControl("lblFieldName");
            Label lblControlTypeName = (Label)row.FindControl("lblControlTypeName");
            Label lblIsMandatory = (Label)row.FindControl("lblIsMandatory");
            Label lblUDCId = (Label)row.FindControl("lblUDCId");


            Label lblIsHavingRef = (Label)row.FindControl("lblIsHavingRef");
            LinkButton lkbtnAddItems = (LinkButton)row.FindControl("lkbtnAddItems");

            switch (e.CommandName.ToUpper())
            {
                case "EDITCUSTOMFIELD":
                    _ObjCommonMethods.BindControlTypes(ddlGridControlTypeName, true);

                    //UserDefinedControlsBE objUserDefinedControlsBE1= new UserDefinedControlsBE();
                    //objUserDefinedControlsBE1.UDCId = Convert.ToInt32(lblUDCId.Text);
                    //objUserDefinedControlsBE1 =
                    //    _ObjiDsignBAL.DeserializeFromXml<UserDefinedControlsBE>(
                    //        objUserDefinedControlsBal.IsCustomerAssociated(objUserDefinedControlsBE1));
                    //if (objUserDefinedControlsBE1.AssciatedCustomerCount > 0)
                    //{
                    //    lblConfirmationEditItemsMsg.Text =
                    //        "Customers are associated with this control you can not edit control. Would you like to edit Field Name ?";
                    //    mpeConfirmationEditItems.Show();
                    //}
                    //else
                    //{ 
                    lblFieldName.Visible = lblControlTypeName.Visible = false;
                    txtGridFieldName.Visible = ddlGridControlTypeName.Visible = chkGridIsMandatory.Enabled = true;

                    //if (hfIsValue.Value == "true")
                    //{
                    //    ddlGridControlTypeName.Visible = false;
                    //    lblControlTypeName.Visible = true;
                    //}



                    UserDefinedControlsBE objUserDefinedControlsBE1 = new UserDefinedControlsBE();
                    objUserDefinedControlsBE1.UDCId = Convert.ToInt32(lblUDCId.Text);
                    objUserDefinedControlsBE1 =
                        _ObjiDsignBAL.DeserializeFromXml<UserDefinedControlsBE>(
                            objUserDefinedControlsBal.IsCustomerAssociated(objUserDefinedControlsBE1));
                    if (objUserDefinedControlsBE1.AssciatedCustomerCount > 0)
                    {
                        lblControlTypeName.Visible = true;
                        ddlGridControlTypeName.Visible = false;
                    }



                    lkbtnAddItems.Visible = Convert.ToBoolean(lblIsHavingRef.Text);

                    txtGridFieldName.Text = lblFieldName.Text;
                    ddlGridControlTypeName.SelectedIndex =
                        ddlGridControlTypeName.Items.IndexOf(
                            ddlGridControlTypeName.Items.FindByValue(lblControlTypeId.Text));

                    row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                    row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;
                    //}
                    break;

                case "CANCELCUSTOMFIELD":
                    row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                    row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                    lblFieldName.Visible = lblControlTypeName.Visible = true;
                    chkIsMandatory.Checked = Convert.ToBoolean(lblIsMandatory.Text);
                    txtGridFieldName.Visible = ddlGridControlTypeName.Visible = chkGridIsMandatory.Enabled = false;
                    lkbtnAddItems.Visible = false;
                    break;

                case "UPDATECUSTOMFIELD":

                    objUserDefinedControlsBE.UDCId = Convert.ToInt32(lblUDCId.Text);
                    objUserDefinedControlsBE.FieldName = txtGridFieldName.Text;
                    objUserDefinedControlsBE.ControlTypeId = Convert.ToInt32(ddlGridControlTypeName.SelectedValue);
                    objUserDefinedControlsBE.IsMandatory = chkGridIsMandatory.Checked;
                    objUserDefinedControlsBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();

                    updateCustomControls(objUserDefinedControlsBE);
                    break;
                case "ACTIVECUSTOMFIELD":
                    hfUDCId.Value = lblUDCId.Text;
                    lblActiveMsg.Text = "Are you sure you want to activate this control?";
                    btnActiveOk.Focus();
                    mpeActivate.Show();
                    break;
                case "DEACTIVECUSTOMFIELD":
                    hfUDCId.Value = lblUDCId.Text;
                    lblDeActive.Text = "Are you sure you want to deactivate this control?";
                    btnDeActiveOk.Focus();
                    mpeDeActive.Show();
                    break;
                case "ADDITEMS":
                    litEditItemFieldName.Text = lblFieldName.Text;
                    lblEditItemsUDCId.Text = hfUDCId.Value = lblUDCId.Text;
                    BindItemsGrid(Convert.ToInt32(lblUDCId.Text));
                    break;
            }

        }

        private void BindItemsGrid(int UDCId)
        {
            UserDefinedControlsBE objUDCLBe = new UserDefinedControlsBE();
            objUDCLBe.UDCId = Convert.ToInt32(UDCId);
            xml = objUserDefinedControlsBal.GetUserDefinedControlItems(objUDCLBe);
            UserDefinedControlsListBE_1 objUserDefinedControlsListBE = new UserDefinedControlsListBE_1();
            objUserDefinedControlsListBE = _ObjiDsignBAL.DeserializeFromXml<UserDefinedControlsListBE_1>(xml);
            grdEditItems.DataSource = objUserDefinedControlsListBE.items;
            grdEditItems.DataBind();
            mpeEditItems.Show();
        }

        protected void btnActiveOk_OnClick(object sender, EventArgs e)
        {
            UserDefinedControlsBE _ObjUserDefinedControlsBE = new UserDefinedControlsBE();
            _ObjUserDefinedControlsBE.UDCId = Convert.ToInt32(hfUDCId.Value);
            _ObjUserDefinedControlsBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            _ObjUserDefinedControlsBE.IsActive = true;

            xml = objUserDefinedControlsBal.UserDefinedControlsDetails(_ObjUserDefinedControlsBE, Statement.Delete);
            _ObjUserDefinedControlsBE = _ObjiDsignBAL.DeserializeFromXml<UserDefinedControlsBE>(xml);

            if (_ObjUserDefinedControlsBE.RowsEffected > 0)
            {
                BindGrid();
                Message("Control activated successfully", UMS_Resource.MESSAGETYPE_SUCCESS);
            }
            else
                Message("Control activation failed", UMS_Resource.MESSAGETYPE_ERROR);
        }

        protected void btnDeActiveOk_OnClick(object sender, EventArgs e)
        {
            UserDefinedControlsBE _ObjUserDefinedControlsBE = new UserDefinedControlsBE();
            _ObjUserDefinedControlsBE.UDCId = Convert.ToInt32(hfUDCId.Value);
            _ObjUserDefinedControlsBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            _ObjUserDefinedControlsBE.IsActive = false;

            xml = objUserDefinedControlsBal.UserDefinedControlsDetails(_ObjUserDefinedControlsBE, Statement.Delete);
            _ObjUserDefinedControlsBE = _ObjiDsignBAL.DeserializeFromXml<UserDefinedControlsBE>(xml);

            if (_ObjUserDefinedControlsBE.RowsEffected > 0)
            {
                BindGrid();
                Message("Control deactivated successfully", UMS_Resource.MESSAGETYPE_SUCCESS);
            }
            //else if (_ObjUserDefinedControlsBE.AssciatedCustomerCount > 0)
            //{
            //    lblgridalertmsg.Text = "Customers are associated with this account you can not deactivate this record";
            //    mpegridalert.Show();
            //}
            else
                Message("Control deactivation failed", UMS_Resource.MESSAGETYPE_ERROR);
        }

        #endregion

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        protected void BtnEditSave_OnClick(object sender, EventArgs e)
        {

        }

        protected void btnEditItemAdd_OnClick(object sender, EventArgs e)
        {
            UserDefinedControlsBE objUserDefinedControlsBe = new UserDefinedControlsBE();
            XmlDocument XmlResult = new XmlDocument();
            objUserDefinedControlsBe.UDCId = Convert.ToInt32(lblEditItemsUDCId.Text);
            objUserDefinedControlsBe.VText = txtEditItemName.Text;
            objUserDefinedControlsBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            XmlResult = objUserDefinedControlsBal.UserDefinedControlsItems(objUserDefinedControlsBe, Statement.Insert);
            objUserDefinedControlsBe = _ObjiDsignBAL.DeserializeFromXml<UserDefinedControlsBE>(XmlResult);

            if (objUserDefinedControlsBe.RowsEffected > 0)
            {
                Message("Item added successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                BindItemsGrid(Convert.ToInt32(lblEditItemsUDCId.Text));
                txtEditItemName.Text = string.Empty;
            }
            else if (objUserDefinedControlsBe.IsControlRefExists)
            {
                Message("Item already exists.", UMS_Resource.MESSAGETYPE_ERROR);
            }
            else
            {
                Message("failed to add the record.", UMS_Resource.MESSAGETYPE_ERROR);
            }
            mpeEditItems.Show();
        }

        protected void grdEditItems_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                Label lblGridEditItemName = (Label)e.Row.FindControl("lblGridEditItemName");
                TextBox txtGridEditItemName = (TextBox)e.Row.FindControl("txtGridEditItemName");

                lkbtnUpdate.Attributes.Add("onclick",
                    "return UpdateValidation('" + txtGridEditItemName.ClientID + "')");

                Label lblIsActive = (Label)e.Row.FindControl("lblIsActive");
                if (!(Convert.ToBoolean(lblIsActive.Text)))
                {
                    e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                    e.Row.FindControl("lkbtnActive").Visible = true;
                }
                else
                {
                    e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                }
            }
        }

        protected void grdEditItems_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            UserDefinedControlsListBE_1 objUserDefinedControlsListBE_1 = new UserDefinedControlsListBE_1();
            UserDefinedControlsBE objUserDefinedControlsBE = new UserDefinedControlsBE();

            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

            TextBox txtGridEditItemName = (TextBox)row.FindControl("txtGridEditItemName");

            Label lblMRefId = (Label)row.FindControl("lblMRefId");
            Label lblGridEditItemName = (Label)row.FindControl("lblGridEditItemName");
            Label lblIsActive = (Label)row.FindControl("lblIsActive");


            switch (e.CommandName.ToUpper())
            {
                case "EDITITEMS":

                    lblGridEditItemName.Visible = false;
                    txtGridEditItemName.Visible = true;

                    txtGridEditItemName.Text = lblGridEditItemName.Text;

                    row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                    row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;
                    break;

                case "CANCELITEMS":
                    row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                    row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                    lblGridEditItemName.Visible = true;
                    txtGridEditItemName.Visible = false;
                    break;

                case "UPDATEITEMS":
                    objUserDefinedControlsBE.MRefId = Convert.ToInt32(lblMRefId.Text);
                    objUserDefinedControlsBE.VText = txtGridEditItemName.Text;
                    objUserDefinedControlsBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objUserDefinedControlsBE.UDCId = Convert.ToInt32(lblEditItemsUDCId.Text);
                    XmlDocument xmlResult = new XmlDocument();
                    xmlResult = objUserDefinedControlsBal.UserDefinedControlsItems(objUserDefinedControlsBE, Statement.Update);
                    objUserDefinedControlsBE = _ObjiDsignBAL.DeserializeFromXml<UserDefinedControlsBE>(xmlResult);

                    if (objUserDefinedControlsBE.RowsEffected > 0)
                    {
                        Message("Item Updated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                        BindItemsGrid(Convert.ToInt32(lblEditItemsUDCId.Text));
                    }
                    else if (objUserDefinedControlsBE.IsFieldNameExists)
                    {
                        Message("Item already exits.", UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else if (objUserDefinedControlsBE.AssciatedCustomerCount > 0)
                    {
                        Message("You can not edit this item, Customers are associated with this item.", UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else
                    {
                        Message("Failed to update Item.", UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    break;
                case "ACTIVEITEMS":
                    hfMRefId.Value = lblMRefId.Text;
                    lblActiveEditItem.Text = "Are you sure you want to activate this item?";
                    btnActiveOk.Focus();
                    mpeEditItemsActive.Show();
                    break;
                case "DEACTIVEITEMS":
                    hfMRefId.Value = lblMRefId.Text;
                    lblDeActiveEditItems.Text = "Are you sure you want to deactivate this item?";
                    btnEditItemsDeActiveOk.Focus();
                    mpeEditItemsDeactive.Show();
                    break;
            }

            mpeEditItems.Show();

        }

        protected void btnActiveEditiItemOk_OnClick(object sender, EventArgs e)
        {
            UserDefinedControlsBE objUserDefinedControlsBe = new UserDefinedControlsBE();
            objUserDefinedControlsBe.IsActive = true;
            objUserDefinedControlsBe.MRefId = Convert.ToInt32(hfMRefId.Value);
            objUserDefinedControlsBe.ModifiedBy = Convert.ToString(Session[UMS_Resource.SESSION_LOGINID]);
            XmlDocument xmlResult = new XmlDocument();
            xmlResult = objUserDefinedControlsBal.UserDefinedControlsItems(objUserDefinedControlsBe, Statement.Delete);
            objUserDefinedControlsBe = _ObjiDsignBAL.DeserializeFromXml<UserDefinedControlsBE>(xmlResult);
            if (objUserDefinedControlsBe.RowsEffected > 0)
            {
                BindItemsGrid(Convert.ToInt32(hfUDCId.Value));
                Message("Item activated successfully", UMS_Resource.MESSAGETYPE_SUCCESS);
            }
            else
                Message("Item activation failed", UMS_Resource.MESSAGETYPE_ERROR);

            mpeEditItems.Show();
        }

        protected void btnEditItemsDeActiveOk_OnClick(object sender, EventArgs e)
        {
            UserDefinedControlsBE objUserDefinedControlsBe = new UserDefinedControlsBE();
            objUserDefinedControlsBe.IsActive = false;
            objUserDefinedControlsBe.MRefId = Convert.ToInt32(hfMRefId.Value);
            objUserDefinedControlsBe.ModifiedBy = Convert.ToString(Session[UMS_Resource.SESSION_LOGINID]);
            XmlDocument xmlResult = new XmlDocument();
            xmlResult = objUserDefinedControlsBal.UserDefinedControlsItems(objUserDefinedControlsBe, Statement.Delete);
            objUserDefinedControlsBe = _ObjiDsignBAL.DeserializeFromXml<UserDefinedControlsBE>(xmlResult);
            if (objUserDefinedControlsBe.RowsEffected > 0)
            {
                BindItemsGrid(Convert.ToInt32(hfUDCId.Value));
                Message("Item deactivated successfully", UMS_Resource.MESSAGETYPE_SUCCESS);
            }
            else
                Message("Item deactivation failed", UMS_Resource.MESSAGETYPE_ERROR);

            mpeEditItems.Show();
        }

        protected void btnConfirmationEditItemsYes_OnClick(object sender, EventArgs e)
        {
            hfIsValue.Value = "true";
        }

        private void updateCustomControls(UserDefinedControlsBE objUserDefinedControlsBE)
        {
            xml = objUserDefinedControlsBal.UserDefinedControlsDetails(objUserDefinedControlsBE,
                            Statement.Update);
            objUserDefinedControlsBE = _ObjiDsignBAL.DeserializeFromXml<UserDefinedControlsBE>(xml);

            if (objUserDefinedControlsBE.RowsEffected > 0)
            {
                Message("User defined control Updated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                BindGrid();
            }
            else if (objUserDefinedControlsBE.IsFieldNameExists)
            {
                Message("Field name already exits.", UMS_Resource.MESSAGETYPE_ERROR);
            }
            else if (objUserDefinedControlsBE.AssciatedCustomerCount > 0)
            {
                Message("You can not edit the control, Customers are associated with this field.", UMS_Resource.MESSAGETYPE_ERROR);
            }
            else
            {
                Message("Failed to update User defined control.", UMS_Resource.MESSAGETYPE_ERROR);
            }
        }
    }
}