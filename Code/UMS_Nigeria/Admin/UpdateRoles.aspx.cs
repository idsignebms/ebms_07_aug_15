﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using iDSignHelper;
using Resources;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;

namespace UMS_Nigeria.Admin
{
    public partial class UpdateRoles : System.Web.UI.Page
    {
        #region Members
        iDsignBAL objIdsignBal = new iDsignBAL();
        AdminBAL objAdminBal = new AdminBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        XmlDocument xml = null;
        AdminListBE_1 _objAdminListBe_1 = new AdminListBE_1();
        AdminListBE _objAdminListBe = new AdminListBE();
        public string Key = "UpdateRoles";
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.Visible = false;
            if (!IsPostBack)
            {
                objCommonMethods.BindAccessLevelsList(ddlAccessLevels, true);
                AdminBE _objAdminBe = new AdminBE();
                _objAdminBe.RoleId = Session[UMS_Resource.SESSION_ROLEID] != null ? 0 : Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString());
                _objAdminBe.AccessLevelID = 0;
                objCommonMethods.BindRolesList(ddlRoleName, _objAdminBe, true);
            }
        }

        protected void rptrPagePermisions_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBoxList cblChildPages = (CheckBoxList)e.Item.FindControl("cblChildPages");
                Label lblMenuId = (Label)e.Item.FindControl("lblMenuId");
                CheckBox ChkAll = (CheckBox)e.Item.FindControl("ChkAll");
                Literal litMainMenuName = (Literal)e.Item.FindControl("litMainMenuName");

                System.Web.UI.HtmlControls.HtmlGenericControl chblDiv = (System.Web.UI.HtmlControls.HtmlGenericControl)(e.Item.FindControl("chblDiv"));

                ChkAll.Attributes.Add("onclick", "CheckAll('" + ChkAll.ClientID + "','" + cblChildPages.ClientID + "')");
                cblChildPages.Attributes.Add("onclick", "UnCheckAll('" + ChkAll.ClientID + "','" + cblChildPages.ClientID + "')");
                var drSubMenuDetails = from x in _objAdminListBe_1.Items.AsEnumerable()
                                       where x.ReferenceMenuId == Convert.ToInt32(lblMenuId.Text)
                                       select new
                                       {
                                           x.MenuId,
                                           x.Name,
                                           x.IsAssigned,
                                           x.IsActive
                                       };
                //foreach (var cblListItems in drSubMenuDetails)
                //{
                //    ListItem listItem = new ListItem();
                //    listItem.Value = cblListItems.MenuId.ToString();
                //    listItem.Text = cblListItems.Name;

                //}

                //Commenting it to show the Configuration Settings -- Karteek Start
                //if (lblMenuId.Text != "7")
                //{
                //    drSubMenuDetails = drSubMenuDetails.Where(x => x.IsActive);
                //}
                //else
                //{
                //    drSubMenuDetails = drSubMenuDetails.Where(x => x.MenuId != 122);
                //}
                if (lblMenuId.Text == "7")
                {
                    drSubMenuDetails = drSubMenuDetails.Where(x => x.MenuId != 122);
                }
                var SubMenuItemCount = drSubMenuDetails.Count();
                if (SubMenuItemCount > 0)
                {
                    chblDiv.Visible = true;
                    cblChildPages.DataSource = drSubMenuDetails;
                    cblChildPages.DataTextField = "Name";
                    cblChildPages.DataValueField = "MenuId";
                    cblChildPages.DataBind();
                }
                else
                {
                    chblDiv.Visible = false;
                }
            }
        }

        protected void ddlAccessLevels_OnSelectedIndexChanged(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(ddlAccessLevels.SelectedValue))
            {
                AdminBE _objAdminBE = new AdminBE();
                _objAdminBE.AccessLevelID = Convert.ToInt32(ddlAccessLevels.SelectedValue);
                BindPagePermisions(Convert.ToInt32(ddlAccessLevels.SelectedValue));
                //_objAdminBE.RoleId = Session[UMS_Resource.SESSION_ROLEID] != null ? 0 : Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString());
                //objCommonMethods.BindRolesList(ddlRoleName, _objAdminBE, true);
            }
        }

        protected void ddlRoleName_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlRoleName.SelectedIndex != 0)
            {
                GetRoleAccessLevel();
                ddlAccessLevels_OnSelectedIndexChanged(sender, e);
                if (ddlAccessLevels.SelectedIndex != 0)
                    BindPagePermisions(Convert.ToInt32(ddlAccessLevels.SelectedValue));
                AdminBE _objAdminBe = new AdminBE();
                if (!string.IsNullOrEmpty(ddlRoleName.SelectedValue))
                {
                    _objAdminBe.RoleId = Convert.ToInt32(ddlRoleName.SelectedValue);
                    xml = objAdminBal.GetRolePermissions(_objAdminBe);

                    AdminListBE_1 objAdminListBe = objIdsignBal.DeserializeFromXml<AdminListBE_1>(xml);

                    for (int i = 0; i < rptrPagePermisions.Items.Count; i++)
                    {
                        bool IsAllChecked = true;
                        CheckBoxList cblChildPages = (CheckBoxList)rptrPagePermisions.Items[i].FindControl("cblChildPages");
                        CheckBox ChkAll = (CheckBox)rptrPagePermisions.Items[i].FindControl("ChkAll");
                        for (int j = 0; j < objAdminListBe.Items.Count; j++)
                        {
                            if (cblChildPages.Items.FindByValue(objAdminListBe.Items[j].MenuId.ToString()) != null)
                            {
                                cblChildPages.Items.FindByValue(objAdminListBe.Items[j].MenuId.ToString()).Selected =
                                    Convert.ToBoolean(objAdminListBe.Items[j].IsAssigned);
                                if (!objAdminListBe.Items[j].IsAssigned)
                                    IsAllChecked = false;
                            }
                        }
                        ChkAll.Checked = IsAllChecked;
                    }
                }
                else
                {
                    ClearAll();
                }
            }
            else
            {
                rptrPagePermisions.DataSource = null;
                rptrPagePermisions.DataBind();
            }
        }

        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlRoleName.SelectedValue))
            {
                lblUpdateMsg.Text = "Are you sure, you want to update this Role ? ";
                mpeUpdate.Show();
                btnUpdateOk.Focus();
            }
            else
            {
                Message("Select the Role to update.", UMS_Resource.MESSAGETYPE_ERROR);
                ddlRoleName.Focus();
            }
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlRoleName.SelectedValue))
            {
                lblDeleteMsg.Text = "Are you sure, you want to delete this Role ? ";
                mpeDelete.Show();
                btnDeleteOk.Focus();
            }
            else
            {
                Message("Select the Role to delete.", UMS_Resource.MESSAGETYPE_ERROR);
                ddlRoleName.Focus();
            }
        }

        protected void btnReset_OnClick(object sender, EventArgs e)
        {
            ClearAll();
        }

        protected void btnUpdateOk_OnClick(object sender, EventArgs e)
        {
            try
            {
                AdminBE _ObjAdminBe = new AdminBE();
                _ObjAdminBe.RoleId = Convert.ToInt32(ddlRoleName.SelectedValue);
                _ObjAdminBe.AccessLevelID = Convert.ToInt32(ddlAccessLevels.SelectedValue);
                _ObjAdminBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _ObjAdminBe.Permissions = string.Empty;
                int itemsCount = rptrPagePermisions.Items.Count;
                for (int i = 0; i < itemsCount; i++)
                {

                    CheckBox ChkAll = (CheckBox)rptrPagePermisions.Items[i].FindControl("ChkAll");
                    Label lblMenuId = (Label)rptrPagePermisions.Items[i].FindControl("lblMenuId");
                    CheckBoxList cblChildPages = (CheckBoxList)rptrPagePermisions.Items[i].FindControl("cblChildPages");

                    //7 is the id for settings and 122 is the id of configuration settings
                    if (lblMenuId.Text == "7")
                    {
                        if (ChkAll.Checked || cblChildPages.Items.Cast<ListItem>().Any(li => li.Selected))
                            _ObjAdminBe.Permissions = _ObjAdminBe.Permissions + (_ObjAdminBe.Permissions == string.Empty ? "122," : ",122,") + lblMenuId.Text;
                        for (int j = 0; j < cblChildPages.Items.Count; j++)
                        {
                            if (cblChildPages.Items[j].Selected)
                            {
                                _ObjAdminBe.Permissions = _ObjAdminBe.Permissions + "," + cblChildPages.Items[j].Value;
                            }
                        }
                    }
                    else
                    {
                        if (ChkAll.Checked || cblChildPages.Items.Cast<ListItem>().Any(li => li.Selected))
                            _ObjAdminBe.Permissions = _ObjAdminBe.Permissions + (_ObjAdminBe.Permissions == string.Empty ? "" : ",") + lblMenuId.Text;
                        for (int j = 0; j < cblChildPages.Items.Count; j++)
                        {
                            if (cblChildPages.Items[j].Selected)
                            {
                                _ObjAdminBe.Permissions = _ObjAdminBe.Permissions + "," + cblChildPages.Items[j].Value;
                            }
                        }
                    }
                }
                xml = objAdminBal.Admin(_ObjAdminBe, Statement.Update);
                _ObjAdminBe = objIdsignBal.DeserializeFromXml<AdminBE>(xml);

                if (_ObjAdminBe.RowsEffected > 0)
                {
                    Message("Role Updated Successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                    ClearAll();
                }
                else
                {
                    Message("Role " + _ObjAdminBe.ExistingRoleName + " with the same permission already exists.",
                        UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeleteOk_OnClick(object sender, EventArgs e)
        {
            try
            {
                AdminBE _ObjAdminBe = new AdminBE();
                _ObjAdminBe.RoleId = Convert.ToInt32(ddlRoleName.SelectedValue);

                xml = objAdminBal.Admin(_ObjAdminBe, Statement.Delete);
                _ObjAdminBe = objIdsignBal.DeserializeFromXml<AdminBE>(xml);

                if (_ObjAdminBe.RowsEffected > 0)
                {
                    Message("Role Deleted Successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                    ClearAll();
                }
                else if (_ObjAdminBe.IsUsersAssociated)
                {
                    Message("Can not delete the role, Users are associated with this role.", UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                {
                    Message("Role Deletion Failure.", UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        public void GetRoleAccessLevel()
        {
            AdminBE objAdminBE = new AdminBE();
            objAdminBE.RoleId = Convert.ToInt32(ddlRoleName.SelectedValue);
            XmlDocument XmlResult = objAdminBal.GetAccessLevel(objAdminBE);
            objAdminBE = objIdsignBal.DeserializeFromXml<AdminBE>(XmlResult);
            if (objAdminBE.AccessLevelID == 0)
                ddlAccessLevels.SelectedIndex = 0;
            else
                ddlAccessLevels.SelectedValue = objAdminBE.AccessLevelID.ToString();
            ddlAccessLevels.Enabled = false;
        }

        public void BindPagePermisions(int AccessLevelsID)
        {
            AdminBE _objAdminBE = new AdminBE();
            _objAdminBE.RoleId = Session[UMS_Resource.SESSION_ROLEID] != null ? 0 : Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString());
            //_objAdminBE.AccessLevelID = Convert.ToInt32(ddlAccessLevels.SelectedValue);
            _objAdminBE.AccessLevelID = AccessLevelsID;
            XmlDocument xmlResult = objAdminBal.Admin(_objAdminBE,ReturnType.Get);

            _objAdminListBe = objIdsignBal.DeserializeFromXml<AdminListBE>(xmlResult);

            _objAdminListBe_1 = objIdsignBal.DeserializeFromXml<AdminListBE_1>(xmlResult);
            rptrPagePermisions.DataSource = _objAdminListBe.Items;
            rptrPagePermisions.DataBind();
            //string[] MenuItems = new string[4]; { };
            //foreach (var Master in _objAdminListBe.Items)
            //{
            //MenuItems[0] = Master.MenuId.ToString();
            //MenuItems[1] = Master.Name;
            //var drSubMenuDetails = from x in _objAdminListBe.Items.AsEnumerable()
            //                       where x.MenuId == Master.MenuId
            //                       select new
            //                       {
            //                           x.MenuId,
            //                           x.Name
            //                       };
            //foreach (var Child in drSubMenuDetails)
            //{
            //    MenuItems[2] = Child.MenuId.ToString();
            //    MenuItems[3] = Child.Name;

            //}
            //}
        }

        public void BindPagePermisionsByRole(int RoleId)
        {

            AdminBE _objAdminBe = new AdminBE();
            _objAdminBe.RoleId = RoleId;
            xml = objAdminBal.GetRolePermissions(_objAdminBe);

            AdminListBE objAdminListBe = objIdsignBal.DeserializeFromXml<AdminListBE>(xml);

            _objAdminListBe_1 = objIdsignBal.DeserializeFromXml<AdminListBE_1>(xml);
            rptrPagePermisions.DataSource = _objAdminListBe.Items;
            rptrPagePermisions.DataBind();

        }

        public void ClearAll()
        {
            for (int i = 0; i < rptrPagePermisions.Items.Count; i++)
            {
                CheckBox chkAll = (CheckBox)rptrPagePermisions.Items[i].FindControl("chkAll");
                chkAll.Checked = false;
                CheckBoxList cblChildPages = (CheckBoxList)rptrPagePermisions.Items[i].FindControl("cblChildPages");
                foreach (ListItem li in cblChildPages.Items)
                {
                    li.Selected = false;
                }
            }
            AdminBE _objAdminBe = new AdminBE();
            _objAdminBe.AccessLevelID = Convert.ToInt32(ddlAccessLevels.SelectedValue);
            _objAdminBe.RoleId = Session[UMS_Resource.SESSION_ROLEID]!=null ? 0 : Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString());
            objCommonMethods.BindRolesList(ddlRoleName, _objAdminBe, true);

        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion
    }
}