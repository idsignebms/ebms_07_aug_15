﻿<%@ Page Title="Bill Adjustment Batch Details" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="AdjustmentBatch.aspx.cs" Inherits="UMS_Nigeria.Billing.AdjustmentBatch" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControls/Authentication.ascx" TagName="Authentication" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upStates" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litReadMethod" runat="server" Text="<%$ Resources:Resource, BILL_ADJST_BTCH_DET%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, BATCHNUM%>"></asp:Literal>
                                <span class="span_star">*</span><br />
                                <asp:TextBox ID="txtBatchNo" onblur="return TextBoxBlurValidationlbl(this,'spanBatchNo','Batch No')"
                                    runat="server" CssClass="text-box" placeholder="Enter Batch No" TabIndex="1"
                                    Style="width: 158px;"></asp:TextBox><%--AutoPostBack="True" OnTextChanged="txtBatchNo_TextChanged"--%>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtBatchNo"
                                    FilterType="Numbers">
                                </asp:FilteredTextBoxExtender>
                                <br />
                                <span id="spanBatchNo" class="span_color"></span>
                            </div>
                            <div class="text-inner">
                                <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource,  BATCH_AMT_UNIT_TOTAL%>"></asp:Literal>
                                <span class="span_star">*</span><br />
                                <asp:TextBox ID="txtBatchTotal" placeholder="Enter Batch Total" runat="server" CssClass="text-box" TabIndex="4"
                                    Style="width: 158px;" MaxLength="18" onblur="return TextBoxBlurValidationlblWithNoZero(this,'spanBatchTotal','Batch Total')"
                                    onkeypress="return isNumberKey(event,this)" onkeyup="GetCurrencyFormate(this);"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtBatchTotal"
                                    FilterType="Numbers, Custom" ValidChars=",.">
                                </asp:FilteredTextBoxExtender>
                                <br />
                                <span id="spanBatchTotal" class="span_color"></span>
                            </div>
                            <div class="text-inner">
                                <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource,  BILL_ADJUSTMENT%>"></asp:Literal>
                                <span class="span_star">*</span><br />
                                <asp:RadioButtonList ID="rdblAdjsutmentType" runat="server" TabIndex="5" RepeatDirection="Vertical">
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, BATCHDATE%>"></asp:Literal>
                                <span class="span_star">*</span><br />
                                <asp:TextBox ID="txtBatchDate" AutoComplete="off" MaxLength="10" onchange="DoBlur(this);"
                                    onblur="DoBlur(this);" Width="133px" placeholder="Enter Batch Date" runat="server"  TabIndex="2"
                                    CssClass="text-box"></asp:TextBox>
                                <asp:Image ID="imgDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                    vertical-align: middle" AlternateText="Calender" runat="server" />
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtBatchDate"
                                    FilterType="Custom,Numbers" ValidChars="/">
                                </asp:FilteredTextBoxExtender>
                                <br />
                                <span id="spanBatchDate" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, REASON%>"></asp:Literal>
                                <span class="span_star">*</span><br />
                                <asp:DropDownList ID="ddlReasonList" runat="server" TabIndex="3" CssClass="text-box select_box">
                                </asp:DropDownList>
                                <br />
                                <span id="spanReason" class="span_color"></span>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <%-- <div class="consume_name">
                        <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource,  BILL_ADJUSTMENT%>"></asp:Literal>
                        <span>*</span>
                    </div>
                    <div class="c_left fltl">
                        <asp:RadioButtonList ID="rdblAdjsutmentType" runat="server" RepeatDirection="Vertical">
                        </asp:RadioButtonList>
                    </div>--%>
                    <div class="clear pad_5">
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <asp:Button ID="btnNext" CssClass="box_s" Text="<%$ Resources:Resource, NEXT%>" runat="server"
                                TabIndex="6" OnClick="btnNext_Click" OnClientClick="return Validate();" />
                        </div>
                    </div>
                </div>
                <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                    TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                </asp:ModalPopupExtender>
                <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                    <div class="popheader popheaderlblred">
                        <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- Grid Alert popup Closed--%>
                <%--Batch Pending Modal popup starts--%>
                <asp:ModalPopupExtender ID="mpeBatchPending" runat="server" TargetControlID="hfBatchPending"
                    PopupControlID="pnlBatchPending" BackgroundCssClass="popbg" CancelControlID="btnBatchPendingCancel">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hfBatchPending" runat="server" />
                <asp:Panel runat="server" ID="pnlBatchPending" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, BATCH_PENDING_MSG%>"></asp:Label>
                    </div>
                    <div class="clear pad_10">
                    </div>
                    <div class="consumer_feild" style="margin-left:5px;">
                        <div class="consume_name">
                            <asp:Literal ID="Literal7" runat="server" Text="Batch No"></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblBatchNo" runat="server"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="Literal81" runat="server" Text="Reason"></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblReason" runat="server"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, BATCHDATE%>"></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblPendingBatchDate" runat="server"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, BATCHTOTAL%>"></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblPendingBatchTotal" runat="server"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="Literal9" runat="server" Text="Pending Amount"></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblBatchPending" runat="server"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="Literal16" runat="server" Text="No of Adjustments"></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblTotalCustomers" runat="server"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="Literal10" runat="server" Text="Adjustment Type"></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblAdjustmentType" runat="server" Text="Adjustment Not Started."></asp:Label>
                        </div>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btnDeleteBatch" runat="server" Text="<%$ Resources:Resource, DELETE_BATCH%>"
                                CssClass="btn_ok" Style="margin-left: 10px;" OnClick="btnDeleteBatch_Click" />
                            <asp:Button ID="btnBatchPendingCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="btn_ok" Style="margin-left: 10px;" />
                            <asp:Button ID="btnBatchPendingContinue" runat="server" Text="<%$ Resources:Resource, CONTINUE%>"
                                CssClass="btn_ok" Style="margin-left: 10px;" OnClick="btnContinue_Click" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%--Batch Pending Modal popup ends--%>
                <asp:ModalPopupExtender ID="mpeDeleteBatchNoti" runat="server" TargetControlID="hfBatchDel"
                    PopupControlID="pnlBatchDelete" BackgroundCssClass="popbg" CancelControlID="btnDelBatchCancel">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hfBatchDel" runat="server" />
                <asp:Panel runat="server" ID="pnlBatchDelete" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, SURE_TO_DELETE_BATCH%>"></asp:Label>
                        <div class="clear pad_10">
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnDelBatchCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="btn_ok" Style="margin-left: 105px;" />
                                <asp:Button ID="btnDelBatchOk" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="btn_ok" OnClick="btnDelBatchOk_Click" Style="margin-left: 10px;" />
                                <br />
                            </div>
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>


                <asp:ModalPopupExtender ID="mpeClosedBatchMessage" runat="server" TargetControlID="hfClosedBatchMessage"
                    PopupControlID="pnlClosedBatchStatus" BackgroundCssClass="popbg" CancelControlID="btnClosedBatchStatusOk">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hfClosedBatchMessage" runat="server" />
                <asp:Panel runat="server" ID="pnlClosedBatchStatus" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, BATCH_CLOSED%>"></asp:Label>
                        <div class="clear pad_10">
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnClosedBatchStatusOk" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="btn_ok" Style="margin-left: 105px;" />
                                <br />
                            </div>
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>



                <asp:HiddenField ID="hfBatchID" runat="server" />
                <asp:HiddenField ID="hfAdjustmentType" runat="server" />
                <asp:HiddenField ID="hfIsAdjusted" runat="server" Value="0" />
                <uc1:Authentication ID="ucAuthentication" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnNext" />
        </Triggers>
    </asp:UpdatePanel>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).ready(function () {
            Calendar('<%=txtBatchDate.ClientID %>', '<%=imgDate.ClientID %>', "-2:+8");
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            Calendar('<%=txtBatchDate.ClientID %>', '<%=imgDate.ClientID %>', "-2:+8");
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/AdjustmentBatch.js?v=1" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
