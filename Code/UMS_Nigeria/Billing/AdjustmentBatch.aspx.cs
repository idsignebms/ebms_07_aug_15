﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using iDSignHelper;
using System.Data;
using System.Configuration;
using System.Resources;
using Resources;

namespace UMS_Nigeria.Billing
{
    public partial class AdjustmentBatch : System.Web.UI.Page
    {

        #region Members

        BillAdjustmentBal _ObjBillAdjustmentBal = new BillAdjustmentBal();
        BillAdjustmentsBe _ObjBillAdjustmentsBe = new BillAdjustmentsBe();
        BillAdjustmentListBe _ObjBillAdjustmentListBe = new BillAdjustmentListBe();
        CommonMethods _OjbCommonMethods = new CommonMethods();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        #endregion

        #region Properties

        public int ReasonID
        {
            get { return string.IsNullOrEmpty(ddlReasonList.SelectedValue) ? 0 : Convert.ToInt32(ddlReasonList.SelectedValue); }
            set { ddlReasonList.SelectedValue = value.ToString(); }
        }
        public string BatchDate
        {
            get { return string.IsNullOrEmpty(txtBatchDate.Text) ? string.Empty : txtBatchDate.Text; }
            set { txtBatchDate.Text = value.ToString(); }
        }
        public string BatchNo
        {
            get { return string.IsNullOrEmpty(txtBatchNo.Text) ? "0" : txtBatchNo.Text; }
            set { txtBatchNo.Text = value.ToString(); }
        }
        public Decimal BatchTotal
        {
            get { return string.IsNullOrEmpty(txtBatchTotal.Text.Replace(",", string.Empty)) ? 0 : Convert.ToDecimal(txtBatchTotal.Text.Replace(",", string.Empty)); }
            set { txtBatchTotal.Text = value.ToString(); }
        }
        public int AdjustmentType
        {
            get { return string.IsNullOrEmpty(rdblAdjsutmentType.SelectedItem.Value) ? 0 : Convert.ToInt32(rdblAdjsutmentType.SelectedItem.Value); }
            set { rdblAdjsutmentType.SelectedItem.Value = value.ToString(); }
        }

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string path = string.Empty;
                path = _OjbCommonMethods.GetPagePath(this.Request.Url);
                if (_OjbCommonMethods.IsAccess(path))
                {
                    //if (Request.QueryString[Resources.UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO] != null)
                    BindReasonList();
                    BindBillAdjustmentType();
                }

                else
                {
                    Response.Redirect(Resources.UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                }
                if (Request.QueryString[Constants.QS_BatchID] != string.Empty && Request.QueryString[Constants.QS_BatchID] != null && Request.QueryString[Constants.QS_BatchDate] != null)
                {
                    BatchNo = _ObjiDsignBAL.Decrypt(Request.QueryString[Constants.QS_BatchID]);
                    GetBatchDetails(BatchNo, _ObjiDsignBAL.Decrypt(Request.QueryString[Constants.QS_BatchDate].ToString()), true);
                }
            }
        }

        protected void txtBatchNo_TextChanged(object sender, EventArgs e)
        {
            GetBatchDetails(BatchNo, txtBatchDate.Text, false);
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            string adjustmentTypeId = string.Empty;
            string adjustmentText = string.Empty;

            if (hfAdjustmentType.Value == "0")
            {
                adjustmentTypeId = rdblAdjsutmentType.SelectedItem.Value;
                adjustmentText = rdblAdjsutmentType.SelectedItem.Text;
            }
            else
            {
                adjustmentTypeId = hfAdjustmentType.Value;
                adjustmentText = rdblAdjsutmentType.Items.FindByValue(adjustmentTypeId).Text;
            }

            string url = Resources.UMS_Resource.BILL_ADJUSTMENTS
                                   + Constants.QS_Initiat + Constants.QS_BatchID + Constants.QS_EqualTo + _ObjiDsignBAL.Encrypt(hfBatchID.Value.ToString())
                                   + Constants.QS_Seprator + Constants.QS_AdjustmentTypeid + Constants.QS_EqualTo + _ObjiDsignBAL.Encrypt(adjustmentTypeId)
                                   + Constants.QS_Seprator + Constants.QS_AdjustmentText + Constants.QS_EqualTo + _ObjiDsignBAL.Encrypt(adjustmentText);

            Response.Redirect(url);

        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            string adjustmentTypeId = string.Empty;
            string adjustmentText = string.Empty;

            if (hfAdjustmentType.Value == "0" || hfAdjustmentType.Value == string.Empty)
            {
                adjustmentTypeId = rdblAdjsutmentType.SelectedItem.Value;
                adjustmentText = rdblAdjsutmentType.SelectedItem.Text;
                lblAdjustmentType.Text = adjustmentText;
            }
            else
            {
                adjustmentTypeId = hfAdjustmentType.Value;
                adjustmentText = rdblAdjsutmentType.Items.FindByValue(adjustmentTypeId).Text;
            }

            //if (hfBatchID.Value == "0" || hfBatchID.Value == "")
            //{
                if (AddNewBatch() > 0)
                {
                    Response.Redirect(Resources.UMS_Resource.BILL_ADJUSTMENTS
                                + Constants.QS_Initiat + Constants.QS_BatchID + Constants.QS_EqualTo + _ObjiDsignBAL.Encrypt(hfBatchID.Value.ToString())
                                + Constants.QS_Seprator + Constants.QS_AdjustmentTypeid + Constants.QS_EqualTo + _ObjiDsignBAL.Encrypt(adjustmentTypeId)
                                + Constants.QS_Seprator + Constants.QS_AdjustmentText + Constants.QS_EqualTo + _ObjiDsignBAL.Encrypt(adjustmentText)
                                + Constants.QS_Seprator + Constants.QS_BatchDate + Constants.QS_EqualTo + _ObjiDsignBAL.Encrypt(txtBatchDate.Text));
                }
                else
                {
                    GetBatchDetails(txtBatchNo.Text, txtBatchDate.Text, false);
                    if (hfClosedBatchMessage.Value == "2")
                    {
                       
                        mpeClosedBatchMessage.Show();
                        ResetControls();
                        txtBatchNo.Text = string.Empty;
                    }
                    else
                    {
                        mpeBatchPending.Show();
                        //Message("Sorry, unable to add new batch. Please try again.", Resources.UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
            //}
            //else
            //{
            //    GetBatchDetails(txtBatchNo.Text, txtBatchDate.Text, false);
            //    mpeBatchPending.Show();
            //}
        }

        protected void btnDeleteBatch_Click(object sender, EventArgs e)
        {
            mpeDeleteBatchNoti.Show();
        }

        protected void btnDelBatchOk_Click(object sender, EventArgs e)
        {
            DeleteBatch();
        }

        #endregion

        #region Method

        private void Message(string Message, string MessageType)
        {
            try
            {
                _OjbCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void BindReasonList()
        {
            _ObjBillAdjustmentListBe = _ObjiDsignBAL.DeserializeFromXml<BillAdjustmentListBe>(_ObjBillAdjustmentBal.GetReasonCodeListBAL());
            if (_ObjBillAdjustmentListBe.items.Count > 0)
            {
                DataSet ds = _ObjiDsignBAL.ConvertListToDataSet<BillAdjustmentsBe>(_ObjBillAdjustmentListBe.items);
                _ObjiDsignBAL.FillDropDownList(ds, ddlReasonList, "ReasonCode", "RCID", "select", true);
            }
        }

        public void BindBillAdjustmentType()
        {
            DataSet ds = new DataSet();
            ds.ReadXml(Server.MapPath(ConfigurationSettings.AppSettings["AdjustmentTypes"]));
            _ObjiDsignBAL.FillRadioButtonList(ds, rdblAdjsutmentType, "name", "value");
            rdblAdjsutmentType.Items[0].Selected = true;
        }

        public int AddNewBatch()
        {
            BillAdjustmentsBe objBillAdjustmentsBe = new BillAdjustmentsBe();
            objBillAdjustmentsBe.BatchNo = Convert.ToInt32(txtBatchNo.Text);
            objBillAdjustmentsBe.ReasonCode = ReasonID.ToString();
            objBillAdjustmentsBe.BatchTotal = Convert.ToDecimal(BatchTotal);
            objBillAdjustmentsBe.CreatedDate = _OjbCommonMethods.Convert_MMDDYY(txtBatchDate.Text);
            objBillAdjustmentsBe.CreatedBy = Session[Resources.UMS_Resource.SESSION_LOGINID].ToString();
            objBillAdjustmentsBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
            objBillAdjustmentsBe.BillAdjustmentType =Convert.ToInt32(rdblAdjsutmentType.SelectedValue);
            int batchID = (_ObjiDsignBAL.DeserializeFromXml<BillAdjustmentsBe>(_ObjBillAdjustmentBal.InsertAdjustmentBatchBAL(objBillAdjustmentsBe))).BatchID; ;
            hfBatchID.Value = batchID.ToString();
            return batchID;
        }

        public int ShowBatchDetails(BillAdjustmentsBe objBillAdjustmentsBe)
        {
            if (objBillAdjustmentsBe != null && objBillAdjustmentsBe.BatchNo > 0)
            {
                lblBatchNo.Text = objBillAdjustmentsBe.BatchNo.ToString();
                lblReason.Text = objBillAdjustmentsBe.ReasonCode;
                lblPendingBatchDate.Text = _OjbCommonMethods.DateFormate(objBillAdjustmentsBe.BatchDate);

                lblPendingBatchTotal.Text = _OjbCommonMethods.GetCurrencyFormat(Convert.ToDecimal(objBillAdjustmentsBe.BatchTotal.ToString()), 2, Constants.MILLION_Format).ToString();
                if (objBillAdjustmentsBe.BillAdjustmentType != 0)
                {
                    if (objBillAdjustmentsBe.BillAdjustmentType == Convert.ToInt32(Constants.Reading_Adjustment) || objBillAdjustmentsBe.BillAdjustmentType == Convert.ToInt32(Constants.Consumption_Adjustment))
                        lblBatchPending.Text = _OjbCommonMethods.GetCurrencyFormat(Convert.ToDecimal(objBillAdjustmentsBe.PendingUnit.ToString()), 2, Constants.MILLION_Format).ToString();
                    else if (objBillAdjustmentsBe.BillAdjustmentType == Convert.ToInt32(Constants.Additional_Charges_Adjustment) || objBillAdjustmentsBe.BillAdjustmentType == Convert.ToInt32(Constants.Bill_Adjustment))
                        lblBatchPending.Text = _OjbCommonMethods.GetCurrencyFormat(Convert.ToDecimal(objBillAdjustmentsBe.PendingAmount.ToString()), 2, Constants.MILLION_Format).ToString();

                    lblAdjustmentType.Text = rdblAdjsutmentType.Items.FindByValue(objBillAdjustmentsBe.BillAdjustmentType.ToString()).Text;
                }
                else
                {
                    //rdblAdjsutmentType.Enabled = true;
                    lblAdjustmentType.Text = rdblAdjsutmentType.SelectedItem.Text;
                }

                if (objBillAdjustmentsBe.BillAdjustmentType == 0)
                {
                    lblBatchPending.Text = _OjbCommonMethods.GetCurrencyFormat(objBillAdjustmentsBe.BatchTotal, 2, Constants.MILLION_Format);
                }

                lblTotalCustomers.Text = objBillAdjustmentsBe.TotalCustomers.ToString();
                hfBatchID.Value = objBillAdjustmentsBe.BatchID.ToString();
                hfAdjustmentType.Value = objBillAdjustmentsBe.BillAdjustmentType.ToString();
                return objBillAdjustmentsBe.BatchID;
            }
            else
                return 0;

        }

        public void BindFields(BillAdjustmentsBe objBillAdjustmentsBe)
        {
            if (objBillAdjustmentsBe != null && objBillAdjustmentsBe.BatchNo > 0)
            {

                txtBatchDate.Text = Convert.ToDateTime(objBillAdjustmentsBe.BatchDate).ToString("dd/MM/yyyy");
                ddlReasonList.SelectedIndex = ddlReasonList.Items.IndexOf(ddlReasonList.Items.FindByValue(objBillAdjustmentsBe.RCID.ToString()));
                ddlReasonList.Items.FindByValue(objBillAdjustmentsBe.RCID.ToString()).Enabled = true;
                txtBatchTotal.Text = _OjbCommonMethods.GetCurrencyFormat(objBillAdjustmentsBe.BatchTotal, 2, Constants.MILLION_Format);
                if (objBillAdjustmentsBe.BillAdjustmentType != 0)
                {
                    rdblAdjsutmentType.Items.FindByValue(objBillAdjustmentsBe.BillAdjustmentType.ToString()).Selected = true;
                    //rdblAdjsutmentType.Enabled = false;
                    hfIsAdjusted.Value = "1";
                }
                else
                {
                    BindBillAdjustmentType();
                    hfIsAdjusted.Value = "0";
                }

            }
            else
            {
                ResetControls();
            }
        }

        private void ResetControls()
        {
            txtBatchDate.Text = string.Empty;
            txtBatchTotal.Text = string.Empty;
            hfBatchID.Value = "0";
            //rdblAdjsutmentType.Enabled = true;
            BindReasonList();
            BindBillAdjustmentType();
        }

        public BillAdjustmentsBe GetBatchDetails(string Batch, string batchDate, bool isDateConverted)
        {
            BillAdjustmentsBe objBillAdjustmentsBe = new BillAdjustmentsBe();
            txtBatchNo.Text.Trim();
            if (Convert.ToInt32(Batch) != 0)
            {
                objBillAdjustmentsBe = new BillAdjustmentsBe();
                objBillAdjustmentsBe.BatchNo = Convert.ToInt32(Batch);
                objBillAdjustmentsBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                if (!isDateConverted) objBillAdjustmentsBe.BatchDate = _OjbCommonMethods.Convert_MMDDYY(batchDate);
                else objBillAdjustmentsBe.BatchDate = batchDate;
                objBillAdjustmentsBe = _ObjiDsignBAL.DeserializeFromXml<BillAdjustmentsBe>(_ObjBillAdjustmentBal.GetAdjustmentBatchByNoBAL(objBillAdjustmentsBe));
                hfClosedBatchMessage.Value =  objBillAdjustmentsBe.BatchStatusId.ToString();
                BindFields(objBillAdjustmentsBe);
                ShowBatchDetails(objBillAdjustmentsBe);

            }
            return objBillAdjustmentsBe;
        }

        public void DeleteBatch()
        {
            BillAdjustmentsBe objBillAdjustmentsBe = new BillAdjustmentsBe();
            objBillAdjustmentsBe.BatchID = Convert.ToInt32(hfBatchID.Value);
            int rowCount = (_ObjiDsignBAL.DeserializeFromXml<BillAdjustmentsBe>(_ObjBillAdjustmentBal.DeleteBatchAdjustmentsBAL(objBillAdjustmentsBe))).RowCount;
            if (rowCount > 0)
            {
                Message(Resources.Resource.BATCH_DEL_SUCCESS, Resources.UMS_Resource.MESSAGETYPE_SUCCESS);
                Response.Redirect(UMS_Resource.Adjustment_Batch);
            }
            else
                Message(Resources.Resource.BATCH_NOT_DELETED, Resources.UMS_Resource.MESSAGETYPE_ERROR);
        }

        #endregion

    }
}