﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.Drawing;
using iDSignHelper;
using Resources;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using System.Data.SqlClient;
using UMS_NigeriaBE;

namespace UMS_Nigeria.Billing
{
    public partial class AdjustmentBatchReopen : System.Web.UI.Page
    {
        #region Properties

        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }

        }
        #endregion

        #region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        BatchStatusBAL _objBatchStatusBal = new BatchStatusBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        UMS_Service _ObjUMS_Service = new UMS_Service();
        XmlDocument xml = new XmlDocument();
        public int PageNum;
        string Key = "AdjustmentBatchReopen";
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
            }
        }

        protected void gvAdjustmentBatchReopen_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            btnBatchReopenOk.Visible = false;
            switch (e.CommandName.ToUpper())
            {
                case "BATCHREOPEN":
                    try
                    {
                        GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                        hfBatchNo.Value = ((Literal)row.FindControl("litID")).Text;
                        mpeBatchClose.Show();
                        btnBatchReopenOk.Visible = true;
                        lblBatchMsg.Text = "Are you sure to Reopen this batch ?";
                        btnBatchReopenOk.Focus();
                    }

                    catch (Exception ex)
                    {
                        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                        try
                        {
                            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                        }
                        catch (Exception logex)
                        {

                        }
                    }
                    break;
            }

        }

        protected void gvAdjustmentBatchReopen_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //decimal amount = Convert.ToDecimal(((Literal)e.Row.FindControl("litPaidAmount")).Text);
                //LinkButton lkDelete = (LinkButton)e.Row.FindControl("lkBatchDelete");
                //Literal litBatchTotal = (Literal)e.Row.FindControl("litBatchTotal");
                //Literal litPaidAmount = (Literal)e.Row.FindControl("litPaidAmount");
                //litBatchTotal.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(litBatchTotal.Text), 2, Constants.MILLION_Format);
                //litPaidAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(litPaidAmount.Text), 2, Constants.MILLION_Format);
                //if (amount > 0)
                //    lkDelete.Visible = false;
            }
        }

        protected void btnBatchReopenOk_Click(object sender, EventArgs e)
        {
            try
            {
                BatchStatusBE _objBatchStatusBe = new BatchStatusBE();
                _objBatchStatusBe.BatchId = Convert.ToInt32(hfBatchNo.Value);
                xml = _objBatchStatusBal.ReopenBatchAdjustments(_objBatchStatusBe);
                _objBatchStatusBe = _ObjiDsignBAL.DeserializeFromXml<BatchStatusBE>(xml);
                if (_objBatchStatusBe.RowsEffected > 0)
                {
                    Message("Batch Reopened Successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }



        #endregion

        #region Methods

        public void BindGrid()
        {
            BatchStatusBAL _objBatchStatusBal = new BatchStatusBAL();
            BatchStatusBE _objBatchStatusBE = new BatchStatusBE();
            _objBatchStatusBE.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
            _objBatchStatusBE.PageNo = Convert.ToInt32(hfPageNo.Value);
            _objBatchStatusBE.PageSize = PageSize;
            DataSet ds = _objBatchStatusBal.GetReopenBatchAdjustments(_objBatchStatusBE);

            string businessUnit = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
            //ds.Tables[0].DefaultView.RowFilter = "BUID = '" + businessUnit + "'";
            hfTotalRecords.Value = ds.Tables[0].Rows.Count.ToString();
            UCPaging1.Visible = UCPaging2.Visible = (ds.Tables[0].Rows.Count > 0);
            if (ds.Tables[0].Rows.Count > 0)
                hfTotalRecords.Value = ds.Tables[0].Rows[0]["TotalRecords"].ToString();
            gvAdjustmentBatchReopen.DataSource = ds.Tables[0];
            gvAdjustmentBatchReopen.DataBind();
        }
        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}