﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AverageConsumptionBulkUpload
                     
 Developer        : Faiz - ID103
 Creation Date    : 30-Apr-2015
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.IO;
using UMS_NigeriaBE;
using Resources;
using iDSignHelper;
using UMS_NigeriaBAL;
using System.Xml;
using Resources;
using System.Net;
using UMS_NigriaDAL;
using System.Collections;
using System.Data.OleDb;
using System.Globalization;
using UMS_NigeriaBAL;

namespace UMS_Nigeria.Billing
{
    public partial class AverageConsumptionBulkUpload : System.Web.UI.Page
    {
        #region Members

        CommonMethods _objCommonMethods = new CommonMethods();
        iDsignBAL _objIdsignBal = new iDsignBAL();
        UserManagementBAL objUserManagementBAL = new UserManagementBAL();
        BillReadingsBAL objBillingBAL = new BillReadingsBAL();
        MastersBAL objMastersBAL = new MastersBAL();

        string Key = "AverageConsumptionBulkUpload";
        public int PageNum;
        DataTable dtExcel = new DataTable();
        DataTable dtXMl = new DataTable();

        #endregion

        #region Properties

        private string Notes
        {
            get { return txtNotes.Text.Trim(); }
            set { txtNotes.Text = value; }
        }

        private int BatchNo
        {
            get { return Convert.ToInt32(txtBatchNoNew.Text.Trim()); }
            set { txtBatchNoNew.Text = value.ToString(); }
        }

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }
        static DataTable GetTable()
        {
            // Here we create a DataTable with four columns.
            DataTable table = new DataTable();
            table.Columns.Add("SNO", typeof(string));
            table.Columns.Add("Global_Account_Number", typeof(string));
            table.Columns.Add("Average_Reading", typeof(string));

            return table;
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowLoading", "ShowLoading(1)", true);
            try
            {
                dtExcel = GetTable();
                Hashtable htableMaxLength = new Hashtable();
                int count = 0;
                string ExcelModifiedFileName = InsersertExcel();
                //if (Path.GetExtension(ExcelModifiedFileName) == ".xml")
                //{
                //    DataSet ds = new DataSet();
                //    ds.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["MeterReading"].ToString()) + ExcelModifiedFileName);
                //    dtExcel = ds.Tables[0];
                //}
                //else 
                if (Path.GetExtension(ExcelModifiedFileName) == ".xls" || Path.GetExtension(ExcelModifiedFileName) == ".xlsx")
                {
                    string FolderPath = ConfigurationManager.AppSettings["MeterReading"];
                    string FilePath = Server.MapPath(FolderPath + ExcelModifiedFileName);
                    String cnstr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FilePath + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";//2007 Onwards

                    OleDbConnection oledbConn = new OleDbConnection(cnstr);
                    String[] s = _objCommonMethods.GetExcelSheetNames(FilePath);

                    if (s != null)
                    {
                        string strSQL = "SELECT * FROM [" + s[0] + "]";
                        OleDbCommand cmd = new OleDbCommand(strSQL, oledbConn);
                        DataSet ds = new DataSet();
                        OleDbDataAdapter odapt = new OleDbDataAdapter(cmd);
                        odapt.Fill(dtExcel);
                    }
                }
                if (dtExcel.Rows.Count > 0)//Validating excel columns.
                {
                    for (int i = 0; i < dtExcel.Columns.Count; i++)
                    {
                        DataSet dsHeaders = new DataSet();
                        dsHeaders.ReadXml(Server.MapPath(ConfigurationSettings.AppSettings["DirectCustReadingUploadHeaders"]));
                        if (dtExcel.Columns.Count == dsHeaders.Tables[0].Rows.Count)
                        {
                            string HeaderName = dtExcel.Columns[i].ColumnName;
                            var result = (from x in dsHeaders.Tables[0].AsEnumerable()
                                          where x.Field<string>("Names").Trim() == HeaderName.Trim()
                                          select new
                                          {
                                              Names = x.Field<string>("Names")
                                          }).SingleOrDefault();
                            if (result == null)
                            {
                                count++;
                                lblMsgPopup.Text = "Uploaded file format is not appropriate.";
                                mpeAverageConsumptionpopup.Show();
                                break;
                            }
                        }
                        else
                        {
                            count++;
                            lblMsgPopup.Text = "Uploaded file format is not appropriate.";
                            mpeAverageConsumptionpopup.Show();
                            break;
                        }
                    }
                    if (count == 0)
                    {
                        bool isExist = false;
                        bool isSuccess = false;

                        DataView dv = dtExcel.DefaultView;
                        dv.RowFilter = "LEN(Global_Account_Number) > 0";
                        dv.Sort = "SNO ASC";
                        dtExcel = dv.ToTable();

                        if (dtExcel.Rows.Count > 0)
                        {
                            int BatchNo = Convert.ToInt32(txtBatchNoNew.Text);
                            string BatchName = txtBatchNoNew.Text;
                            string BatchDate = _objCommonMethods.Convert_MMDDYY(txtBatchDate.Text);
                            string CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                            string FileName = ExcelModifiedFileName;
                            string BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                            int TotalRecords = dtExcel.Rows.Count;

                            isSuccess = objBillingBAL.InsertBatchAverageConsumptionUploadDT(BatchNo, BatchName, BatchDate, CreatedBy, txtNotes.Text, TotalRecords, FileName, dtExcel, BU_ID, out isExist);

                            if (isSuccess)
                            {
                                popgridalert.Attributes.Add("class", "popheader popheaderlblgreen");
                                lblgridalertmsg.Text = "Successfully saved the Batch Average Consumption details.";
                                txtBatchNoNew.Text = txtBatchDate.Text = txtNotes.Text = string.Empty;
                                mpegridalert.Show();
                            }
                            else if (isExist)
                            {
                                popgridalert.Attributes.Add("class", "popheader popheaderlblred");
                                lblgridalertmsg.Text = "Batch Number already exists for selected Batch Month, Please choose another Batch Number";
                                mpegridalert.Show();
                            }
                            else
                            {
                                popgridalert.Attributes.Add("class", "popheader popheaderlblred");
                                lblgridalertmsg.Text = "Batch Average Consumption Upload failed.";
                                mpegridalert.Show();
                            }
                        }
                        else
                        {
                            lblMsgPopup.Text = "No valid data to upload";
                            mpeAverageConsumptionpopup.Show();
                        }
                    }
                    else
                    {
                        lblMsgPopup.Text = "Uploaded file format is not appropriate.";
                        mpeAverageConsumptionpopup.Show();
                    }
                }
                else
                {
                    lblMsgPopup.Text = "Unable to read the file, please try with another file with proper formats.";
                    mpeAverageConsumptionpopup.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            //finally
            //{
            //    Page.ClientScript.RegisterStartupScript(this.GetType(), "HideLoading", "ShowLoading(0)", true);
            //}
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                lblMessage.Text = pnlMessage.CssClass = string.Empty;
                if (!IsPostBack)
                {
                    string path = string.Empty;
                    //path = _objCommonMethods.GetPagePath(this.Request.Url);
                    //if (_objCommonMethods.IsAccess(path))
                    //{

                    //}
                    //else { Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }
                }
            }
            else
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        #endregion

        #region Methods

        public string InsersertExcel()
        {
            try
            {
                string ExcelModifiedFileName = string.Empty;

                if (upDOCExcel.HasFile)
                {
                    string ExcelFileName = System.IO.Path.GetFileName(upDOCExcel.FileName).ToString();//Assinging FileName
                    string ExcelExtension = Path.GetExtension(ExcelFileName);//Storing Extension of file into variable Extension
                    TimeZoneInfo ExcelIND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                    DateTime ExcelcurrentTime = DateTime.Now;
                    DateTime Exceloourtime = TimeZoneInfo.ConvertTime(ExcelcurrentTime, ExcelIND_ZONE);
                    string ExcelCurrentDate = Exceloourtime.ToString("dd-MM-yyyy_hh-mm-ss");
                    ExcelModifiedFileName = Path.GetFileNameWithoutExtension(ExcelFileName) + "_" + ExcelCurrentDate + ExcelExtension;//Setting File Name to store it in database
                    upDOCExcel.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["MeterReading"].ToString()) + ExcelModifiedFileName);//Saving File in to the server.
                }
                return ExcelModifiedFileName;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public bool GetBatchNo(string batchNo)
        {
            return false;
        }

        #endregion
    }
}