﻿<%@ Page Title=":: Batch Payment Status ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master" Theme="Green"
    AutoEventWireup="true" CodeBehind="BatchPaymentStatus.aspx.cs" Inherits="UMS_Nigeria.Billing.BatchPaymentStatus" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upBatchPaymentStatus" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div class="grid_boxes">
                    <div class="grid_paging_top">
                        <div class="paging_top_title">
                            <asp:Literal ID="litBuList" runat="server" Text="Batch Payment Status"></asp:Literal>
                        </div>
                        <div class="paging_top_right_content" id="divPaging1" runat="server">
                            <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="grid_tb" id="divgrid" runat="server">
                    <asp:GridView ID="gvBatchPaymentStatus" runat="server" AutoGenerateColumns="False"
                        AlternatingRowStyle-CssClass="color" HeaderStyle-CssClass="grid_tb_hd" OnRowCommand="gvBatchPaymentStatus_RowCommand"
                        OnRowDataBound="gvBatchPaymentStatus_RowDataBound">
                        <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                        <EmptyDataTemplate>
                            No Details Found.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, GRID_SNO%>"
                                DataField="RowNumber" ItemStyle-CssClass="grid_tb_td" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Batch No" DataField="BatchNo"
                                ItemStyle-CssClass="grid_tb_td" />
                            <%--  <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, BATCH_TOTAL%>"
                                DataField="BatchTotal" ItemStyle-CssClass="grid_tb_td" />--%>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, BATCH_TOTAL%>" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Literal ID="litBatchTotal" runat="server" Text='<%# Eval("BatchTotal") %>'></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, BATCH_DATE%>"
                                DataField="BatchDate" ItemStyle-CssClass="grid_tb_td" />
                            <%--<asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, PAID_AMOUNT%>"
                    DataField="PaidAmount" ItemStyle-CssClass="grid_tb_td" />--%>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, PAID_AMOUNT%>" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Literal ID="litPaidAmount" runat="server" Text='<%# Eval("PaidAmount") %>'></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, STATUS%>"
                                    DataField="BatchStatus" ItemStyle-CssClass="grid_tb_td" />--%>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Literal ID="litID" Visible="False" runat="server" Text='<%# Eval("BatchId") %>'></asp:Literal>
                                    <asp:LinkButton ID="lkBatchClose" runat="server" ToolTip="Batch Close" CommandName="BatchCLose">
                            <img src="../images/cancel.png" alt="DeActive" />
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lkBatchDelete" runat="server" Visible="false" ToolTip="Batch Delete" CommandName="BatchDelete">
                            <img src="../images/delete.png" alt="DeActive" />
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle HorizontalAlign="Center" />
                    </asp:GridView>
                    <asp:HiddenField ID="hfBatchNo" runat="server" />
                    <asp:HiddenField ID="hfPageSize" runat="server" />
                    <asp:HiddenField ID="hfPageNo" runat="server" Value="0" />
                    <asp:HiddenField ID="hfLastPage" runat="server" />
                    <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                </div>
                <div class="grid_boxes">
                    <div id="divPaging2" runat="server">
                        <div class="grid_paging_bottom">
                            <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <%--  Btn popup Start--%>
            <asp:ModalPopupExtender ID="mpeBatchClose" runat="server" PopupControlID="PanelBatchClose"
                TargetControlID="hdnBatchClose" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
            </asp:ModalPopupExtender>
            <asp:HiddenField ID="hdnBatchClose" runat="server" />
            <asp:Panel ID="PanelBatchClose" runat="server" DefaultButton="" CssClass="modalPopup"
                Style="display: none; border-color: #639B00;">
                <div class="popheader" style="background-color: red;">
                    <asp:Label ID="lblBatchMsg" runat="server"></asp:Label>
                </div>
                <br />
                <div class="footer" align="right">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="btnBatchCloseOk" Visible="False" runat="server" Text="<%$ Resources:Resource, OK%>"
                            OnClick="btnBatchCloseOk_Click" CssClass="ok_btn" />
                        <asp:Button ID="btnBatchDeleteOk" Visible="False" runat="server" Text="<%$ Resources:Resource, OK%>"
                            OnClick="btnBatchDeleteOk_Click" CssClass="ok_btn" />
                        <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                            CssClass="cancel_btn" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
            <%-- Active  Btn popup Closed--%>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });
    </script>
</asp:Content>
