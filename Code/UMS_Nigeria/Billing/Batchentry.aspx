﻿<%@ Page Title="::Batch Details::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="Batchentry.aspx.cs" Inherits="UMS_Nigeria.Billing.PaymEntry" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%--<%@ Register Src="../UserControls/Authentication.ascx" TagName="Authentication" TagPrefix="uc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <%--<asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />--%>
        <asp:UpdatePanel ID="upStates" runat="server">
            <ContentTemplate>
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litReadMethod" runat="server" Text="<%$ Resources:Resource, BATCHDETAILS%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, BATCHNUM%>"></asp:Literal>
                                <span class="span_star">*</span><br />
                                <%--<asp:Button ID="btnGetBatchDetails" runat="server" Style="display: none;" OnClick="btnGetBatchDetails_Click" />--%>
                                <asp:TextBox ID="txtBatchNo" CssClass="text-box" runat="server" MaxLength="6" placeholder="Batch No"
                                    onchange="GetBatchDetails();" onblur="return TextBoxBlurValidationlbl(this,'spanBatchNo','Batch No')"></asp:TextBox><%--AutoPostBack="true" 
                                    ontextchanged="txtBatchNo_TextChanged"--%>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtBatchNo"
                                    FilterType="Numbers">
                                </asp:FilteredTextBoxExtender>
                                <br />
                                <span id="spanBatchNo" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, BATCHDATE%>"></asp:Literal>
                                <span class="span_star">*</span><br />
                                <asp:TextBox ID="txtBatchDate" placeholder="Batch Date" Style="width: 184px;" CssClass="text-box"
                                    onblur="TextBoxBlurValidationlbl(this, 'spanBatchDate', 'BatchDate')" onchange="TextBoxBlurValidationlbl(this, 'spanBatchDate', 'Batch Date')"
                                    AutoComplete="off" MaxLength="10" runat="server"></asp:TextBox>
                                <asp:Image ID="imgDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                    vertical-align: middle" AlternateText="Calender" runat="server" />
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtBatchDate"
                                    FilterType="Custom,Numbers" ValidChars="/">
                                </asp:FilteredTextBoxExtender>
                                <%--AutoPostBack="True" ontextchanged="txtBatchDate_TextChanged" onchange="GetBatchDetails();" --%>
                                <span id="spanBatchDate" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, PAYMENT_MODE%>"> </asp:Literal>
                                <span class="span_star">*</span><br />
                                <asp:DropDownList ID="ddlPaymentMode" onchange="DropDownlistOnChangelbl(this,'spanPaymentMode',' Payment Mode')"
                                    runat="server" CssClass="text-box select_box">
                                    <asp:ListItem Text="--Select--">
                                    </asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                <span id="spanPaymentMode" class="span_color"></span>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource,  BATCHTOTAL%>"></asp:Literal>
                                <span class="span_star">*</span><br />
                                <asp:TextBox ID="txtBatchTotal" placeholder="Batch Total" CssClass="text-box" runat="server"
                                    MaxLength="18" onkeyup="GetCurrencyFormate(this);" onblur="GetCurrencyFormate(this);"
                                    onkeypress="return isNumberKey(event,this)"></asp:TextBox><%-- onblur="return TextBoxBlurValidationlbl(this,'spanBatchTotal','Batch Total')" --%>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtBatchTotal"
                                    FilterType="Custom,Numbers" ValidChars=".,">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanBatchTotal" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource,  CASH_OFFICE%>"></asp:Literal>
                                <br />
                                <asp:DropDownList ID="ddlCashOffice" runat="server" CssClass="text-box select_box"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlCashOffice_SelectedIndexChanged">
                                    <asp:ListItem Text="--Select--"></asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                <span id="spanddlCashOffice" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource,  CASHIER%>"></asp:Literal>
                                <br />
                                <asp:DropDownList ID="ddlCashier" runat="server" Enabled="false" CssClass="text-box select_box">
                                    <asp:ListItem Text="--Select--"></asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                <span id="spanddlCashier" class="span_color"></span>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                        <br />
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <asp:Button ID="btnNext" CssClass="box_s" Text="<%$ Resources:Resource, NEXT%>" runat="server"
                                OnClick="btnNext_Click" OnClientClick="return Validate();" />
                        </div>
                    </div>
                </div>
                <asp:ModalPopupExtender ID="mpeBatchPending" runat="server" TargetControlID="btnpopup"
                    PopupControlID="pnlBatchPending" BackgroundCssClass="popbg" CancelControlID="btnCancel">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnpopup" runat="server" Text="" Style="display: none;" />
                <asp:Panel runat="server" ID="pnlBatchPending" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblBatchPendingMsg" runat="server" Text="<%$ Resources:Resource, BATCH_PENDING_MSG%>"></asp:Label>
                    </div>
                    <div class="clear pad_10">
                    </div>
                    <div class="consumer_feild">
                        <div class="consume_name">
                            <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:Resource, BATCHDATE%>"></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblBatchDate" runat="server"></asp:Label>
                        </div>
                        <%--<div class="clear pad_5">
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, BATCHNAME%>"></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblBatchName" runat="server"></asp:Label>
                        </div>--%>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:Resource, BATCHTOTAL%>"></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblBatchTotal" runat="server"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="Literal12" runat="server" Text="Batch Pending"></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblBatchPending" runat="server"></asp:Label>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblBatchId" runat="server" Visible="false"></asp:Label>
                        </div>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btnDeleteBatch" runat="server" Text="<%$ Resources:Resource, DELETE_BATCH%>"
                                CssClass="btn_ok" OnClientClick="CloseMPE();" Style="margin-left: 10px; display: none;"
                                OnClick="btnDeleteBatch_Click" />
                            <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:Resource, NO_THANKS%>"
                                CssClass="btn_ok" Style="margin-left: 10px;" />
                            <asp:Button ID="btnContinue" runat="server" Text="<%$ Resources:Resource, CONTINUE%>"
                                CssClass="btn_ok" Style="margin-left: 10px;" OnClick="btnContinue_Click" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeBatchEnd" runat="server" TargetControlID="btnMpBatchFinish"
                    PopupControlID="pnlBatchFinish" BackgroundCssClass="popbg" CancelControlID="btnBatchFinishOK">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnMpBatchFinish" runat="server" Text="" Style="display: none;" />
                <asp:Panel runat="server" ID="pnlBatchFinish" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, BATCH_END%>"></asp:Label>
                    </div>
                    <div class="clear pad_10">
                    </div>
                    <div class="consumer_feild">
                        <div class="consume_name">
                            <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, BATCHDATE%>"></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblBatchDateFin" runat="server"></asp:Label>
                        </div>
                        <%--<div class="clear pad_5">
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:Resource, BATCHNAME%>"></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblBatchNameFin" runat="server"></asp:Label>
                        </div>--%>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, BATCHTOTAL%>"></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblBatchTotalFin" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btnBatchFinishOK" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="btn_ok" Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeBatchFinished" runat="server" TargetControlID="Button1"
                    PopupControlID="pnlBatchFinished" BackgroundCssClass="popbg" CancelControlID="btnOk">
                </asp:ModalPopupExtender>
                <asp:Button ID="Button1" runat="server" Text="" Style="display: none;" />
                <asp:Panel runat="server" ID="pnlBatchFinished" CssClass="modalPopup" Style="left: 537px;
                    display: none;">
                    <div class="panelMessage">
                        <div class="popheader" style="background-color: red;">
                            <asp:Label ID="lblBatchFinishedMsg" runat="server" Text="<%$ Resources:Resource, BATCH_FINISHED_MSG%>"></asp:Label>
                        </div>
                        <div class="clear pad_10">
                        </div>
                        <div class="buttondiv">
                            <asp:Button ID="btnOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="btn_ok"
                                Style="margin-left: 768px;" />
                            <br />
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeDeleteBatchNoti" runat="server" TargetControlID="btnpopup"
                    PopupControlID="pnlBatchDelete" BackgroundCssClass="popbg" CancelControlID="btnCancel">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hfBatchDel" runat="server" />
                <asp:Panel runat="server" ID="pnlBatchDelete" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, SURE_TO_DELETE_BATCH%>"></asp:Label>
                        <div class="clear pad_10">
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnDelBatchCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="btn_ok" Style="margin-left: 105px;" />
                                <asp:Button ID="btnDelBatchOk" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="btn_ok" OnClick="btnDelBatchOk_Click" Style="margin-left: 10px;" />
                                <br />
                            </div>
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- Grid Alert popup Start--%>
                <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                    TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                </asp:ModalPopupExtender>
                <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                    <div class="popheader popheaderlblred">
                        <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- Grid Alert popup Closed--%>
                <%--<uc1:Authentication ID="ucAuthentication" runat="server" />--%>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="ddlCashOffice" />
                <asp:PostBackTrigger ControlID="btnNext" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <%--==============Validation script ref starts==============--%>
    <%--==============Validation script starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <%--==============Validation script ends==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/Batchentry.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
