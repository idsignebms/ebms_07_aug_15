﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for PaymEntry
                     
 Developer        : Id075-RamaDevi M
 Creation Date    : 04-Apr-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using Resources;
using UMS_NigeriaBE;
using System.Globalization;
using System.Threading;
using UMS_NigriaDAL;
using System.Xml; 

namespace UMS_Nigeria.Billing
{
    public partial class PaymEntry : System.Web.UI.Page
    {
        # region Members

        iDsignBAL _objiDsignBAL = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        BillReadingsBAL objBillingBAL = new BillReadingsBAL();
        iDsignBAL _objIdsignBAL = new iDSignHelper.iDsignBAL();
        MastersBAL _objMastersBAL = new MastersBAL();
        public int PageNum;
        string Key = UMS_Resource.KEY_BATCH_ENTRY;

        #endregion

        #region Properties

        public string Cashier
        {
            get { return string.IsNullOrEmpty(ddlCashier.SelectedValue) ? string.Empty : ddlCashier.SelectedValue; }
            set { ddlCashier.SelectedValue = value.ToString(); }
        }

        public int CashOffice
        {
            get { return string.IsNullOrEmpty(ddlCashOffice.SelectedValue) ? 0 : Convert.ToInt32(ddlCashOffice.SelectedValue); }
            set { ddlCashOffice.SelectedValue = value.ToString(); }
        }

        private string BatchTotal
        {
            get { return txtBatchTotal.Text.Trim(); }
            set { txtBatchTotal.Text = value; }
        }

        private string BatchDate
        {
            get { return txtBatchDate.Text.Trim(); }
            set { txtBatchDate.Text = value; }
        }

        #endregion

        # region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                if (!IsPostBack)
                {
                    string path = string.Empty;
                    path = _objCommonMethods.GetPagePath(this.Request.Url);
                    if (_objCommonMethods.IsAccess(path))
                    {
                        //BindCashiers();
                        string BU_ID = Convert.ToString(Session[UMS_Resource.SESSION_USER_BUID]);
                        _objCommonMethods.BindOfficeByBU_ID(ddlCashOffice,BU_ID,true,false,UMS_Resource.DROPDOWN_SELECT);
                        _objCommonMethods.BindPaymentModes(ddlPaymentMode, true, UMS_Resource.DROPDOWN_SELECT, false);

                        if (Request.QueryString["bno"] != null)
                        {
                            BindBatchDetails();
                            ddlPaymentMode.SelectedIndex = ddlPaymentMode.Items.IndexOf(ddlPaymentMode.Items.FindByValue(_objiDsignBAL.Decrypt(Request.QueryString["pm"])));
                            ddlPaymentMode.Enabled = false;
                        }
                        else
                            BatchDate = "";
                    }
                    else
                    {
                        Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    }
                }
            }
            else
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void ddlCashOffice_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlCashOffice.SelectedIndex > 0)
                {
                    ddlCashier.Enabled = true;
                    _objCommonMethods.BindCashiers(ddlCashier, Convert.ToInt32(ddlCashOffice.SelectedValue), UMS_Resource.DROPDOWN_SELECT, true);
                }
                else
                {
                    ddlCashier.SelectedIndex = 0;
                    ddlCashier.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                BillingBE _objBillingBe = new BillingBE();
                btnDeleteBatch.Enabled = btnCancel.Enabled = btnContinue.Enabled = true;
                bool IsApproval = _objCommonMethods.IsApproval(Resource.FEATURE_PAYMENT_ENTRY);
                _objBillingBe.BatchNo = Convert.ToInt32(txtBatchNo.Text);
                _objBillingBe.BatchName = txtBatchNo.Text;
                _objBillingBe.BatchDate = _objCommonMethods.Convert_MMDDYY(txtBatchDate.Text);
                _objBillingBe.OfficeId = CashOffice;// Convert.ToInt32(ddlCashOffice.SelectedValue);
                _objBillingBe.CashierId = Cashier;// ddlCashier.SelectedValue;
                _objBillingBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objBillingBe.BatchTotal = decimal.Parse(txtBatchTotal.Text.Replace(",", string.Empty));// Added Replace function By Karteek on 25-03-2015
                _objBillingBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();

                if (IsApproval)
                    _objBillingBe.Flag = Convert.ToInt32(Resources.Resource.FLAG_TEMP);
                else
                    _objBillingBe.Flag = Convert.ToInt32(Resources.Resource.INSERT_FINAL);

                _objBillingBe = _objIdsignBAL.DeserializeFromXml<BillingBE>(objBillingBAL.InsertBatchEntry(_objBillingBe, Statement.Insert));

                if (_objBillingBe.IsSuccess == 1)
                {
                    Response.Redirect(UMS_Resource.PAYMENTS_PAGE + "?bid=" + _objiDsignBAL.Encrypt(_objBillingBe.BatchId.ToString()) + "&bno=" + _objiDsignBAL.Encrypt(txtBatchNo.Text) + "&c=" + _objiDsignBAL.Encrypt(ddlCashier.SelectedValue) + "&co=" + _objiDsignBAL.Encrypt(ddlCashOffice.SelectedValue) + "&pm=" + _objiDsignBAL.Encrypt(ddlPaymentMode.SelectedValue) + "&dt=" + _objiDsignBAL.Encrypt(txtBatchDate.Text), false);
                }
                else
                    if (_objBillingBe.BatchStatusId == Convert.ToInt32(GlobalConstants.BATCH_FINISHED_STATUS_ID))
                    {
                        mpeBatchFinished.Show();
                    }
                    else
                        if (_objBillingBe.BatchStatusId == Convert.ToInt32(GlobalConstants.BATCH_PENDING_STATUS_ID))
                        {
                            //if (_objBillingBe.BatchPendingAmount > 0)
                            //{
                            lblBatchDate.Text = _objCommonMethods.DateFormate(_objBillingBe.BatchDate);
                            lblBatchTotal.Text = (_objBillingBe.BatchTotal == 0) ? "NA" : _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(_objBillingBe.BatchTotal.ToString()), 2, Constants.MILLION_Format).ToString();
                            lblBatchPending.Text = (_objBillingBe.BatchTotal == 0) ? "NA" : _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(_objBillingBe.BatchPendingAmount.ToString()), 2, Constants.MILLION_Format).ToString();
                            lblBatchId.Text = _objBillingBe.BatchId.ToString();
                            mpeBatchPending.Show();
                            //}
                            //else
                            //{
                            //lblBatchDateFin.Text = _objCommonMethods.DateFormate(_objBillingBe.BatchDate);
                            //lblBatchTotalFin.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(_objBillingBe.BatchTotal.ToString()), 2, Constants.MILLION_Format).ToString();
                            //mpeBatchEnd.Show();
                            //}
                        }
                        else
                        {
                            Message(Resource.BATCHENTRY_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                        }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            Response.Redirect(UMS_Resource.PAYMENTS_PAGE + "?bid=" + _objiDsignBAL.Encrypt(lblBatchId.Text) + "&bno=" + _objiDsignBAL.Encrypt(txtBatchNo.Text) + "&c=" + _objiDsignBAL.Encrypt(ddlCashier.SelectedValue) + "&co=" + _objiDsignBAL.Encrypt(ddlCashOffice.SelectedValue) + "&pm=" + _objiDsignBAL.Encrypt(ddlPaymentMode.SelectedValue) + "&dt=" + _objiDsignBAL.Encrypt(txtBatchDate.Text), false);
        }

        protected void btnDeleteBatch_Click(object sender, EventArgs e)
        {
            btnDeleteBatch.Enabled = false;
            btnCancel.Enabled = false;
            btnContinue.Enabled = false;
            mpeDeleteBatchNoti.Show();
        }

        protected void btnDelBatchOk_Click(object sender, EventArgs e)
        {
            MastersBE _objMastersBE = new MastersBE();
            _objMastersBE.BatchNo = Convert.ToInt32(txtBatchNo.Text);
            _objMastersBE = _objiDsignBAL.DeserializeFromXml<MastersBE>(_objMastersBAL.DeleteBatch(_objMastersBE, Statement.Delete));
            Message(Resource.BATCH_DELETED, UMS_Resource.MESSAGETYPE_SUCCESS);
        }

        //protected void txtBatchNo_TextChanged(object sender, EventArgs e)
        //{
        //    if (txtBatchNo.Text != "" && getBatchDetails() == true)
        //    {
        //        BatchEntryBe objBatchBe = new BatchEntryBe();
        //        objBatchBe.BatchNo = Convert.ToInt32(txtBatchNo.Text);
        //        objBatchBe.BatchDate = _objCommonMethods.Convert_MMDDYY(txtBatchDate.Text);
        //        objBatchBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
        //        objBatchBe = _objIdsignBAL.DeserializeFromXml<BatchEntryBe>(_objMastersBAL.BatchEntry(objBatchBe, Statement.Check));
        //        if (objBatchBe.PaymentModeId > 0)
        //        {
        //            ddlPaymentMode.SelectedIndex = ddlPaymentMode.Items.IndexOf(ddlPaymentMode.Items.FindByValue(objBatchBe.PaymentModeId.ToString()));
        //            ddlPaymentMode.Enabled = false;
        //        }
        //        else
        //        {
        //            ddlPaymentMode.Enabled = true;
        //        }
        //    }
        //}

        //protected void btnGetBatchDetails_Click(object sender, EventArgs e)
        //{
        //    //getBatchDetails();
        //    //try
        //    //{
        //    BillingBE objBillingBe = new BillingBE();
        //    if (txtBatchNo.Text != "" && txtBatchDate.Text != "")
        //    {

        //        bool IsApproval = _objCommonMethods.IsApproval(Resource.FEATURE_PAYMENT_ENTRY);

        //        objBillingBe.BatchNo = string.IsNullOrEmpty(txtBatchNo.Text.Trim()) ? 0 : Convert.ToInt32(txtBatchNo.Text.Trim());
        //        objBillingBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
        //        objBillingBe.BatchDate = _objCommonMethods.Convert_MMDDYY(txtBatchDate.Text);
        //        if (IsApproval)
        //            objBillingBe.Flag = Convert.ToInt32(Resources.Resource.FLAG_TEMP);
        //        else
        //            objBillingBe.Flag = Convert.ToInt32(Resources.Resource.INSERT_FINAL);
        //        XmlDocument xml = objBillingBAL.Get(objBillingBe, ReturnType.Get);
        //        if (xml != null && !string.IsNullOrEmpty(xml.InnerXml))
        //        {
        //            objBillingBe = _objIdsignBAL.DeserializeFromXml<BillingBE>(xml);
        //            BatchDate = objBillingBe.BatchDate;

        //            // Added Curency Format function By Karteek on 25-03-2015
        //            //txtBatchTotal.Text = Math.Round(Convert.ToDecimal(objBillingBe.BatchTotal)).ToString();
        //            txtBatchTotal.Text = _objCommonMethods.GetCurrencyFormat(objBillingBe.BatchTotal, 2, Constants.MILLION_Format);

        //            ddlCashier.SelectedIndex = ddlCashier.Items.IndexOf(ddlCashier.Items.FindByValue(objBillingBe.CashierId));
        //            ddlCashOffice.SelectedIndex = ddlCashOffice.Items.IndexOf(ddlCashOffice.Items.FindByValue(objBillingBe.OfficeId.ToString()));
        //        }
        //        else
        //        {
        //            txtBatchTotal.Text = string.Empty;
        //            ddlCashier.SelectedIndex = 0;
        //            ddlCashOffice.SelectedIndex = 0;
        //        }
        //        //txtBatchNo_TextChanged(null, null);
        //        //txtBatchDate_TextChanged(null, null);

        //    }
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //    //    try
        //    //    {
        //    //        _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //    //    }
        //    //    catch (Exception logex)
        //    //    {

        //    //    }
        //    //}
        //}

        //protected void txtBatchDate_TextChanged(object sender, EventArgs e)
        //{
        //    if (txtBatchNo.Text != "" && getBatchDetails() == true)
        //    {
        //        BatchEntryBe objBatchBe = new BatchEntryBe();
        //        objBatchBe.BatchNo = Convert.ToInt32(txtBatchNo.Text);
        //        objBatchBe.BatchDate = _objCommonMethods.Convert_MMDDYY(txtBatchDate.Text);
        //        objBatchBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
        //        objBatchBe = _objIdsignBAL.DeserializeFromXml<BatchEntryBe>(_objMastersBAL.BatchEntry(objBatchBe, Statement.Check));
        //        if (objBatchBe.PaymentModeId > 0)
        //        {
        //            ddlPaymentMode.SelectedIndex = ddlPaymentMode.Items.IndexOf(ddlPaymentMode.Items.FindByValue(objBatchBe.PaymentModeId.ToString()));
        //            ddlPaymentMode.Enabled = false;
        //        }
        //        else
        //        {
        //            ddlPaymentMode.Enabled = true;
        //        }
        //    }
        //}

        #endregion

        # region Methods

        private void BindCashiers()
        {
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
            {
                BillingBE objBillingBe = new BillingBE();
                objBillingBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                GlobalMessagesListBE objBillingListBe = _objiDsignBAL.DeserializeFromXml<GlobalMessagesListBE>(objBillingBAL.GetCashierDetailsBAL(objBillingBe));
                _objiDsignBAL.FillDropDownList(_objiDsignBAL.ConvertListToDataSet<BillingBE>(objBillingListBe.CustomerReading), ddlCashier, "Name", "UserId", UMS_Resource.DROPDOWN_SELECT, false);
            }
            else
                _objCommonMethods.BindCashiers(ddlCashier, true);
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void BindBatchDetails()
        {
            BillingBE objBillingBe = new BillingBE();
            bool IsApproval = _objCommonMethods.IsApproval(Resource.FEATURE_PAYMENT_ENTRY);

            objBillingBe.BatchNo = Convert.ToInt32(_objiDsignBAL.Decrypt(Request.QueryString["bno"]));
            objBillingBe.BatchDate = _objCommonMethods.Convert_MMDDYY(_objiDsignBAL.Decrypt(Request.QueryString["dt"]));
            objBillingBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();

            if (IsApproval)
                objBillingBe.Flag = Convert.ToInt32(Resources.Resource.FLAG_TEMP);
            else
                objBillingBe.Flag = Convert.ToInt32(Resources.Resource.INSERT_FINAL);

            objBillingBe = _objIdsignBAL.DeserializeFromXml<BillingBE>(objBillingBAL.Get(objBillingBe, ReturnType.Get));

            if (objBillingBe != null)
            {
                BatchDate = objBillingBe.BatchDate;
                txtBatchNo.Text = objBillingBe.BatchNo.ToString();

                // Added Curency Format function By Karteek on 25-03-2015
                txtBatchTotal.Text = _objCommonMethods.GetCurrencyFormat(objBillingBe.BatchTotal, 2, Constants.MILLION_Format);

                ddlCashier.SelectedValue = objBillingBe.CashierId;
                CashOffice = objBillingBe.OfficeId;
            }
        }

        //public bool getBatchDetails()
        //{
        //    bool retrn = true;
        //    try
        //    {
        //        if (txtBatchNo.Text != "" && txtBatchDate.Text != "")
        //        {
        //            BillingBE objBillingBe = new BillingBE();
        //            bool IsApproval = _objCommonMethods.IsApproval(Resource.FEATURE_PAYMENT_ENTRY);

        //            objBillingBe.BatchNo = string.IsNullOrEmpty(txtBatchNo.Text.Trim()) ? 0 : Convert.ToInt32(txtBatchNo.Text.Trim());
        //            objBillingBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
        //            objBillingBe.BatchDate = _objCommonMethods.Convert_MMDDYY(txtBatchDate.Text);
        //            if (IsApproval)
        //                objBillingBe.Flag = Convert.ToInt32(Resources.Resource.FLAG_TEMP);
        //            else
        //                objBillingBe.Flag = Convert.ToInt32(Resources.Resource.INSERT_FINAL);
        //            XmlDocument xml = objBillingBAL.Get(objBillingBe, ReturnType.Get);
        //            if (xml != null && !string.IsNullOrEmpty(xml.InnerXml))
        //            {
        //                objBillingBe = _objIdsignBAL.DeserializeFromXml<BillingBE>(xml);
        //                BatchDate = objBillingBe.BatchDate;

        //                //txtBatchTotal.Text = "4852144";// Commented By Karteek on 25-03-2015 here the text box filling with that given value so
        //                //txtBatchTotal.Text = Math.Round(Convert.ToDecimal(objBillingBe.BatchTotal)).ToString();

        //                // Added Curency Format function By Karteek on 25-03-2015
        //                //txtBatchTotal.Text = Math.Round(Convert.ToDecimal(objBillingBe.BatchTotal)).ToString();
        //                txtBatchTotal.Text = _objCommonMethods.GetCurrencyFormat(objBillingBe.BatchTotal, 2, Constants.MILLION_Format);

        //                ddlCashier.SelectedIndex = ddlCashier.Items.IndexOf(ddlCashier.Items.FindByValue(objBillingBe.CashierId));
        //                ddlCashOffice.SelectedIndex = ddlCashOffice.Items.IndexOf(ddlCashOffice.Items.FindByValue(objBillingBe.OfficeId.ToString()));
        //            }
        //            else
        //            {
        //                txtBatchTotal.Text = string.Empty;
        //                ddlCashier.SelectedIndex = 0;
        //                ddlCashOffice.SelectedIndex = 0;
        //                retrn = false;
        //            }
        //            //txtBatchNo_TextChanged(null, null);
        //            //txtBatchDate_TextChanged(null, null);

        //        }
        //        else
        //            retrn = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        retrn = false;
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //    return retrn;
        //}

        #endregion

        #region Culture

        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

    }
}