﻿<%@ Page Title="::Bill Adjustments::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="BillAdjustment.aspx.cs" Inherits="UMS_Nigeria.Billing.BillAdjustment" %>

<%@ Register Src="../UserControls/Authentication.ascx" TagName="Authentication" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="consumer_total">
        <asp:HiddenField ID="hfDecimals" runat="server" Value="0" />
        <asp:HiddenField ID="hfReadType" runat="server" Value="0" />
        <asp:HiddenField ID="hfAccountNo" runat="server" Value="0" />
        <asp:HiddenField ID="hfBillNo" runat="server" />
        <asp:Panel ID="pnlMessage" runat="server">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <div class="out-bor">
            <div class="text_total">
                <div class="text-heading">
                    <asp:Literal ID="litSearchCustomer" runat="server" Text="<%$ Resources:Resource, BILL_ADJUSTMENT%>"></asp:Literal>
                </div>
                <div class="star_text" style="margin-right: 60px;">
                    <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="dot-line">
            </div>
            <div class="previous">
                <asp:LinkButton ID="lnkPrevious" runat="server" Text='<%$ Resources:Resource, PREVIOUS %>'
                    OnClick="lnkPrevious_Click"></asp:LinkButton>
            </div>
            <div id="divAdjustmentMain" runat="server">
                <div id="divBatchDetails" runat="server">
                    <div class="out-bor_a" style="margin-left: 25px; margin-top: 21px;">
                        <div class="inner-sec">
                            <div class="heading" style="padding-left: 21px; padding-top: 9px;">
                                <asp:Literal ID="Literal213" runat="server" Text="<%$ Resources:Resource, BATCH_DETAILS%>"></asp:Literal>
                                <asp:Label ID="lblBatchDate" Style="float: right; margin-left: 12px; margin-right: 26px;"
                                    runat="server" Text=""></asp:Label>
                            </div>
                            <div class="out-bor_aTwo">
                                <table class="customerdetailsTamle">
                                    <tr>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal70" runat="server" Text="<%$ Resources:Resource, BATCH_NO%>"> </asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label ID="lblBatchNo" runat="server" Text="--"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal71" runat="server" Text="No of adjustments done"> </asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label ID="lblTotalAdjustments" runat="server" Text="--"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal72" runat="server" Text="Reason"> </asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label ID="lblReason" runat="server" Text="--"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal73" runat="server" Text="<%$ Resources:Resource, BATCH_TOTAL%>"> </asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label ID="lblBatchTotal" runat="server" Text="--"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal74" runat="server" Text="<%$ Resources:Resource, PENDING%>"> </asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label ID="lblBatchPending" runat="server" Text="--"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal2" runat="server" Text="Adjustment Type"> </asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label ID="lblAdjsutmentType" runat="server" Text="--"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                </div>
                <div class="billadd_wid_total">
                    <div class="billadd_wid1">
                        <div class="consumer_feild">
                            <div class="consumer_feild">
                                <div class="consume_name" style="width: 403px; margin-left: 19px;">
                                    <asp:Literal ID="Literal1" runat="server" Text="Global Account No / Old Account No"></asp:Literal>
                                    <span class="span_star">*</span>
                                </div>
                                <div class="consume_input c_left" style="margin-left: 8px;">
                                    <asp:TextBox runat="server" ID="txtccountNo" CssClass="text-box" placeholder="Global / Old Account No."
                                        Style="margin-top: -2px; margin-left: 12px; background: linear-gradient(to bottom, #f9fbf9 0%, #dff0d8 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
                                        border: 1px solid #d6e9c6; box-shadow: none" onkeypress="GetAccountDetails(event)"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="ftbAccountNo" runat="server" TargetControlID="txtccountNo"
                                        FilterMode="ValidChars" FilterType="Numbers">
                                    </asp:FilteredTextBoxExtender>
                                </div>
                                <div>
                                    <asp:Button ID="btnGo" Style="margin-left: 20px;" Text="<%$ Resources:Resource, GET_CUSTOMER_DETAILS%>"
                                        CssClass="box_s" runat="server" OnClientClick="return Validate();" OnClick="btnGo_Click" />
                                </div>
                                <%-- <div class="clear pad_2">
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, BILLNUMBER%>"></asp:Literal>
                            </div>
                            <div class="consume_input c_left">
                                <asp:TextBox runat="server" ID="txtDocNo" onkeypress="GetAccountDetails(event)"></asp:TextBox>
                                <span id="spnDocNo" class="spancolor"></span>
                            </div>--%>
                            </div>
                        </div>
                        <div class="clear pad_2">
                        </div>
                        <div class="consumer_feild">
                            <div class="consumer_feild">
                                <%-- <div class="consume_name">
                                <asp:Literal ID="Literal59" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                            </div>
                            <div class="consume_input c_left" style="width: 217px;">
                                <asp:TextBox runat="server" ID="txtOldAccountNO" onkeypress="GetAccountDetails(event)"></asp:TextBox>
                            </div>
                            <div class="clear pad_2">
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal60" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Literal>
                            </div>
                            <div class="consume_input c_left">
                                <asp:TextBox runat="server" ID="txtMeterNo" onkeypress="GetAccountDetails(event)"></asp:TextBox>
                                <span id="Span1" class="spancolor"></span>
                            </div>
                            <div class="clear pad_2">
                            </div>
                            <div class="consume_name">
                            </div>--%>
                                <div class="fltl c_left" style="margin-left: 5px;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="billadd_wid2 adjustWidth">
                        <div class="consumer_total" runat="server" id="divBatchDetailsGrid" visible="true">
                            <h3 style="text-decoration: none; margin-left: -10px;">
                                <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, ADJUST_BATCH_DETAILS%>"> </asp:Label>
                            </h3>
                            <div class="grid billgrid" style="overflow-x: ">
                                <asp:GridView ID="grdAdjustmentDetails" runat="server" AutoGenerateColumns="False"
                                    AlternatingRowStyle-CssClass="color" OnRowDataBound="grdAdjustmentDetails_RowDataBound">
                                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" BorderStyle="Outset" />
                                    <EmptyDataTemplate>
                                        There is no data.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:BoundField HeaderText="Global Account No." DataField="AccountNo" />
                                        <asp:TemplateField HeaderText="Total Amount Adjusted" ItemStyle-HorizontalAlign="Right">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmountEffected" runat="server" Text='<%#Eval("AmountEffected") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total Amount Adjusted With Tax" ItemStyle-HorizontalAlign="Right">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmountEffectedWithTax" runat="server" Text='<%#Eval("TotalAmountEffected") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Units Adjusted" DataField="AdjustedUnits" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField HeaderText="Adjustment Type" DataField="AdjustmentName" />
                                        <asp:BoundField HeaderText="Reason" DataField="Remarks" />
                                        <asp:TemplateField HeaderText="Adjusted Date" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCreatedDate" runat="server" Text='<%#Eval("CreatedDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div id="divPendingBills" runat="server" visible="false">
                    <div class="out-bor_a" style="margin-left: 25px; margin-top: 21px;">
                        <div class="inner-sec">
                            <div class="heading" style="padding-left: 21px; padding-top: 9px;">
                                <asp:Literal ID="Literal69" runat="server" Text="<%$ Resources:Resource, CUSTOMER_DETAILS%>"></asp:Literal>
                            </div>
                            <div class="out-bor_aTwo">
                                <table class="customerdetailsTamle">
                                    <tr>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, CUSTOMER_NAME%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label runat="server" ID="lblCustomerName"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label runat="server" ID="lblAccNoAndGlobalAccNo"></asp:Label>
                                            <div style="display: none;">
                                                <asp:Label runat="server" ID="lblAccountno"></asp:Label>
                                            </div>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal61" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label runat="server" ID="lblOldAccountNo"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, DOCUMENT_NO%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label runat="server" ID="lblDocumentNo"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label runat="server" ID="lblTariff"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal62" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label runat="server" ID="lblMeterNo"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal78" runat="server" Text="Business Unit"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label runat="server" ID="lblBuid"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal79" runat="server" Text="Service Unit"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label runat="server" ID="lblSuid"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal80" runat="server" Text="Service Center"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label runat="server" ID="lblScid"></asp:Label>
                                        </td>
                                </table>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="inner-sec">
                            <div class="heading" style="padding-left: 21px; padding-top: 9px;">
                                <asp:Literal ID="Literal83" runat="server" Text="Last 5 Adjustment Transaction details"></asp:Literal>
                            </div>
                            <div id="divAdditionalChargesList" runat="server" class="grid_tb" style="padding-left: 26px;
                                padding-top: 10px;">
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="billcustomert" style="margin-left: 25px; width: 94.5%;">
                        <div class="pendingbills" style="width: 323px;">
                            <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, BILLS_PENDING%>"></asp:Literal><span><asp:Label
                                runat="server" ID="lblTotalBills"></asp:Label>
                                <asp:Literal runat="server" ID="litCustomerUniqueNo" Visible="false"></asp:Literal>
                            </span>
                        </div>
                        <div class="pendingbills" style="width: 323px;">
                            <asp:Literal ID="Literal11" runat="server" Text="Total Outstanding Amount"></asp:Literal>
                            <span>
                                <asp:Label runat="server" ID="lblTotalAmountPending"></asp:Label></span></div>
                        <div class="pendingbills" style="margin-right: 0px;">
                            <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:Resource, LAST_AMOUNTPAID%>"></asp:Literal>
                            <span>
                                <asp:Label runat="server" ID="lblLastAmountPaid"></asp:Label></span></div>
                        <div class="clear pad_10">
                        </div>
                        <div class="pendingbills" style="width: 323px;">
                            <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, LAST_BILLGENERATEDDATE%>"></asp:Literal>
                            <span>
                                <asp:Label runat="server" ID="lblLastBillDate"></asp:Label></span></div>
                        <div class="pendingbills" style="width: 323px;">
                            <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:Resource, LAST_PAIDDATE%>"></asp:Literal>
                            <span>
                                <asp:Label runat="server" ID="lblLastPaidDate"></asp:Label></span></div>
                        <div class="clear pad_10">
                        </div>
                        <asp:DataList ID="dtlBills" runat="server" Style="width: 400px;" RepeatDirection="Horizontal"
                            RepeatColumns="4" OnItemDataBound="dtlBills_ItemDataBound" OnItemCommand="dtlBills_ItemCommand">
                            <ItemTemplate>
                                <div class="prev_total routwise">
                                    <div class="billclose">
                                        <asp:LinkButton ID="lnkBtnDetails" runat="server" Visible="false" CommandName="getBADetails"
                                            Font-Bold="true" CommandArgument='<%# Eval("BAID") %>' Text="View" ToolTip="<%$ Resources:Resource, GET_ADJUSTMENT_INFO_TOOL_TIP%>"></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnEdit" runat="server" CommandArgument='<%# Eval("BillNo") %>'
                                            ToolTip="<%$ Resources:Resource, CLICK_FOR_ADJUSTMENT %>" CommandName='<%# Eval("CustomerReadingId") %>'>
                                 <img src="../images/Edit.png" alt="Edit" /></asp:LinkButton>
                                        <asp:HiddenField ID="hfReadCodeId" runat="server" Value='<%# Eval("ReadCodeId") %>' />
                                        <asp:HiddenField ID="hfIsLatestBill" runat="server" Value='<%# Eval("IsSuccess") %>' />
                                    </div>
                                    <ul>
                                        <div style="background: #fafafa; width: 93%;">
                                            <li class="prevleft fltl">
                                                <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:Resource, BILLNUMBER%>"></asp:Literal></li>
                                            <br />
                                            <asp:Label runat="server" ID="lblBillno" Text='<%# Eval("BillNo") %>'></asp:Label>
                                            <li class=" fltl"></li>
                                            <li class=" fltl">
                                                <asp:HiddenField ID="hfIsAdjusted" runat="server" Value='<%# Eval("IsAdjested") %>' />
                                                <asp:LinkButton runat="server" ID="litBillMonth" Visible="false" Text='<%# Eval("BillMonth") %>'></asp:LinkButton>
                                                <asp:LinkButton runat="server" ID="litBillYear" Visible="false" Text='<%# Eval("BillYear") %>'></asp:LinkButton>
                                                <asp:Literal runat="server" ID="litCustomerBillID" Visible="false" Text='<%# Eval("CustomerBillID") %>'></asp:Literal>
                                                <asp:LinkButton runat="server" ID="litCustomerID" Visible="false" Text='<%# Eval("CustomerID") %>'></asp:LinkButton>
                                            </li>
                                        </div>
                                        <div>
                                            <li class="prevleft fltl">
                                                <asp:Literal ID="Literal82" runat="server" Text="Bill Type"></asp:Literal></li>
                                            <li class=" fltl">:</li>
                                            <li class=" fltl">
                                                <asp:Label runat="server" ID="lblBillType" Text='<%#Eval("ReadTypeIndication") %>'></asp:Label></li>
                                        </div>
                                        <div>
                                            <li class="prevleft fltl">
                                                <asp:Literal ID="Literal63" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal></li>
                                            <li class=" fltl">:</li>
                                            <li class=" fltl">
                                                <asp:Label runat="server" ID="lblAccountNoDsply" Text='<%# Eval("AccountNo") %>'></asp:Label></li>
                                        </div>
                                        <div>
                                            <li class="prevleft fltl">
                                                <asp:Literal ID="Literal64" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal></li>
                                            <li class=" fltl">:</li>
                                            <li class=" fltl">
                                                <asp:Label runat="server" ID="lblOldAccountNoDsply" Text='<%# Eval("OldAccountNo") %>'></asp:Label></li>
                                        </div>
                                        <%--Commented By Raja-ID065 Start--%>
                                        <%--<div>
                                        <li class="prevleft fltl">
                                            <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, PREVIOUS_READING%>"></asp:Literal></li><li
                                                class=" fltl">:</li>
                                        <li class=" fltl">
                                            <asp:Label runat="server" ID="lblPreviousReading" Text='<%# Eval("PreviousReading") %>'></asp:Label>
                                        </li>
                                    </div>
                                    <div>
                                        <li class="prevleft fltl">
                                            <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, CURRENT_READING%>"></asp:Literal></li><li
                                                class=" fltl">:</li>
                                        <li class=" fltl">
                                            <asp:Label runat="server" ID="lblPresentReading" Text='<%# Eval("PresentReading") %>'></asp:Label></li>
                                    </div>
                                    <div>
                                        <li class="prevleft fltl">
                                            <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:Resource, CONSUMPTION%>"></asp:Literal>
                                        </li>
                                        <li class=" fltl">:</li><li class=" fltl">
                                            <asp:Label runat="server" ID="lblConsumption" Text='<%# Eval("Consumption") %>'></asp:Label></li>
                                    </div>
                                    <div>
                                        <li class="prevleft fltl">
                                            <asp:Literal ID="Literal31" runat="server" Text="<%$ Resources:Resource, ENERGYCHARGES%>"></asp:Literal></li><li
                                                class=" fltl">:</li>
                                        <li class=" fltl">
                                            <asp:Label runat="server" ID="lblNetEnergyCharges" Text='<%# Eval("NetEnergyCharges") %>'></asp:Label>
                                        </li>
                                    </div>
                                    <asp:Repeater runat="server" ID="rptrAdditionalCharges" OnItemDataBound="rptrAdditionalCharges_ItemDataBound">
                                        <ItemTemplate>
                                            <div>
                                                <li class="prevleft fltl">
                                                    <asp:Label ID="lblChargeName" runat="server" Text='<%# Eval("ChargeName") %>'></asp:Label>
                                                </li>
                                                <li class=" fltl">:</li>
                                                <li class=" fltl">
                                                    <asp:Label runat="server" ID="lblAmount" Text='<%# Eval("Amount") %>'></asp:Label></li>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <div>
                                        <li class="prevleft fltl">
                                            <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:Resource, TOTAL_BILL%>"></asp:Literal></li>
                                        <li class=" fltl">:</li>
                                        <li class=" fltl">
                                            <asp:Label runat="server" ID="Label4" Text='<%# Eval("TotalBillAmount") %>'></asp:Label>
                                        </li>
                                    </div>
                                    <div>
                                        <li class="prevleft fltl">
                                            <asp:Literal ID="Literal22" runat="server" Text="<%$ Resources:Resource, TAX%>"></asp:Literal></li><li
                                                class=" fltl">:</li>
                                        <li class=" fltl">
                                            <asp:Label runat="server" ID="Label5" Text='<%# Eval("Vat") %>'></asp:Label>
                                        </li>
                                    </div>--%>
                                        <%--Commented By Raja-ID065 End--%>
                                        <%--Raja-ID065 End--%>
                                        <div>
                                            <li class="prevleft fltl">
                                                <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, PREVIOUS_READING%>"></asp:Literal></li><li
                                                    class=" fltl">:</li>
                                            <li class=" fltl">
                                                <asp:Label runat="server" ID="lblPreviousReading" Text='<%# Eval("PreviousReading") %>'></asp:Label>
                                            </li>
                                        </div>
                                        <div>
                                            <li class="prevleft fltl">
                                                <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, CURRENT_READING%>"></asp:Literal></li><li
                                                    class=" fltl">:</li>
                                            <li class=" fltl">
                                                <asp:Label runat="server" ID="lblPresentReading" Text='<%# Eval("PresentReading") %>'></asp:Label></li>
                                        </div>
                                        <div>
                                            <li class="prevleft fltl">
                                                <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:Resource, CONSUMPTION%>"></asp:Literal>
                                            </li>
                                            <li class=" fltl">:</li><li class=" fltl">
                                                <asp:Label runat="server" ID="lblConsumption" Text='<%# Math.Round(Convert.ToDouble(Eval("Consumption")),0) %>'></asp:Label></li>
                                        </div>
                                        <div>
                                            <li class="prevleft fltl">
                                                <asp:Literal ID="Literal31" runat="server" Text="<%$ Resources:Resource, ENERGYCHARGES%>"></asp:Literal></li><li
                                                    class=" fltl">:</li>
                                            <li class=" fltl">
                                                <asp:Label runat="server" ID="lblNetEnergyCharges" Text='<%# Eval("NetEnergyCharges") %>'></asp:Label>
                                            </li>
                                        </div>
                                        <asp:Repeater runat="server" ID="rptrAdditionalCharges" OnItemDataBound="rptrAdditionalCharges_ItemDataBound">
                                            <%--Raja-ID065 For Additional Charges--%>
                                            <ItemTemplate>
                                                <div>
                                                    <li class="prevleft fltl">
                                                        <asp:Label ID="lblChargeName" runat="server" Text='<%# Eval("ChargeName") %>'></asp:Label>
                                                    </li>
                                                    <li class=" fltl">:</li>
                                                    <li class=" fltl">
                                                        <asp:Label runat="server" ID="lblAmount" Text='<%# Eval("Amount") %>'></asp:Label></li>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <div>
                                            <li class="prevleft fltl">
                                                <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:Resource, BILL_AMT%>"></asp:Literal></li>
                                            <li class=" fltl">:</li>
                                            <li class=" fltl">
                                                <asp:Label runat="server" ID="lblTotalAmt" Text='<%# Eval("TotalBillAmount") %>'></asp:Label>
                                            </li>
                                        </div>
                                        <div>
                                            <li class="prevleft fltl">
                                                <asp:Literal ID="Literal22" runat="server" Text="<%$ Resources:Resource, TAX%>"></asp:Literal></li><li
                                                    class=" fltl">:</li>
                                            <li class=" fltl">
                                                <asp:Label runat="server" ID="lblTax" Text='<%# Eval("Vat") %>'></asp:Label>
                                            </li>
                                        </div>
                                        <div style="background: none repeat scroll 0 0 #fff;">
                                            <%--Raja-ID065 For Grand Total--%>
                                            <li class="prevleft fltl" style="color: green; font-size: 14px; font-weight: bold;">
                                                <asp:Literal ID="Literal9" runat="server" Text="Total Bill Amount With VAT"></asp:Literal></li><li
                                                    class=" fltl">:</li>
                                            <li class=" fltl" style="color: green; font-size: 14px; font-weight: bold;">
                                                <asp:Label runat="server" ID="lblGT" Text='<%# Eval("TotalBillAmountWithTax") %>'></asp:Label>
                                            </li>
                                        </div>
                                        <div>
                                            <li class="prevleft fltl" style="color: green; font-size: 14px; font-weight: bold;">
                                                <asp:Literal ID="Literal6" runat="server" Text="Net Arrears"></asp:Literal></li><li
                                                    class=" fltl">:</li>
                                            <li class=" fltl" style="color: green; font-size: 14px; font-weight: bold;">
                                                <asp:Label ID="lblPaidAmount" runat="server" Text='<%# Eval("NetArrears") %>'></asp:Label>
                                            </li>
                                        </div>
                                        <div style="background: none repeat scroll 0 0 #cfcfcf;">
                                            <li class="prevleft fltl">
                                                <asp:Literal ID="Literal7" runat="server" Text="Total Bill Amount with Arrears"></asp:Literal></li><li
                                                    class=" fltl">:</li>
                                            <li class=" fltl">
                                                <asp:Label runat="server" ID="lblDueAmount" Text='<%# Eval("TotalBillAmountWithArrears") %>'></asp:Label>
                                            </li>
                                        </div>
                                        <%--Raja-ID065 End--%>
                                    </ul>
                                    <br />
                                    <div id="divAdj" runat="server" visible="false">
                                        <asp:Literal runat="server" ID="lblApprovalStatusId" Visible="false" Text='<%# Eval("ApprovalStatusId") %>'></asp:Literal>
                                        <asp:Label ID="lblAdjMsg" runat="server" Text="<%$Resources:Resource, BILL_ADJUSTMENT_REQ_SENT %>"></asp:Label><br />
                                        <asp:Label ID="Lable222" runat="server" Text="<%$ Resources:Resource, CLICK%>"></asp:Label>
                                        <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, FOR_DETAILS%>"></asp:Label>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:DataList runat="server" ID="dtDirectCustomer" RepeatDirection="Horizontal" RepeatColumns="4"
                            OnItemDataBound="dtDirectCustomer_ItemDataBound">
                            <ItemTemplate>
                                <div class="prev_total routwise">
                                    <ul>
                                        <li class="prevleft fltl">
                                            <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:Resource, BILLNUMBER%>"></asp:Literal></li><li
                                                class=" fltl">:</li>
                                        <li class=" fltl">
                                            <asp:Label runat="server" ID="lblBillno" Text='<%# Eval("BillNo") %>'></asp:Label>
                                            <asp:Literal runat="server" ID="litCustomerBillID" Visible="false" Text='<%# Eval("CustomerBillID") %>'></asp:Literal>
                                        </li>
                                        <li class="clear pad_5"></li>
                                        <li class="prevleft fltl">
                                            <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, ENERGYCHARGES%>"></asp:Literal></li><li
                                                class=" fltl">:</li>
                                        <li class=" fltl">
                                            <asp:Label runat="server" ID="lblDirectNetEnergyCharges" Text='<%# Eval("NetEnergyCharges") %>'></asp:Label>
                                        </li>
                                        <li class="clear pad_5"></li>
                                        <asp:Repeater runat="server" ID="rptrDirectCustomer" OnItemDataBound="rptrDirectCustomer_ItemDataBound">
                                            <ItemTemplate>
                                                <li class="prevleft fltl">
                                                    <asp:Label ID="lblDirectChargeName" runat="server" Text='<%# Eval("ChargeName") %>'></asp:Label>
                                                </li>
                                                <li class=" fltl">:</li><li class=" fltl">
                                                    <asp:Label runat="server" ID="lblDirectAmount" Text='<%# Eval("Amount") %>'></asp:Label></li>
                                                <li class="clear pad_5"></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <li class="clear pad_5"></li>
                                        <li class="prevleft fltl">
                                            <asp:Literal ID="Literal29" runat="server" Text="<%$ Resources:Resource, REMARKS%>"></asp:Literal>
                                        </li>
                                        <li class=" fltl">:</li><li class=" fltl">
                                            <asp:Label runat="server" ID="Label2" Text='<%# Eval("Remarks") %>'></asp:Label></li>
                                        <li class="clear pad_5"></li>
                                    </ul>
                                </div>
                                <div class="billclose">
                                    <asp:LinkButton ID="lkbtnEdit" runat="server" CommandArgument='<%# Eval("BillNo") %>'
                                        CommandName='<%# Eval("CustomerReadingId") %>'>
                                                 <img src="../images/Edit.png" alt="Edit" /></asp:LinkButton>
                                    <div class="clear pad_5">
                                    </div>
                            </ItemTemplate>
                        </asp:DataList>
                    </div>
                    <div id="divNoPendingBills" runat="server" visible="false" class="billcustomert"
                        style="width: auto;">
                        <h3>
                            <asp:Literal ID="Literal27" runat="server" Text="<%$ Resources:Resource, NO_BILLS_PENDING%>"></asp:Literal>
                        </h3>
                    </div>
                </div>
                <asp:ModalPopupExtender ID="mpeMeterReading" runat="server" TargetControlID="hdnTargeId"
                    PopupControlID="pnlReeadingAdjustment" BackgroundCssClass="popbg" CancelControlID="btnOk">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hdnTargeId" runat="server" />
                <asp:Panel runat="server" ID="pnlReeadingAdjustment" CssClass="editpopup_two">
                    <div class="panelMessage">
                        <div>
                            <asp:Panel ID="pnlMsgPopup" runat="server" align="center">
                                <asp:Label ID="lblMsgPopup" runat="server" Text="<%$Resources:Resource, BILL_ADJUSTMENT_REQ_SENT %>"></asp:Label>
                            </asp:Panel>
                        </div>
                        <div class="clear pad_10">
                        </div>
                        <div class="buttondiv">
                            <asp:Button ID="btnOk" runat="server" Text="OK" CssClass="btn_ok" Style="margin-left: 105px;" />
                            <br />
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeReadingAdjustment" runat="server" TargetControlID="btnpopup"
                    PopupControlID="pnlChangePwd" BackgroundCssClass="popbg">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnpopup" runat="server" Text="" Style="display: none;" />
                <asp:Panel ID="pnlChangePwd" runat="server" CssClass="editpopup_two" Style="display: none;
                    width: 720px; left: 259.5px; position: fixed; z-index: 100001; top: 111.5px;">
                    <h3 class="edithead" align="left" style="background: none repeat scroll 0 0 #008000;
                        border-bottom: 1px solid #CCCCCC; color: #FFFFFF; margin-bottom: 20px; padding: 10px 10px 8px 14px;">
                        <asp:Literal ID="Literal26" runat="server" Text="<%$ Resources:Resource, ADJUST_METER_READING%>"></asp:Literal>
                    </h3>
                    <div class="adjust-meter">
                        <ul style="width: 357px; float: left;">
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:Resource, BILLNUMBER%>"></asp:Literal></li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:LinkButton runat="server" ID="lbtnBillNo" Enabled="false" ForeColor="Black"></asp:LinkButton>
                                <asp:Label runat="server" ID="lblBillNo123" Style="display: none;"></asp:Label>
                            </li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal19" runat="server" Text="<%$ Resources:Resource, PREVIOUS_READING%>"></asp:Literal></li><li
                                    class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label runat="server" ID="lblPreviousReading"></asp:Label>
                            </li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, CURRENT_READING%>"></asp:Literal></li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:HiddenField ID="hfReadingBeforeAdjust" runat="server" />
                                <asp:TextBox runat="server" ID="txtPresentReading" onkeyup="ConsumptionByPrReading(this)"
                                    MaxLength="18" autocomplete="off"></asp:TextBox>
                                <span id="spanPresentReading" class="spancolor"></span></li>
                            <asp:FilteredTextBoxExtender ID="ftbTxtAnotherContactNo" runat="server" TargetControlID="txtPresentReading"
                                FilterType="Numbers">
                            </asp:FilteredTextBoxExtender>
                            <li class=" clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal57" runat="server" Text="<%$ Resources:Resource, PRV_USG_BALANCE%>"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblConsumptionToAdjust" runat="server"></asp:Label>
                            </li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:Resource, CONSUMPTION%>"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblRConsumption" runat="server"></asp:Label>
                            </li>
                            <li class=" clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal56" runat="server" Text='<%$ Resources:Resource, ENRGY_CHRGS_BFR_ADJSTMNT %>'></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblEnergyChargesBfrAdjsut" runat="server"></asp:Label>
                                <%--<asp:TextBox runat="server" ID="txtFixedCharges" onkeyup="GetCurrencyFormate(this);"
                                MaxLength="12"></asp:TextBox>--%></li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal25" runat="server" Text="<%$ Resources:Resource, ENRGY_CHRG_AFTR_ADJSTMNT%>"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblREnergyCharges" runat="server" Text="0"></asp:Label>
                                <%--<asp:TextBox runat="server" ID="txtFixedCharges" onkeyup="GetCurrencyFormate(this);"
                                MaxLength="12"></asp:TextBox>--%></li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal37" runat="server" Text="Tax"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblTaxPercent" runat="server"></asp:Label>&nbsp;&nbsp;<span>&#37;</span></li>
                            <li class="clear pad_5"></li>
                            <li class="clear pad_5"></li>
                            <%-- <li class="prevleft fltl">
                    <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:Resource, ADDITIONAL_CHARGES%>"></asp:Literal>
                </li>
                <li class=" fltl">:</li><li class=" fltl">
                    <asp:TextBox runat="server" ID="txtAdditionalCharges"></asp:TextBox></li>--%>
                        </ul>
                        <ul style="width: 357px; float: left;">
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal39" runat="server" Text="Total bill amount before adjustment"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblTotalBill" runat="server"></asp:Label></li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl" style="line-height: 13px;">
                                <asp:Literal ID="Literal40" runat="server" Text="Total bill amount after adjustment"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblTotalAfterAdjustment" runat="server"></asp:Label></li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal38" runat="server" Text="Tariff"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblTarif" runat="server"></asp:Label></li>
                            <li class="prevleft fltl">Tariff Details</li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <table class="tariffdata">
                                    <tbody>
                                        <tr>
                                            <th>
                                                Usage
                                            </th>
                                            <th>
                                                Tariff Rate
                                            </th>
                                        </tr>
                                        <asp:Repeater ID="rptrRTariffList" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <%#Eval("FromKW")%>
                                                        To
                                                        <%#Eval("ToKW")%>
                                                    </td>
                                                    <td>
                                                        <%#Eval("Amount")%>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </li>
                        </ul>
                        <asp:HiddenField ID="hdnCustomerBillID" runat="server" Visible="false" />
                        <asp:HiddenField ID="hdnCustomerID" runat="server" Visible="false" />
                    </div>
                    <div class="clear pad_10">
                    </div>
                    <div class="consumer_feild">
                        <div style="margin-left: 156px; overflow: auto; float: left; height: 43px;">
                            <asp:Button ID="btnUpdate" OnClientClick="return ValidatePayment();" Text="<%$ Resources:Resource, UPDATE%>"
                                CssClass="calculate_but" runat="server" OnClick="btnUpdate_Click" />
                            <asp:Button ID="btnCancel" Text="<%$ Resources:Resource, CANCEL%>" CssClass="calculate_but"
                                runat="server" />
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeReadCodes" runat="server" TargetControlID="Button1"
                    PopupControlID="pnReadCodes" BackgroundCssClass="popbg" CancelControlID="btnCancelOption">
                </asp:ModalPopupExtender>
                <asp:Button ID="Button1" runat="server" Text="" Style="display: none;" />
                <asp:Panel ID="pnReadCodes" runat="server" CssClass="editpopup_two" Style="display: none">
                    <h3 class="edithead" align="left">
                        <asp:Literal ID="Literal20" runat="server" Text="<%$ Resources:Resource, ADJUSTMENT_OPTION%>"></asp:Literal>
                    </h3>
                    <div class="clear">
                        &nbsp;</div>
                    <%--<div class="consume_name">
                    <asp:Literal ID="litReadCode" runat="server" Text="<%$ Resources:Resource, READCODE%>"></asp:Literal><span>*</span></div>--%>
                    <div class="prev_total routwise">
                        <asp:RadioButtonList ID="rdblAdjsutmentType" runat="server" RepeatDirection="Horizontal">
                            <%--<asp:ListItem Text="Reading Adjustment" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Bill Adjustment" Value="2"></asp:ListItem>--%>
                        </asp:RadioButtonList>
                        <br />
                        <asp:Button ID="btnGetDetails" Text="<%$ Resources:Resource, PROCEED%>" CssClass="calculate_but"
                            runat="server" OnClick="btnGetDetails_Click" />
                        <asp:Button ID="btnCancelOption" Text="<%$ Resources:Resource, CANCEL%>" CssClass="calculate_but"
                            runat="server" />
                    </div>
                    <div class="clear pad_10">
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeAdditionalChargesAdjustments" runat="server" TargetControlID="Button2"
                    PopupControlID="pnBillAdjustment" BackgroundCssClass="popbg">
                </asp:ModalPopupExtender>
                <asp:Button ID="Button2" runat="server" Text="" Style="display: none;" />
                <asp:Panel ID="pnBillAdjustment" runat="server" CssClass="editpopup_two" Style="display: none;
                    width: 795px; left: 224px; height: 555px; font-size: 14px;">
                    <h3 class="edithead billAdjustmentPopup" align="left">
                        <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:Resource, AD_CHRG_ADJ%>"></asp:Literal>
                    </h3>
                    <div class="adjust-meter">
                        <ul style="width: 364px; float: left;">
                            <asp:Repeater runat="server" ID="rptrAdditionalchargesshow" OnItemDataBound="rptrAdditionalchargesshow_ItemDataBound">
                                <ItemTemplate>
                                    <li class="clear pad_5"></li>
                                    <li class="prevleft fltl">
                                        <asp:Label ID="lblChargeName" runat="server" Text='<%# Eval("ChargeName") %>' CommandName='<%# Eval("ChargeID") %>'></asp:Label>
                                    </li>
                                    <li class="middle fltl">:</li>
                                    <li class="prevright fltl">
                                        <asp:Label runat="server" ID="lblReadingAdditionalCharges" Text='<%# Eval("Amount") %>'
                                            CssClass="ActualCharges"></asp:Label>
                                    </li>
                                    <li class="clear pad_5"></li>
                                    <li class="prevleft fltl">
                                        <asp:Label ID="lblChargeNameTax" runat="server" Text='<%# Eval("ChargeName") + " Tax Amount"%>'></asp:Label>
                                    </li>
                                    <li class="middle fltl">:</li>
                                    <li class="prevright fltl">
                                        <asp:Label runat="server" ID="lblChargeNameTaxAmount" Text=''></asp:Label>
                                    </li>
                                    <li class="clear pad_5"></li>
                                    <li class="prevleft fltl">
                                        <asp:Label ID="lblChargeNamePreviousAdjust" runat="server" Text='<%# "Previous Adjusted " + Eval("ChargeName") %>'></asp:Label>
                                    </li>
                                    <li class="middle fltl">:</li>
                                    <li class="prevright fltl">
                                        <asp:Label runat="server" ID="lblChargeNamePreviousAdjustAmount" Text='<%# Eval("AdditionalCharges") %>'
                                            CssClass="AdjustedCharges"></asp:Label>
                                    </li>
                                    <li class="clear pad_5"></li>
                                    <li class="prevleft fltl">
                                        <asp:Label ID="lblChargeNamePreviousAdjustTax" runat="server" Text='<%# "Previous Adjusted " + Eval("ChargeName") + " Tax Amount"%>'></asp:Label>
                                    </li>
                                    <li class="middle fltl">:</li>
                                    <li class="prevright fltl">
                                        <asp:Label runat="server" ID="lblVat" Text='<%# Eval("Vat") %>' Visible="false"></asp:Label>
                                        <asp:Label runat="server" ID="lblChargeNamePreviousAdjustTaxAmount" Text=''></asp:Label>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal30" runat="server" Text="Tariff"></asp:Literal></li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label runat="server" ID="lblDTariff"> </asp:Label>
                            </li>
                            <%-- <li class="clear pad_5"></li>
                        <li class="prevleft fltl">
                            <asp:Literal ID="Literal45" runat="server" Text="<%$ Resources:Resource, BILLTYPE%>"></asp:Literal>
                        </li>
                        <li class="middle fltl">:</li>
                        <li class="prevright fltl">
                            <asp:Label runat="server" ID="lblBillType" Enabled="false"></asp:Label>
                        </li>--%>
                            <li class=" clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal58" runat="server" Text="<%$ Resources:Resource, PRV_USG_BALANCE%>"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblDConsumptionToAdjust" runat="server"></asp:Label>
                            </li>
                            <li class=" clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal45" runat="server" Text="Meter Multiplier"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblMeterMultiplier" runat="server"></asp:Label>
                            </li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl" style="line-height: 25px;">
                                <asp:Literal ID="Literal42" runat="server" Text="Consumption"></asp:Literal></li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label runat="server" ID="lblDConsumption"> </asp:Label>
                            </li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal23" runat="server" Text="<%$ Resources:Resource, ENERGYCHARGES%>"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label runat="server" ID="lblEnergyCharges"> </asp:Label>
                                <%-- <asp:TextBox runat="server" ID="txtReadingEnergyCharges" onkeyup="BillAdjustment(this);"
                                MaxLength="10" autocomplete="off"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtReadingEnergyCharges"
                                FilterType="Numbers, Custom" ValidChars=".,">
                            </asp:FilteredTextBoxExtender>
                            <span id="spanDEneryCharges" class="spancolor"></span>--%>
                            </li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl" style="line-height: 25px;">
                                <asp:Literal ID="taxAmtBfrAdjust" runat="server" Text="Tax Before Adjustment"></asp:Literal></li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label runat="server" ID="lblTaxEffected"></asp:Label>&nbsp;&nbsp;</li>
                        </ul>
                        <ul style="width: 347px; line-height: 25px; margin-top: 0px; float: left;">
                            <asp:Repeater runat="server" ID="rptrAdditionalChargesAdjust">
                                <ItemTemplate>
                                    <li class="clear pad_5"></li>
                                    <li class="prevleft fltl" style="line-height: 22px; background-color: #639b00; color: #fff">
                                        <asp:LinkButton runat="server" ID="lbtnChargeName" Text='<%# "Adjusted "+Eval("ChargeName") %>'
                                            CommandName='<%# Eval("ChargeID") %>' ForeColor="Black" class="fetchAddCharge"
                                            Enabled="false" CommandArgument='<%# Eval("Amount") %>'></asp:LinkButton>
                                        <%--  <asp:Label ID="lbtnChargeName" runat="server" Text='<%# Eval("ChargeName") %>' class="fetchAddCharge"></asp:Label>--%>
                                    </li>
                                    <li class="middle fltl">:</li>
                                    <li class="prevright fltl" style="background-color: #639b00;">
                                        <asp:TextBox runat="server" Style="width: 161px;" ID="txtReadingAdditionalCharges"
                                            Text='0' CssClass="fetchCharges" onkeyup="BillAdjustment(); GetCurrencyFormate(this);"
                                            onkeypress="return isNumberKey(event,this)" MaxLength="18" autocomplete="off"></asp:TextBox>
                                        <%--<%# Eval("Amount") %>--%>
                                        <asp:FilteredTextBoxExtender ID="ftbTxtAnotherContactNo" runat="server" TargetControlID="txtReadingAdditionalCharges"
                                            FilterType="Numbers, Custom" ValidChars=".,">
                                        </asp:FilteredTextBoxExtender>
                                        <div style="display: none;">
                                            <asp:TextBox ID="litReadingAdditionalCharges" CssClass="litFetchCharges" Text='<%# Eval("Amount") %>'
                                                runat="server"></asp:TextBox>
                                        </div>
                                        <asp:Label ID="litAdditionalChargeID" runat="server" Visible="false" Text='<%# Eval("ChargeID") %>'></asp:Label>
                                    </li>
                                    <%--<li class="clear pad_5"></li>
                                <li class="prevleft fltl" style="line-height: 15px;">
                                    <asp:Literal ID="Literal75" runat="server" Text="Adjested Amount"></asp:Literal></li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <asp:Label runat="server" ID="lblAdjestedAmount" CssClass="lblAdjestedAmounts">00.00</asp:Label>&nbsp;&nbsp;</li>--%>
                                    <%--<li class="clear pad_5"></li>
                                <li class="prevleft fltl" style="line-height: 15px;">
                                    <asp:Literal ID="taxAmtaftrAdjust" runat="server" Text="Tax Effected"></asp:Literal></li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <asp:Label runat="server" ID="lblTalAfterAdjust" CssClass="fetchChargesTax">00.00</asp:Label>&nbsp;&nbsp;</li>--%>
                                </ItemTemplate>
                            </asp:Repeater>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl" style="line-height: 25px;">
                                <asp:Literal ID="Literal75" runat="server" Text="Amount After Adjustment"></asp:Literal></li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label runat="server" ID="lblAdjestedAmountTotal">0.00</asp:Label>&nbsp;&nbsp;
                            </li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl" style="line-height: 25px;">
                                <asp:Literal ID="taxAmtaftrAdjust" runat="server" Text="Tax After Adjustment"></asp:Literal></li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label runat="server" ID="lblTaxEffectedTotal">0.00</asp:Label>&nbsp;&nbsp;
                            </li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl" style="line-height: 25px;">
                                <asp:Literal ID="Literal76" runat="server" Text="Total Amount After Adjustment"></asp:Literal></li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label runat="server" ID="lblTotalAdjestment">0.00</asp:Label>&nbsp;&nbsp;
                            </li>
                            <%--<li class="prevleft fltl" style="line-height: 15px;">
                            <asp:Literal ID="Literal30" runat="server" Text="Energy Charges Per K/W"></asp:Literal></li>
                        <li class="middle fltl">:</li>
                        <li class="prevright fltl">
                            <asp:Label runat="server" ID="lblEnergyperKw"> </asp:Label>
                        </li>--%>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl" style="line-height: 25px;">
                                <asp:Literal ID="Literal41" runat="server" Text="Tax"></asp:Literal></li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label runat="server" ID="lblDTax"> </asp:Label>&nbsp;&nbsp;<span>&#37;</span></li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft total">
                                <asp:Literal ID="Literal28" runat="server" Text="Total Bill Amount Before Adjustment"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright total">
                                <asp:Label runat="server" ID="lblReadingTotalBill"></asp:Label></li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft total" style="line-height: 25px;">
                                <asp:Literal ID="Literal43" runat="server" Text="Total Bill Amount After Adjustment"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright total">
                                <asp:Label runat="server" ID="lblDTotalBillAfterAdjustment_ForCal" Style="display: none;"></asp:Label>
                                <asp:Label runat="server" ID="lblDTotalBillAfterAdjustment"></asp:Label></li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal24" runat="server" Text="<%$ Resources:Resource, BILLNUMBER%>"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:LinkButton runat="server" ID="lblBillNo" Enabled="false" ForeColor="Black"></asp:LinkButton>
                            </li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal44" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label runat="server" ID="lblAdjustBillAccountNo" Enabled="false"></asp:Label>
                            </li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">Tariff Details</li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <table class="tariffdata">
                                    <tbody>
                                        <tr>
                                            <th>
                                                Usage
                                            </th>
                                            <th>
                                                Tariff Rate
                                            </th>
                                        </tr>
                                        <asp:Repeater ID="rptrDTariffList" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <%#Eval("FromKW")%>
                                                        To
                                                        <%#Eval("ToKW")%>
                                                    </td>
                                                    <td>
                                                        <%#Eval("Amount")%>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">Reason <span class="span_star">*</span></li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:TextBox ID="txtAdditionalReason" runat="server" TextMode="MultiLine" placeholder="Enter Reason"></asp:TextBox>
                            </li>
                        </ul>
                        <div class="clear pad_5">
                        </div>
                        <div class="consumer_feild">
                            <div style="float: right; height: 41px; margin-right: 5px; margin-top: -16px;">
                                <asp:Button ID="btnUpdateCharges" OnClientClick="return ValidateBillAdjustment();"
                                    Text="<%$ Resources:Resource, UPDATE%>" CssClass="box_s" runat="server" OnClick="btnUpdateCharges_Click" />
                                <asp:Button ID="Button4" Text="<%$ Resources:Resource, CANCEL%>" CssClass="box_s"
                                    runat="server" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeBillInProcess" runat="server" TargetControlID="hdnCancel1"
                    PopupControlID="pnlBillInProcess" BackgroundCssClass="popbg" CancelControlID="btnBPOK">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hdnCancel1" runat="server"></asp:HiddenField>
                <asp:Panel runat="server" ID="pnlBillInProcess" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblProcess" runat="server"></asp:Label>
                        <%--<asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, BILL_IN_PROCESS_ADJUSTMENTS%>"></asp:Label>--%>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnBPOK" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="btn_ok"
                                Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeBillClosed" runat="server" TargetControlID="hdfBillClosed"
                    PopupControlID="pnlBillClosed" BackgroundCssClass="popbg" CancelControlID="btnBillClosedOk">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hdfBillClosed" runat="server"></asp:HiddenField>
                <asp:Panel runat="server" ID="pnlBillClosed" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, BILL_IS_CLOSED%>"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnBillClosedOk" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="btn_ok" Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeBADetails" runat="server" TargetControlID="hdnBADetails"
                    PopupControlID="pnlBADetails" BackgroundCssClass="popbg" CancelControlID="lbtnCancelView">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hdnBADetails" runat="server" />
                <asp:Panel runat="server" ID="pnlBADetails" CssClass="editpopup_two billadjustmentPopupsmall"
                    Style="display: none;">
                    <%-- <div class="panelMessage">
                    <div>
                        <asp:Panel ID="Panel2" runat="server" align="center">--%>
                    <div id="divMeterAdjustmentDetails" runat="server">
                        <div class="billpopup routwise" style="float: left;">
                            <ul>
                                <div style="font-weight: bold; font-size: 14px; text-align: center;">
                                    <asp:Label runat="server" ID="lblMInfoTypeOfAdjust" Text="Adjustment Details"></asp:Label><br />
                                </div>
                                <div>
                                    <li class="prevleft fltl">
                                        <asp:Literal ID="Literal32" runat="server" Text="AdjustmentType"></asp:Literal></li><li>
                                            class=" fltl">:</li>
                                    <li class=" fltl">
                                        <asp:Label runat="server" ID="lblAdjustmentType"></asp:Label></li>
                                </div>
                                <div>
                                    <li class="prevleft fltl">
                                        <asp:Literal ID="Literal33" runat="server" Text="Adjustment Amount"></asp:Literal></li><li>
                                            class=" fltl">:</li>
                                    <li class=" fltl">
                                        <asp:Label runat="server" ID="lblAdjustedAmount"></asp:Label>
                                        <br />
                                        <asp:Label runat="server" ID="lblAdjustedUnits"></asp:Label>
                                    </li>
                                </div>
                                <div>
                                    <li class="prevleft fltl">
                                        <asp:Literal ID="Literal34" runat="server" Text="Tax Adjustment"></asp:Literal></li><li>
                                            class=" fltl">:</li><li class=" fltl">
                                                <asp:Label runat="server" ID="lblTaxAdjusted"></asp:Label></li>
                                </div>
                                <div>
                                    <li class="prevleft fltl">
                                        <asp:Literal ID="Literal31" runat="server" Text="Total Adjustment"></asp:Literal></li><li>
                                            class=" fltl">:</li>
                                    <li class=" fltl">
                                        <asp:Label runat="server" ID="lblTotalAdjustment"></asp:Label>
                                    </li>
                                </div>
                                <div>
                                    <li class="prevleft fltl">
                                        <asp:Literal ID="Literal68" runat="server" Text="Total Amout Effected"></asp:Literal></li><li>
                                            class=" fltl">:</li>
                                    <li class=" fltl">
                                        <asp:Label runat="server" ID="lblTotalAmoutEffected"></asp:Label>
                                    </li>
                                </div>
                                <div style="float: left;">
                                    <asp:LinkButton ID="lbtnCancelView" class="box_s" runat="server" Text='<%$ Resources: Resource, OK %>'></asp:LinkButton>
                                </div>
                                <div style="float: right;">
                                    <asp:LinkButton ID="lnkCancelAdjustments" class="box_s" runat="server" Text='<%$ Resources: Resource, CANCEL_ADJUST %>'
                                        OnClick="lnkCancelAdjustments_Click"></asp:LinkButton>
                                </div>
                            </ul>
                        </div>
                    </div>
                    <%--<div id="divBillAdjustmentDetails" runat="server" visible="false">
                                <div class="billpopup routwise" style="float: left;">
                                    <ul>
                                        <div style="clear: both;">
                                            <li style="font-weight: bold; font-size: 14px;">
                                                <asp:Label runat="server" ID="lblTypeOfAdjust"></asp:Label><br />
                                            </li>
                                        </div>
                                        <div style="clear: both;">
                                            <li class="prevleft fltl">
                                                <asp:Literal ID="Literal36" runat="server" Text="<%$ Resources:Resource, BILLNUMBER%>"></asp:Literal></li>
                                            <li class=" fltl">:</li>
                                            <li class=" fltl">
                                                <asp:Label runat="server" ID="lblInfoBillNo"></asp:Label></li>
                                        </div>
                                        <div style="clear: both;">
                                            <li class="prevleft fltl">
                                                <asp:Literal ID="Literal35" runat="server" Text="<%$ Resources:Resource, ENERGYCHARGES%>"></asp:Literal></li><li
                                                    class=" fltl">:</li>
                                            <li class=" fltl">
                                                <asp:Label runat="server" ID="lblInfoEneryCharges"></asp:Label>
                                            </li>
                                        </div>
                                        <asp:Repeater runat="server" ID="rptAdditionalChargesDetails" OnItemDataBound="rptrAdditionalCharges_ItemDataBound">
                                            <ItemTemplate>
                                                <div style="clear: both;">
                                                    <li class="prevleft fltl">
                                                        <asp:Label ID="lblChargeName" runat="server" Text='<%# Eval("ChargeName") %>'></asp:Label>
                                                    </li>
                                                    <li class=" fltl">:</li>
                                                    <li class=" fltl">
                                                        <asp:Label runat="server" ID="lblAmount" Text='<%# Eval("AdditionalCharges") %>'></asp:Label></li>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </div>
                            </div>--%>
                    <%-- </asp:Panel>
                    </div>
                    <div class="buttondiv fltr pad_10">
                        <asp:Button ID="btnBADetails" runat="server" Text="OK" CssClass="btn_ok" Style="margin-right: 12px;
                            background: #008000; padding: 2px 14px;" />
                        <br />
                    </div>
                </div>--%>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeCustomerNA" runat="server" TargetControlID="hdnCancelCNA"
                    PopupControlID="pnlCustomerNA" BackgroundCssClass="popbg" CancelControlID="btnBPOK">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hfMonthStatusCount" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hdnCancelCNA" runat="server"></asp:HiddenField>
                <asp:Panel runat="server" ID="pnlCustomerNA" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, CUSTOMER_NA%>"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="Button3" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="btn_ok"
                                Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeBillNA" runat="server" TargetControlID="btnBillNA"
                    PopupControlID="pnlBillNA" BackgroundCssClass="popbg" CancelControlID="btnBillNAOK">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="btnBillNA" runat="server"></asp:HiddenField>
                <asp:Panel runat="server" ID="pnlBillNA" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, BILL_NA%>"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnBillNAOK" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="btn_ok"
                                Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeNotInScope" runat="server" TargetControlID="hdnNotInScope"
                    PopupControlID="pnlNotInScope" BackgroundCssClass="popbg" CancelControlID="btnNotInScope">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hdnNotInScope" runat="server"></asp:HiddenField>
                <asp:Panel runat="server" ID="pnlNotInScope" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblNotInScopeMsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnNotInScope" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="btn_ok" Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%--Conumption Adjustment Popup Starts--%>
                <asp:ModalPopupExtender ID="mpeConsumptionAdjustment" runat="server" TargetControlID="hfCATargetID"
                    PopupControlID="pnlConsumptionAdjustment" BackgroundCssClass="popbg" CancelControlID="btnCancelCA">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hfCATargetID" runat="server" />
                <asp:Panel ID="pnlConsumptionAdjustment" runat="server" CssClass="editpopup_two"
                    Style="display: none; width: 795px; left: 259.5px; height: 555px; position: fixed;
                    font-size: 14px; z-index: 100001; top: 111.5px;">
                    <h3 class="edithead" align="left" style="background: none repeat scroll 0 0 #639b00;
                        border-bottom: 1px solid #CCCCCC; color: #FFFFFF; margin-bottom: 20px; padding: 5px 10px;">
                        <asp:Literal ID="Literal35" runat="server" Text="<%$ Resources:Resource, CONSUMPTION_ADJUSTMENT%>"></asp:Literal>
                    </h3>
                    <div class="adjust-meter">
                        <ul style="width: 347px; float: left;">
                            <li class="prevleft fltl" style="margin-left: -27px;">
                                <asp:Literal ID="Literal46" runat="server" Text="<%$ Resources:Resource, PREVIOUS_READING%>"></asp:Literal></li><li
                                    class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label runat="server" ID="lblPreviousReadingCA"></asp:Label>
                            </li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal47" runat="server" Text="<%$ Resources:Resource, CURRENT_READING%>"></asp:Literal></li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblPresentReadionCA" runat="server"></asp:Label>
                            </li>
                            <li class=" clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="lblheadCTA" runat="server" Text="<%$ Resources:Resource, PRV_USG_BALANCE%>"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblConsumptionToAdjsutCA" runat="server"></asp:Label>
                            </li>
                            <li class=" clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal54" runat="server" Text="<%$ Resources:Resource, CONSUMPTION%>"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblCurrentConsumption" runat="server"></asp:Label>
                            </li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl" style="background-color: #639b00;">
                                <asp:Literal ID="Literal48" runat="server" Text="<%$ Resources:Resource, CONSUMPTION_TO_ADJUST%>"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl" style="background-color: #639b00;">
                                <asp:HiddenField ID="hfReadingBeforeAdjustCA" runat="server" />
                                <asp:TextBox runat="server" Style="width: 161px;" ID="txtConsumptionCA" onkeyup="ConsumptionAdjustmentDetails(this); GetCurrencyFormate(this);"
                                    MaxLength="10" autocomplete="off" onkeypress="return isNumberKey(event,this)"></asp:TextBox>
                                <span id="spanConsumptionCA" class="spancolor"></span></li>
                            <asp:FilteredTextBoxExtender ID="ftbTxtAnotherContactNoCA" runat="server" TargetControlID="txtConsumptionCA"
                                FilterType="Numbers,Custom" ValidChars=",">
                            </asp:FilteredTextBoxExtender>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal84" runat="server" Text="Total Adjusted Consumption"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblTotalAdjustedConsumption" runat="server" Text=""></asp:Label>
                            </li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal59" runat="server" Text="Tax Effected"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblTaxEffectedConAdj" runat="server" Text="0.00"></asp:Label>
                            </li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal55" runat="server" Text="<%$ Resources:Resource, CONSUMPTION_AFTR_ADJUST%>"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">&nbsp;
                                <asp:Label ID="lblConsumptionAfterAdjst" runat="server" Text="0"></asp:Label>
                            </li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal49" runat="server" Text="<%$ Resources:Resource, ENERGYCHARGES%>"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblREnergyChargesCAForCal" runat="server" Style="display: none;"></asp:Label>
                                <asp:Label ID="lblREnergyChargesCA" runat="server"></asp:Label>
                                <%--<asp:TextBox runat="server" ID="txtFixedCharges" onkeyup="GetCurrencyFormate(this);"
                                MaxLength="12"></asp:TextBox>--%></li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal50" runat="server" Text="Tax"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblTaxPercentCA" runat="server"></asp:Label>&nbsp;&nbsp;<span>&#37;</span></li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal81" runat="server" Text="Total Adjusted Amount"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblAdjustedAmountCAForCal" runat="server" Style="display: none;"></asp:Label>
                                <asp:Label ID="lblAdjustedAmountCA" runat="server"></asp:Label></li>
                            <li class="clear pad_5"></li>
                            <li class="clear pad_5"></li>
                            <%-- <li class="prevleft fltl">
                    <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:Resource, ADDITIONAL_CHARGES%>"></asp:Literal>
                </li>
                <li class=" fltl">:</li><li class=" fltl">
                    <asp:TextBox runat="server" ID="txtAdditionalCharges"></asp:TextBox></li>--%>
                        </ul>
                        <ul style="width: 347px; float: left; margin-left: 37px;">
                            <li class="prevleft fltl" style="margin-left: -27px;">
                                <asp:Literal ID="Literal36" runat="server" Text="<%$ Resources:Resource, BILLNUMBER%>"></asp:Literal></li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:LinkButton runat="server" ID="lbtnBillNoCA" Enabled="false" ForeColor="Black"></asp:LinkButton>
                                <asp:Label runat="server" ID="lblBillNoCA" Style="display: none;"></asp:Label>
                                <li class="clear pad_5"></li>
                                <li class="prevleft fltl" style="line-height: 13px;">
                                    <asp:Literal ID="Literal77" runat="server" Text="Global Account Number"></asp:Literal>
                                </li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <asp:Label ID="lblGloabalAccountNumber" runat="server"></asp:Label></li>
                                <li class="clear pad_5"></li>
                                <li class="prevleft fltl">
                                    <asp:Literal ID="Literal51" runat="server" Text="Total bill amount before adjustment"></asp:Literal>
                                </li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <asp:Label ID="lblTotalBillCA" runat="server"></asp:Label></li>
                                <li class="clear pad_5"></li>
                                <li class="prevleft fltl" style="line-height: 13px;">
                                    <asp:Literal ID="Literal52" runat="server" Text="Total bill amount after adjustment"></asp:Literal>
                                </li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <asp:Label ID="lblTotalAfterAdjustmentCAForCal" runat="server" Style="display: none;"></asp:Label>
                                    <asp:Label ID="lblTotalAfterAdjustmentCA" runat="server"></asp:Label></li>
                                <li class="clear pad_5"></li>
                                <li class="prevleft fltl">
                                    <asp:Literal ID="Literal60" runat="server" Text="Multiplier"></asp:Literal>
                                </li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <asp:Label ID="lblMultiplierCA" runat="server"></asp:Label></li>
                                <li class="clear pad_5"></li>
                                <li class="prevleft fltl">
                                    <asp:Literal ID="Literal53" runat="server" Text="Tariff"></asp:Literal>
                                </li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <asp:Label ID="lblTarifCA" runat="server"></asp:Label></li>
                                <li class="clear pad_5"></li>
                                <li class="prevleft fltl">Tariff Details</li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <table class="tariffdata">
                                        <tbody>
                                            <tr>
                                                <th>
                                                    Usage
                                                </th>
                                                <th>
                                                    Tariff Rate
                                                </th>
                                            </tr>
                                            <asp:Repeater ID="rptrRTariffListCA" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <%#Eval("FromKW")%>
                                                            To
                                                            <%#Eval("ToKW")%>
                                                        </td>
                                                        <td>
                                                            <%#Eval("Amount")%>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </li>
                                <li class="prevleft fltl" style="margin-left: -27px;">Reason<span class="span_star">*</span></li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl" style="margin-top: 5px;">
                                    <asp:TextBox ID="txtMeterReson" runat="server" CssClass="text-box" Width="150px"
                                        Height="50px" TextMode="MultiLine" placeholder="Enter Reason"></asp:TextBox>
                                </li>
                        </ul>
                        <asp:HiddenField ID="hdnCustomerBillIDCA" runat="server" Visible="false" />
                        <asp:HiddenField ID="hdnCustomerIDCA" runat="server" Visible="false" />
                    </div>
                    <div class="consumer_feild">
                        <div style="margin-left: 165px; overflow: auto; float: left; height: 43px;">
                            <asp:Button ID="btnUpdateCA" OnClientClick="return ValidateConsumptionAdjustment();"
                                Text="<%$ Resources:Resource, UPDATE%>" CssClass="box_s" runat="server" OnClick="btnUpdateCA_Click" />
                            <asp:Button ID="btnCancelCA" Text="<%$ Resources:Resource, CANCEL%>" CssClass="box_s"
                                runat="server" />
                        </div>
                    </div>
                </asp:Panel>
                <%--End--%>
                <%--Delete confimation Popup Starts--%>
                <asp:ModalPopupExtender ID="mpeDeleteConfirm" runat="server" TargetControlID="hfDeleteConfTarget"
                    PopupControlID="pnlDeleteConfirm" BackgroundCssClass="popbg" CancelControlID="btnDelConfirmCancel">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hfDeleteConfTarget" runat="server"></asp:HiddenField>
                <asp:Panel runat="server" ID="pnlDeleteConfirm" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblDeleteBillAdjsutmentDet" runat="server" Text="<%$ Resources:Resource, ARE_YOU_SURE_DEL_BIL_ADJUST%>"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btnDeleteBillAdjustment" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="btn_ok" Style="margin-left: 10px;" OnClick="btnDeleteBillAdjustment_Click" />
                            <asp:Button ID="btnDelConfirmCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="btn_ok" Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%--End--%>
            </div>
        </div>
        <uc1:Authentication ID="ucAuthentication" runat="server" />
        <%-- Grid Alert popup Start--%>
        <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
            TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
        </asp:ModalPopupExtender>
        <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
        <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
            <div class="popheader popheaderlblred">
                <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
            </div>
            <div class="footer popfooterbtnrt">
                <div class="fltr popfooterbtn">
                    <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                        CssClass="ok_btn" />
                </div>
                <div class="clear pad_5">
                </div>
            </div>
        </asp:Panel>
        <%-- Grid Alert popup Closed--%>
        <asp:ModalPopupExtender ID="MpestimatedBill" runat="server" PopupControlID="pnlEstimatedBill"
            TargetControlID="btnEstimatedOk" BackgroundCssClass="modalBackground" CancelControlID="btnEstimatedOk">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlEstimatedBill" runat="server" CssClass="modalPopup" Style="display: none;
            border-color: #CD3333;">
            <div class="popheader" style="background-color: #CD3333;">
                <asp:Label ID="lblEstimatedBillMessage" runat="server" Text=""></asp:Label>
            </div>
            <div class="footer" align="right">
                <asp:Button ID="btnEstimatedOk" runat="server" Text="<%$ Resources:Resource, OK%>"
                    CssClass="no" />
            </div>
        </asp:Panel>
        <%--Bill Adjustment Popup Starts--%>
        <asp:ModalPopupExtender ID="mpeBillAdjustments" runat="server" TargetControlID="hfBillAdjustments"
            PopupControlID="pnlBillAdjustments" BackgroundCssClass="popbg" CancelControlID="btnCancelCA">
        </asp:ModalPopupExtender>
        <asp:HiddenField ID="hfBillAdjustments" runat="server" />
        <asp:Panel ID="pnlBillAdjustments" runat="server" CssClass="editpopup_two" Style="display: none;
            width: 450px; height: 270px; left: 259.5px; position: fixed; z-index: 100001;
            top: 111.5px;">
            <h3 class="edithead" align="left" style="background: none repeat scroll 0 0 #639b00;
                border-bottom: 1px solid #CCCCCC; color: #FFFFFF; margin-bottom: 20px; padding: 5px 10px;">
                <asp:Literal ID="Literal65" runat="server" Text="<%$ Resources:Resource, BILL_ADJUSTMENT%>"></asp:Literal>
            </h3>
            <div class="adjust-meter">
                <ul style="width: 357px; float: left; margin-top: 24px;">
                    <li class="prevleft fltl" style="margin-left: -27px;">
                        <%--<asp:Literal ID="Literal66" runat="server" Text="<%$ Resources:Resource, CRDT_TO_CUSTMR%>"></asp:Literal></li>--%>
                        <asp:Literal ID="Literal66" runat="server" Text="Adjustment Type"></asp:Literal></li>
                    <li class="middle fltl">:</li>
                    <li class="prevright fltl">
                        <asp:RadioButtonList ID="rdoAdjustmentTypeBA" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Credit" Value="1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Debit" Value="2"></asp:ListItem>
                        </asp:RadioButtonList>
                        <%-- <asp:TextBox ID="txtCredit" Style="width: 90%;" Class="text-box" runat="server" MaxLength="18"
                            onkeyup="GetCurrencyFormate(this);" onkeypress="return isNumberKey(event,this)"
                            onblur="return ValidatePaidAmount()"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtCredit"
                            FilterType="Numbers,Custom" ValidChars=".,">
                        </asp:FilteredTextBoxExtender>--%>
                    </li>
                    <li class="clear pad_5"></li>
                    <li class="prevleft fltl">
                        <%--<asp:Literal ID="Literal67" runat="server" Text="<%$ Resources:Resource, DBT_TO_CUSTMR%>"></asp:Literal>--%>
                        <asp:Literal ID="Literal67" runat="server" Text="Amount"></asp:Literal>
                        <span class="span_star">*</span> </li>
                    <li class="middle fltl">:</li>
                    <li class="prevright fltl">
                        <asp:TextBox ID="txtCrdDbtAmount" Style="width: 90%;" Class="text-box" runat="server"
                            MaxLength="18" onkeyup="GetCurrencyFormate(this);" onkeypress="return isNumberKey(event,this)"
                            onblur="return ValidatePaidAmount()"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtCrdDbtAmount"
                            FilterType="Numbers,Custom" ValidChars=".,">
                        </asp:FilteredTextBoxExtender>
                        <%-- <asp:TextBox ID="txtDebit" Style="width: 90%;" Class="text-box" runat="server" MaxLength="18"
                            onkeyup="GetCurrencyFormate(this);" onkeypress="return isNumberKey(event,this)"
                            onblur="return ValidatePaidAmount()"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtDebit"
                            FilterType="Numbers,Custom" ValidChars=".,">
                        </asp:FilteredTextBoxExtender>--%>
                    </li>
                    <li class="clear pad_5"></li>
                    <li class="prevleft fltl">
                        <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, REASON%>"></asp:Literal><span
                            class="span_star">*</span></li><li class="middle fltl">:</li>
                    <li class="prevright fltl" style="margin-top: 5px;">
                        <asp:TextBox ID="txtBillAdjustReason" Style="width: 90%;" Class="text-box" TextMode="MultiLine"
                            runat="server"></asp:TextBox>
                    </li>
                </ul>
            </div>
            <div class="consumer_feild">
                <div style="margin-left: 166px; overflow: auto; float: left; height: 43px;">
                    <asp:Button ID="btnBillAdjustmentsUpdate" Text="<%$ Resources:Resource, UPDATE%>"
                        CssClass="box_s" runat="server" OnClick="btnBillAdjustmentsUpdate_Click" OnClientClick="return ValidateAdjustmentInputNew();" />
                    <asp:Button ID="btnBillAdjustmentsCancel" Text="<%$ Resources:Resource, CANCEL%>"
                        CssClass="box_s" runat="server" />
                </div>
            </div>
        </asp:Panel>
        <%--End--%>
        <%--mpeActivate Popup Starts--%>
        <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
            TargetControlID="btnActivate" BackgroundCssClass="modalBackground">
        </asp:ModalPopupExtender>
        <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
        <asp:Panel ID="PanelActivate" runat="server" DefaultButton="btnActiveCancel" CssClass="modalPopup"
            Style="display: none; border-color: #639B00;">
            <div class="popheader popheaderlblgreen" id="pop" runat="server">
                <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
            </div>
            <div class="space">
            </div>
            <div class="footer" align="right">
                <div class="fltr popfooterbtn">
                    <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, OK%>"
                        CssClass="cancel_btn" />
                </div>
                <div class="clear pad_5">
                </div>
            </div>
        </asp:Panel>
        <%--mpeActivate Popup End--%>
        <asp:ModalPopupExtender ID="mpeGenMessage" runat="server" PopupControlID="pnlGenMessage"
            TargetControlID="hfGenMessage" BackgroundCssClass="modalBackground" CancelControlID="BtnGenMessageClose">
        </asp:ModalPopupExtender>
        <asp:HiddenField ID="hfGenMessage" runat="server" />
        <asp:Panel ID="pnlGenMessage" runat="server" CssClass="modalPopup" Style="display: none;
            border-color: #CD3333;">
            <div class="popheader" style="background-color: #CD3333;">
                <asp:Label ID="lblGenMessage" runat="server" Text=""></asp:Label>
            </div>
            <div class="footer" align="right">
                <asp:Button ID="BtnGenMessageClose" runat="server" Text="<%$ Resources:Resource, OK%>"
                    CssClass="no" />
            </div>
        </asp:Panel>
        <asp:ModalPopupExtender ID="mpeAlert" runat="server" TargetControlID="hdnCancelCNA"
            PopupControlID="pnlAlert" BackgroundCssClass="popbg" CancelControlID="Button5">
        </asp:ModalPopupExtender>
        <asp:HiddenField ID="HiddenField1" runat="server"></asp:HiddenField>
        <asp:Panel runat="server" ID="pnlAlert" CssClass="modalPopup" Style="display: none;
            border-color: #639B00;">
            <div class="popheader" id="popActive" runat="server" style="background-color: red;">
                <asp:Label ID="lblAlertMsg" runat="server" Text=""></asp:Label>
            </div>
            <div class="footer" align="right">
                <div class="fltr popfooterbtn">
                    <br />
                    <asp:Button ID="Button5" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="btn_ok"
                        Style="margin-left: 10px;" />
                </div>
                <div class="clear pad_5">
                </div>
            </div>
        </asp:Panel>
        <asp:ModalPopupExtender ID="mpeCrdConfirmation" runat="server" PopupControlID="pnlCrdConfirmation"
            TargetControlID="hfCrdConfimation" BackgroundCssClass="modalBackground" CancelControlID="btnCrdConfirmationCncl">
        </asp:ModalPopupExtender>
        <asp:HiddenField ID="hfCrdConfimation" runat="server" />
        <asp:Panel ID="pnlCrdConfirmation" runat="server" CssClass="modalPopup" Style="display: none;
            border-color: #CD3333;">
            <div class="popheader" style="background-color: #CD3333;">
                <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
            </div>
            <div class="footer" align="right">
                <asp:Button ID="btnCrdConfirmationOK" runat="server" OnClick="btnBillAdjustmentsUpdate_Click"
                    Text="<%$ Resources:Resource, OK%>" CssClass="no" />
                <asp:Button ID="btnCrdConfirmationCncl" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                    CssClass="no" />
            </div>
        </asp:Panel>
        <asp:Literal ID="litMeterDial" runat="server" Visible="false"></asp:Literal>
        <asp:HiddenField ID="hdnConsumption" runat="server" />
        <asp:HiddenField ID="hfBillAdjustmentId" runat="server" Value="0" />
        <asp:HiddenField ID="hfAdjustmentType" runat="server" Value="0" />
        <asp:HiddenField ID="hfBatchID" runat="server" Value="0" />
        <%--==============Escape button script starts==============--%>
        <script type="text/javascript">
            $(document).keydown(function (e) {
                // ESCAPE key pressed 
                if (e.keyCode == 27) {
                    $(".mpeReadingAdjustment").fadeOut("slow");
                    $(".mpeReadCodes").fadeOut("slow");
                    $(".mpeAdditionalChargesAdjustments").fadeOut("slow");
                    $(".mpeMeterReading").fadeOut("slow");
                    $(".mpeBillInProcess").fadeOut("slow");
                    $(".mpeBillClosed").fadeOut("slow");
                    $(".mpeBADetails").fadeOut("slow");
                    $(".mpeCustomerNA").fadeOut("slow");
                    document.getElementById("overlay").style.display = "none";
                }
            });

            $(document).ready(function () {
                DisplayMessage('<%= pnlMessage.ClientID %>');
            });

            var prm = Sys.WebForms.PageRequestManager.getInstance();

            prm.add_endRequest(function () {
                DisplayMessage('<%= pnlMessage.ClientID %>');
            });
        </script>
        <%--==============Escape button script ends==============--%>
        <%--==============Validation script ref starts==============--%>
        <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
        <script src="../JavaScript/PageValidations/BillAdjustment.js?v=3" type="text/javascript"></script>
        <%--==============Validation script ref ends==============--%>
        <%--==============Ajax loading script starts==============--%>
        <%--==============Validation script ref ends==============--%>
</asp:Content>
