﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for Search consumer
                     
 Developer        : id067-naresh
 Creation Date    : 12-04-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using Resources;
using System.Xml;
using System.Data;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.Configuration;
using UMS_Nigeria.UserControls;
namespace UMS_Nigeria.Billing
{
    public partial class BillAdjustment : System.Web.UI.Page
    {
        #region Members
        CustomerDetailsBAL _ObjCustomerDetailsBAL = new CustomerDetailsBAL();
        BillAdjustmentBal _objBillAdjustmentBal = new BillAdjustmentBal();
        BillingMonthOpenBAL _objBillingMonthOpenBAL = new BillingMonthOpenBAL();
        CustomerDetailsBe _ObjCustomerDetailsBe = new CustomerDetailsBe();

        XmlDocument _objXmlDocument = new XmlDocument();

        iDsignBAL objIdsignBal = new iDsignBAL();
        ConsumerBal objConsumerBal = new ConsumerBal();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        public string Key = "BillAdjustments";
        string AccountNo;

        int BatchNo = 0;

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;

            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = _ObjCommonMethods.GetPagePath(this.Request.Url);
                //if (_ObjCommonMethods.IsAccess(path))
                //{
                //if (Request.QueryString[UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO] != null)
                if (!string.IsNullOrEmpty(Request.QueryString[UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO]))
                {
                    string AccountNo = objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO]).ToString();
                    txtccountNo.Text = AccountNo;
                }
                //}

                //else
                //{
                //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                //}

                if (Request.QueryString[Constants.QS_BatchID] != null)
                    GetBatchDetails();
                //else
                //    Response.Redirect(UMS_Resource.Adjustment_Batch);
            }
            GetAdjustmentLastTransactions();
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            BindUserDetails();
        }

        protected void lblSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADVANCE_SEARCH_PAGE + "?" + UMS_Resource.FROM_PAGE + "=" + objIdsignBal.Encrypt(Constants.BillAdjustmentsPageName), false);
                //Response.Redirect(UMS_Resource.EDIT_CONSUMER_PAGE + "?" + UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO + "=" + objIdsignBal.Encrypt(lblCustomerUniqueNo.Text), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //protected void lkbtnEdit_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        string BillMonth = ((LinkButton)sender).Text;
        //        string BillYear = ((LinkButton)sender).Text;


        //        MastersBE objMastersBE = new MastersBE();
        //        objMastersBE.AccountNo = lblAccountno.Text;// hfAccountNo.Value;
        //        objMastersBE.BillMonth = BillMonth;
        //        objMastersBE.BillYear = BillYear;
        //        objMastersBE.Flag = Convert.ToInt16(Resource.FLAG_BILL_ADJUSTMENT);
        //        if (!_ObjCommonMethods.IsCustomerInBillProcess(objMastersBE))
        //        {
        //            CustomerDetailsBe objCustomerDetailsBe = new CustomerDetailsBe();
        //            CustomerDetailsBAL objCustomerDetailsBAL = new CustomerDetailsBAL();
        //            string lkbtnEdit = ((LinkButton)sender).CommandArgument;
        //            string lbtnReadingId = ((LinkButton)sender).CommandName;
        //            hfAccountNo.Value = ((LinkButton)sender).CommandArgument;
        //            hfBillNo.Value = lkbtnEdit;
        //            objCustomerDetailsBe.BillNo = (Convert.ToInt32(lkbtnEdit));
        //            objCustomerDetailsBe.AccountNo = txtccountNo.Text;
        //            objCustomerDetailsBe = objIdsignBal.DeserializeFromXml<CustomerDetailsBe>(objCustomerDetailsBAL.GetBillDetails(objCustomerDetailsBe, ReturnType.Double));
        //            lbtnBillNo.Text = objCustomerDetailsBe.BillNo.ToString();
        //            lblBillNo123.Text = objCustomerDetailsBe.BillNo.ToString();
        //            lbtnBillNo.CommandArgument = lbtnReadingId;
        //            lblPreviousReading.Text = Math.Round(objCustomerDetailsBe.PreviousReading, objCustomerDetailsBe.Decimals).ToString();
        //            txtPresentReading.Text = Math.Round(objCustomerDetailsBe.PresentReading, objCustomerDetailsBe.Decimals).ToString();
        //            txtConsumption.Text = Math.Round(objCustomerDetailsBe.Consumption, objCustomerDetailsBe.Decimals).ToString();
        //            txtFixedCharges.Text = objCustomerDetailsBe.NetEnergyCharges.ToString();
        //            //txtAdditionalCharges.Text = objCustomerDetailsBe.AdditionalCharges.ToString();
        //            objCustomerDetailsBe.AccountNo = txtccountNo.Text;
        //            objCustomerDetailsBe = objIdsignBal.DeserializeFromXml<CustomerDetailsBe>(objCustomerDetailsBAL.GetBillDetails(objCustomerDetailsBe, ReturnType.Fetch));
        //            if (objCustomerDetailsBe.ReadCodeId == 1)
        //            {

        //                BindDirectDetails();
        //            }
        //            else
        //            {
        //                mpeReadCodes.Show();
        //            }

        //            BindUserDetails();
        //        }

        //        else
        //        {
        //            mpeBillInProcess.Show();
        //        }
        //    }
        //catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void BindDirectDetails(string AccountNo, String CustomerBillID)
        {
            try
            {
                CustomerDetailsBe objCustomerDetailsBe = new CustomerDetailsBe();
                CustomerDetailsBAL objCustomerDetailsBAL = new CustomerDetailsBAL();

                XmlDocument objXmlDocument = new XmlDocument();
                CustomerDetailsBillsListBe objCustomerDetailsBillsListBeDirect = new CustomerDetailsBillsListBe();

                objCustomerDetailsBe.AccountNo = AccountNo;
                objCustomerDetailsBe.CustomerBillID = Convert.ToInt32(CustomerBillID);

                objXmlDocument = objCustomerDetailsBAL.GetBillDetails(objCustomerDetailsBe, ReturnType.Double);

                objCustomerDetailsBillsListBeDirect = objIdsignBal.DeserializeFromXml<CustomerDetailsBillsListBe>(objXmlDocument);

                lblDTotalBillAfterAdjustment.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBillsListBeDirect.items[0].AdditionalCharges, 2, Constants.MILLION_Format);
                lblDTotalBillAfterAdjustment_ForCal.Text = objCustomerDetailsBillsListBeDirect.items[0].AdditionalCharges.ToString();
                lblBillNo.Text = objCustomerDetailsBillsListBeDirect.items[0].BillNo;
                lblEnergyCharges.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBillsListBeDirect.items[0].NetEnergyCharges, 2, Constants.MILLION_Format).ToString();
                lblReadingTotalBill.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBillsListBeDirect.items[0].TotalBillAmountWithTax, 2, Constants.MILLION_Format).ToString();
                lblDTariff.Text = objCustomerDetailsBillsListBeDirect.items[0].Tariff;
                lblDTax.Text = objCustomerDetailsBillsListBeDirect.items[0].TaxPercent.ToString();
                lblDConsumption.Text = Math.Round(objCustomerDetailsBillsListBeDirect.items[0].Consumption).ToString();
                lblDConsumptionToAdjust.Text = objCustomerDetailsBillsListBeDirect.items[0].BalanceUsage.ToString();
                lblMeterMultiplier.Text = objCustomerDetailsBillsListBeDirect.items[0].MeterMultiplier.ToString();
                //lblBillType.Text = rdblAdjsutmentType.SelectedItem.Text;
                lblAdjustBillAccountNo.Text = objCustomerDetailsBillsListBeDirect.items[0].AccountNo;


                BindAdditionalCharges(rptrAdditionalChargesAdjust, txtccountNo.Text, Convert.ToInt32(CustomerBillID));
                BindAdditionalCharges(rptrAdditionalchargesshow, txtccountNo.Text, Convert.ToInt32(CustomerBillID));
                DataSet dss = objIdsignBal.ConvertListToDataSet<CustomerDetailsBe>(objCustomerDetailsBillsListBeDirect.items);//taking data in ds to remove first rows which has zero values for FromKW and ToKW.
                dss.Tables[0].Rows.RemoveAt(0);//Removing rows.
                rptrDTariffList.DataSource = dss;//objIdsignBal.ConvertListToDataSet<CustomerDetailsBe>(objCustomerDetailsBillsListBeReading.items);
                rptrDTariffList.DataBind();


                //rptrDTariffList.DataSource = objIdsignBal.ConvertListToDataSet<CustomerDetailsBe>(objCustomerDetailsBillsListBeDirect);
                //txtReadingEnergyCharges.Text=
                //mpeAdditionalChargesAdjustments.Show();

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void dtlBills_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblBillType = (Label)e.Item.FindControl("lblBillType");
                Label lblPreviousReading = (Label)e.Item.FindControl("lblPreviousReading");
                Label lblPresentReading = (Label)e.Item.FindControl("lblPresentReading");
                Label lblConsumption = (Label)e.Item.FindControl("lblConsumption");
                string CustomerBillID = ((Literal)e.Item.FindControl("litCustomerBillID")).Text;
                Label lblNetEnergyCharges = (Label)e.Item.FindControl("lblNetEnergyCharges");
                string lblApprovalStatusId = ((Literal)e.Item.FindControl("lblApprovalStatusId")).Text;
                //Literal lblApprovalStatus = (Literal)e.Item.FindControl("lblApprovalStatus");
                LinkButton lkbtnEdit = (LinkButton)e.Item.FindControl("lkbtnEdit");
                LinkButton lnkBtnDetails = (LinkButton)e.Item.FindControl("lnkBtnDetails");
                HtmlContainerControl divAdj = (HtmlContainerControl)e.Item.FindControl("divAdj");
                HiddenField hfIsAdjusted = (HiddenField)e.Item.FindControl("hfIsAdjusted");
                HiddenField hfReadCodeId = (HiddenField)e.Item.FindControl("hfReadCodeId");
                Label lblTotalAmt = (Label)e.Item.FindControl("lblTotalAmt");
                Label lblTax = (Label)e.Item.FindControl("lblTax");
                Label lblGT = (Label)e.Item.FindControl("lblGT");
                Label lblPaidAmount = (Label)e.Item.FindControl("lblPaidAmount");
                Label lblDueAmount = (Label)e.Item.FindControl("lblDueAmount");


                if (lblPresentReading.Text == "0" && lblPreviousReading.Text == "0")
                {
                    if (lblBillType.Text != "R")
                    {
                        lblPresentReading.Text = lblPreviousReading.Text = "NA";
                    }
                }
                else  //if (Convert.ToInt32(hfReadCodeId.Value) == (int)ReadCode.Read || Convert.ToInt32(hfReadCodeId.Value) == (int)ReadCode.Minimum)
                {
                    lblPresentReading.Text = _ObjCommonMethods.AppendZeros(Convert.ToInt32(lblPresentReading.Text).ToString(), Convert.ToInt32(litMeterDial.Text));
                    lblPreviousReading.Text = _ObjCommonMethods.AppendZeros(Convert.ToInt32(lblPreviousReading.Text).ToString(), Convert.ToInt32(litMeterDial.Text));
                }

                //lblPreviousReading.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblPreviousReading.Text), 2, Constants.INR_Format).ToString();
                //lblPresentReading.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblPresentReading.Text), 2, Constants.INR_Format).ToString();
                //lblPresentReading.Text = string.Format(_ObjCommonMethods.stringFormate(Convert.ToInt32(litMeterDial.Text)), lblPresentReading.Text);
                //lblPreviousReading.Text = string.Format(_ObjCommonMethods.stringFormate(Convert.ToInt32(litMeterDial.Text)), lblPreviousReading.Text);

                //lblConsumption.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblConsumption.Text), 2, Constants.INR_Format).ToString();
                lblNetEnergyCharges.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblNetEnergyCharges.Text), 2, Constants.MILLION_Format).ToString();
                lblTotalAmt.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblTotalAmt.Text), 2, Constants.MILLION_Format).ToString();
                lblTax.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblTax.Text), 2, Constants.MILLION_Format).ToString();
                lblGT.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblGT.Text), 2, Constants.MILLION_Format).ToString();
                lblPaidAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblPaidAmount.Text), 2, Constants.MILLION_Format).ToString();
                lblDueAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblDueAmount.Text), 2, Constants.MILLION_Format).ToString();


                lblConsumption.Text = Math.Round(Convert.ToDecimal(lblConsumption.Text), Convert.ToInt32(hfDecimals.Value)).ToString();
                //lblPresentReading.Text = Math.Round(Convert.ToDecimal(lblPresentReading.Text), Convert.ToInt32(hfDecimals.Value)).ToString();
                //lblConsumption.Text = Math.Round(Convert.ToDecimal(lblConsumption.Text), Convert.ToInt32(hfDecimals.Value)).ToString();

                Repeater rptrAdditionalCharges = (Repeater)e.Item.FindControl("rptrAdditionalCharges");
                BindAdditionalCharges(rptrAdditionalCharges, txtccountNo.Text, Convert.ToInt32(CustomerBillID));

                if (string.Compare(hfIsAdjusted.Value, true.ToString(), true) == 0)
                {
                    lnkBtnDetails.Visible = true;
                    // divAdj.Visible = true;
                }

                //if (lblApprovalStatusId == "0")
                //{
                //    divAdj.Visible = true;
                //    //lblApprovalStatus.Visible = true;
                //    //lblApprovalStatus.Text = Resources.Resource.BILL_IN_ADJ_PROCESS;
                //    lkbtnEdit.Enabled = false;
                //    lkbtnEdit.ToolTip = Resources.Resource.BILL_IN_ADJ_PROCESS;
                //    lnkBtnDetails.Visible = true;
                //}
            }
        }

        protected void rptrAdditionalCharges_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                Label lblChargeName = (Label)e.Item.FindControl("lblChargeName");
                Label lblAmount = (Label)e.Item.FindControl("lblAmount");
                //lblChargeName.Text = objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblChargeName.Text), 2, Constants.INR_Format).ToString();
                lblAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblAmount.Text), 2, Constants.MILLION_Format).ToString();

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void dtDirectCustomer_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //Label lblPreviousReading = (Label)e.Item.FindControl("lblPreviousReading");
                //Label lblPresentReading = (Label)e.Item.FindControl("lblPresentReading");
                Label lblDirectNetEnergyCharges = (Label)e.Item.FindControl("lblDirectNetEnergyCharges");
                lblDirectNetEnergyCharges.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblDirectNetEnergyCharges.Text), 2, Constants.MILLION_Format).ToString();

                Label lblBillno = (Label)e.Item.FindControl("lblBillno");
                Literal litCustomerBillID = (Literal)e.Item.FindControl("litCustomerBillID");
                Repeater rptrAdditionalCharges = (Repeater)e.Item.FindControl("rptrDirectCustomer");
                BindAdditionalCharges(rptrAdditionalCharges, txtccountNo.Text, Convert.ToInt32(litCustomerBillID.Text));

            }
        }

        protected void rptrDirectCustomer_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                //Label lblChargeName = (Label)e.Item.FindControl("lblChargeName");
                Label lblAmount = (Label)e.Item.FindControl("lblDirectAmount");
                //lblChargeName.Text = objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblChargeName.Text), 2, Constants.INR_Format).ToString();
                lblAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblAmount.Text), 2, Constants.MILLION_Format).ToString();

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindAdditionalCharges(Repeater rptr, string AccountNo, int CustomerBillID)
        {
            CustomerDetailsBe objCustomerDetailsBe = new CustomerDetailsBe();
            CustomerDetailsBAL objCustomerDetailsBAL = new CustomerDetailsBAL();
            CustomerDetailsBillsListBe objCustomerDetailsBillsListBe = new CustomerDetailsBillsListBe();
            objCustomerDetailsBe.AccountNo = AccountNo;
            objCustomerDetailsBe.CustomerBillID = CustomerBillID;
            XmlDocument xml = new XmlDocument();
            xml = objCustomerDetailsBAL.GetBillDetails(objCustomerDetailsBe, ReturnType.Get);
            objCustomerDetailsBillsListBe = objIdsignBal.DeserializeFromXml<CustomerDetailsBillsListBe>(xml);
            rptr.DataSource = objCustomerDetailsBillsListBe.items;
            rptr.DataBind();
        }

        protected void rptrAdditionalchargesshow_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                Label lblReadingAdditionalCharges = (Label)e.Item.FindControl("lblReadingAdditionalCharges");
                Label lblChargeNameTaxAmount = (Label)e.Item.FindControl("lblChargeNameTaxAmount");
                Label lblChargeNamePreviousAdjustAmount = (Label)e.Item.FindControl("lblChargeNamePreviousAdjustAmount");
                Label lblChargeNamePreviousAdjustTaxAmount = (Label)e.Item.FindControl("lblChargeNamePreviousAdjustTaxAmount");
                Label lblVat = (Label)e.Item.FindControl("lblVat");

                if (!string.IsNullOrEmpty(lblReadingAdditionalCharges.Text))
                    lblReadingAdditionalCharges.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblReadingAdditionalCharges.Text), 2, Constants.MILLION_Format).ToString();
                else
                    lblReadingAdditionalCharges.Text = "0.00";

                if (!string.IsNullOrEmpty(lblChargeNamePreviousAdjustAmount.Text))
                    lblChargeNamePreviousAdjustAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblChargeNamePreviousAdjustAmount.Text), 2, Constants.MILLION_Format).ToString();
                else
                    lblChargeNamePreviousAdjustAmount.Text = "0.00";

                if (!string.IsNullOrEmpty(lblVat.Text))
                {
                    if (!string.IsNullOrEmpty(lblReadingAdditionalCharges.Text))
                        lblChargeNameTaxAmount.Text = _ObjCommonMethods.GetCurrencyFormat((Convert.ToDecimal(lblReadingAdditionalCharges.Text) * Convert.ToDecimal(lblVat.Text)) / 100, 2, Constants.MILLION_Format).ToString();
                    else
                        lblChargeNameTaxAmount.Text = "0.00";

                    if (!string.IsNullOrEmpty(lblChargeNamePreviousAdjustAmount.Text))
                        lblChargeNamePreviousAdjustTaxAmount.Text = _ObjCommonMethods.GetCurrencyFormat((Convert.ToDecimal(lblChargeNamePreviousAdjustAmount.Text) * Convert.ToDecimal(lblVat.Text)) / 100, 2, Constants.MILLION_Format).ToString();
                    else
                        lblChargeNamePreviousAdjustTaxAmount.Text = "0.00";
                }
                else
                {
                    lblChargeNameTaxAmount.Text = "0.00";
                    lblChargeNamePreviousAdjustTaxAmount.Text = "0.00";
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnGetDetails_Click(object sender, EventArgs e)
        {
            if (rdblAdjsutmentType.SelectedValue == Constants.Reading_Adjustment)
            {
                mpeReadingAdjustment.Show();
            }
            else if (rdblAdjsutmentType.SelectedValue == Constants.Bill_Adjustment)
            {
                BindDirectDetails(lblAccountno.Text, hdnCustomerBillID.Value);
            }
            else if (rdblAdjsutmentType.SelectedValue == Constants.Consumption_Adjustment)
            {
                txtConsumptionCA.Text = "";
                lblConsumptionAfterAdjst.Text = "0";
                txtConsumptionCA.Focus();

                mpeConsumptionAdjustment.Show();
            }
        }

        protected void dtlBills_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                txtAdditionalReason.Text = txtMeterReson.Text = txtBillAdjustReason.Text = string.Empty;

                if (e.CommandName == "getBADetails")
                {
                    BillAdjustmentsBe _objBillAdjustmentsBe = new BillAdjustmentsBe();
                    XmlDocument xml = new XmlDocument();

                    DataSet ds = new DataSet();
                    ds.ReadXml(Server.MapPath(ConfigurationSettings.AppSettings["AdjustmentTypes"]));

                    _objBillAdjustmentsBe.BillAdjustmentId = Convert.ToInt32(e.CommandArgument);
                    xml = _objBillAdjustmentBal.GetAdjustmentDetails(_objBillAdjustmentsBe, ReturnType.Fetch);
                    _objBillAdjustmentsBe = objIdsignBal.DeserializeFromXml<BillAdjustmentsBe>(xml);
                    hfBillAdjustmentId.Value = _objBillAdjustmentsBe.BillAdjustmentId.ToString();
                    lblAdjustedAmount.Text = _ObjCommonMethods.GetCurrencyFormat(_objBillAdjustmentsBe.AmountEffected, 2, Constants.MILLION_Format).ToString();
                    lblTaxAdjusted.Text = _ObjCommonMethods.GetCurrencyFormat(_objBillAdjustmentsBe.TaxEffected, 2, Constants.MILLION_Format).ToString();
                    lblTotalAdjustment.Text = _ObjCommonMethods.GetCurrencyFormat(_objBillAdjustmentsBe.TotalAmountEffected, 2, Constants.MILLION_Format).ToString();
                    lblAdjustmentType.Text = (from x in ds.Tables[0].AsEnumerable()
                                              where x.Field<string>("value").Equals(_objBillAdjustmentsBe.BillAdjustmentType.ToString())
                                              select x.Field<string>("name")).Single();
                    lblTotalAmoutEffected.Text = _ObjCommonMethods.GetCurrencyFormat(_objBillAdjustmentsBe.TotalAmountEffected, 2, Constants.MILLION_Format).ToString();


                    if (_objBillAdjustmentsBe.AdjustedUnits > 0)
                    {
                        lblAdjustedUnits.Text = string.Format(Resource.ADJUSTEMENT_VIEW_UNITS, _objBillAdjustmentsBe.AdjustedUnits);
                    }
                    else lblAdjustedUnits.Text = string.Empty;
                    mpeBADetails.Show();


                    //CustomerDetailsBillsListBe objCustomerDetailsBillsListBe = new CustomerDetailsBillsListBe();
                    //CustomerDetailsBe objCustomerDetailsBe = new CustomerDetailsBe();
                    //_ObjCustomerDetailsBe.BAID = Convert.ToInt32(e.CommandArgument);
                    //_ObjCustomerDetailsBe.AccountNo = lblAccountno.Text;
                    //_objXmlDocument = _ObjCustomerDetailsBAL.GetBillAdjustmentDetailsBAL(_ObjCustomerDetailsBe);
                    //objCustomerDetailsBillsListBe = objIdsignBal.DeserializeFromXml<CustomerDetailsBillsListBe>(_objXmlDocument);

                    //if (objCustomerDetailsBillsListBe.items[0].BATID == (int)ReadCode.Direct)
                    //{
                    //    if (objCustomerDetailsBillsListBe.items.Count > 0)
                    //    {
                    //        divMeterAdjustmentDetails.Visible = true;
                    //        divBillAdjustmentDetails.Visible = false;
                    //        lblMInfoTypeOfAdjust.Text = objCustomerDetailsBillsListBe.items[0].Name;
                    //        lblMInfoBillNo.Text = objCustomerDetailsBillsListBe.items[0].BillNo;
                    //        lblMInfoPreReading.Text = objCustomerDetailsBillsListBe.items[0].PreviousReading.ToString(_ObjCommonMethods.stringFormate(objCustomerDetailsBillsListBe.items[0].MeterDials));
                    //        lblMInfoCurrentReading.Text = objCustomerDetailsBillsListBe.items[0].PresentReading.ToString(_ObjCommonMethods.stringFormate(objCustomerDetailsBillsListBe.items[0].MeterDials));
                    //        lblMInfoNetEnergyCharges.Text = objCustomerDetailsBillsListBe.items[0].NetEnergyCharges.ToString("00.00");
                    //    }
                    //}
                    //else if (objCustomerDetailsBillsListBe.items[0].BATID == (int)ReadCode.Read)
                    //{
                    //    if (objCustomerDetailsBillsListBe.items.Count > 0)
                    //    {
                    //        divMeterAdjustmentDetails.Visible = false;
                    //        divBillAdjustmentDetails.Visible = true;
                    //        lblInfoBillNo.Text = objCustomerDetailsBillsListBe.items[0].BillNo;
                    //        lblInfoEneryCharges.Text = objCustomerDetailsBillsListBe.items[0].NetEnergyCharges.ToString("00.00");
                    //        lblTypeOfAdjust.Text = objCustomerDetailsBillsListBe.items[0].Name;
                    //        rptAdditionalChargesDetails.DataSource = objCustomerDetailsBillsListBe.items;
                    //        rptAdditionalChargesDetails.DataBind();
                    //    }
                    //}
                }
                else
                {

                    string BillMonth = ((LinkButton)e.Item.FindControl("litBillMonth")).Text;// sender).Text;
                    string BillYear = ((LinkButton)e.Item.FindControl("litBillYear")).Text;

                    BillingMonthOpenBE _objBillingMonthOpenBE = new BillingMonthOpenBE();
                    _objBillingMonthOpenBE.Year = Convert.ToInt32(BillYear);
                    _objBillingMonthOpenBE.Month = Convert.ToInt32(BillMonth);
                    XmlDocument xml = _objBillingMonthOpenBAL.GetBillingMonth(_objBillingMonthOpenBE, ReturnType.Single);
                    _objBillingMonthOpenBE = objIdsignBal.DeserializeFromXml<BillingMonthOpenBE>(xml);
                    //if (Convert.ToInt32(hfMonthStatusCount.Value) <= 1)//Raja
                    HiddenField hfReadCodeId = ((HiddenField)e.Item.FindControl("hfReadCodeId"));
                    string CustomerBillId = ((Literal)e.Item.FindControl("litCustomerBillID")).Text;
                    string CustomerID = ((LinkButton)e.Item.FindControl("litCustomerID")).Text;
                    hdnCustomerBillID.Value = CustomerBillId;

                    //if (IsBillAdjested())
                    //{
                    //    lblProcess.Text = Resource.BILL_ADJESTED_MESSAGE;
                    //    mpeBillInProcess.Show();//dont Modified this Modal PopUP design,this is used for another scenarios also
                    //}
                    //else
                    //{
                    if (string.Compare(((HiddenField)e.Item.FindControl("hfIsLatestBill")).Value, true.ToString(), true) == 0)//suresh
                    {
                        //if (!_objBillingMonthOpenBE.IsActiveMonth)// Month Closed Checking is Start
                        {
                            DataSet ds = new DataSet();
                            ds.ReadXml(Server.MapPath(ConfigurationSettings.AppSettings["AdjustmentTypes"]));
                            objIdsignBal.FillRadioButtonList(ds, rdblAdjsutmentType, "name", "value");



                            MastersBE objMastersBE = new MastersBE();
                            objMastersBE.AccountNo = lblAccountno.Text;// hfAccountNo.Value;
                            objMastersBE.BillMonth = BillMonth;
                            objMastersBE.BillYear = BillYear;
                            objMastersBE.Flag = Convert.ToInt16(Resource.FLAG_BILL_ADJUSTMENT);
                            objMastersBE = _ObjCommonMethods.CustomerBillProcessDetails(objMastersBE);
                            if (!objMastersBE.Status)
                            {//2
                                //if (Convert.ToInt32(hfReadCodeId.Value) == (int)ReadCode.Read)
                                //{
                                //rdblAdjsutmentType.Items[rdblAdjsutmentType.Items.IndexOf(rdblAdjsutmentType.Items.FindByValue("1"))].Selected = true;//---------

                                //if (objMastersBE.BillOpenStatusID == 1) // Raja-ID065 Month Open Status Id checking if more than Billed Month Schedule locks are in open mode 
                                //{                                  

                                hdnCustomerBillID.Value = CustomerBillId;
                                hdnCustomerID.Value = CustomerID;
                                CustomerDetailsBe objCustomerDetailsBe = new CustomerDetailsBe();
                                CustomerDetailsBAL objCustomerDetailsBAL = new CustomerDetailsBAL();
                                XmlDocument objXmlDocument = new XmlDocument();
                                CustomerDetailsBillsListBe objCustomerDetailsBillsListBeReading = new CustomerDetailsBillsListBe();
                                string lkbtnEdit = ((LinkButton)e.Item.FindControl("lkbtnEdit")).CommandArgument;
                                string lbtnReadingId = ((LinkButton)e.Item.FindControl("lkbtnEdit")).CommandName;
                                hfAccountNo.Value = ((LinkButton)e.Item.FindControl("lkbtnEdit")).CommandArgument;

                                hfBillNo.Value = lkbtnEdit;
                                objCustomerDetailsBe.CustomerBillID = Convert.ToInt32(hdnCustomerBillID.Value);
                                objCustomerDetailsBe.AccountNo = lblAccountno.Text;//txtccountNo.Text;

                                objXmlDocument = objCustomerDetailsBAL.GetBillDetails(objCustomerDetailsBe, ReturnType.Double);

                                objCustomerDetailsBillsListBeReading = objIdsignBal.DeserializeFromXml<CustomerDetailsBillsListBe>(objXmlDocument);
                                //objCustomerDetailsBe = objIdsignBal.DeserializeFromXml<CustomerDetailsBe>(objCustomerDetailsBAL.GetBillDetails(objCustomerDetailsBe, ReturnType.Double));
                                DataSet dss = null;
                                if (objCustomerDetailsBillsListBeReading.items.Count > 0)
                                {
                                    #region Meter Reading Adjustment Details

                                    lbtnBillNo.Text = objCustomerDetailsBillsListBeReading.items[0].BillNo.ToString();
                                    lblBillNo123.Text = objCustomerDetailsBillsListBeReading.items[0].BillNo.ToString();
                                    lbtnBillNo.CommandArgument = lbtnReadingId;
                                    //lblPreviousReading.Text = Math.Round(objCustomerDetailsBillsListBeReading.items[0].PreviousReading, objCustomerDetailsBe.Decimals).ToString(_ObjCommonMethods.stringFormate(objCustomerDetailsBillsListBeReading.items[0].MeterDials));//Commented By suresh
                                    //txtPresentReading.Text = Math.Round(objCustomerDetailsBillsListBeReading.items[0].PresentReading, objCustomerDetailsBe.Decimals).ToString(_ObjCommonMethods.stringFormate(objCustomerDetailsBillsListBeReading.items[0].MeterDials));//Commented By suresh
                                    lblPreviousReading.Text = objCustomerDetailsBillsListBeReading.items[0].PreviousReading.ToString();//By suresh
                                    lblPreviousReading.Text = _ObjCommonMethods.AppendZeros(lblPreviousReading.Text, Convert.ToInt32(litMeterDial.Text));
                                    txtPresentReading.Text = hfReadingBeforeAdjust.Value = objCustomerDetailsBillsListBeReading.items[0].PresentReading.ToString();//By suresh
                                    txtPresentReading.Text = _ObjCommonMethods.AppendZeros(txtPresentReading.Text, Convert.ToInt32(litMeterDial.Text));
                                    txtPresentReading.MaxLength = Convert.ToInt16(litMeterDial.Text);
                                    lblRConsumption.Text = Math.Round(objCustomerDetailsBillsListBeReading.items[0].Consumption, objCustomerDetailsBe.Decimals).ToString();
                                    lblConsumptionToAdjust.Text = objCustomerDetailsBillsListBeReading.items[0].BalanceUsage.ToString();
                                    //lblEnergyChargesBfrAdjsut.Text = objCustomerDetailsBillsListBeReading.items[0].NetEnergyCharges.ToString("00.00");
                                    lblEnergyChargesBfrAdjsut.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBillsListBeReading.items[0].NetEnergyCharges, 2, Constants.MILLION_Format);
                                    //lblTaxPercent.Text = objCustomerDetailsBillsListBeReading.items[0].TaxPercent.ToString("0.00");
                                    lblTaxPercent.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBillsListBeReading.items[0].TaxPercent, 2, Constants.MILLION_Format);
                                    lblTarif.Text = objCustomerDetailsBillsListBeReading.items[0].Tariff;
                                    lblTotalBill.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBillsListBeReading.items[0].TotalBillAmountWithTax, 2, Constants.MILLION_Format).ToString();
                                    //lblTotalAfterAdjustment.Text = objCustomerDetailsBillsListBeReading.items[0].TotalBillAmountWithTax.ToString("00.00");
                                    lblTotalAfterAdjustment.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBillsListBeReading.items[0].TotalBillAmountWithTax, 2, Constants.MILLION_Format);
                                    //txtAdditionalCharges.Text = objCustomerDetailsBe.AdditionalCharges.ToString();
                                    objCustomerDetailsBe.AccountNo = lblAccountno.Text;//txtccountNo.Text;
                                    // lblTaxEffected.Text = (objCustomerDetailsBillsListBeReading.items[0].TotalBillAmountWithTax - objCustomerDetailsBillsListBeReading.items[0].TotalBillAmount).ToString("00.00");
                                    //lblTaxEffected.Text = objCustomerDetailsBillsListBeReading.items[0].Vat.ToString("00.00");
                                    lblTaxEffected.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBillsListBeReading.items[0].Vat, 2, Constants.MILLION_Format);
                                    //objCustomerDetailsBe.BillNo = txtDocNo.Text.Trim();
                                    dss = objIdsignBal.ConvertListToDataSet<CustomerDetailsBe>(objCustomerDetailsBillsListBeReading.items);//taking data in ds to remove first rows which has zero values for FromKW and ToKW.
                                    dss.Tables[0].Rows.RemoveAt(0);//Removing rows.
                                    rptrRTariffList.DataSource = dss;//objIdsignBal.ConvertListToDataSet<CustomerDetailsBe>(objCustomerDetailsBillsListBeReading.items);
                                    rptrRTariffList.DataBind();

                                    #endregion

                                    #region Consumption Adjustment Details
                                    lbtnBillNoCA.Text = objCustomerDetailsBillsListBeReading.items[0].BillNo.ToString();
                                    lblBillNoCA.Text = objCustomerDetailsBillsListBeReading.items[0].BillNo.ToString();
                                    lblGloabalAccountNumber.Text = objCustomerDetailsBillsListBeReading.items[0].AccountNo;
                                    lbtnBillNoCA.CommandArgument = lbtnReadingId;
                                    //lblPreviousReading.Text = Math.Round(objCustomerDetailsBillsListBeReading.items[0].PreviousReading, objCustomerDetailsBe.Decimals).ToString(_ObjCommonMethods.stringFormate(objCustomerDetailsBillsListBeReading.items[0].MeterDials));//Commented By suresh
                                    //txtPresentReading.Text = Math.Round(objCustomerDetailsBillsListBeReading.items[0].PresentReading, objCustomerDetailsBe.Decimals).ToString(_ObjCommonMethods.stringFormate(objCustomerDetailsBillsListBeReading.items[0].MeterDials));//Commented By suresh
                                    lblPreviousReadingCA.Text = objCustomerDetailsBillsListBeReading.items[0].PreviousReading.ToString();//By suresh
                                    lblPreviousReadingCA.Text = _ObjCommonMethods.AppendZeros(lblPreviousReading.Text, Convert.ToInt32(litMeterDial.Text));
                                    lblPresentReadionCA.Text = hfReadingBeforeAdjust.Value = objCustomerDetailsBillsListBeReading.items[0].PresentReading.ToString();//By suresh
                                    lblCurrentConsumption.Text = Math.Round(objCustomerDetailsBillsListBeReading.items[0].Consumption, objCustomerDetailsBe.Decimals).ToString();
                                    lblTotalAdjustedConsumption.Text = Math.Round(objCustomerDetailsBillsListBeReading.items[0].GrandTotal, objCustomerDetailsBe.Decimals).ToString();

                                    decimal tariffRate = (objCustomerDetailsBillsListBeReading.items[0].Consumption == 0) ? 0 : (objCustomerDetailsBillsListBeReading.items[0].NetEnergyCharges / objCustomerDetailsBillsListBeReading.items[0].Consumption);
                                    decimal previousAdjustment = (objCustomerDetailsBillsListBeReading.items[0].GrandTotal * tariffRate);

                                    lblREnergyChargesCA.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBillsListBeReading.items[0].NetEnergyCharges - previousAdjustment, 2, Constants.MILLION_Format);
                                    lblREnergyChargesCAForCal.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBillsListBeReading.items[0].NetEnergyCharges, 2, Constants.MILLION_Format);
                                    lblTaxPercentCA.Text = objCustomerDetailsBillsListBeReading.items[0].TaxPercent.ToString("0.00");
                                    lblTarifCA.Text = objCustomerDetailsBillsListBeReading.items[0].Tariff;
                                    lblMultiplierCA.Text = objCustomerDetailsBillsListBeReading.items[0].MeterMultiplier;
                                    lblTotalBillCA.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBillsListBeReading.items[0].TotalBillAmountWithTax, 2, Constants.MILLION_Format).ToString();
                                    lblTotalAfterAdjustmentCA.Text = lblTotalAfterAdjustmentCAForCal.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBillsListBeReading.items[0].AdditionalCharges, 2, Constants.MILLION_Format);
                                    lblConsumptionToAdjsutCA.Text = objCustomerDetailsBillsListBeReading.items[0].BalanceUsage.ToString();
                                    lblConsumptionAfterAdjst.Text = Math.Round(objCustomerDetailsBillsListBeReading.items[0].Consumption - objCustomerDetailsBillsListBeReading.items[0].GrandTotal).ToString();
                                    //txtAdditionalCharges.Text = objCustomerDetailsBe.AdditionalCharges.ToString();
                                    lblAdjustedAmountCA.Text = lblAdjustedAmountCAForCal.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBillsListBeReading.items[0].TotalBillAmountWithTax - objCustomerDetailsBillsListBeReading.items[0].AdditionalCharges, 2, Constants.MILLION_Format);
                                    rptrRTariffListCA.DataSource = dss;//objIdsignBal.ConvertListToDataSet<CustomerDetailsBe>(objCustomerDetailsBillsListBeReading.items);
                                    rptrRTariffListCA.DataBind();

                                    #endregion
                                }
                                XmlDocument xmlR = objCustomerDetailsBAL.GetBillDetails(objCustomerDetailsBe, ReturnType.Fetch);
                                objCustomerDetailsBe = objIdsignBal.DeserializeFromXml<CustomerDetailsBe>(xmlR);
                                if (objCustomerDetailsBe.ReadCodeId == (int)ReadCode.Direct)
                                {
                                    string AdjustmentType = objIdsignBal.Decrypt(Request.QueryString[Constants.QS_AdjustmentTypeid]);
                                    BindDirectDetails(lblAccountno.Text, CustomerBillId);
                                    if (AdjustmentType == Constants.Additional_Charges_Adjustment)
                                    {
                                        txtConsumptionCA.Text = "";
                                        //lblConsumptionAfterAdjst.Text = "0";
                                        txtConsumptionCA.Focus();

                                        mpeAdditionalChargesAdjustments.Show();
                                    }
                                    else if (AdjustmentType == Constants.Bill_Adjustment)
                                    {
                                        mpeBillAdjustments.Show();
                                    }
                                    else if (AdjustmentType == Constants.Consumption_Adjustment)
                                    {
                                        txtConsumptionCA.Text = "";
                                        //lblConsumptionAfterAdjst.Text = "0";
                                        txtConsumptionCA.Focus();

                                        mpeConsumptionAdjustment.Show();
                                    }
                                    else if (AdjustmentType == Constants.Reading_Adjustment)
                                    {
                                        lblGenMessage.Text = Resource.READ_ADJUST_NOT_PSBL;
                                        mpeGenMessage.Show();
                                    }
                                }
                                else
                                {
                                    #region Adjustment Windows old req
                                    //mpeReadCodes.Show();
                                    #endregion

                                    #region Directly providing adjustment window as per new req

                                    string AdjustmentType = objIdsignBal.Decrypt(Request.QueryString[Constants.QS_AdjustmentTypeid]);
                                    if (AdjustmentType == Constants.Reading_Adjustment)
                                    {
                                        mpeReadingAdjustment.Show();
                                    }
                                    else if (AdjustmentType == Constants.Additional_Charges_Adjustment)
                                    {
                                        BindDirectDetails(lblAccountno.Text, hdnCustomerBillID.Value);
                                        mpeAdditionalChargesAdjustments.Show();
                                    }
                                    else if (AdjustmentType == Constants.Consumption_Adjustment)
                                    {
                                        txtConsumptionCA.Text = "";
                                        //lblConsumptionAfterAdjst.Text = "0";
                                        txtConsumptionCA.Focus();

                                        mpeConsumptionAdjustment.Show();
                                    }
                                    else if (AdjustmentType == Constants.Bill_Adjustment)
                                    {
                                        mpeBillAdjustments.Show();
                                    }
                                    #endregion
                                }

                                //BindUserDetails();
                                //}
                                //else
                                //{
                                //    mpeBillClosed.Show();
                                //}
                                //}//1
                                //else
                                //{
                                if (Convert.ToInt32(hfReadCodeId.Value) != (int)ReadCode.Read)
                                {
                                    //rdblAdjsutmentType.Items[rdblAdjsutmentType.Items.IndexOf(rdblAdjsutmentType.Items.FindByValue("1"))].Enabled = false;
                                    rdblAdjsutmentType.Items[rdblAdjsutmentType.Items.IndexOf(rdblAdjsutmentType.Items.FindByValue("2"))].Selected = true;
                                    rdblAdjsutmentType.Items.RemoveAt(0);
                                    // mpeReadCodes.Show();
                                    //Label1.Text = Resource.BILL_ADJUSTMENTFORESTIMATE_MESSAGE;
                                    //mpeBillClosed.Show();
                                }//}
                            }//2
                            else
                            {
                                lblProcess.Text = Resource.BILL_IN_PROCESS_ADJUSTMENTS;
                                mpeBillInProcess.Show();//dont Modified this Modal PopUP design,this is used for another scenarios also
                            }


                        }// Month Closed Checking End
                        //else
                        //{
                        //    Label1.Text = Resource.ADJUST_MONTH_CLOSE_MESSAGE;
                        //    mpeBillClosed.Show();
                        //}
                    }
                    else
                    {
                        Label1.Text = Resource.BILL_ADJUSTMENTFORLATESET_MESSAGE;
                        mpeBillClosed.Show();
                    }
                    //}
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnUpdateCharges_Click(object sender, EventArgs e)           //Update energy charges
        {
            try
            {
                bool IsApproval = false;
                IsApproval = _ObjCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.BillAdjustment, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());

                DataTable dt = new DataTable();
                dt.Columns.Add("ChargeID", typeof(int));
                dt.Columns.Add("AmountEffected", typeof(decimal));
                decimal adjustedAmount = 0;
                decimal TaxEffected = 0;
                decimal actualAmount = 0;
                foreach (RepeaterItem item in rptrAdditionalChargesAdjust.Items)
                {
                    LinkButton lbtnChargeName = (LinkButton)item.FindControl("lbtnChargeName");
                    TextBox txtAdditionalCharges = (TextBox)item.FindControl("txtReadingAdditionalCharges");
                    decimal adjusted = (string.IsNullOrEmpty(txtAdditionalCharges.Text.Replace(",", string.Empty)) ? 0 : Convert.ToDecimal(txtAdditionalCharges.Text.Replace(",", string.Empty)));
                    decimal actual = (string.IsNullOrEmpty(lbtnChargeName.CommandArgument) ? 0 : Convert.ToDecimal(lbtnChargeName.CommandArgument));
                    adjustedAmount = adjustedAmount + adjusted;
                    actualAmount = actualAmount + actual;
                    //if (adjusted != actual)
                    dt = GetEffectedBillDetails(dt, Convert.ToInt32(lbtnChargeName.CommandName), adjusted);
                }

                //TaxEffected = Math.Round((adjustedAmount) * (Constants.Vat / 100), 2);
                decimal vat = (!string.IsNullOrEmpty(lblDTax.Text.Trim())) ? Convert.ToDecimal(lblDTax.Text.Trim()) : 0;
                TaxEffected = Math.Round((adjustedAmount) * (vat / 100), 2);
                BillAdjustmentsBe _objBillAdjustmentsBe = new BillAdjustmentsBe();
                _objBillAdjustmentsBe.BatchNo = Convert.ToInt32(objIdsignBal.Decrypt(Request.QueryString[Constants.QS_BatchID]));
                _objBillAdjustmentsBe.AccountNo = lblAdjustBillAccountNo.Text;
                _objBillAdjustmentsBe.BillAdjustmentType = Convert.ToInt32(objIdsignBal.Decrypt(Request.QueryString[Constants.QS_AdjustmentTypeid])); ;
                _objBillAdjustmentsBe.CustomerBillId = string.IsNullOrEmpty(hdnCustomerBillID.Value) ? 0 : Convert.ToInt32(hdnCustomerBillID.Value);
                //_objBillAdjustmentsBe.AmountEffected = actualAmount - adjustedAmount;
                _objBillAdjustmentsBe.AmountEffected = adjustedAmount;
                _objBillAdjustmentsBe.TaxEffected = TaxEffected;
                _objBillAdjustmentsBe.Remarks = objIdsignBal.ReplaceNewLines(txtAdditionalReason.Text, true);
                //_objBillAdjustmentsBe.TotalAmountEffected = _objBillAdjustmentsBe.AmountEffected + TaxEffected;
                _objBillAdjustmentsBe.TotalAmountEffected = adjustedAmount + TaxEffected;
                _objBillAdjustmentsBe.EnergryCharges = (string.IsNullOrEmpty(lblEnergyCharges.Text.Replace(",", string.Empty)) ? 0 : Convert.ToDecimal(lblEnergyCharges.Text.Replace(",", string.Empty)));
                _objBillAdjustmentsBe.AdditionalCharges = actualAmount;

                if (CheckBatchTotal(_objBillAdjustmentsBe.TotalAmountEffected))
                {

                    if (!IsApproval)
                    {
                        _objBillAdjustmentsBe.ApprovalStatusId = (int)EnumApprovalStatus.Process;
                        pop.Attributes.Add("class", "popheader popheaderlblred");
                        lblActiveMsg.Text = Resource.APPROVAL_TARIFF_CHANGE;
                    }
                    else
                    {
                        _objBillAdjustmentsBe.ApprovalStatusId = (int)EnumApprovalStatus.Approved;
                        _objBillAdjustmentsBe.IsFinalApproval = true;
                        pop.Attributes.Add("class", "popheader popheaderlblgreen");
                        lblActiveMsg.Text = Resource.MODIFICATIONS_UPDATED;
                    }

                    _objBillAdjustmentsBe.FunctionId = (int)EnumApprovalFunctions.BillAdjustment;
                    _objBillAdjustmentsBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    _objBillAdjustmentsBe.BatchID = Convert.ToInt32(lblBatchNo.Text);
                    _objBillAdjustmentsBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    XmlDocument xml = _objBillAdjustmentBal.InsertAdjustmentDetails(_objBillAdjustmentsBe, dt, Statement.Insert);
                    _objBillAdjustmentsBe = objIdsignBal.DeserializeFromXml<BillAdjustmentsBe>(xml);

                    if (IsApproval)
                    {
                        SendMailAndMsg(lblAdjustBillAccountNo.Text);
                    }

                    if (_objBillAdjustmentsBe.IsSuccess)
                    {
                        mpeActivate.Show();
                    }
                    else if (_objBillAdjustmentsBe.IsApprovalInProcess)
                    {
                        lblActiveMsg.Text = Resource.REQ_STILL_PENDING;
                        mpeActivate.Show();
                    }

                    BindUserDetails();
                    GetBatchDetails();
                }
                else
                {
                    lblGenMessage.Text = Resource.ADJUST_INCRS_BATCH_LMT;
                    mpeGenMessage.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnBillAdjustmentsUpdate_Click(object sender, EventArgs e)   //Bill Adjustment Neeraj-ID077 25-Aug
        {
            try
            {
                //bool IsApproval = false;
                //IsApproval = _ObjCommonMethods.IsApproval(Resource.FEATURE_BILL_ADJUSTMENT);// Will check approval is required or not for this feature.

                decimal AmountEffected = 0;
                //if (txtCredit.Text != string.Empty)
                //    AmountEffected = Convert.ToDecimal(txtCredit.Text);
                //else
                //    AmountEffected = Convert.ToDecimal(txtDebit.Text) * -1;
                if (rdoAdjustmentTypeBA.SelectedItem.Value == "1")
                    AmountEffected = Convert.ToDecimal(txtCrdDbtAmount.Text);
                else if (rdoAdjustmentTypeBA.SelectedItem.Value == "2")
                    AmountEffected = Convert.ToDecimal(txtCrdDbtAmount.Text) * -1;
                XmlDocument xml = new XmlDocument();

                BillAdjustmentsBe _objBillAdjustmentsBe = new BillAdjustmentsBe();
                _objBillAdjustmentsBe.BatchNo = Convert.ToInt32(objIdsignBal.Decrypt(Request.QueryString[Constants.QS_BatchID]));
                _objBillAdjustmentsBe.AccountNo = lblAccountno.Text;
                _objBillAdjustmentsBe.Remarks = objIdsignBal.ReplaceNewLines(txtBillAdjustReason.Text, true);
                _objBillAdjustmentsBe.BillAdjustmentType = Convert.ToInt32(objIdsignBal.Decrypt(Request.QueryString[Constants.QS_AdjustmentTypeid]));
                _objBillAdjustmentsBe.CustomerBillId = string.IsNullOrEmpty(hdnCustomerBillID.Value) ? 0 : Convert.ToInt32(hdnCustomerBillID.Value);
                _objBillAdjustmentsBe.AmountEffected = AmountEffected;
                _objBillAdjustmentsBe.TotalAmountEffected = AmountEffected;

                //if (CheckBatchTotal(AmountEffected))
                //{
                bool IsFinalApproval = false;
                IsFinalApproval = _ObjCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.BillAdjustment, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());

                if (IsFinalApproval)
                {
                    _objBillAdjustmentsBe.ApprovalStatusId = (int)EnumApprovalStatus.Approved;
                    _objBillAdjustmentsBe.IsFinalApproval = true;
                    pop.Attributes.Add("class", "popheader popheaderlblgreen");
                    lblActiveMsg.Text = Resource.MODIFICATIONS_UPDATED;
                }
                else
                {
                    _objBillAdjustmentsBe.ApprovalStatusId = (int)EnumApprovalStatus.Process;
                    pop.Attributes.Add("class", "popheader popheaderlblred");
                    lblActiveMsg.Text = Resource.APPROVAL_TARIFF_CHANGE;

                }

                _objBillAdjustmentsBe.FunctionId = (int)EnumApprovalFunctions.BillAdjustment;
                _objBillAdjustmentsBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objBillAdjustmentsBe.BatchID = Convert.ToInt32(lblBatchNo.Text);
                _objBillAdjustmentsBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                xml = _objBillAdjustmentBal.BillAdjustmentBAL(_objBillAdjustmentsBe);
                _objBillAdjustmentsBe = objIdsignBal.DeserializeFromXml<BillAdjustmentsBe>(xml);

                if (IsFinalApproval)
                {
                    SendMailAndMsg(lblAccountno.Text);
                }

                if (_objBillAdjustmentsBe.IsSuccess)
                {
                    mpeActivate.Show();
                }
                else if (_objBillAdjustmentsBe.IsApprovalInProcess)
                {
                    lblActiveMsg.Text = Resource.REQ_STILL_PENDING;
                    mpeActivate.Show();
                }

                //if (_objBillAdjustmentsBe.IsSuccess)
                //{
                //    //UpdatedCustOutStanding(); // Commented by Bhimaraju 16-03-2015 (Satya as per the discursion)
                //    Message(Resource.BILL_ADJUSTMENT_COMPLETED, UMS_Resource.MESSAGETYPE_SUCCESS);
                //}
                BindUserDetails();
                GetBatchDetails();
                //}
                //else
                //{
                //    lblGenMessage.Text = Resource.ADJUST_INCRS_BATCH_LMT;
                //    mpeGenMessage.Show();
                //}
                //txtDebit.Text = txtCredit.Text = string.Empty;
                txtCrdDbtAmount.Text = string.Empty;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnUpdateCA_Click(object sender, EventArgs e)                //Consumption Adjustment Neeraj-ID077 25-Aug
        {
            try
            {

                //bool IsApproval = false;
                //IsApproval = _ObjCommonMethods.IsApproval(Resource.FEATURE_BILL_ADJUSTMENT);// Will check approval is required or not for this feature.
                XmlDocument xml = new XmlDocument();

                BillAdjustmentsBe _objBillAdjustmentsBe = new BillAdjustmentsBe();
                _objBillAdjustmentsBe.AccountNo = lblAccountno.Text;
                //_objBillAdjustmentsBe.BillAdjustmentType = Convert.ToInt32(rdblAdjsutmentType.SelectedValue);
                _objBillAdjustmentsBe.BatchNo = Convert.ToInt32(hfBatchID.Value);
                _objBillAdjustmentsBe.Remarks = objIdsignBal.ReplaceNewLines(txtMeterReson.Text, true);
                _objBillAdjustmentsBe.BillAdjustmentType = Convert.ToInt32(objIdsignBal.Decrypt(Request.QueryString[Constants.QS_AdjustmentTypeid]));
                _objBillAdjustmentsBe.CustomerBillId = string.IsNullOrEmpty(hdnCustomerBillID.Value) ? 0 : Convert.ToInt32(hdnCustomerBillID.Value);
                _objBillAdjustmentsBe.Consumption = Convert.ToInt32(lblCurrentConsumption.Text);
                _objBillAdjustmentsBe.AdjustedUnits = Convert.ToInt32(txtConsumptionCA.Text.Replace(",", string.Empty));
                _objBillAdjustmentsBe.FunctionId = (int)EnumApprovalFunctions.BillAdjustment;
                _objBillAdjustmentsBe.BatchNo = Convert.ToInt32(objIdsignBal.Decrypt(Request.QueryString[Constants.QS_BatchID]));


                _objBillAdjustmentsBe.EnergryCharges = (string.IsNullOrEmpty(lblREnergyChargesCA.Text.Replace(",", string.Empty)) ? 0 : Convert.ToDecimal(lblREnergyChargesCA.Text.Replace(",", string.Empty)));

                _objBillAdjustmentsBe.PreviousReading = lblPreviousReadingCA.Text;
                _objBillAdjustmentsBe.CurrentReadingBeforeAdjustment = lblPresentReadionCA.Text;
                //_objBillAdjustmentsBe.CurrentReadingAfterAdjustment = (Convert.ToInt32(lblPresentReadionCA.Text.Replace(",", string.Empty)) - Convert.ToInt32(txtConsumptionCA.Text.Replace(",", string.Empty))).ToString();
                _objBillAdjustmentsBe.CurrentReadingAfterAdjustment = (Convert.ToInt32(lblPresentReadionCA.Text.Replace(",", string.Empty)) - ((Convert.ToInt32(lblTotalAdjustedConsumption.Text.Replace(",", string.Empty)) + Convert.ToInt32(txtConsumptionCA.Text.Replace(",", string.Empty))))).ToString();


                _objBillAdjustmentsBe.FunctionId = (int)EnumApprovalFunctions.BillAdjustment;
                _objBillAdjustmentsBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objBillAdjustmentsBe.BatchID = Convert.ToInt32(lblBatchNo.Text);
                _objBillAdjustmentsBe.BU_ID = ((Session[UMS_Resource.SESSION_USER_BUID] == null)) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();

                if (CheckBatchTotal(_objBillAdjustmentsBe.AdjustedUnits))
                {

                    if (_ObjCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.BillAdjustment, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString()))
                    {
                        _objBillAdjustmentsBe.ApprovalStatusId = (int)EnumApprovalStatus.Approved;
                        _objBillAdjustmentsBe.IsFinalApproval = true;
                        pop.Attributes.Add("class", "popheader popheaderlblgreen");
                        lblActiveMsg.Text = Resource.MODIFICATIONS_UPDATED;
                    }
                    else
                    {
                        _objBillAdjustmentsBe.ApprovalStatusId = (int)EnumApprovalStatus.Process;
                        pop.Attributes.Add("class", "popheader popheaderlblred");
                        lblActiveMsg.Text = Resource.APPROVAL_TARIFF_CHANGE;

                    }

                    xml = _objBillAdjustmentBal.MeterConsumptionAdjustmentBAL(objIdsignBal.DoSerialize<BillAdjustmentsBe>(_objBillAdjustmentsBe));
                    _objBillAdjustmentsBe = objIdsignBal.DeserializeFromXml<BillAdjustmentsBe>(xml);

                    //if (_objBillAdjustmentsBe.IsSuccess)
                    //    Message(Resource.BILL_ADJUSTMENT_COMPLETED, UMS_Resource.MESSAGETYPE_SUCCESS);
                    if (_objBillAdjustmentsBe.IsSuccess)
                    {
                        mpeActivate.Show();
                    }
                    else if (_objBillAdjustmentsBe.IsApprovalInProcess)
                    {
                        lblActiveMsg.Text = Resource.REQ_STILL_PENDING;
                        mpeActivate.Show();
                    }
                    BindUserDetails();
                    GetBatchDetails();
                }
                else
                {
                    lblGenMessage.Text = Resource.ADJUST_INCRS_BATCH_LMT;
                    mpeGenMessage.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                bool IsApproval = false;
                IsApproval = _ObjCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.BillAdjustment, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());

                XmlDocument xml = new XmlDocument();
                BillAdjustmentsBe _objBillAdjustmentsBe = new BillAdjustmentsBe();
                _objBillAdjustmentsBe.AccountNo = lblAccountno.Text;
                _objBillAdjustmentsBe.BillAdjustmentType = Convert.ToInt32(objIdsignBal.Decrypt(Request.QueryString[Constants.QS_AdjustmentTypeid]));
                _objBillAdjustmentsBe.CustomerBillId = string.IsNullOrEmpty(hdnCustomerBillID.Value) ? 0 : Convert.ToInt32(hdnCustomerBillID.Value);
                _objBillAdjustmentsBe.CurrentReadingBeforeAdjustment = hfReadingBeforeAdjust.Value;
                _objBillAdjustmentsBe.CurrentReadingAfterAdjustment = txtPresentReading.Text;
                _objBillAdjustmentsBe.BatchNo = Convert.ToInt32(objIdsignBal.Decrypt(Request.QueryString[Constants.QS_BatchID]));
                int readingAdjusted = Convert.ToInt32(_objBillAdjustmentsBe.CurrentReadingBeforeAdjustment) - Convert.ToInt32(_objBillAdjustmentsBe.CurrentReadingAfterAdjustment);

                if (CheckBatchTotal(readingAdjusted))
                {
                    if (!IsApproval)
                    {
                        _objBillAdjustmentsBe.ApprovalStatusId = (int)EnumApprovalStatus.Process;
                    }
                    else
                    {
                        _objBillAdjustmentsBe.ApprovalStatusId = (int)EnumApprovalStatus.Approved;
                    }

                    _objBillAdjustmentsBe.FunctionId = (int)EnumApprovalFunctions.BillAdjustment;
                    _objBillAdjustmentsBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    _objBillAdjustmentsBe.BatchID = Convert.ToInt32(lblBatchNo.Text);
                    _objBillAdjustmentsBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                    xml = _objBillAdjustmentBal.InsertAdjustedReadDetails(_objBillAdjustmentsBe, Statement.Insert);
                    _objBillAdjustmentsBe = objIdsignBal.DeserializeFromXml<BillAdjustmentsBe>(xml);
                    if (_objBillAdjustmentsBe.IsSuccess)
                        Message(Resource.BILL_ADJUSTMENT_COMPLETED, UMS_Resource.MESSAGETYPE_SUCCESS);


                    BindUserDetails();
                    GetBatchDetails();
                }
                else
                {
                    lblGenMessage.Text = Resource.ADJUST_INCRS_BATCH_LMT;
                    mpeGenMessage.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }               //Update Reading

        protected void lnkCancelAdjustments_Click(object sender, EventArgs e)//delete confirmation Neeraj-ID077 26-Aug
        {
            mpeDeleteConfirm.Show();
        }

        protected void btnDeleteBillAdjustment_Click(object sender, EventArgs e)//Physical delete of adjsutment Neeraj-ID077 26-Aug
        {
            XmlDocument xml = null;
            BillAdjustmentsBe objBillAdjustmentsBe = new BillAdjustmentsBe();
            objBillAdjustmentsBe.BillAdjustmentId = Convert.ToInt32(hfBillAdjustmentId.Value);
            xml = _objBillAdjustmentBal.CancelAdjustmentBAL(objIdsignBal.DoSerialize<BillAdjustmentsBe>(objBillAdjustmentsBe));
            objBillAdjustmentsBe = objIdsignBal.DeserializeFromXml<BillAdjustmentsBe>(xml);
            if (objBillAdjustmentsBe.RowCount > 0)
            {
                BindUserDetails();
                GetBatchDetails();
                Message(Resource.BILL_ADJUST_CANCELED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
            }
            else
            {
                Message(Resource.BILL_ADJUST_CANC_UNSUCCESS, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        protected void grdAdjustmentDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string AdjustmentDate = _ObjCommonMethods.DateFormate(((Label)(e.Row.FindControl("lblCreatedDate"))).Text);
                ((Label)(e.Row.FindControl("lblCreatedDate"))).Text = AdjustmentDate;
                decimal lblAmountEffected = Convert.ToDecimal(((Label)(e.Row.FindControl("lblAmountEffected"))).Text);
                ((Label)(e.Row.FindControl("lblAmountEffected"))).Text = _ObjCommonMethods.GetCurrencyFormat(lblAmountEffected).ToString();
                decimal lblAmountEffectedWithTax = Convert.ToDecimal(((Label)(e.Row.FindControl("lblAmountEffectedWithTax"))).Text);
                ((Label)(e.Row.FindControl("lblAmountEffectedWithTax"))).Text = _ObjCommonMethods.GetCurrencyFormat(lblAmountEffectedWithTax).ToString();
            }
        }

        #region Old Code

        //protected void btnUpdate_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        bool IsApproval = false;
        //        IsApproval = _ObjCommonMethods.IsApproval(Resource.FEATURE_BILL_ADJUSTMENT);// Will check approval is required or not for this feature.
        //        XmlDocument xml = new XmlDocument();
        //        if (IsApproval)//Insert information in log table and wait for the approval.
        //        {
        //            CustomerDetailsBe objCustomerDetailsBe = new CustomerDetailsBe();
        //            CustomerDetailsBAL objCustomerDetailsBAL = new CustomerDetailsBAL();
        //            objCustomerDetailsBe.BillNo = lbtnBillNo.Text;
        //            objCustomerDetailsBe.CustomerReadingId = Convert.ToInt32(lbtnBillNo.CommandArgument);
        //            objCustomerDetailsBe.PresentReading = Convert.ToDecimal(txtPresentReading.Text);
        //            objCustomerDetailsBe.PreviousReading = Convert.ToDecimal(lblPreviousReading.Text);
        //            objCustomerDetailsBe.FixedCharges = Convert.ToDecimal(lblREnergyCharges.Text);
        //            objCustomerDetailsBe.CustomerBillID = Convert.ToInt32(hdnCustomerBillID.Value);
        //            objCustomerDetailsBe.CustomerID = litCustomerUniqueNo.Text;
        //            objCustomerDetailsBe.AccountNo = lblAccountno.Text;
        //            //objCustomerDetailsBe.Consumption = Convert.ToDecimal(txtConsumption.Text);
        //            objCustomerDetailsBe.Consumption = Convert.ToDecimal(lblRConsumption.Text);
        //            objCustomerDetailsBe.BATID = Convert.ToInt16(Resources.Resource.METER_ADJUSTMENT_ID);
        //            xml = objCustomerDetailsBAL.SendMeterAdjustmentForApprovalBAL(objCustomerDetailsBe);
        //            objCustomerDetailsBe = objIdsignBal.DeserializeFromXml<CustomerDetailsBe>(xml);
        //            lblMsgPopup.Text = Resource.BILL_ADJUSTMENT_REQ_SENT;
        //            mpeMeterReading.Show();
        //            BindUserDetails();
        //        }
        //        else//Updated bill details 
        //        {
        //            BillGenerationBe objBillGenerationBe = new BillGenerationBe();
        //            BillGenerationBal objBillGenerationBal = new BillGenerationBal();
        //            objBillGenerationBe.PresentReading = txtPresentReading.Text;
        //            objBillGenerationBe.Usage = (Convert.ToDecimal(txtPresentReading.Text) - Convert.ToDecimal(lblPreviousReading.Text));//Convert.ToDecimal(hdnConsumption.Value);
        //            objBillGenerationBe.BillNo = lbtnBillNo.Text;
        //            objBillGenerationBe.CustomerBillId = Convert.ToInt32(hdnCustomerBillID.Value);
        //            objBillGenerationBe.AccountNo = lblAccountno.Text;
        //            objBillGenerationBe.BillGeneratedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
        //            //objBillGenerationBe.CustomerReadingId = BillGenerationstep2;
        //            xml = objBillGenerationBal.UpdatedMeterReadingAdjustmentsBAL(objBillGenerationBe);
        //            lblMsgPopup.Text = Resource.BILL_ADJUSTMENT_COMPLETED;
        //            mpeMeterReading.Show();
        //            BindUserDetails();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }

        //} //Update Reading


        //protected void btnUpdateCharges_Click(object sender, EventArgs e) //Update energy charges
        //{
        //    try
        //    {
        //        bool IsApproval = false;
        //        IsApproval = _ObjCommonMethods.IsApproval(Resource.FEATURE_BILL_ADJUSTMENT);
        //        XmlDocument xml = new XmlDocument();
        //        CustomerDetailsBe objCustomerDetailsBe = new CustomerDetailsBe();
        //        CustomerDetailsBAL objCustomerDetailsBAL = new CustomerDetailsBAL();
        //        decimal AdditionalCharges = 0;
        //        decimal Tax = 0;
        //        objCustomerDetailsBe.BillNo = lblBillNo.Text;
        //        decimal addCharges;
        //        string addChargesValues = string.Empty;
        //        for (int i = 0; i < rptrAdditionalChargesAdjust.Items.Count; i++)
        //        {
        //            addChargesValues = ((TextBox)rptrAdditionalChargesAdjust.Items[i].FindControl("txtReadingAdditionalCharges")).Text;
        //            if (addChargesValues != "")
        //            {
        //                addCharges = Convert.ToDecimal(addChargesValues);
        //                AdditionalCharges += addCharges;
        //            }
        //            else
        //                break;
        //        }
        //        if (addChargesValues != "")
        //        {
        //            objCustomerDetailsBe.NetEnergyCharges = Convert.ToDecimal(lblEnergyCharges.Text);
        //            objCustomerDetailsBe.NetFixedCharges = AdditionalCharges;
        //            objCustomerDetailsBe.TotalBillAmount = objCustomerDetailsBe.NetEnergyCharges + AdditionalCharges;
        //            Tax = objCustomerDetailsBe.TotalBillAmount * Convert.ToDecimal(lblTaxPercent.Text) / 100;
        //            objCustomerDetailsBe.TotalBillAmountWithTax = objCustomerDetailsBe.TotalBillAmount + Tax;


        //            objCustomerDetailsBe.CustomerBillID = Convert.ToInt32(hdnCustomerBillID.Value);
        //            objCustomerDetailsBe.CustomerID = litCustomerUniqueNo.Text;
        //            objCustomerDetailsBe.AccountNo = lblAccountno.Text;
        //            objCustomerDetailsBe.BATID = Convert.ToInt16(Resources.Resource.BILL_ADJUSTMENT_ID);


        //            if (IsApproval)
        //            {
        //                xml = objCustomerDetailsBAL.SendBillAdjustmentForApprovalBAL(objCustomerDetailsBe);
        //                objCustomerDetailsBe = objIdsignBal.DeserializeFromXml<CustomerDetailsBe>(xml);
        //            }
        //            else
        //                objCustomerDetailsBe = objIdsignBal.DeserializeFromXml<CustomerDetailsBe>(objCustomerDetailsBAL.GetBillDetails(objCustomerDetailsBe, ReturnType.Group));
        //            //objCustomerDetailsBe = objIdsignBal.DeserializeFromXml<CustomerDetailsBe>(xml);
        //            //objCustomerDetailsBe = objIdsignBal.DeserializeFromXml<CustomerDetailsBe>(objCustomerDetailsBAL.GetBillDetails(objCustomerDetailsBe, ReturnType.Group));

        //            if (objCustomerDetailsBe.IsSuccess)
        //            {
        //                foreach (RepeaterItem item in rptrAdditionalChargesAdjust.Items)
        //                {
        //                    CustomerDetailsBe objCustomerDetailsBe1 = new CustomerDetailsBe();
        //                    CustomerDetailsBAL objCustomerDetailsBAL1 = new CustomerDetailsBAL();
        //                    //LinkButton lbtnChargeName = (LinkButton)item.FindControl("lbtnChargeName");
        //                    Label lbtnChargeName = (Label)item.FindControl("lbtnChargeName");
        //                    string AdditionalChargess = ((TextBox)item.FindControl("txtReadingAdditionalCharges")).Text;
        //                    string AdditionalChargesID = ((Literal)item.FindControl("litAdditionalChargeID")).Text;
        //                    objCustomerDetailsBe1.BAID = objCustomerDetailsBe.BAID;
        //                    objCustomerDetailsBe1.AdditionalCharges = Convert.ToDecimal(AdditionalChargess);
        //                    objCustomerDetailsBe1.ChargeID = Convert.ToInt32(AdditionalChargesID);
        //                    objCustomerDetailsBe1.FixedCharges = Convert.ToDecimal(lblEnergyCharges.Text);
        //                    objCustomerDetailsBe1.BillNo = lblBillNo.Text;
        //                    //objCustomerDetailsBe1.AccountNo = txtccountNo.Text;
        //                    //objCustomerDetailsBe1.BillNo = lblBillNo.Text;
        //                    //objCustomerDetailsBe1.NetEnergyCharges = Convert.ToDecimal(txtReadingAdditionalCharges.Text);
        //                    //objCustomerDetailsBe1.CustomerAdditioalCharges = lbtnChargeName.CommandArgument;
        //                    if (IsApproval)
        //                        xml = objCustomerDetailsBAL.SendBillAdjustmentForApprovalBAL(objCustomerDetailsBe1);
        //                    else
        //                        objCustomerDetailsBe1 = objIdsignBal.DeserializeFromXml<CustomerDetailsBe>(objCustomerDetailsBAL1.GetBillDetails(objCustomerDetailsBe1, ReturnType.List));
        //                    objCustomerDetailsBe1 = objIdsignBal.DeserializeFromXml<CustomerDetailsBe>(xml);

        //                    //objCustomerDetailsBe1 = objIdsignBal.DeserializeFromXml<CustomerDetailsBe>(objCustomerDetailsBAL1.GetBillDetails(objCustomerDetailsBe1, ReturnType.List));

        //                }
        //                if (!IsApproval)
        //                    lblMsgPopup.Text = Resource.BILL_ADJUSTMENT_COMPLETED;
        //                else
        //                    lblMsgPopup.Text = Resource.BILL_ADJUSTMENT_REQ_SENT;
        //                mpeMeterReading.Show();
        //            }
        //            BindUserDetails();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        #endregion

        #endregion

        #region Methods

        private bool IsBillAdjested()
        {
            BillAdjustmentsBe _objBillAdjustmentsBe = new BillAdjustmentsBe();
            _objBillAdjustmentsBe.AccountNo = lblAccountno.Text;
            _objBillAdjustmentsBe.CustomerBillId = string.IsNullOrEmpty(hdnCustomerBillID.Value) ? 0 : Convert.ToInt32(hdnCustomerBillID.Value);
            XmlDocument xml = _objBillAdjustmentBal.GetAdjustmentDetails(_objBillAdjustmentsBe, ReturnType.Check);
            _objBillAdjustmentsBe = objIdsignBal.DeserializeFromXml<BillAdjustmentsBe>(xml);

            return _objBillAdjustmentsBe.IsAdjusted;
        }

        private DataTable GetEffectedBillDetails(DataTable dt, int chargeId, decimal adjusted)
        {
            DataRow dr = dt.NewRow();
            dr["ChargeID"] = chargeId;
            dr["AmountEffected"] = adjusted;
            dt.Rows.Add(dr);
            return dt;
        }

        private void BindUserDetails()
        {
            CustomerDetailsBe objCustomerDetailsBe = new CustomerDetailsBe();
            CustomerDetailsListBe objCustomerDetailsListBe = new CustomerDetailsListBe();
            CustomerDetailsBillsListBe objCustomerDetailsBillsListBe = new CustomerDetailsBillsListBe();
            CustomerDetailsBAL objCustomerDetailsBAL = new CustomerDetailsBAL();
            XmlDocument xml = new XmlDocument();

            objCustomerDetailsBe.SearchValue = txtccountNo.Text;
            objCustomerDetailsBe.BusinessUnitName = Convert.ToString(Session[UMS_Resource.SESSION_USER_BUID]);
            xml = objCustomerDetailsBAL.GetBillDetails(objCustomerDetailsBe, ReturnType.Data);
            objCustomerDetailsListBe = objIdsignBal.DeserializeFromXml<CustomerDetailsListBe>(xml);

            //CustomerAdjustmentsListBe _objCustomerAdjustmentsListBe = new CustomerAdjustmentsListBe();
            //_objCustomerAdjustmentsListBe = objIdsignBal.DeserializeFromXml<CustomerAdjustmentsListBe>(xml);
            objCustomerDetailsBillsListBe = objIdsignBal.DeserializeFromXml<CustomerDetailsBillsListBe>(xml);
            if (objCustomerDetailsListBe != null)
            {
                if (objCustomerDetailsListBe.items.Count > 0)
                {
                    objCustomerDetailsBe = objCustomerDetailsListBe.items[0];
                    if (objCustomerDetailsBe.RowsEffected > 0)
                    {
                        if (objCustomerDetailsBe.CustomerTypeId != 3)//checking for Prepaid customer
                        {
                            lblCustomerName.Text = objCustomerDetailsBe.Name;
                            lblAccountno.Text = objCustomerDetailsBe.AccountNo;
                            lblAccNoAndGlobalAccNo.Text = objCustomerDetailsBe.AccNoAndGlobalAccNo;
                            lblDocumentNo.Text = objCustomerDetailsBe.DocumentNo;
                            lblTariff.Text = objCustomerDetailsBe.ClassName;
                            lblBuid.Text = objCustomerDetailsBe.BusinessUnitName;
                            lblSuid.Text = objCustomerDetailsBe.ServiceUnitName;
                            lblScid.Text = objCustomerDetailsBe.ServiceCenterName;
                            lblTotalBills.Text = objCustomerDetailsBe.TotalBillPendings.ToString();
                            litCustomerUniqueNo.Text = objCustomerDetailsBe.CustomerID;
                            lblTotalAmountPending.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(objCustomerDetailsBe.TotalDueAmount), 2, Constants.MILLION_Format).ToString();
                            lblLastAmountPaid.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(objCustomerDetailsBe.LastPaidAmount), 2, Constants.MILLION_Format).ToString();
                            lblLastBillDate.Text = objCustomerDetailsBe.LastBillGeneratedDate;
                            lblLastPaidDate.Text = objCustomerDetailsBe.LastPaymentDate;
                            litMeterDial.Text = Convert.ToString(objCustomerDetailsBe.MeterDials);
                            lblOldAccountNo.Text = objCustomerDetailsBe.OldAccountNo;
                            lblMeterNo.Text = objCustomerDetailsBe.MeterNo;
                            if (objCustomerDetailsBillsListBe.items.Count > 0)
                            {
                                divPendingBills.Visible = true;
                                divNoPendingBills.Visible = false;

                                dtlBills.DataSource = objCustomerDetailsBillsListBe.items;
                                dtlBills.DataBind();
                                hfDecimals.Value = objCustomerDetailsBillsListBe.items[0].Decimals.ToString();
                            }
                            else
                            {
                                divPendingBills.Visible = false;
                                divNoPendingBills.Visible = true;
                                dtlBills.DataSource = new DataTable();
                                dtlBills.DataBind();
                                dtDirectCustomer.DataSource = new DataTable();
                                dtDirectCustomer.DataBind();

                                if (txtccountNo.Text != "")
                                    mpeBillNA.Show();
                                divPendingBills.Visible = false;
                                ClearFields();

                            }
                        }
                        else 
                        {
                            lblAlertMsg.Text = "Customer is Prepaid customer.";
                            mpeAlert.Show();
                            divPendingBills.Visible = false;
                            ClearFields();
                        }
                    }
                    else if (objCustomerDetailsBe.ActiveStatusId == 4)
                    {
                        lblAlertMsg.Text = "Customer status is Closed.";
                        mpeAlert.Show();
                        divPendingBills.Visible = false;
                        ClearFields();
                    }
                    else
                    {
                        lblAlertMsg.Text = Resources.UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                        mpeAlert.Show();
                        divPendingBills.Visible = false;
                        ClearFields();
                    }
                }
                else
                {
                    if (txtccountNo.Text != "")
                        mpeCustomerNA.Show();
                    //else if (txtDocNo.Text != "")
                    //    mpeBillNA.Show();
                    divPendingBills.Visible = false;
                    ClearFields();
                }
            }
            else
            {
                if (txtccountNo.Text != "")
                    mpeCustomerNA.Show();
                //else if (txtDocNo.Text != "")
                //    mpeBillNA.Show();
                divPendingBills.Visible = false;
                ClearFields();
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        [WebMethod]
        public static string GetEnergyPerKW(string Consumptions, string BillNo, string TariffID)
        {
            CustomerDetailsBe objCustomerDetailsBe = new CustomerDetailsBe();
            CustomerDetailsBAL objCustomerDetailsBAL = new CustomerDetailsBAL();
            iDsignBAL objiDsignBAL = new iDsignBAL();
            objCustomerDetailsBe.Consumption = Convert.ToDecimal(Consumptions);
            objCustomerDetailsBe.BillNo = BillNo;
            objCustomerDetailsBe.ClassName = TariffID;
            objCustomerDetailsBe = objiDsignBAL.DeserializeFromXml<CustomerDetailsBe>(objCustomerDetailsBAL.GetBillDetails(objCustomerDetailsBe, ReturnType.Multiple));
            return objCustomerDetailsBe.Amount.ToString();
        }

        [WebMethod]
        public static Array GetMeterReadingAdjustment(string Usage, string BillNo, string AccountNo)
        {
            BillGenerationBe objBillGenerationBe = new BillGenerationBe();
            BillGenerationBal objBillGenerationBal = new BillGenerationBal();
            iDsignBAL objiDsignBAL = new iDsignBAL();
            string[] adjustmentDetails = new string[4];
            objBillGenerationBe.Usage = Convert.ToDecimal(Usage);
            objBillGenerationBe.BillNo = BillNo;
            objBillGenerationBe.AccountNo = AccountNo;
            objBillGenerationBe = objiDsignBAL.DeserializeFromXml<BillGenerationBe>(objBillGenerationBal.GetMeterReadingAdjstDetailsBAL(objBillGenerationBe));
            adjustmentDetails[0] = objBillGenerationBe.NetEnergyCharges.ToString(Resource.DECIMAL_FORMATE);
            adjustmentDetails[1] = objBillGenerationBe.NetFixedCharges.ToString(Resource.DECIMAL_FORMATE); ;
            adjustmentDetails[2] = objBillGenerationBe.TotalBillAmount.ToString(Resource.DECIMAL_FORMATE); ;
            adjustmentDetails[3] = objBillGenerationBe.TotalBillAmountWithTax.ToString(Resource.DECIMAL_FORMATE);
            return adjustmentDetails;
        }

        public void ClearFields()
        {
            txtccountNo.Text = "";
            lblAccountno.Text = "";
            lblCustomerName.Text = "";
            lblAccountno.Text = "";
            lblDocumentNo.Text = "";
            lblSuid.Text = "";
            lblScid.Text = "";
            lblBuid.Text = "";
            lblTariff.Text = "";
        }

        public int UpdatedCustOutStanding()
        {
            decimal AmountEffected = 0;
            //if (txtCredit.Text != string.Empty)
            //    AmountEffected = Convert.ToDecimal(txtCredit.Text);
            //else
            //    AmountEffected = -Convert.ToDecimal(txtDebit.Text);
            if (rdoAdjustmentTypeBA.SelectedItem.Value == "1")
                AmountEffected = Convert.ToDecimal(txtCrdDbtAmount.Text);
            else if (rdoAdjustmentTypeBA.SelectedItem.Value == "2")
                AmountEffected = Convert.ToDecimal(txtCrdDbtAmount.Text);
            _ObjCustomerDetailsBe.AccountNo = lblAccountno.Text;
            _ObjCustomerDetailsBe.OutStandingAmount = AmountEffected;
            return (objIdsignBal.DeserializeFromXml<CustomerDetailsBe>(_ObjCustomerDetailsBAL.UpdateCustomerOutstandingBAL(_ObjCustomerDetailsBe))).RowsEffected;
        }

        public void GetBatchDetails()
        {
            BillAdjustmentsBe objBillAdjustmentsBe = new BillAdjustmentsBe();
            BillAdjustmentListBe objBillAdjustmentListBe = new BillAdjustmentListBe();
            BillAdjustmentLisBeDetails objBillAdjustmentLisBeDetails = new BillAdjustmentLisBeDetails();

            objBillAdjustmentsBe.BatchID = Convert.ToInt32(objIdsignBal.Decrypt(Request.QueryString[Constants.QS_BatchID]));
            objBillAdjustmentsBe.BU_ID = ((Session[UMS_Resource.SESSION_USER_BUID] == null)) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
            XmlDocument xml = _objBillAdjustmentBal.GetAdjustmentBatchDetailsBAL(objBillAdjustmentsBe);

            objBillAdjustmentListBe = objIdsignBal.DeserializeFromXml<BillAdjustmentListBe>(xml);
            objBillAdjustmentLisBeDetails = objIdsignBal.DeserializeFromXml<BillAdjustmentLisBeDetails>(xml);

            if (objBillAdjustmentListBe.items.Count > 0)
            {
                lblBatchDate.Text = _ObjCommonMethods.DateFormate(objBillAdjustmentListBe.items[0].BatchDate);
                lblBatchNo.Text = objBillAdjustmentListBe.items[0].BatchNo.ToString();
                hfBatchID.Value = objBillAdjustmentListBe.items[0].BatchID.ToString();
                lblTotalAdjustments.Text = objBillAdjustmentListBe.items[0].TotalCustomers.ToString();
                lblReason.Text = objBillAdjustmentListBe.items[0].ReasonCode.ToString();
                lblBatchTotal.Text = _ObjCommonMethods.GetCurrencyFormat(objBillAdjustmentListBe.items[0].BatchTotal, 2, Constants.MILLION_Format);


                if (objBillAdjustmentListBe.items[0].BillAdjustmentType == Convert.ToInt32(Constants.Reading_Adjustment) || objBillAdjustmentListBe.items[0].BillAdjustmentType == Convert.ToInt32(Constants.Consumption_Adjustment))
                {
                    Literal73.Text = "Total Batch Units";
                    Literal74.Text = "Pending Units";
                    lblBatchPending.Text = objBillAdjustmentListBe.items[0].PendingUnit.ToString();
                    lblBatchTotal.Text = Convert.ToInt64(objBillAdjustmentListBe.items[0].BatchTotal).ToString();
                }
                if (objBillAdjustmentListBe.items[0].BillAdjustmentType == Convert.ToInt32(Constants.Additional_Charges_Adjustment) || objBillAdjustmentListBe.items[0].BillAdjustmentType == Convert.ToInt32(Constants.Bill_Adjustment))
                    lblBatchPending.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(objBillAdjustmentListBe.items[0].PendingAmount.ToString()), 2, Constants.MILLION_Format).ToString();
                else if (objBillAdjustmentListBe.items[0].BillAdjustmentType == 0)
                {
                    lblBatchPending.Text = _ObjCommonMethods.GetCurrencyFormat(objBillAdjustmentListBe.items[0].BatchTotal, 2, Constants.MILLION_Format);
                }


                if (objBillAdjustmentLisBeDetails.items.Count > 0)
                {
                    divBatchDetailsGrid.Visible = true;
                    grdAdjustmentDetails.DataSource = objBillAdjustmentLisBeDetails.items;
                    grdAdjustmentDetails.DataBind();
                }
                else
                    divBatchDetailsGrid.Visible = false;
                lblAdjsutmentType.Text = objIdsignBal.Decrypt(Request.QueryString[Constants.QS_AdjustmentText]);
            }
            else
            {
                divAdjustmentMain.Visible = false;
                Message("Batch doesn't exists or doesn't belongs to this BU.", UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        public bool CheckBatchTotal(decimal Amount)
        {
            decimal BatchPending = Convert.ToDecimal(lblBatchPending.Text);
            if (Math.Abs(Amount) <= BatchPending)
                return true;
            else
                return false;
        }

        protected void lnkPrevious_Click(object sender, EventArgs e)
        {
            Response.Redirect("AdjustmentBatch.aspx");// + Constants.QS_Initiat + Constants.QS_BatchID + Constants.QS_EqualTo + objIdsignBal.Encrypt(lblBatchNo.Text) + Constants.QS_Seprator + Constants.QS_BatchDate + Constants.QS_EqualTo + objIdsignBal.Encrypt(lblBatchDate.Text)
        }

        //public bool CheckBatchTotal(decimal Amount)
        //{
        //    decimal BatchPending = Convert.ToDecimal(lblBatchTotal.Text);
        //    if (Amount <= BatchPending)
        //        return false;
        //    else
        //        return true;
        //}


        private void GetAdjustmentLastTransactions()
        {
            UC_AdditionalChargesLastTransaction objAdChargTrans = (UC_AdditionalChargesLastTransaction)LoadControl("../UserControls/UC_AdditionalChargesLastTransaction.ascx");
            objAdChargTrans.AccountNo = txtccountNo.Text;
            objAdChargTrans.GetAdjustmentLastTransactions();
            divAdditionalChargesList.Controls.Add(objAdChargTrans);
        }

        private void SendMailAndMsg(string GlobalAccountno)
        {
            string GlobalAccountNo = Convert.ToString(GlobalAccountno);
            string SuccessMessage = string.Empty;
            CommunicationFeaturesBE objCommunicationFeaturesBE = new CommunicationFeaturesBE();
            objCommunicationFeaturesBE.CommunicationFeaturesID = (int)CommunicationFeatures.Adjustments;

            try
            {
                if (_ObjCommonMethods.IsEmailFeatureEnabled(objCommunicationFeaturesBE))
                {
                    CommunicationFeaturesBE _objMailBE = new CommunicationFeaturesBE();
                    _objMailBE.GlobalAccountNumber = Convert.ToString(GlobalAccountNo);
                    XmlDocument xml = _ObjCommonMethods.GetDetails(_objMailBE, ReturnType.Data);
                    _objMailBE = objIdsignBal.DeserializeFromXml<CommunicationFeaturesBE>(xml);

                    if (_objMailBE.EmailId == "0")
                    {
                        SuccessMessage += "Customer not having Mail Id.";
                    }
                    else if (!string.IsNullOrEmpty(_objMailBE.EmailId))
                    {
                        if (_objMailBE.IsSuccess)
                        {
                            string amount = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(_objMailBE.AdjustedAmount), 2, Constants.MILLION_Format);
                            string outstandingamount = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(_objMailBE.OutStandingAmount), 2, Constants.MILLION_Format);
                            string htmlBody = string.Empty;
                            htmlBody = _ObjCommonMethods.GetMailCommTemp(CommunicationFeatures.Adjustments, Convert.ToString(_objMailBE.Name), string.Empty, Convert.ToString(GlobalAccountNo), Convert.ToString(_objMailBE.MeterNumber), string.Empty, Convert.ToString(outstandingamount), string.Empty, string.Empty, string.Empty, Convert.ToString(amount), "Dear Customer, bill adjustment of =N= " + Convert.ToString(amount) + " and your Outstanding amount =N= " + Convert.ToString(outstandingamount) + " for your BEDC acc " + Convert.ToString(GlobalAccountNo) + " has been processed successfully. Txn Id: " + Convert.ToString(_objMailBE.TransactionId) + ".");
                            //htmlBody = _objCommonMethods.GetMailCommTemp(CommunicationFeatures.Adjustments, Convert.ToString(_objMailBE.Name), Convert.ToString(GlobalAccountNo), Convert.ToString(GlobalAccountNo), "Dear Customer, bill payment of =N= " + Convert.ToString(amount) + " for your BEDC acc " + Convert.ToString(GlobalAccountNo) + " has been processed successfully. Txn Id: " + Convert.ToString(_objMailBE.TransactionId) + ".");

                            _ObjCommonMethods.sendmail(htmlBody, ConfigurationManager.AppSettings["SMTPUsername"].ToString(), "Bill Adjusted for acc " + Convert.ToString(GlobalAccountNo), _objMailBE.EmailId, string.Empty);
                            SuccessMessage += "Mail sent successfully.";
                        }
                    }
                    else
                    {
                        SuccessMessage += "Mail not sent to this customer.";
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

            try
            {
                if (_ObjCommonMethods.IsSMSFeatureEnabled(objCommunicationFeaturesBE))
                {
                    CommunicationFeaturesBE _objSmsBE = new CommunicationFeaturesBE();
                    _objSmsBE.GlobalAccountNumber = Convert.ToString(GlobalAccountNo);
                    XmlDocument xml = _ObjCommonMethods.GetDetails(_objSmsBE, ReturnType.Data);
                    _objSmsBE = objIdsignBal.DeserializeFromXml<CommunicationFeaturesBE>(xml);

                    if (_objSmsBE.MobileNo == "0")
                    {
                        SuccessMessage += "Customer not having Mobile No.";
                    }
                    else if (!string.IsNullOrEmpty(_objSmsBE.MobileNo) && _objSmsBE.MobileNo.Length == 10)
                    {
                        if (_objSmsBE.IsSuccess)
                        {
                            string amount = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(_objSmsBE.AdjustedAmount), 2, Constants.MILLION_Format);
                            string outstandingamount = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(_objSmsBE.OutStandingAmount), 2, Constants.MILLION_Format);
                            string Messsage = string.Empty;
                            Messsage = _ObjCommonMethods.GetSMSCommTemp(CommunicationFeatures.Adjustments, Convert.ToString(GlobalAccountNo), string.Empty, Convert.ToString(amount), string.Empty, Convert.ToString(_objSmsBE.TransactionId), Convert.ToString(outstandingamount));

                            _ObjCommonMethods.sendSMS(_objSmsBE.MobileNo, Messsage, ConfigurationManager.AppSettings["SMTPUsername"].ToString());
                            SuccessMessage += "Message sent successfully.";
                        }
                    }
                    else
                    {
                        SuccessMessage += "Message not sent to this customer.";
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}