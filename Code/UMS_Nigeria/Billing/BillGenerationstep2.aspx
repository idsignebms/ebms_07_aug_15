﻿<%@ Page Title="::Bill Generation Step-2::" Language="C#" MasterPageFile="~/MasterPages/EDMUMS.Master" AutoEventWireup="true"
    CodeBehind="BillGenerationstep2.aspx.cs" Inherits="UMS_Nigeria.Billing.BillGenerationstep2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="edmcontainer">
        <h3>
            Bill Generation
        </h3>
        <div class="clear">
            &nbsp;</div>
        <div class="consumer_feild">
            <div class="billing_step">
                Step 2</div>
            <div class="clear pad_10">
            </div>
            <div class="consume_name" style="width: 200px;">
                Your Select of Billing
            </div>
            <div class="consume_input">
                <asp:TextBox ID="txtSelectBilling" runat="server"></asp:TextBox>
                <%--<input type="text" />--%></div>
            <div class="clear pad_5">
            </div>
            <div class="consume_name" style="width: 200px;">
                Billing Maths
            </div>
            <div class="consume_input">
                <asp:TextBox ID="txtBilling" runat="server"></asp:TextBox>
                <%-- <input type="text" />--%></div>
            <div class="clear pad_5">
            </div>
            <div class="consume_name" style="width: 200px;">
                Billing
            </div>
            <div class="consume_input">
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                <%--<input type="text" />--%></div>
            <div class="clear pad_5">
            </div>
            <div class="consume_name" style="width: 200px;">
                Billing
            </div>
            <div class="consume_input">
                <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                <%--<input type="text" />--%></div>
            <div class="clear pad_5">
            </div>
        </div>
        <div style="width: 40%;">
            <div class="fltl">
                <div class="chooslink">
                    <a href="GenerateBill.aspx">Previous</a>
                </div>
            </div>
            <div class="fltr">
                <asp:Button ID="btnConfirm" runat="server" Text="<%$ Resources:Resource, CONFIRM%>"
                    CssClass="calculate_but" />
                <%--  <button class="calculate_but">
                    Confirm
                </button>--%>
            </div>
</asp:Content>
