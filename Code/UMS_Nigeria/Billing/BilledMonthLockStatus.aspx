﻿<%@ Page Title=":: Billed Month Lock Status ::" Language="C#" MasterPageFile="~/MasterPages/EDMUMS.Master"
    AutoEventWireup="true" CodeBehind="BilledMonthLockStatus.aspx.cs" Inherits="UMS_Nigeria.Billing.BilledMonthLockStatus" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="edmcontainer">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upAddCycle" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <h3>
                    <asp:Literal ID="litAddCycle" runat="server" Text="<%$ Resources:Resource, BILLED_MONTH_LOCK_STATUS%>"></asp:Literal>
                </h3>
                <div class="clear">
                    &nbsp;</div>
                <div class="consumer_feild">
                    <div class="consume_name">
                        <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, YEAR%>"></asp:Literal><span>*</span>
                    </div>
                    <div class="consume_input">
                        <asp:DropDownList ID="ddlYear" onchange="DropDownlistOnChangelbl(this,'spanYear',' Year')"
                            AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                            <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        </asp:DropDownList>
                        <span id="spanYear" class="spancolor"></span>
                    </div>
                    <div class="consume_name" style="width: 90px">
                        <asp:Literal ID="litServiceUnit" runat="server" Text="<%$ Resources:Resource, MONTH%>"></asp:Literal><span>*</span>
                    </div>
                    <div class="consume_input">
                        <asp:DropDownList ID="ddlMonth" onchange="DropDownlistOnChangelbl(this,'spanMonth',' Month')"
                            AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged">
                            <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        </asp:DropDownList>
                        <span id="spanMonth" class="spancolor"></span>
                    </div>
                    <div class="consumer_total">
                        <div class="clear pad_10">
                        </div>
                    </div>
                    <div class="consume_input" style="width: 20%; font-weight: bolder; padding-left: 75px">
                        <asp:Label ID="lblStatus" runat="server" Visible="false" Text=""></asp:Label>
                    </div>
                    <%--<div class="consume_name">
                        <asp:Literal ID="litDetails" runat="server" Text="<%$ Resources:Resource, DETAILS%>"></asp:Literal>
                    </div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtDetails" runat="server" placeholder="Enter Details" TextMode="MultiLine"></asp:TextBox>
                    </div>--%>
                    <%--<div class="clear pad_10">
                        <div style="margin-left: 380px;">
                            <asp:Button ID="btnOpen" OnClientClick="return Validate();" OnClick="btnOpen_Click"
                                Text="<%$ Resources:Resource, OPEN%>" CssClass="calculate_but" runat="server" />
                            <asp:Button ID="btnReset" OnClientClick="return ClearControls();" Text="<%$ Resources:Resource, RESET%>"
                                CssClass="calculate_but" runat="server" />
                        </div>--%>
                </div>
                <div class="consumer_total">
                    <div class="pad_10">
                    </div>
                </div>
                <div class="consumer_total">
                    <div class="pad_10">
                        <br />
                    </div>
                </div>
                <div id="divgrid" class="grid" runat="server" style="width: 100% !important; float: left;">
                    <div id="divpaging" runat="server">
                        <div align="right" class="marg_20t" runat="server" id="divLinks">
                            <h3 class="gridhead" align="left">
                                <asp:Literal ID="litBuList" runat="server" Text="<%$ Resources:Resource, BILLED_MONTH_LOCK_STATUS_LIST%>"></asp:Literal>
                            </h3>
                            <asp:LinkButton ID="lkbtnFirst" runat="server" CssClass="pagingfirst" OnClick="lkbtnFirst_Click">
                                <asp:Literal ID="litFirst" runat="server" Text="<%$ Resources:Resource, FIRST%>"></asp:Literal></asp:LinkButton>
                            &nbsp;|
                            <asp:LinkButton ID="lkbtnPrevious" runat="server" CssClass="pagingfirst" OnClick="lkbtnPrevious_Click">
                                <asp:Literal ID="litPrevious" runat="server" Text="<%$ Resources:Resource, PREVIOUS%>"></asp:Literal></asp:LinkButton>
                            &nbsp;|
                            <asp:Label ID="lblCurrentPage" runat="server" Font-Bold="true"></asp:Label>
                            &nbsp;|
                            <asp:LinkButton ID="lkbtnNext" runat="server" CssClass="pagingfirst" OnClick="lkbtnNext_Click">
                                <asp:Literal ID="litNext" runat="server" Text="<%$ Resources:Resource, NEXT%>"></asp:Literal></asp:LinkButton>
                            &nbsp;|
                            <asp:LinkButton ID="lkbtnLast" runat="server" CssClass="pagingfirst" OnClick="lkbtnLast_Click">
                                <asp:Literal ID="litLast" runat="server" Text="<%$ Resources:Resource, LAST%>"></asp:Literal></asp:LinkButton>
                            &nbsp;|<asp:Literal ID="litPageSize" runat="server" Text="<%$ Resources:Resource, PAGE_SIZE%>"></asp:Literal>
                            <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" CssClass="paging-size"
                                OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="consumer_total">
                    <div class="clear">
                    </div>
                </div>
                <div class="grid" id="divPrint" runat="server" align="center" style="overflow-x: auto;">
                    <asp:GridView ID="gvBilledMonthList" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                        OnRowDataBound="gvBilledMonthList_RowDataBound" OnRowCommand="gvBilledMonthList_RowCommand">
                        <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                        <EmptyDataTemplate>
                            There is no billed months data.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblOpenStatusId" Visible="false" runat="server" Text='<%#Eval("OpenStatusId")%>'></asp:Label>
                                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                    <asp:Label ID="lblBillingQueueScheduleId" Visible="false" runat="server" Text='<%#Eval("BillingQueueScheduleId")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, CYCLE_NAME%>" ItemStyle-Width="8%"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblCycle" runat="server" Text='<%#Eval("Cycle") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, FEEDER_NAME%>" ItemStyle-Width="8%"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblFeeder" runat="server" Text='<%#Eval("Feeder") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, TOTAL_BILLS%>" ItemStyle-Width="8%"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotalBills" runat="server" Text='<%#Eval("TotalBills") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, TOTAL_AMOUNT%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotalAmount" runat="server" Text='<%#Eval("TotalAmount") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, LAST_BILL_GENERATED_DATE%>"
                                ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblDate" runat="server" Text='<%#Eval("LastBillGeneratedDate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lkbtnLock" Visible="false" runat="server" Text="Lock" CommandName="Lock"
                                        CommandArgument='<%# Eval("OpenStatusId") %>'>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lkbtnUnLock" Visible="false" CommandArgument='<%# Eval("OpenStatusId") %>'
                                        runat="server" Text="UnLock" CommandName="UnLock"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <asp:HiddenField ID="hfPageSize" runat="server" />
                <asp:HiddenField ID="hfPageNo" runat="server" />
                <asp:HiddenField ID="hfLastPage" runat="server" />
                <asp:HiddenField ID="hfTotalRecords" runat="server" />
                <asp:HiddenField ID="hfRecognize" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="rnkland">
            <%-- Active Btn popup Start--%>
            <asp:ModalPopupExtender ID="mpeReason" runat="server" PopupControlID="pnlReason"
                TargetControlID="btnReasonCancel" BackgroundCssClass="popbg" CancelControlID="btnReasonCancel">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlReason" runat="server" CssClass="editpopup_two" Style="display: none">
                <%-- <h3 class="edithead" align="left">
                    <asp:Literal ID="Literal26" runat="server" Text="<%$ Resources:Resource, RESET_PASSWORD%>"></asp:Literal>
                </h3>--%>
                <div class="clear">
                    &nbsp;</div>
                <div class="consumer_feild">
                    <div class="consume_name">
                        <asp:Literal ID="litEditPassword" runat="server" Text="<%$ Resources:Resource, REASON%>"></asp:Literal><span>*</span>
                    </div>
                    <div class="consume_input" style="width: 163px">
                        <asp:TextBox ID="txtReason" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanReason','Reason')"
                            TextMode="MultiLine" placeholder="Enter Reason"></asp:TextBox>
                        <span id="spanReason" class="spancolor"></span>
                    </div>
                </div>
                <div class="clear pad_10">
                </div>
                <div class="consumer_feild">
                    <%--OnClick="btnChangePwd_Click"--%>
                    <div style="margin-left: 156px; overflow: auto; float: left;">
                        <asp:Button ID="btnSave" OnClientClick="return ReasonValidate();" Text="<%$ Resources:Resource, SAVE%>"
                            CssClass="calculate_but" OnClick="btnSave_Click" runat="server" />
                        <asp:Button ID="btnReasonCancel" Text="<%$ Resources:Resource, CANCEL%>" CssClass="calculate_but"
                            runat="server" />
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/BilledMonthLockStatus.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
