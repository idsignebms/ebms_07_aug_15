﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for BilledMonthLockStatus.aspx
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 07-July-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Configuration;

namespace UMS_Nigeria.Billing
{
    public partial class BilledMonthLockStatus : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBAL = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        BillingMonthOpenBAL objBillingMonthOpenBAL = new BillingMonthOpenBAL();
        BillGenerationBal objBillGenerationBal = new BillGenerationBal();

        public int PageNum;
        XmlDocument xml = null;
        string Key = UMS_Resource.BILLED_MONTH_LOCK_STATUS;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        public int Year
        {
            get { return string.IsNullOrEmpty(ddlYear.SelectedValue) ? Constants.Zero : Convert.ToInt32(ddlYear.SelectedValue); }
            set { ddlYear.SelectedValue = value.ToString(); }
        }
        public int Month
        {
            get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? Constants.Zero : Convert.ToInt32(ddlMonth.SelectedValue); }
            set { ddlMonth.SelectedValue = value.ToString(); }
        }

        //private string Details
        //{
        //    get { return txtDetails.Text.Trim(); }
        //    set { txtDetails.Text = value; }
        //}

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                BindYears();
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridBilledMonthLockStatusList();
                BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
        }

        protected void gvBilledMonthList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                BillingMonthOpenBE objBE = new BillingMonthOpenBE();
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                Label lblBillingQueueScheduleId = (Label)row.FindControl("lblBillingQueueScheduleId");
                Label lblOpenStatusId = (Label)row.FindControl("lblOpenStatusId");
                switch (e.CommandName.ToUpper())
                {
                    case "LOCK":
                        btnSave.CommandArgument = lblBillingQueueScheduleId.Text;
                        hfRecognize.Value = UMS_Resource.LOCK;
                        mpeReason.Show();
                        break;
                    case "UNLOCK":
                        btnSave.CommandArgument = lblBillingQueueScheduleId.Text;
                        hfRecognize.Value = UMS_Resource.UNLOCK;
                        mpeReason.Show();
                        break;
                }
            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void gvBilledMonthList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblTotalAmount = (Label)e.Row.FindControl("lblTotalAmount");
                    lblTotalAmount.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblTotalAmount.Text), 2, Constants.MILLION_Format);

                    Label lblOpenStatusId = (Label)e.Row.FindControl("lblOpenStatusId");

                    if (Convert.ToInt32(lblOpenStatusId.Text) == Constants.Opened)
                    {
                        e.Row.FindControl("lkbtnLock").Visible = true;
                        e.Row.FindControl("lkbtnUnLock").Visible = false;
                    }
                    else if (Convert.ToInt32(lblOpenStatusId.Text) == Constants.Closed)
                    {
                        e.Row.FindControl("lkbtnLock").Visible = false;
                        e.Row.FindControl("lkbtnUnLock").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindMonths();
                //BindGridBilledMonthLockStatusList();
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindGridBilledMonthLockStatusList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                BillingMonthOpenBE objBE = new BillingMonthOpenBE();
                if (hfRecognize.Value == UMS_Resource.LOCK)
                {
                    objBE.BillingQueueScheduleId = Convert.ToInt32(btnSave.CommandArgument);
                    objBE.OpenStatusId = Constants.Closed;
                    objBE.Reason = txtReason.Text;
                    objBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objBE = _objiDsignBAL.DeserializeFromXml<BillingMonthOpenBE>(objBillingMonthOpenBAL.InsertBillingMonth(objBE, Statement.Change));

                    if (objBE.IsSuccess)
                    {
                        Message(Resource.BILLED_MONTH_LOCKED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        BindGridBilledMonthLockStatusList();
                        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        txtReason.Text = string.Empty;
                    }
                    else
                    {
                        Message(Resource.BILLED_MONTH_LOCKED_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
                else if (hfRecognize.Value == UMS_Resource.UNLOCK)
                {
                    objBE.BillingQueueScheduleId = Convert.ToInt32(btnSave.CommandArgument);
                    objBE.OpenStatusId = Constants.Opened;
                    objBE.Reason = txtReason.Text;
                    objBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objBE = _objiDsignBAL.DeserializeFromXml<BillingMonthOpenBE>(objBillingMonthOpenBAL.InsertBillingMonth(objBE, Statement.Change));

                    if (objBE.IsSuccess)
                    {
                        Message(Resource.BILLED_MONTH_UNLOCKED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        BindGridBilledMonthLockStatusList();
                        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    }
                    else
                    {
                        Message(Resource.BILLED_MONTH_UNLOCKED_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        private void BindGridBilledMonthLockStatusList()
        {
            try
            {
                BillingMonthOpenBE objBE = new BillingMonthOpenBE();
                BillingMonthOpenListBE objListBE = new BillingMonthOpenListBE();

                objBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                objBE.PageSize = PageSize;
                objBE.Year = Year;
                objBE.Month = Month;
                objListBE = _objiDsignBAL.DeserializeFromXml<BillingMonthOpenListBE>(objBillingMonthOpenBAL.GetBillingMonth(objBE, ReturnType.Fetch));
                DataTable dt = new DataTable();
                dt = _objiDsignBAL.ConvertListToDataSet<BillingMonthOpenBE>(objListBE.Items).Tables[0];

                if (objListBE.Items.Count > 0)
                {
                   lblStatus.Visible= divgrid.Visible = divpaging.Visible = true;
                    hfTotalRecords.Value = objListBE.Items[0].TotalRecords.ToString();

                    if(objListBE.Items[0].Cycle != Constants.Hypen)
                        lblStatus.Text = Resource.GENERATED_BY_CYCLE;
                    else
                        lblStatus.Text = Resource.GENERATED_BY_FEEDER;
                }
                else
                {
                    hfTotalRecords.Value = objListBE.Items.Count.ToString();
                    lblStatus.Visible = divgrid.Visible = divpaging.Visible = false;
                }
                gvBilledMonthList.DataSource = dt;
                gvBilledMonthList.DataBind();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindYears()
        {
            try
            {

                XmlDocument xmlResult = new XmlDocument();
                xmlResult = objBillGenerationBal.GetDetails(ReturnType.Multiple);
                BillGenerationListBe objBillsListBE = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                _objiDsignBAL.FillDropDownList(_objiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlYear, "Year", "Year", "--Select--", false);
                /// Old Code
                //int CurrentYear = DateTime.Now.Year;
                //int FromYear = Convert.ToInt32(GlobalConstants.TARIFF_ENTRY_NO_OF_LAST_YEARS);
                //for (int i = FromYear; i < CurrentYear; i++)
                //{
                //    ListItem item = new ListItem();
                //    item.Text = item.Value = i.ToString();
                //    ddlYear.Items.Add(item);

                //}
                //for (int i = 0; i <= Convert.ToInt32(GlobalConstants.TARIFF_ENTRY_NO_OF_PRESENT_AND_FUTURE_YEARS); i++)
                //{
                //    ListItem item = new ListItem();
                //    item.Text = item.Value = (CurrentYear + i).ToString();
                //    ddlYear.Items.Add(item);
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindMonths()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                _objBillGenerationBe.Year = this.Year;
                xmlResult = objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.Get);
                BillGenerationListBe objBillsListBE = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                _objiDsignBAL.FillDropDownList(_objiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlMonth, "MonthName", "Month", "--Select--", false);
                //DataSet dsMonths = new DataSet();
                //dsMonths.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["Months"]));
                //objIdsignBal.FillDropDownList(dsMonths.Tables[0], ddlMonth, "name", "value", UMS_Resource.DROPDOWN_SELECT, false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Paging
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridBilledMonthLockStatusList();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridBilledMonthLockStatusList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridBilledMonthLockStatusList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridBilledMonthLockStatusList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                BindGridBilledMonthLockStatusList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                _objiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                _objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}