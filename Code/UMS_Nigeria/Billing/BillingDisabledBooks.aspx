﻿<%@ Page Title=":: Billing Disabled BookNo's ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    AutoEventWireup="true" CodeBehind="BillingDisabledBooks.aspx.cs" Theme="Green"
    Inherits="UMS_Nigeria.Billing.BillingDisabledBooks" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdateProgress ID="UpdateProgress" runat="server">
                <ProgressTemplate>
                    <center>
                        <div id="loading-div" class="pbloading">
                            <div id="title-loading" class="pbtitle">
                                BEDC</div>
                            <center>
                                <img src="../images/loading.GIF" class="pbimg"></center>
                            <p class="pbtxt">
                                Loading Please wait.....</p>
                        </div>
                    </center>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
                PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
            <asp:UpdatePanel ID="updisabledBooks" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddCycle" runat="server" Text="<%$ Resources:Resource, BILLING_DISABLED_BOOK%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <%--<asp:DropDownList Style="width: 80px; float: left;" ID="ddlYear" runat="server" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" class="text-box select_box" Enabled="false">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlMonth" Style="width: 100px; float: left; margin-right: 10px;
                                    margin-left: 7px;" runat="server" class="text-box select_box" Enabled="false">
                                    <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                </asp:DropDownList>--%>
                                <div style="padding-top:10px;">
                                <label for="name">
                                    <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, BILLING_MONTH%>"></asp:Literal>
                                </label>
                                    <asp:Label ID="lblYear" runat="server"></asp:Label>
                                    <asp:Label ID="lblMonth" runat="server"></asp:Label>
                                </div>
                                <div class="clear space">
                                </div>
                                <span id="spanYear" class="span_color"></span><span id="spanMonth" class="span_color"
                                    style="color: Red; margin-left: 90px;"></span>
                            </div>
                        </div>
                        <div ID="divGridDetails1" runat="server">
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal4" runat="server" Text="Disable Type"></asp:Literal><span
                                        class="span_star">*</span>
                                </label>
                                <br />
                                <asp:DropDownList ID="ddlDisableType" runat="server" class="text-box select_box">
                                    <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                                </asp:DropDownList>
                                <div class="space">
                                </div>
                                <span id="spanddlDisableType" class="span_color"></span>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </label>
                                <br />
                                <asp:DropDownList ID="ddlBU" CssClass="text-box select_box" AutoPostBack="true" runat="server"
                                    OnSelectedIndexChanged="ddlBU_SelectedIndexChanged">
                                    <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                                </asp:DropDownList>
                                <div class="space">
                                </div>
                                <span id="spanBU" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litServiceUnit" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </label>
                                <br />
                                <asp:DropDownList ID="ddlSU" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlSU_SelectedIndexChanged"
                                    Enabled="false" CssClass="text-box select_box">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <div class="space">
                                </div>
                                <span id="spanSU" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litServiceCenter" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </label>
                                <br />
                                <asp:DropDownList ID="ddlSC" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSC_SelectedIndexChanged"
                                    Enabled="false" CssClass="text-box select_box">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <div class="space">
                                </div>
                                <span id="spanSC" class="span_color"></span>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litCycles" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </label>
                                <br />
                                <asp:DropDownList ID="ddlCycle" AutoPostBack="true" OnSelectedIndexChanged="ddlCycle_SelectedIndexChanged"
                                    runat="server" Enabled="false" CssClass="text-box select_box">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <div class="space">
                                </div>
                                <span id="spanCycle" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litBookNo" runat="server" Text="<%$ Resources:Resource, BOOK_NAME%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </label>
                                <br />
                                <asp:DropDownList ID="ddlBookNo" CssClass="text-box select_box" runat="server" AutoPostBack="true"
                                    Enabled="false">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <div class="space">
                                </div>
                                <span id="spanBook" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, DETAILS%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </label>
                                <br />
                                <asp:TextBox ID="txtDetails" CssClass="text-box" TextMode="MultiLine" runat="server"
                                    placeholder="Enter Details Here."></asp:TextBox>
                                <div class="space">
                                </div>
                                <span id="spanDetails" class="span_color"></span>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="lblDisableDate" runat="server" Text="<%$ Resources:Resource, DISABLE_DATE%>"></asp:Literal>
                                    <span class="span_star">*</span></label>
                                <div class="space">
                                </div>
                                <asp:TextBox ID="txtDisableDate" CssClass="text-box" runat="server" MaxLength="10"
                                    AutoComplete="off" placeholder="DD/MM/YYYY" onblur="TextBoxBlurValidationlbl(this, 'spanDisableDate', 'Disable Date')"
                                    onChange="TextBoxBlurValidationlbl(this, 'spanDisableDate', 'Disable Date')"></asp:TextBox>
                                <asp:Image ID="imgDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                    vertical-align: middle" AlternateText="Calender" runat="server" />
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtDisableDate"
                                    FilterType="Custom,Numbers" ValidChars="/">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanDisableDate" class="span_color"></span>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Button ID="btnSave" Text="<%$ Resources:Resource, DISABLE%>" CssClass="box_s"
                                    OnClick="btnSave_Click" OnClientClick="return Validate();" runat="server" />
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                &nbsp;
                            </div>
                        </div>
                        </div>
                    </div>
                    <div ID="divGridDetails2" runat="server">
                    <div style="float: right;">
                        <span class="fltl">
                            <div class="pad_5 fltr">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, SELECT_YEAR%>"></asp:Literal>
                                        &nbsp;&nbsp;&nbsp;</label>
                                    <asp:DropDownList ID="ddlGvYear" Style="margin-right: 20px; width: 80px;" runat="server"
                                        OnSelectedIndexChanged="ddlGvYear_SelectedIndexChanged" AutoPostBack="true" CssClass="text-box">
                                        <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, SELECT_MONTH%>"></asp:Literal>
                                    &nbsp;&nbsp;&nbsp;
                                    <asp:DropDownList ID="ddlGvMonth" Style="margin-right: 20px; width: 100px;" runat="server"
                                        OnSelectedIndexChanged="ddlGvMonth_SelectedIndexChanged" AutoPostBack="true"
                                        CssClass="text-box">
                                        <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </span>
                    </div>
                    <div class="grid_boxes">
                        <div class="grid_paging_top">
                            <div class="paging_top_title">
                                <asp:Literal ID="litBuList" runat="server" Text="<%$ Resources:Resource, BILLING_DISABLED_BOOK_LIST%>"></asp:Literal>
                            </div>
                            <div class="paging_top_right_content">
                                <div id="divgrid" class="grid" runat="server" style="width: 100% !important; float: left;">
                                    <div id="divpaging" runat="server">
                                        <div align="right" class="marg_20t" runat="server" id="divLinks">
                                            <asp:LinkButton ID="lkbtnFirst" runat="server" CssClass="pagingfirst" OnClick="lkbtnFirst_Click">
                                                <asp:Literal ID="litFirst" runat="server" Text="<%$ Resources:Resource, FIRST%>"></asp:Literal></asp:LinkButton>
                                            &nbsp;|
                                            <asp:LinkButton ID="lkbtnPrevious" runat="server" CssClass="pagingfirst" OnClick="lkbtnPrevious_Click">
                                                <asp:Literal ID="litPrevious" runat="server" Text="<%$ Resources:Resource, PREVIOUS%>"></asp:Literal></asp:LinkButton>
                                            &nbsp;|
                                            <asp:Label ID="lblCurrentPage" runat="server" Font-Bold="true"></asp:Label>
                                            &nbsp;|
                                            <asp:LinkButton ID="lkbtnNext" runat="server" CssClass="pagingfirst" OnClick="lkbtnNext_Click">
                                                <asp:Literal ID="litNext" runat="server" Text="<%$ Resources:Resource, NEXT%>"></asp:Literal></asp:LinkButton>
                                            &nbsp;|
                                            <asp:LinkButton ID="lkbtnLast" runat="server" CssClass="pagingfirst" OnClick="lkbtnLast_Click">
                                                <asp:Literal ID="litLast" runat="server" Text="<%$ Resources:Resource, LAST%>"></asp:Literal></asp:LinkButton>
                                            &nbsp;|<asp:Literal ID="litPageSize" runat="server" Text="<%$ Resources:Resource, PAGE_SIZE%>"></asp:Literal>
                                            <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" CssClass="paging-size"
                                                OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                                <asp:ListItem>100</asp:ListItem>
                                                <asp:ListItem>200</asp:ListItem>
                                                <asp:ListItem>300</asp:ListItem>
                                                <asp:ListItem>400</asp:ListItem>
                                                <asp:ListItem>500</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_tb" id="divPrint" runat="server" align="center" style="overflow-x: auto;">
                        <asp:GridView ID="gvBooksNo" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvBooksNo_RowDataBound"
                            HeaderStyle-CssClass="grid_tb_hd">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:BoundField HeaderText="<%$ Resources:Resource, GRID_SNO%>" DataField="RowNumber" />
                                <%--<asp:BoundField HeaderText="<%$ Resources:Resource, BOOK_NO%>" DataField="BookNo" />--%>
                                <asp:BoundField HeaderText="<%$ Resources:Resource, BUSINESS_UNIT%>" DataField="BusinessUnitName" />
                                <asp:BoundField HeaderText="<%$ Resources:Resource, SERVICE_UNIT%>" DataField="ServiceUnitName" />
                                <asp:BoundField HeaderText="<%$ Resources:Resource, SERVICE_CENTER%>" DataField="ServiceCenterName" />
                                <asp:BoundField HeaderText="<%$ Resources:Resource, CYCLE%>" DataField="CycleName" />
                                <asp:BoundField HeaderText="<%$ Resources:Resource, BOOK_NAME%>" DataField="ID" />
                                <asp:BoundField HeaderText="<%$ Resources:Resource, MONTH_YEAR%>" DataField="MonthYear" />
                                <asp:BoundField HeaderText="Disable Type" DataField="DisableType" />
                                <asp:BoundField HeaderText="Disable Date" DataField="DisableDate" />
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, DETAILS%>" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDetails" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lkbtnRemove" runat="server" ToolTip="Want to enable this Book"
                                            Text="Enable" OnClick="lkbtnRemove_Click" CommandArgument='<%# Eval("DisabledBookId") %>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle HorizontalAlign="center"></RowStyle>
                        </asp:GridView>
                        <div class="clear pad_10">
                        </div>
                    </div>
                    </div>
                    <asp:HiddenField ID="hfPageSize" runat="server" />
                    <asp:HiddenField ID="hfPageNo" runat="server" />
                    <asp:HiddenField ID="hfLastPage" runat="server" />
                    <asp:HiddenField ID="hfRecognize" runat="server" />
                    <asp:HiddenField ID="hfDisabledBookId" runat="server" />
                    <asp:HiddenField ID="hfTotalRecords" runat="server" />
                    <asp:HiddenField ID="hfMonth" runat="server" Value="0" />
                    <asp:HiddenField ID="hfYear" runat="server" Value="0" />
                    <%-- Active Btn popup Start--%>
                    <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                        TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnActivate" runat="server" Text="Button" CssClass="popbtn" />
                    <%--<asp:Panel ID="PanelActivate" runat="server" DefaultButton="btnActiveOk" CssClass="modalPopup">--%>
                    <asp:Panel ID="PanelActivate" runat="server" CssClass="modalPopup">
                        <div class="popheader popheaderlblgreen" id="pop" runat="server">
                            <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                        </div>
                        <br />
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <asp:CheckBox ID="cbIsPartialBill" Visible="false" runat="server" Text="Is Partial Bill" />
                                <asp:Button ID="btnActiveOk" runat="server" OnClick="btnActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Active  Btn popup Closed--%>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="PanelAlert"
                TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btnAlertOk">
            </asp:ModalPopupExtender>
            <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
            <asp:Panel ID="PanelAlert" runat="server" CssClass="modalPopup" Style="display: none;">
                <div class="popheader popheaderlblred">
                    <asp:Label ID="lblAlertMsg" runat="server"></asp:Label>
                </div>
                <div class="footer popfooterbtnrt">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="btnAlertOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="ok_btn" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/BillingDisabledBooks.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
