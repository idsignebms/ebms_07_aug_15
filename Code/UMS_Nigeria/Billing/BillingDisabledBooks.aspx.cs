﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for BillingDisabledBooks
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 25-Sep-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Configuration;

namespace UMS_Nigeria.Billing
{
    public partial class BillingDisabledBooks : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        BillGenerationBal objBillGenerationBal = new BillGenerationBal();
        BillingDisabledBooksBal objDisabledBooksBal = new BillingDisabledBooksBal();
        public int PageNum;
        string Key = UMS_Resource.BILLING_DISABLE_DBOOKS;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        private int YearId
        {
            get { return string.IsNullOrEmpty(hfYear.Value) ? 0 : Convert.ToInt32(hfYear.Value); }
            //get { return string.IsNullOrEmpty(ddlYear.SelectedValue) ? 0 : Convert.ToInt32(ddlYear.SelectedValue); }
        }
        private int MonthId
        {
            get { return string.IsNullOrEmpty(hfMonth.Value) ? 0 : Convert.ToInt32(hfMonth.Value); }
            //get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? 0 : Convert.ToInt32(ddlMonth.SelectedValue); }
        }
        public string BusinessUnitName
        {
            get { return string.IsNullOrEmpty(ddlBU.SelectedValue) ? string.Empty : ddlBU.SelectedValue; }
            set { ddlBU.SelectedValue = value.ToString(); }
        }
        public string ServiceUnitName
        {
            get { return string.IsNullOrEmpty(ddlSU.SelectedValue) ? string.Empty : ddlSU.SelectedValue; }
            set { ddlSU.SelectedValue = value.ToString(); }
        }
        public string ServiceCenterName
        {
            get { return string.IsNullOrEmpty(ddlSC.SelectedValue) ? string.Empty : ddlSC.SelectedValue; }
            set { ddlSC.SelectedValue = value.ToString(); }
        }
        public string BookNo
        {
            get { return string.IsNullOrEmpty(ddlBookNo.SelectedValue) ? string.Empty : ddlBookNo.SelectedValue; }
            set { ddlBookNo.SelectedValue = value.ToString(); }
        }
        public string Cycle
        {
            get { return string.IsNullOrEmpty(ddlCycle.SelectedValue) ? string.Empty : ddlCycle.SelectedValue; }
            set { ddlCycle.SelectedValue = value.ToString(); }
        }
        private int GvYearId
        {
            get { return string.IsNullOrEmpty(ddlGvYear.SelectedValue) ? 0 : Convert.ToInt32(ddlGvYear.SelectedValue); }
        }
        private int GvMonthId
        {
            get { return string.IsNullOrEmpty(ddlGvMonth.SelectedValue) ? 0 : Convert.ToInt32(ddlGvMonth.SelectedValue); }
        }
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                lblMessage.Text = pnlMessage.CssClass = string.Empty;
                if (!IsPostBack)
                {
                    string path = string.Empty;
                    path = _objCommonMethods.GetPagePath(this.Request.Url);
                    if (_objCommonMethods.IsAccess(path))
                    {
                        bool IsOpenMonthAvilable = BindOpenMonthAndYear();
                        if (IsOpenMonthAvilable)
                        {
                            BindDropDowns();
                            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                            {
                                ddlBU.SelectedIndex = ddlBU.Items.IndexOf(ddlBU.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                                ddlBU.Enabled = false;
                                ddlBU_SelectedIndexChanged(ddlBU, new EventArgs());
                            }
                            hfPageNo.Value = Constants.pageNoValue.ToString();
                            BindDisabledBookNos();
                            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            divGridDetails1.Visible = divGridDetails2.Visible = true;
                        }
                        else 
                        {
                            divGridDetails1.Visible = divGridDetails2.Visible = false;
                            lblMonth.Text = "Open month not avilable";
                        }
                    }
                    else
                    {
                        Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    }
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            hfRecognize.Value = Constants.SAVE;
            pop.Attributes.Add("class", "popheader popheaderlblred");
            lblActiveMsg.Text = Resource.ARE_YOU_SURE_DISABLE_BOOK;
            cbIsPartialBill.Visible = false;
            btnActiveOk.Focus();
            mpeActivate.Show();
        }

        protected void gvBooksNo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblDetails = (Label)e.Row.FindControl("lblDetails");
                    lblDetails.Text = _objiDsignBal.ReplaceNewLines(lblDetails.Text, false);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region DDLEvents
        protected void ddlBU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlBU.SelectedIndex > 0)
                {
                    _objCommonMethods.BindUserServiceUnits_BuIds(ddlSU, BusinessUnitName, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlSU.Enabled = true;
                }
                else
                {
                    ddlSU.SelectedIndex = ddlSC.SelectedIndex = ddlCycle.SelectedIndex = ddlBookNo.SelectedIndex = Constants.Zero;
                    ddlSU.Enabled = ddlSC.Enabled = ddlCycle.Enabled = ddlBookNo.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlSU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlSU.SelectedIndex > 0)
                {
                    _objCommonMethods.BindUserServiceCenters_SuIds(ddlSC, ServiceUnitName, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlSC.Enabled = true;
                }
                else
                {
                    ddlSC.SelectedIndex = ddlCycle.SelectedIndex = ddlBookNo.SelectedIndex = Constants.Zero;
                    ddlSC.Enabled = ddlCycle.Enabled = ddlBookNo.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlSC.SelectedIndex > 0)
                {
                    _objCommonMethods.BindCyclesByBUSUSC(ddlCycle, BusinessUnitName, ServiceUnitName, ServiceCenterName, true, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlCycle.Enabled = true;
                }
                else
                {
                    ddlCycle.Enabled = ddlBookNo.Enabled = false;
                    ddlCycle.SelectedIndex = ddlBookNo.SelectedIndex = Constants.Zero;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlGvYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDisabledBookNos();
            CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
        }
        protected void ddlGvMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDisabledBookNos();
            CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
        }
        protected void ddlCycle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCycle.SelectedIndex > 0)
            {
                _objCommonMethods.BindNotDisabledBooks(ddlBookNo, ddlCycle.SelectedValue, true, false, UMS_Resource.DROPDOWN_SELECT);
                ddlBookNo.Enabled = true;
            }
            else
            {
                ddlBookNo.Enabled = false;
                ddlBookNo.SelectedIndex = Constants.Zero;
            }
        }
        protected void lkbtnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                cbIsPartialBill.Visible = true;
                cbIsPartialBill.Checked = false;
                hfRecognize.Value = Constants.DELETE;
                lblActiveMsg.Text = Resource.ARE_YOU_SURE_REMOVE_BOOK;
                LinkButton lkbtnRemove = (LinkButton)sender;
                hfDisabledBookId.Value = lkbtnRemove.CommandArgument;
                btnActiveOk.Focus();
                mpeActivate.Show();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (hfRecognize.Value == Constants.DELETE)
                {
                    cbIsPartialBill.Visible = false;
                    BillingDisabledBooksBe objDisabledBooksBe = new BillingDisabledBooksBe();
                    objDisabledBooksBe.DisabledBookId = Convert.ToInt32(hfDisabledBookId.Value);
                    if (cbIsPartialBill.Checked == true)
                        objDisabledBooksBe.IsPartialBill = true;
                    else
                        objDisabledBooksBe.IsPartialBill = false;
                    objDisabledBooksBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objDisabledBooksBe = _objiDsignBal.DeserializeFromXml<BillingDisabledBooksBe>(objDisabledBooksBal.Insert(objDisabledBooksBe, Statement.Update));
                    if (objDisabledBooksBe.IsSuccess)
                    {
                        BindDisabledBookNos();
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        cbIsPartialBill.Checked = false;
                        Message(Resource.BILLING_BOOK_REMOVED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    }
                    else
                        Message(Resource.BILLING_BOOK_REMOVED_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (hfRecognize.Value == Constants.SAVE)
                {
                    bool IsApproval = _objCommonMethods.IsApproval(UMS_Resource.FEATURE_BILLING_DISABLED_BOOKS);
                    BillingDisabledBooksBe objDisabledBooksBe = new BillingDisabledBooksBe();
                    objDisabledBooksBe.YearId = YearId;
                    objDisabledBooksBe.MonthId = MonthId;
                    objDisabledBooksBe.BookNo = BookNo;
                    objDisabledBooksBe.DisableTypeId = Convert.ToInt32(ddlDisableType.SelectedValue);
                    objDisabledBooksBe.Details = _objiDsignBal.ReplaceNewLines(txtDetails.Text, true);
                    objDisabledBooksBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objDisabledBooksBe.DisableDate = _objCommonMethods.Convert_MMDDYY(txtDisableDate.Text);
                    if (IsApproval)
                    {
                        objDisabledBooksBe.ApproveStatusId = Convert.ToInt32(EnumApprovalStatus.New);
                        objDisabledBooksBe = _objiDsignBal.DeserializeFromXml<BillingDisabledBooksBe>(objDisabledBooksBal.Insert(objDisabledBooksBe, Statement.Insert));
                    }
                    else
                    {
                        objDisabledBooksBe.ApproveStatusId = Convert.ToInt32(EnumApprovalStatus.Approved);
                        objDisabledBooksBe = _objiDsignBal.DeserializeFromXml<BillingDisabledBooksBe>(objDisabledBooksBal.Insert(objDisabledBooksBe, Statement.Insert));
                    }

                    if (objDisabledBooksBe.IsSuccess)
                    {
                        BindDisabledBookNos();
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        Message(Resource.BILLING_BOOK_SAVED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        //ddlYear.SelectedIndex = ddlMonth.SelectedIndex = 
                        ddlSU.SelectedIndex = ddlSC.SelectedIndex = ddlCycle.SelectedIndex = ddlBookNo.SelectedIndex = ddlDisableType.SelectedIndex = 0;
                        txtDetails.Text = txtDisableDate.Text = string.Empty;
                        ddlSC.Enabled = ddlCycle.Enabled = ddlBookNo.Enabled = false;
                    }
                    else if (objDisabledBooksBe.BookNoExistsForThisMonth)
                    {
                        Message(Resource.BILLING_BOOK_EXISTS_FOR_YEARMONTH, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else
                        Message(Resource.BILLING_BOOK_SAVED_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                }
                hfRecognize.Value = string.Empty;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        private bool BindOpenMonthAndYear()
        {
            bool IsOpenMonthAvilable = false;
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                xmlResult = objBillGenerationBal.GetDetails(ReturnType.Single);
                EstimationBE objEstimationBE = _objiDsignBal.DeserializeFromXml<EstimationBE>(xmlResult);
                if (objEstimationBE != null)
                {
                    hfMonth.Value = objEstimationBE.Month.ToString();
                    hfYear.Value = objEstimationBE.Year.ToString();
                    lblMonth.Text = objEstimationBE.MonthName;
                    lblYear.Text = objEstimationBE.Year.ToString();
                    IsOpenMonthAvilable = true;
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            return IsOpenMonthAvilable;
        }

        private void BindDropDowns()
        {
            _objCommonMethods.BindUserBusinessUnits(ddlBU, string.Empty, false, UMS_Resource.DROPDOWN_SELECT);
            _objCommonMethods.BindAllMonths(ddlGvMonth, true, Resource.DDL_ALL, false);
            _objCommonMethods.BindDisabledTypes(ddlDisableType, true, UMS_Resource.DROPDOWN_SELECT, false);
            BindGvddlYears();
        }
        private void BindDisabledBookNos()
        {
            BillingDisabledBooksBe objDisabledBooksBe = new BillingDisabledBooksBe();
            objDisabledBooksBe.YearId = GvYearId;
            objDisabledBooksBe.MonthId = GvMonthId;
            objDisabledBooksBe.PageNo = Convert.ToInt32(hfPageNo.Value);
            objDisabledBooksBe.PageSize = PageSize;
            objDisabledBooksBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
            DataSet ds = new DataSet();
            ds = objDisabledBooksBal.GetDisabledBooks(objDisabledBooksBe);
            //BillingDisabledBooksListBe _objListBe = _objiDsignBal.DeserializeFromXml<BillingDisabledBooksListBe>(xmlresult);

            if (ds.Tables[0].Rows.Count > 0)
            {
                divgrid.Visible = true;
                hfTotalRecords.Value = ds.Tables[0].Rows[0]["TotalRecords"].ToString();
                gvBooksNo.DataSource = ds.Tables[0];
                gvBooksNo.DataBind();
            }
            else
            {
                divgrid.Visible = false;
                hfTotalRecords.Value = Constants.Zero.ToString();
                gvBooksNo.DataSource = new DataTable();
                gvBooksNo.DataBind();
            }
        }
        private void BindGvddlYears()
        {
            try
            {
                int CurrentYear = DateTime.Now.Year;
                int FromYear = Convert.ToInt32(GlobalConstants.TARIFF_ENTRY_NO_OF_LAST_YEARS);
                for (int i = FromYear; i <= CurrentYear; i++)
                {
                    ListItem item = new ListItem();
                    item.Text = item.Value = i.ToString();
                    ddlGvYear.Items.Add(item);
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        
        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Paging
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindDisabledBookNos();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindDisabledBookNos();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindDisabledBookNos();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindDisabledBookNos();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                BindDisabledBookNos();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                _objiDsignBal.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                _objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}