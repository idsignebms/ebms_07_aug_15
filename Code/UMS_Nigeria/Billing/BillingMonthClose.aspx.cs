﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for Billing Month Close
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 29-July-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Configuration;

namespace UMS_Nigeria.Billing
{
    public partial class BillingMonthClose : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBAL = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        BillingMonthOpenBAL objBillingMonthOpenBAL = new BillingMonthOpenBAL();
        BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
        public int PageNum;
        XmlDocument xml = null;
        string Key = UMS_Resource.BILLING_MONTH_CLOSE;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                string path = string.Empty;
                path = _objCommonMethods.GetPagePath(this.Request.Url);
                if (_objCommonMethods.IsAccess(path))
                {
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    BindGridBillMonthCloseList();
                    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                }
                else { Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }
            }
        }

        protected void gvBillingMonthCloseList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                BillingMonthOpenBE objBE = new BillingMonthOpenBE();
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                Label lblBillMonthId = (Label)row.FindControl("lblBillMonthId");

                switch (e.CommandName.ToUpper())
                {
                    case "STATUSLOCK":
                        lblActiveMsg.Text = Resource.CLOSE_MONTH;
                        hfOpenStatusId.Value = lblBillMonthId.Text;
                        pop.Attributes.Add("class", "popheader popheaderlblred");
                        hfRecognize.Value = UMS_Resource.LOCK;
                        mpeActivate.Show();
                        break;
                }
            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                BillingMonthOpenBE objBE = new BillingMonthOpenBE();
                if (hfRecognize.Value == UMS_Resource.LOCK)
                {
                    objBE.BillMonthId = Convert.ToInt32(hfOpenStatusId.Value);
                    objBE.OpenStatusId = Constants.Closed;
                    objBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objBE = _objiDsignBAL.DeserializeFromXml<BillingMonthOpenBE>(objBillingMonthOpenBAL.InsertBillingMonth(objBE, Statement.Update));

                    if (Convert.ToBoolean(objBE.IsSuccess))
                    {
                        BindGridBillMonthCloseList();
                        BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        Message(UMS_Resource.BILL_MONTH_LOCKED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    }
                    else
                    {
                        Message(UMS_Resource.BILL_MONTH_LOCKED_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        #endregion

        #region Paging
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridBillMonthCloseList();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridBillMonthCloseList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridBillMonthCloseList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridBillMonthCloseList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                BindGridBillMonthCloseList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                _objiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                _objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods

        private void BindGridBillMonthCloseList()
        {
            try
            {
                BillingMonthOpenBE objBE = new BillingMonthOpenBE();
                BillingMonthOpenListBE objListBE = new BillingMonthOpenListBE();

                objBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                objBE.PageSize = PageSize;
                objBE.OpenStatusId = Constants.Opened;

                objListBE = _objiDsignBAL.DeserializeFromXml<BillingMonthOpenListBE>(objBillingMonthOpenBAL.GetBillingMonth(objBE, ReturnType.Get));
                DataTable dt = new DataTable();
                dt = _objiDsignBAL.ConvertListToDataSet<BillingMonthOpenBE>(objListBE.Items).Tables[0];


                if (objListBE.Items.Count > 0)
                {
                    divgrid.Visible = divpaging.Visible = true;
                    hfTotalRecords.Value = objListBE.Items[0].TotalRecords.ToString();
                }
                else
                {
                    hfTotalRecords.Value = objListBE.Items.Count.ToString();
                    divgrid.Visible = divpaging.Visible = false;
                }
                gvBillingMonthCloseList.DataSource = dt;
                gvBillingMonthCloseList.DataBind();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}