﻿<%@ Page Title="::Bill Month Open::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="BillingMonthOpen.aspx.cs" Inherits="UMS_Nigeria.Billing.BillingMonthOpen" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdateProgress ID="UpdateProgress" runat="server">
                <ProgressTemplate>
                    <center>
                        <div id="loading-div" class="pbloading">
                            <div id="title-loading" class="pbtitle">
                                BEDC</div>
                            <center>
                                <img src="../images/loading.GIF" class="pbimg"></center>
                            <p class="pbtxt">
                                Loading Please wait.....</p>
                        </div>
                    </center>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
                PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
            <asp:UpdatePanel ID="upAddCycle" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddCycle" runat="server" Text="<%$ Resources:Resource, BILL_MONTH_OPEN%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, YEAR%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </label>
                                <br />
                                <asp:DropDownList ID="ddlYear" onchange="DropDownlistOnChangelbl(this,'spanYear',' Year')"
                                    AutoPostBack="true" runat="server" CssClass="text-box text-select">
                                    <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                                </asp:DropDownList>
                                <div class="clear space">
                                </div>
                                <span id="spanYear" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litServiceUnit" runat="server" Text="<%$ Resources:Resource, MONTH%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </label>
                                <br />
                                <asp:DropDownList ID="ddlMonth" onchange="DropDownlistOnChangelbl(this,'spanMonth',' Month')"
                                    AutoPostBack="true" runat="server" CssClass="text-box text-select">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <div class="clear space">
                                </div>
                                <span id="spanMonth" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litDetails" runat="server" Text="<%$ Resources:Resource, DETAILS%>"></asp:Literal>
                                </label>
                                <br />
                                <asp:TextBox ID="txtDetails" runat="server" CssClass="text-box" placeholder="Enter Details"
                                    TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <asp:Button ID="btnOpen" OnClientClick="return Validate();" OnClick="btnOpen_Click"
                                Text="<%$ Resources:Resource, OPEN%>" CssClass="box_s" runat="server" />
                            <asp:Button ID="btnReset" OnClientClick="return ClearControls();" Text="<%$ Resources:Resource, RESET%>"
                                CssClass="box_s" runat="server" />
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="grid_boxes">
                        <div class="grid_paging_top">
                            <div class="paging_top_title">
                                <asp:Literal ID="litBuList" runat="server" Text="<%$ Resources:Resource, BILL_MONTH_OPEN_LIST%>"></asp:Literal>
                            </div>
                            <div class="paging_top_right_content">
                                <div id="divgrid" class="grid" runat="server" style="width: 100% !important; float: left;">
                                    <div id="divpaging" runat="server">
                                        <div align="right" class="marg_20t" runat="server" id="divLinks">
                                            <asp:LinkButton ID="lkbtnFirst" runat="server" CssClass="pagingfirst" OnClick="lkbtnFirst_Click">
                                                <asp:Literal ID="litFirst" runat="server" Text="<%$ Resources:Resource, FIRST%>"></asp:Literal></asp:LinkButton>
                                            &nbsp;|
                                            <asp:LinkButton ID="lkbtnPrevious" runat="server" CssClass="pagingfirst" OnClick="lkbtnPrevious_Click">
                                                <asp:Literal ID="litPrevious" runat="server" Text="<%$ Resources:Resource, PREVIOUS%>"></asp:Literal></asp:LinkButton>
                                            &nbsp;|
                                            <asp:Label ID="lblCurrentPage" runat="server" Font-Bold="true"></asp:Label>
                                            &nbsp;|
                                            <asp:LinkButton ID="lkbtnNext" runat="server" CssClass="pagingfirst" OnClick="lkbtnNext_Click">
                                                <asp:Literal ID="litNext" runat="server" Text="<%$ Resources:Resource, NEXT%>"></asp:Literal></asp:LinkButton>
                                            &nbsp;|
                                            <asp:LinkButton ID="lkbtnLast" runat="server" CssClass="pagingfirst" OnClick="lkbtnLast_Click">
                                                <asp:Literal ID="litLast" runat="server" Text="<%$ Resources:Resource, LAST%>"></asp:Literal></asp:LinkButton>
                                            &nbsp;|<asp:Literal ID="litPageSize" runat="server" Text="<%$ Resources:Resource, PAGE_SIZE%>"></asp:Literal>
                                            <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" CssClass="paging-size"
                                                OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_tb" id="divPrint" runat="server" align="center" style="overflow-x: auto;">
                        <asp:GridView ID="gvBillingMonthList" runat="server" AutoGenerateColumns="False"
                            HeaderStyle-CssClass="grid_tb_hd" OnRowCommand="gvBillingMonthList_RowCommand"
                            OnRowDataBound="gvBillingMonthList_RowDataBound">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                    HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblOpenStatusId" Visible="false" runat="server" Text='<%#Eval("OpenStatusId")%>'></asp:Label>
                                        <asp:Label ID="Label1" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        <asp:Label ID="lblBillMonthId" Visible="false" runat="server" Text='<%#Eval("BillMonthId")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, MONTH_YEAR%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblYear" Visible="false" runat="server" Text='<%#Eval("Year") %>'></asp:Label>
                                        <asp:Label ID="lblMonth" Visible="false" runat="server" Text='<%#Eval("Month") %>'></asp:Label>
                                        <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, STATUS_TYPE%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatusType" runat="server" Text='<%#Eval("StatusType") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, DETAILS%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDetails" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lkbtnStatusLock" runat="server" ToolTip="Want to close this month"
                                            Text="Close" CommandName="StatusLock" CommandArgument='<%# Eval("OpenStatusId") %>'>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnUnLock" CommandArgument='<%# Eval("OpenStatusId") %>' runat="server"
                                            Text="Open" ToolTip="Want to open this month" CommandName="UnLock"></asp:LinkButton>
                                            <asp:Label Visible="false" ID="lblHyphen" runat="server" Text="--"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <asp:ModalPopupExtender ID="MpAlert" runat="server" PopupControlID="pnlAlert" TargetControlID="btnAlertOk"
                        BackgroundCssClass="modalBackground" CancelControlID="btnAlertOk">
                    </asp:ModalPopupExtender>
                    <asp:Panel ID="pnlAlert" runat="server" CssClass="modalPopup" Style="display: none;
                        border-color: #CD3333;">
                        <div class="popheader" style="background-color: #CD3333;">
                            <asp:Label ID="lblAlertMessage" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="footer" align="right">
                            <asp:Button ID="btnAlertOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="no" />
                        </div>
                    </asp:Panel>
                    <%-- Active Btn popup Start--%>
                    <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                        TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnActivate" runat="server" Text="Button" CssClass="popbtn" />
                    <asp:Panel ID="PanelActivate" runat="server" DefaultButton="btnActiveOk" CssClass="modalPopup"
                        class="poppnl">
                        <div class="popheader popheaderlblred" id="pop" runat="server">
                            <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                        </div>
                        <br />
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnActiveOk" runat="server" OnClick="btnActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Active  Btn popup Closed--%>
                    <asp:HiddenField ID="hfPageSize" runat="server" />
                    <asp:HiddenField ID="hfPageNo" runat="server" />
                    <asp:HiddenField ID="hfLastPage" runat="server" />
                    <asp:HiddenField ID="hfTotalRecords" runat="server" />
                    <asp:HiddenField ID="hfRecognize" runat="server" />
                    <asp:HiddenField ID="hfYearId" runat="server" />
                    <asp:HiddenField ID="hfMonthId" runat="server" />
                    <asp:HiddenField ID="hfOpenStatusId" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <%--==============Escape button script starts==============--%>
            <%--==============Escape button script ends==============--%>
            <%--==============Validation script ref starts==============--%>
            <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
            <script src="../JavaScript/PageValidations/BillingMonthOpen.js" type="text/javascript"></script>
            <%--==============Validation script ref ends==============--%>
            <%--==============Ajax loading script starts==============--%>
            <script type="text/javascript">
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(BeginRequestHandler);
                // Raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(EndRequestHandler);
                function BeginRequestHandler(sender, args) {
                    //Shows the modal popup - the update progress
                    var popup = $find('<%= modalPopupLoading.ClientID %>');
                    if (popup != null) {
                        popup.show();
                    }
                }

                function EndRequestHandler(sender, args) {
                    //Hide the modal popup - the update progress
                    var popup = $find('<%= modalPopupLoading.ClientID %>');
                    if (popup != null) {
                        popup.hide();
                    }
                }
   
            </script>
            <%--==============Validation script ref ends==============--%>
        </div>
    </div>
</asp:Content>
