﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for Billing Month Open
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 19-May-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Configuration;

namespace UMS_Nigeria.Billing
{
    public partial class BillingMonthOpen : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBAL = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        BillingMonthOpenBAL objBillingMonthOpenBAL = new BillingMonthOpenBAL();
        BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
        public int PageNum;
        XmlDocument xml = null;
        string Key = UMS_Resource.BILLING_MONTH_OPEN;

        private delegate void Delegate_Insert_ClosedMonthData(int year, int month);
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        public int Year
        {
            get { return string.IsNullOrEmpty(ddlYear.SelectedValue) ? Constants.Zero : Convert.ToInt32(ddlYear.SelectedValue); }
            set { ddlYear.SelectedValue = value.ToString(); }
        }
        public int Month
        {
            get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? Constants.Zero : Convert.ToInt32(ddlMonth.SelectedValue); }
            set { ddlMonth.SelectedValue = value.ToString(); }
        }

        private string Details
        {
            get { return txtDetails.Text.Trim(); }
            set { txtDetails.Text = value; }
        }

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                string path = string.Empty;
                path = _objCommonMethods.GetPagePath(this.Request.Url);
                if (_objCommonMethods.IsAccess(path))
                {
                    BindYears();
                    BindMonths();
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    BindGridBillMonthOpenList();
                    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                }
                else { Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }
            }
        }

        protected void gvBillingMonthList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                BillingMonthOpenBE objBE = new BillingMonthOpenBE();
                BillingMonthOpenListBE objListBE = new BillingMonthOpenListBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                LinkButton lkbtnStatusLock = (LinkButton)row.FindControl("lkbtnStatusLock");
                LinkButton lkbtnUnLock = (LinkButton)row.FindControl("lkbtnUnLock");
                Label lblBillMonthId = (Label)row.FindControl("lblBillMonthId");
                Label lblMonth = (Label)row.FindControl("lblMonth");
                Label lblYear = (Label)row.FindControl("lblYear");

                btnActiveOk.Visible = true;
                btnActiveCancel.Text = Resource.CANCEL;

                switch (e.CommandName.ToUpper())
                {
                    case "STATUSLOCK":
                        lblActiveMsg.Text = Resource.CLOSE_MONTH;
                        hfOpenStatusId.Value = lblBillMonthId.Text;
                        hfYearId.Value = lblYear.Text;
                        hfMonthId.Value = lblMonth.Text;
                        pop.Attributes.Add("class", "popheader popheaderlblred");
                        hfRecognize.Value = UMS_Resource.LOCK;
                        mpeActivate.Show();
                        break;
                    case "UNLOCK":
                        if (IsPreviousMonthOpeninGrid(Convert.ToInt32(lblYear.Text), Convert.ToInt32(lblMonth.Text)))
                        {
                            lblAlertMessage.Text = Resource.PREVIOUS_MONTH_CLOSE_MESSAGE;
                            MpAlert.Show();
                        }
                        else if (IsGreaterMonthOpeninGrid(Convert.ToInt32(lblYear.Text), Convert.ToInt32(lblMonth.Text)))
                        {
                            lblAlertMessage.Text = Resource.PREVIOUS_MONTH_CLOSE_MESSAGE;
                            MpAlert.Show();
                        }
                        else
                        {
                            lblActiveMsg.Text = Resource.OPEN_MONTH;
                            hfOpenStatusId.Value = lblBillMonthId.Text;
                            hfRecognize.Value = UMS_Resource.UNLOCK;
                            pop.Attributes.Add("class", "popheader popheaderlblgreen");
                            mpeActivate.Show();
                        }
                        break;
                }
            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                BillingMonthOpenBE objBE = new BillingMonthOpenBE();
                if (hfRecognize.Value == UMS_Resource.LOCK)
                {
                    objBE.BillMonthId = Convert.ToInt32(hfOpenStatusId.Value);
                    objBE.OpenStatusId = Constants.Closed;
                    objBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objBE = _objiDsignBAL.DeserializeFromXml<BillingMonthOpenBE>(objBillingMonthOpenBAL.InsertBillingMonth(objBE, Statement.Update));

                    if (Convert.ToBoolean(objBE.IsSuccess))
                    {
                        BindGridBillMonthOpenList();
                        BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        Delegate_Insert_ClosedMonthData _objDelegate_Insert_ClosedMonthData = new Delegate_Insert_ClosedMonthData(Insert_ClosedMonthData);
                        _objDelegate_Insert_ClosedMonthData.BeginInvoke(Convert.ToInt32(hfYearId.Value), Convert.ToInt32(hfMonthId.Value), null, null);
                        Message(UMS_Resource.BILL_MONTH_LOCKED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    }
                    else if (objBE.IsSomeBGNotBilled)
                    {
                        btnActiveOk.Visible = false;
                        btnActiveCancel.Text = Resource.OK;
                        lblActiveMsg.Text = "Some Book Groups are not yet Billed! You can't close this month.";
                        pop.Attributes.Add("class", "popheader popheaderlblred");
                        mpeActivate.Show();
                    }
                    else
                    {
                        Message(UMS_Resource.BILL_MONTH_LOCKED_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
                else if (hfRecognize.Value == UMS_Resource.UNLOCK)
                {
                    objBE.BillMonthId = Convert.ToInt32(hfOpenStatusId.Value);
                    objBE.OpenStatusId = Constants.Opened;
                    objBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objBE = _objiDsignBAL.DeserializeFromXml<BillingMonthOpenBE>(objBillingMonthOpenBAL.InsertBillingMonth(objBE, Statement.Update));

                    if (Convert.ToBoolean(objBE.IsSuccess))
                    {
                        BindGridBillMonthOpenList();
                        BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        Message(UMS_Resource.BILL_MONTH_UNLOCKED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    }
                    else
                    {
                        Message(UMS_Resource.BILL_MONTH_UNLOCKED_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        public void Insert_ClosedMonthData(int year, int month)
        {
            try
            {
                BillingMonthOpenBE objBE = new BillingMonthOpenBE();
                objBE.Year = year;
                objBE.Month = month;
                objBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objBillingMonthOpenBAL.InsertBillingMonth(objBE, Statement.Modify);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvBillingMonthList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblOpenStatusId = (Label)e.Row.FindControl("lblOpenStatusId");
                    Label lblName = (Label)e.Row.FindControl("lblName");
                    Label lblStatusType = (Label)e.Row.FindControl("lblStatusType");
                    Label lblDetails = (Label)e.Row.FindControl("lblDetails");
                    Label lblHyphen = (Label)e.Row.FindControl("lblHyphen");

                    if (Convert.ToInt32(lblOpenStatusId.Text) == Constants.Opened)
                    {
                        e.Row.FindControl("lkbtnStatusLock").Visible = true;
                        e.Row.FindControl("lkbtnUnLock").Visible = lblHyphen.Visible = false;
                        lblName.ControlStyle.Font.Bold = lblStatusType.ControlStyle.Font.Bold = lblDetails.ControlStyle.Font.Bold = true;
                    }
                    else if (Convert.ToInt32(lblOpenStatusId.Text) == Constants.Closed)
                    {
                        e.Row.FindControl("lkbtnStatusLock").Visible = false;
                        e.Row.FindControl("lkbtnUnLock").Visible = false;
                        lblHyphen.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                BillingMonthOpenBE objBE = new BillingMonthOpenBE();
                objBE = IsPreviousMonthOpen();
                if (objBE.IsExists)
                {
                    lblAlertMessage.Text = Resource.PREVIOUS_MONTH_CLOSE_MESSAGE;
                    MpAlert.Show();
                }
                else if (objBE.IsBillingMonthExists)
                {
                    lblAlertMessage.Text = Resource.MONTHS_CLOSE_BEFORE_OPENING;
                    MpAlert.Show();
                }
                else
                {
                    objBE.Year = Year;
                    objBE.Month = Month;
                    objBE.Details = Details;
                    objBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objBE = _objiDsignBAL.DeserializeFromXml<BillingMonthOpenBE>(objBillingMonthOpenBAL.InsertBillingMonth(objBE, Statement.Insert));
                    if (objBE.IsSuccess)
                    {
                        BindGridBillMonthOpenList();
                        BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        Message(UMS_Resource.BILL_MONTH_OPENED_INSERT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        ddlMonth.SelectedIndex = ddlYear.SelectedIndex = 0;
                        txtDetails.Text = string.Empty;
                    }
                    else if (objBE.IsBillingMonthExists)
                    {
                        Message(UMS_Resource.BILL_MONTH_OPENED_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else if (objBE.IsLesserMonthYear)
                    {
                        Message("You can not open previous months.Future months already existed.", UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else
                    {
                        Message(UMS_Resource.BILL_MONTH_OPENED_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private BillingMonthOpenBE IsPreviousMonthOpen()
        {
            BillingMonthOpenBE _objBillingMonthOpenBE = new BillingMonthOpenBE();
            _objBillingMonthOpenBE.Year = Year;
            _objBillingMonthOpenBE.Month = Month;
            _objBillingMonthOpenBE = _objiDsignBAL.DeserializeFromXml<BillingMonthOpenBE>(objBillingMonthOpenBAL.GetBillingMonth(_objBillingMonthOpenBE, ReturnType.Check));

            return _objBillingMonthOpenBE;
        }

        private bool IsPreviousMonthOpeninGrid(int year1, int month1)
        {
            BillingMonthOpenBE _objBillingMonthOpenBE = new BillingMonthOpenBE();
            _objBillingMonthOpenBE.Year = year1;
            _objBillingMonthOpenBE.Month = month1;
            _objBillingMonthOpenBE = _objiDsignBAL.DeserializeFromXml<BillingMonthOpenBE>(objBillingMonthOpenBAL.GetBillingMonth(_objBillingMonthOpenBE, ReturnType.Check));

            return _objBillingMonthOpenBE.IsExists;
        }
        private bool IsGreaterMonthOpeninGrid(int year1, int month1)
        {
            BillingMonthOpenBE _objBillingMonthOpenBE = new BillingMonthOpenBE();
            _objBillingMonthOpenBE.Year = year1;
            _objBillingMonthOpenBE.Month = month1;
            _objBillingMonthOpenBE = _objiDsignBAL.DeserializeFromXml<BillingMonthOpenBE>(objBillingMonthOpenBAL.GetBillingMonth(_objBillingMonthOpenBE, ReturnType.Double));

            return _objBillingMonthOpenBE.IsExists;
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridBillMonthOpenList();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridBillMonthOpenList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridBillMonthOpenList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridBillMonthOpenList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                BindGridBillMonthOpenList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                _objiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                _objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods

        private void BindGridBillMonthOpenList()
        {
            try
            {
                BillingMonthOpenBE objBE = new BillingMonthOpenBE();
                BillingMonthOpenListBE objListBE = new BillingMonthOpenListBE();

                objBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                objBE.PageSize = PageSize;
                XmlDocument xml = objBillingMonthOpenBAL.GetBillingMonth(objBE, ReturnType.Get);
                objListBE = _objiDsignBAL.DeserializeFromXml<BillingMonthOpenListBE>(xml);
                DataTable dt = new DataTable();
                dt = _objiDsignBAL.ConvertListToDataSet<BillingMonthOpenBE>(objListBE.Items).Tables[0];

                if (objListBE.Items.Count > 0)
                {
                    divgrid.Visible = divpaging.Visible = true;
                    hfTotalRecords.Value = objListBE.Items[0].TotalRecords.ToString();
                }
                else
                {
                    hfTotalRecords.Value = objListBE.Items.Count.ToString();
                    divgrid.Visible = divpaging.Visible = false;
                }
                gvBillingMonthList.DataSource = dt;
                gvBillingMonthList.DataBind();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindYears()
        {
            try
            {
                int CurrentYear = DateTime.Now.Year;
                int year = DateTime.Now.Year - Constants.BILLING_YEAR_LENGTH;
                for (int i = CurrentYear; i >= year; i--)
                {
                    ListItem item = new ListItem();
                    item.Text = item.Value = i.ToString();
                    ddlYear.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindMonths()
        {
            try
            {
                DataSet dsMonths = new DataSet();
                dsMonths.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["Months"]));
                _objiDsignBAL.FillDropDownList(dsMonths.Tables[0], ddlMonth, "name", "value", UMS_Resource.DROPDOWN_SELECT, false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}