﻿<%@ Page Title=":: Consumption Delivered ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="ConsumptionDelivered.aspx.cs"
    Inherits="UMS_Nigeria.Billing.ConsumptionDelivered" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="contentBody" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server"> 
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upAgencies" runat="server">
            <ContentTemplate>
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litConsumptionDelivered" runat="server" Text="Consumption Delivered"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner" style="width: 430px;">
                                <label for="name">
                                    <asp:Literal ID="Literal3" runat="server" Text="Open Month Details :: "></asp:Literal></label>
                                <asp:Label ID="lblMonthDetails" runat="server" Text="---"></asp:Label>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="divHide" runat="server" visible="false">
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal><span
                                            class="span_star">*</span></label><br />
                                    <asp:DropDownList ID="ddlBusinessUnitName" AutoPostBack="true" runat="server" CssClass="text-box select_box"
                                        onchange="DropDownlistOnChangelbl(this,'spanddlBusinessUnitName',' Business Unit')"
                                        TabIndex="1" OnSelectedIndexChanged="ddlBusinessUnitName_SelectedIndexChanged">
                                        <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                                    </asp:DropDownList>
                                    <div class="space">
                                    </div>
                                    <span id="spanddlBusinessUnitName" class="span_color"></span>
                                </div>
                            </div>
                            <%--
                        <div class="clr">
                        </div>--%>
                            <%----==================================================================================================================
                        ----------------------------For Now disablig Feeder. It has to be implemented in this page------------------------------
                        ========================================================================================================================--%>
                            <%--<div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litFeeder" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal></label><br />
                                <asp:DropDownList ID="ddlFeeder" AutoPostBack="true" runat="server" CssClass="text-box select_box"
                                    OnSelectedIndexChanged="ddlFeeder_SelectedIndexChanged">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="clr">
                        </div>--%>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litServiceUnit" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal><span
                                            class="span_star">*</span></label><br />
                                    <asp:DropDownList ID="ddlServiceUnitName" runat="server" CssClass="text-box select_box"
                                        onchange="DropDownlistOnChangelbl(this,'spanddlServiceUnitName',' Service Unit')"
                                        TabIndex="2">
                                        <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                    <div class="space">
                                    </div>
                                    <span id="spanddlServiceUnitName" class="span_color"></span>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litCapacity" runat="server" Text="Capacity"></asp:Literal><span
                                            class="span_star">*</span>
                                    </label>
                                    <br />
                                    <asp:TextBox CssClass="text-box" ID="txtCapacity" TabIndex="3" runat="server" onblur="return TextBoxBlurValidationlblNonZero(this,'spanCapacity','Capacity')"
                                        onkeypress="return isNumberKey(event,this)" placeholder="Enter Capacity" MaxLength="18"></asp:TextBox>
                                    <asp:Label Text="KVA" runat="server" />
                                    <span id="spanCapacity" class="span_color"></span>
                                </div>
                            </div>
                            <%--
                        <div class="clr">
                        </div>--%>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litSUPPMConsumption" runat="server" Text="Service Unit PPM Consumption"></asp:Literal><span
                                            class="span_star">*</span>
                                    </label>
                                    <br />
                                    <asp:TextBox CssClass="text-box" ID="txtSUPPMConsumption" TabIndex="4" runat="server"  MaxLength="18"
                                        onblur="return TextBoxBlurValidationlblNonZero(this,'spanSUPPMConsumption','Service Unit PPM Consumption')"
                                        onkeypress="return isNumberKey(event,this)" placeholder="Enter Service Unit PPM Consumption"></asp:TextBox>
                                    <span id="spanSUPPMConsumption" class="span_color"></span>
                                </div>
                            </div>
                            <%--<div class="clr">
                        </div>--%>
                            <div class="inner-box3">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litSUCreditConsumption" runat="server" Text="Service Unit Credit Consumption"></asp:Literal><span
                                            class="span_star">*</span>
                                    </label>
                                    <br />
                                    <asp:TextBox CssClass="text-box" ID="txtSUCreditConsumption" TabIndex="5" runat="server"  MaxLength="18"
                                        onblur="return TextBoxBlurValidationlblNonZero(this,'spanSUCreditConsumption','Service Unit Credit Consumption')"
                                        onkeypress="return isNumberKey(event,this)" placeholder="Enter Service Unit Credit Consumption"></asp:TextBox>
                                    <span id="spanSUCreditConsumption" class="span_color"></span>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litTotalConsumption" runat="server" Text="Total Consumption"></asp:Literal>
                                    </label>
                                    <br />
                                    <asp:Label ID="lblTotalConsumption" Text="--" runat="server" />
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <asp:Button ID="btnSave" TabIndex="6" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                        CssClass="box_s" runat="server" OnClick="btnSave_Click" />
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div id="DivGridHide" runat="server" visible="false">
                        <div class="grid_boxes">
                            <div class="grid_pagingTwo_top">
                                <div class="paging_top_title">
                                    <asp:Literal ID="litConsumptionDeliveredList" runat="server" Text="Consumption Delivered List"></asp:Literal>
                                </div>
                                <div class="paging_top_rightTwo_content" id="divpaging" runat="server" visible="false">
                                    <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="grid_tb" id="divgrid" runat="server">
                            <asp:GridView ID="gvConsumptionDelivered" runat="server" AutoGenerateColumns="false"
                                OnRowCommand="gvConsumptionDelivered_RowCommand" HeaderStyle-CssClass="grid_tb_hd"
                                OnRowDataBound="gvConsumptionDelivered_RowDataBound">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    There is no data.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, GRID_SNO%>" DataField="RowNumber" />
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, BU%>" DataField="BusinessUnitName" />
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, SU%>" DataField="ServiceUnitName" />
                                    <%--<asp:BoundField HeaderText="Capacity" DataField="Capacity" />
                                    <asp:BoundField HeaderText="SU PPM Consumption" DataField="SUPPMConsumption" />
                                    <asp:BoundField HeaderText="SU Credit Consumption" DataField="SUCreditConsumption" />--%>
                                    <asp:TemplateField HeaderText="Capacity" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridCapacity" runat="server" Text='<%#Eval("Capacity") %>'></asp:Label>
                                            <asp:TextBox CssClass="text-box" ID="txtGridCapacity" placeholder="Enter Capacity"
                                                Visible="false" runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SU PPM Consumption" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-CssClass="grid_tb_td">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridSUPPMConsumption" runat="server" Text='<%#Eval("SUPPMConsumption") %>'></asp:Label>
                                            <asp:TextBox CssClass="text-box" ID="txtGridSUPPMConsumption" placeholder="Enter Service Unit PPM Consumption"
                                                Visible="false" runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SU Credit Consumption" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-CssClass="grid_tb_td">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSUGridCreditConsumption" runat="server" Text='<%#Eval("SUCreditConsumption") %>'></asp:Label>
                                            <asp:TextBox CssClass="text-box" ID="txtGridSUCreditConsumption" placeholder="Enter Service Unit Credit Consumption"
                                                Visible="false" runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total Consumption" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-CssClass="grid_tb_td">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridTotalConsumption" runat="server" Text='<%#Eval("TotalConsumption") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Consumption Month and Year" DataField="ConsumptionMontYear" />
                                    <%--<asp:TemplateField HeaderText="Month/Year" ItemStyle-Width="10%"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridMonth" Text='<%# Eval("ConsumptionMonth") %>' Visible="false" runat="server" />
                                            <asp:Label ID="lblGridYear" Text='<%# Eval("ConsumptionYear") %>' Visible="false" runat="server" />
                                            <asp:Label ID="lblGridCosumptionMonthYear" Text="--" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="10%"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridConsumptionDeliveredId" Text='<%#Eval("ConsumptionDeliveredId")%>'
                                                runat="server" Visible="false" />
                                            <asp:LinkButton ID="lkbtnDelete" runat="server" ToolTip="Delete" Text="Delete" CommandName="DELETECONSUMPTION">
                                        <img src="../images/delete.png" alt="Delete"/></asp:LinkButton>
                                            <asp:LinkButton ID="lkbtnEdit" runat="server" ToolTip="Edit" Visible="false" Text="Edit"
                                                CommandName="EDITCONSUMPTION">
                                        <img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                                            <asp:LinkButton ID="lkbtnUpdate" Visible="false" runat="server" ToolTip="Update"
                                                CommandName="UPDATECONSUMPTION"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                            <asp:LinkButton ID="lkbtnCancel" Visible="false" runat="server" ToolTip="Cancel"
                                                CommandName="CANCELCONSUMPTION"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:HiddenField ID="hfCountryname" runat="server" />
                            <asp:HiddenField ID="hfPageNo" runat="server" />
                            <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                            <asp:HiddenField ID="hfLastPage" runat="server" />
                            <asp:HiddenField ID="hfPageSize" runat="server" />
                            <asp:HiddenField ID="hfMonthDetails" runat="server" />
                            <asp:HiddenField ID="hfMonth" runat="server" />
                            <asp:HiddenField ID="hfYear" runat="server" />
                            <asp:HiddenField ID="hfConsumptionDeliveredId" runat="server" />
                        </div>
                        <div class="grid_boxes">
                            <div id="divdownpaging" runat="server" visible="false">
                                <div class="grid_paging_bottom">
                                    <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <%-- Grid Alert popup Start--%>
                    <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                        TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                        <div class="popheader popheaderlblred">
                            <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Grid Alert popup Closed--%>
                    <%-- Delete  Btn popup Start--%>
                    <asp:ModalPopupExtender ID="mpeDelete" runat="server" PopupControlID="PanelDelete"
                        TargetControlID="btnDelete" BackgroundCssClass="modalBackground" CancelControlID="btnDeleteCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnDelete" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelDelete" runat="server" DefaultButton="btnDeleteOk" CssClass="modalPopup"
                        Style="display: none; border-color: #639B00;">
                        <div class="popheader" style="background-color: red;">
                            <asp:Label ID="lblDeleteMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnDeleteOk" runat="server" OnClick="btnDeleteOk_Click" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnDeleteCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Delete  popup Closed--%>
                    <%-- Force Insert Btn popup Start--%>
                    <asp:ModalPopupExtender ID="mpeForceInsert" runat="server" PopupControlID="PanelForceInsert"
                        TargetControlID="btnForceInsert" BackgroundCssClass="modalBackground" CancelControlID="btnDeleteCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnForceInsert" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelForceInsert" runat="server" DefaultButton="btnForceInsertOk" CssClass="modalPopup"
                        Style="display: none; border-color: #639B00;">
                        <div class="popheader" style="background-color: red;">
                            <asp:Label ID="lblForceInsertMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnForceInsertOk" runat="server" OnClick="btnForceInsertOk_Click" Text="<%$ Resources:Resource, CONTINUE%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="Button3" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Delete  popup Closed--%>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/ConsumptionDelivered.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }  
    </script>
    <%--<script language="javascript" type="text/javascript">
        function ShowProgress() {
            debugger;
            document.getElementById('UMSNigeriaBody_UpdateProgress').style = "position: fixed; z-index: 100001; left: 554.5px; top: 107.5px; display: block;";
            document.getElementById('UMSNigeriaBody_modalPopupLoading_backgroundElement').style = "position: fixed; left: 0px; top: 0px; z-index: 10000; width: 1343px; height: 699px;";
            setTimeout("HideProgress()", 1000);
        }
        function HideProgress() {
            document.getElementById('UMSNigeriaBody_UpdateProgress').style.display = "none";
            document.getElementById('UMSNigeriaBody_modalPopupLoading_backgroundElement').style.display = "none";
        }        
    </script>--%>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
