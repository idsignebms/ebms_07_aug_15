﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using UMS_NigeriaBE;
using System.Xml;
using UMS_NigeriaBAL;
using iDSignHelper;
using UMS_NigriaDAL;
using System.Data;

namespace UMS_Nigeria.Billing
{
    public partial class ConsumptionDelivered : System.Web.UI.Page
    {

        #region Members
        ReportsBal _objReportsBal = new ReportsBal();
        iDsignBAL _objIdsignBal = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        string Key = "ConsumptionDelivered";
        ConsumptionDeliveredBAL _objConsumptionDeliveredBAL = new ConsumptionDeliveredBAL();
        int ConsumptionMonth = 0;
        int ConsumptionYear = 0;
        #endregion

        #region Properties
        public string BusinessUnitName
        {
            get { return string.IsNullOrEmpty(ddlBusinessUnitName.SelectedValue) ? objCommonMethods.CollectAllValues(ddlBusinessUnitName, ",") : ddlBusinessUnitName.SelectedValue; }
            set { ddlBusinessUnitName.SelectedValue = value.ToString(); }
        }
        public string ServiceUnitName
        {
            get { return string.IsNullOrEmpty(ddlServiceUnitName.SelectedValue) ? objCommonMethods.CollectAllValues(ddlServiceUnitName, ",") : ddlServiceUnitName.SelectedValue; }
            set { ddlServiceUnitName.SelectedValue = value.ToString(); }
        }
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;

            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                if (!IsPostBack)
                {
                    GetOpenMonthDetails();

                    if (lblMonthDetails.Text != Resource.OPEN_MNTHS_NOT_AVAIL)
                    {
                        BindBusinessUnits();
                        //string path = string.Empty;
                        //path = objCommonMethods.GetPagePath(this.Request.Url);
                        //if (objCommonMethods.IsAccess(path))
                        //{
                        hfPageNo.Value = Constants.pageNoValue.ToString();
                        hfTotalRecords.Value = Constants.Zero.ToString();
                        BindGrid();

                        UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //}
                        //else
                        //{
                        //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                        //}
                    }
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }

        protected void ddlBusinessUnitName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objCommonMethods.BindServiceUnits(ddlServiceUnitName, BusinessUnitName, true);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            try
            {
                ConsumptionDeliveredBE _objConsumptionDeliveredBE = new ConsumptionDeliveredBE();
                _objConsumptionDeliveredBE.BU_ID = BusinessUnitName;
                _objConsumptionDeliveredBE.SU_ID = ServiceUnitName;
                _objConsumptionDeliveredBE.Capacity = Convert.ToDecimal(txtCapacity.Text.Trim());
                _objConsumptionDeliveredBE.SUPPMConsumption = Convert.ToDecimal(txtSUPPMConsumption.Text.Trim());
                _objConsumptionDeliveredBE.SUCreditConsumption = Convert.ToDecimal(txtSUCreditConsumption.Text.Trim());
                _objConsumptionDeliveredBE.ConsumptionMonth = Convert.ToInt32(hfMonth.Value);
                _objConsumptionDeliveredBE.ConsumptionYear = Convert.ToInt32(hfYear.Value);
                _objConsumptionDeliveredBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                XmlDocument Xml = null;
                Xml = _objConsumptionDeliveredBAL.ConsumptionDelivered(_objConsumptionDeliveredBE, Statement.Insert);
                _objConsumptionDeliveredBE = _objIdsignBal.DeserializeFromXml<ConsumptionDeliveredBE>(Xml);

                if (_objConsumptionDeliveredBE.RowsEffected > 0)
                {
                    BindGrid();
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    txtCapacity.Text = txtSUCreditConsumption.Text = txtSUPPMConsumption.Text = lblTotalConsumption.Text = string.Empty;
                    ddlServiceUnitName.SelectedIndex = 0;
                    Message("Consumption Delivered saved successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else if (_objConsumptionDeliveredBE.ConsumptionRecordExists)
                {
                    mpeForceInsert.Show();
                    hfConsumptionDeliveredId.Value = _objConsumptionDeliveredBE.ConsumptionDeliveredId.ToString();
                    lblForceInsertMsg.Text = "Record already exist. Chnages will be overwirte. Do you want to continue ?";
                    btnForceInsertOk.Focus();
                }
                else
                {
                    Message("Consumption Delivered insertion failed.", UMS_Resource.MESSAGETYPE_ERROR);
                }

            }
            catch (Exception ex)
            {
                Message("Exception in Consumption Delivered Insertion.", UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvConsumptionDelivered_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    TextBox txtGridCapacity = (TextBox)e.Row.FindControl("txtGridCapacity");
                    TextBox txtGridSUPPMConsumption = (TextBox)e.Row.FindControl("txtGridSUPPMConsumption");
                    TextBox txtGridSUCreditConsumption = (TextBox)e.Row.FindControl("txtGridSUCreditConsumption");

                    Label lblGridTotalConsumption = (Label)e.Row.FindControl("lblGridTotalConsumption");

                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridCapacity.ClientID +
                                                    "','" + txtGridSUCreditConsumption.ClientID + "','"
                                                    + txtGridSUPPMConsumption.ClientID + "','"
                                                    + lblGridTotalConsumption.ClientID + "')");
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvConsumptionDelivered_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ConsumptionDeliveredListBE objConsumptionDeliveredListBE = new ConsumptionDeliveredListBE();
                ConsumptionDeliveredBE objConsumptionDeliveredBE = new ConsumptionDeliveredBE();

                XmlDocument xml = null;

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                Label lblGridCapacity = (Label)row.FindControl("lblGridCapacity");
                Label lblGridSUPPMConsumption = (Label)row.FindControl("lblGridSUPPMConsumption");
                Label lblSUGridCreditConsumption = (Label)row.FindControl("lblSUGridCreditConsumption");

                TextBox txtGridCapacity = (TextBox)row.FindControl("txtGridCapacity");
                TextBox txtGridSUPPMConsumption = (TextBox)row.FindControl("txtGridSUPPMConsumption");
                TextBox txtGridSUCreditConsumption = (TextBox)row.FindControl("txtGridSUCreditConsumption");

                Label lblGridConsumptionDeliveredId = (Label)row.FindControl("lblGridConsumptionDeliveredId");

                switch (e.CommandName.ToUpper())
                {
                    case "EDITCONSUMPTION":
                        lblGridCapacity.Visible = lblGridSUPPMConsumption.Visible = lblSUGridCreditConsumption.Visible = false;
                        txtGridCapacity.Visible = txtGridSUPPMConsumption.Visible = txtGridSUCreditConsumption.Visible = true;
                        txtGridCapacity.Text = lblGridCapacity.Text;
                        txtGridSUPPMConsumption.Text = lblGridSUPPMConsumption.Text;
                        txtGridSUCreditConsumption.Text = lblSUGridCreditConsumption.Text;

                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnEdit").Visible = false;
                        break;
                    case "CANCELCONSUMPTION":
                        row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;

                        txtGridCapacity.Visible = txtGridSUPPMConsumption.Visible = txtGridSUCreditConsumption.Visible = false;
                        lblGridCapacity.Visible = lblGridSUPPMConsumption.Visible = lblSUGridCreditConsumption.Visible = true;
                        break;
                    case "UPDATECONSUMPTION":
                        objConsumptionDeliveredBE.ConsumptionDeliveredId = Convert.ToInt32(lblGridConsumptionDeliveredId.Text);
                        objConsumptionDeliveredBE.Capacity = Convert.ToDecimal(txtGridCapacity.Text);
                        objConsumptionDeliveredBE.SUPPMConsumption = Convert.ToDecimal(txtGridSUPPMConsumption.Text);
                        objConsumptionDeliveredBE.SUCreditConsumption = Convert.ToDecimal(txtGridSUCreditConsumption.Text);
                        objConsumptionDeliveredBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        xml = _objConsumptionDeliveredBAL.ConsumptionDelivered(objConsumptionDeliveredBE, Statement.Update);
                        objConsumptionDeliveredBE = _objIdsignBal.DeserializeFromXml<ConsumptionDeliveredBE>(xml);

                        if (objConsumptionDeliveredBE.RowsEffected > 0)
                        {
                            BindGrid();
                            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            Message("Consumption updated successfully", UMS_Resource.MESSAGETYPE_SUCCESS);

                        }
                        else
                            Message("Consumption Updation Failed.", UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "DELETECONSUMPTION":
                        mpeDelete.Show();
                        hfConsumptionDeliveredId.Value = lblGridConsumptionDeliveredId.Text;
                        lblDeleteMsg.Text = "Are you sure to delete Consumption delivered ?";
                        btnDelete.Focus();


                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnDeleteOk_Click(object sender, EventArgs e)
        {
            ConsumptionDeliveredBE objConsumptionDeliveredBE = new ConsumptionDeliveredBE();
            objConsumptionDeliveredBE.ConsumptionDeliveredId = Convert.ToInt32(hfConsumptionDeliveredId.Value);
            objConsumptionDeliveredBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            XmlDocument xml = null;
            xml = _objConsumptionDeliveredBAL.ConsumptionDelivered(objConsumptionDeliveredBE, Statement.Delete);
            objConsumptionDeliveredBE = _objIdsignBal.DeserializeFromXml<ConsumptionDeliveredBE>(xml);

            if (objConsumptionDeliveredBE.RowsEffected > 0)
            {
                BindGrid();
                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                Message("Consumption deleted successfully", UMS_Resource.MESSAGETYPE_SUCCESS);

            }
            else
                Message("Consumption deletion failed.", UMS_Resource.MESSAGETYPE_ERROR);
        }

        protected void btnForceInsertOk_Click(object sender, EventArgs e)
        {
            ConsumptionDeliveredBE objConsumptionDeliveredBE = new ConsumptionDeliveredBE();
            objConsumptionDeliveredBE.ConsumptionDeliveredId = Convert.ToInt32(hfConsumptionDeliveredId.Value);
            objConsumptionDeliveredBE.Capacity = Convert.ToDecimal(txtCapacity.Text);
            objConsumptionDeliveredBE.SUPPMConsumption = Convert.ToDecimal(txtSUPPMConsumption.Text);
            objConsumptionDeliveredBE.SUCreditConsumption = Convert.ToDecimal(txtSUCreditConsumption.Text);
            objConsumptionDeliveredBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            XmlDocument xml = new XmlDocument();
            xml = _objConsumptionDeliveredBAL.ConsumptionDelivered(objConsumptionDeliveredBE, Statement.Update);
            objConsumptionDeliveredBE = _objIdsignBal.DeserializeFromXml<ConsumptionDeliveredBE>(xml);

            if (objConsumptionDeliveredBE.RowsEffected > 0)
            {
                BindGrid();
                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                Message("Consumption updated successfully", UMS_Resource.MESSAGETYPE_SUCCESS);

            }
            else
                Message("Consumption Updation Failed.", UMS_Resource.MESSAGETYPE_ERROR);
        }

        #endregion

        #region Methods
        private void GetOpenMonthDetails()
        {
            RptPreBillingBe _objPreBillingBe = new RptPreBillingBe();
            XmlDocument xmlResult = _objReportsBal.GetPreBillingDetails(_objPreBillingBe, ReturnType.Fetch);
            _objPreBillingBe = _objIdsignBal.DeserializeFromXml<RptPreBillingBe>(xmlResult);
            if (_objPreBillingBe != null)
            {
                divHide.Visible = DivGridHide.Visible = true;
                lblMonthDetails.Text = _objPreBillingBe.NameOfMonth + " - " + _objPreBillingBe.Year;
                hfMonthDetails.Value = _objPreBillingBe.NameOfMonth + "_" + _objPreBillingBe.Year;
                hfMonth.Value = _objPreBillingBe.Month.ToString();
                hfYear.Value = _objPreBillingBe.Year.ToString();
            }
            else
            {
                lblMonthDetails.Text = Resource.OPEN_MNTHS_NOT_AVAIL;
                divHide.Visible = DivGridHide.Visible = false;
            }
        }

        private void BindBusinessUnits()
        {
            objCommonMethods.BindUserBusinessUnits(ddlBusinessUnitName, string.Empty, true, Resource.DDL_ALL);
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
            {
                string BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                ddlBusinessUnitName.SelectedIndex = ddlBusinessUnitName.Items.IndexOf(ddlBusinessUnitName.Items.FindByValue(BUID));
                ddlBusinessUnitName.Enabled = false;
                ddlBusinessUnitName_SelectedIndexChanged(ddlBusinessUnitName, new EventArgs());
            }
        }

        public void BindGrid()
        {
            try
            {
                ConsumptionDeliveredListBE objConsumptionDeliveredListBE = new ConsumptionDeliveredListBE();
                ConsumptionDeliveredBE objConsumptionDeliveredBE = new ConsumptionDeliveredBE();
                XmlDocument resultedXml = new XmlDocument();

                objConsumptionDeliveredBE.ConsumptionMonth = Convert.ToInt32(hfMonth.Value);
                objConsumptionDeliveredBE.ConsumptionYear = Convert.ToInt32(hfYear.Value);
                objConsumptionDeliveredBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                objConsumptionDeliveredBE.PageSize = PageSize;
                objConsumptionDeliveredBE.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID]==null) ? string.Empty : ddlBusinessUnitName.SelectedValue;
                resultedXml = _objConsumptionDeliveredBAL.ConsumptionDelivered(objConsumptionDeliveredBE, ReturnType.Get);

                objConsumptionDeliveredListBE = _objIdsignBal.DeserializeFromXml<ConsumptionDeliveredListBE>(resultedXml);
                if (objConsumptionDeliveredListBE.Items.Count > 0)
                {
                    divdownpaging.Visible = divpaging.Visible = true;
                    hfTotalRecords.Value = objConsumptionDeliveredListBE.Items[0].TotalRecords.ToString();
                    gvConsumptionDelivered.DataSource = objConsumptionDeliveredListBE.Items;
                    gvConsumptionDelivered.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = objConsumptionDeliveredListBE.Items.Count.ToString();
                    divdownpaging.Visible = divpaging.Visible = false;
                    gvConsumptionDelivered.DataSource = new DataTable(); ;
                    gvConsumptionDelivered.DataBind();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion
    }
}