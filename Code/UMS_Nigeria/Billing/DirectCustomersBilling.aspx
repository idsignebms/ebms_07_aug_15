﻿<%@ Page Title="::Direct Customers Billing::" Language="C#" MasterPageFile="~/MasterPages/EDMUMS.Master" AutoEventWireup="true"
    CodeBehind="DirectCustomersBilling.aspx.cs" Inherits="UMS_Nigeria.Billing.DirectCustomersBilling" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function Validate() {
          
        }
        <script type="text/javascript">
        function pageLoad() {
           
            DisplayMessage('<%= pnlMessage.ClientID %>');
        };
    </script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <div class="edmcontainer">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <%-- <asp:UpdatePanel ID="upSearchCustomer" runat="server">
            <ContentTemplate>--%>
        <asp:Panel ID="pnlMessage" runat="server">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <h3>
            <asp:Literal ID="litSearchCustomer" runat="server" Text="<%$ Resources:Resource, CUSTOMER_DETAILS%>"></asp:Literal>
        </h3>
        <div class="clear">
            &nbsp;</div>
        <div class="consumer_feild">
           <div class="consume_name" style="width:118px;">
                <asp:Literal ID="litTariff" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                <span>*</span></div>
            <div class="consume_input">
                <asp:DropDownList ID="ddlTariff" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlTariff_SelectedIndexChanged">
                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                </asp:DropDownList>
                <div class="error">
                    <span id="spanTariff" class="spancolor"></span>
                </div>
            </div>
            <div class="clear pad_10">
            </div>
            <div class="consume_name" style="width:118px;">
                <asp:Literal ID="litDocumentNo" runat="server" Text="<%$ Resources:Resource, EST_ENERGYCHARGES%>"></asp:Literal>
            </div>
            <div class="consume_input">
                <asp:TextBox ID="txtEstEnergyCharges" runat="server" MaxLength="100" placeholder="Document No"></asp:TextBox>
            </div>
            <div class="clear pad_10">
                <div style="margin-left:204px;">
                    <asp:Button ID="btnSave" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                        CssClass="calculate_but" runat="server" OnClick="btnSave_Click" />
                </div>
            </div>
        </div>
          <div class="clear pad_10"></div>
        
      
        <div class="grid fltl" style="width: 40%; margin-left:135px;">
            <asp:GridView ID="GvTariffDetails" runat="server" AutoGenerateColumns="false" OnRowCommand="gvTariffFixedChargesList_RowCommand">
                <EmptyDataTemplate>
                    There is no data
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="<%$ Resources:Resource, TARIFF%>" ItemStyle-HorizontalAlign="Center"
                        ItemStyle-VerticalAlign="Middle">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="lblClassName" Text='<%#Eval("ClassName") %>' CommandName='<%#Eval("ClassId") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="<%$ Resources:Resource, COUNT%>" ItemStyle-HorizontalAlign="Center"
                        ItemStyle-VerticalAlign="Middle">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblCount" Text='<%#Eval("Count") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="<%$ Resources:Resource, EST_ENERGYCHARGES%>" ItemStyle-HorizontalAlign="Center"
                        ItemStyle-VerticalAlign="Middle">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="lblEnergyCharges" Text='<%#Eval("EnergyCharges") %>' />
                            <asp:TextBox ID="txtGridAmount" MaxLength="20" placeholder="Enter Amount" runat="server"
                                Visible="false"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbTxtGridContactNo2" runat="server" TargetControlID="txtGridAmount"
                                FilterType="Numbers,Custom" ValidChars="." FilterMode="ValidChars">
                            </asp:FilteredTextBoxExtender>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkbtnEdit" runat="server" CommandName="EditCharge" OnClientClick="return confirm('Are you sure to Edit this Charges?')">
                                                 <img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                            <asp:LinkButton ID="lkbtnUpdate" Visible="false" runat="server" ToolTip="Update"
                                CommandName="UpdateCharge"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                            <asp:LinkButton ID="lkbtnCancel" Visible="false" runat="server" ToolTip="Cancel"
                                CommandName="CancelCharge"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>

        <div class="grid fltl" style="margin-left:30px; width: 25%;">
            <asp:GridView ID="GvBusinessUnits" runat="server" AutoGenerateColumns="false">
            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                        <EmptyDataTemplate>
                                            There is no Data.
                                        </EmptyDataTemplate>
                <Columns>
                    <asp:BoundField DataField="BU_ID" HeaderText="<%$ Resources:Resource, BU_ID%>" />
                    <asp:TemplateField HeaderText="<%$ Resources:Resource, BU_ID%>" ItemStyle-HorizontalAlign="Center"
                        ItemStyle-VerticalAlign="Middle">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblCount" Text='<%#Eval("Count") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <%--  
            </ContentTemplate>
        </asp:UpdatePanel>--%>
    </div>
</asp:Content>
