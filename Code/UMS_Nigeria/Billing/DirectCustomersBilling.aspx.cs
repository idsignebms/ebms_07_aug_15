﻿
#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for Search consumer
                     
 Developer        : id067-naresh
 Creation Date    : 12-04-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using Resources;
using System.Xml;


namespace UMS_Nigeria.Billing
{
    public partial class DirectCustomersBilling : System.Web.UI.Page
    {

        #region Members

        iDsignBAL objIdsignBal = new iDsignBAL();
        ConsumerBal objConsumerBal = new ConsumerBal();
        CommonMethods objCommonMethods = new CommonMethods();
        public string Key = "DirectCustomersBilling";
        string AccountNo;

        #endregion


        #region Properties

        private string TariffId
        {
            get { return string.IsNullOrEmpty(ddlTariff.SelectedValue) ? string.Empty : ddlTariff.SelectedValue; }
        }
        private string TariffName
        {
            get { return string.IsNullOrEmpty(ddlTariff.SelectedValue) ? string.Empty : ddlTariff.SelectedItem.Text; }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.CssClass = lblMessage.Text = string.Empty;
            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = objCommonMethods.GetPagePath(this.Request.Url);
                //if (objCommonMethods.IsAccess(path))
                //{

                    objCommonMethods.BindTariffSubTypes(ddlTariff, 0, true);
                    BindtariffDetails();
                //}
                //else
                //{
                //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                //}
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            objConsumerBe.TariffId = Convert.ToInt32(this.TariffId);
            objConsumerBe.EnergyCharges = Convert.ToInt32(txtEstEnergyCharges.Text);
            objConsumerBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Insert(objConsumerBe, Statement.Edit));
            if (objConsumerBe.IsSuccess)
            {

            }
            if (objConsumerBe.IsUpdated)
            {

            }

            BindbusinessUnits();
            BindtariffDetails();

        }

        protected void gvTariffFixedChargesList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ConsumerListBe objConsumerListBe = new ConsumerListBe();
                ConsumerBe objConsumerBe = new ConsumerBe();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridAmount = (TextBox)row.FindControl("txtGridAmount");
                LinkButton lblClassName = (LinkButton)row.FindControl("lblClassName");
                LinkButton lblEnergyCharges = (LinkButton)row.FindControl("lblEnergyCharges");

                switch (e.CommandName.ToUpper())
                {
                    case "EDITCHARGE":
                        lblEnergyCharges.Visible = false;
                        txtGridAmount.Visible = true;
                        txtGridAmount.Text = lblEnergyCharges.Text;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnEdit").Visible = false;
                        break;
                    case "CANCELCHARGE":
                        row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                        lblEnergyCharges.Visible = true;
                        txtGridAmount.Visible = false;
                        break;
                    case "UPDATECHARGE":
                        objConsumerBe.TariffId = Convert.ToInt32(lblClassName.CommandName);
                        objConsumerBe.EnergyCharges = Convert.ToInt32(txtGridAmount.Text);
                        objConsumerBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Insert(objConsumerBe, Statement.Edit));

                        if (Convert.ToBoolean(objConsumerBe.IsUpdated))
                        {
                            BindbusinessUnits();
                            BindtariffDetails();
                            Message(UMS_Resource.CHARGE_UPDATED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        else
                        {

                            Message(UMS_Resource.CHARGE_UPDATED_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        break;
                    case "DELETECHARGE":
                        //objTariffManagementBE.ActiveStatusId = Convert.ToBoolean(0);
                        //objTariffManagementBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objTariffManagementBE.AdditionalChargeID = Convert.ToInt32(lblAdditionalChargeId.Text);
                        //xml = objTariffManagementBal.Insert(objTariffManagementBE, Statement.Delete);
                        //objTariffManagementBE = objIdsignBal.DeserializeFromXml<TariffManagementBE>(xml);

                        //if (Convert.ToBoolean(objTariffManagementBE.IsSuccess))
                        //{
                        //    BindGridTariffFixedChargesList();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message(UMS_Resource.CHARGE_DELETE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        //}
                        //else
                        //    Message(UMS_Resource.CHARGE_DELETE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void ddlTariff_SelectedIndexChanged(object sender, EventArgs e)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Get(objConsumerBe, ReturnType.Fetch));
            if (objConsumerBe != null)
            {
                txtEstEnergyCharges.Text = (objConsumerBe.EnergyCharges).ToString();
            }
            BindbusinessUnits();
            BindtariffDetails();
        }


        #endregion

        #region Methods
        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void BindbusinessUnits()
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            ConsumerListBe objConsumerListBe = new ConsumerListBe();
            objConsumerBe.TariffId = Convert.ToInt32(this.TariffId);
            XmlDocument xml = new XmlDocument();
            xml = objConsumerBal.Insert(objConsumerBe, Statement.Check);
            objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(xml);
            GvBusinessUnits.DataSource = objConsumerListBe.items;
            GvBusinessUnits.DataBind();
        }

        private void BindtariffDetails()
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            ConsumerListBe objConsumerListBe = new ConsumerListBe();
            XmlDocument xml = new XmlDocument();
            xml = objConsumerBal.Insert(objConsumerBe, Statement.Change);
            objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(xml);
            GvTariffDetails.DataSource = objConsumerListBe.items;
            GvTariffDetails.DataBind();

        }
        #endregion




    }
}