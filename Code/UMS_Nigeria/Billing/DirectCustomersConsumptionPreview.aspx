﻿<%@ Page Title="Document Preview" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="DirectCustomersConsumptionPreview.aspx.cs"
    Inherits="UMS_Nigeria.Billing.DirectCustReadingsDocPreview" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:HiddenField runat="server" ID="hfValidate" />
        <asp:HiddenField runat="server" ID="hfTotalValue" />
        <asp:Panel ID="pnlMessage" runat="server">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <div class="text_total">
            <div class="text-heading">
                <asp:Literal ID="litSearchCustomer" runat="server" Text="Direct Customers Consumption Upload"></asp:Literal>
            </div>
            <div class="star_text">
            </div>
        </div>
        <div class="clr">
        </div>
        <div class="dot-line">
        </div>
        <div class="previous">
            <a href="DirectCustomersConsumptionUpload.aspx" style="font-weight: bold;">Back To Upload
            </a>
        </div>
        <div class="grid_boxes">
            <div class="grid_pagingTwo_top">
                <div class="paging_top_title">
                    <asp:Literal ID="litCountriesList" runat="server" Text="Valid Customers Consumption Upload"></asp:Literal>
                </div>                
            </div>
        </div>
        <div class="grid_tb" id="divPrint" runat="server">
            <asp:GridView ID="gvDocPrevList" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                HeaderStyle-CssClass="grid_tb_hd" OnRowDataBound="gvMeterReadingList_RowDataBound">
                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                <EmptyDataTemplate>
                    Zero Records found.
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                        HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <%#Container.DataItemIndex+1%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" runat="server"
                        HeaderStyle-Width="16%" HeaderStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label ID="lblActiveStatusId" Visible="false" runat="server" class="lblgvAccountNos"
                                Text='<%#Eval("ActiveStatusId") %>'></asp:Label>
                            <asp:Label ID="lblAccountNo" runat="server" class="lblgvAccountNos" Text='<%#Eval("AccNum") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Old Account No" ItemStyle-Width="16%" ItemStyle-HorizontalAlign="Center"
                        DataField="OldAccountNo" />
                    <asp:TemplateField HeaderText="<%$ Resources:Resource, MIN_READING%>" runat="server"
                        HeaderStyle-Width="16%" HeaderStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label ID="lblGvMinReading" Visible="false" runat="server" Text='<%#Eval("Average_Reading") %>'></asp:Label><br />
                            <asp:TextBox ID="txtGvMinReading" CssClass="text-box" runat="server" Text='<%#Eval("Average_Reading") %>'
                                MaxLength="8"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender34" runat="server" TargetControlID="txtGvMinReading"
                                FilterType="Numbers">
                            </asp:FilteredTextBoxExtender>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div class="clear pad_10">
        </div>
        <div class="box_total">
            <div class="box_total_a">
                <asp:Button ID="btnNext" CssClass="box_s" Text="<%$ Resources:Resource, SAVE%>" runat="server"
                    OnClientClick="return ValidateData()" />
                <asp:HiddenField ID="hfDummyForNext" runat="server" />
            </div>
            <div style="clear: both;">
            </div>
        </div>
        <div class="clear pad_10">
        </div>
        <div class="grid_boxes">
            <div class="grid_pagingTwo_top">
                <div class="paging_top_title">
                    <asp:Literal ID="Literal1" runat="server" Text="InValid Customers Consumption Upload"></asp:Literal>
                </div>                
            </div>
        </div>
        <div class="grid_tb" id="div1" runat="server">
            <asp:GridView ID="gvError" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                HeaderStyle-CssClass="grid_tb_hd" OnRowDataBound="gvMeterReadingList_RowDataBound">
                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                <EmptyDataTemplate>
                    Zero Records found.
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                        HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <%#Container.DataItemIndex+1%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" runat="server"
                        HeaderStyle-Width="16%" HeaderStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label ID="lblActiveStatusId" Visible="false" runat="server" class="lblgvAccountNos"
                                Text='<%#Eval("ActiveStatusId") %>'></asp:Label>
                            <asp:Label ID="lblAccountNo" runat="server" class="lblgvAccountNos" Text='<%#Eval("AccNum") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Old Account No" ItemStyle-Width="16%" ItemStyle-HorizontalAlign="Center"
                        DataField="OldAccountNo" />
                    <asp:TemplateField HeaderText="<%$ Resources:Resource, MIN_READING%>" runat="server"
                        HeaderStyle-Width="16%" HeaderStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label ID="lblGvMinReading" runat="server" Text='<%#Eval("Average_Reading") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks" runat="server" HeaderStyle-Width="16%" HeaderStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:Label ID="lblGvRemarks" class="lblgvUsage" runat="server" Text=""></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <asp:ModalPopupExtender ID="mpeSaveConfirm" runat="server" PopupControlID="pnlSaveConfirm"
        TargetControlID="hfDummyForNext" BackgroundCssClass="modalBackground" CancelControlID="btnSaveCancel">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlSaveConfirm" runat="server" CssClass="modalPopup" Style="display: none;"
        class="poppnl">
        <div class="popheader popheaderlblred" id="pop" runat="server">
            <asp:Label ID="lblEstimatedBillMessage" runat="server" Text="Are you sure, do you want to save these Details ?"></asp:Label>
        </div>
        <div class="footer popfooterbtnrt">
            <div class="fltr popfooterbtn">
                <asp:Button ID="btnConfirm" runat="server" Text="<%$ Resources:Resource, CONFIRM%>"
                    OnClick="btnNext_Click" CssClass="ok_btn" />
                <asp:Button ID="btnSaveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                    CssClass="cancel_btn" />
            </div>
            <div class="clear pad_5">
            </div>
        </div>
    </asp:Panel>
    <asp:ModalPopupExtender ID="mpeCountrypopup" runat="server" TargetControlID="btnpopup"
        CancelControlID="hfdummy" PopupControlID="pnlConfirmation" BackgroundCssClass="popbg">
    </asp:ModalPopupExtender>
    <asp:Button ID="btnpopup" runat="server" Text="" Style="display: none;" />
    <asp:Panel runat="server" ID="pnlConfirmation" CssClass="modalPopup" DefaultButton="btnOk"
        Style="display: none; border-color: #639B00;">
        <%-- <div class="popheader" style="background-color: #EEB72D;">--%>
        <div class="popheader" style="background-color: #639B00;">
            <asp:HiddenField ID="hfdummy" runat="server" />
            <asp:Label ID="lblMsgPopup" runat="server" Text=""></asp:Label>
        </div>
        <br />
        <div class="footer" align="right">
            <div class="fltr popfooterbtn">
                <asp:Button ID="btnOk" runat="server" Text="<%$Resources:Resource, OK%>" CssClass="btn_ok"
                    OnClick="btnOk_Click" Style="margin-left: 105px;" />
            </div>
            <div class="clear pad_5">
            </div>
        </div>
    </asp:Panel>
    <%-- Grid Alert popup Start--%>
    <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
        TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
    </asp:ModalPopupExtender>
    <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
    <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
        <div class="popheader popheaderlblred">
            <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
        </div>
        <div class="footer popfooterbtnrt">
            <div class="fltr popfooterbtn">
                <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                    CssClass="ok_btn" />
            </div>
            <div class="clear pad_5">
            </div>
        </div>
    </asp:Panel>
    <%-- Grid Alert popup Closed--%>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).ready(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });

    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../scripts/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
    <script type="text/javascript">
        function ValidateData() {
            var isValid = true;

            var elements = document.getElementsByClassName("text-box");
            var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
            var mpeSaveConfirm = $find('UMSNigeriaBody_mpeSaveConfirm');
            var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

            for (var i = 0; i < elements.length; i++) {
                if (Trim(elements[i].value) == "") {
                    isValid = false;
                    break;
                }
                else if (parseInt(Trim(elements[i].value)) == 0) {
                    isValid = false;
                    break;
                }
            }

            if (!isValid) {
                lblAlertMsg.innerHTML = "Please enter valid Minimum Reading for all Customers.";
                mpeAlert.show();
            }
            else {
                mpeSaveConfirm.show();
            }
            return false;
        }
    </script>
</asp:Content>
