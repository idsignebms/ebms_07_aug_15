﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.IO;
using UMS_NigeriaBE;
using Resources;
using iDSignHelper;
using UMS_NigeriaBAL;
using System.Xml;
using ClosedXML.Excel;
using UMS_NigriaDAL;
using System.Drawing;

namespace UMS_Nigeria.Billing
{
    public partial class DirectCustReadingsDocPreview : System.Web.UI.Page
    {
        #region Members

        CommonMethods objCommonMethods = new CommonMethods();
        iDsignBAL objIdsignBal = new iDsignBAL();
        BillReadingsBAL objBillReadingBAL = new BillReadingsBAL();
        string Key = UMS_Resource.KEY_METERREADING_DOCUMENTUPLOAD;
        public int PageNum;
        string Filename;

        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["DirectCustomersConsuptionUploadDT"] != null)
                {
                    //Filename = objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.METER_READING_QST_DOCMUNET].ToString());
                    //DataTable dt = new DataTable();
                    //dt = objCommonMethods.ExcelImport(Server.MapPath(ConfigurationManager.AppSettings["MeterReading"].ToString()) + Filename);
                    DataSet ds = new DataSet();
                    ds = (DataSet)Session["DirectCustomersConsuptionUploadDT"];
                    //ds.Tables.Add(dt);

                    //ds = objIdsignBal.ConvertListToDataSet<BillingBE>(objIdsignBal.DeserializeFromXml<GlobalMessagesListBE>(objBillReadingBAL.GetDirectReadingsInfoXL(ds.Tables[0])).CustomerReading);
                    ds = objBillReadingBAL.GetValidateAvgConsumptionUpload(ds.Tables[0]);

                    var listOfValidData = from dtLIst in ds.Tables[0].AsEnumerable()
                                          where dtLIst.Field<Int32>("ActiveStatusId") != 4
                                          select dtLIst;

                    var listOfInvalidData = from dtLIst in ds.Tables[0].AsEnumerable()
                                            where dtLIst.Field<Int32>("ActiveStatusId") == 4
                                            select dtLIst;

                    if (listOfValidData.Count() > 0)
                    {
                        gvDocPrevList.DataSource = listOfValidData.CopyToDataTable();
                        gvDocPrevList.DataBind();
                        btnNext.Visible = true;
                    }
                    else
                    {
                        gvDocPrevList.DataSource = new DataTable();
                        gvDocPrevList.DataBind();
                        btnNext.Visible = false;
                    }

                    if (listOfInvalidData.Count() > 0)
                    {
                        gvError.DataSource = listOfInvalidData.CopyToDataTable();
                        gvError.DataBind();
                    }
                    else
                    {
                        gvError.DataSource = new DataTable();
                        gvError.DataBind();
                    }
                }
                else
                {
                    Response.Redirect(Constants.DirectCustomersReadingsUploadPage, false);
                }
            }
        }

        #region Events

        protected void gvMeterReadingList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");
                    Label lblGvRemarks = (Label)e.Row.FindControl("lblGvRemarks");

                    if (lblActiveStatusId.Text == 4.ToString())
                    {
                        lblGvRemarks.Text = "Closed Customer";
                        lblGvRemarks.ForeColor = Color.White;
                        e.Row.BackColor = Color.Tomato;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                UploadMeterReadings();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void UploadMeterReadings()
        {
            try
            {
                int count = 0;
                //BillingBE _objBilling = new BillingBE();
                DataTable dt = new DataTable();
                dt.Columns.Add("SNo", typeof(int));
                dt.Columns.Add("AccNum", typeof(string));
                dt.Columns.Add("AverageReading", typeof(int));
                dt.Columns.Add("CreatedBy", typeof(string));

                foreach (GridViewRow row in gvDocPrevList.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        Label lblAccountNo = (Label)row.FindControl("lblAccountNo");
                        Label lblActiveStatusId = (Label)row.FindControl("lblActiveStatusId");
                        TextBox txtGvMinReading = (TextBox)row.FindControl("txtGvMinReading");

                        if (lblActiveStatusId.Text != "4")
                        {
                            dt = GetEffectedBillDetails(dt, lblAccountNo.Text, Convert.ToInt32(txtGvMinReading.Text), Session[UMS_Resource.SESSION_LOGINID].ToString());
                        }
                    }
                }

                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                //XmlDocument xml = objIdsignBal.DoSerialize(dt);
                //_objBilling = objIdsignBal.DeserializeFromXml<BillingBE>(objBillReadingBAL.InsertDirectCustomerBillReadingBAL(xml));

                count = objBillReadingBAL.InsertAverageUploadDT(dt);

                if (count > 0)
                {
                    lblMsgPopup.Text = UMS_Resource.DETAILS_INSERTED;
                    mpeCountrypopup.Show();
                }
                else
                {
                    objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, Resource.READINGS_INSERT_FAILED, pnlMessage, lblMessage);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private DataTable GetEffectedBillDetails(DataTable dt, string AccountNo, int AverageReading, string CreatedBy)
        {
            DataRow dr = dt.NewRow();
            dr["SNo"] = dt.Rows.Count + 1;
            dr["AccNum"] = AccountNo;
            dr["AverageReading"] = AverageReading;
            dr["CreatedBy"] = CreatedBy;
            dt.Rows.Add(dr);
            return dt;
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Constants.DirectCustomersReadingsUploadPage, false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion
    }
}