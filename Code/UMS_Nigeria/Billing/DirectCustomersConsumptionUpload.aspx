﻿<%@ Page Title=":: Direct Customers Readings Upload ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="DirectCustomersConsumptionUpload.aspx.cs"
    Inherits="UMS_Nigeria.Billing.DirectCustomersReadingsUpload" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script type="text/javascript">
        function pageLoad() {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        }

        function Validate() {
            var IsValid = true;

            var popup = $find('<%= mpePaymentypopup.ClientID %>');
            var lblmsg = document.getElementById('<%= lblMsgPopup.ClientID %>');
            var fupExcel = document.getElementById('<%= upDOCExcel.ClientID %>');
            if (OnUploadpopup(popup, lblmsg, fupExcel, ['xls', 'xlsx'], " Excel File") == false)
                return false;
        }      
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <div class="inner-sec">
            <asp:UpdatePanel ID="upPaymentUpload" runat="server" ChildrenAsTriggers="true">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="text_total">
                <div class="text-heading">
                    <asp:Literal ID="litAddState" runat="server" Text="Customer Average Consumption Upload"></asp:Literal>
                </div>
                <div class="star_text">
                    <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="dot-line">
            </div>
            <div class="inner-box">
                <div class="inner-box1" style="width: 80%">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, UPLOAD_DOCUMENT%>"></asp:Literal>
                            <span class="span_star">*</span>
                        </label>
                    </div>
                    <div class="space">
                    </div>
                    <asp:FileUpload ID="upDOCExcel" runat="server" />
                    &nbsp &nbsp <a href="../Uploads/MeterReadingDocuments/DirectCustomerConsumptionUploadSample.zip"
                        target="_blank">Download Sample Document</a>
                    <br /><br />
                    <%--<b class="spancolor">Note:</b> <span>Please provide Read_Date format as "mm/dd/yyyy"
                        only in uploaded Excel.</span>--%>
                </div>
            </div>
        </div>
        <br />
        <div style="clear: both;">
        </div>
        <div class="box_total">
            <div class="box_total_a">
                <asp:Button ID="btnNext" CssClass="box_s" Text="<%$ Resources:Resource, NEXT%>" runat="server"
                    OnClick="btnNext_Click" OnClientClick="return Validate();" />
            </div>
        </div>
        <div style="clear: both;">
        </div>
        <asp:ModalPopupExtender ID="mpePaymentypopup" runat="server" TargetControlID="btnpopup"
            PopupControlID="pnlConfirmation" BackgroundCssClass="modalBackground">
        </asp:ModalPopupExtender>
        <asp:Button ID="btnpopup" runat="server" Text="" Style="display: none;" />
        <asp:Panel runat="server" BorderColor="#CD3333" ID="pnlConfirmation" CssClass="modalPopup">
            <div class="popheader" style="background-color: #CD3333;">
                <asp:Panel ID="pnlMsgPopup" runat="server" align="center">
                    <asp:Label ID="lblMsgPopup" runat="server"></asp:Label>
                </asp:Panel>
            </div>
            <div class="clear pad_10">
            </div>
            <div class="footer" align="right">
                <asp:Button ID="btnOk" runat="server" Text="OK" CssClass="btn_ok" Style="margin-left: 200px;" />
                <br />
            </div>
        </asp:Panel>
    </div>
</asp:Content>
