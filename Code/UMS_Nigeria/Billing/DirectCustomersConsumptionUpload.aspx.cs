﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using System.Xml;
using System.Data;
using System.Configuration;
using Resources;
using System.IO;
using UMS_NigriaDAL;
using System.Collections;
using System.Data.OleDb;
using System.Globalization;

namespace UMS_Nigeria.Billing
{
    public partial class DirectCustomersReadingsUpload : System.Web.UI.Page
    {
        #region Members
        CommonMethods objCommonMethods = new CommonMethods();
        iDsignBAL objIdsignBal = new iDsignBAL();
        string Key = "DirectCut_Readings_DocUpload";
        XmlDocument xml = null;
        public int PageNum;
        string Filename;
        DataTable dtExcel = new DataTable();
        DataTable dtXMl = new DataTable();
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string path = string.Empty;
                path = objCommonMethods.GetPagePath(this.Request.Url);
                if (objCommonMethods.IsAccess(path))
                {
                }
                else
                {
                    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE, false);
                }
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                int count = 0;
                bool isValid = true;
                objCommonMethods.ErrorMessage("Before saving Excel", Key, "62");
                string ExcelModifiedFileName = InsersertExcel();
                objCommonMethods.ErrorMessage("After saving Excel", Key, "64");
                DataSet ds = new DataSet();
                if (Path.GetExtension(ExcelModifiedFileName) == ".xls" || Path.GetExtension(ExcelModifiedFileName) == ".xlsx")
                {
                    string FolderPath = ConfigurationManager.AppSettings["MeterReading"];
                    string FilePath = Server.MapPath(FolderPath + ExcelModifiedFileName);
                    String cnstr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FilePath + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";//2007 Onwards

                    objCommonMethods.ErrorMessage(cnstr, Key, "78");
                    OleDbConnection oledbConn = new OleDbConnection(cnstr);
                    objCommonMethods.ErrorMessage(FilePath, Key, "80");
                    String[] s = objCommonMethods.GetExcelSheetNames(FilePath);
                    objCommonMethods.ErrorMessage(s.Length.ToString(), Key, "92");

                    string strSQL = "SELECT * FROM [" + s[0] + "]";
                    OleDbCommand cmd = new OleDbCommand(strSQL, oledbConn);
                    
                    OleDbDataAdapter odapt = new OleDbDataAdapter(cmd);
                    objCommonMethods.ErrorMessage("Error", Key, "92");
                    odapt.Fill(ds);
                    dtExcel = ds.Tables[0];
                }
                if (dtExcel.Rows.Count > 0)//Validating excel columns.
                {
                    objCommonMethods.ErrorMessage("In Validating Excel", Key,"88");
                    for (int i = 0; i < dtExcel.Columns.Count; i++)
                    {
                        DataSet dsHeaders = new DataSet();
                        objCommonMethods.ErrorMessage("Before ReadXml - DirectCustReadingUploadHeaders", Key, "92");
                        dsHeaders.ReadXml(Server.MapPath(ConfigurationSettings.AppSettings["DirectCustReadingUploadHeaders"]));
                        objCommonMethods.ErrorMessage("After ReadXml - DirectCustReadingUploadHeaders", Key, "94");
                        if (dtExcel.Columns.Count == dsHeaders.Tables[0].Rows.Count)
                        {
                            objCommonMethods.ErrorMessage("If Header Count matches with Excel column count", Key, "97");
                            string HeaderName = dtExcel.Columns[i].ColumnName;
                            var result = (from x in dsHeaders.Tables[0].AsEnumerable()
                                          where x.Field<string>("Names").Trim() == HeaderName.Trim()
                                          select new
                                          {
                                              Names = x.Field<string>("Names")
                                          }).SingleOrDefault();
                            if (result == null)
                            {
                                count++;
                                objCommonMethods.ErrorMessage("If Header doesn't matches with Excel column", Key, "108");
                                File.Delete(Server.MapPath(ConfigurationManager.AppSettings["MeterReading"].ToString()) + Filename);
                                objCommonMethods.ErrorMessage("If Header doesn't matches with Excel column Delete file", Key, "110");
                                lblMsgPopup.Text = Resource.COLUMNS_NOT_MATCHNIG;
                                mpePaymentypopup.Show();
                                break;
                            }
                        }
                        else
                        {
                            objCommonMethods.ErrorMessage("If Header Count doesn't matches with Excel column count", Key, "119");
                            count++;
                            File.Delete(Server.MapPath(ConfigurationManager.AppSettings["MeterReading"].ToString()) + Filename);

                            lblMsgPopup.Text = Resource.COLUMNS_NOT_MATCHNIG;
                            mpePaymentypopup.Show();
                        }
                    }
                }
                if (count == 0)
                {
                    try
                    {
                        Session["DirectCustomersConsuptionUploadDT"] = ds;
                        Response.Redirect(Constants.DirecrCustomersConsumptionPreiviewPage + "?" + UMS_Resource.METER_READING_QST_DOCMUNET + "=" + objIdsignBal.Encrypt(ExcelModifiedFileName), false);
                    }
                    catch (Exception ex)
                    {
                        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                        try
                        {
                            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                        }
                        catch (Exception logex)
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }
        #endregion

        #region Methods
        public string InsersertExcel()
        {
            string ExcelModifiedFileName = string.Empty;
            try
            {
                

                if (upDOCExcel.HasFile)
                {
                    string ExcelFileName = System.IO.Path.GetFileName(upDOCExcel.FileName).ToString();//Assinging FileName
                    string ExcelExtension = Path.GetExtension(ExcelFileName);//Storing Extension of file into variable Extension
                    TimeZoneInfo ExcelIND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                    DateTime ExcelcurrentTime = DateTime.Now;
                    DateTime Exceloourtime = TimeZoneInfo.ConvertTime(ExcelcurrentTime, ExcelIND_ZONE);
                    string ExcelCurrentDate = Exceloourtime.ToString("dd_MM_yyyy_hh_mm_ss");
                    ExcelModifiedFileName = Path.GetFileNameWithoutExtension(ExcelFileName) + "_" + ExcelCurrentDate + ExcelExtension;//Setting File Name to store it in database
                    Filename = Path.GetFileNameWithoutExtension(ExcelFileName) + "_" + ExcelCurrentDate + ExcelExtension;//Setting File Name to store it in database
                    upDOCExcel.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["MeterReading"].ToString()) + ExcelModifiedFileName);//Saving File in to the server.

                }
                
            }
            catch (Exception ex)
            {

                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
            return ExcelModifiedFileName;
        }
        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion
    }
}