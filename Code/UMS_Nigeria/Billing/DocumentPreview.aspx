﻿<%@ Page Title="::Payment Doc Preview::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="DocumentPreview.aspx.cs" EnableViewState="true"
    Inherits="UMS_Nigeria.Billing.DocumentPreview" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdateProgress ID="UpdateProgress" runat="server">
                <ProgressTemplate>
                    <center>
                        <div id="loading-div" class="pbloading">
                            <div id="title-loading" class="pbtitle">
                                BEDC</div>
                            <center>
                                <img src="../images/loading.GIF" class="pbimg"></center>
                            <p class="pbtxt">
                                Processing Please wait.....</p>
                        </div>
                    </center>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
                PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
            <asp:HiddenField runat="server" ID="hfValidate" Value="true" />
            <asp:HiddenField runat="server" ID="hfTotalValue" />
            <asp:UpdatePanel ID="upPaymentUpload" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="clr">
                    </div>
                    <div class="pad_5">
                        <a href="../Billing/PaymentUpload.aspx" class="back_but">&lt;&lt; Back To Upload
                        </a>
                    </div>
                    <div class="clr">
                    </div>
                    <br />
                    <div class="grid_tb" id="divPrint" runat="server" align="center" style="overflow-x: auto;
                        width: 95%">
                        <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                            HeaderStyle-CssClass="grid_tb_hd" OnRowDataBound="gvData_RowDataBound" ShowFooter="true">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:BoundField DataField="RowNumber" HeaderText="SNO" ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Global AccountNo" ItemStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnAccountNo" runat="server" Text='<%#Eval("AccountNo") %>'
                                            CommandArgument='<%#Eval("ReceiptNO") %>' ForeColor="Black" Enabled="false"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Old AccountNo" ItemStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnOldAccountNo" runat="server" Text='<%#Eval("OldAccountNo") %>'
                                            ForeColor="Black" Enabled="false"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Amount" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPaidAmount" runat="server" Text='<%#Eval("PaidAmount") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ReceiptNO" HeaderText="ReceiptNO" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Date" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDate" runat="server" Text='<%#Eval("ReceivedDate") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Payment Mode" ItemStyle-HorizontalAlign="Right">
                                    <ItemTemplate>
                                        <%--<asp:Label ID="lblPaymentMode" runat="server" Text='<%#Eval("PaymentMode") %>'></asp:Label>--%>
                                        <asp:LinkButton ID="lbtnPayments" runat="server" ForeColor="Black" Enabled="false"
                                            Text='<%#Eval("PaymentMode") %>' CommandName='<%#Eval("PaymentModeId") %>' CommandArgument='<%#Eval("IsDuplicate") %>'></asp:LinkButton>
                                        <%--<asp:Literal ID="litPaymentModeId" runat="server" Visible="false" Text='<%#Eval("PaymentModeId") %>'></asp:Literal>--%>
                                        <%--<asp:Literal ID="litTotalBillPending" runat="server" Visible="false" Text='<%#Eval("TotalPendingBills") %>'></asp:Literal>
                                        <asp:Literal ID="litIsBillInProcess" runat="server" Visible="false" Text='<%#Eval("IsBillInProcess") %>'></asp:Literal>--%>
                                        <asp:Literal ID="litIsExists" runat="server" Visible="false" Text='<%#Eval("IsExists") %>'></asp:Literal>
                                        <%--<asp:Literal ID="litIsActiveStatusId" runat="server" Visible="false" Text='<%#Eval("ActiveStatusId") %>'></asp:Literal>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Remark">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRemark" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <br />
                    <div class="clr">
                    </div>
                    <div class="space_1">
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <asp:Button ID="btnNext" CssClass="box_s" Text="SAVE" runat="server" OnClick="btnNext_Click" />
                        </div>
                    </div>
                    <div id="hidepopup">
                        <asp:ModalPopupExtender ID="mpeAlertpopup" runat="server" TargetControlID="btnpopup"
                            PopupControlID="pnlConfirmation" BackgroundCssClass="popbg">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnpopup" runat="server" Text="" Style="display: none;" />
                        <asp:Panel runat="server" ID="pnlConfirmation" CssClass="modalPopup" Style="display: none;
                            border-color: #639B00;">
                            <div class="popheader" style="background-color: green;">
                                <asp:Label ID="lblMsgPopup" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="clear pad_10">
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btnOk" runat="server" Text="OK" CssClass="btn_ok" Style="margin-left: 105px;"
                                        OnClick="btnOk_Click" />
                                    <br />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:ModalPopupExtender ID="mpeNoPaymentsValid" runat="server" TargetControlID="hdnCancel"
                            PopupControlID="pnlNoPaymentsValid" BackgroundCssClass="modalBackground">
                        </asp:ModalPopupExtender>
                        <asp:HiddenField ID="hdnCancel" runat="server" />
                        <asp:Panel runat="server" ID="pnlNoPaymentsValid" CssClass="modalPopup" Style="display: none;
                            border-color: #639B00;">
                            <div class="popheader" style="background-color: red;">
                                <asp:Label ID="lblNBP" runat="server"></asp:Label>
                            </div>
                            <div class="clear pad_10">
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btnPBPCancel" runat="server" Text="OK" CssClass="btn_ok" Style="margin-left: 105px;"
                                        OnClick="btnOk_Click" />
                                    <br />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:ModalPopupExtender ID="mpeConfirmValidation" runat="server" TargetControlID="hdnConfValidation"
                            PopupControlID="pnlConfValidation" BackgroundCssClass="popbg" CancelControlID="btnConfirmValidationCancel">
                        </asp:ModalPopupExtender>
                        <asp:HiddenField ID="hdnConfValidation" runat="server" />
                        <asp:Panel runat="server" ID="pnlConfValidation" CssClass="modalPopup" Style="display: none;
                            border-color: #639B00;">
                            <div class="popheader" style="background-color: red;">
                                <asp:Label ID="Label1" runat="server" Text="<%$Resources:Resource, SURE_TO_UPLOAD_PAYMENTS %>"></asp:Label>
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btnConfirmValidation" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="btn_ok" Style="margin-left: 105px;" OnClick="btnConfirmValidation_Click" />
                                    <asp:Button ID="btnConfirmValidationCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" Style="margin-left: 10px;" />
                                    <br />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:HiddenField ID="hdnValidation" runat="server" Value="0" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    `
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
        function pageLoad() {
            DisplayMessage('UMSNigeriaBody_pnlMessage');
        };
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
