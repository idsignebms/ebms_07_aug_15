﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.IO;
using UMS_NigeriaBE;
using Resources;
using iDSignHelper;
using UMS_NigeriaBAL;
using System.Xml;
using ClosedXML.Excel;
using UMS_NigriaDAL;
using System.Drawing;

namespace UMS_Nigeria.Billing
{
    public partial class DocumentPreview : System.Web.UI.Page
    {
        #region Members

        CommonMethods _ObjCommonMethods = new CommonMethods();
        iDsignBAL objIdsignBal = new iDsignBAL();
        UserManagementBAL objUserManagementBAL = new UserManagementBAL();
        BillCalculatorBAL objBillCalculatorBAL = new BillCalculatorBAL();
        string Key = "DocumentPreview";
        public int PageNum;

        BillReadingsBAL objBillingBAL = new BillReadingsBAL();
        MastersBAL _ObjMastersBAL = new MastersBAL();
        ReportsBal objReportsBal = new ReportsBal(); //Raja
        DataSet dsAccountNumbers = new DataSet();
        DataTable dtAccountNumbers = new DataTable();

        #endregion

        #region Methods

        public void UploadPayments()
        {
            try
            {
                mpeConfirmValidation.Hide();
                PaymentsBe _objPaymentsBe = new PaymentsBe();
                BillingBE _objBillingBe = new BillingBE();
                XmlDocument xmlResult = new XmlDocument();

                bool IsApproval = _ObjCommonMethods.IsApproval(Resource.FEATURE_PAYMENT_ENTRY);
                DataTable dt = new DataTable();
                DataTable maindt = ((DataTable)Session[Resource.SESSION_VALIDATE_PAYMNET_UPLOAD]).Copy();

                dt = (from x in maindt.AsEnumerable()
                      where x.Field<bool>("IsValid") == true
                      select x).CopyToDataTable();
                DataSet ds = new DataSet("MastersBEInfoByXml");
                ds.Tables.Add(dt);

                string BatchNo = string.Empty;
                string BatchDate = string.Empty;
                string DocumentName = string.Empty;
                string Notes = string.Empty;

                if (Session["BatchDetails"] != null)
                {
                    string[] BatchDetails = Session["BatchDetails"].ToString().Split('^');
                    BatchNo = BatchDetails[0];//Batch Number
                    Notes = BatchDetails[1];//Batch Notes
                    BatchDate = BatchDetails[2];//Batch Date
                    DocumentName = BatchDetails[3];//Batch File Name
                }

                _objBillingBe.BatchNo = Convert.ToInt32(BatchNo);
                _objBillingBe.BatchName = "Payment Upload";
                _objBillingBe.BatchDate = BatchDate;
                _objBillingBe.Description = Notes;
                _objBillingBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objBillingBe.BatchTotal = string.IsNullOrEmpty(hfTotalValue.Value) ? 0 : decimal.Parse(hfTotalValue.Value);
                _objBillingBe.Flag = Convert.ToInt32(Resources.Resource.FLAG_TEMP);
                _objBillingBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();

                if (IsApproval)
                    _objBillingBe.Flag = Convert.ToInt32(Resources.Resource.FLAG_TEMP);
                else
                    _objBillingBe.Flag = Convert.ToInt32(Resources.Resource.INSERT_FINAL);

                _objBillingBe = objIdsignBal.DeserializeFromXml<BillingBE>(objBillingBAL.InsertBatchEntry(_objBillingBe, Statement.Insert));

                if (_objBillingBe.IsSuccess == 1)
                {
                    bool isSuccess = false;
                    string CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    string DocumentPath = Server.MapPath(ConfigurationManager.AppSettings["Payments"].ToString());
                    int BatchNumber = Convert.ToInt32(BatchNo);
                    string Status=string.Empty;

                    //_objPaymentsBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    //_objPaymentsBe.DocumentPath = Server.MapPath(ConfigurationManager.AppSettings["Payments"].ToString());
                    //_objPaymentsBe.DocumentName = DocumentName;
                    //_objPaymentsBe.BatchNo = Convert.ToInt32(BatchNo);

                    //xmlResult = _ObjMastersBAL.Insert(_objPaymentsBe, dt, Statement.Insert);
                    //_objPaymentsBe = objIdsignBal.DeserializeFromXml<PaymentsBe>(xmlResult);

                    isSuccess = _ObjMastersBAL.InsertPaymentsDT(CreatedBy, DocumentPath, DocumentName, BatchNumber, dt, out Status);

                    if (isSuccess)
                    {
                        Session.Remove(Resource.SESSION_VALIDATE_PAYMNET_UPLOAD);
                        Session.Remove(Resource.SESSION_PAYMENTUPLOADSDT);
                        mpeAlertpopup.Show();
                        lblMsgPopup.Text = "Payments are saved successfully.";
                        btnNext.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                try
                {
                    Message("Exception In Uploads.", UMS_Resource.MESSAGETYPE_ERROR);
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPaymentUpload(DataTable PaymentDT)
        {
            try
            {
                //XmlDocument xml = new XmlDocument();
                //PaymentsBe _objPaymentsBe = new PaymentsBe();
                //PaymentsListBe _objPaymentsListBe = new PaymentsListBe();
                //_objPaymentsBe.BU_Id = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                //xml = _ObjMastersBAL.PaymentUploadValidationBAL(_objPaymentsBe, PaymentDT);
                //_objPaymentsListBe = objIdsignBal.DeserializeFromXml<PaymentsListBe>(xml);

                DataSet dsPayments = new DataSet();
                string BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();

                dsPayments = _ObjMastersBAL.GetValidatePaymentsUpload(BU_ID, PaymentDT);

                if (dsPayments != null && dsPayments.Tables.Count > 0)
                {
                    if (dsPayments.Tables[0].Rows.Count > 0)
                    {
                        //DataTable dt = objIdsignBal.ConvertListToDataSet<PaymentsBe>(_objPaymentsListBe.Items).Tables[0];
                        DataTable dt = dsPayments.Tables[0];
                        DataColumnCollection columns = dt.Columns;
                        if (!columns.Contains("IsValid"))
                        {
                            dt.Columns.Add("IsValid", typeof(bool));
                        }

                        foreach (DataRow row in dt.Rows)
                        {
                            row["IsValid"] = true;
                            row.EndEdit();
                            dt.AcceptChanges();
                        }

                        var rowsToUpdate = (from r in dt.AsEnumerable()
                                            where r.Field<int>("PaymentModeId") == 0
                                                || r.Field<int>("IsExists") == 0
                                                || r.Field<int>("IsDuplicate") == 1
                                            select r).ToList();

                        foreach (var row in rowsToUpdate)
                        {
                            row.SetField("IsValid", false);
                        }

                        hfTotalValue.Value = (from x in dt.AsEnumerable()
                                              where x.Field<bool>("IsValid") == true
                                              select x.Field<decimal>("PaidAmount")).Sum().ToString();

                        Session[Resource.SESSION_VALIDATE_PAYMNET_UPLOAD] = dt;

                        gvData.DataSource = dt;
                        gvData.DataBind();
                        gvData.FooterRow.Cells[0].Text = Resource.BATCH_TOTAL;
                        gvData.FooterRow.Cells[1].Text = string.IsNullOrEmpty(hfTotalValue.Value) ? "0.00" : _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(hfTotalValue.Value), 2, Constants.MILLION_Format);
                        gvData.FooterRow.Cells[1].Attributes.Add("colSpan ", "6");
                        gvData.FooterRow.Cells[1].Attributes.Add("style ", "padding-left:366px;");
                        for (int i = 2; i < gvData.FooterRow.Cells.Count; i++)
                            gvData.FooterRow.Cells[i].Visible = false;
                    }
                    else
                    {
                        mpeNoPaymentsValid.Show();
                        lblNBP.Text = "InValid Data Uploaded";
                    }
                    btnNext.Visible = true;
                }
                else
                {
                    mpeNoPaymentsValid.Show();
                    lblNBP.Text = "InValid Data Uploaded";
                }
            }
            catch (Exception ex)
            {
                try
                {
                    btnNext.Visible = false;
                    Message("Exception In Validation", UMS_Resource.MESSAGETYPE_ERROR);
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable paymentsdt = (DataTable)Session[Resource.SESSION_VALIDATE_PAYMNET_UPLOAD];
                var valid = (from v in paymentsdt.AsEnumerable() where v.Field<bool>("IsValid") == true select v).ToList();
                if (valid.Count() > 0)
                {
                    mpeConfirmValidation.Show();
                }
                else
                {
                    lblNBP.Text = "Please upload valid payments";
                    mpeNoPaymentsValid.Show();
                }
            }
            catch (Exception ex)
            {
                try
                {
                    Message("Error in uploading process.", UMS_Resource.MESSAGETYPE_ERROR);
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString[UMS_Resource.DOCUMENT_BATCHNO] != null)
                    {
                        DataTable dt = new DataTable("Payments");
                        DataSet ds = new DataSet("MastersBEInfoByXml");

                        if (Session[Resource.SESSION_PAYMENTUPLOADSDT] != null)
                        {
                            DataTable uploadedDT = new DataTable();
                            uploadedDT = (DataTable)Session[Resource.SESSION_PAYMENTUPLOADSDT];
                            dt = uploadedDT.Copy();
                        }
                        dt.TableName = "Payments";/// dont Remove or Modify this table name, if modify uploads not working
                        ds.Tables.Add(dt);
                        BindPaymentUpload(dt);
                    }
                }
            }
            else
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["BatchDetails"] != null)
                    Session.Remove("BatchDetails");
                Response.Redirect(UMS_Resource.PAYMENTUPLOAD_PAGE);
            }
            catch (Exception ex)
            {
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnConfirmValidation_Click(object sender, EventArgs e)
        {
            UploadPayments();
        }

        protected void gvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lbtnPayments = (LinkButton)e.Row.FindControl("lbtnPayments");
                    string PaymementModeID = lbtnPayments.CommandName;
                    //string TotalBillPending = ((Literal)e.Row.FindControl("litTotalBillPending")).Text;
                    //string IsBillInProcess = ((Literal)e.Row.FindControl("litIsBillInProcess")).Text;
                    string IsExistsAccountNoExists = ((Literal)e.Row.FindControl("litIsExists")).Text;
                    string isDuplicate = lbtnPayments.CommandArgument.ToLower();
                    LinkButton lbtnAccountNo = (LinkButton)e.Row.FindControl("lbtnAccountNo");
                    Label lblRemark = (Label)e.Row.FindControl("lblRemark");
                    Label lblPaidAmount = (Label)e.Row.FindControl("lblPaidAmount");
                    Label lblDate = (Label)e.Row.FindControl("lblDate");
                    lblDate.Text = _ObjCommonMethods.DateFormate(lblDate.Text);
                    lblPaidAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal((lblPaidAmount.Text)));

                    if (PaymementModeID == "0" || IsExistsAccountNoExists.ToLower() == "false" || isDuplicate == "true")//written by Suresh to allow payments without bills
                    {
                        e.Row.BackColor = Color.Tomato;
                    }
                    if (IsExistsAccountNoExists.Contains("False"))
                    {
                        if (!string.IsNullOrEmpty(lblRemark.Text))
                            lblRemark.Text += "</br>";
                        lblRemark.Text += Resource.CUSTOMER_NA;
                    }
                    else if (PaymementModeID == "0")
                    {
                        if (!string.IsNullOrEmpty(lblRemark.Text))
                            lblRemark.Text += "</br>";
                        lblRemark.Text = Resource.PAY_MOD_NOT_EXISTS;
                    }
                    else if (isDuplicate == "true")
                    {
                        if (!string.IsNullOrEmpty(lblRemark.Text))
                            lblRemark.Text += "</br>";
                        lblRemark.Text = "This payment already done";
                    }
                }
            }
            catch (Exception ex)
            {
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion
    }
}