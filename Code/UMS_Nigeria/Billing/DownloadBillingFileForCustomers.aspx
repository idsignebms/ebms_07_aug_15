﻿<%@ Page Title=":: Download Billing File For Customers::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="DownloadBillingFileForCustomers.aspx.cs"
    Inherits="UMS_Nigeria.Billing.DownloadBillingFileForCustomers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upnlReport" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litReportHeading" runat="server" Text="Download Billing File For Customers"></asp:Literal>
                        </div>
                        <div class="star_text">
                            &nbsp
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="litStateCode" runat="server" Text="Billing Month : "></asp:Literal>
                                <asp:Label ID="lblBillingMonth" Text="" runat="server" />
                                <asp:Label ID="lblYear" Text="" runat="server" />
                                <asp:HiddenField ID="hfYearID" runat="server" Value="0" />
                                <asp:Label ID="lblMonth" Text="" runat="server"/>
                                <asp:HiddenField ID="hfMonthID" runat="server" Value="0" />
                                <asp:Label ID="lblMonthID" Text="" Visible="false" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                        <br />
                        <br />
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="grdDownloadBillingFile" runat="server" AutoGenerateColumns="False"
                            OnRowCommand="grdDownloadBillingFile_RowCommand" HeaderStyle-CssClass="grid_tb_hd">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="S.No." runat="server" HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblSNo" runat="server" Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                                    </ItemTemplate> 
                                </asp:TemplateField>
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" Visible="false" HeaderText="Global Acccount Number" DataField="GlobalAccountNumber"
                                    ItemStyle-CssClass="grid_tb_td" />
                                     <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Global Acccount Number" DataField="AccNoAndGlobalAccNo"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Old Account Number" DataField="OldAccountNo"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Read Type" DataField="ReadType"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Name" DataField="Name"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Service Address" DataField="ServiceAddress"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Tariff" DataField="Tariff"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Bill Generated Date" DataField="BillGeneratedDate"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:TemplateField HeaderText="Report" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:UpdatePanel ID="upnlReportlnk" runat="server">
                                            <ContentTemplate>
                                                <asp:Label runat="server" Visible="false" ID="lblUrl" Text='<%# Eval("FilePath") %>'></asp:Label>
                                                <asp:LinkButton ID="lkbtnNavigate" runat="server" ToolTip="Download Billing File"
                                                    Text="Download" CommandName="DownloadFile">
                                                </asp:LinkButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lkbtnNavigate" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    <asp:HiddenField ID="hfMonth" runat="server" Value="0" />
                    <asp:HiddenField ID="hfYear" runat="server" Value="0" />
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
