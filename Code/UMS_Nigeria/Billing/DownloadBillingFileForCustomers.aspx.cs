﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using System.Xml;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Data;
using System.IO;
using System.Configuration;

namespace UMS_Nigeria.Billing
{
    public partial class DownloadBillingFileForCustomers : System.Web.UI.Page
    {
        #region Members

        iDsignBAL _objIdsignBal = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
        private delegate void EmailSendFile(string filePath, string cycleId, string feederId, string MonthName, int BillingYear, string toEmailId);
        String Key = "DownloadBillingFileForCustoemers";
        string BillingQueueScheduleIdList = null;
        string fileNameList = null;
        string filePathList = null;
        public int PageNum;
        public bool IsFileGenValid = true;

        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                bool IsOpenMonthAvilable = BindOpenMonthAndYear();
                if (IsOpenMonthAvilable)
                {
                    BindGrid();
                }
                else
                {
                    lblMonth.Text = "Open Month is not available";
                }
                //BindYears();
                //BindGrid();
            }

        }
        protected void grdDownloadBillingFile_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblUrl = (Label)row.FindControl("lblUrl");
            //Label lblLastCloseMonth = (Label)row.FindControl("lblLastCloseMonth");

            switch (e.CommandName.ToUpper())
            {
                case "DOWNLOADFILE":

                    ////string fileLocation = Server.MapPath(lblUrl.Text);
                    //string fileLocation = Server.MapPath(ConfigurationManager.AppSettings["BillGeneration"].ToString());
                    //fileLocation +=  lblUrl.Text;
                    //if (File.Exists(fileLocation))
                    //    _ObjCommonMethods.DownloadFile(fileLocation, "text/plain");
                    ////_objIdsignBal.DownLoadFile(fileLocation, "text/plain");

                    
                    string[] fileExtension = lblUrl.Text.Split('.');
                    string fileLocation = string.Empty;
                    if (fileExtension[1].ToString() == "txt")
                    {
                        fileLocation = Server.MapPath(ConfigurationManager.AppSettings["BillGeneration"].ToString());
                        fileLocation += lblUrl.Text;
                    }
                    else
                    { 
                        fileLocation = Server.MapPath("~" + lblUrl.Text);
                    }

                    if (File.Exists(fileLocation))
                        _ObjCommonMethods.DownloadFile(fileLocation, "pdf/text/plain");
                    break;
            }
        }

        private bool BindOpenMonthAndYear()
        {
            bool IsOpenMonthAvilable = false;
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                xmlResult = _objBillGenerationBal.GetDetails(ReturnType.Single);
                EstimationBE objEstimationBE = _objIdsignBal.DeserializeFromXml<EstimationBE>(xmlResult);
                if (objEstimationBE != null)
                {
                    hfMonth.Value = objEstimationBE.Month.ToString();
                    hfYear.Value = objEstimationBE.Year.ToString();
                    lblMonth.Text = objEstimationBE.MonthName;
                    lblYear.Text = objEstimationBE.Year.ToString();
                    lblMonthID.Text = objEstimationBE.Month.ToString();
                    IsOpenMonthAvilable = true;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

            return IsOpenMonthAvilable;
        }

        public void BindGrid()
        {
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            _objBillGenerationBe.Month = Convert.ToInt32(lblMonthID.Text);
            _objBillGenerationBe.Year = Convert.ToInt32(lblYear.Text);
            _objBillGenerationBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID] == null ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
            DataSet dsData = new DataSet();
            dsData = _objBillGenerationBal.DownloadBillingFileForCustomer(_objBillGenerationBe);

            if (dsData.Tables[0].Rows.Count > 0)
            {
                grdDownloadBillingFile.DataSource = dsData.Tables[0];
                grdDownloadBillingFile.DataBind();
            }
            else
            {
                grdDownloadBillingFile.DataSource = new DataTable(); ;
                grdDownloadBillingFile.DataBind();
            }

        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
    }
}