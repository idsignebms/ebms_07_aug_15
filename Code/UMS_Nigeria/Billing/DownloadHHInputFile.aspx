﻿<%@ Page Title="Download HH Input File" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    AutoEventWireup="true" CodeBehind="DownloadHHInputFile.aspx.cs" Theme="Green"
    Inherits="UMS_Nigeria.Billing.DownloadHHInputFile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdateProgress ID="UpdateProgress" runat="server">
                <ProgressTemplate>
                    <center>
                        <div id="loading-div" class="pbloading">
                            <div id="title-loading" class="pbtitle">
                                BEDC</div>
                            <center>
                                <img src="../images/loading.GIF" class="pbimg"></center>
                            <p class="pbtxt">
                                <asp:Label ID="lblLoading" runat="server" Text="<%$ Resources:Resource, LOADING_TEXT%>"></asp:Label></p>
                        </div>
                    </center>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
                PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
            <asp:UpdatePanel ID="upEstimationSetting" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <%--File generation step one Starts--%>
                    <div id="divStepOne" runat="server" class="consumer_feild">
                        <div class="text_total">
                            <div class="text-heading">
                                <asp:Literal ID="litAddCycle" runat="server" Text="<%$ Resources:Resource, DWNLD_HH_INP_FILE%>"></asp:Literal>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="dot-line">
                        </div>
                        <div class="inner-box">
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, BILLING_MONTH%>"></asp:Literal><span
                                            class="span_star">*</span>
                                    </label>
                                    <br />
                                    <asp:DropDownList ID="ddlYear" onchange="DropDownlistOnChangelbl(this,'spanYear',' Year')"
                                        runat="server" CssClass="text-box" Style="width: 80px;">
                                        <asp:ListItem Value="" Text="Year"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="ddlMonth" onchange="DropDownlistOnChangelbl(this,'spanMonth',' Month')"
                                        runat="server" CssClass="text-box" Style="width: 100px;">
                                        <asp:ListItem Text="Month" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                    <div class="clear space">
                                    </div>
                                    <span id="spanYear" class="span_color"></span><span id="spanMonth" style="margin-left: 90px;"
                                        class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="LiteralCycleFeeder" runat="server" Text="<%$ Resources:Resource,CYCLE%>"></asp:Literal><span
                                            class="span_star">*</span>
                                    </label>
                                    <br />
                                    <asp:DropDownList ID="ddlCycleList" CssClass="text-box" runat="server" onchange="DropDownlistOnChangelbl(this,'spanCyclesFeeders',' BookGroup/Feeder')">
                                        <asp:ListItem Text="--Select BookGroup--" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                    <div class="clear space">
                                    </div>
                                    <span id="spanCyclesFeeders" class="span_color"></span>
                                    <br />
                                    <asp:Button ID="btnStepOne" Text="<%$ Resources:Resource, SEARCH%>" ToolTip="Search"
                                        CssClass="box_s" runat="server" OnClientClick="return ValidateStepOne();" OnClick="btnStepOne_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div id="divInfo" runat="server" class="grid_tb" visible="false" align="center" style="overflow-x: auto;">
                            <asp:GridView ID="grdHHInputFileList" runat="server" AutoGenerateColumns="False"
                                HeaderStyle-CssClass="grid_tb_hd" OnRowCommand="grdHHInputFileList_RowCommand">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    There is no data.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblSrno" runat="server" Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, CYCLE%>" runat="server" HeaderStyle-Width="3%"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblCycleName" runat="server" Text='<%#Eval("CycleName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, MONTH%>" runat="server" HeaderStyle-Width="3%"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%#Eval("Year") %>'></asp:Label>&nbsp;&nbsp;
                                            <asp:Label ID="lblMonthName" runat="server" Text='<%#Eval("MonthName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, DOWNLOAD%>" runat="server"
                                        HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:UpdatePanel runat="server">
                                                <%--Download button isn't available for postback triggers--%>
                                                <ContentTemplate>
                                                    <asp:LinkButton ID="lnkDownload" runat="server" Text='<%$ Resources:Resource, DOWNLOAD %>'
                                                        CommandName="download" CommandArgument='<%#Eval("FileName") %>'></asp:LinkButton>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="lnkDownload" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                    <%--End--%>
                    <asp:ModalPopupExtender ID="mpeGenMsg" runat="server" PopupControlID="pnlGenMsg"
                        TargetControlID="hfTargetContId" BackgroundCssClass="modalBackground" CancelControlID="btnGenMsgCancelContId">
                    </asp:ModalPopupExtender>
                    <asp:HiddenField ID="hfTargetContId" runat="server" />
                    <asp:Panel ID="pnlGenMsg" runat="server" CssClass="modalPopup" Style="display: none;">
                        <div class="popheader popheaderlblred">
                            <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnGenMsgCancelContId" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:HiddenField ID="hfPageSize" runat="server" Value="0" />
                    <asp:HiddenField ID="hfPageNo" runat="server" />
                    <asp:HiddenField ID="hfLastPage" runat="server" />
                    <asp:HiddenField ID="hfTotalRecords" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".rnkland").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/DownloadHHInputFile.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
