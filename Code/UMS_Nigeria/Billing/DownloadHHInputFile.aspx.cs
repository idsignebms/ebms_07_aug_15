﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for Estimation Setting
                     
 Developer        : ID077-NEERAJ KANOJIYA
 Creation Date    : 14-Aug-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.IO;

namespace UMS_Nigeria.Billing
{
    public partial class DownloadHHInputFile : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBAL = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        BillingMonthOpenBAL objBillingMonthOpenBAL = new BillingMonthOpenBAL();
        BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
        MastersBAL _ObjMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        string Key = UMS_Resource.BILLING_MONTH_OPEN;
        CustomerDetailsBe objCustomerDetailsBe = new CustomerDetailsBe();
        CustomerDetailsBAL objCustomerDetailsBAL = new CustomerDetailsBAL();
        CustomerDetailsListBe objCustomerDetailsListBe = new CustomerDetailsListBe();
        SpotBillingBE objSpotBillingBE = new SpotBillingBE();
        SpotBillingListBE objSpotBillingListBE = new SpotBillingListBE();


        private int YearId
        {
            get { return string.IsNullOrEmpty(ddlYear.SelectedValue) ? 0 : Convert.ToInt32(ddlYear.SelectedValue); }
        }

        private int MonthId
        {
            get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? 0 : Convert.ToInt32(ddlMonth.SelectedValue); }
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                if (!IsPostBack)
                {
                    BindYears();
                    BindMonths();
                    BindCyclesOrFeeders();
                }
            }
            else
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void btnStepOne_Click(object sender, EventArgs e)
        {
            GetHHInputFilesList();
        }

        protected void grdHHInputFileList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "download")
            {
                string CompletefileName = HttpContext.Current.Server.MapPath(ConfigurationSettings.AppSettings["SpotBilling"]) + e.CommandArgument;
                _objCommonMethods.DownloadFile(CompletefileName, "application/txt");
            }
        }

        protected void lnkDownload_Click(object sender, EventArgs e)
        {

        }

        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void BindYears()
        {
            try
            {

                XmlDocument xmlResult = new XmlDocument();
                xmlResult = _objBillGenerationBal.GetDetails(ReturnType.Multiple);
                BillGenerationListBe objBillsListBE = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                _objiDsignBAL.FillDropDownList(_objiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlYear, "Year", "Year", "Year", true);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindMonths()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                _objBillGenerationBe.Year = this.YearId;
                xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.Get);
                BillGenerationListBe objBillsListBE = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                _objiDsignBAL.FillDropDownList(_objiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlMonth, "MonthName", "Month", "Month", true);

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindCyclesOrFeeders()
        {
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                _objCommonMethods.BindCyclesByBUSUSC(ddlCycleList, Session[UMS_Resource.SESSION_USER_BUID].ToString(), string.Empty, string.Empty, true, UMS_Resource.DROPDOWN_SELECT, false);
            else
                _objCommonMethods.BindNOCyclesList(ddlCycleList, true);
        }

        private void GetHHInputFilesList()
        {
            objSpotBillingBE.Year = Convert.ToInt32(ddlYear.SelectedItem.Value);
            objSpotBillingBE.Month = Convert.ToInt32(ddlMonth.SelectedItem.Value);
            objSpotBillingBE.BookCycleId = ddlCycleList.SelectedItem.Value;
            xml = _ObjMastersBAL.GetHHInputFilesListBAL(_objiDsignBAL.DoSerialize<SpotBillingBE>(objSpotBillingBE));
            objSpotBillingListBE = _objiDsignBAL.DeserializeFromXml<SpotBillingListBE>(xml);
            if (objSpotBillingListBE.items.Count > 0)
            {
                divInfo.Visible = true;
                grdHHInputFileList.DataSource = objSpotBillingListBE.items;
                grdHHInputFileList.DataBind();
            }
            else
            {
                divInfo.Visible = false;
                lblgridalertmsg.Text = Resource.NO_FILE_FOUND;
                mpeGenMsg.Show();
            }
        }

        #endregion

    }
}