﻿<%@ Page Title=":: Estimation Settings ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="EstimateSettings.aspx.cs" Inherits="UMS_Nigeria.Billing.EstimateSettings" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            <asp:Label ID="lblLoading" runat="server" Text="<%$ Resources:Resource, LOADING_TEXT%>"></asp:Label></p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upEstimationSetting" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div id="divInput" runat="server" class="consumer_feild">
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litSearchCustomer" runat="server" Text="<%$ Resources:Resource, SELECT_FOR_ESTIMATION%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div style="width: 37%; float: left; margin-left: 10px;">
                        <div>
                            <div class="consume_name p_left108">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, BILLING_MONTH%>"></asp:Literal></div>
                            <div class="consume_input">
                            </div>
                            <div class="consume_input fltl">
                            </div>
                        </div>
                        <div class="consume_input c_left" style="width: 200px; font-weight: bold; padding-top: 5px;
                            font-size: 14px;">
                            <asp:Label ID="lblYear" runat="server"></asp:Label>
                            <asp:Label ID="lblMonth" runat="server"></asp:Label>
                            <asp:Label ID="lblNoOpenMonth" runat="server"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div id="divRules" runat="server" visible="false">
                        <div class="consume_name" style="width: 164px; font-weight: bold; padding-left: 6px;">
                            <asp:Literal ID="Literal6" runat="server" Text="Bill will generate for "></asp:Literal>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="consume_name p_left108">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, BILLING_RULE%>"></asp:Literal><span>*</span>
                        </div>
                        <div class="consume_input consume_radio" style="font-size: 12px;">
                            <asp:RadioButtonList RepeatDirection="Horizontal" onchange="BillingRules()" Width="300px"
                                ID="rblReadingMode" runat="server">
                                <asp:ListItem Text="As per settings" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Average Reading" Value="2"></asp:ListItem>
                            </asp:RadioButtonList>
                            <span id="spanBillSendingMode" class="spancolor"></span>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name p_left108">
                            <asp:Literal ID="ltrlServiceUnits" runat="server" Text="<%$ Resources:Resource,SERVICE_UNITS%>"></asp:Literal><span>*</span>
                        </div>
                        <div class="consume_input">
                            <asp:DropDownList ID="ddlServiceUnits" runat="server" OnSelectedIndexChanged="ddlServiceUnits_SelectedIndexChanged"
                                AutoPostBack="true">
                                <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                            <span id="spanServiceUnit" class="spancolor"></span>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        </div>
                        <div class="consume_name p_left108">
                            <asp:Literal ID="ltrlServiceCenters" runat="server" Text="Service Centers" Visible="false"></asp:Literal>
                        </div>
                        <div class="consumer_total p_left238">
                            <asp:Repeater ID="rptrSCBtns" runat="server" OnItemCommand="rptrSCBtns_ItemCommand">
                                <ItemTemplate>
                                    <asp:Button ID="btnSC" runat="server" CommandArgument='<%#Eval("ServiceCenterId") %>'
                                        Text='<%#Eval("ServiceCenterName") %>' OnClientClick="return PageaValidation();" />
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                    <div style="float: left;" id="divInformMessages" runat="server">
                        <div class="estimatemessage p_left108" style="width: 285px;">
                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource,EST_TOTAL_READ_KWH%>"></asp:Literal>
                        </div>
                        <div class="estimatemessage" style="width: 120px;">
                            <asp:Label ID="lblTotalReadConsumption" runat="server" ForeColor="Green" Font-Bold="true"
                                Text="0"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="estimatemessage p_left108" style="width: 285px;">
                            <asp:Literal ID="Literal3" runat="server" Text="Total Non Read Customers KWh :"></asp:Literal>
                        </div>
                        <div class="estimatemessage" style="width: 120px;">
                            <asp:Label ID="lblTOtalNonReadCustomersKWH" runat="server" ForeColor="Green" Font-Bold="true"
                                Text="0"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div id="divTotalUploadedCustomersKWH" runat="server">
                            <div class="estimatemessage p_left108" style="width: 285px;">
                                <asp:Literal ID="Literal7" runat="server" Text="Total Uploaded Direct Customers KWh :"></asp:Literal>
                            </div>
                            <div class="estimatemessage" style="width: 120px;">
                                <asp:Label ID="lblTotalUploadedCustomersKWH" runat="server" ForeColor="Green" Font-Bold="true"
                                    Text="0"></asp:Label>
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                        <div id="divTotalNonUploadedCustomersKWH" runat="server">
                            <div class="estimatemessage p_left108" style="width: 285px;">
                                <asp:Literal ID="Literal8" runat="server" Text="Total Non-Uploaded Customers KWh :"></asp:Literal>
                            </div>
                            <div class="estimatemessage" style="width: 120px;">
                                <asp:Label ID="lblTotalNonUploadedCustomersKWH" runat="server" ForeColor="Green"
                                    Font-Bold="true" Text="0"></asp:Label>
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                        <div id="divTotalDirectConsumption" runat="server">
                            <div class="estimatemessage p_left108" style="width: 285px;">
                                <asp:Literal ID="ltrlDirectKWH" runat="server" Text="<%$ Resources:Resource,EST_TOTAL_DIRECT_KWH%>"></asp:Literal>
                            </div>
                            <div class="estimatemessage" style="width: 120px;">
                                <asp:Label ID="lblTotalDirectConsumption" runat="server" ForeColor="Green" Font-Bold="true"
                                    Text="0"></asp:Label>
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                        <%--<div id="divZeroMin" runat="server">
                            <div class="clear pad_5">
                            </div>
                            <div class="estimatemessage p_left108" style="width: 285px;">
                                <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource,EST_ZERO_MIN_CUSTOMERS%>"></asp:Literal>
                            </div>
                            <div class="estimatemessage" style="width: 150px;">
                                <asp:Label ID="lblZeroMinCustomers" runat="server" ForeColor="Green" Font-Bold="true"
                                    Text="0"></asp:Label>
                            </div>
                        </div>--%>
                    </div>
                    <div style="float: left; width: 25%;" id="divCapacity" runat="server" visible="false">
                        <div class="estimatemessage p_left108" style="width: 210px;">
                            <asp:Literal ID="Literal10" runat="server" Text="Capacity in KVA :"></asp:Literal>
                        </div>
                        <div class="estimatemessage" style="width: 90px;">
                            <asp:Label ID="lblCapacityKVA" runat="server" ForeColor="Green" Font-Bold="true"
                                Text="0"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="estimatemessage p_left108" style="width: 210px;">
                            <asp:Literal ID="Literal9" runat="server" Text="Prepaid Meter Consumption :"></asp:Literal>
                        </div>
                        <div class="estimatemessage" style="width: 90px;">
                            <asp:Label ID="lblPrepaidConsumption" runat="server" ForeColor="Green" Font-Bold="true"
                                Text="0"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="estimatemessage p_left108" style="width: 210px;">
                            <asp:Literal ID="Literal11" runat="server" Text="Credit Meter Consumption :"></asp:Literal>
                        </div>
                        <div class="estimatemessage" style="width: 90px;">
                            <asp:Label ID="lblCreditConsumption" runat="server" ForeColor="Green" Font-Bold="true"
                                Text="0"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div id="divnotation" runat="server" class="estimatStp" visible="false">
                    <span style="float: left; font-weight: bold; color: #6E6E6E;">Total Read Customers Count</span>
                    <span style="float: right; font-weight: bold; color: #585858;">Total Direct Customers
                        Count</span>
                    <br />
                    <br />
                    <span style="float: left; font-weight: bold; color: #5882FA;">Total Read Customers Usage</span>
                    <span style="float: right; color: #0101DF; font-weight: bold; padding-left: 10px;">Total
                        Direct Customers Usage</span>
                </div>
                <div class="clear">
                </div>
                <div class="grid printgrid" id="divPrint" runat="server" align="center">
                    <h3 class="gridhead" align="left" style="width: 600px; font-size: 14px;">
                        <asp:Literal ID="litBuList" runat="server" Text="<%$ Resources:Resource, ESTIMATION_SET%>"></asp:Literal>
                        <asp:Label ID="lblScName" Text="" runat="server" Style="color: Green;" />
                    </h3>
                    <div class="gridhead width500 ">
                        <asp:Label ID="lblMonthlyUsage" runat="server"></asp:Label>
                    </div>
                    <div class="gridhead width126" style="text-align: right; padding-right: 6px; float: right;">
                        <asp:LinkButton ID="lnkDeleteEstimation" runat="server" Text='<%$ Resources: Resource, DEL_DATA %>'
                            Visible="false"></asp:LinkButton>
                    </div>
                    <div class="clear pad_5">
                    </div>
                    <asp:GridView ID="grdEstimateSettings" runat="server" AutoGenerateColumns="false"
                        EnableViewState="true" OnRowDataBound="grdEstimateSettings_RowDataBound">
                        <EmptyDataTemplate>
                            There is no Book Group
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Book Group">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnCycle" runat="server" Text='<%#Eval("Cycle")%>' Enabled="false"
                                        CommandArgument='<%#Eval("CycleId")%>' CssClass="telHyd" Style="margin-top: 0px;"></asp:LinkButton>
                                    <table class="totalReadtelyd" style="margin-top: 15px;">
                                        <tr>
                                            <td style="padding-left: 10px; padding-bottom: 7px; text-align: left">
                                                <asp:Label ID="lblReadCustomersName" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td style="padding-left: 10px; padding-bottom: 7px; text-align: left">
                                                <asp:Label ID="lblReadCount" runat="server" Text='<%#Eval("TotalReadCustomersHavingReadings")%>'
                                                    Style="float: right; margin-right: 10px;"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="trNonRead" runat="server">
                                            <td style="padding-left: 10px; padding-bottom: 7px; text-align: left">
                                                Total Non-Read Customers
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:Label ID="lblNonReadCount" runat="server" Text='<%#Eval("TotalNonReadCustomersCount")%>'
                                                    Style="float: right; margin-right: 10px;"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="trDirect" runat="server">
                                            <td style="padding-left: 10px; padding-bottom: 7px; text-align: left">
                                                <asp:Label ID="lblDirectCustomersName" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDirectCount" runat="server" Text='<%#Eval("TotalDirectCustomers")%>'
                                                    Style="float: right; margin-right: 10px;"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="trDirectUploaded" runat="server">
                                            <td style="padding-left: 10px; padding-bottom: 7px; text-align: left">
                                                Total Uploaded Direct Customers
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td style="padding-left: 10px; padding-bottom: 7px; text-align: left">
                                                <asp:Label ID="lblTotalUplodedCustomersCount" runat="server" Text='<%#Eval("TotalUplodedCustomersCount")%>'
                                                    Style="float: right; margin-right: 10px;"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="trNonDirect" runat="server">
                                            <td style="padding-left: 10px; padding-bottom: 7px; text-align: left">
                                                Total Non Uploaded Direct Customers
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td style="padding-left: 10px; padding-bottom: 7px; text-align: left">
                                                <asp:Label ID="lblTotalNonDirectCount" runat="server" Text='<%#Eval("TotalNonUploadedCustomersCount")%>'
                                                    Style="float: right; margin-right: 10px;"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-left: 10px; padding-bottom: 7px; text-align: left">
                                                In-Active Customers
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td style="padding-left: 10px; padding-bottom: 7px; text-align: left">
                                                <asp:Label ID="lblInActiveCount" runat="server" Text='<%#Eval("TotalInActiveCustomersCount")%>'
                                                    Style="float: right; margin-right: 10px;"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cluster Type">
                                <ItemTemplate>
                                    <asp:Label ID="lblCluster" runat="server" Text='<%#Eval("CategoryName")%>'></asp:Label>
                                    <asp:Label ID="lblClusterCategoryId" runat="server" Visible="false" Text='<%#Eval("ClusterCategoryId")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div class="fltl" style="color: Red; font-size: large; margin-left: 300px; margin-top: 10px;">
                        <asp:Label ID="lblAvrageAlertMsg" runat="server" Text="Are you sure, do you want to save Average Readings as estimations to selected service center ?"
                            Font-Bold="true"></asp:Label>
                    </div>
                    <div class="clear">
                    </div>
                    <div id="divSave" runat="server" style="margin-left: 600px;">
                        <asp:Button ID="btnSave" Text="<%$ Resources:Resource, SAVE%>" CssClass="box_s finesh"
                            ValidationGroup="Total" runat="server" OnClientClick="return PageaValidation();"
                            ToolTip='<%$ Resources:Resource, ESTIMATION_EXITS %>' OnClick="btnSave_Click" />
                    </div>
                    <div class="clear pad_10">
                    </div>
                </div>
                <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="pnlGenMsg"
                    TargetControlID="hfGenMsgTargetCntrl" BackgroundCssClass="modalBackground" CancelControlID="btnOkay">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hfGenMsgTargetCntrl" runat="server" />
                <asp:Panel ID="pnlGenMsg" runat="server" CssClass="modalPopup" Style="display: none;">
                    <div class="popheader popheaderlblred">
                        <asp:Label ID="lblGenMsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btnOkay" runat="server" Text="Ok" CssClass="ok_btn" Visible="false" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="hfRecognize" runat="server" />
                <asp:HiddenField ID="hfOpenStatusId" runat="server" />
                <asp:HiddenField ID="hfFocus" runat="server" />
                <asp:HiddenField ID="hfIsAllExists" runat="server" Value="1" />
                <asp:HiddenField ID="hfBillingRule" runat="server" Value="0" />
                <asp:HiddenField ID="hfTotaValue" runat="server" Value="0" />
                <asp:HiddenField ID="hfTotalCustomers" runat="server" Value="0" />
                <asp:HiddenField ID="hfTotalEnergyReaded" runat="server" Value="0" />
                <asp:HiddenField ID="hfMonth" runat="server" Value="0" />
                <asp:HiddenField ID="hfYear" runat="server" Value="0" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".rnkland").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../Javascript/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/EstimateSettings.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }

        function BillingRules() {
            var divgv = document.getElementById("<%=divPrint.ClientID%>");
            var divInformMessages = document.getElementById("<%=divInformMessages.ClientID%>");
            var divnotation = document.getElementById("<%=divnotation.ClientID%>");
            var divCapacity = document.getElementById("<%=divCapacity.ClientID%>");
            divInformMessages.style.display = "none"
            divnotation.style.display = "none"
            divgv.style.display = "none"
            divCapacity.style.display = "none"
        }

        $(document).ready(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
