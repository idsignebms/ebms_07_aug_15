﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using Resources;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using System.Xml;
using System.Data;
using System.Web.Services;
using System.Drawing;
using System.Web.UI.HtmlControls;

namespace UMS_Nigeria.Billing
{
    public partial class EstimateSettings : System.Web.UI.Page
    {
        #region Members

        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        ConsumerBal _ObjConsumerBal = new ConsumerBal();
        BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
        BillCalculatorBAL _ObjBillCalculatorBAL = new BillCalculatorBAL();
        MastersBAL _ObjMastersBAL = new MastersBAL();

        string Key = "Estimate Settings";

        #endregion

        #region Properties

        public string ServiceUnit
        {
            get { return string.IsNullOrEmpty(ddlServiceUnits.SelectedValue) ? string.Empty : ddlServiceUnits.SelectedValue; }
            set { ddlServiceUnits.SelectedValue = value.ToString(); }
        }

        private int YearId
        {
            get { return string.IsNullOrEmpty(hfYear.Value) ? 0 : Convert.ToInt32(hfYear.Value); }
        }

        private int MonthId
        {
            get { return string.IsNullOrEmpty(hfMonth.Value) ? 0 : Convert.ToInt32(hfMonth.Value); }
        }

        private int BillingRule
        {
            get { return string.IsNullOrEmpty(rblReadingMode.SelectedValue) ? 0 : Convert.ToInt32(rblReadingMode.SelectedValue); }
        }

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.CssClass = lblMessage.Text = string.Empty;
            if (!IsPostBack)
            {
                if (Session[UMS_Resource.SESSION_LOGINID] != null)
                {
                    divInformMessages.Visible = divCapacity.Visible = divPrint.Visible = false;
                    divnotation.Visible = divPrint.Visible = false;
                    BindOpenMonthAndYear();
                    AddTariffColumnsToGrid();
                    BindServiceUnits();
                }
            }
        }

        private void BindServiceUnits()
        {
            //rptrSCBtns.DataBind();

            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
            {
                string bu_Id = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                _ObjCommonMethods.BindServiceUnits(ddlServiceUnits, bu_Id, true);
                ddlServiceUnits.Enabled = true;

                if (ddlServiceUnits.Items.Count == 1)
                {
                    lblGenMsg.Text = "Service unit is not available for selected business unit.";
                    mpegridalert.Show();
                }
            }
        }

        protected void ddlServiceUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divInformMessages.Visible = divCapacity.Visible = divPrint.Visible = false;
                divnotation.Visible = divPrint.Visible = false;
                if (ddlServiceUnits.SelectedIndex > 0)
                {
                    ConsumerBe _ObjConsumerBe = new ConsumerBe();
                    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    {
                        _ObjConsumerBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                    }
                    _ObjConsumerBe.SU_ID = ServiceUnit;
                    ConsumerListBe objConsumerListBe = _ObjiDsignBAL.DeserializeFromXml<ConsumerListBe>(_ObjConsumerBal.Get(_ObjConsumerBe, ReturnType.Single));
                    if (objConsumerListBe.items.Count > 0)
                    {
                        ltrlServiceCenters.Visible = true;
                        rptrSCBtns.Visible = true;
                        rptrSCBtns.DataSource = objConsumerListBe.items;
                    }
                    else
                        rptrSCBtns.DataSource = new DataTable();

                    rptrSCBtns.DataBind();
                }
                else
                {
                    lblGenMsg.Text = "Service center not available for selected business unit and service unit.";
                    mpegridalert.Show();
                    rptrSCBtns.DataSource = new DataTable();
                    rptrSCBtns.DataBind();
                    ltrlServiceCenters.Visible = false;
                }
            }
            catch (Exception ex)
            {
                _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        public BillCalculatorListBE GetTariffList()
        {
            BillCalculatorBE _ObjBillCalculatorBe = new BillCalculatorBE();
            _ObjBillCalculatorBe.ClassId = 0;
            BillCalculatorListBE objBillCalculatorListBe = _ObjiDsignBAL.DeserializeFromXml<BillCalculatorListBE>(_ObjBillCalculatorBAL.GetCustomerSubTypes(_ObjBillCalculatorBe, ReturnType.Multiple));
            return objBillCalculatorListBe;
        }

        public void GetCycleList(string SCID, string btnScName)
        {
            lblScName.Text = " - " + btnScName;
            divInformMessages.Visible = divCapacity.Visible = divnotation.Visible = true;
            btnSave.Visible = divCapacity.Visible = false;

            EstimationBE _objEstimationBE = new EstimationBE();
            EstimationLilstBE _objEstimationLilstBE = new EstimationLilstBE();
            EstimationDetailsLilstBE _objEstimationDetailsLilstBE = new EstimationDetailsLilstBE();
            XmlDocument xmlresult = new XmlDocument();
            _objEstimationBE.ServiceCenterId = SCID;
            _objEstimationBE.Month = this.MonthId;
            _objEstimationBE.Year = this.YearId;
            _objEstimationBE.BillingRule = this.BillingRule;
            xmlresult = _ObjMastersBAL.GetEstimatedDetails(_objEstimationBE, ReturnType.List);
            _objEstimationLilstBE = _ObjiDsignBAL.DeserializeFromXml<EstimationLilstBE>(xmlresult);
            _objEstimationDetailsLilstBE = _ObjiDsignBAL.DeserializeFromXml<EstimationDetailsLilstBE>(xmlresult);
            lblTotalDirectConsumption.Text = "0";

            if (_objEstimationLilstBE.items.Count > 0)
            {
                divPrint.Visible = btnSave.Visible = true;
                AddCyclesToDataTable(_objEstimationLilstBE.items);
            }

            if (_objEstimationDetailsLilstBE.items.Count > 0)
            {
                divCapacity.Visible = true;
                lblCapacityKVA.Text = Math.Round(_objEstimationDetailsLilstBE.items[0].CapacityInKVA).ToString();
                lblCreditConsumption.Text = Math.Round(_objEstimationDetailsLilstBE.items[0].SUCreditConsumption).ToString();
                lblPrepaidConsumption.Text = Math.Round(_objEstimationDetailsLilstBE.items[0].SUPPMConsumption).ToString(); ;
            }
            else
            {
                lblCapacityKVA.Text = lblCreditConsumption.Text = lblPrepaidConsumption.Text = "--";
            }

            var result = _objEstimationLilstBE.items.AsEnumerable()
                          .GroupBy(s => new { s.CycleId, s.Cycle, s.ClusterCategoryId, s.CategoryName })
                          .Select(g =>
                            new
                            {
                                CycleId = g.Key.CycleId,
                                Cycle = g.Key.Cycle,
                                ClusterCategoryId = g.Key.ClusterCategoryId,
                                CategoryName = g.Key.CategoryName,
                                TotalReadCustomersHavingReadings = g.Sum(x => x.TotalReadCustomersHavingReadings),
                                TotalNonReadCustomersCount = g.Sum(x => x.TotalNonReadCustomersCount),
                                TotalUplodedCustomersCount = g.Sum(x => x.TotalUplodedCustomersCount),
                                TotalNonUploadedCustomersCount = g.Sum(x => x.TotalNonUploadedCustomersCount),
                                TotalInActiveCustomersCount = g.Sum(x => x.TotalInActiveCustomersCount),
                                TotalDirectCustomers = g.Sum(x => x.TotalDirectCustomers),

                                TotalUsageForReadCustomersHavingReadings = g.Sum(x => x.TotalUsageForReadCustomersHavingReadings),
                                TotalNonReadCustomersUsage = g.Sum(x => x.TotalNonReadCustomersUsage),
                                TotalUsageForUploadedCustomers = g.Sum(x => x.TotalUsageForUploadedCustomers),
                                TotalNonUploadedCustomersUsage = g.Sum(x => x.TotalNonUploadedCustomersUsage)
                            }).ToList();

            lblTotalReadConsumption.Text = result.GroupBy(s => new { s.CycleId }).Select(g => g.Sum(x => x.TotalUsageForReadCustomersHavingReadings)).Sum().ToString();
            lblTOtalNonReadCustomersKWH.Text = result.GroupBy(s => new { s.CycleId }).Select(g => g.Sum(x => x.TotalNonReadCustomersUsage)).Sum().ToString();
            lblTotalUploadedCustomersKWH.Text = result.GroupBy(s => new { s.CycleId }).Select(g => g.Sum(x => x.TotalUsageForUploadedCustomers)).Sum().ToString();
            lblTotalNonUploadedCustomersKWH.Text = result.GroupBy(s => new { s.CycleId }).Select(g => g.Sum(x => x.TotalNonUploadedCustomersUsage)).Sum().ToString();

            int TotalDirectCustomers = result.Select(g => g.TotalUplodedCustomersCount + g.TotalNonUploadedCustomersCount).Sum();

            if (TotalDirectCustomers == 0)
            {
                btnSave.Enabled = false;
                btnSave.Attributes.Add("style", "cursor:auto;");
            }
            else
            {
                btnSave.Enabled = true;
                btnSave.Attributes.Add("style", "cursor:pointer;");
            }

            if (this.BillingRule == 1)
            {
                ltrlDirectKWH.Text = Resource.EST_TOTAL_DIRECT_KWH;
                //divZeroMin.Visible = 
                lblAvrageAlertMsg.Visible = false;
                divTotalUploadedCustomersKWH.Visible = divTotalNonUploadedCustomersKWH.Visible = false;
                divTotalDirectConsumption.Visible = true;
            }
            else
            {
                divTotalUploadedCustomersKWH.Visible = divTotalNonUploadedCustomersKWH.Visible = true;
                divTotalDirectConsumption.Visible = false;
                //var Totallist = (from x in _objEstimationLilstBE.items.AsEnumerable()
                //                 group x by new
                //                 {
                //                     x.CycleName,
                //                     x.CycleId,
                //                     x.Cycle,
                //                     x.Total
                //                 } into gcs
                //                 select new { TotalSum = gcs.Key.Total }).ToList();
                //lblZeroMinCustomers.Text = (from s in Totallist select s.TotalSum).Sum().ToString();

                //divZeroMin.Visible = lblAvrageAlertMsg.Visible = true;
            }
            DataSet ds = new DataSet();
            ds = _ObjiDsignBAL.ConvertListToDataSet<EstimationBE>(_objEstimationLilstBE.items);
            Session["EstimationSettingsDT"] = ds.Tables[0];

            grdEstimateSettings.DataSource = result;
            grdEstimateSettings.DataBind();

            MergeRows(grdEstimateSettings);

        }

        public void MergeRows(GridView gridView)
        {
            try
            {
                for (int rowIndex = gridView.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow row = gridView.Rows[rowIndex];
                    GridViewRow previousRow = gridView.Rows[rowIndex + 1];

                    LinkButton lbtnCycle = (LinkButton)row.FindControl("lbtnCycle");
                    LinkButton lblPreviouslbtnCycle = (LinkButton)previousRow.FindControl("lbtnCycle");

                    for (int i = 0; i < row.Cells.Count; i++)
                    {
                        if (i == 0)
                        {
                            if (lbtnCycle.Text == lblPreviouslbtnCycle.Text)
                            {
                                row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                                previousRow.Cells[i].Visible = false;
                            }
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnAvgConfirmationOk_Click(object sender, EventArgs e)
        {
            SaveAverageSettingDetails();
        }

        private void SaveAverageSettingDetails()
        {
            DataTable dt = (DataTable)Session[UMS_Resource.SESSION_ESTIMATIONSETTINGS_DT];
            foreach (DataRow dr in dt.Rows)
            {
                string cycleId = dr["CycleId"].ToString();
                int ClusterCategoryId = Convert.ToInt32(dr["ClusterCategoryId"]);//added by karthik
                DataSet ds = new DataSet();
                DataTable finalDt = new DataTable();
                finalDt.Columns.Add("TariffName", typeof(string));
                finalDt.Columns.Add("Energey", typeof(string));
                foreach (DataColumn dc in dt.Columns)
                {
                    if (dc.Caption != "CycleId" && dc.Caption != "CycleName" && dc.Caption != "ClusterCategoryId")
                    {
                        DataRow newrow = finalDt.NewRow();
                        newrow["TariffName"] = dc.Caption;
                        newrow["Energey"] = "0";
                        finalDt.Rows.Add(newrow);
                    }
                }
                ds.Tables.Add(finalDt);

                if (finalDt.Rows.Count > 0)
                {
                    EstimationBE _ObjEstimationBE = new EstimationBE();
                    _ObjEstimationBE.Cycle = cycleId;
                    _ObjEstimationBE.ClusterCategoryId = ClusterCategoryId;//added by karthik
                    _ObjEstimationBE.Year = this.YearId;
                    _ObjEstimationBE.Month = this.MonthId;
                    _ObjEstimationBE.BillingRule = this.BillingRule;
                    _ObjEstimationBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    XmlDocument xmlRes = _ObjMastersBAL.InsertEstimationBAL(_ObjEstimationBE, finalDt);
                }
                ds.Tables.Remove(finalDt);
            }
            HideStepTwo();

            Message(Resource.ESTIMATION_SAVE_SUCCESSFULLY, UMS_Resource.MESSAGETYPE_SUCCESS);
        }

        protected void rptrSCBtns_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            ClearDTRows();
            string btnScName = ((Button)e.Item.FindControl("btnSC")).Text;
            GetCycleList(e.CommandArgument.ToString(), btnScName);
        }

        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            HideStepTwo();
        }

        protected void grdEstimateSettings_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                try
                {
                    LinkButton litCycleId = (LinkButton)e.Row.FindControl("lbtnCycle");
                    Label lblClusterCategoryId = (Label)e.Row.FindControl("lblClusterCategoryId");
                    Label lblReadCustomersName = (Label)e.Row.FindControl("lblReadCustomersName");
                    Label lblDirectCustomersName = (Label)e.Row.FindControl("lblDirectCustomersName");
                    Label lblReadCount = (Label)e.Row.FindControl("lblReadCount");
                    Label lblNonReadCount = (Label)e.Row.FindControl("lblNonReadCount");
                    Label lblDirectCount = (Label)e.Row.FindControl("lblDirectCount");
                    Label lblTotalNonDirectCount = (Label)e.Row.FindControl("lblTotalNonDirectCount");
                    Label lblTotalUplodedCustomersCount = (Label)e.Row.FindControl("lblTotalUplodedCustomersCount");
                    Label lblInActiveCount = (Label)e.Row.FindControl("lblInActiveCount");
                    HtmlTableRow trNonRead = (HtmlTableRow)e.Row.FindControl("trNonRead");
                    HtmlTableRow trNonDirect = (HtmlTableRow)e.Row.FindControl("trNonDirect");
                    HtmlTableRow trDirect = (HtmlTableRow)e.Row.FindControl("trDirect");
                    HtmlTableRow trDirectUploaded = (HtmlTableRow)e.Row.FindControl("trDirectUploaded");

                    //EstimationBE _objEstimationBE = new EstimationBE();
                    //EstimationLilstBE _objEstimationLilstBE = new EstimationLilstBE();
                    //EstimationDetailsLilstBE _objEstimationDetailsLilstBE = new EstimationDetailsLilstBE();
                    //_objEstimationBE.Year = this.YearId;
                    //_objEstimationBE.Month = this.MonthId;
                    //_objEstimationBE.Cycle = litCycleId.CommandArgument;
                    //XmlDocument xmlResult = _ObjMastersBAL.GetEstimatedDetails(_objEstimationBE, ReturnType.Get);

                    //_objEstimationLilstBE = _ObjiDsignBAL.DeserializeFromXml<EstimationLilstBE>(xmlResult);
                    //_objEstimationDetailsLilstBE = _ObjiDsignBAL.DeserializeFromXml<EstimationDetailsLilstBE>(xmlResult);

                    DataTable dt = new DataTable();
                    dt = (DataTable)Session["EstimationSettingsDT"];

                    var result = (from x in dt.AsEnumerable()
                                  where x.Field<string>("CycleId") == litCycleId.CommandArgument
                                  select x).ToList();

                    if (this.BillingRule == 1)
                    {
                        lblReadCustomersName.Text = "Total Read Customers Having Readings";
                        lblDirectCustomersName.Text = "Total Uploaded Direct Customers";
                        lblReadCount.Text = result.Sum(x => x.Field<int>("TotalReadCustomersHavingReadings")).ToString();
                        lblNonReadCount.Text = result.Sum(x => x.Field<int>("TotalNonReadCustomersCount")).ToString();
                        //lblDirectCount.Text = result.Sum(x => x.Field<int>("TotalUplodedCustomersCount")).ToString();
                        //lblTotalNonDirectCount.Text = result.Sum(x => x.Field<int>("TotalNonUploadedCustomersCount")).ToString();
                        lblInActiveCount.Text = result.Sum(x => x.Field<int>("TotalInActiveCustomersCount")).ToString();
                        trNonDirect.Visible = trDirect.Visible = trDirectUploaded.Visible = false;
                    }
                    else
                    {
                        lblReadCustomersName.Text = "Total Read Customers";
                        lblDirectCustomersName.Text = "Total Direct Customers";
                        lblReadCount.Text = result.Sum(x => x.Field<int>("TotalReadCustomersCount")).ToString();
                        lblDirectCount.Text = result.Sum(x => x.Field<int>("TotalDirectCustomers")).ToString();
                        lblTotalUplodedCustomersCount.Text = result.Sum(x => x.Field<int>("TotalUplodedCustomersCount")).ToString();
                        lblTotalNonDirectCount.Text = result.Sum(x => x.Field<int>("TotalNonUploadedCustomersCount")).ToString();
                        lblInActiveCount.Text = result.Sum(x => x.Field<int>("TotalInActiveCustomersCount")).ToString();
                        trNonRead.Visible = false;
                    }

                    for (int cell = 2; cell < e.Row.Cells.Count; cell++)
                    {
                        int Energyvalue = 0;
                        Int64 totalEnergy = 0;
                        int readCustomers = 0;
                        int directcustomers = 0;
                        Int64 totalreadcustomersusage = 0;
                        Int64 totaldirectcustomersusage = 0;

                        if (dt.Rows.Count > 0)
                        {
                            IEnumerable<EstimationBE> resultcustomers = (from x in dt.AsEnumerable()
                                                                         where x.Field<string>("ClassName") == grdEstimateSettings.HeaderRow.Cells[cell].Text
                                                                         && x.Field<int>("ClusterCategoryId") == Convert.ToInt32(lblClusterCategoryId.Text)
                                                                         && x.Field<string>("CycleId") == litCycleId.CommandArgument
                                                                         select new EstimationBE
                                                                         {
                                                                             TotalReadCustomersHavingReadings = x.Field<int>("TotalReadCustomersHavingReadings"),
                                                                             TotalNonReadCustomersCount = x.Field<int>("TotalNonReadCustomersCount"),
                                                                             TotalUplodedCustomersCount = x.Field<int>("TotalUplodedCustomersCount"),
                                                                             TotalNonUploadedCustomersCount = x.Field<int>("TotalNonUploadedCustomersCount"),
                                                                             TotalDirectCustomers = x.Field<int>("TotalDirectCustomers"),
                                                                             TotalReadCustomersCount = x.Field<int>("TotalReadCustomersCount"),
                                                                             TotalUsageForReadCustomersHavingReadings = x.Field<Int64>("TotalUsageForReadCustomersHavingReadings"),
                                                                             TotalNonReadCustomersUsage = x.Field<Int64>("TotalNonReadCustomersUsage"),
                                                                             TotalUsageForUploadedCustomers = x.Field<Int64>("TotalUsageForUploadedCustomers"),
                                                                             TotalNonUploadedCustomersUsage = x.Field<Int64>("TotalNonUploadedCustomersUsage"),
                                                                             TotalUsageForReadCustomers = x.Field<Int64>("TotalUsageForReadCustomers"),
                                                                             TotalDirectCustomersUsage = x.Field<Int64>("TotalDirectCustomersUsage")
                                                                         }
                                               );
                            if (resultcustomers.Count() > 0)
                            {
                                try
                                {
                                    readCustomers = resultcustomers.SingleOrDefault().TotalReadCustomersCount;
                                    directcustomers = resultcustomers.SingleOrDefault().TotalDirectCustomers;
                                    totalreadcustomersusage = resultcustomers.SingleOrDefault().TotalUsageForReadCustomers;
                                    totaldirectcustomersusage = resultcustomers.SingleOrDefault().TotalDirectCustomersUsage;
                                }
                                catch
                                {
                                }
                            }
                        }
                        if (dt.Rows.Count > 0)
                        {
                            IEnumerable<EstimationBE> resultvalue = (from x in dt.AsEnumerable()
                                                                     where x.Field<string>("ClassName") == grdEstimateSettings.HeaderRow.Cells[cell].Text
                                                                        && x.Field<int>("ClusterCategoryId") == Convert.ToInt32(lblClusterCategoryId.Text)
                                                                        && x.Field<string>("CycleId") == litCycleId.CommandArgument
                                                                     select new EstimationBE
                                                                     {
                                                                         EnergyToCalculate = x.Field<int>("EnergyToCalculate")
                                                                     });
                            if (resultvalue.Count() > 0)
                            {
                                Energyvalue = resultvalue.SingleOrDefault().EnergyToCalculate;
                            }
                        }
                        if (Energyvalue >= 0 && directcustomers >= 0)
                        {
                            totalEnergy = Energyvalue * directcustomers;

                            if (Convert.ToDecimal(Energyvalue) > 0)
                                UpdateDTwithExisting(grdEstimateSettings.HeaderRow.Cells[cell].Text, litCycleId.CommandArgument, Convert.ToInt32(lblClusterCategoryId.Text), Energyvalue);
                        }
                        if (this.BillingRule == 2)
                        {
                            totalEnergy = totaldirectcustomersusage;
                        }

                        UpdatePanel updatePanel = new UpdatePanel() { ID = "updatePaneltxt" + cell, UpdateMode = UpdatePanelUpdateMode.Conditional };
                        Label lblCustomers = new Label() { Text = directcustomers.ToString(), ID = "lblCustomers" + cell.ToString() };
                        Label lbltotalEnergy = new Label() { Text = totalEnergy.ToString(), ID = "lblTotalEnergy" + cell.ToString(), CssClass = "totalestusageCSS" };

                        Label lblReadCustomers = new Label() { Text = readCustomers.ToString(), ID = "lblReadCustomers" + cell.ToString() };
                        Label lbltotalReadEnergy = new Label() { Text = totalreadcustomersusage.ToString(), ID = "lblTotalReadEnergy" + cell.ToString() };
                        lblReadCustomers.Attributes.Add("Style", "font-weight:bold;float: left;color: #6E6E6E;");
                        lblCustomers.Attributes.Add("Style", "font-weight:bold;float: right;color: #585858;");
                        lbltotalReadEnergy.Attributes.Add("Style", "font-weight:bold;float: left;color: #5882FA;");
                        lbltotalEnergy.Attributes.Add("Style", "font-weight:bold;float: right;color: #0101DF;");

                        updatePanel.ContentTemplateContainer.Controls.Add(lblReadCustomers);
                        updatePanel.ContentTemplateContainer.Controls.Add(lblCustomers);
                        updatePanel.ContentTemplateContainer.Controls.Add(new LiteralControl("<br />"));
                        lblTotalDirectConsumption.Text = (Convert.ToDecimal(lblTotalDirectConsumption.Text) + totalEnergy).ToString();

                        if (this.BillingRule == 1)
                        {
                            ///// this is for Creating dynamic textbox call javascript funciton with page methods ---->START
                            TextBox txt = new TextBox() { Text = Energyvalue.ToString(), ID = "txtEnergyToCalculate" + cell.ToString() };

                            if (directcustomers == 0)
                            {
                                txt.BackColor = Color.LightSteelBlue;
                                txt.Enabled = false;
                            }
                            else
                            {
                                if (Energyvalue == 0)
                                {
                                    txt.BackColor = Color.Red;
                                    txt.ForeColor = Color.White;
                                }
                                else
                                {
                                    txt.BackColor = Color.Green;
                                    txt.ForeColor = Color.White;
                                }
                            }
                            txt.Attributes.Add("Style", "width: 30px;text-align:right;");
                            txt.Attributes.Add("onkeypress", "javascript:return isNumberKey(event,this);");
                            txt.Attributes.Add("onkeyup", "javascript:CaluculateEnergy(this,'" + directcustomers + "','" + lbltotalEnergy + "');");
                            txt.Attributes.Add("onchange", "javascript:EnergyChange(this,'" + grdEstimateSettings.HeaderRow.Cells[cell].Text + "','" + litCycleId.CommandArgument + "','" + lblClusterCategoryId.Text + "');");
                            updatePanel.ContentTemplateContainer.Controls.Add(txt);
                            updatePanel.ContentTemplateContainer.Controls.Add(new LiteralControl("<br />"));
                        }

                        updatePanel.ContentTemplateContainer.Controls.Add(lbltotalReadEnergy);
                        updatePanel.ContentTemplateContainer.Controls.Add(lbltotalEnergy);
                        e.Row.Cells[cell].Controls.Add(updatePanel);
                        ///// this is for Creating dynamic textbox call javascript funciton with page methods ---->END
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        private void UpdateDTwithExisting(string tariffName, string cycleId, int ClusterCategoryId, int energy)
        {
            DataTable dt = new DataTable();
            dt = (DataTable)(HttpContext.Current.Session[UMS_Resource.SESSION_ESTIMATIONSETTINGS_DT]);
            var rowsToUpdate = dt.AsEnumerable().Where(r => r.Field<string>("CycleId") == cycleId)
                                                .Where(r => r.Field<int>("ClusterCategoryId") == ClusterCategoryId);

            foreach (var row in rowsToUpdate)
            {
                row.SetField(tariffName, energy);
            }

            HttpContext.Current.Session[UMS_Resource.SESSION_ESTIMATIONSETTINGS_DT] = dt;
        }

        protected void txt_TextChanged(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.BillingRule == 1)
                SaveEstimateSettingDetails();
            else
                SaveAverageSettingDetails();

            lblTotalDirectConsumption.Text = lblTotalReadConsumption.Text = string.Empty;

            rblReadingMode.Items.FindByValue("2").Selected = false;
            rblReadingMode.Items.FindByValue("1").Selected = true;
            ddlServiceUnits.SelectedIndex = 0;
            ddlServiceUnits_SelectedIndexChanged(ddlServiceUnits, new EventArgs());
        }

        private void SaveEstimateSettingDetails()
        {
            DataTable dt = (DataTable)Session[UMS_Resource.SESSION_ESTIMATIONSETTINGS_DT];
            foreach (DataRow dr in dt.Rows)
            {
                string cycleId = dr["CycleId"].ToString();
                int ClusterCategoryId = Convert.ToInt32(dr["ClusterCategoryId"]);
                DataSet ds = new DataSet();
                DataTable finalDt = new DataTable();
                finalDt.Columns.Add("TariffName", typeof(string));
                finalDt.Columns.Add("Energey", typeof(string));
                foreach (DataColumn dc in dt.Columns)
                {
                    if (dc.Caption != "CycleId" && dc.Caption != "CycleName" && dc.Caption != "ClusterCategoryId")
                    {
                        decimal energyValue = string.IsNullOrEmpty(dr[dc.Caption].ToString()) ? 0 : Convert.ToDecimal(dr[dc.Caption]);
                        if (energyValue != 0)
                        {
                            DataRow newrow = finalDt.NewRow();
                            newrow["TariffName"] = dc.Caption;
                            newrow["Energey"] = energyValue.ToString();
                            finalDt.Rows.Add(newrow);
                        }
                    }
                }
                ds.Tables.Add(finalDt);

                EstimationBE _ObjEstimationBE = new EstimationBE();
                _ObjEstimationBE.Cycle = cycleId;
                _ObjEstimationBE.ClusterCategoryId = ClusterCategoryId;
                _ObjEstimationBE.Year = this.YearId;
                _ObjEstimationBE.Month = this.MonthId;
                _ObjEstimationBE.BillingRule = this.BillingRule;
                _ObjEstimationBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                XmlDocument xmlRes = _ObjMastersBAL.InsertEstimationBAL(_ObjEstimationBE, finalDt);
                _ObjEstimationBE = _ObjiDsignBAL.DeserializeFromXml<EstimationBE>(xmlRes);

                ds.Tables.Remove(finalDt);
            }
            HideStepTwo();

            Message(Resource.ESTIMATION_SAVE_SUCCESSFULLY, UMS_Resource.MESSAGETYPE_SUCCESS);
        }

        #endregion

        #region Methods

        private void BindOpenMonthAndYear()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                xmlResult = _objBillGenerationBal.GetDetails(ReturnType.Single);
                EstimationBE objEstimationBE = _ObjiDsignBAL.DeserializeFromXml<EstimationBE>(xmlResult);
                if (objEstimationBE != null)
                {
                    divRules.Visible = true;
                    hfMonth.Value = objEstimationBE.Month.ToString();
                    hfYear.Value = objEstimationBE.Year.ToString();
                    lblMonth.Text = objEstimationBE.MonthName;
                    lblYear.Text = objEstimationBE.Year.ToString();

                }
                else
                {
                    divRules.Visible = false;
                    lblNoOpenMonth.Text = "There is no Bill Open Month";
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void HideStepTwo()
        {
            divPrint.Visible = false;
            ClearDTRows();
            grdEstimateSettings.DataBind();
        }

        private void ClearDTRows()
        {
            DataTable dt = (DataTable)Session[UMS_Resource.SESSION_ESTIMATIONSETTINGS_DT];

            if (dt != null) dt.Rows.Clear();
            Session[UMS_Resource.SESSION_ESTIMATIONSETTINGS_DT] = dt;
        }

        public void AddTariffColumnsToGrid()
        {
            BillCalculatorListBE objBillCalculatorListBe = GetTariffList();
            TemplateField objTemplateField = null;

            //TextBox txtEstimation = new TextBox();
            //int counter = 0;

            DataTable dt = new DataTable();
            dt = CreateDataTable(dt, objBillCalculatorListBe.items);
            Session[UMS_Resource.SESSION_ESTIMATIONSETTINGS_DT] = dt;

            foreach (BillCalculatorBE list in objBillCalculatorListBe.items)
            {
                objTemplateField = new TemplateField();
                objTemplateField.HeaderText = list.CustomerSubTypeNames;
                grdEstimateSettings.Columns.Add(objTemplateField);
                //counter++;
            }
        }

        private DataTable CreateDataTable(DataTable dt, List<BillCalculatorBE> list)
        {
            dt.Columns.Add("CycleName", typeof(string));
            dt.Columns.Add("CycleId", typeof(string));
            dt.Columns.Add("ClusterCategoryId", typeof(int));// added by karthik
            foreach (BillCalculatorBE item in list)
            {
                dt.Columns.Add(item.CustomerSubTypeNames, typeof(string));
            }
            return dt;
        }

        private void AddCyclesToDataTable(List<EstimationBE> list)
        {
            DataTable dt = new DataTable();
            dt = (DataTable)Session[UMS_Resource.SESSION_ESTIMATIONSETTINGS_DT];
            foreach (EstimationBE item in list)
            {
                DataRow dr = dt.NewRow();
                dr["CycleName"] = item.CycleName;
                dr["CycleId"] = item.CycleId;
                dr["ClusterCategoryId"] = item.ClusterCategoryId;// added by karthik
                dt.Rows.Add(dr);
            }
            Session[UMS_Resource.SESSION_ESTIMATIONSETTINGS_DT] = dt;
        }

        #endregion

        [WebMethod(EnableSession = true)]
        public static string SaveEnergyDetails(string energy, string tariffName, string cycleId, string ClusterCategoryId)
        {

            DataTable dt = new DataTable();
            dt = (DataTable)(HttpContext.Current.Session[UMS_Resource.SESSION_ESTIMATIONSETTINGS_DT]);

            var rowsToUpdate = dt.AsEnumerable().Where(r => r.Field<string>("CycleId") == cycleId)
                                                 .Where(r => r.Field<int>("ClusterCategoryId") == Convert.ToInt32(ClusterCategoryId));//new by karthik

            foreach (var row in rowsToUpdate)
            {
                row.SetField(tariffName, energy);
            }

            HttpContext.Current.Session[UMS_Resource.SESSION_ESTIMATIONSETTINGS_DT] = dt;
            return string.Empty;
        }
    }

    class CreateTextBox : ITemplate
    {
        string CntrlID = "";

        int Width = 20;

        public CreateTextBox(string ControlId, int width)
        {
            CntrlID = ControlId;
            Width = width;
        }

        public void InstantiateIn(System.Web.UI.Control container)
        {
            TextBox txtBox = new TextBox();
            txtBox.ID = CntrlID;
            txtBox.Width = Width;
            txtBox.EnableViewState = true;
            container.Controls.Add(txtBox);
        }
    }
}