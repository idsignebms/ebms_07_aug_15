﻿<%@ Page Title="::Estimated Customers Bill::" Language="C#" MasterPageFile="~/MasterPages/EDMUMS.Master"
    AutoEventWireup="true" CodeBehind="EstimatedCustomerBill.aspx.cs" Inherits="UMS_Nigeria.Billing.EstimatedCustomerBill" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <%--<asp:UpdatePanel ID="upSearchCustomer" runat="server">
            <ContentTemplate>--%>
    <div class="edmcontainer">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:Panel ID="pnlMessage" runat="server">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <h3>
            <asp:Literal ID="litSearchCustomer" runat="server" Text="<%$ Resources:Resource, EST_CUSTOMERBILL%>"></asp:Literal>
        </h3>
        <div class="consumer_feild">
            <div class="consume_name">
                <asp:Literal ID="litTariff" runat="server" Text="<%$ Resources:Resource, BILLING_MONTH%>"></asp:Literal>
                <span>*</span></div>
            <div class="consume_input c_left">
                <asp:DropDownList ID="ddlMonth" AutoPostBack="true" runat="server">
                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                </asp:DropDownList>
                <span id="spanTariff" class="spancolor"></span>
            </div>
            <div class="consume_input">
                <asp:DropDownList ID="ddlYear" AutoPostBack="true" runat="server">
                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="error">
                <span id="span1" class="spancolor"></span>
            </div>
            <div class="clear pad_10">
            </div>
            <div class="consume_name">
                <asp:Literal ID="litDocumentNo" runat="server" Text="<%$ Resources:Resource, TARRIF%>"></asp:Literal>
            </div>
            <div style="margin-left: 26px;" class="fltl c_left">
                <asp:CheckBox runat="server" ID="cbAll" Text="All" onclick="return CheckBoxListSelect(this);" />
            </div>
            <br />
            <div class="fltl c_left" style="margin-left: 154px; margin-top: -13px; clear: both;">
                <asp:CheckBoxList Style="width: 300px;" ID="cblTariff" runat="server" RepeatDirection="Horizontal"
                    RepeatColumns="4" onclick="return Uncheck();" OnSelectedIndexChanged="cblTariff_SelectedIndexChanged"
                    AutoPostBack="true">
                </asp:CheckBoxList>
            </div>
            <div class="clear pad_5">
            </div>
            <div class="consume_name">
                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, BILLING_RULE%>"></asp:Literal>
            </div>
            <div class="fltl c_left" style="margin-left: 24px;">
                <asp:RadioButtonList runat="server" ID="rdblbillingrule">
                    <asp:ListItem Text="<%$ Resources:Resource, DIRECT_CUSTOMERRULE%>" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, AVG_REDINGRULE%>" Value="2"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="clear pad_10">
                <div style="margin-left: 144px;">
                    <asp:Button ID="btnSave" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                        CssClass="calculate_but" runat="server" OnClick="btnSave_Click" />
                </div>
            </div>
        </div>
        <div class="clear pad_10">
            <asp:DataList CssClass="grid" Style="margin-left: 130px;" ID="rptrTraffiD" runat="server"
                OnItemDataBound="rptrTraffiD_ItemDataBound" RepeatDirection="Horizontal" RepeatColumns="4">
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lbthTraffId" Text='<%# Eval("TariffName") %>'
                        CommandArgument='<%# Eval("TariffId") %>'></asp:LinkButton>
                    <div style="margin: 0 20px 20px 0;">
                        <asp:GridView ID="GvBusinessUnits" runat="server" AutoGenerateColumns="false">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no Data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:BoundField DataField="BU_ID" HeaderText="<%$ Resources:Resource, BU_ID%>" />
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, Count%>" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-VerticalAlign="Middle">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblCount" Text='<%#Eval("Count") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </ItemTemplate>
            </asp:DataList>
            <asp:DataList CssClass="grid" Style="padding-right: 20px;" ID="rptrTraffiD1" runat="server"
                OnItemDataBound="rptrTraffiD_ItemDataBound" RepeatDirection="Horizontal" RepeatColumns="4">
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lbthTraffId" Text='<%# Eval("TariffName") %>'
                        CommandArgument='<%# Eval("TariffId") %>'></asp:LinkButton>
                    <asp:GridView ID="GvBusinessUnits" Style="margin-right: 20px;" runat="server" AutoGenerateColumns="false">
                        <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                        <EmptyDataTemplate>
                            There is no Data.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:BoundField DataField="BU_ID" HeaderText="<%$ Resources:Resource, BU_ID%>" />
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, Count%>" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-VerticalAlign="Middle">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblCount" Text='<%#Eval("Count") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="consumer_feild">
            <div class="consume_name">
                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, BILLING_MONTH%>"></asp:Literal>
                <span>*</span></div>
            <div class="consume_input c_left">
                <asp:DropDownList ID="ddlMonth1" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlMonth1_SelectedIndexChanged">
                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                </asp:DropDownList>
                <span id="span2" class="spancolor"></span>
            </div>
            <div class="consume_input">
                <asp:DropDownList ID="ddlYear1" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlYear1_SelectedIndexChanged">
                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="error">
                <span id="span3" class="spancolor"></span>
            </div>
        </div>
        <div class="clear pad_5">
        </div>
        <div class="grid clear">
            <asp:GridView ID="GvTariffBusinessUnits" runat="server" AutoGenerateColumns="false"
                OnRowCommand="GvTariffBusinessUnits_RowCommand">
                <Columns>
                    <asp:TemplateField HeaderText="<%$ Resources:Resource, TARIFF%>" ItemStyle-HorizontalAlign="Center"
                        ItemStyle-VerticalAlign="Middle">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="lblClassName" Text='<%#Eval("ClassName") %>' CommandName='<%#Eval("ClassId") %>'
                                CommandArgument='<%#Eval("EstCustomerRuleId") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="<%$ Resources:Resource, COUNT%>" ItemStyle-HorizontalAlign="Center"
                        ItemStyle-VerticalAlign="Middle">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblCount" Text='<%#Eval("Count") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="<%$ Resources:Resource, BILLING_RULE%>" ItemStyle-HorizontalAlign="Center"
                        ItemStyle-VerticalAlign="Middle">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="lblBillingRule" Text='<%#Eval("RuleName") %>'
                                CommandName='<%#Eval("RuleID") %>' />
                            <asp:RadioButtonList runat="server" ID="rdblbillingrule" Visible="false">
                                <asp:ListItem Text="<%$ Resources:Resource, DIRECT_CUSTOMERRULE%>" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, AVG_REDINGRULE%>" Value="2"></asp:ListItem>
                            </asp:RadioButtonList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                        <ItemTemplate>
                            <asp:LinkButton ID="lkbtnEdit" runat="server" CommandName="EditCharge" OnClientClick="return confirm('Are you sure to Edit this Charges?')">
                                                 <img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                            <asp:LinkButton ID="lkbtnUpdate" Visible="false" runat="server" ToolTip="Update"
                                CommandName="UpdateCharge"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                            <asp:LinkButton ID="lkbtnCancel" Visible="false" runat="server" ToolTip="Cancel"
                                CommandName="CancelCharge"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <%-- </ContentTemplate>
        </asp:UpdatePanel>--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/PageValidations/EstimatedCustomerBill.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
