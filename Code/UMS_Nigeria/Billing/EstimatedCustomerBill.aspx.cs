﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddServiceUnits
                     
 Developer        : id067-Naresh T
 Creation Date    : 14-Apr-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using iDSignHelper;
using Resources;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using System.Drawing;
using System.Xml;

namespace UMS_Nigeria.Billing
{
    public partial class EstimatedCustomerBill : System.Web.UI.Page
    {
        #region Members

        iDsignBAL objIdsignBal = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        TariffManagementBal objTariffBal = new TariffManagementBal();
        ConsumerBal objConsumerBal = new ConsumerBal();
        BillCalculatorBAL objBillCalculatorBal = new BillCalculatorBAL();
        String Key = "EstimatedCustomer";
        public int PageNum;

        #endregion

        #region Properties

        private int Month
        {
            get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? 0 : Convert.ToInt32(ddlMonth.SelectedValue); }
        }

        private string MonthName
        {
            get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? string.Empty : ddlMonth.SelectedItem.Text; }
        }

        private int Year
        {
            get { return string.IsNullOrEmpty(ddlYear.SelectedValue) ? 0 : Convert.ToInt32(ddlYear.SelectedValue); }
        }

        private string YearName
        {
            get { return string.IsNullOrEmpty(ddlYear.SelectedValue) ? string.Empty : ddlYear.SelectedItem.Text; }
        }


        private int Month1
        {
            get { return string.IsNullOrEmpty(ddlMonth1.SelectedValue) ? 0 : Convert.ToInt32(ddlMonth1.SelectedValue); }
        }

        private string MonthName1
        {
            get { return string.IsNullOrEmpty(ddlMonth1.SelectedValue) ? string.Empty : ddlMonth1.SelectedItem.Text; }
        }

        private int Year1
        {
            get { return string.IsNullOrEmpty(ddlYear1.SelectedValue) ? 0 : Convert.ToInt32(ddlYear1.SelectedValue); }
        }

        private string YearName1
        {
            get { return string.IsNullOrEmpty(ddlYear1.SelectedValue) ? string.Empty : ddlYear1.SelectedItem.Text; }
        }

        private string TraffId
        {
            get
            {
                return string.Join(",", (from item in cblTariff.Items.Cast<ListItem>()
                                         where item.Selected
                                         select (item.Value)));
            }
        }

        private string TraffName
        {
            get
            {
                return string.Join(",", (from item in cblTariff.Items.Cast<ListItem>()
                                         where item.Selected
                                         select (item.Text)));
            }
        }

        private int RuleID
        {
            get { return string.IsNullOrEmpty(rdblbillingrule.SelectedValue) ? 0 : Convert.ToInt32(rdblbillingrule.SelectedValue); }
        }
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.CssClass = lblMessage.Text = string.Empty;
            if (!IsPostBack)
            {
                string path = string.Empty;
                path = objCommonMethods.GetPagePath(this.Request.Url);
                if (objCommonMethods.IsAccess(path))
                {
                    BindYears();
                    BindMonths();
                    BindTariffSubTypes();
                }
                else
                {
                    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                }
            }

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ConsumerBe objConsumerBe = new ConsumerBe();
                ConsumerBal objConsumerBal = new ConsumerBal();
                objConsumerBe.Year = this.Year;
                objConsumerBe.Month = this.Month;
                objConsumerBe.RuleID = this.RuleID;
                objConsumerBe.Tariff = this.TraffId;
                objConsumerBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.InsertDetails(objConsumerBe, Statement.Change));
                if (objConsumerBe.IsSuccess == true)
                {

                }
                else
                {

                }
                BinTraffifUnits();
            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblTariff_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindTariffBUID();
            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }


        protected void rptrTraffiD_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    LinkButton lbthTraffId = (LinkButton)e.Item.FindControl("lbthTraffId");
                    GridView GvBusinessUnits = (GridView)e.Item.FindControl("GvBusinessUnits");
                    BindbusinessUnits(Convert.ToInt32(lbthTraffId.CommandArgument), GvBusinessUnits);
                }
            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }


        protected void GvTariffBusinessUnits_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                ConsumerListBe objConsumerListBe = new ConsumerListBe();
                ConsumerBe objConsumerBe = new ConsumerBe();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                LinkButton lblBillingRule = (LinkButton)row.FindControl("lblBillingRule");
                RadioButtonList rdblbillingrule = (RadioButtonList)row.FindControl("rdblbillingrule");
                LinkButton lblClassName = (LinkButton)row.FindControl("lblClassName");


                switch (e.CommandName.ToUpper())
                {
                    case "EDITCHARGE":
                        lblBillingRule.Visible = false;
                        rdblbillingrule.Visible = true;
                        rdblbillingrule.SelectedValue = lblBillingRule.CommandName;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnEdit").Visible = false;
                        break;
                    case "CANCELCHARGE":
                        row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                        lblBillingRule.Visible = true;
                        rdblbillingrule.Visible = false;
                        break;
                    case "UPDATECHARGE":
                        //objConsumerBe.TariffId = Convert.ToInt32(rdblbillingrule.CommandName);
                        objConsumerBe.RuleID = Convert.ToInt32(rdblbillingrule.SelectedValue);
                        objConsumerBe.EstCustomerRuleId = Convert.ToInt32(lblClassName.CommandArgument);
                        objConsumerBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.InsertDetails(objConsumerBe, Statement.Check));
                        rdblbillingrule.Visible = false;
                        lblBillingRule.Visible = true;
                        row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                        if (objConsumerBe.Count > 0)
                        {
                            BinTraffifUnits();
                            Message(UMS_Resource.CHARGE_UPDATED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        else
                        {

                            Message(UMS_Resource.CHARGE_UPDATED_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        break;
                    case "DELETECHARGE":
                        //objTariffManagementBE.ActiveStatusId = Convert.ToBoolean(0);
                        //objTariffManagementBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objTariffManagementBE.AdditionalChargeID = Convert.ToInt32(lblAdditionalChargeId.Text);
                        //xml = objTariffManagementBal.Insert(objTariffManagementBE, Statement.Delete);
                        //objTariffManagementBE = objIdsignBal.DeserializeFromXml<TariffManagementBE>(xml);

                        //if (Convert.ToBoolean(objTariffManagementBE.IsSuccess))
                        //{
                        //    BindGridTariffFixedChargesList();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message(UMS_Resource.CHARGE_DELETE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        //}
                        //else
                        //    Message(UMS_Resource.CHARGE_DELETE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }



        protected void ddlMonth1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.Year1 == 0)
            {

            }
            else
            {
                ConsumerBe objConsumerBe = new ConsumerBe();
                ConsumerListBe objConsumerListBe = new ConsumerListBe();
                objConsumerBe.Month = this.Month1;
                objConsumerBe.Year = this.Year1;
                objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.GetDetails(objConsumerBe, ReturnType.Bulk));
                GvTariffBusinessUnits.DataSource = objConsumerListBe.items;
                GvTariffBusinessUnits.DataBind();
            }

        }

        protected void ddlYear1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.Month1 == 0)
            {

            }
            else
            {
                ConsumerBe objConsumerBe = new ConsumerBe();
                ConsumerListBe objConsumerListBe = new ConsumerListBe();
                objConsumerBe.Month = this.Month1;
                objConsumerBe.Year = this.Year1;
                objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.GetDetails(objConsumerBe, ReturnType.Bulk));
                GvTariffBusinessUnits.DataSource = objConsumerListBe.items;
                GvTariffBusinessUnits.DataBind();
            }
        }

        #endregion

        #region Methods

        private void BindbusinessUnits(int TariffId, GridView GvBusinessUnits)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            ConsumerListBe objConsumerListBe = new ConsumerListBe();
            objConsumerBe.TariffId = Convert.ToInt32(TariffId);
            XmlDocument xml = new XmlDocument();
            xml = objConsumerBal.Insert(objConsumerBe, Statement.Check);
            objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(xml);
            GvBusinessUnits.DataSource = objConsumerListBe.items;
            GvBusinessUnits.DataBind();
        }
        public void BindTariffSubTypes()
        {
            DataSet dataset = new DataSet();
            BillCalculatorBE objBillCalculatorBe = new BillCalculatorBE();
            BillCalculatorListBE objBillCalculatorListBe = objIdsignBal.DeserializeFromXml<BillCalculatorListBE>(objBillCalculatorBal.GetCustomerSubTypes(objBillCalculatorBe, ReturnType.Multiple));
            dataset = objIdsignBal.ConvertListToDataSet<BillCalculatorBE>(objBillCalculatorListBe.items);
            objIdsignBal.FillCheckBoxList(dataset.Tables[0], cblTariff, "CustomerSubTypeNames", "ClassId");

        }


        private void BindYears()
        {
            try
            {
                int CurrentYear = DateTime.Now.Year;
                int FromYear = Convert.ToInt32(GlobalConstants.TARIFF_ENTRY_NO_OF_LAST_YEARS);
                for (int i = FromYear; i < CurrentYear; i++)
                {
                    ListItem item = new ListItem();
                    item.Text = item.Value = i.ToString();
                    ddlYear.Items.Add(item);

                }
                for (int i = 0; i <= Convert.ToInt32(GlobalConstants.TARIFF_ENTRY_NO_OF_PRESENT_AND_FUTURE_YEARS); i++)
                {
                    ListItem item = new ListItem();
                    item.Text = item.Value = (CurrentYear + i).ToString();
                    ddlYear.Items.Add(item);
                    ddlYear1.Items.Add(item);

                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindMonths()
        {
            try
            {
                DataSet dsMonths = new DataSet();
                dsMonths.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["Months"]));
                objIdsignBal.FillDropDownList(dsMonths.Tables[0], ddlMonth, "name", "value", UMS_Resource.DROPDOWN_SELECT, false);
                objIdsignBal.FillDropDownList(dsMonths.Tables[0], ddlMonth1, "name", "value", UMS_Resource.DROPDOWN_SELECT, false);

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindTariffBUID()
        {
            if (this.TraffId != "")
            {
                string[] TariffId = (this.TraffId).Split(',');
                string[] TariffName = (this.TraffName).Split(',');

                DataTable dt = new DataTable();
                dt.Columns.Add("TariffId");
                dt.Columns.Add("TariffName");
                int cnt = TariffId.Length;
                for (int i = 0; i < cnt; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr["TariffId"] = TariffId[i];
                    dr["TariffName"] = TariffName[i];
                    dt.Rows.Add(dr);
                }

                rptrTraffiD.DataSource = dt;
                rptrTraffiD.DataBind();

            }
        }


        private void BinTraffifUnits()
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            ConsumerListBe objConsumerListBe = new ConsumerListBe();
            objConsumerBe.Tariff = this.TraffId;
            objConsumerBe.Month = this.Month;
            objConsumerBe.Year = this.Year;
            objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.GetDetails(objConsumerBe, ReturnType.Bulk));
            GvTariffBusinessUnits.DataSource = objConsumerListBe.items;
            GvTariffBusinessUnits.DataBind();

        }


        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion
    }
}