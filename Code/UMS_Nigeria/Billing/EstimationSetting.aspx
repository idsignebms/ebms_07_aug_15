﻿<%@ Page Title=":: Estimation Settings ::" Language="C#" MasterPageFile="~/MasterPages/EDMUMS.Master"
    AutoEventWireup="true" CodeBehind="EstimationSetting.aspx.cs" Inherits="UMS_Nigeria.Billing.EstimationSetting" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script type="text/javascript">
        function Validate() {
            var ddlYear = document.getElementById('<%=ddlYear.ClientID %>');
            var ddlMonth = document.getElementById('<%=ddlMonth.ClientID %>');
            var ddlCycleList = document.getElementById('<%=ddlCycleList.ClientID %>');

            var IsValid = true;

            if (DropDownlistValidationlbl(ddlYear, document.getElementById("spanYear"), "Year") == false) IsValid = false;
            if (DropDownlistValidationlbl(ddlMonth, document.getElementById("spanMonth"), "Month") == false) IsValid = false;
            if (DropDownlistValidationlbl(ddlCycleList, document.getElementById("spanCyclesFeeders"), "BookGroup/Feeder") == false) IsValid = false;

            if (!IsValid) {
                return false;
            }
        }

        function GridValidation() {
            var mpeAlert = $find('<%= mpegridalert.ClientID %>');
            var lblAlertMsg = document.getElementById('<%= lblgridalertmsg.ClientID %>');
            var gv = document.getElementById('<%=gvEstimatedCustomerList.ClientID %>');
            var hfFocus = document.getElementById('<%=hfFocus.ClientID %>');
            for (var i = 0; i < gv.getElementsByTagName("input").length; i++) {
                if (gv.getElementsByTagName("input").item(i).value == 0 || gv.getElementsByTagName("input").item(i).value == "") {
                    document.getElementById('<%=lblgridalertmsg.ClientID %>').innerHTML = "Enter estimated energy.";
                    mpeAlert.show();
                    hfFocus.value = i;
                    return false;
                }
            }
        }
        function CloseMP() {
            var hfFocus = document.getElementById('<%=hfFocus.ClientID %>');
            var mpeAlert = $find('<%= mpegridalert.ClientID %>');
            var gv = document.getElementById('<%=gvEstimatedCustomerList.ClientID %>');
            gv.getElementsByTagName("input").item(hfFocus.value).focus();

            //mpeAlert.close();
        }



        function BillingRules() {
            var rblReadingMode = document.getElementById('<%= rblReadingMode.ClientID %>');
            var rblReadingModeList = rblReadingMode.getElementsByTagName("input");
            if (rblReadingModeList[1].checked) {
                document.getElementById('<%= divPrint.ClientID %>').style.display = "none";
            }
            else {
                document.getElementById('<%= divPrint.ClientID %>').style.display = "none";
            }
        }

        //        function Previous() {

        //            document.getElementById('<%= divResult.ClientID %>').style.display = "none";
        ////            alert("Working");
        //            document.getElementById('<%= divInput.ClientID %>').style.display = "block";
        //        }
    </script>
    <script type="text/javascript">
        function pageLoad() {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        };
    </script>
    <script type="text/javascript">
        function GetSelectedRow(lnk) {
            var totalOfETC = 0;
            var totalReadedlbl = 0;
            var TotalReaded = 0;
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var TotalCust = row.cells[2].getElementsByTagName("span")[0].innerHTML;
            var TotalReading = row.cells[3].getElementsByTagName("input")[0].value;
            row.cells[4].getElementsByTagName("span")[0].innerHTML = TotalCust * TotalReading;
            //            totalOfETC = parseInt(document.getElementById('<%= hfTotaValue.ClientID %>').value) + parseInt(TotalReading);
            var lblEnergyToCal = $(".EnergyToCalCSS");
            var lblCustomerList = $(".CustomerCss");
            for (var i = 0; i < lblEnergyToCal.length; i++) {
                if (lblEnergyToCal[i].value > 0) {
                    totalOfETC = parseInt(totalOfETC) + parseInt(lblEnergyToCal[i].value);
                    TotalReaded = (parseInt(TotalReaded) + parseInt(lblEnergyToCal[i].value) * lblCustomerList[i].innerHTML);
                }
            }

            var TotalLbl = $(".totalOfECT");
            TotalLbl[0].innerHTML = totalOfETC + " (kWh)";

            
            var TotalReadedFooter = $(".totalEnergyReaded");
            TotalReadedFooter[0].innerHTML = TotalReaded + " (kWh)";
        }
        function DoFocus(fld) {
        }
        function GetSelectedRow1(lnk) {
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var AvgReading = row.cells[4].getElementsByTagName("input")[0].value;
            var TotalReading = row.cells[2].getElementsByTagName("span")[0].innerHTML;
            row.cells[3].getElementsByTagName("span")[0].innerHTML = Math.round(parseFloat(AvgReading / TotalReading).toFixed(2));
        }
    </script>
    <script language="javascript" type="text/javascript">
        function ValidationOfEnergy() {
            var gv = document.getElementById("<%= gvEstimatedCustomerList.ClientID %>");
            for (var rowId = 0; rowId < gv.rows.length; rowId++) {
                var txtbx = gv.rows[rowId].cells[3].children[0];
                alert(txtbx);
            }
            return false;
        }        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <div class="edmcontainer">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            <asp:Label ID="lblLoading" runat="server" Text="<%$ Resources:Resource, LOADING_TEXT%>"></asp:Label></p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upEstimationSetting" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div class="clear">
                    &nbsp;</div>
                <div id="divInput" runat="server" class="consumer_feild">
                    <h3>
                        <asp:Literal ID="litAddCycle" runat="server" Text="<%$ Resources:Resource, SELECT_FOR_ESTIMATION%>"></asp:Literal>
                    </h3>
                    <div class="consume_name p_left108">
                        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, BILLING_MONTH%>"></asp:Literal><span>*</span></div>
                    <div class="consume_input">
                        <asp:DropDownList ID="ddlYear" onchange="DropDownlistOnChangelbl(this,'spanYear',' Year')"
                            AutoPostBack="true" runat="server">
                            <asp:ListItem Value="" Text="Year"></asp:ListItem>
                        </asp:DropDownList>
                        <span id="spanYear" class="spancolor"></span>
                    </div>
                    <div class="consume_input fltl">
                        <asp:DropDownList ID="ddlMonth" onchange="DropDownlistOnChangelbl(this,'spanMonth',' Month')"
                            AutoPostBack="true" runat="server">
                            <asp:ListItem Text="Month" Value=""></asp:ListItem>
                        </asp:DropDownList>
                        <span id="spanMonth" class="spancolor"></span>
                    </div>
                    <div class="clear pad_5">
                    </div>
                    <div class="consume_name p_left108">
                        <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, BILLING_RULE%>"></asp:Literal><span>*</span>
                    </div>
                    <div class="consume_input consume_radio">
                        <asp:RadioButtonList RepeatDirection="Horizontal" onchange="BillingRules()" Width="250px"
                            ID="rblReadingMode" runat="server">
                            <asp:ListItem Text="As per settings" Value="1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Average Reading" Value="2"></asp:ListItem>
                        </asp:RadioButtonList>
                        <span id="spanBillSendingMode" class="spancolor"></span>
                    </div>
                    <div class="clear pad_5">
                    </div>
                    <div class="consume_name width224">
                        <asp:Literal ID="LiteralCycleFeeder" runat="server" Text="<%$ Resources:Resource,CHOOSE_CYCLEFEEDER%>"></asp:Literal><span>*</span>
                    </div>
                    <div class="consume_input">
                        <asp:DropDownList ID="ddlCycleList" runat="server" onchange="DropDownlistOnChangelbl(this,'spanCyclesFeeders',' BookGroup/Feeder')">
                            <asp:ListItem Text="--Select BookGroup--" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        <span id="spanCyclesFeeders" class="spancolor"></span>
                    </div>
                    <div class="clear pad_5">
                    </div>
                    <div class="consumer_total p_left238">
                        <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, NEXT%>" ToolTip="Search"
                            CssClass="calculate_but" OnClientClick="return Validate()" runat="server" OnClick="btnSearch_Click" />
                    </div>
                </div>
                <div id="divResult" runat="server">
                    <div class="consumer_total">
                        <div class="clear">
                        </div>
                    </div>
                    <div class="grid gridbg-pad" id="divPrint" runat="server" visible="false" align="center">
                        <h3 class="gridhead" align="left">
                            <asp:Literal ID="litBuList" runat="server" Text="<%$ Resources:Resource, ESTIMATION_SET%>"></asp:Literal>
                        </h3>
                        <div class="gridhead width500 ">
                            <asp:Label ID="lblMonthlyUsage" runat="server"></asp:Label>
                        </div>
                        <div class="gridhead width126" style="text-align: right; padding-right: 6px; float: right;">
                            <asp:LinkButton ID="lnkDeleteEstimation" runat="server" Text='<%$ Resources: Resource, DEL_DATA %>'
                                OnClick="lnkDeleteEstimation_Click" Visible="false"></asp:LinkButton>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <asp:GridView ID="gvEstimatedCustomerList" runat="server" AutoGenerateColumns="False"
                            ShowFooter="true" AlternatingRowStyle-CssClass="color" OnRowDataBound="gvEstimatedCustomerList_RowDataBound">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                    HeaderStyle-Width="8%" HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Literal ID="litEstimationSettingId" Visible="false" runat="server" Text='<%#Eval("EstimationSettingId")%>'></asp:Literal>
                                        <asp:Label ID="lblSrno" runat="server" Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                                        <asp:Label ID="lblClassID" Visible="false" runat="server" Text='<%#Eval("ClassID")%>'></asp:Label>
                                        <asp:Literal ID="litIsExists" Visible="false" runat="server" Text='<%#Eval("IsExists")%>'></asp:Literal>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, TARIFF%>" ItemStyle-Width="15%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNameClassName" runat="server" Text='<%#Eval("ClassName") %>'></asp:Label>
                                        <asp:Label ID="tblTariffID" runat="server" Text='<%#Eval("ClassID") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, Total_Cst_Read_Non_Read%>"
                                    ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEstimatedCustomers" runat="server" Text='<%#Eval("EstimatedCustomers") %>'
                                            CssClass="CustomerCss"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, ENRGY_TO_CAL%>" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAvgReadingPerCust" runat="server" Visible="false" Text='<%#Eval("AvgReadingPerCust") %>'></asp:Label>
                                        <asp:Label ID="lblEnergyToCalculate" runat="server" Visible="false" Text='<%#Eval("EnergyToCalculate") %>'></asp:Label>
                                        <asp:TextBox ID="txtTotal" runat="server" MaxLength="6" AutoComplete="off" onkeyup="GetSelectedRow(this)"
                                            Text='<%#Eval("EnergyToCalculate") %>' CssClass="EnergyToCalCSS" onfocus="DoFocus(this);"></asp:TextBox>
                                        <asp:Label ID="lblKWH" runat="server" Text="<%$Resources: Resource, KWH %>"></asp:Label>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtTotal"
                                            FilterType="Numbers" ValidChars="/">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spantxtTotal" class="spancolor"></span>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, TOTAL_ENRGY_RED%>" ItemStyle-Width="30%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalAverage" runat="server" Text="0"></asp:Label>
                                        <asp:TextBox ID="txtAvgTotal" runat="server" MaxLength="6" onkeyup="GetSelectedRow1(this)"
                                            Text='<%#Eval("AvgUsage") %>' Visible="false" Enabled="false"></asp:TextBox>
                                        <asp:Label ID="lblKWHEst" runat="server" Text="<%$Resources: Resource, KWH %>"></asp:Label>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtAvgTotal"
                                            FilterType="Numbers" ValidChars="/">
                                        </asp:FilteredTextBoxExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <div id="div1" runat="server" class="consumer_total fltr">
                            <asp:Button ID="btnPrevious" Text="<%$ Resources:Resource, PREVIOUS%>" CssClass="calculate_but"
                                runat="server" OnClientClick="Previous();" OnClick="btnPrevious_Click" />
                        </div>
                        <div id="divSave" runat="server" class="consumer_total fltr">
                            <asp:Button ID="btnSave" Text="<%$ Resources:Resource, SAVE%>" CssClass="calculate_but"
                                ValidationGroup="Total" runat="server" OnClick="btnSave_Click" OnClientClick="return GridValidation();"
                                ToolTip='<%$ Resources:Resource, ESTIMATION_EXITS %>' />
                            <asp:Button ID="btnUpdate" Text="<%$ Resources:Resource, UPDATE%>" CssClass="calculate_but"
                                OnClick="btnUpdate_Click" ValidationGroup="Total" runat="server" OnClientClick="return GridValidation();"
                                ToolTip='<%$ Resources:Resource, UPDATE %>' Visible="false" />
                        </div>
                    </div>
                </div>
                <div id="rnkland">
                    <%-- Grid Alert popup Start--%>
                    <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                        TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                        <div class="popheader popheaderlblred">
                            <asp:Label ID="lblgridalertmsg" runat="server" Text='<%$Resources:Resource, ENTR_EST_ENRGY %>'></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnEditEstimation" runat="server" Text="Edit Estimation" CssClass="ok_btn"
                                    Visible="false" OnClick="btnEditEstimation_Click" />
                                <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Grid Alert popup Closed--%>
                    <%-- Delete Alert popup Start--%>
                    <asp:ModalPopupExtender ID="mpeDeleteConfirmation" runat="server" PopupControlID="pnlDeleteConfirmation"
                        TargetControlID="hfDelTarget" BackgroundCssClass="modalBackground" CancelControlID="btnCancelDelPopup">
                    </asp:ModalPopupExtender>
                    <asp:HiddenField ID="hfDelTarget" runat="server" />
                    <asp:Panel ID="pnlDeleteConfirmation" runat="server" CssClass="modalPopup" Style="display: none;">
                        <div class="popheader popheaderlblred">
                            <asp:Label ID="lblDeleteConfirmation" runat="server" Text='<%$Resources:Resource, SURE_TO_DEL_EST_SET %>'></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnDeleteConfirmation" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" OnClick="btnDeleteConfirmation_Click" />
                                <asp:Button ID="btnCancelDelPopup" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="ok_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Grid Alert popup Closed--%>
                </div>
                <asp:HiddenField ID="hfRecognize" runat="server" />
                <asp:HiddenField ID="hfOpenStatusId" runat="server" />
                <asp:HiddenField ID="hfFocus" runat="server" />
                <asp:HiddenField ID="hfIsAllExists" runat="server" Value="1" />
                <asp:HiddenField ID="hfBillingRule" runat="server" Value="0" />
                <asp:HiddenField ID="hfTotaValue" runat="server" Value="0" />
                <asp:HiddenField ID="hfTotalCustomers" runat="server" Value="0" />
                <asp:HiddenField ID="hfTotalEnergyReaded" runat="server" Value="0" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".rnkland").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
</asp:Content>
