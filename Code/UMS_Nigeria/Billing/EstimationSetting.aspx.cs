﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for Estimation Setting
                     
 Developer        : ID077-NEERAJ KANOJIYA
 Creation Date    : 4-AUG-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Configuration;
namespace UMS_Nigeria.Billing
{
    public partial class EstimationSetting : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBAL = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        BillingMonthOpenBAL objBillingMonthOpenBAL = new BillingMonthOpenBAL();
        BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
        EstimationBE _ObjEstimationBE = new EstimationBE();
        MastersBAL _ObjMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        string Key = UMS_Resource.BILLING_MONTH_OPEN;
        DataTable dtEstimationInfo = new DataTable();


        private int YearId
        {
            get { return string.IsNullOrEmpty(ddlYear.SelectedValue) ? 0 : Convert.ToInt32(ddlYear.SelectedValue); }
        }

        private int MonthId
        {
            get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? 0 : Convert.ToInt32(ddlMonth.SelectedValue); }
        }

        #endregion

        #region Properties
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                hfIsAllExists.Value = "1";
                lblMessage.Text = pnlMessage.CssClass = string.Empty;
                if (!IsPostBack)
                {
                    BindYears();
                    BindMonths();
                    BindCyclesOrFeeders();
                }
            }
            else
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void gvEstimatedCustomerList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string readingType = rblReadingMode.SelectedItem.Value;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (readingType == ((int)BillingRules.AsPerAverageReadingRule).ToString())
                {
                    if (Convert.ToBoolean(((Literal)e.Row.FindControl("litIsExists")).Text))//If estimation exists
                    {
                        int EnergytToCalculate = Convert.ToInt32(((Label)e.Row.FindControl("lblEnergyToCalculate")).Text);
                        int EstimatedCustomers = Convert.ToInt32(((Label)e.Row.FindControl("lblEstimatedCustomers")).Text);
                        int energyReaded = (EnergytToCalculate * EstimatedCustomers);

                        ((TextBox)e.Row.FindControl("txtAvgTotal")).Text = energyReaded.ToString();
                        ((Label)e.Row.FindControl("lblAvgReadingPerCust")).Visible = false;
                        ((Label)e.Row.FindControl("lblEnergyToCalculate")).Visible = true;


                        hfTotaValue.Value = (EnergytToCalculate + Convert.ToInt32(hfTotaValue.Value)).ToString();

                        hfTotalCustomers.Value = (EstimatedCustomers + Convert.ToInt32(hfTotalCustomers.Value)).ToString();
                        hfTotalEnergyReaded.Value = (energyReaded + Convert.ToInt32(hfTotalEnergyReaded.Value)).ToString();
                    }
                    else//If estimation not exists
                    {
                        int EstimatedCustomers = Convert.ToInt32(((Label)e.Row.FindControl("lblEstimatedCustomers")).Text);
                        string lblAvgReadingPerCust = ((Label)e.Row.FindControl("lblAvgReadingPerCust")).Text;

                        int txtAvgTotal = Convert.ToInt32(((TextBox)e.Row.FindControl("txtAvgTotal")).Text);

                        hfTotaValue.Value = (Convert.ToInt32(lblAvgReadingPerCust) + Convert.ToInt32(hfTotaValue.Value)).ToString();
                        ((Label)e.Row.FindControl("lblAvgReadingPerCust")).Visible = true;
                        ((Label)e.Row.FindControl("lblEnergyToCalculate")).Visible = false;
                        hfTotalCustomers.Value = (EstimatedCustomers + Convert.ToInt32(hfTotalCustomers.Value)).ToString();
                        // int energyReaded = (EstimatedCustomers * Convert.ToInt32(lblAvgReadingPerCust));
                        hfTotalEnergyReaded.Value = (txtAvgTotal + Convert.ToInt32(hfTotalEnergyReaded.Value)).ToString();
                    }
                    ((TextBox)e.Row.FindControl("txtAvgTotal")).Visible = true;

                    ((Label)e.Row.FindControl("lblTotalAverage")).Visible = false;
                    ((TextBox)e.Row.FindControl("txtTotal")).Visible = false;



                }
                if (readingType == ((int)BillingRules.AsPerSettingsRule).ToString())
                {
                    if (Convert.ToBoolean(((Literal)e.Row.FindControl("litIsExists")).Text))//If estimation exists
                    {
                        int EnergytToCalculate = Convert.ToInt32(((Label)e.Row.FindControl("lblEnergyToCalculate")).Text);
                        int EstimatedCustomers = Convert.ToInt32(((Label)e.Row.FindControl("lblEstimatedCustomers")).Text);
                        Label lblTotalAverage = (Label)e.Row.FindControl("lblTotalAverage");
                        lblTotalAverage.Text = (EnergytToCalculate * EstimatedCustomers).ToString();
                        lblTotalAverage.Visible = true;
                        ((TextBox)e.Row.FindControl("txtAvgTotal")).Visible = false;
                        hfTotaValue.Value = (EnergytToCalculate + Convert.ToInt32(hfTotaValue.Value)).ToString();
                        hfTotalCustomers.Value = (EstimatedCustomers + Convert.ToInt32(hfTotalCustomers.Value)).ToString();

                        int energyReaded = (EstimatedCustomers * EnergytToCalculate);
                        hfTotalEnergyReaded.Value = (energyReaded + Convert.ToInt32(hfTotalEnergyReaded.Value)).ToString();
                    }
                    else
                    {
                        int EnergytToCalculate = Convert.ToInt32(((Label)e.Row.FindControl("lblEnergyToCalculate")).Text);
                        int EstimatedCustomers = Convert.ToInt32(((Label)e.Row.FindControl("lblEstimatedCustomers")).Text);
                        hfTotaValue.Value = (EnergytToCalculate + Convert.ToInt32(hfTotaValue.Value)).ToString();
                        hfTotalCustomers.Value = (EstimatedCustomers + Convert.ToInt32(hfTotalCustomers.Value)).ToString();
                    }
                }
                if (Convert.ToBoolean(((Literal)e.Row.FindControl("litIsExists")).Text))
                {
                    e.Row.Enabled = !Convert.ToBoolean(((Literal)e.Row.FindControl("litIsExists")).Text);
                    e.Row.ToolTip = Resource.ESTIMATION_EXITS;
                    lnkDeleteEstimation.Visible = true;
                }

                if (!Convert.ToBoolean(((Literal)e.Row.FindControl("litIsExists")).Text))
                {
                    hfIsAllExists.Value = "0";
                    divSave.Attributes.Remove("style");
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {

            if (BindEstimation())
            {
                lblgridalertmsg.Text = Resource.EST_DONE;
                btnEditEstimation.Visible = true;
                if (rblReadingMode.SelectedItem.Value == ((int)BillingRules.AsPerAverageReadingRule).ToString())
                    btnEditEstimation.Visible = false;
                else
                    btnEditEstimation.Visible = true;
                mpegridalert.Show();
                divSave.Visible = false;
            }
            else
                divSave.Visible = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string readingType = rblReadingMode.SelectedItem.Value;
            if (Session["dtEstimationInfo"] != null)
            {
                if (readingType == "1")
                {
                    dtEstimationInfo = (DataTable)Session["dtEstimationInfo"];
                    //foreach (DataRowCollection row in gvEstimatedCustomerList.Rows)
                    for (int i = 0; i < gvEstimatedCustomerList.Rows.Count; i++)
                    {

                        if (!Convert.ToBoolean(((Literal)gvEstimatedCustomerList.Rows[i].FindControl("litIsExists")).Text))
                        {
                            dtEstimationInfo.Rows[i]["EnergyToCalculate"] = ((TextBox)gvEstimatedCustomerList.Rows[i].FindControl("txtTotal")).Text;
                            dtEstimationInfo.Rows[i]["ClassID"] = ((Label)gvEstimatedCustomerList.Rows[i].FindControl("tblTariffID")).Text;
                            dtEstimationInfo.Rows[i]["BillingRule"] = rblReadingMode.SelectedItem.Value;
                            dtEstimationInfo.Rows[i]["Year"] = ddlYear.SelectedItem.Value;
                            dtEstimationInfo.Rows[i]["Month"] = ddlMonth.SelectedItem.Value;
                            dtEstimationInfo.Rows[i]["Cycle"] = ddlCycleList.SelectedItem.Value;
                            dtEstimationInfo.Rows[i]["CreatedBy"] = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        }
                    }
                }
                else if (readingType == "2")
                {
                    dtEstimationInfo = (DataTable)Session["dtEstimationInfo"];
                    //foreach (GridViewRowCollection row in gvEstimatedCustomerList.Rows)
                    for (int i = 0; i < gvEstimatedCustomerList.Rows.Count; i++)
                    {
                        if (!Convert.ToBoolean(((Literal)gvEstimatedCustomerList.Rows[i].FindControl("litIsExists")).Text))
                        {
                            int energyTocal = Convert.ToInt32(((TextBox)gvEstimatedCustomerList.Rows[i].FindControl("txtAvgTotal")).Text) / Convert.ToInt32(((Label)gvEstimatedCustomerList.Rows[i].FindControl("lblEstimatedCustomers")).Text);
                            dtEstimationInfo.Rows[i]["EnergyToCalculate"] = energyTocal;
                            dtEstimationInfo.Rows[i]["ClassID"] = ((Label)gvEstimatedCustomerList.Rows[i].FindControl("tblTariffID")).Text;
                            dtEstimationInfo.Rows[i]["BillingRule"] = rblReadingMode.SelectedItem.Value;
                            dtEstimationInfo.Rows[i]["Year"] = ddlYear.SelectedItem.Value;
                            dtEstimationInfo.Rows[i]["Month"] = ddlMonth.SelectedItem.Value;
                            dtEstimationInfo.Rows[i]["Cycle"] = ddlCycleList.SelectedItem.Value;
                            dtEstimationInfo.Rows[i]["CreatedBy"] = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        }
                    }
                }
                _ObjEstimationBE = _objiDsignBAL.DeserializeFromXml<EstimationBE>(_ObjMastersBAL.InsertEstimationBAL(_ObjEstimationBE, dtEstimationInfo));
                if (_ObjEstimationBE.IsSuccess)
                {
                    StepTwo();
                    Message(Resource.EST_SET_SF_SVD, UMS_Resource.MESSAGETYPE_SUCCESS);
                    divPrint.Visible = false;
                }
            }
            Session["dtEstimationInfo"] = null;
        }

        protected void btnEditEstimation_Click(object sender, EventArgs e)
        {
            rblReadingMode.Items[0].Selected = true;
            BindEstimation();
            foreach (GridViewRow row in gvEstimatedCustomerList.Rows)
            {
                //if (!row.Enabled)
                row.Enabled = true;
            }
            //Label lblTotalOFETC = new Label();
            //lblTotalOFETC.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(hfTotaValue.Value)) + "&nbsp;" + Resource.KWH;
            //lblTotalOFETC.Attributes.Add("class", "totalOfECT");
            //gvEstimatedCustomerList.FooterRow.Cells[2].Controls.Add(lblTotalOFETC);

            divSave.Visible = true;
            btnEditEstimation.Visible = false;
            btnSave.Visible = false;
            btnUpdate.Visible = true;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string readingType = rblReadingMode.SelectedItem.Value;
            if (Session["dtEstimationInfo"] != null)
            {
                if (readingType == ((int)BillingRules.AsPerSettingsRule).ToString())
                {
                    dtEstimationInfo = (DataTable)Session["dtEstimationInfo"];
                    foreach (GridViewRow row in gvEstimatedCustomerList.Rows)
                    {
                        dtEstimationInfo.Rows[row.RowIndex]["EnergyToCalculate"] = ((TextBox)gvEstimatedCustomerList.Rows[row.RowIndex].FindControl("txtTotal")).Text;
                        dtEstimationInfo.Rows[row.RowIndex]["ClassID"] = ((Label)gvEstimatedCustomerList.Rows[row.RowIndex].FindControl("tblTariffID")).Text;
                        dtEstimationInfo.Rows[row.RowIndex]["BillingRule"] = rblReadingMode.SelectedItem.Value;
                        dtEstimationInfo.Rows[row.RowIndex]["Year"] = ddlYear.SelectedItem.Value;
                        dtEstimationInfo.Rows[row.RowIndex]["Month"] = ddlMonth.SelectedItem.Value;
                        dtEstimationInfo.Rows[row.RowIndex]["Cycle"] = ddlCycleList.SelectedItem.Value;
                        dtEstimationInfo.Rows[row.RowIndex]["EstimationSettingId"] = ((Literal)gvEstimatedCustomerList.Rows[row.RowIndex].FindControl("litEstimationSettingId")).Text;
                        dtEstimationInfo.Rows[row.RowIndex]["ModifiedBy"] = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    }
                }
                //else if (readingType == ((int)BillingRules.AsPerAverageReadingRule).ToString())//This functionality Is Disabled
                //{
                //    dtEstimationInfo = (DataTable)Session["dtEstimationInfo"];
                //    foreach (GridViewRow row in gvEstimatedCustomerList.Rows)
                //    {
                //        int energyTocal = Convert.ToInt32(((TextBox)gvEstimatedCustomerList.Rows[row.RowIndex].FindControl("txtAvgTotal")).Text) / Convert.ToInt32(((Label)gvEstimatedCustomerList.Rows[row.RowIndex].FindControl("lblEstimatedCustomers")).Text);
                //        dtEstimationInfo.Rows[row.RowIndex]["EnergyToCalculate"] = energyTocal;
                //        dtEstimationInfo.Rows[row.RowIndex]["ClassID"] = ((Label)gvEstimatedCustomerList.Rows[row.RowIndex].FindControl("tblTariffID")).Text;
                //        dtEstimationInfo.Rows[row.RowIndex]["BillingRule"] = rblReadingMode.SelectedItem.Value;
                //        dtEstimationInfo.Rows[row.RowIndex]["Year"] = ddlYear.SelectedItem.Value;
                //        dtEstimationInfo.Rows[row.RowIndex]["Month"] = ddlMonth.SelectedItem.Value;
                //        dtEstimationInfo.Rows[row.RowIndex]["Cycle"] = ddlCycleList.SelectedItem.Value;
                //        dtEstimationInfo.Rows[row.RowIndex]["EstimationSettingId"] = ((Literal)gvEstimatedCustomerList.Rows[row.RowIndex].FindControl("litEstimationSettingId")).Text;
                //        dtEstimationInfo.Rows[row.RowIndex]["ModifiedBy"] = Session[UMS_Resource.SESSION_LOGINID].ToString();
                //    }
                //}
                _ObjEstimationBE = _objiDsignBAL.DeserializeFromXml<EstimationBE>(_ObjMastersBAL.UpdateEstimationBAL(dtEstimationInfo));
                if (_ObjEstimationBE.RowsAffected > 0)
                {
                    StepTwo();
                    Message(Resource.EST_SET_SF_SVD, UMS_Resource.MESSAGETYPE_SUCCESS);
                    divPrint.Visible = false;
                }
            }

        }

        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            StepTwo();
        }

        protected void btnDeleteConfirmation_Click(object sender, EventArgs e)
        {
            _ObjEstimationBE.Year = Convert.ToInt32(ddlYear.SelectedItem.Value);
            _ObjEstimationBE.Month = Convert.ToInt32(ddlMonth.SelectedItem.Value);
            _ObjEstimationBE.Cycle = ddlCycleList.SelectedItem.Value;
            _ObjEstimationBE = _objiDsignBAL.DeserializeFromXml<EstimationBE>(_ObjMastersBAL.DeleteEstimationSettingBAL(_objiDsignBAL.DoSerialize<EstimationBE>(_ObjEstimationBE)));

            if (_ObjEstimationBE.RowsAffected > 0)
            {
                StepTwo();
                divPrint.Visible = false;
                Message(Resource.EST_DELETED, UMS_Resource.MESSAGETYPE_SUCCESS);
            }
            else
                Message(Resource.DELETION_FAILS, UMS_Resource.MESSAGETYPE_ERROR);
        }

        protected void lnkDeleteEstimation_Click(object sender, EventArgs e)
        {
            mpeDeleteConfirmation.Show();
        }

        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void BindYears()
        {
            try
            {

                XmlDocument xmlResult = new XmlDocument();
                xmlResult = _objBillGenerationBal.GetDetails(ReturnType.Multiple);
                BillGenerationListBe objBillsListBE = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                _objiDsignBAL.FillDropDownList(_objiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlYear, "Year", "Year", "Year", true);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindMonths()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                _objBillGenerationBe.Year = this.YearId;
                xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.Get);
                BillGenerationListBe objBillsListBE = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                _objiDsignBAL.FillDropDownList(_objiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlMonth, "MonthName", "Month", "Month", true);

                //DataSet dsMonths = new DataSet();
                //dsMonths.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["Months"]));
                //objIdsignBal.FillDropDownList(dsMonths.Tables[0], ddlMonth, "name", "value", UMS_Resource.DROPDOWN_SELECT, false);

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindCyclesOrFeeders()
        {
            _objCommonMethods.BindNOCyclesList(ddlCycleList, true);
        }

        public void StepOne()
        {
            divInput.Visible = false;
            divResult.Visible = true;
            btnSave.Visible = true;
        }

        public void StepTwo()
        {
            divInput.Visible = true;
            divResult.Visible = false;
        }

        public bool BindEstimation()
        {
            hfTotalEnergyReaded.Value = "0";
            hfTotalCustomers.Value = "0";
            hfTotaValue.Value = "0";
            lnkDeleteEstimation.Visible = false;
            hfTotaValue.Value = "0";
            btnUpdate.Visible = false;
            lblMonthlyUsage.Text = "0";
            bool isExists = false;
            EstimationLilstBE ObjEstimationLilstBE = new EstimationLilstBE();
            EstimationDetailsLilstBE ObjEstimationDetailsLilstBE = new EstimationDetailsLilstBE();
            _ObjEstimationBE.Cycle = ddlCycleList.SelectedItem.Value;
            _ObjEstimationBE.Year = Convert.ToInt32(ddlYear.SelectedItem.Value);
            _ObjEstimationBE.Month = Convert.ToInt32(ddlMonth.SelectedItem.Value);
            //ObjEstimationLilstBE = _objiDsignBAL.DeserializeFromXml<EstimationLilstBE>(_ObjMastersBAL.GetEstimatedCustomersBAL(_ObjEstimationBE));
            XmlDocument xml = _ObjMastersBAL.GetEstimatedCustomersBAL(_ObjEstimationBE);
            ObjEstimationLilstBE = _objiDsignBAL.DeserializeFromXml<EstimationLilstBE>(xml);
            ObjEstimationDetailsLilstBE = _objiDsignBAL.DeserializeFromXml<EstimationDetailsLilstBE>(xml);
            if (ObjEstimationDetailsLilstBE.items.Count > 0)
                //if (ObjEstimationDetailsLilstBE.items[0].MonthlyUsage > 0)
                lblMonthlyUsage.Text = string.Format(Resource.TOTAL_ENRGY_CAL, _objCommonMethods.GetCurrencyFormat(ObjEstimationDetailsLilstBE.items[0].MonthlyUsage).ToString());
            dtEstimationInfo = ((DataSet)_objiDsignBAL.ConvertListToDataSet<EstimationBE>(ObjEstimationLilstBE.items)).Tables[0];
            if (dtEstimationInfo.Rows.Count > 0)
            {
                StepOne();
                divPrint.Visible = true;
                hfBillingRule.Value = Convert.ToString(_ObjEstimationBE.BillingRule);
                gvEstimatedCustomerList.DataSource = dtEstimationInfo;
                gvEstimatedCustomerList.DataBind();
                gvEstimatedCustomerList.FooterRow.Cells[0].Text = Resource.TOTAL + " " + Resource.ENRGY_TO_CAL;
                gvEstimatedCustomerList.FooterRow.Cells[0].Attributes.Add("colspan", "2");
                Label lblTotalOFETC = new Label();
                lblTotalOFETC.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(hfTotaValue.Value)) + "&nbsp;" + Resource.KWH;
                lblTotalOFETC.Attributes.Add("class", "totalOfECT");//css for javascript calcualtion

                gvEstimatedCustomerList.FooterRow.Cells[2].Controls.Add(lblTotalOFETC);
                gvEstimatedCustomerList.FooterRow.Cells[1].Text = hfTotalCustomers.Value;
                gvEstimatedCustomerList.FooterRow.Cells[1].Attributes.Add("style ", "text-align:center;");

                Label lblTotalEnergyReaded = new Label();
                lblTotalEnergyReaded.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(hfTotalEnergyReaded.Value)) + "&nbsp;" + Resource.KWH;
                lblTotalEnergyReaded.Attributes.Add("class", "totalEnergyReaded");//css for javascript calcualtion
                gvEstimatedCustomerList.FooterRow.Cells[3].Controls.Add(lblTotalEnergyReaded);
                gvEstimatedCustomerList.FooterRow.Cells[3].Attributes.Add("style ", "text-align:center;");
                //gvEstimatedCustomerList.FooterRow.Cells[1].Attributes.Add("colspan", "1");
                //gvEstimatedCustomerList.FooterRow.Cells[1].Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(hfTotaValue.Value)) + "&nbsp;" + Resource.KWH;
                //gvEstimatedCustomerList.FooterRow.Cells[1].Attributes.Add("colSpan ", "5");
                gvEstimatedCustomerList.FooterRow.Cells[2].Attributes.Add("style ", "padding-left:10px;");

                //for (int i = 2; i < gvEstimatedCustomerList.FooterRow.Cells.Count; i++)
                //    gvEstimatedCustomerList.FooterRow.Cells[i].Visible = false;
                gvEstimatedCustomerList.FooterRow.Cells[4].Visible = false;

                Session["dtEstimationInfo"] = dtEstimationInfo;
                if (hfIsAllExists.Value == "1")
                    isExists = true;

            }
            else
            {
                divPrint.Visible = false;
                btnEditEstimation.Visible = false;
                lblgridalertmsg.Text = Resource.NO_INFO_AVAIL;
                mpegridalert.Show();
            }
            return isExists;
        }
        #endregion
    }
}