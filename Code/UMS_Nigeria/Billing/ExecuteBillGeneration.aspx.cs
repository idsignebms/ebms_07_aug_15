﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Configuration;
using System.Xml;
using UMS_NigeriaBE;
using iDSignHelper;
using Resources;

namespace UMS_Nigeria.Billing
{
    public partial class ExecuteBillGeneration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnExecute_Click(object sender, EventArgs e)
        {
            CommonMethods _ObjCommonMethods = new CommonMethods();
            UMS_Service _objUMS_Service = new UMS_Service();
            iDsignBAL _objiDsignBAL = new iDsignBAL();
            BillGenerationListBe _objBillGenerationListBe = new BillGenerationListBe();


            XmlDocument xml = _objUMS_Service.GetBillGenerationDeatils_ByServiceStartDate();
            _objBillGenerationListBe = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xml);

            string feederId = string.Empty;
            string cycleId = string.Empty;
            int billingQueueScheduleId = 0;
            foreach (BillGenerationBe item in _objBillGenerationListBe.Items)
            {
                if (item.CycleId != null)
                    cycleId = item.CycleId;
                if (item.FeederId != null)
                    feederId = item.FeederId;

                billingQueueScheduleId = item.BillingQueueScheduleId;

                string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".txt";
                //set up a filestream
                string filePath = Server.MapPath(ConfigurationManager.AppSettings["BillGeneration"].ToString()) + Path.GetFileName(fileName);
                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);



                DataSet ds = _objUMS_Service.GenerateBill_ByBillQueueDetails(feederId, cycleId, billingQueueScheduleId);
                //DataSet ds = _objUMS_Service.GenerateBill_ByBillQueueDetails("", "", 0);

                //string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss");
                ////set up a filestream
                //FileStream fs = new FileStream(@"D:\EDMS_BillGenerations\" + fileName + ".txt", FileMode.OpenOrCreate, FileAccess.Write);

                //set up a streamwriter for adding text
                StreamWriter sw = new StreamWriter(fs);

                //find the end of the underlying filestream
                sw.BaseStream.Seek(0, SeekOrigin.End);
                //string accountNumbers = string.Empty;
                int rowCount = 0;
                string billDetails = string.Empty;
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    rowCount++;
                    //accountNumbers = string.IsNullOrEmpty(accountNumbers) ? dr["AccountNo"].ToString() : accountNumbers + "," + dr["AccountNo"].ToString();
                    billDetails += string.Format("{0,-6},{1,-107},{2,-11}", string.Empty, rowCount, rowCount).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-15},{1,-70},{2,-15},{3,-6}", "Route: 0", "Seq.", "Route: 0", "Seq.").Replace(',', ' ') +
                                Environment.NewLine +
                                Environment.NewLine + string.Format("{0,-34},{1,-63},{2,-25}", string.Empty, dr["BusinessUnitName"].ToString(), dr["BusinessUnitName"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-23},{1,-74},{2,-9}", string.Empty, "PLS PAY AT ANY BEDC OFFICE OR DESIGNATED BANKS", dr["MonthName"].ToString() + dr["BillYear"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-31},{1,-27},{2,-36},{3,-22}", string.Empty, "Private Account", dr["MonthName"].ToString() + dr["BillYear"].ToString(), "Private Account", dr["MonthName"].ToString() + dr["BillYear"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-96},{1,-16}", string.Empty, dr["AccountNo"].ToString()).Replace(',', ' ') +
                                Environment.NewLine +
                                Environment.NewLine + string.Format("{0,8},{1,-38},{2,-28},{3,-22},{4,-30}", string.Empty, dr["AccountNo"].ToString(), string.Empty, string.Empty, dr["Name"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,8},{1,-38},{2,-28},{3,-22},{4,-30}", string.Empty, dr["Name"].ToString(), DateTime.Now.AddDays(5).ToString("dd/MM/yyyy"), dr["PreviousBalance"].ToString(), dr["PostalHouseNo"].ToString() + " " + dr["PostalStreet"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,8},{1,-38},{2,-28},{3,-22},{4,-30}", string.Empty, dr["PostalHouseNo"].ToString() + " " + dr["PostalStreet"].ToString(), dr["MeterNo"].ToString(), dr["NetEnergyCharges"].ToString(), dr["PostalZipCode"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,8},{1,-38},{2,-28},{3,-22},{4,-30}", string.Empty, dr["PostalZipCode"].ToString(), dr["NetFixedCharges"].ToString(), string.Empty, dr["ServiceHouseNo"].ToString() + " " + dr["ServiceStreet"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,8},{1,-38},{2,-28},{3,-22},{4,-30}", string.Empty, string.Empty, dr["Dials"].ToString(), dr["NetArrears"].ToString(), dr["ServiceZipCode"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,8},{1,-90},{2,-15}", string.Empty, "THIS BILLING IS FOR APRIL 2014 CONSUMPTION", dr["MeterNo"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + Environment.NewLine +
                                Environment.NewLine + string.Format("{0,-13},{1,-7},{2,-16},{3,-12},{4,-7},{5,-7},{6,7},{7,11}", "ENERGY", dr["TariffId"].ToString(), dr["ReadDate"].ToString(), dr["PreviousReading"].ToString(), dr["PresentReading"].ToString(), dr["Multiplier"].ToString(), dr["Usage"].ToString(), dr["NetEnergyCharges"].ToString()).Replace(',', ' ') +
                                Environment.NewLine +
                                Environment.NewLine + string.Format("{0,-13},{1,-7},{2,-16},{3,-12},{4,-7},{5,-7},{6,7},{7,11}", "FCR2S", dr["TariffId"].ToString(), "----", "----", "----", "----", "----", dr["NetFixedCharges"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-22},{1,-19}", string.Empty, "Billing Periods: 01").Replace(',', ' ') +
                                Environment.NewLine + Environment.NewLine +
                                Environment.NewLine + string.Format("{0,-33},{1,-22}", "LAST PAYMENT DATE  " + dr["LastPaymentDate"].ToString(), "AMOUNT      " + dr["LastPaidAmount"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-100},{1,-11}", "RECONNECTION FEE IS NOW =N=5000", DateTime.Now.AddDays(5).ToString("dd/MM/yyyy")).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-70},{1,17},{2,27}", "PAY WITHIN 7 DAYS TO AVOID DISCONNECTION", dr["NetArrears"].ToString(), dr["NetArrears"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-70},{1,17},{2,27}", string.Empty, dr["TotalBillAmount"].ToString(), dr["TotalBillAmount"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-70},{1,17},{2,27}", string.Empty, dr["VAT"].ToString(), dr["VAT"].ToString()).Replace(',', ' ') +
                                Environment.NewLine +
                                Environment.NewLine + string.Format("{0,-70},{1,17},{2,27}", string.Empty, dr["TotalBillAmountWithTax"].ToString(), dr["TotalBillAmountWithTax"].ToString()).Replace(',', ' ') +
                                Environment.NewLine +
                                Environment.NewLine + string.Format("{0,-27},{1,-77},{2,-16}", "OLD ACCT --XYZ--", "ENR2S- Rate:=N= 11.37", "--XYZ--").Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-2},{1,-60}", string.Empty, "THANKS FOR YOUR PATRONAGE").Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-5},{1,-26},{2,-61}", string.Empty, "- ENR2S- Rate:=N= 11.37", " Marketer:  ENOGHAYANGBON P.(07039738955)").Replace(',', ' ') +
                                Environment.NewLine;
                    //+ Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine +
                    // "-------------------------------------------------------------------------------------------------------------------" + Environment.NewLine;
                }


                //add the text
                sw.WriteLine(billDetails);
                //add the text to the underlying filestream

                sw.Flush();
                //close the writer
                sw.Close();
                //fs.Flush();
                //fs.Close();
                _objUMS_Service.UpdateBillFileDetails(billingQueueScheduleId, (int)EnumBillGenarationStatus.FileCreated, fileName);

                _ObjCommonMethods.SendMail_BillGeneration(filePath, cycleId, feederId, item.MonthName, item.BillingYear, Session[UMS_Resource.SESSION_USER_EMAILID].ToString());

                _objUMS_Service.UpdateBillGenerationStatus(billingQueueScheduleId, (int)EnumBillGenarationStatus.EmailSent);
                _objUMS_Service.UpdateBillGenerationStatus(billingQueueScheduleId, (int)EnumBillGenarationStatus.BillingQueeClosed);


            }
        }
    }
}