﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddServiceUnits
                     
 Developer        : id067-Naresh T
 Creation Date    : 15-Apr-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using iDSignHelper;
using Resources;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using System.Drawing;
using System.Xml;
using System.IO;
using System.Web.Services;
using System.Data.SqlClient;
using System.Globalization;

namespace UMS_Nigeria.Billing
{
    public partial class BillGenerationStepOne : System.Web.UI.Page
    {

        #region Members

        iDsignBAL _objIdsignBal = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        TariffManagementBal _objTariffBal = new TariffManagementBal();
        ConsumerBal _objConsumerBal = new ConsumerBal();
        BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
        private delegate void EmailSendFile(string filePath, string cycleId, string feederId, string MonthName, int BillingYear, string toEmailId);
        String Key = "BillGenerationStepOne";
        string BillingQueueScheduleIdList = null;
        string fileNameList = null;
        string filePathList = null;
        public int PageNum;
        public bool IsFileGenValid = true;

        #endregion

        #region Properties

        private int BillProcessType
        {
            get { return string.IsNullOrEmpty(rblBillProcessTypes.SelectedValue) ? 0 : Convert.ToInt32(rblBillProcessTypes.SelectedValue); }
        }

        private int YearId
        {
            //get { return string.IsNullOrEmpty(ddlYear.SelectedValue) ? 0 : Convert.ToInt32(ddlYear.SelectedValue); }
            get { return string.IsNullOrEmpty(hfYearID.Value) ? 0 : Convert.ToInt32(hfYearID.Value); }
        }

        private int MonthId
        {
            //get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? 0 : Convert.ToInt32(ddlMonth.SelectedValue); }
            get { return string.IsNullOrEmpty(hfMonthID.Value) ? 0 : Convert.ToInt32(hfMonthID.Value); }
        }

        //private string BillCycleFeederValue
        //{
        //    get { return string.IsNullOrEmpty(ddlCycleList.SelectedValue) ? string.Empty : ddlCycleList.SelectedValue; }
        //}

        private int BillSendingMode
        {
            get { return string.IsNullOrEmpty(rblBillSendingMode.SelectedValue) ? 0 : Convert.ToInt32(rblBillSendingMode.SelectedValue); }
        }

        private string SelectedMonth
        {
            //get { return (string.IsNullOrEmpty(ddlMonth.SelectedValue) ? string.Empty : ddlMonth.SelectedItem.Text) + "-" + (string.IsNullOrEmpty(ddlYear.SelectedValue) ? string.Empty : ddlYear.SelectedItem.Text); }
            get { return (string.IsNullOrEmpty(hfMonthID.Value) ? string.Empty : lblMonth.Text) + "-" + (string.IsNullOrEmpty(hfYearID.Value) ? string.Empty : lblYear.Text); }
        }

        //public string BusinessUnit
        //{
        //    get { return Session[UMS_Resource.SESSION_USER_BUID].ToString(); }
        //    //get { return string.IsNullOrEmpty(ddlBusinessUnits.SelectedValue) ? string.Empty : ddlBusinessUnits.SelectedValue; }
        //    //set { ddlBusinessUnits.SelectedValue = value.ToString(); }
        //}

        //public string ServiceUnit
        //{
        //    get { return string.IsNullOrEmpty(ddlServiceUnits.SelectedValue) ? string.Empty : ddlServiceUnits.SelectedValue; }
        //    set { ddlServiceUnits.SelectedValue = value.ToString(); }
        //}

        public string ServiceUnit
        {
            get { return _ObjCommonMethods.CollectSelectedItemsValues(cblServiceUnits, ","); }
        }

        //public string ServiceCenter
        //{
        //    get { return string.IsNullOrEmpty(ddlServiceCenters.SelectedValue) ? string.Empty : ddlServiceCenters.SelectedValue; }
        //    set { ddlServiceCenters.SelectedValue = value.ToString(); }
        //}

        public string ServiceCenter
        {
            get { return _ObjCommonMethods.CollectSelectedItemsValues(cblServiceCenters, ","); }
        }

        public string Cycles
        {
            get { return _ObjCommonMethods.CollectSelectedItemsValues(cblCycles, ","); }
        }

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblTariffEnergies.Text = PanelTarifEnergy.CssClass = pnlMessage.CssClass = lblMessage.Text = string.Empty;
            if (!IsPostBack)
            {
                divDisabledBooks.Visible = false;
                string path = string.Empty;
                path = _ObjCommonMethods.GetPagePath(this.Request.Url);
                //if (_ObjCommonMethods.IsAccess(path))
                {
                    //((TextBox)ucAuthentication.FindControl("txtPassword")).Focus();
                    BindYears();
                    //BindMonths();
                    BindBillProcessTypes();
                    // BindCyclesOrFeeders();
                    //ddlServiceCenters.Enabled = false;
                    BindServiceUnits();
                    CheckBoxesJS();
                    lblBillEmailId.Text = Session[UMS_Resource.SESSION_USER_EMAILID].ToString();
                    //_ObjCommonMethods.BindBusinessUnits(ddlBusinessUnits, string.Empty, true);
                    lblBillEmailId.Text = Session[UMS_Resource.SESSION_USER_EMAILID].ToString();
                }
                //else
                //{
                //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                //}
            }
        }

        //private void BindCyclesOrFeeders()
        //{
        //    switch (this.BillProcessType)
        //    {
        //        case (int)EnumBillProcessType.CycleWsie:
        //            _ObjCommonMethods.BindNOCyclesList(ddlCycleList, true);
        //            break;
        //        case (int)EnumBillProcessType.FeederWise:
        //            _ObjCommonMethods.BindFeedersList(ddlCycleList, UMS_Resource.DROPDOWN_SELECT);
        //            break;
        //    }
        //}

        protected void btnConfirmationOk_Click(object sender, EventArgs e)
        {
            SaveBillGenerationCustomers();
            //GenearateBills(); //Old Generate bill logic
            if (GenerateCustomersBills() > 0)// New Generate bill logic --Neeraj ID-077 (13-Feb-15)
            {

                //EnableSteps(divStep3);
                divStatusSuccess.Visible = true;
                divStatusError.Visible = false;
                EnableSteps(divBillingMessage);
                ClearAllControls();

                if (Session[UMS_Resource.SESSION_USER_EMAILID].ToString() != null)
                    lblBillThankyouMessage.Text = string.Format(Resource.BILL_THANKYOU_MESSAGE, Session[UMS_Resource.SESSION_USER_EMAILID].ToString());
                else
                    lblBillThankyouMessage.Text = string.Format(Resource.BILL_THANKYOU_MESSAGE, string.Empty);
                //lblBillThankyouMessage.Text = string.Format(Resource.BILL_THANKYOU_MESSAGE, string.Empty);
                Message(Resource.BILL_GEN_SUC_MSG_BOX, UMS_Resource.MESSAGETYPE_SUCCESS);
            }
            else
            {
                lblStatusError.Text = string.Format(Resource.BILL_GEN_NOT_SUCD, string.Empty);
                divStatusSuccess.Visible = false;
                divStatusError.Visible = true;
                Message(Resource.BILL_GEN_NOT_SUC_MSG_BOX, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        protected void rblBillProcessTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //BindCyclesOrFeeders();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindMonths();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            hfIsValid.Value = "0";
            string selectedProcessType = string.Empty;
            if (IsCustomersExists())
            {
                EnableSteps(divStep2);
                lblSelectedCycles.Text = _ObjCommonMethods.CollectSelectedItems(cblCycles, ",");
                lblBillingMonth.Text = this.SelectedMonth;
                lblGeneratedBY.Text = Session[UMS_Resource.SESSION_UserName].ToString() + "(" + Session[UMS_Resource.SESSION_USER_EMAILID].ToString() + ")";
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                _objBillGenerationBe = IsBillExitsForSelectedMonth();
                if (_objBillGenerationBe.IsActive)
                {
                    divMessages.Visible = true;
                    gvCustomerDetails.Visible = divStep2GridView.Visible = false;
                    if (_objBillGenerationBe.IsExists)
                        //lblBillingAlertMessage.Text = Resource.BILL_GENERATION_EXITS_MESSAGE;//"Already Bill Generated for the selected Months to selected Cycle/Feeder";
                        lblBillingAlertMessage.Text = Resource.BILL_GENERATION_CYCLE_EXITS_MESSAGE;
                    else if (!_objBillGenerationBe.IsExistingProcessType)
                    {
                        lblBillingAlertMessage.Text = Resource.BILL_UNIQUE_PROCESSTYPE_MESSAGE;//"Please select Unique Process type for selected Month";
                        btnRegeneration.Visible = false;
                    }
                    else
                    {
                        divStep2GridView.Visible = true;
                        divMessages.Visible = false;
                    }
                    if (_objBillGenerationBe.IsExistingProcessType)
                    {
                        btnRegeneration.Visible = gvCustomerDetails.Visible = true;
                        //divStep2GridView.Visible = true;
                        //divMessages.Visible = false;
                        BindCustomersDetails();
                    }
                }
                else
                {
                    divStep2GridView.Visible = true;
                    divMessages.Visible = false;
                    BindCustomersDetails();
                }
                GetDisabledBookDetails();

                //hfIsValid.Value = "1";
                //XmlDocument xmlResult = new XmlDocument();
                //BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                //_objBillGenerationBe.BillingMonth = this.MonthId;
                //_objBillGenerationBe.BillingYear = this.YearId;
                //_objBillGenerationBe.BillSendingMode = this.BillSendingMode;
                //_objBillGenerationBe.BillProcessTypeId = this.BillProcessType;
                //switch (this.BillProcessType)
                //{
                //    case (int)EnumBillProcessType.CycleWsie:
                //        _objBillGenerationBe.CycleId = this.BillCycleFeederValue;
                //        _objBillGenerationBe.FeederId = string.Empty; ;
                //        selectedProcessType = Resource.CYCLE;
                //        break;
                //    case (int)EnumBillProcessType.FeederWise:
                //        _objBillGenerationBe.CycleId = string.Empty;
                //        _objBillGenerationBe.FeederId = this.BillCycleFeederValue;
                //        selectedProcessType = Resource.FEEDER;
                //        break;
                //}

                //xmlResult = objBillGenerationBal.InsertDetails(_objBillGenerationBe, Statement.Insert);
                //_objBillGenerationBe = objIdsignBal.DeserializeFromXml<BillGenerationBe>(xmlResult);
                //if (_objBillGenerationBe.BillingQueueScheduleId > 0)
                //{
                //    lblThankYouMessage.Text = String.Format(Resource.BILL_GENERATION_THANKU, selectedProcessType, selectedProcessType);
                //    divBillThankuMsg.Attributes.Add("style", "background-color: #639B00;");
                //    mpAfterBillGeneration.Show();
                //}
            }
            else
            {
                hfIsValid.Value = "0";
                switch (this.BillProcessType)
                {
                    case (int)EnumBillProcessType.CycleWsie:
                        selectedProcessType = Resource.CYCLE;
                        break;
                    case (int)EnumBillProcessType.FeederWise:
                        selectedProcessType = Resource.FEEDER;
                        break;
                }
                lblThankYouMessage.Text = String.Format(Resource.BILL_CUSTOMER_NOT_EXISTS, selectedProcessType, selectedProcessType);
                divBillThankuMsg.Attributes.Add("style", "background-color: Red;");
                mpAfterBillGeneration.Show();
            }

        }

        protected void btnThankyouOK_Click(object sender, EventArgs e)
        {
            if (hfIsValid.Value == "1")
            {
                Response.Redirect(Constants.BillGenerationstep2, true);
            }
        }

        protected void lbtnPrevious_Click(object sender, EventArgs e)
        {
            EnableSteps(divStep1);
        }

        protected void btnGoToBilling_Click(object sender, EventArgs e)
        {
            EnableSteps(divStep1);
        }

        protected void btnGoNextBilling_Click(object sender, EventArgs e)
        {
            EnableSteps(divStep1);
        }

        protected void btnRegeneration_Click(object sender, EventArgs e)
        {
            UpdateBillGeneration();
            //GenearateBills();
            //EnableSteps(divStep3);
            if (GenerateCustomersBills() > 0)// New Generate bill logic --Neeraj ID-077 (13-Feb-15)
            {
                //EnableSteps(divStep3);
                divStatusSuccess.Visible = true;
                divStatusError.Visible = false;
                EnableSteps(divBillingMessage);
                ClearAllControls();

                if (Session[UMS_Resource.SESSION_USER_EMAILID].ToString() != null)
                    lblBillThankyouMessage.Text = string.Format(Resource.BILL_THANKYOU_MESSAGE, Session[UMS_Resource.SESSION_USER_EMAILID].ToString());
                else
                    lblBillThankyouMessage.Text = string.Format(Resource.BILL_THANKYOU_MESSAGE, string.Empty);
                //lblBillThankyouMessage.Text = string.Format(Resource.BILL_THANKYOU_MESSAGE, string.Empty);
                Message(Resource.BILL_GEN_SUC_MSG_BOX, UMS_Resource.MESSAGETYPE_SUCCESS);
            }
            else
            {
                lblStatusError.Text = string.Format(Resource.BILL_GEN_NOT_SUCD, string.Empty);
                divStatusSuccess.Visible = false;
                divStatusError.Visible = true;
                Message(Resource.BILL_GEN_NOT_SUC_MSG_BOX, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        protected void ddlBusinessUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    if (ddlBusinessUnits.SelectedIndex > 0)
            //    {
            //        _ObjCommonMethods.BindServiceUnits(ddlServiceUnits, this.BusinessUnit, true);
            //        ddlServiceUnits.Enabled = true;
            //        ddlServiceCenters.Items.Clear();
            //        cblCycles.Items.Clear();
            //        //ddlCycleList.Items.Clear();
            //        ddlServiceCenters.Enabled = false; //ddlCycleList.Enabled =
            //        ddlServiceUnits_SelectedIndexChanged(this, null);
            //    }
            //    else
            //    {
            //        ddlServiceCenters.Items.Clear();
            //        //ddlCycleList.Items.Clear();
            //        cblCycles.Items.Clear();
            //        ddlServiceUnits.Items.Clear();
            //        ddlServiceCenters.Enabled = ddlServiceUnits.Enabled = false;//ddlCycleList.Enabled =
            //    }
            //    cbAll.Checked = false;
            //}
            //catch (Exception ex)
            //{
            //    _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
            //    try
            //    {
            //        _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            //    }
            //    catch (Exception logex)
            //    {

            //    }
            //}
        }

        //protected void ddlServiceUnits_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ddlServiceUnits.SelectedIndex > 0)
        //        {
        //            _ObjCommonMethods.BindServiceCenters(ddlServiceCenters, this.ServiceUnit, true);
        //            ddlServiceCenters.Enabled = true;
        //            //ddlCycleList.Items.Clear();
        //            cblCycles.Items.Clear();
        //            ddlServiceCenters_SelectedIndexChanged(this, null);
        //        }
        //        else
        //        {
        //            ddlServiceCenters.Items.Clear();
        //            //ddlCycleList.Items.Clear();
        //            cblCycles.Items.Clear();
        //            ddlServiceCenters.Enabled = false; //ddlCycleList.Enabled =
        //        }
        //        cbAll.Checked = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void cblServiceUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                cblCycles.Items.Clear();
                cblServiceCenters.Items.Clear();
                chkSCAll.Checked = divServiceCenters.Visible = divCycles.Visible = false;
                if (!string.IsNullOrEmpty(this.ServiceUnit))
                {
                    _ObjCommonMethods.BindUserServiceCenters_SuIds(cblServiceCenters, this.ServiceUnit, Resource.DDL_ALL, false);
                    if (cblServiceCenters.Items.Count > 0)
                    {
                        divServiceCenters.Visible = true;
                    }
                    else
                    {

                        divServiceCenters.Visible = false;
                    }
                }

                cbAll.Checked = false;
            }
            catch (Exception ex)
            {
                _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //protected void ddlServiceCenters_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ddlServiceCenters.SelectedIndex > 0)
        //        {
        //            _ObjCommonMethods.BindCyclesByBUSUSC(cblCycles, Session[UMS_Resource.SESSION_USER_BUID].ToString(), this.ServiceUnit, this.ServiceCenter, true, "Select Cycle", true);
        //            //ddlCycleList.Enabled = true;
        //        }
        //        else
        //        {
        //            //ddlCycleList.Items.Clear();
        //            cblCycles.Items.Clear();
        //            //ddlCycleList.Enabled = false;
        //        }
        //        cbAll.Checked = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void cblServiceCenters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //cblCycles.Items.Clear();
                divCycles.Visible = false;
                if (!string.IsNullOrEmpty(this.ServiceCenter))
                {
                    //_ObjCommonMethods.BindCyclesByBUSUSC(cblCycles, Session[UMS_Resource.SESSION_USER_BUID].ToString(), this.ServiceUnit, this.ServiceCenter, true, string.Empty, false);
                    string BU_Id = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                    BindCY_BY_BU(cblCycles, BU_Id, this.ServiceUnit, this.ServiceCenter, true, string.Empty, false);
                    if (cblCycles.Items.Count > 0)
                        divCycles.Visible = true;
                    else
                        divCycles.Visible = false;
                }
                else
                {
                    divCycles.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDummyforCycle_Click(object sender, EventArgs e)
        {
            cblServiceCenters_SelectedIndexChanged(sender, e);
            if (!string.IsNullOrEmpty(this.Cycles))
            {
                if (CheckTariffEnergyCharges())
                    CheckDisableBookDetails();
                bool IsValid = true;
                if (!CheckIsReadingDone(this.MonthId, this.YearId, this.Cycles))
                    IsValid = false;
                if (!CheckPaymentBatchClose(this.MonthId, this.YearId))
                    IsValid = false;
                if (!CheckAdjustmentBatchClose(this.MonthId, this.YearId))
                    IsValid = false;
                if (!CheckIsEstimationDone(this.MonthId, this.YearId, this.Cycles))
                    IsValid = false;
                if (IsValid)
                {
                    btnNext.Visible = true;
                    divErrorMessges.Visible = false;
                }
                else
                    divErrorMessges.Visible = true;
            }
            else
                divInValidReadings.Visible = divInValidPaymentBatches.Visible = divInvalidAdustmentBatches.Visible = divEstimationValid.Visible = divErrorMessges.Visible = false;
        }

        private bool CheckTariffEnergyCharges()
        {
            bool isValid = false;
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            BillGenerationListBe _objBillGenerationListBe = new BillGenerationListBe();
            _objBillGenerationBe.BillingMonth = this.MonthId;
            _objBillGenerationBe.BillingYear = this.YearId;
            _objBillGenerationBe.CycleId = this.Cycles;
            XmlDocument xmlResult = _objBillGenerationBal.GetBillCycleDetails(_objBillGenerationBe, ReturnType.Check);
            _objBillGenerationListBe = _objIdsignBal.DeserializeFromXml<BillGenerationListBe>(xmlResult);
            if (_objBillGenerationListBe.Items.Count > 0)
            {
                if (!_objBillGenerationListBe.Items[0].IsExists)
                {
                    divafterCycles.Visible = false;
                    var result = string.Join(",", _objBillGenerationListBe.Items.AsEnumerable()
                                                 .Select(p => p.CycleName.ToString()));
                    lblTariffEnergies.Text = string.Format(Resource.PROVIDE_TARIFF_ENERGY_CYCLES, result);
                    PanelTarifEnergy.CssClass = "billingmessage error";
                    isValid = false;
                }
                else
                {
                    lblTariffEnergies.Text = PanelTarifEnergy.CssClass = string.Empty;
                    divafterCycles.Visible = true;
                    isValid = true;
                }
            }
            return isValid;
        }

        private void CheckDisableBookDetails()
        {
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            BillGenerationListBe _objBillGenerationListBe = new BillGenerationListBe();
            _objBillGenerationBe.BillingMonth = this.MonthId;
            _objBillGenerationBe.BillingYear = this.YearId;
            _objBillGenerationBe.CycleId = this.Cycles;
            XmlDocument xmlResult = _objBillGenerationBal.GetBillCycleDetails(_objBillGenerationBe, ReturnType.Group);
            _objBillGenerationListBe = _objIdsignBal.DeserializeFromXml<BillGenerationListBe>(xmlResult);
            if (_objBillGenerationListBe.Items.Count > 0)
            {
                divDisabledBooks.Visible = true;
                var result = string.Join(",", _objBillGenerationListBe.Items.AsEnumerable()
                                             .Select(p => p.BookNo.ToString()));
                lblDisabledBooks.Text = result.ToString();
            }
            else
            {
                lblDisabledBooks.Text = string.Empty;
                divDisabledBooks.Visible = false;
            }
            //rblReadings.SelectedIndex = rblAdjustments.SelectedIndex = rblBatches.SelectedIndex = rblEstimation.SelectedIndex = -1;
        }

        private void GetDisabledBookDetails()
        {
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            BillGenerationListBe _objBillGenerationListBe = new BillGenerationListBe();
            _objBillGenerationBe.BillingMonth = this.MonthId;
            _objBillGenerationBe.BillingYear = this.YearId;
            _objBillGenerationBe.CycleId = this.Cycles;
            XmlDocument xmlResult = _objBillGenerationBal.GetBillCycleDetails(_objBillGenerationBe, ReturnType.List);
            _objBillGenerationListBe = _objIdsignBal.DeserializeFromXml<BillGenerationListBe>(xmlResult);

            lblDisableCustomers.Text = _objBillGenerationListBe.Items.Count.ToString();
        }

        //protected void btnSave_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (rblReadings.SelectedValue == "0")
        //        {
        //            mpeBatchFinished.Show();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }

        //}
        #endregion

        #region Methods

        private void BindYears()
        {
            try
            {

                XmlDocument xmlResult = new XmlDocument();
                xmlResult = _objBillGenerationBal.GetDetails(ReturnType.Multiple);
                BillGenerationListBe objBillsListBE = _objIdsignBal.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                if (objBillsListBE.Items.Count > 0)
                {
                    lblYear.Text = objBillsListBE.Items[0].Year.ToString();
                    hfYearID.Value = objBillsListBE.Items[0].Year.ToString();
                    // _objIdsignBal.FillDropDownList(_objIdsignBal.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlYear, "Year", "Year", "Year", true);
                    BindMonths();
                    divAlert.Visible = false;
                    divBillInput.Visible = true;
                }
                else
                {
                    divAlert.Visible = true;
                    divBillInput.Visible = false;
                }

                /// Old Code
                //int CurrentYear = DateTime.Now.Year;
                //int FromYear = Convert.ToInt32(GlobalConstants.TARIFF_ENTRY_NO_OF_LAST_YEARS);
                //for (int i = FromYear; i < CurrentYear; i++)
                //{
                //    ListItem item = new ListItem();
                //    item.Text = item.Value = i.ToString();
                //    ddlYear.Items.Add(item);

                //}
                //for (int i = 0; i <= Convert.ToInt32(GlobalConstants.TARIFF_ENTRY_NO_OF_PRESENT_AND_FUTURE_YEARS); i++)
                //{
                //    ListItem item = new ListItem();
                //    item.Text = item.Value = (CurrentYear + i).ToString();
                //    ddlYear.Items.Add(item);
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindMonths()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                _objBillGenerationBe.Year = this.YearId;
                xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.Get);
                BillGenerationListBe objBillsListBE = _objIdsignBal.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                if (objBillsListBE.Items.Count > 0)
                {
                    lblMonth.Text = objBillsListBE.Items[0].MonthName;
                    hfMonthID.Value = objBillsListBE.Items[0].Month.ToString();
                    divAlert.Visible = false;
                    divBillInput.Visible = true;
                    // _objIdsignBal.FillDropDownList(_objIdsignBal.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlMonth, "MonthName", "Month", "Month", true);
                }
                else
                {
                    divAlert.Visible = true;
                    divBillInput.Visible = false;
                }
                //DataSet dsMonths = new DataSet();
                //dsMonths.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["Months"]));
                //objIdsignBal.FillDropDownList(dsMonths.Tables[0], ddlMonth, "name", "value", UMS_Resource.DROPDOWN_SELECT, false);

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void CheckBoxesJS()
        {
            ChkSUAll.Attributes.Add("onclick", "CheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            chkSCAll.Attributes.Add("onclick", "CheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");

            cblServiceUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            cblServiceCenters.Attributes.Add("onclick", "UnCheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
        }

        private void BindBillProcessTypes()
        {
            _ObjCommonMethods.BindBillProcessTypes(rblBillProcessTypes, string.Empty);
            if (rblBillProcessTypes.Items.Count == 1)
            {
                rblBillProcessTypes.Items[0].Selected = true;
            }
        }

        private void SaveBillGenerationCustomers()
        {
            XmlDocument xmlResult = new XmlDocument();
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            _objBillGenerationBe.BillingMonth = this.MonthId;
            _objBillGenerationBe.BillingYear = this.YearId;
            _objBillGenerationBe.BillSendingMode = this.BillSendingMode;
            _objBillGenerationBe.BillProcessTypeId = this.BillProcessType;
            _objBillGenerationBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            switch (this.BillProcessType)
            {
                case (int)EnumBillProcessType.CycleWsie:
                    _objBillGenerationBe.CycleId = this.Cycles; //this.BillCycleFeederValue;
                    _objBillGenerationBe.FeederId = string.Empty;
                    //selectedProcessType = Resource.CYCLE;
                    break;
                case (int)EnumBillProcessType.FeederWise:
                    _objBillGenerationBe.CycleId = string.Empty;
                    _objBillGenerationBe.FeederId = this.Cycles; //this.BillCycleFeederValue;
                    //selectedProcessType = Resource.FEEDER;
                    break;
            }

            xmlResult = _objBillGenerationBal.InsertDetails(_objBillGenerationBe, Statement.Insert);
            _objBillGenerationBe = _objIdsignBal.DeserializeFromXml<BillGenerationBe>(xmlResult);
            //if (_objBillGenerationBe.TotalRecords > 0)
            //{
            //    EnableSteps(divStep3);
            //    ClearAllControls();
            //    //lblThankYouMessage.Text = Resource.BILL_GENERATION_THANKU;
            //    //divBillThankuMsg.Attributes.Add("style", "background-color: #639B00;");
            //    //mpAfterBillGeneration.Show();
            //}
        }

        //private void ClearAllControls()
        //{
        //    ddlServiceUnits.SelectedIndex = ddlMonth.SelectedIndex = ddlYear.SelectedIndex = 0;//ddlCycleList.SelectedIndex =
        //    ddlServiceCenters.Items.Clear();
        //    cbAll.Checked = false;
        //    rblReadings.SelectedIndex = rblBatches.SelectedIndex = rblAdjustments.SelectedIndex = rblEstimation.SelectedIndex = -1;//rblBillProcessTypes.SelectedIndex =  cblCycles.SelectedIndex = 
        //}

        private void ClearAllControls()
        {
            //ddlMonth.SelectedIndex = ddlYear.SelectedIndex = 0;//ddlCycleList.SelectedIndex =
            hfMonthID.Value = hfYearID.Value = "0";//ddlCycleList.SelectedIndex =
            cblServiceCenters.Items.Clear();
            divServiceCenters.Visible = false;
            cbAll.Checked = false;
            //rblReadings.SelectedIndex = rblBatches.SelectedIndex = rblAdjustments.SelectedIndex = rblEstimation.SelectedIndex = -1;//rblBillProcessTypes.SelectedIndex =  cblCycles.SelectedIndex = 
        }

        private void GenearateBills()
        {
            CommonMethods _ObjCommonMethods = new CommonMethods();
            UMS_Service _objUMS_Service = new UMS_Service();
            iDsignBAL _objiDsignBAL = new iDsignBAL();
            BillGenerationListBe _objBillGenerationListBe = new BillGenerationListBe();

            XmlDocument xml = _objUMS_Service.GetBillGenerationDeatils_ByServiceStartDate();
            _objBillGenerationListBe = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xml);

            string feederId = string.Empty;
            string cycleId = string.Empty;
            int billingQueueScheduleId = 0;
            foreach (BillGenerationBe item in _objBillGenerationListBe.Items)
            {
                if (item.CycleId != null)
                    cycleId = item.CycleId;
                if (item.FeederId != null)
                    feederId = item.FeederId;

                billingQueueScheduleId = item.BillingQueueScheduleId;

                string BUPath = Server.MapPath(ConfigurationManager.AppSettings["BillGeneration"].ToString()) + item.BusinessUnitName + "/";
                string SUPath = BUPath + item.ServiceUnitName + "/";
                string SCPath = SUPath + item.ServiceCenterName + "/";
                string MonthPath = SCPath + item.MonthName + item.BillingYear.ToString() + "/";

                if (!string.IsNullOrEmpty(item.BusinessUnitName))
                {
                    CreateDirectory(BUPath);// Create BU Directory
                }
                if (!string.IsNullOrEmpty(item.ServiceUnitName))
                {
                    CreateDirectory(SUPath);// Create SU Directory
                }
                if (!string.IsNullOrEmpty(item.ServiceCenterName))
                {
                    CreateDirectory(SCPath);// Create SC Directory
                }
                if (!string.IsNullOrEmpty(item.MonthName))
                {
                    CreateDirectory(MonthPath);// Create Month Directory
                }

                fileNameList += item.CycleName + ".txt" + "|"; //+"_" + DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".txt";
                //set up a filestream
                //string filePath = Server.MapPath(ConfigurationManager.AppSettings["BillGeneration"].ToString()) + Path.GetFileName(fileName);
                filePathList += MonthPath + fileNameList;

                if (File.Exists(filePathList))
                {
                    try
                    {
                        File.Delete(filePathList);
                    }
                    catch
                    {
                    }
                }

                DataSet ds = new DataSet();
                ds = _objUMS_Service.GenerateBill_ByBillQueueDetails(feederId, cycleId, billingQueueScheduleId);

                //SqlParameter[] parameterCollections = new SqlParameter[2];
                //parameterCollections[0] = new SqlParameter("CycleId", cycleId);
                //parameterCollections[1] = new SqlParameter("BillingQueuescheduleId", billingQueueScheduleId);
                //ds = _ObjCommonMethods.Insert_Get_DS("USP_InsertBillGenerationDetails_DEMO", parameterCollections);
                try
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            FileStream fs = new FileStream(filePathList, FileMode.OpenOrCreate, FileAccess.Write);
                            //set up a streamwriter for adding text
                            StreamWriter sw = new StreamWriter(fs);

                            //find the end of the underlying filestream
                            sw.BaseStream.Seek(0, SeekOrigin.End);
                            //string accountNumbers = string.Empty;
                            int rowCount = 0;
                            string billDetails = string.Empty;

                            foreach (DataRow dr in ds.Tables[0].Rows)
                            {
                                rowCount = rowCount + 1;

                                double previouseBalanceAmount = Convert.ToDouble(dr["PreviousBalance"]);
                                string previouseBalnce = string.Format(CultureInfo.InvariantCulture, "{0:#,#}", previouseBalanceAmount);

                                double adjustmentAmount = Convert.ToDouble(dr["AdjustmentAmmount"]);
                                string adjustment = string.Format(CultureInfo.InvariantCulture, "{0:#,#}", adjustmentAmount);

                                double netFixedChargesAmount = Convert.ToDouble(dr["NetFixedCharges"]);
                                string netFixedCharges = string.Format(CultureInfo.InvariantCulture, "{0:#,#}", netFixedChargesAmount);

                                double netArrearsAmount = Convert.ToDouble(dr["NetArrears"]);
                                string netArrears = string.Format(CultureInfo.InvariantCulture, "{0:#,#}", netArrearsAmount);

                                double netEnergyChargesAmount = Convert.ToDouble(dr["NetEnergyCharges"]);
                                string netEnergyCharges = string.Format(CultureInfo.InvariantCulture, "{0:#,#}", netEnergyChargesAmount);

                                double lastPaidAmount = Convert.ToDouble(dr["LastPaidAmount"]);
                                string lastPaidAmountValue = string.Format(CultureInfo.InvariantCulture, "{0:#,#}", netEnergyChargesAmount);


                                double vatAmount = Convert.ToDouble(dr["VAT"]);
                                string vat = string.Format(CultureInfo.InvariantCulture, "{0:#,#}", vatAmount);

                                double totalBillAmount = Convert.ToDouble(dr["TotalBillAmount"]);
                                string totalBill = string.Format(CultureInfo.InvariantCulture, "{0:#,#}", totalBillAmount);

                                double totalBillAmountWithTaxAmount = totalBillAmount + netArrearsAmount ; 
                                string totalBillAmountWithTax = string.Format(CultureInfo.InvariantCulture, "{0:#,#}", totalBillAmountWithTaxAmount);




                                billDetails += string.Format("{0,-6}#{1,-107}#{2,-11}", string.Empty, rowCount, rowCount).Replace('#', ' ') +
                                            Environment.NewLine + string.Format("{0,-15}#{1,-74}#{2,-11}#{3,-6}", "Route: 0", "Seq.", "Route: 0", "Seq.").Replace('#', ' ') +
                                            Environment.NewLine + Environment.NewLine +
                                            Environment.NewLine + string.Format("{0,-34}#{1,-63}#{2,-25}", string.Empty, dr["BusinessUnitName"].ToString() + " Business Unit", dr["BusinessUnitName"].ToString() + " Business Unit").Replace('#', ' ') +
                                            Environment.NewLine + string.Format("{0,-23}#{1,-74}#{2,-9}", string.Empty, "PLS PAY AT ANY BEDC OFFICE OR DESIGNATED BANKS", dr["MonthName"].ToString() + dr["BillYear"].ToString()).Replace('#', ' ') +
                                            Environment.NewLine + string.Format("{0,-31}#{1,-27}#{2,-36}#{3,-22}", string.Empty, "Private Account", dr["MonthName"].ToString() + dr["BillYear"].ToString(), "Private Account", dr["MonthName"].ToString() + dr["BillYear"].ToString()).Replace('#', ' ') +
                                             Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", "Bill No ", dr["BillNo"].ToString(), string.Empty, "Bill No", dr["BillNo"].ToString()).Replace('#', ' ') +
                                            Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, dr["AccountNo"].ToString(), string.Empty, string.Empty, dr["AccountNo"].ToString()).Replace('#', ' ') +
                                            Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, dr["Name"].ToString(), DateTime.Now.AddDays(5).ToString("dd/MM/yyyy"), previouseBalnce, dr["Name"].ToString()) +
                                            Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, dr["PostalHouseNo"].ToString() + " " + dr["PostalStreet"].ToString(), dr["MeterNo"].ToString(), netEnergyCharges, dr["PostalHouseNo"].ToString() + " " + dr["PostalStreet"].ToString() + " " + dr["PostalZipCode"].ToString())+
                                            Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, dr["PostalZipCode"].ToString(), netFixedCharges, adjustment, dr["ServiceHouseNo"].ToString() + " " + dr["ServiceStreet"].ToString()).Replace('#', ' ') +
                                            Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, string.Empty, dr["Dials"].ToString(), netArrears, dr["ServiceZipCode"].ToString()).Replace('#', ' ') +
                                            Environment.NewLine + string.Format("{0,8}#{1,-90}#{2,-15}", string.Empty, string.Format(Resource.BILLMONTH_MESSAGE, dr["PrevMonth"]), dr["MeterNo"].ToString()).Replace('#', ' ') +
                                            Environment.NewLine + Environment.NewLine + Environment.NewLine +
                                            Environment.NewLine + string.Format("{0,-13}#{1,-7}#{2,-16}#{3,-12}#{4,-7}#{5,-7}#{6,7}#{7,11}", "ENERGY", dr["TariffName"].ToString(), dr["ReadDate"].ToString(), dr["PresentReading"].ToString(), dr["PreviousReading"].ToString(), dr["Multiplier"].ToString(), dr["Usage"].ToString(), netEnergyCharges) +
                                            Environment.NewLine +
                                            Environment.NewLine + string.Format("{0,-13}#{1,-7}#{2,-16}#{3,-12}#{4,-7}#{5,-7}#{6,7}#{7,11}", "FIXED", dr["TariffName"].ToString(), "----", "----", "----", "----", "----", netFixedCharges).Replace('#', ' ') +
                                            Environment.NewLine + string.Format("{0,-22}#{1,-19}", string.Empty, "Billing Periods: 01").Replace('#', ' ') +
                                            Environment.NewLine +
                                            Environment.NewLine + string.Format("{0,-33}#{1,-22}", "LAST PAYMENT DATE  " + dr["LastPaymentDate"].ToString(), "AMOUNT      " + lastPaidAmountValue)+
                                            Environment.NewLine + string.Format("{0,-100}#{1,-11}", "RECONNECTION FEE IS NOW =N=5000", DateTime.Now.AddDays(5).ToString("dd/MM/yyyy")).Replace('#', ' ') +
                                            Environment.NewLine + string.Format("{0,-70}#{1,17}#{2,27}", "PAY WITHIN 7 DAYS TO AVOID DISCONNECTION", netArrears, netArrears )+
                                            Environment.NewLine + string.Format("{0,-70}#{1,17}#{2,27}", string.Empty, totalBill, totalBill )+
                                            Environment.NewLine + string.Format("{0,-70}#{1,17}#{2,27}", string.Empty, vat, vat )+
                                            Environment.NewLine +
                                            Environment.NewLine + string.Format("{0,-70}#{1,17}#{2,27}", string.Empty, totalBillAmountWithTax, totalBillAmountWithTax )+
                                            Environment.NewLine +
                                            Environment.NewLine + string.Format("{0,8}#{1,-19}#{2,-77}#{3,-16}", string.Empty, dr["OldAccountNo"].ToString(), "ENR2S- Rate:=N= 11.37", dr["OldAccountNo"].ToString()).Replace('#', ' ') +
                                            Environment.NewLine + string.Format("{0,-2}#{1,-60}", string.Empty, "THANKS FOR YOUR PATRONAGE").Replace('#', ' ') +
                                            Environment.NewLine + string.Format("{0,-5}#{1,-26}#{2,-61}", string.Empty, "- ENR2S- Rate:=N= 11.37", " Marketer:  ENOGHAYANGBON P.(07039738955)").Replace('#', ' ') +
                                            Environment.NewLine;
                            }
                            //add the text
                            sw.WriteLine(billDetails);
                            //add the text to the underlying filestream
                            sw.Flush();
                            //close the writer
                            sw.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                // Asynchronous Function Calling
                EmailSendFile delReferee = new EmailSendFile(_ObjCommonMethods.SendMail_BillGeneration);
                delReferee.BeginInvoke(filePathList, item.CycleName, feederId, item.MonthName, item.BillingYear, Session[UMS_Resource.SESSION_USER_EMAILID].ToString(), null, null);
                System.Threading.Thread.Sleep(500);
                //// _ObjCommonMethods.SendMail_BillGeneration(filePath, cycleId, feederId, item.MonthName, item.BillingYear, Session[UMS_Resource.SESSION_USER_EMAILID].ToString());

                _objUMS_Service.UpdateBillGenerationStatus(billingQueueScheduleId, (int)EnumBillGenarationStatus.EmailSent);
            }
            _objUMS_Service.UpdateBillGenFile(BillingQueueScheduleIdList, (int)EnumBillGenarationStatus.FileCreated, fileNameList);
        }

        private void CreateDirectory(string path)
        {
            bool exists = System.IO.Directory.Exists(path);

            if (!exists)
                System.IO.Directory.CreateDirectory(path);
        }

        private void UpdateBillGeneration()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                _objBillGenerationBe.BillingMonth = this.MonthId;
                _objBillGenerationBe.BillingYear = this.YearId;
                _objBillGenerationBe.BillSendingMode = this.BillSendingMode;
                _objBillGenerationBe.BillProcessTypeId = this.BillProcessType;
                _objBillGenerationBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                switch (this.BillProcessType)
                {
                    case (int)EnumBillProcessType.CycleWsie:
                        _objBillGenerationBe.CycleId = this.Cycles;// this.BillCycleFeederValue;
                        _objBillGenerationBe.FeederId = string.Empty; ;
                        //selectedProcessType = Resource.CYCLE;
                        break;
                    case (int)EnumBillProcessType.FeederWise:
                        _objBillGenerationBe.CycleId = string.Empty;
                        _objBillGenerationBe.FeederId = this.Cycles;//this.BillCycleFeederValue;
                        //selectedProcessType = Resource.FEEDER;
                        break;
                }

                xmlResult = _objBillGenerationBal.UpdateBillDetails(_objBillGenerationBe, Statement.Change);
                _objBillGenerationBe = _objIdsignBal.DeserializeFromXml<BillGenerationBe>(xmlResult);
                //if (_objBillGenerationBe.BillingQueueScheduleId > 0)
                //{
                //    EnableSteps(divStep3);
                //    ClearAllControls();
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        private BillGenerationBe IsBillExitsForSelectedMonth()
        {
            XmlDocument xmlResult = new XmlDocument();
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            _objBillGenerationBe.BillingMonth = this.MonthId;
            _objBillGenerationBe.BillingYear = this.YearId;
            _objBillGenerationBe.BillProcessTypeId = this.BillProcessType;
            switch (this.BillProcessType)
            {
                case (int)EnumBillProcessType.CycleWsie:
                    _objBillGenerationBe.CycleId = this.Cycles;//this.BillCycleFeederValue;
                    _objBillGenerationBe.FeederId = string.Empty;
                    break;
                case (int)EnumBillProcessType.FeederWise:
                    _objBillGenerationBe.CycleId = string.Empty; ;
                    _objBillGenerationBe.FeederId = this.Cycles;//this.BillCycleFeederValue;
                    break;
            }
            xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.Fetch);
            _objBillGenerationBe = _objIdsignBal.DeserializeFromXml<BillGenerationBe>(xmlResult);
            return _objBillGenerationBe;
        }

        private void BindCustomersDetails()
        {
            XmlDocument xmlResult = new XmlDocument();
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            BillGenerationListBe _objBillGenerationListBe = new BillGenerationListBe();
            switch (this.BillProcessType)
            {
                case (int)EnumBillProcessType.CycleWsie:
                    _objBillGenerationBe.CycleId = this.Cycles;// this.BillCycleFeederValue;
                    _objBillGenerationBe.FeederId = string.Empty; ;
                    break;
                case (int)EnumBillProcessType.FeederWise:
                    _objBillGenerationBe.CycleId = string.Empty;
                    _objBillGenerationBe.FeederId = this.Cycles;//this.BillCycleFeederValue;
                    break;
            }
            _objBillGenerationBe.Year = this.YearId;
            _objBillGenerationBe.Month = this.MonthId;
            xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.Retrieve);
            _objBillGenerationListBe = _objIdsignBal.DeserializeFromXml<BillGenerationListBe>(xmlResult);
            //lblTotal.Text = _objBillGenerationListBe.Items.Count.ToString();
            gvCustomerDetails.DataSource = _objBillGenerationListBe.Items;
            gvCustomerDetails.DataBind();

            ((Label)gvCustomerDetails.FooterRow.FindControl("lblTotalCustomers")).Text = lblTotal.Text = (from x in _objBillGenerationListBe.Items.AsEnumerable() select x.TotalRecords).Sum().ToString();
            ((Label)gvCustomerDetails.FooterRow.FindControl("lblTotalUsage")).Text = (from x in _objBillGenerationListBe.Items.AsEnumerable() select x.Usage).Sum().ToString();

        }

        private void EnableSteps(System.Web.UI.HtmlControls.HtmlGenericControl divStep)
        {
            //divStep1.Visible = divStep2.Visible = divStep3.Visible = false;
            divStep1.Visible = divStep2.Visible = divBillingMessage.Visible = false;
            divStep.Visible = true;
        }

        private bool IsCustomersExists()
        {
            XmlDocument xmlResult = new XmlDocument();
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            switch (this.BillProcessType)
            {
                case (int)EnumBillProcessType.CycleWsie:
                    _objBillGenerationBe.CycleId = this.Cycles;//this.BillCycleFeederValue;
                    _objBillGenerationBe.FeederId = string.Empty;
                    break;
                case (int)EnumBillProcessType.FeederWise:
                    _objBillGenerationBe.CycleId = string.Empty; ;
                    _objBillGenerationBe.FeederId = this.Cycles;//this.BillCycleFeederValue;
                    break;
            }
            xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.Check);
            _objBillGenerationBe = _objIdsignBal.DeserializeFromXml<BillGenerationBe>(xmlResult);
            return _objBillGenerationBe.IsExists;
        }

        //private void BindServiceUnits()
        //{
        //    _ObjCommonMethods.BindServiceUnits(cblServiceUnits, Session[UMS_Resource.SESSION_USER_BUID].ToString(), true);
        //    ddlServiceCenters.Items.Clear();
        //    cblCycles.Items.Clear();
        //    ddlServiceCenters.Enabled = false;
        //    //if (ddlServiceUnits.SelectedIndex > 0)  commented by karthik
        //    //    ddlServiceUnits_SelectedIndexChanged(this, null);
        //}

        private void BindServiceUnits()
        {
            string BU_Id = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
            _ObjCommonMethods.BindServiceUnits(cblServiceUnits, BU_Id, true);
            cblServiceCenters.Items.Clear();
            cblCycles.Items.Clear();
            divServiceCenters.Visible = divCycles.Visible = false;
            //if (ddlServiceUnits.SelectedIndex > 0)  commented by karthik
            //    ddlServiceUnits_SelectedIndexChanged(this, null);
        }

        //public void BindSU_BY_BU(Control ctrl, string su_Ids, string selectText, bool isSelectFirstItem)
        //{
        //    MastersBE _ObjMastersBE = new MastersBE();
        //    MastersListBE objMastersListBE = new MastersListBE();
        //    MastersBAL objMastersBAL = new MastersBAL();

        //    XmlDocument xmlResult = new XmlDocument();
        //    _ObjMastersBE.SU_ID = su_Ids;
        //    _ObjMastersBE.UserId = HttpContext.Current.Session[UMS_Resource.SESSION_LOGINID].ToString();
        //    xmlResult = objMastersBAL.BindCycleByBU(_ObjMastersBE);
        //    if (xmlResult != null)
        //    {
        //        objMastersListBE = _objIdsignBal.DeserializeFromXml<MastersListBE>(xmlResult);
        //        if (objMastersListBE.Items.Count > 0)
        //        {

        //            cblCycles.DataValueField = "ServiceCenterId";
        //            cblCycles.DataTextField = "ServiceCenterName";
        //            cblCycles.DataSource = objMastersListBE.Items;
        //            cblCycles.DataBind();
        //            foreach (ListItem lstItem in cblServiceCenters.Items)
        //            {
        //                //var count = objMastersListBE.Items.Where(x => x.ServiceCenterId == lstItem.Value).Select(y => y.BillGenCount).ToList();
        //                var masterList = from x in objMastersListBE.Items
        //                                             where x.ServiceCenterId == lstItem.Value
        //                                             select new 
        //                                       {
        //                                          BillGenCount= x.BillGenCount,
        //                                          PaymentCount=x.PaymentCount
        //                                       };
        //                foreach(var s in masterList)
        //                {
        //                    if (s.BillGenCount > 0)
        //                    {
        //                        lstItem.Enabled = false;
        //                        lstItem.Attributes.Add("", "");
        //                    }
        //                }
        //            }
        //        }
        //    }

        //}
        public void BindCY_BY_BU(Control ctrl, string BU_ID, string SU_ID, string ServiceCenterId, bool IsRequiredSelect, string selectText, bool selectFirstItem) //This Method and BindCyclesList methods are same only diff is select value passing from aspx.cs page
        {
            try
            {
                string[] CycleList = null;
                if (Cycles != "")
                {
                    CycleList = Cycles.Split(',');
                }
                BindYears();
                MastersBE _ObjMastersBE = new MastersBE();
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBAL objMastersBAL = new MastersBAL();

                XmlDocument xmlResult = new XmlDocument();
                _ObjMastersBE.BU_ID = BU_ID;
                _ObjMastersBE.SU_ID = SU_ID;
                _ObjMastersBE.BillYear = YearId.ToString();
                _ObjMastersBE.BillMonth = MonthId.ToString();
                _ObjMastersBE.ServiceCenterId = ServiceCenterId;
                xmlResult = objMastersBAL.BindCycleByBU(_ObjMastersBE);
                if (xmlResult != null)
                {
                    objMastersListBE = _objIdsignBal.DeserializeFromXml<MastersListBE>(xmlResult);
                    if (objMastersListBE.Items.Count > 0)
                    {

                        cblCycles.DataValueField = "CycleId";
                        cblCycles.DataTextField = "CycleName";
                        cblCycles.DataSource = objMastersListBE.Items;
                        cblCycles.DataBind();
                        foreach (ListItem lstItem in cblCycles.Items)
                        {
                            //var count = objMastersListBE.Items.Where(x => x.ServiceCenterId == lstItem.Value).Select(y => y.BillGenCount).ToList();
                            var masterList = from x in objMastersListBE.Items
                                             where x.CycleId == lstItem.Value
                                             select new
                                             {
                                                 BillGenCount = x.BillGenCount,
                                                 PaymentCount = x.PaymentCount,
                                                 BillAdjustmentCount = x.BillAdjustmentCount,
                                                 TotalCustomers = x.TotalCustomers
                                             };
                            foreach (var BookGroupList in masterList)
                            {
                                if (BookGroupList.PaymentCount > 0 && BookGroupList.BillAdjustmentCount > 0)
                                {
                                    lstItem.Enabled = false;
                                    lstItem.Attributes.Add("title", "Bill Generated, Payment and Adjustment Received.");
                                    lstItem.Attributes.Add("Style", "color: #FF8000;");
                                }
                                else if (BookGroupList.PaymentCount > 0)
                                {
                                    lstItem.Enabled = false;
                                    lstItem.Attributes.Add("title", "Bill Generated and Payment Received.");
                                    lstItem.Attributes.Add("Style", "color: #B40486;");
                                }
                                else if (BookGroupList.BillAdjustmentCount > 0)
                                {
                                    lstItem.Enabled = false;
                                    lstItem.Attributes.Add("title", "Bill Generated and Adjustment Received.");
                                    lstItem.Attributes.Add("Style", "color: #58ACFA;");
                                }
                                else if (BookGroupList.TotalCustomers == 0)
                                {
                                    lstItem.Enabled = false;
                                    lstItem.Attributes.Add("title", "No Customer Is Available.");
                                    lstItem.Attributes.Add("Style", "color: ThreeDShadow;");
                                }
                                else if (BookGroupList.BillGenCount > 0)
                                {
                                    lstItem.Enabled = true;
                                    lstItem.Attributes.Add("title", "Bill Generated.");
                                    lstItem.Attributes.Add("Style", "color: #FE2E64;");
                                }
                            }
                            if (CycleList != null)
                            {
                                if (!string.IsNullOrEmpty(Array.Find(CycleList, element => element.Equals(lstItem.Value))))
                                {
                                    lstItem.Selected = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                Message("Sorry cannot bind service center. There is some error.", UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        public int GenerateCustomersBills()
        {
            divCycles.Visible = false;
            CommonMethods _ObjCommonMethods = new CommonMethods();
            UMS_Service _objUMS_Service = new UMS_Service();
            iDsignBAL _objiDsignBAL = new iDsignBAL();
            BillGenerationListBe _objBillGenerationListBe = new BillGenerationListBe();
            BillGenerationBe objBillGenerationBe = new BillGenerationBe();
            XmlDocument xml = _objUMS_Service.GetBillGenerationDeatils_ByServiceStartDate();
            DataSet ds = new DataSet();
            int RowsEffected = 0;
            try
            {
                _objBillGenerationListBe = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xml);
                if (_objBillGenerationListBe.Items.Count > 0)
                {
                    string CycleID = null;

                    foreach (BillGenerationBe item in _objBillGenerationListBe.Items)
                    {
                        CycleID = CycleID + item.CycleId + "|";
                        BillingQueueScheduleIdList = BillingQueueScheduleIdList + item.BillingQueueScheduleId + "|";
                    }

                    #region Generate bill without fill writing
                    ds = _objUMS_Service.GenerateBill_ByBillQueueDetailsV1(CycleID, BillingQueueScheduleIdList);
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            //BillGenerationListBe _objBillGenerationListBeFinal = new BillGenerationListBe();
                            //_objBillGenerationListBeFinal = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(XMLBillList);
                            //ds = _objiDsignBAL.ConvertListToDataSet<BillGenerationBe>(_objBillGenerationListBeFinal.Items);

                            //Follwoing statement will update bill in queue status to Bill Generation Completed
                            _objUMS_Service.UpdateBillGenFile(BillingQueueScheduleIdList, (int)EnumBillGenarationStatus.BillingGenarationCompleted);
                            RowsEffected = ds.Tables[0].Rows.Count;
                            CreateCustomersBillFile(ds);
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
            //return objBillGenerationBe.RowsEffected;            
            return RowsEffected;
        }//Neeraj ID077

        private void CreateCustomersBillFile(DataSet ds)
        {
            CommonMethods _ObjCommonMethods = new CommonMethods();
            UMS_Service _objUMS_Service = new UMS_Service();
            iDsignBAL _objiDsignBAL = new iDsignBAL();
            BillGenerationListBe _objBillGenerationListBe = new BillGenerationListBe();
            string fileName = null;
            string filePath = null;
            XmlDocument xml = _objUMS_Service.GetBillGenerationDeatils_ByServiceStartDate();
            string CurrentCycleId = null;
            string CurrentLoopCycleId = null;
            _objBillGenerationListBe = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xml);

            string feederId = string.Empty;
            string cycleId = string.Empty;
            string CycleList = string.Empty;
            string BillMonthList = string.Empty;
            string BillYearList = string.Empty;
            int billingQueueScheduleId = 0;
            int EmilCount = 0;
            foreach (BillGenerationBe item in _objBillGenerationListBe.Items)
            {
                if (item.CycleId != null)
                    cycleId = item.CycleId;
                if (item.FeederId != null)
                    feederId = item.FeederId;

                billingQueueScheduleId = item.BillingQueueScheduleId;
                CycleList += item.CycleId;
                BillMonthList += item.BillMonth + "|";
                BillYearList += item.BillYear + "|";

                string BUPath = Server.MapPath(ConfigurationManager.AppSettings["BillGeneration"].ToString()) + item.BusinessUnitName + "/";
                string SUPath = BUPath + item.ServiceUnitName + "/";
                string SCPath = SUPath + item.ServiceCenterName + "/";
                string MonthPath = SCPath + item.MonthName + item.BillingYear.ToString() + "/";

                if (!string.IsNullOrEmpty(item.BusinessUnitName))
                {
                    CreateDirectory(BUPath);// Create BU Directory
                }
                if (!string.IsNullOrEmpty(item.ServiceUnitName))
                {
                    CreateDirectory(SUPath);// Create SU Directory
                }
                if (!string.IsNullOrEmpty(item.ServiceCenterName))
                {
                    CreateDirectory(SCPath);// Create SC Directory
                }
                if (!string.IsNullOrEmpty(item.MonthName))
                {
                    CreateDirectory(MonthPath);// Create Month Directory
                }

                fileName = item.CycleName + ".txt"; //+"_" + DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".txt"; //Modification Req
                //set up a filestream
                //string filePath = Server.MapPath(ConfigurationManager.AppSettings["BillGeneration"].ToString()) + Path.GetFileName(fileName);
                filePath = MonthPath + fileName;

                if (File.Exists(filePath))
                {
                    try
                    {
                        File.Delete(filePath);
                    }
                    catch (Exception ex)
                    {
                        _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                    }
                }
                try
                {
                    // Modification Get Customers Bills By Passing Service Center.
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {

                            FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
                            //set up a streamwriter for adding text
                            StreamWriter sw = new StreamWriter(fs);

                            //find the end of the underlying filestream
                            sw.BaseStream.Seek(0, SeekOrigin.End);
                            //string accountNumbers = string.Empty;
                            int rowCount = 0;
                            string billDetails = string.Empty;
                            EmilCount++;
                            foreach (DataRow dr in ds.Tables[0].Rows)
                            {
                                CurrentCycleId = Convert.ToString(dr["CycleId"]).Trim();
                                CurrentLoopCycleId = item.CycleId.Trim();

                                if (CurrentCycleId == CurrentLoopCycleId)//Modification Remove Checking
                                {
                                    rowCount = rowCount + 1;

                                    double previouseBalanceAmount = Convert.ToDouble(dr["PreviousBalance"]);
                                    string previouseBalnce = string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", previouseBalanceAmount);


                                    string str = null;
                                    string[] strArr = null;
                                    str = dr["AdjustmentAmmount"].ToString().TrimEnd();
                                    char[] splitchar = { ' ' };
                                    strArr = str.Split(splitchar);
                                    string adjustment="0.00";
                                    double adjustmentAmount = Convert.ToDouble(strArr[0]);
                                    if(adjustmentAmount != 0)
                                     adjustment = string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", adjustmentAmount) + " " +strArr[1];

                                    //if (strArr[1] == "DR")
                                    //{
                                    //    adjustmentAmount = adjustmentAmount * -1;
                                    //}


                                    string totalPayment = "0.00";
                                    double totalPaymentAmount=0;
                                    if (!string.IsNullOrWhiteSpace(dr["TotalPayment"].ToString()))
                                    {
                                        totalPaymentAmount = Convert.ToDouble(dr["TotalPayment"]);
                                        totalPayment = string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", totalPaymentAmount);
                                    }

                                    double netFixedChargesAmount = Convert.ToDouble(dr["NetFixedCharges"]);
                                    string netFixedCharges = string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", netFixedChargesAmount);

                                    double netArrearsAmount = previouseBalanceAmount - adjustmentAmount - totalPaymentAmount; 
                                    string netArrears = string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", netArrearsAmount);

                                    double netEnergyChargesAmount = Convert.ToDouble(dr["NetEnergyCharges"]);
                                    string netEnergyCharges = string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", netEnergyChargesAmount);

                                    //double lastPaidAmount = Convert.ToDouble(dr["LastPaidAmount"]);
                                    //string lastPaidAmountValue = string.Format(CultureInfo.InvariantCulture, "{0:#,#}", netEnergyChargesAmount);

                                    double vatAmount = Convert.ToDouble(dr["VAT"]);
                                    string vat = string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", vatAmount);

                                    double totalBillAmount = Convert.ToDouble(dr["TotalBillAmount"]);
                                    string totalBill = string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", totalBillAmount);

                                    double totalBillAmountWithTaxAmount = totalBillAmount + netArrearsAmount+vatAmount;
                                    string totalBillAmountWithTax = string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", totalBillAmountWithTaxAmount);


                                    //accountNumbers = string.IsNullOrEmpty(accountNumbers) ? dr["AccountNo"].ToString() : accountNumbers + "," + dr["AccountNo"].ToString();
                                    billDetails += string.Format("{0,-6}#{1,-107}#{2,-11}", string.Empty, rowCount, rowCount).Replace('#', ' ') +
                                                Environment.NewLine + string.Format("{0,-15}#{1,-74}#{2,-11}#{3,-6}", "Route: 0", "Seq.", "Route: 0", "Seq.").Replace('#', ' ') +
                                                Environment.NewLine + Environment.NewLine +
                                                Environment.NewLine + string.Format("{0,-34}#{1,-63}#{2,-25}", string.Empty, dr["BusinessUnitName"].ToString() + " Business Unit", dr["BusinessUnitName"].ToString() + " Business Unit").Replace('#', ' ') +
                                                Environment.NewLine + string.Format("{0,-23}#{1,-74}#{2,-9}", string.Empty, "PLS PAY AT ANY BEDC OFFICE OR DESIGNATED BANKS", dr["MonthName"].ToString() + dr["BillYear"].ToString()).Replace('#', ' ') +
                                                Environment.NewLine + string.Format("{0,-31}#{1,-27}#{2,-36}#{3,-22}", string.Empty, "Private Account", dr["MonthName"].ToString() + dr["BillYear"].ToString(), "Private Account", dr["MonthName"].ToString() + dr["BillYear"].ToString()).Replace('#', ' ') +
                                                 Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", "Bill No ", dr["BillNo"].ToString(), string.Empty, "Bill No", dr["BillNo"].ToString()).Replace('#', ' ') +
                                                Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, dr["AccountNo"].ToString(), string.Empty, string.Empty, dr["AccountNo"].ToString()).Replace('#', ' ') +
                                                Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, dr["Name"].ToString(), DateTime.Now.AddDays(5).ToString("dd/MM/yyyy"), previouseBalnce, dr["Name"].ToString()).Replace('#', ' ') +
                                                Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, dr["ServiceAddress"].ToString() + " ", dr["MeterNo"].ToString(), totalPayment, dr["PostalAddress"].ToString()).Replace('#', ' ') +
                                                Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, dr["PostalZipCode"].ToString(), netFixedCharges, adjustment, dr["ServiceHouseNo"].ToString() + " " + dr["ServiceStreet"].ToString()).Replace('#', ' ') +
                                                Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, string.Empty, dr["Dials"].ToString(), netArrears, dr["ServiceZipCode"].ToString()).Replace('#', ' ')  +
                                                Environment.NewLine + string.Format("{0,8}#{1,-90}#{2,-15}", string.Empty, string.Format(Resource.BILLMONTH_MESSAGE, dr["PrevMonth"]), dr["MeterNo"].ToString()).Replace('#', ' ') +
                                                Environment.NewLine + Environment.NewLine + Environment.NewLine +
                                                Environment.NewLine + string.Format("{0,-13}#{1,-7}#{2,-16}#{3,-12}#{4,-7}#{5,-7}#{6,7}#{7,11}", "ENERGY", dr["TariffName"].ToString(), dr["ReadDate"].ToString(), dr["PresentReading"].ToString(), dr["PreviousReading"].ToString(), dr["Multiplier"].ToString(), dr["Usage"].ToString(), netEnergyCharges).Replace('#', ' ') +
                                                Environment.NewLine +
                                                Environment.NewLine + string.Format("{0,-13}#{1,-7}#{2,-16}#{3,-12}#{4,-7}#{5,-7}#{6,7}#{7,11}", "FIXED", dr["TariffName"].ToString(), "----", "----", "----", "----", "----", netFixedCharges).Replace('#', ' ')  +
                                                Environment.NewLine + string.Format("{0,-22}#{1,-19}", string.Empty, "Billing Periods: 01").Replace('#', ' ') +
                                                Environment.NewLine +
                                                Environment.NewLine + string.Format("{0,-33}#{1,-22}", "LAST PAYMENT DATE  " + dr["LastPaymentDate"].ToString(), "AMOUNT      " + dr["LastPaidAmount"].ToString()).Replace('#', ' ') +
                                                Environment.NewLine + string.Format("{0,-100}#{1,-11}", "RECONNECTION FEE IS NOW =N=5000", DateTime.Now.AddDays(5).ToString("dd/MM/yyyy")).Replace('#', ' ') +
                                                Environment.NewLine + string.Format("{0,-70}#{1,17}#{2,27}", "PAY WITHIN 7 DAYS TO AVOID DISCONNECTION", netArrears, netArrears).Replace('#', ' ')  +
                                                Environment.NewLine + string.Format("{0,-70}#{1,17}#{2,27}", string.Empty, totalBill, totalBill).Replace('#', ' ')  +
                                                Environment.NewLine + string.Format("{0,-70}#{1,17}#{2,27}", string.Empty, vat, vat).Replace('#', ' ')  +
                                                Environment.NewLine +
                                                Environment.NewLine + string.Format("{0,-70}#{1,17}#{2,27}", string.Empty, totalBillAmountWithTax, totalBillAmountWithTax).Replace('#', ' ')  +
                                                Environment.NewLine +
                                                Environment.NewLine + string.Format("{0,8}#{1,-19}#{2,-77}#{3,-16}", string.Empty, dr["OldAccountNo"].ToString(), "ENR2S- Rate:=N= 11.37", dr["OldAccountNo"].ToString()).Replace('#', ' ') +
                                                Environment.NewLine + string.Format("{0,-2}#{1,-60}", string.Empty, "THANKS FOR YOUR PATRONAGE").Replace('#', ' ') +
                                                Environment.NewLine + string.Format("{0,-5}#{1,-26}#{2,-61}", string.Empty, "- ENR2S- Rate:=N= 11.37", " Marketer:  ENOGHAYANGBON P.(07039738955)").Replace('#', ' ') +
                                                Environment.NewLine;
                                    //+ Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine +
                                    // "-------------------------------------------------------------------------------------------------------------------" + Environment.NewLine;
                                }
                            }

                            //add the text
                            sw.WriteLine(billDetails.ToUpper());
                            //add the text to the underlying filestream

                            sw.Flush();
                            //close the writer
                            sw.Close();

                        }
                    }
                }
                catch (Exception ex)
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                    IsFileGenValid = false;
                }
                if (IsFileGenValid)
                {
                    fileNameList += fileName + "|";
                    filePathList += "|" + filePath;
                    fileName = string.Empty;
                    filePath = string.Empty;
                }

            }
            //Follwoing statement will update Billing in queue status for file generated.
            if (BillingQueueScheduleIdList.Length > 0)
                _objUMS_Service.UpdateBillGenFile(BillingQueueScheduleIdList, (int)EnumBillGenarationStatus.FileCreated, fileNameList);

            //Follwoing statement will send bill attachment file with details.
            EmailSend(filePathList, CycleList, BillMonthList, BillYearList, BillingQueueScheduleIdList, EmilCount);

        }//Neeraj ID077

        private void EmailSend(string fileList, string CycleName, string BillMonth, string BillYear, string BillingQueueScheduleIdList, int Count)//Neeraj ID077
        {
            try
            {
                UMS_Service _objUMS_Service = new UMS_Service();

                string[] file = fileList.Split('|');
                file = file.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                string[] Cycle = CycleName.Split('|');
                Cycle = Cycle.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                string[] BMonth = BillMonth.Split('|');
                BMonth = BMonth.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                string[] BYear = BillYear.Split('|');
                BYear = BYear.Where(x => !string.IsNullOrEmpty(x)).ToArray();

                if (file.Length == Count && Cycle.Length == Count && BMonth.Length == Count && BYear.Length == Count)
                {
                    for (int k = 0; k < Count; k++)
                    {
                        // Asynchronous Function Calling
                        EmailSendFile delReferee = new EmailSendFile(_ObjCommonMethods.SendMail_BillGeneration);
                        delReferee.BeginInvoke(file[k], Cycle[k], "", BMonth[k], Convert.ToInt32(BYear[k]), Session[UMS_Resource.SESSION_USER_EMAILID].ToString(), null, null);
                        System.Threading.Thread.Sleep(500);
                    }
                    if (BillingQueueScheduleIdList.Length > 0)
                    {
                        //Follwoing statement will update Billing in queue status for mail sent.
                        _objUMS_Service.UpdateBillGenFile(BillingQueueScheduleIdList, (int)EnumBillGenarationStatus.EmailSent);
                    }
                    //Follwoing statement will update Billing in queue status for Bill in queue close.
                    _objUMS_Service.UpdateBillGenFile(BillingQueueScheduleIdList, (int)EnumBillGenarationStatus.BillingQueeClosed);
                }
            }
            catch (Exception ex)
            {
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }
        //private void SendMailsTo
        #endregion

        #region WebMethods

        //[WebMethod(EnableSession = true)]
        public bool CheckIsReadingDone(int billingMonth, int billingYear, string cycleId)
        {
            string InValidCycles = string.Empty;
            XmlDocument xmlResult = new XmlDocument();
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            BillGenerationListBe _objBillGenerationListBe = new BillGenerationListBe();
            BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
            iDsignBAL _objiDsignBAL = new iDsignBAL();

            _objBillGenerationBe.CycleId = cycleId;
            _objBillGenerationBe.BillingMonth = billingMonth;
            _objBillGenerationBe.BillingYear = billingYear;

            xmlResult = _objBillGenerationBal.CheckBillValidations(_objBillGenerationBe, ReturnType.Get);
            _objBillGenerationListBe = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);

            if (_objBillGenerationListBe.Items.Count > 0)
            {
                if (!_objBillGenerationListBe.Items[0].IsExists)
                {
                    InValidCycles = string.Join(",", (from item in _objBillGenerationListBe.Items
                                                      select item.CycleName));
                }
            }

            if (!string.IsNullOrEmpty(InValidCycles))
            {
                divInValidReadings.Visible = true;
                lblReadingsCycles.Text = InValidCycles;
                return false;
            }
            else
            {
                divInValidReadings.Visible = false;
                lblReadingsCycles.Text = string.Empty;
                return true;
            }
            //return InValidCycles;
        }



        //[WebMethod(EnableSession = true)]
        public bool CheckPaymentBatchClose(int billingMonth, int billingYear)
        {
            string InValidCycles = string.Empty;
            XmlDocument xmlResult = new XmlDocument();
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
            iDsignBAL _objiDsignBAL = new iDsignBAL();

            _objBillGenerationBe.BillingMonth = billingMonth;
            _objBillGenerationBe.BillingYear = billingYear;

            xmlResult = _objBillGenerationBal.CheckBillValidations(_objBillGenerationBe, ReturnType.Fetch);
            _objBillGenerationBe = _objiDsignBAL.DeserializeFromXml<BillGenerationBe>(xmlResult);

            if (_objBillGenerationBe.IsExists)
            {
                divInValidPaymentBatches.Visible = true;
                lblPaymentBatches.Text = _objBillGenerationBe.OpenBatches;
                return false;
            }
            else
            {
                divInValidPaymentBatches.Visible = false;
                lblPaymentBatches.Text = string.Empty;
                return true;
            }

            //return _objBillGenerationBe.IsExists.ToString().ToLower() + "|" + _objBillGenerationBe.OpenBatches;
        }

        //[WebMethod(EnableSession = true)]
        public bool CheckAdjustmentBatchClose(int billingMonth, int billingYear)
        {
            string InValidCycles = string.Empty;
            XmlDocument xmlResult = new XmlDocument();
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
            iDsignBAL _objiDsignBAL = new iDsignBAL();

            _objBillGenerationBe.BillingMonth = billingMonth;
            _objBillGenerationBe.BillingYear = billingYear;

            xmlResult = _objBillGenerationBal.CheckBillValidations(_objBillGenerationBe, ReturnType.Single);
            _objBillGenerationBe = _objiDsignBAL.DeserializeFromXml<BillGenerationBe>(xmlResult);

            if (_objBillGenerationBe.IsExists)
            {
                divInvalidAdustmentBatches.Visible = true;
                lblAdjustmentBatches.Text = _objBillGenerationBe.OpenBatches;
                return false;
            }
            else
            {
                divInvalidAdustmentBatches.Visible = false;
                lblAdjustmentBatches.Text = string.Empty;
                return true;
            }

            //return _objBillGenerationBe.IsExists.ToString().ToLower() + "|" + _objBillGenerationBe.OpenBatches;
        }

        //[WebMethod(EnableSession = true)]
        public bool CheckIsEstimationDone(int billingMonth, int billingYear, string cycleId)
        {
            string InValidCycles = string.Empty;
            XmlDocument xmlResult = new XmlDocument();
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            BillGenerationListBe _objBillGenerationListBe = new BillGenerationListBe();
            BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
            iDsignBAL _objiDsignBAL = new iDsignBAL();

            _objBillGenerationBe.CycleId = cycleId;
            _objBillGenerationBe.BillingMonth = billingMonth;
            _objBillGenerationBe.BillingYear = billingYear;

            xmlResult = _objBillGenerationBal.CheckBillValidations(_objBillGenerationBe, ReturnType.Check);
            _objBillGenerationListBe = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);

            if (_objBillGenerationListBe.Items.Count > 0)
            {
                if (!_objBillGenerationListBe.Items[0].IsExists)
                {
                    InValidCycles = string.Join(",", (from item in _objBillGenerationListBe.Items
                                                      select item.CycleName));
                }
            }

            if (!string.IsNullOrEmpty(InValidCycles))
            {
                divEstimationValid.Visible = true;
                lblInValidEstimation.Text = InValidCycles;
                return false;
            }
            else
            {
                divEstimationValid.Visible = false;
                lblInValidEstimation.Text = string.Empty;
                return true;
            }

            //return InValidCycles;
        }

        #endregion
    }
}