﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/EDMUMS.Master" AutoEventWireup="true"
    Title="::Customer Bill Generation::" CodeBehind="GenerateCustomerBill.aspx.cs"
    Inherits="UMS_Nigeria.Billing.GenerateCustomerBill" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" AssociatedUpdatePanelID="UpdatePanelTotal">
        <ProgressTemplate>
            <center>
                <div id="Div1" class="pbloading">
                    <div id="Div2" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="UpdatePanelTotal" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
            <div class="consumer_feild">
                <div class="consume_name" style="width: 179px;">
                    <asp:Literal ID="Literal48" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NUMBER%>"></asp:Literal>
                </div>
                <div class="consume_input">
                    <asp:TextBox ID="txtAccountNo" runat="server" onkeypress="return IsAccountNoChange()"></asp:TextBox>
                    <span id="spanAccountNo" class="spancolor"></span>
                </div>
                <div class="consume_name" style="width: 200px;">
                    <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:Resource, BILLING_MONTHS%>"></asp:Literal>
                </div>
                <div class="consume_input c_left" style="width: 210px;">
                    <asp:DropDownList Style="width: 80px; float: left;" ID="ddlYear" runat="server" AutoPostBack="true"
                        onchange="ddl_changed()" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                    </asp:DropDownList>
                    <span class="pad_5"></span>
                    <asp:DropDownList ID="ddlMonth" Style="width: 100px; float: left; margin-right: 10px;
                        margin-left: 8px;" runat="server">
                        <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                    </asp:DropDownList>
                    <span id="spanYear" class="spancolor"></span><span id="spanMonth" class="spancolor">
                    </span>
                </div>
                <div>
                    <asp:Button ID="btnGo" runat="server" ToolTip="Edit Customer" CssClass="calculate_but"
                        OnClientClick="return ValidateDetails()" Text="<%$ Resources:Resource, GO_BUTTON%>"
                        OnClick="btnGo_Click" />
                </div>
            </div>
            <div id="divCustomerDetails" runat="server">
                <div class="edmcontainer">
                    <h3>
                        <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, CUSTOMER_DETAILS%>"></asp:Literal>
                    </h3>
                    <div class="pad_10">
                    </div>
                    <div class="consumer_feild search_unique_total">
                        <div style="text-align: left; width: 160px;" class="consume_name c_left">
                            <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, CUSTOMER_UNIQUE_NO%>"></asp:Literal>
                        </div>
                        <div class="fltl">
                            :</div>
                        <div class="consume_input">
                            <asp:Label ID="lblCustomerUniqueNo" runat="server" Text="----"></asp:Label>
                        </div>
                        <div class="consume_name c_left" style="text-align: left; width: 160px;">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                        </div>
                        <div class="fltl">
                            :</div>
                        <div class="consume_input">
                            <asp:Label ID="lblAccountNo" runat="server" Text="----"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name c_left" style="text-align: left; width: 160px;">
                            <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                        </div>
                        <div class="fltl">
                            :</div>
                        <div class="consume_input">
                            <asp:Label ID="lblName" runat="server" Text="----"></asp:Label>
                        </div>
                        <div class="consume_name c_left" style="text-align: left; width: 160px;">
                            <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, METERNO%>"></asp:Literal>
                        </div>
                        <div class="fltl">
                            :</div>
                        <div class="consume_input">
                            <asp:Label ID="lblMeterNo" runat="server" Text="----"></asp:Label>
                        </div>
                        <div class="consume_name c_left" style="text-align: left; width: 160px;">
                            <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource,READCODE %>"></asp:Literal>
                        </div>
                        <div class="fltl">
                            :</div>
                        <div class="consume_input">
                            <asp:HiddenField ID="hfReadCodeId" runat="server" />
                            <asp:Label ID="lblReadCode" runat="server" Text="----"></asp:Label>
                        </div>
                        <div>
                            <asp:HiddenField ID="hfIsBilled" runat="server" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name c_left" style="text-align: left; width: 160px;">
                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, PREVIOUS_READING%>"></asp:Literal>
                        </div>
                        <div class="fltl">
                            :</div>
                        <div class="consume_input">
                            <asp:Label ID="lblPreviousReading" runat="server" Text="----"></asp:Label>
                        </div>
                        <div class="consume_name c_left" style="text-align: left; width: 160px;">
                            <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, CURRENT_READING%>"></asp:Literal>
                        </div>
                        <div class="fltl">
                            :</div>
                        <div class="consume_input">
                            <asp:Label ID="lblPresentReading" runat="server" Text="----"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name c_left" style="text-align: left; width: 160px;">
                            <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, LATEST_READDATE%>"></asp:Literal>
                        </div>
                        <div class="fltl">
                            :</div>
                        <div class="consume_input">
                            <asp:Label ID="lblLatestReadDate" runat="server" Text="----"></asp:Label>
                        </div>
                        <div class="consume_name c_left" style="text-align: left; width: 160px;">
                            <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, LATEST_BILL_GENERATION%>"></asp:Literal>
                        </div>
                        <div class="fltl">
                            :</div>
                        <div class="consume_input">
                            <asp:Label ID="lblLastBillGeneration" runat="server" Text="----"></asp:Label>
                        </div>
                        <div class="consumer_total">
                            <div class="clear pad_10">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <asp:Button ID="btnGenerateBill" runat="server" ToolTip="Bill Generate" CssClass="calculate_but"
                    OnClientClick="return ValidateDetails()" Text="<%$ Resources:Resource, GENERATE_BILL%>"
                    OnClick="btnGenerateBill_Click" />
            </div>
            <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="pnlAlert" TargetControlID="btnThankyouOK"
                BackgroundCssClass="modalBackground" CancelControlID="btnThankyouOK">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlAlert" runat="server" DefaultButton="btnThankyouOK" CssClass="modalPopup"
                Style="display: none; border-color: #639B00;">
                <div class="popheader" style="background-color: Red;" id="divBillThankuMsg" runat="server">
                    <asp:Label ID="lblAlertMessage" runat="server" Text=""></asp:Label>
                </div>
                <br />
                <div class="footer" align="right">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="btnThankyouOK" runat="server" Text="<%$Resources:Resource, OK%>"
                            CssClass="btn_ok" Style="margin-left: 105px;" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="mpeConfirm" runat="server" PopupControlID="pnlConfirm"
                TargetControlID="btnConfirmOK" BackgroundCssClass="modalBackground" CancelControlID="btnConfirmOK">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlConfirm" runat="server" CssClass="modalPopup" Style="display: none;
                border-color: #CD3333;">
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanelTotal">
                    <ProgressTemplate>
                        <center>
                            <div id="loading-div" class="pbloading">
                                <div id="title-loading" class="pbtitle">
                                    BEDC</div>
                                <center>
                                    <img src="../images/loading.GIF" class="pbimg"></center>
                                <p class="pbtxt">
                                    Loading Please wait.....</p>
                            </div>
                        </center>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <div class="popheader" style="background-color: #CD3333;">
                    <asp:Label ID="lblConfirmMessage" runat="server"></asp:Label>
                </div>
                <div class="footer" align="right">
                    <asp:Button ID="btnReGenerate" runat="server" Text="<%$ Resources:Resource, REGENERATE%>"
                        CssClass="no" OnClick="btnReGenerate_Click" />
                    <asp:Button ID="btnConfirmOK" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                        CssClass="no" />
                </div>
            </asp:Panel>
            <asp:HiddenField ID="hfIsRegenerate" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">

        $(document).ready(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/GenerateCustomerBill.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
