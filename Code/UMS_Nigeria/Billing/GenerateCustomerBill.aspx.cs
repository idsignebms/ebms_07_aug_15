﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using iDSignHelper;
using Resources;
using System.Xml;
using UMS_NigriaDAL;
using UMS_NigeriaBE;
using System.IO;
using System.Configuration;

namespace UMS_Nigeria.Billing
{
    public partial class GenerateCustomerBill : System.Web.UI.Page
    {
        BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        UMS_Service _objUMS_Service = new UMS_Service();
        //string Key = UMS_Resource.KEY_METERREADING;

        #region Properties

        private int YearId
        {
            get { return string.IsNullOrEmpty(ddlYear.SelectedValue) ? 0 : Convert.ToInt32(ddlYear.SelectedValue); }
        }

        private int MonthId
        {
            get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? 0 : Convert.ToInt32(ddlMonth.SelectedValue); }
        }

        private string MonthName
        {
            get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? string.Empty : ddlMonth.SelectedItem.Text; }
        }
        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                btnGenerateBill.Attributes.Add("style", "display:none;");
                BindYears();
                BindMonths();
            }
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            hfIsRegenerate.Value = false.ToString();
            XmlDocument xml = new XmlDocument();
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            _objBillGenerationBe.AccountNo = txtAccountNo.Text;
            _objBillGenerationBe.BillingYear = this.YearId;
            _objBillGenerationBe.BillingMonth = this.MonthId;
            xml = _objBillGenerationBal.GetCustomerReadingDetails(_objBillGenerationBe, ReturnType.Multiple);
            _objBillGenerationBe = _ObjiDsignBAL.DeserializeFromXml<BillGenerationBe>(xml);
            if (_objBillGenerationBe != null)
            {
                if (_objBillGenerationBe.MeterTypeId == (int)EnumMeterTypes.MD)
                {
                    lblAlertMessage.Text = Resource.NONMD_SELECTION_MESSAGE;
                    mpeAlert.Show();
                }
                else
                {
                    lblCustomerUniqueNo.Text = _objBillGenerationBe.CustomerId;
                    lblAccountNo.Text = _objBillGenerationBe.AccountNo;
                    lblName.Text = _objBillGenerationBe.Name;
                    lblReadCode.Text = _objBillGenerationBe.ReadCode;
                    hfReadCodeId.Value = _objBillGenerationBe.ReadCodeId.ToString();
                    lblPreviousReading.Text = _objBillGenerationBe.PreviousReading;
                    lblPresentReading.Text = _objBillGenerationBe.PresentReading;
                    lblMeterNo.Text = _objBillGenerationBe.MeterNo;
                    lblLatestReadDate.Text = _objBillGenerationBe.ReadDate;
                    lblLastBillGeneration.Text = _objBillGenerationBe.LastPaymentDate;
                    hfIsBilled.Value = _objBillGenerationBe.IsBilled.ToString();
                    btnGenerateBill.Attributes.Add("style", "display:inline;");
                }
            }
            else
            {
                lblCustomerUniqueNo.Text = lblAccountNo.Text = lblName.Text = lblPreviousReading.Text = lblPresentReading.Text
                                                   = lblReadCode.Text = lblMeterNo.Text = lblLatestReadDate.Text = lblLastBillGeneration.Text = string.Empty;
                lblAlertMessage.Text = Resource.ACCOUNTNO_NOT_VALIED_MESSAGE;
                mpeAlert.Show();
            }
            //if (string.IsNullOrEmpty(lblAccountNo.Text))
            //{
            //    lblAlertMessage.Text = Resource.ACCOUNTNO_NOT_VALIED_MESSAGE;
            //    mpeAlert.Show();
            //}
            //else
            //    btnGenerateBill.Attributes.Add("style", "display:inline;");

        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindMonths();
                btnGenerateBill.Attributes.Add("style", "display:none;");
            }
            catch (Exception ex)
            {
                //_ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        private void BindYears()
        {
            try
            {

                XmlDocument xmlResult = new XmlDocument();
                xmlResult = _objBillGenerationBal.GetDetails(ReturnType.Multiple);
                BillGenerationListBe objBillsListBE = _ObjiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlYear, "Year", "Year", "Year", true);

            }
            catch (Exception ex)
            {

            }
        }

        private void BindMonths()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                _objBillGenerationBe.Year = this.YearId;
                xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.Get);
                BillGenerationListBe objBillsListBE = _ObjiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlMonth, "MonthName", "Month", "Month", true);

            }
            catch (Exception ex)
            {
            }
        }

        protected void btnGenerateBill_Click(object sender, EventArgs e)
        {
            //if (hfReadCodeId.Value == ((int)ReadCode.Read).ToString() && string.IsNullOrEmpty(lblPresentReading.Text))
            //{
            //    lblAlertMessage.Text = Resource.READ_CUSTOMER_SHOULD_READING;
            //    mpeAlert.Show();
            //}
            //else 

            hfIsRegenerate.Value = false.ToString();
            if (string.IsNullOrEmpty(lblAccountNo.Text))
            {
                lblAlertMessage.Text = Resource.ACCOUNTNO_NOT_VALIED_MESSAGE;
                mpeAlert.Show();
            }
            else
                if (hfIsBilled.Value.ToLower() == true.ToString().ToLower())
                {
                    lblConfirmMessage.Text = Resource.IS_BILL_REGENERATE_MESSAGE;
                    mpeConfirm.Show();
                }
                else
                {
                    GenerateBill();
                }
        }

        protected void btnReGenerate_Click(object sender, EventArgs e)
        {
            hfIsRegenerate.Value = true.ToString();
            GenerateBill();
        }
        private void GenerateBill()
        {
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            _objBillGenerationBe.AccountNo = txtAccountNo.Text;
            _objBillGenerationBe.BillGeneratedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            _objBillGenerationBe.BillingYear = this.YearId;
            _objBillGenerationBe.BillingMonth = this.MonthId;

            _objBillGenerationBe = _ObjiDsignBAL.DeserializeFromXml<BillGenerationBe>(_objBillGenerationBal.InsertDetails(_objBillGenerationBe, Statement.Generate));

            if (!_objBillGenerationBe.IsExists && _objBillGenerationBe.IsSuccess)
            {
                string mailBody = string.Empty;
                if (hfIsRegenerate.Value == true.ToString())
                {
                    mailBody = "You Have Successfully Regenerated Bill for" + txtAccountNo.Text + ", Please find the attached Bills";
                    _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_SUCCESS, Resource.BILL_REGENERATION_SUCCESS, pnlMessage, lblMessage);
                }
                else
                {
                    _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_SUCCESS, Resource.BILL_GENERATION_SUCCESS, pnlMessage, lblMessage);
                    mailBody = "You Have Successfully Generated Bill for" + txtAccountNo.Text + ", Please find the attached Bills";
                }
                string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".txt";
                //set up a filestream
                string filePath = Server.MapPath(ConfigurationManager.AppSettings["BillGeneration"].ToString()) + Path.GetFileName(fileName);
                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);

                //set up a streamwriter for adding text
                StreamWriter sw = new StreamWriter(fs);

                //find the end of the underlying filestream
                sw.BaseStream.Seek(0, SeekOrigin.End);

                int rowCount = 0;
                string billDetails = string.Empty;

                billDetails += string.Format("{0,-6},{1,-107},{2,-11}", string.Empty, rowCount, rowCount).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-15},{1,-70},{2,-15},{3,-6}", "Route: 0", "Seq.", "Route: 0", "Seq.").Replace(',', ' ') +
                                Environment.NewLine +
                                Environment.NewLine + string.Format("{0,-34},{1,-63},{2,-25}", string.Empty, _objBillGenerationBe.BusinessUnitName, _objBillGenerationBe.BusinessUnitName).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-23},{1,-74},{2,-9}", string.Empty, "PLS PAY AT ANY BEDC OFFICE OR DESIGNATED BANKS", _objBillGenerationBe.MonthName + _objBillGenerationBe.BillYear.ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-31},{1,-27},{2,-36},{3,-22}", string.Empty, "Private Account", _objBillGenerationBe.MonthName + _objBillGenerationBe.BillYear.ToString(), "Private Account", _objBillGenerationBe.MonthName + _objBillGenerationBe.BillYear.ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-96},{1,-16}", string.Empty, _objBillGenerationBe.AccountNo).Replace(',', ' ') +
                                Environment.NewLine +
                                Environment.NewLine + string.Format("{0,8},{1,-38},{2,-28},{3,-22},{4,-30}", string.Empty, _objBillGenerationBe.AccountNo, string.Empty, string.Empty, _objBillGenerationBe.Name).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,8},{1,-38},{2,-28},{3,-22},{4,-30}", string.Empty, _objBillGenerationBe.Name, DateTime.Now.AddDays(5).ToString("dd/MM/yyyy"), _objBillGenerationBe.PreviousBalance, _objBillGenerationBe.PostalHouseNo + " " + _objBillGenerationBe.PostalStreet).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,8},{1,-38},{2,-28},{3,-22},{4,-30}", string.Empty, _objBillGenerationBe.PostalHouseNo + " " + _objBillGenerationBe.PostalStreet, _objBillGenerationBe.MeterNo, _objBillGenerationBe.NetEnergyCharges, _objBillGenerationBe.PostalZipCode).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,8},{1,-38},{2,-28},{3,-22},{4,-30}", string.Empty, _objBillGenerationBe.PostalZipCode, _objBillGenerationBe.NetFixedCharges, string.Empty, _objBillGenerationBe.ServiceHouseNo + " " + _objBillGenerationBe.ServiceStreet).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,8},{1,-38},{2,-28},{3,-22},{4,-30}", string.Empty, string.Empty, _objBillGenerationBe.Dials, _objBillGenerationBe.NetArrears.ToString(), _objBillGenerationBe.ServiceZipCode).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,8},{1,-90},{2,-15}", string.Empty, "THIS BILLING IS FOR APRIL 2014 CONSUMPTION", _objBillGenerationBe.MeterNo).Replace(',', ' ') +
                                Environment.NewLine + Environment.NewLine +
                                Environment.NewLine + string.Format("{0,-13},{1,-7},{2,-16},{3,-12},{4,-7},{5,-7},{6,7},{7,11}", "ENERGY", _objBillGenerationBe.TariffId, _objBillGenerationBe.ReadDate, _objBillGenerationBe.PreviousReading, _objBillGenerationBe.PresentReading, _objBillGenerationBe.Multiplier, _objBillGenerationBe.Usage, _objBillGenerationBe.NetEnergyCharges).Replace(',', ' ') +
                                Environment.NewLine +
                                Environment.NewLine + string.Format("{0,-13},{1,-7},{2,-16},{3,-12},{4,-7},{5,-7},{6,7},{7,11}", "FCR2S", _objBillGenerationBe.TariffId, "----", "----", "----", "----", "----", _objBillGenerationBe.NetFixedCharges).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-22},{1,-19}", string.Empty, "Billing Periods: 01").Replace(',', ' ') +
                                Environment.NewLine + Environment.NewLine +
                                Environment.NewLine + string.Format("{0,-33},{1,-22}", "LAST PAYMENT DATE  " + _objBillGenerationBe.LastPaymentDate, "AMOUNT      " + _objBillGenerationBe.LastPaidAmount).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-100},{1,-11}", "RECONNECTION FEE IS NOW =N=5000", DateTime.Now.AddDays(5).ToString("dd/MM/yyyy")).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-70},{1,17},{2,27}", "PAY WITHIN 7 DAYS TO AVOID DISCONNECTION", _objBillGenerationBe.NetArrears, _objBillGenerationBe.NetArrears).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-70},{1,17},{2,27}", string.Empty, _objBillGenerationBe.TotalBillAmount, _objBillGenerationBe.TotalBillAmount).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-70},{1,17},{2,27}", string.Empty, _objBillGenerationBe.VAT, _objBillGenerationBe.VAT).Replace(',', ' ') +
                                Environment.NewLine +
                                Environment.NewLine + string.Format("{0,-70},{1,17},{2,27}", string.Empty, _objBillGenerationBe.TotalBillAmountWithTax, _objBillGenerationBe.TotalBillAmountWithTax).Replace(',', ' ') +
                                Environment.NewLine +
                                Environment.NewLine + string.Format("{0,-27},{1,-77},{2,-16}", "OLD ACCT --XYZ--", "ENR2S- Rate:=N= 11.37", "--XYZ--").Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-2},{1,-60}", string.Empty, "THANKS FOR YOUR PATRONAGE").Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-5},{1,-26},{2,-61}", string.Empty, "- ENR2S- Rate:=N= 11.37", " Marketer:  ENOGHAYANGBON P.(07039738955)").Replace(',', ' ') +
                                Environment.NewLine +
                    //+ Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine +
                    "-------------------------------------------------------------------------------------------------------------------" + Environment.NewLine;


                //add the text
                sw.WriteLine(billDetails);
                //add the text to the underlying filestream

                sw.Flush();
                //close the writer
                sw.Close();

                _objUMS_Service.UpdateCustomerBillFile(txtAccountNo.Text, this.YearId, this.MonthId, fileName);

                _ObjCommonMethods.SendMail(filePath, mailBody, this.MonthName, this.YearId, Session[UMS_Resource.SESSION_USER_EMAILID].ToString());
            }
            else
            {
                if (_objBillGenerationBe.IsExists)
                    _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, "Bill was Already Generated", pnlMessage, lblMessage);
                else
                    _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, "Bill was failed to Generate", pnlMessage, lblMessage);
            }

            txtAccountNo.Text = string.Empty;
            ddlYear.SelectedIndex = ddlMonth.SelectedIndex = 0;
            lblCustomerUniqueNo.Text = lblAccountNo.Text = lblName.Text = lblReadCode.Text = hfReadCodeId.Value = lblPreviousReading.Text =
            lblPresentReading.Text = lblMeterNo.Text = lblLatestReadDate.Text = lblLastBillGeneration.Text = hfIsBilled.Value = string.Empty;
            btnGenerateBill.Attributes.Add("style", "display:none;");
        }


    }
}