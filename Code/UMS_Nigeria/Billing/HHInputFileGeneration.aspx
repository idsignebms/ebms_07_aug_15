﻿<%@ Page Title="HH Input File Generation" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    AutoEventWireup="true" CodeBehind="HHInputFileGeneration.aspx.cs" Theme="Green"
    Inherits="UMS_Nigeria.Billing.HHInputFileGeneration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <script type="text/javascript">
        window.onunload = StepOne;
    </script>
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdateProgress ID="UpdateProgress" runat="server">
                <ProgressTemplate>
                    <center>
                        <div id="loading-div" class="pbloading">
                            <div id="title-loading" class="pbtitle">
                                BEDC</div>
                            <center>
                                <img src="../images/loading.GIF" class="pbimg"></center>
                            <p class="pbtxt">
                                <asp:Label ID="lblLoading" runat="server" Text="<%$ Resources:Resource, LOADING_TEXT%>"></asp:Label></p>
                        </div>
                    </center>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
                PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
            <%--   <asp:UpdatePanel ID="upEstimationSetting" runat="server">
            <ContentTemplate>--%>
            <asp:Panel ID="pnlMessage" runat="server">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
            <%--File generation step one Starts--%>
            <div id="divStepOne" runat="server" class="consumer_feild">
                <div class="text_total">
                    <div class="text-heading">
                        <asp:Literal ID="litAddCycle" runat="server" Text="<%$ Resources:Resource, SP_BIL_FILE_GEN_STP_1%>"></asp:Literal>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div class="dot-line">
                </div>
                <div class="inner-box">
                    <div class="inner-box1">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, BILLING_MONTH%>"></asp:Literal><span
                                    class="span_star">*</span>
                            </label>
                            <asp:DropDownList ID="ddlYear" CssClass="text_select" onchange="DropDownlistOnChangelbl(this,'spanYear',' Year')"
                                runat="server" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="" Text="Year"></asp:ListItem>
                            </asp:DropDownList>
                            <span id="spanYear" class="span_color"></span>
                            <asp:DropDownList ID="ddlMonth" CssClass="text_select" onchange="DropDownlistOnChangelbl(this,'spanMonth',' Month')"
                                runat="server">
                                <asp:ListItem Text="Month" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <span id="spanMonth" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box2">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="LiteralCycleFeeder" runat="server" Text="<%$ Resources:Resource,CYCLE%>"></asp:Literal><span
                                    class="span_star">*</span>
                            </label>
                            <asp:DropDownList ID="ddlCycleList" CssClass="text-box" runat="server" onchange="DropDownlistOnChangelbl(this,'spanCyclesFeeders',' BookGroup/Feeder')">
                                <asp:ListItem Text="--Select BookGroup--" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                            <span id="spanCyclesFeeders" class="span_color"></span>
                        </div>
                    </div>                    
                    <div class="inner-box3">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="Literal5" runat="server" Text="Notes"></asp:Literal><span class="span_star">*</span>
                            </label></div><div class="text-inner inner-box-b">
                            <asp:TextBox ID="txtNotes" runat="server" placeHolder="Notes" TextMode="MultiLine"
                                MaxLength="250" ToolTip="Add note max of 250 charecters."></asp:TextBox>
                            <span id="spnNotes" class="span_color"></span>
                        </div>
                    </div>
                </div>
                <div class="clear pad_5">
                </div>
                <div class="consumer_total p_left238">
                    <asp:Button ID="btnStepOne" Text="<%$ Resources:Resource, NEXT%>" ToolTip="Search"
                        CssClass="box_s" runat="server" OnClick="btnStepOne_Click" OnClientClick="return ValidateStepOne();" />
                </div>
            </div>
            <%--End--%>
            <%--File generation step two Starts--%>
            <div id="divStepTwo" runat="server" visible="false">
                <div class="text_total">
                    <div class="text-heading">
                        <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, SP_BIL_FILE_GEN_STP_2%>"></asp:Literal>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div class="dot-line">
                </div>
                <div class="grid gridbg-pad" id="divPrint" runat="server" visible="false" align="center">
                    <h3 class="gridhead" align="left">
                        <asp:Literal ID="litBuList" runat="server" Text=""></asp:Literal>
                    </h3>
                    <div class="gridhead width500 ">
                        <asp:Label ID="lblMonthlyUsage" runat="server"></asp:Label>
                    </div>
                    <div class="gridhead width126" style="text-align: right; padding-right: 6px; float: right;">
                    </div>
                </div>
                <div class="clear pad_5">
                </div>
                <div class="grid_boxes">
                    <div class="grid marg_5" id="divInfo" visible="true" runat="server" align="center"
                        style="overflow-x: auto;">
                        <div class="gridhead width500 ">
                            <asp:Label ID="lblCurrntCycle" runat="server"></asp:Label>
                        </div>
                        <div align="right" style="color: Red; padding-left: 70px; margin-top: 1px;">
                            <asp:Label ID="lblTotal" runat="server"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div id="divpaging" visible="true" runat="server">
                            <div align="right" class="marg_20t" runat="server" id="divLinks">
                                <asp:LinkButton ID="lkbtnFirst" runat="server" CssClass="pagingfirst" OnClick="lkbtnFirst_Click">
                                    <asp:Literal ID="litFirst" runat="server" Text="<%$ Resources:Resource, FIRST%>"></asp:Literal></asp:LinkButton>
                                &nbsp;|
                                <asp:LinkButton ID="lkbtnPrevious" runat="server" CssClass="pagingfirst" OnClick="lkbtnPrevious_Click">
                                    <asp:Literal ID="litPrevious" runat="server" Text="<%$ Resources:Resource, PREVIOUS%>"></asp:Literal></asp:LinkButton>
                                &nbsp;|
                                <asp:Label ID="lblCurrentPage" runat="server" Font-Bold="true"></asp:Label>
                                &nbsp;|
                                <asp:LinkButton ID="lkbtnNext" runat="server" CssClass="pagingfirst" OnClick="lkbtnNext_Click">
                                    <asp:Literal ID="litNext" runat="server" Text="<%$ Resources:Resource, NEXT%>"></asp:Literal></asp:LinkButton>
                                &nbsp;|
                                <asp:LinkButton ID="lkbtnLast" runat="server" CssClass="pagingfirst" OnClick="lkbtnLast_Click">
                                    <asp:Literal ID="litLast" runat="server" Text="<%$ Resources:Resource, LAST%>"></asp:Literal></asp:LinkButton>
                                &nbsp;|<asp:Literal ID="litPageSize" runat="server" Text="<%$ Resources:Resource, PAGE_SIZE%>"></asp:Literal>
                                <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" CssClass="paging-size"
                                    OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="grid_tb">
                            <asp:GridView ID="grdCustomersDetails" runat="server" AutoGenerateColumns="False"
                                HeaderStyle-CssClass="grid_tb_hd">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    There is no data.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblSrno" runat="server" Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" runat="server"
                                        HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblAccountNo" runat="server" Text='<%#Eval("AccountNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" runat="server"
                                        HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNo" runat="server" Text='<%#Eval("OldAccountNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, CUSTOMER_NAME%>" runat="server"
                                        HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ADDRESS%>" runat="server"
                                        HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblCustAddress" runat="server" Text='<%#Eval("CustomerAddress") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, CONTACT_NO%>" runat="server"
                                        HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Left">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblContactNo" runat="server" Text='<%#Eval("ContactNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, TARIFF%>" runat="server" HeaderStyle-Width="3%"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblClassName" runat="server" Text='<%#Eval("ClassName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div class="pad_5 clear">
                        </div>
                        <div class="consumer_total p_left238">
                            <asp:Button ID="btnPrevious" Text="<%$ Resources:Resource, PREVIOUS%>" ToolTip="Search"
                                runat="server" OnClick="btnPrevious_Click" CssClass="box_s" />
                            <asp:Button ID="btnStepTwo" Text="<%$ Resources:Resource, GEN_FILE%>" ToolTip="Search"
                                CssClass="box_s" runat="server" OnClick="btnStepTwo_Click" />
                        </div>
                    </div>
                </div>
                <div class="clear pad_5">
                </div>
            </div>
            <%--End--%>
            <asp:HiddenField ID="hfPageSize" runat="server" Value="0" />
            <asp:HiddenField ID="hfPageNo" runat="server" />
            <asp:HiddenField ID="hfLastPage" runat="server" />
            <asp:HiddenField ID="hfTotalRecords" runat="server" />
            <asp:ModalPopupExtender ID="mpeGenMsg" runat="server" PopupControlID="pnlGenMsg"
                TargetControlID="hfTargetContId" BackgroundCssClass="modalBackground" CancelControlID="btnGenMsgCancelContId">
            </asp:ModalPopupExtender>
            <asp:HiddenField ID="hfTargetContId" runat="server" />
            <asp:Panel ID="pnlGenMsg" runat="server" CssClass="modalPopup" Style="display: none;">
                <div class="popheader popheaderlblred">
                    <asp:Label ID="lblGenMsg" runat="server"></asp:Label>
                </div>
                <div class="footer popfooterbtnrt">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="btnGenMsgCancelContId" runat="server" Text="<%$ Resources:Resource, OK%>"
                            CssClass="ok_btn" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="mpeConfirmationMsg" runat="server" PopupControlID="pnlConfirmationMsg"
                TargetControlID="hfTargetConfirmation" BackgroundCssClass="modalBackground" CancelControlID="btnConfirmationMsgCancle">
            </asp:ModalPopupExtender>
            <asp:HiddenField ID="hfTargetConfirmation" runat="server" />
            <asp:Panel ID="pnlConfirmationMsg" runat="server" CssClass="modalPopup" Style="display: none;">
                <div class="popheader popheaderlblred">
                    <asp:Label ID="LblConfirmationMsg" runat="server"></asp:Label>
                </div>
                <div class="footer popfooterbtnrt">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="DeleteFile" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="ok_btn"
                            OnClick="DeleteFile_Click" />
                        <asp:Button ID="btnConfirmationMsgCancle" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                            CssClass="ok_btn" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="mpeSureToDeleteFile" runat="server" PopupControlID="pnlSureDeleteInputFile"
                TargetControlID="hhTagetSureToDel" BackgroundCssClass="modalBackground" CancelControlID="btnSureToDeleteCalcel">
            </asp:ModalPopupExtender>
            <asp:HiddenField ID="hhTagetSureToDel" runat="server" />
            <asp:Panel ID="pnlSureDeleteInputFile" runat="server" CssClass="modalPopup" Style="display: none;">
                <div class="popheader popheaderlblred">
                    <asp:Label ID="lblSureTodelte" runat="server"></asp:Label>
                </div>
                <div class="footer popfooterbtnrt">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="btnSureToDelete" runat="server" Text="<%$ Resources:Resource, OK%>"
                            CssClass="ok_btn" OnClick="btnSureToDelete_Click" />
                        <asp:Button ID="btnSureToDeleteCalcel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                            CssClass="ok_btn" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
            <asp:HiddenField ID="hfIsFileGanerated" runat="server" Value="0" />
            <%--   </ContentTemplate>
       <Triggers>
         <asp:PostBackTrigger ControlID="btnStepTwo"  />
         </Triggers>
        </asp:UpdatePanel>--%>
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance(); //Raised before processing of an    asynchronous postback starts and the postback request is sent to the server. 
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser. 
        prm.add_endRequest(EndRequestHandler); function BeginRequestHandler(sender, args) { //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }
        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        } 
    </script>
</asp:Content>
