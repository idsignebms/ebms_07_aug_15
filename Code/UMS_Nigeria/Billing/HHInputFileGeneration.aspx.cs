﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for Estimation Setting
                     
 Developer        : ID077-NEERAJ KANOJIYA
 Creation Date    : 14-Aug-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.IO;

namespace UMS_Nigeria.Billing
{
    public partial class HHInputFileGeneration : System.Web.UI.Page
    {
        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = "";
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                if (!IsPostBack)
                {
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    BindYears();
                    BindMonths();
                    BindCyclesOrFeeders();
                }
            }
            else
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void btnStepOne_Click(object sender, EventArgs e)
        {
            divStepOne.Visible = false;
            divStepTwo.Visible = true;
            BindGrdCustomersDetails();
        }

        protected void btnStepTwo_Click(object sender, EventArgs e)
        {
            //divStepOne.Visible = false;
            //divStepTwo.Visible = false;
            //divStepThree.Visible = true;
            GetSpotBillingInputs();
        }

        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            divStepOne.Visible = true;
            divStepTwo.Visible = false;
        }

        protected void DeleteFile_Click(object sender, EventArgs e)
        {
            lblSureTodelte.Text = Resource.SURE_TO_DEL_HHINPUT_FILE;
            mpeSureToDeleteFile.Show();
        }

        protected void btnSureToDelete_Click(object sender, EventArgs e)
        {
            XmlDocument xml = null;
            objSpotBillingBE.Year = Convert.ToInt32(ddlYear.SelectedItem.Value);
            objSpotBillingBE.Month = Convert.ToInt32(ddlMonth.SelectedItem.Value);
            objSpotBillingBE.BookCycleId = ddlCycleList.SelectedItem.Value;
            objSpotBillingBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();

            xml = _ObjMastersBAL.DeleteHHInputFileBAL(_objiDsignBAL.DoSerialize<SpotBillingBE>(objSpotBillingBE));//DELETE File
            objSpotBillingBE = _objiDsignBAL.DeserializeFromXml<SpotBillingBE>(xml);
            if (objSpotBillingBE.RowCount > 0)
            {
                File.Delete(HttpContext.Current.Server.MapPath(ConfigurationSettings.AppSettings["SpotBilling"]) + objSpotBillingBE.FileName);
                Message(Resource.HHINPUT_FILE_DELETED, UMS_Resource.MESSAGETYPE_SUCCESS);
                divStepOne.Visible = false;
                divStepTwo.Visible = true;
                BindGrdCustomersDetails();
            }
        }

        protected void PreRender(object sender, EventArgs e)
        {
            divStepOne.Visible = true;
            divStepTwo.Visible = false;
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindMonths();
        }

        #endregion

        # region Members
        iDsignBAL _objiDsignBAL = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        BillingMonthOpenBAL objBillingMonthOpenBAL = new BillingMonthOpenBAL();
        BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
        MastersBAL _ObjMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        string Key = UMS_Resource.BILLING_MONTH_OPEN;
        CustomerDetailsBe objCustomerDetailsBe = new CustomerDetailsBe();
        CustomerDetailsBAL objCustomerDetailsBAL = new CustomerDetailsBAL();
        CustomerDetailsListBe objCustomerDetailsListBe = new CustomerDetailsListBe();
        SpotBillingBE objSpotBillingBE = new SpotBillingBE();
        SpotBillingListBE objSpotBillingListBE = new SpotBillingListBE();
        private delegate void CallAsynchonousFunctions(string FileCompletePath, string FileType);

        private int YearId
        {
            get { return string.IsNullOrEmpty(ddlYear.SelectedValue) ? 0 : Convert.ToInt32(ddlYear.SelectedValue); }
        }

        private int MonthId
        {
            get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? 0 : Convert.ToInt32(ddlMonth.SelectedValue); }
        }

        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void BindYears()
        {
            try
            {

                XmlDocument xmlResult = new XmlDocument();
                xmlResult = _objBillGenerationBal.GetDetails(ReturnType.Multiple);
                BillGenerationListBe objBillsListBE = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                _objiDsignBAL.FillDropDownList(_objiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlYear, "Year", "Year", "Year", true);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindMonths()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                _objBillGenerationBe.Year = this.YearId;
                xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.Get);
                BillGenerationListBe objBillsListBE = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                _objiDsignBAL.FillDropDownList(_objiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlMonth, "MonthName", "Month", "Month", true);

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindCyclesOrFeeders()
        {
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                _objCommonMethods.BindCyclesByBUSUSC(ddlCycleList, Session[UMS_Resource.SESSION_USER_BUID].ToString(), string.Empty, string.Empty, true, UMS_Resource.DROPDOWN_SELECT, false);
            else
                _objCommonMethods.BindNOCyclesList(ddlCycleList, true);
        }

        public void BindGrdCustomersDetails()
        {
            objSpotBillingBE.Year = Convert.ToInt32(ddlYear.SelectedItem.Value);
            objSpotBillingBE.Month = Convert.ToInt32(ddlMonth.SelectedItem.Value);
            objSpotBillingBE.BookCycleId = ddlCycleList.SelectedItem.Value;

            XmlDocument xml = _ObjMastersBAL.IsHHInputFileExistsBAL(_objiDsignBAL.DoSerialize<SpotBillingBE>(objSpotBillingBE));
            objSpotBillingBE = _objiDsignBAL.DeserializeFromXml<SpotBillingBE>(xml);

            if (!objSpotBillingBE.IsExists)//Checking file existence
            {
                lblCurrntCycle.Text = string.Format(Resource.LIST_OF_CST_FOR_CYCLE, ddlCycleList.SelectedItem.Text);
                objCustomerDetailsBe.BookCycleId = ddlCycleList.SelectedItem.Value;
                objCustomerDetailsBe.PageNo = Convert.ToInt32(hfPageNo.Value); ;
                objCustomerDetailsBe.PageSize = PageSize;
                xml = objCustomerDetailsBAL.GetCustomersDetailsForSPTBillingBAL(_objiDsignBAL.DoSerialize<CustomerDetailsBe>(objCustomerDetailsBe));
                objCustomerDetailsListBe = _objiDsignBAL.DeserializeFromXml<CustomerDetailsListBe>(xml);

                if (objCustomerDetailsListBe.items.Count > 0)
                {
                    lblTotal.Text = Resource.TOTAL_CUSTOMERS + " " + objCustomerDetailsListBe.items[0].TotalRecords;
                    divInfo.Visible = divpaging.Visible = true;
                    hfTotalRecords.Value = objCustomerDetailsListBe.items[0].TotalRecords.ToString();
                    divStepOne.Visible = false;
                    divStepTwo.Visible = true;
                }
                else
                {
                    lblGenMsg.Text = Resource.NO_CSTMR_ASOCTD_WITH_CYCLE;
                    mpeGenMsg.Show();
                    divStepOne.Visible = true;
                    divStepTwo.Visible = false;
                    divInfo.Visible = divpaging.Visible = false;
                    hfTotalRecords.Value = objCustomerDetailsListBe.items.Count.ToString();
                }
                grdCustomersDetails.DataSource = objCustomerDetailsListBe.items;
                grdCustomersDetails.DataBind();
                BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            else
            {
                divStepOne.Visible = true;
                divStepTwo.Visible = false;
                divInfo.Visible = divpaging.Visible = false;
                LblConfirmationMsg.Text = Resource.FILE_ALRDY_GEN_WNT_TO_GEN_AGAIN;
                mpeConfirmationMsg.Show();
            }
        }

        public void GetSpotBillingInputs()
        {
            string SpotBillingInputs = null;
            string FileName = ddlCycleList.SelectedItem.Text + "_" + ddlYear.SelectedItem.Text + "_" + ddlMonth.SelectedItem.Text + "_" + DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + Resource.Formate_Text;
            string CompletefileName = HttpContext.Current.Server.MapPath(ConfigurationSettings.AppSettings["SpotBilling"]) + FileName;
            char SpotBillingInputSeprator = Constants.SpotBillingInputSeprator;
            string SpotBillingInputReplacement = Constants.SpotBillingInputReplacement;

            objSpotBillingBE.Year = Convert.ToInt32(ddlYear.SelectedItem.Value);
            objSpotBillingBE.Month = Convert.ToInt32(ddlMonth.SelectedItem.Value);
            objSpotBillingBE.BookCycleId = ddlCycleList.SelectedItem.Value;

            XmlDocument xml = _ObjMastersBAL.IsHHInputFileExistsBAL(_objiDsignBAL.DoSerialize<SpotBillingBE>(objSpotBillingBE));
            objSpotBillingBE = _objiDsignBAL.DeserializeFromXml<SpotBillingBE>(xml);

            if (!objSpotBillingBE.IsExists)//Checking file existence
            {
                objSpotBillingBE.Year = Convert.ToInt32(ddlYear.SelectedItem.Value);
                objSpotBillingBE.Month = Convert.ToInt32(ddlMonth.SelectedItem.Value);
                objSpotBillingBE.BookCycleId = ddlCycleList.SelectedItem.Value;

                xml = _ObjMastersBAL.GetSpotBillingInputsDAL(_objiDsignBAL.DoSerialize<SpotBillingBE>(objSpotBillingBE));
                objSpotBillingListBE = _objiDsignBAL.DeserializeFromXml<SpotBillingListBE>(xml);
                if (objSpotBillingListBE.items.Count > 0)//Generate Input 
                {
                    foreach (SpotBillingBE LstSpotBillingBE in objSpotBillingListBE.items)//Neeraj-ID077, This order is according to "BEDC_SpotBilling_Logic_R1.xls" checked on 23-Aug.
                    {
                        SpotBillingInputs += LstSpotBillingBE.Cons_AccNo + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.Cons_SeqNo, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                       _objCommonMethods.ReplaceText(LstSpotBillingBE.Meter_No, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.Service_No, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.BU_Name, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.Cons_Name, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.Cons_Address, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      LstSpotBillingBE.Cons_TariffCode + SpotBillingInputSeprator +
                                      LstSpotBillingBE.Prev_Reading + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.Prev_ReadDate, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      LstSpotBillingBE.Adj_Units + SpotBillingInputSeprator +
                                      LstSpotBillingBE.Adj_Ammount + SpotBillingInputSeprator +
                                      LstSpotBillingBE.Cons_Cycle_No + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.Cons_Cycle_Name, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.CustomerCare_No, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.Message1, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.Message2, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.Message3, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.Message4, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      LstSpotBillingBE.Fixed_Charges + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.Prev_Bill_Date, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      LstSpotBillingBE.Net_Arrers + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.Last_Payment_Date, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.Marketer_Name, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                            //LstSpotBillingBE.Estimated_units + SpotBillingInputSeprator +
                                      Estimated_units(LstSpotBillingBE.ReadCodeId, LstSpotBillingBE.Estimated_units) + SpotBillingInputSeprator +
                                      LstSpotBillingBE.Late_Payment_Charges + SpotBillingInputSeprator +
                                      LstSpotBillingBE.Tax + SpotBillingInputSeprator +
                                      LstSpotBillingBE.Rate + SpotBillingInputSeprator +
                                      LstSpotBillingBE.DueDateFlag + SpotBillingInputSeprator +
                                      DueDateDays(LstSpotBillingBE.ReadCodeId, LstSpotBillingBE.DueDateDays) + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.Due_Date, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      LstSpotBillingBE.Multiplication_Factors + SpotBillingInputSeprator +//----
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.MeterChangeStatus, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      LstSpotBillingBE.Prv_Mtr_Consumption + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.InitialReading, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.Phase, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.OldAccNo, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.EstimationType, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.PreviousBilltype, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      LstSpotBillingBE.PreviousBilledUnits + SpotBillingInputSeprator +
                                      Math.Round(LstSpotBillingBE.AvgUnits, 2) + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.Dails, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      _objCommonMethods.ReplaceText(LstSpotBillingBE.Highestconsumption, SpotBillingInputReplacement) + SpotBillingInputSeprator +
                                      LstSpotBillingBE.CurrentBillSetting +
                                      Environment.NewLine;
                    }

                    try
                    {

                        FileStream fs = default(FileStream);
                        fs = new FileStream(CompletefileName, FileMode.Append);
                        StreamWriter sw = new StreamWriter(fs);
                        sw.Write(SpotBillingInputs);
                        sw.Close();


                        objSpotBillingBE.Year = Convert.ToInt32(ddlYear.SelectedItem.Value);
                        objSpotBillingBE.Month = Convert.ToInt32(ddlMonth.SelectedItem.Value);
                        objSpotBillingBE.BookCycleId = ddlCycleList.SelectedItem.Value;
                        objSpotBillingBE.FileName = FileName;
                        objSpotBillingBE.Notes = txtNotes.Text;
                        objSpotBillingBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();

                        _ObjMastersBAL.InsertHHIputFileDetailsBAL(_objiDsignBAL.DoSerialize<SpotBillingBE>(objSpotBillingBE));//Inserting file details


                        _objCommonMethods.DownloadFile(CompletefileName, "application/txt"); //Download file for admin

                        //CallAsynchonousFunctions objCallAsynchonousFunctions = new CallAsynchonousFunctions(_objCommonMethods.DownloadFile);
                        //objCallAsynchonousFunctions(CompletefileName, "application/txt");

                        Message(Resource.HHINPUT_FILE_GENTD, UMS_Resource.MESSAGETYPE_SUCCESS);
                        //divStepOne.Visible = true;
                        //divStepTwo.Visible = false;


                    }
                    catch (Exception Ex)
                    {
                        Message(Resource.HHINPUT_FILE_NOT_GENTD, UMS_Resource.MESSAGETYPE_ERROR);
                        throw Ex;
                    }
                }
                else
                {
                    lblGenMsg.Text = Resource.HH_INPUT_CANNOT_GEN;
                    mpeGenMsg.Show();
                }
            }
            else
            {
                lblGenMsg.Text = Resource.HH_FILE_GENRATED;
                mpeGenMsg.Show();
            }
        }

        public string Estimated_units(int ReadCodeID, int Estimated_units)
        {
            string EstimatedUnits = Estimated_units.ToString();
            if (ReadCodeID == (int)ReadCode.Read)
            {
                EstimatedUnits = Constants.SpotBillingInputReplacement;
            }
            return EstimatedUnits;
        }

        public string DueDateDays(int ReadCodeID, int DueDateDays)
        {
            string DueDays = DueDateDays.ToString();
            if (ReadCodeID == (int)ReadCode.Read)
            {
                DueDays = Constants.SpotBillingInputReplacement;
            }
            return DueDays;
        }
        //protected void DownloadFile(string pathId)
        //{
        //    if (!(string.IsNullOrEmpty(pathId)))
        //    {
        //        try
        //        {
        //            Response.ContentType = "application/txt";
        //            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pathId + ".txt");
        //            //string path = ConfigurationManager.AppSettings["Pdf_Path"].ToString() + "\\" + pathId.Trim() + ".txt";
        //            Response.TransmitFile(pathId);
        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }
        //        finally
        //        {
        //            HttpContext.Current.ApplicationInstance.CompleteRequest();
        //        }
        //    }
        //}

        #endregion

        #region Paging

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrdCustomersDetails();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrdCustomersDetails();

                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGrdCustomersDetails();

                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGrdCustomersDetails();

                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                BindGrdCustomersDetails();

                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                _objiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                _objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

    }
}