﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HHOutputUploads.aspx.cs"
    MasterPageFile="~/MasterPages/EBMS.Master" Theme="Green" Title="HH Output File Upload"
    Inherits="UMS_Nigeria.Billing.HHOutputUploads" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdatePanel ID="updatePanelTotal" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddCycle" runat="server" Text="HH Output File Upload"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="LiteralUnits" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </label>
                                <br />
                                <asp:DropDownList ID="ddlBusinessUnits" CssClass="text-box" runat="server" OnSelectedIndexChanged="ddlBusinessUnits_SelectedIndexChanged"
                                    AutoPostBack="true">
                                    <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                                </asp:DropDownList>
                                <div class="clear space">
                                </div>
                                <span id="spanddlBusinessUnit" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="ltrlServiceUnits" runat="server" Text="<%$ Resources:Resource,SERVICE_UNITS%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </label>
                                <br />
                                <asp:DropDownList ID="ddlServiceUnits" CssClass="text-box" runat="server" OnSelectedIndexChanged="ddlServiceUnits_SelectedIndexChanged"
                                    AutoPostBack="true">
                                    <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <div class="clear space">
                                </div>
                                <span id="spanServiceUnit" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="ltrlServiceCenter" runat="server" Text="<%$ Resources:Resource,SERVICE_CENTERS%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </label>
                                <br />
                                <asp:DropDownList ID="ddlServiceCenters" CssClass="text-box" runat="server" OnSelectedIndexChanged="ddlServiceCenters_SelectedIndexChanged"
                                    AutoPostBack="true">
                                    <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <div class="clear space">
                                </div>
                                <span id="spanServiceCenter" class="span_color"></span>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="LiteralCycleFeeder" runat="server" Text="<%$ Resources:Resource,CYCLE%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </label>
                                <br />
                                <asp:DropDownList ID="ddlCycleList" CssClass="text-box" runat="server">
                                    <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <div class="clear space">
                                </div>
                                <span id="spanCycles" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, BILLING_MONTH%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </label>
                                <br />
                                <asp:DropDownList ID="ddlYear" CssClass="text-box" onchange="DropDownlistOnChangelbl(this,'spanYear',' Year')"
                                    runat="server" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Value="" Text="Year"></asp:ListItem>
                                </asp:DropDownList>
                                <div class="clear space">
                                </div>
                                <span id="spanYear" class="span_color"></span>
                                <asp:DropDownList ID="ddlMonth" CssClass="text-box" onchange="DropDownlistOnChangelbl(this,'spanMonth',' Month')"
                                    runat="server">
                                    <asp:ListItem Text="Month" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <div class="clear space">
                                </div>
                                <span id="spanMonth" class="span_color"></span>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, UPLOAD_DOCUMENT%>"></asp:Literal>
                                    <span class="span_star">*</span>
                                </label>
                                <asp:FileUpload ID="uploadHHOutputFile" runat="server" /><br />
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <br />
                                <asp:Button ID="btnUpload" Text="<%$ Resources:Resource, NEXT%>" ToolTip="Upload"
                                    OnClientClick="return Validate();" CssClass="box_s" runat="server" OnClick="btnUpload_Click" />
                            </div>
                        </div>
                        <div id="divgridHeader" class="grid gridpaging" runat="server">
                            <div id="divpaging" runat="server">
                                <div class="marg_20t gridpaginglnk" runat="server" id="divLinks">
                                    <div class="grid_paging_top">
                                        <div class="paging_top_title">
                                            <asp:Literal ID="litBuList" runat="server" Text="Valid Customer Details"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid_tb" id="divPrint" runat="server">
                            <asp:GridView ID="gvValidCustomerDetails" runat="server" AutoGenerateColumns="False"
                                ShowHeaderWhenEmpty="true" HeaderStyle-CssClass="grid_tb_hd">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    There is no data.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField DataField="Cons_AccNo" HeaderText="AccountNo" />
                                    <asp:BoundField DataField="BillNo" HeaderText="BillNo" />
                                    <asp:BoundField DataField="Meter_No" HeaderText="Meter No" />
                                    <asp:BoundField DataField="Prev_Reading" HeaderText="Prev_Reading" />
                                    <asp:BoundField DataField="CurrentReading" HeaderText="CurrentReading" />
                                    <asp:BoundField DataField="BilledDate" HeaderText="BilledDate" />
                                    <asp:BoundField DataField="BillAmount" HeaderText="BillAmount" />
                                    <asp:BoundField DataField="Check_Meter_Reading" HeaderText="Check_Meter_Reading" />
                                    <asp:BoundField DataField="Check_Meter_No" HeaderText="Check_MeterNo" />
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <br />
                                <asp:Button ID="btnSave" Text="<%$ Resources:Resource, SAVE%>" ToolTip="Upload" CssClass="box_s"
                                    runat="server" OnClick="btnSave_Click" />
                            </div>
                        </div>
                        <div id="divInvalidGvHeader" class="grid gridpaging" runat="server">
                            <div id="div3" runat="server">
                                <div class="marg_20t gridpaginglnk" runat="server" id="div4">
                                    <div class="grid_paging_top">
                                        <div class="paging_top_title">
                                            <asp:Literal ID="Literal1" runat="server" Text="InValid Customer Details"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid_tb" id="div1" runat="server">
                            <asp:GridView ID="gvInValidDetails" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="grid_tb_hd"
                                ShowHeaderWhenEmpty="true" OnRowDataBound="gvInValidDetails_RowDataBound">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    There is no data.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField DataField="Cons_AccNo" HeaderText="AccountNo" />
                                    <asp:BoundField DataField="BillNo" HeaderText="BillNo" />
                                    <asp:BoundField DataField="Meter_No" HeaderText="Meter No" />
                                    <asp:BoundField DataField="Prev_Reading" HeaderText="Prev_Reading" />
                                    <asp:BoundField DataField="CurrentReading" HeaderText="CurrentReading" />
                                    <asp:BoundField DataField="BilledDate" HeaderText="BilledDate" />
                                    <asp:BoundField DataField="BillAmount" HeaderText="BillAmount" />
                                    <asp:BoundField DataField="Check_Meter_Reading" HeaderText="Check_Meter_Reading" />
                                    <asp:BoundField DataField="Check_Meter_No" HeaderText="Check_MeterNo" />
                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnRemarks" runat="server" CommandArgument='<%# Eval("IsExists") %>'
                                                Font-Bold="true" ForeColor="Red" Enabled="false" CommandName='<%# Eval("IsValid") %>'
                                                Text=""></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
            <asp:ModalPopupExtender ID="mpeAlertMessage" runat="server" TargetControlID="hfAlertMpe"
                PopupControlID="pnlAlertMessage" BackgroundCssClass="modalBackground" CancelControlID="btnOk">
            </asp:ModalPopupExtender>
            <asp:HiddenField ID="hfAlertMpe" runat="server" />
            <asp:Panel runat="server" BorderColor="#CD3333" ID="pnlAlertMessage" CssClass="modalPopup">
                <div class="popheader" style="background-color: #CD3333;">
                    <asp:Label ID="lblAlertMessage" runat="server"></asp:Label>
                </div>
                <div class="clear pad_10">
                </div>
                <div class="footer" align="right">
                    <asp:Button ID="btnOk" runat="server" Text="OK" CssClass="btn_ok" Style="margin-left: 200px;" />
                    <br />
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpload" />
        </Triggers>
    </asp:UpdatePanel>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).ready(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/HHOutputUploads.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
