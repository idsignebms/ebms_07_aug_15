﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBE;
using Resources;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;
using System.Data;
using UMS_NigeriaBAL;
using System.Xml;
using UMS_NigriaDAL;
using System.Xml.Linq;

namespace UMS_Nigeria.Billing
{
    public partial class HHOutputUploads : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        SpotBillingOutputBal _objSpotBillOptBal = new SpotBillingOutputBal();
        BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
        string Key = "HHOutputUploads";

        #endregion

        #region Properties

        public string BusinessUnit
        {
            get { return string.IsNullOrEmpty(ddlBusinessUnits.SelectedValue) ? string.Empty : ddlBusinessUnits.SelectedValue; }
            set { ddlBusinessUnits.SelectedValue = value.ToString(); }
        }

        public string ServiceUnit
        {
            get { return string.IsNullOrEmpty(ddlServiceUnits.SelectedValue) ? string.Empty : ddlServiceUnits.SelectedValue; }
            set { ddlServiceUnits.SelectedValue = value.ToString(); }
        }

        public string ServiceCenter
        {
            get { return string.IsNullOrEmpty(ddlServiceCenters.SelectedValue) ? string.Empty : ddlServiceCenters.SelectedValue; }
            set { ddlServiceCenters.SelectedValue = value.ToString(); }
        }

        public string Cycle
        {
            get { return string.IsNullOrEmpty(ddlCycleList.SelectedValue) ? string.Empty : ddlCycleList.SelectedValue; }
            set { ddlCycleList.SelectedValue = value.ToString(); }
        }

        private int YearId
        {
            get { return string.IsNullOrEmpty(ddlYear.SelectedValue) ? 0 : Convert.ToInt32(ddlYear.SelectedValue); }
        }

        private int MonthId
        {
            get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? 0 : Convert.ToInt32(ddlMonth.SelectedValue); }
        }


        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.CssClass = lblMessage.Text = string.Empty;
            if (!IsPostBack)
            {
                gvValidCustomerDetails.DataBind();
                gvInValidDetails.DataBind();
                BindYears();
                //divInvalidGvHeader.Visible = divgridHeader.Visible =
                btnSave.Visible = ddlServiceCenters.Enabled = ddlServiceUnits.Enabled = ddlCycleList.Enabled = false;
                _ObjCommonMethods.BindBusinessUnits(ddlBusinessUnits, string.Empty, true);
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                {
                    ddlBusinessUnits.SelectedIndex = ddlBusinessUnits.Items.IndexOf(ddlBusinessUnits.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                    ddlBusinessUnits.Enabled = false;
                    ddlBusinessUnits_SelectedIndexChanged(ddlBusinessUnits, new EventArgs());
                }
            }
        }

        protected void ddlBusinessUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlBusinessUnits.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindServiceUnits(ddlServiceUnits, this.BusinessUnit, true);
                    ddlServiceUnits.Enabled = true;
                    //ddlServiceCenters.Items.Clear();
                    //ddlCycleList.Items.Clear();
                    ddlServiceCenters.Enabled = ddlCycleList.Enabled = false;
                    //ddlServiceUnits_SelectedIndexChanged(this, null);
                }
                else
                {
                    //ddlServiceCenters.Items.Clear();
                    //ddlCycleList.Items.Clear();
                    //ddlServiceUnits.Items.Clear();
                    ddlServiceCenters.SelectedIndex = ddlServiceUnits.SelectedIndex = ddlCycleList.SelectedIndex = Constants.Zero;
                    ddlServiceCenters.Enabled = ddlServiceUnits.Enabled = ddlCycleList.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlServiceUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlServiceUnits.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindServiceCenters(ddlServiceCenters, this.ServiceUnit, true);
                    ddlServiceCenters.Enabled = true;
                    ddlCycleList.SelectedIndex = Constants.Zero;
                    ddlCycleList.Enabled = false;
                    //ddlCycleList.Items.Clear();
                    //ddlServiceCenters_SelectedIndexChanged(this, null);
                }
                else
                {
                    //ddlServiceCenters.Items.Clear();
                    //ddlCycleList.Items.Clear();
                    ddlCycleList.SelectedIndex = ddlServiceCenters.SelectedIndex = Constants.Zero;
                    ddlServiceCenters.Enabled = ddlCycleList.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlServiceCenters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlServiceCenters.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindCyclesByBUSUSC(ddlCycleList, this.BusinessUnit, this.ServiceUnit, this.ServiceCenter, true, "Select BookGroup", true);
                    ddlCycleList.Enabled = true;
                }
                else
                {
                    //ddlCycleList.Items.Clear();
                    ddlCycleList.SelectedIndex = Constants.Zero;
                    ddlCycleList.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            IEnumerable<SpotBillingOutputBe> customersDetails = (IEnumerable<SpotBillingOutputBe>)Session[UMS_Resource.SESSION_HHOUTPUT_DATA];
            //DataTable dt = CreateDataTableForOutput();
            //foreach (SpotBillingOutputBe item in customersDetails)
            //{
            //    DataRow dr = dt.NewRow();
            //    dr["Cons_AccNo"] = item.Cons_AccNo;
            //    dr["BillNo"] = item.BillNo;
            //    dr["Meter_No"] = item.Meter_No;
            //    dr["Prev_Reading"] = item.Prev_Reading;
            //    dr["CurrentReading"] = item.CurrentReading;
            //    dr["BilledDate"] = item.BilledDate;
            //    dr["BillAmount"] = item.BillAmount;
            //    dr["Check_Meter_Reading"] = item.Check_Meter_Reading;
            //    dr["Check_Meter_No"] = item.Check_Meter_No;
            //    dt.Rows.Add(dr);
            //}

            //DataSet ds = new DataSet();
            //ds.Tables.Add(dt);

            //XElement xEle = new XElement("SpotBillOutPutBEInfoByXml",
            //                   from emp in customersDetails
            //                   select new XElement("SpotBillOutputDetails",
            //                                new XElement("Cons_AccNo", emp.Cons_AccNo),
            //                                  new XElement("BillNo", emp.BillNo),
            //                                  new XElement("Meter_No", emp.Meter_No),
            //                                  new XElement("Prev_Reading", emp.Prev_Reading),
            //                                  new XElement("CurrentReading", emp.CurrentReading),
            //                                  new XElement("BilledDate", emp.BilledDate),
            //                                  new XElement("BillAmount", emp.BillAmount),
            //                                  new XElement("Check_Meter_Reading", emp.Check_Meter_Reading),
            //                                  new XElement("Check_Meter_No", emp.Check_Meter_No)
            //                              ));

            //XmlDocument doc = new XmlDocument();
            //doc.LoadXml(xEle.ToString());
            int count = 0;
            string loginUser = Session[UMS_Resource.SESSION_LOGINID].ToString();
            foreach (SpotBillingOutputBe item in customersDetails)
            {
                item.CreatedBy = loginUser;
                item.Year = this.YearId;
                item.Month = this.MonthId;
                _objSpotBillOptBal.Insert(item, Statement.Insert);
                count++;
            }

            if (count > 0)
            {
                Message(Resource.HHOUTPUT_UPLOAD_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
            }
            else
            {
                Message(Resource.HHOUTPUT_UPLOAD_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            btnSave.Visible = false;
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                btnSave.Visible = false;
                DataTable dt = new DataTable();
                gvValidCustomerDetails.DataSource = dt;
                gvValidCustomerDetails.DataBind();
                gvInValidDetails.DataSource = dt;
                gvInValidDetails.DataBind();
                if (uploadHHOutputFile.HasFile)
                {
                    string AccountNos = string.Empty;
                    string CheckMeterNos = string.Empty;
                    string BillNos = string.Empty;
                    string fileName = System.IO.Path.GetFileName(uploadHHOutputFile.FileName).ToString();//Assinging FileName
                    string fileExtension = Path.GetExtension(fileName);//Storing Extension of file into variable Extension
                    if (fileExtension == ".txt".ToLower())
                    {
                        using (Stream fileStream = uploadHHOutputFile.PostedFile.InputStream)
                        using (StreamReader sr = new StreamReader(fileStream))
                        {
                            dt = CreateDataTableForOutput();
                            string input;
                            while ((input = sr.ReadLine()) != null)
                            {
                                if (!string.IsNullOrEmpty(input))
                                {
                                    string[] result = input.Split('|');
                                    dt = AddRow(dt, result);
                                    AccountNos = string.IsNullOrEmpty(AccountNos) ? result[0] : AccountNos + "," + result[0];
                                    CheckMeterNos = string.IsNullOrEmpty(CheckMeterNos) ? result[20] : CheckMeterNos + "," + result[20];
                                    BillNos = string.IsNullOrEmpty(BillNos) ? result[1] : BillNos + "," + result[1];
                                }
                            }
                        }
                        DataSet ds = CheckUploadedDetails(dt, AccountNos, CheckMeterNos, BillNos);
                        var isValid = (from x in ds.Tables[0].AsEnumerable()
                                       where x.Field<bool>("IsValidCycle").Equals(false)
                                       select x).ToList();

                        if (isValid.Count() > 0)
                        {
                            lblAlertMessage.Text = Resource.HHOUTPU_CUSTOMERS_NOT_UNDER_CYCLE;//"Uploaded customers are not under selected cycle.";
                            mpeAlertMessage.Show();
                        }
                        else
                        {
                            IEnumerable<SpotBillingOutputBe> result = (from x in dt.AsEnumerable()
                                                                       join y in ds.Tables[0].AsEnumerable() on x.Field<string>("Cons_AccNo") equals y.Field<string>("Cons_AccNo") into final
                                                                       from r in final.DefaultIfEmpty()
                                                                       select new SpotBillingOutputBe
                                                                       {
                                                                           Cons_AccNo = x.Field<string>("Cons_AccNo"),
                                                                           BillNo = x.Field<string>("BillNo"),
                                                                           Meter_No = x.Field<string>("Meter_No"),
                                                                           Prev_Reading = x.Field<string>("Prev_Reading"),
                                                                           CurrentReading = x.Field<string>("CurrentReading"),
                                                                           BilledDate = x.Field<string>("BilledDate"),
                                                                           BillAmount = Convert.ToDecimal(x.Field<string>("BillAmount")),
                                                                           Check_Meter_Reading = x.Field<string>("Check_Meter_Reading"),
                                                                           Check_Meter_No = x.Field<string>("Check_Meter_No"),
                                                                           CycleId = r.Field<string>("CycleId"),
                                                                           IsValid = r.Field<bool>("IsValid"),
                                                                           IsExists = r.Field<bool>("IsExists"),
                                                                           EnregyUnits = Convert.ToInt32(x.Field<string>("EnregyUnits")),
                                                                           EnergyCharges = Convert.ToDecimal(x.Field<string>("EnergyCharges")),
                                                                           FixedChages = Convert.ToDecimal(x.Field<string>("Fixed_Chages")),
                                                                           Tax = Convert.ToInt32(x.Field<string>("Tax")),
                                                                           Tax_Amount = Convert.ToDecimal(x.Field<string>("Tax_Amount")),
                                                                           NetArres = Convert.ToDecimal(x.Field<string>("NetArres")),
                                                                           AdjustmentAmount = Convert.ToDecimal(x.Field<string>("AdjustmentAmount")),
                                                                           BillType = x.Field<string>("BillType"),
                                                                           BalanceUnits = Convert.ToInt32(x.Field<string>("Balance_Units"))

                                                                       }).ToList();


                            IEnumerable<SpotBillingOutputBe> Valid = (from x in result.AsEnumerable()
                                                                      where x.IsValid.Equals(true) && x.IsExists.Equals(false)
                                                                      select new SpotBillingOutputBe
                                                                      {
                                                                          Cons_AccNo = x.Cons_AccNo,
                                                                          BillNo = x.BillNo,
                                                                          Meter_No = x.Meter_No,
                                                                          Prev_Reading = x.Prev_Reading,
                                                                          CurrentReading = x.CurrentReading,
                                                                          BilledDate = x.BilledDate,
                                                                          BillAmount = x.BillAmount,
                                                                          Check_Meter_Reading = x.Check_Meter_Reading,
                                                                          Check_Meter_No = x.Check_Meter_No,
                                                                          CycleId = x.CycleId,
                                                                          IsValid = x.IsValid,
                                                                          IsExists = x.IsExists,
                                                                          EnregyUnits = x.EnregyUnits,
                                                                          EnergyCharges = x.EnergyCharges,
                                                                          FixedChages = x.FixedChages,
                                                                          Tax = x.Tax,
                                                                          Tax_Amount = x.Tax_Amount,
                                                                          NetArres = x.NetArres,
                                                                          AdjustmentAmount = x.AdjustmentAmount,
                                                                          BillType = x.BillType,
                                                                          BalanceUnits = x.BalanceUnits
                                                                      }).ToList();

                            Session[UMS_Resource.SESSION_HHOUTPUT_DATA] = Valid;
                            gvValidCustomerDetails.DataSource = Valid;
                            gvValidCustomerDetails.DataBind();
                            if (Valid.Count() > 0)
                            {
                                btnSave.Visible = true;
                            }

                            IEnumerable<SpotBillingOutputBe> InValid = (from x in result.AsEnumerable()
                                                                        where x.IsValid.Equals(false) || x.IsExists.Equals(true)
                                                                        select new SpotBillingOutputBe
                                                                        {
                                                                            Cons_AccNo = x.Cons_AccNo,
                                                                            BillNo = x.BillNo,
                                                                            Meter_No = x.Meter_No,
                                                                            Prev_Reading = x.Prev_Reading,
                                                                            CurrentReading = x.CurrentReading,
                                                                            BilledDate = x.BilledDate,
                                                                            BillAmount = x.BillAmount,
                                                                            Check_Meter_Reading = x.Check_Meter_Reading,
                                                                            Check_Meter_No = x.Check_Meter_No,
                                                                            CycleId = x.CycleId,
                                                                            IsValid = x.IsValid,
                                                                            IsExists = x.IsExists
                                                                        }).ToList();

                            gvInValidDetails.DataSource = InValid;
                            gvInValidDetails.DataBind();
                        }
                    }
                    else
                    {
                        lblAlertMessage.Text = Resource.INVALID_FILE_FORMATE;
                        mpeAlertMessage.Show();
                    }
                }
                else
                {
                    lblAlertMessage.Text = Resource.PLEASE_BROWSE_FILE;
                    mpeAlertMessage.Show();
                }
            }
            catch
            {
            }
        }

        protected void gvInValidDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lbtnRemarks = (LinkButton)e.Row.FindControl("lbtnRemarks");
                if (lbtnRemarks.CommandArgument.ToLower() == true.ToString().ToLower())
                {
                    lbtnRemarks.Text = Resource.DUPLICATE_BILLNO;
                }
                else if (lbtnRemarks.CommandName.ToLower() == false.ToString().ToLower())
                {
                    lbtnRemarks.Text = Resource.INVALID_CHECKMETER;
                }
            }
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindMonths();
        }

        #endregion

        #region Methods

        private void BindYears()
        {
            try
            {

                XmlDocument xmlResult = new XmlDocument();
                xmlResult = _objBillGenerationBal.GetDetails(ReturnType.Multiple);
                BillGenerationListBe objBillsListBE = _ObjiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlYear, "Year", "Year", "Year", true);
                if (!string.IsNullOrEmpty(ddlYear.SelectedValue))
                {
                    BindMonths();
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        private void BindMonths()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                _objBillGenerationBe.Year = this.YearId;
                xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.Get);
                BillGenerationListBe objBillsListBE = _ObjiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlMonth, "MonthName", "Month", "Month", true);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));

            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private DataSet CheckUploadedDetails(DataTable dt, string accountNos, string checkMeterNos, string billNos)
        {
            XmlDocument xmlResult = new XmlDocument();
            SpotBillingOutputBe _objSpotBillOptBe = new SpotBillingOutputBe();
            _objSpotBillOptBe.Cons_AccNo = accountNos;
            _objSpotBillOptBe.Check_Meter_No = checkMeterNos;
            _objSpotBillOptBe.BillNo = billNos;
            _objSpotBillOptBe.CycleId = this.Cycle;
            xmlResult = _objSpotBillOptBal.GetDetails(_objSpotBillOptBe, ReturnType.Get);

            SpotBillingOutputListBe _objSpotBillOptListBe = new SpotBillingOutputListBe();

            _objSpotBillOptListBe = _ObjiDsignBAL.DeserializeFromXml<SpotBillingOutputListBe>(xmlResult);
            return _ObjiDsignBAL.ConvertListToDataSet<SpotBillingOutputBe>(_objSpotBillOptListBe.Items);

        }

        private DataTable AddRow(DataTable outputDTable, string[] result)
        {
            DataRow dr = outputDTable.NewRow();
            dr["Cons_AccNo"] = result[0];
            dr["BillNo"] = result[1];
            dr["Meter_No"] = result[2];
            dr["Prev_Reading"] = result[3];
            dr["CurrentReading"] = result[4];
            dr["BilledDate"] = result[5];
            dr["BillAmount"] = result[11];
            dr["Check_Meter_Reading"] = result[19];
            dr["Check_Meter_No"] = result[20];

            dr["EnregyUnits"] = result[6];
            dr["EnergyCharges"] = result[7];
            dr["Fixed_Chages"] = result[8];
            dr["Tax"] = result[9];
            dr["Tax_Amount"] = result[10];
            dr["NetArres"] = result[12];
            dr["AdjustmentAmount"] = result[13];
            dr["BillType"] = result[17];
            dr["Balance_Units"] = result[21];


            outputDTable.Rows.Add(dr);
            return outputDTable;
        }

        private DataTable CreateDataTableForOutput()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Cons_AccNo", typeof(string));
            dt.Columns.Add("BillNo", typeof(string));
            dt.Columns.Add("Meter_No", typeof(string));
            dt.Columns.Add("Prev_Reading", typeof(string));
            dt.Columns.Add("CurrentReading", typeof(string));
            dt.Columns.Add("BilledDate", typeof(string));
            dt.Columns.Add("BillAmount", typeof(string));
            dt.Columns.Add("Check_Meter_Reading", typeof(string));
            dt.Columns.Add("Check_Meter_No", typeof(string));

            dt.Columns.Add("EnregyUnits", typeof(string));
            dt.Columns.Add("EnergyCharges", typeof(string));
            dt.Columns.Add("Fixed_Chages", typeof(string));
            dt.Columns.Add("Tax", typeof(string));
            dt.Columns.Add("Tax_Amount", typeof(string));
            dt.Columns.Add("NetArres", typeof(string));
            dt.Columns.Add("AdjustmentAmount", typeof(string));
            dt.Columns.Add("BillType", typeof(string));
            dt.Columns.Add("Balance_Units", typeof(string));

            return dt;
        }

        #endregion
    }
}