﻿<%@ Page Title=":: Meter Reading ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="MeterReading.aspx.cs" Inherits="UMS_Nigeria.Billing.MeterReading" %>

<%@ Register Src="../UserControls/Authentication.ascx" TagName="Authentication" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../App_Themes/Green/style.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .normalfld
        {
            background-color: #FFFFFF;
        }
        .focusfld
        {
            background-color: #FFFFCC !important;
        }
        .gvtext
        {
            background: #CCC;
            color: #333;
            border: 1px solid #666;
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" AssociatedUpdatePanelID="UpdatePanelTotal">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="UpdatePanelTotal" runat="server" ChildrenAsTriggers="true">
        <ContentTemplate>
            <div class="out-bor">
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <asp:HiddenField ID="hfDial" runat="server" />
                <asp:HiddenField ID="hfDecimal" runat="server" />
                <div class="inner-sec">
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddState" runat="server" Text="<%$ Resources:Resource, METER_READING%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="clear">
                            </div>
                            <div class="inner-box">
                                <div class="inner-box1" style="width: 600px;">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litCountryName" runat="server" Text="<%$ Resources:Resource, READ_METHOD%>"></asp:Literal>
                                            <span class="span_star">*</span></label>
                                        <div class="space">
                                        </div>
                                        <asp:RadioButtonList ID="rblAccountType" CssClass="consume_radio" runat="server"
                                            RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="rblAccountType_SelectedIndexChanged">
                                        </asp:RadioButtonList>
                                        <span id="spanAccountType" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div id="divRoutesddl" runat="server">
                                    <div id="divBookNoDet" runat="server" visible="false">
                                        <div class="inner-box1">
                                            <div class="text-inner">
                                                <label for="name">
                                                    <asp:Literal ID="ltrlSearchAccNo" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>&nbsp;
                                                    /&nbsp; Old Account No&nbsp;
                                                </label>
                                                <div class="space">
                                                </div>
                                                <asp:TextBox ID="txtSearchAccount" Style="margin-left: 326px; position: relative;
                                                    top: -29px; margin-bottom: -36px;" runat="server" AutoPostBack="true" placeholder="Enter Global/Old Account No"
                                                    OnTextChanged="txtSearchAccount_TextChanged" CssClass="text-box" MaxLength="20"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtSearchAccount"
                                                    FilterType="Numbers">
                                                </asp:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                        <div class="inner-box1">
                                            <div class="text-inner">
                                                <label for="name">
                                                    <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                                                    <span class="span_star">*</span>
                                                </label>
                                                <div class="space">
                                                </div>
                                                <asp:DropDownList ID="ddlBU" AutoPostBack="true" CssClass="text-box text-select"
                                                    runat="server" OnSelectedIndexChanged="ddlBU_SelectedIndexChanged">
                                                    <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                                                </asp:DropDownList>
                                                <div class="space">
                                                </div>
                                                <span id="spanBU" class="span_color"></span>
                                            </div>
                                        </div>
                                        <div class="inner-box2">
                                            <div class="text-inner">
                                                <label for="name">
                                                    <asp:Literal ID="litServiceUnit" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal>
                                                    <span class="span_star">*</span>
                                                </label>
                                                <div class="space">
                                                </div>
                                                <asp:DropDownList ID="ddlSU" AutoPostBack="true" CssClass="text-box text-select"
                                                    Enabled="false" runat="server" OnSelectedIndexChanged="ddlSU_SelectedIndexChanged">
                                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                                </asp:DropDownList>
                                                <div class="space">
                                                </div>
                                                <span id="spanSU" class="span_color"></span>
                                            </div>
                                        </div>
                                        <div class="inner-box3">
                                            <div class="text-inner">
                                                <label for="name">
                                                    <asp:Literal ID="litServiceCenter" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal>
                                                    <span class="span_star">*</span>
                                                </label>
                                                <div class="space">
                                                </div>
                                                <asp:DropDownList ID="ddlSC" Enabled="false" CssClass="text-box text-select" runat="server"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSC_SelectedIndexChanged">
                                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                                </asp:DropDownList>
                                                <div class="space">
                                                </div>
                                                <span id="spanSC" class="span_color"></span>
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                        <div class="inner-box1">
                                            <div class="text-inner">
                                                <label for="name">
                                                    <asp:Literal ID="litCycles" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal>
                                                    <span class="span_star">*</span>
                                                </label>
                                                <div class="space">
                                                </div>
                                                <asp:DropDownList ID="ddlCycle" Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="ddlCycle_SelectedIndexChanged"
                                                    runat="server" CssClass="text-box text-select">
                                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                                </asp:DropDownList>
                                                <div class="space">
                                                </div>
                                                <span id="spanCycle" class="span_color"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inner-box2">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litBookNo" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal>
                                                <span class="span_star">*</span>
                                                <div id="divroutecaption" runat="server" style="width: 123px" visible="False">
                                                    <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:Resource, ROUTE_RADIOBUTTON%>"></asp:Literal>
                                                    <span class="span_star">*</span>
                                                </div>
                                            </label>
                                            <div class="space">
                                            </div>
                                            <div id="divRoutes" runat="server">
                                                <asp:DropDownList ID="ddlRoutes" CssClass="text-box text-select" Enabled="false"
                                                    runat="server" Visible="false" onblur="DisableGrid();">
                                                    <%--AutoPostBack="true" OnSelectedIndexChanged="ddlRoutes_SelectedIndexChanged"--%>
                                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:Button ID="btnRoutesDummy" runat="server" Style="display: none;" OnClick="btnRoutesDummy_Click" />
                                                <div class="space">
                                                </div>
                                                <span id="spanRoute" class="span_color"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <br />
                                </div>
                                <div class="clr">
                                </div>
                                <div id="divAccountNo" runat="server" visible="false">
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <label for="name" style="width: 410px; display: block; margin-bottom: -33px;">
                                                <asp:Literal ID="litAccountNumber" runat="server" Text="Global Account No"></asp:Literal>&nbsp;
                                                /&nbsp; Old Account No &nbsp; / &nbsp; Meter No&nbsp;<span class="span_star">*</span>
                                            </label>
                                            <asp:TextBox ID="txtAccountNumber" Style="margin-left: 431px; position: relative;
                                                top: -1px;" runat="server" placeholder="Enter Global/Old Account/Meter No" CssClass="text-box"
                                                MaxLength="20" onchange="DisableGrid;" onblur="DisableGrid();"></asp:TextBox>
                                            <%-- OnTextChanged="txtAccountNumber_TextChanged"
                                                AutoPostBack="true"--%>
                                            <asp:LinkButton ID="lkbSearchAcc" Visible="false" runat="server" Text="Search" OnClick="lkbSearchAcc_Click"></asp:LinkButton>
                                            <span id="spanAccNum" style="margin-left: 431px; width: 100%;" class="span_color">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litReadingDate" runat="server" Text="<%$ Resources:Resource, READING_DATE%>"></asp:Literal>
                                            <span class="span_star">*</span>
                                        </label>
                                        <div class="space">
                                        </div>
                                        <asp:TextBox ID="txtReadingDate" Style="width: 186px;" onchange="DoBlur(this);" onblur="DoBlur(this); DisableGrid();"
                                            onkeypress="DisableGrid();" CssClass="text-box" runat="server" placeholder="Enter Reading Date"
                                            AutoComplete="off"></asp:TextBox>
                                        <asp:Image ID="imgDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                            vertical-align: middle" AlternateText="Calender" runat="server" />
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtReadingDate"
                                            FilterType="Custom,Numbers" ValidChars="/">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanReadingDate" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box2">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litMeterReader" runat="server" Text="Marketer"></asp:Literal>
                                            <span class="span_star">*</span>
                                        </label>
                                        <div class="space">
                                        </div>
                                        <asp:DropDownList ID="ddlMeterReader" CssClass="text-box text-select" runat="server"
                                            onchange="return DropDownlistOnChangelbl(this,'spanMeterReader','Marketer')">
                                        </asp:DropDownList>
                                        <br />
                                        <span id="spanMeterReader" class="span_color"></span>
                                    </div>
                                </div>
                                <div style="clear: both;">
                                </div>
                                <div class="box_total">
                                    <div class="box_total_a" style="margin-left: 14px;">
                                        <asp:Button ID="btnGo" Text="<%$ Resources:Resource, GO_BUTTON%>" CssClass="box_s"
                                            OnClientClick="return Validate();" runat="server" OnClick="btnGo_Click" />
                                    </div>
                                </div>
                            </div>
                            <asp:Button ID="btnPopup" runat="server" Text="Button" Style="display: none;" />
                            <asp:Button ID="btnInvalid" runat="server" Text="Button" Style="display: none;" />
                            <asp:Button ID="btnUsageInvalid" runat="server" Text="Button" Style="display: none;" />
                            <asp:Button ID="btnInvalidPopupUsage" runat="server" Text="Button" Style="display: none;" />
                            <asp:ModalPopupExtender ID="ModalPopupExtenderPopupInvalidUsage" runat="server" PopupControlID="pnlPopupInvalidUsage"
                                TargetControlID="btnInvalidPopupUsage" BackgroundCssClass="modalBackground">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="pnlPopupInvalidUsage" runat="server" CssClass="modalPopup" Style="display: none;
                                border-color: #EEB72D;">
                                <div class="popheader" style="background-color: #EEB72D;">
                                    <asp:Label ID="lblinvalidUsagepopupHeader" runat="server" Text=""></asp:Label>
                                    <%-- <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, INVALIDACCOUNTNUMBER_HEADER%>"></asp:Literal>--%>
                                </div>
                                <div class="body">
                                    <asp:Label ID="lblinvalidUsagepopupBody" runat="server" Text=""></asp:Label>
                                    <%--  <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, INVALIDACCOUNTNUMBER_BODY%>"></asp:Literal>--%>
                                </div>
                                <div class="footer" align="right">
                                    <asp:Button ID="Button1" runat="server" Text="CONTINUE" CssClass="no" OnClick="btnContinuePopup_Click" />
                                    <asp:Button ID="Button2" runat="server" Text="<%$ Resources:Resource, CANCEL%>" CssClass="no"
                                        OnClick="Button2_Click" />
                                </div>
                            </asp:Panel>
                            <asp:ModalPopupExtender ID="ModalPopupExtenderInvalidUsage" runat="server" PopupControlID="pnlInvalidUsage"
                                TargetControlID="btnUsageInvalid" BackgroundCssClass="modalBackground" CancelControlID="btnstop">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="pnlInvalidUsage" runat="server" CssClass="modalPopup" Style="display: none;
                                border-color: #EEB72D;">
                                <div class="popheader" style="background-color: #EEB72D;">
                                    <asp:Label ID="lblInvalidUsageHeader" runat="server" Text=""></asp:Label>
                                    <%-- <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, INVALIDACCOUNTNUMBER_HEADER%>"></asp:Literal>--%>
                                </div>
                                <div class="body">
                                    <asp:Label ID="lblInvalidUsageBody" runat="server" Text=""></asp:Label>
                                    <%--  <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, INVALIDACCOUNTNUMBER_BODY%>"></asp:Literal>--%>
                                </div>
                                <div class="footer" align="right">
                                    <asp:Button ID="btnContinue" runat="server" Text="<%$ Resources:UMS_Resource, CONTINUE%>"
                                        CssClass="no" OnClick="btnContinue_Click" />
                                    <asp:Button ID="btnstop" runat="server" Text="<%$ Resources:Resource, CANCEL%>" CssClass="no" />
                                </div>
                            </asp:Panel>
                            <asp:ModalPopupExtender ID="ModalPopupExtenderGo" runat="server" BackgroundCssClass="modalBackground"
                                PopupControlID="pnlGo" TargetControlID="btnPopup" CancelControlID="btnGoOK">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="pnlGo" runat="server" Style="top: 50px; display: none;" CssClass="modalPopup"
                                ScrollBars="Auto">
                                <div class="popheader popheaderlblgreen">
                                    <asp:Literal ID="litAlreadyGenerated" runat="server" Text="<%$ Resources:Resource, EXISTEDUSER_METERREADING%>"></asp:Literal>
                                </div>
                                <div style="border: Solid 1px;">
                                    <div class="consumer_feild" style="padding-left: 5px; padding-top: 5px;">
                                        <div class="consume_name" style="width: 130px; margin-left: 0px;">
                                            <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NUMBER%>"></asp:Literal>
                                            :
                                        </div>
                                        <div class="consume_input" style="font-size: 12px; margin-top: 1px;">
                                            <asp:Label ID="lblpopGoAccNum" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblpopGlobalAccountNum" runat="server"></asp:Label>
                                            <asp:Label ID="lblpopRoute" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblpopGoCount" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblpopBillExist" runat="server" Visible="false"></asp:Label>
                                        </div>
                                        <div class="clear">
                                            &nbsp;</div>
                                        <div class="consume_name" style="width: 130px">
                                            <asp:Literal ID="Literal24" runat="server" Text="Old Account Number"></asp:Literal>
                                            :
                                        </div>
                                        <div class="consume_input">
                                            <asp:Label ID="lblpopOldAccNum" runat="server"></asp:Label>
                                        </div>
                                        <div class="clear">
                                            &nbsp;</div>
                                        <div class="consume_name" style="width: 130px">
                                            <asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:Resource, AVERAGE_READING%>"></asp:Literal>
                                            :
                                        </div>
                                        <div class="consume_input">
                                            <asp:Label ID="lblpopAvg" Style="font-size: 14px; font-weight: bold; color: #DE09FA;"
                                                runat="server"></asp:Label>
                                        </div>
                                        <div class="clear">
                                            &nbsp;</div>
                                        <div class="consume_name" style="width: 130px">
                                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, CUSTOMER_NAME%>"></asp:Literal>
                                            :
                                        </div>
                                        <div class="consume_input" style="width: 400px; font-size: 14px;">
                                            <asp:Label ID="lblpopGoCustomerDetails" runat="server"></asp:Label>
                                        </div>
                                        <div class="clear">
                                            &nbsp;</div>
                                        <div class="consume_name" style="width: 131px;">
                                            <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, PREVIOUS_READING%>"></asp:Literal>
                                            :
                                        </div>
                                        <div class="consume_input" style="width: 132px; position: relative; top: 0px;">
                                            <asp:Label ID="lblpopGoPreviousReading" Style="font-size: 14px; font-weight: bold;
                                                color: #DD4D5E;" runat="server"></asp:Label>
                                        </div>
                                        <div class="consume_name" style="width: 111px; position: relative; top: 4px;">
                                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, METER_NUMBER%>"></asp:Literal>
                                            :
                                        </div>
                                        <div class="consume_input" style="position: relative; top: 4px; font-size: 14px;">
                                            <asp:Label ID="lblpopGoMeterReadin" runat="server"></asp:Label>
                                        </div>
                                        <div class="clear">
                                            &nbsp;</div>
                                        <div class="consume_name" style="width: 105px">
                                            <asp:Literal ID="Literal13" runat="server" Text="Is ByPass /<br/>Tamper"></asp:Literal>
                                            :
                                        </div>
                                        <div class="consume_input">
                                            <asp:CheckBox ID="cbPopIsTamper" AutoPostBack="true" OnCheckedChanged="cbPopIsTamper_CheckedChanged"
                                                runat="server" />
                                        </div>
                                        <div class="clear">
                                            &nbsp;</div>
                                        <div class="consume_name" style="width: 105px">
                                            <asp:Literal ID="Literal22" runat="server" Text="Roll Over"></asp:Literal>
                                            :
                                        </div>
                                        <div class="consume_input">
                                            <asp:CheckBox ID="chkPopRolleOver" runat="server" onclick="CalculateNewUsagePopupChecked();" />
                                        </div>
                                        <div class="clear">
                                            &nbsp;</div>
                                        <div id="divPopTamperRemarks" runat="server" visible="false">
                                            <div class="consume_name" style="width: 144px;">
                                                <asp:Literal ID="Literal20" runat="server" Text="Tamper Remarks"></asp:Literal>
                                                :
                                            </div>
                                            <div class="consume_input" style="width: 232px; font-size: 14px;">
                                                <asp:Label ID="lblPopTamperRemarks" runat="server"></asp:Label>
                                            </div>
                                            <div class="clear">
                                                &nbsp;</div>
                                        </div>
                                        <div class="consume_name" style="width: 130px">
                                            <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, CURRENT_READING%>"></asp:Literal>
                                            :
                                        </div>
                                        <div class="consume_input" style="font-size: 14px;">
                                            <asp:Label ID="lblpopGoCurrent" runat="server" Text="" Visible="false"></asp:Label>
                                            <asp:Label ID="lblPopGoCurrentReading" runat="server" Text="" Style="display: none;"></asp:Label>
                                            <asp:TextBox ID="txtpopGoCurrent" runat="server" placeholder="Enter Current Reading"
                                                AutoComplete="off" Visible="false" onblur="return TextBoxBlurValidationlbl(this,'spanpopGoCurReading','Current Reading')"
                                                Width="180px"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtpopGoCurrent"
                                                FilterType="Numbers,Custom" FilterMode="ValidChars">
                                            </asp:FilteredTextBoxExtender>
                                            <span id="spanpopGoCurReading" class="span_color"></span>
                                        </div>
                                        <div class="clear">
                                            &nbsp;</div>
                                        <div class="consume_name">
                                            <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, USAGE_DETAILS%>"></asp:Literal>
                                            :
                                        </div>
                                        <div class="consume_input">
                                            <asp:Label ID="lblpopGoUsage" Style="color: green !important; font-weight: bold;
                                                font-size: 14px;" runat="server"></asp:Label>
                                            <asp:Label ID="lblpopInvalidUsageError" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label>
                                        </div>
                                        <div class="consumer_total">
                                            <div class="pad_10">
                                            </div>
                                        </div>
                                        <div class="consumer_feild">
                                            <div style="margin-left: 156px; float: left;">
                                                <asp:Button ID="btnEdit" Text="EDIT" CssClass="box_s" runat="server" OnClick="btnEdit_Click" />
                                                <asp:Button ID="btnUpdate" Text="<%$ Resources:Resource, UPDATE%>" CssClass="box_s"
                                                    OnClientClick="return ValidPopup()" Visible="false" runat="server" OnClick="btnUpdate_Click" />
                                                <asp:Button ID="btnGoOK" Text="<%$ Resources:Resource, CANCEL%>" CssClass="box_s"
                                                    runat="server" />
                                            </div>
                                        </div>
                                        <div class="consumer_total">
                                            <div class="pad_10">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="consumer_total">
                                        <div class="pad_10">
                                        </div>
                                    </div>
                                    <br />
                                    <br />
                                </div>
                            </asp:Panel>
                            <asp:ModalPopupExtender ID="ModalPopupExtenderInvalidAccount" runat="server" PopupControlID="PanelInvalidAccount"
                                TargetControlID="btnInvalid" BackgroundCssClass="modalBackground" CancelControlID="btnNo">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="PanelInvalidAccount" runat="server" CssClass="modalPopup" Style="display: none;
                                border-color: #CD3333;">
                                <div class="popheader" style="background-color: #CD3333;">
                                    <asp:Label ID="lblHeaderInvalid" runat="server" Text=""></asp:Label>
                                    <%-- <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, INVALIDACCOUNTNUMBER_HEADER%>"></asp:Literal>--%>
                                </div>
                                <div class="body">
                                    <asp:Label ID="lblInvalidBody" runat="server" Text=""></asp:Label>
                                    <%--  <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, INVALIDACCOUNTNUMBER_BODY%>"></asp:Literal>--%>
                                </div>
                                <div class="footer" align="right">
                                    <asp:Button ID="btnNo" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="no" />
                                </div>
                            </asp:Panel>
                            <asp:Button ID="btnGvPopInvalidUsage" runat="server" Text="Button" Style="display: none;" />
                            <asp:ModalPopupExtender ID="ModalPopupExtenderGvInvalidUsage" runat="server" PopupControlID="PanelGvPopInvalidUsage"
                                TargetControlID="btnGvPopInvalidUsage" BackgroundCssClass="modalBackground" CancelControlID="btnCancelGvPopInvalidUsage">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="PanelGvPopInvalidUsage" runat="server" CssClass="modalPopup" Style="display: none;
                                border-color: #EEB72D;">
                                <div class="popheader" style="background-color: #EEB72D;">
                                    <asp:Label ID="lblGvPopupInvalidUsageHeader" runat="server" Text=""></asp:Label>
                                    <%-- <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, INVALIDACCOUNTNUMBER_HEADER%>"></asp:Literal>--%>
                                </div>
                                <div class="body">
                                    <asp:Label ID="lblGvPopupInvalidUsageBody" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="lblInvalidUsageGvAlerts" runat="server" Text="" Visible="false"></asp:Label>
                                    <%--  <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, INVALIDACCOUNTNUMBER_BODY%>"></asp:Literal>--%>
                                </div>
                                <div class="footer" align="right">
                                    <asp:Button ID="Button4" runat="server" Text="<%$ Resources:UMS_Resource, CONTINUE%>"
                                        CssClass="no" OnClick="btnContinuePopupGvInvalidUsage_Click" />
                                    <asp:Button ID="btnCancelGvPopInvalidUsage" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="no" />
                                </div>
                            </asp:Panel>
                            <asp:Button ID="btnLogin" runat="server" Text="Button" Style="display: none;" />
                            <asp:ModalPopupExtender ID="ModalPopupExtenderLogin" runat="server" PopupControlID="PanelLogin"
                                TargetControlID="btnLogin" BackgroundCssClass="modalPopup" CancelControlID="ButtonL">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="PanelLogin" DefaultButton="btnAuthentication" runat="server" CssClass="modalPopup"
                                Style="display: none; border-color: #639B00;">
                                <div class="popheader" style="background-color: #639B00;">
                                    <asp:Label ID="Label1" runat="server" Text="Authentication Required"></asp:Label>
                                    <%-- <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, INVALIDACCOUNTNUMBER_HEADER%>"></asp:Literal>--%>
                                </div>
                                <div class="body" style="padding: 30px;">
                                    <asp:Label ID="Label2" runat="server" Text="Retype Your Password"></asp:Label>
                                    <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" placeholder="Enter Password"
                                        autocomplete="off" onblur="return TextBoxBlurValidationlbl(this,'spanAuthentication','Password')"></asp:TextBox>
                                    <span id="spanAuthentication" runat="server" class="span_color"></span>
                                    <br />
                                    <asp:Label ID="LabelStatusLogin" runat="server" class="span_color" Visible="false"></asp:Label>
                                    <%--  <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, INVALIDACCOUNTNUMBER_BODY%>"></asp:Literal>--%>
                                </div>
                                <div class="footer" align="right">
                                    <div class="fltl Nothanks">
                                        <a href="../Home.aspx">No Thanks, Go To Home</a></div>
                                    <div class="fltl" style="margin-top: 5px;">
                                        <asp:Button ID="btnAuthentication" runat="server" Text="<%$ Resources:UMS_Resource, CONTINUE%>"
                                            CssClass="continue" OnClick="btnAuthenticarion_Click" />
                                        <asp:Button ID="ButtonL" runat="server" Text="<%$ Resources:Resource, CANCEL%>" CssClass="nobt" />
                                        <%--<asp:Button ID="ButtonCancelLogin" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                            CssClass="no" OnClientClick="window.location.assign('../Home.aspx')" />--%>
                                    </div>
                                    <div class="clear pad_5">
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Button ID="ButtonNegativeUsage" runat="server" Text="Button" Style="display: none;" />
                            <asp:ModalPopupExtender ID="ModalPopupExtenderNegativeUsage" runat="server" PopupControlID="PanelNegUsage"
                                TargetControlID="ButtonNegativeUsage" BackgroundCssClass="modalBackground" CancelControlID="ButtonCancelNeg">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="PanelNegUsage" runat="server" CssClass="modalPopup" Style="display: none;
                                border-color: #EEB72D; width: 560px;">
                                <div class="popheader" style="background-color: #EEB72D;">
                                    <asp:Literal ID="LiteralNegUsageHeader" runat="server" Text="<%$ Resources:Resource, RESETMETER%>"></asp:Literal>
                                </div>
                                <div class="body">
                                    <asp:Literal ID="LiteralNegUsageBody" runat="server" Text="<%$ Resources:Resource, RESETMETER_BODY%>"></asp:Literal>
                                </div>
                                <div class="footer" align="right">
                                    <asp:Button ID="Button5" runat="server" Text="CONTINUE" CssClass="no" OnClick="btnNegUsageContinue_Click" />
                                    <asp:Button ID="ButtonCancelNeg" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="no" />
                                </div>
                            </asp:Panel>
                            <div class="clr">
                            </div>
                            <br />
                            <!-- dkfjsdjfsdf-->
                            <div>
                                <div id="divAccountWise" runat="server" class="out-bor" visible="false" onkeypress="return WebForm_FireDefaultButton(event, '<%= btnSaveNext.ClientID %>')">
                                    <div class="consumer_feild" style="padding-left: 5px; padding-top: 5px;">
                                        <div class="consume_name" style="width: 130px;">
                                            <asp:Literal ID="litAccountNumber1" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NUMBER%>"></asp:Literal>
                                            :
                                        </div>
                                        <div class="consume_input" style="font-size: 14px; width: 168px;">
                                            <asp:Label ID="lblAccountNumber" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblGlobalAccountNumber" runat="server"></asp:Label>
                                            <asp:Label ID="lblRoute" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblCountAcc" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblBillExist" runat="server" Visible="false"></asp:Label>
                                        </div>
                                        <div class="consume_name  consume_left" style="width: 130px">
                                            <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, AVERAGE_READING%>"></asp:Literal>
                                            :
                                        </div>
                                        <div class="consume_input" style="width: 155px;">
                                            <asp:Label ID="lblAvgAcc" Style="font-size: 14px; font-weight: bold; color: #DE09FA;"
                                                runat="server"></asp:Label>
                                        </div>
                                        <div class="consume_name consume_left" style="width: 144px; margin-left: 5px;">
                                            <asp:Literal ID="litCustomerDetails" runat="server" Text="Customer Name"></asp:Literal>
                                            :
                                        </div>
                                        <div class="consume_input" style="width: 232px; font-size: 14px;">
                                            <asp:Label ID="lblCustomerDetails" runat="server"></asp:Label>
                                        </div>
                                        <div class="clear">
                                            &nbsp;</div>
                                        <div class="consume_name" style="width: 130px;">
                                            <asp:Literal ID="Literal23" runat="server" Text="Old Account Number"></asp:Literal>
                                            :
                                        </div>
                                        <div class="consume_input" style="width: 166px; font-size: 14px;">
                                            <asp:Label ID="lblOldAccountNo" runat="server"></asp:Label>
                                        </div>
                                        <div class="consume_name  consume_left" style="width: 135px">
                                            <asp:Literal ID="litPreviousReading" runat="server" Text="<%$ Resources:Resource, PREVIOUS_READING%>"></asp:Literal>
                                            :
                                        </div>
                                        <div class="consume_input" style="width: 153px;">
                                            <asp:Label ID="lblPreviousReading" Style="font-size: 14px; font-weight: bold; color: #DD4D5E;"
                                                runat="server"></asp:Label>
                                        </div>
                                        <div class="consume_name consume_left" style="width: 144px; margin-left: 5px;">
                                            <asp:Literal ID="litMeterNumber" runat="server" Text="<%$ Resources:Resource, METER_NUMBER%>"></asp:Literal>
                                            :
                                        </div>
                                        <div class="consume_input" style="font-size: 14px;">
                                            <asp:Label ID="lblMeterNumber" runat="server"></asp:Label>
                                        </div>
                                        <div class="clear">
                                            &nbsp;</div>
                                        <div class="consume_name" style="width: 130px;">
                                            <asp:Literal ID="Literal14" runat="server" Text="IS ByPass / Tamper"></asp:Literal>
                                            :
                                            <asp:CheckBox ID="cbCustIsByPass" AutoPostBack="true" OnCheckedChanged="cbCustIsByPass_CheckedChanged"
                                                runat="server" Style="margin-left: 25px;" />
                                        </div>
                                        <div class="consume_input" style="width: 155px;">
                                        </div>
                                        <div class="consume_name  consume_left" style="width: 130px; margin-left: 6px; margin-right: 17px;">
                                            <asp:Literal ID="Literal21" runat="server" Text="Roll Over"></asp:Literal>:
                                            <asp:CheckBox ID="chkRollOver" runat="server" onclick="CalculateNewUsageChecked();" />
                                        </div>
                                        <div class="consume_input no_border" style="text-align: left; margin-left: -45px;
                                            width: 185px;">
                                        </div>
                                        <div class="consume_name consume_left" style="width: 144px; margin-left: 5px;">
                                            <asp:Literal ID="litCurrentReading" runat="server" Text="<%$ Resources:Resource, CURRENT_READING%>"></asp:Literal>
                                            :
                                        </div>
                                        <div class="consume_input no_border">
                                            <asp:Label ID="lblCurrentReading" runat="server" Text="" Visible="false"></asp:Label>
                                            <asp:TextBox ID="txtCurrentReading" runat="server" placeholder="Enter Current Reading"
                                                AutoComplete="off" Visible="false" onblur="return TextBoxBlurValidationlbl(this,'spanCurrentReading','Current Reading')"
                                                onkeypress="return isNumberKey(event,this)" onkeyup="GetUsage(this)" Width="146px"
                                                Style="margin-left: -15px;"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtCurrentReading"
                                                FilterType="Numbers,Custom" FilterMode="ValidChars">
                                            </asp:FilteredTextBoxExtender>
                                            <span id="spanCurrentReading" class="span_color"></span>
                                        </div>
                                        <div class="clear">
                                            &nbsp;</div>
                                        <div class="consume_name" style="width: 130px;">
                                            <asp:Literal ID="litUsageDetails" runat="server" Text="<%$ Resources:Resource, USAGE_DETAILS%>"></asp:Literal>
                                            :
                                        </div>
                                        <div class="consume_input" style="width: 165px; font-size: 14px;">
                                            <asp:Label ID="lblUsageDetails" CssClass="usagedtl" runat="server"></asp:Label>
                                        </div>
                                        <div id="divTamperRemarks" runat="server" visible="false">
                                            <div class="consume_name  consume_left" style="width: 130px">
                                                <asp:Literal ID="Literal19" runat="server" Text="Tamper Remarks"></asp:Literal>
                                                :
                                            </div>
                                            <div class="consume_input" style="width: 232px; font-size: 14px;">
                                                <asp:Label ID="lblTamperRemarks" runat="server"></asp:Label>
                                            </div>
                                            <div class="clear">
                                                &nbsp;</div>
                                        </div>
                                        <%--  <div class="consume_name  consume_left" style="width: 130px; margin-left: 6px; margin-right: 17px;">
                                            <asp:Literal ID="Literal22" runat="server" Text="Roll Over"></asp:Literal>:
                                            <asp:CheckBox ID="CheckBox1" runat="server" onclick="CalculateNewUsageChecked();" />
                                        </div>
                                        <div class="consume_input no_border" style="text-align: left;">
                                        </div>--%>
                                        <br />
                                        <div style="font-weight: bold; padding-right: 84px;">
                                            <%--<b>NOTE:</b>--%>
                                            <asp:Label ID="lblEstimatedBillDetails" runat="server"></asp:Label>
                                        </div>
                                        <div class="consumer_total">
                                            <div class="pad_10">
                                            </div>
                                        </div>
                                        <div class="consumer_feild">
                                            <div style="height: 40px; margin-right: 167px; overflow: auto;">
                                                <asp:Button ID="btnSaveNext" Text="<%$ Resources:Resource, SAVENEXT%>" CssClass="box_s"
                                                    Style="float: right !important;" runat="server" OnClick="btnSaveNext_Click" OnClientClick="return ValidSaveReading()" /><%----%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="consumer_total">
                                        <div class="pad_10">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="curent_meter_total2">
                                <div id="divRouteSequenceWise" runat="server" class="routwise pad_10" visible="false"
                                    style="border: 1px solid green;">
                                    <div class="grid_boxes" id="divgrid" runat="server">
                                        <div class="grid_pagingTwo_top">
                                            <div class="paging_top_titleTwo">
                                                <asp:Literal ID="litCountriesList" runat="server" Text="<%$ Resources:Resource, ROUTE_WISE%>"></asp:Literal>
                                            </div>
                                            <div align="right" class="fltl" runat="server" id="divExport" visible="true" style="margin-bottom: 15px;">
                                                <%--<asp:Button ID="btnPrint" runat="server" OnClientClick="return printGrid()" CssClass="print"
                                                Text="Print" />--%>
                                                <asp:Button ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" Visible="false"
                                                    CssClass="box_s" Text="EXPORT TO EXCEL" /></div>
                                            <div class="paging_top_rightTwo_content" id="divpaging" runat="server">
                                                <div align="right" class="marg_20t" runat="server" id="divLinks">
                                                    <asp:LinkButton ID="lkbtnFirst" runat="server" CssClass="pagingfirst" OnClick="lkbtnFirst_Click">
                                                        <asp:Literal ID="litFirst" runat="server" Text="<%$ Resources:Resource, FIRST%>"></asp:Literal></asp:LinkButton>
                                                    &nbsp;|
                                                    <asp:LinkButton ID="lkbtnPrevious" runat="server" CssClass="pagingfirst" OnClick="lkbtnPrevious_Click">
                                                        <asp:Literal ID="litPrevious" runat="server" Text="<%$ Resources:Resource, PREVIOUS%>"></asp:Literal></asp:LinkButton>
                                                    &nbsp;|
                                                    <asp:Label ID="lblCurrentPage" runat="server" Font-Bold="true"></asp:Label>
                                                    &nbsp;|
                                                    <asp:LinkButton ID="lkbtnNext" runat="server" CssClass="pagingfirst" OnClick="lkbtnNext_Click">
                                                        <asp:Literal ID="litNext" runat="server" Text="<%$ Resources:Resource, NEXT%>"></asp:Literal></asp:LinkButton>
                                                    &nbsp;|
                                                    <asp:LinkButton ID="lkbtnLast" runat="server" CssClass="pagingfirst" OnClick="lkbtnLast_Click">
                                                        <asp:Literal ID="litLast" runat="server" Text="<%$ Resources:Resource, LAST%>"></asp:Literal></asp:LinkButton>
                                                    &nbsp;|<asp:Literal ID="litPageSize" runat="server" Text="<%$ Resources:Resource, PAGE_SIZE%>"></asp:Literal>
                                                    <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" CssClass="paging-size"
                                                        OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <div id="Div1" class="grid_tb" runat="server" align="center" style="overflow-x: auto;">
                                        <asp:GridView ID="gvMeterReadingList" runat="server" AutoGenerateColumns="False"
                                            AlternatingRowStyle-CssClass="color" HeaderStyle-CssClass="grid_tb_hd" ShowHeaderWhenEmpty="true"
                                            Width="100%" OnRowDataBound="gvMeterReadingList_RowDataBound">
                                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                            <EmptyDataTemplate>
                                                No Records found For selected criteria.
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                                    HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%--    <%#Container.DataItemIndex+1%>--%>
                                                        <asp:Label ID="lblRowNumber" runat="server" class="lblgvAccountNos" Text='<%#Eval("RowNumber") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" runat="server"
                                                    Visible="false" HeaderStyle-Width="8%" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAccountNo" runat="server" class="lblgvAccountNos" Text='<%#Eval("AccNum") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" runat="server"
                                                    HeaderStyle-Width="8%" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGlobalAccountNo" runat="server" class="lblgvAccountNos" Text='<%#Eval("AccountNo")+"-" +Eval("AccNum") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" runat="server" HeaderStyle-Width="8%"
                                                    HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCustomerTypeId" runat="server" Text='<%#Eval("CustomerTypeId") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblCustomerDetails" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--<asp:TemplateField HeaderText="Last Bill ReadType" runat="server" HeaderStyle-Width="10%"
                                                    HeaderStyle-HorizontalAlign="Center" Visible="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLastBillReadType" runat="server" Text='<%#Eval("LastBillReadType") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="<%$ Resources:Resource, METER_NUMBER_WITH_DAILS%>"
                                                    runat="server" HeaderStyle-Width="8%" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMeterNoOnly" Visible="false" runat="server" Text='<%#Eval("MeterNo") %>'></asp:Label>
                                                        <asp:Label ID="lblMeterNo" runat="server" Text='<%#Eval("MeterNo") + " [" +  Eval("MeterDials") + "]" %>'></asp:Label>
                                                        <asp:Label ID="lblMultiplier" runat="server" Text='<%#Eval("Multiplier") %>' Style="display: none;"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" runat="server"
                                                    HeaderStyle-Width="8%" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAOldccountNo" runat="server" class="lblgvAccountNos" Text='<%#Eval("OldAccountNo") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<%$ Resources:Resource, PREVIOUS_READING%>" runat="server"
                                                    HeaderStyle-Width="6%" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGvPreviousReading" Style="font-size: 14px; font-weight: bold;"
                                                            runat="server" Text='<%#Eval("PreviousReading") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<%$ Resources:Resource, CURRENT_READING%>" runat="server"
                                                    HeaderStyle-Width="6%" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:UpdatePanel runat="server" ID="UpId" ChildrenAsTriggers="true">
                                                            <ContentTemplate>
                                                                <asp:Label ID="lblGvCurrentReading" runat="server" Text='<%#Eval("PresentReading") %>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:Label ID="lblItemDials" runat="server" Text='<%#Eval("MeterDials") %>' Style="display: none;"></asp:Label>
                                                                <asp:Label ID="lblItemDecimals" runat="server" Text='<%#Eval("Decimals") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblGvExist" runat="server" Text='<%#Eval("PrvExist") %>' Visible="false"></asp:Label>
                                                                <%-- <asp:Label ID="lblIsBillingMonthLocked" runat="server" Text='<%#Eval("IsActiveMonth") %>'
                                                                    Visible="false"></asp:Label>--%>
                                                                <asp:Label ID="lblGvIsBilled" runat="server" Text='<%#Eval("IsBilled") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblGvCountReading" runat="server" class="lblgvReadingsCount" Text='<%#Eval("TotalReadings") %>'
                                                                    Visible="false"></asp:Label>
                                                                <asp:TextBox ID="txtGvCurrentReading" CssClass="text-box" runat="server" class="txtgvCurrentReading"
                                                                    Text='<%#Eval("PresentReading") %>' MaxLength='<%#Eval("MeterDials") %>' AutoComplete="off"
                                                                    placeholder="Enter Current Reading"></asp:TextBox>
                                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtGvCurrentReading"
                                                                    FilterType="Numbers" FilterMode="ValidChars">
                                                                </asp:FilteredTextBoxExtender>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<%$ Resources:Resource, USAGE_DETAILS%>" runat="server"
                                                    HeaderStyle-Width="6%" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGvUsage" class="lblgvUsage" Text='<%#Eval("Usage") %>' Style="font-size: 14px;
                                                            font-weight: bold;" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<%$ Resources:Resource, AVERAGE_READING%>" runat="server"
                                                    HeaderStyle-Width="6%" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGvAvgReading" Style="font-size: 14px; font-weight: bold;" runat="server"
                                                            Text='<%#Eval("AverageReading") %>' class="lblgvReadingAverage"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Last Reading Date" runat="server" HeaderStyle-Width="6%"
                                                    HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGvLatestReadDate" runat="server" Text='<%#Eval("LatestDate") %>'
                                                            class="lblgvReadingAverage"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="IsByPass / Tamper" runat="server" HeaderStyle-Width="4%"
                                                    HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIsTamper" Visible="false" runat="server" Text='<%#Eval("IsTamper") %>'></asp:Label>
                                                        <asp:CheckBox ID="cbIsTamper" runat="server" AutoPostBack="true" OnCheckedChanged="cbIsTamper_CheckedChanged" />
                                                        <asp:HiddenField ID="hfIsTamper" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Rollover" runat="server" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center"
                                                    Visible="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRollover" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<%$ Resources:Resource, REMARKS%>" runat="server"
                                                    HeaderStyle-Width="20%" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRemarks" class="lblgvUsage" Style="font-size: 14px; font-weight: bold;"
                                                            runat="server"></asp:Label>
                                                        <asp:HiddenField ID="hfIsHaveLatestReading" runat="server" Value='<%#Eval("IsExists") %>' />
                                                        <%--<asp:HiddenField ID="hfIsLocked" runat="server" Value='<%#Eval("IsLocked") %>' />--%>
                                                        <asp:HiddenField ID="hfIsMeterChangedDateExists" runat="server" Value='<%#Eval("IsMeterChangedDateExists") %>' />
                                                        <asp:HiddenField ID="hfIsMeterAssignedDateExists" runat="server" Value='<%#Eval("IsMeterAssignedDateExists") %>' />
                                                        <asp:HiddenField ID="hfIsMeterApproval" runat="server" Value='<%#Eval("IsMeterChangeApproval") %>' />
                                                        <asp:HiddenField ID="hfIsRollOver" runat="server" Value='<%#Eval("Rollover") %>' />
                                                        <asp:HiddenField ID="hfDescription" runat="server" Value='<%#Eval("Description") %>' />
                                                        <asp:HiddenField ID="hfIsPerformAction" runat="server" Value='<%#Eval("IsPerformAction") %>' />
                                                        <asp:HiddenField ID="hfIsReadToDirectApproval" runat="server" Value='<%#Eval("IsReadToDirectApproval") %>' />
                                                        <asp:HiddenField ID="hfApproveStatusId" runat="server" Value='<%#Eval("ApproveStatusId") %>' />
                                                        <asp:HiddenField ID="hfIsFutureMonthBilled" runat="server" Value='<%#Eval("IsLatestBill") %>' />
                                                        <asp:HiddenField ID="hfEstimatedDate" runat="server" Value='<%#Eval("EstimatedBillDate") %>' />
                                                        <asp:HiddenField ID="hfIsValid" runat="server" />
                                                        <asp:HiddenField ID="hfActiveStatus" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <div class="clr">
                                        </div>
                                        <br />
                                        <div class="consumer_feild">
                                            <%--OnClientClick="return ValidateRouteOrBookWise()"--%>
                                            <div style="margin-left: 156px; height: 40px; overflow: auto; float: right;">
                                                <asp:Button ID="btnGvSave" Text="VALIDATE" CssClass="box_s" runat="server" OnClick="btnGvSave_Click" />
                                                <asp:Button ID="btnGridRecordsCancel" runat="server" Text="EDIT" CssClass="box_s"
                                                    OnClick="btnGridRecordsCancel_Click" />
                                                <asp:Button ID="btnGridRecordsProceed" runat="server" Text="<%$ Resources:Resource, SAVE%>"
                                                    OnClick="btnGvContinue_Click" CssClass="box_s" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="consumer_total">
                                        <div class="pad_10">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:Button ID="btnFinalGvSave" runat="server" Text="Button" Style="display: none;" />
                            <asp:ModalPopupExtender ID="mpRouteWiseReadingsAlert" runat="server" PopupControlID="pnlListBoxes"
                                TargetControlID="btnFinalGvSave" BackgroundCssClass="modalBackground" CancelControlID="btnGvCancel">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="pnlListBoxes" runat="server" CssClass="modalPopup" Style="display: none;
                                border-color: #EEB72D; min-height: 50%; min-width: 40%; left: 376px;">
                                <div class="popheader" style="background-color: #EEB72D;">
                                    <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, LISTBOXESHEADER_METERREADING%>"></asp:Literal>
                                </div>
                                <div class="body">
                                    <div class="consumer_total">
                                        <div class="pad_10">
                                        </div>
                                    </div>
                                    <div class="consumer_total">
                                        <div class="pad_10">
                                        </div>
                                    </div>
                                    <table>
                                        <tr>
                                            <td>
                                                <div>
                                                    <asp:Literal ID="Litera27" runat="server" Text="<%$ Resources:Resource, ACCHAVINGNEGATIVE_METERREADING%>"></asp:Literal>
                                                    <br />
                                                    <asp:ListBox ID="lbxInvalidUsageDetails" Height="330px" Width="160px" runat="server">
                                                    </asp:ListBox>
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, ACCHAVINGHUGE_METERREADING%>"></asp:Literal>
                                                    <br />
                                                    <asp:ListBox ID="lbxHugeUsage" Height="330px" Width="160px" runat="server"></asp:ListBox>
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:Resource, ACCHAVINGEMPTY_METERREADING%>"></asp:Literal>
                                                    <br />
                                                    <asp:ListBox ID="lbxEmpty" Height="330px" Width="160px" runat="server"></asp:ListBox>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="footer" align="right">
                                    <asp:Button ID="btnGvContinue" runat="server" Text="<%$ Resources:UMS_Resource, CONTINUE%>"
                                        OnClick="btnGvContinue_Click" CssClass="no" />
                                    <asp:Button ID="btnGvCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="no" />
                                </div>
                            </asp:Panel>
                            <asp:Button ID="btnpopInvalidLatest" runat="server" Text="Button" Style="display: none;" />
                            <asp:ModalPopupExtender ID="mpeInvalidLatest" runat="server" PopupControlID="pnlInvalidLatest"
                                TargetControlID="btnpopInvalidLatest" BackgroundCssClass="modalBackground" CancelControlID="btnInvalidLatest">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="pnlInvalidLatest" runat="server" CssClass="modalPopup" Style="display: none;
                                border-color: #CD3333;">
                                <div class="popheader" style="background-color: #CD3333;">
                                    <asp:Label ID="lnlInvalidLatestHeader" runat="server" Text=""></asp:Label>
                                    <%-- <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, INVALIDACCOUNTNUMBER_HEADER%>"></asp:Literal>--%>
                                </div>
                                <div class="body">
                                    <asp:Label ID="lnlInvalidLatestBody" runat="server" Text=""></asp:Label>&nbsp;&nbsp;
                                    <asp:Label ID="lblLatestPopupNew" runat="server" Style="color: #2921C0;" Text=""></asp:Label>
                                    <asp:Label ID="lblLatestPopup" runat="server" Style="color: #2921C0;" Text="" Visible="false"></asp:Label>
                                    <%--  <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, INVALIDACCOUNTNUMBER_BODY%>"></asp:Literal>--%>
                                </div>
                                <div class="footer" align="right">
                                    <asp:Button ID="btnInvalidLatest" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="no" />
                                    <asp:Button ID="btnProceed" runat="server" Text="PROCEED" CssClass="no" OnClick="btnProceed_Click" />
                                </div>
                            </asp:Panel>
                            <asp:ModalPopupExtender ID="MpestimatedBill" runat="server" PopupControlID="pnlEstimatedBill"
                                TargetControlID="btnEstimatedOk" BackgroundCssClass="modalBackground" CancelControlID="btnEstimatedOk">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="pnlEstimatedBill" runat="server" CssClass="modalPopup" Style="display: none;
                                border-color: #CD3333;">
                                <div class="popheader" style="background-color: #CD3333;">
                                    <asp:Label ID="lblEstimatedBillMessage" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="footer" align="right">
                                    <asp:Button ID="btnEstimatedOk" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="no" />
                                </div>
                            </asp:Panel>
                            <asp:ModalPopupExtender ID="MpeShowExistMesage" runat="server" PopupControlID="pnlShowExistMesage"
                                TargetControlID="btnShowExistMesage" BackgroundCssClass="modalBackground" CancelControlID="btnShowExistMesage">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="pnlShowExistMesage" runat="server" CssClass="modalPopup" Style="display: none;
                                border-color: #CD3333;">
                                <div class="popheader" style="background-color: red;">
                                    <asp:Label ID="lblShowExistMesage" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="clear pad_10">
                                </div>
                                <div class="consumer_feild">
                                    <div class="consume_name" style="margin-left: 60px; width: 170px;">
                                        <asp:Literal ID="Literal25" runat="server" Text="Read Date"></asp:Literal>
                                    </div>
                                    <div class="consume_input c_left">
                                        <asp:Label ID="lblShowReadDate" runat="server"></asp:Label>
                                    </div>
                                    <div class="clear pad_5">
                                    </div>
                                    <div class="consume_name" style="margin-left: 60px; width: 170px;">
                                        <asp:Literal ID="Literal27" runat="server" Text="Previous Reading"></asp:Literal>
                                    </div>
                                    <div class="consume_input c_left">
                                        <asp:Label ID="lblShowPreviousReading" runat="server"></asp:Label>
                                    </div>
                                    <div class="consume_name" style="margin-left: 60px; width: 170px;">
                                        <asp:Literal ID="Literal28" runat="server" Text="Current Reading"></asp:Literal>
                                    </div>
                                    <div class="consume_input c_left">
                                        <asp:Label ID="lblShowPresentReading" runat="server"></asp:Label>
                                    </div>
                                    <div class="clear pad_5">
                                    </div>
                                    <div class="consume_name" style="margin-left: 60px; width: 170px;">
                                        <asp:Literal ID="Literal29" runat="server" Text="Usage"></asp:Literal>
                                    </div>
                                    <div class="consume_input c_left">
                                        <asp:Label ID="lblShowUsage" runat="server"></asp:Label>
                                    </div>
                                    <div class="consume_name" style="margin-left: 60px; width: 170px;">
                                        <asp:Literal ID="Literal30" runat="server" Text="Average"></asp:Literal>
                                    </div>
                                    <div class="consume_input c_left">
                                        <asp:Label ID="lblShowAverage" runat="server"></asp:Label>
                                    </div>
                                    <div class="clear pad_5">
                                    </div>
                                </div>
                                <div class="footer" align="right">
                                    <asp:Button ID="btnShowExistMesage" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="no" />
                                </div>
                            </asp:Panel>
                            <%-- Grid Alert popup Start--%>
                            <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                                TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                            </asp:ModalPopupExtender>
                            <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                            <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                                <div class="popheader popheaderlblred">
                                    <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                                </div>
                                <div class="footer popfooterbtnrt">
                                    <div class="fltr popfooterbtn">
                                        <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                            CssClass="ok_btn" />
                                    </div>
                                    <div class="clear pad_5">
                                    </div>
                                </div>
                            </asp:Panel>
                            <%-- Grid Alert popup Closed--%>
                            <asp:HiddenField ID="hfRouteAverage" runat="server" />
                            <asp:HiddenField ID="hfPageNo" runat="server" />
                            <asp:HiddenField ID="hfTotalRecords" runat="server" />
                            <asp:HiddenField ID="hfLastPage" runat="server" />
                            <asp:HiddenField ID="hfPageSize" runat="server" />
                            <asp:HiddenField ID="hfIsHugeReset" runat="server" Value="0" />
                            <asp:HiddenField ID="hfGlobalAccNoForIsTamper" runat="server" />
                            <asp:HiddenField ID="hfPopup1" runat="server" />
                            <%--<asp:HiddenField ID="hfTotalPages" runat="server" Value="0" />--%>
                            <asp:HiddenField ID="hfIsReset" runat="server" Value="0" />
                            <asp:HiddenField ID="hfIsResetPopup" runat="server" Value="0" />
                            <asp:HiddenField ID="hfRollover" runat="server" Value="false" />
                            <asp:HiddenField ID="hfRolloverPopup" runat="server" Value="false" />
                            <asp:HiddenField ID="hfIsExists" runat="server" Value="0" />
                            <asp:HiddenField ID="hdIsEditTamper" runat="server" Value="0" />
                            <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="PanelAlert"
                                TargetControlID="btnAlertDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnAlertOk">
                            </asp:ModalPopupExtender>
                            <asp:Button ID="btnAlertDeActivate" runat="server" Text="Button" CssClass="popbtn" />
                            <asp:Panel ID="PanelAlert" runat="server" CssClass="modalPopup" class="poppnl" Style="display: none;">
                                <div class="popheader popheaderlblred" id="pop" runat="server">
                                    <asp:Label ID="lblAlertMsg" runat="server"></asp:Label>
                                </div>
                                <div class="footer popfooterbtnrt">
                                    <div class="fltr popfooterbtn">
                                        <br />
                                        <asp:Button ID="btnAlertOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="cancel_btn" />
                                    </div>
                                    <div class="clear pad_5">
                                    </div>
                                </div>
                            </asp:Panel>
                            <%--IsTamper Reason Popup Start Here--%>
                            <asp:ModalPopupExtender ID="mpeIsTAmper" runat="server" TargetControlID="hfPopup1"
                                PopupControlID="pnlIsTamper" BackgroundCssClass="popbg">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="pnlIsTamper" runat="server" CssClass="successfully" Style="display: none">
                                <div class="text_total">
                                    <div class="text-heading">
                                        <asp:Literal ID="Literal26" runat="server" Text="Is Tampered"></asp:Literal>
                                    </div>
                                    <div class="star_text">
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box">
                                    <div class="inner-box1" style="width: 80%;">
                                        <div class="text-inner">
                                            <asp:Literal ID="Literal15" runat="server" Text="Global Account Number"></asp:Literal>
                                            :
                                            <asp:Label runat="server" ID="lblIsTamperGlobalAccNo" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1" style="width: 80%;">
                                        <div class="text-inner">
                                            <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:Resource, METER_NUMBER%>"></asp:Literal>
                                            :
                                            <asp:Label runat="server" ID="lblIsTamperMeterNo" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1" style="width: 80%;">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:Resource, REASON%>"></asp:Literal>
                                                <span class="span_star">*</span>
                                            </label>
                                            <div class="space">
                                            </div>
                                            <asp:TextBox ID="txtIsTamperReason" onblur="return TextBoxBlurValidationlbl(this,'spanIsTamperReason','Reason')"
                                                runat="server" CssClass="text-box" TextMode="MultiLine" placeholder="Enter Reason"
                                                MaxLength="200"></asp:TextBox>
                                            <span id="spanIsTamperReason" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="box_total">
                                        <div class="box_total_a">
                                            <asp:Button ID="btnSaveIsTamper" Text="<%$ Resources:Resource, SAVE%>" CssClass="box_s"
                                                runat="server" OnClientClick="return IsTamperReasonValidate();" OnClick="btnSaveIsTamper_Click" />
                                            <asp:Button ID="btnCancelIsTamper" Text="<%$ Resources:Resource, CANCEL%>" CssClass="box_s"
                                                runat="server" OnClick="btnCancelIsTamper_Click" OnClientClick="ClearTamperReading();" />
                                        </div>
                                    </div>
                                    <div style="clear: both;">
                                    </div>
                                </div>
                            </asp:Panel>
                            <%--IsTamper Reason Popup End Here--%>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <uc1:Authentication ID="ucAuthentication" runat="server" />
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
            <asp:PostBackTrigger ControlID="btnContinue" />
        </Triggers>
    </asp:UpdatePanel>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        //        function pageLoad() {
        //            DisplayMessage('<%= pnlMessage.ClientID %>');

        //            $get("<%=txtPassword.ClientID%>").focus();

        //        }
      
    </script>
    <%-- $get("<%=txtPassword.ClientID%>").focus();--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {

                $find('<%= ModalPopupExtenderInvalidAccount.ClientID %>').fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
        window.onload = function () {
            document.onkeydown = function (e) {
                if (e.which == 116 || e.keyCode == 116)
                    return false;
            };
        }
    </script>
    <%--<script type="text/javascript">
        function pageLoad() {
            DisplayMessage('<%= pnlMessage.ClientID %>');
            var table = '<%= gvMeterReadingList.ClientID %>';
            $("#<%= btnExportExcel.ClientID%>").click(function () {

                $("#<%= gvMeterReadingList.ClientID %>").btechco_excelexport({
                    containerid: table
               , datatype: $datatype.Table
                });
            });
        }
    </script>--%>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../scripts/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../Javascript/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/MeterReading.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            //            Calendar('<%=txtReadingDate.ClientID %>', '<%=imgDate.ClientID %>', "-2:+8");
            DatePicker($('#<%=txtReadingDate.ClientID %>'), $('#<%=imgDate.ClientID %>'));
            DisplayMessage('<%= pnlMessage.ClientID %>');
            //AutoComplete($('#<%=txtAccountNumber.ClientID %>'), 1);

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            //            Calendar('<%=txtReadingDate.ClientID %>', '<%=imgDate.ClientID %>', "-2:+8");
            DatePicker($('#<%=txtReadingDate.ClientID %>'), $('#<%=imgDate.ClientID %>'));
            DisplayMessage('<%= pnlMessage.ClientID %>');
            //AutoComplete($('#<%=txtAccountNumber.ClientID %>'), 1);
        });

    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
