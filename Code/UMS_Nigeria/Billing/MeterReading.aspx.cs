﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for MeterReading
                     
 Developer        : id075-RamaDevi M
 Creation Date    : 22-03-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBE;
using System.Xml;
using UMS_NigeriaBAL;
using Resources;
using UMS_NigriaDAL;
using System.Data;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Collections.Generic;
using System.Reflection;
using org.in2bits.MyXls;

namespace UMS_Nigeria.Billing
{
    public partial class MeterReading : System.Web.UI.Page
    {
        #region Members

        BillReadingsBAL _objBillingBAL = new BillReadingsBAL();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        ReportsBal objReportsBal = new ReportsBal();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        CommonDAL _ObjCommonDAL = new CommonDAL();
        IsTamperBal objIsTamperBal = new IsTamperBal();
        string Key = UMS_Resource.KEY_METERREADING;
        public int PageNum;

        #endregion

        #region Properties

        public int PageSize
        {
            get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.MeterReadingPageStart : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }

        #endregion

        #region Events

        #region OverallEvents

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }
        //Authentication Required.Dropdownlists for Route,Employee are binded.
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = pnlMessage.CssClass = string.Empty;
                if (!IsPostBack && Session[UMS_Resource.SESSION_LOGINID] != null)
                {
                    divRoutesddl.Visible = false;
                    divAccountNo.Visible = true;
                    BindMeterReadingTypes();

                    string BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                    _ObjCommonMethods.BindMarketers(ddlMeterReader, BU_ID, UMS_Resource.DROPDOWN_SELECT, true);

                    //rblAccountType_SelectedIndexChanged(this, null);
                    if (Request.QueryString[UMS_Resource.QSTR_ACC_NO] != null)//written if condition for getting customer no form query string by suresh
                    {
                        txtAccountNumber.Text = _ObjiDsignBAL.Decrypt(Request.QueryString[UMS_Resource.QSTR_ACC_NO].ToString());
                    }
                    else
                        txtAccountNumber.Focus();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindMeterReadingTypes()
        {
            DataSet ds = new DataSet();
            ds.ReadXml(Server.MapPath(ConfigurationSettings.AppSettings["MeterReadingTypeDetails"]));
            _ObjiDsignBAL.FillRadioButtonList(ds, rblAccountType, "name", "value");
            if (rblAccountType.Items.Count > 0)
            {
                rblAccountType.SelectedIndex = 0;
            }
        }

        //Check the user password correct or not.
        protected void btnAuthenticarion_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPassword.Text != "")
                {
                    XmlDocument xml = new XmlDocument();
                    LoginPageBE objLoginPageBE = new LoginPageBE();
                    LoginPageBAL objLoginPageBAL = new LoginPageBAL();
                    objLoginPageBE.UserName = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objLoginPageBE.Password = _ObjiDsignBAL.AESEncryptString(txtPassword.Text);
                    xml = objLoginPageBAL.Login(objLoginPageBE, ReturnType.Get);
                    objLoginPageBE = _ObjiDsignBAL.DeserializeFromXml<LoginPageBE>(xml);

                    if (objLoginPageBE.IsSuccess)
                    {
                        LabelStatusLogin.Visible = false;

                        if (objLoginPageBE.RoleId != 1)
                        {
                            ddlMeterReader.ClearSelection();
                            ddlMeterReader.Items.FindByText(objLoginPageBE.UserId).Selected = true;
                            ddlMeterReader.Enabled = false;
                        }
                        rblAccountType.Items.FindByValue("1").Selected = true;
                        rblAccountType_SelectedIndexChanged(this, null);
                        if (Request.QueryString[UMS_Resource.QSTR_ACC_NO] != null)//written if condition for getting customer no form query string by suresh
                        {
                            txtAccountNumber.Text = _ObjiDsignBAL.Decrypt(Request.QueryString[UMS_Resource.QSTR_ACC_NO].ToString());
                        }
                        else
                            txtAccountNumber.Focus();

                        ModalPopupExtenderLogin.Hide();
                    }
                    else
                    {
                        LabelStatusLogin.Visible = true;
                        LabelStatusLogin.Text = Resource.AUTHENTICATIONFAILED;
                        txtPassword.Focus();
                        ModalPopupExtenderLogin.Show();
                    }
                }
                else
                {
                    LabelStatusLogin.Visible = true;
                    LabelStatusLogin.Text = "Enter Password";
                    txtPassword.Focus();
                    ModalPopupExtenderLogin.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void rblAccountType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlMeterReader.Items.Clear();
                ddlRoutes.Visible = divRoutesddl.Visible = true;
                divroutecaption.Visible = divAccountNo.Visible = divBookNoDet.Visible = ddlRoutes.Enabled = false;
                txtReadingDate.Text = txtAccountNumber.Text = txtSearchAccount.Text = string.Empty;
                ddlSU.SelectedIndex = ddlSC.SelectedIndex = ddlCycle.SelectedIndex = 0;
                if (ddlRoutes.Items.Count > 0)
                    ddlRoutes.SelectedIndex = 0;
                if (ddlSU.Items.Count > 0)
                    ddlSU.SelectedIndex = 0;
                if (ddlSC.Items.Count > 0)
                    ddlSC.SelectedIndex = 0;
                if (ddlCycle.Items.Count > 0)
                    ddlCycle.SelectedIndex = 0;

                string BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();

                switch (rblAccountType.SelectedItem.Value)
                {
                    case "1":
                        ddlRoutes.Visible = divRoutesddl.Visible = false;
                        divAccountNo.Visible = true;
                        _ObjCommonMethods.BindMarketers(ddlMeterReader, BU_ID, UMS_Resource.DROPDOWN_SELECT, true);
                        break;
                    case "2":
                        ddlRoutes.Attributes.Add("onchange", "return Routes_IndexCHanged(this,'spanRoute','Book Number')");
                        divroutecaption.Visible = ddlRoutes.Enabled = true;
                        _ObjCommonMethods.BindRoutes(ddlRoutes, BU_ID, true);
                        break;
                    case "3":
                        ddlRoutes.Attributes.Add("onchange", "return Routes_IndexCHanged(this,'spanRoute','Book Number')");
                        BindBookNoWithDetails();
                        break;
                }

                divAccountWise.Visible = false;
                divRouteSequenceWise.Visible = false;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindBookNoWithDetails()
        {
            try
            {
                divBookNoDet.Visible = true;
                divRoutes.Attributes.Add("style", "margin-left:0px;");
                _ObjCommonMethods.BindUserBusinessUnits(ddlBU, string.Empty, false, UMS_Resource.DROPDOWN_SELECT);
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                {
                    ddlBU.SelectedIndex = ddlBU.Items.IndexOf(ddlBU.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                    ddlBU.Enabled = false;
                    ddlBU_SelectedIndexChanged(ddlBU, new EventArgs());
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        //GO click Event for get records based on read method
        protected void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                divTamperRemarks.Visible = divPopTamperRemarks.Visible = false;
                cbCustIsByPass.Checked = cbPopIsTamper.Checked = chkRollOver.Checked = chkPopRolleOver.Checked = false;
                //Commented By Karteek -- It is unnessesary code
                //BillingBE _objBillingBE = new BillingBE();
                //_objBillingBE.ReadDate = _ObjCommonMethods.Convert_MMDDYY(txtReadingDate.Text);
                //_objBillingBE = _ObjiDsignBAL.DeserializeFromXml<BillingBE>(_objBillingBAL.Get(_objBillingBE, ReturnType.Check));
                txtCurrentReading.Text = string.Empty;
                GetMeterReadingDetails(sender);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void GetMeterReadingDetails(object sender)
        {
            try
            {
                if (rblAccountType.SelectedItem.Value == "1")
                {
                    txtAccountNumber.Text = txtAccountNumber.Text.TrimStart();
                    txtAccountNumber.Text = txtAccountNumber.Text.Trim();

                    RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
                    objLedgerBe.AccountNo = txtAccountNumber.Text;//.Trim();--Faiz Id103
                    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                    else
                        objLedgerBe.BUID = string.Empty;
                    objLedgerBe = _ObjiDsignBAL.DeserializeFromXml<RptCustomerLedgerBe>(objReportsBal.GetLedger(objLedgerBe, ReturnType.Set));
                    if (objLedgerBe.IsSuccess)
                    {
                        if (objLedgerBe.IsCustExistsInOtherBU)
                        {
                            lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                            mpeAlert.Show();
                        }
                        else if (objLedgerBe.CustIsNotActive)
                        {
                            if (objLedgerBe.ActiveStatusId == 2)
                                lblAlertMsg.Text = "Customer status is Inactive.";
                            else if (objLedgerBe.ActiveStatusId == 3)
                                lblAlertMsg.Text = "Customer status is Hold.";
                            else if (objLedgerBe.ActiveStatusId == 4)
                                lblAlertMsg.Text = "Customer status is Closed.";
                            else
                                lblAlertMsg.Text = "Customer is not Active.";
                            mpeAlert.Show();
                        }
                        else if (objLedgerBe.CustomerTypeId == 3)//3 -- Prepaid Customer
                        {
                            lblAlertMsg.Text = "Customer is Prepaid customer.";
                            mpeAlert.Show();
                        }
                        else if (objLedgerBe.IsDisabledBook)
                        {
                            lblAlertMsg.Text = "Book disabled for this Customer.";
                            mpeAlert.Show();
                        }
                        else
                        {
                            GetAccountWiseDetails(objLedgerBe.AccountNo);
                        }
                    }
                    else
                    {
                        Message(UMS_Resource.CUSTOMER_NOT_FOUND_MSG, UMS_Resource.MESSAGETYPE_ERROR);
                        divAccountWise.Visible = false;//Faiz Id103
                    }
                }
                else
                {
                    EnableConfirmButtons(false);
                    BillingBE _objBilling = new BillingBE();
                    _objBilling.ReadDate = _ObjCommonMethods.Convert_MMDDYY(txtReadingDate.Text);
                    if (sender as Button != null)
                    {
                        hfPageNo.Value = Constants.pageNoValue.ToString();
                    }
                    divAccountWise.Visible = false;
                    divRouteSequenceWise.Visible = true;
                    switch (rblAccountType.SelectedItem.Value)
                    {
                        case "2":
                            _objBilling.ReadMethod = "route";
                            litCountriesList.Text = Resource.ROUTE_WISE;
                            break;
                        case "3":
                            _objBilling.ReadMethod = "BookNo";
                            litCountriesList.Text = Resource.BOOK_WISE;
                            break;
                    }
                    _objBilling.BookNo = ddlRoutes.SelectedItem.Value;
                    _objBilling.PageNo = Convert.ToInt32(hfPageNo.Value);
                    _objBilling.PageSize = PageSize;
                    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        _objBilling.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                    else
                        _objBilling.BUID = string.Empty;
                    _objBilling.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    GlobalMessagesListBE ObjBillList = _ObjiDsignBAL.DeserializeFromXml<UMS_NigeriaBE.GlobalMessagesListBE>(_objBillingBAL.GetPreviousCustomerReadingsBookwise(_ObjiDsignBAL.DoSerialize<BillingBE>(_objBilling)));

                    if (ObjBillList.CustomerReading.Count > 0)
                    {
                        hfTotalRecords.Value = ObjBillList.CustomerReading[0].TotalRecords.ToString();
                        divpaging.Visible = true; btnGvSave.Visible = true;

                        if (sender as Button != null)
                        {
                            BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        }
                        gvMeterReadingList.DataSource = _ObjiDsignBAL.ConvertListToDataSet<BillingBE>(ObjBillList.CustomerReading).Tables[0];
                        gvMeterReadingList.DataBind();
                        btnExportExcel.Visible = true;
                    }
                    else
                    {
                        hfTotalRecords.Value = Constants.Zero.ToString();
                        divpaging.Visible = false; btnGvSave.Visible = false;
                        gvMeterReadingList.DataSource = new DataTable();
                        gvMeterReadingList.DataBind();
                        btnExportExcel.Visible = false;
                    }
                    AppendNextCursor();
                    txtSearchAccount.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void AppendNextCursor()
        {
            try
            {
                for (int rowIndex = 0; rowIndex <= gvMeterReadingList.Rows.Count - 1; rowIndex++)
                {
                    GridViewRow row = gvMeterReadingList.Rows[rowIndex];
                    TextBox txtCurReading = (TextBox)row.FindControl("txtGvCurrentReading");
                    if (rowIndex < gvMeterReadingList.Rows.Count - 1)
                    {
                        GridViewRow nextRow = gvMeterReadingList.Rows[rowIndex + 1];
                        TextBox NexttxtCurReading = (TextBox)nextRow.FindControl("txtGvCurrentReading");
                        txtCurReading.Attributes.Add("onkeypress", "return controlEnter('" + NexttxtCurReading.ClientID + "', event)");
                    }
                    else
                    {
                        txtCurReading.Attributes.Add("onkeypress", "return controlEnter('" + btnGvSave.ClientID + "', event)");
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cbCustIsByPass_CheckedChanged(object sender, EventArgs e)//AccountWise 
        {
            try
            {
                if (rblAccountType.SelectedItem.Value == "1")
                {
                    if (cbCustIsByPass.Checked == true)
                    {
                        hdIsEditTamper.Value = "0";
                        lblIsTamperGlobalAccNo.Text = lblAccountNumber.Text;
                        lblIsTamperMeterNo.Text = lblMeterNumber.Text;
                        mpeIsTAmper.Show();
                    }
                    else
                    {
                        divTamperRemarks.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cbPopIsTamper_CheckedChanged(object sender, EventArgs e)//AccountWise Popup
        {
            try
            {
                if (rblAccountType.SelectedItem.Value == "1")
                {
                    if (cbPopIsTamper.Checked == true)
                    {
                        hdIsEditTamper.Value = "1";
                        lblIsTamperGlobalAccNo.Text = lblpopGoAccNum.Text;
                        lblIsTamperMeterNo.Text = lblpopGoMeterReadin.Text;
                        ModalPopupExtenderGo.Show();
                        mpeIsTAmper.Show();
                    }
                    else
                    {
                        divPopTamperRemarks.Visible = false;
                        ModalPopupExtenderGo.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cbIsTamper_CheckedChanged(object sender, EventArgs e)//Route/BookNo Wise Popup
        {
            try
            {
                CheckBox cbIsTamper = (CheckBox)sender;
                Label lblMeterNo = (Label)cbIsTamper.NamingContainer.FindControl("lblMeterNoOnly");
                Label lblAccountNo = (Label)cbIsTamper.NamingContainer.FindControl("lblAccountNo");

                if (cbIsTamper.Checked == true)
                {
                    hfGlobalAccNoForIsTamper.Value = lblAccountNo.Text;
                    lblIsTamperGlobalAccNo.Text = lblAccountNo.Text;
                    lblIsTamperMeterNo.Text = lblMeterNo.Text;
                    mpeIsTAmper.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSaveIsTamper_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtIsTamperReason.Text))
                {
                    divTamperRemarks.Visible = divPopTamperRemarks.Visible = false;

                    IsTamperBe objIsTamperBe = new IsTamperBe();
                    objIsTamperBe.GlobalAccountNumber = lblIsTamperGlobalAccNo.Text;
                    objIsTamperBe.MeterNo = lblIsTamperMeterNo.Text;
                    objIsTamperBe.Reason = _ObjiDsignBAL.ReplaceNewLines(txtIsTamperReason.Text, true);
                    objIsTamperBe.ReadingDate = _ObjCommonMethods.Convert_MMDDYY(txtReadingDate.Text);
                    objIsTamperBe.MeterReaderId = Convert.ToInt32(ddlMeterReader.SelectedValue);
                    objIsTamperBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objIsTamperBe = _ObjiDsignBAL.DeserializeFromXml<IsTamperBe>(objIsTamperBal.InsertTamper(objIsTamperBe, Statement.Insert));
                    if (objIsTamperBe.IsSuccess)
                    {
                        txtIsTamperReason.Text = string.Empty;

                        if (rblAccountType.SelectedItem.Value == "1")
                        {
                            if (hdIsEditTamper.Value == "1")
                            {
                                lblPopTamperRemarks.Text = objIsTamperBe.Reason;
                                divPopTamperRemarks.Visible = true;
                                ModalPopupExtenderGo.Show();
                            }
                            else
                            {
                                lblTamperRemarks.Text = objIsTamperBe.Reason;
                                divTamperRemarks.Visible = true;
                            }
                        }
                    }
                    else
                        Message("Details are failed to Save.", UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                {
                    mpeIsTAmper.Show();
                    txtIsTamperReason.Focus();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnCancelIsTamper_Click(object sender, EventArgs e)
        {
            try
            {
                if (rblAccountType.SelectedItem.Value == "1")
                {
                    if (cbCustIsByPass.Checked == true)
                    {
                        cbCustIsByPass.Checked = false;
                    }
                    if (cbPopIsTamper.Checked == true)
                    {
                        cbPopIsTamper.Checked = false;
                        ModalPopupExtenderGo.Show();
                    }
                }
                else
                {
                    if (hfGlobalAccNoForIsTamper.Value != "")
                    {
                        foreach (GridViewRow row in gvMeterReadingList.Rows)
                        {
                            if (row.RowType == DataControlRowType.DataRow)
                            {
                                CheckBox cbIsTamper = row.FindControl("cbIsTamper") as CheckBox;
                                Label lblAccountNo = row.FindControl("lblAccountNo") as Label;
                                if (lblAccountNo.Text == hfGlobalAccNoForIsTamper.Value)
                                {
                                    cbIsTamper.Checked = false;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void GetAccountWiseDetails(string AccountNo)
        {
            try
            {
                divAccountWise.Visible = true;
                divRouteSequenceWise.Visible = false;
                BillingBE _objBilling = new BillingBE();

                _objBilling.AccNum = AccountNo;
                XmlDocument xmlMeterResult = _objBillingBAL.GetMeterDecimalValues(_ObjiDsignBAL.DoSerialize<BillingBE>(_objBilling));
                _objBilling = _ObjiDsignBAL.DeserializeFromXml<BillingBE>(xmlMeterResult);

                if (IsReaded(_objBilling.ReadCodeId))
                {
                    divAccountWise.Visible = false;
                    lblAlertMsg.Text = "This is Direct Customer, Please Enter Read Customer Global Account No / Old Account No / Meter No.";//Resource.DIRECT_CUST_ACC_NO; 
                    mpeAlert.Show();
                }
                else
                {
                    hfDial.Value = _objBilling.MeterDials.ToString();
                    hfDecimal.Value = _objBilling.Decimals.ToString();

                    _objBilling.ReadDate = _ObjCommonMethods.Convert_MMDDYY(txtReadingDate.Text);
                    _objBilling.ReadMethod = "account";
                    _objBilling.AccNum = txtAccountNumber.Text;//.Trim();--Faiz Id103
                    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        _objBilling.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                    else
                        _objBilling.BUID = string.Empty;
                    _objBilling.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();

                    XmlDocument xmlResult = new XmlDocument();
                    xmlResult = _objBillingBAL.GetPreviousCustomerReadings(_ObjiDsignBAL.DoSerialize<BillingBE>(_objBilling));
                    GlobalMessagesListBE ObjBillList = _ObjiDsignBAL.DeserializeFromXml<UMS_NigeriaBE.GlobalMessagesListBE>(xmlResult);

                    if (ObjBillList.CustomerReading.Count > 0)
                    {
                        BillingBE _objBillingBe = ObjBillList.CustomerReading[0];

                        if (!_objBillingBe.IsPerformAction)
                        {
                            divAccountWise.Visible = false;
                            lblgridalertmsg.Text = "Already request is in process for this Customer, " + _objBillingBe.Description;
                            mpegridalert.Show();
                        }
                        else if (_objBillingBe.IsApprovalProces)
                        {
                            ShowExistMessage(_objBillingBe, 1);
                        }
                        else if (_objBillingBe.IsMeterChangeApproval)
                        {
                            divAccountWise.Visible = false;
                            lblgridalertmsg.Text = "Meter change request is in process for this Customer.";
                            mpegridalert.Show();
                        }
                        else if (_objBillingBe.IsReadToDirectApproval)
                        {
                            divAccountWise.Visible = false;
                            lblgridalertmsg.Text = "Meter Disconnect change request is in process for this Customer.";
                            mpegridalert.Show();
                        }
                        else if (_objBillingBe.IsActive && _objBillingBe.IsPrvExists)
                        {
                            ShowExistMessage(_objBillingBe, 2);
                        }
                        else if (_objBillingBe.IsBilled == 1 && _objBillingBe.IsLatestBill)
                        {
                            ShowExistMessage(_objBillingBe, 3);
                        }
                        else if (_objBillingBe.IsMeterChangedDateExists)
                        {
                            divAccountWise.Visible = false;
                            lblgridalertmsg.Text = "Meter Reading Date should be greater than the Meter Changed Date.";
                            mpegridalert.Show();
                        }
                        else if (_objBillingBe.IsMeterAssignedDateExists)
                        {
                            divAccountWise.Visible = false;
                            lblgridalertmsg.Text = "Meter Reading Date should be greater than the Meter Assigned Date.";
                            mpegridalert.Show();
                        }
                        else if (_objBillingBe.IsExists && _objBillingBe.IsPrvExists)
                        {
                            btnGoOK.Text = Resource.CANCEL;
                            divAccountWise.Visible = false; lblpopGoCurrent.Visible = true; txtpopGoCurrent.Visible = false;
                            btnEdit.Visible = true;
                            btnUpdate.Visible = cbPopIsTamper.Enabled = chkPopRolleOver.Enabled = false;
                            cbPopIsTamper.Checked = _objBillingBe.IsTamper;
                            chkPopRolleOver.Checked = _objBillingBe.Rollover;
                            hfIsResetPopup.Value = (_objBillingBe.Rollover) ? "1" : "0";
                            lblpopGoAccNum.Text = _objBillingBe.AccNum;
                            lblpopGlobalAccountNum.Text = _objBillingBe.AccountNo + "-" + _objBillingBe.AccNum;
                            lblpopOldAccNum.Text = _objBillingBe.OldAccountNo;
                            lblpopGoCustomerDetails.Text = _objBillingBe.Name;
                            lblpopGoPreviousReading.Text = _objBillingBe.PreviousReading;
                            lblpopGoPreviousReading.Text = Math.Round(Convert.ToDecimal(lblpopGoPreviousReading.Text), Convert.ToInt32(hfDecimal.Value)).ToString();
                            lblpopGoPreviousReading.Text = AppendZeros(lblpopGoPreviousReading.Text, Convert.ToInt32(hfDial.Value));

                            lblpopRoute.Text = _objBillingBe.RouteNum;
                            lblpopAvg.Text = Math.Round(string.IsNullOrEmpty(_objBillingBe.AverageReading) ? 0 : Convert.ToDecimal(_objBillingBe.AverageReading), 2).ToString();
                            lblpopGoMeterReadin.Text = _objBillingBe.MeterNo;
                            //lblpopBillExist.Text = _objBillingBe.IsBilled.ToString();
                            if (!string.IsNullOrEmpty(_objBillingBe.PresentReading))
                            {
                                lblpopGoCurrent.Text = _objBillingBe.PresentReading;
                                lblpopGoCurrent.Text = Math.Round(Convert.ToDecimal(_objBillingBe.PresentReading), Convert.ToInt32(hfDecimal.Value)).ToString();
                                lblpopGoCurrent.Text = AppendZeros(lblpopGoCurrent.Text, Convert.ToInt32(hfDial.Value));
                                txtpopGoCurrent.Text = lblPopGoCurrentReading.Text = lblpopGoCurrent.Text;
                            }
                            lblpopGoCount.Text = _objBillingBe.TotalReadings;
                            lblpopInvalidUsageError.Visible = false;
                            lblpopGoUsage.Text = Math.Round(Convert.ToDecimal(_objBillingBe.Usage), Convert.ToInt32(hfDecimal.Value)).ToString();
                            hfIsExists.Value = _objBillingBe.IsPrvExists.ToString();

                            lnlInvalidLatestHeader.Text = Resource.LATEST_READDATE;
                            lnlInvalidLatestBody.Text = Resource.METERREADING_INVALIDLATESTBODY;
                            lblLatestPopup.Text = _objBillingBe.ReadDate;
                            lblLatestPopupNew.Text = _objBillingBe.LatestDate;
                            mpeInvalidLatest.Show();
                        }
                        else
                        {
                            hfIsResetPopup.Value = (_objBillingBe.Rollover) ? "1" : "0";
                            lblAccountNumber.Text = _objBillingBe.AccNum;
                            lblGlobalAccountNumber.Text = _objBillingBe.AccountNo + "-<br />" + _objBillingBe.AccNum;
                            lblCustomerDetails.Text = _objBillingBe.Name;
                            lblOldAccountNo.Text = _objBillingBe.OldAccountNo;
                            if (string.IsNullOrEmpty(_objBillingBe.InititalReading) && string.IsNullOrEmpty(_objBillingBe.PresentReading))
                            {
                                divAccountWise.Visible = false;
                                Message("Error in data. Initital Reading not available.", UMS_Resource.MESSAGETYPE_ERROR);
                                return;
                            }
                            if (string.IsNullOrEmpty(_objBillingBe.PresentReading))
                                lblPreviousReading.Text = _objBillingBe.InititalReading;//PreviousReading;
                            else
                                lblPreviousReading.Text = _objBillingBe.PresentReading;
                            if (!string.IsNullOrEmpty(lblPreviousReading.Text))
                            {
                                lblPreviousReading.Text = AppendZeros(Math.Round(Convert.ToDecimal(lblPreviousReading.Text), Convert.ToInt32(hfDecimal.Value)).ToString(), Convert.ToInt32(hfDial.Value));
                            }
                            lblMeterNumber.Text = _objBillingBe.MeterNo;
                            lblRoute.Text = Convert.ToString(_objBillingBe.RouteNum);
                            lblAvgAcc.Text = _objBillingBe.AverageReading;
                            lblAvgAcc.Text = Math.Round(string.IsNullOrEmpty(lblAvgAcc.Text) ? 0 : Convert.ToDecimal(lblAvgAcc.Text), 2).ToString();
                            lblCountAcc.Text = _objBillingBe.TotalReadings;
                            hfIsExists.Value = _objBillingBe.IsPrvExists.ToString();

                            lblCurrentReading.Visible = false; txtCurrentReading.Visible = true;

                            divAccountWise.Visible = true;
                            txtCurrentReading.Enabled = true;
                            txtCurrentReading.Focus();

                            lblUsageDetails.Text = "0";
                            txtCurrentReading.Attributes.Add("onkeypress", "return AllowNumbers('" + hfDial.ClientID + "','" + hfDecimal.ClientID + "',this,event)");
                            txtCurrentReading.Attributes.Add("onkeyup", "return CalculateNewUsage('" + lblPreviousReading.ClientID + "','" + lblUsageDetails.ClientID + "',this,'" + hfDial.ClientID + "')");
                        }
                    }
                    else
                    {
                        lblAccountNumber.Text = lblCustomerDetails.Text = lblOldAccountNo.Text = lblPreviousReading.Text = lblMeterNumber.Text = "--";
                        divAccountWise.Visible = false;
                        lblHeaderInvalid.Text = Resource.INVALIDACCOUNTNUMBER_HEADER;
                        lblInvalidBody.Text = Resource.INVALIDACCOUNTNUMBER_BODY;
                        ModalPopupExtenderInvalidAccount.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #region Previous Code

        //else
        //                {
        //                    cbPopIsTamper.Checked = _objBillingBe.IsTamper;
        //                    chkPopRolleOver.Checked = _objBillingBe.Rollover;
        //                    hfIsResetPopup.Value = (_objBillingBe.Rollover) ? "1" : "0";
        //                    cbPopIsTamper.Enabled = chkPopRolleOver.Enabled = false;
        //                    lblAccountNumber.Text = _objBillingBe.AccNum;
        //                    lblGlobalAccountNumber.Text = _objBillingBe.AccountNo + "-<br />" + _objBillingBe.AccNum;
        //                    lblCustomerDetails.Text = _objBillingBe.Name;
        //                    lblOldAccountNo.Text = _objBillingBe.OldAccountNo;
        //                    if (string.IsNullOrEmpty(_objBillingBe.InititalReading) && string.IsNullOrEmpty(_objBillingBe.PresentReading))
        //                    {
        //                        divAccountWise.Visible = false;
        //                        Message("Error in data. Initital Reading not available.", UMS_Resource.MESSAGETYPE_ERROR);
        //                        return;
        //                    }
        //                    if (string.IsNullOrEmpty(_objBillingBe.PresentReading)) lblPreviousReading.Text = _objBillingBe.InititalReading;//PreviousReading;
        //                    else lblPreviousReading.Text = _objBillingBe.PresentReading;
        //                    if (!string.IsNullOrEmpty(lblPreviousReading.Text))
        //                    {
        //                        lblPreviousReading.Text = AppendZeros(Math.Round(Convert.ToDecimal(lblPreviousReading.Text), Convert.ToInt32(hfDecimal.Value)).ToString(), Convert.ToInt32(hfDial.Value));
        //                    }
        //                    lblMeterNumber.Text = _objBillingBe.MeterNo;
        //                    lblRoute.Text = Convert.ToString(_objBillingBe.RouteNum);
        //                    lblAvgAcc.Text = _objBillingBe.AverageReading;
        //                    lblAvgAcc.Text = Math.Round(string.IsNullOrEmpty(lblAvgAcc.Text) ? 0 : Convert.ToDecimal(lblAvgAcc.Text), 2).ToString();
        //                    lblCountAcc.Text = _objBillingBe.TotalReadings;
        //                    hfIsExists.Value = _objBillingBe.IsExists.ToString();

        //                    //if (!_objBillingBe.IsActiveMonth)
        //                    //{
        //                    if (_objBillingBe.IsExists)
        //                    {
        //                        btnGoOK.Text = Resource.CANCEL;
        //                        divAccountWise.Visible = false; lblpopGoCurrent.Visible = true; txtpopGoCurrent.Visible = false;
        //                        btnEdit.Visible = true;
        //                        btnUpdate.Visible = cbPopIsTamper.Enabled = chkPopRolleOver.Enabled = false;
        //                        cbPopIsTamper.Checked = _objBillingBe.IsTamper;
        //                        chkPopRolleOver.Checked = _objBillingBe.Rollover;
        //                        hfIsResetPopup.Value = (_objBillingBe.Rollover) ? "1" : "0";
        //                        lblpopGoAccNum.Text = _objBillingBe.AccNum;
        //                        lblpopGlobalAccountNum.Text = _objBillingBe.AccountNo + "-" + _objBillingBe.AccNum;
        //                        lblpopOldAccNum.Text = _objBillingBe.OldAccountNo;
        //                        lblpopGoCustomerDetails.Text = _objBillingBe.Name;
        //                        lblpopGoPreviousReading.Text = _objBillingBe.PreviousReading;
        //                        lblpopGoPreviousReading.Text = Math.Round(Convert.ToDecimal(lblpopGoPreviousReading.Text), Convert.ToInt32(hfDecimal.Value)).ToString();
        //                        lblpopGoPreviousReading.Text = AppendZeros(lblpopGoPreviousReading.Text, Convert.ToInt32(hfDial.Value));

        //                        lblpopRoute.Text = _objBillingBe.RouteNum;
        //                        lblpopAvg.Text = _objBillingBe.AverageReading;
        //                        lblpopAvg.Text = Math.Round(string.IsNullOrEmpty(lblpopAvg.Text) ? 0 : Convert.ToDecimal(lblpopAvg.Text), 2).ToString();
        //                        lblpopGoMeterReadin.Text = _objBillingBe.MeterNo;
        //                        lblpopBillExist.Text = _objBillingBe.IsBilled.ToString();
        //                        if (!string.IsNullOrEmpty(_objBillingBe.PresentReading))
        //                        {
        //                            lblpopGoCurrent.Text = _objBillingBe.PresentReading;
        //                            lblpopGoCurrent.Text = Math.Round(Convert.ToDecimal(lblpopGoCurrent.Text), Convert.ToInt32(hfDecimal.Value)).ToString();
        //                            lblpopGoCurrent.Text = AppendZeros(lblpopGoCurrent.Text, Convert.ToInt32(hfDial.Value));
        //                            txtpopGoCurrent.Text = lblPopGoCurrentReading.Text = lblpopGoCurrent.Text;
        //                        }
        //                        lblpopGoCount.Text = _objBillingBe.TotalReadings;
        //                        lblpopInvalidUsageError.Visible = false;
        //                        lblpopGoUsage.Text = Math.Round(Convert.ToDecimal(_objBillingBe.Usage), Convert.ToInt32(hfDecimal.Value)).ToString();
        //                        hfIsExists.Value = _objBillingBe.IsExists.ToString();

        //                        if (_objBillingBe.IsExists)
        //                        {
        //                            lnlInvalidLatestHeader.Text = Resource.LATEST_READDATE;
        //                            lnlInvalidLatestBody.Text = Resource.METERREADING_INVALIDLATESTBODY;
        //                            lblLatestPopup.Text = _objBillingBe.LatestDate;
        //                            mpeInvalidLatest.Show();
        //                        }
        //                        else
        //                        {
        //                            if (Convert.ToBoolean(_objBillingBe.IsBilled))
        //                            {
        //                                if (_objBillingBe.IsActiveMonth)
        //                                    btnEdit.Visible = false;
        //                                else
        //                                    btnEdit.Visible = true;
        //                            }
        //                            else
        //                                btnEdit.Visible = true;
        //                            ModalPopupExtenderGo.Show();
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (!string.IsNullOrEmpty(_objBillingBe.EstimatedBillDate))
        //                        {
        //                            lblEstimatedBillDetails.Text = string.Format(Resource.ESTIMATED_DATE_MESSAGE, _objBillingBe.EstimatedBillDate);
        //                        }
        //                        else { lblEstimatedBillDetails.Text = string.Empty; }
        //                        lblBillExist.Text = _objBillingBe.IsBilled.ToString();
        //                        lblCurrentReading.Visible = false; txtCurrentReading.Visible = true;

        //                        if (_objBillingBe.IsExists)
        //                        {
        //                            txtCurrentReading.Enabled = false;
        //                            divAccountWise.Visible = false;
        //                            btnEdit.Visible = false;
        //                            lnlInvalidLatestHeader.Text = Resource.LATEST_READDATE;
        //                            lnlInvalidLatestBody.Text = Resource.METERREADING_INVALIDLATESTBODY;
        //                            lblLatestPopup.Text = _objBillingBe.LatestDate;
        //                            mpeInvalidLatest.Show();
        //                        }
        //                        else
        //                        {
        //                            divAccountWise.Visible = true;
        //                            txtCurrentReading.Enabled = true;
        //                            txtCurrentReading.Focus();
        //                        }

        //                        lblUsageDetails.Text = "0";
        //                        txtCurrentReading.Attributes.Add("onkeypress", "return AllowNumbers('" + hfDial.ClientID + "','" + hfDecimal.ClientID + "',this,event)");
        //                        txtCurrentReading.Attributes.Add("onkeyup", "return CalculateNewUsage('" + lblPreviousReading.ClientID + "','" + lblUsageDetails.ClientID + "',this,'" + hfDial.ClientID + "')");
        //                    }
        //                    //}
        //                    //else
        //                    //{
        //                    //    divAccountWise.Visible = false;
        //                    //    lblEstimatedBillMessage.Text = Resource.BILL_MONTH_LOCKED;
        //                    //    MpestimatedBill.Show();
        //                    //}
        //                }

        #endregion

        private void ShowExistMessage(BillingBE _objBillingBe, int status)
        {
            divAccountWise.Visible = false;
            if (status == 1)
                lblShowExistMesage.Text = "Already request is in process for this Customer, Please find the Last Readings of this Customer below.";
            else if (status == 2)
                lblShowExistMesage.Text = "Already Readings available for this Customer, Please find the Last Readings of this Customer below.";
            else if (status == 3)
                lblShowExistMesage.Text = "Bill is already generated for this customer on " + _objBillingBe.EstimatedBillDate + ", Please find the Last Readings of this Customer below.";

            lblShowReadDate.Text = _objBillingBe.LatestDate;
            lblShowPreviousReading.Text = AppendZeros(Math.Round(Convert.ToDecimal(_objBillingBe.PreviousReading), Convert.ToInt32(hfDecimal.Value)).ToString(), Convert.ToInt32(hfDial.Value));
            lblShowPresentReading.Text = AppendZeros(Math.Round(Convert.ToDecimal(_objBillingBe.PresentReading), Convert.ToInt32(hfDecimal.Value)).ToString(), Convert.ToInt32(hfDial.Value));
            lblShowUsage.Text = Math.Round(Convert.ToDecimal(_objBillingBe.Usage), Convert.ToInt32(hfDecimal.Value)).ToString();
            lblShowAverage.Text = _objBillingBe.AverageReading;
            MpeShowExistMesage.Show();
        }

        #endregion

        #region AccoutWise

        public string Convert_DDMMYY(string Date)
        {
            string[] Dob = Date.Split('-');
            Date = Dob[1] + '/' + Dob[0] + '/' + Dob[2];
            return Date;
        }
        //If The ReadDate is Lessthan the latestdate.After click on proceed button
        protected void btnProceed_Click(object sender, EventArgs e)
        {
            try
            {
                ModalPopupExtenderGo.Show();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        //(Insert Record)If the Readmethod is Accountwise.And Record not exist in Read date.
        protected void btnSaveNext_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkRollOver.Checked)
                {
                    hfRollover.Value = "True";
                    hfRolloverPopup.Value = "False";
                }
                else
                {
                    hfRollover.Value = hfRolloverPopup.Value = "False";
                }
                if (lblCurrentReading.Visible == false && txtCurrentReading.Visible == true && txtCurrentReading.Text != "" && txtCurrentReading.Enabled == true)
                {
                    if (hfIsReset.Value == "1")
                    {
                        string StrValue = GetAllNines(Convert.ToInt32(hfDial.Value));
                        lblUsageDetails.Text = ((Convert.ToDecimal(StrValue) - Convert.ToDecimal(lblPreviousReading.Text)) + Convert.ToDecimal(txtCurrentReading.Text) + 1).ToString();
                    }
                    else
                        lblUsageDetails.Text = (Convert.ToDecimal(txtCurrentReading.Text == "" ? "0" : txtCurrentReading.Text) - Convert.ToDecimal(lblPreviousReading.Text)).ToString();

                    if (hfIsReset.Value == "0")
                    {
                        //Check The Average Greater than the Xmlvalue
                        if (Convert.ToDecimal(lblAvgAcc.Text) > 0)
                        {
                            if (_ObjCommonMethods.IsHugeAmount(Convert.ToDecimal(lblUsageDetails.Text), Convert.ToDecimal(lblAvgAcc.Text)))
                            {
                                lblInvalidUsageHeader.Text = UMS_Resource.INVALIDUSAGEHEADER_METERREADING;
                                lblInvalidUsageBody.Text = UMS_Resource.INVALIDUSAGEBODY_METERREADING_NEW;
                                ModalPopupExtenderInvalidUsage.Show();
                            }
                            else
                            {
                                FromSaveNext();
                                ModalPopupExtenderGo.Hide();
                                txtCurrentReading.Text = string.Empty;
                                divAccountWise.Visible = false;
                            }
                        }
                        else
                        {
                            FromSaveNext();
                            ModalPopupExtenderGo.Hide();
                            txtCurrentReading.Text = string.Empty;
                            divAccountWise.Visible = false;
                        }
                    }
                    else
                    {
                        Session[UMS_Resource.SESSION_NEGUSAGE] = lblPreviousReading.Text + "," + txtCurrentReading.Text + "," + lblUsageDetails.Text + "," + lblAccountNumber.Text + "," + "0" + "," + "0" + "," + "1";
                        ModalPopupExtenderNegativeUsage.Show();
                    }
                }
                else if (lblCurrentReading.Visible == false && txtCurrentReading.Visible == true && txtCurrentReading.Text == "")
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "Script1", "TextBoxBlurValidationlbl(this,'txtCurrentReading','Current Reading');", false);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        //If Usage is negative but meter rest Condition
        protected void btnNegUsageContinue_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToBoolean(hfRollover.Value))
                {
                    FromSaveNext();
                    ModalPopupExtenderGo.Hide();
                    divAccountWise.Visible = false;
                    txtAccountNumber.Text = txtReadingDate.Text = string.Empty;
                    ddlMeterReader.SelectedIndex = 0;
                }
                else if (Convert.ToBoolean(hfRolloverPopup.Value))
                {
                    UpdateMeterReadingUseCalculation();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        //Edit Click in the Accountwise popup
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                btnGoOK.Text = Resource.CANCEL;
                btnEdit.Visible = false; btnUpdate.Visible = true;
                txtpopGoCurrent.Text = lblpopGoCurrent.Text;
                cbPopIsTamper.Enabled = chkPopRolleOver.Enabled = true;
                lblpopGoCurrent.Visible = false; txtpopGoCurrent.Visible = true;
                txtpopGoCurrent.Focus();
                txtpopGoCurrent.Attributes.Add("onkeypress", "return AllowNumbers('" + hfDial.ClientID + "','" + hfDecimal.ClientID + "',this,event)");
                txtpopGoCurrent.Attributes.Add("onkeyup", "return CalculateNewUsagePop('" + lblpopGoPreviousReading.ClientID + "','" + lblpopGoUsage.ClientID + "',this,'" + hfDial.ClientID + "')");
                ModalPopupExtenderGo.Show();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        //(Insert Record) Confirm to Enter Huge Usage in Accountwise.
        protected void btnContinue_Click(object sender, EventArgs e)
        {
            try
            {
                FromSaveNext();
                divAccountWise.Visible = false;
                txtAccountNumber.Text = txtReadingDate.Text = string.Empty;
                ModalPopupExtenderGo.Hide();
                ModalPopupExtenderInvalidUsage.Hide();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        //Update Click in the Accountwise popup
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkPopRolleOver.Checked)
                {
                    hfRolloverPopup.Value = "True";
                    hfRollover.Value = "False";
                }
                else
                {
                    hfRolloverPopup.Value = hfRollover.Value = "False";
                }

                if (chkPopRolleOver.Checked)
                {
                    ModalPopupExtenderNegativeUsage.Show();
                }
                else
                {
                    UpdateMeterReadingUseCalculation();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void UpdateMeterReadingUseCalculation()
        {
            string PopCurrent = txtpopGoCurrent.Text;

            if ((lblpopGoPreviousReading.Text.IndexOf('9', 0) == 0 && Convert.ToDecimal(lblpopGoPreviousReading.Text) > Convert.ToDecimal(txtpopGoCurrent.Text)) || hfIsResetPopup.Value == "1")// != -1)
            {
                string StrValue = GetAllNines(Convert.ToInt32(hfDial.Value));
                lblpopGoUsage.Text = ((Convert.ToDecimal(StrValue) - Convert.ToDecimal(lblpopGoPreviousReading.Text)) + Convert.ToDecimal(PopCurrent) + 1).ToString();
            }
            else
            {
                lblpopGoUsage.Text = (Convert.ToDecimal(PopCurrent == "" ? "0" : PopCurrent) - Convert.ToDecimal(lblpopGoPreviousReading.Text)).ToString();
            }
            string PopUsage = lblpopGoUsage.Text;
            if (lblpopGoPreviousReading.Text.IndexOf('9', 0) != 0 || (Convert.ToDecimal(lblpopGoPreviousReading.Text) < Convert.ToDecimal(txtpopGoCurrent.Text)))// == -1)
            {
                if ((_ObjCommonMethods.IsHugeAmount(Convert.ToDecimal(PopUsage), Convert.ToDecimal(lblpopAvg.Text))))
                {
                    lblinvalidUsagepopupHeader.Visible = true;
                    lblinvalidUsagepopupHeader.Text = UMS_Resource.INVALIDUSAGEHEADER_METERREADING;
                    lblinvalidUsagepopupBody.Text = UMS_Resource.INVALIDUSAGEBODY_METERREADING;
                    ModalPopupExtenderPopupInvalidUsage.Show();
                }
                else
                {
                    lblpopInvalidUsageError.Visible = false;
                    NegUsageValid(lblpopGoPreviousReading.Text, PopCurrent, PopUsage, lblpopGoAccNum.Text, lblpopAvg.Text, lblpopGoCount.Text, "2", cbPopIsTamper.Checked.ToString(), (int)MeterReadingFrom.AccountWise);
                }
            }
            else
            {
                Session[UMS_Resource.SESSION_NEGUSAGE] = lblpopGoPreviousReading.Text + "," + PopCurrent + "," + PopUsage + "," + lblpopGoAccNum.Text + "," + lblpopAvg.Text + "," + lblpopGoCount.Text + "," + "2";

                if (_ObjCommonMethods.IsHugeAmount(Convert.ToDecimal(PopUsage), Convert.ToDecimal(lblpopAvg.Text)))
                {
                    hfIsHugeReset.Value = "1";
                    lblinvalidUsagepopupHeader.Visible = true;
                    lblinvalidUsagepopupHeader.Text = UMS_Resource.INVALIDUSAGEHEADER_METERREADING;
                    lblinvalidUsagepopupBody.Text = UMS_Resource.INVALIDUSAGEBODY_METERREADING;
                    ModalPopupExtenderPopupInvalidUsage.Show();
                }
                else
                {
                    lblpopInvalidUsageError.Visible = false;
                    NegUsageValid(lblpopGoPreviousReading.Text, PopCurrent, PopUsage, lblpopGoAccNum.Text, lblpopAvg.Text, lblpopGoCount.Text, "2", cbPopIsTamper.Checked.ToString(), (int)MeterReadingFrom.AccountWise);
                }
            }
        }

        private decimal AboveHugeAmount(decimal averageValue)
        {
            return averageValue + (averageValue * Constants.Meter_Reading_HugePercentage / 100);
        }
        //Update Huge Usage in the Accountwise Of popup
        protected void btnContinuePopup_Click(object sender, EventArgs e)
        {
            try
            {
                //if (hfIsHugeReset.Value == "1")
                //{
                //    ModalPopupExtenderNegativeUsage.Show();
                //}
                //else
                //PopUpUpdate();
                lblpopInvalidUsageError.Visible = false;
                NegUsageValid(lblpopGoPreviousReading.Text, txtpopGoCurrent.Text, lblpopGoUsage.Text, lblpopGoAccNum.Text, lblpopAvg.Text, lblpopGoCount.Text, "2", cbPopIsTamper.Checked.ToString(), (int)MeterReadingFrom.AccountWise);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        //Search Customer based on AccNum
        protected void lkbSearchAcc_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADVANCE_SEARCH_PAGE + "?" + UMS_Resource.FROM_PAGE + "=" + _ObjiDsignBAL.Encrypt(Constants.MeterReadingPageName)
                                                                   + "&" + UMS_Resource.QSTR_ACC_NO + "=" + _ObjiDsignBAL.Encrypt(txtAccountNumber.Text), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region RouteWise

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                btnGo_Click(this, null);
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                btnGo_Click(this, null);
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                btnGo_Click(this, null);
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                btnGo_Click(this, null);
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                btnGo_Click(this, null);
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvMeterReadingList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //Label lblExist = (Label)e.Row.FindControl("lblGvExist");
                    Label lblCurReading = (Label)e.Row.FindControl("lblGvCurrentReading");
                    Label lblUSG = (Label)e.Row.FindControl("lblGvUsage");
                    Label lblPre = (Label)e.Row.FindControl("lblGvPreviousReading");
                    Label lblDials = (Label)e.Row.FindControl("lblItemDials");
                    Label lblDeciumals = (Label)e.Row.FindControl("lblItemDecimals");
                    Label lblIsTamper = (Label)e.Row.FindControl("lblIsTamper");
                    Label lblGvIsBilled = (Label)e.Row.FindControl("lblGvIsBilled");
                    Label lblCustomerTypeId = (Label)e.Row.FindControl("lblCustomerTypeId");
                    
                    TextBox txtCurReading = (TextBox)e.Row.FindControl("txtGvCurrentReading");
                    CheckBox cbIsTamper = (CheckBox)e.Row.FindControl("cbIsTamper");
                    //LinkButton lkEdit = (LinkButton)e.Row.FindControl("lkEditReading");
                    Label lblRemarks = (Label)e.Row.FindControl("lblRemarks");
                    HiddenField hfIsHaveLatestReading = (HiddenField)e.Row.FindControl("hfIsHaveLatestReading");
                    //HiddenField hfIsLocked = (HiddenField)e.Row.FindControl("hfIsLocked");
                    HiddenField hfApproveStatusId = (HiddenField)e.Row.FindControl("hfApproveStatusId");
                    HiddenField hfActiveStatus = (HiddenField)e.Row.FindControl("hfActiveStatus");
                    HiddenField hfIsRollOver = (HiddenField)e.Row.FindControl("hfIsRollOver");
                    HiddenField hfIsMeterApproval = (HiddenField)e.Row.FindControl("hfIsMeterApproval");
                    HiddenField hfIsPerformAction = (HiddenField)e.Row.FindControl("hfIsPerformAction");
                    HiddenField hfDescription = (HiddenField)e.Row.FindControl("hfDescription");
                    HiddenField hfIsReadToDirectApproval = (HiddenField)e.Row.FindControl("hfIsReadToDirectApproval");
                    HiddenField hfIsMeterAssignedDateExists = (HiddenField)e.Row.FindControl("hfIsMeterAssignedDateExists");
                    HiddenField hfIsMeterChangedDateExists = (HiddenField)e.Row.FindControl("hfIsMeterChangedDateExists");
                    HiddenField hfIsFutureMonthBilled = (HiddenField)e.Row.FindControl("hfIsFutureMonthBilled");

                    hfActiveStatus.Value = false.ToString();

                    txtCurReading.MaxLength = Convert.ToInt32(lblDials.Text);

                    lblUSG.Text = Math.Round(Convert.ToDecimal(lblUSG.Text), Convert.ToInt32(lblDeciumals.Text)).ToString();

                    if (lblIsTamper.Text == true.ToString())
                    {
                        cbIsTamper.Checked = true;
                    }

                    if (lblPre.Text.Length < Convert.ToInt32(lblDials.Text))
                    {
                        lblPre.Text = AppendZeros(lblPre.Text, Convert.ToInt32(lblDials.Text));
                    }

                    if (!string.IsNullOrEmpty(lblCurReading.Text))
                        txtCurReading.Text = AppendZeros(lblCurReading.Text, Convert.ToInt32(lblDials.Text));

                    if (hfIsPerformAction.Value.ToLower() == false.ToString().ToLower())
                    {
                        txtCurReading.CssClass = "gvtext";
                        cbIsTamper.Enabled = txtCurReading.Enabled = false;
                        lblRemarks.Text = hfDescription.Value;
                    }
                    else if (lblCustomerTypeId.Text == "3")
                    {
                        txtCurReading.CssClass = "gvtext";
                        cbIsTamper.Enabled = txtCurReading.Enabled = false;
                        lblRemarks.Text = "Prepaid customer.";
                    }
                    else if (hfApproveStatusId.Value == "2")
                    {
                        txtCurReading.CssClass = "gvtext";
                        cbIsTamper.Enabled = txtCurReading.Enabled = false;
                        lblRemarks.Text = "Approval is in process";
                    }
                    else if (hfIsMeterApproval.Value.ToLower() == true.ToString().ToLower())
                    {
                        txtCurReading.CssClass = "gvtext";
                        cbIsTamper.Enabled = txtCurReading.Enabled = false;
                        lblRemarks.Text = "Meter change request is in process";
                    }
                    else if (hfIsReadToDirectApproval.Value.ToLower() == true.ToString().ToLower())
                    {
                        txtCurReading.CssClass = "gvtext";
                        cbIsTamper.Enabled = txtCurReading.Enabled = false;
                        lblRemarks.Text = "Meter Disconnect change request is in process";
                    }
                    else if (hfApproveStatusId.Value == "3" && hfIsHaveLatestReading.Value.ToLower() == true.ToString().ToLower())
                    {
                        txtCurReading.CssClass = "gvtext";
                        cbIsTamper.Enabled = txtCurReading.Enabled = false;
                        lblRemarks.Text = "Readings already available";
                    }
                    else if (hfApproveStatusId.Value == "1" && hfIsHaveLatestReading.Value.ToLower() == true.ToString().ToLower())
                    {
                        txtCurReading.CssClass = "gvtext";
                        cbIsTamper.Enabled = txtCurReading.Enabled = false;
                        lblRemarks.Text = "Readings available for Future Date";
                    }
                    else if (hfIsMeterChangedDateExists.Value.ToLower() == true.ToString().ToLower())
                    {
                        txtCurReading.CssClass = "gvtext";
                        cbIsTamper.Enabled = txtCurReading.Enabled = false;
                        lblRemarks.Text = "Meter Reading Date should be greater than the Meter Changed Date";
                    }
                    else if (hfIsMeterAssignedDateExists.Value.ToLower() == true.ToString().ToLower())
                    {
                        txtCurReading.CssClass = "gvtext";
                        cbIsTamper.Enabled = txtCurReading.Enabled = false;
                        lblRemarks.Text = "Meter Reading Date should be greater than the Meter Assigned Date";
                    }
                    else if (hfIsRollOver.Value.ToLower() == true.ToString().ToLower())
                    {
                        txtCurReading.CssClass = "gvtext";
                        cbIsTamper.Enabled = txtCurReading.Enabled = false;
                        lblRemarks.Text = "Meter Reset";
                    }
                    else if (lblGvIsBilled.Text == "1" && hfIsFutureMonthBilled.Value.ToLower() == true.ToString().ToLower())
                    {
                        txtCurReading.CssClass = "gvtext";
                        cbIsTamper.Enabled = txtCurReading.Enabled = false;
                        lblRemarks.Text = "Bill Generated for Future Date";
                    }
                    else
                    {
                        hfActiveStatus.Value = true.ToString();

                        cbIsTamper.Enabled = txtCurReading.Enabled = true;
                        txtCurReading.Attributes.Add("onkeyup", "CalculateNewUsageGV('" + lblPre.ClientID + "','" + lblUSG.ClientID + "',this,'" + lblDials.ClientID + "')");
                        txtCurReading.Attributes.Add("onkeypress", "return GVAllowNumbers('" + lblDials.ClientID + "','" + lblDeciumals.ClientID + "',this,event)");

                        lblPre.Text = AppendZeros(lblPre.Text, Convert.ToInt32(lblDials.Text));
                        txtCurReading.Attributes.Add("onFocus", "DoFocus(this);");
                        txtCurReading.Attributes.Add("onBlur", "DoBlur(this);");
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #region Previous Code
        //protected void gvMeterReadingList_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    try
        //    {
        //        if (e.Row.RowType == DataControlRowType.DataRow)
        //        {
        //            Label lblExist = (Label)e.Row.FindControl("lblGvExist");
        //            Label lblCurReading = (Label)e.Row.FindControl("lblGvCurrentReading");
        //            Label lblUSG = (Label)e.Row.FindControl("lblGvUsage");
        //            Label lblPre = (Label)e.Row.FindControl("lblGvPreviousReading");
        //            Label lblDials = (Label)e.Row.FindControl("lblItemDials");
        //            Label lblDeciumals = (Label)e.Row.FindControl("lblItemDecimals");
        //            //Label lblIsBilled = (Label)e.Row.FindControl("lblGvIsBilled");
        //            //Label lblIsBillingMonthLocked = (Label)e.Row.FindControl("lblIsBillingMonthLocked");
        //            Label lblIsTamper = (Label)e.Row.FindControl("lblIsTamper");

        //            TextBox txtCurReading = (TextBox)e.Row.FindControl("txtGvCurrentReading");
        //            CheckBox cbIsTamper = (CheckBox)e.Row.FindControl("cbIsTamper");
        //            LinkButton lkEdit = (LinkButton)e.Row.FindControl("lkEditReading");
        //            Label lblRemarks = (Label)e.Row.FindControl("lblRemarks");
        //            HiddenField hfIsHaveLatestReading = (HiddenField)e.Row.FindControl("hfIsHaveLatestReading");
        //            //HiddenField hfEstimatedDate = (HiddenField)e.Row.FindControl("hfEstimatedDate");
        //            //HiddenField hfIsFutureMonthBilled = (HiddenField)e.Row.FindControl("hfIsFutureMonthBilled");
        //            txtCurReading.MaxLength = Convert.ToInt32(lblDials.Text);

        //            lblUSG.Text = Math.Round(Convert.ToDecimal(lblUSG.Text), Convert.ToInt32(lblDeciumals.Text)).ToString();

        //            if (lblIsTamper.Text == true.ToString())
        //            {
        //                cbIsTamper.Checked = true;
        //            }

        //            if (hfIsFutureMonthBilled.Value.ToLower() == false.ToString().ToLower())
        //            {
        //                if (lblPre.Text.Length < Convert.ToInt32(lblDials.Text))
        //                {
        //                    lblPre.Text = AppendZeros(lblPre.Text, Convert.ToInt32(lblDials.Text));
        //                }

        //                if (!string.IsNullOrEmpty(lblCurReading.Text))
        //                    txtCurReading.Text = AppendZeros(lblCurReading.Text, Convert.ToInt32(lblDials.Text));

        //                if (hfIsHaveLatestReading.Value.ToLower() == true.ToString().ToLower())
        //                {
        //                    txtCurReading.CssClass = "gvtext";
        //                    cbIsTamper.Enabled = txtCurReading.Enabled = false;
        //                    lblRemarks.Text = Resource.LATEST_DATE_EXISTS;
        //                }
        //                else
        //                {
        //                    if (Convert.ToBoolean(lblIsBillingMonthLocked.Text))
        //                    {
        //                        cbIsTamper.Enabled = txtCurReading.Enabled = false;
        //                        txtCurReading.CssClass = "gvtext";
        //                        lblRemarks.Text = Resource.MONTH_LOCKED;
        //                    }
        //                    else
        //                    {
        //                        cbIsTamper.Enabled = txtCurReading.Enabled = true;
        //                        txtCurReading.Attributes.Add("onkeyup", "CalculateNewUsageGV('" + lblPre.ClientID + "','" + lblUSG.ClientID + "',this,'" + lblDials.ClientID + "')");
        //                        txtCurReading.Attributes.Add("onkeypress", "return GVAllowNumbers('" + lblDials.ClientID + "','" + lblDeciumals.ClientID + "',this,event)");
        //                    }
        //                    lblPre.Text = AppendZeros(lblPre.Text, Convert.ToInt32(lblDials.Text));
        //                    txtCurReading.Attributes.Add("onFocus", "DoFocus(this);");
        //                    txtCurReading.Attributes.Add("onBlur", "DoBlur(this);");
        //                }
        //            }
        //            else
        //            {
        //                txtCurReading.CssClass = "gvtext";
        //                cbIsTamper.Enabled = txtCurReading.Enabled = false;
        //                lblRemarks.Text = "Billed Future Months";
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        #endregion

        //Show ListBoxes of Accounts based on Usage in GV Savenext
        protected void btnGvSave_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow row in gvMeterReadingList.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        Label lblExist = (Label)row.FindControl("lblGvExist");
                        Label lblAcc = (Label)row.FindControl("lblAccountNo");
                        Label lblCurReading = (Label)row.FindControl("lblGvCurrentReading");
                        Label lblPre = (Label)row.FindControl("lblGvPreviousReading");
                        Label lblAvg = (Label)row.FindControl("lblGvAvgReading");
                        Label lblCnt = (Label)row.FindControl("lblGvCountReading");
                        Label lblusg = (Label)row.FindControl("lblGvUsage");
                        Label lblDials = (Label)row.FindControl("lblItemDials");
                        //Label lblIsBilled = (Label)row.FindControl("lblGvIsBilled");
                        TextBox txtCurReading = (TextBox)row.FindControl("txtGvCurrentReading");
                        CheckBox cbIsTamper = (CheckBox)row.FindControl("cbIsTamper");
                        Label lblRemarks = (Label)row.FindControl("lblRemarks");
                        HiddenField hfActiveStatus = (HiddenField)row.FindControl("hfActiveStatus");
                        HiddenField hfIsHaveLatestReading = (HiddenField)row.FindControl("hfIsHaveLatestReading");
                        //HiddenField hfIsLocked = (HiddenField)row.FindControl("hfIsLocked");
                        HiddenField hfApproveStatusId = (HiddenField)row.FindControl("hfApproveStatusId");
                        HiddenField hfIsValid = (HiddenField)row.FindControl("hfIsValid");

                        //HiddenField hfEstimatedDate = (HiddenField)row.FindControl("hfEstimatedDate");
                        //Label lblIsBillingMonthLocked = (Label)row.FindControl("lblIsBillingMonthLocked");
                        //HiddenField hfIsFutureMonthBilled = (HiddenField)row.FindControl("hfIsFutureMonthBilled");

                        //bool IsRollover = true;
                        row.Style.Remove("Background-Color");
                        row.BackColor = System.Drawing.Color.White;

                        hfIsValid.Value = true.ToString();

                        if (hfActiveStatus.Value.ToLower() == true.ToString().ToLower())
                        {
                            if (!string.IsNullOrEmpty(txtCurReading.Text))
                            {
                                decimal CheckCurrentReading = Convert.ToDecimal(txtCurReading.Text);
                                if (CheckCurrentReading > 0)
                                    lblusg.Text = (CheckCurrentReading - Convert.ToDecimal(lblPre.Text)).ToString();
                                else
                                    lblusg.Text = "0";

                                if (CheckCurrentReading - Convert.ToDecimal(lblPre.Text) >= 0)
                                {
                                    cbIsTamper.Enabled = txtCurReading.Enabled = false;
                                    if (Convert.ToDecimal(lblAvg.Text) > 0)
                                    {
                                        if (_ObjCommonMethods.IsHugeAmount(Convert.ToDecimal(lblusg.Text), Convert.ToDecimal(lblAvg.Text)))
                                        {
                                            lbxHugeUsage.Items.Add(lblAcc.Text);
                                            lblRemarks.Text = Resource.HUGE_MILD;
                                        }
                                        else
                                        {
                                            lblRemarks.Text = Resource.VALID;
                                            row.BackColor = System.Drawing.Color.White;
                                        }
                                    }
                                    else
                                    {
                                        lblRemarks.Text = Resource.VALID;
                                        row.BackColor = System.Drawing.Color.White;
                                    }
                                }
                                else
                                {
                                    lblRemarks.Text = "Invalid Usage";
                                    lblRemarks.ForeColor = System.Drawing.Color.White;
                                    hfIsValid.Value = false.ToString();
                                    row.BackColor = System.Drawing.Color.Tomato;
                                    cbIsTamper.Enabled = txtCurReading.Enabled = false;
                                }
                            }
                            else
                            {
                                lblRemarks.Text = "Invalid Readings";
                                lblRemarks.ForeColor = System.Drawing.Color.White;
                                hfIsValid.Value = false.ToString();
                                row.BackColor = System.Drawing.Color.Tomato;
                                cbIsTamper.Enabled = txtCurReading.Enabled = false;
                            }
                        }
                        else
                        {
                            lblRemarks.ForeColor = System.Drawing.Color.White;
                            row.BackColor = System.Drawing.Color.Tomato;
                            cbIsTamper.Enabled = txtCurReading.Enabled = false;
                        }
                    }
                }
                EnableConfirmButtons(true);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //Continue with ListBoxes of Accounts based on Usage>0 in GV Savenext
        protected void btnGvContinue_Click(object sender, EventArgs e)
        {
            try
            {
                FromGvSaveNext();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnGridRecordsCancel_Click(object sender, EventArgs e)
        {
            try
            {
                EnableConfirmButtons(false);
                foreach (GridViewRow row in gvMeterReadingList.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        //HiddenField hfIsFutureMonthBilled = (HiddenField)row.FindControl("hfIsFutureMonthBilled");
                        //HiddenField hfIsHaveLatestReading = (HiddenField)row.FindControl("hfIsHaveLatestReading");
                        HiddenField hfActiveStatus = (HiddenField)row.FindControl("hfActiveStatus");
                        HiddenField hfIsValid = (HiddenField)row.FindControl("hfIsValid");
                        //HiddenField hfEstimatedDate = (HiddenField)row.FindControl("hfEstimatedDate");
                        Label lblRemarks = (Label)row.FindControl("lblRemarks");
                        //Label lblIsBilled = (Label)row.FindControl("lblGvIsBilled");
                        //TextBox txtCurReading = (TextBox)row.FindControl("txtGvCurrentReading");
                        //Label lblIsBillingMonthLocked = (Label)row.FindControl("lblIsBillingMonthLocked");

                        if (hfActiveStatus.Value.ToLower() == true.ToString().ToLower())
                        {
                            ((CheckBox)row.FindControl("cbIsTamper")).Enabled = ((TextBox)row.FindControl("txtGvCurrentReading")).Enabled = true;
                            lblRemarks.Text = string.Empty;
                        }
                        else if (hfIsValid.Value.ToLower() == false.ToString().ToLower())
                        {
                            ((CheckBox)row.FindControl("cbIsTamper")).Enabled = ((TextBox)row.FindControl("txtGvCurrentReading")).Enabled = true;
                            lblRemarks.Text = string.Empty;
                            //if (Convert.ToBoolean(lblIsBillingMonthLocked.Text))
                            //{
                            //    ((CheckBox)row.FindControl("cbIsTamper")).Enabled = ((TextBox)row.FindControl("txtGvCurrentReading")).Enabled = false;
                            //}
                            //else
                            //    if (hfIsHaveLatestReading.Value.ToLower() == false.ToString().ToLower())//&& string.IsNullOrEmpty(hfEstimatedDate.Value)&& lblIsBilled.Text == "0" 
                            //    {
                            //        ((CheckBox)row.FindControl("cbIsTamper")).Enabled = ((TextBox)row.FindControl("txtGvCurrentReading")).Enabled = true;
                            //        lblRemarks.Text = string.Empty;
                            //    }
                        }
                        //else
                        //{
                        //    ((CheckBox)row.FindControl("cbIsTamper")).Enabled = ((TextBox)row.FindControl("txtGvCurrentReading")).Enabled = false;
                        //}
                        //txtCurReading.Enabled = true;
                        row.Style.Remove("Background-Color");
                        lblRemarks.ForeColor = System.Drawing.Color.Black;
                        //if (row.RowIndex % 2 == 1)
                        //    row.Style["Background-Color"] = "#E8FAEA";
                        //else
                        row.BackColor = System.Drawing.Color.White;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void EnableConfirmButtons(bool p)
        {
            divpaging.Visible = btnGvSave.Visible = !p;
            btnGridRecordsProceed.Visible = btnGridRecordsCancel.Visible = p;
        }
        //If Continue click in High Usage popup of Edit Column                 
        protected void btnContinuePopupGvInvalidUsage_Click(object sender, EventArgs e)
        {
            try
            {
                string[] strDetails = System.Text.RegularExpressions.Regex.Split(lblInvalidUsageGvAlerts.Text, ",");
                if (strDetails.Count() > 0)
                {
                    GridUpdateReading(strDetails[0], strDetails[1], strDetails[2], strDetails[3], strDetails[4], strDetails[5], strDetails[6]);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #endregion

        #region ddlEvents

        protected void ddlBU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divRouteSequenceWise.Visible = false;
                if (ddlBU.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindUserServiceUnits_BuIds(ddlSU, ddlBU.SelectedValue, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlSU.Enabled = true;
                    ddlSC.Enabled = ddlCycle.Enabled = ddlRoutes.Enabled = false;
                    //if (ddlSU.Items.Count > 0)
                    //    ddlSU.SelectedIndex = 0;
                }
                else
                {
                    ddlSU.Enabled = ddlSC.Enabled = ddlCycle.Enabled = ddlRoutes.Enabled = false;
                }
                if (ddlSU.Items.Count > 0)
                    ddlSU.SelectedIndex = 0;
                if (ddlSC.Items.Count > 0)
                    ddlSC.SelectedIndex = 0;
                if (ddlCycle.Items.Count > 0)
                    ddlCycle.SelectedIndex = 0;
                if (ddlRoutes.Items.Count > 0)
                    ddlRoutes.SelectedIndex = 0;

                ddlMeterReader.Items.Clear();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlSU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divRouteSequenceWise.Visible = false;
                if (ddlSU.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindUserServiceCenters_SuIds(ddlSC, ddlSU.SelectedValue, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlSC.Enabled = true;
                    ddlCycle.Enabled = ddlRoutes.Enabled = false;
                }
                else
                {
                    ddlSC.Enabled = ddlCycle.Enabled = ddlRoutes.Enabled = false;
                }
                if (ddlSC.Items.Count > 0)
                    ddlSC.SelectedIndex = 0;
                if (ddlCycle.Items.Count > 0)
                    ddlCycle.SelectedIndex = 0;
                if (ddlRoutes.Items.Count > 0)
                    ddlRoutes.SelectedIndex = 0;

                ddlMeterReader.Items.Clear();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divRouteSequenceWise.Visible = false;
                if (ddlSC.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindCyclesBySC_HavingBooks(ddlCycle, ddlSC.SelectedValue, true, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlCycle.Enabled = true;
                    ddlRoutes.Enabled = false;
                }
                else
                {
                    ddlCycle.Enabled = ddlRoutes.Enabled = false;
                }
                if (ddlCycle.Items.Count > 0)
                    ddlCycle.SelectedIndex = 0;
                if (ddlRoutes.Items.Count > 0)
                    ddlRoutes.SelectedIndex = 0;

                ddlMeterReader.Items.Clear();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlCycle_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divRouteSequenceWise.Visible = false;
                if (ddlCycle.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindAllBookNumbersWithDetails_MeterReadings(ddlRoutes, ddlCycle.SelectedValue, true, false, UMS_Resource.DROPDOWN_SELECT);
                    ddlRoutes.Enabled = true;
                }
                else
                {
                    ddlRoutes.Items.Clear();
                    ddlRoutes.Enabled = false;
                }
                if (ddlRoutes.Items.Count > 0)
                    ddlRoutes.SelectedIndex = 0;

                ddlMeterReader.Items.Clear();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        //Get Max Number of Given Dials
        private string GetAllNines(int Dails)
        {
            string Str = "";
            for (int i = 0; i < Dails; i++)
            {
                Str += "9";
            }
            return Str;
        }
        //Append Zeros Before the String Passed
        //Get Max Number of Given Dials
        private string AppendZeros(string Reading, int Dails)
        {
            string Str = "";
            if (Reading.Length < Dails)
            {
                for (int i = 0; i < (Dails - Reading.Length); i++)
                {
                    Str += "0";
                }
            }
            return Str + Reading;
        }

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                _ObjiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                lkbtnNext.Attributes.Remove("style");
                lkbtnLast.Attributes.Remove("style");
                lkbtnFirst.Attributes.Remove("style");
                lkbtnPrevious.Attributes.Remove("style");

                if (totalpages == 1)
                {
                    lkbtnPrevious.Enabled = lkbtnFirst.Enabled = lkbtnNext.Enabled = lkbtnLast.Enabled = false;
                    lkbtnFirst.Attributes.Add("style", "color:#BBB");
                    lkbtnPrevious.Attributes.Add("style", "color:#BBB");
                    lkbtnNext.Attributes.Add("style", "color:#BBB");
                    lkbtnLast.Attributes.Add("style", "color:#BBB");
                }
                else
                {
                    if (Math.Round(totalpages, 0) == Convert.ToInt32(hfPageNo.Value))
                    {
                        lkbtnNext.Enabled = lkbtnLast.Enabled = false;
                        lkbtnNext.Attributes.Add("style", "color:#BBB");
                        lkbtnLast.Attributes.Add("style", "color:#BBB");
                        lkbtnFirst.Attributes.Add("style", "color:#0078C5");
                        lkbtnPrevious.Attributes.Add("style", "color:#0078C5");
                    }
                    else if (Convert.ToInt32(hfPageNo.Value) == 1)
                    {
                        lkbtnFirst.Enabled = lkbtnPrevious.Enabled = false;
                        lkbtnFirst.Attributes.Add("style", "color:#BBB");
                        lkbtnPrevious.Attributes.Add("style", "color:#BBB");
                        lkbtnNext.Attributes.Add("style", "color:#0078C5");
                        lkbtnLast.Attributes.Add("style", "color:#0078C5");
                    }
                    else
                    {
                        lkbtnNext.Enabled = lkbtnLast.Enabled = lkbtnFirst.Enabled = lkbtnPrevious.Enabled = true;
                        lkbtnFirst.Attributes.Add("style", "color:#0078C5");
                        lkbtnPrevious.Attributes.Add("style", "color:#0078C5");
                        lkbtnNext.Attributes.Add("style", "color:#0078C5");
                        lkbtnLast.Attributes.Add("style", "color:#0078C5");
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                _ObjCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private DataTable CreateMeterReadingsDT(DataTable dt)
        {
            dt.Columns.Add("SNo", typeof(int));
            dt.Columns.Add("AccNum", typeof(string));
            dt.Columns.Add("TotalReadings", typeof(int));
            dt.Columns.Add("AverageReading", typeof(string));
            dt.Columns.Add("PresentReading", typeof(string));
            dt.Columns.Add("Usage", typeof(string));
            dt.Columns.Add("IsTamper", typeof(bool));
            dt.Columns.Add("PreviousReading", typeof(string));
            dt.Columns.Add("IsExists", typeof(bool));
            dt.Columns.Add("Multiplier", typeof(int));
            dt.Columns.Add("ReadDate", typeof(string));
            dt.Columns.Add("ReadBy", typeof(string));
            return dt;
        }

        private DataTable UpdateMeterReadingsDT(DataTable dt, string accNum, int totalReadings, string averageReading, string presentReading, string usage, bool isTamper, string previousReading, bool isExists, int Multiplier, string ReadDate, string ReadBy)
        {
            DataRow dr = dt.NewRow();
            dr["SNo"] = dt.Rows.Count + 1;
            dr["AccNum"] = accNum;
            dr["TotalReadings"] = totalReadings;
            dr["AverageReading"] = averageReading;
            dr["PresentReading"] = presentReading;
            dr["Usage"] = usage;
            dr["IsTamper"] = isTamper;
            dr["PreviousReading"] = previousReading;
            dr["IsExists"] = isExists;
            dr["Multiplier"] = Multiplier;
            dr["ReadDate"] = ReadDate;
            dr["ReadBy"] = ReadBy;
            dt.Rows.Add(dr);
            return dt;
        }

        private void FromGvSaveNext()
        {
            try
            {
                DataTable dt = new DataTable();
                DataSet ds = new DataSet();
                dt = CreateMeterReadingsDT(dt);
                bool isSuccess = false;
                bool isFinalApproval = false;
                int ActiveStatusId = (int)EnumApprovalStatus.Process;
                isFinalApproval = _ObjCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.MeterReadings, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());

                if (isFinalApproval)
                    ActiveStatusId = (int)EnumApprovalStatus.Approved;

                //BillingBE _objBilling = new BillingBE();
                ////_objBilling.ReadDate = _ObjCommonMethods.Convert_MMDDYY(txtReadingDate.Text);
                ////_objBilling.ReadBy = ddlMeterReader.SelectedItem.Value;
                //_objBilling.MeterReadingFrom = Convert.ToInt32(MeterReadingFrom.BookWise);
                //_objBilling.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                ////_objBilling.IsFinalApproval = isFinalApproval;

                foreach (GridViewRow row in gvMeterReadingList.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        HiddenField hfActiveStatus = (HiddenField)row.FindControl("hfActiveStatus");
                        HiddenField hfIsValid = (HiddenField)row.FindControl("hfIsValid");

                        if (hfActiveStatus.Value == true.ToString() && hfIsValid.Value == true.ToString())
                        {
                            Label lblExist = (Label)row.FindControl("lblGvExist");
                            Label lblAcc = (Label)row.FindControl("lblAccountNo");
                            Label lblCurReading = (Label)row.FindControl("lblGvCurrentReading");
                            Label lblPre = (Label)row.FindControl("lblGvPreviousReading");
                            Label lblAvg = (Label)row.FindControl("lblGvAvgReading");
                            Label lblCnt = (Label)row.FindControl("lblGvCountReading");
                            Label lblMultiplier = (Label)row.FindControl("lblMultiplier");
                            Label lblDials = (Label)row.FindControl("lblItemDials");
                            TextBox txtCurReading = (TextBox)row.FindControl("txtGvCurrentReading");
                            HiddenField hfIsTamper = (HiddenField)row.FindControl("hfIsTamper");
                            CheckBox cbIsTamper = (CheckBox)row.FindControl("cbIsTamper");
                            Label lblMeterNoOnly = (Label)row.FindControl("lblMeterNoOnly");

                            hfIsTamper.Value = cbIsTamper.Checked.ToString();
                            decimal CheckCurrentReading = string.IsNullOrEmpty(txtCurReading.Text) ? 0 : Convert.ToDecimal(txtCurReading.Text);
                            bool isExist = false;
                            string usage = string.Empty;
                            int totalReadings = string.IsNullOrEmpty(lblCnt.Text) ? 0 : Convert.ToInt32(lblCnt.Text);
                            int multiplier = string.IsNullOrEmpty(lblMultiplier.Text) ? 0 : Convert.ToInt32(lblMultiplier.Text);

                            usage = (CheckCurrentReading - Convert.ToDecimal(lblPre.Text)).ToString();

                            if (lblExist.Text == "1")
                            {
                                isExist = true;
                                //count++;
                                //GridUpdateReading(lblPre.Text, txtCurReading.Text, usage, lblAvg.Text, lblCnt.Text, lblAcc.Text, cbIsTamper.Checked.ToString());
                            }
                            //else if (lblExist.Text == "0")
                            //{
                            //    BillingBE _objBilling = new BillingBE();
                            //    _objBilling.ReadDate = _ObjCommonMethods.Convert_MMDDYY(txtReadingDate.Text);
                            //    _objBilling.ReadBy = ddlMeterReader.SelectedItem.Value;
                            //    _objBilling.PreviousReading = lblPre.Text.Trim();
                            //    _objBilling.PresentReading = AppendZeros(txtCurReading.Text.Trim(), Convert.ToInt32(lblDials.Text));//txtCurReading.Text.Trim();
                            //    _objBilling.Usage = usage;
                            //    _objBilling.MeterReadingFrom = Convert.ToInt32(MeterReadingFrom.BookWise);
                            //    _objBilling.IsTamper = cbIsTamper.Checked;
                            //    _objBilling.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                            //    _objBilling.AccNum = lblAcc.Text;
                            //    _objBilling = _ObjiDsignBAL.DeserializeFromXml<BillingBE>(_objBillingBAL.InsertCustomerBillReadingBAL(_ObjiDsignBAL.DoSerialize<BillingBE>(_objBilling)));
                            //    count++;
                            //}

                            dt = UpdateMeterReadingsDT(dt, lblAcc.Text, totalReadings, lblAvg.Text, txtCurReading.Text, usage, cbIsTamper.Checked, lblPre.Text, isExist, multiplier, _ObjCommonMethods.Convert_MMDDYY(txtReadingDate.Text), ddlMeterReader.SelectedItem.Value);
                        }
                    }
                }
                if (dt.Rows.Count > 0)
                {
                    ds.Tables.Add(dt);
                    string BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                    //isSuccess = _objBillingBAL.InsertMeterReadingsDT(Session[UMS_Resource.SESSION_LOGINID].ToString(), (int)MeterReadingFrom.BulkUpload, dt);
                    isSuccess = _objBillingBAL.InsertBookwiseMeterReadingsDT(Session[UMS_Resource.SESSION_LOGINID].ToString(), (int)MeterReadingFrom.BookWise, dt, isFinalApproval, (int)EnumApprovalFunctions.MeterReadings, ActiveStatusId, BU_ID);
                }
                if (isSuccess)
                {
                    //foreach (GridViewRow row in gvMeterReadingList.Rows)
                    //{
                    //    if (row.RowType == DataControlRowType.DataRow)
                    //    {
                    //        HiddenField hfIsHaveLatestReading = (HiddenField)row.FindControl("hfIsHaveLatestReading");
                    //        HiddenField hfEstimatedDate = (HiddenField)row.FindControl("hfEstimatedDate");

                    //        if (hfIsHaveLatestReading.Value.ToLower() == false.ToString().ToLower() && string.IsNullOrEmpty(hfEstimatedDate.Value))
                    //        {
                    //            ((TextBox)row.FindControl("txtGvCurrentReading")).Enabled = true;
                    //        }
                    //    }
                    //}
                    EnableConfirmButtons(false);
                    divRouteSequenceWise.Visible = false;
                    if (isFinalApproval)
                        Message(Resource.BILLREADING_INSERTSUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    else
                        Message(Resource.APPROVAL_TARIFF_CHANGE, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                {
                    EnableConfirmButtons(true);
                    Message("There is no valid Meter Readings to save.", UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        public void FromSaveNext()
        {
            try
            {
                bool isFinalApproval = false;
                int ActiveStatusId = (int)EnumApprovalStatus.Process;
                isFinalApproval = _ObjCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.MeterReadings, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());

                if (isFinalApproval)
                    ActiveStatusId = (int)EnumApprovalStatus.Approved;

                BillingBE _objBilling = new BillingBE();
                _objBilling.ReadDate = _ObjCommonMethods.Convert_MMDDYY(txtReadingDate.Text);
                _objBilling.ReadBy = ddlMeterReader.SelectedItem.Value;
                _objBilling.PreviousReading = lblPreviousReading.Text;
                _objBilling.MeterReadingFrom = Convert.ToInt32(MeterReadingFrom.AccountWise);
                _objBilling.PresentReading = AppendZeros(txtCurrentReading.Text.Trim(), Convert.ToInt32(hfDial.Value));//ID-065 append zeros to current reading
                _objBilling.IsTamper = cbCustIsByPass.Checked;
                _objBilling.Usage = lblUsageDetails.Text;
                _objBilling.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objBilling.AccNum = lblAccountNumber.Text;
                _objBilling.Rollover = Convert.ToBoolean(hfRollover.Value);
                _objBilling.IsFinalApproval = isFinalApproval;
                _objBilling.ActiveStatusId = ActiveStatusId;
                _objBilling.IsExists = Convert.ToBoolean(hfIsExists.Value);
                _objBilling.FunctionId = (int)EnumApprovalFunctions.MeterReadings;
                _objBilling.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                //_objBilling = _ObjiDsignBAL.DeserializeFromXml<BillingBE>(_objBillingBAL.InsertCustomerBillReadingBAL(_ObjiDsignBAL.DoSerialize<BillingBE>(_objBilling)));
                _objBilling = _ObjiDsignBAL.DeserializeFromXml<BillingBE>(_objBillingBAL.InsertMeterReadingsLogs(_ObjiDsignBAL.DoSerialize<BillingBE>(_objBilling)));

                if (_objBilling.IsSuccess == 1)
                {
                    //save and next click
                    if (isFinalApproval)
                        Message(Resource.BILLREADING_INSERTSUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    else
                        Message(Resource.APPROVAL_TARIFF_CHANGE, UMS_Resource.MESSAGETYPE_SUCCESS);
                    txtCurrentReading.Text = string.Empty;
                }
                else
                {
                    Message(UMS_Resource.BILLREADING_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            chkRollOver.Checked = false;
            hfRollover.Value = "False";
        }

        protected void NegUsageValid(string Previous, string Current, string Usage, string AccNum, string Average, string Count, string InsertOrUpdate, string IsTamper, int MeterReadingFrom)
        {
            try
            {
                bool isFinalApproval = false;
                int ActiveStatusId = (int)EnumApprovalStatus.Process;
                isFinalApproval = _ObjCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.MeterReadings, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());

                if (isFinalApproval)
                    ActiveStatusId = (int)EnumApprovalStatus.Approved;

                BillingBE _objBilling = new BillingBE();
                _objBilling.ReadDate = _ObjCommonMethods.Convert_MMDDYY(lblLatestPopup.Text); //Convert_DDMMYY(lblLatestPopup.Text);
                _objBilling.ReadBy = ddlMeterReader.SelectedItem.Value;
                _objBilling.PreviousReading = Previous;
                _objBilling.PresentReading = Current;
                _objBilling.Usage = Usage;
                _objBilling.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objBilling.AccNum = AccNum;
                _objBilling.IsTamper = Convert.ToBoolean(IsTamper);
                _objBilling.Rollover = Convert.ToBoolean(hfRolloverPopup.Value);
                _objBilling.MeterReadingFrom = MeterReadingFrom;
                _objBilling.IsFinalApproval = isFinalApproval;
                _objBilling.ActiveStatusId = ActiveStatusId;
                _objBilling.IsExists = Convert.ToBoolean(hfIsExists.Value);
                _objBilling.FunctionId = (int)EnumApprovalFunctions.MeterReadings;
                _objBilling.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                _objBilling = _ObjiDsignBAL.DeserializeFromXml<BillingBE>(_objBillingBAL.InsertMeterReadingsLogs(_ObjiDsignBAL.DoSerialize<BillingBE>(_objBilling)));

                //if (InsertOrUpdate == "1")
                //{
                //    _objBilling = _ObjiDsignBAL.DeserializeFromXml<BillingBE>(_objBillingBAL.InsertCustomerBillReadingBAL(_ObjiDsignBAL.DoSerialize<BillingBE>(_objBilling)));
                //}
                //else if (InsertOrUpdate == "2")
                //{
                //    _objBilling.AverageReading = Average;
                //    _objBilling.TotalReadings = Count;
                //    _objBilling = _ObjiDsignBAL.DeserializeFromXml<BillingBE>(_objBillingBAL.UpdateCustomerBillReadingBAL(_ObjiDsignBAL.DoSerialize<BillingBE>(_objBilling)));
                //}

                if (_objBilling.IsSuccess == 1)
                {
                    if (isFinalApproval)
                        Message(Resource.BILLREADING_INSERTSUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    else
                        Message(Resource.APPROVAL_TARIFF_CHANGE, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                {
                    Message(UMS_Resource.BILLREADING_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        public void PopUpUpdate()
        {
            try
            {
                bool isFinalApproval = false;
                int ActiveStatusId = (int)EnumApprovalStatus.Process;
                isFinalApproval = _ObjCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.MeterReadings, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());

                if (isFinalApproval)
                    ActiveStatusId = (int)EnumApprovalStatus.Approved;

                BillingBE _objBilling = new BillingBE();
                _objBilling.ReadDate = _ObjCommonMethods.Convert_MMDDYY(lblLatestPopup.Text); //Convert_DDMMYY(lblLatestPopup.Text);
                _objBilling.ReadBy = ddlMeterReader.SelectedItem.Value;
                _objBilling.PreviousReading = lblpopGoPreviousReading.Text;
                _objBilling.PresentReading = AppendZeros(txtpopGoCurrent.Text.Trim(), Convert.ToInt32(hfDial.Value));
                _objBilling.IsTamper = cbPopIsTamper.Checked;
                _objBilling.Rollover = Convert.ToBoolean(hfRolloverPopup.Value);
                _objBilling.MeterReadingFrom = Convert.ToInt32(MeterReadingFrom.AccountWise);
                _objBilling.Usage = lblpopGoUsage.Text;
                _objBilling.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objBilling.AverageReading = lblpopAvg.Text;
                _objBilling.TotalReadings = lblpopGoCount.Text;
                _objBilling.AccNum = lblpopGoAccNum.Text;
                _objBilling.IsFinalApproval = isFinalApproval;
                _objBilling.ActiveStatusId = ActiveStatusId;
                _objBilling.IsExists = Convert.ToBoolean(hfIsExists.Value);
                _objBilling.FunctionId = (int)EnumApprovalFunctions.MeterReadings;
                _objBilling.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                // _objBilling = _ObjiDsignBAL.DeserializeFromXml<BillingBE>(_objBillingBAL.UpdateCustomerBillReadingBAL(_ObjiDsignBAL.DoSerialize<BillingBE>(_objBilling)));
                _objBilling = _ObjiDsignBAL.DeserializeFromXml<BillingBE>(_objBillingBAL.InsertMeterReadingsLogs(_ObjiDsignBAL.DoSerialize<BillingBE>(_objBilling)));
                if (_objBilling.IsSuccess == 1)
                {
                    //update click
                    if (isFinalApproval)
                        Message(Resource.BILLREADING_INSERTSUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    else
                        Message(Resource.APPROVAL_TARIFF_CHANGE, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                {
                    Message(UMS_Resource.BILLREADING_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        public void GridUpdateReading(string Previous, string Current, string Usage, string Avg, string TotalRead, string AccNum, string IsTamper)
        {
            try
            {
                bool isFinalApproval = false;
                int ActiveStatusId = (int)EnumApprovalStatus.Process;
                isFinalApproval = _ObjCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.MeterReadings, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());

                if (isFinalApproval)
                    ActiveStatusId = (int)EnumApprovalStatus.Approved;

                BillingBE _objBilling = new BillingBE();
                _objBilling.ReadDate = _ObjCommonMethods.Convert_MMDDYY(txtReadingDate.Text);
                _objBilling.ReadBy = ddlMeterReader.SelectedItem.Value;
                _objBilling.PreviousReading = Previous;
                _objBilling.PresentReading = Current;
                _objBilling.Usage = Usage;
                _objBilling.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objBilling.AverageReading = Avg;
                _objBilling.MeterReadingFrom = Convert.ToInt32(MeterReadingFrom.BookWise);
                _objBilling.TotalReadings = TotalRead;
                _objBilling.IsTamper = Convert.ToBoolean(IsTamper);
                _objBilling.AccNum = AccNum;
                _objBilling.IsFinalApproval = isFinalApproval;
                _objBilling.ActiveStatusId = ActiveStatusId;
                _objBilling.IsExists = Convert.ToBoolean(hfIsExists.Value);
                _objBilling.FunctionId = (int)EnumApprovalFunctions.MeterReadings;
                _objBilling.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                _objBilling = _ObjiDsignBAL.DeserializeFromXml<BillingBE>(_objBillingBAL.InsertMeterReadingsLogs(_ObjiDsignBAL.DoSerialize<BillingBE>(_objBilling)));
                //_objBilling = _ObjiDsignBAL.DeserializeFromXml<BillingBE>(_objBillingBAL.UpdateCustomerBillReadingBAL(_ObjiDsignBAL.DoSerialize<BillingBE>(_objBilling)));
                if (_objBilling.IsSuccess == 1)
                {
                    if (isFinalApproval)
                        Message(Resource.BILLREADING_INSERTSUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    else
                        Message(Resource.APPROVAL_TARIFF_CHANGE, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                {
                    Message(UMS_Resource.BILLREADING_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        //protected void txtAccountNumber_TextChanged(object sender, EventArgs e)
        //{
        //    //ConsumerBe objConsumerBe = new ConsumerBe();
        //    //objConsumerBe.AccountNo = txtAccountNumber.Text.Trim();
        //    //objConsumerBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();

        //    //ConsumerBal _objConsumerBal = new ConsumerBal();
        //    //XmlDocument xml = _objConsumerBal.GetMarketerByAccountNo(objConsumerBe, ReturnType.Single);
        //    //objConsumerBe = _ObjiDsignBAL.DeserializeFromXml<ConsumerBe>(xml);

        //    //if (ddlMeterReader.Items.Count > 1)
        //    //{
        //    //    ddlMeterReader.SelectedIndex =
        //    //        ddlMeterReader.Items.IndexOf(ddlMeterReader.Items.FindByValue(Convert.ToString(objConsumerBe.MarketerId)));
        //    //}

        //    ddlMeterReader.Items.Clear();
        //    if (!string.IsNullOrEmpty(txtAccountNumber.Text.Trim()))
        //    {
        //        BindMarketersByBookNo(1, txtAccountNumber.Text.Trim(), string.Empty);
        //    }
        //}

        protected void btnRoutesDummy_Click(object sender, EventArgs e)
        {
            ddlMeterReader.Items.Clear();
            if (ddlRoutes.SelectedIndex > 0)
            {
                BindMarketersByBookNo(2, string.Empty, ddlRoutes.SelectedValue);
            }
        }

        private void BindMarketersByBookNo(int flag, string accountNo, string bookNo)
        {
            XmlDocument xmlResult = new XmlDocument();
            MarketersBal _objMarketersBal = new MarketersBal();
            MarketersBe _objMarketersBe = new MarketersBe();
            MarketersListBe _objMarketersListBe = new MarketersListBe();
            _objMarketersBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
            _objMarketersBe.Flag = flag;
            _objMarketersBe.AccountNo = accountNo;
            _objMarketersBe.BookNo = bookNo;

            xmlResult = _objMarketersBal.GetMarketer(_objMarketersBe, ReturnType.Single);
            _objMarketersListBe = _ObjiDsignBAL.DeserializeFromXml<MarketersListBe>(xmlResult);

            _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<MarketersBe>(_objMarketersListBe.Items), ddlMeterReader, "Marketer", "MarketerId", UMS_Resource.DROPDOWN_SELECT, true);
        }

        protected void txtSearchAccount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtSearchAccount.Text = txtSearchAccount.Text.TrimStart();
                txtSearchAccount.Text = txtSearchAccount.Text.Trim();

                divRouteSequenceWise.Visible = false;
                if (!string.IsNullOrEmpty(txtSearchAccount.Text))
                {
                    RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
                    objLedgerBe.AccountNo = txtSearchAccount.Text.Trim();
                    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                    else
                        objLedgerBe.BUID = string.Empty;
                    objLedgerBe = _ObjiDsignBAL.DeserializeFromXml<RptCustomerLedgerBe>(objReportsBal.GetLedger(objLedgerBe, ReturnType.Single));
                    if (objLedgerBe.IsSuccess)
                    {
                        if (objLedgerBe.IsCustExistsInOtherBU)
                        {
                            lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                            mpeAlert.Show();
                        }
                        else
                        {
                            ConsumerBe _objConsumerBe = new ConsumerBe();
                            ConsumerBal _objConsumerBal = new ConsumerBal();

                            _objConsumerBe.AccountNo = txtSearchAccount.Text; //.Trim();--faizid103

                            XmlDocument xml = _objConsumerBal.GetCustomerDeails(_objConsumerBe, ReturnType.Single);
                            _objConsumerBe = _ObjiDsignBAL.DeserializeFromXml<ConsumerBe>(xml);

                            if (_objConsumerBe != null)
                            {
                                if (ddlBU.Items.Count > 1)
                                {
                                    ddlBU.SelectedIndex = ddlBU.Items.IndexOf(ddlBU.Items.FindByValue(_objConsumerBe.BU_ID));
                                    if (!string.IsNullOrEmpty(ddlBU.SelectedValue))
                                        this.ddlBU_SelectedIndexChanged(this, null);
                                }
                                if (ddlSU.Items.Count > 1)
                                {
                                    ddlSU.SelectedIndex = ddlSU.Items.IndexOf(ddlSU.Items.FindByValue(_objConsumerBe.SU_ID));
                                    if (!string.IsNullOrEmpty(ddlSU.SelectedValue))
                                        this.ddlSU_SelectedIndexChanged(this, null);
                                }
                                if (ddlSC.Items.Count > 1)
                                {
                                    ddlSC.SelectedIndex =
                                        ddlSC.Items.IndexOf(ddlSC.Items.FindByValue(_objConsumerBe.ServiceCenterId));
                                    if (!string.IsNullOrEmpty(ddlSC.SelectedValue))
                                        this.ddlSC_SelectedIndexChanged(this, null);
                                }
                                if (ddlCycle.Items.Count > 1)
                                {
                                    ddlCycle.SelectedIndex =
                                        ddlCycle.Items.IndexOf(ddlCycle.Items.FindByValue(_objConsumerBe.CycleId));
                                    if (!string.IsNullOrEmpty(ddlCycle.SelectedValue))
                                        this.ddlCycle_SelectedIndexChanged(this, null);
                                }
                                if (ddlRoutes.Items.Count > 1)
                                {
                                    ddlRoutes.SelectedIndex =
                                        ddlRoutes.Items.IndexOf(ddlRoutes.Items.FindByValue(_objConsumerBe.BookNo));
                                    if (!string.IsNullOrEmpty(ddlRoutes.SelectedValue))
                                        BindMarketersByBookNo(2, string.Empty, ddlRoutes.SelectedValue);
                                }
                                if (ddlMeterReader.Items.Count > 1)
                                {
                                    ddlMeterReader.SelectedIndex =
                                        ddlMeterReader.Items.IndexOf(ddlMeterReader.Items.FindByValue(_objConsumerBe.MarketerId.ToString()));
                                }
                            }
                        }
                    }
                    else
                    {
                        _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.CUSTOMER_NOT_FOUND_MSG,
                            pnlMessage, lblMessage);
                        divAccountWise.Visible = false;//Faiz Id103
                    }
                }
                else
                {
                    ddlSU.SelectedIndex = ddlSC.SelectedIndex = ddlCycle.SelectedIndex = ddlRoutes.SelectedIndex = 0;
                    if (ddlMeterReader.Items.Count > 1)
                        ddlMeterReader.SelectedIndex = 0;
                    txtReadingDate.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        public bool IsReaded(int readCodeId)
        {
            return readCodeId == (int)ReadCode.Direct;
        }

        #region ExportToExcel

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            BillingBE _objBilling = new BillingBE();
            _objBilling.ReadDate = _ObjCommonMethods.Convert_MMDDYY(txtReadingDate.Text);
            if (sender as Button != null)
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
            }
            divAccountWise.Visible = false;
            divRouteSequenceWise.Visible = true;
            switch (rblAccountType.SelectedItem.Value)
            {
                case "2":
                    _objBilling.ReadMethod = "route";
                    litCountriesList.Text = Resource.ROUTE_WISE;
                    break;
                case "3":
                    _objBilling.ReadMethod = "BookNo";
                    litCountriesList.Text = Resource.BOOK_WISE;
                    break;
            }
            _objBilling.BookNo = ddlRoutes.SelectedItem.Value;
            _objBilling.PageNo = Constants.pageNoValue;//Convert.ToInt32(hfPageNo.Value);
            _objBilling.PageSize = Convert.ToInt32(hfTotalRecords.Value);//PageSize;
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                _objBilling.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                _objBilling.BUID = string.Empty;
            GlobalMessagesListBE ObjBillList = _ObjiDsignBAL.DeserializeFromXml<UMS_NigeriaBE.GlobalMessagesListBE>(_objBillingBAL.GetPreviousCustomerReadingsBookwise(_ObjiDsignBAL.DoSerialize<BillingBE>(_objBilling)));

            XlsDocument xls = new XlsDocument();
            Worksheet sheet = xls.Workbook.Worksheets.Add("sheet");
            //state the name of the column headings 
            Cells cells = sheet.Cells;
            int i = 6, j = 1;
            Cell _objCell = null;

            cells.Add(2, 2, "Business Unit");
            cells.Add(2, 5, "Service Unit");
            cells.Add(3, 2, "Service Center");
            cells.Add(3, 5, "Book Group");
            cells.Add(4, 2, "Book Code");

            _objCell = cells.Add(2, 3, ddlBU.SelectedItem.Text);
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell = cells.Add(2, 6, ddlSU.SelectedItem.Text);
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell = cells.Add(3, 3, ddlSC.SelectedItem.Text);
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell = cells.Add(3, 6, ddlCycle.SelectedItem.Text);
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell = cells.Add(4, 3, ddlRoutes.SelectedItem.Text);
            _objCell.Font.Weight = FontWeight.ExtraBold;

            _objCell = cells.Add(i, 1, "S.No");
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            _objCell = cells.Add(i, 2, "Global Account Number");
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            _objCell = cells.Add(i, 3, "Name");
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            //_objCell = cells.Add(i, 4, "Last Bill ReadType");
            //_objCell.Font.Weight = FontWeight.ExtraBold;
            //_objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            _objCell = cells.Add(i, 4, "Meter Number");
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            _objCell = cells.Add(i, 5, "Old Account No");
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            _objCell = cells.Add(i, 6, "Previous Reading");
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            _objCell = cells.Add(i, 7, "Current Reading");
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            _objCell = cells.Add(i, 8, "Usage(kWh)");
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            _objCell = cells.Add(i, 9, "Average Reading");
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            _objCell = cells.Add(i, 10, "Last Reading Date");
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            _objCell = cells.Add(i, 11, "Is Tampered");
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            _objCell = cells.Add(i, 12, "Remarks");
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;

            foreach (BillingBE item in ObjBillList.CustomerReading)
            {
                i++;
                cells.Add(i, 1, j);
                cells.Add(i, 2, item.AccountNo + "-" + item.AccNum);
                cells.Add(i, 3, item.Name);
                //cells.Add(i, 4, item.LastBillReadType);
                cells.Add(i, 4, item.MeterNo + " [" + item.MeterDials + "]");
                cells.Add(i, 5, item.OldAccountNo);
                cells.Add(i, 6, string.IsNullOrEmpty(item.PreviousReading) ? "" : AppendZeros(item.PreviousReading, item.MeterDials));
                cells.Add(i, 7, string.IsNullOrEmpty(item.PresentReading) ? "" : AppendZeros(item.PresentReading, item.MeterDials));
                cells.Add(i, 8, Math.Round(Convert.ToDecimal(item.Usage), item.Decimals).ToString());
                cells.Add(i, 9, item.AverageReading);
                cells.Add(i, 10, item.LatestDate);
                cells.Add(i, 11, (item.IsTamper ? "True" : "False"));

                string remarks = string.Empty;

                if (!item.IsPerformAction)
                {
                    remarks = item.Description;
                }
                else if (item.ApproveStatusId == 2)
                {
                    remarks = "Approval is in process";
                }
                else if (item.IsMeterChangeApproval)
                {
                    remarks = "Meter change request is in process";
                }
                else if (item.IsReadToDirectApproval)
                {
                    remarks = "Meter Disconnect change request is in process";
                }
                else if (item.ApproveStatusId == 3 && item.IsExists)
                {
                    remarks = "Readings already available";
                }
                else if (item.ApproveStatusId == 1 && item.IsExists)
                {
                    remarks = "Readings available for Future Date";
                }
                else if (item.IsMeterChangedDateExists)
                {
                    remarks = "Meter Reading Date should be greater than the Meter Changed Date";
                }
                else if (item.IsMeterAssignedDateExists)
                {
                    remarks = "Meter Reading Date should be greater than the Meter Assigned Date";
                }
                else if (item.Rollover)
                {
                    remarks = "Meter Reset";
                }
                else if (item.IsBilled == 1 && item.IsLatestBill)
                {
                    remarks = "Bill Generated for Future Date";
                }

                cells.Add(i, 12, remarks);
                j++;
            }

            string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
            fileName = ddlRoutes.SelectedItem.Text.ToString().Replace(" ", "") + "-" + fileName;
            string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
            xls.FileName = filePath;
            xls.Save();
            _ObjiDsignBAL.DownLoadFile(filePath, "application//vnd.ms-excel");
        }

        #endregion

        protected void Button2_Click(object sender, EventArgs e)
        {
            ModalPopupExtenderGo.Show();
        }
    }
}