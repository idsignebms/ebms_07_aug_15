﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/EBMS.Master" AutoEventWireup="true"
    Title="::Meter Reading Upload::" Theme="Green" CodeBehind="MeterReadingDocumentPriview.aspx.cs"
    Inherits="UMS_Nigeria.Billing.MeterReadingDocumentPriview" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdateProgress ID="UpdateProgress" runat="server">
                <ProgressTemplate>
                    <center>
                        <div id="loading-div" class="pbloading">
                            <div id="title-loading" class="pbtitle">
                                BEDC</div>
                            <center>
                                <img src="../images/loading.GIF" class="pbimg"></center>
                            <p class="pbtxt">
                                Loading Please wait.....</p>
                        </div>
                    </center>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
                PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
            <asp:HiddenField runat="server" ID="hfValidate" />
            <asp:HiddenField runat="server" ID="hfTotalValue" />
            <asp:Panel ID="pnlMessage" runat="server">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
            <div class="clear pad_10">
            </div>
            <div>
                <a href="MeterReadingUpload.aspx" style="font-weight: bold;">&lt;&lt; Back To Upload
                </a>
            </div>
            <div class="grid_tb" id="divPrint" runat="server" align="center" style="overflow-x: auto;">
                <asp:GridView ID="gvDocPrevList" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="grid_tb_hd"
                    OnRowDataBound="gvDocPrevList_RowDataBound">
                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                    <EmptyDataTemplate>
                        NO Records found.
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                            HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NUMBER%>" runat="server"
                            HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblGlobalAccountNo" runat="server" Text='<%#Eval("AccNum")%>' Visible="false"></asp:Label>
                                <asp:Label ID="lblAccountNo" runat="server" class="lblgvAccountNos" Text='<%#Eval("AccNum")+" - "+ Eval("AccountNo")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Old Account Number" runat="server" HeaderStyle-Width="10%"
                            HeaderStyle-HorizontalAlign="Center">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblOldAccountNo" runat="server" class="lblgvAccountNos" Text='<%#Eval("OldAccountNo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$ Resources:Resource, CUSTOMER_NAME%>" runat="server"
                            HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblCustomerDetails" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$ Resources:Resource, METER_NUMBER%>" runat="server"
                            HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblMeterNo" runat="server" Text='<%#Eval("MeterNo") + "["+Eval("MeterDials")+"]" %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$ Resources:Resource, READING_DATE%>" runat="server"
                            HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblReadDate" runat="server" Text='<%#Eval("ReadDate") %>'></asp:Label>
                                <asp:HiddenField ID="hfIsGreaterDate" runat="server" Value='<%#Eval("IsExists") %>' />
                                <asp:HiddenField ID="hfReadCode" runat="server" Value='<%#Eval("ReadCodeId") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Last Read Date" runat="server" HeaderStyle-Width="10%"
                            HeaderStyle-HorizontalAlign="Center">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblLastReadDate" runat="server" Text='<%#Eval("LatestDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$ Resources:Resource, AVERAGE_READING%>" runat="server"
                            HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblGvAverageReading" runat="server" Text='<%#Eval("AverageReading") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$ Resources:Resource, PREVIOUS_READING%>" runat="server"
                            HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblGvPreviousReading" runat="server" Text='<%#Eval("PreviousReading") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$ Resources:Resource, CURRENT_READING%>" runat="server"
                            HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:UpdatePanel runat="server" ID="UpId" ChildrenAsTriggers="true">
                                    <ContentTemplate>
                                        <asp:Label ID="lblGvCurrentReading" runat="server" Text='<%#Eval("PresentReading") %>'></asp:Label>
                                        <asp:Label ID="lblItemDials" runat="server" Text='<%#Eval("MeterDials") %>' Style="display: none;"></asp:Label>
                                        <asp:Label ID="Label1" runat="server" Text='<%#Eval("Decimals") %>' Style="display: none;"></asp:Label>
                                        <asp:Label ID="lblMultiplier" runat="server" Text='<%#Eval("ReadingMultiplier") %>'
                                            Style="display: none;"></asp:Label>
                                        <asp:Label ID="lblGvExist" runat="server" Text='<%#Eval("PrvExist") %>' Style="display: none;"></asp:Label>
                                        <asp:Label ID="lblGvCountReading" runat="server" class="lblgvReadingsCount" Text='<%#Eval("TotalReadings") %>'
                                            Style="display: none;"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$ Resources:Resource, USAGE_DETAILS%>" runat="server"
                            HeaderStyle-HorizontalAlign="Center">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblGvUsage" class="lblgvUsage" runat="server" Text='<%#Eval("Usage") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remarks" runat="server" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblGvRemarks" class="lblgvUsage" runat="server" Text=""></asp:Label>
                                <asp:HiddenField ID="hfBU_ID" runat="server" Value='<%#Eval("BU_ID") %>' />
                                <asp:HiddenField ID="hfIsBilled" runat="server" Value='<%#Eval("IsBilled") %>' />
                                <asp:HiddenField ID="hfIsDuplicate" runat="server" Value='<%#Eval("IsDuplicate") %>' />
                                <asp:HiddenField ID="hfReadBy" runat="server" Value='<%#Eval("MarketerId") %>' />
                                <asp:HiddenField ID="hfCustActiveStatusId" runat="server" Value='<%#Eval("ActiveStatusId") %>' />
                                <asp:HiddenField ID="hfActiveStatus" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <div class="clear pad_10">
            </div>
            <div style="margin-left: 557px;">
                <div class=" fltl">
                    <asp:Button ID="btnNext" CssClass="box_s" Text="<%$ Resources:Resource, SAVE%>" runat="server" />
                </div>
            </div>
            <asp:ModalPopupExtender ID="mpeSaveConfirm" runat="server" PopupControlID="pnlSaveConfirm"
                TargetControlID="btnNext" BackgroundCssClass="modalBackground" CancelControlID="btnSaveCancel">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlSaveConfirm" runat="server" CssClass="modalPopup" Style="display: none;"
                class="poppnl">
                <div class="popheader popheaderlblred" id="pop" runat="server">
                    <asp:Label ID="lblEstimatedBillMessage" runat="server" Text="Are you sure, do you want to save these Details ?"></asp:Label>
                </div>
                <div class="footer popfooterbtnrt">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="btnConfirm" runat="server" Text="<%$ Resources:Resource, CONFIRM%>"
                            OnClick="btnNext_Click" CssClass="ok_btn" />
                        <asp:Button ID="btnSaveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                            CssClass="cancel_btn" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="mpeCountrypopup" runat="server" TargetControlID="btnpopup"
                CancelControlID="hfdummy" PopupControlID="pnlConfirmation" BackgroundCssClass="popbg">
            </asp:ModalPopupExtender>
            <asp:Button ID="btnpopup" runat="server" Text="" Style="display: none;" />
            <asp:Panel runat="server" ID="pnlConfirmation" CssClass="modalPopup" DefaultButton="btnOk"
                Style="display: none; border-color: #639B00;">
                <%-- <div class="popheader" style="background-color: #EEB72D;">--%>
                <div class="popheader" style="background-color: #639B00;">
                    <asp:HiddenField ID="hfdummy" runat="server" />
                    <asp:Label ID="lblMsgPopup" runat="server" Text=""></asp:Label>
                </div>
                <br />
                <div class="footer" align="right">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="btnOk" runat="server" Text="<%$Resources:Resource, OK%>" CssClass="btn_ok"
                            OnClick="btnOk_Click" Style="margin-left: 105px;" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).ready(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });

    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../scripts/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
