﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.IO;
using UMS_NigeriaBE;
using Resources;
using iDSignHelper;
using UMS_NigeriaBAL;
using System.Xml;
using Resources;
using ClosedXML.Excel;
using UMS_NigriaDAL;
using System.Drawing;
using System.Globalization;

namespace UMS_Nigeria.Billing
{
    public partial class MeterReadingDocumentPriview : System.Web.UI.Page
    {
        #region Members

        CommonMethods objCommonMethods = new CommonMethods();
        iDsignBAL objIdsignBal = new iDsignBAL();
        BillReadingsBAL objBillReadingBAL = new BillReadingsBAL();
        string Key = UMS_Resource.KEY_METERREADING_DOCUMENTUPLOAD;

        string Filename;

        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["MeterReadingUploadDT"] != null)
                { 
                    //Filename = objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.METER_READING_QST_DOCMUNET].ToString());
                    //DataTable dt = new DataTable();
                    //dt = objCommonMethods.ExcelImport(Server.MapPath(ConfigurationManager.AppSettings["MeterReading"].ToString()) + Filename);
                    DataSet ds = new DataSet();
                    ds = (DataSet)Session["MeterReadingUploadDT"];
                    //ds = objIdsignBal.ConvertListToDataSet<BillingBE>(objIdsignBal.DeserializeFromXml<GlobalMessagesListBE>(objBillReadingBAL.GetFulInfoXL(ds.Tables[0])).CustomerReading);
                    ds = objBillReadingBAL.GetValidateReadingsUpload(ds.Tables[0]);

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        int TotalDails = Convert.ToInt32(string.IsNullOrEmpty(dr["MeterDials"].ToString()) ? 0 : Convert.ToInt32(dr["MeterDials"].ToString()));

                        if (dr["PreviousReading"].ToString().Length < TotalDails && !string.IsNullOrEmpty(dr["PreviousReading"].ToString()))
                        {
                            dr["PreviousReading"] = AddDails(TotalDails, dr["PreviousReading"].ToString());
                        }
                        if (dr["PresentReading"].ToString().Length < TotalDails && !string.IsNullOrEmpty(dr["PresentReading"].ToString()))
                        {
                            dr["PresentReading"] = AddDails(TotalDails, dr["PresentReading"].ToString());
                        }
                    }

                    gvDocPrevList.DataSource = ds;
                    gvDocPrevList.DataBind();
                }
                else
                {
                    Response.Redirect(UMS_Resource.UPLOAD_REDIRECT, false);
                }
            }
        }

        #region Events

        protected void gvDocPrevList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblCustomerDetails = (Label)e.Row.FindControl("lblCustomerDetails");
                //Label lblPrev = (Label)e.Row.FindControl("lblGvExist");
                Label lblDials = (Label)e.Row.FindControl("lblItemDials");
                Label lblPre = (Label)e.Row.FindControl("lblGvPreviousReading");
                Label lblusg = (Label)e.Row.FindControl("lblGvUsage");
                Label lblGvCurrentReading = (Label)e.Row.FindControl("lblGvCurrentReading");
                Label lblAvg = (Label)e.Row.FindControl("lblGvAverageReading");
                Label lblCnt = (Label)e.Row.FindControl("lblGvCountReading");
                Label lblRemarks = (Label)e.Row.FindControl("lblGvRemarks");
                HiddenField hfActiveStatus = (HiddenField)e.Row.FindControl("hfActiveStatus");
                Label lblReadDate = (Label)e.Row.FindControl("lblReadDate");
                Label lblLastReadDate = (Label)e.Row.FindControl("lblLastReadDate");
                HiddenField hfIsGreaterDate = (HiddenField)e.Row.FindControl("hfIsGreaterDate");
                HiddenField hfReadCode = (HiddenField)e.Row.FindControl("hfReadCode");
                HiddenField hfCustActiveStatusId = (HiddenField)e.Row.FindControl("hfCustActiveStatusId");
                HiddenField hfBU_ID = (HiddenField)e.Row.FindControl("hfBU_ID");
                HiddenField hfIsDuplicate = (HiddenField)e.Row.FindControl("hfIsDuplicate");
                HiddenField hfIsBilled = (HiddenField)e.Row.FindControl("hfIsBilled");

                if (!string.IsNullOrEmpty(lblReadDate.Text))
                {
                    bool isCheck = false;
                    //string receivedDate = lblReadDate.Text;
                    //string format = "mm/dd/yyyy";
                    //CultureInfo provider = CultureInfo.InvariantCulture;
                    //DateTime result = DateTime.ParseExact(receivedDate, format, provider);
                    DateTime result = Convert.ToDateTime(lblReadDate.Text);
                    //DateTime lastReadDate = Convert.ToDateTime(lblLastReadDate.Text);

                    if (string.IsNullOrEmpty(lblCustomerDetails.Text.Trim()))
                    {
                        lblRemarks.Text = Resource.INVALID_ACCOUNTNO;
                        lblRemarks.ForeColor = Color.White;
                        hfActiveStatus.Value = false.ToString();
                        e.Row.BackColor = Color.Tomato;
                        isCheck = true;
                    }
                    else if (Session[UMS_Resource.SESSION_USER_BUID] != null && hfBU_ID.Value != Convert.ToString(Session[UMS_Resource.SESSION_USER_BUID]))
                    {
                        lblRemarks.Text += "This customer is in another BU";
                        lblRemarks.ForeColor = Color.White;
                        hfActiveStatus.Value = false.ToString();
                        e.Row.BackColor = Color.Tomato;
                        isCheck = true;
                    }
                    else if (!string.IsNullOrEmpty(hfIsDuplicate.Value) && hfIsDuplicate.Value.ToLower() == "1")
                    {
                        lblRemarks.Text = Resource.DUPLICATE_ACCOUNT;
                        lblRemarks.ForeColor = Color.White;
                        hfActiveStatus.Value = false.ToString();
                        e.Row.BackColor = Color.Tomato;
                        isCheck = true;
                    }
                    else if (hfReadCode.Value == "1")
                    {
                        lblRemarks.Text = "Direct Customer";
                        lblRemarks.ForeColor = Color.White;
                        hfActiveStatus.Value = false.ToString();
                        e.Row.BackColor = Color.Tomato;
                        isCheck = true;
                    }
                    else if (hfCustActiveStatusId.Value == "2")
                    {
                        lblRemarks.Text = Resource.INACTIVE_CUST;
                        lblRemarks.ForeColor = Color.White;
                        hfActiveStatus.Value = false.ToString();
                        e.Row.BackColor = Color.Tomato;
                        isCheck = true;
                    }
                    else if (hfCustActiveStatusId.Value == "3")
                    {
                        lblRemarks.Text = "Hold";
                        lblRemarks.ForeColor = Color.White;
                        hfActiveStatus.Value = false.ToString();
                        e.Row.BackColor = Color.Tomato;
                        isCheck = true;
                    }
                    else if (hfCustActiveStatusId.Value == "4")
                    {
                        lblRemarks.Text = "Closed";
                        lblRemarks.ForeColor = Color.White;
                        hfActiveStatus.Value = false.ToString();
                        e.Row.BackColor = Color.Tomato;
                        isCheck = true;
                    }
                    else if (result > DateTime.Now)
                    {
                        lblRemarks.Text = "Future Date Not Applicable";
                        lblRemarks.ForeColor = Color.White;
                        hfActiveStatus.Value = false.ToString();
                        e.Row.BackColor = Color.Tomato;
                        isCheck = true;
                    }
                    else if (!string.IsNullOrEmpty(hfIsGreaterDate.Value) && hfIsGreaterDate.Value.ToLower() == "1")
                    {
                        lblRemarks.Text = "Already Readings Available";
                        lblRemarks.ForeColor = Color.White;
                        hfActiveStatus.Value = false.ToString();
                        e.Row.BackColor = Color.Tomato;
                        isCheck = true;
                    }
                    else if (!string.IsNullOrEmpty(hfIsBilled.Value) && hfIsBilled.Value.ToLower() == "1")
                    {
                        lblRemarks.Text = "Already Billed";
                        lblRemarks.ForeColor = Color.White;
                        hfActiveStatus.Value = false.ToString();
                        e.Row.BackColor = Color.Tomato;
                        isCheck = true;
                    }

                    if (!isCheck)
                    {
                        decimal currentReading, previousReading = 0;

                        if (objCommonMethods.IsNumeric(lblGvCurrentReading.Text))
                        {
                            currentReading = string.IsNullOrEmpty(lblGvCurrentReading.Text) ? 0 : Convert.ToInt32(lblGvCurrentReading.Text);
                            previousReading = string.IsNullOrEmpty(lblPre.Text) ? 0 : Convert.ToDecimal(lblPre.Text);

                            if (lblGvCurrentReading.Text.Length == Convert.ToInt32(lblDials.Text) && currentReading >= previousReading)
                            {
                                lblRemarks.ForeColor = Color.Black;
                                hfActiveStatus.Value = true.ToString();
                                //bool isReset = false;
                                //if (lblPre.Text.Substring(0, 1) == "9" && lblGvCurrentReading.Text.Substring(0, 1) == "0")
                                //{
                                //    string StrValue = objCommonMethods.GetAllNines(Convert.ToInt32(lblDials.Text));
                                //    lblusg.Text = ((Convert.ToDecimal(StrValue) - Convert.ToDecimal(lblPre.Text)) + Convert.ToDecimal(lblGvCurrentReading.Text) + 1).ToString();
                                //    isReset = true;
                                //}
                                //else
                                //{
                                lblusg.Text = (Convert.ToDecimal(lblGvCurrentReading.Text) - Convert.ToDecimal(lblPre.Text)).ToString();
                                //}
                                if (Convert.ToDecimal(lblusg.Text) >= 0)
                                {
                                    decimal NewAvgUsage = (((Convert.ToDecimal(lblAvg.Text) * Convert.ToDecimal(lblCnt.Text)) + Convert.ToDecimal(lblusg.Text)) / (Convert.ToDecimal(lblCnt.Text) + 1));

                                    if (Convert.ToDecimal(lblAvg.Text) > 0)
                                    {
                                        if (objCommonMethods.IsHugeAmount(NewAvgUsage, Convert.ToDecimal(lblAvg.Text)))
                                        {
                                            lblRemarks.Text = Resource.HUGE_MILD;
                                        }
                                        else
                                        {
                                            lblRemarks.Text = Resource.VALID;
                                            e.Row.BackColor = Color.White;
                                        }
                                    }
                                    else
                                    {
                                        lblRemarks.Text = Resource.VALID;
                                        e.Row.BackColor = Color.White;
                                    }
                                }
                                else
                                {
                                    lblRemarks.Text = "Invalid Usage";
                                    lblRemarks.ForeColor = Color.White;
                                    hfActiveStatus.Value = false.ToString();
                                    e.Row.BackColor = Color.Tomato;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(lblGvCurrentReading.Text))
                                {
                                    decimal prevReading = string.IsNullOrEmpty(lblPre.Text) ? 0 : Convert.ToDecimal(lblPre.Text);
                                    lblusg.Text = (currentReading - prevReading).ToString();
                                }
                                else lblusg.Text = string.Empty;

                                if (lblGvCurrentReading.Text.Length > 0 && lblGvCurrentReading.Text.Length != Convert.ToInt32(lblDials.Text))
                                    lblRemarks.Text = Resource.INVALID_DIALS;
                                else
                                    lblRemarks.Text = Resource.INVALID;
                                lblRemarks.ForeColor = Color.White;
                                hfActiveStatus.Value = false.ToString();
                                e.Row.BackColor = Color.Tomato;
                            }
                        }
                        else
                        {
                            lblRemarks.Text = Resource.INVALID;
                            lblRemarks.ForeColor = Color.White;
                            hfActiveStatus.Value = false.ToString();
                            e.Row.BackColor = Color.Tomato;
                        }
                    }
                    //else
                    //{
                    //    lblRemarks.Text = Resource.INVALID;
                    //    lblRemarks.ForeColor = Color.White;
                    //    hfActiveStatus.Value = false.ToString();
                    //    e.Row.BackColor = Color.Tomato;
                    //}
                }
                else
                {
                    lblRemarks.Text = Resource.INVALID_DATE;
                    lblRemarks.ForeColor = Color.White;
                    hfActiveStatus.Value = false.ToString();
                    e.Row.BackColor = Color.Tomato;
                }
                if (!string.IsNullOrEmpty(lblusg.Text))
                {
                    lblusg.Text = Math.Round(Convert.ToDecimal(lblusg.Text)).ToString();
                }
            }
        }

        private string AddDails(int TotalDails, string reading)
        {
            string result = reading;
            for (int d = 0; d < (TotalDails - reading.Length); d++)
            {
                result = "0" + result;
            }
            return result;
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            UploadMeterReadings();
        }

        private DataTable CreateMeterReadingsDT(DataTable dt)
        {
            dt.Columns.Add("SNo", typeof(int));
            dt.Columns.Add("AccNum", typeof(string));
            dt.Columns.Add("TotalReadings", typeof(int));
            dt.Columns.Add("AverageReading", typeof(string));
            dt.Columns.Add("PresentReading", typeof(string));
            dt.Columns.Add("Usage", typeof(string));
            dt.Columns.Add("IsTamper", typeof(bool));
            dt.Columns.Add("PreviousReading", typeof(string));
            dt.Columns.Add("IsExists", typeof(bool));
            dt.Columns.Add("Multiplier", typeof(int));
            dt.Columns.Add("ReadDate", typeof(string));
            dt.Columns.Add("ReadBy", typeof(string));
            return dt;
        }

        private DataTable UpdateMeterReadingsDT(DataTable dt, string accNum, int totalReadings, string averageReading, string presentReading, string usage, bool isTamper, string previousReading, bool isExists, int Multiplier, string ReadDate, string ReadBy)
        {
            DataRow dr = dt.NewRow();
            dr["SNo"] = dt.Rows.Count + 1;
            dr["AccNum"] = accNum;
            dr["TotalReadings"] = totalReadings;
            dr["AverageReading"] = averageReading;
            dr["PresentReading"] = presentReading;
            dr["Usage"] = usage;
            dr["IsTamper"] = isTamper;
            dr["PreviousReading"] = previousReading;
            dr["IsExists"] = isExists;
            dr["Multiplier"] = Multiplier;
            dr["ReadDate"] = ReadDate;
            dr["ReadBy"] = ReadBy;
            dt.Rows.Add(dr);
            return dt;
        }

        private void UploadMeterReadings()
        {
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            dt = CreateMeterReadingsDT(dt);
            bool isSuccess = false;
            //BillingBE _objBilling = new BillingBE();
            //_objBilling.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            //_objBilling.MeterReadingFrom = (int)MeterReadingFrom.BulkUpload;

            foreach (GridViewRow row in gvDocPrevList.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lblGlobalAccountNo = (Label)row.FindControl("lblGlobalAccountNo");
                    Label lblPrev = (Label)row.FindControl("lblGvExist");
                    Label lblDials = (Label)row.FindControl("lblItemDials");
                    Label lblPre = (Label)row.FindControl("lblGvPreviousReading");
                    Label lblusg = (Label)row.FindControl("lblGvUsage");
                    Label lblGvCurrentReading = (Label)row.FindControl("lblGvCurrentReading");
                    Label lblAvg = (Label)row.FindControl("lblGvAverageReading");
                    Label lblCnt = (Label)row.FindControl("lblGvCountReading");
                    Label lblRemarks = (Label)row.FindControl("lblGvRemarks");
                    HiddenField hfActiveStatus = (HiddenField)row.FindControl("hfActiveStatus");
                    HiddenField hfReadBy = (HiddenField)row.FindControl("hfReadBy");
                    HiddenField hfIsGreaterDate = (HiddenField)row.FindControl("hfIsGreaterDate");
                    Label lblReadDate = (Label)row.FindControl("lblReadDate");
                    Label lblMultiplier = (Label)row.FindControl("lblMultiplier");


                    if (hfActiveStatus.Value == true.ToString())
                    {
                        bool isExist = false;
                        if (lblPrev.Text.ToLower() == "1")
                        {
                            isExist = true;
                            //GridUpdateReading(lblPre.Text, lblGvCurrentReading.Text, lblusg.Text, lblAvg.Text, lblCnt.Text, lblGlobalAccountNo.Text, Convert.ToDateTime(lblReadDate.Text).ToString("MM/dd/yyyy"), hfReadBy.Value);
                            //count++;
                        }
                        int multiplier = string.IsNullOrEmpty(lblMultiplier.Text) ? 0 : Convert.ToInt32(lblMultiplier.Text);
                        dt = UpdateMeterReadingsDT(dt, lblGlobalAccountNo.Text, 0, lblAvg.Text, lblGvCurrentReading.Text, lblusg.Text, false, lblPre.Text, isExist, multiplier, Convert.ToDateTime(lblReadDate.Text).ToString("MM/dd/yyyy"), hfReadBy.Value);
                    }
                    //else if (lblPrev.Text.ToLower() == "0" && hfActiveStatus.Value == true.ToString())
                    //{
                    //    BillingBE _objBilling = new BillingBE();
                    //    _objBilling.ReadDate = Convert.ToDateTime(lblReadDate.Text).ToString("MM/dd/yyyy");
                    //    _objBilling.ReadBy = hfReadBy.Value;
                    //    _objBilling.PreviousReading = lblPre.Text;
                    //    _objBilling.PresentReading = lblGvCurrentReading.Text;
                    //    _objBilling.Usage = lblusg.Text;
                    //    _objBilling.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    //    _objBilling.AccNum = .Text;
                    //    _objBilling.MeterReadingFrom = (ilblGlobalAccountNont)MeterReadingFrom.BulkUpload;
                    //    _objBilling = objIdsignBal.DeserializeFromXml<BillingBE>(objBillReadingBAL.InsertCustomerBillReadingBAL(objIdsignBal.DoSerialize<BillingBE>(_objBilling)));
                    //    if (_objBilling.IsSuccess == 1)
                    //    {
                    //        count++;
                    //    }
                    //}
                }
            }
            if (dt.Rows.Count > 0)
            {
                ds.Tables.Add(dt);
                //_objBilling = objIdsignBal.DeserializeFromXml<BillingBE>(objBillReadingBAL.InsertBulkReadings(_objBilling, dt, Statement.Insert));
                //isSuccess = _objBilling.IsSuccess;

                isSuccess = objBillReadingBAL.InsertMeterReadingsDT(Session[UMS_Resource.SESSION_LOGINID].ToString(), (int)MeterReadingFrom.BulkUpload, dt);
            }
            if (isSuccess)
            {
                lblMsgPopup.Text = "Readings saved successfully.";
                mpeCountrypopup.Show();
            }
            else
            {
                objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, "Meter readings failed to save.", pnlMessage, lblMessage);
            }
        }

        //public void GridUpdateReading(string Previous, string Current, string Usage, string Avg, string TotalRead, string AccNum, int Multiplier, string readDate, string ReadBy)
        //{
        //    BillingBE _objBilling = new BillingBE();
        //    _objBilling.ReadDate = readDate;
        //    _objBilling.ReadBy = ReadBy;
        //    _objBilling.PreviousReading = Previous;
        //    _objBilling.PresentReading = Current;
        //    _objBilling.Usage = Usage;
        //    _objBilling.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
        //    _objBilling.AverageReading = Avg;
        //    _objBilling.TotalReadings = TotalRead;
        //    _objBilling.AccNum = AccNum;
        //    _objBilling.MeterReadingFrom = (int)MeterReadingFrom.BulkUpload;
        //    _objBilling = objIdsignBal.DeserializeFromXml<BillingBE>(objBillReadingBAL.UpdateCustomerBillReadingBAL(objIdsignBal.DoSerialize<BillingBE>(_objBilling)));
        //}

        protected void btnOk_Click(object sender, EventArgs e)
        {
            Response.Redirect(Constants.MeterReadingUpload, false);
        }

        #endregion
    }
}