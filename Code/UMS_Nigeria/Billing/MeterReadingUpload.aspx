﻿<%@ Page Title="::Meter Reading Upload::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    AutoEventWireup="true" CodeBehind="MeterReadingUpload.aspx.cs" Theme="Green"
    Inherits="UMS_Nigeria.Billing.MeterReadingUpload" %>

<%@ Register Src="../UserControls/Authentication.ascx" TagName="Authentication" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdateProgress ID="UpdateProgress" runat="server">
                <ProgressTemplate>
                    <center>
                        <div id="loading-div" class="pbloading">
                            <div id="title-loading" class="pbtitle">
                                BEDC</div>
                            <center>
                                <img src="../images/loading.GIF" class="pbimg"></center>
                            <p class="pbtxt">
                                Loading Please wait.....</p>
                        </div>
                    </center>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
                PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
            <asp:UpdatePanel ID="upPaymentUpload" runat="server" ChildrenAsTriggers="true">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="consumer_total">
                <div class="pad_5">
                </div>
                <div class="text_total">
                    <div class="text-heading">
                        <asp:Literal ID="litReadMethod" runat="server" Text="<%$ Resources:Resource, METERREADING_UPLOADING%>"></asp:Literal>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div class="dot-line">
                </div>
            </div>
            <%-- <div class="consumer_feild">
            <div class="consume_name">
                <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, NOTES%>"></asp:Literal>
            </div>
            <div class="consume_input c_left">
                <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>--%>
            <div class="clear pad_10">
            </div>
            <div class="consumer_feild">
                <div class="consume_name" style="width: 200px;">
                    <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, UPLOAD_DOCUMENT%>"></asp:Literal>
                    <span class="span_star">*</span>
                </div>
                <div class="consume_input c_left" style="width:329px;">
                    <asp:FileUpload ID="upDOCExcel" runat="server" />
                </div>  
                <div class="pad_10">
                    <a href="../Uploads/MeterReadingDocuments/MeterReadingSample.zip" target="_blank">
                        Download Sample Document</a>
                </div>
                <div class="clear pad_10">
                </div>
                <div style="color: #939393; font-size: 12px; margin-left: 14px;">
                    <b class="spancolor">Note :</b> <span>
                        <br />
                        1. Please provide Read_Date format as <b>"mm/dd/yyyy"</b> only in uploaded Excel.
                        <br />
                        2. Maximum records limit is <b>1000</b> per excel file. </span>
                </div>
                <div class="clear pad_10">
                </div>
                <div style="margin-left: 199px;">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class=" fltl">
                                <asp:Button ID="btnNext" CssClass="box_s" Text="NEXT"
                                    runat="server" OnClick="btnNext_Click" OnClientClick="return Validate();" />
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnNext" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
            <asp:ModalPopupExtender ID="mpePaymentypopup" runat="server" TargetControlID="btnpopup"
                PopupControlID="pnlConfirmation" BackgroundCssClass="modalBackground" CancelControlID="btnOk">
            </asp:ModalPopupExtender>
            <asp:Button ID="btnpopup" runat="server" Text="" Style="display: none;" />
            <asp:Panel runat="server" BorderColor="#CD3333" ID="pnlConfirmation" CssClass="modalPopup">
                <div class="popheader" style="background-color: #CD3333;">
                    <asp:Panel ID="pnlMsgPopup" runat="server" align="center">
                        <asp:Label ID="lblMsgPopup" runat="server"></asp:Label>
                    </asp:Panel>
                </div>
                <div class="clear pad_10">
                </div>
                <div class="footer" align="right">
                    <asp:Button ID="btnOk" runat="server" Text="OK" CssClass="btn_ok" Style="margin-left: 200px;" />
                    <br />
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="ModalPopupExtenderLogin" runat="server" PopupControlID="PanelLogin"
                TargetControlID="hfpopUp" BackgroundCssClass="modalPopup" CancelControlID="ButtonL">
            </asp:ModalPopupExtender>
            <asp:HiddenField ID="hfpopUp" runat="server" />
            <asp:Panel ID="PanelLogin" DefaultButton="btnAuthentication" runat="server" CssClass="modalPopup"
                Style="display: none; border-color: #639B00;">
                <div class="popheader" style="background-color: #639B00;">
                    <asp:Label ID="Label1" runat="server" Text="Authentication Required"></asp:Label>
                </div>
                <div class="body" style="padding: 30px;">
                    <asp:Label ID="Label2" runat="server" Text="Retype Your Password"></asp:Label>
                    <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" placeholder="Enter Password"
                        autocomplete="off" onblur="return TextBoxBlurValidationlbl(this,'spanAuthentication','Password')"></asp:TextBox>
                    <span id="spanAuthentication" runat="server" class="spancolor"></span>
                    <br />
                    <asp:Label ID="LabelStatusLogin" runat="server" class="spancolor" Visible="false"></asp:Label>
                </div>
                <div class="footer" align="right">
                    <div class="fltl Nothanks">
                        <a href="../Home.aspx">No Thanks, Go To Home</a></div>
                    <div class="fltl" style="margin-top: 5px;">
                        <asp:Button ID="btnAuthentication" runat="server" Text="<%$ Resources:UMS_Resource, CONTINUE%>"
                            CssClass="continue" OnClick="btnAuthenticarion_Click" />
                        <asp:Button ID="ButtonL" runat="server" Text="<%$ Resources:Resource, CANCEL%>" CssClass="nobt" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
    <uc1:Authentication ID="ucAuthentication" runat="server" />
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/MeterReadingUpload.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
