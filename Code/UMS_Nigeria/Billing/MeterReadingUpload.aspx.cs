﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using System.Xml;
using System.Data;
using System.Configuration;
using Resources;
using System.IO;
using UMS_NigriaDAL;
using System.Globalization;
using System.Data.OleDb;
using System.Collections;

namespace UMS_Nigeria.Billing
{
    public partial class MeterReadingUpload : System.Web.UI.Page
    {
        #region Members

        CommonMethods objCommonMethods = new CommonMethods();
        iDsignBAL objIdsignBal = new iDsignBAL();
        UserManagementBAL objUserManagementBAL = new UserManagementBAL();
        BillReadingsBAL objBillingBAL = new BillReadingsBAL();

        string Key = UMS_Resource.KEY_METERREADING_DOCUMENTUPLOAD;

        string Filename = string.Empty;
        DataTable dtExcel = new DataTable();
        DataTable dtXMl = new DataTable();

        #endregion

        #region Properties

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["MeterReadingUploadDT"] != null)
                    Session.Remove("MeterReadingUploadDT");
                string path = string.Empty;
                path = objCommonMethods.GetPagePath(this.Request.Url);
                if (objCommonMethods.IsAccess(path))
                {
                }
                else
                {
                    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE, false);
                }
            }
        }

        #region OldCodestart for Next btn
        //protected void btnNext_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        int count = 0;
        //        bool isValid = true;
        //        string ExcelModifiedFileName = InsersertExcel();

        //        string filePath = Server.MapPath(ConfigurationManager.AppSettings["MeterReading"].ToString()) + ExcelModifiedFileName;

        //        var workbook = new XLWorkbook(filePath);
        //        var xlWorksheet = workbook.Worksheet(1);
        //        var range = xlWorksheet.Range(xlWorksheet.FirstCellUsed(), xlWorksheet.LastCellUsed());

        //        int col = range.ColumnCount();
        //        int row = range.RowCount();
        //        DataSet ds = new DataSet();
        //        ds.ReadXml(Server.MapPath(ConfigurationSettings.AppSettings["MeterReadingUploadHeaders"]));

        //        if (ds.Tables[0].Rows.Count == col)
        //        {
        //            for (int i = 1; i <= col; i++)
        //            {
        //                IXLCell column = xlWorksheet.Cell(1, i);
        //                var r = (from x in ds.Tables[0].AsEnumerable() where x.Field<string>("Sno") == i.ToString() select x.Field<string>("Names")).Single();
        //                if (r.ToString() != column.Value.ToString())
        //                {
        //                    isValid = false;
        //                    break;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            isValid = false;
        //        }

        //        if ((xlWorksheet.FirstCellUsed() == null && xlWorksheet.LastCellUsed() == null) || !isValid)
        //        {
        //            File.Delete(Server.MapPath(ConfigurationManager.AppSettings["MeterReading"].ToString()) + Filename);
        //            lblMsgPopup.Text = UMS_Resource.UPLOADEDFILE_FORMAT;
        //            mpePaymentypopup.Show();
        //        }
        //        else
        //        {
        //            dtExcel = objCommonMethods.ExcelImport(filePath);

        //            if (dtExcel.Rows.Count > 0)
        //            {
        //                for (int i = 0; i < dtExcel.Columns.Count; i++)
        //                {
        //                    DataSet dsHeaders = new DataSet();
        //                    dsHeaders.ReadXml(Server.MapPath(ConfigurationSettings.AppSettings["MeterReadingUploadHeaders"]));
        //                    string HeaderName = dtExcel.Columns[i].ColumnName;
        //                    var result = (from x in dsHeaders.Tables[0].AsEnumerable()
        //                                  where x.Field<string>("Names") == HeaderName.Trim()
        //                                  select new
        //                                  {
        //                                      Names = x.Field<string>("Names")
        //                                  }).SingleOrDefault();
        //                    if (result == null)
        //                    {
        //                        count++;
        //                        File.Delete(Server.MapPath(ConfigurationManager.AppSettings["MeterReading"].ToString()) + Filename);
        //                        lblMsgPopup.Text = UMS_Resource.UPLOADEDFILE_FORMAT;
        //                        mpePaymentypopup.Show();
        //                        break;
        //                    }
        //                }

        //                int totalRows = 1000;

        //                if (totalRows >= dtExcel.Rows.Count)
        //                {
        //                    modalPopupLoading.Show();
        //                    totalRows = dtExcel.Rows.Count;

        //                    for (int i = 0; i < totalRows; i++)
        //                    {
        //                        try
        //                        {
        //                            //string receivedDate = dtExcel.Rows[i]["Read_Date"].ToString();
        //                            //CultureInfo provider = CultureInfo.InvariantCulture;
        //                            //DateTime result = Convert.ToDateTime(receivedDate);

        //                            string receivedDate = dtExcel.Rows[i]["Read_Date"].ToString();
        //                            string format = "d";
        //                            CultureInfo provider = CultureInfo.InvariantCulture;
        //                            DateTime result = DateTime.ParseExact(receivedDate, format, provider);

        //                            if (result.Year < Constants.StartValidYear)
        //                            {
        //                                count++;
        //                                File.Delete(Server.MapPath(ConfigurationManager.AppSettings["MeterReading"].ToString()) + Filename);
        //                                lblMsgPopup.Text = Resource.LESSTHAN_1900_DATE_READINGS;
        //                                mpePaymentypopup.Show();
        //                                break;
        //                            }
        //                        }
        //                        catch
        //                        {
        //                            count++;
        //                            File.Delete(Server.MapPath(ConfigurationManager.AppSettings["MeterReading"].ToString()) + Filename);
        //                            lblMsgPopup.Text = "Invalid Date Format.";
        //                            modalPopupLoading.Hide();
        //                            mpePaymentypopup.Show();
        //                            return;
        //                        }
        //                    }
        //                    if (count == 0)
        //                    {
        //                        try
        //                        {
        //                            Response.Redirect(UMS_Resource.METER_READING_DOCUMENT_PREVIEW + "?" + UMS_Resource.METER_READING_QST_DOCMUNET + "=" + objIdsignBal.Encrypt(ExcelModifiedFileName), false);
        //                        }
        //                        catch (Exception ex)
        //                        {
        //                            Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //                            try
        //                            {
        //                                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //                            }
        //                            catch (Exception logex)
        //                            {

        //                            }
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    lblMsgPopup.Text = Resource.EXCEL_MORETHAN_1000_MESSAGE;
        //                    mpePaymentypopup.Show();
        //                }
        //            }
        //            else
        //            {
        //                lblMsgPopup.Text = "File has uploaded without records.";
        //                mpePaymentypopup.Show();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        File.Delete(Server.MapPath(ConfigurationManager.AppSettings["MeterReading"].ToString()) + Filename);
        //        //throw ex;
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        #endregion OldCodeend

        protected void btnNext_Click(object sender, EventArgs e)
        {
            string ExcelModifiedFileName = string.Empty;
            try
            {
                int count = 0;
                DataSet ds = new DataSet();
                ExcelModifiedFileName = InsersertExcel();
                if (Path.GetExtension(ExcelModifiedFileName) == ".xml")
                {
                    ds.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["MeterReading"].ToString()) + ExcelModifiedFileName);
                    dtExcel = ds.Tables[0];
                }
                else if (Path.GetExtension(ExcelModifiedFileName) == ".xls" || Path.GetExtension(ExcelModifiedFileName) == ".xlsx")
                {
                    string FolderPath = ConfigurationManager.AppSettings["MeterReading"];
                    string FilePath = Server.MapPath(FolderPath + ExcelModifiedFileName);
                    String cnstr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FilePath + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";//2007 Onwards

                    OleDbConnection oledbConn = new OleDbConnection(cnstr);
                    String[] s = objCommonMethods.GetExcelSheetNames(FilePath);
                    string strSQL = "SELECT * FROM [" + s[0] + "]";
                    OleDbCommand cmd = new OleDbCommand(strSQL, oledbConn);
                    OleDbDataAdapter odapt = new OleDbDataAdapter(cmd);
                    odapt.Fill(ds);
                    dtExcel = ds.Tables[0];
                }

                if (dtExcel.Rows.Count > 0)//Validating excel columns.
                {
                    for (int i = 0; i < dtExcel.Columns.Count; i++)
                    {
                        DataSet dsHeaders = new DataSet();
                        dsHeaders.ReadXml(Server.MapPath(ConfigurationSettings.AppSettings["MeterReadingUploadHeaders"]));
                        if (dtExcel.Columns.Count == dsHeaders.Tables[0].Rows.Count)
                        {
                            string HeaderName = dtExcel.Columns[i].ColumnName;
                            var result = (from x in dsHeaders.Tables[0].AsEnumerable()
                                          where x.Field<string>("Names").Trim() == HeaderName.Trim()
                                          select new
                                          {
                                              Names = x.Field<string>("Names")
                                          }).SingleOrDefault();
                            if (result == null)
                            {
                                count++;
                                lblMsgPopup.Text = Resource.COLUMNS_NOT_MATCHNIG;
                                mpePaymentypopup.Show();
                                break;
                            }
                        }
                        else
                        {
                            count++;
                            lblMsgPopup.Text = Resource.COLUMNS_NOT_MATCHNIG;
                            mpePaymentypopup.Show();
                        }
                    }

                    int totalRows = 1000;

                    if (totalRows >= dtExcel.Rows.Count)
                    {
                        modalPopupLoading.Show();
                        totalRows = dtExcel.Rows.Count;

                        for (int i = 0; i < totalRows; i++)
                        {
                            try
                            {
                                string receivedDate = DateTime.ParseExact(dtExcel.Rows[i]["Read_Date"].ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy");
                                    //Convert.ToDateTime(dtExcel.Rows[i]["Read_Date"]).ToString("MM/dd/yyyy");
                                string format = "d";
                                CultureInfo provider = CultureInfo.InvariantCulture;
                                DateTime result = DateTime.ParseExact(receivedDate, format, provider);

                                if (result.Year < Constants.StartValidYear)
                                {
                                    count++;
                                    File.Delete(Server.MapPath(ConfigurationManager.AppSettings["MeterReading"].ToString()) + ExcelModifiedFileName);
                                    lblMsgPopup.Text = Resource.LESSTHAN_1900_DATE_READINGS;
                                    mpePaymentypopup.Show();
                                    break;
                                }
                            }
                            catch
                            {
                                count++;
                                File.Delete(Server.MapPath(ConfigurationManager.AppSettings["MeterReading"].ToString()) + ExcelModifiedFileName);
                                lblMsgPopup.Text = "Invalid Date Format.";
                                modalPopupLoading.Hide();
                                mpePaymentypopup.Show();
                                return;
                            }
                        }
                        if (count == 0)
                        {
                            try
                            {
                                Session["MeterReadingUploadDT"] = ds;
                                Response.Redirect(UMS_Resource.METER_READING_DOCUMENT_PREVIEW + "?" + UMS_Resource.METER_READING_QST_DOCMUNET + "=" + objIdsignBal.Encrypt(ExcelModifiedFileName), false);
                            }
                            catch (Exception ex)
                            {
                                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                                try
                                {
                                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                                }
                                catch (Exception logex)
                                {

                                }
                            }
                        }
                    }
                    else
                    {
                        lblMsgPopup.Text = Resource.EXCEL_MORETHAN_1000_MESSAGE;
                        mpePaymentypopup.Show();
                    }
                }
                else
                {
                    lblMsgPopup.Text = "File has uploaded without records.";
                    mpePaymentypopup.Show();
                }

            }
            catch (Exception ex)
            {
                File.Delete(Server.MapPath(ConfigurationManager.AppSettings["MeterReading"].ToString()) + ExcelModifiedFileName);
                //throw ex;
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }





        ////Check the user password correct or not.


        protected void btnAuthenticarion_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPassword.Text != "")
                {
                    XmlDocument xml = new XmlDocument();
                    LoginPageBE objLoginPageBE = new LoginPageBE();
                    LoginPageBAL objLoginPageBAL = new LoginPageBAL();
                    objLoginPageBE.UserName = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objLoginPageBE.Password = objIdsignBal.AESEncryptString(txtPassword.Text);
                    xml = objLoginPageBAL.Login(objLoginPageBE, ReturnType.Get);
                    objLoginPageBE = objIdsignBal.DeserializeFromXml<LoginPageBE>(xml);

                    if (objLoginPageBE.IsSuccess)
                    {
                        LabelStatusLogin.Visible = false;
                        ModalPopupExtenderLogin.Hide();
                    }
                    else
                    {
                        LabelStatusLogin.Visible = true;
                        LabelStatusLogin.Text = Resource.AUTHENTICATIONFAILED;
                        txtPassword.Focus();
                        ModalPopupExtenderLogin.Show();
                    }
                }
                else
                {
                    LabelStatusLogin.Visible = true;
                    LabelStatusLogin.Text = "Enter Password";
                    txtPassword.Focus();
                    ModalPopupExtenderLogin.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        private bool ValidateDate(string stringDateValue)
        {
            try
            {
                System.Globalization.CultureInfo CultureInfoDateCulture = new System.Globalization.CultureInfo("en-US");
                DateTime d = DateTime.ParseExact(stringDateValue, "MM/dd/yyyy", CultureInfoDateCulture);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public string InsersertExcel()
        {
            try
            {
                string ExcelModifiedFileName = string.Empty;
                if (upDOCExcel.HasFile)
                {
                    string ExcelFileName = System.IO.Path.GetFileName(upDOCExcel.FileName).ToString();//Assinging FileName
                    string ExcelExtension = Path.GetExtension(ExcelFileName);//Storing Extension of file into variable Extension
                    TimeZoneInfo ExcelIND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                    DateTime ExcelcurrentTime = DateTime.Now;
                    DateTime Exceloourtime = TimeZoneInfo.ConvertTime(ExcelcurrentTime, ExcelIND_ZONE);
                    string ExcelCurrentDate = Exceloourtime.ToString("dd-MM-yyyy_hh-mm-ss");
                    ExcelModifiedFileName = Path.GetFileNameWithoutExtension(ExcelFileName) + "_" + ExcelCurrentDate + ExcelExtension;//Setting File Name to store it in database
                    upDOCExcel.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["MeterReading"].ToString()) + ExcelModifiedFileName);//Saving File in to the server.
                }
                return ExcelModifiedFileName;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        //public string InsersertExcel()
        //{
        //    try
        //    {
        //        string ExcelModifiedFileName;

        //        if (upDOCExcel.HasFile)
        //        {
        //            string ExcelFileName = System.IO.Path.GetFileName(upDOCExcel.FileName).ToString();//Assinging FileName
        //            string ExcelExtension = Path.GetExtension(ExcelFileName);//Storing Extension of file into variable Extension
        //            TimeZoneInfo ExcelIND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
        //            DateTime ExcelcurrentTime = DateTime.Now;
        //            DateTime Exceloourtime = TimeZoneInfo.ConvertTime(ExcelcurrentTime, ExcelIND_ZONE);
        //            string ExcelCurrentDate = Exceloourtime.ToString("dd-MM-yyyy_hh-mm-ss");
        //            ExcelModifiedFileName = Path.GetFileNameWithoutExtension(ExcelFileName) + "_" + ExcelCurrentDate + ExcelExtension;//Setting File Name to store it in database
        //            Filename = Path.GetFileNameWithoutExtension(ExcelFileName) + "_" + ExcelCurrentDate + ExcelExtension;//Setting File Name to store it in database
        //            upDOCExcel.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["MeterReading"].ToString()) + ExcelModifiedFileName);//Saving File in to the server.
        //        }
        //        return Filename;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion
    }
}