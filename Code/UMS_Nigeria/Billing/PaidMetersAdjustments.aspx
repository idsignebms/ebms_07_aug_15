﻿<%@ Page Title=":: Paid Meter Adjustments ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    AutoEventWireup="true" CodeBehind="PaidMetersAdjustments.aspx.cs" Theme="Green"
    Inherits="UMS_Nigeria.Billing.PaidMetersAdjustments" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdateProgress ID="UpdateProgress" runat="server">
                <ProgressTemplate>
                    <center>
                        <div id="loading-div" class="pbloading">
                            <div id="title-loading" class="pbtitle">
                                BEDC</div>
                            <center>
                                <img src="../images/loading.GIF" class="pbimg"></center>
                            <p class="pbtxt">
                                Loading Please wait.....</p>
                        </div>
                    </center>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
                PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
            <asp:UpdatePanel ID="upSearchCustomer" DefaultButton="btnSearch" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litSearchCustomer" runat="server" Text="<%$ Resources:Resource, PAID_MTR_ADJST%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litAccountNo" runat="server" Text="<%$ Resources:Resource, ACCNO_OLD_ACCNO%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </label>
                                <asp:TextBox ID="txtAccountNo" CssClass="text-box" runat="server" MaxLength="50"
                                    placeholder="Global Account No / Old Account No"></asp:TextBox>
                                <div class="clear space">
                                </div>
                                <span id="spanAccNo" class="span_color"></span>
                            </div>
                        </div>
                    </div>
                    <div class="inner-box1">
                        <div style="position: absolute; margin: 19px -156px 10px -160px">
                            <asp:Button ID="btnGo" OnClientClick="return Validate();" Text="<%$ Resources:Resource, GET_CUSTOMER_DETAILS%>"
                                CssClass="box_s" OnClick="btnGo_Click" runat="server" />
                        </div>
                    </div>
                    <div class="clear">
                        &nbsp;</div>
                    <div id="divdetails" visible="false" class="out-bor_a" runat="server">
                        <div class="inner-sec">
                            <div class="heading">
                                <asp:Literal ID="Literal213" runat="server" Text="<%$ Resources:Resource, DETAILS%>"></asp:Literal>
                            </div>
                            <div class="pad_10">
                            </div>
                            <div class="out-bor_c" style="width: 87%; margin-left: 10px;margin-top: 10px;">
                                <div class="inner-box" style="margin-top: 10px;">
                                    <div class="inner-box1_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_b">
                                        <div class="text-inner">
                                            :</div>
                                    </div>
                                    <div class="inner-box3_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblAccNoAndGlobalAccNo" runat="server" Text="--"></asp:Label>
                                            <asp:Label ID="lblAccountNo" Visible="false" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box4_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box5_b">
                                        <div class="text-inner">
                                            :</div>
                                    </div>
                                    <div class="inner-box6_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblOldAccNo" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_b">
                                        <div class="text-inner">
                                            :</div>
                                    </div>
                                    <div class="inner-box3_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblName" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box4_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box5_b">
                                        <div class="text-inner">
                                            :</div>
                                    </div>
                                    <div class="inner-box6_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblMeterNo" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_b">
                                        <div class="text-inner">
                                            :</div>
                                    </div>
                                    <div class="inner-box3_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblTariff" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box4_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:Resource, MTR_COST%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box5_b">
                                        <div class="text-inner">
                                            :</div>
                                    </div>
                                    <div class="inner-box6_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblMeterCost" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:Resource, ADJUST_AMT%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_b">
                                        <div class="text-inner">
                                            :</div>
                                    </div>
                                    <div class="inner-box3_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblAdjustedAmt" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box4_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal6" runat="server" Text="Bill Adjustment Amount"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box5_b">
                                        <div class="text-inner">
                                            :</div>
                                    </div>
                                    <div class="inner-box6_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblBillAdjustmentAmount" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, OUT_STANDING_AMT%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_b">
                                        <div class="text-inner">
                                            :</div>
                                    </div>
                                    <div class="inner-box3_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblOutStandingAmt" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <%--<div class="clr">
                                    </div>
                                    <div class="inner-box4_b">
                                        <div class="text_cus">
                                        </div>
                                    </div>
                                    <div class="inner-box5_b">
                                        <div class="text-inner">
                                            :</div>
                                    </div>
                                    <div class="inner-box6_b">
                                        <div class="text_cusa_a">
                                        </div>
                                    </div>
                                    <div class="consumer_total">
                                        <div class="clear pad_10">
                                        </div>
                                    </div>--%>
                                </div>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="consumer_feild">
                            <h3>
                                <asp:Literal ID="litPostalAddress" runat="server" Text=""></asp:Literal></h3>
                            <div class="inner-box">
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal2" runat="server" Text="Amount"></asp:Literal><span class="span_star">*</span>
                                        </label>
                                        <br />
                                        <asp:TextBox ID="txtAmount" CssClass=" text-box" runat="server" placeholder="Enter Amount"
                                            MaxLength="16" onblur="return TextBoxBlurValidationlblWithNoZero(this,'spanAmount','Amount')"
                                            onkeyup="GetCurrencyFormate(this);" onkeypress="return isNumberKey(event,this)"></asp:TextBox><br />
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtAmount"
                                            FilterType="Custom,Numbers" ValidChars=".,">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanAmount" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box2">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, REASON%>"></asp:Literal><span
                                                class="span_star">*</span>
                                        </label>
                                    </div>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox ID="txtReason" CssClass="text_area_a" runat="server" TextMode="MultiLine"
                                        placeholder="Reason" onblur="return TextBoxBlurValidationlblZeroAccepts(this,'spanReason','Reason')"></asp:TextBox>
                                    <span id="spanReason" class="span_color"></span>
                                </div>
                            </div>
                            <div class="pad_5 clear">
                            </div>
                            <div class="clear pad_5">
                                <div style="margin-left: 85px;">
                                    <asp:Button ID="btnSave" Text="<%$ Resources:Resource, UPDATE%>" CssClass="box_s"
                                        OnClientClick="return UpdateValidate();" OnClick="btnUpdate_Click" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear pad_10">
                    </div>
                    <div id="rnkland">
                        <%-- Confirm popup Start--%>
                        <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                            TargetControlID="btnActivate" BackgroundCssClass="modalBackground">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="PanelActivate" runat="server" DefaultButton="btnActiveCancel" CssClass="modalPopup"
                            Style="display: none; border-color: #639B00;">
                            <div class="popheader" style="background-color: #639B00;">
                                <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                            </div>
                            <br />
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Confirm popup Closed--%>
                    </div>
                    <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="PanelAlert"
                        TargetControlID="btnAlertDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnAlertOk">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnAlertDeActivate" runat="server" Text="Button" CssClass="popbtn" />
                    <asp:Panel ID="PanelAlert" runat="server" CssClass="modalPopup" class="poppnl" Style="display: none;">
                        <div class="popheader popheaderlblred" id="pop" runat="server">
                            <asp:Label ID="lblAlertMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnAlertOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:HiddenField ID="hfOutStandingAmount" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".rnkland").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/PaidMetersAdjustments.js?v=1" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
