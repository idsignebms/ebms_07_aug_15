﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for PaidMetersAdjustments
                     
 Developer        : id065-Bhimaraju Vanka
 Creation Date    : 07-11-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using Resources;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Threading;
using System.Globalization;

namespace UMS_Nigeria.Billing
{
    public partial class PaidMetersAdjustments : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        PaidMeterBal _objPaidMeterBal = new PaidMeterBal();
        ReportsBal objReportsBal = new ReportsBal();
        string Key = "PaidMeterAdjustments";
        #endregion

        #region Properties
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                lblMessage.Text = pnlMessage.CssClass = string.Empty;
                if (!IsPostBack)
                {
                    //string path = string.Empty;
                    //path = _objCommonMethods.GetPagePath(this.Request.Url);
                    //if (_objCommonMethods.IsAccess(path))
                    //{

                    //}
                    //else
                    //{
                    //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    //}
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }
        protected void btnGo_Click(object sender, EventArgs e)
        {
            RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
            objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                objLedgerBe.BUID = string.Empty;
            objLedgerBe = _objiDsignBal.DeserializeFromXml<RptCustomerLedgerBe>(objReportsBal.GetLedger(objLedgerBe, ReturnType.Single));
            if (objLedgerBe.IsSuccess)
            {
                if (objLedgerBe.IsCustExistsInOtherBU)
                {
                    lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                    mpeAlert.Show();
                }
                else
                {
                    PaidMeterBe _objPaidMeterBe = new PaidMeterBe();
                    _objPaidMeterBe.AccountNo = txtAccountNo.Text.Trim();
                    _objPaidMeterBe = _objiDsignBal.DeserializeFromXml<PaidMeterBe>(_objPaidMeterBal.Get(_objPaidMeterBe, ReturnType.Get));
                    if (!_objPaidMeterBe.IsNotPaidCustomer)
                    {
                        if (_objPaidMeterBe.OutStandingAmount > 0)
                        {
                            divdetails.Visible = true;
                            lblAccountNo.Text = _objPaidMeterBe.AccountNo;
                            lblOldAccNo.Text = _objPaidMeterBe.OldAccountNo;
                            lblAccNoAndGlobalAccNo.Text = _objPaidMeterBe.AccNoAndGlobalAccNo;
                            lblName.Text = _objPaidMeterBe.Name;
                            lblTariff.Text = _objPaidMeterBe.Tariff;
                            lblMeterNo.Text = _objPaidMeterBe.MeterNo;
                            lblMeterCost.Text = _objCommonMethods.GetCurrencyFormat(_objPaidMeterBe.MeterCost, 2, Constants.MILLION_Format);
                            lblOutStandingAmt.Text = _objCommonMethods.GetCurrencyFormat(_objPaidMeterBe.OutStandingAmount, 2, Constants.MILLION_Format);
                            hfOutStandingAmount.Value = _objPaidMeterBe.OutStandingAmount.ToString();
                            lblAdjustedAmt.Text = _objCommonMethods.GetCurrencyFormat(_objPaidMeterBe.PaidAmount, 2, Constants.MILLION_Format);
                            lblBillAdjustmentAmount.Text = _objCommonMethods.GetCurrencyFormat(_objPaidMeterBe.BilladjustmentAmount, 2, Constants.MILLION_Format);
                        }
                        else 
                        {
                            divdetails.Visible = false;
                            Message("This " + txtAccountNo.Text + " not a paid customer", UMS_Resource.MESSAGETYPE_ERROR);
                            txtAccountNo.Text = string.Empty;
                        }
                    }
                    else if (_objPaidMeterBe.IsNotPaidCustomer)
                    {
                        divdetails.Visible = false;
                        Message("This " + txtAccountNo.Text + " not a paid customer", UMS_Resource.MESSAGETYPE_ERROR);
                        txtAccountNo.Text = string.Empty;
                    }
                    txtAmount.Text = txtReason.Text = string.Empty;
                }
            }
            else
            {
                CustomerNotFound();
            }
        }

        private void CustomerNotFound()
        {
            divdetails.Visible = false;
            txtAccountNo.Text = string.Empty;
            txtAmount.Text = txtReason.Text = string.Empty;
            Message(UMS_Resource.CUSTOMER_NOT_FOUND_MSG, UMS_Resource.MESSAGETYPE_ERROR);
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            PaidMeterBe _objPaidMeterBe = new PaidMeterBe();
            _objPaidMeterBe.AccountNo = lblAccountNo.Text;// txtAccountNo.Text.Trim();
            _objPaidMeterBe.PaidAmount = Convert.ToDecimal(txtAmount.Text.Replace(",", string.Empty));
            _objPaidMeterBe.Details = _objiDsignBal.ReplaceNewLines(txtReason.Text, true);
            _objPaidMeterBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();

            //bool IsApproval = _objCommonMethods.IsApproval(UMS_Resource.FEATURE_PAID_METER_ADJUST);
            //if (IsApproval)
            //{
            //_objPaidMeterBe.ApprovalStatusId = Convert.ToInt32(Resources.Resource.FLAG_TEMP);
            //_objPaidMeterBe = _objiDsignBal.DeserializeFromXml<PaidMeterBe>(_objPaidMeterBal.Insert(_objPaidMeterBe, Statement.Update));
            //if (_objPaidMeterBe.IsSuccess)
            //{
            //    txtAccountNo.Text = txtAmount.Text = txtReason.Text = string.Empty;
            //    divdetails.Visible = false;
            //    lblActiveMsg.Text = Resource.APPROVAL_TARIFF_CHANGE;
            //    mpeActivate.Show();
            //}
            //else
            //{
            //    divdetails.Visible = true;
            //    Message(Resource.CUSTOMER_ADJUSTED_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
            //}
            //}
            //else
            //{
            _objPaidMeterBe.ApprovalStatusId = 2;
            _objPaidMeterBe = _objiDsignBal.DeserializeFromXml<PaidMeterBe>(_objPaidMeterBal.Insert(_objPaidMeterBe, Statement.Insert));
            if (_objPaidMeterBe.IsSuccess)
            {
                txtAccountNo.Text = txtAmount.Text = txtReason.Text = string.Empty;
                divdetails.Visible = false;
                //lblActiveMsg.Text = Resource.MODIFICATIONS_UPDATED;
                //mpeActivate.Show();
                Message(Resource.CUSTOMER_ADJUSTED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
            }
            else
            {
                divdetails.Visible = true;
                Message(Resource.CUSTOMER_ADJUSTED_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
            }
            //}
        }
        #endregion

        #region Methods
        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}