﻿<%@ Page Title=":: Payment Approvals ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="PaymentApproval.aspx.cs" Inherits="UMS_Nigeria.Billing.PaymentApproval" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upBatchPaymentStatus" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddCycle" runat="server" Text="Payments Approval"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal1" runat="server" Text="Payment Type"></asp:Literal><span
                                        class="span_star">*</span>
                                </label>
                                <div class="space">
                                </div>
                                <asp:DropDownList ID="ddlPaymentType" CssClass="text-box select_box" runat="server"
                                    AutoPostBack="true">
                                    <asp:ListItem>--Select--</asp:ListItem>
                                </asp:DropDownList>
                                <span id="spanPaymentType" class="spancolor"></span>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div id="divBatchesList" runat="server" visible="false">
                        <div id="divgrid" class="grid marg_5" runat="server" style="width: 99% !important;
                            float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="litBuList" runat="server" Text="Batch wise Approvals"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid_tb">
                            <asp:GridView ID="gvBatches" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                HeaderStyle-CssClass="grid_tb_hd">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No Details Found.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, GRID_SNO%>"
                                        DataField="RowNumber" ItemStyle-CssClass="grid_tb_td" />
                                    <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Batch Number" DataField="BatchID"
                                        ItemStyle-CssClass="grid_tb_td" />
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, BATCH_DATE%>" ItemStyle-CssClass="grid_tb_td">
                                        <ItemTemplate>
                                            <asp:Literal ID="litBatchTotal" runat="server" Text=""></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, BATCH_TOTAL%>"
                                        DataField="" ItemStyle-CssClass="grid_tb_td" />
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, PAID_AMOUNT%>" ItemStyle-CssClass="grid_tb_td">
                                        <ItemTemplate>
                                            <asp:Literal ID="litPaidAmount" runat="server" Text=""></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Total Customers" DataField=""
                                        ItemStyle-CssClass="grid_tb_td" />
                                    <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="User Id" DataField=""
                                        ItemStyle-CssClass="grid_tb_td" />
                                    <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="User Name" DataField=""
                                        ItemStyle-CssClass="grid_tb_td" />
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-CssClass="grid_tb_td">
                                        <ItemTemplate>
                                            <asp:Literal ID="litID" Visible="False" runat="server" Text=""></asp:Literal>
                                            <asp:LinkButton ID="lbtnApprove" runat="server"> Approve
                                            </asp:LinkButton>
                                            <span style="margin: 0 5px;">/</span>
                                            <asp:LinkButton ID="lbtnOther" runat="server"> Other
                                            </asp:LinkButton>
                                            <span style="margin: 0 5px;">/</span>
                                            <asp:LinkButton ID="lbtnView" runat="server"> View
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle HorizontalAlign="Center" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });
    </script>
</asp:Content>
