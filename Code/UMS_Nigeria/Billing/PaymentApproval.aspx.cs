﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.Drawing;
using iDSignHelper;
using Resources;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using System.Data.SqlClient;
using UMS_NigeriaBE;

namespace UMS_Nigeria.Billing
{
    public partial class PaymentApproval : System.Web.UI.Page
    {
        #region Properties

        public int PaymentType
        {
            get { return string.IsNullOrEmpty(ddlPaymentType.SelectedValue) ? 0 : Convert.ToInt32(ddlPaymentType.SelectedValue); }
        }

        #endregion

        #region Members

        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        public int PageNum;
        string Key = "PaymentsApproval";

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                _ObjCommonMethods.BindPaymentTypes(ddlPaymentType, true);
            }
        }

        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

        #region Culture

        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion
        
    }
}