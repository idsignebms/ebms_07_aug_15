﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using Resources;
using UMS_NigeriaBE;
using System.Xml;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using System.Data;
using org.in2bits.MyXls;
using System.Configuration;
using System.IO;

namespace UMS_Nigeria.Billing
{
    public partial class PaymentBatchProcesFailures : System.Web.UI.Page
    {
        #region Properties

        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStartsReport : Convert.ToInt32(hfPageSize.Value); }
        }

        #endregion

        #region Members

        CommonMethods objCommonMethods = new CommonMethods();
        PaymentsBal _objPaymentsBal = new PaymentsBal();
        iDsignBAL objIdsignBal = new iDsignBAL();
        string Key = "PaymentBatchProcesFailures";
        //int BatchId;

        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString[UMS_Resource.PAYMENTS_BATCH_ID] != null)
                    {
                        //if (!string.IsNullOrEmpty(objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.PAYMENTS_BATCH_ID].ToString())))
                        hfPUploadBatchId.Value = objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.PAYMENTS_BATCH_ID].ToString());
                        hfPageNo.Value = Constants.pageNoValue.ToString();
                        BindGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            try
            {
                btnExportToExcel.Enabled = false;
                if (Request.QueryString[UMS_Resource.PAYMENTS_BATCH_ID] != null)
                {
                    PaymentsBatchBE objPaymentsBatchBE = new PaymentsBatchBE();
                    //PaymentsBatchListBE objPaymentsBatchListBE = new PaymentsBatchListBE();
                    DataSet PaymentsBatchListDs = new DataSet();
                    objPaymentsBatchBE.PUploadFileId = Convert.ToInt32(hfPUploadBatchId.Value);
                    objPaymentsBatchBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                    objPaymentsBatchBE.PageSize =  Convert.ToInt32(hfTotalRecords.Value);
                    PaymentsBatchListDs = _objPaymentsBal.PaymentBatchProcesFailures(objPaymentsBatchBE, ReturnType.Get);

                    if (PaymentsBatchListDs != null)
                    {
                        if (PaymentsBatchListDs.Tables.Count > 0 && PaymentsBatchListDs.Tables[1].Rows.Count > 0)
                        {
                            XlsDocument xls = new XlsDocument();
                            Worksheet sheet = xls.Workbook.Worksheets.Add("Payment Batch Process Failure Transactions");
                            Cells cells = sheet.Cells;
                            Cell _objCell = null;

                            int DetailsRowNo = 1;
                            int CustomersRowNo = 2;

                            _objCell = cells.Add(DetailsRowNo, 1, "S.No");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;
                            _objCell = cells.Add(DetailsRowNo, 2, "Global / Old Account No.");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;
                            _objCell = cells.Add(DetailsRowNo, 3, "Amount Paid");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;
                            _objCell = cells.Add(DetailsRowNo, 4, "Receipt No");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;
                            _objCell = cells.Add(DetailsRowNo, 5, "Received Date");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;
                            _objCell = cells.Add(DetailsRowNo, 6, "Payment Mode");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;
                            _objCell = cells.Add(DetailsRowNo, 7, "Comments");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;

                            for (int n = 0; n < PaymentsBatchListDs.Tables[1].Rows.Count; n++)
                            {
                                DataRow dr = (DataRow)PaymentsBatchListDs.Tables[1].Rows[n];
                                cells.Add(CustomersRowNo, 1, dr["SNO"].ToString());
                                cells.Add(CustomersRowNo, 2, dr["AccountNo"].ToString());
                                cells.Add(CustomersRowNo, 3, dr["AmountPaid"].ToString());
                                cells.Add(CustomersRowNo, 4, dr["ReceiptNo"].ToString());
                                cells.Add(CustomersRowNo, 5, dr["ReceivedDate"].ToString());
                                cells.Add(CustomersRowNo, 6, dr["PaymentMode"].ToString());
                                cells.Add(CustomersRowNo, 7, dr["Comments"].ToString());

                                CustomersRowNo++;
                            }

                            string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                            fileName = "PaymentBatchProcessFailureTransactions_" + fileName;
                            string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                            if (!File.Exists(filePath))
                            {
                                xls.FileName = filePath;
                                xls.Save();
                            }
                            objIdsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                        }
                        else
                            Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            finally
            {
                btnExportToExcel.Enabled = true;
            }
        }

        #region Methods

        public void BindGrid()
        {
            PaymentsBatchBE objPaymentsBatchBE = new PaymentsBatchBE();
            //PaymentsBatchListBE objPaymentsBatchListBE = new PaymentsBatchListBE();
            DataSet PaymentsBatchListDs = new DataSet();
            objPaymentsBatchBE.PUploadFileId = Convert.ToInt32(hfPUploadBatchId.Value);
            objPaymentsBatchBE.PageNo = Convert.ToInt32(hfPageNo.Value);
            objPaymentsBatchBE.PageSize = PageSize;
            PaymentsBatchListDs = _objPaymentsBal.PaymentBatchProcesFailures(objPaymentsBatchBE, ReturnType.Get);

            lblBatchNo.Text = PaymentsBatchListDs.Tables[0].Rows[0]["BatchNo"].ToString();
            lblBatchDate.Text = PaymentsBatchListDs.Tables[0].Rows[0]["BatchDate"].ToString();
            lblNotes.Text = PaymentsBatchListDs.Tables[0].Rows[0]["Notes"].ToString();
            lblTotalTransactionFailed.Text = PaymentsBatchListDs.Tables[0].Rows[0]["TotalFailureTransactions"].ToString();
            lblTotalCustomers.Text = PaymentsBatchListDs.Tables[0].Rows[0]["TotalCustomers"].ToString();
            
            if (PaymentsBatchListDs.Tables[1].Rows.Count > 0)
            {
                divPaging1.Visible = divPaging2.Visible = divEportTOExcel.Visible = true;
                UCPaging1.Visible = UCPaging2.Visible = true;
                hfTotalRecords.Value = PaymentsBatchListDs.Tables[1].Rows[0]["TotalRecords"].ToString();
                gvPaymentBatchProcesFailures.DataSource = PaymentsBatchListDs.Tables[1];
                gvPaymentBatchProcesFailures.DataBind();
            }
            else
            {
                hfTotalRecords.Value = PaymentsBatchListDs.Tables[1].Rows.Count.ToString();
                divPaging1.Visible = divPaging2.Visible = divEportTOExcel.Visible = false;
                UCPaging1.Visible = UCPaging2.Visible = false;
                gvPaymentBatchProcesFailures.DataSource = new DataTable(); ;
                gvPaymentBatchProcesFailures.DataBind();
            }

        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion
    }
}