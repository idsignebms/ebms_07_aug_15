﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using iDSignHelper;
using System.Xml;
using UMS_NigeriaBE;
using Resources;
using System.Data;
using System.Threading;
using System.Globalization;
using UMS_NigriaDAL;
using System.Collections;
using System.IO;
using System.Configuration;
using System.Data.OleDb;

namespace UMS_Nigeria.Billing
{
    public partial class PaymentBatchProcessStatus : System.Web.UI.Page
    {
        #region Properties

        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }

        }
        #endregion

        #region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        PaymentsBal _objPaymentsBal = new PaymentsBal();
        BillReadingsBAL _objBillReadingsBAL = new BillReadingsBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        UMS_Service _ObjUMS_Service = new UMS_Service();
        XmlDocument xml = new XmlDocument();
        public int PageNum;
        string Key = "PaymentBatchProcessStatus";
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                string path = string.Empty;
                path = _ObjCommonMethods.GetPagePath(this.Request.Url);
                if (_ObjCommonMethods.IsAccess(path))
                {
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    BindGrid();
                }
                else { Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            //BindGrid();
            Response.Redirect("PaymentBatchProcessStatus.aspx", false);
        }

        protected void gvPaymentBatchProcessStatus_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            btnBatchCloseOk.Visible = btnUploadTransactionsOk.Visible = false;
            switch (e.CommandName.ToUpper())
            {
                case "BATCHCLOSE":
                    try
                    {
                        GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                        //hfBatchNo.Value = ((Label)row.FindControl("lblBatchID")).Text;
                        hfUploadBatchId.Value = ((Label)row.FindControl("lblBatchUploadId")).Text;
                        mpeBatchClose.Show();
                        btnBatchCloseOk.Visible = true;
                        lblBatchMsg.Text = "Are you sure to you want to Close this batch ?";
                        btnBatchCloseOk.Focus();
                    }

                    catch (Exception ex)
                    {
                        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                        try
                        {
                            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                        }
                        catch (Exception logex)
                        {

                        }
                    }
                    break;
                case "UPLOADTRANSACTIONS":
                    try
                    {
                        GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                        hfBatchNo.Value = ((Label)row.FindControl("lblBatchID")).Text;
                        hfUploadBatchId.Value = ((Label)row.FindControl("lblBatchUploadId")).Text;
                        //int id = Convert.ToInt32(((Literal)row.FindControl("litID")).Text);
                        mpeUploadTransaction.Show();
                        btnUploadTransactionsOk.Visible = true;
                        lblUploadTransactionMsg.Text = "Upload file";
                        btnUploadTransactionsOk.Focus();
                    }

                    catch (Exception ex)
                    {
                        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                        try
                        {
                            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                        }
                        catch (Exception logex)
                        {

                        }
                    }
                    break;
                case "VIEWFAILURETRANSACTIONS":
                    try
                    {
                        GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                        //hfBatchNo.Value = ((Label)row.FindControl("lblBatchID")).Text;
                        string PUploadFileId = ((Label)row.FindControl("lblPUploadFileId")).Text;
                        Response.Redirect(UMS_Resource.PAYMENT_BATCH_PROCESS_FAILURE + "?" + UMS_Resource.PAYMENTS_BATCH_ID + "=" + _ObjiDsignBAL.Encrypt(PUploadFileId), false);
                    }

                    catch (Exception ex)
                    {
                        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                        try
                        {
                            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                        }
                        catch (Exception logex)
                        {

                        }
                    }
                    break;
                case "VIEWSUCCESSTRANSACTIONS":
                    try
                    {
                        GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                        //hfBatchNo.Value = ((Label)row.FindControl("lblBatchID")).Text;
                        string PUploadFileId = ((Label)row.FindControl("lblPUploadFileId")).Text;
                        Response.Redirect("PaymentBatchProcessSuccessTransactions.aspx?" + UMS_Resource.PAYMENTS_BATCH_ID + "=" + _ObjiDsignBAL.Encrypt(PUploadFileId), false);
                    }

                    catch (Exception ex)
                    {
                        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                        try
                        {
                            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                        }
                        catch (Exception logex)
                        {

                        }
                    }
                    break;
                case "DOWNLOADACTUALFILE":
                    try
                    {
                        GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                        //hfBatchNo.Value = ((Label)row.FindControl("lblBatchID")).Text;
                        string ActualFilePath = Server.MapPath(ConfigurationManager.AppSettings["Payments"].ToString()) + ((Label)row.FindControl("lblActualFilePath")).Text;
                        if (File.Exists(ActualFilePath))
                            _ObjCommonMethods.DownloadFile(ActualFilePath, "application//vnd.ms-excel");
                    }

                    catch (Exception ex)
                    {
                        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                        try
                        {
                            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                        }
                        catch (Exception logex)
                        {

                        }
                    }
                    break;
            }

        }

        protected void gvPaymentBatchProcessStatus_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[10].ColumnSpan = 4;
                e.Row.Cells[10].Text = "Action";
                e.Row.Cells[11].Visible = e.Row.Cells[12].Visible = e.Row.Cells[13].Visible = false;
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int SuccessTransactions = Convert.ToInt32(((Literal)e.Row.FindControl("litSuccessTransactions")).Text);
                int FailureTransactions = Convert.ToInt32(((Literal)e.Row.FindControl("litFailureTransactions")).Text);
                int TotalCustomers = Convert.ToInt32(((Literal)e.Row.FindControl("litTotalCustomers")).Text);
                LinkButton lkbtnViewFailureTransactions = (LinkButton)e.Row.FindControl("lkbtnViewFailureTransactions");
                LinkButton lkbtnViewSuccessTransactions = (LinkButton)e.Row.FindControl("lkbtnViewSuccessTransactions");
                lkbtnViewFailureTransactions.Enabled = lkbtnViewSuccessTransactions.Enabled = true;
                Image imgStatus = ((Image)e.Row.FindControl("imgStatus"));
                if (TotalCustomers == (SuccessTransactions + FailureTransactions))
                {
                    if (FailureTransactions > 0)
                    {
                        lkbtnViewFailureTransactions.Enabled = true;
                        lkbtnViewFailureTransactions.ForeColor = System.Drawing.Color.FromArgb(0, 120, 197);//defalt link color
                        imgStatus.ImageUrl = "~/images/icon_error.png";
                    }
                    else
                    {
                        lkbtnViewFailureTransactions.Enabled = false;
                        lkbtnViewFailureTransactions.ForeColor = System.Drawing.Color.Gray;
                        imgStatus.ImageUrl = "~/images/Activate.gif";
                    }
                    if (SuccessTransactions > 0)
                    {
                        lkbtnViewSuccessTransactions.Enabled = true;
                        lkbtnViewSuccessTransactions.ForeColor = System.Drawing.Color.FromArgb(0, 120, 197);//defalt link color
                    }
                    else
                    {
                        lkbtnViewSuccessTransactions.Enabled = false;
                        lkbtnViewSuccessTransactions.ForeColor = System.Drawing.Color.Gray;
                    }
                }
                else
                {
                    imgStatus.ImageUrl = "~/images/update1.png";
                    if (FailureTransactions > 0)
                    {
                        lkbtnViewFailureTransactions.Enabled = true;
                        lkbtnViewFailureTransactions.ForeColor = System.Drawing.Color.FromArgb(0, 120, 197);//defalt link color
                    }
                    else
                    {
                        lkbtnViewFailureTransactions.Enabled = false;
                        lkbtnViewFailureTransactions.ForeColor = System.Drawing.Color.Gray;
                    }
                    if (SuccessTransactions > 0)
                    {
                        lkbtnViewSuccessTransactions.Enabled = true;
                        lkbtnViewSuccessTransactions.ForeColor = System.Drawing.Color.FromArgb(0, 120, 197);//defalt link color
                    }
                    else
                    {
                        lkbtnViewSuccessTransactions.Enabled = false;
                        lkbtnViewSuccessTransactions.ForeColor = System.Drawing.Color.Gray;
                    }
                }

                //decimal amount = Convert.ToDecimal(((Literal)e.Row.FindControl("litPaidAmount")).Text);
                //LinkButton lkDelete = (LinkButton)e.Row.FindControl("lkBatchDelete");
                //Literal litBatchTotal = (Literal)e.Row.FindControl("litBatchTotal");
                //Literal litPaidAmount = (Literal)e.Row.FindControl("litPaidAmount");
                //litBatchTotal.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(litBatchTotal.Text), 2, Constants.MILLION_Format);
                //litPaidAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(litPaidAmount.Text), 2, Constants.MILLION_Format);
                //if (amount > 0)
                //    lkDelete.Visible = false;
            }
        }

        protected void btnBatchCloseOk_Click(object sender, EventArgs e)
        {
            try
            {
                PaymentsBatchBE _objPaymentsBatchBE = new PaymentsBatchBE();
                _objPaymentsBatchBE.PUploadBatchId = Convert.ToInt32(hfUploadBatchId.Value);
                _objPaymentsBatchBE.LastModifyBy = Convert.ToString(Session[UMS_Resource.SESSION_LOGINID]);
                xml = _objPaymentsBal.PaymentUploadBatches(_objPaymentsBatchBE, Statement.Delete);
                _objPaymentsBatchBE = _ObjiDsignBAL.DeserializeFromXml<PaymentsBatchBE>(xml);
                if (_objPaymentsBatchBE.RowsEffected > 0)
                {
                    Message("Batch Closed Successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }
        static DataTable GetTable()
        {
            // Here we create a DataTable with four columns.
            DataTable table = new DataTable();
            table.Columns.Add("SNO", typeof(string));
            table.Columns.Add("AccountNo", typeof(string));
            table.Columns.Add("AmountPaid", typeof(string));
            table.Columns.Add("ReceiptNO", typeof(string));
            table.Columns.Add("ReceivedDate", typeof(string));
            table.Columns.Add("PaymentMode", typeof(string));

            return table;
        }
        protected void btnUploadTransactionsOk_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtExcel = GetTable();
                Hashtable htableMaxLength = new Hashtable();
                int count = 0;
                string ExcelModifiedFileName = InsersertExcel();
                //if (Path.GetExtension(ExcelModifiedFileName) == ".xml")
                //{
                //    DataSet ds = new DataSet();
                //    ds.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["Payments"].ToString()) + ExcelModifiedFileName);
                //    dtExcel = ds.Tables[0];
                //}
                //else 
                if (Path.GetExtension(ExcelModifiedFileName) == ".xls" || Path.GetExtension(ExcelModifiedFileName) == ".xlsx")
                {
                    string FolderPath = ConfigurationManager.AppSettings["Payments"];
                    string FilePath = Server.MapPath(FolderPath + ExcelModifiedFileName);
                    String cnstr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FilePath + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";//2007 Onwards

                    OleDbConnection oledbConn = new OleDbConnection(cnstr);
                    String[] s = _ObjCommonMethods.GetExcelSheetNames(FilePath);

                    if (s != null)
                    {
                        string strSQL = "SELECT * FROM [" + s[0] + "]";
                        OleDbCommand cmd = new OleDbCommand(strSQL, oledbConn);
                        DataSet ds = new DataSet();
                        OleDbDataAdapter odapt = new OleDbDataAdapter(cmd);
                        odapt.Fill(dtExcel);
                    }
                    //dtExcel = ds.Tables[0];
                }
                if (dtExcel.Rows.Count > 0)//Validating excel columns.
                {
                    for (int i = 0; i < dtExcel.Columns.Count; i++)
                    {
                        DataSet dsHeaders = new DataSet();
                        dsHeaders.ReadXml(Server.MapPath(ConfigurationSettings.AppSettings["PaymentUploadHeaders"]));
                        if (dtExcel.Columns.Count == dsHeaders.Tables[0].Rows.Count)
                        {
                            string HeaderName = dtExcel.Columns[i].ColumnName;
                            var result = (from x in dsHeaders.Tables[0].AsEnumerable()
                                          where x.Field<string>("Names").Trim() == HeaderName.Trim()
                                          select new
                                          {
                                              Names = x.Field<string>("Names")
                                          }).SingleOrDefault();
                            if (result == null)
                            {
                                count++;
                                lblgridalertmsg.Text = "Uploaded file format is not appropriate.";
                                mpegridalert.Show();
                                break;
                            }
                        }
                        else
                        {
                            count++;
                            lblgridalertmsg.Text = "Uploaded file format is not appropriate.";
                            mpegridalert.Show();
                            break;
                        }
                    }
                    if (count == 0)
                    {
                        bool isSuccess = false;

                        DataView dv = dtExcel.DefaultView;
                        dv.RowFilter = "LEN(AccountNo) > 0";
                        dv.Sort = "SNO ASC";
                        dtExcel = dv.ToTable();

                        if (dtExcel.Rows.Count > 0)
                        {
                            int BatchNo = Convert.ToInt32(hfBatchNo.Value);
                            int UploadBatchId = Convert.ToInt32(hfUploadBatchId.Value);
                            string CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                            //string FileName = Server.MapPath(ConfigurationManager.AppSettings["Payments"].ToString()) + ExcelModifiedFileName;
                            string FileName = ExcelModifiedFileName;//Faiz-ID103
                            int TotalRecords = dtExcel.Rows.Count;

                            isSuccess = _objBillReadingsBAL.InsertPaymentUploadTransactionDT(BatchNo, UploadBatchId, CreatedBy, TotalRecords, FileName, dtExcel);

                            if (isSuccess)
                            {
                                BindGrid();
                                popalert.Attributes.Add("class", "popheader popheaderlblgreen");
                                lblgridalertmsg.Text = "Successfully saved the Upload Transaction details.";
                                mpegridalert.Show();
                            }
                            else
                            {
                                popalert.Attributes.Add("class", "popheader popheaderlblred");
                                lblgridalertmsg.Text = "Upload Transaction details failed to save.";
                                mpegridalert.Show();
                            }
                        }
                        else
                        {
                            popalert.Attributes.Add("class", "popheader popheaderlblred");
                            lblgridalertmsg.Text = "No valid data to upload";
                            mpegridalert.Show();
                        }
                    }
                    else
                    {
                        popalert.Attributes.Add("class", "popheader popheaderlblred");
                        lblgridalertmsg.Text = "Uploaded file format is not appropriate.";
                        mpegridalert.Show();
                    }
                }
                else
                {
                    popalert.Attributes.Add("class", "popheader popheaderlblred");
                    lblgridalertmsg.Text = "Unable to read the file, please try with another file with proper formats.";
                    mpegridalert.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        public void BindGrid()
        {
            PaymentsBatchBE objPaymentsBatchBE = new PaymentsBatchBE();
            PaymentsBatchListBE objPaymentsBatchListBE = new PaymentsBatchListBE();
            XmlDocument resultedXml = new XmlDocument();
            objPaymentsBatchBE.CreatedBy = Convert.ToString(Session[UMS_Resource.SESSION_LOGINID]);
            objPaymentsBatchBE.PageNo = Convert.ToInt32(hfPageNo.Value);
            objPaymentsBatchBE.PageSize = PageSize;
            objPaymentsBatchBE.BU_ID = Convert.ToString(Session[UMS_Resource.SESSION_USER_BUID]);
            resultedXml = _objPaymentsBal.PaymentUploadBatches(objPaymentsBatchBE, ReturnType.Get);
            objPaymentsBatchListBE = _ObjiDsignBAL.DeserializeFromXml<PaymentsBatchListBE>(resultedXml);

            if (objPaymentsBatchListBE.Items.Count > 0)
            {
                divPaging1.Visible = divPaging2.Visible = true;
                UCPaging1.Visible = UCPaging2.Visible = true;
                hfTotalRecords.Value = objPaymentsBatchListBE.Items[0].TotalRecords.ToString();
                gvPaymentBatchProcessStatus.DataSource = objPaymentsBatchListBE.Items;
                gvPaymentBatchProcessStatus.DataBind();
                MergeRows(gvPaymentBatchProcessStatus);
            }
            else
            {
                hfTotalRecords.Value = objPaymentsBatchListBE.Items.Count.ToString();
                divPaging1.Visible = divPaging2.Visible = false;
                UCPaging1.Visible = UCPaging2.Visible = false;
                gvPaymentBatchProcessStatus.DataSource = new DataTable(); ;
                gvPaymentBatchProcessStatus.DataBind();
            }

        }

        public static void MergeRows(GridView gridView)
        {
            for (int rowIndex = gridView.Rows.Count - 2; rowIndex >= 0; rowIndex--)
            {
                GridViewRow row = gridView.Rows[rowIndex];
                GridViewRow previousRow = gridView.Rows[rowIndex + 1];

                Literal litBatchName = (Literal)row.FindControl("litBatchName");
                Literal litPreviousBatchName = (Literal)previousRow.FindControl("litBatchName");

                Literal lblFromDate = (Literal)row.FindControl("litBatchDate");
                Literal lblPreviousFromDate = (Literal)previousRow.FindControl("litBatchDate");
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    if (litBatchName.Text == litPreviousBatchName.Text && lblFromDate.Text == lblPreviousFromDate.Text)
                    {
                        if (i == 1)//For Batch Name
                        {
                            row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                            previousRow.Cells[i].Visible = false;
                        }
                        if (i == 2)//For Batch Date
                        {
                            row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                            previousRow.Cells[i].Visible = false;
                        }
                        if (i == 12)//For Upload Again
                        {
                            row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                            previousRow.Cells[i].Visible = false;
                        }
                        if (i == 13)//For Close Batch
                        {
                            row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                            previousRow.Cells[i].Visible = false;
                        }
                    }
                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                //throw Ex;
            }
        }

        public string InsersertExcel()
        {
            string ExcelModifiedFileName = string.Empty;
            try
            {
                if (fupUploadTransactions.HasFile)
                {
                    string ExcelFileName = System.IO.Path.GetFileName(fupUploadTransactions.FileName).ToString();//Assinging FileName
                    string ExcelExtension = Path.GetExtension(ExcelFileName);//Storing Extension of file into variable Extension
                    TimeZoneInfo ExcelIND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                    DateTime ExcelcurrentTime = DateTime.Now;
                    DateTime Exceloourtime = TimeZoneInfo.ConvertTime(ExcelcurrentTime, ExcelIND_ZONE);
                    string ExcelCurrentDate = Exceloourtime.ToString("dd-MM-yyyy_hh-mm-ss");
                    ExcelModifiedFileName = Path.GetFileNameWithoutExtension(ExcelFileName) + "_" + ExcelCurrentDate + ExcelExtension;//Setting File Name to store it in database
                    fupUploadTransactions.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["Payments"].ToString()) + ExcelModifiedFileName);//Saving File in to the server.
                }
            }
            catch (Exception ex)
            {
                Message("Error in Excel File Read", UMS_Resource.MESSAGETYPE_ERROR);
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
            return ExcelModifiedFileName;
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}