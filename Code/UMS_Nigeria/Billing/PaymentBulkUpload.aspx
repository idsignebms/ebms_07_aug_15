﻿<%@ Page Title="::Payment Upload::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="PaymentBulkUpload.aspx.cs" Inherits="UMS_Nigeria.Billing.PaymentBulkUpload" %>

<%@ Register Src="../UserControls/Authentication.ascx" TagName="Authentication" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdateProgress ID="UpdateProgress" runat="server">
                <ProgressTemplate>
                    <center>
                        <div id="loading-div" class="pbloading">
                            <div id="title-loading" class="pbtitle">
                                BEDC</div>
                            <center>
                                <img src="../images/loading.GIF" class="pbimg"></center>
                            <p class="pbtxt">
                                Loading Please wait.....</p>
                        </div>
                    </center>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
                PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
            <asp:UpdatePanel ID="upPaymentUpload" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddBookNumber" runat="server" Text="<%$ Resources:Resource, PAYMENT_UPLOAD%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, BATCHNUM%>"></asp:Literal>
                                    <span class="span_star">*</span></label><div class="space">
                                    </div>
                                <asp:TextBox ID="txtBatchNoNew" Style="width: 160px;" runat="server" CssClass="text-box" MaxLength="10" 
                                    placeholder="Enter Batch No" autocomplete="off" onblur="return TextBoxBlurValidationlbl(this,'spanBatchNo','Batch No')"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtBatchNoNew"
                                    FilterType="Numbers" ValidChars=".,">
                                </asp:FilteredTextBoxExtender>
                                <div class="space">
                                </div>
                                <span id="spanBatchNo" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, BATCHDATE%>"></asp:Literal>
                                <span class="span_star">*</span><br />
                                <asp:TextBox ID="txtBatchDate" placeholder="Enter Batch Date" Style="width: 184px;" CssClass="text-box" 
                                    onBlur="DoBlur(this);" onchange="DoBlur(this);" AutoComplete="off" MaxLength="10" runat="server"></asp:TextBox>
                                <asp:Image ID="imgDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                    vertical-align: middle" AlternateText="Calender" runat="server" />
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtBatchDate"
                                    FilterType="Custom,Numbers" ValidChars="/">
                                </asp:FilteredTextBoxExtender>
                                <%--AutoPostBack="True" ontextchanged="txtBatchDate_TextChanged"--%>
                                <span id="spanBatchDate" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, NOTES%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:TextBox ID="txtNotes" placeholder="Enter Notes" CssClass="text-box" MaxLength="200" runat="server" TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, PAYMENTSDOCUPLOAD%>"></asp:Literal>
                                    <span class="span_star">*</span>
                                </label>
                                <div class="space">
                                </div>
                                <asp:FileUpload ID="upDOCExcel" runat="server" />
                                <br />
                                <a href="../Uploads/Payments/PaymentsSampleExcel.zip" target="_blank">
                                    <asp:Label ID="Literal4" runat="server" Text="<%$ Resources:Resource, DOWNLOAD_SAMP_DOC%>"></asp:Label></a>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="inner-box1" style="width: 50%;">
                            <div class="text-inner">
                                <label for="name">
                                    <span class="span_star">
                                        <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, NOTE%>"></asp:Label></span>
                                </label>
                                <div class="space">
                                </div>
                                <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, PROVIDE_DATE_FORMATE%>"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <asp:Button ID="btnNext" CssClass="box_s" Text="Upload Data" runat="server" OnClick="btnNext_Click"
                                OnClientClick="return Validate();" />
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnNext" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
        TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
    </asp:ModalPopupExtender>
    <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
    <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
        <div class="popheader popheaderlblred" id="popgridalert" runat="server">
            <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
        </div>
        <div class="footer popfooterbtnrt">
            <div class="fltr popfooterbtn">
                <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                    CssClass="ok_btn" />
            </div>
            <div class="clear pad_5">
            </div>
        </div>
    </asp:Panel>
    <asp:ModalPopupExtender ID="mpePaymentypopup" runat="server" TargetControlID="btnpopup"
        PopupControlID="pnlConfirmation" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Button ID="btnpopup" runat="server" Text="" Style="display: none;" />
    <asp:Panel runat="server" BorderColor="#CD3333" ID="pnlConfirmation" CssClass="modalPopup"
        Style="display: none;">
        <div class="popheader" style="background-color: #CD3333;">
            <asp:Panel ID="pnlMsgPopup" runat="server" align="center">
                <asp:Literal ID="lblMsgPopup" runat="server" Text="<%$ Resources:UMS_Resource, UPLOADEDFILE_FORMAT%>"></asp:Literal>
            </asp:Panel>
        </div>
        <div class="clear pad_10">
        </div>
        <div class="footer" align="right">
            <asp:Button ID="btnOk" runat="server" Text="OK" CssClass="btn_ok" Style="margin-left: 280px;" />
            <br />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlLoadingUpload" runat="server" Style="display: none;">
        <center>
            <div id="loading-div" class="pbloading">
                <div id="title-loading" class="pbtitle">
                    BEDC</div>
                <center>
                    <img src="../images/loading.GIF" class="pbimg"></center>
                <p class="pbtxt">
                    Loading Please wait.....</p>
            </div>
        </center>
    </asp:Panel>
    <%--==============Escape button script starts==============--%>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/PaymentUpload.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
    <script type="text/javascript">
        window.onload = function () {
            document.onkeydown = function (e) {
                if (e.which == 116 || e.keyCode == 116)
                    return false;
            };
        }
    </script>
</asp:Content>
