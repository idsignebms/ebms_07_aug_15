﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for PaymentUpload
                     
 Developer        : id067-naresh
 Creation Date    : 08-04-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.IO;
using UMS_NigeriaBE;
using Resources;
using iDSignHelper;
using UMS_NigeriaBAL;
using System.Xml;
using Resources;
using System.Net;
using UMS_NigriaDAL;
using System.Collections;
using System.Data.OleDb;
using System.Globalization;

namespace UMS_Nigeria.Billing
{
    public partial class PaymentUpload : System.Web.UI.Page
    {
        #region Members

        CommonMethods _objCommonMethods = new CommonMethods();
        iDsignBAL _objIdsignBal = new iDsignBAL();
        UserManagementBAL objUserManagementBAL = new UserManagementBAL();
        BillReadingsBAL objBillingBAL = new BillReadingsBAL();

        string Key = "PaymentBulkUploads";
        public int PageNum;
        DataTable dtExcel = new DataTable();
        DataTable dtXMl = new DataTable();

        #endregion

        #region Properties

        private string Notes
        {
            get { return txtNotes.Text.Trim(); }
            set { txtNotes.Text = value; }
        }

        private int BatchNo
        {
            get { return Convert.ToInt32(txtBatchNoNew.Text.Trim()); }
            set { txtBatchNoNew.Text = value.ToString(); }
        }

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        public void DownLoadFile(string FileNameWithPath, string ContentType)
        {
            try
            {
                HttpResponse response = HttpContext.Current.Response;
                response.ClearContent();
                response.Clear();
                response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(Server.MapPath(ConfigurationManager.AppSettings["Payments"].ToString()) + "ZIPFILES").Replace(" ", "_") + ";");
                response.ContentType = ContentType;
                response.TransmitFile(FileNameWithPath);
                response.Flush();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            Hashtable htableMaxLength = new Hashtable();
            try
            {
                int count = 0;
                string ExcelModifiedFileName = InsersertExcel();
                if (Path.GetExtension(ExcelModifiedFileName) == ".xml")
                {
                    DataSet ds = new DataSet();
                    ds.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["Payments"].ToString()) + ExcelModifiedFileName);
                    dtExcel = ds.Tables[0];
                }
                else if (Path.GetExtension(ExcelModifiedFileName) == ".xls" || Path.GetExtension(ExcelModifiedFileName) == ".xlsx")
                {
                    string FolderPath = ConfigurationManager.AppSettings["Payments"];
                    string FilePath = Server.MapPath(FolderPath + ExcelModifiedFileName);
                    String cnstr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FilePath + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";//2007 Onwards

                    OleDbConnection oledbConn = new OleDbConnection(cnstr);
                    String[] s = _objCommonMethods.GetExcelSheetNames(FilePath);
                    string strSQL = "SELECT * FROM [" + s[0] + "]";
                    OleDbCommand cmd = new OleDbCommand(strSQL, oledbConn);
                    DataSet ds = new DataSet();
                    OleDbDataAdapter odapt = new OleDbDataAdapter(cmd);
                    odapt.Fill(ds);
                    dtExcel = ds.Tables[0];
                }
                if (dtExcel.Rows.Count > 0)//Validating excel columns.
                {
                    for (int i = 0; i < dtExcel.Columns.Count; i++)
                    {
                        DataSet dsHeaders = new DataSet();
                        dsHeaders.ReadXml(Server.MapPath(ConfigurationSettings.AppSettings["PaymentUploadHeaders"]));
                        if (dtExcel.Columns.Count == dsHeaders.Tables[0].Rows.Count)
                        {
                            string HeaderName = dtExcel.Columns[i].ColumnName;
                            var result = (from x in dsHeaders.Tables[0].AsEnumerable()
                                          where x.Field<string>("Names").Trim() == HeaderName.Trim()
                                          select new
                                          {
                                              Names = x.Field<string>("Names")
                                          }).SingleOrDefault();
                            if (result == null)
                            {
                                count++;
                                lblMsgPopup.Text = Resource.COLUMNS_NOT_MATCHNIG;
                                mpePaymentypopup.Show();
                                break;
                            }
                        }
                        else
                        {
                            count++;
                            lblMsgPopup.Text = Resource.COLUMNS_NOT_MATCHNIG;
                            mpePaymentypopup.Show();
                        }
                    }
                    if (count == 0)//Duplicate account number validation.
                    {
                        //for (int i = dtExcel.Rows.Count - 1; i >= 0; i--)//Commented By Karteek - Because of this 1 row is missing in the DataTable
                        for (int i = 0; i < dtExcel.Rows.Count; i++)
                        {
                            if (dtExcel.Rows[i]["AccountNo"].ToString().Trim() == "")
                                dtExcel.Rows.RemoveAt(i);
                        }

                        Session[Resource.SESSION_PAYMENTUPLOADSDT] = dtExcel;

                        htableMaxLength = _objCommonMethods.getApplicationSettings(Resource.SETTING_GS_ACCOUNT_DUP_ALLOW);
                        if (htableMaxLength["IsDuplicateAllowed"].ToString().Contains("true"))
                        {
                            var duplicates1 = (from x in dtExcel.AsEnumerable()
                                                   .Where(w => w.Field<string>("AccountNo") != string.Empty)
                                                   .GroupBy(r => r.Field<string>("AccountNo"))
                                               select new
                                               {
                                                   Key = x.Key,
                                                   Count = x.Count()
                                               });

                            var result = (from x in duplicates1.AsEnumerable() where x.Count > 1 select x);
                            if (result.Count() > 0)
                            {
                                count++;
                                lblMsgPopup.Text = Resource.DUPLICATE_ACC_NOT_ALLWO;
                                mpePaymentypopup.Show();
                            }
                        }
                    }
                    if (count == 0)
                    {
                        BillingBE _objBillingBe = new BillingBE();
                        _objBillingBe.BatchNo = Convert.ToInt32(txtBatchNoNew.Text);
                        _objBillingBe.BatchDate = _objCommonMethods.Convert_MMDDYY(txtBatchDate.Text);
                        _objBillingBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                        _objBillingBe = _objIdsignBal.DeserializeFromXml<BillingBE>(objBillingBAL.Batch(_objIdsignBal.DoSerialize(_objBillingBe)));
                        if (!_objBillingBe.IsExists)
                        {
                            bool IsAmountPaid = true;
                            bool IsReceivedDate = true;
                            for (int i = 0; i < dtExcel.Rows.Count; i++)
                            {
                                try
                                {
                                    //Amount column validation Start
                                    if (dtExcel.Rows[i]["AmountPaid"] == DBNull.Value)
                                    {
                                        lblMsgPopup.Text = Resource.AMOUNT_COL_NOT_NULL;
                                        IsAmountPaid = false;
                                    }
                                    else if (Convert.ToDecimal((string.IsNullOrEmpty(Convert.ToString(dtExcel.Rows[i]["AmountPaid"])) ? 0 : dtExcel.Rows[i]["AmountPaid"])) < 0)//Payment can allow decimal values also -- Suresh-ID031
                                    {
                                        if (Convert.ToDecimal(dtExcel.Rows[i]["AmountPaid"]) == 0)
                                            lblMsgPopup.Text = Resource.PAYMENT_AMOUNT_INCRT;
                                        else
                                            lblMsgPopup.Text = Resource.AMTPD_NEG_VALUE;

                                        IsAmountPaid = false;
                                    }

                                    if (!IsAmountPaid)
                                    {
                                        count++;
                                        mpePaymentypopup.Show();
                                        break;
                                    }
                                    //Amount column validation END
                                }
                                catch (FormatException)//If payment column input is null or other then numeric value.--Neeraj ID077
                                {
                                    if (string.IsNullOrEmpty(Convert.ToString(dtExcel.Rows[i]["AmountPaid"]).Trim()))
                                    {
                                        lblMsgPopup.Text = Resource.AMOUNT_COL_NOT_NULL;
                                    }
                                    else
                                    {
                                        lblMsgPopup.Text = Resource.INPUT_INCORT;
                                    }
                                    count++;
                                    mpePaymentypopup.Show();
                                    break;
                                }
                                try
                                {
                                    //ReceiptNO column validation Start
                                    if (string.IsNullOrEmpty(dtExcel.Rows[i]["ReceiptNO"].ToString()))//fields is numberic or not --Neeraj ID077
                                    {
                                        count++;
                                        lblMsgPopup.Text = Resource.RECEIPT_NO_NOTNULL;
                                        mpePaymentypopup.Show();
                                        break;
                                    }
                                    //ReceiptNO column validation End
                                }
                                catch (FormatException)//If payment column input is null or other then numeric value.--Neeraj ID077
                                {
                                    count++;
                                    lblMsgPopup.Text = Resource.INPUT_INCORT;
                                    mpePaymentypopup.Show();
                                    break;
                                }
                                try
                                {
                                    //Date Validation Start
                                    int[] calDate = new int[3];
                                    lblMsgPopup.Text = Resource.UPLOAD_DATE_INCORRECT;
                                    if (!string.IsNullOrEmpty(dtExcel.Rows[i]["ReceivedDate"].ToString()))//fields is numberic or not --Neeraj ID077
                                    {
                                        string receivedDate = DateTime.ParseExact(dtExcel.Rows[i]["ReceivedDate"].ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy"); 
                                        //    dtExcel.Rows[i]["ReceivedDate"].ToString();
                                        string format = "d";
                                        CultureInfo provider = CultureInfo.InvariantCulture;
                                        DateTime result = DateTime.ParseExact(receivedDate, format, provider);
                                        calDate[0] = result.Year;
                                        if (calDate[0] < DateTime.Now.Year)//Validating year with less than current year --Neeraj ID077
                                        {
                                            if (calDate[0] < DateTime.Now.Year - 5)//Validating year with less than current year - 5 yrs --Neeraj ID077
                                            {
                                                IsReceivedDate = false;
                                                lblMsgPopup.Text = Resource.ACCEPTS_LAST_FIV_YR_DT;
                                            }
                                        }
                                        else if (calDate[0] == DateTime.Now.Year)//Validating year with equal current year --Raja ID065
                                        {
                                            calDate[1] = result.Month;
                                            if (calDate[1] > DateTime.Now.Month)//Validating year with greater than month --Raja ID065
                                            {
                                                IsReceivedDate = false;
                                                lblMsgPopup.Text = Resource.SOME_UPLOADED_DATE_HAS_FUTURE_DATE;
                                            }
                                            else if (calDate[1] == DateTime.Now.Month) //Validating year with current month --Raja ID065
                                            {
                                                calDate[2] = result.Day;
                                                if (calDate[2] > DateTime.Now.Day) //Validating year with current month and greater than day --Raja ID065
                                                {
                                                    IsReceivedDate = false;
                                                    lblMsgPopup.Text = Resource.SOME_UPLOADED_DATE_HAS_FUTURE_DATE;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            IsReceivedDate = false;
                                            lblMsgPopup.Text = Resource.SOME_UPLOADED_DATE_HAS_FUTURE_DATE;
                                        }
                                    }
                                    else
                                    {//If value is zero or less than loop will terminate with notification.--Neeraj ID077
                                        IsReceivedDate = false;
                                        lblMsgPopup.Text = Resource.DATE_CANNOT_BE_NULL;
                                    }
                                    //Date Validation End

                                    if (!IsReceivedDate)
                                    {
                                        count++;
                                        mpePaymentypopup.Show();
                                        break;
                                    }
                                }
                                catch (FormatException)//If payment column input is null or other then numeric value.--Neeraj ID077
                                {
                                    count++;
                                    lblMsgPopup.Text = Resource.INPUT_INCORT;
                                    mpePaymentypopup.Show();
                                    break;
                                }
                            }
                            if (count == 0)
                            {
                                Session["BatchDetails"] = BatchNo + "^" + Notes + "^" + _objCommonMethods.Convert_MMDDYY(txtBatchDate.Text) + "^" + ExcelModifiedFileName;
                                Response.Redirect(UMS_Resource.DOCUMENT_PREVIEW + "?" + UMS_Resource.DOCUMENT_BATCHNO + "=" + _objIdsignBal.Encrypt(BatchNo.ToString()), false);
                                //Response.Redirect(UMS_Resource.DOCUMENT_PREVIEW + "?" + UMS_Resource.DOCUMENT_REDIRECTDETAILS + "=" + _objIdsignBal.Encrypt(ExcelModifiedFileName) + "&" + UMS_Resource.DOCUMENT_BATCHNO + "=" + _objIdsignBal.Encrypt(txtBatchNoNew.Text), false);
                            }
                        }
                        else
                        {
                            Message("Please try another batch number.", UMS_Resource.MESSAGETYPE_ERROR);
                        }
                    }
                }
                else
                {
                    lblMsgPopup.Text = Resource.COLUMNS_NOT_MATCHNIG;
                    mpePaymentypopup.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //protected void btnNext_Click(object sender, EventArgs e)
        //{
        //    Hashtable htableMaxLength = new Hashtable();
        //    try
        //    {
        //        int count = 0;
        //        string ExcelModifiedFileName = InsersertExcel();
        //        if (Path.GetExtension(ExcelModifiedFileName) == ".xml")
        //        {
        //            DataSet ds = new DataSet();
        //            ds.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["Payments"].ToString()) + ExcelModifiedFileName);
        //            dtExcel = ds.Tables[0];
        //        }
        //        else if (Path.GetExtension(ExcelModifiedFileName) == ".xls" || Path.GetExtension(ExcelModifiedFileName) == ".xlsx")
        //        {
        //            string FolderPath = ConfigurationManager.AppSettings["Payments"];
        //            string FilePath = Server.MapPath(FolderPath + ExcelModifiedFileName);
        //            String cnstr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FilePath + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";//2007 Onwards

        //            OleDbConnection oledbConn = new OleDbConnection(cnstr);
        //            String[] s = _objCommonMethods.GetExcelSheetNames(FilePath);
        //            string strSQL = "SELECT * FROM [" + s[0] + "]";
        //            OleDbCommand cmd = new OleDbCommand(strSQL, oledbConn);
        //            DataSet ds = new DataSet();
        //            OleDbDataAdapter odapt = new OleDbDataAdapter(cmd);
        //            odapt.Fill(ds);
        //            dtExcel = ds.Tables[0];
        //        }
        //        if (dtExcel.Rows.Count > 0)//Validating excel columns.
        //        {
        //            for (int i = 0; i < dtExcel.Columns.Count; i++)
        //            {
        //                DataSet dsHeaders = new DataSet();
        //                dsHeaders.ReadXml(Server.MapPath(ConfigurationSettings.AppSettings["PaymentUploadHeaders"]));
        //                if (dtExcel.Columns.Count == dsHeaders.Tables[0].Rows.Count)
        //                {
        //                    string HeaderName = dtExcel.Columns[i].ColumnName;
        //                    var result = (from x in dsHeaders.Tables[0].AsEnumerable()
        //                                  where x.Field<string>("Names").Trim() == HeaderName.Trim()
        //                                  select new
        //                                  {
        //                                      Names = x.Field<string>("Names")
        //                                  }).SingleOrDefault();
        //                    if (result == null)
        //                    {
        //                        count++;
        //                        lblMsgPopup.Text = Resource.COLUMNS_NOT_MATCHNIG;
        //                        mpePaymentypopup.Show();
        //                        break;
        //                    }
        //                }
        //                else
        //                {
        //                    count++;
        //                    lblMsgPopup.Text = Resource.COLUMNS_NOT_MATCHNIG;
        //                    mpePaymentypopup.Show();
        //                }
        //            }
        //            if (count == 0)//Duplicate account number validation.
        //            {
        //                for (int i = dtExcel.Rows.Count - 1; i >= 0; i--)
        //                {
        //                    if (dtExcel.Rows[i]["AccountNo"].ToString().Trim() == "")
        //                        dtExcel.Rows.RemoveAt(i);
        //                }

        //                Session[Resource.SESSION_PAYMENTUPLOADSDT] = dtExcel;

        //                htableMaxLength = _objCommonMethods.getApplicationSettings(Resource.SETTING_GS_ACCOUNT_DUP_ALLOW);
        //                if (htableMaxLength["IsDuplicateAllowed"].ToString().Contains("true"))
        //                {
        //                    var duplicates1 = (from x in dtExcel.AsEnumerable()
        //                                           .Where(w => w.Field<string>("AccountNo") != string.Empty)
        //                                           .GroupBy(r => r.Field<string>("AccountNo"))
        //                                       select new
        //                                       {
        //                                           Key = x.Key,
        //                                           Count = x.Count()
        //                                       });

        //                    var result = (from x in duplicates1.AsEnumerable() where x.Count > 1 select x);
        //                    if (result.Count() > 0)
        //                    {
        //                        count++;
        //                        lblMsgPopup.Text = Resource.DUPLICATE_ACC_NOT_ALLWO;
        //                        mpePaymentypopup.Show();
        //                    }
        //                }
        //            }
        //            if (count == 0)//Amount column validation
        //            {
        //                for (int i = 0; i < dtExcel.Rows.Count; i++)
        //                {
        //                    try
        //                    {
        //                        if (dtExcel.Rows[i]["AmountPaid"] != DBNull.Value && Convert.ToDecimal((string.IsNullOrEmpty(Convert.ToString(dtExcel.Rows[i]["AmountPaid"])) ? 0 : dtExcel.Rows[i]["AmountPaid"])) > 0)//Payment can allow decimal values also -- Suresh-ID031
        //                        {
        //                            //No Action
        //                        }
        //                        else
        //                        {//If value is zero or less than loop will terminate with notification.--Neeraj ID077
        //                            if (dtExcel.Rows[i]["AmountPaid"] == DBNull.Value)
        //                                lblMsgPopup.Text = Resource.AMOUNT_COL_NOT_NULL;
        //                            else if (Convert.ToDecimal((string.IsNullOrEmpty(Convert.ToString(dtExcel.Rows[i]["AmountPaid"])) ? 0 : dtExcel.Rows[i]["AmountPaid"])) < 0)//Payment can allow decimal values also -- Suresh-ID031
        //                                lblMsgPopup.Text = Resource.AMTPD_NEG_VALUE;
        //                            else
        //                                lblMsgPopup.Text = Resource.PAYMENT_AMOUNT_INCRT;
        //                            count++;

        //                            mpePaymentypopup.Show();
        //                            break;
        //                        }
        //                    }
        //                    catch (FormatException)//If payment column input is null or other then numeric value.--Neeraj ID077
        //                    {
        //                        count++;
        //                        if (string.IsNullOrEmpty(Convert.ToString(dtExcel.Rows[i]["AmountPaid"]).Trim()))
        //                            lblMsgPopup.Text = Resource.AMOUNT_COL_NOT_NULL;
        //                        else
        //                            lblMsgPopup.Text = Resource.INPUT_INCORT;
        //                        mpePaymentypopup.Show();
        //                    }
        //                }
        //            }
        //            if (count == 0)//Receipt column validation
        //            {
        //                for (int i = 0; i < dtExcel.Rows.Count; i++)
        //                {
        //                    try
        //                    {
        //                        if (!string.IsNullOrEmpty(dtExcel.Rows[i]["ReceiptNO"].ToString()))//fields is numberic or not --Neeraj ID077
        //                        {
        //                            //No Action
        //                        }
        //                        else
        //                        {//If value is zero or less than loop will terminate with notification.--Neeraj ID077
        //                            count++;
        //                            lblMsgPopup.Text = Resource.RECEIPT_NO_NOTNULL;
        //                            mpePaymentypopup.Show();
        //                            break;
        //                        }
        //                    }
        //                    catch (FormatException)//If payment column input is null or other then numeric value.--Neeraj ID077
        //                    {
        //                        count++;
        //                        lblMsgPopup.Text = Resource.INPUT_INCORT;
        //                        mpePaymentypopup.Show();
        //                    }
        //                }
        //            }
        //            if (count == 0)//Date column validation
        //            {
        //                int[] calDate = new int[3];
        //                for (int i = 0; i < dtExcel.Rows.Count; i++)
        //                {
        //                    try
        //                    {
        //                        lblMsgPopup.Text = Resource.UPLOAD_DATE_INCORRECT;
        //                        if (!string.IsNullOrEmpty(dtExcel.Rows[i]["ReceivedDate"].ToString()))//fields is numberic or not --Neeraj ID077
        //                        {
        //                            string receivedDate = dtExcel.Rows[i]["ReceivedDate"].ToString();
        //                            string format = "d";
        //                            CultureInfo provider = CultureInfo.InvariantCulture;
        //                            DateTime result = DateTime.ParseExact(receivedDate, format, provider);
        //                            calDate[0] = result.Year;
        //                            if (calDate[0] < DateTime.Now.Year)//Validating year with less than current year --Neeraj ID077
        //                            {
        //                                if (calDate[0] < DateTime.Now.Year - 5)//Validating year with less than current year - 5 yrs --Neeraj ID077
        //                                {
        //                                    count++;
        //                                    lblMsgPopup.Text = Resource.ACCEPTS_LAST_FIV_YR_DT;
        //                                    mpePaymentypopup.Show();
        //                                    break;
        //                                }
        //                            }
        //                            else if (calDate[0] == DateTime.Now.Year)//Validating year with equal current year --Raja ID065
        //                            {
        //                                calDate[1] = result.Month;
        //                                if (calDate[1] > DateTime.Now.Month)//Validating year with greater than month --Raja ID065
        //                                {
        //                                    count++;
        //                                    lblMsgPopup.Text = Resource.SOME_UPLOADED_DATE_HAS_FUTURE_DATE;
        //                                    mpePaymentypopup.Show();
        //                                    break;
        //                                }
        //                                else if (calDate[1] == DateTime.Now.Month) //Validating year with current month --Raja ID065
        //                                {
        //                                    calDate[2] = result.Day;
        //                                    if (calDate[2] > DateTime.Now.Day) //Validating year with current month and greater than day --Raja ID065
        //                                    {
        //                                        count++;
        //                                        lblMsgPopup.Text = Resource.SOME_UPLOADED_DATE_HAS_FUTURE_DATE;
        //                                        mpePaymentypopup.Show();
        //                                        break;
        //                                    }
        //                                }
        //                            }
        //                            else
        //                            {
        //                                count++;
        //                                lblMsgPopup.Text = Resource.SOME_UPLOADED_DATE_HAS_FUTURE_DATE;
        //                                mpePaymentypopup.Show();
        //                                break;
        //                            }
        //                        }
        //                        else
        //                        {//If value is zero or less than loop will terminate with notification.--Neeraj ID077
        //                            count++;
        //                            lblMsgPopup.Text = Resource.DATE_CANNOT_BE_NULL;
        //                            mpePaymentypopup.Show();
        //                            break;
        //                        }
        //                    }
        //                    catch (FormatException ex)//If payment column input is null or other then numeric value.--Neeraj ID077
        //                    {
        //                        count++;
        //                        mpePaymentypopup.Show();
        //                    }
        //                }
        //            }
        //            if (count == 0)//If validation pass.
        //            {
        //                try
        //                {
        //                    BillingBE _objBillingBe = new BillingBE();
        //                    _objBillingBe.BatchNo = Convert.ToInt32(txtBatchNoNew.Text);
        //                    _objBillingBe.BatchDate = _objCommonMethods.Convert_MMDDYY(txtBatchDate.Text);
        //                    _objBillingBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
        //                    _objBillingBe = _objIdsignBal.DeserializeFromXml<BillingBE>(objBillingBAL.Batch(_objIdsignBal.DoSerialize(_objBillingBe)));
        //                    if (!_objBillingBe.IsExists)
        //                    {
        //                        Session["BatchDetails"] = BatchNo + "^" + Notes + "^" + _objCommonMethods.Convert_MMDDYY(txtBatchDate.Text) + "^" + ExcelModifiedFileName;
        //                        Response.Redirect(UMS_Resource.DOCUMENT_PREVIEW + "?" + UMS_Resource.DOCUMENT_BATCHNO + "=" + _objIdsignBal.Encrypt(BatchNo.ToString()), false);
        //                        //Response.Redirect(UMS_Resource.DOCUMENT_PREVIEW + "?" + UMS_Resource.DOCUMENT_REDIRECTDETAILS + "=" + _objIdsignBal.Encrypt(ExcelModifiedFileName) + "&" + UMS_Resource.DOCUMENT_BATCHNO + "=" + _objIdsignBal.Encrypt(txtBatchNoNew.Text), false);
        //                    }
        //                    else
        //                    {
        //                        Message("Please try another batch number.", UMS_Resource.MESSAGETYPE_ERROR);
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //                    try
        //                    {
        //                        _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //                    }
        //                    catch (Exception logex)
        //                    {

        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            lblMsgPopup.Text = Resource.COLUMNS_NOT_MATCHNIG;
        //            mpePaymentypopup.Show();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                lblMessage.Text = pnlMessage.CssClass = string.Empty;
                if (!IsPostBack)
                {
                    string path = string.Empty;
                    path = _objCommonMethods.GetPagePath(this.Request.Url);
                    if (_objCommonMethods.IsAccess(path))
                    {

                    }
                    else { Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }
                }
            }
            else
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        //Check the user password correct or not.
        protected void btnAuthenticarion_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPassword.Text != "")
                {
                    XmlDocument xml = new XmlDocument();
                    LoginPageBE objLoginPageBE = new LoginPageBE();
                    LoginPageBAL objLoginPageBAL = new LoginPageBAL();
                    objLoginPageBE.UserName = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objLoginPageBE.Password = _objIdsignBal.AESEncryptString(txtPassword.Text);
                    xml = objLoginPageBAL.Login(objLoginPageBE, ReturnType.Get);
                    objLoginPageBE = _objIdsignBal.DeserializeFromXml<LoginPageBE>(xml);

                    if (objLoginPageBE.IsSuccess)
                    {
                        ModalPopupExtenderLogin.Hide();
                    }
                    else
                    {
                        LabelStatusLogin.Visible = true;
                        LabelStatusLogin.Text = Resource.AUTHENTICATIONFAILED;
                        txtPassword.Focus();
                        ModalPopupExtenderLogin.Show();
                    }
                }
                else
                {
                    LabelStatusLogin.Visible = true;
                    LabelStatusLogin.Text = "Enter Password";
                    txtPassword.Focus();
                    ModalPopupExtenderLogin.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        public string InsersertExcel()
        {
            try
            {
                string ExcelModifiedFileName = string.Empty;
                BillingBE objBillingBE = new BillingBE();
                objBillingBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objBillingBE.Notes = this.Notes;
                objBillingBE.BatchNo = this.BatchNo;
                if (upDOCExcel.HasFile)
                {
                    string ExcelFileName = System.IO.Path.GetFileName(upDOCExcel.FileName).ToString();//Assinging FileName
                    string ExcelExtension = Path.GetExtension(ExcelFileName);//Storing Extension of file into variable Extension
                    TimeZoneInfo ExcelIND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                    DateTime ExcelcurrentTime = DateTime.Now;
                    DateTime Exceloourtime = TimeZoneInfo.ConvertTime(ExcelcurrentTime, ExcelIND_ZONE);
                    string ExcelCurrentDate = Exceloourtime.ToString("dd-MM-yyyy_hh-mm-ss");
                    ExcelModifiedFileName = Path.GetFileNameWithoutExtension(ExcelFileName) + "_" + ExcelCurrentDate + ExcelExtension;//Setting File Name to store it in database
                    upDOCExcel.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["Payments"].ToString()) + ExcelModifiedFileName);//Saving File in to the server.
                }
                return ExcelModifiedFileName;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public bool GetBatchNo(string batchNo)
        {
            return false;
        }

        #endregion
    }
}