﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentsToAccountNo.aspx.cs"
    Title=":: Customer Advance Payments ::" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" Inherits="UMS_Nigeria.Billing.PaymentsToAccountNo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="contentBody" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server" class="modalBackground">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <div class="inner-sec">
            <asp:UpdatePanel ID="UpdatePanelTotal" runat="server" ChildrenAsTriggers="true">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <%--<asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, CUST_PAYS_ACC_WISE%>"></asp:Literal>--%>
                            <asp:Literal ID="litAddState" runat="server" Text="Customer Advance Payments"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litAccountNo" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                                    <asp:Literal ID="Literal4" runat="server" Text="/ Old Account No"></asp:Literal>
                                    <span class="span_star">*</span>
                                </label>
                                <br />
                                <asp:TextBox ID="txtAccountNo" CssClass="text-box" runat="server" MaxLength="15"
                                    onchange="GetRecords()" placeholder="Global Account No /Old Account No"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtAccountNo"
                                    FilterType="Numbers" ValidChars="">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanAccountNo" class="span_color"></span>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <asp:Button ID="btnSearch" OnClientClick="return validateGo();" Text="<%$ Resources:Resource, GET_CUSTOMER_DETAILS%>"
                                OnClick="btnSearch_Click" CssClass="box_s" runat="server" />
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div id="divCustomerDetails" runat="server" visible="false">
                        <div class="out-bor_a">
                            <div class="inner-sec">
                                <div class="heading" style="padding-left: 16px; padding-top: 17px;">
                                    <asp:Literal ID="Literal213" runat="server" Text="<%$ Resources:Resource, CUSTOMER_DETAILS%>"></asp:Literal>
                                </div>
                                <div class="out-bor_aTwo">
                                    <table class="customerdetailsTamle">
                                        <tr>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label runat="server" ID="lblAccNoAndGlobalAccNo"></asp:Label>
                                                <asp:Label runat="server" Visible="false" ID="lblGlobalAccountno"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label runat="server" ID="lblOldAccountNo"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label runat="server" ID="lblServiceAddress"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label runat="server" ID="lblName"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label ID="lblMeterNo" runat="server"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label ID="lblTariff" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal215" runat="server" Text="<%$ Resources:Resource, LAST_BILL_GENERATED_DATE%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label ID="lblLastBillGeneratedDate" runat="server"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:Resource, LAST_PAID_DATE%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label ID="lblLastPaidDate" runat="server"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:Resource, LAST_PAID_AMOUNT%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label ID="lblLastPaidAmount" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal216" runat="server" Text="<%$ Resources:Resource, OUT_STANDING_AMT%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label ID="lblOutStanding" runat="server"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, LAST_BILL_AMT%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label ID="lblLastBillAmount" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="inner-box">
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, PAID_AMOUNT%>"></asp:Literal>
                                            <span class="span_star">*</span>
                                        </label>
                                        <br />
                                        <asp:TextBox ID="txtAmount" CssClass="text-box" runat="server" MaxLength="10" onkeypress="return isNumberKey(event,this)"
                                            onblur="return ValidatePaidAmount()" placeholder="Enter Amount" onkeyup="GetCurrencyFormate(this);"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="ftbTxtAnotherContactNo" runat="server" TargetControlID="txtAmount"
                                            FilterType="Numbers, Custom" ValidChars=".,">
                                        </asp:FilteredTextBoxExtender>
                                        <br />
                                        <span id="spanPaidAmount" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box2">
                                    <div class="text-inner">
                                        <asp:Literal ID="Literal17" runat="server" Text="Payment Received Date"></asp:Literal><span
                                            class="span_star">*</span><br />
                                        <asp:TextBox CssClass="text-box" ID="txtPaidDate" autocomplete="off" onblur="return TextBoxBlurValidationlbl(this,'spanPaidDate','Payment Received Date')"
                                            onchange="return TextBoxBlurValidationlbl(this,'spanPaidDate','Payment Received Date')"
                                            MaxLength="10" Width="186px" placeholder="Payment Received Date" runat="server"></asp:TextBox>
                                        <asp:Image ID="imgMeterChangedDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                            vertical-align: middle" AlternateText="Calender" runat="server" />
                                        <asp:FilteredTextBoxExtender ID="ftbNextDate" runat="server" TargetControlID="txtPaidDate"
                                            FilterType="Custom,Numbers" ValidChars="/">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanPaidDate" class="span_color"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div class="box_total">
                                <div class="box_total_a">
                                    <asp:Button ID="btnGo" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                        OnClick="btnGo_Click" CssClass="box_s" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="PanelAlert"
                        TargetControlID="btnAlertDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnAlertOk">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnAlertDeActivate" runat="server" Text="Button" CssClass="popbtn" />
                    <asp:Panel ID="PanelAlert" runat="server" CssClass="modalPopup" class="poppnl" Style="display: none;">
                        <div class="popheader popheaderlblred" id="pop" runat="server">
                            <asp:Label ID="lblAlertMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnAlertOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).ready(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
            DatePicker($('#<%=txtPaidDate.ClientID %>'), $('#<%=imgMeterChangedDate.ClientID %>'));
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
            DatePicker($('#<%=txtPaidDate.ClientID %>'), $('#<%=imgMeterChangedDate.ClientID %>'));
        });

    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script type="text/javascript" src="../JavaScript/CommonValidations.js"></script>
    <script src="../JavaScript/PageValidations/PaymentsToAccountNo.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
