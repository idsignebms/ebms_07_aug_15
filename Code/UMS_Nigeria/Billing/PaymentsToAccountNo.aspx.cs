﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using iDSignHelper;
using Resources;
using System.Xml;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using System.Configuration;

namespace UMS_Nigeria.Billing
{
    public partial class PaymentsToAccountNo : System.Web.UI.Page
    {
        #region Members

        CommonMethods _ObjCommonMethods = new CommonMethods();
        iDsignBAL _objiDsignBAL = new iDsignBAL();
        PaymentsBal _objPaymentsBal = new PaymentsBal();
        ReportsBal objReportsBal = new ReportsBal();
        MastersBAL _ObjMastersBAL = new MastersBAL();
        public string Key = "PaymentsToAccount";

        #endregion

        #region Properties

        public string AccountNo
        {
            get { return string.IsNullOrEmpty(txtAccountNo.Text.Trim()) ? string.Empty : txtAccountNo.Text.Trim(); }
        }

        public decimal PaidAmount
        {
            get { return string.IsNullOrEmpty(txtAmount.Text.Trim()) ? 0 : Convert.ToDecimal(txtAmount.Text.Replace(",", string.Empty).Trim()); }
        }

        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {

            }
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                InsertPayments();
                ClearControls();
            }
            catch (Exception ex)
            {
                _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
                objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
                objLedgerBe.Flag = 1;

                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    objLedgerBe.BUID = string.Empty;

                objLedgerBe = _objiDsignBAL.DeserializeFromXml<RptCustomerLedgerBe>(objReportsBal.GetLedger(objLedgerBe, ReturnType.Single));

                if (objLedgerBe.IsSuccess)
                {
                    if (objLedgerBe.IsCustExistsInOtherBU)
                    {
                        ClearControls();
                        lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                        mpeAlert.Show();
                    }
                    else if (objLedgerBe.ActiveStatusId == 4)
                    {
                        ClearControls();
                        _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, "Customer status is Closed", pnlMessage, lblMessage);
                    }
                    else if (objLedgerBe.CustomerTypeId == 3)//check the customer is prepaid or not
                    {
                        lblAlertMsg.Text = "Customer is Prepaid customer.";
                        mpeAlert.Show();
                        ClearControls();
                    }
                    else
                    {
                        PaymentsBe objPaymentsBe = new PaymentsBe();
                        objPaymentsBe.AccountNo = txtAccountNo.Text;
                        objPaymentsBe = _objiDsignBAL.DeserializeFromXml<PaymentsBe>(_objPaymentsBal.GetDetails(objPaymentsBe, ReturnType.Get));
                        if (objPaymentsBe != null)
                        {
                            if (objPaymentsBe.IsExists)
                            {
                                divCustomerDetails.Visible = true;
                                lblGlobalAccountno.Text = objPaymentsBe.GlobalAccountNumber;
                                lblAccNoAndGlobalAccNo.Text = objPaymentsBe.AccNoAndGlobalAccNo;
                                lblName.Text = objPaymentsBe.Name;
                                lblOldAccountNo.Text = objPaymentsBe.OldAccountNo;
                                lblServiceAddress.Text = objPaymentsBe.ServiceAddress;
                                lblTariff.Text = objPaymentsBe.ClassName;
                                lblMeterNo.Text = objPaymentsBe.MeterNumber;
                                lblOutStanding.Text = _ObjCommonMethods.GetCurrencyFormat(objPaymentsBe.OutStandingAmount, 2, Constants.MILLION_Format).ToString();
                                lblLastPaidDate.Text = objPaymentsBe.LastPaidDate;
                                lblLastPaidAmount.Text = _ObjCommonMethods.GetCurrencyFormat(objPaymentsBe.LastPaidAmount, 2, Constants.MILLION_Format).ToString();
                                lblLastBillGeneratedDate.Text = objPaymentsBe.LastBillGeneratedDate;
                                lblLastBillAmount.Text = _ObjCommonMethods.GetCurrencyFormat(objPaymentsBe.LastBillAmount, 2, Constants.MILLION_Format).ToString();
                            }
                        }
                        else
                        {
                            ClearControls();
                            _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.CUSTOMER_NOT_FOUND_MSG, pnlMessage, lblMessage);
                        }
                    }
                }
                else if (objLedgerBe.ActiveStatusId == 4)
                {
                    ClearControls();
                    _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, "Customer status is Closed", pnlMessage, lblMessage);
                }
                else
                {
                    ClearControls();
                    _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.CUSTOMER_NOT_FOUND_MSG, pnlMessage, lblMessage);
                }
            }
            catch (Exception ex)
            {
                _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void ClearControls()
        {
            divCustomerDetails.Visible = false;
            txtAccountNo.Text = txtAmount.Text = txtPaidDate.Text = string.Empty;
        }

        private void InsertPayments()
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                XmlDocument xmlResult = new XmlDocument();
                objMastersBE.AccountNo = lblGlobalAccountno.Text;
                objMastersBE.PaymentModeId = 1;//Cash-- need to make input from interface
                objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.PaidAmount = this.PaidAmount;
                objMastersBE.PaymentRecievedDate = _ObjCommonMethods.Convert_MMDDYY(txtPaidDate.Text);
                objMastersBE.PaymentType = (int)PaymentType.CustomerPayment;
                objMastersBE.FunctionId = (int)EnumApprovalFunctions.PaymentEntry;
                objMastersBE.PaymentFromId = (int)PaymentFrom.AdvancePayment;
                if (!string.IsNullOrEmpty(Session[UMS_Resource.SESSION_USER_BUID].ToString()))
                    objMastersBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    objMastersBE.BU_ID = string.Empty;

                if (_ObjCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.PaymentEntry, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString()))
                {
                    objMastersBE.ApprovalStatusId = (int)EnumApprovalStatus.Approved;
                    objMastersBE.IsFinalApproval = true;
                    lblAlertMsg.Text = Resource.MODIFICATIONS_UPDATED;
                    objMastersBE = _objiDsignBAL.DeserializeFromXml<MastersBE>(_ObjMastersBAL.PaymentEntry(objMastersBE, Statement.Insert));
                    SendMailAndMsg();//This method is for sending messages & Mails after approval
                }
                else
                {
                    objMastersBE.ApprovalStatusId = (int)EnumApprovalStatus.Process;
                    lblAlertMsg.Text = Resource.APPROVAL_TARIFF_CHANGE;
                    objMastersBE = _objiDsignBAL.DeserializeFromXml<MastersBE>(_ObjMastersBAL.PaymentEntry(objMastersBE, Statement.Insert));
                }

                if (objMastersBE.IsSuccess)
                {
                    pop.Attributes.Add("class", "popheader popheaderlblgreen");
                    mpeAlert.Show();

                }
                else
                    Message(UMS_Resource.PAYMENT_ENTRY_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void SendMailAndMsg()
        {
            string GlobalAccountNo = lblGlobalAccountno.Text;
            string SuccessMessage = string.Empty;
            SuccessMessage = "Bill generated successfully.";
            CommunicationFeaturesBE objCommunicationFeaturesBE = new CommunicationFeaturesBE();
            objCommunicationFeaturesBE.CommunicationFeaturesID = (int)CommunicationFeatures.Payments;

            try
            {
                if (_ObjCommonMethods.IsEmailFeatureEnabled(objCommunicationFeaturesBE))
                {
                    CommunicationFeaturesBE _objMailBE = new CommunicationFeaturesBE();
                    _objMailBE.GlobalAccountNumber = Convert.ToString(GlobalAccountNo);
                    XmlDocument xml = _ObjCommonMethods.GetDetails(_objMailBE, ReturnType.Bulk);
                    _objMailBE = _objiDsignBAL.DeserializeFromXml<CommunicationFeaturesBE>(xml);

                    if (_objMailBE.EmailId == "0")
                    {
                        SuccessMessage += "Customer not having Mail Id.";
                    }
                    else if (!string.IsNullOrEmpty(_objMailBE.EmailId))
                    {
                        if (_objMailBE.IsSuccess)
                        {
                            string amount = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(_objMailBE.PaidAmount), 2, Constants.MILLION_Format);
                            string outstandingamount = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(_objMailBE.OutStandingAmount), 2, Constants.MILLION_Format);
                            string htmlBody = string.Empty;
                            htmlBody = _ObjCommonMethods.GetMailCommTemp(CommunicationFeatures.Payments, Convert.ToString(_objMailBE.Name), string.Empty, Convert.ToString(GlobalAccountNo), Convert.ToString(_objMailBE.MeterNumber), string.Empty, Convert.ToString(outstandingamount), string.Empty, Convert.ToString(amount), Convert.ToString(_objMailBE.PaidDate), string.Empty, "Dear Customer, bill payment of =N= " + Convert.ToString(amount) + " for your BEDC acc " + Convert.ToString(GlobalAccountNo) + " has been processed successfully. <br/>Txn Id: " + Convert.ToString(_objMailBE.TransactionId) + ".");

                            _ObjCommonMethods.sendmail(htmlBody, ConfigurationManager.AppSettings["SMTPUsername"].ToString(), "Payment Received from acc " + Convert.ToString(GlobalAccountNo), _objMailBE.EmailId, string.Empty);
                            SuccessMessage += "Mail sent successfully.";
                        }
                    }
                    else
                    {
                        SuccessMessage += "Mail not sent to this customer.";
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

            try
            {
                if (_ObjCommonMethods.IsSMSFeatureEnabled(objCommunicationFeaturesBE))
                {
                    CommunicationFeaturesBE _objSmsBE = new CommunicationFeaturesBE();
                    _objSmsBE.GlobalAccountNumber = Convert.ToString(GlobalAccountNo);
                    XmlDocument xml = _ObjCommonMethods.GetDetails(_objSmsBE, ReturnType.Bulk);
                    _objSmsBE = _objiDsignBAL.DeserializeFromXml<CommunicationFeaturesBE>(xml);

                    if (_objSmsBE.MobileNo == "0")
                    {
                        SuccessMessage += "Customer not having Mobile No.";
                    }
                    else if (!string.IsNullOrEmpty(_objSmsBE.MobileNo) && _objSmsBE.MobileNo.Length == 10)
                    {
                        if (_objSmsBE.IsSuccess)
                        {
                            string amount = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(_objSmsBE.PaidAmount), 2, Constants.MILLION_Format);
                            string Messsage = string.Empty;
                            Messsage = _ObjCommonMethods.GetSMSCommTemp(CommunicationFeatures.Payments, Convert.ToString(GlobalAccountNo), string.Empty, Convert.ToString(amount), string.Empty, Convert.ToString(_objSmsBE.TransactionId), string.Empty);

                            _ObjCommonMethods.sendSMS(_objSmsBE.MobileNo, Messsage, ConfigurationManager.AppSettings["SMTPUsername"].ToString());
                            SuccessMessage += "Message sent successfully.";
                        }
                    }
                    else
                    {
                        SuccessMessage += "Message not sent to this customer.";
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //private void InsertPayments()
        //{
        //    MastersBE objMastersBE = new MastersBE();
        //    MastersBAL _ObjMastersBAL = new MastersBAL();
        //    XmlDocument xmlResult = new XmlDocument();
        //    objMastersBE.AccountNo = lblGlobalAccountno.Text;
        //    objMastersBE.PaymentModeId = 1;//Cash-- need to make iput from interface
        //    objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
        //    objMastersBE.PaidAmount = this.PaidAmount;
        //    objMastersBE.PaymentRecievedDate = _ObjCommonMethods.Convert_MMDDYY(txtPaidDate.Text);
        //    objMastersBE.PaymentType = (int)PaymentType.CustomerPayment;

        //    objMastersBE = _objiDsignBAL.DeserializeFromXml<MastersBE>(_ObjMastersBAL.PaymentEntry(objMastersBE, Statement.Insert));

        //    if (objMastersBE.IsSuccess)
        //    {
        //        Message(UMS_Resource.PAYMENT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
        //        txtAccountNo.Text = lblGlobalAccountno.Text;
        //        txtAmount.Text = txtPaidDate.Text = string.Empty;
        //    }
        //    else
        //    {
        //        Message(UMS_Resource.PAYMENT_ENTRY_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
        //    }

        //    //PaymentsBe _objPaymentsBe = new PaymentsBe();
        //    //XmlDocument xmlResult = new XmlDocument();
        //    //_objPaymentsBe.AccountNo = lblGlobalAccountno.Text;
        //    //_objPaymentsBe.PaidAmount = this.PaidAmount;
        //    //_objPaymentsBe.PaymentReceivedDate = _ObjCommonMethods.Convert_MMDDYY(txtPaidDate.Text);
        //    //_objPaymentsBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString(); ;
        //    //xmlResult = _objPaymentsBal.Insert(_objPaymentsBe, Statement.Insert);
        //    //_objPaymentsBe = _objiDsignBAL.DeserializeFromXml<PaymentsBe>(xmlResult);

        //    //if (_objPaymentsBe.EffectedRows > 0)
        //    //{
        //    //    Message(Resource.PAYMENT_INSERT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
        //    //}
        //    //else
        //    //{
        //    //    Message(Resource.PAYMENT_INSERT_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
        //    //}
        //}
    }
}