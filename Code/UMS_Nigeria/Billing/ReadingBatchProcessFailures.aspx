﻿<%@ Page Title=":: Reading Batch Process Failures ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="ReadingBatchProcessFailures.aspx.cs"
    Inherits="UMS_Nigeria.Billing.ReadingBatchProcesFailures" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControls/UCPagingV2.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upReadingBatchProcesFailures" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div class="inner-sec">
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litSearchCustomer" runat="server" Text="Reading Batch Failure Transactions"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <%--<asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>--%>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="previous">
                            <a href="ReadingBatchProcessStatus.aspx"> < < Back to Status </a>
                        </div>
                </div>
                <div class="clr">
                </div>
                <div class="out-bor_a" id="divdetails" runat="server">
                <%--<div class="clear pad_10 fltl">
                        <a style="padding-right: 82px; font-size: 13px; text-decoration: underline;" class="back-btn"
                            id="btnBack" href="ReadingBatchProcessStatus.aspx"><< Back To Meter Reading Batch Process Status</a>
                    </div>--%>
                    <div class="inner-sec">
                        <div class="out-bor_cTwo">
                            <div class="inner-box">
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:Resource, BATCHNUM%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_bTwo">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblBatchNo" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, BATCHDATE%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblBatchDate" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, TOTAL_TRANSACTION_FAILED%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_bTwo">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblTotalTransactionFailed" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:Resource, TOTALCUSTOMERS%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblTotalCustomers" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, NOTES%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_bTwo">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a" style="width: 49%;">
                                        <asp:Label ID="lblNotes" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                    </div>
                </div>
                <div class="grid_boxes">
                    <div class="grid_paging_top">
                        <div class="paging_top_title" style="position:relative;top:24px;">
                            <asp:Literal ID="litBuList" runat="server" Text="Reading Batch Failure Transactions List"></asp:Literal>
                        </div><br />
                        <div class="clr"></div>
                        <div class="paging_top_right_content" id="divEportTOExcel" runat="server" style="float:left">
                            <asp:Button ID="btnExportToExcel" runat="server" CssClass="exporttoexcelButton"
                                OnClick="btnExportToExcel_Click" />
                        </div>
                        <div class="paging_top_right_content" id="divPaging1" runat="server">
                            <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="grid_tb" id="divgrid" runat="server">
                    <asp:GridView ID="gvReadingBatchProcesFailures" runat="server" AutoGenerateColumns="False"
                        AlternatingRowStyle-CssClass="color" HeaderStyle-CssClass="grid_tb_hd">
                        <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                        <EmptyDataTemplate>
                            No Details Found.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, GRID_SNO%>"
                                DataField="RowNumber" ItemStyle-CssClass="grid_tb_td" />
                            <asp:TemplateField HeaderText="Global / Old Account No." ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblReadingFailureransactionID" Visible="false" runat="server" Text='<%# Eval("ReadingFailureransactionID") %>'></asp:Label>
                                    <asp:Literal ID="litAccountNo" runat="server" Text='<%# Eval("AccountNo") %>'></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, CURRENT_READING%>" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Literal ID="litCurrentReading" runat="server" Text='<%# Eval("CurrentReading") %>'></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, READING_DATE%>" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Literal ID="litReadingDate" runat="server" Text='<%# Eval("ReadingDate") %>'></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, COMMENTS%>" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Literal ID="litComments" runat="server" Text='<%# Eval("Comments") %>'></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle HorizontalAlign="Center" />
                    </asp:GridView>
                    <asp:HiddenField ID="hfPUploadBatchId" runat="server" />
                    <asp:HiddenField ID="hfBatchNo" runat="server" />
                    <asp:HiddenField ID="hfPageSize" runat="server" />
                    <asp:HiddenField ID="hfPageNo" runat="server" Value="0" />
                    <asp:HiddenField ID="hfLastPage" runat="server" />
                    <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                </div>
                <div class="grid_boxes">
                    <div id="divPaging2" runat="server">
                        <div class="grid_paging_bottom">
                            <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <%--  Btn popup Start--%>
            <%--<asp:ModalPopupExtender ID="mpeBatchClose" runat="server" PopupControlID="PanelBatchClose"
                TargetControlID="hdnBatchClose" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
            </asp:ModalPopupExtender>
            <asp:HiddenField ID="hdnBatchClose" runat="server" />
            <asp:Panel ID="PanelBatchClose" runat="server" DefaultButton="" CssClass="modalPopup"
                Style="display: none; border-color: #639B00;">
                <div class="popheader" style="background-color: red;">
                    <asp:Label ID="lblBatchMsg" runat="server"></asp:Label>
                </div>
                <br />
                <div class="footer" align="right">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="btnUploadTransactionsOk" Visible="False" runat="server" Text="<%$ Resources:Resource, OK%>"
                            OnClick="btnUploadTransactionsOk_Click" CssClass="ok_btn" />
                        <asp:Button ID="btnBatchCloseOk" Visible="False" runat="server" Text="<%$ Resources:Resource, OK%>"
                            OnClick="btnBatchCloseOk_Click" CssClass="ok_btn" />
                        <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                            CssClass="cancel_btn" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>--%>
            <%-- Active  Btn popup Closed--%>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
        </Triggers>
    </asp:UpdatePanel>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });
    </script>
</asp:Content>
