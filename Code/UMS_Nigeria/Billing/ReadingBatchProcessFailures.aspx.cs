﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using Resources;
using UMS_NigeriaBE;
using System.Xml;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using System.Data;
using System.Configuration;
using org.in2bits.MyXls;
using System.IO;

namespace UMS_Nigeria.Billing
{
    public partial class ReadingBatchProcesFailures : System.Web.UI.Page
    {
        #region Properties

        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStartsReport : Convert.ToInt32(hfPageSize.Value); }
        }

        #endregion

        #region Members

        CommonMethods objCommonMethods = new CommonMethods();
        ReadingsBal _objReadingsBal = new ReadingsBal();
        iDsignBAL objIdsignBal = new iDsignBAL();
        string Key = "ReadingBatchProcesFailures";
        //int BatchId;

        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString[UMS_Resource.READINGS_BATCH_ID] != null)
                    {
                        //if (!string.IsNullOrEmpty(objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.READINGS_BATCH_ID].ToString())))
                        hfPUploadBatchId.Value = objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.READINGS_BATCH_ID].ToString());
                        hfPageNo.Value = Constants.pageNoValue.ToString();
                        BindGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            try
            {
                btnExportToExcel.Enabled = false;
                if (Request.QueryString[UMS_Resource.READINGS_BATCH_ID] != null)
                {
                    ReadingsBatchBE objReadingsBatchBE = new ReadingsBatchBE();
                    //ReadingsBatchListBE objReadingsBatchListBE = new ReadingsBatchListBE();
                    DataSet ReadingsBatchListDs = new DataSet();
                    objReadingsBatchBE.RUploadFileId = Convert.ToInt32(hfPUploadBatchId.Value);
                    objReadingsBatchBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                    objReadingsBatchBE.PageSize = Convert.ToInt32(hfTotalRecords.Value);
                    ReadingsBatchListDs = _objReadingsBal.ReadingBatchProcesFailures(objReadingsBatchBE, ReturnType.Get);

                    if (ReadingsBatchListDs != null)
                    {
                        if (ReadingsBatchListDs.Tables.Count > 0 && ReadingsBatchListDs.Tables[1].Rows.Count > 0)
                        {
                            XlsDocument xls = new XlsDocument();
                            Worksheet sheet = xls.Workbook.Worksheets.Add("Reading Batch Process Failure Transactions");
                            Cells cells = sheet.Cells;
                            Cell _objCell = null;

                            int DetailsRowNo = 1;
                            int CustomersRowNo = 2;

                            _objCell = cells.Add(DetailsRowNo, 1, "S.No");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;
                            _objCell = cells.Add(DetailsRowNo, 2, "Global / Old Account No.");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;
                            _objCell = cells.Add(DetailsRowNo, 3, "Current Reading");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;
                            _objCell = cells.Add(DetailsRowNo, 4, "Reading Date");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;
                            _objCell = cells.Add(DetailsRowNo, 5, "Comments");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;

                            for (int n = 0; n < ReadingsBatchListDs.Tables[1].Rows.Count; n++)
                            {
                                DataRow dr = (DataRow)ReadingsBatchListDs.Tables[1].Rows[n];
                                cells.Add(CustomersRowNo, 1, dr["SNO"].ToString());
                                cells.Add(CustomersRowNo, 2, dr["AccountNo"].ToString());
                                cells.Add(CustomersRowNo, 3, dr["CurrentReading"].ToString());
                                cells.Add(CustomersRowNo, 4, dr["ReadingDate"].ToString());
                                cells.Add(CustomersRowNo, 5, dr["Comments"].ToString());

                                CustomersRowNo++;
                            }

                            string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                            fileName = "ReadingBatchProcessFailureTransactions_" + fileName;
                            string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                            if (!File.Exists(filePath))
                            {
                                xls.FileName = filePath;
                                xls.Save();
                            }
                            objIdsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                        }
                        else
                            Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            finally
            {
                btnExportToExcel.Enabled = true;
            }
        }

        #region Methods

        public void BindGrid()
        {
            ReadingsBatchBE objReadingsBatchBE = new ReadingsBatchBE();
            //ReadingsBatchListBE objReadingsBatchListBE = new ReadingsBatchListBE();
            DataSet ReadingsBatchListDs = new DataSet();
            objReadingsBatchBE.RUploadFileId = Convert.ToInt32(hfPUploadBatchId.Value);
            objReadingsBatchBE.PageNo = Convert.ToInt32(hfPageNo.Value);
            objReadingsBatchBE.PageSize = PageSize;
            ReadingsBatchListDs = _objReadingsBal.ReadingBatchProcesFailures(objReadingsBatchBE, ReturnType.Get);

            lblBatchNo.Text = ReadingsBatchListDs.Tables[0].Rows[0]["BatchNo"].ToString();
            lblBatchDate.Text = ReadingsBatchListDs.Tables[0].Rows[0]["BatchDate"].ToString();
            lblNotes.Text = ReadingsBatchListDs.Tables[0].Rows[0]["Notes"].ToString();
            lblTotalTransactionFailed.Text = ReadingsBatchListDs.Tables[0].Rows[0]["TotalFailureTransactions"].ToString();
            lblTotalCustomers.Text = ReadingsBatchListDs.Tables[0].Rows[0]["TotalCustomers"].ToString();


            if (ReadingsBatchListDs.Tables[1].Rows.Count > 0)
            {
                divPaging1.Visible = divPaging2.Visible = divEportTOExcel.Visible = true;
                UCPaging1.Visible = UCPaging2.Visible = true;
                hfTotalRecords.Value = ReadingsBatchListDs.Tables[1].Rows[0]["TotalRecords"].ToString();
                gvReadingBatchProcesFailures.DataSource = ReadingsBatchListDs.Tables[1];
                gvReadingBatchProcesFailures.DataBind();
            }
            else
            {
                hfTotalRecords.Value = ReadingsBatchListDs.Tables[1].Rows.Count.ToString();
                divPaging1.Visible = divPaging2.Visible = divEportTOExcel.Visible = false;
                UCPaging1.Visible = UCPaging2.Visible = false;
                gvReadingBatchProcesFailures.DataSource = new DataTable(); 
                gvReadingBatchProcesFailures.DataBind();
            }

        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion
    }
}