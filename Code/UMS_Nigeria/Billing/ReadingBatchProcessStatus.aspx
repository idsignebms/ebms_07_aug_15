﻿<%@ Page Title=":: Reading Batch Process Status ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="ReadingBatchProcessStatus.aspx.cs"
    Inherits="UMS_Nigeria.Billing.ReadingBatchProcesStatus" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upReadingBatchProcesStatus" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div class="grid_boxes">
                    <div class="grid_paging_top">
                        <div class="paging_top_title" style="position: relative; width: 40%;">
                            <asp:Literal ID="litBuList" runat="server" Text="Reading Batch Process Status"></asp:Literal>
                        </div>
                        <div class="refreshButton" id="div1" runat="server" style="float: left; top: 0;">
                            <asp:Button ID="btnRefresh" CssClass="refreshButtononly_two" runat="server" Text="Refresh"
                                OnClick="btnRefresh_Click" />
                        </div>
                        <div class="paging_top_right_content" id="divPaging1" runat="server">
                            <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                        </div>
                        <div class="clr">
                        </div>
                    </div>
                </div>
                <div class="grid_tb" id="divgrid" runat="server">
                    <asp:GridView ID="gvReadingBatchProcesStatus" runat="server" AutoGenerateColumns="False"
                        AlternatingRowStyle-CssClass="color" HeaderStyle-CssClass="grid_tb_hd" OnRowCommand="gvReadingBatchProcessStatus_RowCommand"
                        OnRowDataBound="gvReadingBatchProcessStatus_RowDataBound">
                        <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                        <EmptyDataTemplate>
                            No Details Found.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, GRID_SNO%>"
                                DataField="RowNumber" ItemStyle-CssClass="grid_tb_td" />
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, BATCHNUM%>" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblBatchUploadId" runat="server" Visible="false" Text='<%# Eval("RUploadBatchId") %>'></asp:Label>
                                    <asp:Label ID="lblBatchID" runat="server" Visible="false" Text='<%# Eval("RUploadFileId") %>'></asp:Label>
                                    <asp:Literal ID="litBatchName" runat="server" Text='<%# Eval("BatchNo") %>'></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, BATCHDATE%>" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Literal ID="litBatchDate" runat="server" Text='<%# Eval("StrBatchDate") %>'></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, NOTES%>" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Literal ID="litNotes" runat="server" Text='<%# Eval("NOTES") %>'></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, TRANSACTION_DATE%>" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Literal ID="litTransactionDate" runat="server" Text='<%# Eval("StrCreatedDate") %>'></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, TOTALCUSTOMERS%>" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Literal ID="litTotalCustomers" runat="server" Text='<%# Eval("TotalCustomers") %>'></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, SUCCESS_TRANSATIONS%>" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Literal ID="litSuccessTransactions" runat="server" Text='<%# Eval("TotalSucessTransactions") %>'></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, FAILURE_TRANSATIONS%>" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Literal ID="litFailureTransactions" runat="server" Text='<%# Eval("TotalFailureTransactions") %>'></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Image ID="imgStatus" ImageUrl="~/images/Activate.gif" Height="20px" Width="20px"
                                        runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, DOWNLOAD_ACTUAL_FILE%>" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblActualFilePath" Visible="false" Text='<%# Eval("FilePath") %>'
                                        runat="server" />
                                    <%--<asp:LinkButton ID="lkbtnDownloadActualFile" Text='<%# Eval("FilePath") %>'
                                        runat="server" ToolTip="Download Actual File" CommandName="DownloadActualFile">
                                    </asp:LinkButton>--%>
                                    <asp:LinkButton ID="lkbtnDownloadActualFile" runat="server" ToolTip="Download Actual File"
                                        CommandName="DownloadActualFile">
                                        <asp:Image ID="imgDownload" ImageUrl="~/images/download.png" Width="20px" Height="20px"
                                            runat="server" ToolTip='<%# Eval("FilePath") %>' />
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lkbtnViewFailureTransactions" Text="<%$ Resources:Resource, FAILURE_TRANSATIONS%>"
                                        runat="server" ToolTip="View Failure Transactions" CommandName="ViewFailureTransactions">
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lkbtnViewSuccessTransactions" Text="<%$ Resources:Resource, SUCCESS_TRANSATIONS%>"
                                        runat="server" ToolTip="View Success Transactions" CommandName="ViewSuccessTransactions">
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lkbtnUploadTransactions" Text="<%$ Resources:Resource, UPLOAD_AGAIN%>"
                                        runat="server" ToolTip="Upload Transactions Again" CommandName="UploadTransactions">
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lkbtnBatchClose" Text="<%$ Resources:Resource, CLOSE_BATCH%>"
                                        runat="server" ToolTip="Batch Close" CommandName="BatchClose">
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle HorizontalAlign="Center" />
                    </asp:GridView>
                    <asp:HiddenField ID="hfUploadBatchId" runat="server" />
                    <asp:HiddenField ID="hfBatchNo" runat="server" />
                    <asp:HiddenField ID="hfPageSize" runat="server" />
                    <asp:HiddenField ID="hfPageNo" runat="server" Value="0" />
                    <asp:HiddenField ID="hfLastPage" runat="server" />
                    <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                </div>
                <div class="grid_boxes">
                    <div id="divPaging2" runat="server">
                        <div class="grid_paging_bottom">
                            <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <%--  Btn Upload Transaction popup Start--%>
            <asp:ModalPopupExtender ID="mpeUploadTransaction" runat="server" PopupControlID="PanelUploadTransaction"
                TargetControlID="hdnUploadTransaction" BackgroundCssClass="modalBackground" CancelControlID="btnUploadTransactionCancel">
            </asp:ModalPopupExtender>
            <asp:HiddenField ID="hdnUploadTransaction" runat="server" />
            <asp:Panel ID="PanelUploadTransaction" runat="server" DefaultButton="" CssClass="modalPopup"
                Style="display: none; border-color: #639B00;">
                <%--<div class="popheader" style="background-color: red;">
                    
                </div>--%>
                <div class="inner-boxOne" style="padding: 10px 0;">
                    <h4 class="smHeading">
                        Upload Transaction</h4>
                    <div class="clr">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <br />
                            <asp:Label ID="lblUploadTransactionMsg" runat="server"></asp:Label><br />
                            <asp:FileUpload ID="fupUploadTransactions" runat="server" Style="width: 270px;" />
                        </div>
                    </div>
                    <br />
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btnUploadTransactionsOk" Visible="False" runat="server" Text="<%$ Resources:Resource, OK%>"
                                OnClick="btnUploadTransactionsOk_Click" OnClientClick="return ValidateUpload();"
                                CssClass="ok_btn" />
                            <asp:Button ID="btnUploadTransactionCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </div>
                <div style="clear: both;">
                </div>
            </asp:Panel>
            <%-- Active  Btn popup Closed--%>
            <%--  Btn popup Start--%>
            <asp:ModalPopupExtender ID="mpeBatchClose" runat="server" PopupControlID="PanelBatchClose"
                TargetControlID="hdnBatchClose" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
            </asp:ModalPopupExtender>
            <asp:HiddenField ID="hdnBatchClose" runat="server" />
            <asp:Panel ID="PanelBatchClose" runat="server" DefaultButton="" CssClass="modalPopup"
                Style="display: none; border-color: #639B00;">
                <div class="popheader" style="background-color: red;">
                    <asp:Label ID="lblBatchMsg" runat="server"></asp:Label>
                </div>
                <br />
                <div class="footer" align="right">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="btnBatchCloseOk" Visible="False" runat="server" Text="<%$ Resources:Resource, OK%>"
                            OnClick="btnBatchCloseOk_Click" CssClass="ok_btn" />
                        <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                            CssClass="cancel_btn" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
            </asp:ModalPopupExtender>
            <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
            <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                <div class="popheader popheaderlblred" id="popalert" runat="server">
                    <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                </div>
                <div class="footer popfooterbtnrt">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                            CssClass="ok_btn" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
            <%-- Active  Btn popup Closed--%>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadTransactionsOk" />
            <asp:PostBackTrigger ControlID="btnBatchCloseOk" />
            <asp:PostBackTrigger ControlID="gvReadingBatchProcesStatus" />
            <asp:PostBackTrigger ControlID="btnRefresh" />
        </Triggers>
    </asp:UpdatePanel>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/ReadingBatchProcessStatus.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });
    </script>
    <script type="text/javascript">
        window.onload = function () {
            document.onkeydown = function (e) {
                if (e.which == 116 || e.keyCode == 116)
                    return false;
            };
        }
    </script>
</asp:Content>
