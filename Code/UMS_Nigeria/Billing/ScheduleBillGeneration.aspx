﻿<%@ Page Title=":: Schedule Bills ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    AutoEventWireup="true" CodeBehind="ScheduleBillGeneration.aspx.cs" Inherits="UMS_Nigeria.Billing.ScheduleBillGeneration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/UserControls/Authentication.ascx" TagName="Authentication" TagPrefix="UC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../App_Themes/Green/style.css" rel="stylesheet" type="text/css" />
    <script>
        function closePopup() {
            //            btnConfirmationOk
            var mpee = $find('UMSNigeriaBody_mpeBatchFinished');
            mpee.hide();
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <script language="javascript" type="text/javascript">
        window.history.forward();
    </script>
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upSearchCustomer" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div id="divBillInput" runat="server" class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litSearchCustomer" runat="server" Text="Schedule Customers Bill Generation"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div id="divStep1" runat="server">
                        <div class="clear">
                            <asp:Button ID="btnDummyforCycle" runat="server" OnClick="btnDummyforCycle_Click"
                                Style="display: none;" />
                            &nbsp;</div>
                        <div class="consumer_feild">
                            <div style="float: left; width: 56%;">
                                <%--   <div class="billing_step">
                                    Step 1</div>--%>
                                <div class="clear pad_5">
                                </div>
                                <asp:Panel ID="PanelTarifEnergy" runat="server">
                                    <asp:Label ID="lblTariffEnergies" runat="server" Text=""></asp:Label>
                                </asp:Panel>
                                <div class="clear pad_5">
                                </div>
                                <div class="consume_name" style="width: 200px; display: none;">
                                    <asp:Literal ID="LiteralMonth" runat="server" Text="<%$ Resources:Resource, BILLING_MONTHS%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </div>
                                <div class="consume_input c_left" style="width: 200px; display: none;">
                                    <%--<asp:DropDownList Style="width: 80px; float: left;" ID="ddlYear" CssClass="text-box"
                                        runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="ddlMonth" CssClass="text-box" Style="width: 100px; float: left;
                                        margin-right: 10px; margin-left: -2px;" runat="server">
                                        <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                    </asp:DropDownList>--%>
                                </div>
                                <div class="clear pad_5">
                                </div>
                                <div class="consume_name" style="width: 164px; font-weight: bold; padding-left: 15px;">
                                    <asp:Literal ID="Literal4" runat="server" Text="Bill will generate for "></asp:Literal>
                                </div>
                                <div class="consume_input c_left" style="width: 200px; font-weight: bold; padding-top: 5px;
                                    font-size: 14px;">
                                    <asp:Label ID="lblYear" runat="server"></asp:Label>
                                    <asp:Label ID="lblNoBillingMonths" Visible="false" runat="server"></asp:Label>
                                    <asp:Label ID="lblMonth" runat="server"></asp:Label>
                                </div>
                                <%--<div class="clear pad_5">
                        </div>
                        <div class="consume_name" style="width: 200px;">
                            <asp:Literal ID="LiteralUnits" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal><span>*</span></div>
                        <div class="consume_input c_left" style="width: 200px;">
                            <asp:DropDownList ID="ddlBusinessUnits" runat="server" OnSelectedIndexChanged="ddlBusinessUnits_SelectedIndexChanged"
                                AutoPostBack="true">
                                <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                            </asp:DropDownList>
                            <span id="spanddlBusinessUnit" class="spancolor"></span>
                        </div>--%>
                                <div id="divsc" runat="server" visible="false">
                                    <div class="clear pad_5">
                                    </div>
                                    <div id="divServiceUnits" runat="server">
                                        <%--  <div class="consume_name" style="width: 200px;">
                                    <asp:Literal ID="ltrlServiceUnits" runat="server" Text="<%$ Resources:Resource,SERVICE_UNITS%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </div>--%>
                                        <%-- <asp:DropDownList ID="ddlServiceUnits" CssClass="text-box" runat="server" OnSelectedIndexChanged="ddlServiceUnits_SelectedIndexChanged"
                                    AutoPostBack="true">
                                    <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                </asp:DropDownList>--%>
                                        <div class="check_baaTwo">
                                            <div class="text-inner_a">
                                                <asp:Literal ID="LiteralSu" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal><span
                                                    class="span_star">*</span>
                                            </div>
                                            <div class="check_ba">
                                                <div class="text-inner mster-input">
                                                    <asp:CheckBox runat="server" ID="ChkSUAll" Text="All" AutoPostBack="true" />
                                                    <asp:CheckBoxList ID="cblServiceUnits" Width="127%" Style="margin-left: -3px;" AutoPostBack="true"
                                                        RepeatDirection="Horizontal" RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceUnits_SelectedIndexChanged">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear pad_5">
                                    </div>
                                </div>
                                <div id="divServiceCenters" runat="server">
                                    <%--   <div class="consume_name" style="width: 200px;">
                                    <asp:Literal ID="ltrlServiceCenter" runat="server" Text="<%$ Resources:Resource,SERVICE_CENTERS%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </div>--%>
                                    <%--  <asp:DropDownList ID="ddlServiceCenters" CssClass="text-box" runat="server" OnSelectedIndexChanged="ddlServiceCenters_SelectedIndexChanged"
                                    AutoPostBack="true">
                                    <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                </asp:DropDownList>--%>
                                    <div class="check_baaTwo">
                                        <div class="text-inner_a">
                                            <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal><span
                                                class="span_star">*</span>
                                        </div>
                                        <div class="check_ba">
                                            <div class="text-inner mster-input">
                                                <%--   <asp:UpdatePanel ID="updSC" runat="server">
                                                    <ContentTemplate>--%>
                                                <asp:CheckBox runat="server" ID="chkSCAll" Text="All" AutoPostBack="true" />
                                                <asp:CheckBoxList ID="cblServiceCenters" Width="127%" Style="margin-left: -3px;"
                                                    RepeatDirection="Horizontal" AutoPostBack="true" RepeatColumns="3" runat="server"
                                                    OnSelectedIndexChanged="cblServiceCenters_SelectedIndexChanged">
                                                </asp:CheckBoxList>
                                                <%--  </ContentTemplate>
                                                </asp:UpdatePanel>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear pad_5">
                                </div>
                                <div id="divProcessType" runat="server" visible="false">
                                    <div class="consume_name" style="width: 200px;">
                                        <asp:Literal ID="LiteralProcessType" runat="server" Text="<%$ Resources:Resource, CHOOSE_PROCESSTYPE%>"></asp:Literal>
                                    </div>
                                    <div class="fltl c_left">
                                        <asp:RadioButtonList Style="width: 192px;" RepeatDirection="Horizontal" ID="rblBillProcessTypes"
                                            AutoPostBack="true" runat="server" OnSelectedIndexChanged="rblBillProcessTypes_SelectedIndexChanged">
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div class="clear pad_5">
                                </div>
                                <div id="divCycles" runat="server">
                                    <div class="check_baaTwo">
                                        <div class="text-inner_a">
                                            <%-- Select Cycle / Feeder--%>
                                            <asp:Literal ID="LiteralCycleFeeder" runat="server" Text="<%$ Resources:Resource,CHOOSE_CYCLES%>"></asp:Literal><span
                                                class="span_star">*</span>
                                        </div>
                                        <div class="check_ba">
                                            <div class="text-inner mster-input">
                                                <%-- <asp:DropDownList ID="ddlCycleList" runat="server">
                                <asp:ListItem Text="--Select Cycle--" Value="0"></asp:ListItem>
                            </asp:DropDownList>--%>
                                                <asp:UpdatePanel ID="UpdatePanelBG" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:CheckBox ID="cbAll" runat="server" Text="All" onclick="Selectall(this);" />
                                                        <asp:CheckBoxList ID="cblCycles" Width="127%" Style="margin-left: -3px;" runat="server"
                                                            RepeatDirection="Horizontal" RepeatColumns="3" onclick="CheckSelectAll();">
                                                        </asp:CheckBoxList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <div id="divnotation" runat="server" style="border: solid 1px black; height: 97px;
                                                    width: auto; float: left; padding: 8px;">
                                                    <div style="background-color: #FF8000; float: left; height: 13px; margin-top: 3px;
                                                        width: 15px;">
                                                        &nbsp;&nbsp;&nbsp;
                                                    </div>
                                                    <span style="float: left; font-weight: bold; color: #6E6E6E;">&nbsp;Bill Generated,
                                                        Payment and Adjustment Received.</span>
                                                    <br />
                                                    <div style="background-color: #B40486; float: left; height: 13px; margin-top: 3px;
                                                        width: 15px;">
                                                        &nbsp;&nbsp;&nbsp;
                                                    </div>
                                                    <span style="float: left; font-weight: bold; color: #6E6E6E;">&nbsp;Bill Generated and
                                                        Payment Received.</span>
                                                    <br />
                                                    <div style="background-color: #58ACFA; float: left; height: 13px; margin-top: 3px;
                                                        width: 15px;">
                                                        &nbsp;&nbsp;&nbsp;
                                                    </div>
                                                    <span style="float: left; font-weight: bold; color: #6E6E6E;">&nbsp;Bill Generated and
                                                        Adjustment Received.</span>
                                                    <br />
                                                    <div style="background-color: #FE2E64; float: left; height: 13px; margin-top: 3px;
                                                        width: 15px;">
                                                        &nbsp;&nbsp;&nbsp;
                                                    </div>
                                                    <span style="float: left; color: #6E6E6E; font-weight: bold;">&nbsp;Bill Generated.</span>
                                                    <br />
                                                    <div style="background-color: ThreeDShadow; float: left; height: 13px; margin-top: 3px;
                                                        width: 15px;">
                                                        &nbsp;&nbsp;&nbsp;
                                                    </div>
                                                    <span style="float: left; color: #6E6E6E; font-weight: bold;">&nbsp;No Customer Is Available..</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="divErrorMessges" class="consume_name" style="width: 560px;" runat="server"
                                    visible="false">
                                    <div class="clear">
                                        &nbsp;
                                    </div>
                                    Please complete below requirements to proceed to Bill Generation :
                                </div>
                                <div id="divafterCycles" runat="server" class="pad_10">
                                    <div id="divDisabledBooks" runat="server">
                                        <div class="clear pad_5">
                                        </div>
                                        <div class="consume_name" style="width: 280px;">
                                            <asp:Literal ID="ltrlDisabledBooks" runat="server" Text="Disabled Books:"></asp:Literal>
                                        </div>
                                        <div class="fltl c_left">
                                            <asp:Label ID="lblDisabledBooks" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <%--  <div class="clear pad_5">
                                    </div>
                                    <div class="consume_name" style="width: 240px;">
                                        <asp:Literal ID="litTariff" runat="server" Text="<%$ Resources:Resource, READINGS_COMPLETE%>"></asp:Literal>
                                    </div>
                                    <div class="text-inner">
                                        <asp:RadioButtonList Style="width: 170px; margin-top: -21px;" RepeatDirection="Horizontal"
                                            ID="rblReadings" onclick="return CheckReadValidation()" runat="server">
                                            <asp:ListItem Text="YES" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>--%>
                                    <div id="divInValidReadings" runat="server" visible="false">
                                        <div class="clear pad_5">
                                        </div>
                                        <div class="consume_name" style="width: 280px; color: Red;">
                                            <asp:Literal ID="Literal3" runat="server" Text="Readings Not Completed Book Groups:"></asp:Literal>
                                        </div>
                                        <div class="fltl c_left">
                                            <%--<asp:Label ID="lblReadingsCycles" runat="server" Text=""></asp:Label>--%>
                                            <div class="rptr_tb">
                                                <asp:DataList ID="dtlReadingsCycles" RepeatColumns="4" Width="200%" RepeatDirection="Horizontal"
                                                    runat="server">
                                                    <ItemTemplate>
                                                        <div>
                                                            <%#Container.ItemIndex+1 %>
                                                            .&nbsp
                                                            <asp:Label ID="lblCycleName" runat="server" Text='<%# Eval("CycleName") %>' />
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="divAfterRead" runat="server">
                                        <%--  <div class="clear pad_5">
                                        </div>
                                        <div class="consume_name" style="width: 240px;">
                                            <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, PAYMENT_BATCHESCLOSED%>"></asp:Literal>
                                        </div>
                                        <div class="text-inner">
                                            <asp:RadioButtonList Style="width: 170px; margin-top: -21px; margin-left: 2px;" RepeatDirection="Horizontal"
                                                ID="rblBatches" runat="server" onclick="return CheckPaymentBatchClose()">
                                                <asp:ListItem Text="YES" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>--%>
                                        <div id="divInValidPaymentBatches" runat="server" visible="false">
                                            <div class="clear pad_5">
                                            </div>
                                            <div class="consume_name" style="width: 280px; color: Red;">
                                                <asp:Literal ID="Literal1" runat="server" Text="Please Close Payment Batches:"></asp:Literal>
                                            </div>
                                            <div class="fltl c_left">
                                                <%--<asp:Label ID="lblPaymentBatches" runat="server" Text=""></asp:Label>--%>
                                                <div class="rptr_tb">
                                                    <asp:DataList ID="dtlPaymentBatches" RepeatColumns="4" Width="200%" RepeatDirection="Horizontal"
                                                        runat="server">
                                                        <ItemTemplate>
                                                            <div>
                                                                <%#Container.ItemIndex+1 %>
                                                                .&nbsp
                                                                <asp:Label ID="lblPaymentBatch" runat="server" Text='<%# Eval("PaymentBatch") %>' />
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divAfterPaymentBatch" runat="server">
                                            <%--  <div class="clear pad_5">
                                            </div>
                                            <div class="consume_name" style="width: 240px;">
                                                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, ADJUSTMENTS_CLOSED%>"></asp:Literal>
                                            </div>
                                            <div class="text-inner">
                                                <asp:RadioButtonList Style="width: 170px; margin-top: -21px; margin-left: 2px;" RepeatDirection="Horizontal"
                                                    onclick="return CheckAdjustmentBatchClose()" ID="rblAdjustments" runat="server">
                                                    <asp:ListItem Text="YES" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>--%>
                                            <div id="divInvalidAdustmentBatches" runat="server" visible="false">
                                                <div class="clear pad_5">
                                                </div>
                                                <div class="consume_name" style="width: 280px; color: Red;">
                                                    <asp:Literal ID="Literal2" runat="server" Text="Please Close Adjustment Batches:"></asp:Literal>
                                                </div>
                                                <div class="fltl c_left">
                                                    <%--<asp:Label ID="lblAdjustmentBatches" runat="server" Text=""></asp:Label>--%>
                                                    <div class="rptr_tb">
                                                        <asp:DataList ID="dtlAdjustmentBatches" RepeatColumns="4" Width="200%" RepeatDirection="Horizontal"
                                                            runat="server">
                                                            <ItemTemplate>
                                                                <div>
                                                                    <%#Container.ItemIndex+1 %>
                                                                    .&nbsp
                                                                    <asp:Label ID="lblAdjustmentBatches" runat="server" Text='<%# Eval("AdjustmentBatch") %>' />
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:DataList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divAfterAdjustment" runat="server">
                                                <div class="clear pad_5">
                                                </div>
                                                <%--  <div class="consume_name" style="width: 240px;">
                                                    <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, ESTIMATIONS_COMPLETED%>"></asp:Literal>
                                                </div>
                                                <div class="text-inner">
                                                    <asp:RadioButtonList Style="width: 170px; margin-top: -21px; margin-left: 2px;" RepeatDirection="Horizontal"
                                                        ID="rblEstimation" runat="server" onclick="return CheckEstimationValidation()">
                                                        <asp:ListItem Text="YES" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="NO" Value="0"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>--%>
                                                <div id="divEstimationValid" runat="server" visible="false">
                                                    <div class="clear pad_5">
                                                    </div>
                                                    <div class="consume_name" style="width: 280px; color: Red;">
                                                        <asp:Literal ID="Literal5" runat="server" Text="Estimation Not Completed Book Groups"></asp:Literal>
                                                    </div>
                                                    <div class="fltl c_left">
                                                        <%--<asp:Label ID="lblInValidEstimation" runat="server" Text=""></asp:Label>--%>
                                                        <div class="rptr_tb">
                                                            <asp:DataList ID="dtlInValidEstimation" RepeatColumns="4" Width="200%" RepeatDirection="Horizontal"
                                                                runat="server">
                                                                <ItemTemplate>
                                                                    <div>
                                                                        <%#Container.ItemIndex+1 %>
                                                                        .&nbsp
                                                                        <asp:Label ID="lblInValidEstimation" runat="server" Text='<%# Eval("CycleName") %>' />
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:DataList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="margin-left: 233px;">
                                        <%--  <asp:Button ID="btnSave" Text="<%$ Resources:Resource, GO_BUTTON%>" CssClass="calculate_but"
                            runat="server" OnClientClick="return PageaValidation();" OnClick="btnSave_Click" />--%>
                                        <%--  <div style="margin-left:">
                           <button class="calculate_but">Next </button>
                            </div>--%>
                                    </div>
                                    <div class=" clear pad_10">
                                    </div>
                                    <div class="choos_step">
                                        <div class="">
                                            <div class="" id="divNote" runat="server" visible="false">
                                                <%--                                            <asp:Literal ID="leteralBillingMode" runat="server" Text="<%$ Resources:Resource,CHOOSE_THE_MODEOF_BILLING%>"></asp:Literal>
                                                --%>
                                                <b>Note: </b>
                                                <asp:Literal ID="leteralBillingMode" runat="server" Text="Bill Email will be sent to "></asp:Literal><asp:Label
                                                    ID="lblBillEmailId" runat="server" Text=""></asp:Label>
                                            </div>
                                            <div class="text-inner" style="display: none;">
                                                <asp:RadioButtonList Style="width: 192px;" RepeatDirection="Horizontal" ID="rblBillSendingMode"
                                                    runat="server">
                                                    <asp:ListItem Text="By Email" Value="0" Selected="True"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="fltl" style="margin: 10px 4px 6px 14px;">
                                        <asp:Button ID="btnNext" CssClass="box_s" runat="server" Text="Proceed to Generate Bill"
                                            OnClientClick="return PageaValidation();" OnClick="btnNext_Click" Visible="false" />
                                        <%-- <button class="calculate_but">
                        Next
                    </button>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="errorsMS">
                            <span id="spanYear" class="span_color"></span>
                            <br />
                            <span id="spanMonth" class="span_color"></span>
                            <br />
                            <span id="spanServiceUnit" class="span_color"></span>
                            <br />
                            <span id="spanServiceCenter" class="span_color"></span>
                            <br />
                            <span id="spanCyclesFeeders" class="span_color"></span>
                            <br />
                            <span id="spanReadings" class="span_color"></span>
                            <br />
                            <span id="spanBatches" class="span_color"></span>
                            <br />
                            <span id="spanAdjustments" class="span_color"></span>
                            <br />
                            <span id="spanEstimation" class="span_color"></span>
                            <br />
                            <span id="spanBillSendingMode" class="span_color"></span>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div id="divStep2" runat="server" visible="false">
                        <div class="consumer_feild" style="margin-left: 10px;">
                            <div class="billing_step">
                                <h4 style="color: #006600;">
                                    Step 2</h4>
                            </div>
                            <div class="clear pad_10">
                            </div>
                            <h5 style="color: #006600;">
                                Billing Selection Details</h5>
                            <%-- <div class="consume_name" style="width: 200px;">
                            Your Select of Billing
                        </div>
                        <div class="consume_input">
                            <asp:Label ID="lblSelectBilling" runat="server"></asp:Label>
                        </div>--%>
                            <div class="clear pad_5">
                            </div>
                            <div class="consume_name" style="width: 450px;">
                                Billing Month :
                            </div>
                            <div class="consume_input">
                                <asp:Label ID="lblBillingMonth" runat="server" Font-Bold="true"></asp:Label>
                            </div>
                            <div class="clear pad_5">
                            </div>
                            <div class="consume_name" style="width: 450px;">
                                Billing Generated By :
                            </div>
                            <div class="consume_input">
                                <asp:Label ID="lblGeneratedBY" runat="server" Font-Bold="true"></asp:Label>
                            </div>
                            <div class="clear pad_5">
                            </div>
                            <div class="consume_name" style="width: 450px;">
                                Total number of customer in selected book groups :
                            </div>
                            <div class="consume_input">
                                <asp:Label ID="lblTotal" runat="server" Font-Bold="true" Text="0"></asp:Label>
                            </div>
                            <div class="clear pad_5">
                            </div>
                            <div class="consume_name" style="width: 450px;">
                                Selected Book Groups :
                            </div>
                            <div class="consume_input" style="width: 500px;">
                                <asp:Label ID="lblSelectedCycles" Visible="false" runat="server" Font-Bold="true" Text="0"></asp:Label>
                                <div class="rptr_tb">
                                    <asp:DataList ID="dtlSelectedCycles" RepeatColumns="4" Width="200%" RepeatDirection="Horizontal"
                                        runat="server">
                                        <ItemTemplate>
                                            <div>
                                                <%#Container.ItemIndex+1 %>
                                                .&nbsp
                                                <asp:Label ID="lblCycleName" runat="server" Text='<%# Eval("CycleName") %>' />
                                            </div>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>
                            </div>
                            <div class="clear pad_5">
                            </div>
                            <div class="consume_name" style="width: 450px;">
                                Disabled books having customers:
                            </div>
                            <div class="consume_input">
                                <asp:Label ID="lblDisableCustomers" runat="server" Font-Bold="true" Text="0"></asp:Label>
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                        <div class="grid_tb" runat="server" style="overflow-y: scroll; height: 200px; width: 300px;">
                            <div class="grid_tb" align="center" style="overflow-x: auto; margin-left: 5px;">
                                <asp:GridView ID="gvCustomerDetails" runat="server" AutoGenerateColumns="false" ShowFooter="True"
                                    HeaderStyle-CssClass="grid_tb_hd">
                                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                    <EmptyDataTemplate>
                                        There is no data.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="SNo">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tariff">
                                            <ItemTemplate>
                                                <%# Eval("TariffName")%>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotal" runat="server" Text="Total"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Customers">
                                            <ItemTemplate>
                                                <%# Eval("TotalRecords")%>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalCustomers" runat="server" Text="0"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total Energies">
                                            <ItemTemplate>
                                                <%# Eval("Usage")%>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalUsage" runat="server" Text="0"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <%--  <asp:BoundField DataField="TariffName" HeaderText="Tariff" />
                                <asp:BoundField DataField="TotalRecords" HeaderText="Customers" />--%>
                                        <%-- <asp:TemplateField HeaderText="Total"></asp:TemplateField>--%>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        <br />
                        <div id="divMessages" runat="server" visible="false">
                            <asp:Label ID="lblBillingAlertMessage" runat="server" Font-Bold="true" Font-Size="Medium"
                                ForeColor="Red"></asp:Label>
                            <br />
                            <asp:Button ID="btnGoToBilling" runat="server" Text="<%$ Resources:Resource, GOTO_BILLING%>"
                                CssClass="box_s" OnClick="btnGoToBilling_Click" />
                            <asp:Button ID="btnRegeneration" runat="server" Text="<%$ Resources:Resource, REGENERATE%>"
                                OnClientClick="return Confirm('Are you sures, do you want to Regenerate the bill for your selection ?')"
                                CssClass="box_s" OnClick="btnRegeneration_Click" />
                        </div>
                        <div id="divStep2GridView" runat="server">
                            <div style="width: 40%;">
                                <div class="fltl">
                                    <div class="chooslink">
                                        <asp:LinkButton ID="lbtnPrevious" runat="server" Text="Previous" OnClick="lbtnPrevious_Click"></asp:LinkButton>
                                        <%-- <a href="BillGenerationStepOne.aspx">Previous</a>--%>
                                    </div>
                                </div>
                                <div class="fltr">
                                    <asp:Button ID="btnConfirm" runat="server" Text="<%$ Resources:Resource, CONFIRM%>"
                                        CssClass="box_s" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="divStep3" runat="server" visible="false">
                        <div class="consumer_feild">
                            <div class="billing_step">
                                Step 3
                                <div class="billthanku_total">
                                    <center class="bill_thnku">
                                        Thank You</center>
                                    <p class=" pad_10">
                                    </p>
                                    <p>
                                        <%-- <asp:Label ID="lblBillThankyouMessage" runat="server"></asp:Label>--%>
                                        <%--  Thank you for billing ,You will receive a email to "xxxx@gmail.com" about ftp detail
                                    to download the file--%>
                                    </p>
                                    <div class="clear pad_10">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="width: 50%;">
                            <div class="fltl">
                                <div class="chooslink">
                                    <a href="../Home.aspx">Go to Home</a>
                                </div>
                            </div>
                            <div class="fltr">
                                <%--   <asp:Button ID="btnGoNextBilling" runat="server" Text="Go to Next Billing" CssClass="box_s"
                                    OnClick="btnGoNextBilling_Click" />--%>
                                <%--<button class="calculate_but">
                                Go to Next Billing
                            </button>--%>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="divAlert" runat="server" visible="false">
                    <div class="info">
                        Bill Generate</div>
                    <div class="error">
                        Before generate bill kindly follow below points:<br />
                        <br />
                        1. Open Bill Month and Year.<br />
                        2. Add reading to Read Customers.<br />
                        3. Do Estimation for Direct Customers.<br />
                    </div>
                    <%--   Click To Go Back&nbsp;
                    <asp:LinkButton ID="lnkNewRegstration" runat="server" Text="Here" OnClick="lnkNewRegstration_Click"></asp:LinkButton>.--%>
                </div>
                <div id="divBillingMessage" runat="server" visible="false">
                    <div class="info">
                        Thank you...!</div>
                    <div class="success" id="divStatusSuccess" runat="server" visible="false">
                        <asp:Label ID="lblBillThankyouMessage" runat="server"></asp:Label>
                    </div>
                    <div class="error" id="divStatusError" runat="server" visible="false">
                        <asp:Label ID="lblStatusError" runat="server"></asp:Label>
                    </div>
                    <div class="fltr">
                        <asp:Button ID="btnGoNextBilling" runat="server" Text="Go to Next Billing" CssClass="box_s"
                            OnClick="btnGoNextBilling_Click" />
                    </div>
                    <div class="chooslink">
                        <a href="../Home.aspx">Go to Home</a>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="hfMonthID" runat="server" />
            <asp:HiddenField ID="hfYearID" runat="server" />
            <asp:ModalPopupExtender ID="mpeBatchFinished" runat="server" TargetControlID="btnConfirm"
                PopupControlID="pnlBatchFinished" BackgroundCssClass="popbg" CancelControlID="btnConfirmNo">
            </asp:ModalPopupExtender>
            <asp:Panel runat="server" ID="pnlBatchFinished" CssClass="modalPopup" Style="display: none;
                border-color: #639B00;">
                <div class="panelMessage">
                    <div class="popheader" style="background-color: Red;">
                        <asp:Label ID="lblBatchFinishedMsg" runat="server" Text="<%$ Resources:Resource,BILL_GENERATION_CONFIRMATION%>"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnConfirmationOk" runat="server" Text="<%$ Resources:Resource, YES%>"
                                CssClass="ok_btn" OnClientClick="return closePopup();" OnClick="btnConfirmationOk_Click" />
                            <asp:Button ID="btnConfirmNo" runat="server" Text="<%$ Resources:Resource,NO%>" Style="padding-left: 10px;"
                                CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="mpAfterBillGeneration" runat="server" TargetControlID="hfIsValid"
                PopupControlID="pnlBillThanku" BackgroundCssClass="popbg" CancelControlID="hfIsValid">
            </asp:ModalPopupExtender>
            <asp:HiddenField ID="hfIsValid" runat="server" />
            <asp:Panel ID="pnlBillThanku" runat="server" DefaultButton="btnThankyouOK" CssClass="modalPopup"
                Style="display: none; border-color: #639B00;">
                <div class="popheader" style="background-color: #639B00;" id="divBillThankuMsg" runat="server">
                    <asp:Label ID="lblThankYouMessage" runat="server" Text="<%$ Resources:Resource,BILL_GENERATION_THANKU%>"></asp:Label>
                </div>
                <br />
                <div class="footer" align="right">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="btnThankyouOK" runat="server" Text="<%$ Resources:Resource, OK%>"
                            CssClass="btn_ok" Style="margin-left: 105px;" OnClick="btnThankyouOK_Click" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="mpeAlert" runat="server" TargetControlID="hfMpeAlert"
                PopupControlID="pnlAlert" BackgroundCssClass="popbg">
            </asp:ModalPopupExtender>
            <asp:HiddenField ID="hfMpeAlert" runat="server" />
            <asp:Panel ID="pnlAlert" runat="server" DefaultButton="btnThankyouOK" CssClass="modalPopup"
                Style="display: none; border-color: #639B00;">
                <div class="popheader" style="background-color: #639B00;" id="div1" runat="server">
                    <asp:Label ID="Label1" runat="server" Text="Bills are already generated for this book group."></asp:Label>
                </div>
                <br />
                <div class="footer" align="right">
                    <div class="fltr popfooterbtn">
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
            <UC:Authentication ID="ucAuthentication" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">

        $(document).ready(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
            rblChangeValidations();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
            rblChangeValidations();
        });

        function rblChangeValidations() {
            var rblReadings = document.getElementById('<= rblReadings.ClientID  %>');
            var rblBatches = document.getElementById('<= rblBatches.ClientID  %>');
            var rblAdjustments = document.getElementById('<= rblAdjustments.ClientID  %>');
            var rblEstimation = document.getElementById('<= rblEstimation.ClientID  %>');
            //            var rblBillProcessTypes = document.getElementById('<%= rblBillProcessTypes.ClientID  %>');
            var rblBillSendingMode = document.getElementById('<= rblBillSendingMode.ClientID  %>');

            //            $('#<%=rblBillProcessTypes.ClientID %>').change(function () {
            //                RadioButtonListlblValue(rblReadings, 'spanReadings', "Please Complete All The Readings")
            //                RadioButtonListlblValue(rblBatches, 'spanBatches', "Please Close All The Batches")
            //                RadioButtonListlblValue(rblAdjustments, 'spanAdjustments', "Please Close All The Adjustments")
            //                RadioButtonListlblValue(rblEstimation, 'spanEstimation', "Please Close All The Estimations")
            //            });

            $('#<=rblReadings.ClientID %>').change(function () {
                if ($('#<=rblReadings.ClientID %> input:checked').val() == "1") {
                    if (RadioButtonListlblValue(rblReadings, 'spanReadings', "Please Complete All The Readings") == false) return false;
                }
            });
            $('#<=rblBatches.ClientID %>').change(function () {
                if ($('#<=rblBatches.ClientID %> input:checked').val() == "1") {
                    if (RadioButtonListlblValue(rblBatches, 'spanBatches', "Please Close All The Batches") == false) return false;
                }
            });
            $('#<=rblAdjustments.ClientID %>').change(function () {
                if ($('#<=rblAdjustments.ClientID %> input:checked').val() == "1") {
                    if (RadioButtonListlblValue(rblAdjustments, 'spanAdjustments', "Please Close All The Adjustments") == false) return false;
                }
            });
            $('#<=rblEstimation.ClientID %>').change(function () {
                if ($('#<=rblEstimation.ClientID %> input:checked').val() == "1") {
                    if (RadioButtonListlblValue(rblEstimation, 'spanEstimation', "Please Close All The Estimations") == false) return false;
                }
            });
            $('#<=rblBillSendingMode.ClientID %>').change(function () {
                if (RadioButtonListlbl(rblBillSendingMode, 'spanBillSendingMode', "Bill Sending Mode") == false) {
                    IsValid = false;
                }
            });
        }
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/BillGenerationStepOne.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }

    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
