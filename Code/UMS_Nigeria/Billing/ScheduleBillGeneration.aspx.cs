﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddServiceUnits
                     
 Developer        : id067-Naresh T
 Creation Date    : 15-Apr-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using iDSignHelper;
using Resources;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using System.Drawing;
using System.Xml;
using System.IO;
using System.Web.Services;
using System.Data.SqlClient;
using System.Globalization;

namespace UMS_Nigeria.Billing
{
    public partial class ScheduleBillGeneration : System.Web.UI.Page
    {
        #region Members

        iDsignBAL _objIdsignBal = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        TariffManagementBal _objTariffBal = new TariffManagementBal();
        ConsumerBal _objConsumerBal = new ConsumerBal();
        BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
        MastersListBE _objMasterListBe = new MastersListBE();
        private delegate void SendMailScheduledBill(string cycleId, string MonthName, string BillingYear, string toEmailId);
        String Key = "BillGenerationStepOne";
        string BillingQueueScheduleIdList = null;
        string fileNameList = null;
        string filePathList = null;
        public int PageNum;
        public bool IsFileGenValid = true;
        #endregion

        #region Properties

        private int BillProcessType
        {
            get { return string.IsNullOrEmpty(rblBillProcessTypes.SelectedValue) ? 0 : Convert.ToInt32(rblBillProcessTypes.SelectedValue); }
        }

        private int YearId
        {
            //get { return string.IsNullOrEmpty(ddlYear.SelectedValue) ? 0 : Convert.ToInt32(ddlYear.SelectedValue); }
            get { return string.IsNullOrEmpty(hfYearID.Value) ? 0 : Convert.ToInt32(hfYearID.Value); }
        }

        private int MonthId
        {
            //get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? 0 : Convert.ToInt32(ddlMonth.SelectedValue); }
            get { return string.IsNullOrEmpty(hfMonthID.Value) ? 0 : Convert.ToInt32(hfMonthID.Value); }
        }

        //private string BillCycleFeederValue
        //{
        //    get { return string.IsNullOrEmpty(ddlCycleList.SelectedValue) ? string.Empty : ddlCycleList.SelectedValue; }
        //}

        private int BillSendingMode
        {
            get { return string.IsNullOrEmpty(rblBillSendingMode.SelectedValue) ? 0 : Convert.ToInt32(rblBillSendingMode.SelectedValue); }
        }

        private string SelectedMonth
        {
            //get { return (string.IsNullOrEmpty(ddlMonth.SelectedValue) ? string.Empty : ddlMonth.SelectedItem.Text) + "-" + (string.IsNullOrEmpty(ddlYear.SelectedValue) ? string.Empty : ddlYear.SelectedItem.Text); }
            get { return (string.IsNullOrEmpty(hfMonthID.Value) ? string.Empty : lblMonth.Text) + "-" + (string.IsNullOrEmpty(hfYearID.Value) ? string.Empty : lblYear.Text); }
        }

        //public string BusinessUnit
        //{
        //    get { return Session[UMS_Resource.SESSION_USER_BUID].ToString(); }
        //    //get { return string.IsNullOrEmpty(ddlBusinessUnits.SelectedValue) ? string.Empty : ddlBusinessUnits.SelectedValue; }
        //    //set { ddlBusinessUnits.SelectedValue = value.ToString(); }
        //}

        //public string ServiceUnit
        //{
        //    get { return string.IsNullOrEmpty(ddlServiceUnits.SelectedValue) ? string.Empty : ddlServiceUnits.SelectedValue; }
        //    set { ddlServiceUnits.SelectedValue = value.ToString(); }
        //}

        public string ServiceUnit
        {
            get { return _ObjCommonMethods.CollectSelectedItemsValues(cblServiceUnits, ","); }
        }

        //public string ServiceCenter
        //{
        //    get { return string.IsNullOrEmpty(ddlServiceCenters.SelectedValue) ? string.Empty : ddlServiceCenters.SelectedValue; }
        //    set { ddlServiceCenters.SelectedValue = value.ToString(); }
        //}

        public string ServiceCenter
        {
            get { return _ObjCommonMethods.CollectSelectedItemsValues(cblServiceCenters, ","); }
        }

        public string Cycles
        {
            get { return _ObjCommonMethods.CollectSelectedItemsValues(cblCycles, ","); }
        }
        public string CyclesTextList
        {
            get { return _ObjCommonMethods.CollectSelectedItemsText(cblCycles, ","); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblTariffEnergies.Text = PanelTarifEnergy.CssClass = pnlMessage.CssClass = lblMessage.Text = string.Empty;
            if (!IsPostBack)
            {
                divDisabledBooks.Visible = false;
                string path = string.Empty;
                path = _ObjCommonMethods.GetPagePath(this.Request.Url);
                //if (_ObjCommonMethods.IsAccess(path))
                {
                    //((TextBox)ucAuthentication.FindControl("txtPassword")).Focus();

                    BindOpenMonthAndYear();

                    //BindYears();
                    //BindMonths();
                    BindBillProcessTypes();
                    // BindCyclesOrFeeders();
                    //ddlServiceCenters.Enabled = false;
                    BindServiceUnits();
                    CheckBoxesJS();
                    lblBillEmailId.Text = Session[UMS_Resource.SESSION_USER_EMAILID].ToString();
                    //_ObjCommonMethods.BindBusinessUnits(ddlBusinessUnits, string.Empty, true);
                    lblBillEmailId.Text = Session[UMS_Resource.SESSION_USER_EMAILID].ToString();
                }
                //else
                //{
                //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                //}
            }
        }

        //private void BindCyclesOrFeeders()
        //{
        //    switch (this.BillProcessType)
        //    {
        //        case (int)EnumBillProcessType.CycleWsie:
        //            _ObjCommonMethods.BindNOCyclesList(ddlCycleList, true);
        //            break;
        //        case (int)EnumBillProcessType.FeederWise:
        //            _ObjCommonMethods.BindFeedersList(ddlCycleList, UMS_Resource.DROPDOWN_SELECT);
        //            break;
        //    }
        //}

        protected void btnConfirmationOk_Click(object sender, EventArgs e)
        {
            if (SaveBillGenerationCustomers() > 0)
            {
                EmailSend();
                divStatusSuccess.Visible = true;
                divStatusError.Visible = false;
                EnableSteps(divBillingMessage);
                ClearAllControls();
                //lblBillThankyouMessage.Text = string.Format(Resource.BILLS_ARE_SCHEDULED, "10");
                if (Session[UMS_Resource.SESSION_USER_EMAILID].ToString() != null)
                    lblBillThankyouMessage.Text = string.Format(Resource.BILL_THANKYOU_MESSAGE, Session[UMS_Resource.SESSION_USER_EMAILID].ToString());
                else
                    lblBillThankyouMessage.Text = string.Format(Resource.BILL_THANKYOU_MESSAGE, string.Empty);
                Message(Resource.BILL_GEN_SUC_MSG_BOX, UMS_Resource.MESSAGETYPE_SUCCESS);
            }
            else
            {
                lblStatusError.Text = Resource.BILLS_ARE_NOT_SCHEDULED;
                divStatusSuccess.Visible = false;
                divStatusError.Visible = true;
                Message(Resource.BILLS_ARE_NOT_SCHEDULED, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        protected void rblBillProcessTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //BindCyclesOrFeeders();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        BindMonths();
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //    }
        //}

        protected void btnNext_Click(object sender, EventArgs e)
        {
            hfIsValid.Value = "0";
            string selectedProcessType = string.Empty;
            if (IsCustomersExists())
            {
                EnableSteps(divStep2);
                lblSelectedCycles.Text = _ObjCommonMethods.CollectSelectedItems(cblCycles, ",");

                string[] SelectedCycles = lblSelectedCycles.Text.Split(',');

                DataTable dtSelectedCycles = new DataTable();
                dtSelectedCycles.Columns.Add("CycleName", typeof(string));
                for (int i = 0; i < SelectedCycles.Length; i++)
                {
                    dtSelectedCycles.Rows.Add(SelectedCycles[i]);
                }

                dtlSelectedCycles.DataSource = dtSelectedCycles;
                dtlSelectedCycles.DataBind();


                lblBillingMonth.Text = this.SelectedMonth;
                lblGeneratedBY.Text = Session[UMS_Resource.SESSION_UserName].ToString() + "(" + Session[UMS_Resource.SESSION_USER_EMAILID].ToString() + ")";
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                _objBillGenerationBe = IsBillExitsForSelectedMonth();
                if (_objBillGenerationBe.IsActive)
                {
                    divMessages.Visible = true;
                    gvCustomerDetails.Visible = divStep2GridView.Visible = false;
                    if (_objBillGenerationBe.IsExists)
                        //lblBillingAlertMessage.Text = Resource.BILL_GENERATION_EXITS_MESSAGE;//"Already Bill Generated for the selected Months to selected Cycle/Feeder";
                        lblBillingAlertMessage.Text = Resource.BILL_GENERATION_CYCLE_EXITS_MESSAGE;
                    else if (!_objBillGenerationBe.IsExistingProcessType)
                    {
                        lblBillingAlertMessage.Text = Resource.BILL_UNIQUE_PROCESSTYPE_MESSAGE;//"Please select Unique Process type for selected Month";
                        btnRegeneration.Visible = false;
                    }
                    else
                    {
                        divStep2GridView.Visible = true;
                        divMessages.Visible = false;
                    }
                    if (_objBillGenerationBe.IsExistingProcessType)
                    {
                        btnRegeneration.Visible = gvCustomerDetails.Visible = true;
                        //divStep2GridView.Visible = true;
                        //divMessages.Visible = false;
                        BindCustomersDetails();
                    }
                }
                else
                {
                    divStep2GridView.Visible = true;
                    divMessages.Visible = false;
                    BindCustomersDetails();
                }
                GetDisabledBookDetails();

                //hfIsValid.Value = "1";
                //XmlDocument xmlResult = new XmlDocument();
                //BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                //_objBillGenerationBe.BillingMonth = this.MonthId;
                //_objBillGenerationBe.BillingYear = this.YearId;
                //_objBillGenerationBe.BillSendingMode = this.BillSendingMode;
                //_objBillGenerationBe.BillProcessTypeId = this.BillProcessType;
                //switch (this.BillProcessType)
                //{
                //    case (int)EnumBillProcessType.CycleWsie:
                //        _objBillGenerationBe.CycleId = this.BillCycleFeederValue;
                //        _objBillGenerationBe.FeederId = string.Empty; ;
                //        selectedProcessType = Resource.CYCLE;
                //        break;
                //    case (int)EnumBillProcessType.FeederWise:
                //        _objBillGenerationBe.CycleId = string.Empty;
                //        _objBillGenerationBe.FeederId = this.BillCycleFeederValue;
                //        selectedProcessType = Resource.FEEDER;
                //        break;
                //}

                //xmlResult = objBillGenerationBal.InsertDetails(_objBillGenerationBe, Statement.Insert);
                //_objBillGenerationBe = objIdsignBal.DeserializeFromXml<BillGenerationBe>(xmlResult);
                //if (_objBillGenerationBe.BillingQueueScheduleId > 0)
                //{
                //    lblThankYouMessage.Text = String.Format(Resource.BILL_GENERATION_THANKU, selectedProcessType, selectedProcessType);
                //    divBillThankuMsg.Attributes.Add("style", "background-color: #639B00;");
                //    mpAfterBillGeneration.Show();
                //}
            }
            else
            {
                hfIsValid.Value = "0";
                switch (this.BillProcessType)
                {
                    case (int)EnumBillProcessType.CycleWsie:
                        selectedProcessType = Resource.CYCLE;
                        break;
                    case (int)EnumBillProcessType.FeederWise:
                        selectedProcessType = Resource.FEEDER;
                        break;
                }
                lblThankYouMessage.Text = String.Format(Resource.BILL_CUSTOMER_NOT_EXISTS, selectedProcessType, selectedProcessType);
                divBillThankuMsg.Attributes.Add("style", "background-color: Red;");
                mpAfterBillGeneration.Show();
            }

        }

        protected void btnThankyouOK_Click(object sender, EventArgs e)
        {
            if (hfIsValid.Value == "1")
            {
                Response.Redirect(Constants.BillGenerationstep2, true);
            }
        }

        protected void lbtnPrevious_Click(object sender, EventArgs e)
        {
            EnableSteps(divStep1);
        }

        protected void btnGoToBilling_Click(object sender, EventArgs e)
        {
            EnableSteps(divStep1);
        }

        protected void btnGoNextBilling_Click(object sender, EventArgs e)
        {
            EnableSteps(divStep1);
        }

        protected void btnRegeneration_Click(object sender, EventArgs e)
        {
            if (UpdateBillGeneration() > 0)
            {
                EmailSend();
                divStatusSuccess.Visible = true;
                divStatusError.Visible = false;
                EnableSteps(divBillingMessage);
                ClearAllControls();
                if (Session[UMS_Resource.SESSION_USER_EMAILID].ToString() != null)
                    lblBillThankyouMessage.Text = string.Format(Resource.BILL_THANKYOU_MESSAGE, Session[UMS_Resource.SESSION_USER_EMAILID].ToString());
                else
                    lblBillThankyouMessage.Text = string.Format(Resource.BILL_THANKYOU_MESSAGE, string.Empty);
                Message(Resource.BILL_GEN_SUC_MSG_BOX, UMS_Resource.MESSAGETYPE_SUCCESS);
            }
            else
            {
                lblStatusError.Text = string.Format(Resource.BILL_GEN_NOT_SUCD, string.Empty);
                divStatusSuccess.Visible = false;
                divStatusError.Visible = true;
                Message(Resource.BILL_GEN_NOT_SUC_MSG_BOX, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        protected void ddlBusinessUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    if (ddlBusinessUnits.SelectedIndex > 0)
            //    {
            //        _ObjCommonMethods.BindServiceUnits(ddlServiceUnits, this.BusinessUnit, true);
            //        ddlServiceUnits.Enabled = true;
            //        ddlServiceCenters.Items.Clear();
            //        cblCycles.Items.Clear();
            //        //ddlCycleList.Items.Clear();
            //        ddlServiceCenters.Enabled = false; //ddlCycleList.Enabled =
            //        ddlServiceUnits_SelectedIndexChanged(this, null);
            //    }
            //    else
            //    {
            //        ddlServiceCenters.Items.Clear();
            //        //ddlCycleList.Items.Clear();
            //        cblCycles.Items.Clear();
            //        ddlServiceUnits.Items.Clear();
            //        ddlServiceCenters.Enabled = ddlServiceUnits.Enabled = false;//ddlCycleList.Enabled =
            //    }
            //    cbAll.Checked = false;
            //}
            //catch (Exception ex)
            //{
            //    _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
            //    try
            //    {
            //        _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            //    }
            //    catch (Exception logex)
            //    {

            //    }
            //}
        }

        //protected void ddlServiceUnits_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ddlServiceUnits.SelectedIndex > 0)
        //        {
        //            _ObjCommonMethods.BindServiceCenters(ddlServiceCenters, this.ServiceUnit, true);
        //            ddlServiceCenters.Enabled = true;
        //            //ddlCycleList.Items.Clear();
        //            cblCycles.Items.Clear();
        //            ddlServiceCenters_SelectedIndexChanged(this, null);
        //        }
        //        else
        //        {
        //            ddlServiceCenters.Items.Clear();
        //            //ddlCycleList.Items.Clear();
        //            cblCycles.Items.Clear();
        //            ddlServiceCenters.Enabled = false; //ddlCycleList.Enabled =
        //        }
        //        cbAll.Checked = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void cblServiceUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divDisabledBooks.Visible = divInValidReadings.Visible = divInValidPaymentBatches.Visible = divInvalidAdustmentBatches.Visible = divEstimationValid.Visible = divErrorMessges.Visible = false;
                //divAfterRead.Visible = false;
                cblCycles.Items.Clear();
                cblServiceCenters.Items.Clear();
                chkSCAll.Checked = divServiceCenters.Visible = divCycles.Visible = false;
                if (!string.IsNullOrEmpty(this.ServiceUnit))
                {
                    _ObjCommonMethods.BindUserServiceCenters_SuIds(cblServiceCenters, this.ServiceUnit, Resource.DDL_ALL, false);
                    if (cblServiceCenters.Items.Count > 0)
                    {
                        divServiceCenters.Visible = true;
                    }
                    //else divDisabledBooks.Visible = divInValidReadings.Visible = divServiceCenters.Visible = divErrorMessges.Visible = false;
                }
                //else divDisabledBooks.Visible = divInValidReadings.Visible = divServiceCenters.Visible = divErrorMessges.Visible = false;
                cbAll.Checked = false;
                CallBGStatusBindFrmSession();
            }
            catch (Exception ex)
            {
                _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //protected void ddlServiceCenters_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ddlServiceCenters.SelectedIndex > 0)
        //        {
        //            _ObjCommonMethods.BindCyclesByBUSUSC(cblCycles, Session[UMS_Resource.SESSION_USER_BUID].ToString(), this.ServiceUnit, this.ServiceCenter, true, "Select Cycle", true);
        //            //ddlCycleList.Enabled = true;
        //        }
        //        else
        //        {
        //            //ddlCycleList.Items.Clear();
        //            cblCycles.Items.Clear();
        //            //ddlCycleList.Enabled = false;
        //        }
        //        cbAll.Checked = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void cblServiceCenters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divDisabledBooks.Visible = divInValidReadings.Visible = divInValidPaymentBatches.Visible = divInvalidAdustmentBatches.Visible = divEstimationValid.Visible = divErrorMessges.Visible = false;
                //divAfterRead.Visible = false;
                //cblCycles.Items.Clear();
                divCycles.Visible = false;
                if (!string.IsNullOrEmpty(this.ServiceCenter))
                {
                    //_ObjCommonMethods.BindCyclesByBUSUSC(cblCycles, Session[UMS_Resource.SESSION_USER_BUID].ToString(), this.ServiceUnit, this.ServiceCenter, true, string.Empty, false);
                    string BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                    BindCY_BY_BU(cblCycles, BU_ID, this.ServiceUnit, this.ServiceCenter, true, string.Empty, false);
                    if (cblCycles.Items.Count > 0)
                        divCycles.Visible = true;
                    else
                        divCycles.Visible = false;
                }
                //else
                //{
                //    divDisabledBooks.Visible = divCycles.Visible = divInValidReadings.Visible = divInValidPaymentBatches.Visible = divInvalidAdustmentBatches.Visible = divEstimationValid.Visible = divErrorMessges.Visible = false;
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDummyforCycle_Click(object sender, EventArgs e)
        {
            divDisabledBooks.Visible = divInValidReadings.Visible = divInValidPaymentBatches.Visible = divInvalidAdustmentBatches.Visible = divEstimationValid.Visible = divErrorMessges.Visible = false;
            //cblServiceCenters_SelectedIndexChanged(sender, e);
            if (!string.IsNullOrEmpty(this.Cycles))
            {
                if (CheckTariffEnergyCharges())
                    CheckDisableBookDetails();
                bool IsValid = true;
                if (!CheckIsReadingDone(this.MonthId, this.YearId, this.Cycles))
                    IsValid = false;
                if (!CheckPaymentBatchClose(this.MonthId, this.YearId))
                    IsValid = false;
                if (!CheckAdjustmentBatchClose(this.MonthId, this.YearId))
                    IsValid = false;
                if (!CheckIsEstimationDone(this.MonthId, this.YearId, this.Cycles))
                    IsValid = false;
                if (IsValid)
                {
                    btnNext.Visible = true;
                    divErrorMessges.Visible = false;
                }
                else
                    divErrorMessges.Visible = true;
            }
            CallBGStatusBindFrmSession();
            //else
            //    divDisabledBooks.Visible = divInValidReadings.Visible = divInValidPaymentBatches.Visible = divInvalidAdustmentBatches.Visible = divEstimationValid.Visible = divErrorMessges.Visible = false;
        }

        private bool CheckTariffEnergyCharges()
        {
            bool isValid = false;
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            BillGenerationListBe _objBillGenerationListBe = new BillGenerationListBe();
            _objBillGenerationBe.BillingMonth = this.MonthId;
            _objBillGenerationBe.BillingYear = this.YearId;
            _objBillGenerationBe.CycleId = this.Cycles;
            XmlDocument xmlResult = _objBillGenerationBal.GetBillCycleDetails(_objBillGenerationBe, ReturnType.Check);
            _objBillGenerationListBe = _objIdsignBal.DeserializeFromXml<BillGenerationListBe>(xmlResult);
            if (_objBillGenerationListBe.Items.Count > 0)
            {
                if (!_objBillGenerationListBe.Items[0].IsExists)
                {
                    divafterCycles.Visible = false;
                    var result = string.Join(",", _objBillGenerationListBe.Items.AsEnumerable()
                                                 .Select(p => p.TariffName.ToString()));
                    lblTariffEnergies.Text = string.Format(Resource.PROVIDE_TARIFF_ENERGY_CYCLES, result);
                    PanelTarifEnergy.CssClass = "billingmessage error";
                    isValid = false;
                }
                else
                {
                    lblTariffEnergies.Text = PanelTarifEnergy.CssClass = string.Empty;
                    divafterCycles.Visible = true;
                    isValid = true;
                }
            }
            return isValid;
        }

        private void CheckDisableBookDetails()
        {
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            BillGenerationListBe _objBillGenerationListBe = new BillGenerationListBe();
            _objBillGenerationBe.BillingMonth = this.MonthId;
            _objBillGenerationBe.BillingYear = this.YearId;
            _objBillGenerationBe.CycleId = this.Cycles;
            XmlDocument xmlResult = _objBillGenerationBal.GetBillCycleDetails(_objBillGenerationBe, ReturnType.Group);
            _objBillGenerationListBe = _objIdsignBal.DeserializeFromXml<BillGenerationListBe>(xmlResult);
            if (_objBillGenerationListBe.Items.Count > 0)
            {
                divDisabledBooks.Visible = true;
                var result = string.Join(",", _objBillGenerationListBe.Items.AsEnumerable()
                                             .Select(p => p.BookNo.ToString()));
                lblDisabledBooks.Text = result.ToString();
            }
            else
            {
                lblDisabledBooks.Text = string.Empty;
                divDisabledBooks.Visible = false;
            }
            //rblReadings.SelectedIndex = rblAdjustments.SelectedIndex = rblBatches.SelectedIndex = rblEstimation.SelectedIndex = -1;
        }

        private void GetDisabledBookDetails()
        {
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            BillGenerationListBe _objBillGenerationListBe = new BillGenerationListBe();
            _objBillGenerationBe.BillingMonth = this.MonthId;
            _objBillGenerationBe.BillingYear = this.YearId;
            _objBillGenerationBe.CycleId = this.Cycles;
            XmlDocument xmlResult = _objBillGenerationBal.GetBillCycleDetails(_objBillGenerationBe, ReturnType.Single);
            _objBillGenerationBe = _objIdsignBal.DeserializeFromXml<BillGenerationBe>(xmlResult);

            lblDisableCustomers.Text = _objBillGenerationBe.TotalRecords.ToString();
        }

        //protected void btnSave_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (rblReadings.SelectedValue == "0")
        //        {
        //            mpeBatchFinished.Show();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }

        //}
        #endregion

        #region Methods

        private void BindOpenMonthAndYear()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                xmlResult = _objBillGenerationBal.GetDetails(ReturnType.Single);
                EstimationBE objEstimationBE = _objIdsignBal.DeserializeFromXml<EstimationBE>(xmlResult);
                if (objEstimationBE != null)
                {
                    divsc.Visible = divNote.Visible = true;
                    hfMonthID.Value = objEstimationBE.Month.ToString();
                    hfYearID.Value = objEstimationBE.Year.ToString();
                    lblMonth.Text = objEstimationBE.MonthName;
                    lblYear.Text = objEstimationBE.Year.ToString();
                    lblNoBillingMonths.Visible = false;
                }
                else
                {
                    lblNoBillingMonths.Text = "There is no Bill Open Month.";
                    divsc.Visible = divNote.Visible = false;
                    lblNoBillingMonths.Visible = true;

                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //private void BindYears()
        //{
        //    try
        //    {

        //        XmlDocument xmlResult = new XmlDocument();
        //        xmlResult = _objBillGenerationBal.GetDetails(ReturnType.Multiple);
        //        BillGenerationListBe objBillsListBE = _objIdsignBal.DeserializeFromXml<BillGenerationListBe>(xmlResult);
        //        if (objBillsListBE.Items.Count > 0)
        //        {
        //            lblYear.Text = objBillsListBE.Items[0].Year.ToString();
        //            hfYearID.Value = objBillsListBE.Items[0].Year.ToString();
        //            // _objIdsignBal.FillDropDownList(_objIdsignBal.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlYear, "Year", "Year", "Year", true);
        //            BindMonths();
        //            divAlert.Visible = false;
        //            divBillInput.Visible = true;
        //        }
        //        else
        //        {
        //            divAlert.Visible = true;
        //            divBillInput.Visible = false;
        //        }

        //        /// Old Code
        //        //int CurrentYear = DateTime.Now.Year;
        //        //int FromYear = Convert.ToInt32(GlobalConstants.TARIFF_ENTRY_NO_OF_LAST_YEARS);
        //        //for (int i = FromYear; i < CurrentYear; i++)
        //        //{
        //        //    ListItem item = new ListItem();
        //        //    item.Text = item.Value = i.ToString();
        //        //    ddlYear.Items.Add(item);

        //        //}
        //        //for (int i = 0; i <= Convert.ToInt32(GlobalConstants.TARIFF_ENTRY_NO_OF_PRESENT_AND_FUTURE_YEARS); i++)
        //        //{
        //        //    ListItem item = new ListItem();
        //        //    item.Text = item.Value = (CurrentYear + i).ToString();
        //        //    ddlYear.Items.Add(item);
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //private void BindMonths()
        //{
        //    try
        //    {
        //        XmlDocument xmlResult = new XmlDocument();
        //        BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
        //        _objBillGenerationBe.Year = this.YearId;
        //        xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.Get);
        //        BillGenerationListBe objBillsListBE = _objIdsignBal.DeserializeFromXml<BillGenerationListBe>(xmlResult);
        //        if (objBillsListBE.Items.Count > 0)
        //        {
        //            lblMonth.Text = objBillsListBE.Items[0].MonthName;
        //            hfMonthID.Value = objBillsListBE.Items[0].Month.ToString();
        //            divAlert.Visible = false;
        //            divBillInput.Visible = true;
        //            // _objIdsignBal.FillDropDownList(_objIdsignBal.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlMonth, "MonthName", "Month", "Month", true);
        //        }
        //        else
        //        {
        //            divAlert.Visible = true;
        //            divBillInput.Visible = false;
        //        }
        //        //DataSet dsMonths = new DataSet();
        //        //dsMonths.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["Months"]));
        //        //objIdsignBal.FillDropDownList(dsMonths.Tables[0], ddlMonth, "name", "value", UMS_Resource.DROPDOWN_SELECT, false);

        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void CheckBoxesJS()
        {
            ChkSUAll.Attributes.Add("onclick", "CheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            chkSCAll.Attributes.Add("onclick", "CheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");

            cblServiceUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            cblServiceCenters.Attributes.Add("onclick", "UnCheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
        }

        private void BindBillProcessTypes()
        {
            _ObjCommonMethods.BindBillProcessTypes(rblBillProcessTypes, string.Empty);
            if (rblBillProcessTypes.Items.Count == 1)
            {
                rblBillProcessTypes.Items[0].Selected = true;
            }
        }

        private int SaveBillGenerationCustomers()
        {
            XmlDocument xmlResult = new XmlDocument();
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            try
            {
                _objBillGenerationBe.BillingMonth = this.MonthId;
                _objBillGenerationBe.BillingYear = this.YearId;
                _objBillGenerationBe.BillSendingMode = this.BillSendingMode;
                _objBillGenerationBe.BillProcessTypeId = this.BillProcessType;
                _objBillGenerationBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                switch (this.BillProcessType)
                {
                    case (int)EnumBillProcessType.CycleWsie:
                        _objBillGenerationBe.CycleId = this.Cycles; //this.BillCycleFeederValue;
                        _objBillGenerationBe.FeederId = string.Empty;
                        //selectedProcessType = Resource.CYCLE;
                        break;
                    case (int)EnumBillProcessType.FeederWise:
                        _objBillGenerationBe.CycleId = string.Empty;
                        _objBillGenerationBe.FeederId = this.Cycles; //this.BillCycleFeederValue;
                        //selectedProcessType = Resource.FEEDER;
                        break;
                }

                xmlResult = _objBillGenerationBal.InsertDetails(_objBillGenerationBe, Statement.Insert);
                _objBillGenerationBe = _objIdsignBal.DeserializeFromXml<BillGenerationBe>(xmlResult);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
            return _objBillGenerationBe.TotalRecords;
            //if (_objBillGenerationBe.TotalRecords > 0)
            //{
            //    EnableSteps(divStep3);
            //    ClearAllControls();
            //    //lblThankYouMessage.Text = Resource.BILL_GENERATION_THANKU;
            //    //divBillThankuMsg.Attributes.Add("style", "background-color: #639B00;");
            //    //mpAfterBillGeneration.Show();
            //}
        }

        private int UpdateBillGeneration()
        {
            XmlDocument xmlResult = new XmlDocument();
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            try
            {

                _objBillGenerationBe.BillingMonth = this.MonthId;
                _objBillGenerationBe.BillingYear = this.YearId;
                _objBillGenerationBe.BillSendingMode = this.BillSendingMode;
                _objBillGenerationBe.BillProcessTypeId = this.BillProcessType;
                _objBillGenerationBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                switch (this.BillProcessType)
                {
                    case (int)EnumBillProcessType.CycleWsie:
                        _objBillGenerationBe.CycleId = this.Cycles;// this.BillCycleFeederValue;
                        _objBillGenerationBe.FeederId = string.Empty; ;
                        //selectedProcessType = Resource.CYCLE;
                        break;
                    case (int)EnumBillProcessType.FeederWise:
                        _objBillGenerationBe.CycleId = string.Empty;
                        _objBillGenerationBe.FeederId = this.Cycles;//this.BillCycleFeederValue;
                        //selectedProcessType = Resource.FEEDER;
                        break;
                }

                xmlResult = _objBillGenerationBal.UpdateBillDetails(_objBillGenerationBe, Statement.Change);
                _objBillGenerationBe = _objIdsignBal.DeserializeFromXml<BillGenerationBe>(xmlResult);

                //if (_objBillGenerationBe.BillingQueueScheduleId > 0)
                //{
                //    EnableSteps(divStep3);
                //    ClearAllControls();
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
            return _objBillGenerationBe.TotalRecords;
        }

        //private void ClearAllControls()
        //{
        //    ddlServiceUnits.SelectedIndex = ddlMonth.SelectedIndex = ddlYear.SelectedIndex = 0;//ddlCycleList.SelectedIndex =
        //    ddlServiceCenters.Items.Clear();
        //    cbAll.Checked = false;
        //    rblReadings.SelectedIndex = rblBatches.SelectedIndex = rblAdjustments.SelectedIndex = rblEstimation.SelectedIndex = -1;//rblBillProcessTypes.SelectedIndex =  cblCycles.SelectedIndex = 
        //}

        private void ClearAllControls()
        {
            //ddlMonth.SelectedIndex = ddlYear.SelectedIndex = 0;//ddlCycleList.SelectedIndex =
            hfMonthID.Value = hfYearID.Value = "0";//ddlCycleList.SelectedIndex =
            cblServiceCenters.Items.Clear();
            cblServiceUnits.Items.Clear();
            cblCycles.Items.Clear();
            divafterCycles.Visible = divCycles.Visible = divAlert.Visible = divServiceCenters.Visible = false;
            cbAll.Checked = false;
            BindBillProcessTypes();
            BindServiceUnits();
            CheckBoxesJS();
            //lblBillEmailId.Text = Session[UMS_Resource.SESSION_USER_EMAILID].ToString();
            ChkSUAll.Checked = false;
        }

        private void CreateDirectory(string path)
        {
            bool exists = System.IO.Directory.Exists(path);

            if (!exists)
                System.IO.Directory.CreateDirectory(path);
        }

        private BillGenerationBe IsBillExitsForSelectedMonth()
        {
            XmlDocument xmlResult = new XmlDocument();
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            _objBillGenerationBe.BillingMonth = this.MonthId;
            _objBillGenerationBe.BillingYear = this.YearId;
            _objBillGenerationBe.BillProcessTypeId = this.BillProcessType;
            switch (this.BillProcessType)
            {
                case (int)EnumBillProcessType.CycleWsie:
                    _objBillGenerationBe.CycleId = this.Cycles;//this.BillCycleFeederValue;
                    _objBillGenerationBe.FeederId = string.Empty;
                    break;
                case (int)EnumBillProcessType.FeederWise:
                    _objBillGenerationBe.CycleId = string.Empty; ;
                    _objBillGenerationBe.FeederId = this.Cycles;//this.BillCycleFeederValue;
                    break;
            }
            xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.Fetch);
            _objBillGenerationBe = _objIdsignBal.DeserializeFromXml<BillGenerationBe>(xmlResult);
            return _objBillGenerationBe;
        }

        private void BindCustomersDetails()
        {
            XmlDocument xmlResult = new XmlDocument();
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            BillGenerationListBe _objBillGenerationListBe = new BillGenerationListBe();
            switch (this.BillProcessType)
            {
                case (int)EnumBillProcessType.CycleWsie:
                    _objBillGenerationBe.CycleId = this.Cycles;// this.BillCycleFeederValue;
                    _objBillGenerationBe.FeederId = string.Empty; ;
                    break;
                case (int)EnumBillProcessType.FeederWise:
                    _objBillGenerationBe.CycleId = string.Empty;
                    _objBillGenerationBe.FeederId = this.Cycles;//this.BillCycleFeederValue;
                    break;
            }
            _objBillGenerationBe.Year = this.YearId;
            _objBillGenerationBe.Month = this.MonthId;
            xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.List);
            _objBillGenerationListBe = _objIdsignBal.DeserializeFromXml<BillGenerationListBe>(xmlResult);
            //lblTotal.Text = _objBillGenerationListBe.Items.Count.ToString();
            gvCustomerDetails.DataSource = _objBillGenerationListBe.Items;
            gvCustomerDetails.DataBind();

            ((Label)gvCustomerDetails.FooterRow.FindControl("lblTotalCustomers")).Text = lblTotal.Text = (from x in _objBillGenerationListBe.Items.AsEnumerable() select x.TotalRecords).Sum().ToString();
            ((Label)gvCustomerDetails.FooterRow.FindControl("lblTotalUsage")).Text = (from x in _objBillGenerationListBe.Items.AsEnumerable() select x.Usage).Sum().ToString();

        }

        private void EnableSteps(System.Web.UI.HtmlControls.HtmlGenericControl divStep)
        {
            //divStep1.Visible = divStep2.Visible = divStep3.Visible = false;
            divStep1.Visible = divStep2.Visible = divBillingMessage.Visible = false;
            divStep.Visible = true;
            CallBGStatusBindFrmSession();
        }

        private bool IsCustomersExists()
        {
            XmlDocument xmlResult = new XmlDocument();
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            switch (this.BillProcessType)
            {
                case (int)EnumBillProcessType.CycleWsie:
                    _objBillGenerationBe.CycleId = this.Cycles;//this.BillCycleFeederValue;
                    _objBillGenerationBe.FeederId = string.Empty;
                    break;
                case (int)EnumBillProcessType.FeederWise:
                    _objBillGenerationBe.CycleId = string.Empty; ;
                    _objBillGenerationBe.FeederId = this.Cycles;//this.BillCycleFeederValue;
                    break;
            }
            xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.Check);
            _objBillGenerationBe = _objIdsignBal.DeserializeFromXml<BillGenerationBe>(xmlResult);
            return _objBillGenerationBe.IsExists;
        }

        //private void BindServiceUnits()
        //{
        //    _ObjCommonMethods.BindServiceUnits(cblServiceUnits, Session[UMS_Resource.SESSION_USER_BUID].ToString(), true);
        //    ddlServiceCenters.Items.Clear();
        //    cblCycles.Items.Clear();
        //    ddlServiceCenters.Enabled = false;
        //    //if (ddlServiceUnits.SelectedIndex > 0)  commented by karthik
        //    //    ddlServiceUnits_SelectedIndexChanged(this, null);
        //}

        private void BindServiceUnits()
        {
            string BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
            _ObjCommonMethods.BindServiceUnits(cblServiceUnits, BU_ID, true);
            cblServiceCenters.Items.Clear();
            cblCycles.Items.Clear();
            divServiceCenters.Visible = divCycles.Visible = false;
            //if (ddlServiceUnits.SelectedIndex > 0)  commented by karthik
            //    ddlServiceUnits_SelectedIndexChanged(this, null);
        }

        //public void BindSU_BY_BU(Control ctrl, string su_Ids, string selectText, bool isSelectFirstItem)
        //{
        //    MastersBE _ObjMastersBE = new MastersBE();
        //    MastersListBE objMastersListBE = new MastersListBE();
        //    MastersBAL objMastersBAL = new MastersBAL();

        //    XmlDocument xmlResult = new XmlDocument();
        //    _ObjMastersBE.SU_ID = su_Ids;
        //    _ObjMastersBE.UserId = HttpContext.Current.Session[UMS_Resource.SESSION_LOGINID].ToString();
        //    xmlResult = objMastersBAL.BindCycleByBU(_ObjMastersBE);
        //    if (xmlResult != null)
        //    {
        //        objMastersListBE = _objIdsignBal.DeserializeFromXml<MastersListBE>(xmlResult);
        //        if (objMastersListBE.Items.Count > 0)
        //        {

        //            cblCycles.DataValueField = "ServiceCenterId";
        //            cblCycles.DataTextField = "ServiceCenterName";
        //            cblCycles.DataSource = objMastersListBE.Items;
        //            cblCycles.DataBind();
        //            foreach (ListItem lstItem in cblServiceCenters.Items)
        //            {
        //                //var count = objMastersListBE.Items.Where(x => x.ServiceCenterId == lstItem.Value).Select(y => y.BillGenCount).ToList();
        //                var masterList = from x in objMastersListBE.Items
        //                                             where x.ServiceCenterId == lstItem.Value
        //                                             select new 
        //                                       {
        //                                          BillGenCount= x.BillGenCount,
        //                                          PaymentCount=x.PaymentCount
        //                                       };
        //                foreach(var s in masterList)
        //                {
        //                    if (s.BillGenCount > 0)
        //                    {
        //                        lstItem.Enabled = false;
        //                        lstItem.Attributes.Add("", "");
        //                    }
        //                }
        //            }
        //        }
        //    }

        //}

        public void BindCY_BY_BU(Control ctrl, string BU_ID, string SU_ID, string ServiceCenterId, bool IsRequiredSelect, string selectText, bool selectFirstItem) //This Method and BindCyclesList methods are same only diff is select value passing from aspx.cs page
        {
            try
            {
                string[] CycleList = null;
                if (Cycles != "")
                {
                    CycleList = Cycles.Split(',');
                }
                //BindYears();
                //BindOpenMonthAndYear();
                MastersBE _ObjMastersBE = new MastersBE();
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBAL objMastersBAL = new MastersBAL();

                XmlDocument xmlResult = new XmlDocument();
                _ObjMastersBE.BU_ID = BU_ID;
                _ObjMastersBE.SU_ID = SU_ID;
                _ObjMastersBE.BillYear = YearId.ToString();
                _ObjMastersBE.BillMonth = MonthId.ToString();
                _ObjMastersBE.ServiceCenterId = ServiceCenterId;
                xmlResult = objMastersBAL.BindCycleByBU(_ObjMastersBE);
                if (xmlResult != null)
                {
                    objMastersListBE = _objIdsignBal.DeserializeFromXml<MastersListBE>(xmlResult);

                    if (objMastersListBE.Items.Count > 0)
                    {
                        Session["BookGroupColorStatus"] = objMastersListBE;
                        BookGroupStatusBind(objMastersListBE, false);
                        //cblCycles.DataValueField = "CycleId";
                        //cblCycles.DataTextField = "CycleName";
                        //cblCycles.DataSource = objMastersListBE.Items;
                        //cblCycles.DataBind();
                        //foreach (ListItem lstItem in cblCycles.Items)
                        //{
                        //    //var count = objMastersListBE.Items.Where(x => x.ServiceCenterId == lstItem.Value).Select(y => y.BillGenCount).ToList();
                        //    var masterList = from x in objMastersListBE.Items
                        //                     where x.CycleId == lstItem.Value
                        //                     select new
                        //                     {
                        //                         BillGenCount = x.BillGenCount,
                        //                         PaymentCount = x.PaymentCount,
                        //                         BillAdjustmentCount = x.BillAdjustmentCount,
                        //                         TotalCustomers = x.TotalCustomers
                        //                     };


                        //    foreach (var BookGroupList in masterList)
                        //    {
                        //        if (BookGroupList.PaymentCount > 0 && BookGroupList.BillAdjustmentCount > 0)
                        //        {
                        //            lstItem.Enabled = false;
                        //            lstItem.Attributes.Add("title", "Bill Generated, Payment and Adjustment Received.");
                        //            lstItem.Attributes.Add("Style", "color: #FF8000;");
                        //        }
                        //        else if (BookGroupList.PaymentCount > 0)
                        //        {
                        //            lstItem.Enabled = false;
                        //            lstItem.Attributes.Add("title", "Bill Generated and Payment Received.");
                        //            lstItem.Attributes.Add("Style", "color: #B40486;");
                        //        }
                        //        else if (BookGroupList.BillAdjustmentCount > 0)
                        //        {
                        //            lstItem.Enabled = false;
                        //            lstItem.Attributes.Add("title", "Bill Generated and Adjustment Received.");
                        //            lstItem.Attributes.Add("Style", "color: #58ACFA;");
                        //        }
                        //        else if (BookGroupList.TotalCustomers == 0)
                        //        {
                        //            lstItem.Enabled = false;
                        //            lstItem.Attributes.Add("title", "No Customer Is Available.");
                        //            lstItem.Attributes.Add("Style", "color: ThreeDShadow;");
                        ////        }
                        //        else if (BookGroupList.BillGenCount > 0)
                        //        {
                        //            lstItem.Enabled = true;
                        //            lstItem.Attributes.Add("title", "Bill Generated.");
                        //            lstItem.Attributes.Add("Style", "color: #FE2E64;");
                        //        }
                        //    }
                        //    if (CycleList != null)
                        //    {
                        //        if (!string.IsNullOrEmpty(Array.Find(CycleList, element => element.Equals(lstItem.Value))))
                        //        {
                        //            lstItem.Selected = true;
                        //        }
                        //    }
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                Message("Sorry cannot bind service center. There is some error.", UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        private void EmailSend()//Neeraj ID077
        {
            try
            {
                SendMailScheduledBill delReferee = new SendMailScheduledBill(_ObjCommonMethods.SendMailScheduledBill);
                delReferee.BeginInvoke(CyclesTextList, MonthId.ToString(), YearId.ToString(), Session[UMS_Resource.SESSION_USER_EMAILID].ToString(), null, null);
                System.Threading.Thread.Sleep(500);
            }
            catch (Exception ex)
            {
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        private void BookGroupStatusBind(MastersListBE objMastersListBE, bool IsSession)
        {
            if (objMastersListBE.Items.Count > 0)
            {
                if (!IsSession)
                {
                    cblCycles.DataValueField = "CycleId";
                    cblCycles.DataTextField = "CycleName";
                    cblCycles.DataSource = objMastersListBE.Items;
                    cblCycles.DataBind();
                }
                foreach (ListItem lstItem in cblCycles.Items)
                {
                    //var count = objMastersListBE.Items.Where(x => x.ServiceCenterId == lstItem.Value).Select(y => y.BillGenCount).ToList();
                    var masterList = from x in objMastersListBE.Items
                                     where x.CycleId == lstItem.Value
                                     select new
                                     {
                                         BillGenCount = x.BillGenCount,
                                         PaymentCount = x.PaymentCount,
                                         BillAdjustmentCount = x.BillAdjustmentCount,
                                         TotalCustomers = x.TotalCustomers
                                     };
                    foreach (var BookGroupList in masterList)
                    {
                        if (BookGroupList.PaymentCount > 0 && BookGroupList.BillAdjustmentCount > 0)
                        {
                            lstItem.Enabled = false;
                            lstItem.Attributes.Add("title", "Bill Generated, Payment and Adjustment Received.");
                            lstItem.Attributes.Add("Style", "color: #FF8000;");
                        }
                        else if (BookGroupList.PaymentCount > 0)
                        {
                            lstItem.Enabled = false;
                            lstItem.Attributes.Add("title", "Bill Generated and Payment Received.");
                            lstItem.Attributes.Add("Style", "color: #B40486;");
                        }
                        else if (BookGroupList.BillAdjustmentCount > 0)
                        {
                            lstItem.Enabled = false;
                            lstItem.Attributes.Add("title", "Bill Generated and Adjustment Received.");
                            lstItem.Attributes.Add("Style", "color: #58ACFA;");
                        }
                        else if (BookGroupList.TotalCustomers == 0)
                        {
                            lstItem.Enabled = false;
                            lstItem.Attributes.Add("title", "No Customer Is Available.");
                            lstItem.Attributes.Add("Style", "color: ThreeDShadow;");
                        }
                        else if (BookGroupList.BillGenCount > 0)
                        {
                            lstItem.Enabled = true;
                            lstItem.Attributes.Add("title", "Bill Generated.");
                            lstItem.Attributes.Add("Style", "color: #FE2E64;");
                        }
                    }
                }
            }
        }

        private void CallBGStatusBindFrmSession()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["BookGroupColorStatus"])))
            {
                _objMasterListBe = (MastersListBE)Session["BookGroupColorStatus"];
                BookGroupStatusBind(_objMasterListBe, true);
            }
        }
        #endregion

        #region WebMethods

        //[WebMethod(EnableSession = true)]
        public bool CheckIsReadingDone(int billingMonth, int billingYear, string cycleId)
        {
            string InValidCycles = string.Empty;
            XmlDocument xmlResult = new XmlDocument();
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            BillGenerationListBe _objBillGenerationListBe = new BillGenerationListBe();
            BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
            iDsignBAL _objiDsignBAL = new iDsignBAL();

            _objBillGenerationBe.CycleId = cycleId;
            _objBillGenerationBe.BillingMonth = billingMonth;
            _objBillGenerationBe.BillingYear = billingYear;

            xmlResult = _objBillGenerationBal.CheckBillValidations(_objBillGenerationBe, ReturnType.Get);
            _objBillGenerationListBe = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);

            if (_objBillGenerationListBe.Items.Count > 0)
            {
                if (!_objBillGenerationListBe.Items[0].IsExists)
                {
                    InValidCycles = string.Join(",", (from item in _objBillGenerationListBe.Items
                                                      select item.CycleName));
                }
            }

            if (!string.IsNullOrEmpty(InValidCycles))
            {
                divInValidReadings.Visible = true;
                //lblReadingsCycles.Text = InValidCycles;
                dtlReadingsCycles.DataSource = _objBillGenerationListBe.Items;
                dtlReadingsCycles.DataBind();
                return false;
            }
            else
            {
                divInValidReadings.Visible = false;
                //lblReadingsCycles.Text = string.Empty;
                dtlReadingsCycles.DataSource = new DataTable();
                dtlReadingsCycles.DataBind();
                return true;
            }
            //return InValidCycles;
        }



        //[WebMethod(EnableSession = true)]
        public bool CheckPaymentBatchClose(int billingMonth, int billingYear)
        {
            string InValidCycles = string.Empty;
            XmlDocument xmlResult = new XmlDocument();
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
            iDsignBAL _objiDsignBAL = new iDsignBAL();

            _objBillGenerationBe.BusinessUnitName = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
            _objBillGenerationBe.BillingMonth = billingMonth;
            _objBillGenerationBe.BillingYear = billingYear;

            xmlResult = _objBillGenerationBal.CheckBillValidations(_objBillGenerationBe, ReturnType.Fetch);
            _objBillGenerationBe = _objiDsignBAL.DeserializeFromXml<BillGenerationBe>(xmlResult);

            if (_objBillGenerationBe.IsExists)
            {
                divInValidPaymentBatches.Visible = true;

                string[] PaymentBatches = _objBillGenerationBe.OpenBatches.Split(',');

                DataTable dtPaymentBatches = new DataTable();
                dtPaymentBatches.Columns.Add("PaymentBatch", typeof(string));
                for (int i = 0; i < PaymentBatches.Length; i++)
                {
                    dtPaymentBatches.Rows.Add(PaymentBatches[i]);
                }

                dtlPaymentBatches.DataSource = dtPaymentBatches;
                dtlPaymentBatches.DataBind();
                //lblPaymentBatches.Text = _objBillGenerationBe.OpenBatches;
                return false;
            }
            else
            {
                divInValidPaymentBatches.Visible = false;
                //lblPaymentBatches.Text = string.Empty;
                dtlPaymentBatches.DataSource = new DataTable();
                dtlPaymentBatches.DataBind();
                return true;
            }

            //return _objBillGenerationBe.IsExists.ToString().ToLower() + "|" + _objBillGenerationBe.OpenBatches;
        }

        //[WebMethod(EnableSession = true)]
        public bool CheckAdjustmentBatchClose(int billingMonth, int billingYear)
        {
            string InValidCycles = string.Empty;
            XmlDocument xmlResult = new XmlDocument();
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
            iDsignBAL _objiDsignBAL = new iDsignBAL();

            _objBillGenerationBe.BusinessUnitName = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
            _objBillGenerationBe.BillingMonth = billingMonth;
            _objBillGenerationBe.BillingYear = billingYear;

            xmlResult = _objBillGenerationBal.CheckBillValidations(_objBillGenerationBe, ReturnType.Single);
            _objBillGenerationBe = _objiDsignBAL.DeserializeFromXml<BillGenerationBe>(xmlResult);

            if (_objBillGenerationBe.IsExists)
            {
                divInvalidAdustmentBatches.Visible = true;
                //lblAdjustmentBatches.Text = _objBillGenerationBe.OpenBatches;
                string[] AdjustmentBatches = _objBillGenerationBe.OpenBatches.Split(',');

                DataTable dtAdjustmentBatches = new DataTable();
                dtAdjustmentBatches.Columns.Add("AdjustmentBatch", typeof(string));
                for (int i = 0; i < AdjustmentBatches.Length; i++)
                {
                    dtAdjustmentBatches.Rows.Add(AdjustmentBatches[i]);
                }

                dtlAdjustmentBatches.DataSource = dtAdjustmentBatches;
                dtlAdjustmentBatches.DataBind();
                return false;
            }
            else
            {
                divInvalidAdustmentBatches.Visible = false;
                //lblAdjustmentBatches.Text = string.Empty;
                dtlAdjustmentBatches.DataSource = new DataTable();
                dtlAdjustmentBatches.DataBind();
                return true;
            }

            //return _objBillGenerationBe.IsExists.ToString().ToLower() + "|" + _objBillGenerationBe.OpenBatches;
        }

        //[WebMethod(EnableSession = true)]
        public bool CheckIsEstimationDone(int billingMonth, int billingYear, string cycleId)
        {
            string InValidCycles = string.Empty;
            XmlDocument xmlResult = new XmlDocument();
            BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
            BillGenerationListBe _objBillGenerationListBe = new BillGenerationListBe();
            BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
            iDsignBAL _objiDsignBAL = new iDsignBAL();

            _objBillGenerationBe.CycleId = cycleId;
            _objBillGenerationBe.BillingMonth = billingMonth;
            _objBillGenerationBe.BillingYear = billingYear;

            xmlResult = _objBillGenerationBal.CheckBillValidations(_objBillGenerationBe, ReturnType.Check);
            _objBillGenerationListBe = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);

            if (_objBillGenerationListBe.Items.Count > 0)
            {
                if (!_objBillGenerationListBe.Items[0].IsExists)
                {
                    InValidCycles = string.Join(",", (from item in _objBillGenerationListBe.Items
                                                      select item.CycleName));
                }
            }

            if (!string.IsNullOrEmpty(InValidCycles))
            {
                divEstimationValid.Visible = true;
                //lblInValidEstimation.Text = InValidCycles;
                dtlInValidEstimation.DataSource = _objBillGenerationListBe.Items;
                dtlInValidEstimation.DataBind();
                return false;
            }
            else
            {
                divEstimationValid.Visible = false;
                //lblInValidEstimation.Text = string.Empty;
                dtlInValidEstimation.DataSource = _objBillGenerationListBe.Items;
                dtlInValidEstimation.DataBind();
                return true;
            }

            //return InValidCycles;
        }

        #endregion
    }
}