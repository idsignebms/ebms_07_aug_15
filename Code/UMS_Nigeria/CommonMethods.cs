﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Security;
using System.IO;
using System.Data;
using System.Collections;
using System.Web.UI;
using System.Configuration;
using System.Diagnostics;
using System.Xml;
using UMS_NigeriaBE;
using Resources;
using UMS_NigeriaBAL;
using iDSignHelper;
using UMS_NigriaDAL;
using ClosedXML.Excel;
using System.Globalization;
using System.Net;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Xml.Linq;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace UMS_Nigeria
{
    public class CommonMethods
    {
        #region Members

        StackTrace st = new StackTrace(true);
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        ConsumerBal objConsumerBal = new ConsumerBal();
        PoleBAL objPoleBal = new PoleBAL();
        MastersBAL objMastersBAL = new MastersBAL();
        BillReadingsBAL _objBillingBal = new BillReadingsBAL();
        BillCalculatorBAL objBillCalculatorBal = new BillCalculatorBAL();
        UserManagementBAL objUserManagementBAL = new UserManagementBAL();
        iDsignBAL objIdsignBal = new iDsignBAL();
        GlobalMessagesBAL _objGlobalMessageBal = new GlobalMessagesBAL();
        TariffManagementBal objTariffManagementBal = new TariffManagementBal();
        BillGenerationBal objBillGenerationBal = new BillGenerationBal();
        ChangeBookNoBal _objChangeBookNoBal = new ChangeBookNoBal();
        ReportsBal objReportsBal = new ReportsBal();
        MastersBAL _ObjMastersBAL = new MastersBAL();
        MastersBE _ObjMastersBE = new MastersBE();
        ApprovalBal _objApprovalBal = new ApprovalBal();
        BillingDisabledBooksBal _objBillingDisabledBooksBal = new BillingDisabledBooksBal();
        UserManagementBE _ObjUserManagementBE = new UserManagementBE();//Neeraj-ID077
        ApprovalBal objApprovalBal = new ApprovalBal();
        MarketersBal _objMarketersBal = new MarketersBal();
        EmployeeBAL objEmployeeBAL = new EmployeeBAL();
        LGABAL _ObjLGABAL = new LGABAL();
        RegionsBAL _objRegionsBAL = new RegionsBAL();
        AdminBAL _objAdminBal = new AdminBAL();
        GovtAccountTypeBAL _objGovtAccountTypeBAL = new GovtAccountTypeBAL();
        UserDefinedControlsBAL objUserDefinedControlsBAL = new UserDefinedControlsBAL();
        #endregion

        #region Methods

        public void BindUnMappedBooksList(Control ctrl, bool IsRequiredSelect)
        {
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Retrieve(ReturnType.Set));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlBookNos = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlBookNos, "BookNo", "BookNo", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlBookNos, "BookNo", "BookNo", false);
                    break;
            }
        }

        public void BindCyclesList(Control ctrl, bool IsRequiredSelect)
        {
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Retrieve(ReturnType.Group));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlCycles = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlCycles, "CycleName", "CycleId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlCycles, "CycleName", "CycleId", false);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblCycles = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), cblCycles, "CycleName", "CycleId");
                    break;
            }
        }

        public void BindAllCyclesList(Control ctrl, bool IsRequiredSelect, string selectText, bool selectFirstItem) //This Method and BindCyclesList methods are same only diff is select value passing from aspx.cs page
        {
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Retrieve(ReturnType.Group));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlCycles = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlCycles, "CycleName", "CycleId", selectText, selectFirstItem);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlCycles, "CycleName", "CycleId", false);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblCycles = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), cblCycles, "CycleName", "CycleId");
                    break;
            }
        }

        public void BindCyclesByBUSUSC(Control ctrl, string BU_ID, string SU_ID, string ServiceCenterId, bool IsRequiredSelect, string selectText, bool selectFirstItem) //This Method and BindCyclesList methods are same only diff is select value passing from aspx.cs page
        {
            MastersBE objMastersBE = new MastersBE();
            objMastersBE.BU_ID = BU_ID;
            objMastersBE.SU_ID = SU_ID;
            objMastersBE.ServiceCenterId = ServiceCenterId;
            MastersListBE objMastersListBE = objIdsignBal.DeserializeFromXml<MastersListBE>(objMastersBAL.Cycle(objMastersBE, ReturnType.Fetch));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlCycles = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), ddlCycles, "CycleName", "CycleId", selectText, selectFirstItem);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), ddlCycles, "CycleName", "CycleId", selectFirstItem);
                    break;
                case "CheckBoxList":
                    CheckBoxList cbl = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), cbl, "CycleName", "CycleId");
                    break;

            }
        }

        public void BindCyclesBySC_HavingBooks(Control ctrl, string ServiceCenterId, bool IsRequiredSelect, string selectText, bool selectFirstItem) //This Method and BindCyclesList methods are same only diff is select value passing from aspx.cs page
        {
            MastersBE objMastersBE = new MastersBE();
            objMastersBE.ServiceCenterId = ServiceCenterId;
            MastersListBE objMastersListBE = objIdsignBal.DeserializeFromXml<MastersListBE>(objMastersBAL.Cycle(objMastersBE, ReturnType.List));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlCycles = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), ddlCycles, "CycleName", "CycleId", selectText, selectFirstItem);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), ddlCycles, "CycleName", "CycleId", selectFirstItem);
                    break;
                case "CheckBoxList":
                    CheckBoxList cbl = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), cbl, "CycleName", "CycleId");
                    break;

            }
        }

        public void BindNOCyclesList(Control ctrl, bool IsRequiredSelect) //insted of select nocycle text Naresh
        {
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Retrieve(ReturnType.Group));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlCycles = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlCycles, "CycleName", "CycleId", Resource.DDL_SELECT_CYCLE, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlCycles, "CycleName", "CycleId", false);
                    break;
            }
        }

        public void BindEmployeesList(Control ctrl, bool IsRequiredSelect)
        {
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Retrieve(ReturnType.Multiple));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlEmployeesList = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlEmployeesList, "UserId", "UserId", UMS_Resource.DROPDOWN_SELECT, true);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlEmployeesList, "UserId", "UserId", true);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblEmployeesList = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), cblEmployeesList, "UserId", "UserId");
                    break;
            }
        }

        public void BindProcessedBy(Control ctrl, bool IsRequiredSelect)
        {
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Retrieve(ReturnType.Get));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlProcessedBy = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlProcessedBy, "ProcessedBy", "ProccessedById", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlProcessedBy, "ProcessedBy", "ProccessedById", false);
                    break;
                case "RadioButtonList":
                    RadioButtonList rblProcessedBy = (RadioButtonList)ctrl;
                    objIdsignBal.FillRadioButtonList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), rblProcessedBy, "ProcessedBy", "ProcessedById");
                    break;
            }
        }

        public void BindCustomerTypes(Control ctrl, bool IsRequiredSelect)
        {
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Get(ReturnType.Group));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlCustomerTypes = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlCustomerTypes, "CustomerType", "CustomerTypeId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlCustomerTypes, "CustomerType", "CustomerTypeId", false);
                    break;
            }
        }

        //For CustomerTypeChange
        public void BindCustomerTypes(Control ctrl, int CustomerTypeId, bool IsRequiredSelect)
        {
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Get(ReturnType.Group));
            objConsumerListBe.items.Remove(objConsumerListBe.items.Find(x => x.CustomerTypeId == CustomerTypeId));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlCustomerTypes = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlCustomerTypes, "CustomerType", "CustomerTypeId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlCustomerTypes, "CustomerType", "CustomerTypeId", false);
                    break;
            }
        }

        public void BindCashiers(Control ctrl, bool IsRequiredSelect)
        {
            BillingBE objBillingBE = new BillingBE();
            BillReadingsBAL objBillingBal = new BillReadingsBAL();
            GlobalMessagesListBE objBillingListBe = objIdsignBal.DeserializeFromXml<GlobalMessagesListBE>(objBillingBal.GetCashierDetailsBAL());
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlCustomerTypes = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillingBE>(objBillingListBe.CustomerReading), ddlCustomerTypes, "Name", "UserId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillingBE>(objBillingListBe.CustomerReading), ddlCustomerTypes, "Name", "UserId", false);
                    break;
            }
        }

        public void BindCashiers(Control ctrl, int CashOfficeId, string selectText, bool IsRequiredSelect)
        {
            CashierBE objCashierBE = new CashierBE();
            objCashierBE.CashOfficeId = CashOfficeId;
            CashierBAL objCashierBAL = new CashierBAL();
            CashierListBE objCashierListBE = objIdsignBal.DeserializeFromXml<CashierListBE>(objCashierBAL.GetCashier(objCashierBE, ReturnType.Get));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlCustomerTypes = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<CashierBE>(objCashierListBE.Items), ddlCustomerTypes, "Cashier", "CashierId", selectText, IsRequiredSelect);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<CashierBE>(objCashierListBE.Items), ddlCustomerTypes, "Cashier", "CashierId", false);
                    break;
            }
        }

        public void BindReadCodes(Control ctrl, bool IsRequiredSelect)
        {
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Get(ReturnType.Bulk));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlReadCodes = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlReadCodes, "ReadCode", "ReadCodeId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlReadCodes, "ReadCode", "ReadCodeId", false);
                    break;
                case "RadioButtonList":
                    RadioButtonList rdblReadCode = (RadioButtonList)ctrl;
                    objIdsignBal.FillRadioButtonList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), rdblReadCode, "ReadCode", "ReadCodeId");
                    break;
            }
        }

        public void BindAccountTypes(Control ctrl, bool IsRequiredSelect)
        {
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Get(ReturnType.Retrieve));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlAccountTypes = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlAccountTypes, "AccountType", "AccountTypeId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlAccountTypes, "AccountType", "AccountTypeId", false);
                    break;
            }
        }

        public void BindMeterTypes(Control ctrl, bool IsRequiredSelect)
        {
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Get(ReturnType.Set));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlMeterTypes = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlMeterTypes, "MeterType", "MeterTypeId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlMeterTypes, "MeterType", "MeterTypeId", false);
                    break;
            }
        }

        public void BindMeterStatus(Control ctrl, bool IsRequiredSelect)
        {
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Get(ReturnType.Single));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlMeterStatus = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlMeterStatus, "MeterStatus", "MeterStatusId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlMeterStatus, "MeterStatus", "MeterStatusId", false);
                    break;
            }
        }

        public void BindAgencies(Control ctrl, bool IsRequiredSelect)
        {
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Retrieve(ReturnType.Retrieve));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlMeterStatus = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlMeterStatus, "AgencyName", "AgencyId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlMeterStatus, "AgencyName", "AgencyId", false);
                    break;
            }
        }

        public void BindBookNumbersByServiceDetails(Control ctrl, string BU_ID, string SU_ID, string ServiceCenterId, bool IsRequiredSelect)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            objConsumerBe.BU_ID = BU_ID;
            objConsumerBe.SU_ID = SU_ID;
            objConsumerBe.ServiceCenterId = ServiceCenterId;
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.GetServiceDetails(objConsumerBe, ReturnType.Multiple));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlBookNumbers = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlBookNumbers, "BookNo", "BookNo", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlBookNumbers, "BookNo", "BookNo", false);
                    break;
            }
        }

        public void BindTariffSubTypes(Control ctrl, int ClassId, bool IsRequiredSelect)
        {
            BillCalculatorBE objBillCalculatorBe = new BillCalculatorBE();
            objBillCalculatorBe.ClassId = ClassId;
            BillCalculatorListBE objBillCalculatorListBe = objIdsignBal.DeserializeFromXml<BillCalculatorListBE>(objBillCalculatorBal.GetCustomerSubTypes(objBillCalculatorBe, ReturnType.Multiple));

            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlTariffSubTypes = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillCalculatorBE>(objBillCalculatorListBe.items), ddlTariffSubTypes, "CustomerSubTypeNames", "ClassId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillCalculatorBE>(objBillCalculatorListBe.items), ddlTariffSubTypes, "CustomerSubTypeNames", "ClassId", false);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblTariff = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<BillCalculatorBE>(objBillCalculatorListBe.items), cblTariff, "CustomerSubTypeNames", "ClassId");
                    break;
            }
        }

        public void BindUsersForMeterReadings(Control ctrl, bool IsRequiredSelect)// Added By Karteek 25, Mar 2015
        {
            UserManagementListBE objUserManagementListBE = objIdsignBal.DeserializeFromXml<UserManagementListBE>(objUserManagementBAL.Get(ReturnType.Get));

            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlUsers = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<UserManagementBE>(objUserManagementListBE.Items), ddlUsers, "Name", "UserId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<UserManagementBE>(objUserManagementListBE.Items), ddlUsers, "Name", "UserId", false);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblUsers = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<UserManagementBE>(objUserManagementListBE.Items), cblUsers, "Name", "UserId");
                    break;
            }
        }

        public void BindMeterReadingsFrom(Control ctrl, bool IsRequiredSelect)// Added By Karteek 25, Mar 2015
        {
            RptMeterReadingsListBe objReportsListBe = objIdsignBal.DeserializeFromXml<RptMeterReadingsListBe>(objReportsBal.Get(ReturnType.List));

            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlReadingsFrom = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<RptMeterReadingBE>(objReportsListBe.items), ddlReadingsFrom, "MeterReadingFrom", "MeterReadingFromId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<RptMeterReadingBE>(objReportsListBe.items), ddlReadingsFrom, "MeterReadingFrom", "MeterReadingFromId", false);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblReadingsFrom = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<RptMeterReadingBE>(objReportsListBe.items), cblReadingsFrom, "MeterReadingFrom", "MeterReadingFromId");
                    break;
            }
        }

        public void BindUsersForAverageUploads(Control ctrl, bool IsRequiredSelect)// Added By Karteek 26, Mar 2015
        {
            UserManagementListBE objUserManagementListBE = objIdsignBal.DeserializeFromXml<UserManagementListBE>(objUserManagementBAL.Get(ReturnType.Multiple));

            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlUsers = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<UserManagementBE>(objUserManagementListBE.Items), ddlUsers, "Name", "UserId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<UserManagementBE>(objUserManagementListBE.Items), ddlUsers, "Name", "UserId", false);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblUsers = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<UserManagementBE>(objUserManagementListBE.Items), cblUsers, "Name", "UserId");
                    break;
            }
        }

        public void BindUsersForPayments(Control ctrl, bool IsRequiredSelect)// Added By Karteek 26, Mar 2015
        {
            UserManagementListBE objUserManagementListBE = objIdsignBal.DeserializeFromXml<UserManagementListBE>(objUserManagementBAL.Get(ReturnType.Retrieve));

            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlUsers = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<UserManagementBE>(objUserManagementListBE.Items), ddlUsers, "Name", "UserId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<UserManagementBE>(objUserManagementListBE.Items), ddlUsers, "Name", "UserId", false);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblUsers = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<UserManagementBE>(objUserManagementListBE.Items), cblUsers, "Name", "UserId");
                    break;
            }
        }

        public void BindUsersForAdjustments(Control ctrl, bool IsRequiredSelect)// Added By Karteek 26, Mar 2015
        {
            UserManagementListBE objUserManagementListBE = objIdsignBal.DeserializeFromXml<UserManagementListBE>(objUserManagementBAL.Get(ReturnType.Fetch));

            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlUsers = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<UserManagementBE>(objUserManagementListBE.Items), ddlUsers, "Name", "UserId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<UserManagementBE>(objUserManagementListBE.Items), ddlUsers, "Name", "UserId", false);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblUsers = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<UserManagementBE>(objUserManagementListBE.Items), cblUsers, "Name", "UserId");
                    break;
            }
        }

        public void BindAdjustmentTypes(Control ctrl, bool IsRequiredSelect)// Added By Karteek 26, Mar 2015
        {
            RptAdjustmentEditListListBe objReportsListBe = objIdsignBal.DeserializeFromXml<RptAdjustmentEditListListBe>(objReportsBal.Get(ReturnType.Get));

            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlReadingsFrom = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<RptAdjustmentEditListBE>(objReportsListBe.items), ddlReadingsFrom, "AdjustmentName", "AdjustmentId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<RptAdjustmentEditListBE>(objReportsListBe.items), ddlReadingsFrom, "AdjustmentName", "AdjustmentId", false);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblReadingsFrom = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<RptAdjustmentEditListBE>(objReportsListBe.items), cblReadingsFrom, "AdjustmentName", "AdjustmentId");
                    break;
            }
        }

        public void BindPaymentTypes(Control ctrl, bool IsRequiredSelect)// Added By Karteek 27, Mar 2015
        {
            RptPaymentsEditListListBe objReportsListBe = objIdsignBal.DeserializeFromXml<RptPaymentsEditListListBe>(objReportsBal.Get(ReturnType.Multiple));

            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlReadingsFrom = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<RptPaymentsEditListBE>(objReportsListBe.items), ddlReadingsFrom, "TransactionFrom", "TransactionFromId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<RptPaymentsEditListBE>(objReportsListBe.items), ddlReadingsFrom, "TransactionFrom", "TransactionFromId", false);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblReadingsFrom = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<RptPaymentsEditListBE>(objReportsListBe.items), cblReadingsFrom, "TransactionFrom", "TransactionFromId");
                    break;
            }
        }

        public void BindTotalTariffSubTypes(Control ctrl, int ClassId, bool IsRequiredSelect, string selectText)
        {
            BillCalculatorBE objBillCalculatorBe = new BillCalculatorBE();
            objBillCalculatorBe.ClassId = ClassId;
            BillCalculatorListBE objBillCalculatorListBe = objIdsignBal.DeserializeFromXml<BillCalculatorListBE>(objBillCalculatorBal.GetCustomerSubTypes(objBillCalculatorBe, ReturnType.Multiple));

            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlTariffSubTypes = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillCalculatorBE>(objBillCalculatorListBe.items), ddlTariffSubTypes, "CustomerSubTypeNames", "ClassId", selectText, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillCalculatorBE>(objBillCalculatorListBe.items), ddlTariffSubTypes, "CustomerSubTypeNames", "ClassId", false);
                    break;
            }
        }

        public void BindTariffSubTypesWithOutCustomerHavingTariff(Control ctrl, int ClassId, string SubTypeName, bool IsRequiredSelect, string selectText, bool selectFirstItem)
        {
            BillCalculatorBE objBillCalculatorBe = new BillCalculatorBE();
            objBillCalculatorBe.ClassId = ClassId;
            objBillCalculatorBe.CustomerSubTypeNames = SubTypeName;
            BillCalculatorListBE objBillCalculatorListBe = objIdsignBal.DeserializeFromXml<BillCalculatorListBE>(objBillCalculatorBal.GetCustomerSubTypes(objBillCalculatorBe, ReturnType.Multiple));

            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlTariffSubTypes = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillCalculatorBE>(objBillCalculatorListBe.items), ddlTariffSubTypes, "CustomerSubTypeNames", "ClassId", selectText, selectFirstItem);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillCalculatorBE>(objBillCalculatorListBe.items), ddlTariffSubTypes, "CustomerSubTypeNames", "ClassId", selectFirstItem);
                    break;
            }
        }

        public void BindTariff(Control ctrl, bool IsRequiredSelect)
        {
            BillCalculatorListBE objBillCalculatorListBe = new BillCalculatorListBE();
            objBillCalculatorListBe = objIdsignBal.DeserializeFromXml<BillCalculatorListBE>(objBillCalculatorBal.GetCustomerType(ReturnType.Multiple));

            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlTariff = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillCalculatorBE>(objBillCalculatorListBe.items), ddlTariff, "CustomerTypeName", "ClassId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillCalculatorBE>(objBillCalculatorListBe.items), ddlTariff, "CustomerTypeName", "ClassId", false);
                    break;
            }
        }

        public void BindTariffChargeTypes(Control ctrl, int ChargeId, bool IsRequiredSelect)
        {
            TariffManagementBE objTariffManagementBE = new TariffManagementBE();
            objTariffManagementBE.ChargeId = ChargeId;
            TariffManagementListBE objTariffManagementListBE = new TariffManagementListBE();
            objTariffManagementListBE = objIdsignBal.DeserializeFromXml<TariffManagementListBE>(objTariffManagementBal.Get(objTariffManagementBE, ReturnType.Get));

            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlTariffCharge = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<TariffManagementBE>(objTariffManagementListBE.items), ddlTariffCharge, "ChargeName", "ChargeId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<TariffManagementBE>(objTariffManagementListBE.items), ddlTariffCharge, "ChargeName", "ChargeId", false);
                    break;
                //case "DataList":
                //    DataList dtlist = new DataList();
                //    if (objTariffManagementListBE.items.Count > 0)
                //    {
                //        dtlist.DataSource = objTariffManagementListBE.items;
                //        dtlist.DataBind();
                //    }

                //    break;
            }
        }

        public void BindConnectionReasons(Control ctrl, bool IsRequiredSelect)
        {
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Get(ReturnType.Double));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlConnectionReasons = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlConnectionReasons, "Reason", "ConnectionReasonId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlConnectionReasons, "Reason", "ConnectionReasonId", false);
                    break;
            }
        }

        public void BindPhases(Control ctrl, bool IsRequiredSelect)
        {
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Get(ReturnType.Multiple));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlPhases = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlPhases, "Phase", "PhaseId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlPhases, "Phase", "PhaseId", false);
                    break;
            }
        }

        public void BindCountries(Control ctrl, bool IsRequiredSelect, bool IsSelectFirstItem)
        {
            MastersBE objMastersBE = new MastersBE();
            MastersListBE objMastersListBE = new MastersListBE();
            objMastersListBE = objIdsignBal.DeserializeFromXml<MastersListBE>(objMastersBAL.GetCountries(ReturnType.Get));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlCountries = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), ddlCountries, "CountryName", "CountryCode", UMS_Resource.DROPDOWN_SELECT, IsSelectFirstItem);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), ddlCountries, "CountryName", "CountryCode", IsSelectFirstItem);
                    break;
            }
        }

        public void BindLGA(Control ctrl, bool IsRequiredSelect, bool IsSelectFirstItem)
        {
            LGABE _ObjLGABE = new LGABE();
            LGAListBE objLGAListBE = new LGAListBE();
            objLGAListBE = objIdsignBal.DeserializeFromXml<LGAListBE>(_ObjLGABAL.LGA(_ObjLGABE, ReturnType.List));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlLGA = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<LGABE>(objLGAListBE.items), ddlLGA, "LGAName", "LGAId", UMS_Resource.DROPDOWN_SELECT, IsSelectFirstItem);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<LGABE>(objLGAListBE.items), ddlLGA, "LGAName", "LGAId", IsSelectFirstItem);
                    break;
            }
        }

        public void BindBookNo(Control ctrl, string BU_ID, string selectText, bool IsSelectFirstItem)
        {
            MastersBE objMastersBE = new MastersBE();
            MastersListBE objMastersListBE = new MastersListBE();
            objMastersBE.BU_ID = BU_ID;
            objMastersListBE = objIdsignBal.DeserializeFromXml<MastersListBE>(objMastersBAL.BookNumber(objMastersBE, ReturnType.List));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlBook = (DropDownList)ctrl;
                    objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), ddlBook, "BookNo", "BookNo", selectText, IsSelectFirstItem);
                    break;
            }
        }

        public void BindStates(Control ctrl, string CountryCode, bool IsRequiredSelect)
        {
            MastersBE objMastersBE = new MastersBE();
            MastersListBE objMastersListBE = new MastersListBE();
            objMastersBE.CountryCode = CountryCode;
            objMastersListBE = objIdsignBal.DeserializeFromXml<MastersListBE>(objMastersBAL.Get(objMastersBE, ReturnType.Single));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlStates = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), ddlStates, "StateName", "StateCode", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), ddlStates, "StateName", "StateCode", false);
                    break;
            }
        }

        public void BindPaymentModes(Control ctrl, bool IsRequiredSelect, string selectText, bool isSelectFirstItem)
        {
            MastersBE objMastersBE = new MastersBE();
            MastersListBE objMastersListBE = new MastersListBE();
            objMastersListBE = objIdsignBal.DeserializeFromXml<MastersListBE>(objMastersBAL.PaymentEntry(ReturnType.Get));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlPaymentMode = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), ddlPaymentMode, "PaymentMode", "PaymentModeId", selectText, isSelectFirstItem);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), ddlPaymentMode, "PaymentMode", "PaymentModeId", isSelectFirstItem);
                    //for (int i = 0; i < objMastersListBE.Items.Count; i++)
                    //{
                    //    if (objMastersListBE.Items[i].IsDefault)
                    //    {
                    //        ddlPaymentMode.SelectedIndex = ddlPaymentMode.Items.IndexOf(ddlPaymentMode.Items.FindByValue(objMastersListBE.Items[i].PaymentModeId.ToString()));
                    //        break;
                    //    }
                    //}
                    break;
            }
        }

        public void BindStatesByDistricts(Control ctrl, string UserId, string DistrictCode, bool IsRequiredSelect)
        {
            MastersBE objMastersBE = new MastersBE();
            MastersListBE objMastersListBE = new MastersListBE();
            objMastersBE.DistrictCode = DistrictCode;
            objMastersBE.UserId = UserId;
            objMastersListBE = objIdsignBal.DeserializeFromXml<MastersListBE>(objMastersBAL.Get(objMastersBE, ReturnType.Fetch));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlStates = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), ddlStates, "StateName", "StateCode", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), ddlStates, "StateName", "StateCode", false);
                    break;
            }
        }

        public void BindStatesByBusinessUnits(Control ctrl, string UserId, string BU_ID, bool IsRequiredSelect, string selectedText, bool selectfirstitem)
        {
            MastersBE objMastersBE = new MastersBE();
            MastersListBE objMastersListBE = new MastersListBE();
            objMastersBE.BU_ID = BU_ID;
            objMastersBE.UserId = UserId;
            objMastersListBE = objIdsignBal.DeserializeFromXml<MastersListBE>(objMastersBAL.GetBusinessUnits(objMastersBE, ReturnType.Fetch));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlStates = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), ddlStates, "StateName", "StateCode", selectedText, selectfirstitem);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), ddlStates, "StateName", "StateCode", false);
                    break;
            }
        }

        public void BindDistricts(Control ctrl, string StateCode, bool IsRequiredSelect)
        {
            MastersBE objMastersBE = new MastersBE();
            MastersListBE objMastersListBE = new MastersListBE();
            objMastersBE.StateCode = StateCode;
            objMastersBE.UserId = HttpContext.Current.Session[UMS_Resource.SESSION_LOGINID].ToString();
            objMastersListBE = objIdsignBal.DeserializeFromXml<MastersListBE>(objMastersBAL.GetBusinessUnits(objMastersBE, ReturnType.Get));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlDistricts = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), ddlDistricts, "DistrictName", "DistrictCode", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), ddlDistricts, "DistrictName", "DistrictCode", false);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblDistricts = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), cblDistricts, "DistrictState", "DistrictCode");
                    break;
            }
        }

        public void BindGender(Control ctrl, int GenderId)
        {
            UserManagementBE objUserManagementBE = new UserManagementBE();
            UserManagementListBE objUserManagementListBE = new UserManagementListBE();
            objUserManagementBE.GenderId = GenderId;
            objUserManagementListBE = objIdsignBal.DeserializeFromXml<UserManagementListBE>(objUserManagementBAL.Get(objUserManagementBE, ReturnType.Get));
            switch (ctrl.GetType().Name)
            {
                case "EditableRadioButtonList":
                case "RadioButtonList":
                    RadioButtonList rblGender = (RadioButtonList)ctrl;
                    objIdsignBal.FillRadioButtonList(objIdsignBal.ConvertListToDataSet<UserManagementBE>(objUserManagementListBE.Items), rblGender, "GenderName", "GenderId");
                    break;
            }
        }

        public void BindRoles(Control ctrl, int RoleId, bool IsRequiredSelect)
        {
            UserManagementBE objUserManagementBE = new UserManagementBE();
            UserManagementListBE objUserManagementListBE = new UserManagementListBE();
            objUserManagementBE.RoleId = RoleId;
            objUserManagementListBE = objIdsignBal.DeserializeFromXml<UserManagementListBE>(objUserManagementBAL.Get(objUserManagementBE, ReturnType.Group));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlRoles = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<UserManagementBE>(objUserManagementListBE.Items), ddlRoles, "RoleName", "RoleId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<UserManagementBE>(objUserManagementListBE.Items), ddlRoles, "RoleName", "RoleId", false);
                    break;
            }
        }

        public void BindDesignations(Control ctrl, int DesignationId, bool IsRequiredSelect)
        {
            UserManagementBE objUserManagementBE = new UserManagementBE();
            UserManagementListBE objUserManagementListBE = new UserManagementListBE();
            objUserManagementBE.DesignationId = DesignationId;
            objUserManagementListBE = objIdsignBal.DeserializeFromXml<UserManagementListBE>(objUserManagementBAL.Get(objUserManagementBE, ReturnType.Multiple));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlDesignation = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<UserManagementBE>(objUserManagementListBE.Items), ddlDesignation, "DesignationName", "DesignationId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<UserManagementBE>(objUserManagementListBE.Items), ddlDesignation, "DesignationName", "DesignationId", false);
                    break;
            }
        }

        public void BindInjectionSubStations(Control ctrl, bool IsRequiredSelect)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();

            objConsumerBe.ServiceCenterId = string.Empty;
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Get(objConsumerBe, ReturnType.Get));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlInjectionSubStations = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlInjectionSubStations, "SubStationName", "SubStationId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlInjectionSubStations, "SubStationName", "SubStationId", false);
                    break;
            }
        }

        public void BindAllInjectionSubStations(Control ctrl, bool IsRequiredSelect, string selectText, bool selectfirstvalue) //This Method and BindInjectionSubStations methods are same only diff is select value passing from aspx.cs page
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            objConsumerBe.ServiceCenterId = string.Empty;
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Get(objConsumerBe, ReturnType.Get));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlInjectionSubStations = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlInjectionSubStations, "SubStationName", "SubStationId", selectText, selectfirstvalue);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlInjectionSubStations, "SubStationName", "SubStationId", selectfirstvalue);
                    break;
            }
        }

        public void BindFeeders(Control ctrl, string SubStationId, string SelectedText, bool IsRequiredSelect)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();

            objConsumerBe.SubStationId = SubStationId;
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.GetServiceDetails(objConsumerBe, ReturnType.Get));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlInjectionSubStations = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlInjectionSubStations, "FeederName", "FeederId", SelectedText, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlInjectionSubStations, "FeederName", "FeederId", false);
                    break;
            }
        }

        public void BindTransformers(Control ctrl, string FeederId, bool IsRequiredSelect)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();

            objConsumerBe.FeederId = FeederId;
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.GetServiceDetails(objConsumerBe, ReturnType.Single));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlInjectionSubStations = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlInjectionSubStations, "TransFormerName", "TransFormerId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlInjectionSubStations, "TransFormerName", "TransFormerId", false);
                    break;
            }
        }

        public void BindPoles(Control ctrl, string TransformerId, bool IsRequiredSelect)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            objConsumerBe.TransFormerId = TransformerId;
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.GetServiceDetails(objConsumerBe, ReturnType.Double));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlInjectionSubStations = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlInjectionSubStations, "PoleName", "PoleId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlInjectionSubStations, "PoleName", "PoleId", false);
                    break;
            }
        }

        public void BindAllFeeders(Control ctrl, string SubStationId, string SelectedText, bool IsRequiredSelect, bool selectfirstvalue)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();

            objConsumerBe.SubStationId = SubStationId;
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.GetServiceDetails(objConsumerBe, ReturnType.Get));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlInjectionSubStations = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlInjectionSubStations, "FeederName", "FeederId", SelectedText, selectfirstvalue);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlInjectionSubStations, "FeederName", "FeederId", selectfirstvalue);
                    break;
            }
        } //This Method and BindFeeders methods are same only diff is select value passing from aspx.cs page

        public void BindAllTransformers(Control ctrl, string FeederId, string SelectedText, bool IsRequiredSelect, bool selectfirstvalue)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();

            objConsumerBe.FeederId = FeederId;
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.GetServiceDetails(objConsumerBe, ReturnType.Single));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlInjectionSubStations = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlInjectionSubStations, "TransFormerName", "TransFormerId", SelectedText, selectfirstvalue);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlInjectionSubStations, "TransFormerName", "TransFormerId", selectfirstvalue);
                    break;
            }
        } //This Method and BindTransformers methods are same only diff is select value passing from aspx.cs page

        public void BindAllPoles(Control ctrl, string TransformerId, string SelectedText, bool IsRequiredSelect, bool selectfirstvalue)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            objConsumerBe.TransFormerId = TransformerId;
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.GetServiceDetails(objConsumerBe, ReturnType.Double));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlInjectionSubStations = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlInjectionSubStations, "PoleName", "PoleId", SelectedText, selectfirstvalue);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlInjectionSubStations, "PoleName", "PoleId", selectfirstvalue);
                    break;
            }
        } //This Method and BindPoles methods are same only diff is select value passing from aspx.cs page

        public void BindAllMonths(Control ctrl, bool IsRequiredSelect, string SelectedText, bool selectfirstvalue)
        {
            DataSet dsMonths = new DataSet();
            dsMonths.ReadXml(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["Months"]));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlMonth = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        _ObjiDsignBAL.FillDropDownList(dsMonths.Tables[0], ddlMonth, "name", "value", SelectedText, selectfirstvalue);
                    else
                        objIdsignBal.FillDropDownList(dsMonths.Tables[0], ddlMonth, "name", "value", selectfirstvalue);
                    break;
            }
        }
        public void BindAllOpenYears(Control ctrl, bool IsRequiredSelect, string SelectedText, bool selectfirstvalue)
        {
            BillGenerationListBe objBillsListBE = _ObjiDsignBAL.DeserializeFromXml<BillGenerationListBe>(objBillGenerationBal.GetDetails(ReturnType.Bulk));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlYear = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlYear, "Year", "Year", Resource.DDL_ALL, selectfirstvalue);
                    else
                        _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlYear, "Year", "Year", selectfirstvalue);
                    break;
            }
        }
        public void BindServiceCenters(Control ctrl, string SU_ID, bool IsRequiredSelect)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();

            objConsumerBe.SU_ID = SU_ID;
            objConsumerBe.UserId = HttpContext.Current.Session[UMS_Resource.SESSION_LOGINID].ToString();
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Get(objConsumerBe, ReturnType.Single));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlServiceCenters = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlServiceCenters, "ServiceCenterName", "ServiceCenterId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlServiceCenters, "ServiceCenterName", "ServiceCenterId", false);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblServiceCenters = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), cblServiceCenters, "ServiceCenterName", "ServiceCenterId");
                    break;
            }
        }

        public void BindServiceUnits(Control ctrl, string BU_ID, bool IsRequiredSelect)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();

            objConsumerBe.BU_ID = BU_ID;
            objConsumerBe.UserId = HttpContext.Current.Session[UMS_Resource.SESSION_LOGINID].ToString();
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Get(objConsumerBe, ReturnType.Double));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlServiceUnits = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlServiceUnits, "ServiceUnitName", "SU_ID", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlServiceUnits, "ServiceUnitName", "SU_ID", false);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblServiceUnits = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), cblServiceUnits, "ServiceUnitName", "SU_ID");
                    break;
            }
        }
        public void BindBillingTypes(Control ctrl, bool IsRequiredSelect)
        {
            MastersBE objMasterBe = new MastersBE();
            MastersListBE objMasterListBe = new MastersListBE();
            objMasterListBe = objIdsignBal.DeserializeFromXml<MastersListBE>(_objGlobalMessageBal.GetBillingTypesBAL());
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlServiceUnits = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMasterListBe.Items), ddlServiceUnits, "MessageType", "MessageId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMasterListBe.Items), ddlServiceUnits, "MessageType", "MessageId", false);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblServiceUnits = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMasterListBe.Items), cblServiceUnits, "MessageType", "MessageId");
                    break;
                case "RadioButtonList":
                    RadioButtonList rblServiceUnits = (RadioButtonList)ctrl;
                    objIdsignBal.FillRadioButtonList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMasterListBe.Items).Tables[0], rblServiceUnits, "MessageType", "MessageId");
                    break;
            }
        }

        public void BindCashier(Control ctrl, bool IsRequiredSelect)
        {
            BillingBE objBillingBE = new BillingBE();
            GlobalMessagesListBE objGlobalMessagesListBE = new GlobalMessagesListBE();
            //objGlobalMessagesListBE = objIdsignBal.DeserializeFromXml<GlobalMessagesListBE>(_objGlobalMessageBal.GetBillingTypesBAL(ReturnType.List));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlCashier = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillingBE>(objGlobalMessagesListBE.CustomerReading), ddlCashier, "CashierName", "CashierId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillingBE>(objGlobalMessagesListBE.CustomerReading), ddlCashier, "CashierName", "CashierId", false);
                    break;
            }
        }

        public void BindOffice(Control ctrl, bool IsRequiredSelect)
        {
            BillingBE objBillingBE = new BillingBE();
            GlobalMessagesListBE objGlobalMessagesListBE = new GlobalMessagesListBE();
            objGlobalMessagesListBE = objIdsignBal.DeserializeFromXml<GlobalMessagesListBE>(_objBillingBal.GetCashierOfficesDAL());
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlOffice = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillingBE>(objGlobalMessagesListBE.CustomerReading), ddlOffice, "OfficeName", "OfficeId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillingBE>(objGlobalMessagesListBE.CustomerReading), ddlOffice, "OfficeName", "OfficeId", false);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblBusinessUnits = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<BillingBE>(objGlobalMessagesListBE.CustomerReading), cblBusinessUnits, "OfficeName", "OfficeId");
                    break;
            }
        }

        public void BindOfficeByBU_ID(Control ctrl, string BU_ID, string SU_ID, string SC_ID, bool IsRequiredSelect, bool selectFirstItem, string selectText)
        {
            BillingBE objBillingBE = new BillingBE();
            GlobalMessagesListBE objGlobalMessagesListBE = new GlobalMessagesListBE();
            objBillingBE.BU_ID = BU_ID;
            objBillingBE.SU_ID = SU_ID;
            objBillingBE.SC_ID = SC_ID;
            objGlobalMessagesListBE = objIdsignBal.DeserializeFromXml<GlobalMessagesListBE>(_objBillingBal.Get(objBillingBE, ReturnType.List));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlOffice = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillingBE>(objGlobalMessagesListBE.CustomerReading), ddlOffice, "OfficeName", "OfficeId", selectText, selectFirstItem);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillingBE>(objGlobalMessagesListBE.CustomerReading), ddlOffice, "OfficeName", "OfficeId", selectFirstItem);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblCashOffice = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<BillingBE>(objGlobalMessagesListBE.CustomerReading), cblCashOffice, "OfficeName", "OfficeId");
                    break;
            }
        }

        public void BindOfficeByBU_ID(Control ctrl, string BU_ID, bool IsRequiredSelect, bool selectFirstItem, string selectText)
        {
            BillingBE objBillingBE = new BillingBE();
            GlobalMessagesListBE objGlobalMessagesListBE = new GlobalMessagesListBE();
            objBillingBE.BU_ID = BU_ID;
            objGlobalMessagesListBE = objIdsignBal.DeserializeFromXml<GlobalMessagesListBE>(_objBillingBal.Get(objBillingBE, ReturnType.Multiple));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlOffice = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillingBE>(objGlobalMessagesListBE.CustomerReading), ddlOffice, "OfficeName", "OfficeId", selectText, selectFirstItem);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillingBE>(objGlobalMessagesListBE.CustomerReading), ddlOffice, "OfficeName", "OfficeId", selectFirstItem);
                    break;
            }
        }

        public void BindBusinessUnits(Control ctrl, string StateCode, bool IsRequiredSelect)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();

            objConsumerBe.StateCode = StateCode;
            objConsumerBe.UserId = HttpContext.Current.Session[UMS_Resource.SESSION_LOGINID].ToString();
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Get(objConsumerBe, ReturnType.Multiple));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlBusinessUnits = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlBusinessUnits, "BusinessUnitName", "BU_ID", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlBusinessUnits, "BusinessUnitName", "BU_ID", false);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblBusinessUnits = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), cblBusinessUnits, "BusinessUnitName", "BU_ID");
                    break;
            }
        }

        public void BindUserBusinessUnits(Control ctrl, string StateCode, bool IsRequiredSelect, string selectText)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();

            objConsumerBe.StateCode = StateCode;
            objConsumerBe.UserId = HttpContext.Current.Session[UMS_Resource.SESSION_LOGINID].ToString();
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Get(objConsumerBe, ReturnType.Multiple));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                    // case "DropDownList":
                    //DropDownList ddlBusinessUnits = (DropDownList)ctrl;
                    //if (IsRequiredSelect)
                    //    objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlBusinessUnits, "BusinessUnitName", "BU_ID", selectText, IsRequiredSelect);
                    //else
                    //    objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlBusinessUnits, "BusinessUnitName", "BU_ID", IsRequiredSelect);

                    break;
                case "DropDownList":
                    DropDownList ddlBusinessUnits = (DropDownList)ctrl;
                    _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlBusinessUnits, "BusinessUnitName", "BU_ID", selectText, IsRequiredSelect);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblBusinessUnits = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), cblBusinessUnits, "BusinessUnitName", "BU_ID");
                    break;
            }
        }

        public void BindRegions(Control ctrl, string CountryCode, bool IsRequiredSelect, string selectText)
        {
            RegionsBE _objRegionsBE = new RegionsBE();

            _objRegionsBE.CountryCode = CountryCode;
            RegionsListBE _objRegionsListBE = objIdsignBal.DeserializeFromXml<RegionsListBE>(_objRegionsBAL.Get(_objRegionsBE, ReturnType.List));
            switch (ctrl.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddlRegions = (DropDownList)ctrl;
                    _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<RegionsBE>(_objRegionsListBE.Items), ddlRegions, "RegionName", "RegionId", selectText, IsRequiredSelect);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblRegions = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<RegionsBE>(_objRegionsListBE.Items), cblRegions, "RegionName", "RegionId");
                    break;
            }
        }

        public void BindIdentityTypes(Control ctrl, bool IsRequiredSelect)
        {
            ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Get(ReturnType.Get));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlIdentityTypes = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlIdentityTypes, "IdentityType", "IdentityTypeId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlIdentityTypes, "IdentityType", "IdentityTypeId", false);
                    break;
            }
        }

        public bool IsAccess(string PageName)
        {
            MastersBE objMastersBE = new MastersBE();
            objMastersBE.UserId = HttpContext.Current.Session[UMS_Resource.SESSION_LOGINID].ToString();
            objMastersBE.PageName = PageName.Trim('~');
            objMastersBE = objIdsignBal.DeserializeFromXml<MastersBE>(objMastersBAL.GetBusinessUnits(objMastersBE, ReturnType.Single));
            bool IsPageAccess = objMastersBE.IsPageAccess;
            return IsPageAccess;
        }

        public void BindRoutes(Control ctrl, string BU_ID, bool IsRequiredSelect)
        {
            ConsumerBe objConsumerBE = new ConsumerBe();
            ConsumerListBe objConsumerListBE = new ConsumerListBe();
            objConsumerBE.BU_ID = BU_ID;
            objConsumerListBE = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.GetServiceDetails(objConsumerBE, ReturnType.Check));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlRoutes = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBE.items), ddlRoutes, "Route", "RouteId", UMS_Resource.DROPDOWN_SELECT, true);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBE.items), ddlRoutes, "Route", "RouteId", true);
                    break;
            }
        }



        public void BindBookNumbersWithDetails(Control ctrl, bool IsRequiredSelect)
        {
            ConsumerListBe objConsumerListBE = new ConsumerListBe();
            XmlDocument xml = new XmlDocument();
            xml = objConsumerBal.GetBookNoListBAL();
            objConsumerListBE = objIdsignBal.DeserializeFromXml<ConsumerListBe>(xml);
            switch (ctrl.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddlRoutes = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBE.items), ddlRoutes, "BookNoWithDetails", "BookNo", UMS_Resource.DROPDOWN_SELECT, true);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBE.items), ddlRoutes, "BookNoWithDetails", "BookNo", true);
                    break;
            }
        }

        public void BindBookNumbers(Control ctrl, bool IsRequiredSelect)
        {
            ConsumerListBe objConsumerListBE = new ConsumerListBe();
            XmlDocument xml = new XmlDocument();
            xml = objConsumerBal.GetBookNoListBAL();
            objConsumerListBE = objIdsignBal.DeserializeFromXml<ConsumerListBe>(xml);
            switch (ctrl.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddlRoutes = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBE.items), ddlRoutes, "BookNo", "BookNo", UMS_Resource.DROPDOWN_SELECT, true);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBE.items), ddlRoutes, "BookNo", "BookNo", true);
                    break;
            }
        }
        public void BindAllBookNumbers(Control ctrl, string CycleId, bool IsRequiredSelect, bool selectFirstItem, string selectText)
        {
            ReportsListBe objReportsListBe = new ReportsListBe();
            ReportsBe objReportsBe = new ReportsBe();
            objReportsBe.CycleId = CycleId;
            XmlDocument xml = new XmlDocument();
            xml = objReportsBal.Get(objReportsBe, ReturnType.Check);
            //xml = objReportsBal.GetBooksForBookwiseMeterReading(objReportsBe);
            objReportsListBe = objIdsignBal.DeserializeFromXml<ReportsListBe>(xml);
            switch (ctrl.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddlBookNo = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                    {
                        // ddlBookNo.Items.Clear();
                        if (objReportsListBe.items.Count > 0)
                            objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ReportsBe>(objReportsListBe.items), ddlBookNo, "BookNoWithDetails", "BookNo", selectText, selectFirstItem);
                        //else
                        //    objIdsignBal.FillDropDownList(new DataTable(), ddlBookNo, "BookNoWithDetails", "BookNo", selectText, selectFirstItem);
                    }
                    break;
                case "CheckBoxList":
                    CheckBoxList cblBookNos = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<ReportsBe>(objReportsListBe.items), cblBookNos, "BookNoWithDetails", "BookNo");
                    break;
            }
        }
        public void BindAllBookNumbersWithDetails(Control ctrl, string CycleId, bool IsRequiredSelect, bool selectFirstItem, string selectText)
        {
            ReportsListBe objReportsListBe = new ReportsListBe();
            ReportsBe objReportsBe = new ReportsBe();
            objReportsBe.CycleId = CycleId;
            XmlDocument xml = new XmlDocument();
            //xml = objReportsBal.Get(objReportsBe, ReturnType.Check);
            xml = objReportsBal.GetBooksForBookwiseMeterReading(objReportsBe);
            objReportsListBe = objIdsignBal.DeserializeFromXml<ReportsListBe>(xml);
            switch (ctrl.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddlBookNo = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                    {
                        //if (objReportsListBe.items.Count > 0)
                        //{
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ReportsBe>(objReportsListBe.items), ddlBookNo, "BookNoWithDetails", "BookNo", selectText, selectFirstItem);
                        //objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ReportsBe>(objReportsListBe.items), ddlBookNo, "BookNo", "BookNo", selectText, selectFirstItem);
                        //}
                        //else
                        //{
                        //    ddlBookNo.Items.Clear();
                        //}
                    }
                    break;
                case "CheckBoxList":
                    CheckBoxList cblBookNos = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<ReportsBe>(objReportsListBe.items), cblBookNos, "BookNoWithDetails", "BookNo");
                    break;
            }
        }
        public void BindAllBookNumbersWithDetails_MeterReadings(Control ctrl, string CycleId, bool IsRequiredSelect, bool selectFirstItem, string selectText)
        {
            ReportsListBe objReportsListBe = new ReportsListBe();
            ReportsBe objReportsBe = new ReportsBe();
            objReportsBe.CycleId = CycleId;
            XmlDocument xml = new XmlDocument();
            //xml = objReportsBal.Get(objReportsBe, ReturnType.Check);
            xml = objReportsBal.GetBooksForBookwiseMeterReading(objReportsBe);
            objReportsListBe = objIdsignBal.DeserializeFromXml<ReportsListBe>(xml);
            switch (ctrl.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddlBookNo = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                    {
                        if (objReportsListBe.items.Count > 0)
                        {
                            objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ReportsBe>(objReportsListBe.items), ddlBookNo, "BookNoWithDetails", "BookNo", selectText, selectFirstItem);
                            //objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ReportsBe>(objReportsListBe.items), ddlBookNo, "BookNo", "BookNo", selectText, selectFirstItem);
                        }
                        else
                        {
                            ddlBookNo.Items.Clear();
                        }
                    }
                    break;
                case "CheckBoxList":
                    CheckBoxList cblBookNos = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<ReportsBe>(objReportsListBe.items), cblBookNos, "BookNoWithDetails", "BookNo");
                    break;
            }
        }
        public void BindBooksForBookwiseCustomerExport(Control ctrl, string CycleId, bool IsRequiredSelect, bool selectFirstItem, string selectText)
        {
            ReportsListBe objReportsListBe = new ReportsListBe();
            ReportsBe objReportsBe = new ReportsBe();
            objReportsBe.CycleId = CycleId;
            XmlDocument xml = new XmlDocument();
            xml = objReportsBal.GetBooksForBookwiseCustomerExport(objReportsBe);
            objReportsListBe = objIdsignBal.DeserializeFromXml<ReportsListBe>(xml);
            switch (ctrl.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddlBookNo = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                    {
                        if (objReportsListBe.items.Count > 0)
                        {
                            objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ReportsBe>(objReportsListBe.items), ddlBookNo, "BookNoWithDetails", "BookNo", selectText, selectFirstItem);
                            //objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ReportsBe>(objReportsListBe.items), ddlBookNo, "BookNo", "BookNo", selectText, selectFirstItem);
                        }
                        else
                        {
                            ddlBookNo.Items.Clear();
                        }
                    }
                    break;
                case "CheckBoxList":
                    CheckBoxList cblBookNos = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<ReportsBe>(objReportsListBe.items), cblBookNos, "BookNoWithDetails", "BookNo");
                    break;
            }
        }
        public void BindNotDisabledBooks(Control ctrl, string CycleId, bool IsRequiredSelect, bool selectFirstItem, string selectText)
        {
            ReportsListBe objReportsListBe = new ReportsListBe();
            ReportsBe objReportsBe = new ReportsBe();
            objReportsBe.CycleId = CycleId;
            XmlDocument xml = new XmlDocument();
            xml = objReportsBal.GetNotDisabledBooks(objReportsBe);
            objReportsListBe = objIdsignBal.DeserializeFromXml<ReportsListBe>(xml);
            switch (ctrl.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddlBookNo = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                    {
                        if (objReportsListBe.items.Count > 0)
                        {
                            objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ReportsBe>(objReportsListBe.items), ddlBookNo, "BookNoWithDetails", "BookNo", selectText, selectFirstItem);
                            //objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ReportsBe>(objReportsListBe.items), ddlBookNo, "BookNo", "BookNo", selectText, selectFirstItem);
                        }
                        else
                        {
                            ddlBookNo.Items.Clear();
                        }
                    }
                    break;
                case "CheckBoxList":
                    CheckBoxList cblBookNos = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<ReportsBe>(objReportsListBe.items), cblBookNos, "BookNoWithDetails", "BookNo");
                    break;
            }
        }
        public void BindFeedersList(Control ctrl, string ddlselectText)
        {
            XmlDocument xmlResult = new XmlDocument();
            xmlResult = objMastersBAL.GetFeeders(ReturnType.Get);
            MastersListBE objMastersListBE = objIdsignBal.DeserializeFromXml<MastersListBE>(xmlResult);
            switch (ctrl.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddlFeeders = (DropDownList)ctrl;
                    objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), ddlFeeders, "FeederName", "FeederId", Resource.DDL_SELECT_FEEDER, true);
                    break;
            }
        }

        public void BindApprovalStatusList(Control ctrl, string ddlselectText)
        {
            DataSet ds = objApprovalBal.GetApprovalStatusList();
            switch (ctrl.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddl = (DropDownList)ctrl;
                    objIdsignBal.FillDropDownList(ds, ddl, "ApprovalStatus", "ApprovalStatusId", ddlselectText, true);
                    break;
            }
        }

        public void BindMoidfyApprovalStatusList(Control ctrl, string ddlselectText)
        {
            DataSet ds = objApprovalBal.GetApprovalStatusList();
            var results = from myRow in ds.Tables[0].AsEnumerable()
                          where myRow.Field<int>("ApprovalStatusId") == 3 || myRow.Field<int>("ApprovalStatusId") == 4
                          select myRow;

            DataTable dt = new DataTable();
            dt.Columns.Add("ApprovalStatus", typeof(string));
            dt.Columns.Add("ApprovalStatusId", typeof(int));
            foreach (DataRow row in results)
            {
                DataRow newRow = dt.NewRow();
                newRow["ApprovalStatus"] = row["ApprovalStatus"];
                newRow["ApprovalStatusId"] = row["ApprovalStatusId"];
                dt.Rows.Add(newRow);
            }
            switch (ctrl.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddl = (DropDownList)ctrl;
                    objIdsignBal.FillDropDownList(dt, ddl, "ApprovalStatus", "ApprovalStatusId", ddlselectText, true);
                    break;
            }
        }

        public void BindBillProcessTypes(Control ctrl, string ddlselectText)
        {
            XmlDocument xmlResult = new XmlDocument();
            xmlResult = objBillGenerationBal.GetDetails(ReturnType.Get);
            BillGenerationListBe objBillsListBE = objIdsignBal.DeserializeFromXml<BillGenerationListBe>(xmlResult);
            switch (ctrl.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddlFeeders = (DropDownList)ctrl;
                    objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlFeeders, "BillProcessType", "BillProcessTypeId", UMS_Resource.DROPDOWN_SELECT, true);
                    break;
                case "RadioButtonList":
                    RadioButtonList rbl = (RadioButtonList)ctrl;
                    objIdsignBal.FillRadioButtonList(objIdsignBal.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), rbl, "BillProcessType", "BillProcessTypeId");
                    break;
                case "CheckBoxList":
                    CheckBoxList cbl = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), cbl, "BillProcessType", "BillProcessTypeId");
                    break;
            }
        }

        public void ShowMessage(string MessageType, string Message, Panel pnl, Label lbl)//{Ramaraju -id064}
        {
            lbl.Text = Message;
            pnl.Visible = true;
            if (MessageType == UMS_Resource.MESSAGETYPE_ERROR)
            {
                pnl.CssClass = "message_nw error_nw";

            }
            else if (MessageType == UMS_Resource.MESSAGETYPE_SUCCESS)
            {
                pnl.CssClass = "message_nw success_nw";

            }
        }

        public string Convert_MMDDYY(string Date)
        {
            string[] Dob = Date.Split('/');
            Date = Dob[1] + '/' + Dob[0] + '/' + Dob[2];
            return Date;
        }

        public void ErrorMessage(string Message, string Key, string LineNumber)
        {
            try
            {
                //StackFrame frame = st.GetFrame(0);//Get the first stack frame
                //int LineNumber = frame.GetFileLineNumber();//Get the line number from the stack frame

                if (!File.Exists(HttpContext.Current.Server.MapPath(ConfigurationSettings.AppSettings[Key])))
                {
                    StreamWriter sw = File.CreateText(HttpContext.Current.Server.MapPath(ConfigurationSettings.AppSettings[Key]));
                    StoreDetails(sw, Message, LineNumber);
                    sw.Close();
                }
                else
                {
                    StreamWriter sw;
                    sw = File.AppendText(HttpContext.Current.Server.MapPath(ConfigurationSettings.AppSettings[Key]));
                    StoreDetails(sw, Message, LineNumber);
                    sw.Close();
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void StoreDetails(StreamWriter sw, string Message, string LineNumber)
        {
            DateTime ourTime = GetIndiaTime();
            try
            {
                string User = "Ananonymus User";
                if (HttpContext.Current.Session[UMS_Resource.SESSION_SOCIAL_LOGINID] != null)
                    User = HttpContext.Current.Session[UMS_Resource.SESSION_SOCIAL_LOGINID].ToString();
                if (HttpContext.Current.Session[UMS_Resource.SESSION_LOGINID] != null)
                    User = HttpContext.Current.Session[UMS_Resource.SESSION_LOGINID].ToString();
                sw.WriteLine("    " + ourTime + "    |     " + User + "      |       " + Message + "      | LineNumber :    " + LineNumber);
                sw.WriteLine("------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        internal void DownloadFile(string FileNameWithPath, string ContentType)
        {
            try
            {
                HttpResponse response = HttpContext.Current.Response;
                response.ClearContent();
                response.Clear();
                response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(FileNameWithPath).Replace(" ", "_") + ";");
                response.ContentType = ContentType;
                response.TransmitFile(FileNameWithPath);
                response.Flush();
                response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal DateTime GetIndiaTime()
        {
            TimeZoneInfo IND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            DateTime currentTime = DateTime.Now;
            return TimeZoneInfo.ConvertTime(currentTime, IND_ZONE);
        }

        internal void ClearInputs(ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                if (ctrl is TextBox)
                    ((TextBox)ctrl).Text = string.Empty;
                else if (ctrl is DropDownList)
                    ((DropDownList)ctrl).ClearSelection();
                else if (ctrl is CheckBoxList)
                    ((CheckBoxList)ctrl).ClearSelection();
                else if (ctrl is CheckBox)
                    ((CheckBox)ctrl).Checked = false;
                else if (ctrl is RadioButtonList)
                    ((RadioButtonList)ctrl).ClearSelection();
                else if (ctrl is RadioButton)
                    ((RadioButton)ctrl).Checked = false;

                ClearInputs(ctrl.Controls);
            }
        }
        //internal DataTable ExcelImport1(string ExcelModifiedFileName)
        //{
        //    var dt = new DataTable();

        //    if (Path.GetExtension(ExcelModifiedFileName) == ".xls" || Path.GetExtension(ExcelModifiedFileName) == ".xlsx")
        //    {
        //        //dtExcel = _objCommonMethods.ExcelImport(Server.MapPath(ConfigurationManager.AppSettings["Payments"].ToString()) + ExcelModifiedFileName);

        //        string FolderPath = ConfigurationManager.AppSettings["MeterReading"];
        //        string FilePath = FolderPath + ExcelModifiedFileName;
        //        //string cnstr = "Provider=Microsoft.Jet.Oledb.4.0;Data Source=" + FilePath + "; Extended Properties=Excel 4.0";///upto 2007
        //        String cnstr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FilePath + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";//2007 Onwards

        //        OleDbConnection oledbConn = new OleDbConnection(cnstr);
        //        String[] s = GetExcelSheetNames(FilePath);
        //        string strSQL = "SELECT * FROM [" + s[0] + "]";
        //        OleDbCommand cmd = new OleDbCommand(strSQL, oledbConn);
        //        DataSet ds = new DataSet();
        //        OleDbDataAdapter odapt = new OleDbDataAdapter(cmd);
        //        odapt.Fill(ds);
        //        dt = ds.Tables[0];
        //    }
        //    return dt;
        //}

        //internal DataTable ExcelImport(string FileNameWithPath)
        //{
        //    var datatable = new DataTable();
        //    var workbook = new XLWorkbook(FileNameWithPath);
        //    var xlWorksheet = workbook.Worksheet(1);
        //    //IXLRange xlRangeRow = xlWorksheet.AsRange();     

        //    var range = xlWorksheet.Range(xlWorksheet.FirstCellUsed(), xlWorksheet.LastCellUsed());
        //    // IXLCell rowCell = xlWorksheet.LastCellUsed(); 
        //    int col = range.ColumnCount();
        //    int row = range.RowCount();
        //    datatable.Clear();
        //    for (int i = 1; i <= col; i++)
        //    {
        //        IXLCell column = xlWorksheet.Cell(1, i);
        //        datatable.Columns.Add(column.Value.ToString());
        //    }
        //    // add rows data   
        //    int firstHeadRow = 0;
        //    foreach (var item in range.Rows())
        //    {
        //        if (firstHeadRow != 0)
        //        {
        //            var array = new object[col];
        //            for (int y = 1; y <= col; y++)
        //            {
        //                array[y - 1] = item.Cell(y).Value;
        //            }
        //            datatable.Rows.Add(array);
        //        }
        //        firstHeadRow++;
        //    }
        //    return datatable;
        //}

        //internal void ExportToExcel(DataTable data, string sheetName)
        //{
        //    XLWorkbook wb = new XLWorkbook();
        //    var ws = wb.Worksheets.Add(sheetName);
        //    ws.Cell(1, 1).InsertTable(data);
        //    HttpContext.Current.Response.Clear();
        //    HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //    HttpContext.Current.Response.AddHeader("content-disposition", String.Format(@"attachment;filename={0}.xlsx", sheetName.Replace(" ", "_")));

        //    using (MemoryStream memoryStream = new MemoryStream())
        //    {
        //        wb.SaveAs(memoryStream);
        //        memoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
        //        memoryStream.Close();
        //    }

        //    HttpContext.Current.Response.End();
        //}

        //eg 01 Jan 2014
        internal string DateFormate(string date)
        {
            try
            {
                date = Convert.ToDateTime(date).ToString("dd MMM yyyy");
            }
            catch (Exception ex)
            {
            }
            return date;
        }

        internal string GetCurrencyFormat(decimal value, int decimalPlaces, int Value)
        {
            value = Math.Round(value, decimalPlaces);
            bool isNegative = false;
            if (value < 0)
            {
                isNegative = true;
                value = value * -1;
            }
            string result = string.Empty;
            NumberFormatInfo nfo = new NumberFormatInfo();
            nfo.CurrencyGroupSeparator = ",";
            if
            (Value == Constants.INR_Format)
            {
                nfo.CurrencyGroupSizes = new int[] { 3, 2 };
            }
            else if (Value == Constants.MILLION_Format)
            {
                nfo.CurrencyGroupSizes = new int[] { 3, 3 };
            }
            nfo.CurrencySymbol = "";
            if (value.ToString().Contains('.'))
            {
                string[] list = value.ToString().Split('.');
                //if (Convert.ToDecimal(list[1]) > 0)
                //{
                result = value.ToString("c" + decimalPlaces.ToString(), nfo);// TO get decimals if the value is having '.'
                //}
                //else
                //{
                //    result = value.ToString("c0", nfo);
                //}
            }
            else
            {
                //To show the decimals if there is decimal places greater than 0
                if (decimalPlaces > 0)
                {
                    result = value.ToString("c" + decimalPlaces.ToString(), nfo);
                }
                else
                {
                    result = value.ToString("c0", nfo);
                }
                //result = value.ToString("c0", nfo);
            }
            if (isNegative)
            {
                result = "-" + result;
            }
            return result;
        }

        internal string GetPagePath(Uri uri)
        {
            string path = string.Empty;
            path = uri.AbsolutePath.TrimStart('/');
            //if (uri.Segments.Length > 0)
            //{
            //    int maxlength = uri.Segments.Length;
            //    path = uri.Segments[maxlength - 2] + uri.Segments[maxlength - 1];
            //}

            return path;
        }

        internal void BindPagingDropDownList(int TotalRecords, int currentPageSize, DropDownList ddlPageSize)
        {
            ddlPageSize.Items.Clear();
            if (TotalRecords < Constants.PageSizeStarts)
            {
                ListItem item = new ListItem(Constants.PageSizeStarts.ToString(), Constants.PageSizeStarts.ToString());
                ddlPageSize.Items.Add(item);
            }
            else
            {
                int previousSize = Constants.PageSizeStarts;
                for (int i = Constants.PageSizeStarts; i <= TotalRecords; i = i + Constants.PageSizeIncrement)
                {
                    ListItem item = new ListItem(i.ToString(), i.ToString());
                    ddlPageSize.Items.Add(item);
                    previousSize = i;
                }
                if (previousSize < TotalRecords)
                {
                    ListItem item = new ListItem((previousSize + Constants.PageSizeIncrement).ToString(), (previousSize + Constants.PageSizeIncrement).ToString());
                    ddlPageSize.Items.Add(item);
                }
            }

            ddlPageSize.SelectedIndex = ddlPageSize.Items.IndexOf(ddlPageSize.Items.FindByText(currentPageSize.ToString()));
        }

        internal void BindPagingDropDownList(int TotalRecords, int currentPageSize, DropDownList ddlPageSize, int PageSize, int PageSizeIncrement)
        {
            ddlPageSize.Items.Clear();
            if (TotalRecords < PageSize)
            {
                ListItem item = new ListItem(PageSize.ToString(), PageSize.ToString());
                ddlPageSize.Items.Add(item);
            }
            else
            {
                int previousSize = PageSize;
                for (int i = PageSize; i <= TotalRecords; i = i + PageSizeIncrement)
                {
                    ListItem item = new ListItem(i.ToString(), i.ToString());
                    ddlPageSize.Items.Add(item);
                    previousSize = i;
                }
                if (previousSize < TotalRecords)
                {
                    ListItem item = new ListItem((previousSize + PageSizeIncrement).ToString(), (previousSize + PageSizeIncrement).ToString());
                    ddlPageSize.Items.Add(item);
                }
            }

            ddlPageSize.SelectedIndex = ddlPageSize.Items.IndexOf(ddlPageSize.Items.FindByText(currentPageSize.ToString()));
        }

        //public bool IsCustomerInBillProcess(MastersBE objMastersBE)
        //{
        //    bool status = false;
        //    XmlDocument xml = new System.Xml.XmlDocument();
        //    _ObjMastersBE.AccountNo = objMastersBE.AccountNo;
        //    _ObjMastersBE.CycleId = objMastersBE.CycleId;
        //    _ObjMastersBE.BookNo = objMastersBE.BookNo;
        //    _ObjMastersBE.FeederId = objMastersBE.FeederId;
        //    _ObjMastersBE.Flag = objMastersBE.Flag;
        //    _ObjMastersBE.BillYear=objMastersBE.BillYear;
        //    _ObjMastersBE.BillMonth = objMastersBE.BillMonth;
        //    xml = _ObjMastersBAL.IsCustomerInBillProcessBAL(_ObjMastersBE, Statement.Check);
        //    _ObjMastersBE = objIdsignBal.DeserializeFromXml<MastersBE>(xml);
        //    status = _ObjMastersBE.Status;
        //    return status;
        //}
        internal MastersBE CustomerBillProcessDetails(MastersBE objMastersBE)
        {
            XmlDocument xml = new System.Xml.XmlDocument();
            _ObjMastersBE.AccountNo = objMastersBE.AccountNo;
            _ObjMastersBE.CycleId = objMastersBE.CycleId;
            _ObjMastersBE.BookNo = objMastersBE.BookNo;
            _ObjMastersBE.FeederId = objMastersBE.FeederId;
            _ObjMastersBE.Flag = objMastersBE.Flag;
            _ObjMastersBE.BillYear = objMastersBE.BillYear;
            _ObjMastersBE.BillMonth = objMastersBE.BillMonth;
            xml = _ObjMastersBAL.IsCustomerInBillProcessBAL(_ObjMastersBE, Statement.Check);
            _ObjMastersBE = objIdsignBal.DeserializeFromXml<MastersBE>(xml);
            return _ObjMastersBE;
        }


        public void BindFeeders_BySuId(Control ctrl, string su_Id, string selectText, bool isSelectFirstItem)
        {
            ConsumerBe _objConsumerBe = new ConsumerBe();
            XmlDocument xmlResult = new XmlDocument();
            _objConsumerBe.SU_ID = su_Id;
            xmlResult = objConsumerBal.GetCustomerBillDeails(_objConsumerBe, ReturnType.Get);
            ConsumerListBe objConsumerListBe = _ObjiDsignBAL.DeserializeFromXml<ConsumerListBe>(xmlResult);
            switch (ctrl.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddlInjectionSubStations = (DropDownList)ctrl;
                    _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlInjectionSubStations, "FeederName", "FeederId", selectText, isSelectFirstItem);
                    break;
            }
        }

        public void BindCycles_BySuId(Control ctrl, string su_Id, string selectText, bool isSelectFirstItem)
        {
            ConsumerBe _objConsumerBe = new ConsumerBe();
            XmlDocument xmlResult = new XmlDocument();
            _objConsumerBe.SU_ID = su_Id;
            xmlResult = objConsumerBal.GetCustomerBillDeails(_objConsumerBe, ReturnType.Multiple);
            ConsumerListBe objConsumerListBe = _ObjiDsignBAL.DeserializeFromXml<ConsumerListBe>(xmlResult);
            switch (ctrl.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddlInjectionSubStations = (DropDownList)ctrl;
                    _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlInjectionSubStations, "CycleName", "CycleId", selectText, isSelectFirstItem);
                    break;
            }
        }

        public void BindUserServiceUnits_BuIds(Control ctrl, string bu_Ids, string selectText, bool isSelectFirstItem)
        {
            ConsumerBe _objConsumerBe = new ConsumerBe();
            XmlDocument xmlResult = new XmlDocument();
            _objConsumerBe.BU_ID = bu_Ids;
            _objConsumerBe.UserId = HttpContext.Current.Session[UMS_Resource.SESSION_LOGINID].ToString();
            xmlResult = objConsumerBal.GetCustomerBillDeails(_objConsumerBe, ReturnType.Fetch);
            ConsumerListBe objConsumerListBe = _ObjiDsignBAL.DeserializeFromXml<ConsumerListBe>(xmlResult);
            switch (ctrl.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddlInjectionSubStations = (DropDownList)ctrl;
                    _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlInjectionSubStations, "ServiceUnitName", "SU_ID", selectText, isSelectFirstItem);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblSU = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(_ObjiDsignBAL.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), cblSU, "ServiceUnitName", "SU_ID");
                    break;
            }
        }

        public void BindUserServiceCenters_SuIds(Control ctrl, string su_Ids, string selectText, bool isSelectFirstItem)
        {
            ConsumerBe _objConsumerBe = new ConsumerBe();
            XmlDocument xmlResult = new XmlDocument();
            _objConsumerBe.SU_ID = su_Ids;
            _objConsumerBe.UserId = HttpContext.Current.Session[UMS_Resource.SESSION_LOGINID].ToString();
            xmlResult = objConsumerBal.GetCustomerBillDeails(_objConsumerBe, ReturnType.Bulk);
            ConsumerListBe objConsumerListBe = _ObjiDsignBAL.DeserializeFromXml<ConsumerListBe>(xmlResult);
            switch (ctrl.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddlServiceCenter = (DropDownList)ctrl;
                    _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), ddlServiceCenter, "ServiceCEnterName", "ServiceCenterId", selectText, isSelectFirstItem);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblSC = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(_ObjiDsignBAL.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), cblSC, "ServiceCenterName", "ServiceCenterId");
                    break;
            }
        }

        public void BindMarketers(Control ctrl, string BU_ID, string selectText, bool isSelectFirstItem)
        {
            //
            MarketersBe _objMarketersBe = new MarketersBe();
            XmlDocument xmlResult = new XmlDocument();
            _objMarketersBe.BU_ID = BU_ID;
            xmlResult = _objMarketersBal.GetMarketer(_objMarketersBe, ReturnType.Get);
            MarketersListBe _objMarketersListBe = _ObjiDsignBAL.DeserializeFromXml<MarketersListBe>(xmlResult);
            switch (ctrl.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddlMarketers = (DropDownList)ctrl;
                    _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<MarketersBe>(_objMarketersListBe.Items), ddlMarketers, "Marketer", "MarketerId", selectText, isSelectFirstItem);
                    break;
                case "CheckBoxList":
                    CheckBoxList cblMarketers = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(_ObjiDsignBAL.ConvertListToDataSet<MarketersBe>(_objMarketersListBe.Items), cblMarketers, "MarketerName", "MarketerId");
                    break;
            }
        }

        public string GetAllNines(int dails)
        {
            string Str = "";
            for (int i = 0; i < dails; i++)
            {
                Str += "9";
            }
            return Str;
        }

        public decimal AboveHugeAmount(decimal averageValue)
        {
            return averageValue + (averageValue * Constants.Meter_Reading_HugePercentage / 100);
        }

        public decimal BelowHugeAmount(decimal averageValue)
        {
            return averageValue - (averageValue * Constants.Meter_Reading_HugePercentage / 100);
        }

        public bool IsHugeAmount(decimal newAvgUsage, decimal prevAvgUsage)
        {
            if (newAvgUsage > AboveHugeAmount(prevAvgUsage) || newAvgUsage < BelowHugeAmount(prevAvgUsage))
                return true;
            else
                return false;
        }

        public bool IsApproval(int functionId)
        {
            ApprovalBe _objApprovalBe = new ApprovalBe();
            _objApprovalBe.FunctionId = functionId;
            XmlDocument xmlResult = _objApprovalBal.IsApproval(_objApprovalBe);
            _objApprovalBe = _ObjiDsignBAL.DeserializeFromXml<ApprovalBe>(xmlResult);
            return _objApprovalBe.IsActive;
        }

        public bool IsApproval(string Feature)///dummy one to avoiding errors from old code
        {
            return false;
        }


        public string CollectSelectedItemsValues(CheckBoxList cbl, string seperator)
        {
            return string.Join(seperator, (from item in cbl.Items.Cast<ListItem>()
                                           where item.Selected
                                           select item.Value));
        }

        public string CollectSelectedItemsText(CheckBoxList cbl, string seperator)
        {
            return string.Join(seperator, (from item in cbl.Items.Cast<ListItem>()
                                           where item.Selected
                                           select item.Text));
        }

        public string CollectSelectedItems(CheckBoxList cbl, string seperator)
        {
            return string.Join(seperator, (from item in cbl.Items.Cast<ListItem>()
                                           where item.Selected
                                           select item.Text));
        }

        public string CollectAllValues(DropDownList ddl, string seperator)
        {
            return string.Join(seperator, (from item in ddl.Items.Cast<ListItem>()
                                           select item.Value));
        }

        public void SaveExcel(DataTable dataTable)
        {
            try
            {
                DateTime ourTime = GetIndiaTime();
                GridView gv = new GridView();

                var result = (from x in dataTable.AsEnumerable()
                              select new
                              {
                                  DistCode = x.Field<string>("DistCode"),
                                  DistName = x.Field<string>("DistName"),
                                  TrnId = x.Field<string>("TrnId"),
                                  Product = x.Field<string>("Product"),
                                  Acno = x.Field<string>("Acno"),
                                  TrnDate = x.Field<DateTime>("TrnDate").ToString(Resource.EBILL_DATE_FRMT3),
                                  TrnTime = x.Field<string>("TrnTime"),
                                  TrnAmount = x.Field<decimal>("TrnAmount"),
                                  SourceBank = x.Field<string>("SourceBank"),
                                  PayStatus = x.Field<string>("PayStatus")
                              });
                gv.DataSource = result;
                gv.DataBind();
                // string sheetName = fileName.Replace(" ", "") + "_" + ourTime.ToString(Resource.EBILL_DATE_FRMT2) + Resource.Formate_Excel;


                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        //  Create a form to contain the grid
                        Table table = new Table();
                        // include grid lines
                        table.GridLines = gv.GridLines;
                        //  add the header row to the table

                        if (gv.HeaderRow != null)
                        {
                            PrepareControlForExport(gv.HeaderRow);
                            //   GridViewExportUtil.PrepareControlForExport(gv.HeaderRow);
                            table.Rows.Add(gv.HeaderRow);
                        }

                        //add each of the data rows to the table
                        foreach (GridViewRow row in gv.Rows)
                        {
                            PrepareControlForExport(row);
                            //      GridViewExportUtil.PrepareControlForExport(row);
                            table.Rows.Add(row);
                        }

                        //  add the footer row to the table
                        if (gv.FooterRow != null)
                        {
                            PrepareControlForExport(gv.FooterRow);
                            // GridViewExportUtil.PrepareControlForExport(gv.FooterRow);
                            table.Rows.Add(gv.FooterRow);
                        }

                        //  render the table into the htmlwriter
                        table.RenderControl(htw);



                        FileStream fs = default(FileStream);
                        fs = new FileStream(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["EBillPayFiles"]) + Resource.EBILL_EXCELL_NAME + "_" + DateTime.Now.ToString(Resource.EBILL_DATE_FRMT1) + Resource.Formate_Excel, FileMode.Append);
                        StreamWriter sw1 = new StreamWriter(fs);
                        sw1.Write(sw.ToString());
                        sw1.Close();
                        fs.Close();
                    }
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }


        internal DataTable SaveExcel(List<MastersBE> Mas, String FileName)//Neeraj Kanojiya --ID077
        {
            DataTable dt = new DataTable();
            string CompleteFileName;
            try
            {

                dt.Columns.Add("DistCode");
                dt.Columns.Add("DistName");
                dt.Columns.Add("TrnId");
                dt.Columns.Add("Product");
                dt.Columns.Add("Acno");
                dt.Columns.Add("TrnDate");
                dt.Columns.Add("TrnTime");
                dt.Columns.Add("TrnAmount");
                dt.Columns.Add("SourceBank");
                dt.Columns.Add("PayStatus");


                DateTime ourTime = GetIndiaTime();
                GridView gv = new GridView();
                List<MastersBE> MASTER = new List<MastersBE>();
                //foreach (MastersBE m in Mas)
                //{
                MASTER = Mas;
                //}
                var result = (from x in MASTER.AsEnumerable()
                              select new
                              {
                                  DistCode = x.DistCode,
                                  DistName = x.DistName,
                                  TrnId = x.TrnId,
                                  Product = x.Product,
                                  Acno = x.Acno,
                                  TrnDate = x.TrnDate.ToString(Resource.EBILL_DATE_FRMT3),
                                  TrnTime = x.TrnTime,
                                  TrnAmount = x.TrnAmount,
                                  SourceBank = x.SourceBank,
                                  PayStatus = x.PayStatus
                              });
                foreach (var v in result)
                {
                    DataRow dr = dt.NewRow();
                    dr["DistCode"] = v.DistCode;
                    dr["DistName"] = v.DistName;
                    dr["TrnId"] = v.TrnId;
                    dr["Product"] = v.Product;
                    dr["Acno"] = v.Acno;
                    dr["TrnDate"] = v.TrnDate;
                    dr["TrnTime"] = v.TrnTime;
                    dr["TrnAmount"] = v.TrnAmount;
                    dr["SourceBank"] = v.SourceBank;
                    dr["PayStatus"] = v.PayStatus;
                    dt.Rows.Add(dr);
                }
                gv.DataSource = result;
                gv.DataBind();
                // string sheetName = fileName.Replace(" ", "") + "_" + ourTime.ToString(Resource.EBILL_DATE_FRMT2) + Resource.Formate_Excel;


                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        //  Create a form to contain the grid
                        Table table = new Table();
                        // include grid lines
                        table.GridLines = gv.GridLines;
                        //  add the header row to the table

                        if (gv.HeaderRow != null)
                        {
                            PrepareControlForExport(gv.HeaderRow);
                            //   GridViewExportUtil.PrepareControlForExport(gv.HeaderRow);
                            table.Rows.Add(gv.HeaderRow);
                        }

                        //add each of the data rows to the table
                        foreach (GridViewRow row in gv.Rows)
                        {
                            PrepareControlForExport(row);
                            //      GridViewExportUtil.PrepareControlForExport(row);
                            table.Rows.Add(row);
                        }

                        //  add the footer row to the table
                        if (gv.FooterRow != null)
                        {
                            PrepareControlForExport(gv.FooterRow);
                            // GridViewExportUtil.PrepareControlForExport(gv.FooterRow);
                            table.Rows.Add(gv.FooterRow);
                        }

                        //  render the table into the htmlwriter
                        table.RenderControl(htw);



                        FileStream fs = default(FileStream);
                        CompleteFileName = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["EBillPayFiles"]) + FileName + Resource.Formate_Excel;
                        fs = new FileStream(CompleteFileName, FileMode.Append);
                        StreamWriter sw1 = new StreamWriter(fs);
                        sw1.Write(sw.ToString());
                        sw1.Close();
                        fs.Close();

                    }
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return dt;
        }

        internal DataTable GenDTForConsolidateRpt(List<MastersBE> Mas)//Neeraj Kanojiya --ID077
        {
            DataTable dt = new DataTable();
            try
            {

                dt.Columns.Add("DistCode");
                dt.Columns.Add("DistName");
                dt.Columns.Add("TrnId");
                dt.Columns.Add("Product");
                dt.Columns.Add("Acno");
                dt.Columns.Add("TrnDate");
                dt.Columns.Add("TrnTime");
                dt.Columns.Add("TrnAmount");
                dt.Columns.Add("SourceBank");
                dt.Columns.Add("PayStatus");


                DateTime ourTime = GetIndiaTime();
                GridView gv = new GridView();
                List<MastersBE> MASTER = new List<MastersBE>();
                //foreach (MastersBE m in Mas)
                //{
                MASTER = Mas;
                //}
                var result = (from x in MASTER.AsEnumerable()
                              select new
                              {
                                  DistCode = x.DistCode,
                                  DistName = x.DistName,
                                  TrnId = x.TrnId,
                                  Product = x.Product,
                                  Acno = x.Acno,
                                  TrnDate = x.TrnDate.ToString(Resource.EBILL_DATE_FRMT3),
                                  TrnTime = x.TrnTime,
                                  TrnAmount = x.TrnAmount,
                                  SourceBank = x.SourceBank,
                                  PayStatus = x.PayStatus
                              });
                foreach (var v in result)
                {
                    DataRow dr = dt.NewRow();
                    dr["DistCode"] = v.DistCode;
                    dr["DistName"] = v.DistName;
                    dr["TrnId"] = v.TrnId;
                    dr["Product"] = v.Product;
                    dr["Acno"] = v.Acno;
                    dr["TrnDate"] = v.TrnDate;
                    dr["TrnTime"] = v.TrnTime;
                    dr["TrnAmount"] = v.TrnAmount;
                    dr["SourceBank"] = v.SourceBank;
                    dr["PayStatus"] = v.PayStatus;
                    dt.Rows.Add(dr);
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return dt;
        }

        private void PrepareControlForExport(Control control)
        {
            try
            {
                for (int i = 0; i < control.Controls.Count; i++)
                {
                    Control current = control.Controls[i];
                    if (current is LinkButton)
                    {
                        control.Controls.Remove(current);
                        control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text));
                    }
                    else if (current is ImageButton)
                    {
                        control.Controls.Remove(current);
                        control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
                    }
                    else if (current is HyperLink)
                    {
                        control.Controls.Remove(current);
                        control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text));
                    }
                    else if (current is DropDownList)
                    {
                        control.Controls.Remove(current);
                        control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
                    }
                    else if (current is CheckBox)
                    {
                        control.Controls.Remove(current);
                        control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                    }

                    if (current.HasControls())
                    {
                        PrepareControlForExport(current);
                        //GridViewExportUtil.PrepareControlForExport(current);
                    }
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void SendMail_BillGeneration(string fileName, string cycleName, string feederId, string billMonth, int billYear, string toEmailId)
        {
            try
            {
                string item = string.IsNullOrEmpty(cycleName) ? "Feeder : " + feederId : "Cycle : " + cycleName;

                string smtpUserName = ConfigurationManager.AppSettings["SMTPUsername"].ToString();
                string smtpPassword = ConfigurationManager.AppSettings["SMTPPassword"].ToString();
                string smtpServer = ConfigurationManager.AppSettings["SMTPServer"].ToString();
                string smtpPort = ConfigurationManager.AppSettings["SMTPPort_TLS"].ToString();

                NetworkCredential cred = new NetworkCredential(smtpUserName, smtpPassword);
                MailMessage msg = new MailMessage();
                msg.To.Add(toEmailId);
                msg.Subject = "BEDC Bill Generations for " + cycleName;
                msg.Attachments.Add(new Attachment(fileName));
                msg.Body = "You Have Successfully Generated Bill for " + cycleName + " under " + billMonth + "-" + billYear.ToString() + ", Please find the attached Bills";
                msg.From = new MailAddress(smtpUserName, "BEDC Billing Team"); // Your Email Id
                SmtpClient client = new SmtpClient(smtpServer, Convert.ToInt32(smtpPort));
                //SmtpClient client1 = new SmtpClient("smtp.mail.yahoo.com", 465);
                client.Credentials = cred;
                client.EnableSsl = true;
                client.Send(msg);
            }
            catch (Exception ex)
            {
            }
        }

        public void SendMail(string fileName, string body, string billMonth, int billYear, string toEmailId)
        {
            NetworkCredential cred = new NetworkCredential("bedc.billing@gmail.com", "bedcbedc");
            MailMessage msg = new MailMessage();
            msg.To.Add("sureshkumar.dasi@idsigntechnologies.com,ies.tester1@gmail.com,ies.tester2@gmail.com");
            msg.Subject = "BEDC Bill Generations";
            msg.Attachments.Add(new Attachment(fileName));
            msg.Body = body;
            msg.From = new MailAddress("bedctesting@gmail.com", "BEDC Test Generated Bill"); // Your Email Id
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            //SmtpClient client1 = new SmtpClient("smtp.mail.yahoo.com", 465);
            client.Credentials = cred;
            client.EnableSsl = true;
            client.Send(msg);
        }

        public string GenerateTxtEBillFile(string str, string fileNameTxt)
        {
            string CompleteFileNameTxt = string.Empty;
            try
            {
                CompleteFileNameTxt = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["EBillPayFiles"]) + fileNameTxt + "_" + DateTime.Now.ToString(Resource.EBILL_DATE_FRMT1) + Resource.Formate_Text;
                FileStream fs = default(FileStream);
                fs = new FileStream(CompleteFileNameTxt, FileMode.Append);
                StreamWriter sw = new StreamWriter(fs);
                sw.Write(str);
                sw.Close();
                fs.Close();
            }
            catch (Exception ex)
            {
            }
            return CompleteFileNameTxt;
        }

        public void SendMailToBUEBillFiles(string FileNametxt, string FileNameXls)//Neeraj Kanojiya --ID077
        {
            //string txtFile = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["EBillPayFiles"]) + FileNametxt + Resource.Formate_Text;
            string fileNameExcel = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["EBillPayFiles"]) + FileNameXls + Resource.Formate_Excel;
            NetworkCredential cred = new NetworkCredential("bedctesting@gmail.com", "bedc@Testing");
            MailMessage msg = new MailMessage();
            msg.To.Add("neeraj.k@idsigntechnologies.com");
            msg.Subject = "BEDC EBill Payments Details.";
            msg.Attachments.Add(new Attachment(FileNametxt));
            msg.Attachments.Add(new Attachment(fileNameExcel));
            msg.Body = "This is auto generated EBill Payment Details, Please find the attached E-Bills details.";
            msg.From = new MailAddress("bedctesting@gmail.com", "BEDC EBill Testing"); // Your Email Id
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            //SmtpClient client1 = new SmtpClient("smtp.mail.yahoo.com", 465);
            client.Credentials = cred;
            client.EnableSsl = true;
            //client.Send(msg);
        }

        public void CreateExeclWithMultiWorksheets(DataSet WorkBook, string FileName)//Neeraj Kanojiya --ID077
        {

            Microsoft.Office.Interop.Excel.Application ExcelApp = new Microsoft.Office.Interop.Excel.Application();

            Microsoft.Office.Interop.Excel.Workbook ExcelWorkBook = null;

            Microsoft.Office.Interop.Excel.Worksheet ExcelWorkSheet = null;

            ExcelApp.Visible = true;
            string FolderPath = ConfigurationManager.AppSettings["EBillPayFiles"];//Custom
            ExcelWorkBook = ExcelApp.Workbooks.Add(Microsoft.Office.Interop.Excel.XlWBATemplate.xlWBATWorksheet);
            try
            {

                for (int i = 1; i < WorkBook.Tables.Count; i++)
                    ExcelWorkBook.Worksheets.Add(); //Adding New sheet in Excel Workbook


                for (int i = 0; i < WorkBook.Tables.Count; i++)
                {
                    int r = 1; // Initialize Excel Row Start Position  = 1
                    ExcelWorkSheet = ExcelWorkBook.Worksheets[i + 1];
                    //Writing Columns Name in Excel Sheet
                    for (int col = 1; col < WorkBook.Tables[i].Columns.Count; col++)
                        ExcelWorkSheet.Cells[r, col] = WorkBook.Tables[i].Columns[col - 1].ColumnName;

                    r++;



                    //Writing Rows into Excel Sheet

                    for (int row = 0; row < WorkBook.Tables[i].Rows.Count; row++) //r stands for ExcelRow and col for ExcelColumn
                    {
                        // Excel row and column start positions for writing Row=1 and Col=1

                        for (int col = 1; col < WorkBook.Tables[i].Columns.Count; col++)
                        {
                            ExcelWorkSheet.Cells[r, col] = WorkBook.Tables[i].Rows[row][col - 1].ToString();
                            if (row == 0 && col == 1)
                                ExcelWorkSheet.Name = WorkBook.Tables[i].Rows[0][1].ToString();//Sheet Name according to present data.//Custom
                        }

                        r++;

                    }

                    //ExcelWorkSheet.Name = SheetNames[i];//Renaming the ExcelSheets
                }

                ExcelWorkBook.SaveAs(FileName);

                ExcelWorkBook.Close();

                ExcelApp.Quit();

                Marshal.ReleaseComObject(ExcelWorkSheet);

                Marshal.ReleaseComObject(ExcelWorkBook);

                Marshal.ReleaseComObject(ExcelApp);

            }

            catch (Exception exHandle)
            {
                Console.WriteLine("Exception: " + exHandle.Message);
                Console.ReadLine();
            }

            finally
            {
                foreach (Process process in Process.GetProcessesByName("Excel"))
                    process.Kill();
            }



        }

        public void ReleaseComObject(object reference)//Neeraj Kanojiya --ID077
        {
            try
            {
                while (System.Runtime.InteropServices.Marshal.ReleaseComObject(reference) <= 0)
                {
                }
            }
            catch
            {
            }
        }

        public bool AuthenticateKey(string key)
        {
            _ObjMastersBE.Key = key;
            _ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(_ObjMastersBAL.AuthenticateKey(_ObjMastersBE));
            return _ObjMastersBE.Status;

        }

        public string AppendZeros(string Reading, int Dails)
        {
            string Str = "";
            if (Reading.Length < Dails)
            {
                for (int i = 0; i < (Dails - Reading.Length); i++)
                {
                    Str += "0";
                }
            }
            return Str + Reading;
        }

        public string stringFormate(int num)
        {
            string formate = "0";
            for (int i = 1; i < num; i++)
            {
                formate += "0";
            }
            return formate;
        }

        public Hashtable getApplicationSettings(string SettingName)
        {

            XmlDocument doc = new XmlDocument();
            doc.Load(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ApplicationSettings"]));
            Hashtable ht = new Hashtable();
            XmlNodeList xnl2 = doc.SelectNodes("/ApplicationSettings" + SettingName);
            foreach (XmlNode node2 in xnl2)
            {
                ht.Add(node2["Key"].InnerText, node2["Value"].InnerText);
            }
            return ht;
        }

        public string GetCurrencyFormat(decimal value)
        {
            string result = string.Empty;
            result = string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", value);
            //bool is Negative = false;
            //if (value < 0)
            //{
            //    isNegative = true;
            //    value = value * -1;
            //}

            //NumberFormatInfo nfo = new NumberFormatInfo();
            //nfo.CurrencyGroupSeparator = ",";
            //nfo.CurrencyGroupSizes = new int[] { 3, 2 };
            //nfo.CurrencySymbol = "";
            //if (value.ToString().Contains('.'))
            //{
            //    string[] list = value.ToString().Split('.');
            //    if (Convert.ToDecimal(list[1]) > 0)
            //    {
            //        result = value.ToString("c2", nfo);
            //    }
            //    else
            //    {
            //        result = value.ToString("c0", nfo);
            //    }
            //}
            //else
            //{
            //    result = value.ToString("c0", nfo);
            //}
            //if (isNegative)
            //{
            //    result = "-" + result;
            //}
            return result;
        }

        public bool IsValidYear(int year)
        {
            if (year >= DateTime.Now.Year - Constants.LastValidYear)
                return true;
            else
                return false;

        }

        public bool IsNumeric(string s)
        {
            int output;
            return int.TryParse(s, out output);
        }

        public int DisableUser(string UserId)
        {
            XmlDocument xml = new XmlDocument();
            _ObjUserManagementBE.UserId = UserId;
            xml = objUserManagementBAL.DisableUserBAL(_ObjUserManagementBE);
            _ObjUserManagementBE = _ObjiDsignBAL.DeserializeFromXml<UserManagementBE>(xml);
            return _ObjUserManagementBE.RowNumber;
        }

        public string ReplaceText(string oldString, string newString)
        {
            return string.IsNullOrEmpty(oldString) ? newString : oldString;
        }
        public string ReplaceText(int oldNum, string newString)
        {
            if (oldNum > 0)
            {
                newString = oldNum.ToString();
            }
            return newString;
        }

        public void BindActiveStatusDetails(Control ctrl, string selectText, bool isSelectFirstItem)
        {
            ChangeBookNoBe _objBookBe = new ChangeBookNoBe();
            XmlDocument xmlResult = new XmlDocument();
            xmlResult = _objChangeBookNoBal.Get(_objBookBe, ReturnType.List);
            ChangeBookNoListBe objChangeBookNoListBe = _ObjiDsignBAL.DeserializeFromXml<ChangeBookNoListBe>(xmlResult);
            switch (ctrl.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddlStatus = (DropDownList)ctrl;
                    _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<ChangeBookNoBe>(objChangeBookNoListBe.Items), ddlStatus, "ActiveStatus", "ActiveStatusId", selectText, isSelectFirstItem);
                    break;
            }
        }

        public void BindCustomerStatusDetails(Control ctrl, string selectText, bool isSelectFirstItem)
        {
            ChangeBookNoBe _objBookBe = new ChangeBookNoBe();
            XmlDocument xmlResult = new XmlDocument();
            xmlResult = _objChangeBookNoBal.Get(_objBookBe, ReturnType.Bulk);
            ChangeBookNoListBe objChangeBookNoListBe = _ObjiDsignBAL.DeserializeFromXml<ChangeBookNoListBe>(xmlResult);
            switch (ctrl.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddlStatus = (DropDownList)ctrl;
                    _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<ChangeBookNoBe>(objChangeBookNoListBe.Items), ddlStatus, "ActiveStatus", "ActiveStatusId", selectText, isSelectFirstItem);
                    break;
            }
        }

        //For CustomerStatusChange
        public void BindCustomerStatusDetails(Control ctrl, int CustomerStatusId, string selectText, bool isSelectFirstItem)
        {
            ChangeBookNoBe _objBookBe = new ChangeBookNoBe();
            XmlDocument xmlResult = new XmlDocument();
            xmlResult = _objChangeBookNoBal.Get(_objBookBe, ReturnType.Bulk);
            ChangeBookNoListBe objChangeBookNoListBe = _ObjiDsignBAL.DeserializeFromXml<ChangeBookNoListBe>(xmlResult);
            objChangeBookNoListBe.Items.Remove(objChangeBookNoListBe.Items.Find(x => x.ActiveStatusId == CustomerStatusId));
            switch (ctrl.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddlStatus = (DropDownList)ctrl;
                    _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<ChangeBookNoBe>(objChangeBookNoListBe.Items), ddlStatus, "ActiveStatus", "ActiveStatusId", selectText, isSelectFirstItem);
                    break;
            }
        }

        public bool CheckApprovalAuthorization()
        {
            ApprovalBe objApprovalBe = new ApprovalBe();
            objApprovalBe.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerTariff;
            objApprovalBe.UserId = HttpContext.Current.Session[UMS_Resource.SESSION_LOGINID].ToString();
            objApprovalBe.RoleId = Convert.ToInt32(HttpContext.Current.Session[UMS_Resource.SESSION_ROLEID]);
            objApprovalBe = objIdsignBal.DeserializeFromXml<ApprovalBe>(objApprovalBal.IsAuthorizeApproval(objApprovalBe));
            return objApprovalBe.IsActive;
        }

        /// <summary>
        /// Method to bind records from database to the dropdownlist
        /// </summary>
        /// <param name="excelFile"></param>       
        public String[] GetExcelSheetNames(string excelFile)
        {
            OleDbConnection objConn = null;
            System.Data.DataTable dt = null;
            try
            {
                // Connection String. Change the excel file to the file you

                // will search.

                //String connString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                //    "Data Source=" + excelFile + ";Extended Properties=Excel 4.0;";/// UPto 2007

                String connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelFile + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";// 2007 onWards

                // Create connection object by using the preceding connection string.

                objConn = new OleDbConnection(connString);
                // Open connection with the database.
                if (objConn.State == ConnectionState.Closed)
                {
                    ErrorMessage("Message Connection Open", "DirectCut_Readings_DocUpload", "78");
                    objConn.Open();
                    ErrorMessage("Message Connection Closed", "DirectCut_Readings_DocUpload", "78");
                }
                // Get the data table containg the schema guid.
                ErrorMessage("before GetOleDbSchemaTable", "DirectCut_Readings_DocUpload", "78");
                dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                ErrorMessage("after GetOleDbSchemaTable", "DirectCut_Readings_DocUpload", "78");
                if (dt == null)
                {
                    ErrorMessage("If dt Null", "DirectCut_Readings_DocUpload", "78");
                    return null;
                }

                String[] excelSheets = new String[dt.Rows.Count];
                int i = 0;

                // Add the sheet name to the string array.
                ErrorMessage("Before Add the sheet name to the string array.", "DirectCut_Readings_DocUpload", "78");
                foreach (DataRow row in dt.Rows)
                {
                    excelSheets[i] = row["TABLE_NAME"].ToString();
                    i++;
                }
                ErrorMessage("after Add the sheet name to the string array.", "DirectCut_Readings_DocUpload", "78");
                // Loop through all of the sheets if you want too...

                ErrorMessage("Before Loop through all of the sheets if you want too", "DirectCut_Readings_DocUpload", "78");
                for (int j = 0; j < excelSheets.Length; j++)
                {
                    // Query each excel sheet.

                }
                ErrorMessage("After Loop through all of the sheets if you want too", "DirectCut_Readings_DocUpload", "78");
                return excelSheets;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                // Clean up.
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                if (dt != null)
                {
                    dt.Dispose();
                }
            }
        }

        public void ExportToExcelSavedReport(DataTable Data, string FileName)
        {

            org.in2bits.MyXls.XlsDocument xls = new org.in2bits.MyXls.XlsDocument();
            org.in2bits.MyXls.Worksheet sheet = xls.Workbook.Worksheets.Add("sheet");
            //state the name of the column headings 
            org.in2bits.MyXls.Cells cells = sheet.Cells;
            int columnPosition = 1, rowPosition = 1;
            // Cell _objCell = null;

            string data = string.Empty;
            foreach (DataColumn column in Data.Columns)
            {
                sheet.Cells.Add(1, columnPosition, column.ColumnName);
                columnPosition++;
            }

            foreach (DataRow row in Data.Rows)
            {
                rowPosition++;
                int col = 1;
                foreach (DataColumn column in Data.Columns)
                {
                    data = row[column.ColumnName].ToString();
                    if (data.Trim() == "0")
                        data = "";
                    cells.Add(rowPosition, col, data);
                    col++;
                }
            }

            string filePath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + FileName + "_" + DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";

            if ((System.IO.File.Exists(filePath)))
            {
                System.IO.File.Delete(filePath);
            }

            //set up a filestream
            xls.FileName = filePath;

            //xls.FileName = Server.MapPath("Files/test.xls");
            xls.Save();

            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            objIdsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");

        }

        public void BindDisabledTypes(Control ctrl, bool IsRequiredSelect, string selectText, bool selectFirstItem) //This Method is for Billing Disabled Causes Types
        {
            BillingDisabledBooksBe _objDisabledBe = new BillingDisabledBooksBe();
            BillingDisabledBooksListBe _objListBe = objIdsignBal.DeserializeFromXml<BillingDisabledBooksListBe>(_objBillingDisabledBooksBal.Insert(_objDisabledBe, Statement.Generate));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlDisableType = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillingDisabledBooksBe>(_objListBe.items), ddlDisableType, "DisableType", "DisableTypeId", selectText, selectFirstItem);
                    break;
                case "CheckBoxList":
                    CheckBoxList cbl = (CheckBoxList)ctrl;
                    objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<BillingDisabledBooksBe>(_objListBe.items), cbl, "DisableType", "DisableTypeId");
                    break;

            }
        }

        internal void BindPagingDropDownListReport(int TotalRecords, int currentPageSize, DropDownList ddlPageSize)
        {
            ddlPageSize.Items.Clear();
            if (TotalRecords < Constants.PageSizeStartsReport)
            {
                ListItem item = new ListItem(Constants.PageSizeStartsReport.ToString(), Constants.PageSizeStartsReport.ToString());
                ddlPageSize.Items.Add(item);
            }
            else
            {
                int previousSize = Constants.PageSizeStartsReport;
                for (int i = Constants.PageSizeStartsReport; i <= TotalRecords; i = i + Constants.PageSizeIncrementReport)
                {
                    ListItem item = new ListItem(i.ToString(), i.ToString());
                    ddlPageSize.Items.Add(item);
                    previousSize = i;
                }
                if (previousSize < TotalRecords)
                {
                    ListItem item = new ListItem((previousSize + Constants.PageSizeIncrementReport).ToString(), (previousSize + Constants.PageSizeIncrementReport).ToString());
                    ddlPageSize.Items.Add(item);
                }
            }

            ddlPageSize.SelectedIndex = ddlPageSize.Items.IndexOf(ddlPageSize.Items.FindByText(currentPageSize.ToString()));
        }

        //kalyan

        //internal void BindPagingDDLReport(int TotalRecords, int currentPageSize, DropDownList ddlPageSize)
        //{
        //    ddlPageSize.Items.Clear();
        //    if (TotalRecords < Constants.PageSizeStartsReport)
        //    {
        //        ListItem item = new ListItem(Constants.PageSizeStartsReport.ToString(), Constants.PageSizeStartsReport.ToString());
        //        ddlPageSize.Items.Add(item);
        //    }

        //    else
        //    {
        //        int previousSize = Constants.PageSizeStartsReport;
        //        int j = 0;

        //        j = Convert.ToInt32(currentPageSize / 1000);
        //        if (j == 0)
        //        {
        //            ddlPageSize.Items.Add("100");
        //            ddlPageSize.Items.Add("200");
        //            ddlPageSize.Items.Add("500");
        //            ddlPageSize.Items.Add("1000");
        //        }
        //        else
        //        {
        //            j = j * 1000;
        //            ddlPageSize.Items.Add(Convert.ToString(j));
        //            ddlPageSize.Items.Add(Convert.ToString(j + 200));
        //            ddlPageSize.Items.Add(Convert.ToString(j + 500));
        //            ddlPageSize.Items.Add(Convert.ToString(j + 1000));
        //            previousSize += (j + 1000);
        //        }

        //        //for (int i = Constants.PageSizeStartsReport; i <= TotalRecords; i = i + Constants.PageSizeIncrementReport)
        //        //{
        //        //    ListItem item = new ListItem(i.ToString(), i.ToString());

        //        //    j = (Convert.ToInt32(item.Value) / 100) % 10;

        //        //    if (j == 0 || j == 1 || j == 2 || j == 5 || j == 10)
        //        //    {
        //        //        ddlPageSize.Items.Add(item);
        //        //        previousSize = i;
        //        //    }



        //        //    // previousSize = i;
        //        //}
        //        //if (previousSize < TotalRecords)
        //        //{
        //        //    ListItem item = new ListItem((previousSize + Constants.PageSizeIncrementReport).ToString(), (previousSize + Constants.PageSizeIncrementReport).ToString());
        //        //    ddlPageSize.Items.Add(item);
        //        //}
        //    }

        //    ddlPageSize.SelectedIndex = ddlPageSize.Items.IndexOf(ddlPageSize.Items.FindByText(currentPageSize.ToString()));
        //}

        public void BindPolesMaster(Control ctrl, bool IsRequiredSelect, bool IsSelectFirstItem)
        {
            PoleBE objPoleBE = new PoleBE();
            PoleListBE objPoleListBe = objIdsignBal.DeserializeFromXml<PoleListBE>(objPoleBal.GetPoles(objPoleBE, ReturnType.Get));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlPoleName = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<PoleBE>(objPoleListBe.items), ddlPoleName, "Name", "PoleMasterId", UMS_Resource.DROPDOWN_SELECT, IsSelectFirstItem);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<PoleBE>(objPoleListBe.items), ddlPoleName, "Name", "PoleMasterId", IsSelectFirstItem);
                    break;
            }
        }

        public void BindParentPole(Control ctrl, int PoleMasterId, bool IsRequiredSelect, bool IsSelectFirstItem)
        {
            PoleBE objPoleBE = new PoleBE();
            objPoleBE.PoleMasterId = PoleMasterId;
            PoleListBE objPoleListBe =
                objIdsignBal.DeserializeFromXml<PoleListBE>(objPoleBal.GetParentPoles(objPoleBE, ReturnType.Get));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlParent = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<PoleBE>(objPoleListBe.items), ddlParent, "Name", "PoleId", UMS_Resource.DROPDOWN_SELECT, IsSelectFirstItem);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<PoleBE>(objPoleListBe.items), ddlParent, "Name", "PoleId", IsSelectFirstItem);
                    break;
            }
        }

        public void BindEmployeeList(Control ctrl, bool IsRequiredSelect)
        {
            EmployeeListBE objEmployeeListBE = objIdsignBal.DeserializeFromXml<EmployeeListBE>(objEmployeeBAL.GetEmployeeForListBindDAL());
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlEmployeeList = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<EmployeeBE>(objEmployeeListBE.Items), ddlEmployeeList, "EmployeeName", "BEDCEmpId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<EmployeeBE>(objEmployeeListBE.Items), ddlEmployeeList, "EmployeeName", "BEDCEmpId", false);
                    break;
            }
        }

        public void BindMeterList(MastersBE objMastersBE, Control ctrl, bool IsRequiredSelect)
        {
            MastersListBE objMastersListBE = objIdsignBal.DeserializeFromXml<MastersListBE>(objMastersBAL.GetMeterNoList(objMastersBE));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlMeterList = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), ddlMeterList, "MeterNo", "MeterId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<MastersBE>(objMastersListBE.Items), ddlMeterList, "MeterNo", "MeterId", false);
                    break;
            }
        }

        public void BindPoleByOrderID(Control ctrl, PoleBE objPoleBE, bool IsRequiredSelect)
        {
            PoleListBE poleListBE = objIdsignBal.DeserializeFromXml<PoleListBE>(objPoleBal.GetPoleByOrderId(objPoleBE));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlPoleByOrdIdList = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<PoleBE>(poleListBE.items), ddlPoleByOrdIdList, "Name", "PoleId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<PoleBE>(poleListBE.items), ddlPoleByOrdIdList, "Name", "PoleId", false);
                    break;
            }
        }

        public void BindClusterCategories(Control ctrl, bool IsRequiredSelect)
        {
            ClusterCategoriesListBE objClusterCategoriesListBE = objIdsignBal.DeserializeFromXml<ClusterCategoriesListBE>(objMastersBAL.GetClusterCategoriesListToBind());
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlClusterCategories = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ClusterCategoriesBE>(objClusterCategoriesListBE.items), ddlClusterCategories, "CategoryName", "ClusterCategoryId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                    {
                        if (objClusterCategoriesListBE.items.Count > 0)
                            objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ClusterCategoriesBE>(objClusterCategoriesListBE.items), ddlClusterCategories, "CategoryName", "ClusterCategoryId", UMS_Resource.DROPDOWN_SELECT, false);
                        else
                            objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<ClusterCategoriesBE>(objClusterCategoriesListBE.items), ddlClusterCategories, "CategoryName", "ClusterCategoryId", false);
                    }
                    break;
            }
        }

        public void BindRolesList(Control ctrl, AdminBE objAdminBE, bool IsRequiredSelect)
        {
            AdminListBE _ObjAdminListBE = objIdsignBal.DeserializeFromXml<AdminListBE>(_objAdminBal.GetRolesList(objAdminBE));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlCopyRole = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<AdminBE>(_ObjAdminListBE.Items), ddlCopyRole, "RoleName", "RoleId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<AdminBE>(_ObjAdminListBE.Items), ddlCopyRole, "RoleName", "RoleId", false);
                    break;
            }
        }

        public void BindAccessLevelsList(Control ctrl, bool IsRequiredSelect)
        {
            AdminListBE _ObjAdminListBE = objIdsignBal.DeserializeFromXml<AdminListBE>(_objAdminBal.GetAccessLevelsList());
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlAccessLevels = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<AdminBE>(_ObjAdminListBE.Items), ddlAccessLevels, "AccessLevelName", "AccessLevelID", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<AdminBE>(_ObjAdminListBE.Items), ddlAccessLevels, "AccessLevelName", "AccessLevelID", false);
                    break;
            }
        }

        public void BindGovtAccountTypeList(GovtAccountTypeBE _objGovtAccountTypeBe, Control ctrl, bool IsRequiredSelect)
        {
            GovtAccountTypeListBE _ObjGovtAccountTypeListBE = objIdsignBal.DeserializeFromXml<GovtAccountTypeListBE>(_objGovtAccountTypeBAL.GetGovtAccountTypeList(_objGovtAccountTypeBe));
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlGovtAccountType = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<GovtAccountTypeBE>(_ObjGovtAccountTypeListBE.Items), ddlGovtAccountType, "AccountType", "GActTypeID", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<GovtAccountTypeBE>(_ObjGovtAccountTypeListBE.Items), ddlGovtAccountType, "AccountType", "GActTypeID", false);
                    break;
            }
        }

        public void BindControlTypes(Control ctrl, bool IsRequiredSelect)
        {
            XmlDocument xml = new XmlDocument();
            xml = objUserDefinedControlsBAL.GetUserDefinedControlsList();
            UserDefinedControlsListBE_1 objUserDefinedControlsListBE_1 =
                objIdsignBal.DeserializeFromXml<UserDefinedControlsListBE_1>(xml);
            switch (ctrl.GetType().Name)
            {
                case "EditableDropDownList":
                case "DropDownList":
                    DropDownList ddlControlTypes = (DropDownList)ctrl;
                    if (IsRequiredSelect)
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<UserDefinedControlsBE>(objUserDefinedControlsListBE_1.items), ddlControlTypes, "ControlTypeName", "ControlTypeId", UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<UserDefinedControlsBE>(objUserDefinedControlsListBE_1.items), ddlControlTypes, "ControlTypeName", "ControlTypeId", false);
                    break;
            }
        }

        public DataSet Insert_Get_DS(string procedureName, SqlParameter[] parameterCollections)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["Sqlcon"].ToString());
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            foreach (SqlParameter param in parameterCollections)
            {
                cmd.Parameters.Add(param.ParameterName, param.Value);
            }
            cmd.CommandText = procedureName;// "USP_InsertBillGenerationDetails";
            cmd.Connection = con;
            //int i = 0;
            //i = cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }

        public void SendMailScheduledBill(string cycleName, string billMonth, string billYear, string toEmailId)
        {
            try
            {
                //string item = string.IsNullOrEmpty(cycleName) ? "Feeder : " + feederId : "Cycle : " + cycleName;

                string smtpUserName = ConfigurationManager.AppSettings["SMTPUsername"].ToString();
                string smtpPassword = ConfigurationManager.AppSettings["SMTPPassword"].ToString();
                string smtpServer = ConfigurationManager.AppSettings["SMTPServer"].ToString();
                string smtpPort = ConfigurationManager.AppSettings["SMTPPort_TLS"].ToString();

                NetworkCredential cred = new NetworkCredential(smtpUserName, smtpPassword);
                MailMessage msg = new MailMessage();
                msg.To.Add(toEmailId);
                msg.CC.Add("satya.kuppi@idsigntechnologies.com,neeraj.k@idsigntechnologies.com");
                msg.Subject = "BEDC Bill Scheduled";
                //msg.Attachments.Add(new Attachment(fileName));
                msg.Body = "Billing Month: " + billMonth + "-" + billYear.ToString() + "<br />Bills are successfully scheduled for the following Book Group:  <br />" + cycleName.Replace(",", "<br />");
                msg.From = new MailAddress(smtpUserName, "BEDC Department Of Bill Generation"); // Your Email Id
                SmtpClient client = new SmtpClient(smtpServer, Convert.ToInt32(smtpPort));
                //SmtpClient client1 = new SmtpClient("smtp.mail.yahoo.com", 465);
                client.Credentials = cred;
                client.EnableSsl = true;
                msg.IsBodyHtml = true;
                client.Send(msg);
            }
            catch (Exception ex)
            {
                //throw
            }
        }

        public int IsNullValue_Int(string value)
        {
            int intValue = 0;
            if (!string.IsNullOrEmpty(value))
                intValue = Convert.ToInt32(value);
            return intValue;
        }

        //public bool IsFinalApproval(int FunctionId, int RoleId, string UserId)
        //{
        //    bool isFinalApproval = false;

        //    ApprovalBe _objApprovalBe = new ApprovalBe();
        //    _objApprovalBe.RoleId = RoleId;
        //    _objApprovalBe.FunctionId = FunctionId;
        //    _objApprovalBe.UserId = UserId;
        //    _objApprovalBe = objIdsignBal.DeserializeFromXml<ApprovalBe>(objApprovalBal.Insert(_objApprovalBe, Statement.Check));
        //    if (_objApprovalBe != null)
        //    {
        //        isFinalApproval = _objApprovalBe.IsFinalApproval;
        //    }
        //    return isFinalApproval;
        //}

        public bool IsFinalApproval(int FunctionId, int RoleId, string UserId, string BU_ID)
        {
            bool isFinalApproval = false;

            ApprovalBe _objApprovalBe = new ApprovalBe();
            _objApprovalBe.RoleId = RoleId;
            _objApprovalBe.FunctionId = FunctionId;
            _objApprovalBe.UserId = UserId;
            _objApprovalBe.BU_ID = BU_ID;
            _objApprovalBe = objIdsignBal.DeserializeFromXml<ApprovalBe>(objApprovalBal.Insert(_objApprovalBe, Statement.Check));
            if (_objApprovalBe != null)
            {
                isFinalApproval = _objApprovalBe.IsFinalApproval;
            }
            return isFinalApproval;
        }

        public bool IsEmailFeatureEnabled(CommunicationFeaturesBE objCommunicationFeaturesBE)
        {
            CommunicationFeaturesBAL objCommunicationFeaturesBAL = new CommunicationFeaturesBAL();
            CommunicationFeaturesBE _objCommunicationFeaturesBE = new CommunicationFeaturesBE();
            XmlDocument xml = new XmlDocument();
            xml = objCommunicationFeaturesBAL.IsEmailFeatureEnabled(objCommunicationFeaturesBE);
            _objCommunicationFeaturesBE = objIdsignBal.DeserializeFromXml<CommunicationFeaturesBE>(xml);
            return _objCommunicationFeaturesBE.IsEmailFeature;
        }

        public bool IsSMSFeatureEnabled(CommunicationFeaturesBE objCommunicationFeaturesBE)
        {
            CommunicationFeaturesBAL objCommunicationFeaturesBAL = new CommunicationFeaturesBAL();
            CommunicationFeaturesBE _objCommunicationFeaturesBE = new CommunicationFeaturesBE();
            XmlDocument xml = new XmlDocument();
            xml = objCommunicationFeaturesBAL.IsSMSFeatureEnabled(objCommunicationFeaturesBE);
            _objCommunicationFeaturesBE = objIdsignBal.DeserializeFromXml<CommunicationFeaturesBE>(xml);
            return _objCommunicationFeaturesBE.IsSMSFeature;
        }

        public XmlDocument GetDetails(CommunicationFeaturesBE objCommunicationFeaturesBE, ReturnType value)
        {
            CommunicationFeaturesBAL objCommunicationFeaturesBAL = new CommunicationFeaturesBAL();
            CommunicationFeaturesBE _objCommunicationFeaturesBE = new CommunicationFeaturesBE();
            XmlDocument xml = new XmlDocument();
            xml = objCommunicationFeaturesBAL.GetDetails(objCommunicationFeaturesBE, value);
            return xml;
        }

        public string GetMailCommTemp(CommunicationFeatures featureName, string userName, string YearMonth, string GlobalAccountNo, string MeterNo, string TotalBillAmount, string OutStandingAmount, string DueDate, string PaidAmount, string PaidDate, string AdjustedAmount, string description)
        {
            string path = string.Empty;
            string body = string.Empty;
            switch (featureName)
            {
                case CommunicationFeatures.BillGeneration:
                    path = HttpContext.Current.Server.MapPath(ConfigurationSettings.AppSettings["BillGenerationMailTemplate"]);

                    if (!string.IsNullOrEmpty(path))
                    {
                        using (StreamReader reader = new StreamReader(path))
                        {
                            body = reader.ReadToEnd();
                        }
                        body = body.Replace("{UserName}", userName);
                        body = body.Replace("{YearMonth}", YearMonth);
                        body = body.Replace("{GlobalAccountNo}", GlobalAccountNo);
                        body = body.Replace("{MeterNo}", MeterNo);
                        body = body.Replace("{TotalBillAmount}", TotalBillAmount);
                        body = body.Replace("{OutStandingAmount}", OutStandingAmount);
                        body = body.Replace("{DueDate}", DueDate);
                        body = body.Replace("{Description}", description);
                    }

                    break;
                case CommunicationFeatures.Payments:
                    path = HttpContext.Current.Server.MapPath(ConfigurationSettings.AppSettings["PaymentMailTemplate"]);
                    if (!string.IsNullOrEmpty(path))
                    {
                        using (StreamReader reader = new StreamReader(path))
                        {
                            body = reader.ReadToEnd();
                        }
                        body = body.Replace("{UserName}", userName);
                        body = body.Replace("{GlobalAccountNo}", GlobalAccountNo);
                        body = body.Replace("{MeterNo}", MeterNo);
                        body = body.Replace("{PaidAmount}", PaidAmount);
                        body = body.Replace("{OutStandingAmount}", OutStandingAmount);
                        body = body.Replace("{PaidDate}", PaidDate);
                        body = body.Replace("{Description}", description);
                    }
                    break;
                case CommunicationFeatures.Adjustments:
                    path = HttpContext.Current.Server.MapPath(ConfigurationSettings.AppSettings["AdjustmentMailTemplate"]);
                    if (!string.IsNullOrEmpty(path))
                    {
                        using (StreamReader reader = new StreamReader(path))
                        {
                            body = reader.ReadToEnd();
                        }
                        body = body.Replace("{UserName}", userName);
                        body = body.Replace("{GlobalAccountNo}", GlobalAccountNo);
                        body = body.Replace("{MeterNo}", MeterNo);
                        body = body.Replace("{AdjustedAmount}", AdjustedAmount);
                        body = body.Replace("{OutStandingAmount}", OutStandingAmount);
                        body = body.Replace("{Description}", description);
                    }
                    break;
            }
            return body;
        }

        public string GetSMSCommTemp(CommunicationFeatures featureName, string accountNo, string monthyear, string amount, string duedate, string transactionId, string outstanding)
        {
            string textmessage = string.Empty;
            DataSet dsTemplate = new DataSet();
            string message = string.Empty;
            dsTemplate.ReadXml(HttpContext.Current.Server.MapPath(ConfigurationSettings.AppSettings["SmsTemplate"]));
            switch (featureName)
            {
                case CommunicationFeatures.BillGeneration:
                    message = dsTemplate.Tables[0].Rows[0]["billgeneration"].ToString();
                    textmessage = string.Format(message, accountNo, monthyear, amount, duedate);
                    break;
                case CommunicationFeatures.Payments:
                    message = dsTemplate.Tables[0].Rows[0]["payments"].ToString();
                    textmessage = string.Format(message, amount, accountNo, transactionId);
                    break;
                case CommunicationFeatures.Adjustments:
                    message = dsTemplate.Tables[0].Rows[0]["adjustments"].ToString();
                    textmessage = string.Format(message, amount, outstanding, accountNo, transactionId);
                    break;
            }
            return textmessage;
        }

        public void sendmail(string EmailBody, string FromEmailId, string Subject, string ToEmailId, string attachment)
        {
            Communication.EmailInput.EmailInput objEmailInput = new Communication.EmailInput.EmailInput();
            Communication.Mail.Email objEmail = new Communication.Mail.Email();
            objEmailInput.EmailBody = EmailBody + DateTime.Now.ToShortDateString();
            objEmailInput.FromEmail = FromEmailId;
            objEmailInput.Subject = Subject;
            objEmailInput.Attachment = attachment;
            objEmailInput.To = ToEmailId;
            objEmailInput.SMTPServer = ConfigurationManager.AppSettings["SMTPServer"];
            objEmailInput.SMTPPort = Convert.ToInt16(ConfigurationManager.AppSettings["SMTPPort"]);
            objEmailInput.SMTPUsername = ConfigurationManager.AppSettings["SMTPUsername"];
            objEmailInput.SMTPPassword = ConfigurationManager.AppSettings["SMTPPassword"];
            objEmailInput.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
            objEmailInput.UserDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]);
            objEmail.SendMail(objEmailInput);
        }

        public void sendSMS(string MobileNo, string SMSMessage, string DeliveryEmailId)
        {
            Communication.SmsInput.SmsInput _objSmsInput = new Communication.SmsInput.SmsInput();
            Communication.Sms.Sms _objsms = new Communication.Sms.Sms();
            _objSmsInput.SMSLoginId = ConfigurationManager.AppSettings["SMSLoginId"];
            _objSmsInput.SMSLoginPassword = ConfigurationManager.AppSettings["SMSLoginPassword"];
            _objSmsInput.SMSCallBack = ConfigurationManager.AppSettings["SMSCallBack"];
            _objSmsInput.SMSSiteTokenId = ConfigurationManager.AppSettings["SMSSiteTokenId"];
            _objSmsInput.SMSMobileNo = MobileNo;
            _objSmsInput.SMSMessage = SMSMessage;
            _objSmsInput.SMSDeliveryEmailId = DeliveryEmailId;
            _objsms.SendSMS(_objSmsInput);
        }
        #endregion
    }
}