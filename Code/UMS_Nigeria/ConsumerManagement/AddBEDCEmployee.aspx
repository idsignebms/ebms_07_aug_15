﻿<%@ Page Title=":: Add BEDC Employee ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="AddBEDCEmployee.aspx.cs" Inherits="UMS_Nigeria.ConsumerManagement.AddBEDCEmployee" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" class="modalBackground">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdatePanel ID="upAddCountries" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddState" runat="server" Text="<%$ Resources:Resource, BEDC_EMPLOYEE%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="litCountryName" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                                <span class="span_star">*</span><div class="space">
                                </div>
                                <asp:TextBox CssClass="text-box" ID="txtName" TabIndex="1" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanName','Name')"
                                    MaxLength="50" placeholder="Enter Name"></asp:TextBox>
                                <span id="spanName" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, DESIGNATION%>"></asp:Literal>
                                <span class="span_star">*</span><div class="space">
                                </div>
                                <%--<asp:TextBox CssClass="text-box" ID="txtDesignation" TabIndex="2" runat="server"
                                    onblur="return TextBoxBlurValidationlbl(this,'spanCountryName','Country Name')"
                                    MaxLength="50" placeholder="Enter Designation"></asp:TextBox>--%>
                                <asp:DropDownList ID="ddlDesignation" TabIndex="2" AutoPostBack="true" runat="server"
                                    CssClass="text-box text-select">
                                    <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                                </asp:DropDownList>
                                <div class="space">
                                </div>
                                <span id="spanDesignation" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, LOCATION%>"></asp:Literal>
                                <span class="span_star">*</span><div class="space">
                                </div>
                                <asp:TextBox CssClass="text-box" ID="txtLocation" TabIndex="3" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanLocation','Location')"
                                    MaxLength="50" placeholder="Enter Location"></asp:TextBox>
                                <span id="spanLocation" class="span_color"></span>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litContactNo" runat="server" Text="<%$ Resources:Resource, CONTACT_NO%>"></asp:Literal>
                                    <span class="span_star">*</span></label>
                                <div class="space">
                                </div>
                                <asp:TextBox CssClass="text-box" TabIndex="4" ID="txtCode1" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanContactNo','Code')"
                                    MaxLength="3" Width="11%" placeholder="Code"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtCode1"
                                    FilterType="Numbers" ValidChars=" ">
                                </asp:FilteredTextBoxExtender>
                                <asp:TextBox CssClass="text-box" ID="txtContactNo" TabIndex="5" Width="40%" runat="server"
                                    onblur="return TextBoxBlurValidationlbl(this,'spanContactNo','Contact No')" MaxLength="10"
                                    placeholder="Enter Contact No"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtContactNo"
                                    FilterType="Numbers" ValidChars=" ">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanContactNo" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litAnotherContactNo" runat="server" Text="<%$ Resources:Resource, ANOTHER_CONTACT_NO%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:TextBox CssClass="text-box" TabIndex="6" ID="txtCode2" runat="server" MaxLength="3"
                                    Width="11%" placeholder="Code"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtCode2"
                                    FilterType="Numbers" ValidChars=" ">
                                </asp:FilteredTextBoxExtender>
                                <asp:TextBox CssClass="text-box" TabIndex="7" ID="txtAnotherContactNo" Width="40%"
                                    runat="server" MaxLength="10" placeholder="Enter Alternate Contact No"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbTxtAnotherContactNo" runat="server" TargetControlID="txtAnotherContactNo"
                                    FilterType="Numbers" ValidChars=" ">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanAnotherNo" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litAddress" runat="server" Text="<%$ Resources:Resource, ADDRESS%>"></asp:Literal>
                                    <span class="span_star">*</span></label>
                                <div class="space">
                                </div>
                                <asp:TextBox ID="txtAddress" TabIndex="8" CssClass="text-box" runat="server" placeholder="Enter Address"
                                    TextMode="MultiLine" onblur="return TextBoxBlurValidationlbl(this,'spanAddress','Address')"></asp:TextBox>
                                <span id="spanAddress" class="span_color"></span>
                            </div>
                        </div>
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <asp:Button ID="btnSave" TabIndex="6" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                CssClass="box_s" runat="server" OnClick="btnSave_Click" />
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="grid_boxes">
                        <div class="grid_pagingTwo_top">
                            <div class="paging_top_titleTwo">
                                <asp:Literal ID="litCountriesList" runat="server" Text="<%$ Resources:Resource, EMPLOYEE%>"></asp:Literal>
                            </div>
                            <div class="paging_top_rightTwo_content" id="divpaging" runat="server">
                                <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvEmployeeList" runat="server" AutoGenerateColumns="False" OnRowCommand="gvEmployeeList_RowCommand"
                            OnRowDataBound="gvEmployeeList_RowDataBound" HeaderStyle-CssClass="grid_tb_hd ">
                            <EmptyDataRowStyle HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                    HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%--<asp:Label ID="lblEmpId" Visible="false" runat="server" Text='<%#Eval("BEDCEmpId")%>'></asp:Label>--%>
                                        <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        <asp:Label ID="lblActiveStatusId" Visible="false" runat="server" Text='<%#Eval("ActiveStatusId")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Employee ID" runat="server" HeaderStyle-Width="3%"
                                    HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmpId" runat="server" Text='<%#Eval("BEDCEmpId")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblName" runat="server" Text='<%#Eval("EmployeeName") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridName" MaxLength="50" placeholder="Enter Name"
                                            Visible="false" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, DESIGNATION%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDesignation" runat="server" Text='<%#Eval("Designation") %>'></asp:Label>
                                        <%--<asp:TextBox CssClass="text-box" ID="txtGridDesignation" MaxLength="50" placeholder="Enter Designation"
                                            Visible="false" runat="server"></asp:TextBox>--%>
                                        <asp:Label ID="lblGridDesignationId" runat="server" Visible="false" Text='<%#Eval("DesignationId") %>'></asp:Label>
                                        <asp:DropDownList ID="ddlGridDesignation" runat="server" Visible="false">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, LOCATION %>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLocation" runat="server" Text='<%#Eval("Location") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" MaxLength="50" ID="txtGridLocation" placeholder="Enter Location"
                                            Visible="false" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, CONTACT_NO%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContactNo1" runat="server" Text='<%#Eval("ContactNo") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridCode1" runat="server" MaxLength="3" Width="11%"
                                            placeholder="Code" Visible="false"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtGridCode1"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:TextBox CssClass="text-box" ID="txtGridContactNo1" runat="server" MaxLength="10"
                                            placeholder="Enter Contact No" Visible="false"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="ftbGridTxtContactNo7" runat="server" TargetControlID="txtGridContactNo1"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, ANOTHER_CONTACT_NO%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContactNo2" runat="server" Text='<%#Eval("AnotherContactNo") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridCode2" Visible="false" runat="server"
                                            MaxLength="3" Width="11%" placeholder="Code"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtGridCode2"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:TextBox CssClass="text-box" ID="txtGridContactNo2" MaxLength="10" placeholder="Enter Alternate Contact No"
                                            runat="server" Visible="false">
                                        </asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="ftbTxtGridContactNo5" runat="server" TargetControlID="txtGridContactNo2"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, ADDRESS%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress" runat="server" Text='<%#Eval("Address") %>'></asp:Label>
                                        <asp:TextBox ID="txtGridAddress" CssClass="text-box" Rows="4" cols="50" placeholder="Enter Address"
                                            runat="server" Visible="false" TextMode="MultiLine"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lkbtnEdit" runat="server" ToolTip="Edit" Text="Edit" CommandName="EditEmployee">
                                        <img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnUpdate" Visible="false" runat="server" ToolTip="Update"
                                            CommandName="UpdateEmployee"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnCancel" Visible="false" runat="server" ToolTip="Cancel"
                                            CommandName="CancelEmployee"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnActive" Visible="false" runat="server" ToolTip="Activate"
                                            CommandName="ActiveEmployee"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnDeActive" runat="server" ToolTip="DeActivate" CommandName="DeActiveEmployee">
                                    <img src="../images/Deactivate.gif"   alt="DeActive" /></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:HiddenField ID="hfPageNo" runat="server" />
                        <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                        <asp:HiddenField ID="hfLastPage" runat="server" />
                        <asp:HiddenField ID="hfEmpId" runat="server" />
                        <asp:HiddenField ID="hfRecognize" runat="server" />
                        <asp:HiddenField ID="hfPageSize" runat="server" />
                    </div>
                    <div class="grid_boxes">
                        <div id="divdownpaging" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div id="hidepopup">
                        <%-- Active Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                            TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="PanelActivate" runat="server" CssClass="modalPopup" Style="display: none;
                            border-color: #639B00;">
                            <div class="popheader" id="pop" runat="server">
                                <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                            </div>
                            <br />
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btnActiveOk" runat="server" OnClick="btnActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                    <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Active  Btn popup Closed--%>
                        <%-- Grid Alert popup Start--%>
                        <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                            TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;"
                            DefaultButton="btngridalertok">
                            <div class="popheader popheaderlblred">
                                <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                            </div>
                            <div class="footer popfooterbtnrt">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Grid Alert popup Closed--%>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <script src="../JavaScript/PageValidations/AddBEDCEmployee.js" type="text/javascript"></script>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
      <script language="javascript" type="text/javascript">
          $(document).ready(function () { assignDiv('rptrMainMasterMenu_174_10', '174') });
    </script>
</asp:Content>
