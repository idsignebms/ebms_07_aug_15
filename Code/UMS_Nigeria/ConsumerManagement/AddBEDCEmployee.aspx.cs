﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddBEDCEmployee
                     
 Developer        : id065-Bhimaraju V
 Creation Date    : 09-Feb-2015
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBE;
using System.Xml;
using UMS_NigeriaBAL;
using Resources;
using System.Data;
using System.Threading;
using System.Globalization;
using System.Drawing;
using UMS_NigriaDAL;
using System.Web.Services;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class AddBEDCEmployee : System.Web.UI.Page
    {
        #region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        CommonDAL _objCommonDAL = new CommonDAL();
        public int PageNum;
        XmlDocument xml = null;
        EmployeeBE _objEmployeeBe = new EmployeeBE();
        EmployeeBAL _objEmployeeBAL = new EmployeeBAL();
        string Key = "AddBEDCEmployee";
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            try
            {
                if (Session[UMS_Resource.SESSION_LOGINID] == null)
                    Response.Redirect(UMS_Resource.DEFAULT_PAGE, false);
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, ErrorConstants.Page_PreInit, ErrorLog.LogType.Error, ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = pnlMessage.CssClass = string.Empty;
                if (!IsPostBack)
                {
                    _objCommonMethods.BindDesignations(ddlDesignation, 0, true);
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message,ErrorConstants.Page_Load, ErrorLog.LogType.Error, ex);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                EmployeeBE _objEmployeeBE = InsertEmployee();
                if (_objEmployeeBE.IsSuccess)
                {
                    BindGrid();
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    txtName.Text = txtLocation.Text = txtCode1.Text = txtCode2.Text = txtContactNo.Text = txtAnotherContactNo.Text = txtAddress.Text = string.Empty;
                    ddlDesignation.SelectedIndex = 0;
                    Message(Resource.EMP_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                {
                    Message(Resource.EMP_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, ErrorConstants.btnSave_Click, ErrorLog.LogType.Error, ex);
            }
        }

        private EmployeeBE InsertEmployee()
        {
            EmployeeBE _objEmployeeBE = new EmployeeBE();
            try
            {
                _objEmployeeBE.EmployeeName = txtName.Text;
                _objEmployeeBE.Address = _objiDsignBal.ReplaceNewLines(txtAddress.Text, true);
                //_objEmployeeBE.Designation = txtDesignation.Text;
                _objEmployeeBE.DesignationId = Convert.ToInt32(ddlDesignation.SelectedValue);
                _objEmployeeBE.Location = txtLocation.Text;
                _objEmployeeBE.ContactNo = txtCode1.Text + "-" + txtContactNo.Text;
                if (txtAnotherContactNo.Text != "")
                    _objEmployeeBE.AnotherContactNo = txtCode2.Text + "-" + txtAnotherContactNo.Text;
                _objEmployeeBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objEmployeeBE = _objiDsignBal.DeserializeFromXml<EmployeeBE>(_objEmployeeBAL.InsertEmployee(_objEmployeeBE, Statement.Insert));

            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message,ErrorConstants.Insert, ErrorLog.LogType.Error, ex);
            }
            return _objEmployeeBE;
        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (hfRecognize.Value == UMS_Resource.ACTIVATE)
                {
                    UpdateActiveStatus(Constants.Active, Resource.EMP_ACTIVATED_SUCCESS, Resource.EMP_CHARGE_ACTIVATED_FAILED);
                }
                else if (hfRecognize.Value == UMS_Resource.DEACTIVATE)
                {
                    UpdateActiveStatus(Constants.DeActive, Resource.EMP_CHARGE_DEACTIVATED_SUCCESS, Resource.EMP_CHARGE_DEACTIVATED_FAILED);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message,ErrorConstants.btnActiveOk_Click, ErrorLog.LogType.Error, ex);
            }
        }

        private void UpdateActiveStatus(int activeStatus, string successMessage, string failedMessage)
        {
            try
            {
                hfRecognize.Value = string.Empty;
                EmployeeBE _objEmployeeBE = new EmployeeBE();
                _objEmployeeBE.BEDCEmpId = Convert.ToInt32(hfEmpId.Value);
                _objEmployeeBE.ActiveStatusId = activeStatus;
                _objEmployeeBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objEmployeeBE = _objiDsignBal.DeserializeFromXml<EmployeeBE>(_objEmployeeBAL.InsertEmployee(_objEmployeeBE, Statement.Change));
                if (_objEmployeeBE.IsSuccess)
                {
                    Message(successMessage, UMS_Resource.MESSAGETYPE_SUCCESS);
                    BindGrid();
                }
                else
                {
                    Message(failedMessage, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message,ErrorConstants.Update, ErrorLog.LogType.Error, ex);
            }
        }

        protected void gvEmployeeList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    TextBox txtGridName = (TextBox)e.Row.FindControl("txtGridName");
                    //TextBox txtGridDesignation = (TextBox)e.Row.FindControl("txtGridDesignation");
                    DropDownList ddlGridDesignation = (DropDownList)e.Row.FindControl("ddlGridDesignation");
                    TextBox txtGridLocation = (TextBox)e.Row.FindControl("txtGridLocation");
                    TextBox txtGridCode1 = (TextBox)e.Row.FindControl("txtGridCode1");
                    TextBox txtGridContactNo1 = (TextBox)e.Row.FindControl("txtGridContactNo1");
                    TextBox txtGridCode2 = (TextBox)e.Row.FindControl("txtGridCode2");
                    TextBox txtGridContactNo2 = (TextBox)e.Row.FindControl("txtGridContactNo2");
                    TextBox txtGridAddress = (TextBox)e.Row.FindControl("txtGridAddress");


                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridName.ClientID + "','"
                                                                                      + ddlGridDesignation.ClientID + "','"
                                                                                      + txtGridLocation.ClientID + "','"
                                                                                      + txtGridCode1.ClientID + "','"
                                                                                      + txtGridContactNo1.ClientID + "','"
                                                                                      + txtGridCode2.ClientID + "','"
                                                                                      + txtGridContactNo2.ClientID + "','"
                                                                                      + txtGridAddress.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");

                    if (Convert.ToInt32(lblActiveStatusId.Text) == Constants.DeActive)
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message,ErrorConstants.RowDataBound, ErrorLog.LogType.Error, ex);
            }
        }

        protected void gvEmployeeList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                EmployeeBE _objEmployeeBE = new EmployeeBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridName = (TextBox)row.FindControl("txtGridName");
                //TextBox txtGridDesignation = (TextBox)row.FindControl("txtGridDesignation");
                DropDownList ddlGridDesignation = (DropDownList)row.FindControl("ddlGridDesignation");
                TextBox txtGridLocation = (TextBox)row.FindControl("txtGridLocation");
                TextBox txtGridCode1 = (TextBox)row.FindControl("txtGridCode1");
                TextBox txtGridContactNo1 = (TextBox)row.FindControl("txtGridContactNo1");
                TextBox txtGridCode2 = (TextBox)row.FindControl("txtGridCode2");
                TextBox txtGridContactNo2 = (TextBox)row.FindControl("txtGridContactNo2");
                TextBox txtGridAddress = (TextBox)row.FindControl("txtGridAddress");

                Label lblEmpId = (Label)row.FindControl("lblEmpId");
                Label lblName = (Label)row.FindControl("lblName");
                Label lblDesignation = (Label)row.FindControl("lblDesignation");
                Label lblGridDesignationId = (Label)row.FindControl("lblGridDesignationId");
                Label lblLocation = (Label)row.FindControl("lblLocation");
                Label lblContactNo1 = (Label)row.FindControl("lblContactNo1");
                Label lblContactNo2 = (Label)row.FindControl("lblContactNo2");
                Label lblAddress = (Label)row.FindControl("lblAddress");

                switch (e.CommandName.ToUpper())
                {
                    case "EDITEMPLOYEE":
                        txtGridName.Visible = ddlGridDesignation.Visible = txtGridLocation.Visible = txtGridCode1.Visible = txtGridContactNo1.Visible = txtGridCode2.Visible = txtGridContactNo2.Visible = txtGridAddress.Visible = true;
                        lblName.Visible = lblDesignation.Visible = lblLocation.Visible = lblContactNo1.Visible = lblContactNo2.Visible = lblAddress.Visible = false;
                        txtGridName.Text = lblName.Text;
                        ddlGridDesignation.SelectedValue = lblGridDesignationId.Text;
                        txtGridLocation.Text = lblLocation.Text;
                        if (lblContactNo1.Text.Length > 0 && lblContactNo1.Text != "--")
                        {
                            string[] contactNo = lblContactNo1.Text.Split('-');
                            if (contactNo.Length < 2)
                            {
                                txtGridCode1.Text = lblContactNo1.Text.Substring(0, 3);
                                txtGridContactNo1.Text = lblContactNo1.Text.Remove(0, 3);
                            }
                            else
                            {
                                txtGridCode1.Text = contactNo[0].ToString();
                                txtGridContactNo1.Text = contactNo[1].ToString();
                            }
                        }
                        else
                            txtGridCode1.Text = txtGridContactNo1.Text = string.Empty;

                        if (lblContactNo2.Text.Length > 0 && lblContactNo2.Text != "--")
                        {
                            string[] contactNo = lblContactNo2.Text.Split('-');
                            if (contactNo.Length < 2)
                            {
                                txtGridCode2.Text = lblContactNo2.Text.Substring(0, 3);
                                txtGridContactNo2.Text = lblContactNo2.Text.Remove(0, 3);
                            }
                            else
                            {
                                txtGridCode2.Text = contactNo[0].ToString();
                                txtGridContactNo2.Text = contactNo[1].ToString();
                            }
                        }
                        else
                            txtGridCode2.Text = txtGridContactNo2.Text = string.Empty;

                        txtGridAddress.Text = _objiDsignBal.ReplaceNewLines(lblAddress.Text, false);
                        int DesignationId = Convert.ToInt32(lblGridDesignationId.Text);
                        _objCommonMethods.BindDesignations(ddlGridDesignation, 0, true);

                        ddlDesignation.SelectedIndex = ddlDesignation.Items.IndexOf(ddlDesignation.Items.FindByValue(lblGridDesignationId.Text));


                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;
                        break;
                    case "CANCELEMPLOYEE":
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                        txtGridName.Visible = ddlGridDesignation.Visible = txtGridLocation.Visible = txtGridCode1.Visible = txtGridContactNo1.Visible = txtGridCode2.Visible = txtGridContactNo2.Visible = txtGridAddress.Visible = false;
                        lblName.Visible = lblDesignation.Visible = lblLocation.Visible = lblContactNo1.Visible = lblContactNo2.Visible = lblAddress.Visible = true;
                        break;
                    case "UPDATEEMPLOYEE":
                        _objEmployeeBE.BEDCEmpId = Convert.ToInt32(lblEmpId.Text);
                        _objEmployeeBE.EmployeeName = txtGridName.Text;
                        _objEmployeeBE.Designation = ddlGridDesignation.Text;
                        _objEmployeeBE.DesignationId = Convert.ToInt32(ddlGridDesignation.SelectedValue);
                        _objEmployeeBE.Location = txtGridLocation.Text;
                        _objEmployeeBE.ContactNo = txtGridCode1.Text + "-" + txtGridContactNo1.Text;
                        if (txtGridContactNo2.Text != "")
                            _objEmployeeBE.AnotherContactNo = txtGridCode2.Text + "-" + txtGridContactNo2.Text;
                        _objEmployeeBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        _objEmployeeBE.Address = _objiDsignBal.ReplaceNewLines(txtGridAddress.Text, true);
                        xml = _objEmployeeBAL.InsertEmployee(_objEmployeeBE, Statement.Update);
                        _objEmployeeBE = _objiDsignBal.DeserializeFromXml<EmployeeBE>(xml);
                        if (Convert.ToBoolean(_objEmployeeBE.IsSuccess))
                        {
                            BindGrid();
                            Message(Resource.EMP_UPDATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        else
                            Message(Resource.EMP_UPDATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                        break;
                    case "ACTIVEEMPLOYEE":
                        mpeActivate.Show();
                        hfRecognize.Value = UMS_Resource.ACTIVATE;
                        hfEmpId.Value = lblEmpId.Text;
                        pop.Attributes.Add("class", "popheader popheaderlblgreen");
                        lblActiveMsg.Text = Resource.ACTIVE_EMP_POPUP_TEXT;
                        btnActiveOk.Focus();
                        break;
                    case "DEACTIVEEMPLOYEE":
                        mpeActivate.Show();
                        hfRecognize.Value = UMS_Resource.DEACTIVATE;
                        hfEmpId.Value = lblEmpId.Text;
                        pop.Attributes.Add("class", "popheader popheaderlblred");
                        lblActiveMsg.Text = Resource.DEACTIVE_EMP_POPUP_TEXT;
                        btnActiveOk.Focus();
                        break;
                }
            }

            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message,ErrorConstants.RowCommand, ErrorLog.LogType.Error, ex);
            }
        }
        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, ErrorConstants.Message, ErrorLog.LogType.Error, ex);
            }
        }

        public void BindGrid()
        {
            try
            {
                EmployeeListBE _objEmployeeListBE = new EmployeeListBE();
                EmployeeBE _objEmployeeBE = new EmployeeBE();
                XmlDocument resultedXml = new XmlDocument();

                _objEmployeeBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                _objEmployeeBE.PageSize = PageSize;
                resultedXml = _objEmployeeBAL.GetEmployee(_objEmployeeBE, ReturnType.Get);
                _objEmployeeListBE = _objiDsignBal.DeserializeFromXml<EmployeeListBE>(resultedXml);

                if (_objEmployeeListBE.Items.Count > 0)
                {
                    divdownpaging.Visible = divpaging.Visible = true;
                    UCPaging1.Visible = UCPaging2.Visible = true;
                    hfTotalRecords.Value = _objEmployeeListBE.Items[0].TotalRecords.ToString();
                    gvEmployeeList.DataSource = _objEmployeeListBE.Items;
                    gvEmployeeList.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = _objEmployeeListBE.Items.Count.ToString();
                    divdownpaging.Visible = divpaging.Visible = false;
                    gvEmployeeList.DataSource = new DataTable(); ;
                    gvEmployeeList.DataBind();
                }

            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, ErrorConstants.BindGrid, ErrorLog.LogType.Error, ex);
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message,ErrorConstants.InitializeCulture, ErrorLog.LogType.Error, ex);
            }
        }
        #endregion
    }
}