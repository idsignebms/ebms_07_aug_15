﻿<%@ Page Title="::Adjustment(No Bills)::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    AutoEventWireup="true" CodeBehind="Adjustment_NoBill.aspx.cs" Inherits="UMS_Nigeria.ConsumerManagement.Adjustment_NoBill"
    Theme="Green" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControls/Authentication.ascx" TagName="Authentication" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/UC_AdditionalChargesLastTransaction.ascx" TagName="UC_AdditionalChargesLastTransaction"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function Validation() {
            var IsValid = true;
            var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo');

            if (TextBoxValidationlbl(txtAccountNo, document.getElementById("spanAccountNo"), "Global Account No / Old Account No") == false) IsValid = false;
            return IsValid;
        }
    </script>
    <style type="text/css">
        #UMSNigeriaBody_pnlAlert
        {
            z-index: 100003 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out_bor_Payment">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upPaymentEntry" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div class="text_total">
                    <div class="text-heading">
                        <asp:Literal ID="litAddState" runat="server" Text="Adjustment (No-Bills)"></asp:Literal>
                    </div>
                    <div class="star_text">
                        <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div class="dot-line">
                </div>
                <div class="inner-box">
                    <div class="inner-box1">
                        <div class="text-inner">
                            <asp:Literal ID="litCountryName" runat="server" Text="<%$ Resources:Resource, ACCNO_OLD_ACCNO%>"></asp:Literal>
                            <span class="span_star">*</span><div class="space">
                            </div>
                            <asp:TextBox CssClass="text-box" runat="server" ID="txtAccountNo" MaxLength="20"
                                onblur="return TextBoxBlurValidationlbl(this,'spanAccountNo','Global Account No / Old Account No')"
                                placeholder="Enter Global Account No / Old Account No"></asp:TextBox>
                            &nbsp
                            <asp:FilteredTextBoxExtender ID="ftbAccNo" runat="server" TargetControlID="txtAccountNo"
                                FilterType="Numbers, Custom" ValidChars=".">
                            </asp:FilteredTextBoxExtender>
                            <asp:LinkButton runat="server" Visible="false" ID="lblSearch" OnClick="lblSearch_Click"
                                CssClass="search_img"></asp:LinkButton>
                            <br />
                            <span id="spanAccountNo" class="span_color"></span>
                        </div>
                    </div>
                </div>
                <div class="box_total">
                    <div class="box_total_a" style="margin-left: 15px;">
                        <asp:Button ID="btnGo" Text="<%$ Resources:Resource, GET_CUSTOMER_DETAILS%>" CssClass="box_s"
                            runat="server" OnClick="btnGo_Click" OnClientClick="return Validation();" />
                    </div>
                </div>
                <div class="clr">
                </div>
                <br />
                <div id="divCustomerDetails" runat="server" visible="false">
                    <div class="out-bor_a">
                        <div class="inner-sec">
                            <div class="heading" style="padding-left: 16px; padding-top: 17px;">
                                <asp:Literal ID="Literal213" runat="server" Text="<%$ Resources:Resource, CUSTOMER_DETAILS%>"></asp:Literal>
                            </div>
                            <div class="out-bor_aTwo">
                                <table class="customerdetailsTamle">
                                    <tr>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label runat="server" ID="lblGlobalAccNoAndAccNo"></asp:Label>
                                            <asp:Label runat="server" Visible="false" ID="lblAccountno"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, ROUTE_SEQUENCE_NO%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Literal ID="litRouteSequenceNo" runat="server"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label runat="server" ID="lblServiceAddress"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal3" runat="server" Text="Name"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label runat="server" ID="lblName"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, TOTAL_DUE%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label ID="lblTotalDueAmount" runat="server"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal6" runat="server" Text="Read Type"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Literal ID="lblReadType" runat="server" Text=""></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="litOldAccountNo" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Literal ID="lblOldAccountNo" runat="server" Text=""></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="inner-sec">
                            <div class="heading" style="padding-left: 21px; padding-top: 9px;">
                                <asp:Literal ID="Literal83" runat="server" Text="Adjustment Last Transactions"></asp:Literal>
                            </div>
                            <div id="divAdditionalChargesList" runat="server" class="grid_tb" style="padding-left: 26px;
                                padding-top: 10px;">
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <br />
                        <br />
                        <div id="divAssignMeter" runat="server" visible="false" style="width: auto;">
                            <div class="inner-box">
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litAgencyName" runat="server" Text="Credit Amount"></asp:Literal>
                                        </label>
                                        <div class="space">
                                        </div>
                                        <asp:TextBox CssClass="text-box" ID="txtCreditAmount" TabIndex="1" runat="server"
                                            onkeypress="return isNumberKey(event,this)" onkeyup="GetCurrencyFormate(this);"
                                            placeholder="Credit Amount" MaxLength="18"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtCreditAmount"
                                            FilterType="Numbers, Custom" ValidChars=",.">
                                        </asp:FilteredTextBoxExtender>
                                        <br />
                                        <span id="spnCreditAmount" runat="server" class="span_color"></span>
                                    </div>
                                    <div class="box_totalThree_a">
                                        <br />
                                        <asp:Button ID="btnSave" TabIndex="8" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                            CssClass="box_s" runat="server" OnClick="btnSave_Click" />
                                    </div>
                                </div>
                                <div class="inner-box2" style="width: 12%;">
                                    <div class="text-inner">
                                        <label for="name">
                                            &nbsp
                                        </label>
                                        <div class="space">
                                        </div>
                                        (OR)
                                    </div>
                                </div>
                                <div class="inner-box3">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal1" runat="server" Text="Debit Amount"></asp:Literal>
                                        </label>
                                        <div class="space">
                                        </div>
                                        <asp:TextBox CssClass="text-box" ID="txtDebitAmount" TabIndex="1" runat="server"
                                            onkeypress="return isNumberKey(event,this)" onkeyup="GetCurrencyFormate(this);"
                                            placeholder="Debit Amount" MaxLength="18"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender34" runat="server" TargetControlID="txtDebitAmount"
                                            FilterType="Numbers, Custom" ValidChars=",.">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spnDebitAmount" class="span_color"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:ModalPopupExtender ID="mpeConfirmSaveAdjustment" runat="server" TargetControlID="hfAdjsutment"
                    PopupControlID="pnlAdjsutment" BackgroundCssClass="popbg" CancelControlID="btnCancelAjst">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hfAdjsutment" runat="server"></asp:HiddenField>
                <asp:Panel runat="server" ID="pnlAdjsutment" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblAdjustmentText" runat="server" Text="Are you sure to save this adjustment?"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnCancelAjst" runat="server" Text="<%$ Resources:Resource, NO%>"
                                OnClick="btnAdjustmentConfirm_Click" CssClass="btn_ok" Style="margin-left: 10px;" />
                            <asp:Button ID="btnAdjustmentConfirm" runat="server" Text="<%$ Resources:Resource, OK%>"
                                OnClick="btnAdjustmentConfirm_Click" CssClass="btn_ok" Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeAlert" runat="server" TargetControlID="hdnCancelCNA"
                    PopupControlID="pnlAlert" BackgroundCssClass="popbg" CancelControlID="btnBPOK">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hdnCancelCNA" runat="server"></asp:HiddenField>
                <asp:Panel runat="server" ID="pnlAlert" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" id="popActive" runat="server" style="background-color: red;">
                        <asp:Label ID="lblAlertMsg" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnBPOK" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="btn_ok"
                                Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeAdjustmentDetails" runat="server" TargetControlID="hdnAdjustmentDetails"
                    PopupControlID="pnlAdjustmentDetails" BackgroundCssClass="popbg" CancelControlID="btnOk">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hdnAdjustmentDetails" runat="server"></asp:HiddenField>
                <asp:Panel runat="server" ID="pnlAdjustmentDetails" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblMeterDetails" runat="server" Text="Edit Adjustment"></asp:Label>
                    </div>
                    <div class="clear pad_10">
                    </div>
                    <div class="consumer_feild">
                        <div class="consume_name">
                            <asp:Literal ID="Literal13" runat="server" Text="Global Account No."></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblGlobalAccountNo" runat="server"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="Literal14" runat="server" Text="Credit Amount"></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:TextBox ID="txtAmoutCreditEdit" runat="server" CssClass="text-box" onkeypress="return isNumberKey(event,this)"
                                onkeyup="GetCurrencyFormate(this);" MaxLength="14"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtAmoutCreditEdit"
                                FilterType="Numbers, Custom" ValidChars=".,">
                            </asp:FilteredTextBoxExtender>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="Literal15" runat="server" Text="Debit Amount"></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:TextBox ID="txtAmountDebitEdit" runat="server" CssClass="text-box" onkeypress="return isNumberKey(event,this)"
                                onkeyup="GetCurrencyFormate(this);" MaxLength="14"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtAmountDebitEdit"
                                FilterType="Numbers, Custom" ValidChars=".,">
                            </asp:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btnOk" runat="server" Text="CANCEL" CssClass="btn_ok" Style="margin-left: 10px;" />
                            <asp:Button ID="btnEditAdjustment" runat="server" Text="SAVE" CssClass="btn_ok" OnClientClick="return ValidateEdit();"
                                OnClick="btnEditAdjustment_Click" Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeDeleteConfirmation" runat="server" TargetControlID="hdnDeleteConfirmation"
                    PopupControlID="pnlDeleteConfirmation" BackgroundCssClass="popbg" CancelControlID="Button3">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hdnDeleteConfirmation" runat="server"></asp:HiddenField>
                <asp:Panel runat="server" ID="pnlDeleteConfirmation" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="Label1" runat="server" Text="Are you sure to delete this adjustment?"></asp:Label>
                    </div>
                    <div class="clear pad_10">
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="Button3" runat="server" Text="NO" CssClass="btn_ok" Style="margin-left: 10px;" />
                            <asp:Button ID="btnDeleteAdjustment" runat="server" Text="YES" CssClass="btn_ok"
                                OnClick="btnDeleteAdjustment_Click" Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeAdjustmentOptions" runat="server" TargetControlID="hfAdjustmentOptions"
                    PopupControlID="pnlAdjustmentOptions" BackgroundCssClass="popbg" CancelControlID="btnAdjCancel">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hfAdjustmentOptions" runat="server"></asp:HiddenField>
                <asp:Panel runat="server" ID="pnlAdjustmentOptions" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblAdjustmentOption" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnAdjCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="btn_ok" Style="margin-left: 10px;" />
                            <asp:Button ID="btnDeleteConfirm" runat="server" OnClick="btnDeleteConfirm_Click"
                                Text="<%$ Resources:Resource, DELETE%>" CssClass="btn_ok" Style="margin-left: 10px;" />
                            <asp:Button ID="btnEditAdjustmentShow" runat="server" Text="<%$ Resources:Resource, EDIT%>"
                                CssClass="btn_ok" OnClick="btnEditAdjustmentShow_Click" Style="margin-left: 10px;" />
                        </div>
                    </div>
                    <div class="clear pad_5">
                    </div>
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="hfAdjsutmentLogId" runat="server" />
                <asp:HiddenField ID="hdnCustomerTypeID" runat="server" Value="" />
                <asp:HiddenField ID="hfAdjustmentID" runat="server" Value="" />
                <asp:HiddenField ID="hfnGlobalAccountNo" runat="server" Value="" />
                <asp:HiddenField ID="hfIsApproved" runat="server" Value="" />
                <asp:HiddenField ID="hfIsInProcess" runat="server" Value="" />
                <asp:HiddenField ID="hfIsNotNew" runat="server" Value="" />
                <asp:HiddenField ID="hfIsAdjustmentDone" runat="server" Value="" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <%-- Authentication --%>
        <uc1:Authentication ID="ucAuthentication" runat="server" />
        <%-- Authentication --%>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        //        $(document).keydown(function (e) {
        //            // ESCAPE key pressed 
        //            if (e.keyCode == 27) {
        //                $(".rnkland").fadeOut("slow");
        //                $(".mpeConfirmDelete").fadeOut("slow");
        //                document.getElementById("overlay").style.display = "none";
        //            }
        //        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        //        function pageLoad() {
        //            $get("<%=txtAccountNo.ClientID%>").focus();
        //        };


        $(document).ready(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
            // AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
            //AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);
        });
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../Javascript/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/Adjustment_NoBill.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
</asp:Content>
