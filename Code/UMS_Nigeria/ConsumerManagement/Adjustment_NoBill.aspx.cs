﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddRouteManagement
                     
 Developer        : ID077-NEERAJ KANOJIYA
 Creation Date    : 1-Apr-2015
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Xml;
using System.Data;
using Resources;
using UMS_Nigeria.UserControls;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class Adjustment_NoBill : System.Web.UI.Page
    {
        #region Members
        ReportsBal objReportsBal = new ReportsBal();
        iDsignBAL _ObjIdsignBal = new iDsignBAL();
        ConsumerBal objConsumerBal = new ConsumerBal();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        public string Key = "Adjustment_NoBill";
        string AccountNo;
        #endregion

        #region Methods
        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public void ClearFields()
        {
            hdnCustomerTypeID.Value = hfAdjustmentID.Value = hfnGlobalAccountNo.Value = lblAccountno.Text = lblName.Text = lblServiceAddress.Text = lblTotalDueAmount.Text = litRouteSequenceNo.Text = lblReadType.Text = txtCreditAmount.Text = txtDebitAmount.Text = txtAmountDebitEdit.Text = txtAmoutCreditEdit.Text = lblOldAccountNo.Text = string.Empty;
        }
        private void BindUserDetails()
        {
            ClearFields();
            #region New Code
            if (IsBillExists())
            {
                popActive.Attributes.Add("class", "popheader popheaderlblred");
                lblAlertMsg.Text = "This customer has bill. Adjustment (No Bill) not applicable here.";
                mpeAlert.Show();
            }
            else
            {
                BillAdjustmentsBe objBillAdjustmentsBe = IsAdjustmentDone();
                if (objBillAdjustmentsBe.IsInProcess == true && objBillAdjustmentsBe.IsNotNew == true)
                {
                    divCustomerDetails.Visible = false;
                    divAssignMeter.Visible = false;
                    btnDeleteConfirm.Visible = btnEditAdjustmentShow.Visible = false;
                    btnAdjCancel.Text = Resource.OK;
                    lblAdjustmentOption.Text = "Your previous Adjustment already is in process. You can't do any action.";
                    mpeAdjustmentOptions.Show();
                }
                else if (objBillAdjustmentsBe.IsInProcess == false && objBillAdjustmentsBe.IsNotNew == true)
                {
                    btnDeleteConfirm.Visible = btnEditAdjustmentShow.Visible = true;
                    btnAdjCancel.Text = Resource.CANCEL;
                    lblAdjustmentOption.Text = "Adjustment already done. You can do following action:";
                    mpeAdjustmentOptions.Show();
                }
                else
                {
                    #region try

                    CustomerRegistrationBE objCustomerRegistrationBE = new CustomerRegistrationBE();
                    XmlDocument xml = new XmlDocument();
                    try
                    {
                        objCustomerRegistrationBE.GolbalAccountNumber = txtAccountNo.Text;
                        xml = objConsumerBal.GetCustomerForAdjustmentNoBill(objCustomerRegistrationBE);
                        if (xml != null)
                        {
                            objCustomerRegistrationBE = _ObjIdsignBal.DeserializeFromXml<CustomerRegistrationBE>(xml);
                            if (objCustomerRegistrationBE.IsSuccessful)
                            {
                                hfnGlobalAccountNo.Value = objCustomerRegistrationBE.GlobalAccountNumber;
                                divCustomerDetails.Visible = divAssignMeter.Visible = true;
                                lblAccountno.Text = objCustomerRegistrationBE.AccountNo;
                                lblGlobalAccNoAndAccNo.Text=objCustomerRegistrationBE.GlobalAccNoAndAccNo;
                                lblServiceAddress.Text = objCustomerRegistrationBE.FullServiceAddress;
                                lblName.Text = objCustomerRegistrationBE.FirstNameLandlord;
                                //lblName.Text = objCustomerRegistrationBE.Name;
                                litRouteSequenceNo.Text = objCustomerRegistrationBE.RouteName;// Convert.ToString(objCustomerRegistrationBE.RouteSequenceNumber);
                                lblTotalDueAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(objCustomerRegistrationBE.OutStandingAmount.ToString()), 2, Constants.MILLION_Format).ToString(); //Raja-ID065 For Getting amt with comma seperated
                                hdnCustomerTypeID.Value = Convert.ToString(objCustomerRegistrationBE.CustomerTypeId);
                                lblOldAccountNo.Text = objCustomerRegistrationBE.OldAccountNo;
                                if (objCustomerRegistrationBE.ReadCodeID == (int)ReadCode.Read) lblReadType.Text = ReadCode.Read.ToString();
                                else if (objCustomerRegistrationBE.ReadCodeID == (int)ReadCode.Direct) lblReadType.Text = ReadCode.Direct.ToString();
                            }
                            else
                            {
                                divCustomerDetails.Visible = false;
                                divAssignMeter.Visible = false;
                                ClearFields();
                                Message("No customer found.", UMS_Resource.MESSAGETYPE_SUCCESS);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                        Message("Error in customer search.", UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    #endregion
                }
            }
            //else if (hfIsAdjustmentDone.Value == "False" && hfIsApproved.Value == "True" && hfIsInProcess.Value == "False" && hfIsNotNew.Value == "True")//0101
            //{
            //    divCustomerDetails.Visible = false;
            //    divAssignMeter.Visible = false;
            //    btnDeleteConfirm.Visible = btnEditAdjustmentShow.Visible = false;
            //    btnAdjCancel.Text = Resource.OK;
            //    lblAdjustmentOption.Text = "Adjustment already approved. You can't do any action.";
            //    mpeAdjustmentOptions.Show();
            //}
            //else if (hfIsAdjustmentDone.Value == "True" && hfIsApproved.Value == "False" && hfIsInProcess.Value == "True" && hfIsNotNew.Value == "True")//1011
            //{
            //    btnDeleteConfirm.Visible = btnEditAdjustmentShow.Visible = true;
            //    btnAdjCancel.Text = Resource.CANCEL;
            //    lblAdjustmentOption.Text = "Adjustment already done. You can do following action:";
            //    mpeAdjustmentOptions.Show();
            //}
            //else if (hfIsAdjustmentDone.Value == "True" && hfIsApproved.Value == "False" && hfIsInProcess.Value == "False" && hfIsNotNew.Value == "True")//1001
            //{
            //    divCustomerDetails.Visible = false;
            //    divAssignMeter.Visible = false;
            //    btnDeleteConfirm.Visible = btnEditAdjustmentShow.Visible = false;
            //    btnAdjCancel.Text = Resource.OK;
            //    lblAdjustmentOption.Text = "Your previous Adjustment already is in process. You can't do any action.";
            //    mpeAdjustmentOptions.Show();
            //}
            //else //if (hfIsAdjustmentDone.Value == "True" && hfIsApproved.Value == "False" && hfIsInProcess.Value == "False" && hfIsNotNew.Value == "False")//1000
            //{
            //    #region try

            //    CustomerRegistrationBE objCustomerRegistrationBE = new CustomerRegistrationBE();
            //    XmlDocument xml = new XmlDocument();
            //    try
            //    {
            //        objCustomerRegistrationBE.GolbalAccountNumber = txtAccountNo.Text;
            //        xml = objConsumerBal.GetCustomerForAdjustmentNoBill(objCustomerRegistrationBE);
            //        if (xml != null)
            //        {
            //            objCustomerRegistrationBE = _ObjIdsignBal.DeserializeFromXml<CustomerRegistrationBE>(xml);
            //            if (objCustomerRegistrationBE.IsSuccessful)
            //            {
            //                hfnGlobalAccountNo.Value = objCustomerRegistrationBE.GlobalAccountNumber;
            //                divCustomerDetails.Visible = divAssignMeter.Visible = true;
            //                lblAccountno.Text = objCustomerRegistrationBE.AccountNo;
            //                lblServiceAddress.Text = objCustomerRegistrationBE.FullServiceAddress;
            //                lblName.Text = objCustomerRegistrationBE.FirstNameLandlord;
            //                //lblName.Text = objCustomerRegistrationBE.Name;
            //                litRouteSequenceNo.Text = objCustomerRegistrationBE.RouteName;// Convert.ToString(objCustomerRegistrationBE.RouteSequenceNumber);
            //                lblTotalDueAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(objCustomerRegistrationBE.OutStandingAmount.ToString()), 2, Constants.MILLION_Format).ToString(); //Raja-ID065 For Getting amt with comma seperated
            //                hdnCustomerTypeID.Value = Convert.ToString(objCustomerRegistrationBE.CustomerTypeId);
            //                if (objCustomerRegistrationBE.ReadCodeID == (int)ReadCode.Read) lblReadType.Text = ReadCode.Read.ToString();
            //                else if (objCustomerRegistrationBE.ReadCodeID == (int)ReadCode.Direct) lblReadType.Text = ReadCode.Direct.ToString();
            //            }
            //            else
            //            {
            //                divCustomerDetails.Visible = false;
            //                divAssignMeter.Visible = false;
            //                ClearFields();
            //                Message("No customer found.", UMS_Resource.MESSAGETYPE_SUCCESS);
            //            }
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            //        Message("Error in customer search.", UMS_Resource.MESSAGETYPE_ERROR);
            //    }
            //    #endregion
            //}
            #endregion
            #region Old Code
            //if (!IsBillExists())
            //{
            //    if (!IsApproved())
            //    {
            //        if (!IsInProcess())
            //        {
            //            if (!IsAdjustmentDone())
            //            {
            //                #region try

            //                CustomerRegistrationBE objCustomerRegistrationBE = new CustomerRegistrationBE();
            //                XmlDocument xml = new XmlDocument();
            //                try
            //                {
            //                    objCustomerRegistrationBE.GolbalAccountNumber = txtAccountNo.Text;
            //                    xml = objConsumerBal.GetCustomerForAdjustmentNoBill(objCustomerRegistrationBE);
            //                    if (xml != null)
            //                    {
            //                        objCustomerRegistrationBE = _ObjIdsignBal.DeserializeFromXml<CustomerRegistrationBE>(xml);
            //                        if (objCustomerRegistrationBE.IsSuccessful)
            //                        {
            //                            hfnGlobalAccountNo.Value = objCustomerRegistrationBE.GlobalAccountNumber;
            //                            divCustomerDetails.Visible = divAssignMeter.Visible = true;
            //                            lblAccountno.Text = objCustomerRegistrationBE.AccountNo;
            //                            lblServiceAddress.Text = objCustomerRegistrationBE.FullServiceAddress;
            //                            lblName.Text = objCustomerRegistrationBE.FirstNameLandlord;
            //                            //lblName.Text = objCustomerRegistrationBE.Name;
            //                            litRouteSequenceNo.Text = objCustomerRegistrationBE.RouteName;// Convert.ToString(objCustomerRegistrationBE.RouteSequenceNumber);
            //                            lblTotalDueAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(objCustomerRegistrationBE.OutStandingAmount.ToString()), 2, Constants.MILLION_Format).ToString(); //Raja-ID065 For Getting amt with comma seperated
            //                            hdnCustomerTypeID.Value = Convert.ToString(objCustomerRegistrationBE.CustomerTypeId);
            //                            if (objCustomerRegistrationBE.ReadCodeID == (int)ReadCode.Read) lblReadType.Text = ReadCode.Read.ToString();
            //                            else if (objCustomerRegistrationBE.ReadCodeID == (int)ReadCode.Direct) lblReadType.Text = ReadCode.Direct.ToString();
            //                        }
            //                        else
            //                        {
            //                            divCustomerDetails.Visible = false;
            //                            divAssignMeter.Visible = false;
            //                            ClearFields();
            //                            Message("No customer found.", UMS_Resource.MESSAGETYPE_SUCCESS);
            //                        }
            //                    }
            //                }
            //                catch (Exception ex)
            //                {
            //                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            //                    Message("Error in customer search.", UMS_Resource.MESSAGETYPE_ERROR);
            //                }
            //                #endregion
            //            }
            //            else
            //            {
            //                btnDeleteConfirm.Visible = btnEditAdjustmentShow.Visible = true;
            //                btnAdjCancel.Text = Resource.CANCEL;
            //                lblAdjustmentOption.Text = "Adjustment already done. You can do following action:";
            //                mpeAdjustmentOptions.Show();
            //            }
            //        }
            //        else
            //        {
            //            divCustomerDetails.Visible = false;
            //            divAssignMeter.Visible = false;
            //            btnDeleteConfirm.Visible = btnEditAdjustmentShow.Visible = false;
            //            btnAdjCancel.Text = Resource.OK;
            //            lblAdjustmentOption.Text = "Your prevoius Adjustment already is in process. You can't do any action.";
            //            mpeAdjustmentOptions.Show();
            //        }
            //    }
            //    else
            //    {
            //        divCustomerDetails.Visible = false;
            //        divAssignMeter.Visible = false;
            //        btnDeleteConfirm.Visible = btnEditAdjustmentShow.Visible = false;
            //        btnAdjCancel.Text = Resource.OK;
            //        lblAdjustmentOption.Text = "Adjustment already approved. You can't do any action.";
            //        mpeAdjustmentOptions.Show();
            //    }
            //}
            //else
            //{
            //    popActive.Attributes.Add("class", "popheader popheaderlblred");
            //    lblAlertMsg.Text = "This customer has bill. Adjustment (No Bill) not applicable here.";
            //    mpeAlert.Show();
            //}
            #endregion
        }
        public void AddAdjustment()
        {
            try
            {
                if (hfnGlobalAccountNo.Value != "")
                {
                    if (string.IsNullOrEmpty(txtCreditAmount.Text)) txtCreditAmount.Text = "0";
                    if (string.IsNullOrEmpty(txtDebitAmount.Text)) txtDebitAmount.Text = "0";

                    if (decimal.Parse(txtCreditAmount.Text) == 0) txtCreditAmount.Text = string.Empty;
                    if (decimal.Parse(txtDebitAmount.Text) == 0) txtDebitAmount.Text = string.Empty;
                    decimal AmountEffected = 0;
                    if (!string.IsNullOrEmpty(txtCreditAmount.Text))
                    {
                        AmountEffected = Convert.ToDecimal(txtCreditAmount.Text.Replace(",", string.Empty));
                    }
                    else if (!string.IsNullOrEmpty(txtDebitAmount.Text))
                    {
                        AmountEffected = Convert.ToDecimal(txtDebitAmount.Text.Replace(",", string.Empty)) * -1;
                    }
                    if (AmountEffected != 0)
                    {
                        XmlDocument xml = new XmlDocument();
                        BillAdjustmentsBe _objBillAdjustmentsBe = new BillAdjustmentsBe();
                        BillAdjustmentBal _objBillAdjustmentBal = new BillAdjustmentBal();
                        _objBillAdjustmentsBe.AccountNo = lblAccountno.Text;
                        _objBillAdjustmentsBe.BillAdjustmentType = 1;//Bill Adjustment
                        _objBillAdjustmentsBe.AmountEffected = AmountEffected;
                        _objBillAdjustmentsBe.FunctionId = (int)EnumApprovalFunctions.BillAdjustment;
                        _objBillAdjustmentsBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                            _objBillAdjustmentsBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                        else
                            _objBillAdjustmentsBe.BU_ID = string.Empty;

                        if (_ObjCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.BillAdjustment, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString()))
                        {
                            _objBillAdjustmentsBe.ApprovalStatusId = (int)EnumApprovalStatus.Approved;
                            _objBillAdjustmentsBe.IsFinalApproval = true;
                            lblAlertMsg.Text = Resource.MODIFICATIONS_UPDATED;
                        }
                        else
                        {
                            _objBillAdjustmentsBe.ApprovalStatusId = (int)EnumApprovalStatus.Process;
                            lblAlertMsg.Text = Resource.APPROVAL_TARIFF_CHANGE;
                        }

                        xml = _objBillAdjustmentBal.AdjustmentForNoBill(_objBillAdjustmentsBe, AdjustmentNoBill.AddAdjustment);
                        _objBillAdjustmentsBe = _ObjIdsignBal.DeserializeFromXml<BillAdjustmentsBe>(xml);

                        if (_objBillAdjustmentsBe.IsSuccess)
                        {
                            popActive.Attributes.Add("class", "popheader popheaderlblgreen");
                            divAssignMeter.Visible = divCustomerDetails.Visible = false;
                            //Message(Resource.BILL_ADJUSTMENT_COMPLETED, UMS_Resource.MESSAGETYPE_SUCCESS);
                            ClearFields();
                            txtAccountNo.Text = string.Empty;
                            mpeAlert.Show();
                        }
                        else if (_objBillAdjustmentsBe.IsApprovalInProcess)
                        {
                            popActive.Attributes.Add("class", "popheader popheaderlblred");
                            divAssignMeter.Visible = divCustomerDetails.Visible = false;
                            ClearFields();
                            txtAccountNo.Text = string.Empty;
                            lblAlertMsg.Text = Resource.REQ_STILL_PENDING;
                            mpeAlert.Show();
                        }
                        else
                        {
                            divAssignMeter.Visible = divCustomerDetails.Visible = false;
                            ClearFields();
                            txtAccountNo.Text = string.Empty;
                            Message(Resource.ERROR_IN_ADJSTMNT, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                    }
                    else
                    {
                        Message("Error in input.", UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
                else
                {
                    Message(Resource.ERROR_IN_ADJSTMNT, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        public bool IsBillExists()
        {
            try
            {
                XmlDocument xml = new XmlDocument();
                BillAdjustmentsBe _objBillAdjustmentsBe = new BillAdjustmentsBe();
                BillAdjustmentBal _objBillAdjustmentBal = new BillAdjustmentBal();
                _objBillAdjustmentsBe.AccountNo = txtAccountNo.Text;
                xml = _objBillAdjustmentBal.AdjustmentForNoBill(_objBillAdjustmentsBe, AdjustmentNoBill.IsBillExists);

                _objBillAdjustmentsBe = _ObjIdsignBal.DeserializeFromXml<BillAdjustmentsBe>(xml);

                if (_objBillAdjustmentsBe.IsSuccess) return true;
                else return false;

            }
            catch (Exception ex)
            {
                Message("Error in Bill Checking.", UMS_Resource.MESSAGETYPE_ERROR);
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                return false;
            }
        }
        public BillAdjustmentsBe IsAdjustmentDone()
        {
            BillAdjustmentsBe _objBillAdjustmentsBe = new BillAdjustmentsBe();
            try
            {
                XmlDocument xml = new XmlDocument();
                BillAdjustmentBal _objBillAdjustmentBal = new BillAdjustmentBal();
                _objBillAdjustmentsBe.AccountNo = txtAccountNo.Text;
                xml = _objBillAdjustmentBal.AdjustmentForNoBill(_objBillAdjustmentsBe, AdjustmentNoBill.IsAdjustmentDone);

                _objBillAdjustmentsBe = _ObjIdsignBal.DeserializeFromXml<BillAdjustmentsBe>(xml);
                hfAdjsutmentLogId.Value = _objBillAdjustmentsBe.AdjustmentLogId.ToString();
                //hfIsApproved.Value = _objBillAdjustmentsBe.IsAllReadyApproved.ToString();
                //hfIsInProcess.Value = _objBillAdjustmentsBe.IsInProcess.ToString();
                //hfIsAdjustmentDone.Value = _objBillAdjustmentsBe.IsSuccess.ToString();
                //hfIsNotNew.Value = _objBillAdjustmentsBe.IsNotNew.ToString();
                return _objBillAdjustmentsBe;
            }
            catch (Exception ex)
            {
                Message("Error in Bill Checking.", UMS_Resource.MESSAGETYPE_ERROR);
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                return _objBillAdjustmentsBe;
            }
        }
        public void UpdateAdjustment()
        {

            try
            {
                decimal AmountEffected = 0;
                if (string.IsNullOrEmpty(txtAmoutCreditEdit.Text)) txtAmoutCreditEdit.Text = "0";
                if (string.IsNullOrEmpty(txtAmountDebitEdit.Text)) txtAmountDebitEdit.Text = "0";

                if (decimal.Parse(txtAmoutCreditEdit.Text) == 0) txtAmoutCreditEdit.Text = string.Empty;
                if (decimal.Parse(txtAmountDebitEdit.Text) == 0) txtAmountDebitEdit.Text = string.Empty;
                if (!string.IsNullOrEmpty(txtAmoutCreditEdit.Text))
                {
                    AmountEffected = Convert.ToDecimal(txtAmoutCreditEdit.Text.Replace(",", string.Empty));
                }
                else if (!string.IsNullOrEmpty(txtAmountDebitEdit.Text))
                {
                    AmountEffected = Convert.ToDecimal(txtAmountDebitEdit.Text.Replace(",", string.Empty)) * -1;
                }
                if (AmountEffected != 0)
                {
                    XmlDocument xml = new XmlDocument();

                    BillAdjustmentsBe _objBillAdjustmentsBe = new BillAdjustmentsBe();
                    BillAdjustmentBal _objBillAdjustmentBal = new BillAdjustmentBal();
                    _objBillAdjustmentsBe.AmountEffected = AmountEffected;
                    //_objBillAdjustmentsBe.BillAdjustmentId = Convert.ToInt32(hfAdjustmentID.Value.Replace(",", ""));
                    _objBillAdjustmentsBe.AdjustmentLogId = Convert.ToInt32(hfAdjsutmentLogId.Value.Replace(",", ""));
                    _objBillAdjustmentsBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    xml = _objBillAdjustmentBal.AdjustmentForNoBill(_objBillAdjustmentsBe, AdjustmentNoBill.UpdateAdjustment);

                    _objBillAdjustmentsBe = _ObjIdsignBal.DeserializeFromXml<BillAdjustmentsBe>(xml);

                    if (_objBillAdjustmentsBe.IsSuccess)
                    {
                        _objBillAdjustmentsBe.ApprovalStatusId = (int)EnumApprovalStatus.Process;
                        lblAlertMsg.Text = Resource.APPROVAL_TARIFF_CHANGE;
                        popActive.Attributes.Add("class", "popheader popheaderlblgreen");
                        divAssignMeter.Visible = divCustomerDetails.Visible = false;
                        //Message(Resource.BILL_ADJUSTMENT_COMPLETED, UMS_Resource.MESSAGETYPE_SUCCESS);
                        ClearFields();
                        txtAccountNo.Text = string.Empty;
                        mpeAlert.Show();
                        //divAssignMeter.Visible = divCustomerDetails.Visible = false;
                        //Message("Adjustment Successfully Updated.", UMS_Resource.MESSAGETYPE_SUCCESS);
                        //ClearFields();
                        //txtAccountNo.Text = string.Empty;
                    }
                    else
                    {
                        Message("Error in adjustment update.", UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
                else
                {
                    Message("Error in input.", UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }
        public void DeleteAdjustment()
        {
            try
            {
                XmlDocument xml = new XmlDocument();

                BillAdjustmentsBe _objBillAdjustmentsBe = new BillAdjustmentsBe();
                BillAdjustmentBal _objBillAdjustmentBal = new BillAdjustmentBal();
                _objBillAdjustmentsBe.AccountNo = txtAccountNo.Text;
                _objBillAdjustmentsBe.AdjustmentLogId = Convert.ToInt32(hfAdjsutmentLogId.Value.Replace(",", ""));
                xml = _objBillAdjustmentBal.AdjustmentForNoBill(_objBillAdjustmentsBe, AdjustmentNoBill.DeleteAdjustment);

                _objBillAdjustmentsBe = _ObjIdsignBal.DeserializeFromXml<BillAdjustmentsBe>(xml);

                if (_objBillAdjustmentsBe.IsSuccess)
                {
                    divAssignMeter.Visible = divCustomerDetails.Visible = false;
                    Message("Adjustment deleted successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                    ClearFields();
                }
                else
                {
                    Message("Error in adjustment deletion.", UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        public void GetAdjsutmentDetails()
        {
            try
            {
                XmlDocument xml = new XmlDocument();

                BillAdjustmentsBe _objBillAdjustmentsBe = new BillAdjustmentsBe();
                BillAdjustmentBal _objBillAdjustmentBal = new BillAdjustmentBal();
                _objBillAdjustmentsBe.AccountNo = txtAccountNo.Text;
                xml = _objBillAdjustmentBal.AdjustmentForNoBill(_objBillAdjustmentsBe, AdjustmentNoBill.GetAdjsutmentDetails);

                _objBillAdjustmentsBe = _ObjIdsignBal.DeserializeFromXml<BillAdjustmentsBe>(xml);

                if (_objBillAdjustmentsBe.IsSuccess)
                {
                    hfAdjustmentID.Value = Convert.ToString(_objBillAdjustmentsBe.BillAdjustmentId);
                    lblGlobalAccountNo.Text = _objBillAdjustmentsBe.AccountNo;
                    hfAdjsutmentLogId.Value = _objBillAdjustmentsBe.AdjustmentLogId.ToString();
                    if (_objBillAdjustmentsBe.AmountEffected < 0)
                        txtAmountDebitEdit.Text = _ObjCommonMethods.GetCurrencyFormat(_objBillAdjustmentsBe.AmountEffected, 2, Constants.MILLION_Format);
                    else if (_objBillAdjustmentsBe.AmountEffected > 0)
                        txtAmoutCreditEdit.Text = _ObjCommonMethods.GetCurrencyFormat(_objBillAdjustmentsBe.AmountEffected, 2, Constants.MILLION_Format);
                    mpeAdjustmentDetails.Show();
                }
                else
                {
                    Message("Error in adjustment details.", UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void GetAdjustmentLastTransactions()
        {
            UC_AdditionalChargesLastTransaction objAdChargTrans = (UC_AdditionalChargesLastTransaction)LoadControl("../UserControls/UC_AdditionalChargesLastTransaction.ascx");
            objAdChargTrans.AccountNo = txtAccountNo.Text;
            objAdChargTrans.GetAdjustmentLastTransactions();            
            divAdditionalChargesList.Controls.Add(objAdChargTrans);
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString[UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO]))
                {
                    string AccountNo = _ObjIdsignBal.Decrypt(Request.QueryString[UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO]).ToString();
                    txtAccountNo.Text = AccountNo;
                }
            }
            GetAdjustmentLastTransactions();
        }

        protected void lblSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADVANCE_SEARCH_PAGE + "?" + UMS_Resource.FROM_PAGE + "=" + _ObjIdsignBal.Encrypt(Constants.CustomerBillPayment), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
            objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                objLedgerBe.BUID = string.Empty;
           
            //objLedgerBe = _ObjIdsignBal.DeserializeFromXml<RptCustomerLedgerBe>(objReportsBal.GetLedger(objLedgerBe, ReturnType.Multiple));
            objLedgerBe = _ObjIdsignBal.DeserializeFromXml<RptCustomerLedgerBe>(objReportsBal.GetLedger(objLedgerBe, ReturnType.Single));

            if (objLedgerBe.IsSuccess)
            {
                if (objLedgerBe.IsCustExistsInOtherBU)
                {
                    popActive.Attributes.Add("class", "popheader popheaderlblred");
                    lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                    mpeAlert.Show();
                    divAssignMeter.Visible = false;
                }
                else if (objLedgerBe.ActiveStatusId == 4)
                {
                    popActive.Attributes.Add("class", "popheader popheaderlblred");
                    lblAlertMsg.Text = "Customer is not active";
                    mpeAlert.Show();
                    divAssignMeter.Visible = false;

                    _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, "Customer status is Closed", pnlMessage, lblMessage);
                }
                else if (objLedgerBe.CustomerTypeId == 3)//check the customer is prepaid or not
                {
                    popActive.Attributes.Add("class", "popheader popheaderlblred");
                    lblAlertMsg.Text = "Customer is Prepaid customer.";
                    mpeAlert.Show();
                    divAssignMeter.Visible = false;
                }
                else
                {
                    BindUserDetails();
                }
            }
            else
            {
                divCustomerDetails.Visible = false;
                divAssignMeter.Visible = false;
                ClearFields();
                _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.CUSTOMER_NOT_FOUND_MSG, pnlMessage, lblMessage);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            mpeConfirmSaveAdjustment.Show();
        }

        protected void btnDeleteConfirm_Click(object sender, EventArgs e)
        {
            mpeDeleteConfirmation.Show();
        }

        protected void btnDeleteAdjustment_Click(object sender, EventArgs e)
        {
            DeleteAdjustment();
        }

        protected void btnEditAdjustmentShow_Click(object sender, EventArgs e)
        {
            GetAdjsutmentDetails();
        }

        protected void btnEditAdjustment_Click(object sender, EventArgs e)
        {
            UpdateAdjustment();
        }

        protected void btnAdjustmentConfirm_Click(object sender, EventArgs e)
        {
            AddAdjustment();
        }
        #endregion
    }
}
