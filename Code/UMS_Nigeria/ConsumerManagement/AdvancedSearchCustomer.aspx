﻿<%@ Page Title="::Advance Search::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    AutoEventWireup="true" CodeBehind="AdvancedSearchCustomer.aspx.cs" Theme="Green"
    ClientIDMode="Predictable" Inherits="UMS_Nigeria.ConsumerManagement.AdvancedSearchCustomer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%-- AutomComplete Textbox supported Files--%>
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:Panel ID="pnlAdvanceSerach" DefaultButton="btnSearch" runat="server">
        <asp:UpdatePanel ID="upSearchCustomer" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="out-bor">
            <div class="inner-sec">
                <div class="text_total">
                    <div class="text-heading">
                        <asp:Literal ID="litSearchCustomer" runat="server" Text="<%$ Resources:Resource, SEARCH_CUSTOMER%>"></asp:Literal>
                    </div>
                    <div class="star_text">
                        &nbsp<%--<asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>--%>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div class="dot-line">
                </div>
                <div class="previous">
                    <asp:LinkButton ID="lbtnBack" runat="server" Text='<%$ Resources:Resource, BACK %>'
                        OnClick="btnBack_Click"></asp:LinkButton>
                </div>
                <div class="clr">
                </div>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <%--<div class="clear pad_5 fltl">
                            <asp:Button ID="btnBack" Text="<%$ Resources:Resource, GO_TO_BACK%>" CssClass="back_but"
                                runat="server" OnClick="btnBack_Click" />
                        </div>
                        <div class="clear">
                            &nbsp;</div>--%>
                        <div class="inner-box">
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                                    </label>
                                    <br />
                                    <asp:TextBox ID="txtName" TabIndex="1" CssClass="text-box" runat="server" MaxLength="50"
                                        placeholder="Name of the Customer"></asp:TextBox>
                                    <%--<asp:FilteredTextBoxExtender ID="ftbPrimaryEmail" runat="server" TargetControlID="txtName"
                                        ValidChars="@ ." FilterType="LowercaseLetters,UppercaseLetters,Custom">
                                    </asp:FilteredTextBoxExtender>--%>
                                </div>
                            </div>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                                    </label>
                                    <br />
                                    <asp:TextBox ID="txtAccNo" TabIndex="2" CssClass="text-box" MaxLength="10" placeholder="Enter Global Account No."
                                        runat="server"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtAccNo"
                                        FilterType="Numbers" ValidChars=" ">
                                    </asp:FilteredTextBoxExtender>
                                </div>
                            </div>
                            <div class="inner-box3">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                    </label>
                                    <br />
                                    <asp:TextBox ID="txtOldAccountNo" TabIndex="3" CssClass="text-box" placeholder="Enter Old Acc No"
                                        runat="server"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="ftTxtOldAccountNo" runat="server" TargetControlID="txtOldAccountNo"
                                        FilterType="Numbers">
                                    </asp:FilteredTextBoxExtender>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Literal>
                                    </label>
                                    <br />
                                    <asp:TextBox ID="txtMeterNo" TabIndex="4" CssClass="text-box" runat="server" MaxLength="20"
                                        placeholder="Meter No"></asp:TextBox>
                                </div>
                            </div>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT_NAME%>"></asp:Literal>
                                    </label>
                                    <br />
                                    <asp:DropDownList ID="ddlBU" TabIndex="5" runat="server" OnSelectedIndexChanged="ddlBU_SelectedIndexChanged"
                                        AutoPostBack="true" CssClass="text-box text-select">
                                        <asp:ListItem Value="" Text="All"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="inner-box3">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT_NAME%>"></asp:Literal>
                                    </label>
                                    <br />
                                    <asp:DropDownList ID="ddlSU" TabIndex="6" OnSelectedIndexChanged="ddlSU_SelectedIndexChanged"
                                        AutoPostBack="true" runat="server" CssClass="text-box text-select">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER_NAME%>"></asp:Literal>
                                    </label>
                                    <br />
                                    <asp:DropDownList ID="ddlSC" TabIndex="7" AutoPostBack="true" OnSelectedIndexChanged="ddlSC_SelectedIndexChanged"
                                        runat="server" CssClass="text-box text-select">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal>
                                    </label>
                                    <br />
                                    <asp:DropDownList ID="ddlCycle" TabIndex="8" runat="server" CssClass="text-box text-select">
                                        <asp:ListItem Text="All"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="inner-box3">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                                    </label>
                                    <br />
                                    <asp:DropDownList ID="ddlTariff" TabIndex="9" runat="server" CssClass="text-box text-select">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litReadCode" runat="server" Text="<%$ Resources:Resource, READCODE%>"></asp:Literal>
                                    </label>
                                    <br />
                                    <asp:DropDownList ID="ddlReadCode" TabIndex="10" runat="server" CssClass="text-box text-select">
                                        <%--AutoPostBack="true" OnSelectedIndexChanged="ddlReadCode_SelectedIndexChanged"--%>
                                        <asp:ListItem Text="All" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, STATUS%>"></asp:Literal>
                                    </label>
                                    <br />
                                    <asp:DropDownList ID="ddlStatus" TabIndex="11" runat="server" CssClass="text-box text-select">
                                        <asp:ListItem Text="All" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="space">
                                </div>
                            </div>
                            <div class="inner-box3">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:Resource, CONTACT_NO%>"></asp:Literal>
                                    </label>
                                    <br />
                                    <asp:TextBox ID="txtCode1" TabIndex="12" CssClass="text-box" runat="server" Width="11%"
                                        MaxLength="3" placeholder="Code"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtCode1"
                                        FilterType="Numbers" ValidChars=" ">
                                    </asp:FilteredTextBoxExtender>
                                    <asp:TextBox ID="txtMobileNum" TabIndex="13" CssClass="text-box" runat="server" Width="40%"
                                        MaxLength="10" placeholder="Contact No"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtMobileNum"
                                        FilterType="Numbers" ValidChars=" ">
                                    </asp:FilteredTextBoxExtender>
                                </div>
                            </div>
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div class="box_total">
                            <div class="box_total_a">
                                <asp:Button ID="btnSearch" TabIndex="14" Text="<%$ Resources:Resource, SEARCH%>"
                                    ToolTip="Search Customer" CssClass="box_s" runat="server" OnClick="btnSearch_Click" />
                                <asp:Button ID="btnReset" TabIndex="14" Text="<%$ Resources:Resource, RESET%>" ToolTip="Search Customer"
                                    CssClass="box_s" runat="server" OnClick="btnReset_Click" />
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div style="clear: both;">
                </div>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="grid_boxes">
                            <div class="grid_paging_top">
                                <div class="paging_top_title">
                                    <asp:Literal ID="litSearchList" runat="server" Text="<%$ Resources:Resource, SEARCH_RESULTS%>"></asp:Literal>
                                </div>
                                <div class="paging_top_right_content" id="div1" runat="server">
                                    <div id="divgrid" class="grid marg_5" runat="server" style="width: 99% !important;
                                        float: left;">
                                        <div id="divpaging" runat="server">
                                            <div align="right" class="marg_20t" runat="server" id="divLinks" visible="false">
                                                <div class=" countri_paging">
                                                    <div align="right" style="color: Red; padding-left: 70px; margin-top: 1px;">
                                                        <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:Resource, TOTALCUSTOMERS%>"></asp:Literal>
                                                        :
                                                        <asp:Label ID="lblCountRecords" runat="server" Text=""></asp:Label>
                                                    </div>
                                                    <br />
                                                    <asp:LinkButton ID="lkbtnFirst" runat="server" CssClass="pagingfirst" OnClick="lkbtnFirst_Click">
                                                        <asp:Literal ID="litFirst" runat="server" Text="<%$ Resources:Resource, FIRST%>"></asp:Literal></asp:LinkButton>
                                                    &nbsp;|
                                                    <asp:LinkButton ID="lkbtnPrevious" runat="server" CssClass="pagingfirst" OnClick="lkbtnPrevious_Click">
                                                        <asp:Literal ID="litPrevious" runat="server" Text="<%$ Resources:Resource, PREVIOUS%>"></asp:Literal></asp:LinkButton>
                                                    &nbsp;|
                                                    <asp:Label ID="lblCurrentPage" runat="server" Font-Bold="true"></asp:Label>
                                                    &nbsp;|
                                                    <asp:LinkButton ID="lkbtnNext" runat="server" CssClass="pagingfirst" OnClick="lkbtnNext_Click">
                                                        <asp:Literal ID="litNext" runat="server" Text="<%$ Resources:Resource, NEXT%>"></asp:Literal></asp:LinkButton>
                                                    &nbsp;|
                                                    <asp:LinkButton ID="lkbtnLast" runat="server" CssClass="pagingfirst" OnClick="lkbtnLast_Click">
                                                        <asp:Literal ID="litLast" runat="server" Text="<%$ Resources:Resource, LAST%>"></asp:Literal></asp:LinkButton>
                                                    &nbsp;|<asp:Literal ID="litPageSize" runat="server" Text="<%$ Resources:Resource, PAGE_SIZE%>"></asp:Literal>
                                                    <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" CssClass="paging-size"
                                                        OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                                        <asp:ListItem>100</asp:ListItem>
                                                        <asp:ListItem>200</asp:ListItem>
                                                        <asp:ListItem>300</asp:ListItem>
                                                        <asp:ListItem>400</asp:ListItem>
                                                        <asp:ListItem>500</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid_tb" id="divPrint" runat="server">
                            <asp:GridView ID="gvSearchCustomers" runat="server" OnRowCommand="gvSearchCustomers_RowCommand"
                                OnRowDataBound="gvSearchCustomers_RowDataBound" AutoGenerateColumns="False" HeaderStyle-CssClass="grid_tb_hd ">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No Results Found for given search options.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>' EnableViewState="false"></asp:Label>
                                            <asp:Label ID="lblIsBookNoChanged" Visible="false" runat="server" Text='<%#Eval("IsBookNoChanged")%>'
                                                EnableViewState="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" runat="server"
                                        HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <%-- <%#Container.DataItemIndex+1%>--%>
                                            <asp:LinkButton ID="lnkAccNoAndGlobalAccNo" Enabled="false" Style="color: #000" runat="server"
                                                CommandName="Redirect" CommandArgument='<%#Eval("GlobalAccountNumber")%>' Text='<%#Eval("AccNoAndGlobalAccNo")%>'></asp:LinkButton>
                                            <asp:LinkButton ID="lbtnAccNumber" Visible="false" Enabled="false" Style="color: #000"
                                                runat="server" CommandName="Redirect" Text='<%#Eval("GlobalAccountNumber")%>'></asp:LinkButton>
                                            <asp:Label ID="spanBookNoChanged" Style="color: Red" Visible="false" runat="server"
                                                EnableViewState="false" Text="*"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="false" HeaderText="<%$ Resources:Resource, ACC_NO%>"
                                        ItemStyle-Width="8%" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGvAccNo" runat="server" EnableViewState="false" Text='<%#Eval("AccountNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-Width="8%"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGvOldAccNo" runat="server" EnableViewState="false" Text='<%#Eval("OldAccountNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, METER_NO%>" ItemStyle-Width="8%"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGvMeterNo" runat="server" EnableViewState="false" Text='<%#Eval("MeterNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="false" HeaderText="<%$ Resources:Resource, CUSTOMER_UNIQUE_NO%>"
                                        ItemStyle-Width="8%" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGvCustUnique" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-Width="8%"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGvName" runat="server" EnableViewState="false" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, CONTACT_NO%>" ItemStyle-Width="8%"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <div>
                                                <asp:Label ID="lblGvContact" runat="server" EnableViewState="false" Text='<%#Eval("HomeContactNumber") %>'></asp:Label><br />
                                                <asp:Label ID="lblGvBusinessContactNumber" runat="server" EnableViewState="false"
                                                    Text='<%#Eval("BusinessContactNumber") %>'></asp:Label><br />
                                                <asp:Label ID="lblGvOtherContactNumber" runat="server" EnableViewState="false" Text='<%#Eval("OtherContactNumber") %>'></asp:Label><br />
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="<%$ Resources:Resource, BUSINESS_UNIT_NAME%>" ItemStyle-Width="10%"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGvBU" runat="server" Text='<%#Eval("BusinessUnitName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, SERVICE_UNIT_NAME%>" ItemStyle-Width="10%"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGvSU" runat="server" Text='<%#Eval("ServiceUnitName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, SERVICE_ADDRESS%>" ItemStyle-Width="10%"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGvAddress" runat="server" EnableViewState="false" Text='<%#Eval("ServiceAddress") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, SERVICE_CENTER_NAME%>" ItemStyle-Width="10%"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGvSC" runat="server" EnableViewState="false" Text='<%#Eval("ServiceCenterName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, TARIFF%>" ItemStyle-Width="10%"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGvTariff" runat="server" EnableViewState="false" Text='<%#Eval("ClassName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%-- <asp:TemplateField HeaderText="<%$ Resources:Resource, LASTBILL%>" ItemStyle-Width="10%"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGvLastBill" runat="server" Text="-"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, STATUS%>" ItemStyle-Width="10%"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGvStatus" runat="server" EnableViewState="false" Text='<%#Eval("StatusName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PROFILE" ItemStyle-Width="8%" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkProfile" ToolTip="View Customer Details" runat="server" Text="View"></asp:LinkButton>
                                            <%--     <asp:Label ID="lblGvCustUnique" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="<%$ Resources:Resource, READCODE%>" ItemStyle-Width="10%"
                                        ItemStyle-HorizontalAlign="Center" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGvReadCodeId" runat="server" Text='<%#Eval("ReadCodeId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="10%" HeaderText="<%$ Resources:Resource, ATTACHMENTS%>"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Repeater runat="server" ID="rptrDocuments" OnItemDataBound="rptrDocuments_ItemDataBound">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lkDownload" runat="server" Text="<%$ Resources:Resource, DOWNLOAD%>"></asp:LinkButton><br />
                                                    <asp:LinkButton ID="lbtnDocName" ForeColor="Green" runat="server" OnClick="lbtnDocName_Click"
                                                        CommandArgument='<%#Eval("CustomerDocumentNames") %>' Text='<%#" Download "+(Container.ItemIndex + 1)%>'></asp:LinkButton>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Label runat="server" Visible="false" ID="lblDocName" Text='--'></asp:Label>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                        <asp:HiddenField ID="hfSearchString" runat="server" />
                        <asp:HiddenField ID="hfPageSize" runat="server" />
                        <asp:HiddenField ID="hfPageNo" runat="server" Value="1" />
                        <asp:HiddenField ID="hfLastPage" runat="server" />
                        <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                        <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="PanelAlert"
                            TargetControlID="btnAlertDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnAlertOk">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnAlertDeActivate" runat="server" Text="Button" CssClass="popbtn" />
                        <asp:Panel ID="PanelAlert" runat="server" CssClass="modalPopup" class="poppnl" Style="display: none;">
                            <div class="popheader popheaderlblred" id="pop" runat="server">
                                <asp:Label ID="lblAlertMsg" runat="server"></asp:Label>
                            </div>
                            <div class="footer popfooterbtnrt">
                                <div class="fltr popfooterbtn">
                                    <br />
                                    <asp:Button ID="btnAlertOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                        <asp:PostBackTrigger ControlID="ddlReadCode" />
                        <asp:PostBackTrigger ControlID="btnReset" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </asp:Panel>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).ready(function () {
//            AutoComplete($('#<%=txtAccNo.ClientID %>'), 1);
//            AutoComplete($('#<%=txtMeterNo.ClientID %>'), 2);
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
//            AutoComplete($('#<%=txtAccNo.ClientID %>'), 1);
//            AutoComplete($('#<%=txtMeterNo.ClientID %>'), 2);
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Javascript/jquery.min.js" type="text/javascript"></script>
    <script src="../Javascript/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
