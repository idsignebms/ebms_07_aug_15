﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using Resources;
using UMS_NigriaDAL;
using System.Data;
using iDSignHelper;
using UMS_NigeriaBAL;
using System.Drawing;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Globalization;
using System.Xml;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class AdvancedSearchCustomer : System.Web.UI.Page
    {
        #region Members
        CommonMethods _ObjCommonMethods = new CommonMethods();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonDAL _ObjCommonDAL = new CommonDAL();
        ConsumerBe _objConsumerBe = new ConsumerBe();
        ConsumerBal _objCoonsumerBAl = new ConsumerBal();
        ConsumerBal objConsumerBal = new ConsumerBal();
        ReportsBal objReportsBal = new ReportsBal();
        int PageNum;
        string Key = UMS_Resource.ADVANCEDSEARCHCUSTOMER;
        #endregion

        #region Properties

        public int PageSize
        {
            get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.SearchCustPageSize : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        public string BusinessUnit
        {
            get { return string.IsNullOrEmpty(ddlBU.SelectedValue) ? string.Empty : ddlBU.SelectedValue; }
            set { ddlBU.SelectedValue = value.ToString(); }
        }
        public string ServiceUnit
        {
            get { return string.IsNullOrEmpty(ddlSU.SelectedValue) ? _ObjCommonMethods.CollectAllValues(ddlSU, ",") : ddlSU.SelectedValue; }
            set { ddlSU.SelectedValue = value.ToString(); }
        }
        public string ServiceCenter
        {
            get { return string.IsNullOrEmpty(ddlSC.SelectedValue) ? _ObjCommonMethods.CollectAllValues(ddlSC, ",") : ddlSC.SelectedValue; }
            set { ddlSC.SelectedValue = value.ToString(); }
        }

        public int Status
        {
            get { return string.IsNullOrEmpty(ddlStatus.SelectedValue) ? Constants.Zero : Convert.ToInt32(ddlStatus.SelectedValue); }
            set { ddlStatus.SelectedValue = value.ToString(); }
        }

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            try
            {
                if (Session[UMS_Resource.SESSION_LOGINID] == null)
                    Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, "Page_PreInit", ErrorLog.LogType.Error, ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = pnlMessage.CssClass = string.Empty;
                if (!IsPostBack)
                {
                    string path = string.Empty;
                    path = _ObjCommonMethods.GetPagePath(this.Request.Url);
                    if (_ObjCommonMethods.IsAccess(path))
                    {
                        hfSearchString.Value = string.Empty;
                        BindDropDowns();
                        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        {
                            ddlBU.SelectedIndex = ddlBU.Items.IndexOf(ddlBU.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                            ddlBU.Enabled = false;
                            ddlBU_SelectedIndexChanged(ddlBU, new EventArgs());
                            ddlSU_SelectedIndexChanged(null, new EventArgs());
                            ddlSC_SelectedIndexChanged(null, new EventArgs());
                        }
                        GetQueryStringData();
                        hfPageNo.Value = Constants.pageNoValue.ToString();
                        //bindSearch(string.Empty);
                        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    }
                    else
                    {
                        Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE, false);
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, "Page_Load", ErrorLog.LogType.Error, ex);
            }
        }


        private void GetQueryStringData()
        {
            try
            {
                if (Request.QueryString[UMS_Resource.FROM_PAGE] != null)
                {
                    string fromPage = _ObjiDsignBAL.Decrypt(Request.QueryString[UMS_Resource.FROM_PAGE].ToString());
                    switch (fromPage)
                    {
                        case Constants.MeterReadingPageName:
                            ddlReadCode.SelectedIndex = ddlReadCode.Items.IndexOf(ddlReadCode.Items.FindByValue(((int)ReadCode.Read).ToString()));
                            ddlReadCode.Enabled = false;
                            break;
                    }
                }
                if (Request.QueryString[UMS_Resource.QSTR_ACC_NO] != null)
                {
                    txtAccNo.Text = _ObjiDsignBAL.Decrypt(Request.QueryString[UMS_Resource.QSTR_ACC_NO].ToString());
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, "GetQueryStringData", ErrorLog.LogType.Error, ex);
            }
        }

        private void BindDropDowns()
        {
            try
            {
                //_ObjCommonMethods.BindDistricts(ddlDistricts, string.Empty, true);
                _ObjCommonMethods.BindBusinessUnits(ddlBU, string.Empty, true);
                _ObjCommonMethods.BindServiceCenters(ddlSC, string.Empty, true);
                _ObjCommonMethods.BindServiceUnits(ddlSU, string.Empty, true);
                _ObjCommonMethods.BindTariffSubTypes(ddlTariff, 0, true);
                ddlTariff.Items.RemoveAt(0);
                ddlTariff.Items.Insert(0, new ListItem("All", ""));
                _ObjCommonMethods.BindReadCodes(ddlReadCode, true);
                ddlReadCode.Items.RemoveAt(0);
                ddlReadCode.Items.Insert(0, new ListItem("All", ""));
                _ObjCommonMethods.BindCyclesList(ddlCycle, true);
                //_ObjCommonMethods.BindActiveStatusDetails(ddlStatus, UMS_Resource.SELECT, true);
                _ObjCommonMethods.BindCustomerStatusDetails(ddlStatus, Resource.DDL_ALL, true);
                //_ObjCommonMethods.BindFeeders(ddlFeeder, string.Empty, UMS_Resource.DROPDOWN_SELECT, true);
                //  _ObjCommonMethods.BindTariff(ddlTariff, true);
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, "BindDropDowns", ErrorLog.LogType.Error, ex);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                txtName.Text = txtAccNo.Text = txtOldAccountNo.Text = txtMeterNo.Text = txtCode1.Text = txtMobileNum.Text = string.Empty;
                ddlSU.SelectedIndex = ddlSC.SelectedIndex = ddlCycle.SelectedIndex = ddlTariff.SelectedIndex = ddlStatus.SelectedIndex = ddlReadCode.SelectedIndex = 0;
                //bindSearch(string.Empty);
                lblCountRecords.Text = hfTotalRecords.Value = Constants.Zero.ToString();
                hfSearchString.Value = string.Empty;
                gvSearchCustomers.DataSource = new DataTable();
                gvSearchCustomers.DataBind();
                divLinks.Visible = divpaging.Visible = false;
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, "BindDropDowns", ErrorLog.LogType.Error, ex);
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                //bool isSearch = true;
                hfSearchString.Value = string.Empty;
                hfPageNo.Value = Constants.pageNoValue.ToString();
                //if (!string.IsNullOrEmpty(txtAccNo.Text.Trim()))
                //{
                //    RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
                //    objLedgerBe.AccountNo = txtAccNo.Text.Trim();
                //    objLedgerBe.Flag = 1;
                //    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                //        objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                //    else
                //        objLedgerBe.BUID = string.Empty;
                //    objLedgerBe = _ObjiDsignBAL.DeserializeFromXml<RptCustomerLedgerBe>(objReportsBal.GetLedger(objLedgerBe, ReturnType.Single));
                //    if (objLedgerBe.IsSuccess)
                //    {
                //        if (objLedgerBe.IsCustExistsInOtherBU)
                //        {
                //            isSearch = false;
                //            lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                //            mpeAlert.Show();
                //        }
                //        else
                //        {
                //            isSearch = true;
                //        }
                //    }
                //    else
                //    {
                //        isSearch = false;
                //        _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.CUSTOMER_NOT_FOUND_MSG, pnlMessage, lblMessage);
                //    }
                //}

                //if (isSearch)
                //{
                bool atleastOne = false;
                string searchString = " WHERE ";

                if (!string.IsNullOrEmpty(txtAccNo.Text.Trim()))
                {
                    searchString += " GlobalAccountNumber LIKE '%" + txtAccNo.Text.Trim() + "%' ";
                    atleastOne = true;
                }
                if (!string.IsNullOrEmpty(txtName.Text.Trim()))
                {
                    if (atleastOne)
                        searchString += " AND Name LIKE '%" + txtName.Text.Trim() + "%' ";
                    else
                        searchString += " Name LIKE '%" + txtName.Text.Trim() + "%' ";

                    atleastOne = true;
                }
                if (!string.IsNullOrEmpty(txtOldAccountNo.Text.Trim()))
                {
                    if (atleastOne)
                        searchString += " AND OldAccountNo LIKE '%" + txtOldAccountNo.Text.Trim() + "%' ";
                    else
                        searchString += " OldAccountNo LIKE '%" + txtOldAccountNo.Text.Trim() + "%' ";

                    atleastOne = true;
                }
                if (!string.IsNullOrEmpty(txtMeterNo.Text.Trim()))
                {
                    if (atleastOne)
                        searchString += " AND MeterNumber LIKE '%" + txtMeterNo.Text.Trim() + "%' ";
                    else
                        searchString += " MeterNumber LIKE '%" + txtMeterNo.Text.Trim() + "%' ";
                    
                    atleastOne = true;
                }
                if (!string.IsNullOrEmpty(ddlBU.SelectedValue))
                {
                    if (atleastOne)
                        searchString += " AND BU_ID = '" + ddlBU.SelectedValue + "' ";
                    else
                        searchString += " BU_ID = '" + ddlBU.SelectedValue + "' ";
                    
                    atleastOne = true;
                }
                if (!string.IsNullOrEmpty(ddlSU.SelectedValue))
                {
                    if (atleastOne)
                        searchString += " AND SU_ID = '" + ddlSU.SelectedValue + "' ";
                    else
                        searchString += " SU_ID = '" + ddlSU.SelectedValue + "' ";
                    
                    atleastOne = true;
                }
                if (!string.IsNullOrEmpty(ddlSC.SelectedValue))
                {
                   if (atleastOne)
                       searchString += " AND ServiceCenterId = '" + ddlSC.SelectedValue + "' ";
                    else
                       searchString += " ServiceCenterId = '" + ddlSC.SelectedValue + "' ";
                    
                    atleastOne = true;
                }
                if (!string.IsNullOrEmpty(ddlCycle.SelectedValue))
                {
                    if (atleastOne)
                        searchString += " AND CycleId = '" + ddlCycle.SelectedValue + "' ";
                    else
                        searchString += " CycleId = '" + ddlCycle.SelectedValue + "' ";
                    
                    atleastOne = true;
                }
                if (!string.IsNullOrEmpty(ddlTariff.SelectedValue))
                {
                    if (atleastOne)
                        searchString += " AND TariffId = '" + ddlTariff.SelectedValue + "' ";
                    else
                        searchString += " TariffId = '" + ddlTariff.SelectedValue + "' ";
                    
                    atleastOne = true;
                }
                if (!string.IsNullOrEmpty(ddlReadCode.SelectedValue))
                {
                    if (atleastOne)
                        searchString += " AND ReadCodeId = '" + ddlReadCode.SelectedValue + "' ";
                    else
                        searchString += " ReadCodeId = '" + ddlReadCode.SelectedValue + "' ";
                    
                    atleastOne = true;
                }
                if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                {
                    if (atleastOne)
                        searchString += "AND ActiveStatusId = '" + ddlStatus.SelectedValue + "' ";
                    else
                        searchString += " ActiveStatusId = '" + ddlStatus.SelectedValue + "' ";
                    
                    atleastOne = true;
                }
                #region Old Code for Contact No search
                //if (!string.IsNullOrEmpty(txtCode1.Text.Trim()) || !string.IsNullOrEmpty(txtMobileNum.Text.Trim()))
                //{
                //    if (atleastOne)
                //    {
                //        searchString += " AND (HomeContactNumber LIKE '%" + txtCode1.Text.Trim() + "-" + txtMobileNum.Text.Trim() + "%' ";
                //        searchString += "OR BusinessContactNumber LIKE '%" + txtCode1.Text.Trim() + "-" + txtMobileNum.Text.Trim() + "%' ";
                //        searchString += "OR OtherContactNumber LIKE '%" + txtCode1.Text.Trim() + "-" + txtMobileNum.Text.Trim() + "%' )";
                //    }
                //    else
                //    {
                //        searchString += " (HomeContactNumber LIKE '%" + txtCode1.Text.Trim() + "-" + txtMobileNum.Text.Trim() + "%' ";
                //        searchString += "OR BusinessContactNumber LIKE '%" + txtCode1.Text.Trim() + "-" + txtMobileNum.Text.Trim() + "%' ";
                //        searchString += "OR OtherContactNumber LIKE '%" + txtCode1.Text.Trim() + "-" + txtMobileNum.Text.Trim() + "%' )";
                //    }
                //    atleastOne = true;
                //}
                #endregion
                #region New Code for Contact No search
                if (!string.IsNullOrEmpty(txtCode1.Text.Trim()))
                {
                    if (atleastOne)
                    {
                        searchString += " AND (Left(HomeContactNumber,charindex('-' , HomeContactNumber)) LIKE '%" + txtCode1.Text.Trim() + "%' ";
                        searchString += "OR Left(BusinessContactNumber,charindex('-' , BusinessContactNumber)) LIKE '%" + txtCode1.Text.Trim() + "%' ";
                        searchString += "OR Left(OtherContactNumber,charindex('-' , OtherContactNumber)) LIKE '%" + txtCode1.Text.Trim() + "%' )";
                    }
                    else
                    {
                        searchString += " (Left(HomeContactNumber,charindex('-' , HomeContactNumber)) LIKE '%" + txtCode1.Text.Trim() + "%' ";
                        searchString += "OR Left(BusinessContactNumber,charindex('-' , BusinessContactNumber)) LIKE '%" + txtCode1.Text.Trim() + "%' ";
                        searchString += "OR Left(OtherContactNumber,charindex('-' , OtherContactNumber)) LIKE '%" + txtCode1.Text.Trim() + "%' )";
                    }
                    atleastOne = true;
                }
                if (!string.IsNullOrEmpty(txtMobileNum.Text.Trim()))
                {
                    if (atleastOne)
                    {
                        searchString += " AND (Stuff(HomeContactNumber, 1, charindex('-' , HomeContactNumber), NULL) LIKE '%" + txtMobileNum.Text.Trim() + "%' ";
                        searchString += "OR Stuff(BusinessContactNumber, 1, charindex('-' , BusinessContactNumber), NULL) LIKE '%" + txtMobileNum.Text.Trim() + "%' ";
                        searchString += "OR Stuff(OtherContactNumber, 1, charindex('-' , OtherContactNumber), NULL) LIKE '%" + txtMobileNum.Text.Trim() + "%' )";
                    }
                    else
                    {
                        searchString += " (Stuff(HomeContactNumber, 1, charindex('-' , HomeContactNumber), NULL) LIKE '%" + txtMobileNum.Text.Trim() + "%' ";
                        searchString += "OR Stuff(BusinessContactNumber, 1, charindex('-' , BusinessContactNumber), NULL) LIKE '%" + txtMobileNum.Text.Trim() + "%' ";
                        searchString += "OR Stuff(OtherContactNumber, 1, charindex('-' , OtherContactNumber), NULL) LIKE '%" + txtMobileNum.Text.Trim() + "%' )";
                    }
                    atleastOne = true;
                }
                #endregion
                if (!atleastOne)
                    searchString += "1=1";

                hfSearchString.Value = searchString;
                bindSearch(hfSearchString.Value);
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                //}
                //else
                //{
                //    gvSearchCustomers.DataSource = new DataTable();
                //    gvSearchCustomers.DataBind();
                //    divLinks.Visible = divpaging.Visible = false;
                //}
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, "btnSearch_Click", ErrorLog.LogType.Error, ex);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString[UMS_Resource.FROM_PAGE] != null)
                {
                    switch (_ObjiDsignBAL.Decrypt(Request.QueryString[UMS_Resource.FROM_PAGE]))
                    {
                        case Constants.SearchCustomerPageName:
                            Response.Redirect(UMS_Resource.SEARCH_CUSTOMER_PAGE, false);
                            break;
                        case Constants.MeterReadingPageName:
                            if (Request.QueryString[UMS_Resource.QSTR_ACC_NO] != null)//written if condition for getting customer no form query string and send back by suresh
                            {
                                Response.Redirect(UMS_Resource.METER_READING_PAGE + "?" + UMS_Resource.QSTR_ACC_NO + "=" + Request.QueryString[UMS_Resource.QSTR_ACC_NO].ToString(), false);//Adding Qerty string parameter by Suresh
                            }
                            else
                                Response.Redirect(UMS_Resource.METER_READING_PAGE, false);
                            break;
                        case Constants.BillAdjustmentsPageName:
                            Response.Redirect(UMS_Resource.BILL_ADJUSTMENTS, false);
                            break;
                        case Constants.CustomerBillPayment:
                            Response.Redirect(UMS_Resource.CUSTOMER_BILL_PAYMENT, false);
                            break;
                        case Constants.TariffChange:
                            Response.Redirect(Constants.CustomerChangeTariff, false);
                            break;
                        case Constants.CustomerLedger:
                            Response.Redirect(Constants.RptCustomerLedgerPage, false);
                            break;

                    }
                }
                else
                {
                    Response.Redirect(UMS_Resource.HomePage);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, "btnBack_Click", ErrorLog.LogType.Error, ex);
            }
        }


        //protected void ddlReadCode_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //    try
        //    {
        //        bindSearch(string.Empty);
        //        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLog.LoggingIntoText(ex.Message, "ddlReadCode_SelectedIndexChanged", ErrorLog.LogType.Error, ex);
        //    }
        //}

        protected void rptrDocuments_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                Repeater rptDemo = sender as Repeater; // Get the Repeater control object.

                // If the Repeater contains no data.
                if (rptDemo != null && rptDemo.Items.Count < 1)
                {
                    if (e.Item.ItemType == ListItemType.Footer)
                    {
                        // Show the Error Label (if no data is present).
                        Label lblErrorMsg = e.Item.FindControl("lblDocName") as Label;
                        if (lblErrorMsg != null)
                        {
                            lblErrorMsg.Visible = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, "rptrDocuments_ItemDataBound", ErrorLog.LogType.Error, ex);
            }
        }
        protected void gvSearchCustomers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //Repeater rptrDocuments = (Repeater)e.Row.FindControl("rptrDocuments");
                    Label lblGvCustUnique = (Label)e.Row.FindControl("lblGvCustUnique");
                    string GlobalAccountNo = ((LinkButton)e.Row.FindControl("lbtnAccNumber")).Text;
                    //BindDocuments(rptrDocuments, lblGvCustUnique.Text);

                    Label lblIsBookNoChanged = (Label)e.Row.FindControl("lblIsBookNoChanged");
                    Label spanBookNoChanged = (Label)e.Row.FindControl("spanBookNoChanged");
                    LinkButton lnkProfile = (LinkButton)e.Row.FindControl("lnkProfile");
                    lnkProfile.PostBackUrl = UMS_Resource.CST_DETAILS_PAGE + "?ac=" + _ObjiDsignBAL.Encrypt(GlobalAccountNo);
                    bool isbookchanged = string.IsNullOrEmpty(lblIsBookNoChanged.Text) ? false : Convert.ToBoolean(lblIsBookNoChanged.Text);

                    //if (Convert.ToBoolean(lblIsBookNoChanged.Text))
                    if (isbookchanged)
                        spanBookNoChanged.Visible = true;
                    else
                        spanBookNoChanged.Visible = false;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, "gvSearchCustomers_RowDataBound", ErrorLog.LogType.Error, ex);
            }
        }

        public void BindDocuments(Repeater rptrDocuments, string lblGvCustUnique)
        {
            try
            {
                ConsumerBe objConsumerBe = new ConsumerBe();
                ConsumerListBe objConsumerListBe = new ConsumerListBe();
                objConsumerBe.CustomerUniqueNo = lblGvCustUnique;
                objConsumerListBe = _ObjiDsignBAL.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Retrieve(objConsumerBe, ReturnType.Set));
                if (objConsumerListBe.items.Count > 0)
                {
                    rptrDocuments.DataSource = objConsumerListBe.items;
                    rptrDocuments.DataBind();
                }
                else
                {

                    rptrDocuments.DataSource = new DataTable();
                    rptrDocuments.DataBind();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, "BindDocuments", ErrorLog.LogType.Error, ex);
            }
        }

        protected void lbtnDocName_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtnDocName = (LinkButton)sender;
                string fileLocation = Server.MapPath(ConfigurationManager.AppSettings["CustomerDocuments"].ToString()) + lbtnDocName.CommandArgument;
                if (File.Exists(fileLocation))
                    _ObjCommonMethods.DownloadFile(fileLocation, "");
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, "lbtnDocName_Click", ErrorLog.LogType.Error, ex);
            }
        }

        protected void gvSearchCustomers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                LinkButton lbtnAccNumber = (LinkButton)row.FindControl("lbtnAccNumber");
                Label lblGvCustUnique = (Label)row.FindControl("lblGvCustUnique");
                switch (e.CommandName.ToUpper())
                {
                    case "REDIRECT":
                        if (Request.QueryString[UMS_Resource.FROM_PAGE] != null)
                        {
                            switch (_ObjiDsignBAL.Decrypt(Request.QueryString[UMS_Resource.FROM_PAGE]))
                            {
                                case Constants.SearchCustomerPageName:
                                    Response.Redirect(UMS_Resource.SEARCH_CUSTOMER_PAGE + "?" + UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO + "=" + _ObjiDsignBAL.Encrypt(lbtnAccNumber.Text), false);
                                    break;
                                case Constants.MeterReadingPageName:
                                    Response.Redirect(UMS_Resource.METER_READING_PAGE + "?" + UMS_Resource.QSTR_ACC_NO + "=" + _ObjiDsignBAL.Encrypt(lbtnAccNumber.Text), false);//Adding Qerty string parameter by Suresh
                                    break;
                                case Constants.BillAdjustmentsPageName:
                                    Response.Redirect(UMS_Resource.BILL_ADJUSTMENTS + "?" + UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO + "=" + _ObjiDsignBAL.Encrypt(lbtnAccNumber.Text) + "&" + UMS_Resource.QUERY_CUSTOMER_READ_CODE + "=" + _ObjiDsignBAL.Encrypt("0"), false);
                                    break;
                                case Constants.CustomerBillPayment:
                                    Response.Redirect(UMS_Resource.CUSTOMER_BILL_PAYMENT + "?" + UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO + "=" + _ObjiDsignBAL.Encrypt(lbtnAccNumber.Text) + "&" + UMS_Resource.QUERY_CUSTOMER_READ_CODE + "=" + _ObjiDsignBAL.Encrypt("0"), false);
                                    break;
                                case Constants.TariffChange:
                                    Response.Redirect(UMS_Resource.CUSTOMER_CHANGE_TARIFF_PAGE + "?" + UMS_Resource.QSTR_ACC_NO + "=" + _ObjiDsignBAL.Encrypt(lbtnAccNumber.Text), false);
                                    break;
                                case Constants.CustomerLedger:
                                    Response.Redirect(Constants.RptCustomerLedgerPage + "?" + UMS_Resource.QSTR_ACC_NO + "=" + _ObjiDsignBAL.Encrypt(lbtnAccNumber.Text), false);
                                    break;
                            }
                        }
                        else
                            Response.Redirect(UMS_Resource.EDIT_CONSUMER_PAGE + "?" + UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO + "=" + _ObjiDsignBAL.Encrypt(lblGvCustUnique.Text), false);
                        break;
                }

            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, "gvSearchCustomers_RowCommand", ErrorLog.LogType.Error, ex);
            }
        }
        #endregion

        #region ddls
        protected void ddlBU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _ObjCommonMethods.BindUserServiceUnits_BuIds(ddlSU, BusinessUnit, Resource.DDL_ALL, false);
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, "ddlBU_SelectedIndexChanged", ErrorLog.LogType.Error, ex);
            }
        }
        protected void ddlSU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _ObjCommonMethods.BindUserServiceCenters_SuIds(ddlSC, ServiceUnit, Resource.DDL_ALL, false);
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, "ddlSU_SelectedIndexChanged", ErrorLog.LogType.Error, ex);
            }
        }
        protected void ddlSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _ObjCommonMethods.BindCyclesByBUSUSC(ddlCycle, BusinessUnit, ServiceUnit, ServiceCenter, true, Resource.DDL_ALL, false);
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, "ddlSC_SelectedIndexChanged", ErrorLog.LogType.Error, ex);
            }
        }
        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, "Message", ErrorLog.LogType.Error, ex);
            }
        }

        private void bindSearch(string SearchString)
        {
            try
            {
                //_objConsumerBe.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                //_objConsumerBe.Name = txtName.Text;
                //_objConsumerBe.HomeContactNo = txtCode1.Text + "-" + txtMobileNum.Text;
                //_objConsumerBe.BU_ID = ddlBU.SelectedItem.Value;
                //_objConsumerBe.SU_ID = ddlSU.SelectedItem.Value;
                //_objConsumerBe.ServiceCenterId = ddlSC.SelectedItem.Value;
                //_objConsumerBe.TariffId = Convert.ToInt32(ddlTariff.SelectedItem.Value == "" ? "0" : ddlTariff.SelectedItem.Value);
                //_objConsumerBe.AccountNo = txtAccNo.Text;
                //_objConsumerBe.OldAccountNo = txtOldAccountNo.Text;
                //_objConsumerBe.MeterNo = txtMeterNo.Text;
                //_objConsumerBe.CycleId = ddlCycle.SelectedValue;
                //_objConsumerBe.ReadCodeId = ddlReadCode.SelectedValue == "" ? Constants.Zero : Convert.ToInt32(ddlReadCode.SelectedValue);
                //_objConsumerBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                //_objConsumerBe.PageSize = PageSize;
                //_objConsumerBe.Name = txtName.Text;
                //_objConsumerBe.StatusId = Status;

                int startIndex, endIndex = 0;
                startIndex = ((Convert.ToInt32(hfPageNo.Value) - 1) * PageSize) + 1;
                endIndex = (Convert.ToInt32(hfPageNo.Value) * PageSize);

                DataSet ds = _objCoonsumerBAl.GetSearchCustomers(SearchString, startIndex, endIndex);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvSearchCustomers.DataSource = ds.Tables[0];
                    gvSearchCustomers.DataBind();
                    divLinks.Visible = divpaging.Visible = true;
                    lblCountRecords.Text = hfTotalRecords.Value = ds.Tables[0].Rows[0]["TotalRecords"].ToString();
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                }
                else
                {
                    lblCountRecords.Text = hfTotalRecords.Value = Constants.Zero.ToString();
                    gvSearchCustomers.DataSource = new DataTable();
                    gvSearchCustomers.DataBind();
                    divLinks.Visible = divpaging.Visible = false;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, "bindSearch", ErrorLog.LogType.Error, ex);
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, "InitializeCulture", ErrorLog.LogType.Error, ex);
            }
        }
        #endregion

        #region Paging
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                bindSearch(hfSearchString.Value);
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                bindSearch(hfSearchString.Value);
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                bindSearch(hfSearchString.Value);
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                bindSearch(hfSearchString.Value);
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                bindSearch(hfSearchString.Value);
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                _ObjiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; }
                else
                {
                    lkbtnNext.Enabled = lkbtnLast.Enabled = true;
                    lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5");
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                _ObjCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize, Constants.SearchCustPageSize, Constants.SearchCustPageSizeIncrement);
                //ddlPageSize.Items.Clear();
                //if (TotalRecords < Constants.PageSizeStarts)
                //{
                //    ListItem item = new ListItem(Constants.PageSizeStarts.ToString(), Constants.PageSizeStarts.ToString());
                //    ddlPageSize.Items.Add(item);
                //}
                //else
                //{
                //    int previousSize = Constants.PageSizeStarts;
                //    for (int i = Constants.PageSizeStarts; i <= TotalRecords; i = i + Constants.PageSizeIncrement)
                //    {
                //        ListItem item = new ListItem(i.ToString(), i.ToString());
                //        ddlPageSize.Items.Add(item);
                //        previousSize = i;
                //    }
                //    if (previousSize < TotalRecords)
                //    {
                //        ListItem item = new ListItem((previousSize + Constants.PageSizeIncrement).ToString(), (previousSize + Constants.PageSizeIncrement).ToString());
                //        ddlPageSize.Items.Add(item);
                //    }
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}