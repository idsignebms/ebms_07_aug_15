﻿<%@ Page Title="AssignMeters" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    AutoEventWireup="true" CodeBehind="AssignMeters.aspx.cs" Inherits="UMS_Nigeria.ConsumerManagement.AssignMeters"
    Theme="Green" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControls/Authentication.ascx" TagName="Authentication" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out_bor_Payment">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upPaymentEntry" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div class="text_total">
                    <div class="text-heading">
                        <asp:Literal ID="litAddState" runat="server" Text="Assign Meter"></asp:Literal>
                    </div>
                    <div class="star_text">
                        <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div class="dot-line">
                </div>
                <div class="inner-box">
                    <div class="inner-box1">
                        <div class="text-inner">
                            <asp:Literal ID="litCountryName" runat="server" Text="<%$ Resources:Resource, ACCNO_OLD_ACCNO%>"></asp:Literal>
                            <span class="span_star">*</span><div class="space">
                            </div>
                            <asp:TextBox CssClass="text-box" runat="server" ID="txtAccountNo" MaxLength="20"
                                onblur="return TextBoxBlurValidationlbl(this,'spanAccountNo','Global Account No / Old Account No')"
                                onkeypress="GetAccountDetails(event)" placeholder="Enter Global Account No / Old Account No"></asp:TextBox>
                            &nbsp
                            <asp:FilteredTextBoxExtender ID="ftbAccNo" runat="server" TargetControlID="txtAccountNo"
                                FilterType="Numbers" ValidChars=" ">
                            </asp:FilteredTextBoxExtender>
                            <asp:LinkButton runat="server" Visible="false" ID="lblSearch" OnClick="lblSearch_Click"
                                CssClass="search_img"></asp:LinkButton>
                            <br />
                            <span id="spanAccountNo" class="span_color"></span>
                        </div>
                    </div>
                </div>
                <div class="box_total">
                    <div class="box_total_a" style="margin-left: 15px;">
                        <asp:Button ID="btnGo" Text="<%$ Resources:Resource, GET_CUSTOMER_DETAILS%>" CssClass="box_s"
                            runat="server" OnClientClick="return ValidateGo();" OnClick="btnGo_Click" />
                    </div>
                </div>
                <div class="clr">
                </div>
                <br />
                <div id="divCustomerDetails" runat="server" visible="false">
                    <div class="out-bor_a">
                        <div class="inner-sec">
                            <div class="heading" style="padding-left: 16px; padding-top: 17px;">
                                <asp:Literal ID="Literal213" runat="server" Text="<%$ Resources:Resource, CUSTOMER_DETAILS%>"></asp:Literal>
                            </div>
                            <div class="out-bor_aTwo">
                                <table class="customerdetailsTamle">
                                    <tr>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label runat="server" ID="lblGlobalAccNo"></asp:Label>
                                            <asp:Label runat="server" Visible="false" ID="lblAccountno"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="litOldAccountNo" runat="server" Text="Old Account No"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label runat="server" ID="lblOldAccountNo"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label runat="server" ID="lblServiceAddress"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal3" runat="server" Text="Name"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label runat="server" ID="lblName"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, OUT_STANDING_AMT%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label ID="lblTotalDueAmount" runat="server"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal6" runat="server" Text="Read Type"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Literal ID="lblReadType" runat="server" Text=""></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, ROUTE_SEQUENCE_NO%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Literal ID="litRouteSequenceNo" runat="server"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                        </td>
                                        <td class="customerdetailstamleTd">
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                        </td>
                                        <td class="customerdetailstamleTd">
                                        </td>
                                        <td class="customerdetailstamleTd">
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div id="divAssignMeter" runat="server" visible="false" style="width: auto;">
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litAgencyName" runat="server" Text="Meter No"></asp:Literal>
                                        <span class="span_star">*</span></label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox CssClass="text-box" ID="txtMeterNo" TabIndex="1" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spnMeterNo','Meter No')"
                                        placeholder="Meter No" AutoPostBack="true" MaxLength="50" OnTextChanged="txtMeterNo_TextChanged"></asp:TextBox>
                                    <span id="spnMeterNo" class="span_color"></span>
                                    <br />
                                    <span id="spnMeterNoStatus" runat="server" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal1" runat="server" Text="Initial Meter Reading"></asp:Literal>
                                        <span class="span_star">*</span></label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox CssClass="text-box" ID="txtInitialReading" TabIndex="1" runat="server"
                                        onblur="return TextBoxBlurMeterValidationlbl(this,'spnInitialReading','Initial Meter Reading')"
                                        placeholder="Initial Meter Reading" MaxLength="9"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender34" runat="server" TargetControlID="txtInitialReading"
                                        FilterType="Numbers">
                                    </asp:FilteredTextBoxExtender>
                                    <span id="spnInitialReading" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box3">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litReadingDate" runat="server" Text="Meter Assigned Date"></asp:Literal>
                                        <span class="span_star">*</span>
                                    </label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox ID="txtMeterAssignedDate" Style="width: 186px;" onchange="DoBlur(this);"
                                        onblur="DoBlur(this);" CssClass="text-box" runat="server" placeholder="Enter Meter Assigned Date"
                                        AutoComplete="off"></asp:TextBox>
                                    <asp:Image ID="imgDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                        vertical-align: middle" AlternateText="Calender" runat="server" />
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtMeterAssignedDate"
                                        FilterType="Custom,Numbers" ValidChars="/">
                                    </asp:FilteredTextBoxExtender>
                                    <span id="spanMeterAssignedDate" class="span_color"></span>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, REASON%>"></asp:Literal>
                                        <span class="span_star">*</span></label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox CssClass="text-box" ID="txtReason" runat="server" onblur="return TextBoxBlurValidationlbl(this,'SpanReason','Reason')"
                                        placeholder="Enter Reason" TextMode="MultiLine"></asp:TextBox>
                                    <span id="SpanReason" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box2" id="divCAPMI" runat="server" visible="false">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal4" runat="server" Text="CAPMI Amount"></asp:Literal>
                                    </label>
                                    <div class="space">
                                    </div>
                                    <asp:Label ID="lblCAPMIAmount" runat="server" ForeColor="Black"></asp:Label>
                                </div>
                            </div>
                            <div class="space">
                            </div>
                            <div class="box_total">
                                <div class="box_total_a">
                                    <asp:Button ID="btnSave" TabIndex="8" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                        CssClass="box_s" runat="server" OnClick="btnSave_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--Alert--%>
                <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                    TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                </asp:ModalPopupExtender>
                <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                    <div class="popheader popheaderlblred">
                        <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%--Alert--%>
                <%-- Confirm popup Start--%>
                <%--<asp:ModalPopupExtender ID="mpeCofirm" runat="server" PopupControlID="PanelCofirm"
                    TargetControlID="btnCofirm" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnCofirm" runat="server" Text="Button" CssClass="popbtn" />
                <asp:Panel ID="PanelCofirm" runat="server" DefaultButton="btnYes" CssClass="modalPopup"
                    class="poppnl" Style="display: none;">
                    <div class="popheader popheaderlblred" id="popup" runat="server">
                        <asp:Label ID="lblConfirmMsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnYes" runat="server" OnClientClick="return closePopup();" OnClick="btnYes_Click"
                                Text="<%$ Resources:Resource, YES%>" CssClass="ok_btn" />
                            <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, NO%>"
                                CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>--%>
                <%-- Confirm popup End--%>
                <asp:ModalPopupExtender ID="mpeAlert" runat="server" TargetControlID="hdnCancelCNA"
                    PopupControlID="pnlAlert" BackgroundCssClass="popbg" CancelControlID="btnBPOK">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hdnCancelCNA" runat="server"></asp:HiddenField>
                <asp:Panel runat="server" ID="pnlAlert" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblAlertMsg" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnBPOK" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="btn_ok"
                                Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                    TargetControlID="btnActivate" BackgroundCssClass="modalBackground">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="PanelActivate" runat="server" DefaultButton="btnActiveCancel" CssClass="modalPopup"
                    Style="display: none; border-color: #639B00;">
                    <div class="popheader" style="background-color: #639B00;">
                        <asp:Label ID="lblActiveMsg2" runat="server"></asp:Label>
                    </div>
                    <div class="space">
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeMeterDetails" runat="server" TargetControlID="hdnMeterDetails"
                    PopupControlID="pnlMeterDetials" BackgroundCssClass="popbg" CancelControlID="btnAssignNew">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hdnMeterDetails" runat="server"></asp:HiddenField>
                <asp:Panel runat="server" ID="pnlMeterDetials" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblMeterDetails" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="clear pad_10">
                    </div>
                    <div class="consumer_feild">
                        <div class="consume_name">
                            <asp:Literal ID="Literal13" runat="server" Text="Global Account No."></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblGlobalAccountNo" runat="server"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="Literal14" runat="server" Text="Meter No."></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblMeterNo" runat="server"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div id="divOldCAPMI" runat="server">
                            <div class="consume_name">
                                <asp:Literal ID="Literal5" runat="server" Text="Is CAPMI Meter?"></asp:Literal>
                            </div>
                            <div class="consume_input c_left">
                                <asp:Label ID="lblIsOldCAPMIMeter" runat="server"></asp:Label>
                            </div>
                            <div class="clear pad_5">
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal9" runat="server" Text="CAPMI Amount"></asp:Literal>
                            </div>
                            <div class="consume_input c_left">
                                <asp:Label ID="lblOldCAPMIAmount" runat="server"></asp:Label>
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="Literal15" runat="server" Text="Change Date"></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblChangeDate" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btnAssignOld" runat="server" Text="Assign This Meter" OnClick="btnAssignOld_TextChanged"
                                CssClass="btn_ok" Style="margin-left: 10px;" />
                            <asp:Button ID="btnAssignNew" runat="server" Text="Assign New" CssClass="btn_ok"
                                Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="hdnCustomerTypeID" runat="server" Value="" />
                <asp:ModalPopupExtender ID="mpe" runat="server" PopupControlID="PanelActivate1" TargetControlID="btnActivate1"
                    BackgroundCssClass="modalBackground">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnActivate1" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="PanelActivate1" runat="server" DefaultButton="btnActiveCancel1" CssClass="modalPopup"
                    Style="display: none; border-color: #639B00;">
                    <div class="popheader" id="pop" runat="server" style="background-color: #639B00;">
                        <asp:Label ID="lblActive1" runat="server"></asp:Label>
                    </div>
                    <div class="space">
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btnActiveCancel1" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <uc1:Authentication ID="ucAuthentication" runat="server" />
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        //        $(document).keydown(function (e) {
        //            // ESCAPE key pressed 
        //            if (e.keyCode == 27) {
        //                $(".rnkland").fadeOut("slow");
        //                $(".mpeConfirmDelete").fadeOut("slow");
        //                document.getElementById("overlay").style.display = "none";
        //            }
        //        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        //        function pageLoad() {
        //            $get("<%=txtAccountNo.ClientID%>").focus();
        //        };


        $(document).ready(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
            DatePicker($('#UMSNigeriaBody_txtMeterAssignedDate'), $('#UMSNigeriaBody_imgDate'));
            // AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
            DatePicker($('#UMSNigeriaBody_txtMeterAssignedDate'), $('#UMSNigeriaBody_imgDate'));
            //AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);
        });
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/AssignMeters.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
</asp:Content>
