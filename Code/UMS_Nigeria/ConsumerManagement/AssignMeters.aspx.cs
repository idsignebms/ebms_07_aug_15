﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Xml;
using System.Data;
using Resources;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class AssignMeters : System.Web.UI.Page
    {
        #region Members
        MastersBAL _ObjMastersBAL = new MastersBAL();
        ReportsBal objReportsBal = new ReportsBal();
        iDsignBAL _ObjIdsignBal = new iDsignBAL();
        ConsumerBal objConsumerBal = new ConsumerBal();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        public string Key = "AssignMeter";
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString[UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO]))
                {
                    string AccountNo = _ObjIdsignBal.Decrypt(Request.QueryString[UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO]).ToString();
                    txtAccountNo.Text = AccountNo;
                }
            }
        }

        protected void lblSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADVANCE_SEARCH_PAGE + "?" + UMS_Resource.FROM_PAGE + "=" + _ObjIdsignBal.Encrypt(Constants.CustomerBillPayment), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
            objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                objLedgerBe.BUID = string.Empty;
            objLedgerBe = _ObjIdsignBal.DeserializeFromXml<RptCustomerLedgerBe>(objReportsBal.GetLedger(objLedgerBe, ReturnType.Set));
            if (objLedgerBe.IsSuccess)
            {
                if (objLedgerBe.IsCustExistsInOtherBU)
                {
                    lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                    mpeAlert.Show();
                    divAssignMeter.Visible = false;
                }
                else if (objLedgerBe.CustIsNotActive)
                {
                    if (objLedgerBe.ActiveStatusId == 2)
                        lblAlertMsg.Text = "Customer status is Inactive.";
                    else if (objLedgerBe.ActiveStatusId == 3)
                        lblAlertMsg.Text = "Customer status is Hold.";
                    else if (objLedgerBe.ActiveStatusId == 4)
                        lblAlertMsg.Text = "Customer status is Closed.";
                    else
                        lblAlertMsg.Text = "Customer is not Active.";
                    mpeAlert.Show();
                    divAssignMeter.Visible = false;
                }
                else if (objLedgerBe.IsDisabledBook)
                {
                    divAssignMeter.Visible = false;
                    lblAlertMsg.Text = "Customer Book Number is Disabled.";
                    mpeAlert.Show();
                }
                else
                {
                    BindUserDetails();
                }
            }
            else
            {
                divCustomerDetails.Visible = false;
                divAssignMeter.Visible = false;
                ClearFields();
                _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.CUSTOMER_NOT_FOUND_MSG, pnlMessage, lblMessage);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            AssignMeter();
        }

        protected void txtMeterNo_TextChanged(object sender, EventArgs e)
        {
            BindAvailableMeterList();
        }

        protected void btnAssignOld_TextChanged(object sender, EventArgs e)
        {
            ReAssignMeter();
        }
        #endregion

        #region Methods

        private void BindUserDetails()
        {
            XmlDocument xml = new XmlDocument();
            try
            {
                CustomerRegistrationBE objCustomerRegistrationBE = new CustomerRegistrationBE();
                objCustomerRegistrationBE.GolbalAccountNumber = txtAccountNo.Text;
                xml = objConsumerBal.GetCustomerForAssignMeter(objCustomerRegistrationBE);
                if (xml != null)
                {
                    objCustomerRegistrationBE = _ObjIdsignBal.DeserializeFromXml<CustomerRegistrationBE>(xml);
                    if (objCustomerRegistrationBE != null)
                    {
                        if (objCustomerRegistrationBE.IsSuccessful)
                        {
                            lblGlobalAccNo.Text = objCustomerRegistrationBE.GlobalAccountNumber;
                            lblAccountno.Text = objCustomerRegistrationBE.AccountNo;
                            lblServiceAddress.Text = objCustomerRegistrationBE.FullServiceAddress;
                            lblName.Text = objCustomerRegistrationBE.FirstNameLandlord;
                            //lblName.Text = objCustomerRegistrationBE.Name;
                            litRouteSequenceNo.Text = objCustomerRegistrationBE.RouteName;// Convert.ToString(objCustomerRegistrationBE.RouteSequenceNumber);
                            lblTotalDueAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(objCustomerRegistrationBE.OutStandingAmount.ToString()), 2, Constants.MILLION_Format).ToString(); //Raja-ID065 For Getting amt with comma seperated
                            lblOldAccountNo.Text = objCustomerRegistrationBE.OldAccountNo;
                            hdnCustomerTypeID.Value = Convert.ToString(objCustomerRegistrationBE.CustomerTypeId);
                            if (objCustomerRegistrationBE.ReadCodeID == (int)ReadCode.Read)
                            {
                                lblReadType.Text = ReadCode.Read.ToString();
                                divAssignMeter.Visible = false;
                                divCustomerDetails.Visible = true;
                                lblAlertMsg.Text = "This is read customer. Meter already assigned.";
                                mpeAlert.Show();
                            }
                            else if (objCustomerRegistrationBE.ReadCodeID == (int)ReadCode.Direct)
                            {
                                if (GetDisconnectedMeterDetails())
                                {
                                    divAssignMeter.Visible = true;
                                    divCustomerDetails.Visible = true;
                                    lblReadType.Text = ReadCode.Direct.ToString();
                                }
                                else
                                {
                                    divAssignMeter.Visible = true;
                                    divCustomerDetails.Visible = true;
                                    lblReadType.Text = ReadCode.Direct.ToString();
                                }
                            }
                        }
                        else
                        {
                            divCustomerDetails.Visible = false;
                            divAssignMeter.Visible = false;
                            ClearFields();
                            Message("No customer found.", UMS_Resource.MESSAGETYPE_ERROR);
                        }
                    }
                    else
                    {
                        divCustomerDetails.Visible = false;
                        divAssignMeter.Visible = false;
                        ClearFields();
                        Message("No customer found.", UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
            }
            catch (Exception ex)
            {
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                Message("Error in customer search.", UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void ClearFields()
        {
            txtAccountNo.Text = lblAccountno.Text = lblName.Text = lblServiceAddress.Text = lblTotalDueAmount.Text
                = litRouteSequenceNo.Text = txtMeterNo.Text = txtInitialReading.Text = lblReadType.Text =
                txtMeterAssignedDate.Text = txtReason.Text = spnMeterNoStatus.InnerHtml = string.Empty;
        }

        public void AssignMeter()
        {
            CustomerRegistrationBE objCustomerRegistrationBE = new CustomerRegistrationBE();
            objCustomerRegistrationBE.GolbalAccountNumber = lblAccountno.Text;
            objCustomerRegistrationBE.MeterNumber = txtMeterNo.Text;
            objCustomerRegistrationBE.InitialBillingKWh = Convert.ToInt32(txtInitialReading.Text);
            objCustomerRegistrationBE.MeterAssignedDate = _ObjCommonMethods.Convert_MMDDYY(txtMeterAssignedDate.Text);
            objCustomerRegistrationBE.ReadCodeID = (int)ReadCode.Read;
            objCustomerRegistrationBE.Reason = _ObjIdsignBal.ReplaceNewLines(txtReason.Text, true);
            objCustomerRegistrationBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            objCustomerRegistrationBE.FunctionId = (int)EnumApprovalFunctions.AssignMeter;
            objCustomerRegistrationBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            try
            {
                XmlDocument xml = null;
                if (_ObjCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.AssignMeter, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString()))
                {
                    objCustomerRegistrationBE.ApprovalStatusId = (int)EnumApprovalStatus.Approved;
                    objCustomerRegistrationBE.IsFinalApproval = true;
                    xml = objConsumerBal.AssignMeter(objCustomerRegistrationBE);
                    objCustomerRegistrationBE = _ObjIdsignBal.DeserializeFromXml<CustomerRegistrationBE>(xml);
                    lblActive1.Text = Resource.MODIFICATIONS_UPDATED;
                }
                else
                {
                    objCustomerRegistrationBE.ApprovalStatusId = (int)EnumApprovalStatus.Process;
                    xml = objConsumerBal.AssignMeter(objCustomerRegistrationBE);
                    objCustomerRegistrationBE = _ObjIdsignBal.DeserializeFromXml<CustomerRegistrationBE>(xml);
                    lblActive1.Text = Resource.APPROVAL_TARIFF_CHANGE;
                }

                if (xml != null)
                {
                    if (objCustomerRegistrationBE.IsSuccessful)
                    {
                        pop.Attributes.Add("class", "popheader popheaderlblgreen");
                        divAssignMeter.Visible = divCustomerDetails.Visible = false;
                        ClearFields();
                        mpe.Show();
                    }
                    else if (objCustomerRegistrationBE.IsNotCreditMeter)
                    {
                        pop.Attributes.Add("class", "popheader popheaderlblred");
                        divAssignMeter.Visible = divCustomerDetails.Visible = false;
                        ClearFields();
                        lblActive1.Text = "This Meter is not a Credit Meter, So please select Credit Meter.";
                        mpe.Show();
                    }
                    else if (objCustomerRegistrationBE.IsMeterAlreadyAssigned)
                    {
                        pop.Attributes.Add("class", "popheader popheaderlblred");
                        divAssignMeter.Visible = divCustomerDetails.Visible = false;
                        ClearFields();
                        lblActive1.Text = "This Meter No. is already requested to some other customer, please try with another Meter No.";
                        mpe.Show();
                    }
                    else if (objCustomerRegistrationBE.IsApprovalInProcess)
                    {
                        pop.Attributes.Add("class", "popheader popheaderlblred");
                        divAssignMeter.Visible = divCustomerDetails.Visible = false;
                        ClearFields();
                        lblActive1.Text = Resource.REQ_STILL_PENDING;
                        mpe.Show();
                    }
                    else if (objCustomerRegistrationBE.IsMeterChangeApprovalInProcess)
                    {
                        pop.Attributes.Add("class", "popheader popheaderlblred");
                        divAssignMeter.Visible = divCustomerDetails.Visible = false;
                        ClearFields();
                        lblActive1.Text = "Meter No. is already requested to change for one customer, please try with another Meter No.";
                        mpe.Show();
                    }
                    else if (!objCustomerRegistrationBE.IsSuccessful)
                    {
                        lblActive1.Text = "Meter is deactivated or not available.";
                        pop.Attributes.Add("class", "popheader popheaderlblred");
                        divAssignMeter.Visible = divCustomerDetails.Visible = false;
                        ClearFields();
                        mpe.Show();
                    }
                    else
                    {
                        Message("Meter cannot assign.", UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
                else
                {
                    Message("Meter cannot assign.", UMS_Resource.MESSAGETYPE_SUCCESS);
                }
            }
            catch (Exception ex)
            {
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        public void ReAssignMeter()
        {
            CustomerRegistrationBE objCustomerRegistrationBE = new CustomerRegistrationBE();
            objCustomerRegistrationBE.GolbalAccountNumber = lblAccountno.Text;
            objCustomerRegistrationBE.MeterNumber = lblMeterNo.Text;
            objCustomerRegistrationBE.ReadCodeID = (int)ReadCode.Read;
            objCustomerRegistrationBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            objCustomerRegistrationBE.FunctionId = (int)EnumApprovalFunctions.AssignMeter;
            objCustomerRegistrationBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            try
            {
                XmlDocument xml = null;
                if (_ObjCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.AssignMeter, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString()))
                {
                    objCustomerRegistrationBE.ApprovalStatusId = (int)EnumApprovalStatus.Approved;
                    objCustomerRegistrationBE.IsFinalApproval = true;
                    xml = objConsumerBal.ReAssignMeter(objCustomerRegistrationBE);
                    objCustomerRegistrationBE = _ObjIdsignBal.DeserializeFromXml<CustomerRegistrationBE>(xml);
                    lblActive1.Text = Resource.MODIFICATIONS_UPDATED;
                }
                else
                {
                    objCustomerRegistrationBE.ApprovalStatusId = (int)EnumApprovalStatus.Process;
                    xml = objConsumerBal.ReAssignMeter(objCustomerRegistrationBE);
                    objCustomerRegistrationBE = _ObjIdsignBal.DeserializeFromXml<CustomerRegistrationBE>(xml);
                    lblActive1.Text = Resource.APPROVAL_TARIFF_CHANGE;
                }

                if (xml != null)
                {
                    if (objCustomerRegistrationBE.IsNotCreditMeter)
                    {
                        pop.Attributes.Add("class", "popheader popheaderlblred");
                        divAssignMeter.Visible = divCustomerDetails.Visible = false;
                        ClearFields();
                        lblActive1.Text = "This Meter is not a Credit Meter, So please select Credit Meter.";
                        mpe.Show();
                    }
                    else if (objCustomerRegistrationBE.IsMeterAlreadyAssigned)
                    {
                        pop.Attributes.Add("class", "popheader popheaderlblred");
                        divAssignMeter.Visible = divCustomerDetails.Visible = false;
                        ClearFields();
                        lblActive1.Text = "This meter already assigned.";
                        mpe.Show();
                    }
                    else if (objCustomerRegistrationBE.IsMeterReqByAnotherCust)
                    {
                        pop.Attributes.Add("class", "popheader popheaderlblred");
                        divAssignMeter.Visible = divCustomerDetails.Visible = false;
                        ClearFields();
                        lblActive1.Text = "This meter requested by another customer.";
                        mpe.Show();
                    }
                    else if (objCustomerRegistrationBE.IsSuccessful)
                    {
                        pop.Attributes.Add("class", "popheader popheaderlblgreen");
                        divAssignMeter.Visible = divCustomerDetails.Visible = false;
                        ClearFields();
                        mpe.Show();
                    }
                    else if (objCustomerRegistrationBE.IsApprovalInProcess)
                    {
                        pop.Attributes.Add("class", "popheader popheaderlblred");
                        divAssignMeter.Visible = divCustomerDetails.Visible = false;
                        ClearFields();
                        lblActive1.Text = Resource.REQ_STILL_PENDING;
                        mpe.Show();
                    }
                    else if (!objCustomerRegistrationBE.IsSuccessful)
                    {
                        lblActive1.Text = "Meter is deactivated or not available.";
                        pop.Attributes.Add("class", "popheader popheaderlblred");
                        divAssignMeter.Visible = divCustomerDetails.Visible = false;
                        ClearFields();
                        mpe.Show();
                    }
                    else
                    {
                        Message("Meter cannot re-assign.", UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
                else
                {
                    Message("Meter cannot re-assign.", UMS_Resource.MESSAGETYPE_SUCCESS);
                }
            }
            catch (Exception ex)
            {
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        public void BindAvailableMeterList()
        {
            try
            {
                txtMeterNo.Text = txtMeterNo.Text.TrimStart();
                txtMeterNo.Text = txtMeterNo.Text.Trim();
                if (!string.IsNullOrEmpty(txtMeterNo.Text))
                {
                    divCAPMI.Visible = false;
                    MastersBE _ObjeMastersBE = new MastersBE();
                    _ObjeMastersBE.MeterNo = txtMeterNo.Text;
                    //_ObjeMastersBE.MeterSerialNo = txtMeterSerialNo.Text;
                    _ObjeMastersBE.CustomerTypeId = Convert.ToInt32(hdnCustomerTypeID.Value);
                    //Convert.ToInt32(ddlAccountType.SelectedValue);
                    if (Session[UMS_Resource.SESSION_USER_BUID] != null) _ObjeMastersBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                    else _ObjeMastersBE.BU_ID = string.Empty;
                    XmlDocument objXmlDocument = new XmlDocument();
                    objXmlDocument = _ObjMastersBAL.IsMeterExist(_ObjeMastersBE);
                    _ObjeMastersBE = _ObjIdsignBal.DeserializeFromXml<MastersBE>(objXmlDocument);
                    if (_ObjeMastersBE.IsSuccess)
                    {
                        if (_ObjeMastersBE.IsCAPMIMeter)
                        {
                            spnMeterNoStatus.InnerText = "This is CAPMI Meter.";
                            lblCAPMIAmount.Text = _ObjCommonMethods.GetCurrencyFormat(_ObjeMastersBE.CAPMIAmount, 2, Constants.MILLION_Format);
                            divCAPMI.Visible = true;
                        }
                        else
                        {
                            spnMeterNoStatus.InnerText = "This meter no. is available.";
                        }
                        spnMeterNoStatus.Attributes.Add("class", "span_color_green");
                        txtInitialReading.MaxLength = _ObjeMastersBE.MeterDials; ;
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "Script1", "setMeterDials(" + _ObjeMastersBE.MeterDials + ");", true);
                    }
                    else
                    {
                        spnMeterNoStatus.InnerText = "\"" + txtMeterNo.Text + "\" meter no. is not available.";
                        spnMeterNoStatus.Attributes.Add("class", "span_color");
                        //txtMeterNo.Text = string.Empty;
                    }
                }
                else
                    spnMeterNoStatus.InnerText = "";
            }
            catch (Exception ex)
            {
                Message("Exception in meter no availability check.", UMS_Resource.MESSAGETYPE_ERROR);
                txtMeterNo.Text = string.Empty;
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        public bool GetDisconnectedMeterDetails()
        {
            bool IsOldMeterExists = false;
            try
            {
                MastersBE _ObjeMastersBE = new MastersBE();
                CustomerRegistrationBE objCustomerRegistrationBE = new CustomerRegistrationBE();
                _ObjeMastersBE.AccountNo = txtAccountNo.Text;
                if (Session[UMS_Resource.SESSION_USER_BUID] != null) _ObjeMastersBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else _ObjeMastersBE.BU_ID = string.Empty;
                XmlDocument objXmlDocument = new XmlDocument();
                objXmlDocument = _ObjMastersBAL.GetDisconnectedMeterDetails(_ObjeMastersBE);
                objCustomerRegistrationBE = _ObjIdsignBal.DeserializeFromXml<CustomerRegistrationBE>(objXmlDocument);
                if (objCustomerRegistrationBE != null)
                {
                    if (objCustomerRegistrationBE.MeterNumber != null)
                    {
                        lblMeterDetails.Text = "Details of last disconnected meter.";
                        lblGlobalAccountNo.Text = objCustomerRegistrationBE.GlobalAccountNumber;
                        lblMeterNo.Text = objCustomerRegistrationBE.MeterNumber;
                        lblChangeDate.Text = _ObjCommonMethods.DateFormate(objCustomerRegistrationBE.CreatedDate);

                        if (objCustomerRegistrationBE.IsCAPMI)
                        {
                            lblIsOldCAPMIMeter.Text = "Yes";
                            lblOldCAPMIAmount.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerRegistrationBE.MeterAmount, 2, Constants.MILLION_Format);
                            divOldCAPMI.Visible = true;
                        }
                        else
                        {
                            divOldCAPMI.Visible = false;
                        }

                        mpeMeterDetails.Show();
                        IsOldMeterExists = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message("Exception in Get Disconnected Meter Details availability check.", UMS_Resource.MESSAGETYPE_ERROR);
                txtMeterNo.Text = string.Empty;
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
            return IsOldMeterExists;
        }
        #endregion
    }
}