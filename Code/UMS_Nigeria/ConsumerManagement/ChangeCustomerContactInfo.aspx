﻿<%@ Page Title=":: Change Customer Contact Info ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="ChangeCustomerContactInfo.aspx.cs"
    Inherits="UMS_Nigeria.ConsumerManagement.ChangeCustomerContactInfo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor" id="divContactDetails" runat="server">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upSearchCustomer" DefaultButton="btnSearch" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div class="inner-sec">
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litSearchCustomer" runat="server" Text="Change Customer Contact Information"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litAccountNo" runat="server" Text="<%$ Resources:Resource, ACCNO_OLD_ACCNO%>"></asp:Literal>
                                    <span class="span_star">*</span></label>
                                <div class="space">
                                </div>
                                <asp:TextBox ID="txtAccountNo" CssClass="text-box" runat="server" MaxLength="15"
                                    placeholder="Global Account No / Old Account No"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtAccountNo"
                                    FilterType="Numbers" ValidChars=" ">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanAccNo" class="span_color"></span>
                            </div>
                        </div>
                        <div class="box_total">
                            <div class="box_total_a">
                                <div class="search" style="padding-left: 7px;">
                                    <asp:Button ID="btnGo" OnClientClick="return Validate();" Text="<%$ Resources:Resource, GET_CUSTOMER_DETAILS%>"
                                        CssClass="box_s" OnClick="btnGo_Click" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="out-bor_a" id="divdetails" runat="server">
                    <div class="inner-sec">
                        <div class="heading" style="padding-left: 16px; padding-top: 17px;">
                            <asp:Literal ID="Literal213" runat="server" Text="<%$ Resources:Resource, CUSTOMER_DETAILS%>"></asp:Literal>
                        </div>
                        <div class="out-bor_cTwo">
                            <div class="inner-box">
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblGlobalAccNoAndAccNo" runat="server" Text="--"></asp:Label>
                                        <asp:Label ID="lblGlobalAccountNo" Visible="false" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <%--<div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal3" runat="server" Text="Account Number"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblAccountNo" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>--%>
                                <div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblOldAccNo" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblName" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <%--<div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal15" runat="server" Text="Service Address"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblServiceAddress" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>--%>
                                <div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal16" runat="server" Text="Email-Id"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblEmailId" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal17" runat="server" Text="Home"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblHome" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal7" runat="server" Text="Business"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblBusiness" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal2" runat="server" Text="Others"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblOthers" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="litOutstandingAmount" runat="server" Text="<%$ Resources:Resource, OUT_STANDING_AMT%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblOutstandingAmount" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="text-heading" style="margin-bottom: 9px; margin-left: 17px;">
                            <asp:Literal ID="litPostalAddress" runat="server" Text="Edit Contact Details"></asp:Literal>
                        </div>
                        <div>
                            <div class="inner-box1">
                                <div class="customerDetailstext">
                                    <label for="name">
                                        <asp:Literal ID="litOEmailId" runat="server" Text="<%$ Resources:Resource, EMAIL_ID%>"></asp:Literal></label>
                                    <br />
                                    <asp:TextBox ID="txtOEmailId" runat="server" TabIndex="1" CssClass="text-box" placeholder="Enter EmailId"
                                        AutoPostBack="true" onblur=" OnlyEmailValidation(this,'spanOEmailId','Email Id')"></asp:TextBox><%--OnTextChanged="txtOEmailId_TextChanged"--%>
                                    <br />
                                    <span id="spanOEmailId" class="span_color"></span><span id="spanOEMailExists" runat="server">
                                    </span>
                                </div>
                            </div>
                            <div class="inner-box2">
                                <div class="customerDetailstext">
                                    <label for="name">
                                        <asp:Literal ID="litOHPhone" runat="server" Text="<%$ Resources:Resource, HOME%>"></asp:Literal>
                                    </label>
                                    <br />
                                    <asp:TextBox ID="txtOHPhoneCode" Width="10%" runat="server" TabIndex="2" CssClass="text-box"
                                        placeholder="Code" MaxLength="3"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtOHPhoneCode"
                                        FilterType="Numbers">
                                    </asp:FilteredTextBoxExtender>
                                    <asp:TextBox ID="txtOHPhone" Width="40%" runat="server" TabIndex="3" CssClass="text-box"
                                        placeholder="Enter Home Phone" MaxLength="10"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="txtOHPhone"
                                        FilterType="Numbers">
                                    </asp:FilteredTextBoxExtender>
                                    <span id="spanOHPhone" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box3">
                                <div class="customerDetailstext">
                                    <label for="name">
                                        <asp:Literal ID="litOBPhone" runat="server" Text="<%$ Resources:Resource, BUSINESS%>"></asp:Literal></label>
                                    <br />
                                    <asp:TextBox ID="txtOBPhoneCode" Width="10%" runat="server" TabIndex="4" CssClass="text-box"
                                        placeholder="Code" MaxLength="3"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="txtOBPhoneCode"
                                        FilterType="Numbers">
                                    </asp:FilteredTextBoxExtender>
                                    <asp:TextBox ID="txtOBPhone" Width="40%" runat="server" TabIndex="5" CssClass="text-box"
                                        placeholder="Enter Business Phone" MaxLength="10"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="txtOBPhone"
                                        FilterType="Numbers">
                                    </asp:FilteredTextBoxExtender>
                                    <span id="spanOBPhone" class="span_color"></span>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <br />
                            <div class="inner-box1">
                                <div class="customerDetailstext">
                                    <label for="name">
                                        <asp:Literal ID="litOOPhone" runat="server" Text="<%$ Resources:Resource, OTHERS%>"></asp:Literal></label>
                                    <br />
                                    <asp:TextBox ID="txtOOPhoneCode" Width="10%" runat="server" TabIndex="6" CssClass="text-box"
                                        placeholder="Code" MaxLength="3"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="txtOOPhoneCode"
                                        FilterType="Numbers">
                                    </asp:FilteredTextBoxExtender>
                                    <asp:TextBox ID="txtOOPhone" Width="40%" runat="server" TabIndex="7" CssClass="text-box"
                                        placeholder="Enter Others Phone" MaxLength="10"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" TargetControlID="txtOOPhone"
                                        FilterType="Numbers">
                                    </asp:FilteredTextBoxExtender>
                                    <span id="spanOOPhone" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box3">
                                <div class="customerDetailstext">
                                    <label for="name">
                                        <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, REASON%>"></asp:Literal>
                                        <span class="span_star">*</span></label><div class="space">
                                        </div>
                                    <asp:TextBox ID="txtReason" CssClass="text-box" runat="server" TextMode="MultiLine"
                                        placeholder="Reason" TabIndex="8" onblur="return TextBoxBlurValidationlblZeroAccepts(this,'spanReason','Reason')"></asp:TextBox>
                                    <span id="spanReason" class="span_color"></span>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div class="inner-box1">
                                <div class="search" style="margin-top: 19px; margin-left: -9px; margin-bottom: 17px;">
                                    <asp:Button ID="btnSave" TabIndex="9" Text="<%$ Resources:Resource, UPDATE%>" CssClass="box_s"
                                        OnClick="btnUpdate_Click" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="rnkland">
                    <asp:ModalPopupExtender ID="mpeCofirm" runat="server" PopupControlID="PanelCofirm"
                        TargetControlID="btnCofirm" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnCofirm" runat="server" Text="Button" CssClass="popbtn" />
                    <asp:Panel ID="PanelCofirm" runat="server" DefaultButton="btnYes" CssClass="modalPopup"
                        class="poppnl" Style="display: none;">
                        <div class="popheader popheaderlblred" id="popup" runat="server">
                            <asp:Label ID="lblConfirmMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnYes" runat="server" OnClientClick="return closePopup();" OnClick="btnYes_Click"
                                    Text="<%$ Resources:Resource, YES%>" CssClass="ok_btn" />
                                <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, NO%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Confirm popup Start--%>
                    <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                        TargetControlID="btnActivate" BackgroundCssClass="modalBackground">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelActivate" runat="server" DefaultButton="btnActiveCancel" CssClass="modalPopup"
                        Style="display: none; border-color: #639B00;">
                        <div class="popheader" style="background-color: #639B00;">
                            <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                        </div>
                        <div class="space">
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Confirm popup Closed--%>
                </div>
                <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="PanelAlert"
                    TargetControlID="btnAlertDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnAlertOk">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnAlertDeActivate" runat="server" Text="Button" CssClass="popbtn" />
                <asp:Panel ID="PanelAlert" runat="server" CssClass="modalPopup" class="poppnl" Style="display: none;">
                    <div class="popheader popheaderlblred" id="pop" runat="server">
                        <asp:Label ID="lblAlertMsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <div class="space">
                            </div>
                            <asp:Button ID="btnAlertOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%--Popus Message Starts--%>
                <asp:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="hfMessage"
                    PopupControlID="pnlMessageValidation" BackgroundCssClass="popbg" CancelControlID="btnMessageOk">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hfMessage" runat="server" />
                <asp:Panel ID="pnlMessageValidation" runat="server" DefaultButton="" CssClass="modalPopup"
                    Style="display: none; border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblMessageValidation" runat="server"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnMessageOk" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%--Popus Pole Ends--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".rnkland").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../Javascript/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/ChangeCustomerContactInfo.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            //AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);

        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            //AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);
        });
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
