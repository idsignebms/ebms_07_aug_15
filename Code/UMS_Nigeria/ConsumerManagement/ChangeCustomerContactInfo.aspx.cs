﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for ChangeCustomerName
                     
 Developer        : id065-Bhimaraju Vanka
 Creation Date    : 06-10-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using iDSignHelper;
using UMS_NigeriaBAL;
using Resources;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Data;
using System.Configuration;
using System.Text;
using System.Xml;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class ChangeCustomerContactInfo : System.Web.UI.Page
    {
        # region Members

        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        ChangeBookNoBal _objBookNoBal = new ChangeBookNoBal();
        ReportsBal objReportsBal = new ReportsBal();
        ConsumerBal objConsumerBal = new ConsumerBal();
        string Key = "Customer Contact Info Change";

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                lblMessage.Text = pnlMessage.CssClass = string.Empty;
                if (!IsPostBack)
                {
                    BindMandatory();
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }

        protected void txtOEmailId_TextChanged(object sender, EventArgs e)
        {
            //IsEmailAlreadyExists(txtOEmailId, true);
        }

        protected void btnGo_Click1(object sender, EventArgs e)
        {
            RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
            objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                objLedgerBe.BUID = string.Empty;

            objLedgerBe = _objiDsignBal.DeserializeFromXml<RptCustomerLedgerBe>(objReportsBal.GetLedger(objLedgerBe, ReturnType.Single));

            if (objLedgerBe.IsSuccess)
            {
                if (objLedgerBe.IsCustExistsInOtherBU)
                {
                    lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                    mpeAlert.Show();
                }
                else
                {
                    ChangeContactBE _objChangeContactBE = new ChangeContactBE();
                    _objChangeContactBE.GlobalAccountNumber = txtAccountNo.Text.Trim();
                    _objChangeContactBE = _objiDsignBal.DeserializeFromXml<ChangeContactBE>(_objBookNoBal.GetDetails(_objChangeContactBE, ReturnType.Get));

                    if (!_objChangeContactBE.IsAccountNoNotExists)
                    {
                        divdetails.Attributes.Add("style", "display:block;");
                        lblGlobalAccNoAndAccNo.Text = _objChangeContactBE.GlobalAccNoAndAccountNo;
                        lblGlobalAccountNo.Text = _objChangeContactBE.GlobalAccountNumber;
                        //lblAccountNo.Text = _objChangeContactBE.AccountNo;
                        lblOldAccNo.Text = _objChangeContactBE.OldAccountNo;
                        lblName.Text = _objChangeContactBE.Name;
                        //lblServiceAddress.Text = _objChangeContactBE.ServiceAddress;
                        lblEmailId.Text = txtOEmailId.Text = _objChangeContactBE.OldEmailId;
                        lblHome.Text = _objChangeContactBE.OldHomeContactNumber;
                        lblBusiness.Text = _objChangeContactBE.OldBusinessContactNumber;
                        lblOthers.Text = _objChangeContactBE.OldOtherContactNumber;
                        lblOutstandingAmount.Text = _objCommonMethods.GetCurrencyFormat(_objChangeContactBE.OutStandingAmount, 2, Constants.MILLION_Format);

                        txtOHPhoneCode.Text = _objChangeContactBE.OldHomeContactNumber.Split('-').First();
                        txtOHPhone.Text = _objChangeContactBE.OldHomeContactNumber.Split('-').Last();

                        txtOOPhoneCode.Text = _objChangeContactBE.OldOtherContactNumber.Split('-').First();
                        txtOOPhone.Text = _objChangeContactBE.OldOtherContactNumber.Split('-').Last();

                        txtOBPhoneCode.Text = _objChangeContactBE.OldOtherContactNumber.Split('-').First();
                        txtOBPhone.Text = _objChangeContactBE.OldBusinessContactNumber.Split('-').Last();
                    }
                    else
                    {
                        CustomerNotFound();
                    }
                }
            }
            else
            {
                CustomerNotFound();
            }
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            XmlDocument xml = new XmlDocument();
            RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
            ChangeCusotmerTypeBAL ObjChangeCusotmerTypeBAL = new ChangeCusotmerTypeBAL();

            objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                objLedgerBe.BUID = string.Empty;

            xml = _objBookNoBal.GetCustDetForContactInfoByAccno_ByCheck(objLedgerBe);
            if (xml.SelectSingleNode("/*").Name == "RptCustomerLedgerBe")
            {
                objLedgerBe = _objiDsignBal.DeserializeFromXml<RptCustomerLedgerBe>(xml);
                if (objLedgerBe.IsSuccess)
                {
                    if (objLedgerBe.IsCustExistsInOtherBU)
                    {
                        lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                        mpeAlert.Show();
                    }
                    else if (objLedgerBe.ActiveStatusId == (int)CustomerStatus.Hold)
                    {
                        lblAlertMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.Hold, "Contact information change");
                        mpeAlert.Show();
                    }
                    else if (objLedgerBe.ActiveStatusId == (int)CustomerStatus.Closed)
                    {
                        lblAlertMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.Closed, "Contact information change");
                        mpeAlert.Show();
                    }
                }
                else
                {
                    CustomerNotFound();
                }
            }
            else if (xml.SelectSingleNode("/*").Name == "ChangeContactBE")
            {
                ChangeContactBE _objChangeContactBE = new ChangeContactBE();
                _objChangeContactBE = _objiDsignBal.DeserializeFromXml<ChangeContactBE>(xml);
                divdetails.Attributes.Add("style", "display:block;");
                lblGlobalAccNoAndAccNo.Text = _objChangeContactBE.GlobalAccNoAndAccountNo;
                lblGlobalAccountNo.Text = _objChangeContactBE.GlobalAccountNumber;
                //lblAccountNo.Text = _objChangeContactBE.AccountNo;
                lblOldAccNo.Text = _objChangeContactBE.OldAccountNo;
                lblName.Text = _objChangeContactBE.Name;
                //lblServiceAddress.Text = _objChangeContactBE.ServiceAddress;
                lblEmailId.Text = txtOEmailId.Text = _objChangeContactBE.OldEmailId;
                lblHome.Text = _objChangeContactBE.OldHomeContactNumber;
                lblBusiness.Text = _objChangeContactBE.OldBusinessContactNumber;
                lblOthers.Text = _objChangeContactBE.OldOtherContactNumber;
                lblOutstandingAmount.Text = _objCommonMethods.GetCurrencyFormat(_objChangeContactBE.OutStandingAmount, 2, Constants.MILLION_Format);

                txtOHPhoneCode.Text = _objChangeContactBE.OldHomeContactNumber.Split('-').First();
                txtOHPhone.Text = _objChangeContactBE.OldHomeContactNumber.Split('-').Last();

                txtOOPhoneCode.Text = _objChangeContactBE.OldOtherContactNumber.Split('-').First();
                txtOOPhone.Text = _objChangeContactBE.OldOtherContactNumber.Split('-').Last();

                txtOBPhoneCode.Text = _objChangeContactBE.OldOtherContactNumber.Split('-').First();
                txtOBPhone.Text = _objChangeContactBE.OldBusinessContactNumber.Split('-').Last();
            }
        }

        private void CustomerNotFound()
        {
            divdetails.Attributes.Add("style", "display:none;");
            txtAccountNo.Text = string.Empty;
            Message(UMS_Resource.CUSTOMER_NOT_FOUND_MSG, UMS_Resource.MESSAGETYPE_ERROR);
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            //if (_objCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.ChangeCustomerName, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString())))
            //{
            lblConfirmMsg.Text = "Are you sure you want to change the contact details of this Customer?";
            mpeCofirm.Show();
            //}
            //else
            //{
            //ChangeContactBE _objChangeContactBE = new ChangeContactBE();
            //_objChangeContactBE.GlobalAccountNumber = lblGlobalAccountNo.Text;
            //_objChangeContactBE.NewEmailId = txtOEmailId.Text;
            //_objChangeContactBE.NewHomeContactNumber = txtOHPhoneCode.Text + "-" + txtOHPhone.Text;
            //_objChangeContactBE.NewBusinessContactNumber = txtOBPhoneCode.Text + "-" + txtOBPhone.Text;
            //_objChangeContactBE.NewOtherContactNumber = txtOOPhoneCode.Text + "-" + txtOOPhone.Text;
            //_objChangeContactBE.Details = _objiDsignBal.ReplaceNewLines(txtReason.Text, true);
            //_objChangeContactBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            //_objChangeContactBE.ApprovalStatusId = 2;//(int)EnumApprovalStatus.Process;
            //_objChangeContactBE.FunctionId = 0;//(int)EnumApprovalFunctions.ChangeCustomerName;
            //_objChangeContactBE.IsFinalApproval = true;

            //_objChangeContactBE = _objiDsignBal.DeserializeFromXml<ChangeContactBE>(_objBookNoBal.InsertDetails(_objChangeContactBE, Statement.Insert));

            //if (_objChangeContactBE.IsSuccess)
            //{
            //    lblActiveMsg.Text = Resource.APPROVAL_TARIFF_CHANGE;
            //    divdetails.Visible = false;
            //    txtOEmailId.Text = txtOHPhoneCode.Text = txtOHPhone.Text = txtOBPhoneCode.Text = txtOBPhone.Text = string.Empty;
            //    txtOOPhoneCode.Text = txtOOPhone.Text = txtReason.Text = string.Empty;
            //    mpeActivate.Show();
            //}
            //else if (_objChangeContactBE.IsApprovalInProcess)
            //{
            //    divdetails.Visible = false;
            //    txtOEmailId.Text = txtOHPhoneCode.Text = txtOHPhone.Text = txtOBPhoneCode.Text = txtOBPhone.Text = string.Empty;
            //    txtOOPhoneCode.Text = txtOOPhone.Text = txtReason.Text = string.Empty;
            //    lblActiveMsg.Text = Resource.REQ_STILL_PENDING;
            //    mpeActivate.Show();
            //}
            //else
            //{
            //    divdetails.Visible = true;
            //    Message(UMS_Resource.CUSTOMER_UPDATED_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            //}
            //}
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ChangeContactBE _objChangeContactBE = new ChangeContactBE();
                _objChangeContactBE.GlobalAccountNumber = lblGlobalAccountNo.Text;
                _objChangeContactBE.NewEmailId = txtOEmailId.Text;
                _objChangeContactBE.NewHomeContactNumber = txtOHPhoneCode.Text + "-" + txtOHPhone.Text;
                _objChangeContactBE.NewBusinessContactNumber = txtOBPhoneCode.Text + "-" + txtOBPhone.Text;
                _objChangeContactBE.NewOtherContactNumber = txtOOPhoneCode.Text + "-" + txtOOPhone.Text;
                _objChangeContactBE.Details = _objiDsignBal.ReplaceNewLines(txtReason.Text, true);
                _objChangeContactBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objChangeContactBE.ApprovalStatusId = 2;//(int)EnumApprovalStatus.Process;
                _objChangeContactBE.FunctionId = 0;//(int)EnumApprovalFunctions.ChangeCustomerName;
                _objChangeContactBE.IsFinalApproval = true;
                _objChangeContactBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                _objChangeContactBE = _objiDsignBal.DeserializeFromXml<ChangeContactBE>(_objBookNoBal.InsertDetails(_objChangeContactBE, Statement.Insert));

                if (_objChangeContactBE.IsSuccess)
                {
                    lblActiveMsg.Text = Resource.MODIFICATIONS_UPDATED;
                    divdetails.Attributes.Add("style", "display:none;");
                    txtOEmailId.Text = txtOHPhoneCode.Text = txtOHPhone.Text = txtOBPhoneCode.Text = txtOBPhone.Text = string.Empty;
                    txtOOPhoneCode.Text = txtOOPhone.Text = txtReason.Text = txtAccountNo.Text = string.Empty;
                    mpeActivate.Show();
                }
                else
                {
                    divdetails.Attributes.Add("style", "display:block;");
                    Message(UMS_Resource.CUSTOMER_UPDATED_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        private void BindMandatory()
        {
            DataSet dsControls = new DataSet();
            dsControls.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["NewCustomer_Mandatory_Fields"]));
            StringBuilder jscript = new StringBuilder();

            ControlCollection Pagecontrols = null;

            jscript.Append("<script type=" + "'text/javascript'" + ">");
            jscript.Append("function FinalValidate(){");
            jscript.Append("var IsValid = true;");

            Pagecontrols = divdetails.Controls;

            for (int i = 0; i < Pagecontrols.Count; i++)
            {
                foreach (DataRow dr in dsControls.Tables[0].Rows)
                {
                    if (Pagecontrols[i].ID == dr["ControlId"].ToString())
                    {
                        if (Convert.ToBoolean(dr["IsMandatory"]))
                        {
                            Literal LiteralId = (Literal)divContactDetails.FindControl(dr["LiteralId"].ToString());
                            LiteralId.Text += "<span class=" + "span_star" + ">*</span>";
                            #region If TextBox
                            if (Pagecontrols[i].GetType().Name.ToString() == "TextBox")
                            {
                                TextBox txt = (TextBox)Pagecontrols[i];
                                if (txt.ID == "txtOEmailId" || txt.ID == "txtTEmailId")
                                {
                                    //txt.Attributes.Add("onblur", "emailValidation(this,'" + dr["SpanId"] + "','" + dr["SpanText"] + "')");
                                    txt.Attributes.Add("onblur", "emailValidation(this,'" + dr["SpanId"] + "','" + dr["SpanText"] + "')");
                                    jscript.Append("if(emailValidation(document.getElementById('UMSNigeriaBody_" + dr["ControlId"] + "'),'" + dr["SpanId"] + "','" + dr["SpanText"] + "') == false) IsValid = false;");
                                }
                            }
                            #endregion
                        }
                        break;
                    }
                }
            }

            jscript.Append("if (document.getElementById('UMSNigeriaBody_txtOEmailId').value != '') { if (OnlyEmailValidation(document.getElementById('UMSNigeriaBody_txtOEmailId'),'spanOEmailId','Please Enter Valid Email Id')==false){ IsValid = false; } }");
            jscript.Append("if (document.getElementById('UMSNigeriaBody_txtOHPhone').value != '' || document.getElementById('UMSNigeriaBody_txtOHPhoneCode').value != '') { if (CustomerContactNoLengthMinMax(document.getElementById('UMSNigeriaBody_txtOHPhone'),6,10,document.getElementById('spanOHPhone'),document.getElementById('UMSNigeriaBody_txtOHPhoneCode'),3)==false){ IsValid = false; } }");
            jscript.Append("if (document.getElementById('UMSNigeriaBody_txtOBPhone').value != '' || document.getElementById('UMSNigeriaBody_txtOBPhoneCode').value != '') { if (CustomerContactNoLengthMinMax(document.getElementById('UMSNigeriaBody_txtOBPhone'),6,10,document.getElementById('spanOBPhone'),document.getElementById('UMSNigeriaBody_txtOBPhoneCode'),3)==false){ IsValid = false; } }");
            jscript.Append("if (document.getElementById('UMSNigeriaBody_txtOOPhone').value != '' || document.getElementById('UMSNigeriaBody_txtOOPhoneCode').value != '') { if (CustomerContactNoLengthMinMax(document.getElementById('UMSNigeriaBody_txtOOPhone'),6,10,document.getElementById('spanOOPhone'),document.getElementById('UMSNigeriaBody_txtOOPhoneCode'),3)==false){ IsValid = false; } }");
            jscript.Append("if (document.getElementById('UMSNigeriaBody_txtReason').value == '') {  IsValid = false; }");
            jscript.Append("document.getElementById('UMSNigeriaBody_spanOEMailExists').innerHTML ='';");
            jscript.Append("if(TextBoxValidationlbl(document.getElementById('UMSNigeriaBody_txtReason'), document.getElementById('spanReason'),'Resason') == false) { IsValid = false;}");

            jscript.Append("if (!IsValid){document.getElementById('UMSNigeriaBody_lblMessageValidation').innerHTML = 'Please enter all mandatory fields.'; ShowMessage()}");
            jscript.Append("if (!IsValid){return false;}");
            jscript.Append("}</script>");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ConfirmSubmit", jscript.ToString(), false);
            btnSave.Attributes.Add("onclick", "return FinalValidate()");
            divdetails.Attributes.Add("style", "display: none;");
        }

        public void IsEmailAlreadyExists(TextBox emailID, bool IsLandLoardEmail)
        {
            CustomerRegistrationBE objCustomerRegistrationBE = new CustomerRegistrationBE();
            objCustomerRegistrationBE.EmailID = emailID.Text.Trim();
            objCustomerRegistrationBE = _objiDsignBal.DeserializeFromXml<CustomerRegistrationBE>(objConsumerBal.IsEmailExists(objCustomerRegistrationBE));
            if (objCustomerRegistrationBE.IsSuccessful)
            {
                if (IsLandLoardEmail)
                {
                    spanOEMailExists.InnerText = string.Empty;
                    spanOEMailExists.InnerText = emailID.Text + " Email already Exists.";
                    spanOEMailExists.Attributes.Add("class", "span_color");
                    emailID.Text = string.Empty;
                }
            }
            else
            {
                if (IsLandLoardEmail)
                {
                    spanOEMailExists.InnerText = "You can use this EmailId.";
                    spanOEMailExists.Attributes.Add("Display", "Bloack");
                    spanOEMailExists.Attributes.Add("class", "span_color_green");
                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

        #region Culture

        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        [System.Web.Services.WebMethod]
        public static bool IsEmailExists(string EmailId)
        {
            bool IsExists = false;
            iDsignBAL _objiDsignBal = new iDsignBAL();
            ConsumerBal objConsumerBal = new ConsumerBal();
            CustomerRegistrationBE objCustomerRegistrationBE = new CustomerRegistrationBE();
            objCustomerRegistrationBE.EmailID = EmailId.Trim();
            objCustomerRegistrationBE = _objiDsignBal.DeserializeFromXml<CustomerRegistrationBE>(objConsumerBal.IsEmailExists(objCustomerRegistrationBE));
            if (objCustomerRegistrationBE.IsSuccessful)
            {
                IsExists = true;
            }
            return IsExists;
        }
    }
}