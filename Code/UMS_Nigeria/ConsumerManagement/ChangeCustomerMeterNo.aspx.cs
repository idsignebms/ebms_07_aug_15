﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for ChangeCustomerMeterNo
                     
 Developer        : id065-Bhimaraju Vanka
 Creation Date    : 07-10-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using iDSignHelper;
using UMS_NigeriaBAL;
using Resources;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Xml;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class ChangeCustomerMeterNo : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        ChangeBookNoBal _objBookNoBal = new ChangeBookNoBal();
        ReportsBal objReportsBal = new ReportsBal();
        string Key = UMS_Resource.CHNG_CUST_MTRNO;
        #endregion

        #region Properties
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                lblMessage.Text = pnlMessage.CssClass = string.Empty;
                if (!IsPostBack)
                {
                    string path = string.Empty;
                    path = _objCommonMethods.GetPagePath(this.Request.Url);
                    if (_objCommonMethods.IsAccess(path))
                    {

                    }
                    else
                    {
                        Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    }
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }
        protected void btnGo_Click(object sender, EventArgs e)
        {
            RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
            objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                objLedgerBe.BUID = string.Empty;
            objLedgerBe = _objiDsignBal.DeserializeFromXml<RptCustomerLedgerBe>(objReportsBal.GetLedger(objLedgerBe, ReturnType.Set));
            if (objLedgerBe.IsSuccess)
            {
                if (objLedgerBe.IsCustExistsInOtherBU)
                {
                    divdetails.Visible = false;
                    popmpeActivate.Attributes.Add("class", "popheader popheaderlblred");
                    lblActiveMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                    mpeActivate.Show();
                }
                else if (objLedgerBe.CustIsNotActive)
                {
                    divdetails.Visible = false;
                    popmpeActivate.Attributes.Add("class", "popheader popheaderlblred");
                    if (objLedgerBe.ActiveStatusId == 2)
                        lblActiveMsg.Text = "Customer status is Inactive.";
                    else if (objLedgerBe.ActiveStatusId == 3)
                        lblActiveMsg.Text = "Customer status is Hold.";
                    else if (objLedgerBe.ActiveStatusId == 4)
                        lblActiveMsg.Text = "Customer status is Closed.";
                    else
                        lblActiveMsg.Text = "Customer is not Active.";
                    mpeActivate.Show();
                }
                else if (objLedgerBe.IsDisabledBook)
                {
                    divdetails.Visible = false;
                    popmpeActivate.Attributes.Add("class", "popheader popheaderlblred");
                    lblActiveMsg.Text = "Customer Book Number is Disabled.";
                    mpeActivate.Show();
                }
                else
                {
                    ChangeBookNoBe _objBookNoBe = new ChangeBookNoBe();
                    _objBookNoBe.AccountNo = txtAccountNo.Text.Trim();
                    _objBookNoBe.Flag = 3;
                    _objBookNoBe = _objiDsignBal.DeserializeFromXml<ChangeBookNoBe>(_objBookNoBal.Get(_objBookNoBe, ReturnType.Get));

                    if (_objBookNoBe.IsDirectCustomer)
                    {
                        divdetails.Visible = false;
                        popmpeActivate.Attributes.Add("class", "popheader popheaderlblred");
                        lblActiveMsg.Text = Resource.METER_DIRECT_CUST_ACC_NO;
                        mpeActivate.Show();
                    }
                    else if (_objBookNoBe.IsAccountNoNotExists)
                    {
                        CustomerNotFound();
                    }
                    else
                    {
                        divdetails.Visible = true;
                        lblGlobalAccountNumber.Text = _objBookNoBe.GlobalAccountNumber;
                        lblGlobalAccNoAndAccNo.Text = _objBookNoBe.GlobalAccNoAndAccNo;
                        lblOldAccNo.Text = _objBookNoBe.OldAccountNo;
                        lblName.Text = _objBookNoBe.Name;
                        lblTariff.Text = _objBookNoBe.Tariff;
                        lblMeterNo.Text = _objBookNoBe.MeterNo;
                        //lblAccountNo.Text = _objBookNoBe.AccountNo;
                        lblLastReadDate.Text = _objBookNoBe.PreviousReadingDate;
                        lblLastReading.Text = _objBookNoBe.PreviousReading;
                        lblLastReadType.Text = _objBookNoBe.LastReadType;
                        lblAverageUsage.Text = _objBookNoBe.AverageUsage;
                        lblOutstandingAmount.Text = _objCommonMethods.GetCurrencyFormat(_objBookNoBe.OutStandingAmount, 2, 2);
                        txtOldMeterReading.MaxLength = Convert.ToInt32(_objBookNoBe.MeterDials);
                        lblAverageDailyConsumption.Text = _objBookNoBe.AverageDailyUsage;
                    }
                }
            }
            else
            {
                CustomerNotFound();
            }
            txtMeterChangedDate.Text = txtReadingDate.Text = txtMeterInitialReading.Text = txtOldMeterReading.Text = txtInitialReading.Text = txtReason.Text = txtMeterNo.Text = string.Empty;
        }
        protected void btnGo_Click1(object sender, EventArgs e)
        {
            XmlDocument xml = new XmlDocument();
            XmlDocument XmlCustomerRegistrationBE = new XmlDocument();
            RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
            ChangeBookNoBal objChangeBookNoBal = new ChangeBookNoBal();
            ChangeBookNoBe _objBookNoBe = new ChangeBookNoBe();
            objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
            objLedgerBe.Flag = 3;
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                objLedgerBe.BUID = string.Empty;

            xml = objReportsBal.GetCustomerDetails_ByCheck(objLedgerBe);
            if (xml.SelectSingleNode("/*").Name == "RptCustomerLedgerBe")
            {
                objLedgerBe = _objiDsignBal.DeserializeFromXml<RptCustomerLedgerBe>(xml);
                if (objLedgerBe.IsSuccess)
                {
                    if (objLedgerBe.IsCustExistsInOtherBU)
                    {
                        divdetails.Visible = false;
                        txtAccountNo.Text = string.Empty;
                        popmpeActivate.Attributes.Add("class", "popheader popheaderlblred");
                        lblActiveMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                        mpeActivate.Show();
                    }
                    else if (objLedgerBe.ActiveStatusId == (int)CustomerStatus.Hold)
                    {
                        divdetails.Visible = false;
                        txtAccountNo.Text = string.Empty;
                        popmpeActivate.Attributes.Add("class", "popheader popheaderlblred");
                        lblActiveMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.Hold, "Meter change");
                        mpeActivate.Show();
                    }
                    else if (objLedgerBe.ActiveStatusId == (int)CustomerStatus.Closed)
                    {
                        divdetails.Visible = false;
                        txtAccountNo.Text = string.Empty;
                        popmpeActivate.Attributes.Add("class", "popheader popheaderlblred");
                        lblActiveMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.Closed, "Meter change");
                        mpeActivate.Show();
                    }
                }
                else
                {
                    CustomerNotFound();
                }
            }
            else if (xml.SelectSingleNode("/*").Name == "ChangeBookNoBe")
            {
                _objBookNoBe = _objiDsignBal.DeserializeFromXml<ChangeBookNoBe>(xml);
                divdetails.Visible = true;
                lblGlobalAccountNumber.Text = _objBookNoBe.GlobalAccountNumber;
                lblGlobalAccNoAndAccNo.Text = _objBookNoBe.GlobalAccNoAndAccNo;
                lblOldAccNo.Text = _objBookNoBe.OldAccountNo;
                lblName.Text = _objBookNoBe.Name;
                lblTariff.Text = _objBookNoBe.Tariff;
                lblMeterNo.Text = _objBookNoBe.MeterNo;
                //lblAccountNo.Text = _objBookNoBe.AccountNo;
                lblLastReadDate.Text = _objBookNoBe.PreviousReadingDate;
                lblLastReading.Text = _objBookNoBe.PreviousReading;
                lblLastReadType.Text = _objBookNoBe.LastReadType;
                lblAverageUsage.Text = _objBookNoBe.AverageUsage;
                lblOutstandingAmount.Text = _objCommonMethods.GetCurrencyFormat(_objBookNoBe.OutStandingAmount, 2, 2);
                txtOldMeterReading.MaxLength = Convert.ToInt32(_objBookNoBe.MeterDials);
                lblAverageDailyConsumption.Text = _objBookNoBe.AverageDailyUsage;
            }
        }
        //protected void btnGo_Click(object sender, EventArgs e)
        //{
        //    XmlDocument xml = new XmlDocument();
        //    XmlDocument XmlCustomerRegistrationBE = new XmlDocument();
        //    RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
        //    ChangeBookNoBal objChangeBookNoBal = new ChangeBookNoBal();
        //    ChangeBookNoBe _objBookNoBe = new ChangeBookNoBe();
        //    objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
        //    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
        //        objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
        //    else
        //        objLedgerBe.BUID = string.Empty;

        //    xml = objReportsBal.GetCustomerDetails_ByCheck(objLedgerBe);
        //    if (xml.SelectSingleNode("/*").Name == "RptCustomerLedgerBe")
        //    {
        //        objLedgerBe = _objiDsignBal.DeserializeFromXml<RptCustomerLedgerBe>(xml);
        //        if (objLedgerBe.IsSuccess)
        //        {
        //            if (objLedgerBe.IsCustExistsInOtherBU)
        //            {
        //                lblActiveMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
        //                mpeActivate.Show();
        //            }
        //            else if (objLedgerBe.ActiveStatusId == (int)CustomerStatus.InActive)
        //            {
        //                lblActiveMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.InActive, "Meter No Change");
        //                mpeActivate.Show();
        //            }
        //            else if (objLedgerBe.ActiveStatusId == (int)CustomerStatus.Hold)
        //            {
        //                lblActiveMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.Hold, "Meter No Change");
        //                mpeActivate.Show();
        //            }
        //            else if (objLedgerBe.ActiveStatusId == (int)CustomerStatus.Closed)
        //            {
        //                lblActiveMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.Closed, "Meter No Change");
        //                mpeActivate.Show();
        //            }
        //        }
        //        else
        //        {
        //            CustomerNotFound();
        //        }
        //    }
        //    else if (xml.SelectSingleNode("/*").Name == "ChangeBookNoBe")
        //    {
        //        _objBookNoBe = _objiDsignBal.DeserializeFromXml<ChangeBookNoBe>(xml);
        //        divdetails.Visible = true;
        //        lblGlobalAccountNumber.Text = _objBookNoBe.GlobalAccountNumber;
        //        lblOldAccNo.Text = _objBookNoBe.OldAccountNo;
        //        lblName.Text = _objBookNoBe.Name;
        //        lblTariff.Text = _objBookNoBe.Tariff;
        //        lblMeterNo.Text = _objBookNoBe.MeterNo;
        //        //lblAccountNo.Text = _objBookNoBe.AccountNo;
        //        lblLastReadDate.Text = _objBookNoBe.PreviousReadingDate;
        //        lblLastReading.Text = _objBookNoBe.PreviousReading;
        //        lblLastReadType.Text = _objBookNoBe.LastReadType;
        //        lblAverageUsage.Text = _objBookNoBe.AverageUsage;
        //        txtOldMeterReading.MaxLength = Convert.ToInt32(_objBookNoBe.MeterDials);
        //    }
        //}
        protected void txtMeterNo_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtMeterNo.Text))
            {
                ConsumerBe objConsumerBe = new ConsumerBe();
                ConsumerBal objConsumerBal = new ConsumerBal();
                //txtMeterNo.Text = txtMeterNo.Text.TrimStart();
                //txtMeterNo.Text = txtMeterNo.Text.Trim();
                objConsumerBe.MeterNo = txtMeterNo.Text.Trim();

                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    objConsumerBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    objConsumerBe.BU_ID = string.Empty;

                objConsumerBe = _objiDsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Retrieve(objConsumerBe, ReturnType.Multiple));

                if (objConsumerBe.IsSuccess)
                {
                    txtInitialReading.MaxLength = Convert.ToInt32(objConsumerBe.MeterDials);
                    txtMeterInitialReading.MaxLength = Convert.ToInt32(objConsumerBe.MeterDials);
                    txtMeterInitialReading.Text = txtInitialReading.Text = string.Empty;

                    if (objConsumerBe.IsCAPMIMeter)
                    {
                        popmpeActivate.Attributes.Add("class", "popheader popheaderlblgreen");
                        lblActiveMsg.Text = "This is CAPMI Meter. And CAPMI Amount is : " + _objCommonMethods.GetCurrencyFormat(objConsumerBe.MeterAmt, 2, Constants.MILLION_Format);
                        mpeActivate.Show();
                    }

                    //txtOldMeterReading.Focus();
                    //spanMeterNo.InnerText = "This meter no. is available.";
                    //spanMeterNo.Attributes.Add("class", "span_color_green");
                }
                else
                {
                    popmpeActivate.Attributes.Add("class", "popheader popheaderlblred");
                    lblActiveMsg.Text = UMS_Resource.METER_NO_NOT_EXISTS;
                    mpeActivate.Show();
                    //spanMeterNo.InnerText = "\"" + txtMeterNo.Text + "\" meter no. is not available.";
                    //spanMeterNo.Attributes.Add("class", "span_color");
                    //txtMeterNo.Text = string.Empty;
                }
            }
        }
        private void CustomerNotFound()
        {
            divdetails.Visible = false;
            txtAccountNo.Text = string.Empty;
            Message(UMS_Resource.CUSTOMER_NOT_FOUND_MSG, UMS_Resource.MESSAGETYPE_ERROR);
        }
        private string AppendZeros(string Reading, Int64 Dails)
        {
            string Str = "";
            if (Reading.Length < Dails)
            {
                for (int i = 0; i < (Dails - Reading.Length); i++)
                {
                    Str += "0";
                }
            }
            return Str + Reading;
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            ChangeBookNoBe _objBookNoBe = new ChangeBookNoBe();
            _objBookNoBe.AccountNo = lblGlobalAccountNumber.Text;
            if (!string.IsNullOrEmpty(txtInitialReading.Text))
                _objBookNoBe.InitialReading = AppendZeros(txtInitialReading.Text, Convert.ToInt64(txtInitialReading.MaxLength));
            else
                _objBookNoBe.InitialReading = string.Empty;
            _objBookNoBe.OldMeterReading = AppendZeros(txtOldMeterReading.Text, Convert.ToInt64(txtOldMeterReading.MaxLength));
            _objBookNoBe.OldMeterReadingDate = _objCommonMethods.Convert_MMDDYY(txtMeterChangedDate.Text);
            _objBookNoBe.NewMeterInitialReading = AppendZeros(txtMeterInitialReading.Text, Convert.ToInt64(txtMeterInitialReading.MaxLength));
            _objBookNoBe.MeterNo = txtMeterNo.Text.Trim();
            if (!string.IsNullOrEmpty(txtReadingDate.Text))
                _objBookNoBe.NewMeterReadingDate = _objCommonMethods.Convert_MMDDYY(txtReadingDate.Text);
            else
                _objBookNoBe.NewMeterReadingDate = string.Empty;

            _objBookNoBe.OldMeterNo = lblMeterNo.Text.Trim();
            _objBookNoBe.Details = _objiDsignBal.ReplaceNewLines(txtReason.Text, true);
            _objBookNoBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                _objBookNoBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                _objBookNoBe.BU_ID = string.Empty;

            //bool IsApproval = _objCommonMethods.IsApproval((int)EnumApprovalFunctions.ChangeCustomerMeterNo);
            bool IsFinalApproval = _objCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.ChangeCustomerMeterNo, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());
            if (IsFinalApproval)
            {
                _objBookNoBe.ApprovalStatusId = (int)EnumApprovalStatus.Approved;
                _objBookNoBe.IsFinalApproval = true;
                _objBookNoBe.Flag = 0;//For final approval or there is no approval levels
            }
            else
            {
                _objBookNoBe.Flag = 1;//if there is approval levels
            }

            _objBookNoBe.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerMeterNo;
            _objBookNoBe = _objiDsignBal.DeserializeFromXml<ChangeBookNoBe>(_objBookNoBal.Insert(_objBookNoBe, Statement.Edit));
            if (_objBookNoBe.IsSuccess)
            {
                txtMeterChangedDate.Text = txtReadingDate.Text = txtMeterInitialReading.Text = txtOldMeterReading.Text = txtInitialReading.Text = txtReason.Text = txtMeterNo.Text = txtAccountNo.Text = string.Empty;

                divdetails.Visible = false;
                popmpeActivate.Attributes.Add("class", "popheader popheaderlblgreen");
                if (IsFinalApproval)
                    lblActiveMsg.Text = Resource.MODIFICATIONS_UPDATED;
                else
                    lblActiveMsg.Text = Resource.APPROVAL_TARIFF_CHANGE;
                mpeActivate.Show();
            }
            else if (_objBookNoBe.IsNotCreditMeter)
            {
                popmpeActivate.Attributes.Add("class", "popheader popheaderlblred");
                lblActiveMsg.Text = "This Meter is not a Credit Meter, So please select Credit Meter.";
                mpeActivate.Show();
            }
            else if (_objBookNoBe.IsMeterNumberNotExists)
            {
                popmpeActivate.Attributes.Add("class", "popheader popheaderlblred");
                lblActiveMsg.Text = UMS_Resource.METER_NO_NOT_EXISTS;
                mpeActivate.Show();
            }
            else if (_objBookNoBe.IsMeterNoExists)
            {
                popmpeActivate.Attributes.Add("class", "popheader popheaderlblred");
                lblActiveMsg.Text = UMS_Resource.CUSTOMER_METERNO_EXISTS;
                mpeActivate.Show();
            }
            else if (_objBookNoBe.IsExist)
            {
                popmpeActivate.Attributes.Add("class", "popheader popheaderlblred");
                lblActiveMsg.Text = Resource.PREVIOUS_REQUEST_PENDING;
                mpeActivate.Show();
            }
            else if (_objBookNoBe.IsMeterNoAlreadyRaised)
            {
                popmpeActivate.Attributes.Add("class", "popheader popheaderlblred");
                lblActiveMsg.Text = Resource.MTR_RAISED_BY_ANOTHER;
                mpeActivate.Show();
            }
            else if (_objBookNoBe.IsOldMeterReadingIsWrong)
            {
                popmpeActivate.Attributes.Add("class", "popheader popheaderlblred");
                lblActiveMsg.Text = Resource.OLD_MTR_READ_WRNG;
                mpeActivate.Show();
            }
            else if (_objBookNoBe.IsReadingsExist)
            {
                popmpeActivate.Attributes.Add("class", "popheader popheaderlblred");
                lblActiveMsg.Text = "Meter Readings request is in process for this Customer.";
                mpeActivate.Show();
            }
            else if (_objBookNoBe.IsReadToDirectApproval)
            {
                popmpeActivate.Attributes.Add("class", "popheader popheaderlblred");
                lblActiveMsg.Text = "Meter Disconnect request is in process for this Customer.";
                mpeActivate.Show();
            }
            else if (_objBookNoBe.IsHavingCapmiAmt)
            {
                popmpeActivate.Attributes.Add("class", "popheader popheaderlblred");
                lblActiveMsg.Text = "Previous Meter is CAPMI Meter and Outstanding amount is not adjusted.";
                mpeActivate.Show();
            }
            else
            {
                divdetails.Visible = true;
                Message(UMS_Resource.CUSTOMER_UPDATED_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }
        #endregion

        #region Methods
        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}