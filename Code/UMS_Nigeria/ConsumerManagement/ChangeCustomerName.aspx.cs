﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for ChangeCustomerName
                     
 Developer        : id065-Bhimaraju Vanka
 Creation Date    : 06-10-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using iDSignHelper;
using UMS_NigeriaBAL;
using Resources;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Xml;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class ChangeCustomerName : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        ChangeBookNoBal _objBookNoBal = new ChangeBookNoBal();
        ApprovalBal objApprovalBal = new ApprovalBal();
        ReportsBal objReportsBal = new ReportsBal();
        string Key = UMS_Resource.CHNG_CUST_NAME;
        #endregion

        #region Properties
        private string MiddleName
        {
            get { return txtOMName.Text.Trim(); }
            set { txtOMName.Text = value; }
        }
        private string KnownAs
        {
            get { return txtOKnownAs.Text.Trim(); }
            set { txtOKnownAs.Text = value; }
        }
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                lblMessage.Text = pnlMessage.CssClass = string.Empty;
                if (!IsPostBack)
                {
                    string path = string.Empty;
                    path = _objCommonMethods.GetPagePath(this.Request.Url);
                    if (_objCommonMethods.IsAccess(path))
                    {

                    }
                    else
                    {
                        Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    }
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }
        protected void btnGo_Click1(object sender, EventArgs e)
        {
            RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
            objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                objLedgerBe.BUID = string.Empty;
            objLedgerBe = _objiDsignBal.DeserializeFromXml<RptCustomerLedgerBe>(objReportsBal.GetLedger(objLedgerBe, ReturnType.Single));
            if (objLedgerBe.IsSuccess)
            {
                if (objLedgerBe.IsCustExistsInOtherBU)
                {
                    lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                    mpeAlert.Show();
                }
                else
                {
                    ChangeBookNoBe _objBookNoBe = new ChangeBookNoBe();
                    _objBookNoBe.AccountNo = txtAccountNo.Text.Trim();
                    _objBookNoBe.Flag = 1;//CustomerName Change
                    _objBookNoBe = _objiDsignBal.DeserializeFromXml<ChangeBookNoBe>(_objBookNoBal.Get(_objBookNoBe, ReturnType.Get));
                    if (!_objBookNoBe.IsAccountNoNotExists)
                    {
                        divdetails.Visible = true;
                        lblGlobalAndAccountNo.Text = _objBookNoBe.GlobalAccNoAndAccNo;
                        lblTitle.Text = _objBookNoBe.Title;
                        //lblAccountNo.Text = _objBookNoBe.AccountNo;
                        lblOldAccNo.Text = _objBookNoBe.OldAccountNo;
                        lblFirstName.Text = _objBookNoBe.FirstName;
                        lblTariff.Text = _objBookNoBe.Tariff;
                        lblMeterNo.Text = _objBookNoBe.MeterNo;
                        lblLastName.Text = _objBookNoBe.LastName;
                        lblMiddleName.Text = _objBookNoBe.MiddleName;
                        lblKnownAs.Text = _objBookNoBe.KnownAs;
                        lblGlobalAccountNo.Text = _objBookNoBe.GlobalAccountNumber;
                        lblOutstandingAmount.Text = _objCommonMethods.GetCurrencyFormat(_objBookNoBe.OutStandingAmount, 2, 2);
                    }
                    else if (_objBookNoBe.IsAccountNoNotExists)
                    {
                        CustomerNotFound();
                    }
                }
            }
            else
            {
                CustomerNotFound();
            }
        }
        protected void btnGo_Click(object sender, EventArgs e)
        {
            XmlDocument xml = new XmlDocument();
            XmlDocument XmlCustomerRegistrationBE = new XmlDocument();
            RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
            ChangeBookNoBal objChangeBookNoBal = new ChangeBookNoBal();
            objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
            objLedgerBe.Flag = 1;
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                objLedgerBe.BUID = string.Empty;

            //xml = objChangeBookNoBal.GetCustomerDetails_ByCheck(objLedgerBe);
            xml = objChangeBookNoBal.GetCustomerDetailsForNameChangeByAccno_ByCheck(objLedgerBe);

            if (xml.SelectSingleNode("/*").Name == "RptCustomerLedgerBe")
            {
                objLedgerBe = _objiDsignBal.DeserializeFromXml<RptCustomerLedgerBe>(xml);
                if (objLedgerBe.IsSuccess)
                {
                    if (objLedgerBe.IsCustExistsInOtherBU)
                    {
                        divdetails.Visible = false;
                        txtAccountNo.Text = string.Empty;
                        lblActiveMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                        mpeActivate.Show();
                    }
                    else if (objLedgerBe.ActiveStatusId == (int)CustomerStatus.Hold)
                    {
                        divdetails.Visible = false;
                        txtAccountNo.Text = string.Empty;
                        lblActiveMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.Hold, "Name change");
                        mpeActivate.Show();
                    }
                    else if (objLedgerBe.ActiveStatusId == (int)CustomerStatus.Closed)
                    {
                        divdetails.Visible = false;
                        txtAccountNo.Text = string.Empty;
                        lblActiveMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.Closed, "Name change");
                        mpeActivate.Show();
                    }
                }
                else
                {
                    CustomerNotFound();
                }
            }
            else if (xml.SelectSingleNode("/*").Name == "ChangeBookNoBe")
            {
                ChangeBookNoBe _objBookNoBe = new ChangeBookNoBe();
                _objBookNoBe = _objiDsignBal.DeserializeFromXml<ChangeBookNoBe>(xml);
                divdetails.Visible = true;
                lblGlobalAndAccountNo.Text = _objBookNoBe.GlobalAccNoAndAccNo;
                lblTitle.Text = _objBookNoBe.Title;
                //lblAccountNo.Text = _objBookNoBe.AccountNo;
                lblOldAccNo.Text = _objBookNoBe.OldAccountNo;
                lblFirstName.Text = _objBookNoBe.FirstName;
                lblTariff.Text = _objBookNoBe.Tariff;
                lblMeterNo.Text = _objBookNoBe.MeterNo;
                lblLastName.Text = _objBookNoBe.LastName;
                lblMiddleName.Text = _objBookNoBe.MiddleName;
                lblKnownAs.Text = _objBookNoBe.KnownAs;
                lblGlobalAccountNo.Text = _objBookNoBe.GlobalAccountNumber;
                lblOutstandingAmount.Text = _objCommonMethods.GetCurrencyFormat(_objBookNoBe.OutStandingAmount, 2, 2);
            }
        }

        private void CustomerNotFound()
        {
            divdetails.Visible = false;
            txtAccountNo.Text = string.Empty;
            Message(UMS_Resource.CUSTOMER_NOT_FOUND_MSG, UMS_Resource.MESSAGETYPE_ERROR);
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (_objCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.ChangeCustomerName, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString()))
            {
                lblConfirmMsg.Text = "Are you sure you want to change Name of this Customer?";
                mpeCofirm.Show();
            }
            else
            {
                ChangeBookNoBe _objBookNoBe = new ChangeBookNoBe();
                _objBookNoBe.GlobalAccountNumber = lblGlobalAccountNo.Text;
                if (ddlOTitle.SelectedIndex > 0)
                    _objBookNoBe.Title = ddlOTitle.SelectedItem.Text;
                else
                    _objBookNoBe.Title = string.Empty;
                _objBookNoBe.FirstName = txtOFName.Text;
                _objBookNoBe.MiddleName = MiddleName;
                _objBookNoBe.LastName = txtOLName.Text;
                _objBookNoBe.KnownAs = KnownAs;
                _objBookNoBe.Details = _objiDsignBal.ReplaceNewLines(txtReason.Text, true);
                _objBookNoBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objBookNoBe.ApprovalStatusId = (int)EnumApprovalStatus.Process;
                _objBookNoBe.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerName;
                _objBookNoBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                _objBookNoBe.IsFinalApproval = false;

                _objBookNoBe = _objiDsignBal.DeserializeFromXml<ChangeBookNoBe>(_objBookNoBal.Insert(_objBookNoBe, Statement.Update));
                if (_objBookNoBe.IsSuccess)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblgreen");
                    lblActiveMsg.Text = Resource.APPROVAL_TARIFF_CHANGE;
                    divdetails.Visible = false;
                    txtOFName.Text = txtOMName.Text = txtOLName.Text = txtReason.Text = txtAccountNo.Text = txtOKnownAs.Text = string.Empty;
                    ddlOTitle.SelectedIndex = 0;
                    mpeActivate.Show();
                }
                else if (_objBookNoBe.IsApprovalInProcess)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblred");
                    divdetails.Visible = false;
                    txtOFName.Text = txtOMName.Text = txtOLName.Text = txtReason.Text = txtAccountNo.Text = string.Empty;
                    ddlOTitle.SelectedIndex = 0;
                    lblActiveMsg.Text = Resource.REQ_STILL_PENDING;
                    mpeActivate.Show();
                }
                else
                {
                    divdetails.Visible = true;
                    Message(UMS_Resource.CUSTOMER_UPDATED_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ChangeBookNoBe _objBookNoBe = new ChangeBookNoBe();
                _objBookNoBe.GlobalAccountNumber = lblGlobalAccountNo.Text;
                if (ddlOTitle.SelectedIndex > 0)
                    _objBookNoBe.Title = ddlOTitle.SelectedItem.Text;
                else
                    _objBookNoBe.Title = string.Empty;
                _objBookNoBe.FirstName = txtOFName.Text;
                _objBookNoBe.MiddleName = txtOMName.Text;
                _objBookNoBe.LastName = txtOLName.Text;
                _objBookNoBe.KnownAs = txtOKnownAs.Text;
                _objBookNoBe.Details = _objiDsignBal.ReplaceNewLines(txtReason.Text, true);
                _objBookNoBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objBookNoBe.ApprovalStatusId = (int)EnumApprovalStatus.Approved;
                _objBookNoBe.IsFinalApproval = true;
                _objBookNoBe.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerName;

                _objBookNoBe = _objiDsignBal.DeserializeFromXml<ChangeBookNoBe>(_objBookNoBal.Insert(_objBookNoBe, Statement.Update));
                if (_objBookNoBe.IsSuccess)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblgreen");
                    lblActiveMsg.Text = Resource.MODIFICATIONS_UPDATED;
                    divdetails.Visible = false;
                    txtOFName.Text = txtOMName.Text = txtOLName.Text = txtReason.Text = txtAccountNo.Text = txtOKnownAs.Text = string.Empty;
                    ddlOTitle.SelectedIndex = 0;
                    mpeActivate.Show();
                }
                else if (_objBookNoBe.IsApprovalInProcess)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblred");
                    divdetails.Visible = false;
                    txtOFName.Text = txtOMName.Text = txtOLName.Text = txtReason.Text = txtAccountNo.Text = string.Empty;
                    ddlOTitle.SelectedIndex = 0;
                    lblActiveMsg.Text = Resource.REQ_STILL_PENDING;
                    mpeActivate.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}