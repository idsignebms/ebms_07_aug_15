﻿<%@ Page Title=":: Change Customer Status ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="ChangeCustomerStatus.aspx.cs"
    Inherits="UMS_Nigeria.ConsumerManagement.ChangeCustomerStatus" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upSearchCustomer" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div class="inner-sec">
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litSearchCustomer" runat="server" Text="<%$ Resources:Resource, CHNG_CUST_STATUS%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, ACCNO_OLD_ACCNO%>"></asp:Literal>
                                    <span class="span_star">*</span></label>
                                <div class="space">
                                </div>
                                <asp:Panel ID="pnlAccountNo" DefaultButton="btnGo" runat="server">
                                    <asp:TextBox ID="txtAccountNo" CssClass="text-box" runat="server" MaxLength="15"
                                        placeholder="Global Account No / Old Account No" onkeypress="GetAccountDetails(event)"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtAccountNo"
                                        FilterType="Numbers" ValidChars=" ">
                                    </asp:FilteredTextBoxExtender>
                                    <span id="spanAccNo" class="span_color"></span>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box1">
                        <div class="search searchRight">
                            <asp:Button ID="btnGo" OnClientClick="return Validate();" Text="<%$ Resources:Resource, GET_CUSTOMER_DETAILS%>"
                                CssClass="box_s" OnClick="btnGo_Click" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="out-bor_a" id="divdetails" visible="false" runat="server">
                    <div class="inner-sec">
                        <div class="heading customerDetailstwo">
                            <asp:Literal ID="Literal213" runat="server" Text="<%$ Resources:Resource, CUSTOMER_DETAILS%>"></asp:Literal>
                        </div>
                        <div class="out-bor_cTwo">
                            <div class="inner-box">
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblGlobalAccNoAndAccNo" runat="server" Text="--"></asp:Label>
                                        <asp:Label ID="lblGlobalAccountNumber" Visible="false" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblOldAccNo" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblName" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblMeterNo" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblTariff" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <%--<div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:Resource, ACC_NO%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblAccountNo" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>--%>
                                <div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal3" runat="server" Text="Customer Status"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblCurrentStatus" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="litOustanding" runat="server" Text="Outstanding"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblOutstanding" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="litLastTransactionDate" runat="server" Text="Last Transaction Date"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblLastTransactionDate" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="DivCustomerStatusChange" runat="server">
                            <div class="text-heading customerStatus">
                                <asp:Literal ID="litPostalAddress" runat="server" Text="<%$ Resources:Resource, CHNG_STATUS%>"></asp:Literal>
                            </div>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, STATUS%>"></asp:Literal>
                                        <span class="span_star">*</span></label><div class="space">
                                        </div>
                                    <asp:DropDownList ID="ddlStatus" runat="server" onchange="DropDownlistOnChangelbl(this,'spanStatus','Status')"
                                        CssClass="text-box text-select">
                                        <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                    <div class="space">
                                    </div>
                                    <span id="spanStatus" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box2" style="display: none;">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="lblChangeDate" runat="server" Text="<%$ Resources:Resource, CHANGE_DATE%>"></asp:Literal>
                                        <span class="span_star">*</span></label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox ID="txtChangeDate" CssClass="text-box" runat="server" MaxLength="10"
                                        AutoComplete="off" placeholder="DD/MM/YYYY" onblur="TextBoxBlurValidationlbl(this, 'spanChangeDate', 'Change Date')"
                                        onChange="DoBlur(this);"></asp:TextBox>
                                    <asp:Image ID="imgDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                        vertical-align: middle" AlternateText="Calender" runat="server" />
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtChangeDate"
                                        FilterType="Custom,Numbers" ValidChars="/">
                                    </asp:FilteredTextBoxExtender>
                                    <span id="spanChangeDate" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box3">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, REASON%>"></asp:Literal>
                                        <span class="span_star">*</span></label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox ID="txtReason" CssClass="text-box" runat="server" TextMode="MultiLine"
                                        placeholder="Reason" onblur="return TextBoxBlurValidationlblZeroAccepts(this,'spanReason','Reason')"></asp:TextBox>
                                    <span id="spanReason" class="span_color"></span>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div class="inner-box1">
                                <div class="search searchButton">
                                    <asp:Button ID="btnSave" Text="<%$ Resources:Resource, UPDATE%>" CssClass="box_s"
                                        OnClientClick="return UpdateValidate();" OnClick="btnUpdate_Click" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="rnkland">
                    <%-- Confirm popup Start--%>
                    <asp:ModalPopupExtender ID="mpeCofirm" runat="server" PopupControlID="PanelCofirm"
                        TargetControlID="btnCofirm" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnCofirm" runat="server" Text="Button" CssClass="popbtn" />
                    <asp:Panel ID="PanelCofirm" runat="server" DefaultButton="btnYes" CssClass="modalPopup"
                        class="poppnl" Style="display: none;">
                        <div class="popheader popheaderlblred" id="popup" runat="server">
                            <asp:Label ID="lblConfirmMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnYes" runat="server" OnClientClick="return closePopup();" OnClick="btnYes_Click"
                                    Text="<%$ Resources:Resource, YES%>" CssClass="ok_btn" />
                                <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, NO%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                        TargetControlID="btnActivate" BackgroundCssClass="modalBackground">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelActivate" runat="server" DefaultButton="btnActiveCancel" CssClass="modalPopup"
                        Style="display: none; border-color: #639B00;">
                        <div class="popheader" style="background-color: #639B00;" id="popheader" runat="server">
                            <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                        </div>
                        <br />
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Confirm popup Closed--%>
                </div>
                <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="PanelAlert"
                    TargetControlID="btnAlertDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnAlertOk">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnAlertDeActivate" runat="server" Text="Button" CssClass="popbtn" />
                <asp:Panel ID="PanelAlert" runat="server" CssClass="modalPopup" class="poppnl" Style="display: none;">
                    <div class="popheader popheaderlblred" id="pop" runat="server">
                        <asp:Label ID="lblAlertMsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnAlertOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".rnkland").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../Javascript/jquery-ui.min.js" type="text/javascript"></script>
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/ChangeCustomerStatus.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            //AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);

        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            //AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);
        });
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
