﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for ChangeCustomerStatus
                     
 Developer        : id065-Bhimaraju Vanka
 Creation Date    : 07-10-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using iDSignHelper;
using UMS_NigeriaBAL;
using Resources;
using UMS_NigeriaBE;
using UMS_NigriaDAL;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class ChangeCustomerStatus : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        ChangeBookNoBal _objBookNoBal = new ChangeBookNoBal();
        ReportsBal objReportsBal = new ReportsBal();
        string Key = UMS_Resource.CHNG_CUST_STATUS;
        DateTime LastTransactionDate = new DateTime();
        #endregion

        #region Properties
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                lblMessage.Text = pnlMessage.CssClass = string.Empty;
                if (!IsPostBack)
                {
                    string path = string.Empty;
                    path = _objCommonMethods.GetPagePath(this.Request.Url);
                    if (_objCommonMethods.IsAccess(path))
                    {

                    }
                    else
                    {
                        Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    }
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }
        protected void btnGo_Click(object sender, EventArgs e)
        {
            RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
            objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                objLedgerBe.BUID = string.Empty;
            objLedgerBe.Flag = 1;
            objLedgerBe = _objiDsignBal.DeserializeFromXml<RptCustomerLedgerBe>(objReportsBal.GetLedger(objLedgerBe, ReturnType.Single));
            if (objLedgerBe.IsSuccess)
            {
                if (objLedgerBe.IsCustExistsInOtherBU)
                {
                    divdetails.Visible = false;
                    txtAccountNo.Text = string.Empty;
                    lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                    mpeAlert.Show();
                }
                else
                {
                    ChangeBookNoBe _objBookNoBe = new ChangeBookNoBe();
                    _objBookNoBe.AccountNo = txtAccountNo.Text.Trim();
                    _objBookNoBe.Flag = 2;//CustomerStatus Change
                    _objBookNoBe = _objiDsignBal.DeserializeFromXml<ChangeBookNoBe>(_objBookNoBal.Get(_objBookNoBe, ReturnType.Get));
                    if (!_objBookNoBe.IsAccountNoNotExists)
                    {
                        divdetails.Visible = true;
                        lblGlobalAccountNumber.Text = _objBookNoBe.GlobalAccountNumber;
                        lblGlobalAccNoAndAccNo.Text = _objBookNoBe.GlobalAccNoAndAccNo;
                        lblOldAccNo.Text = _objBookNoBe.OldAccountNo;
                        lblName.Text = _objBookNoBe.Name;
                        lblTariff.Text = _objBookNoBe.Tariff;
                        lblMeterNo.Text = _objBookNoBe.MeterNo;
                        txtChangeDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                        //lblAccountNo.Text = _objBookNoBe.AccountNo;
                        lblCurrentStatus.Text = _objBookNoBe.ActiveStatus;
                        lblOutstanding.Text = _objCommonMethods.GetCurrencyFormat(_objBookNoBe.OutStandingAmount, 2, Constants.MILLION_Format);
                        lblLastTransactionDate.Text = _objBookNoBe.LastTransactionDate1.ToString();
                        if (_objBookNoBe.ActiveStatusId == 4)
                        {
                            DivCustomerStatusChange.Visible = false;
                            Message(Resource.CUST_STATUS_CLOSED, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else
                        {
                            _objCommonMethods.BindCustomerStatusDetails(ddlStatus, _objBookNoBe.ActiveStatusId, UMS_Resource.DROPDOWN_SELECT, false);
                            //ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(_objBookNoBe.ActiveStatusId.ToString()));
                        }
                    }
                    else if (_objBookNoBe.IsAccountNoNotExists)
                    {
                        CustomerNotFound();
                    }
                }
            }
            else
            {
                CustomerNotFound();
            }
        }

        private void CustomerNotFound()
        {
            divdetails.Visible = false;
            txtAccountNo.Text = string.Empty;
            Message(UMS_Resource.CUSTOMER_NOT_FOUND_MSG, UMS_Resource.MESSAGETYPE_ERROR);
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            //DateTime ChangeDate = DateTime.ParseExact(txtChangeDate.Text, "dd/MM/yyyy", null);
            //DateTime ChangeDate = Convert.ToDateTime(txtChangeDate.Text);
            //DateTime LastTransactionDate = Convert.ToDateTime(lblLastTransactionDate.Text);
            if (ddlStatus.SelectedItem.Text.ToLower() == "closed" && Convert.ToDecimal(lblOutstanding.Text) > 0)
            {
                Message("Outstanding of the customer should be less then or equal to zero. ", UMS_Resource.MESSAGETYPE_ERROR);
            }
            //else if(ChangeDate<=Convert.ToDateTime(lblLastTransactionDate.Text))
            //else if (ChangeDate <= LastTransactionDate)
            //{
            //    Message("Change date should be greater than last transation date ", UMS_Resource.MESSAGETYPE_ERROR);
            //}
            else
            {
                if (_objCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.ChangeCustomerStatus, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString()))
                {
                    lblConfirmMsg.Text = "Are you sure you want to change the Status of this Customer?";
                    mpeCofirm.Show();
                }
                else
                {

                    ChangeBookNoBe _objBookNoBe = new ChangeBookNoBe();
                    _objBookNoBe.GlobalAccountNumber = lblGlobalAccountNumber.Text;
                    _objBookNoBe.ActiveStatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                    _objBookNoBe.Details = _objiDsignBal.ReplaceNewLines(txtReason.Text, true);
                    _objBookNoBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    _objBookNoBe.ApprovalStatusId = (int)EnumApprovalStatus.Process;
                    //_objBookNoBe.ChangeDate = _objCommonMethods.Convert_MMDDYY(txtChangeDate.Text);
                    _objBookNoBe.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerStatus;
                    _objBookNoBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                    _objBookNoBe = _objiDsignBal.DeserializeFromXml<ChangeBookNoBe>(_objBookNoBal.Insert(_objBookNoBe, Statement.Check));
                    if (_objBookNoBe.IsSuccess)
                    {
                        popheader.Attributes.Add("class", "popheader popheaderlblgreen");
                        lblActiveMsg.Text = Resource.APPROVAL_TARIFF_CHANGE;
                        txtAccountNo.Text = txtReason.Text = string.Empty;
                        divdetails.Visible = false;
                        mpeActivate.Show();
                    }
                    else if (_objBookNoBe.IsApprovalInProcess)
                    {
                        popheader.Attributes.Add("class", "popheader popheaderlblred");
                        txtAccountNo.Text = txtReason.Text = string.Empty;
                        divdetails.Visible = false;
                        lblActiveMsg.Text = Resource.REQ_STILL_PENDING;
                        mpeActivate.Show();
                    }
                    else
                    {
                        divdetails.Visible = true;
                        Message(UMS_Resource.CUSTOMER_UPDATED_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
            }
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ChangeBookNoBe _objBookNoBe = new ChangeBookNoBe();
                _objBookNoBe.GlobalAccountNumber = lblGlobalAccountNumber.Text;
                _objBookNoBe.ActiveStatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                _objBookNoBe.Details = _objiDsignBal.ReplaceNewLines(txtReason.Text, true);
                _objBookNoBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objBookNoBe.ApprovalStatusId = (int)EnumApprovalStatus.Process;
                //_objBookNoBe.ChangeDate = _objCommonMethods.Convert_MMDDYY(txtChangeDate.Text);
                _objBookNoBe.ApprovalStatusId = (int)EnumApprovalStatus.Approved;
                _objBookNoBe.IsFinalApproval = true;
                _objBookNoBe.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerStatus;
                _objBookNoBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                _objBookNoBe = _objiDsignBal.DeserializeFromXml<ChangeBookNoBe>(_objBookNoBal.Insert(_objBookNoBe, Statement.Check));

                if (_objBookNoBe.IsSuccess)
                {
                    popheader.Attributes.Add("class", "popheader popheaderlblgreen");
                    lblActiveMsg.Text = Resource.MODIFICATIONS_UPDATED;
                    divdetails.Visible = false;
                    txtAccountNo.Text = txtReason.Text = string.Empty;
                    ddlStatus.SelectedIndex = 0;
                    mpeActivate.Show();
                }
                else if (_objBookNoBe.IsApprovalInProcess)
                {
                    popheader.Attributes.Add("class", "popheader popheaderlblred");
                    txtAccountNo.Text = txtReason.Text = string.Empty;
                    divdetails.Visible = false;
                    lblActiveMsg.Text = Resource.REQ_STILL_PENDING;
                    mpeActivate.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods
        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}