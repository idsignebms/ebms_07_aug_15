﻿<%@ Page Title=":: Change Customer Type ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    AutoEventWireup="true" Theme="Green" CodeBehind="ChangeCustomerType.aspx.cs"
    Inherits="UMS_Nigeria.ConsumerManagement.ChangeCustomerType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upSearchCustomer" DefaultButton="btnSearch" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div class="inner-sec">
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litSearchCustomer" runat="server" Text="<%$ Resources:Resource, CHNG_CUST_TYPE%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litAccountNo" runat="server" Text="<%$ Resources:Resource, ACCNO_OLD_ACCNO%>"></asp:Literal>
                                    <span class="span_star">*</span></label>
                                <div class="space">
                                </div>
                                <asp:TextBox ID="txtAccountNo" CssClass="text-box" runat="server" MaxLength="15"
                                    placeholder="Global Account No / Old Account No"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtAccountNo"
                                    FilterType="Numbers">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanAccNo" class="span_color"></span>
                            </div>
                        </div>
                        <div class="box_total">
                            <div class="box_total_a">
                                <div class="search" style="padding-left: 7px;">
                                    <asp:Button ID="btnGo" OnClientClick="return Validate();" Text="<%$ Resources:Resource, GET_CUSTOMER_DETAILS%>"
                                        CssClass="box_s" OnClick="btnGo_Click" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="out-bor_a" id="divdetails" visible="false" runat="server">
                    <div class="inner-sec">
                        <div class="heading" style="padding-left: 16px; padding-top: 17px;">
                            <asp:Literal ID="Literal213" runat="server" Text="<%$ Resources:Resource, CUSTOMER_DETAILS%>"></asp:Literal>
                        </div>
                        <div class="out-bor_cTwo">
                            <div class="inner-box">
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblGlobalAccNoAndAccNo" runat="server" Text="--"></asp:Label>
                                        <asp:Label ID="lblGlobalAccountNo" Visible="false" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblOldAccNo" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, F_NAME%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblFirstName" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblMeterNo" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, M_NAME%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblMiddleName" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <%--<div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblTariff" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>--%>
                                <div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:Resource, KNOWN_AS%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblKnownAs" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:Resource, L_NAME%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblLastName" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <%--<div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, ACC_NO%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblAccountNo" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>--%>
                                <div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblTariff" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, CUSTOMER_TYPE%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblCustomerType" runat="server" Text="--" Font-Bold="true"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="litOutstandingAmount" runat="server" Text="<%$ Resources:Resource, OUT_STANDING_AMT%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblOutstandingAmount" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="text-heading" style="margin-bottom: 9px; margin-left: 17px;">
                            <asp:Literal ID="litPostalAddress" runat="server" Text="<%$ Resources:Resource, EDIT_CUST_TYPE%>"></asp:Literal>
                        </div>
                        <div class="inner-box1">
                            <div class="customerDetailstext">
                                <label for="name">
                                    <asp:Literal ID="litOTitle" runat="server" Text="<%$ Resources:Resource, CUSTOMER_TYPE%>"></asp:Literal>
                                </label>
                                <span class="span_star">*</span></label><div class="space">
                                </div>
                                <asp:DropDownList ID="ddlCustomerType" TabIndex="1" runat="server" onchange="DropDownlistOnChangelbl(this,'spanCustomerType',' Customer Type')"
                                    CssClass="text-box text-select">
                                    <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <div class="space">
                                </div>
                                <span id="spanCustomerType" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="customerDetailstext">
                                <label for="name">
                                    <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, REASON%>"></asp:Literal>
                                    <span class="span_star">*</span></label><div class="space">
                                    </div>
                                <asp:TextBox ID="txtReason" CssClass="text-box" runat="server" TextMode="MultiLine"
                                    Rows="5" placeholder="Reason" TabIndex="6" onblur="return TextBoxBlurValidationlblZeroAccepts(this,'spanReason','Reason')"></asp:TextBox>
                                <span id="spanReason" class="span_color"></span>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="inner-box1">
                            <div class="search" style="margin-top: 19px; margin-left: -9px; margin-bottom: 17px;">
                                <asp:Button ID="btnSave" TabIndex="7" Text="<%$ Resources:Resource, UPDATE%>" CssClass="box_s"
                                    OnClientClick="return UpdateValidate();" OnClick="btnUpdate_Click" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="rnkland">
                    <%-- Confirm popup Start--%>
                    <asp:ModalPopupExtender ID="mpeCofirm" runat="server" PopupControlID="PanelCofirm"
                        TargetControlID="btnCofirm" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnCofirm" runat="server" Text="Button" CssClass="popbtn" />
                    <asp:Panel ID="PanelCofirm" runat="server" DefaultButton="btnYes" CssClass="modalPopup"
                        class="poppnl" Style="display: none;">
                        <div class="popheader popheaderlblred" id="popup" runat="server">
                            <asp:Label ID="lblConfirmMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnYes" runat="server" OnClientClick="return closePopup();" OnClick="btnYes_Click"
                                    Text="<%$ Resources:Resource, YES%>" CssClass="ok_btn" />
                                <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, NO%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                        TargetControlID="btnActivate" BackgroundCssClass="modalBackground">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelActivate" runat="server" DefaultButton="btnActiveCancel" CssClass="modalPopup"
                        Style="display: none; border-color: #639B00;">
                        <div class="popheader" id="popActivate" runat="server" style="background-color: #639B00;">
                            <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                        </div>
                        <div class="space">
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Confirm popup Closed--%>
                </div>
                <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="PanelAlert"
                    TargetControlID="btnAlertDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnAlertOk">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnAlertDeActivate" runat="server" Text="Button" CssClass="popbtn" />
                <asp:Panel ID="PanelAlert" runat="server" CssClass="modalPopup" class="poppnl" Style="display: none;">
                    <div class="popheader popheaderlblred" id="pop" runat="server">
                        <asp:Label ID="lblAlertMsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <div class="space">
                            </div>
                            <asp:Button ID="btnAlertOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".rnkland").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/ChangeCustomerType.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
