﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using iDSignHelper;
using UMS_NigeriaBAL;
using Resources;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Xml;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class ChangeCustomerType : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        ChangeCusotmerTypeBAL _objChangeCusotmerTypeBAL = new ChangeCusotmerTypeBAL();
        ReportsBal objReportsBal = new ReportsBal();
        string Key = UMS_Resource.CHNG_CUST_NAME;
        #endregion

        #region Properties
        public string CustomerTypeId
        {
            get { return string.IsNullOrEmpty(ddlCustomerType.SelectedValue) ? Constants.SelectedValue : ddlCustomerType.SelectedValue; }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                lblMessage.Text = pnlMessage.CssClass = string.Empty;
                if (!IsPostBack)
                {
                    _objCommonMethods.BindCustomerTypes(ddlCustomerType, true);
                    //string path = string.Empty;
                    //path = _objCommonMethods.GetPagePath(this.Request.Url);
                    //if (_objCommonMethods.IsAccess(path))
                    //{

                    //}
                    //else
                    //{
                    //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    //}
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }

        protected void btnGo_Click1(object sender, EventArgs e)
        {
            RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
            objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                objLedgerBe.BUID = string.Empty;
            objLedgerBe = _objiDsignBal.DeserializeFromXml<RptCustomerLedgerBe>(objReportsBal.GetLedger(objLedgerBe, ReturnType.Single));
            if (objLedgerBe.IsSuccess)
            {
                if (objLedgerBe.IsCustExistsInOtherBU)
                {
                    lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                    mpeAlert.Show();
                }
                else
                {
                    ChangeCustomerTypeBE _objChangeCusotmerTypeBE = new ChangeCustomerTypeBE();
                    _objChangeCusotmerTypeBE.AccountNo = txtAccountNo.Text.Trim();
                    _objChangeCusotmerTypeBE = _objiDsignBal.DeserializeFromXml<ChangeCustomerTypeBE>(_objChangeCusotmerTypeBAL.Get(_objChangeCusotmerTypeBE, ReturnType.Get));
                    if (!_objChangeCusotmerTypeBE.IsAccountNoNotExists)
                    {
                        divdetails.Visible = true;
                        //lblAccountNo.Text = _objChangeCusotmerTypeBE.AccountNo;
                        lblGlobalAccNoAndAccNo.Text = _objChangeCusotmerTypeBE.GlobalAccNoAndAccNo;
                        lblOldAccNo.Text = _objChangeCusotmerTypeBE.OldAccountNo;
                        lblFirstName.Text = _objChangeCusotmerTypeBE.FirstName;
                        lblTariff.Text = _objChangeCusotmerTypeBE.Tariff;
                        lblMeterNo.Text = _objChangeCusotmerTypeBE.MeterNo;
                        lblLastName.Text = _objChangeCusotmerTypeBE.LastName;
                        lblMiddleName.Text = _objChangeCusotmerTypeBE.MiddleName;
                        lblKnownAs.Text = _objChangeCusotmerTypeBE.KnownAs;
                        lblGlobalAccountNo.Text = _objChangeCusotmerTypeBE.GlobalAccountNumber;
                        lblCustomerType.Text = _objChangeCusotmerTypeBE.CustomerType;
                        lblOutstandingAmount.Text = _objCommonMethods.GetCurrencyFormat(_objChangeCusotmerTypeBE.OutStandingAmount, 2, Constants.MILLION_Format);
                        //ddlCustomerType.SelectedValue = Convert.ToString(_objChangeCusotmerTypeBE.CustomerTypeId);
                        _objCommonMethods.BindCustomerTypes(ddlCustomerType, _objChangeCusotmerTypeBE.CustomerTypeId, true);
                    }
                    else
                    {
                        CustomerNotFound();
                    }
                }
            }
            else
            {
                CustomerNotFound();
            }
        }
        protected void btnGo_Click(object sender, EventArgs e)
        {
            XmlDocument xml = new XmlDocument();
            RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
            ChangeCusotmerTypeBAL ObjChangeCusotmerTypeBAL = new ChangeCusotmerTypeBAL();
            objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                objLedgerBe.BUID = string.Empty;

            xml = ObjChangeCusotmerTypeBAL.GetCustDetForTypeChange_ByCheck(objLedgerBe);
            if (xml.SelectSingleNode("/*").Name == "RptCustomerLedgerBe")
            {
                objLedgerBe = _objiDsignBal.DeserializeFromXml<RptCustomerLedgerBe>(xml);
                if (objLedgerBe.IsSuccess)
                {
                    if (objLedgerBe.IsCustExistsInOtherBU)
                    {
                        lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                        mpeAlert.Show();
                    }
                    else if (objLedgerBe.ActiveStatusId == (int)CustomerStatus.Hold)
                    {
                        lblAlertMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.Hold, "Change Type ");
                        mpeAlert.Show();
                    }
                    else if (objLedgerBe.ActiveStatusId == (int)CustomerStatus.Closed)
                    {
                        lblAlertMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.Closed, "Change Type ");
                        mpeAlert.Show();
                    }
                    //else if (objLedgerBe.ActiveStatusId == (int)CustomerStatus.InActive)
                    //{
                    //    lblAlertMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.InActive, "Change Type ");
                    //    mpeAlert.Show();
                    //}
                }
                else
                {
                    CustomerNotFound();
                }
            }
            else if (xml.SelectSingleNode("/*").Name == "ChangeCustomerTypeBE")
            {
                ChangeCustomerTypeBE _objChangeCusotmerTypeBE = new ChangeCustomerTypeBE();
                _objChangeCusotmerTypeBE = _objiDsignBal.DeserializeFromXml<ChangeCustomerTypeBE>(xml);
                divdetails.Visible = true;
                lblGlobalAccNoAndAccNo.Text = _objChangeCusotmerTypeBE.AccountNo;
                lblOldAccNo.Text = _objChangeCusotmerTypeBE.OldAccountNo;
                lblFirstName.Text = _objChangeCusotmerTypeBE.FirstName;
                lblTariff.Text = _objChangeCusotmerTypeBE.Tariff;
                lblMeterNo.Text = _objChangeCusotmerTypeBE.MeterNo;
                lblLastName.Text = _objChangeCusotmerTypeBE.LastName;
                lblMiddleName.Text = _objChangeCusotmerTypeBE.MiddleName;
                lblKnownAs.Text = _objChangeCusotmerTypeBE.KnownAs;
                lblGlobalAccountNo.Text = _objChangeCusotmerTypeBE.GlobalAccountNumber;
                lblCustomerType.Text = _objChangeCusotmerTypeBE.CustomerType;
                _objCommonMethods.BindCustomerTypes(ddlCustomerType, _objChangeCusotmerTypeBE.CustomerTypeId, true);
            }
        }
        private void CustomerNotFound()
        {
            divdetails.Visible = false;
            txtAccountNo.Text = string.Empty;
            Message(UMS_Resource.CUSTOMER_NOT_FOUND_MSG, UMS_Resource.MESSAGETYPE_ERROR);
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (_objCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.CustomerTypeChange, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString()))
            {
                lblConfirmMsg.Text = "Are you sure you want to change Type of this Customer?";
                mpeCofirm.Show();
            }
            else
            {
                ChangeCustomerTypeBE _objChangeCustomerTypeBE = new ChangeCustomerTypeBE();
                _objChangeCustomerTypeBE.GlobalAccountNumber = lblGlobalAccountNo.Text;
                _objChangeCustomerTypeBE.CustomerTypeId = Convert.ToInt32(CustomerTypeId);
                _objChangeCustomerTypeBE.Reason = _objiDsignBal.ReplaceNewLines(txtReason.Text, true);
                _objChangeCustomerTypeBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objChangeCustomerTypeBE.ApprovalStatusId = (int)EnumApprovalStatus.Process;
                _objChangeCustomerTypeBE.FunctionId = (int)EnumApprovalFunctions.CustomerTypeChange;
                _objChangeCustomerTypeBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                _objChangeCustomerTypeBE.IsFinalApproval = false;

                _objChangeCustomerTypeBE = _objiDsignBal.DeserializeFromXml<ChangeCustomerTypeBE>(_objChangeCusotmerTypeBAL.CustomerType(_objChangeCustomerTypeBE, Statement.Update));

                if (_objChangeCustomerTypeBE.IsSuccess)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblgreen");
                    lblActiveMsg.Text = Resource.APPROVAL_TARIFF_CHANGE;
                    divdetails.Visible = false;
                    txtAccountNo.Text = txtReason.Text = string.Empty;
                    ddlCustomerType.SelectedIndex = 0;
                    mpeActivate.Show();

                }
                else if (_objChangeCustomerTypeBE.IsApprovalInProcess)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblred");
                    divdetails.Visible = false;
                    txtReason.Text = string.Empty;
                    ddlCustomerType.SelectedIndex = 0;
                    lblActiveMsg.Text = Resource.REQ_STILL_PENDING;
                    mpeActivate.Show();
                }
                else
                {
                    divdetails.Visible = true;
                    Message(UMS_Resource.CUSTOMER_UPDATED_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                ChangeCustomerTypeBE _objChangeCustomerTypeBE = new ChangeCustomerTypeBE();
                _objChangeCustomerTypeBE.GlobalAccountNumber = lblGlobalAccountNo.Text;
                _objChangeCustomerTypeBE.CustomerTypeId = Convert.ToInt32(CustomerTypeId);
                _objChangeCustomerTypeBE.Reason = _objiDsignBal.ReplaceNewLines(txtReason.Text, true);
                _objChangeCustomerTypeBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objChangeCustomerTypeBE.ApprovalStatusId = (int)EnumApprovalStatus.Approved;
                _objChangeCustomerTypeBE.IsFinalApproval = true;
                _objChangeCustomerTypeBE.FunctionId = (int)EnumApprovalFunctions.CustomerTypeChange;
                _objChangeCustomerTypeBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                _objChangeCustomerTypeBE = _objiDsignBal.DeserializeFromXml<ChangeCustomerTypeBE>(_objChangeCusotmerTypeBAL.CustomerType(_objChangeCustomerTypeBE, Statement.Update));

                if (_objChangeCustomerTypeBE.IsSuccess)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblgreen");
                    lblActiveMsg.Text = Resource.MODIFICATIONS_UPDATED;
                    divdetails.Visible = false;
                    txtAccountNo.Text = txtReason.Text = string.Empty;
                    ddlCustomerType.SelectedIndex = 0;
                    mpeActivate.Show();
                }
                else if (_objChangeCustomerTypeBE.IsApprovalInProcess)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblred");
                    divdetails.Visible = false;
                    txtReason.Text = string.Empty;
                    ddlCustomerType.SelectedIndex = 0;
                    lblActiveMsg.Text = Resource.REQ_STILL_PENDING;
                    mpeActivate.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods
        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}