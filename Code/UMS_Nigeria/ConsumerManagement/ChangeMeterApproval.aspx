﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangeMeterApproval.aspx.cs"
    Inherits="UMS_Nigeria.ConsumerManagement.ChangeMeterApproval" Title="::Customer Tariff Approval::"
    MasterPageFile="~/MasterPages/EBMS.Master" Theme="Green" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upStates" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddCycle" runat="server" Text="Customer MeterNo Approval"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                                <span class="span_star">*</span><br />
                                <asp:DropDownList ID="ddlBU" AutoPostBack="true" OnSelectedIndexChanged="ddlBU_SelectedIndexChanged"
                                    runat="server" CssClass="text-box select_box">
                                    <asp:ListItem Value="" Text="ALL"></asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                <span id="spanBU" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litServiceUnit" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal>
                                    <span class="span_star">*</span></label><br />
                                <asp:DropDownList ID="ddlBook" runat="server"  CssClass="text-box select_box">
                                    <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <span id="spanBook" class="span_color"></span>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s"
                                OnClick="btnSearch_Click" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="hfPageSize" runat="server" />
            <asp:HiddenField ID="hfPageNo" runat="server" />
            <asp:HiddenField ID="hfLastPage" runat="server" />
            <asp:HiddenField ID="hfTotalRecords" runat="server" />
            <asp:HiddenField ID="hfRecognize" runat="server" />
            <asp:HiddenField ID="hfAccountNo" runat="server" />
            <asp:HiddenField ID="hfTariffChangeRequestId" runat="server" />
            <asp:HiddenField ID="hfclassId" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
