﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using iDSignHelper;
using UMS_NigeriaBE;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class ChangeMeterApproval : System.Web.UI.Page
    {

        # region Members
        
        iDsignBAL _objiDsignBAL = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        public int PageNum;
        string Key = UMS_Resource.CHANGEMETER_APPROVAL;
        
        #endregion

        #region Properties

        //public int PageSize
        //{
        //    get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
        //}
        public string BookNo
        {
            get { return string.IsNullOrEmpty(ddlBook.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlBook, ",") : ddlBook.SelectedValue; }
            set { ddlBook.SelectedValue = value.ToString(); }
        }

        public string BusinessUnit
        {
            get { return string.IsNullOrEmpty(ddlBU.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlBU, ",") : ddlBU.SelectedValue; }
            set { ddlBU.SelectedValue = value.ToString(); }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                try
                {
                    _objCommonMethods.BindUserBusinessUnits(ddlBU, string.Empty, false, Resource.DDL_ALL);
                    _objCommonMethods.BindBookNo(ddlBook, string.Empty, Resource.DDL_ALL, false);
                    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    {
                        ddlBU.SelectedIndex = ddlBU.Items.IndexOf(ddlBU.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                        ddlBU.Enabled = false;
                        ddlBU_SelectedIndexChanged(ddlBU, new EventArgs());
                    }
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                  
                }
                catch (Exception ex)
                {
                    _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                    try
                    {
                        _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                    }
                    catch (Exception logex)
                    {

                    }
                }
            }

        }

        protected void ddlBU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _objCommonMethods.BindBookNo(ddlBook, this.BusinessUnit, Resource.DDL_ALL, false);
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {

        }
    }
}