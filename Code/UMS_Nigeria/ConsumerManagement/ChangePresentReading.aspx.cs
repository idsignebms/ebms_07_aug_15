﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using iDSignHelper;
using UMS_NigeriaBAL;
using Resources;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Xml;
using System.Data;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class ChangePresentReading : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        ChangeCusotmerTypeBAL _objChangeCusotmerTypeBAL = new ChangeCusotmerTypeBAL();
        ReportsBal objReportsBal = new ReportsBal();
        string Key = "ChangePresentReading";
        #endregion

        #region Properties

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                lblMessage.Text = pnlMessage.CssClass = string.Empty;
                if (!IsPostBack)
                {

                    //string path = string.Empty;
                    //path = _objCommonMethods.GetPagePath(this.Request.Url);
                    //if (_objCommonMethods.IsAccess(path))
                    //{

                    //}
                    //else
                    //{
                    //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    //}
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            XmlDocument xml = new XmlDocument();
            XmlDocument XmlCustomerRegistrationBE = new XmlDocument();
            RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
            ChangeBookNoBal objChangeBookNoBal = new ChangeBookNoBal();
            ChangeBookNoBe _objBookNoBe = new ChangeBookNoBe();
            CustomerReadingListBe _objCustomerReadingListBe = new CustomerReadingListBe();
            CustomerReadingListBeDetails _objCustomerDetails = new CustomerReadingListBeDetails();
            objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                objLedgerBe.BUID = string.Empty;

            xml = objReportsBal.GetCusotmerReading(objLedgerBe);

            if (xml.SelectSingleNode("/*").Name == "RptCustomerLedgerBe")
            {
                objLedgerBe = _objiDsignBal.DeserializeFromXml<RptCustomerLedgerBe>(xml);
                if (objLedgerBe.IsSuccess)
                {
                    if (objLedgerBe.IsCustExistsInOtherBU)
                    {
                        divdetails.Visible = false;
                        txtAccountNo.Text = txtCurrentReading.Text = string.Empty;
                        popActivate.Attributes.Add("class", "popheader popheaderlblred");
                        lblActiveMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                        mpeActivate.Show();
                    }
                    else if (objLedgerBe.ActiveStatusId == (int)CustomerStatus.Hold)
                    {
                        divdetails.Visible = false;
                        txtAccountNo.Text = txtCurrentReading.Text = string.Empty;
                        popActivate.Attributes.Add("class", "popheader popheaderlblred");
                        lblActiveMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.Hold, "Reading change");
                        mpeActivate.Show();
                    }
                    else if (objLedgerBe.ActiveStatusId == (int)CustomerStatus.Closed)
                    {
                        divdetails.Visible = false;
                        txtAccountNo.Text = txtCurrentReading.Text = string.Empty;
                        popActivate.Attributes.Add("class", "popheader popheaderlblred");
                        lblActiveMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.Closed, "Reading change");
                        mpeActivate.Show();
                    }
                }
                else
                {
                    CustomerNotFound();
                }
            }
            else if (xml.SelectSingleNode("/*").Name == "CustomerReadingListBeByXml")
            {
                txtCurrentReading.Text = string.Empty;
                //_objBookNoBe = _objiDsignBal.DeserializeFromXml<ChangeBookNoBe>(xml);
                _objCustomerDetails = _objiDsignBal.DeserializeFromXml<CustomerReadingListBeDetails>(xml);
                _objCustomerReadingListBe = _objiDsignBal.DeserializeFromXml<CustomerReadingListBe>(xml);

                divdetails.Visible = true;
                lblGlobalAccountNo.Text = _objCustomerReadingListBe.items[0].GlobalAccountNumber;
                lblGlobalAccNoAndAccNo.Text = _objCustomerReadingListBe.items[0].GlobalAccNoAndAccNo;
                lblOldAccNo.Text = _objCustomerReadingListBe.items[0].OldAccountNo;
                lblName.Text = _objCustomerReadingListBe.items[0].Name;
                lblTariff.Text = _objCustomerReadingListBe.items[0].Tariff;
                lblMeterNo.Text = _objCustomerReadingListBe.items[0].MeterNo;
                lblLastPreviousReading.Text = _objCustomerReadingListBe.items[0].PreviousReading;
                lblLastPresentReading.Text = _objCustomerReadingListBe.items[0].PresentReading;
                lblTotalReadings.Text = _objCustomerReadingListBe.items[0].TotalReadings;
                lblCustomerType.Text = _objCustomerReadingListBe.items[0].CustomerType;
                //lblAccountNo.Text = _objBookNoBe.AccountNo;
                lblOutstandingAmount.Text = _objCommonMethods.GetCurrencyFormat(_objCustomerReadingListBe.items[0].OutstandingAmount, 2, 2);
                if (_objCustomerDetails.items.Count > 0)
                {
                    gvReadings.DataSource = _objCustomerDetails.items;
                    gvReadings.DataBind();
                    btnSave.Visible = true;
                }
                else
                {
                    gvReadings.DataSource = new DataTable();
                    gvReadings.DataBind();
                    btnSave.Visible = true;
                    //lblAlertMsg.Text = "No Reading avilable for this Customer";
                    //mpeAlert.Show();
                    //btnAlertOk.Focus();
                    //btnSave.Visible = false;
                }
            }
            else
            {
                BillingBE _objBillingBE = new BillingBE();
                _objBillingBE = _objiDsignBal.DeserializeFromXml<BillingBE>(xml);

                if (_objBillingBE.IsCustomerHaveReadings)
                {
                    lblAlertMsg.Text = "This Customer having Readings you can not Adjust.";
                    mpeAlert.Show();
                    btnAlertOk.Focus();
                    divdetails.Visible = btnSave.Visible = false;
                    txtAccountNo.Text = txtCurrentReading.Text = string.Empty;
                }
                else if (_objBillingBE.IsDirectCustomer)
                {
                    lblAlertMsg.Text = "This is Direct Customer";
                    mpeAlert.Show();
                    btnAlertOk.Focus();
                    divdetails.Visible = btnSave.Visible = false;
                    txtAccountNo.Text = txtCurrentReading.Text = string.Empty;
                }
                else
                { CustomerNotFound(); }
            }
        }


        private void CustomerNotFound()
        {
            divdetails.Visible = false;
            txtAccountNo.Text = txtCurrentReading.Text = string.Empty;
            Message(UMS_Resource.CUSTOMER_NOT_FOUND_MSG, UMS_Resource.MESSAGETYPE_ERROR);
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (_objCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.PresentMeterReadingAdjustment, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString()))
            {
                btnYes.Focus();
                popup.Attributes.Add("class", "popheader popheaderlblgreen");
                lblConfirmMsg.Text = "Are you sure you want to change Reading of this Customer?";
                mpeCofirm.Show();
            }
            else
            {

                BillingBE _objBillingBE = new BillingBE();
                BillReadingsBAL _objBillReadingsBAL = new BillReadingsBAL();
                _objBillingBE.PreviousReading = lblLastPreviousReading.Text;
                _objBillingBE.PresentReading = lblLastPresentReading.Text;
                _objBillingBE.CurrentReading = txtCurrentReading.Text;
                _objBillingBE.GlobalAccountNumber = lblGlobalAccountNo.Text;
                _objBillingBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objBillingBE.MeterNo = lblMeterNo.Text;

                _objBillingBE.ApproveStatusId = (int)EnumApprovalStatus.Process;
                _objBillingBE.FunctionId = (int)EnumApprovalFunctions.PresentMeterReadingAdjustment;
                _objBillingBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                _objBillingBE.IsFinalApproval = false;

                _objBillingBE = _objiDsignBal.DeserializeFromXml<BillingBE>(_objBillReadingsBAL.PresentReadingAdjustment(_objBillingBE, Statement.Insert));

                if (_objBillingBE.IsInitialBillingkWh)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblgreen");
                    lblActiveMsg.Text = Resource.MODIFICATIONS_UPDATED;
                    divdetails.Visible = btnSave.Visible = false;
                    txtAccountNo.Text = txtCurrentReading.Text = string.Empty;
                    mpeActivate.Show();
                }
                else if (_objBillingBE.IsSuccess == 1)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblgreen");
                    lblActiveMsg.Text = Resource.APPROVAL_TARIFF_CHANGE;
                    divdetails.Visible = btnSave.Visible = false;
                    txtAccountNo.Text = txtCurrentReading.Text = string.Empty;
                    mpeActivate.Show();

                }
                else if (_objBillingBE.IsApprovalInProcess)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblred");
                    divdetails.Visible = btnSave.Visible = false;
                    txtAccountNo.Text = txtCurrentReading.Text = string.Empty;
                    lblActiveMsg.Text = Resource.REQ_STILL_PENDING;
                    mpeActivate.Show();
                }
                else if (_objBillingBE.IsBilled == 1)
                {
                    lblAlertMsg.Text = "This Customer having readings you can not Adjust.";
                    mpeAlert.Show();
                    btnAlertOk.Focus();
                    divdetails.Visible = btnSave.Visible = false;
                    txtAccountNo.Text = txtCurrentReading.Text = string.Empty;
                }
                else if (_objBillingBE.IsMaxReading)
                {
                    lblAlertMsg.Text = "Current Reading should not be greater than the Last Present Reading.";
                    mpeAlert.Show();
                    btnAlertOk.Focus();
                    divdetails.Visible = btnSave.Visible = false;
                    txtAccountNo.Text = txtCurrentReading.Text = string.Empty;
                }
                else if (_objBillingBE.IsMinReading)
                {
                    lblAlertMsg.Text = "Current Reading should not be less than the Last Previous Reading.";
                    mpeAlert.Show();
                    btnAlertOk.Focus();
                    divdetails.Visible = btnSave.Visible = false;
                    txtAccountNo.Text = txtCurrentReading.Text = string.Empty;
                }
                else
                {
                    divdetails.Visible = true;
                    Message(UMS_Resource.CUSTOMER_UPDATED_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                BillingBE _objBillingBE = new BillingBE();
                BillReadingsBAL _objBillReadingsBAL = new BillReadingsBAL();
                _objBillingBE.PreviousReading = lblLastPreviousReading.Text;
                _objBillingBE.PresentReading = lblLastPresentReading.Text;
                _objBillingBE.CurrentReading = txtCurrentReading.Text;
                _objBillingBE.GlobalAccountNumber = lblGlobalAccountNo.Text;
                _objBillingBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objBillingBE.MeterNo = lblMeterNo.Text;

                _objBillingBE.ApproveStatusId = (int)EnumApprovalStatus.Approved;
                _objBillingBE.IsFinalApproval = true;
                _objBillingBE.FunctionId = (int)EnumApprovalFunctions.PresentMeterReadingAdjustment;
                _objBillingBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                _objBillingBE = _objiDsignBal.DeserializeFromXml<BillingBE>(_objBillReadingsBAL.PresentReadingAdjustment(_objBillingBE, Statement.Insert));

                if (_objBillingBE.IsInitialBillingkWh)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblgreen");
                    lblActiveMsg.Text = Resource.MODIFICATIONS_UPDATED;
                    divdetails.Visible = btnSave.Visible = false;
                    txtAccountNo.Text = txtCurrentReading.Text = string.Empty;
                    mpeActivate.Show();
                }
                else if (_objBillingBE.IsSuccess == 1)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblgreen");
                    lblActiveMsg.Text = Resource.MODIFICATIONS_UPDATED;
                    divdetails.Visible = btnSave.Visible = false;
                    txtAccountNo.Text = txtCurrentReading.Text = string.Empty;
                    mpeActivate.Show();
                }
                else if (_objBillingBE.IsApprovalInProcess)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblred");
                    divdetails.Visible = btnSave.Visible = false;
                    txtAccountNo.Text = txtCurrentReading.Text = string.Empty;
                    lblActiveMsg.Text = Resource.REQ_STILL_PENDING;
                    mpeActivate.Show();
                }
                else if (_objBillingBE.IsBilled == 1)
                {
                    lblAlertMsg.Text = "This Customer having readings you can not Adjust.";
                    mpeAlert.Show();
                    btnAlertOk.Focus();
                    divdetails.Visible = btnSave.Visible = false;
                    txtAccountNo.Text = txtCurrentReading.Text = string.Empty;
                }
                else if (_objBillingBE.IsMaxReading)
                {
                    lblAlertMsg.Text = "Current Reading should not be greater than the Last Present Reading.";
                    mpeAlert.Show();
                    btnAlertOk.Focus();
                    divdetails.Visible = btnSave.Visible = false;
                    txtAccountNo.Text = txtCurrentReading.Text = string.Empty;
                }
                else if (_objBillingBE.IsMinReading)
                {
                    lblAlertMsg.Text = "Current Reading should not be less than the Last Previous Reading.";
                    mpeAlert.Show();
                    btnAlertOk.Focus();
                    divdetails.Visible = btnSave.Visible = false;
                    txtAccountNo.Text = txtCurrentReading.Text = string.Empty;
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods
        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}