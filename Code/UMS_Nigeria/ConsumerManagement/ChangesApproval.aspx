﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangesApproval.aspx.cs"
    Inherits="UMS_Nigeria.ConsumerManagement.ChangesApproval" Title="::Customer Info Change Approval::"
    MasterPageFile="~/MasterPages/EBMS.Master" Theme="Green" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upStates" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddCycle" runat="server" Text="Customer Change Approvals"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="litBusinessUnit" runat="server" Text="Function"></asp:Literal>
                                <span class="span_star">*</span><br />
                                <asp:DropDownList ID="ddlFunctions" AutoPostBack="true" OnSelectedIndexChanged="ddlFunctions_SelectedIndexChanged"
                                    runat="server" CssClass="text-box select_box">
                                </asp:DropDownList>
                                <br />
                                <span id="spanBU" class="span_color"></span>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div id="divTarrifApprovalsList" runat="server" visible="false">
                        <div id="divgrid" class="grid marg_5" runat="server" style="width: 99% !important;
                            float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="litBuList" runat="server" Text="Customer Tariff Change Logs"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="divPrint" runat="server" align="center" style="float: left;
                            width: 97%; margin-left: 20px;">
                            <asp:GridView ID="gvTarrifApprovals" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%" OnRowDataBound="gvTarrifApprovals_RowDataBound">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOriGlobalAccNo" Visible="false" runat="server" Text='<%#Eval("AccountNo") %>'></asp:Label>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-Width="8%"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Tarrif" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldTarrifId" runat="server" Text='<%#Eval("OldClassID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblOldTarrif" runat="server" Text='<%#Eval("PreviousTariffName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Tarrif" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewTarrifId" runat="server" Text='<%#Eval("NewClassID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblNewTarrif" runat="server" Text='<%#Eval("ChangeRequestedTariffName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Cluster Type" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldClusterTypeId" runat="server" Text='<%#Eval("OldClusterTypeId") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblOldClusterType" runat="server" Text='<%#Eval("PreviousClusterName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Cluster Type" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewClusterTypeId" runat="server" Text='<%#Eval("NewClusterTypeId") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblNewClusterType" runat="server" Text='<%#Eval("ChangeRequestedClusterName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="10%" HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("TariffChangeRequestId") %>'
                                                    CommandName="approve" ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>"
                                                    OnClick="lnkApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkReject" CommandArgument='<%#Eval("TariffChangeRequestId") %>'
                                                    CommandName="reject" ForeColor="Green" runat="server" Text="Other" OnClick="lnkReject_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divNameApprovalsList" runat="server" visible="false">
                        <div id="div2" class="grid marg_5" runat="server" style="width: 99% !important; float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal1" runat="server" Text="Customer Name Change Logs"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="div3" runat="server" align="center" style="float: left; width: 97%;
                            margin-left: 20px;">
                            <asp:GridView ID="gvNameApprovals" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%" OnRowDataBound="gvNameApprovals_RowDataBound">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Name" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldTitle" runat="server" Text='<%#Eval("OldName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Name" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewTitle" runat="server" Text='<%#Eval("NewName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Known As" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldKnownAs" runat="server" Text='<%#Eval("OldKnownAs") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Known As" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewKnownAs" runat="server" Text='<%#Eval("KnownAs") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("NameChangeLogId") %>' CommandName="approve"
                                                    ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>" OnClick="lnkNameApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("NameChangeLogId") %>' CommandName="reject"
                                                    ForeColor="Green" runat="server" Text="Other" OnClick="lnkNameOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divTypeApprovalsList" runat="server" visible="false">
                        <div id="div4" class="grid marg_5" runat="server" style="width: 99% !important; float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal3" runat="server" Text="Customer Type Change Logs"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="div5" runat="server" align="center" style="float: left; width: 97%;
                            margin-left: 20px;">
                            <asp:GridView ID="gvTypeApprovals" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%" OnRowDataBound="gvTypeApprovals_RowDataBound">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Customer Type" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldTypeId" runat="server" Text='<%#Eval("OldCustomerTypeId") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblOldType" runat="server" Text='<%#Eval("OldCustomerType") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Customer Type" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewTypeId" runat="server" Text='<%#Eval("NewCustomerTypeId") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblNewType" runat="server" Text='<%#Eval("NewCustomerType") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("CustomerTypeChangeLogId") %>'
                                                    CommandName="approve" ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>"
                                                    OnClick="lnkTypeApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("CustomerTypeChangeLogId") %>'
                                                    CommandName="reject" ForeColor="Green" runat="server" Text="Other" OnClick="lnkTypeOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divStatusApprovalList" runat="server" visible="false">
                        <div id="div6" class="grid marg_5" runat="server" style="width: 99% !important; float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal4" runat="server" Text="Customer Status Change Logs"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="div7" runat="server" align="center" style="float: left; width: 97%;
                            margin-left: 20px;">
                            <asp:GridView ID="gvStatusApprovals" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%" OnRowDataBound="gvStatusApprovals_RowDataBound">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldStatusd" runat="server" Text='<%#Eval("OldStatusId") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblOldStatus" runat="server" Text='<%#Eval("OldStatus") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewStatusId" runat="server" Text='<%#Eval("NewStatusId") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblNewStatus" runat="server" Text='<%#Eval("NewStatus") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("ActiveStatusChangeLogId") %>'
                                                    CommandName="approve" ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>"
                                                    OnClick="lnkStatusApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("ActiveStatusChangeLogId") %>'
                                                    CommandName="reject" ForeColor="Green" runat="server" Text="Other" OnClick="lnkStatusOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divMeterApprovalList" runat="server" visible="false">
                        <div id="div8" class="grid marg_5" runat="server" style="width: 99% !important; float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal5" runat="server" Text="Customer Meter Number Change Logs"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="div9" runat="server" align="center" style="float: left; width: 97%;
                            margin-left: 20px;">
                            <asp:GridView ID="gvMeterApprovals" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%" OnRowDataBound="gvMeterApprovals_RowDataBound">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Meter Number" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldMeterNo" runat="server" Text='<%#Eval("OldMeterNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Meter Number" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewMeterNo" runat="server" Text='<%#Eval("NewMeterNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Meter Reading" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMeterReading" runat="server" Text='<%#Eval("OldMeterReading") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Meter Reading" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewMeterReading" runat="server" Text='<%#Eval("NewMeterInitialReading") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Meter Change Date" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMeterChangeDate" runat="server" Text='<%#Eval("MeterChangedDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("MeterInfoChangeLogId") %>'
                                                    CommandName="approve" ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>"
                                                    OnClick="lnkMeterApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("MeterInfoChangeLogId") %>'
                                                    CommandName="reject" ForeColor="Green" runat="server" Text="Other" OnClick="lnkMeterOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divAddressApprovalsList" runat="server" visible="false">
                        <div id="div1" class="grid marg_5" runat="server" style="width: 99% !important; float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="litAddress" runat="server" Text="Customer Address Change Logs"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="divAddress" runat="server" align="center" style="float: left;
                            width: 97%; margin-left: 20px;">
                            <asp:GridView ID="gvAddressApprovals" runat="server" AutoGenerateColumns="False"
                                AlternatingRowStyle-CssClass="color" Width="100%" OnRowDataBound="gvAddressApprovals_RowDataBound">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Postal Address" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldPAddress" runat="server" Text='<%#Eval("OldPostalAddress") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Postal Address" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewPAddress" runat="server" Text='<%#Eval("NewPostalAddress") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Service Address" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldSAddress" runat="server" Text='<%#Eval("OldServiceAddress") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Service Address" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewSAddress" runat="server" Text='<%#Eval("NewServiceAddress") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("AddressChangeLogId") %>'
                                                    CommandName="approve" ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>"
                                                    OnClick="lnkAddressApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("AddressChangeLogId") %>'
                                                    CommandName="reject" ForeColor="Green" runat="server" Text="Other" OnClick="lnkAddressOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divReadToDirectApprovalsList" runat="server" visible="false">
                        <div id="div10" class="grid marg_5" runat="server" style="width: 99% !important;
                            float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal6" runat="server" Text="Customer Read To Direct Change Logs"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="div11" runat="server" align="center" style="float: left; width: 97%;
                            margin-left: 20px;">
                            <asp:GridView ID="gvReadToDirectCust" runat="server" AutoGenerateColumns="False"
                                AlternatingRowStyle-CssClass="color" Width="100%" OnRowDataBound="gvReadToDirectCust_RowDataBound">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Meter No" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMeterNo" runat="server" Text='<%#Eval("MeterNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("ReadToDirectId") %>' CommandName="approve"
                                                    ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>" OnClick="lnkReadToDirectApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("ReadToDirectId") %>' CommandName="reject"
                                                    ForeColor="Green" runat="server" Text="Other" OnClick="lnkReadToDirectOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divAssignMeterApprovalsList" runat="server" visible="false">
                        <div id="div13" class="grid marg_5" runat="server" style="width: 99% !important;
                            float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal7" runat="server" Text="Customer Assigned Meter Change Logs"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="div14" runat="server" align="center" style="float: left; width: 97%;
                            margin-left: 20px;">
                            <asp:GridView ID="gvAssignMeter" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%" OnRowDataBound="gvAssignMeter_RowDataBound">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Meter No" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMeterNo" runat="server" Text='<%#Eval("MeterNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Initial Reading" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInitialReading" runat="server" Text='<%#Eval("InitialReading") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Meter Assigned Date" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMeterAssignedDate" runat="server" Text='<%#Eval("MeterAssignedDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("AssignedMeterId") %>' CommandName="approve"
                                                    ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>" OnClick="lnkAssignApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("AssignedMeterId") %>' CommandName="reject"
                                                    ForeColor="Green" runat="server" Text="Other" OnClick="lnkAssignOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divBookNoApprovalList" runat="server" visible="false">
                        <div id="div15" class="grid marg_5" runat="server" style="width: 99% !important;
                            float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal8" runat="server" Text="Customer Book No Change Logs"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="div16" runat="server" align="center" style="float: left; width: 97%;
                            margin-left: 20px;">
                            <asp:GridView ID="gvBookNoApproval" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%" OnRowDataBound="gvBookNoApproval_RowDataBound">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Postal Address" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldPAddress" runat="server" Text='<%#Eval("OldPostalAddress") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Postal Address" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewPAddress" runat="server" Text='<%#Eval("NewPostalAddress") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Service Address" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldSAddress" runat="server" Text='<%#Eval("OldServiceAddress") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Service Address" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewSAddress" runat="server" Text='<%#Eval("NewServiceAddress") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Book Details" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldBook" runat="server" Text='<%#Eval("OldBookNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Book Details" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <div id="lblNewBook" runat="server">
                                                <%#Eval("NewBookNo") %></div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("BookNoChangeLogId") %>'
                                                    CommandName="approve" ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>"
                                                    OnClick="lnkBookApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("BookNoChangeLogId") %>' CommandName="reject"
                                                    ForeColor="Green" runat="server" Text="Other" OnClick="lnkBookOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divMeterReadingApproval" runat="server" visible="false">
                        <div id="div17" class="grid marg_5" runat="server" style="width: 99% !important;
                            float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal9" runat="server" Text="Customer Meter Readings"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="div18" runat="server" align="center" style="float: left; width: 97%;
                            margin-left: 20px;">
                            <asp:GridView ID="gvMeterReadings" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%" OnRowDataBound="gvMeterReadingsApproval_RowDataBound">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAccountNumber" runat="server" Text='<%#Eval("AccountNumber") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Meter No" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMeterNo" runat="server" Text='<%#Eval("MeterNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Previous Reading" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPreviousReading" runat="server" Text='<%#Eval("PreviousReading") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Present Reading" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentReading" runat="server" Text='<%#Eval("PresentReading") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Average Usage" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAverageUsage" runat="server" Text='<%#Eval("AverageUsage") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Usage" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUsage" runat="server" Text='<%#Eval("Usage") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Read Date" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblReadDate" runat="server" Text='<%#Eval("ReadDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Meter Reading From" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMeterReadingFromId" runat="server" Visible="false" Text='<%#Eval("MeterReadingFromId") %>'></asp:Label>
                                            <asp:Label ID="lblMeterReadingFrom" runat="server" Text='<%#Eval("MeterReadingFrom") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsRollover" runat="server" Text='<%#Eval("IsRollOver") %>' Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("MeterReadingLogId") %>'
                                                    CommandName="approve" ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>"
                                                    OnClick="lnkMeterReadingApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("MeterReadingLogId") %>' CommandName="reject"
                                                    ForeColor="Green" runat="server" Text="Other" OnClick="lnkMeterReadingOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divPaymentsApproval" runat="server" visible="false">
                        <div id="div19" class="grid marg_5" runat="server" style="width: 99% !important;
                            float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal10" runat="server" Text="Customer Payments"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="div20" runat="server" align="center" style="float: left; width: 97%;
                            margin-left: 20px;">
                            <asp:GridView ID="gvPayments" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%" OnRowDataBound="gvPaymentsApproval_RowDataBound">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Receipt Number" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblReceiptNumber" runat="server" Text='<%#Eval("ReceiptNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Payment Mode" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewPAddress" runat="server" Text='<%#Eval("PaymentMode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Paid Amount" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPaidAmount" runat="server" Text='<%#Eval("PaidAmount") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Paid Date" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPaidDate" runat="server" Text='<%#Eval("PaidDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("PaymentLogId") %>' CommandName="approve"
                                                    ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>" OnClick="lnkPaymentsApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("PaymentLogId") %>' CommandName="reject"
                                                    ForeColor="Green" runat="server" Text="Other" OnClick="lnkPaymentsOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divAvgChangeApproval" runat="server" visible="false">
                        <div id="div21" class="grid marg_5" runat="server" style="width: 99% !important;
                            float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal11" runat="server" Text="Direct Customer Average upload change"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="div22" runat="server" align="center" style="float: left; width: 97%;
                            margin-left: 20px;">
                            <asp:GridView ID="gvAvgChange" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%" OnRowDataBound="gvAvgChange_RowDataBound">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Previous Average" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPreviousAverage" runat="server" Text='<%#Eval("PreviousAverage") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Average" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewAverage" runat="server" Text='<%#Eval("NewAverage") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("AverageChangeLogId") %>'
                                                    CommandName="approve" ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>"
                                                    OnClick="lnkAvgChangeApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("AverageChangeLogId") %>'
                                                    CommandName="reject" ForeColor="Green" runat="server" Text="Other" OnClick="lnkAvgChangeOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divAdjustmentApproval" runat="server" visible="false">
                        <div id="div23" class="grid marg_5" runat="server" style="width: 99% !important;
                            float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal12" runat="server" Text="Change Customer Adjustment Approval"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="divAdjustmentApprovalgrid" runat="server" align="center" style="float: left;
                            width: 97%; margin-left: 20px; overflow-x: scroll; overflow-y: hidden;">
                            <asp:GridView ID="gvAdjustment" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%" OnRowDataBound="gvAdjustment_RowDataBound">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bill Number" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBillNumber" runat="server" Text='<%#Eval("BillNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Adjustment Type" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBillAdjustmentTypeId" runat="server" Text='<%#Eval("BillAdjustmentType") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Meter No" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMeterNumber" runat="server" Text='<%#Eval("MeterNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Previous Reading" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPreviousReading" runat="server" Text='<%#Eval("PreviousReading") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Current Reading Before Adjustment" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCurrentReadingBeforeAdjustment" runat="server" Text='<%#Eval("CurrentReadingBeforeAdjustment") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Current Reading After Adjustment" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCurrentReadingAfterAdjustment" runat="server" Text='<%#Eval("CurrentReadingAfterAdjustment") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Consumption" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblConsumption" runat="server" Text='<%#Eval("Consumption") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Adjusted Units" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAdjustedUnits" runat="server" Text='<%#Eval("AdjustedUnits") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tax Effected" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTaxEffected" runat="server" Text='<%#Eval("TaxEffected") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total Amount Effected" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotalAmountEffected" runat="server" Text='<%#Eval("TotalAmountEffected") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Energy Charges" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEnergyCharges" runat="server" Text='<%#Eval("EnergryCharges") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Additional Charges" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAdditionalCharges" runat="server" Text='<%#Eval("AdditionalCharges") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Request Date" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRequestDate" runat="server" Text='<%#Eval("RequestDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Action" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("AdjustmentLogId") %>' CommandName="approve"
                                                    ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>" OnClick="lnkAdjustmentApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("AdjustmentLogId") %>' CommandName="reject"
                                                    ForeColor="Green" runat="server" Text="Other" OnClick="lnkAdjustmentOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divPresentMeterReadingAdjustment" runat="server" visible="false">
                        <div id="div12" class="grid marg_5" runat="server" style="width: 99% !important;
                            float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal42" runat="server" Text="Customer Present Meter Reading Adjusted Logs"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="div25" runat="server" align="center" style="float: left; width: 97%;
                            margin-left: 20px;">
                            <asp:GridView ID="gvPresentMeterReading" runat="server" AutoGenerateColumns="False"
                                AlternatingRowStyle-CssClass="color" Width="100%" OnRowDataBound="gvPresentMeterReading_RowDataBound">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Last Previous Reading" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPreviousReading" runat="server" Text='<%#Eval("PreviousReading") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Last Present Reading" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentReading" runat="server" Text='<%#Eval("PresentReading") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Previous Read Date" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPreviousReadDate" runat="server" Text='<%#Eval("PreviousReadDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Adjusted Reading" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewPresentReading" runat="server" Text='<%#Eval("NewPresentReading") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Adjusted Reading Date" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentReadDate" runat="server" Text='<%#Eval("PresentReadDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Meter Number" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMeterNumber" runat="server" Text='<%#Eval("MeterNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("PresentReadingAdjustmentLogId") %>'
                                                    CommandName="approve" ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>"
                                                    OnClick="lnkPresentApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("PresentReadingAdjustmentLogId") %>'
                                                    CommandName="reject" ForeColor="Green" runat="server" Text="Other" OnClick="lnkPresentOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divCustomerRegistrationApproval" runat="server" visible="false">
                        <div id="div24" class="grid marg_5" runat="server" style="width: 99% !important;
                            float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal13" runat="server" Text="Change Customer Registration Approval"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="divCustomerRegistrationApprovalgrid" runat="server" align="center"
                            style="float: left; width: 97%; margin-left: 20px; overflow-x: scroll; overflow-y: hidden;">
                            <asp:GridView ID="gvCustomerRegistration" runat="server" AutoGenerateColumns="False"
                                AlternatingRowStyle-CssClass="color" Width="100%" OnRowDataBound="gvCustomerRegistration_RowDataBound">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Global Account Number" runat="server" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalAccountNumber" runat="server" Text='<%#Eval("GlobalAccountNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Full Name" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFirstNameLandlord" runat="server" Text='<%#Eval("FullName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Customer Type" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCustomerType" runat="server" Text='<%#Eval("CustomerType") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tariff" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTariff" runat="server" Text='<%#Eval("Tariff") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Read Type" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblReadType" runat="server" Text='<%#Eval("ReadType") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phase" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPhaseId" runat="server" Text='<%#Eval("PhaseId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Route Name" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRouteName" runat="server" Text='<%#Eval("RouteName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Business Unit" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBusinessUnitName" runat="server" Text='<%#Eval("BusinessUnit") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Service Unit" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblServiceUnitName" runat="server" Text='<%#Eval("ServiceUnitName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Service Center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblServiceCenterName" runat="server" Text='<%#Eval("ServiceCenterName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Book Group" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBookGroup" runat="server" Text='<%#Eval("CycleName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Book Number" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBookCode" runat="server" Text='<%#Eval("BookCode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Detail" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkgvCustomerRegistrationDetails" CommandArgument='<%#Eval("ARID") %>'
                                                CommandName="details" ForeColor="Green" runat="server" Text="View" OnClick="lnkgvCustomerRegistrationDetails_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Action" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkgvCustomerRegistrationApprove" CommandArgument='<%#Eval("ARID") %>'
                                                    CommandName="approve" ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>"
                                                    OnClick="lnkgvCustomerRegistrationApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkgvCustomerRegistrationOther" CommandArgument='<%#Eval("ARID") %>'
                                                    CommandName="reject" ForeColor="Green" runat="server" Text="Other" OnClick="lnkgvCustomerRegistrationOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="consumer_total">
                        <div class="pad_10">
                        </div>
                    </div>
                </div>
            </div>
            <asp:ModalPopupExtender ID="mpeCustomerGAN" runat="server" PopupControlID="pnlCustomerGAN"
                TargetControlID="hdnCustomerGAN" BackgroundCssClass="modalBackground" CancelControlID="Button2">
            </asp:ModalPopupExtender>
            <asp:HiddenField ID="hdnCustomerGAN" runat="server" />
            <asp:Panel ID="pnlCustomerGAN" runat="server" CssClass="modalPopup" Style="display: none;">
                <div class="success">
                    <asp:Label ID="lblMsgGAN" runat="server"></asp:Label>
                </div>
                <div class="footer popfooterbtnrt">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="Button2" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="ok_btn" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
            </asp:ModalPopupExtender>
            <asp:Button ID="btnActivate" runat="server" Text="Button" CssClass="popbtn" />
            <asp:Panel ID="PanelActivate" runat="server" DefaultButton="btnActiveOk" CssClass="modalPopup"
                class="poppnl" Style="display: none;">
                <div class="popheader popheaderlblgreen" id="pop" runat="server">
                    <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                </div>
                <br />
                <div class="footer popfooterbtnrt">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="btnActiveOk" runat="server" OnClick="btnActiveOk_Click" Text="<%$ Resources:Resource, PROCEED%>"
                            CssClass="ok_btn" />
                        <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                            CssClass="cancel_btn" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
            </asp:ModalPopupExtender>
            <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
            <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                <div class="popheader popheaderlblred">
                    <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                </div>
                <div class="footer popfooterbtnrt">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                            CssClass="ok_btn" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="mpeRequestAction" runat="server" PopupControlID="pnlRequestAction"
                TargetControlID="HiddenField1" BackgroundCssClass="modalBackground" CancelControlID="btnCancel">
            </asp:ModalPopupExtender>
            <asp:HiddenField ID="HiddenField1" runat="server" />
            <asp:Panel ID="pnlRequestAction" runat="server" CssClass="modalPopup" Style="display: none;
                min-width: 362px;" DefaultButton="btnOK">
                <div class="consumer_feild">
                    <div class="Dflt_pop">
                        Request Action</div>
                    <div class="consume_name" style="margin-left: 10px;">
                        <asp:Literal ID="LiteralUnits" runat="server" Text="Select Status"></asp:Literal>
                        <span class="span_star">*</span></div>
                    <div class="consume_input">
                        <asp:DropDownList ID="ddlApprovalStatus" TabIndex="1" runat="server" onchange="DropDownlistOnChangelbl(this,'spanApprovalStatus',' Status')">
                            <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        </asp:DropDownList>
                        <span id="spanApprovalStatus" class="span_color"></span>
                    </div>
                    <div class="clear pad_5">
                    </div>
                    <div class="consume_name" style="margin-left: 10px;">
                        Remarks <span class="span_star">*</span>
                    </div>
                    <div class="consume_input">
                        <asp:TextBox CssClass="text-box" Style="width: 91%;" ID="txtRemarks" runat="server"
                            TextMode="MultiLine" onblur="return TextBoxBlurValidationlbl(this,'spanRemarks','Remarks')"
                            placeholder="Enter Remarks Here"></asp:TextBox>
                        <span id="spanRemarks" class="span_color"></span>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="clear pad_10">
                    </div>
                    <div class="consume_name c_left">
                    </div>
                    <div class="fltl">
                        <asp:Button ID="btnOK" runat="server" TabIndex="2" Text="<%$ Resources:Resource, OK%>"
                            CssClass="ok_btn" OnClientClick="return ValidateRequest()" OnClick="btnActiveOk_Click" />
                        <asp:Button ID="btnCancel" runat="server" TabIndex="3" Text="<%$ Resources:Resource, CANCEL%>"
                            CssClass="ok_btn" />
                    </div>
                    <div class="clear pad_10">
                    </div>
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="mpeCustomerDetails" runat="server" PopupControlID="pnlCustomerDetails"
                TargetControlID="HiddenField2" BackgroundCssClass="modalBackground" CancelControlID="btnClose">
            </asp:ModalPopupExtender>
            <asp:HiddenField ID="HiddenField2" runat="server" />
            <asp:Panel ID="pnlCustomerDetails" runat="server" CssClass="modalPopup">
                <div style="width: 1100px; position: relative; top: 0px; height: 35px; background-color: #ffffff">
                    <div class="text-heading">
                        <asp:Literal ID="litAddState" runat="server" Text="APPROVAL PENDING REGISTRATION"></asp:Literal>
                    </div>
                    <div style="width: 21px; float: right; font-size: 12px; position: relative; margin-right: 50PX;">
                        <div class="star_text">
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="ok_btn" />
                        </div>
                    </div>
                </div>
                <div style="width: 1100px; height: 600px; overflow-x: scroll;">
                    <div>
                        <div class="inner-sec">
                            <div class="text_total" style="margin-bottom: -9px;">
                                <%-- <div class="text-heading">
                                    <asp:Literal ID="litAddState" runat="server" Text="CUSTOMER DETAILS"></asp:Literal>
                                </div>
                                <div style="width: 21px; float: right; font-size: 12px; position: relative; margin-right: 40PX;">
                                    <div class="star_text">
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="ok_btn" />
                                    </div>
                                </div>--%>
                                <div class="clr">
                                </div>
                                <div class="inner-sec">
                                    <h4 class="subHeading">
                                        <asp:Literal ID="Literal35" runat="server" Text="BOOK INFORMATION"></asp:Literal>
                                    </h4>
                                    <div class="out-bor_aDetails">
                                        <table class="customerdetailsTamle">
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal37" runat="server" Text="Business Unit"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblBuDsp" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal36" runat="server" Text="Service Unit"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblSUDsp" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal38" runat="server" Text="Service Center"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblSCDsp" runat="server" Text="--"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal39" runat="server" Text="Book Group"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblBookGroupDsp" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal40" runat="server" Text="Book Number"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblBookCodeDsp" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-sec">
                                    <h4 class="subHeading">
                                        <asp:Literal ID="Literal41" runat="server" Text="ACCOUNT STATUS DETAILS"></asp:Literal>
                                    </h4>
                                    <div class="out-bor_aDetails">
                                        <table class="customerdetailsTamle">
                                            <%--style="margin-left: -20px;"--%>
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <%--style="width: 50%"--%>
                                                    <asp:Literal ID="Literal14" runat="server" Text="Global Account No"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <%--class="customerdetailstamleTdsize"--%>
                                                    <asp:Label ID="lblGlobalAccountNo" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <%--style="width: 50%"--%>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <%--style="width: 50%"--%>
                                                    <%--<asp:Literal ID="litAccountNo" runat="server" Text="Account No"></asp:Literal>--%>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    <%--:--%>
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <%--<asp:Label ID="lblAccountNo" runat="server" Text="--"></asp:Label>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <%--<div class="inner-box1">--%>
                                <div class="out-bor_aDetails">
                                    <table class="customerdetailsTamle">
                                        <tr>
                                            <td class="customerdetailstamleTd-two">
                                                <asp:Literal ID="litCustomerType" runat="server" Text="<%$ Resources:Resource, CUSTOMER_TYPE%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd-dot">
                                                :
                                            </td>
                                            <%--<td class="customerdetailstamleTdsize">--%>
                                            <td class="customerdetailstamleTdsize-two">
                                                <asp:Label ID="lblCustomerType" runat="server" Text="--"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd-two">
                                                <asp:Literal ID="litCustomerStatus" runat="server" Text="<%$ Resources:Resource, STATUS%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd-dot">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize-two" colspan="4">
                                                <b>
                                                    <asp:Label ID="lblCustomerStatus" runat="server" Text="--"></asp:Label></b>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="clr">
                                </div>
                                <%--<div class="inner-box1">--%>
                                <div class="out-bor_aDetails">
                                    <table class="customerdetailsTamle" width="25px;">
                                        <tr>
                                            <td class="customerdetailstamleTd-two">
                                                <asp:Literal ID="litOutStanding" runat="server" Text="<%$ Resources:Resource, OUT_STANDING_AMT%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd-dot">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize-two">
                                                <asp:Label ID="lblOutStanding" runat="server" Text="--"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd-two">
                                            </td>
                                            <td class="customerdetailstamleTd-dot">
                                            </td>
                                            <td class="customerdetailstamleTdsize-two" colspan="4">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <%-------------------------Land Lord Information Start----------------------%>
                                <div class="clr">
                                </div>
                                <div class="inner-sec">
                                    <h4 class="subHeading">
                                        <asp:Literal ID="Literal213" runat="server" Text="<%$ Resources:Resource, LAND_LORD_INFO%>"></asp:Literal>
                                    </h4>
                                    <div class="out-bor_aDetails">
                                        <table class="customerdetailsTamle">
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:Resource, TITLE%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblLInfoTitle" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, F_NAME%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblLInfoFirstname" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:Resource, M_NAME%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblLInfoMiddleName" runat="server" Text="--"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:Resource, L_NAME%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblLInfoLastName" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:Resource, KNOWN_AS%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblLInfoKnownAs" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal19" runat="server" Text="<%$ Resources:Resource, EMAIL_ID%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblLInfoEmail" runat="server" Text="--"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal20" runat="server" Text="<%$ Resources:Resource, HOME%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblLInfoHomePhone" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal21222" runat="server" Text="<%$ Resources:Resource, BUSINESS%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblLInfoBussPhone" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal22" runat="server" Text="<%$ Resources:Resource, OTHERS%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblLInfoOtherPhone" runat="server" Text="--"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <%-------------------------Land Lord Information End----------------------%>
                                <%-------------------------Tenent Information Start----------------------%>
                                <table class="customerdetailsTamle" style="width: 34%">
                                    <tr>
                                        <td class="customerdetailstamleTd-two">
                                            <asp:Label ID="Label1" runat="server" Text="Is Tenant"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd-dot">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize-two">
                                            <asp:Label ID="lblIsTenant" runat="server" Text="--"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                                <div class="clr">
                                </div>
                                <div id="divTenent" runat="server" visible="false">
                                    <div class="inner-sec">
                                        <h4 class="subHeading">
                                            <asp:Literal ID="Literal23" runat="server" Text="<%$ Resources:Resource, TENENT_INFO%>"></asp:Literal>
                                        </h4>
                                        <div class="out-bor_aDetails">
                                            <table class="customerdetailsTamle">
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal24" runat="server" Text="<%$ Resources:Resource, TITLE%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblTanentTitle" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal25" runat="server" Text="<%$ Resources:Resource, F_NAME%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblTanentFirstName" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal26" runat="server" Text="<%$ Resources:Resource, M_NAME%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblTanentMiddleName" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal27" runat="server" Text="<%$ Resources:Resource, L_NAME%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblTanentLastName" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd">
                                                        <asp:Literal ID="Literal28" runat="server" Text="<%$ Resources:Resource, EMAIL_ID%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblTanentEmail" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litTHPhone" runat="server" Text="<%$ Resources:Resource, HOME%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblTanentHomeContactNo" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal29" runat="server" Text="<%$ Resources:Resource, ANOTHER_CONTACT_NO%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblTanentOtherContactNo" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                                <%-------------------------Tenent Information End----------------------%>
                                <%-------------------------Postal Information Start----------------------%>
                                <div class="clr">
                                </div>
                                <div class="inner-sec">
                                    <h4 class="subHeading">
                                        <asp:Literal ID="litPostalAddress" runat="server" Text="Postal Address"></asp:Literal>
                                    </h4>
                                    <div class="out-bor_aDetails">
                                        <table class="customerdetailsTamle">
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litPHNo" runat="server" Text="<%$ Resources:Resource, H_NO%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblPostalHouseNo" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litPStreet" runat="server" Text="<%$ Resources:Resource, STREET%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblPostalStreet" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litPLGAVillage" runat="server" Text="<%$ Resources:Resource, CITY%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblPostalVillage" runat="server" Text="--"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litPZip" runat="server" Text="<%$ Resources:Resource, ZIP%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblPostalZip" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal30" runat="server" Text="<%$ Resources:Resource, AREA_CODE%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblPostalAreaCode" runat="server" Text="--"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <%-------------------------Postal Information End----------------------%>
                                <table class="customerdetailsTamle" style="width: 50%">
                                    <tr>
                                        <td class="customerdetailstamleTd-two">
                                            <asp:Label ID="Label4" runat="server" Text="Is Same As Postal Address "></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd-dot">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize-two">
                                            <asp:Label ID="lblsSameAsService" runat="server" Text="Yes"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                                <div class="clr">
                                </div>
                                <%-------------------------Service Information Start----------------------%>
                                <div class="inner-sec">
                                    <h4 class="subHeading">
                                        <asp:Literal ID="litServiceAddress" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Literal>
                                    </h4>
                                    <div class="out-bor_aDetails">
                                        <table class="customerdetailsTamle">
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litSHNo" runat="server" Text="<%$ Resources:Resource, H_NO%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblServiceHouseNo" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd">
                                                    <asp:Literal ID="litSStreet" runat="server" Text="<%$ Resources:Resource, STREET%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblServiceStreet" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litSLGAVillage" runat="server" Text="<%$ Resources:Resource, CITY%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblServiceVillage" runat="server" Text="--"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litSZip" runat="server" Text="<%$ Resources:Resource, ZIP%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblServiceZip" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litSArea" runat="server" Text="<%$ Resources:Resource, AREA_CODE%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblServiceAreaCode" runat="server" Text="--"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="customerdetailsTamle" style="width: 41%">
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal31" runat="server" Text="<%$ Resources:Resource, COMMUNICATION_ADD%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblCommunicationAddress" runat="server" Text="--"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <%-------------------------Service Information End----------------------%>
                                    <div class="clr">
                                    </div>
                                    <h4 class="subHeading">
                                        Other Details</h4>
                                    <table class="customerdetailsTamle">
                                        <tr>
                                            <td class="customerdetailstamleTd-two">
                                                <asp:Label ID="lblIsBedc" runat="server" Text="Is BEDC Employee"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd-dot">
                                                :
                                            </td>
                                            <td style="width: 11%;" class="cDfontsize">
                                                <asp:Label ID="lblIsBedcEmployee" runat="server" Text="No"></asp:Label>
                                            </td>
                                            <td>
                                                <table>
                                                    <tr id="divEmployeeCode" runat="server" visible="false">
                                                        <td style="width: 3%; color: #1a6807; font-size: 14px; font-weight: bold;">
                                                            <asp:Literal ID="litEmployeCode" runat="server" Text="<%$ Resources:Resource, EMPLOYEE_CODE%>"></asp:Literal>
                                                        </td>
                                                        <td class="customerdetailstamleTd-dot">
                                                            :
                                                        </td>
                                                        <td class="customerdetailstamleTdsize-two">
                                                            <asp:Label ID="lblEmployeeCode" runat="server" Text="--"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="customerdetailstamleTd-two" style="width: 2%;">
                                                <asp:Label ID="Label3" runat="server" Text="Is Embassy"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd-dot">
                                                :
                                            </td>
                                            <td class="cDfontsize">
                                                <asp:Label ID="lblIsEmbassyCustomer" runat="server" Text="No"></asp:Label>
                                            </td>
                                            <td>
                                                <table id="tblEmbassyCode" runat="server" visible="false">
                                                    <tr>
                                                        <td style="width: 3%; color: #1a6807; font-size: 14px; font-weight: bold;">
                                                            <asp:Literal ID="litEmbassy" runat="server" Text="<%$ Resources:Resource, EMBASSY_CODE%>"></asp:Literal>
                                                        </td>
                                                        <td class="customerdetailstamleTd-dot">
                                                            :
                                                        </td>
                                                        <td class="customerdetailstamleTdsize-two">
                                                            <asp:Label ID="lblEmbassyCode" runat="server" Text="--"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="customerdetailstamleTd-two">
                                                <asp:Label ID="Label5" runat="server" Text="Is VIP"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd-dot">
                                                :
                                            </td>
                                            <td colspan="2" class="cDfontsize">
                                                <asp:Label ID="lblIsVipCustomer" runat="server" Text="No"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="clr">
                                    </div>
                                </div>
                            </div>
                            <%----------------------------------------------------------Step 2----------------------------------------- --%>
                            <div class="">
                                <div class="clr">
                                </div>
                                <div class="inner-sec">
                                    <h4 class="subHeading">
                                        Account Information</h4>
                                    <div class="out-bor_aDetails">
                                        <table class="customerdetailsTamle">
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litAccountType" runat="server" Text="<%$ Resources:Resource, ACCOUNT_TYPE%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblAccountType" runat="server" Text="--"></asp:Label>
                                                    <asp:Label ID="lblGovtAccountTypeName" runat="server" Visible="false" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litBook" runat="server" Text="Book Number"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblBookCode" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litPole" runat="server" Text="<%$ Resources:Resource, POLE%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblPoleNo" runat="server" Text="--"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litTariff" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblTariff" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litClusterType" runat="server" Text="<%$ Resources:Resource, CLUSTER_TYPE%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblClusterType" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litPhase" runat="server" Text="<%$ Resources:Resource, PHASE%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblPhase" runat="server" Text="--"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litRouteNo" runat="server" Text="<%$ Resources:Resource, ROUTE_NO%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblRouteNo" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litInitialBilling" runat="server" Text="<%$ Resources:Resource, INITIAL_BILLING_KWH%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblInitialBillingkWh" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal32" runat="server" Text="<%$ Resources:Resource, READCODE%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblReadCode" runat="server" Text="--"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="divReadCustomer" runat="server" visible="false">
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litMeterNoDisplay" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblMeterNo" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Label ID="lblcm" runat="server" Text="Is CAPMI : "></asp:Label>
                                                    <%--<asp:Literal ID="Literal20" runat="server" Text="Present Reading"></asp:Literal>--%>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblIsCapmy" runat="server" Text="No"></asp:Label>
                                                    <%--  <asp:Label ID="lblPresentReading" runat="server"Text="--"></asp:Label>--%>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litAmount" runat="server" Text="<%$ Resources:Resource, AMOUNT%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblMeterAmount" runat="server" Text="--"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="divDirectCustomer" runat="server" visible="false">
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal33" runat="server" Text="<%$ Resources:Resource, AVERAGE_READING%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblAverageReading" runat="server" Text="--"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trPresentReading" runat="server" visible="false">
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litCapmiOutstandingAmount" runat="server" Text="CAPMI Outstanding"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblCapmiOutstandingAmount" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal34" runat="server" Text="Initial Meter Reading"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize">
                                                    <asp:Label ID="lblPresentReading" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                </td>
                                                <td class="customerdetailstamleTd">
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <%--Popus Govt Account Type Ends--%>
                                <div class="clr">
                                </div>
                                <div class="inner-sec">
                                    <h4 class="subHeading">
                                        Setup Information</h4>
                                    <div class="out-bor_aDetails">
                                        <table class="customerdetailsTamle">
                                            <tr>
                                                <td class="customerdetailstamleTd">
                                                    <asp:Literal ID="litApplicationDate" runat="server" Text="<%$ Resources:Resource, APPLICATION_DATE%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblApplicationDate" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litConnectionDate" runat="server" Text="<%$ Resources:Resource, CONNECTION_DATE%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblConnectionDate" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litSetupDate" runat="server" Text="<%$ Resources:Resource, SETUP_DATE%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblSetUpDate" runat="server" Text="--"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litCertifiedBy" runat="server" Text="<%$ Resources:Resource, CERTIFIED_BY%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblCertifiedBy" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litSeal1" runat="server" Text="<%$ Resources:Resource, SEAL_1%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblSeal1" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litSeal2" runat="server" Text="<%$ Resources:Resource, SEAL_2%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblSeal2" runat="server" Text="--"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litOldAccountNo" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblOldAccount" runat="server" Text="--"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <table class="customerdetailsTamle" style="margin-left: 6px; width: 50%;">
                                    <tr>
                                        <td class="customerdetailstamleTd-change">
                                            <asp:Literal ID="litApplicationProcessedBy" runat="server" Text="<%$ Resources:Resource, APPLICATION_PROCCESED_BY%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd-dot">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize-twochange">
                                            <asp:Label ID="lblAppProcessedBy" runat="server" Text="--"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="customerdetailstamleTd-two">
                                            <asp:Literal ID="litInstalledBy" runat="server" Text="<%$ Resources:Resource, INSTALLED_BY%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd-dot">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize-two">
                                            <asp:Label ID="lblInstallByName" runat="server" Text="--"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="out-bor" id="divIdentityInfo" runat="server" visible="false">
                            <div class="clr">
                            </div>
                            <div class="inner-sec">
                                <h4 class="subHeading">
                                    Identity Information</h4>
                                <div class="out-bor_aDetails">
                                    <asp:Repeater ID="rptrIdentityDetails" runat="server">
                                        <ItemTemplate>
                                            <table class="customerdetailsTamle">
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litIdentityType" runat="server" Text="Type"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <%#Eval("IdentityTypeIdList")%>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litIdentityNo" runat="server" Text="Number"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <%#Eval("IdentityNumberList")%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="out-bor" id="divUploadsList" runat="server" visible="false">
                            <div class="clr">
                            </div>
                            <div class="inner-sec">
                                <h4 class="subHeading">
                                    Uploads</h4>
                                <div class="out-bor_aDetails">
                                    <asp:Repeater ID="rptrGetCustomerDocuments" runat="server">
                                        <ItemTemplate>
                                            <table class="customerdetailsTamle">
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal22" runat="server" Text="Document"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <%#Eval("DocumentName")%>
                                                    </td>
                                                </tr>
                                            </table>
                                            <%-- <%#Eval("Path")%>--%>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="out-bor" id="divUDFList" runat="server" visible="false">
                            <div class="clr">
                            </div>
                            <div class="inner-sec">
                                <h4 class="subHeading">
                                    User Defined Fields</h4>
                                <div class="out-bor_aDetails">
                                    <asp:Repeater ID="rptrGetCustomerUDFValues" runat="server">
                                        <ItemTemplate>
                                            <table class="customerdetailsTamle">
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal24" runat="server" Text='<%#Eval("UDFTypeIdList")%>'></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <%#Eval("UDFValueList")%>
                                                    </td>
                                                    <%--   <td class="customerdetailstamleTd-two">
                                            <asp:Literal ID="Literal25" runat="server" Text="Ref Doc"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd-dot">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize-two">
                                            <%#Eval("UDFTypeIdList")%>
                                        </td>--%>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:HiddenField ID="hfRecognize" runat="server" />
            <asp:HiddenField ID="hfAccountNo" runat="server" />
            <asp:HiddenField ID="hfTariffChangeRequestId" runat="server" />
            <asp:HiddenField ID="hfClusterTypeId" runat="server" />
            <asp:HiddenField ID="hfclassId" runat="server" />
            <asp:HiddenField ID="hfNameChangeLogId" runat="server" />
            <asp:HiddenField ID="hfTypeChangeLogId" runat="server" />
            <asp:HiddenField ID="hfStatusChangeLogId" runat="server" />
            <asp:HiddenField ID="hfAddressChangeLogId" runat="server" />
            <asp:HiddenField ID="hfReadToDirectChangeLogId" runat="server" />
            <asp:HiddenField ID="hfAssignedMeterId" runat="server" />
            <asp:HiddenField ID="hfBookNoChangeLogId" runat="server" />
            <asp:HiddenField ID="hfMeterReadingLogId" runat="server" />
            <asp:HiddenField ID="hfPaymentLogId" runat="server" />
            <asp:HiddenField ID="hfAdjustmentLogId" runat="server" />
            <asp:HiddenField ID="hfAverageChangeLogId" runat="server" />
            <asp:HiddenField ID="hfMeterNumberChangeLogId" runat="server" />
            <asp:HiddenField ID="hfMeterReadingFromId" runat="server" />
            <asp:HiddenField ID="hfIsRollOver" runat="server" />
            <asp:HiddenField ID="hfARID" runat="server" />
            <asp:HiddenField ID="hfPresentMeterReadingChangeLogId" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--==============Ajax loading script starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/ChangesApproval.js" type="text/javascript"></script>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <script type="text/javascript">
        function pageLoad() {
            DisplayMessage('UMSNigeriaBody_pnlMessage');
        }; 
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
