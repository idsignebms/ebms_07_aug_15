﻿<%@ Page Title="::Consumer Registration::" Language="C#" MasterPageFile="~/MasterPages/EDMUMS.Master"
    AutoEventWireup="true" CodeBehind="ConsumerRegistration.aspx.cs" Inherits="UMS_Nigeria.ConsumerRegistration" %>

<%@ Register Assembly="EditableDropDownList" Namespace="EditableControls" TagPrefix="editable" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--    <script src="scripts/jquery-2.0.0.min.js" type="text/javascript"></script>--%>
    <%--<script src="scripts/jquery-1.6.4.min.js" type="text/javascript"></script>--%>
    <script src="../scripts/jquery.smartWizard.js" type="text/javascript"></script>
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../scripts/jquery.ui.widget.js" type="text/javascript"></script>
    <%-- <script src="../scripts/jquery.ui.button.js" type="text/javascript"></script>
    <script src="../scripts/jquery.ui.position.js" type="text/javascript"></script>
    <script src="../scripts/jquery.ui.autocomplete.js" type="text/javascript"></script>
    <script src="../scripts/jquery.ui.combobox.js" type="text/javascript"></script>--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <script src="../scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <%--<link href="../Styles/style.css" rel="stylesheet" type="text/css" />--%>
    <%--<script type="text/javascript">
        function doSubmit1() {
            document.getElementById("__LASTFOCUS").value = document.activeElement.id;

        }
    </script>--%>
    <script type="text/javascript">
        //        function IsDocumentNoExists(inputData) {
        //            var exists = true;
        //            var documentNo = $('#spanDocumentNo');
        //            //            if (TextBoxBlurValidationlbl(inputData, 'spanDocumentNo', 'Document No') == false) {
        //            //                exists = false;
        //            //            }
        //            if (Trim(inputData.value) != "") {
        //                alert(inputData.value)
        //                var pagePath = window.location.pathname;
        //                //pnlLoading.setAttribute("style", "display:block;position:fixed;");            
        //                $.ajax({
        //                    type: "POST",
        //                    url: pagePath + "/IsDocumentNoExists",
        //                    data: "{ 'DocumentNo': '" + $.trim(inputData.value) + "' }",
        //                    dataType: "json",
        //                    contentType: "application/json; charset=utf-8",
        //                    success: function (data) {

        //                        if (data['d'] == "True") {
        //                            exists = false;
        //                            // alert("DocumentNo already exists.");
        //                            //pnlLoading.setAttribute("style", "display:none;position:fixed;");
        //                            documentNo.text("Document No already exists.");
        //                            documentNo.css("display", "block");
        //                            inputData.focus();
        //                            //return false;
        //                        }
        //                        else {
        //                            documentNo.css("display", "none");
        //                            documentNo.text("");
        //                        }
        //                    }
        //                });
        //            }
        //            else {
        //                documentNo.css("display", "none");
        //                documentNo.text("");
        //            }

        //            if (!exists) {
        //                return false;
        //            }
        //            //            else
        //            //                pnlLoading.setAttribute("style", "display:none;position:fixed;");
        //        }

        function IsSequenceNoExists(inputData) {
            var exists = true;
            var spanCustomerSequenceNo = $('#spanCustomerSequenceNo');
            if (TextBoxBlurValidationlbl(inputData, 'spanCustomerSequenceNo', 'Customer SequenceNo') == false) exists = false;
            else {
                var pagePath = window.location.pathname;
                $.ajax({
                    type: "POST",
                    url: pagePath + "/IsSequenceNoExists",
                    data: "{ 'SequenceNo': '" + $.trim(inputData.value) + "' }",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data['d'] == "True") {
                            exists = false;
                            // alert("DocumentNo already exists.");
                            spanCustomerSequenceNo.text("Sequence No already exists.");
                            spanCustomerSequenceNo.css("display", "block");
                            inputData.focus();
                            //return false;
                        }
                    }
                });
            }
            if (!exists) {
                return false;
            }
        }


        function IsMeterNoExists(inputData) {
            var exists = true;
            var spanMeterNo = $('#spanMeterNo');
            var spanInitialReading = $('#spanInitialReading');
            var txtInitialReading = document.getElementById('<%= txtInitialReading.ClientID %>').disabled = false;
            if (TextBoxBlurValidationlbl(inputData, 'spanMeterNo', 'Meter No') == false) exists = false;
            else {
                var pagePath = window.location.pathname;
                $.ajax({
                    type: "POST",
                    url: pagePath + "/IsMeterNoExists",
                    data: "{ 'MeterNo': '" + $.trim(inputData.value) + "' }",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data['d'] == "True") {
                            exists = false;
                            spanMeterNo.text("Meter No Is Invalid.");
                            spanMeterNo.css("display", "block");
                            var txtInitialReading = document.getElementById('<%= txtInitialReading.ClientID %>').disabled = true;
                            inputData.focus();
                        }
                    }
                });
            }
            if (!exists) {
                var txtInitialReading = document.getElementById('<%= txtInitialReading.ClientID %>').disabled = true;
                spanInitialReading.css("display", "none");
                return false;
            }
        }

        function MeterDials(inputData) {
            var exists = true;
            var txtMeterNo = document.getElementById('<%= txtMeterNo.ClientID %>');
            var hfMeterDials = document.getElementById('<%=hfMeterDials.ClientID %>').value;
            var spanMeterNo = $('#spanInitialReading');
            var txtInitialReading = document.getElementById('<%= txtInitialReading.ClientID %>');
            if (TextBoxValidationlblZeroAccepts(txtInitialReading, document.getElementById("spanInitialReading"), "Initial Reading") == false) exists = false;
            else {
                var pagePath = window.location.pathname;
                $.ajax({
                    type: "POST",
                    url: pagePath + "/MeterDials",
                    data: "{ 'MeterNo': '" + $.trim(txtMeterNo.value) + "' }",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data['d'] == txtInitialReading.value.length) {
                            hfMeterDials.value = data['d'];
                            spanMeterNo.css("display", "none");
                        }
                        else {
                            exists = false;
                            spanMeterNo.text("Initial reading must be " + data['d'] + " characters");
                            spanMeterNo.css("display", "block");
                            inputData.focus();
                        }
                    }
                });
            }

            if (!exists) {
                return false;
            }
        }

        function IsOldAccountNoExists(inputData) {
            var exists = true;
            var spanOldAccountNo = $('#spanOldAccountNo');
            //            if (TextBoxBlurValidationlbl(inputData, 'spanOldAccountNo', 'Old Account No') == false) exists = false;
            //            else {
            var pagePath = window.location.pathname;
            $.ajax({
                type: "POST",
                url: pagePath + "/IsOldAccountNoExists",
                data: "{ 'OldAccountNo': '" + $.trim(inputData.value) + "' }",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data['d'] == "True") {
                        exists = false;
                        spanOldAccountNo.text("Old Account No already exists.");
                        spanOldAccountNo.css("display", "block");
                        inputData.focus();
                    }
                    else {
                        spanOldAccountNo.css("display", "none");
                    }
                }
            });
            //            }
            if (!exists) {
                return false;
            }
        }

        function IsNeighborAccountNoExists(inputData) {
            var exists = true;
            var spanneighbor = $('#spanneighbor');
            spanneighbor.text("");
            //            if (TextBoxBlurValidationlbl(inputData, 'spanneighbor', 'Neighbour account no') == false) exists = false;
            //            else {
            if (Trim(inputData.value) != "") {
                var pagePath = window.location.pathname;
                $.ajax({
                    type: "POST",
                    url: pagePath + "/IsNeighborAccountNoExists",
                    data: "{ 'NeighbourAccNo': '" + $.trim(inputData.value) + "' }",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (inputData.value != "") {
                            if (data['d'] == "False") {
                                exists = false;
                                spanneighbor.text("Neighbour account no does not exists.");
                                spanneighbor.css("display", "block");
                                inputData.focus();
                            }
                            else {
                                spanneighbor.text("");
                                spanneighbor.css("display", "1px solid #8E8E8E");
                            }
                        }
                    }
                });
                //}
            }
            if (!exists) {
                return false;
            }
        }

        function IsMeterSerialNoExists(inputData) {
            var exists = true;
            var spanMeterSerialNo = $('#spanMeterSerialNo');
            //if (TextBoxBlurValidationlbl(inputData, 'spanMeterSerialNo', 'Meterserialno') == false) exists = false;
            if (Trim(inputData.value) != "") {
                var pagePath = window.location.pathname;
                $.ajax({
                    type: "POST",
                    url: pagePath + "/IsMeterSerialNoExists",
                    data: "{ 'MeterSerialNo': '" + $.trim(inputData.value) + "' }",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data['d'] == "True") {
                            exists = false;
                            spanMeterSerialNo.text("Meter serialno already exists.");
                            spanMeterSerialNo.css("display", "block");
                            inputData.focus();
                        }
                    }
                });
            }
            if (!exists) {
                return false;
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            Calendar('<%=txtApplicationDate.ClientID %>', '<%=imgDate.ClientID %>', "-2:+8");
            Calendar('<%=txtConnectionDate.ClientID %>', '<%=imgDate2.ClientID %>', "-2:+8");
            //Calendar('txtSetupDate.ClientID ', 'imgDate3.ClientID ', "-2:+8");
            // Smart Wizard 	
            $('#wizard').smartWizard({
                onLeaveStep: leaveAStepCallback,
                onFinish: onFinishCallback,
                onShowStep: ShowStep
            });

            DisplayMessage('<%= pnlMessage.ClientID %>');

            $(window).bind('beforeunload', function () {
                return 'Are you sure you want to leave? All unsaved information will be lost';
            });
        });

        function pageLoad() {
            Calendar('<%=txtApplicationDate.ClientID %>', '<%=imgDate.ClientID %>', "-4:+0");
            Calendar('<%=txtConnectionDate.ClientID %>', '<%=imgDate2.ClientID %>', "-2:+8");
            //Calendar('txtSetupDate.ClientID ', 'imgDate3.ClientID ', "-2:+8");

            // Smart Wizard 	
            $('#wizard').smartWizard({
                onLeaveStep: leaveAStepCallback,
                onFinish: onFinishCallback,
                onShowStep: ShowStep
            });

            DisplayMessage('<%= pnlMessage.ClientID %>');
        }; 
    </script>
    <script type="text/javascript">
        var i = 1;
        function AddFile() {
            i++;
            var div = document.createElement('DIV');
            div.innerHTML = '<input id="file' + i + '" name="file' + i + '" type="file" /><a href="#" style="font-weight:bold;" onclick="return RemoveFile(this)" Style="padding-top: 4px;">Remove</a>';
            document.getElementById("divFile").appendChild(div);
            return false;
        }
        function RemoveFile(file) {
            document.getElementById("divFile").removeChild(file.parentNode);
            return false;
        }
    </script>
    <script type="text/javascript">
        function onFinishCallback(objs, context) {
            if (Validate(1) == false)
                $('#wizard').smartWizard('goToStep', 1);
            else if (Validate(2) == false)
                $('#wizard').smartWizard('goToStep', 2);
            else if (Validate(3) == false)
                $('#wizard').smartWizard('goToStep', 3);
            else {


                //Document validation
                var divFile = document.getElementById("divFile");
                var Docitems = divFile.getElementsByTagName("input");
                for (var i = 0; i < Docitems.length; i++) {
                    var spanDocuments = document.getElementById("spanDocuments").innerHTML = "";
                    if (!Trim(Docitems[i].value) == "") {
                        if (!/(\.doc|\.docx|\.pdf|\.xlsx|\.xls)$/i.test(Docitems[i].value)) {
                            var spanDocuments = document.getElementById("spanDocuments");
                            spanDocuments.innerHTML = "Please select .doc, .docx, .xlsx , .pdf and .xls File Formats only";
                            spanDocuments.style.display = "block";
                            return false;
                            break;
                        }

                    }
                    else {
                        var spanDocuments = document.getElementById("spanDocuments").innerHTML = "";
                    }
                }
                var btnFinish = document.getElementById('<%= btnFinish.ClientID %>');
                $(window).unbind('beforeunload');

                btnFinish.click();
            }
        }

        function NaviagateOk() {
            $(window).unbind('beforeunload');
        }

        function leaveAStepCallback(obj, context) {
            if (context.toStep > context.fromStep)
                return Validate(context.fromStep); // return false to stay on step and true to continue navigation 
            else
                return true;
        }
        function ShowStep(obj, context) {
            var hfValidStep = document.getElementById('<%= hfValidStep.ClientID %>');
            switch (context.toStep) {
                case 1:
                    hfValidStep.value = "0";
                    break;
                case 2:
                    hfValidStep.value = "1";
                    break;
                case 3:
                    hfValidStep.value = "2";
                    break;
            }
        }

        function ValidateMobileNo() {
            var txtMobileNo = document.getElementById('<%= txtMobileNo.ClientID %>');
            var ContactNoLength = "<%= Resources.GlobalConstants.CONTACT_NO_LENGTH %>";
            ContactNoLength = parseInt(ContactNoLength);
            if (TextBoxValidationlbl(txtMobileNo, document.getElementById("spanMobileNo"), "Mobile Number") == false) return false;
            //if (TextBoxValidContactLbl(txtMobileNo, "Mobile No", document.getElementById("spanMobileNo"), ContactNoLength) == false) return false;
        }

        function CopyNames() {
            var txtName = document.getElementById('<%= txtName.ClientID %>');
            var txtSurName = document.getElementById('<%= txtSurName.ClientID %>');
            var txtKnownAs = document.getElementById('<%= txtKnownAs.ClientID %>');
            txtKnownAs.value = txtName.value + " " + txtSurName.value;
        }

        function Validate(stepnumber) {
            var ddlTitle = document.getElementById('<%= ddlTitle.ClientID %>');
            var txtName = document.getElementById('<%= txtName.ClientID %>');
            var txtSurName = document.getElementById('<%= txtSurName.ClientID %>');
            var txtDocumentNo = document.getElementById('<%= txtDocumentNo.ClientID %>');

            var txtHome = document.getElementById('<%= txtHome.ClientID %>');
            var txtBusiness = document.getElementById('<%= txtBussiness.ClientID %>');
            var txtOthers = document.getElementById('<%= txtOthers.ClientID %>');
            var txtLandMark = document.getElementById('<%= txtLandMark.ClientID %>');
            var txtHouseNo = document.getElementById('<%= txtHouseNo.ClientID %>');
            var txtCity = document.getElementById('<%= txtCity.ClientID %>');
            var txtStreet = document.getElementById('<%= txtStreet.ClientID %>');
            //var ddlState = document.getElementById( ddlState.ClientID );
            var txtPostalDetails = document.getElementById('<%= txtPostalDetails.ClientID %>');
            var txtZipCode = document.getElementById('<%= txtZipCode.ClientID %>');
            //            var txtIdentityNumber = document.getElementById('<%= txtIdentityNumber.ClientID %>');
            var txtEmailId = document.getElementById('<%= txtEmailId.ClientID %>');
            //            var ddlIdentityType = document.getElementById('<%= ddlIdentityType.ClientID %>');

            var ddlBusinessUnit = document.getElementById('<%= ddlBusinessUnit.ClientID %>');
            var ddlServiceUnit = document.getElementById('<%= ddlServiceUnit.ClientID %>');
            var ddlServiceCenter = document.getElementById('<%= ddlServiceCenter.ClientID %>');
            var ddlCycle = document.getElementById('<%= ddlCycle.ClientID %>');
            var ddlBookNumber = document.getElementById('<%= ddlBookNumber.ClientID %>');
            var txtInjectionSubStation = document.getElementById('<%= ddlInjectionSubStation.ClientID %>');
            var txtFeeder = document.getElementById('<%= ddlFeeder.ClientID %>');
            var txtServiceLandMark = document.getElementById('<%= txtServiceLandMark.ClientID %>');
            var txtServiceHouseNo = document.getElementById('<%= txtServiceHouseNo.ClientID %>');
            var txtServiceDetails = document.getElementById('<%= txtServiceDetails.ClientID %>');
            var txtServiceCity = document.getElementById('<%= txtServiceCity.ClientID %>');
            var txtServiceStreet = document.getElementById('<%= txtServiceStreet.ClientID %>');
            var txtServiceZipCode = document.getElementById('<%= txtServiceZipCode.ClientID %>');
            var txtServiceEmailId = document.getElementById('<%= txtServiceEmailId.ClientID %>');
            var txtneighbor = document.getElementById('<%= txtneighbor.ClientID %>');
            var txtMobileNo = document.getElementById('<%= txtMobileNo.ClientID %>');
            var txtRouteSequenceNo = document.getElementById('<%= txtRouteSequenceNo.ClientID %>');
            var ddlRouteNo = document.getElementById('<%= ddlRouteNo.ClientID %>');

            var ddlTariff = document.getElementById('<%= ddlTariff.ClientID %>');
            var txtMeterNo = document.getElementById('<%= txtMeterNo.ClientID %>');
            var txtOldAccountNo = document.getElementById('<%= txtOldAccountNo.ClientID %>');
            var txtApplicationDate = document.getElementById('<%= txtApplicationDate.ClientID %>');
            var ddlAccountType = document.getElementById('<%= ddlAccountType.ClientID %>');
            var ddlReadCode = document.getElementById('<%= ddlReadCode.ClientID %>');
            var divMinReading = document.getElementById('<%=divMinReading.ClientID %>');
            var ddlCustomerType = document.getElementById('<%= ddlCustomerType.ClientID %>');
            var txtMinReading = document.getElementById('<%= txtMinReading.ClientID %>');
            var ddlMeterType = document.getElementById('<%= ddlMeterType.ClientID %>');
            var ddlPhase = document.getElementById('<%= ddlPhase.ClientID %>');
            var txtMeeterSerialNo = document.getElementById('<%= txtMeeterSerialNo.ClientID %>');
            var txtConnectionDate = document.getElementById('<%= txtConnectionDate.ClientID %>');
            var ddlMeeterStatus = document.getElementById('<%= ddlMeeterStatus.ClientID %>');
            var ddlConnectionReason = document.getElementById('<%= ddlConnectionReason.ClientID %>');
            var txtInitialReading = document.getElementById('<%= txtInitialReading.ClientID %>');
            var txtCurrentReading = document.getElementById('txtCurrentReading.ClientID');
            var txtAverageReading = document.getElementById('<%= txtAverageReading.ClientID %>');
            //var txtSetupDate = document.getElementById('txtSetupDate');
            var ddlCertifiedBy = document.getElementById('<%= ddlCertifiedBy.ClientID %>');
            var rblApplicationProccessedBy = document.getElementById('<%= rblApplicationProccessedBy.ClientID %>');
            var ddlAgencyName = document.getElementById('<%= ddlAgencyName.ClientID %>');
            var ddlInstalledBy = document.getElementById('<%= ddlInstalledBy.ClientID %>');
            var txtEmbassyCode = document.getElementById('<%= txtEmbassyCode.ClientID %>');
            var txtMeterAmt = document.getElementById('<%= txtMeterAmt.ClientID %>');
            var chkIsEmbassyCustomer = document.getElementById('<%= chkIsEmbassyCustomer.ClientID %>');
            var cbIsPaidMeter = document.getElementById('<%= cbIsPaidMeter.ClientID %>');
            var divMeterInformation = document.getElementById('<%=divMeterInformation.ClientID %>');
            var hfMeterDials = document.getElementById('<%=hfMeterDials.ClientID %>').value;
            var hfEmbassytxt = document.getElementById('<%=hfEmbassytxt.ClientID %>').value;

            if (stepnumber == 1) {
                var IsValid = true;
                //if (TextBoxValidationlbl(ddlTitle, document.getElementById("spanTitle"), "Title") == false) IsValid = false;
                //if (DropDownlistOnChangelbl(ddlTitle, "spanTitle", "Title") == false) IsValid = false;
                if (TextBoxValidationlbl(txtName, document.getElementById("spanName"), "Name") == false) IsValid = false;
                //if (TextBoxValidationlbl(txtSurName, document.getElementById("spanSurName"), "Surname") == false) IsValid = false;
                //                if (TextBoxBlurValidationlbl(txtDocumentNo, 'spanDocumentNo', 'Document No') == false) {
                //                    IsValid = false;
                //                }
                //                else {
                //                if (Trim(txtDocumentNo.value) != "") {
                //                    alert(txtDocumentNo.value + "Data")
                //                    if (IsDocumentNoExists(txtDocumentNo) == false) IsValid = false;
                //                }
                //}
                //if (document.getElementById("spanDocumentNo").innerHTML != "") IsValid = false;
                //if (IsSequenceNoExists(txtCustomerSequenceNo) == false) IsValid = false;
                // if (document.getElementById("spanCustomerSequenceNo").innerHTML != "") IsValid = false;
                //Contact Fields are not restricted with length
                //                if (Trim(txtHome.value) != "") {
                //                    var ContactNoLength = "<%= Resources.GlobalConstants.CONTACT_NO_LENGTH %>";
                //                    ContactNoLength = parseInt(ContactNoLength);
                //                    if (TextBoxValidContactLbl(txtHome, "Contact No", document.getElementById("spanHome"), ContactNoLength) == false) IsValid = false;
                //                    else {
                //                        txtHome.style.border = "1px solid #8E8E8E";
                //                        document.getElementById("spanHome").innerHTML = "";
                //                    }
                //                }
                //                else {
                //                    txtHome.style.border = "1px solid #8E8E8E";
                //                    document.getElementById("spanHome").innerHTML = "";
                //                }
                //                if (Trim(txtBusiness.value) != "") {
                //                    var ContactNoLength = "<%= Resources.GlobalConstants.CONTACT_NO_LENGTH %>";
                //                    ContactNoLength = parseInt(ContactNoLength);
                //                    if (TextBoxValidContactLbl(txtBusiness, "Contact No", document.getElementById("spanBusiness"), ContactNoLength) == false) IsValid = false;
                //                    else {
                //                        txtBusiness.style.border = "1px solid #8E8E8E";
                //                        document.getElementById("spanBusiness").innerHTML = "";
                //                    }
                //                }
                //                else {
                //                    txtBusiness.style.border = "1px solid #8E8E8E";
                //                    document.getElementById("spanBusiness").innerHTML = "";
                //                }
                //                if (Trim(txtOthers.value) != "") {
                //                    var ContactNoLength = "<%= Resources.GlobalConstants.CONTACT_NO_LENGTH %>";
                //                    ContactNoLength = parseInt(ContactNoLength);
                //                    if (TextBoxValidContactLbl(txtOthers, "Contact No", document.getElementById("spanOthers"), ContactNoLength) == false) IsValid = false;
                //                    else {
                //                        txtOthers.style.border = "1px solid #8E8E8E";
                //                        document.getElementById("spanOthers").innerHTML = "";
                //                    }
                //                }
                //                else {
                //                    txtOthers.style.border = "1px solid #8E8E8E";
                //                    document.getElementById("spanOthers").innerHTML = "";
                //                }

                //Contact Fields are not restricted with length
                //if (TextBoxValidationlbl(txtLandMark, document.getElementById("spanLandMark"), "Landmark") == false) IsValid = false;
                if (TextBoxValidationlblZeroAccepts(txtHouseNo, document.getElementById("spanHouseNo"), "House No") == false) IsValid = false;
                //if (TextBoxValidationlbl(txtPostalDetails, document.getElementById("spanPostDetails"), "Postal Details") == false) IsValid = false;
                //if (TextBoxValidationlbl(txtZipCode, document.getElementById("spanZipCode"), "ZipCode") == false) IsValid = false;
                if (TextBoxValidationlbl(txtStreet, document.getElementById("spanStreet"), "Address 1") == false) IsValid = false;
                //if (TextBoxValidationlbl(ddlState, document.getElementById("spanState"), "State") == false) IsValid = false;
                //if (DropDownlistOnChangelbl(ddlState, "spanState", "State") == false) IsValid = false;
                //if (TextBoxValidationlbl(ddlDistrict, document.getElementById("spanDistrict"), "District") == false) IsValid = false;
                //if (DropDownlistOnChangelbl(ddlDistrict, "spanDistrict", "District") == false) IsValid = false;
                //if (TextBoxValidationlbl(txtCity, document.getElementById("spanCity"), "LGA/City/Village") == false) IsValid = false;
                //if (TextBoxValidationlbl(txtEmailId, document.getElementById("spanemail"), "Email Id") == false) IsValid = false;
                CopyNames();
                if (Trim(txtEmailId.value) != "") {
                    if (EmailValidation(txtEmailId) == false) {
                        var spanEmail = document.getElementById("spanemail");
                        spanEmail.innerHTML = "Please Enter Valid Email Id";
                        spanEmail.style.display = "block";
                        txtEmailId.style.border = "1px solid red";
                        IsValid = false;
                    }
                    else {
                        txtEmailId.style.border = "1px solid #8E8E8E";
                        document.getElementById("spanemail").innerHTML = "";
                    }
                }
                else {
                    txtEmailId.style.border = "1px solid #8E8E8E";
                    document.getElementById("spanemail").innerHTML = "";
                }
                //if (TextBoxValidationlbl(txtIdentityNumber, document.getElementById("spanIdentityNo"), "Identity Number") == false) IsValid = false;
                //if (TextBoxValidationlbl(ddlIdentityType, document.getElementById("spanIdentityType"), "Identity Type") == false) IsValid = false;
                //if (DropDownlistOnChangelbl(ddlIdentityType, "spanIdentityType", "Identity Type") == false) IsValid = false;

                var rbtnPostalAddress = document.getElementById('<%= rbtnPostalAddress.ClientID %>');
                var rbtnServiceAddress = document.getElementById('<%= rbtnServiceAddress.ClientID %>');
                if (rbtnPostalAddress.checked) {
                    CopyAddress(true);
                }
                //                else {
                //                    rbtnServiceAddress.checked = true;
                //                    CopyAddress(false);
                //                }

                if (!IsValid) {
                    return false;
                }
                else {
                    var hfValidStep = document.getElementById('<%= hfValidStep.ClientID %>');
                    var btnStep1 = document.getElementById('<%= btnStep1.ClientID %>');
                    if (hfValidStep.value == "0") {
                        btnStep1.click();
                    }
                    else
                        return true;
                }
            }
            else if (stepnumber == 2) {

                var IsValid = true;

                //if (TextBoxValidationlbl(ddlBusinessUnit, document.getElementById("spanBusinessUnit"), "Busniess Unit") == false) IsValid = false;
                if (DropDownlistOnChangelbl(ddlBusinessUnit, "spanBusinessUnit", "Business Unit") == false) IsValid = false;
                //if (TextBoxValidationlbl(ddlServiceUnit, document.getElementById("spanServiceUnit"), "Service Unit") == false) IsValid = false;
                if (DropDownlistOnChangelbl(ddlServiceUnit, "spanServiceUnit", "Service Unit") == false) IsValid = false;
                //if (TextBoxValidationlbl(ddlServiceCenter, document.getElementById("spanServiceCenter"), "Service Center") == false) IsValid = false;
                if (DropDownlistOnChangelbl(ddlServiceCenter, "spanServiceCenter", "Service Center") == false) IsValid = false;
                if (DropDownlistOnChangelbl(ddlCycle, "spanCycle", "Cycle") == false) IsValid = false;
                //if (TextBoxValidationlbl(ddlBookNumber, document.getElementById("spanBookNumber"), "Book Number") == false) IsValid = false;
                if (DropDownlistOnChangelbl(ddlBookNumber, "spanBookNumber", "Book Number") == false) IsValid = false;


                //if (TextBoxValidationlbl(txtServiceLandMark, document.getElementById("spanServiceLandMark"), "Service Landmark") == false) IsValid = false;
                if (TextBoxValidationlblZeroAccepts(txtServiceHouseNo, document.getElementById("spanServiceHouseNo"), "Service House Number") == false) IsValid = false;
                //if (TextBoxValidationlbl(txtServiceDetails, document.getElementById("spanServiceDetails"), "Service Details") == false) IsValid = false;
                //if (TextBoxValidationlbl(txtServiceCity, document.getElementById("spanServiceCity"), "Service LGA/City/Village") == false) IsValid = false;
                if (TextBoxValidationlbl(txtServiceStreet, document.getElementById("spanServiceStreet"), "Service Address 1") == false) IsValid = false;
                //if (TextBoxValidationlbl(txtServiceEmailId, document.getElementById("spanemailadd"), "Service Email Address") == false) IsValid = false;
                if (Trim(txtServiceEmailId.value) != "") {
                    if (EmailValidation(txtServiceEmailId) == false) {
                        var spanemailadd = document.getElementById("spanemailadd")
                        spanemailadd.innerHTML = "Please Enter Valid Service Email Id";
                        spanemailadd.style.display = "block";
                        txtServiceEmailId.style.border = "1px solid red";
                        IsValid = false;
                    }
                    else {
                        txtServiceEmailId.style.border = "1px solid #8E8E8E";
                        document.getElementById("spanemailadd").innerHTML = "";
                    }
                }
                else {
                    txtServiceEmailId.style.border = "1px solid #8E8E8E";
                    document.getElementById("spanemailadd").innerHTML = "";
                }
                //if (TextBoxValidationlbl(txtServiceZipCode, document.getElementById("spanServiceZipCode"), "Service ZipCode") == false) IsValid = false;
                if (ValidateMobileNo() == false) IsValid = false;
                //if (TextBoxValidationlbl(txtRouteSequenceNo, document.getElementById("spanRouteSequenceNo"), "Route Sequence Number") == false) IsValid = false;
                if (DropDownlistOnChangelbl(ddlRouteNo, "spanRouteNo", "Route No") == false) IsValid = false;
                if (IsNeighborAccountNoExists(txtneighbor) == false) IsValid = false;
                if (!IsValid) {
                    return false;
                }
                else {
                    //                    var hfValidStep = document.getElementById('<%= hfValidStep.ClientID %>');
                    //                    var btnStep2 = document.getElementById('<%= btnStep2.ClientID %>');
                    //                    if (hfValidStep.value == "1")
                    //                        btnStep2.click();
                    //                    else
                    return true;
                }
            }
            else if (stepnumber == 3) {
                var IsValid = true;
                //if (TextBoxValidationlbl(ddlTariff, document.getElementById("spanTariff"), "Tariff") == false) IsValid = false;
                if (DropDownlistOnChangelbl(ddlTariff, "spanTariff", "Tariff") == false) IsValid = false;
                //if (TextBoxValidationlbl(ddlAccountType, document.getElementById("spanAccountType"), "Account Type") == false) IsValid = false;
                if (DropDownlistOnChangelbl(ddlAccountType, "spanAccountType", "Account Type") == false) IsValid = false;
                if (Trim(txtApplicationDate.value) != "") {
                    if (isDateLbl(txtApplicationDate.value, txtApplicationDate, "spanApplicationDate") == false) IsValid = false;
                    if (GreaterToday(txtApplicationDate, "Application Date should be Less than or equal to Today's Date") == false) IsValid = false;
                }
                else {
                    document.getElementById("spanApplicationDate").innerHTML = "";
                    txtApplicationDate.style.border = "1px solid #8E8E8E";
                }
                if (txtOldAccountNo.value != "") {
                    if (IsOldAccountNoExists(txtOldAccountNo) == false) IsValid = false;
                }
                //if (TextBoxValidationlbl(ddlReadCode, document.getElementById("spanReadCode"), "Read Code") == false) IsValid = false;
                if (DropDownlistOnChangelbl(ddlReadCode, "spanReadCode", "Read Code") == false) IsValid = false;
                //if (TextBoxValidationlbl(ddlCustomerType, document.getElementById("spanCustomerType"), "Customer Type") == false) IsValid = false;
                if (DropDownlistOnChangelbl(ddlCustomerType, "spanCustomerType", "Customer Type") == false) IsValid = false;
                if (chkIsEmbassyCustomer.checked) {
                    if (TextBoxValidationlbl(txtEmbassyCode, document.getElementById("spanEmbassyCode"), "Embassy Code") == false) IsValid = false;
                }

                if (divMinReading != null) {
                    if (TextBoxValidationlbl(txtMinReading, document.getElementById("spanMinReading"), "Min Reading") == false) IsValid = false;
                }

                if (divMeterInformation != null) {
                    if (cbIsPaidMeter.checked) {
                        if (TextBoxValidationlbl(txtMeterAmt, document.getElementById("spanMeterAmt"), "Meter Amount") == false) IsValid = false;
                    }
                    //if (TextBoxValidationlbl(ddlMeterType, document.getElementById("spanMeterType"), "Meter Type") == false) IsValid = false;
                    if (DropDownlistOnChangelbl(ddlMeterType, "spanMeterType", "Meter Type") == false) IsValid = false;
                    //if (TextBoxValidationlbl(ddlPhase, document.getElementById("spanPhase"), "Phase") == false) IsValid = false;
                    if (DropDownlistOnChangelbl(ddlPhase, "spanPhase", "Phase") == false) IsValid = false;
                    //if (TextBoxValidationlbl(txtMeterNo, document.getElementById("spanMeterNo"), "Meter Number") == false) IsValid = false;
                    //if (TextBoxValidationlbl(txtMeeterSerialNo, document.getElementById("spanMeterSerialNo"), "Serial Number") == false) IsValid = false;
                    if (IsMeterNoExists(txtMeterNo) == false) IsValid = false;
                    if (document.getElementById("spanMeterNo").innerHTML != "") IsValid = false;
                    if (Trim(txtMeeterSerialNo.value) != "") {
                        if (IsMeterSerialNoExists(txtMeeterSerialNo) == false) {
                            IsValid = false;
                        }
                    }
                    else {
                        txtMeeterSerialNo.style.border = "1px solid #8E8E8E";
                        document.getElementById("spanMeterSerialNo").innerHTML = "";
                    }
                    //if (document.getElementById("spanMeterSerialNo").innerHTML != "") IsValid = false;
                    if (TextBoxValidationlbl(txtConnectionDate, document.getElementById("spanConnectionDate"), "Connection Date") == false) IsValid = false;
                    if (isDateLbl(txtConnectionDate.value, txtConnectionDate, "spanConnectionDate") == false) IsValid = false;
                    if (GreaterToday(txtConnectionDate, "Connection Date should be Less than or equal to Today's Date") == false) IsValid = false;
                    //if (TextBoxValidationlbl(ddlMeeterStatus, document.getElementById("spanMeterStatus"), "Meter Status") == false) IsValid = false;
                    //if (DropDownlistOnChangelbl(ddlMeeterStatus, "spanMeterStatus", "Meter Status") == false) IsValid = false;
                    //if (TextBoxValidationlbl(ddlConnectionReason, document.getElementById("spanConnectionReason"), "Connection Reason") == false) IsValid = false;
                    if (DropDownlistOnChangelbl(ddlConnectionReason, "spanConnectionReason", "Connection Reason") == false) IsValid = false;
                    //if (TextBoxValidationlblZeroAccepts(txtInitialReading, document.getElementById("spanInitialReading"), "Initial Reading") == false) IsValid = false;
                    if (MeterDials(txtInitialReading) == false) IsValid = false;
                    //                    if (txtInitialReading.value != "") {
                    //                        if (hfMeterDials != "") {
                    //                            if (InitialMeterReading(txtInitialReading, document.getElementById("spanInitialReading"), hfMeterDials) == false) IsValid = false;
                    //                        }
                    //                    }
                    //                    if (hfMeterDials.value != 0) {
                    //                        if (hfMeterDials.value != txtInitialReading.value) {
                    //                            txtInitialReading.style.border = "1px solid red";
                    //                            document.getElementById("spanInitialReading").innerHTML = "Initial Reading Must be" + hfMeterDials.value + "Charecters!";
                    //                            IsValid = false;
                    //                        }
                    //                        else {
                    //                            alert(1);
                    //                            txtInitialReading.style.border = "1px solid #8E8E8E";
                    //                            alert(2);
                    //                            document.getElementById("spanInitialReading").innerHTML = "";
                    //                            alert(3);
                    //                        }
                    //                    }
                    //if (TextBoxValidationlbl(txtCurrentReading, document.getElementById("spanCurrentReading"), "Current Reading") == false) IsValid = false;
                    if (TextBoxValidationlbl(txtAverageReading, document.getElementById("spanAverageReading"), "Average Reading") == false) IsValid = false;
                    //if (TextBoxValidationlbl(txtSetupDate, document.getElementById("spanSetupDate"), "Setup Date") == false) IsValid = false;
                    // if (isDateLbl(txtSetupDate.value, txtSetupDate, "spanSetupDate") == false) IsValid = false;
                    //                if (TextBoxValidationlbl(ddlCertifiedBy, document.getElementById("spanCertifiedBy"), "Certified By") == false) IsValid = false;
                    var rblApplicationProccessedByList = rblApplicationProccessedBy.getElementsByTagName("input");
                    if (RadioButtonListlbl(rblApplicationProccessedBy, "spanApplicationProcessedBy", " One") == false) IsValid = false;
                    else {
                        if (rblApplicationProccessedByList[1].checked) {
                            // if (TextBoxValidationlbl(ddlAgencyName, document.getElementById("spanAgency"), "Agency") == false) IsValid = false;
                            if (DropDownlistOnChangelbl(ddlAgencyName, "spanAgency", "Agency Name") == false) IsValid = false;
                        }
                        else {
                            //if (TextBoxValidationlbl(ddlInstalledBy, document.getElementById("spanInstalledBy"), "InstalledBy") == false) IsValid = false;
                            if (DropDownlistOnChangelbl(ddlInstalledBy, "spanInstalledBy", "Installed By") == false) IsValid = false;
                        }
                    }
                }

                if (!IsValid) {
                    return false;
                }
                else {
                    var lblTitle = document.getElementById('<%= lblTitle.ClientID %>');
                    if (ddlTitle.selectedIndex > 0) {
                        lblTitle.innerHTML = ddlTitle.options[ddlTitle.selectedIndex].text;
                    }
                    else {
                        lblTitle.innerHTML = "--";
                    }

                    var lblName = document.getElementById('<%= lblName.ClientID %>');
                    lblName.innerHTML = txtName.value;

                    var lblSurName = document.getElementById('<%= lblSurName.ClientID %>');
                    if (txtSurName.value != "") {
                        lblSurName.innerHTML = txtSurName.value;
                    }
                    else lblSurName.innerHTML = "--";

                    //                    var lblDocumentNo = document.getElementById('<%= lblDocumentNo.ClientID %>');
                    //                    lblDocumentNo.innerHTML = txtDocumentNo.value;

                    var lblPAHouseNo = document.getElementById('<%= lblPAHouseNo.ClientID %>');
                    lblPAHouseNo.innerHTML = txtHouseNo.value;

                    var lblPAStreet = document.getElementById('<%= lblPAStreet.ClientID %>');
                    lblPAStreet.innerHTML = txtStreet.value;

                    var lblPACity = document.getElementById('<%= lblPACity.ClientID %>');
                    lblPACity.innerHTML = txtCity.value;

                    var lblPALandMark = document.getElementById('<%= lblPALandMark.ClientID %>');
                    lblPALandMark.innerHTML = txtLandMark.value;

                    //                    var lblIdentityNo = document.getElementById(' lblIdentityNo.ClientID ');
                    //                    lblIdentityNo.innerHTML = txtIdentityNumber.value;

                    //                    var lblIdentityType = document.getElementById('lblIdentityType.ClientID ');
                    //                    lblIdentityType.innerHTML = ddlIdentityType.options[ddlIdentityType.selectedIndex].text;

                    var lblBusinessUnit = document.getElementById('<%= lblBusinessUnit.ClientID %>');
                    lblBusinessUnit.innerHTML = ddlBusinessUnit.options[ddlBusinessUnit.selectedIndex].text;

                    var lblServiceUnit = document.getElementById('<%= lblServiceUnit.ClientID %>');
                    lblServiceUnit.innerHTML = ddlServiceUnit.options[ddlServiceUnit.selectedIndex].text;

                    var lblServiceCenter = document.getElementById('<%= lblServiceCenter.ClientID %>');
                    lblServiceCenter.innerHTML = ddlServiceCenter.options[ddlServiceCenter.selectedIndex].text;

                    var lblCycle = document.getElementById('<%= lblCycle.ClientID %>');
                    lblCycle.innerHTML = ddlCycle.options[ddlCycle.selectedIndex].text;

                    var lblBookNumber = document.getElementById('<%= lblBookNumber.ClientID %>');
                    lblBookNumber.innerHTML = ddlBookNumber.options[ddlBookNumber.selectedIndex].text;

                    //                    var lblInjectionSubstation = document.getElementById('lblInjectionSubstation.ClientID');
                    //                    lblInjectionSubstation.innerHTML = txtInjectionSubStation.options[txtInjectionSubStation.selectedIndex].text;

                    //                    var lblFeeder = document.getElementById(' lblFeeder.ClientID');
                    //                    lblFeeder.innerHTML = txtFeeder.options[txtFeeder.selectedIndex].text;

                    var lblSAHouseNo = document.getElementById('<%= lblSAHouseNo.ClientID %>');
                    lblSAHouseNo.innerHTML = txtServiceHouseNo.value;

                    var lblSAStreet = document.getElementById('<%= lblSAStreet.ClientID %>');
                    lblSAStreet.innerHTML = txtServiceStreet.value;

                    var lblSACity = document.getElementById('<%= lblSACity.ClientID %>');
                    lblSACity.innerHTML = txtServiceCity.value;

                    var lblSALandMark = document.getElementById('<%= lblSALandMark.ClientID %>');
                    lblSALandMark.innerHTML = txtServiceLandMark.value;

                    var lblMobileNo = document.getElementById('<%= lblMobileNo.ClientID %>');
                    lblMobileNo.innerHTML = txtMobileNo.value;

                    var lblRouteNo = document.getElementById('<%= lblRouteNo.ClientID %>');
                    lblRouteNo.innerHTML = ddlRouteNo.options[ddlRouteNo.selectedIndex].text;

                    var lblTariff = document.getElementById('<%= lblTariff.ClientID %>');
                    lblTariff.innerHTML = ddlTariff.options[ddlTariff.selectedIndex].text;

                    var lblConnectionType = document.getElementById('<%= lblConnectionType.ClientID %>');
                    lblConnectionType.innerHTML = ddlAccountType.options[ddlAccountType.selectedIndex].text;

                    //                    var lblOldAccountNo = document.getElementById(lblOldAccountNo.ClientID );
                    //                    lblOldAccountNo.innerHTML = txtOldAccountNo.value;

                    var lblReadCode = document.getElementById('<%= lblReadCode.ClientID %>');
                    lblReadCode.innerHTML = ddlReadCode.options[ddlReadCode.selectedIndex].text;

                    var lblCustomerType = document.getElementById('<%= lblCustomerType.ClientID %>');
                    lblCustomerType.innerHTML = ddlCustomerType.options[ddlCustomerType.selectedIndex].text;

                    if (divMinReading != null) {
                        var lblMinReading = document.getElementById('<%= lblMinReading.ClientID %>');
                        lblMinReading.innerHTML = txtMinReading.value;
                    }

                    if (divMeterInformation != null) {
                        var lblMeterConnectionType = document.getElementById('<%= lblMeterConnectionType.ClientID %>');
                        lblMeterConnectionType.innerHTML = ddlMeterType.options[ddlMeterType.selectedIndex].text;

                        var lblPhase = document.getElementById('<%= lblPhase.ClientID %>');
                        lblPhase.innerHTML = ddlPhase.options[ddlPhase.selectedIndex].text;

                        var lblMeterNo = document.getElementById('<%= lblMeterNo.ClientID %>');
                        lblMeterNo.innerHTML = txtMeterNo.value;

                        //                    var lblMeterSerialNo = document.getElementById('lblMeterSerialNo.ClientID');
                        //                    lblMeterSerialNo.innerHTML = txtMeeterSerialNo.value;

                        var lblConnectionDate = document.getElementById('<%= lblConnectionDate.ClientID %>');
                        lblConnectionDate.innerHTML = txtConnectionDate.value;

                        var lblMeterStatus = document.getElementById('<%= lblMeterStatus.ClientID %>');
                        lblMeterStatus.innerHTML = ddlMeeterStatus.options[ddlMeeterStatus.selectedIndex].text;

                        var lblConnectionReason = document.getElementById('<%= lblConnectionReason.ClientID %>');
                        lblConnectionReason.innerHTML = ddlConnectionReason.options[ddlConnectionReason.selectedIndex].text;

                        var lblInitialReading = document.getElementById('<%= lblInitialReading.ClientID %>');
                        lblInitialReading.innerHTML = txtInitialReading.value;

                        //                    var lblCurrentReading = document.getElementById(' lblCurrentReading.ClientID');
                        //                    lblCurrentReading.innerHTML = txtCurrentReading.value;

                        var lblAverageReading = document.getElementById('<%= lblAverageReading.ClientID %>');
                        lblAverageReading.innerHTML = txtAverageReading.value;

                        //                        var lblReadDate = document.getElementById(' lblReadDate.ClientID ');
                        //                        lblReadDate.innerHTML = txtSetupDate.value;

                        var rblApplicationProccessedByList = rblApplicationProccessedBy.getElementsByTagName("input");
                        if (rblApplicationProccessedByList[1].checked) {
                            var ddlAgencyName = document.getElementById('<%= ddlAgencyName.ClientID %>');
                            if (ddlAgencyName != null) {
                                var lblAgencyName = document.getElementById('<%= lblAgencyName.ClientID %>');
                                lblAgencyName.innerHTML = ddlAgencyName.options[ddlAgencyName.selectedIndex].text;
                            }
                            var txtContactPersonName = document.getElementById('<%= txtContactPersonName.ClientID %>');
                            if (txtContactPersonName.value != '') {
                                var lblContactPersonName = document.getElementById('<%= lblContactPersonName.ClientID %>');
                                lblContactPersonName.innerHTML = txtContactPersonName.value;
                            }

                            var txtAgencyDetails = document.getElementById('<%= txtAgencyDetails.ClientID %>');
                            if (txtAgencyDetails.value != '') {
                                var lblDetails = document.getElementById('<%= lblDetails.ClientID %>');
                                lblDetails.innerHTML = txtAgencyDetails.value;
                            }
                        }
                        else {
                            var ddlInstalledBy = document.getElementById('<%= ddlInstalledBy.ClientID %>');
                            if (ddlInstalledBy != null) {
                                var lblInstallBy = document.getElementById('<%= lblInstallBy.ClientID %>');
                                lblInstallBy.innerHTML = ddlInstalledBy.options[ddlInstalledBy.selectedIndex].text;
                            }

                        }
                    }

                    //                    var lblCertifiedBy = document.getElementById(' lblCertifiedBy.ClientID ');
                    //                    lblCertifiedBy.innerHTML = ddlCertifiedBy.options[ddlCertifiedBy.selectedIndex].text;

                    //                    var hfValidStep = document.getElementById('<%= hfValidStep.ClientID %>');
                    //                    var btnStep3 = document.getElementById('<%= btnStep3.ClientID %>');
                    //                    if (hfValidStep.value == "2")
                    //                        btnStep3.click();
                    //                    else lblInstallBy

                    return true;
                }
            }
        }

        function InstalledBy() {
            var rblApplicationProccessedBy = document.getElementById('<%= rblApplicationProccessedBy.ClientID %>');
            var rblApplicationProccessedByList = rblApplicationProccessedBy.getElementsByTagName("input");
            if (rblApplicationProccessedByList[1].checked) {
                document.getElementById("divApplicationByOthers").style.display = "block";
                document.getElementById("divBEDCApplication").style.display = "none";
                document.getElementById("divOthersFinal").style.display = "block";
                document.getElementById("divBEDCApplicationFinal").style.display = "none";
            }
            else {
                document.getElementById("divApplicationByOthers").style.display = "none";
                document.getElementById("divBEDCApplication").style.display = "block";
                document.getElementById("divOthersFinal").style.display = "none";
                document.getElementById("divBEDCApplicationFinal").style.display = "block";
            }
        }

        function CopyAddress(IsCopy) {
            var txtLandMark = document.getElementById('<%= txtLandMark.ClientID %>');
            var txtHouseNo = document.getElementById('<%= txtHouseNo.ClientID %>');
            var txtCity = document.getElementById('<%= txtCity.ClientID %>');
            var txtStreet = document.getElementById('<%= txtStreet.ClientID %>');
            var txtZipCode = document.getElementById('<%= txtZipCode.ClientID %>');
            // var txtEmailId = document.getElementById('<%= txtEmailId.ClientID %>');
            var txtPostalDetails = document.getElementById('<%= txtPostalDetails.ClientID %>');
            var txtHome = document.getElementById('<%= txtHome.ClientID %>');


            var txtServiceLandMark = document.getElementById('<%= txtServiceLandMark.ClientID %>');
            var txtServiceHouseNo = document.getElementById('<%= txtServiceHouseNo.ClientID %>');
            var txtServiceCity = document.getElementById('<%= txtServiceCity.ClientID %>');
            var txtServiceStreet = document.getElementById('<%= txtServiceStreet.ClientID %>');
            var txtServiceZipCode = document.getElementById('<%= txtServiceZipCode.ClientID %>');
            //var txtServiceEmailId = document.getElementById('<%= txtServiceEmailId.ClientID %>');
            var txtServiceDetails = document.getElementById('<%= txtServiceDetails.ClientID %>');
            var txtMobileNo = document.getElementById('<%= txtMobileNo.ClientID %>');


            if (IsCopy) {
                txtServiceLandMark.value = txtLandMark.value;
                txtServiceHouseNo.value = txtHouseNo.value;
                txtServiceCity.value = txtCity.value;
                txtServiceStreet.value = txtStreet.value;
                txtServiceZipCode.value = txtZipCode.value;
                // txtServiceEmailId.value = txtEmailId.value;
                txtServiceDetails.value = txtPostalDetails.value;
                if (txtHome.value != "")
                    txtMobileNo.value = txtHome.value;
            }
            else {
                txtServiceLandMark.value = txtServiceHouseNo.value = txtServiceCity.value = txtServiceStreet.value = txtServiceZipCode.value = txtServiceDetails.value = txtMobileNo.value = "";
            }
        }

        function Embassy() {
            var chkIsEmbassyCustomer = document.getElementById('<%= chkIsEmbassyCustomer.ClientID %>');
            var txtEmbassyCode = document.getElementById('<%= txtEmbassyCode.ClientID %>');
            var divEmbassyCode = document.getElementById("divEmbassyCode");
            var hfEmbassytxt = document.getElementById('<%=hfEmbassytxt.ClientID %>').value;
            if (chkIsEmbassyCustomer.checked) {
                txtEmbassyCode.value = "";
                divEmbassyCode.style.display = "block";
                if (hfEmbassytxt != "") {
                    txtEmbassyCode.value = hfEmbassytxt;
                }
            }
            else {
                txtEmbassyCode.value = "";
                divEmbassyCode.style.display = "none";
            }
        }

        function IsPaidMeter() {
            var cbIsPaidMeter = document.getElementById('<%= cbIsPaidMeter.ClientID %>');
            var txtMeterAmt = document.getElementById('<%= txtMeterAmt.ClientID %>');
            var divPaidMeter = document.getElementById("divPaidMeter");
            var hftxtMeterAmt = document.getElementById('<%=hftxtMeterAmt.ClientID %>').value;
            if (cbIsPaidMeter.checked) {
                txtMeterAmt.value = "";
                divPaidMeter.style.display = "block";
                if (hftxtMeterAmt != "") {
                    txtMeterAmt.value = hftxtMeterAmt;
                }
            }
            else {
                txtMeterAmt.value = "";
                divPaidMeter.style.display = "none";
            }
        }

        $(document).ready(function () {
            Embassy();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            Embassy();
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <%-- <h2 class="consume_head">
        <asp:Literal ID="lsitConsumerRegisteration" runat="server" Text="<%$ Resources:Resource, CONSUMER_REGISTERATION%>"></asp:Literal>
    </h2>--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="wizard" class="swMain">
        <ul>
            <li><a href="#step-1">
                <label class="stepNumber">
                    1</label>
                <span class="stepDesc"><small>
                    <asp:Literal ID="litPersonal" runat="server" Text="<%$ Resources:Resource, PERSONAL%>"></asp:Literal></small>
                </span></a></li>
            <li><a href="#step-2">
                <label class="stepNumber">
                    2</label>
                <span class="stepDesc"><small>
                    <asp:Literal ID="litService" runat="server" Text="<%$ Resources:Resource, SERVICE%>"></asp:Literal>
                </small></span></a></li>
            <li><a href="#step-3">
                <label class="stepNumber">
                    3</label>
                <span class="stepDesc"><small>
                    <asp:Literal ID="litAccount" runat="server" Text="<%$ Resources:Resource, ACCOUNT%>"></asp:Literal>
                </small></span></a></li>
            <li><a href="#step-4">
                <label class="stepNumber">
                    4</label>
                <span class="stepDesc"><small>
                    <asp:Literal ID="litFinalInfo" runat="server" Text="<%$ Resources:Resource, FINAL_INFORMATION%>"></asp:Literal></small>
                </span></a></li>
            <li class="head">
                <h2 class="consume_head">
                    <asp:Literal ID="Literal50" runat="server" Text="<%$ Resources:Resource, CONSUMER_REGISTERATION%>"></asp:Literal>
                </h2>
            </li>
        </ul>
        <div id="step-1" style="height: 637PX;">
            <h2 class="StepTitle">
                <asp:Literal ID="litCustomerDetails" runat="server" Text="<%$ Resources:Resource, CUSTOMER_DETAILS%>"></asp:Literal>
                <%--<div class="fltr" style="font-weight: bold; padding-right: 20px;">
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="lbtnRefreshStep1" TabIndex="0" runat="server" OnClick="lbtnRefreshStep1_Click">Refresh</asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>--%>
            </h2>
            <div class="consumer_total">
                <div class="pad_5">
                </div>
                <h3>
                    <asp:Literal ID="litPersonalDetails" runat="server" Text="<%$ Resources:Resource, PERSONAL_DETAILS%>"></asp:Literal>
                </h3>
                <div class="pad_5">
                </div>
                <div class="consumer_feild">
                    <div class="consume_name">
                        <asp:Literal ID="litTitle" runat="server" Text="<%$ Resources:Resource, TITLE%>"></asp:Literal></div>
                    <div class="consume_input">
                        <%-- <editable:EditableDropDownList ID="ddlTitle" runat="server" Sorted="false" AutoselectFirstItem="False"
                            placeholder="Select Title">
                            <asp:ListItem Text="Mr" Value="1">
                            </asp:ListItem>
                            <asp:ListItem Text="Miss" Value="2">
                            </asp:ListItem>
                        </editable:EditableDropDownList>--%>
                        <asp:DropDownList ID="ddlTitle" TabIndex="1" runat="server">
                            <asp:ListItem Text="Select" Value=""></asp:ListItem>
                            <asp:ListItem Text="Mr" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Miss" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Mrs" Value="3"></asp:ListItem>
                        </asp:DropDownList>
                        <span id="spanTitle" class="spancolor"></span>
                    </div>
                </div>
                <div class="clear pad_5">
                </div>
                <div class="consumer_feild">
                    <div class="consume_name">
                        <asp:Literal ID="litName" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal><span>*</span></div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtName" TabIndex="2" onkeyup="CopyNames()" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanName','Name')"></asp:TextBox>
                        <%-- <asp:FilteredTextBoxExtender ID="ftbName" runat="server" TargetControlID="txtName"
                            ValidChars=" " FilterMode="ValidChars" FilterType="LowercaseLetters,UppercaseLetters,Custom">
                        </asp:FilteredTextBoxExtender>--%>
                        <span id="spanName" class="spancolor"></span>
                    </div>
                    <div class="consume_name c_left">
                        <asp:Literal ID="litSurName" runat="server" Text="<%$ Resources:Resource, SURNAME%>"></asp:Literal></div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtSurName" TabIndex="3" onkeyup="CopyNames()" runat="server"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbSurname" runat="server" TargetControlID="txtSurName"
                            ValidChars=" " FilterMode="ValidChars" FilterType="LowercaseLetters,UppercaseLetters,Custom">
                        </asp:FilteredTextBoxExtender>
                        <span id="spanSurName" class="spancolor"></span>
                    </div>
                    <div class="consume_name c_left">
                        <asp:Literal ID="Literal49" runat="server" Text="<%$ Resources:Resource, EMAIL_ID%>"></asp:Literal></div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtEmailId" runat="server" TabIndex="4" placeholder="Enter Email Id"></asp:TextBox><br />
                        <span id="spanemail" class="spancolor"></span>
                    </div>
                </div>
                <div class="clear pad_5">
                </div>
                <div class="consumer_feild">
                    <div class="consume_name">
                        <asp:Literal ID="litKnownAs" runat="server" Text="<%$ Resources:Resource, KNOWN_AS%>"></asp:Literal>
                    </div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtKnownAs" TabIndex="5" runat="server"></asp:TextBox>
                        <span id="spanKnownAs" class="spancolor"></span>
                    </div>
                    <div class="consume_name c_left">
                        <asp:Literal ID="litDocumentNo" runat="server" Text="<%$ Resources:Resource, DOCUMENT_NO%>"></asp:Literal>
                    </div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtDocumentNo" TabIndex="6" runat="server"></asp:TextBox>
                        <span id="spanDocumentNo" class="spancolor"></span>
                    </div>
                    <%-- <div class="consume_name c_left">onblur="return IsDocumentNoExists(this)"
                        <asp:Literal ID="Literal55" runat="server" Text="<%$ Resources:Resource, CUSTOMER_SEQUENCE_NO%>"></asp:Literal>
                        <span>*</span></div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtCustomerSequenceNo" MaxLength="7" runat="server" onblur="return IsSequenceNoExists(this)"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" TargetControlID="txtCustomerSequenceNo"
                            runat="server" FilterType="Numbers">
                        </asp:FilteredTextBoxExtender>
                        <span id="spanCustomerSequenceNo" class="spancolor"></span>
                    </div>--%>
                    <div class="consume_name c_left">
                        Employee code
                    </div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtEmployeeCode" TabIndex="7" runat="server"></asp:TextBox>
                        <span id="spanEmployeeCode" class="spancolor"></span>
                    </div>
                </div>
                <div class="pad_5 clear">
                </div>
                <h3>
                    <asp:Literal ID="litContactPhoneNos" runat="server" Text="<%$ Resources:Resource, CONTACT_PHONE_NUMBERS%>"></asp:Literal>
                </h3>
                <div class="consumer_feild">
                    <div class="consume_name">
                        <asp:Literal ID="litHome" runat="server" Text="<%$ Resources:Resource, HOME%>"></asp:Literal>
                    </div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtHome" TabIndex="8" MaxLength="20" runat="server"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" TargetControlID="txtHome"
                            runat="server" FilterType="Numbers" FilterMode="ValidChars">
                        </asp:FilteredTextBoxExtender>
                        <span id="spanHome" class="spancolor"></span>
                    </div>
                    <div class="consume_name c_left">
                        <asp:Literal ID="litBusiness" runat="server" Text="<%$ Resources:Resource, BUSINESS%>"></asp:Literal>
                    </div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtBussiness" TabIndex="9" runat="server" MaxLength="20"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" TargetControlID="txtBussiness"
                            runat="server" FilterType="Numbers" FilterMode="ValidChars">
                        </asp:FilteredTextBoxExtender>
                        <span id="spanBusiness" class="spancolor"></span>
                    </div>
                    <div class="consume_name c_left">
                        <asp:Literal ID="litOthers" runat="server" Text="<%$ Resources:Resource, OTHERS%>"></asp:Literal>
                    </div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtOthers" TabIndex="10" runat="server" MaxLength="20"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" TargetControlID="txtOthers"
                            runat="server" FilterType="Numbers" FilterMode="ValidChars">
                        </asp:FilteredTextBoxExtender>
                        <span id="spanOthers" class="spancolor"></span>
                    </div>
                    <div class="pad_5 clear">
                    </div>
                </div>
                <div class="pad_5 clear">
                </div>
                <div class="consumer_feild">
                    <asp:UpdatePanel ID="upConsumer" runat="server">
                        <ContentTemplate>
                            <%--<div class="consume_name">
                                State <span>*</span></div>
                            <div class="consume_input" style="width: 220px;">
                                  //commented start<editable:EditableDropDownList ID="ddlState" runat="server" Sorted="false" AutoselectFirstItem="False"
                                    placeholder="Select State" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged">
                                </editable:EditableDropDownList>//commentedend
                                <asp:DropDownList ID="ddlState" TabIndex="11" OnSelectedIndexChanged="ddlState_SelectedIndexChanged"
                                    runat="server" onchange="DropDownlistOnChangelbl(this,'spanState','State')">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <asp:HyperLink ID="hlkAddState" runat="server" TabIndex="12" Target="_blank" NavigateUrl="~/Masters/AddStates.aspx"
                                    ToolTip="Add State">Add</asp:HyperLink>
                                <div class="clear">
                                </div>
                                <div class="error">
                                    <span id="spanState" class="spancolor"></span>
                                </div>
                            </div>--%>
                            <%--<div class="consume_name c_left">
                                District
                            </div>
                            <div class="consume_input" style="width: 200px;">                                
                                <asp:DropDownList ID="ddlDistrict" runat="server">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <asp:HyperLink ID="hlkAddDistrict" runat="server" Target="_blank" NavigateUrl="~/Masters/AddDistricts.aspx"
                                    ToolTip="Add District">Add</asp:HyperLink>
                                <br />
                                <div class="error">
                                    <span id="spanDistrict" class="spancolor"></span>
                                </div>
                            </div>--%>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="clear">
                </div>
                <h3>
                    <asp:Literal ID="litPostalAddress" runat="server" Text="<%$ Resources:Resource, POSTAL_ADDRESS%>"></asp:Literal></h3>
                <div class="consumer_feild">
                    <div class="consume_name">
                        House No <span>*</span>
                    </div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtHouseNo" runat="server" TabIndex="13" placeholder="House No"
                            onblur="return TextBoxBlurValidationlblZeroAccepts(this,'spanHouseNo','House No')"></asp:TextBox><br />
                        <div class="error">
                            <span id="spanHouseNo" class="spancolor"></span>
                        </div>
                    </div>
                    <div class="consume_name c_left">
                        Address 1 <span>*</span></div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtStreet" runat="server" TabIndex="14" placeholder="Address 1"
                            onblur="return TextBoxBlurValidationlbl(this,'spanStreet','Address 1')"></asp:TextBox><br />
                        <div class="error">
                            <span id="spanStreet" class="spancolor"></span>
                        </div>
                    </div>
                    <div class="consume_name c_left">
                        Address 2</div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtCity" runat="server" TabIndex="15" placeholder="Address 2"></asp:TextBox>
                        <br />
                        <div class="error">
                            <span id="spanCity" class="spancolor"></span>
                        </div>
                    </div>
                    <div class="clear pad_5">
                    </div>
                    <div class="consume_name">
                        Landmark</div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtLandMark" runat="server" TabIndex="16" placeholder="Landmark"></asp:TextBox><br />
                        <div class="error">
                            <span id="spanLandMark" class="spancolor"></span>
                        </div>
                    </div>
                    <%--  <div class="text_box">
                                <asp:TextBox ID="txtLineNo" runat="server" placeholder="Line No 1" onblur="return TextBoxBlurValidationlbl(this,'spanLineNo','Line No')"></asp:TextBox><br />
                                <div class="error">
                                    <span id="spanLineNo" class="spancolor"></span>
                                </div>
                            </div>--%>
                    <div class="consume_name c_left">
                        Details</div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtPostalDetails" TabIndex="17" TextMode="MultiLine" runat="server"
                            placeholder="Details"></asp:TextBox><br />
                        <div class="error">
                            <span id="spanPostDetails" class="spancolor"></span>
                        </div>
                    </div>
                    <div class="pad_5">
                    </div>
                    <div class="consume_name c_left">
                        Zip Code</div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtZipCode" runat="server" TabIndex="18" placeholder="Zip Code"></asp:TextBox><br />
                        <div class="error">
                            <span id="spanZipCode" class="spancolor"></span>
                        </div>
                    </div>
                </div>
                <div class="clear pad_5">
                </div>
                <div class="consumer_feild" style="margin-left: 13px;">
                    Copy this address to service address
                    <input id="rbtnPostalAddress" name="Address" tabindex="19" type="radio" onclick="CopyAddress(true)"
                        runat="server" />
                    Yes
                    <input id="rbtnServiceAddress" name="Address" tabindex="20" type="radio" onclick="return CopyAddress(false)"
                        runat="server" />
                    No
                </div>
                <div class="pad_5 clear">
                </div>
                <div class="consumer_feild">
                    <div class="consume_name">
                        <asp:Literal ID="litIdentityType" runat="server" Text="<%$ Resources:Resource, IDENTITY_TYPE%>"></asp:Literal>
                    </div>
                    <div class="consume_input">
                        <%-- <editable:EditableDropDownList ID="ddlIdentityType" runat="server" Sorted="false"
                            placeholder="Select IdentityType" AutoselectFirstItem="False">
                        </editable:EditableDropDownList>--%>
                        <asp:DropDownList ID="ddlIdentityType" TabIndex="21" runat="server">
                            <asp:ListItem Text="Select" Value=""></asp:ListItem>
                        </asp:DropDownList>
                        <div class="error">
                            <span id="spanIdentityType" class="spancolor"></span>
                        </div>
                    </div>
                    <div class="consume_name c_left">
                        <asp:Literal ID="litIdentityNo" runat="server" Text="<%$ Resources:Resource, IDENTITY_NUMBER%>"></asp:Literal>
                    </div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtIdentityNumber" runat="server" TabIndex="22"></asp:TextBox>
                        <div class="error">
                            <span id="spanIdentityNo" class="spancolor"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%--  End content--%>
        <%----------------------------------------------------------Step 2----------------------------------------- --%>
        <div id="step-2" style="height: 637px;">
            <h2 class="StepTitle">
                <asp:Literal ID="litServiceDetails" runat="server" Text="<%$ Resources:Resource, SERVICE_DETAILS%>"></asp:Literal>
                <div class="fltr" style="font-weight: bold; padding-right: 20px;">
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="lbtnRefreshStep2" TabIndex="23" runat="server" OnClick="lbtnRefreshStep2_Click">Refresh</asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </h2>
            <div class="consumer_total">
                <div class="pad_5">
                </div>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal><span>*</span></div>
                            <div class="consume_input add-c">
                                <%-- <editable:EditableDropDownList ID="ddlBusinessUnit" runat="server" Sorted="false"
                                    placeholder="Select Business Unit" AutoselectFirstItem="False" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlBusinessUnit_SelectedIndexChanged">
                                </editable:EditableDropDownList>--%>
                                <asp:DropDownList ID="ddlBusinessUnit" onchange="DropDownlistOnChangelbl(this,'spanBusinessUnit','Business Unit')"
                                    runat="server" AutoPostBack="true" TabIndex="24" OnSelectedIndexChanged="ddlBusinessUnit_SelectedIndexChanged">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <%--<asp:HyperLink ID="hlkAddBU" runat="server" TabIndex="25" Target="_blank" NavigateUrl="~/Masters/AddBusinessUnits.aspx"
                                    ToolTip="Add Business Unit">Add</asp:HyperLink>--%>
                                <div class="error">
                                    <span id="spanBusinessUnit" class="spancolor"></span>
                                </div>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="litUnderTaking" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal>
                                <span>*</span>
                            </div>
                            <div class="consume_input add-c">
                                <%-- <editable:EditableDropDownList ID="ddlServiceUnit" runat="server" AutoPostBack="true"
                                    placeholder="Select Service Unit" Sorted="false" AutoselectFirstItem="False"
                                    OnSelectedIndexChanged="ddlServiceUnit_SelectedIndexChanged">
                                </editable:EditableDropDownList>--%>
                                <asp:DropDownList ID="ddlServiceUnit" TabIndex="26" onchange="DropDownlistOnChangelbl(this,'spanServiceUnit','Service Unit')"
                                    runat="server" AutoPostBack="true" Enabled="false" OnSelectedIndexChanged="ddlServiceUnit_SelectedIndexChanged">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <asp:HyperLink ID="hlkAddSU" runat="server" TabIndex="27" Target="_blank" NavigateUrl="~/Masters/AddServiceUnits.aspx"
                                    ToolTip="Add Service Unit">Add</asp:HyperLink>
                                <div class="error">
                                    <span id="spanServiceUnit" class="spancolor"></span>
                                </div>
                            </div>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="litServiceCenter" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal>
                                <span>*</span></div>
                            <div class="consume_input add-c">
                                <%--  <editable:EditableDropDownList ID="ddlServiceCenter" runat="server" Sorted="false"
                                    placeholder="Select Service Center" AutoselectFirstItem="False" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlServiceCenter_SelectedIndexChanged">
                                </editable:EditableDropDownList>--%>
                                <asp:DropDownList ID="ddlServiceCenter" TabIndex="28" Enabled="false" onchange="DropDownlistOnChangelbl(this,'spanServiceCenter','Service Center')"
                                    runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlServiceCenter_SelectedIndexChanged">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <asp:HyperLink ID="hlkServiceCenter" TabIndex="29" runat="server" Target="_blank"
                                    NavigateUrl="~/Masters/AddServiceCenters.aspx" ToolTip="Add Service Center">Add</asp:HyperLink>
                                <div class="error">
                                    <span id="spanServiceCenter" class="spancolor"></span>
                                </div>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal56" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal><span>*</span></div>
                            <div class="consume_input add-c">
                                <asp:DropDownList ID="ddlCycle" Enabled="false" TabIndex="30" onchange="DropDownlistOnChangelbl(this,'spanCycle','Cycle')"
                                    runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCycle_SelectedIndexChanged">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <asp:HyperLink ID="hlkCycle" runat="server" TabIndex="31" Target="_blank" NavigateUrl="~/Masters/AddCycle.aspx"
                                    ToolTip="Add Cycle">Add</asp:HyperLink>
                                <div class="error">
                                    <span id="spanCycle" class="spancolor"></span>
                                </div>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="litBookNumber" runat="server" Text="<%$ Resources:Resource, BOOK_NUMBER%>"></asp:Literal><span>*</span></div>
                            <div class="consume_input add-c">
                                <%-- <editable:EditableDropDownList ID="ddlBookNumber" runat="server" Sorted="false" AutoselectFirstItem="False"
                                    placeholder="Select Book Number">
                                </editable:EditableDropDownList>--%>
                                <asp:DropDownList ID="ddlBookNumber" Enabled="false" TabIndex="32" onchange="DropDownlistOnChangelbl(this,'spanBookNumber','Book No')"
                                    runat="server">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <asp:HyperLink ID="hlkAddBookNo" runat="server" Target="_blank" TabIndex="33" NavigateUrl="~/Masters/AddBookNumbers.aspx"
                                    ToolTip="Add Book Number">Add</asp:HyperLink>
                                <div class="error">
                                    <span id="spanBookNumber" class="spancolor"></span>
                                </div>
                            </div>
                        </div>
                        <div class="clear ">
                        </div>
                        <h3>
                            <asp:Literal ID="Literal35" runat="server" Text="<%$ Resources:Resource, TRANSFORMER_DETAILS%>"></asp:Literal></h3>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="Literal36" runat="server" Text="<%$ Resources:Resource, INJECTION_SUBSTATION%>"></asp:Literal>
                            </div>
                            <div class="consume_input add-c">
                                <%-- <editable:EditableDropDownList ID="ddlInjectionSubStation" runat="server" Sorted="false"
                                            placeholder="Select SubStation" AutoPostBack="true" AutoselectFirstItem="False"
                                            OnSelectedIndexChanged="ddlInjectionSubStation_SelectedIndexChanged">
                                        </editable:EditableDropDownList>--%>
                                <asp:DropDownList ID="ddlInjectionSubStation" TabIndex="34" AutoPostBack="true" OnSelectedIndexChanged="ddlInjectionSubStation_SelectedIndexChanged"
                                    runat="server">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <asp:HyperLink ID="hlkAddSubStation" runat="server" TabIndex="35" Target="_blank"
                                    NavigateUrl="~/Masters/AddSubStations.aspx" ToolTip="Add Injection SubStation">Add</asp:HyperLink>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal37" runat="server" Text="<%$ Resources:Resource, FEEDER%>"></asp:Literal>
                            </div>
                            <div class="consume_input add-c">
                                <%--  <editable:EditableDropDownList ID="ddlFeeder" runat="server" Sorted="false" AutoselectFirstItem="False"
                                            placeholder="Select Feeder" AutoPostBack="true" OnSelectedIndexChanged="ddlFeeder_SelectedIndexChanged">
                                        </editable:EditableDropDownList>--%>
                                <asp:DropDownList ID="ddlFeeder" Enabled="false" runat="server" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlFeeder_SelectedIndexChanged" TabIndex="36">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <asp:HyperLink ID="hlkAddFeeder" runat="server" Target="_blank" TabIndex="37" NavigateUrl="~/Masters/AddFeeders.aspx"
                                    ToolTip="Add Feeder">Add</asp:HyperLink>
                            </div>
                            <div class="pad_5 clear">
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal38" runat="server" Text="<%$ Resources:Resource, TRANSFORMER%>"></asp:Literal>
                            </div>
                            <div class="consume_input add-c">
                                <%-- <editable:EditableDropDownList ID="ddlTransformer" runat="server" Sorted="false"
                                            placeholder="Select Transformer" AutoselectFirstItem="False" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlTransformer_SelectedIndexChanged">
                                        </editable:EditableDropDownList>--%>
                                <asp:DropDownList ID="ddlTransformer" Enabled="false" runat="server" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlTransformer_SelectedIndexChanged" TabIndex="38">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <asp:HyperLink ID="hlkAddTransformer" runat="server" TabIndex="39" Target="_blank"
                                    NavigateUrl="~/Masters/AddTransformers.aspx" ToolTip="Add Transformer">Add</asp:HyperLink>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal39" runat="server" Text="<%$ Resources:Resource, POLE%>"></asp:Literal>
                            </div>
                            <div class="consume_input add-c">
                                <%--<editable:EditableDropDownList ID="ddlPole" runat="server" Sorted="false" AutoselectFirstItem="False"
                                            placeholder="Select Pole">
                                        </editable:EditableDropDownList>--%>
                                <asp:DropDownList ID="ddlPole" Enabled="false" TabIndex="40" runat="server">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <asp:HyperLink ID="hlkAddPole" runat="server" TabIndex="41" Target="_blank" NavigateUrl="~/Masters/AddPoles.aspx"
                                    ToolTip="Add Pole">Add</asp:HyperLink>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="pad_5 clear">
                </div>
                <h3>
                    <asp:Literal ID="litServiceAddress" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Literal>
                </h3>
                <div class="consumer_feild" style="margin-left: 13px;">
                    Same as postal address
                    <input id="Radio1" name="Address" tabindex="19" type="radio" onclick="CopyAddress(true)"
                        runat="server" />
                    Yes
                    <input id="Radio2" name="Address" tabindex="20" type="radio" onclick="return CopyAddress(false)"
                        runat="server" />
                    No
                </div>
                <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                    <ContentTemplate>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                House No<span>*</span></div>
                            <div class="consume_input add-c">
                                <asp:TextBox ID="txtServiceHouseNo" runat="server" TabIndex="42" placeholder="House No"
                                    onblur="return TextBoxBlurValidationlblZeroAccepts(this,'spanServiceHouseNo','Service House No')"></asp:TextBox><br />
                                <div class="error">
                                    <span id="spanServiceHouseNo" class="spancolor"></span>
                                </div>
                            </div>
                            <div class="consume_name">
                                Address 1<span>*</span></div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtServiceStreet" runat="server" TabIndex="43" placeholder="Address 1"
                                    onblur="return TextBoxBlurValidationlbl(this,'spanServiceStreet','Service Address 1')"></asp:TextBox><br />
                                <div class="error">
                                    <span id="spanServiceStreet" class="spancolor"></span>
                                </div>
                            </div>
                            <div class="consume_name">
                                Address 2</div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtServiceCity" placeholder="Address 2" runat="server" TabIndex="44"></asp:TextBox><br />
                                <div class="error">
                                    <span id="spanServiceCity" class="spancolor"></span>
                                </div>
                            </div>
                            <div class="pad_5 clear">
                            </div>
                            <div class="consume_name">
                                Landmark</div>
                            <div class="consume_input add-c">
                                <asp:TextBox ID="txtServiceLandMark" runat="server" TabIndex="45" placeholder="Landmark"></asp:TextBox>
                                <div class="error">
                                    <span id="spanServiceLandMark" class="spancolor"></span>
                                </div>
                            </div>
                            <%-- <div class="text_box">
                                        <asp:TextBox ID="txtServiceLineNo" runat="server" placeholder="Line No 1" onblur="return TextBoxBlurValidationlbl(this,'spanServiceLineNo','Service Line No')"></asp:TextBox><br />
                                        <div class="error">
                                            <span id="spanServiceLineNo" class="spancolor"></span>
                                        </div>
                                    </div>--%>
                            <div class="consume_name">
                                Details</div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtServiceDetails" TextMode="MultiLine" TabIndex="46" runat="server"
                                    placeholder="Details"></asp:TextBox><br />
                                <div class="error">
                                    <span id="spanServiceDetails" class="spancolor"></span>
                                </div>
                            </div>
                            <div class="consume_name">
                                ZipCode</div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtServiceZipCode" runat="server" TabIndex="47" placeholder="ZipCode"></asp:TextBox><br />
                                <div class="error">
                                    <span id="spanServiceZipCode" class="spancolor"></span>
                                </div>
                            </div>
                        </div>
                        <div class="pad_5 clear">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="litMobileNo" runat="server" Text="<%$ Resources:Resource, MOBILE_NO%>"></asp:Literal>
                                <span>*</span>
                            </div>
                            <div class="consume_input add-c">
                                <asp:TextBox ID="txtMobileNo" runat="server" MaxLength="20" TabIndex="48" onblur="return ValidateMobileNo()"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtMobileNo"
                                    runat="server" FilterType="Numbers" FilterMode="ValidChars">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanMobileNo" class="spancolor"></span>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal51" runat="server" Text="<%$ Resources:Resource, EMAIL_ID%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtServiceEmailId" runat="server" TabIndex="49" placeholder="Enter Emaid Id"></asp:TextBox><br />
                                <span id="spanemailadd" class="spancolor"></span>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal47" runat="server" Text="<%$ Resources:Resource, LATITUDE%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtLatitude" runat="server" TabIndex="50"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtLatitude"
                                    runat="server" FilterType="Numbers,Custom" FilterMode="ValidChars" ValidChars=".">
                                </asp:FilteredTextBoxExtender>
                            </div>
                        </div>
                        <div class="pad_5 clear">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="litLongitude" runat="server" Text="<%$ Resources:Resource, LONGITUDE%>"></asp:Literal>
                            </div>
                            <div class="consume_input add-c">
                                <asp:TextBox ID="txtLongitude" runat="server" TabIndex="51"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" TargetControlID="txtLongitude"
                                    runat="server" FilterType="Custom,Numbers" FilterMode="ValidChars" ValidChars=".">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanLongitude" class="spancolor"></span>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal31" runat="server" Text="<%$ Resources:Resource, ROUTE_NO%>"></asp:Literal><span>*</span></div>
                            <div class="consume_input">
                                <%-- <editable:EditableDropDownList ID="ddlBusinessUnit" runat="server" Sorted="false"
                                    placeholder="Select Business Unit" AutoselectFirstItem="False" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlBusinessUnit_SelectedIndexChanged">
                                </editable:EditableDropDownList>--%>
                                <asp:DropDownList ID="ddlRouteNo" runat="server" TabIndex="52">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <div class="error">
                                    <span id="spanRouteNo" class="spancolor"></span>
                                </div>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="litRouteNo" runat="server" Text="<%$ Resources:Resource, ROUTE_SEQUENCE_NO%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtRouteSequenceNo" runat="server" TabIndex="53" MaxLength="50"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" TargetControlID="txtRouteSequenceNo"
                                    runat="server" FilterType="Numbers" FilterMode="ValidChars">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanRouteSequenceNo" class="spancolor"></span>
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                        <%--  End content--%>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="Literal40" runat="server" Text="<%$ Resources:Resource, NEIGHBOR_ACCOUNT_NO%>"></asp:Literal>
                            </div>
                            <div class="consume_input add-c">
                                <asp:TextBox ID="txtneighbor" runat="server" TabIndex="54" onblur="return IsNeighborAccountNoExists(this)"></asp:TextBox>
                                <span id="spanneighbor" class="spancolor"></span>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <%----------------------------------------------------------Step 3----------------------------------------- --%>
        <div id="step-3" style="height: 700px;">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <div class="tariff_Details" style="position: absolute; right: 4px; z-index: 1000;">
                        <div id="divTariff" runat="server" visible="false">
                            Tariff Type :
                            <asp:Label ID="lblTariffType" runat="server" Text=""></asp:Label></div>
                        <asp:GridView ID="gvTariffDetails" AutoGenerateColumns="false" runat="server" Visible="true"
                            CssClass="charges_bind">
                            <Columns>
                                <asp:BoundField HeaderText="Charge" DataField="ChargeName" NullDisplayText="--" />
                                <asp:BoundField HeaderText="Amount" DataField="Amount" NullDisplayText="--" />
                                <asp:BoundField HeaderText="Tax" Visible="false" DataField="Tax" NullDisplayText="--" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <h2 class="StepTitle">
                <asp:Literal ID="litAccountInformation" runat="server" Text="<%$ Resources:Resource, ACCOUNT_INFORMATION%>"></asp:Literal>
                <%--<div class="fltr" style="font-weight: bold; padding-right: 20px;">
                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="lbtnRefreshStep3" runat="server" OnClick="lbtnRefreshStep3_Click">Refresh</asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>--%>
            </h2>
            <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                <ContentTemplate>
                    <div class="consumer_total">
                        <div class="pad_5">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="litTariff" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                                <span>*</span></div>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                    <div class="consume_input">
                                        <%--  <editable:EditableDropDownList ID="ddlTariff" runat="server" Sorted="false" AutoPostBack="true"
                                    AutoselectFirstItem="False" placeholder="Select Tariff" OnSelectedIndexChanged="ddlTariff_SelectedIndexChanged">
                                </editable:EditableDropDownList>--%>
                                        <asp:DropDownList ID="ddlTariff" TabIndex="55" onchange="DropDownlistOnChangelbl(this,'spanTariff','Tariff')"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlTariff_SelectedIndexChanged" runat="server"
                                            Width="171px">
                                            <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                        <div class="error">
                                            <span id="spanTariff" class="spancolor"></span>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div class="consume_name">
                                <asp:Literal ID="Literal42" runat="server" Text="<%$ Resources:Resource, MULTIPLICATION_FACTOR%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtTariffMultiplier" TabIndex="56" Text="1" runat="server"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" TargetControlID="txtTariffMultiplier"
                                    runat="server" FilterType="Numbers,Custom" FilterMode="ValidChars" ValidChars=".">
                                </asp:FilteredTextBoxExtender>
                            </div>
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="litConnectionType" runat="server" Text="<%$ Resources:Resource, ACCOUNT_TYPE%>"></asp:Literal>
                                <span>*</span></div>
                            <div class="consume_input">
                                <%--  <editable:EditableDropDownList ID="ddlAccountType" runat="server" Sorted="false"
                            placeholder="Select Account Type" AutoselectFirstItem="False">
                        </editable:EditableDropDownList>--%>
                                <asp:DropDownList ID="ddlAccountType" TabIndex="57" onchange="DropDownlistOnChangelbl(this,'spanAccountType','Account Type')"
                                    runat="server">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <div class="error">
                                    <span id="spanAccountType" class="spancolor"></span>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="consumer_feild">
                                <div class="consume_name">
                                    <asp:Literal ID="Literal48" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                </div>
                                <div class="consume_input">
                                    <asp:TextBox ID="txtOldAccountNo" TabIndex="58" onblur="return IsOldAccountNoExists(this)"
                                        runat="server"></asp:TextBox>
                                    <span id="spanOldAccountNo" class="spancolor"></span>
                                </div>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="litApplicationDate" runat="server" Text="<%$ Resources:Resource, APPLICATION_DATE%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <asp:TextBox runat="server" CssClass="calender_total" TabIndex="59" ID="txtApplicationDate"
                                    Style="width: 133px;" AutoComplete="off"></asp:TextBox>
                                <asp:Image ID="imgDate" ImageUrl="~/images/icon_calendar.png" TabIndex="60" Style="width: 20px;
                                    vertical-align: middle" AlternateText="Calender" runat="server" />
                                <asp:FilteredTextBoxExtender ID="ftxtApplicationDate" runat="server" TargetControlID="txtApplicationDate"
                                    FilterType="Custom,Numbers" ValidChars="/">
                                </asp:FilteredTextBoxExtender>
                                <div class="error">
                                    <span id="spanApplicationDate" class="spancolor"></span>
                                </div>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="litReadCode" runat="server" Text="<%$ Resources:Resource, READCODE%>"></asp:Literal><span>*</span></div>
                            <div class="consume_input">
                                <%-- <editable:EditableDropDownList ID="ddlReadCode" runat="server" Sorted="false" AutoselectFirstItem="False"
                            placeholder="Select Read Code">
                        </editable:EditableDropDownList>--%>
                                <asp:UpdatePanel ID="upnlReadCode" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlReadCode" TabIndex="61" onchange="DropDownlistOnChangelbl(this,'spanReadCode','Read Code')"
                                            runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlReadCode_SelectedIndexChanged">
                                            <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                        <div class="error">
                                            <span id="spanReadCode" class="spancolor"></span>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class=" clear">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="litInstitueCode" runat="server" Text="<%$ Resources:Resource, ORGANIZATION_INSTITUTION_CODE%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <asp:TextBox runat="server" TabIndex="62" ID="txtOrganizationCode"></asp:TextBox></div>
                            <div class="consume_name">
                                <asp:Literal ID="litCustomerType" runat="server" Text="<%$ Resources:Resource, CUSTOMER_TYPE%>"></asp:Literal>
                                <span>*</span></div>
                            <div class="consume_input">
                                <%-- <editable:EditableDropDownList ID="ddlCustomerType" runat="server" Sorted="false"
                            placeholder="Select Customer Type" AutoselectFirstItem="False">
                        </editable:EditableDropDownList>--%>
                                <asp:DropDownList ID="ddlCustomerType" TabIndex="63" onchange="DropDownlistOnChangelbl(this,'spanCustomerType','Customer Type')"
                                    runat="server">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <div class="error">
                                    <span id="spanCustomerType" class="spancolor"></span>
                                </div>
                            </div>
                            <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                <ContentTemplate>
                                    <div id="divMinReading" visible="false" runat="server">
                                        <div class="consume_name">
                                            <asp:Literal ID="Literal34" runat="server" Text="<%$ Resources:Resource, MIN_READING%>"></asp:Literal>
                                            <span>*</span></div>
                                        <div class="consume_input">
                                            <asp:TextBox runat="server" ID="txtMinReading" TabIndex="64" onblur="return TextBoxBlurValidationlbl(this,'spanMinReading','Min Reading')"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="txtMinReading"
                                                FilterType="Custom,Numbers">
                                            </asp:FilteredTextBoxExtender>
                                            <div class="error">
                                                <span id="spanMinReading" class="spancolor"></span>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div class="clear">
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal52" runat="server" Text="<%$ Resources:Resource, IS_EMBASSY_CUSTOMER%>"></asp:Literal>
                            </div>
                            <div class="consume_check">
                                <asp:CheckBox ID="chkIsEmbassyCustomer" TabIndex="65" runat="server" onclick="return Embassy()" />
                            </div>
                            <div id="divEmbassyCode" style="display: none;">
                                <div class="consume_name" style="width: 100px;">
                                    <asp:Literal ID="Literal53" runat="server" Text="<%$ Resources:Resource, EMBASSY_CODE%>"></asp:Literal>
                                </div>
                                <div class="consume_input">
                                    <asp:TextBox runat="server" ID="txtEmbassyCode" TabIndex="66" onblur="return TextBoxBlurValidationlbl(this,'spanEmbassyCode','Embassy Code')"></asp:TextBox>
                                    <span id="spanEmbassyCode" class="spancolor"></span>
                                </div>
                            </div>
                        </div>
                        <div class="pad_5 clear">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="Literal54" runat="server" Text="<%$ Resources:Resource, VIP_CUSTOMER%>"></asp:Literal>
                            </div>
                            <div class="consume_check">
                                <asp:CheckBox ID="chkIsVipCustomer" TabIndex="67" runat="server" /></div>
                            <div class="clear">
                            </div>
                            <div id="divMeterInformation" runat="server">
                                <asp:UpdatePanel ID="upnlMeterInformation" runat="server">
                                    <ContentTemplate>
                                        <h2 class="StepTitle">
                                            <asp:Literal ID="litMeterInformation" runat="server" Text="<%$ Resources:Resource, METER_INFORMATION%>"></asp:Literal>
                                        </h2>
                                        <div class="pad_5">
                                        </div>
                                        <div class="consumer_feild">
                                            <div class="consume_name">
                                                <asp:Literal ID="litConnectionType2" runat="server" Text="<%$ Resources:Resource, METER_TYPE%>"></asp:Literal><span>*</span></div>
                                            <div class="consume_input">
                                                <%--   <editable:EditableDropDownList ID="ddlMeterType" runat="server" Sorted="false" placeholder="Select Meter Type"
                                AutoselectFirstItem="False">
                            </editable:EditableDropDownList>--%>
                                                <asp:DropDownList ID="ddlMeterType" TabIndex="68" onchange="DropDownlistOnChangelbl(this,'spanMeterType','Meter Type')"
                                                    runat="server">
                                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                </asp:DropDownList>
                                                <div class="error">
                                                    <span id="spanMeterType" class="spancolor"></span>
                                                </div>
                                            </div>
                                            <div class="consume_name">
                                                <asp:Literal ID="litPhase" runat="server" Text="<%$ Resources:Resource, PHASE%>"></asp:Literal>
                                                <span>*</span></div>
                                            <div class="consume_input">
                                                <%--  <editable:EditableDropDownList ID="ddlPhase" runat="server" Sorted="false" AutoselectFirstItem="False"
                                placeholder="Select Phase">
                            </editable:EditableDropDownList>--%>
                                                <asp:DropDownList ID="ddlPhase" TabIndex="69" onchange="DropDownlistOnChangelbl(this,'spanPhase','Phase')"
                                                    runat="server">
                                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                </asp:DropDownList>
                                                <div class="error">
                                                    <span id="spanPhase" class="spancolor"></span>
                                                </div>
                                            </div>
                                            <asp:UpdatePanel ID="upnlmeterno" runat="server">
                                                <ContentTemplate>
                                                    <div class="consume_name">
                                                        <asp:Literal ID="litMeterNo" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Literal>
                                                        <span>*</span></div>
                                                    <div class="consume_input" style="width: 200px">
                                                        <asp:TextBox ID="txtMeterNo" runat="server" TabIndex="70" onblur="return IsMeterNoExists(this)"></asp:TextBox>
                                                        <asp:HyperLink ID="hlkAddMeterInfo" runat="server" TabIndex="71" Target="_blank"
                                                            NavigateUrl="~/Masters/MeterInformation.aspx" ToolTip="Add State">Add</asp:HyperLink>
                                                        <br />
                                                        <span id="spanMeterNo" class="spancolor"></span>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <%--    <div class="consumer_feild">
                     <div class="consume_name">
                                    <asp:Literal ID="litMeterModel" runat="server" Text="<%$ Resources:Resource, METER_MODEL%>"></asp:Literal>
                                    <span>*</span></div>
                                <div class="fltl">
                                    <editable:EditableDropDownList ID="ddlMeterModel" runat="server" Sorted="false" AutoselectFirstItem="False"
                                        placeholder="Select Meter Model">
                                    </editable:EditableDropDownList>
                                    <div class="error">
                                        <span id="spanMeterModel" class="spancolor"></span>
                                    </div>
                                </div>
                     
                    </div>--%>
                                        <div class="clear">
                                        </div>
                                        <div class="consumer_feild">
                                            <div class="consume_name">
                                                <asp:Literal ID="litMeterSerialNo" runat="server" Text="<%$ Resources:Resource, METER_SERIAL_NUMBER%>"></asp:Literal>
                                            </div>
                                            <div class="consume_input">
                                                <asp:TextBox ID="txtMeeterSerialNo" TabIndex="72" runat="server" onblur="return IsMeterSerialNoExists(this)"></asp:TextBox>
                                                <span id="spanMeterSerialNo" class="spancolor"></span>
                                            </div>
                                            <div class="consume_name">
                                                <asp:Literal ID="litConnectionDate" runat="server" Text="<%$ Resources:Resource, CONNECTION_DATE%>"></asp:Literal>
                                                <span>*</span></div>
                                            <div class="consume_input">
                                                <asp:TextBox Style="width: 133px;" ID="txtConnectionDate" TabIndex="73" CssClass="calender_total"
                                                    runat="server" AutoComplete="off" onchange="return TextBoxBlurValidationlbl(this,'spanConnectionDate','Connection Date')"
                                                    onblur="return TextBoxBlurValidationlbl(this,'spanConnectionDate','Connection Date')"></asp:TextBox>
                                                <asp:Image ID="imgDate2" ImageUrl="~/images/icon_calendar.png" TabIndex="74" Style="width: 20px;
                                                    vertical-align: middle" AlternateText="Calender" runat="server" />
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtConnectionDate"
                                                    FilterType="Custom,Numbers" ValidChars="/">
                                                </asp:FilteredTextBoxExtender>
                                                <div class="error">
                                                    <span id="spanConnectionDate" class="spancolor"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="consumer_feild">
                                            <div class="consume_name">
                                                <asp:Literal ID="litMeterStatus" runat="server" Text="<%$ Resources:Resource, METER_STATUS%>"></asp:Literal>
                                            </div>
                                            <div class="consume_input">
                                                <%--   <editable:EditableDropDownList ID="ddlMeeterStatus" runat="server" Sorted="false"
                                placeholder="Select Meter Status" AutoselectFirstItem="False">
                            </editable:EditableDropDownList>--%>
                                                <asp:DropDownList ID="ddlMeeterStatus" runat="server" TabIndex="75">
                                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                </asp:DropDownList>
                                                <div class="error">
                                                    <span id="spanMeterStatus" class="spancolor"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear ">
                                        </div>
                                        <div class="consumer_feild">
                                            <div class="consume_name">
                                                <asp:Literal ID="litConnectionReason" runat="server" Text="<%$ Resources:Resource, CONNECTION_REASON%>"></asp:Literal>
                                                <span>*</span></div>
                                            <div class="consume_input">
                                                <%-- <editable:EditableDropDownList ID="ddlConnectionReason" runat="server" Sorted="false"
                                placeholder="Select ConnectionReason" AutoselectFirstItem="False">
                            </editable:EditableDropDownList>--%>
                                                <asp:DropDownList ID="ddlConnectionReason" onchange="DropDownlistOnChangelbl(this,'spanConnectionReason','Connection Reason')"
                                                    runat="server" TabIndex="76">
                                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                </asp:DropDownList>
                                                <div class="error">
                                                    <span id="spanConnectionReason" class="spancolor"></span>
                                                </div>
                                            </div>
                                            <div class="consume_name">
                                                <asp:Literal ID="litInitialReading" runat="server" Text="<%$ Resources:Resource, INITIAL_READING%>"></asp:Literal>
                                                <span>*</span></div>
                                            <div class="consume_input">
                                                <%-- onblur="return TextBoxValidationlblZeroAccepts(this,'spanInitialReading','Initial Reading')"--%>
                                                <asp:TextBox ID="txtInitialReading" TabIndex="77" Enabled="false" runat="server"
                                                    onblur="return MeterDials(this)"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" TargetControlID="txtInitialReading"
                                                    runat="server" FilterType="Numbers,Custom" FilterMode="ValidChars" ValidChars=".">
                                                </asp:FilteredTextBoxExtender>
                                                <span id="spanInitialReading" class="spancolor"></span>
                                            </div>
                                            <%--  <div style="width: 221px;" class="consume_name">
                            <asp:Literal ID="litCurrentReading" runat="server" Text="<%$ Resources:Resource, CURRENT_READING%>"></asp:Literal>
                            <span>*</span></div>
                        <div class="consume_input">
                            <asp:TextBox ID="txtCurrentReading" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanCurrentReading','Current Reading')"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" TargetControlID="txtCurrentReading"
                                runat="server" FilterType="Numbers,Custom" FilterMode="ValidChars" ValidChars=".">
                            </asp:FilteredTextBoxExtender>
                            <span id="spanCurrentReading" class="spancolor"></span>
                        </div>--%>
                                            <%--<div class="consume_name">
                                                <asp:Literal ID="litReadDate" runat="server" Text="<%$ Resources:Resource, SETUP_DATE%>"></asp:Literal>
                                                <span>*</span></div>
                                            <div class="consume_input">
                                                <asp:TextBox Style="width: 133px;" ID="txtSetupDate" TabIndex="78" runat="server"
                                                    AutoComplete="off" CssClass="calender_total" onchange="return TextBoxBlurValidationlbl(this,'spanSetupDate','Setup Date')"
                                                    onblur="return TextBoxBlurValidationlbl(this,'spanSetupDate','Setup Date')"></asp:TextBox>
                                                <asp:Image ID="imgDate3" ImageUrl="~/images/icon_calendar.png" TabIndex="79" Style="width: 20px;
                                                    vertical-align: middle" AlternateText="Calender" runat="server" />
                                                <asp:FilteredTextBoxExtender ID="ftbsetupdate" runat="server" TargetControlID="txtSetupDate"
                                                    FilterType="Custom,Numbers" ValidChars="/">
                                                </asp:FilteredTextBoxExtender>
                                                <div class="error">
                                                    <span id="spanSetupDate" class="spancolor"></span>
                                                </div>
                                            </div>--%>
                                        </div>
                                        <div class="clear ">
                                        </div>
                                        <div class="consumer_feild">
                                            <div class="consume_name">
                                                <asp:Literal ID="litAverageReading" runat="server" Text="<%$ Resources:Resource, AVERAGE_READING%>"></asp:Literal>
                                                <span>*</span></div>
                                            <div class="consume_input">
                                                <asp:TextBox ID="txtAverageReading" runat="server" TabIndex="80" onblur="return TextBoxBlurValidationlbl(this,'spanAverageReading','Average Reading')"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" TargetControlID="txtAverageReading"
                                                    runat="server" FilterType="Numbers,Custom" FilterMode="ValidChars" ValidChars=".">
                                                </asp:FilteredTextBoxExtender>
                                                <span id="spanAverageReading" class="spancolor"></span>
                                            </div>
                                        </div>
                                        <div class="pad_5">
                                        </div>
                                        <div class="consumer_feild">
                                            <div class="consume_name">
                                                <asp:Literal ID="litCertifiedBy" runat="server" Text="<%$ Resources:Resource, CERTIFIED_BY%>"></asp:Literal>
                                            </div>
                                            <div class="consume_input">
                                                <%-- <editable:EditableDropDownList ID="ddlCertifiedBy" runat="server" Sorted="false"
                                placeholder="Select CertifiedBy" AutoselectFirstItem="False">
                            </editable:EditableDropDownList>--%>
                                                <asp:DropDownList ID="ddlCertifiedBy" TabIndex="81" runat="server">
                                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                </asp:DropDownList>
                                                <div class="error">
                                                    <span id="spanCertifiedBy" class="spancolor"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear pad_5">
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div class="consume_name">
                                    <asp:Literal ID="Literal5" runat="server" Text="Is CAPMI"></asp:Literal>
                                </div>
                                <div class="consume_check">
                                    <%--onclick="return IsPaidMeter()--%>
                                    <asp:CheckBox ID="cbIsPaidMeter" OnCheckedChanged="chkIsPaidMeter_CheckedChanged"
                                        AutoPostBack="true" TabIndex="65" runat="server" />
                                </div>
                                <div id="divPaidMeter" runat="server" visible="false">
                                    <div class="consume_name" style="width: 100px;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Meter Amount"></asp:Literal>
                                    </div>
                                    <div class="consume_input">
                                        <asp:TextBox runat="server" ID="txtMeterAmt" TabIndex="66" onblur="return TextBoxBlurValidationlbl(this,'spanMeterAmt','Meter Amount')"></asp:TextBox>
                                        <span id="spanMeterAmt" class="spancolor"></span>
                                    </div>
                                </div>
                                <div class="clear pad_5">
                                </div>
                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" style="padding-top: 7px;">
                                    <ContentTemplate>
                                        <div class="consumer_feild">
                                            <div class="consume_name">
                                                <asp:Literal ID="Literal43" runat="server" Text="<%$ Resources:Resource, APPLICATION_PROCCESED_BY%>"></asp:Literal>
                                            </div>
                                            <div>
                                                <asp:RadioButtonList ID="rblApplicationProccessedBy" runat="server" AutoPostBack="true"
                                                    RepeatDirection="Horizontal" TabIndex="82" OnSelectedIndexChanged="rblApplicationProccessedBy_SelectedIndexChanged">
                                                </asp:RadioButtonList>
                                                <span id="spanApplicationProcessedBy" class="spancolor"></span>
                                                <%--                                    <asp:RadioButton ID="rbtnBEDC" Text="BEDC" GroupName="AppBy" runat="server" Checked="True"
                                        onclick="return InstalledBy(false)" />
                                    <asp:RadioButton ID="rbtnOthers" Text="Others" GroupName="AppBy" runat="server" onclick="return InstalledBy(true)" />
                                                --%>
                                            </div>
                                        </div>
                                        <div class="clear pad_5">
                                        </div>
                                        <div id="divBEDCApplication" runat="server">
                                            <div class="consumer_feild">
                                                <div class="consume_name">
                                                    <asp:Literal ID="Literal44" runat="server" Text="<%$ Resources:Resource, INSTALLED_BY%>"></asp:Literal>
                                                    <span>*</span>
                                                </div>
                                                <div class="consume_input">
                                                    <%--  <editable:EditableDropDownList ID="ddlInstalledBy" placeholder="Select InstalledBy"
                                    runat="server" Sorted="false" AutoselectFirstItem="False">
                                </editable:EditableDropDownList>--%>
                                                    <asp:DropDownList ID="ddlInstalledBy" onchange="DropDownlistOnChangelbl(this,'spanInstalledBy','Installed By')"
                                                        runat="server" TabIndex="83">
                                                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <span style="color: Red; display: block;" id="spanInstalledBy"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divApplicationByOthers" visible="false" runat="server">
                                            <div class="consumer_feild">
                                                <div class="consume_name">
                                                    <asp:Literal ID="Literal45" runat="server" Text="<%$ Resources:Resource, AGENCY_NAME%>"></asp:Literal>
                                                    <span>*</span>
                                                </div>
                                                <div class="consume_input">
                                                    <%--    <editable:EditableDropDownList ID="ddlAgencyName" runat="server" Sorted="false" AutoselectFirstItem="False"
                                    placeholder="Select Agency Name">
                                </editable:EditableDropDownList>--%>
                                                    <asp:DropDownList ID="ddlAgencyName" TabIndex="84" onchange="DropDownlistOnChangelbl(this,'spanAgency','Agency')"
                                                        runat="server">
                                                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <span style="color: Red; display: block;" id="spanAgency"></span>
                                                </div>
                                            </div>
                                            <div class="clear pad_5">
                                            </div>
                                            <div class="consumer_feild">
                                                <div class="consume_name">
                                                    Contact Person Name</div>
                                                <div class="consume_input">
                                                    <asp:TextBox ID="txtContactPersonName" TabIndex="85" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="clear pad_5">
                                            </div>
                                            <div class="consumer_feild">
                                                <div class="consume_name">
                                                    <asp:Literal ID="Literal46" runat="server" Text="<%$ Resources:Resource, DETAILS%>"></asp:Literal>
                                                </div>
                                                <div class="consume_input">
                                                    <asp:TextBox ID="txtAgencyDetails" TabIndex="86" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <%--  End content--%>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <%----------------------------------------------------------Step 4----------------------------------------- --%>
        <%--  End content--%>
        <%----------------------------------------------------------Step 5----------------------------------------- --%>
        <div id="step-4">
            <asp:UpdatePanel ID="UpdatePanel3" runat="server" style="padding-top: 7px;">
                <ContentTemplate>
                    <h2 class="StepTitle">
                        <asp:Literal ID="Literal24" runat="server" Text="<%$ Resources:Resource, UPLOAD_DOCUMENT%>"></asp:Literal></h2>
                    <div class="consumer_feild">
                        <div class="consume_name" style="width: 220px;">
                            <asp:Literal ID="Literal41" runat="server" Text="Select Documents"></asp:Literal>
                        </div>
                        <div id="divFile" class="fltl">
                            <asp:FileUpload ID="fupDocument" runat="server" CssClass="fltl" />&nbsp;
                            <asp:LinkButton ID="lnkAddFiles" runat="server" CssClass="fltl" OnClientClick="return AddFile()"
                                Font-Bold="True" Style="padding-top: 4px;">Add</asp:LinkButton>
                        </div>
                        <span id="spanDocuments" style="color: Red; width: 252px;"></span>
                    </div>
                    <h2 class="StepTitle">
                        <asp:Literal ID="litFinalInfo2" runat="server" Text="<%$ Resources:Resource, PERSONAL_DETAILS%>"></asp:Literal></h2>
                    <div class="consumer_total">
                        <div class="pad_5">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="litTitle2" runat="server" Text="<%$ Resources:Resource, TITLE%>"></asp:Literal></div>
                            <div class="consume_input">
                                <div style="padding-left: 12px;">
                                    <asp:Label ID="lblTitle" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal></div>
                            <div class="consume_input">
                                <div style="padding-left: 12px;">
                                    <asp:Label ID="lblName" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, SURNAME%>"></asp:Literal></div>
                            <div class="consume_input">
                                <div style="padding-left: 12px;">
                                    <asp:Label ID="lblSurName" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, DOCUMENT_NO%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <div style="padding-left: 12px;">
                                    <asp:Label ID="lblDocumentNo" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="pad_5 clear">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, POSTAL_ADDRESS%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <div class="consumer_postal" style="width: 748PX;">
                                    <div style="padding-left: 59px; float: left; width: 250px;">
                                        House No :
                                        <asp:Label ID="lblPAHouseNo" runat="server" Text="--"></asp:Label>
                                    </div>
                                    <div style="padding-left: 12px; float: left; width: 250px;">
                                        Address 1 :
                                        <asp:Label ID="lblPAStreet" runat="server" Text="--"></asp:Label>
                                    </div>
                                    <div class="clear pad_5">
                                    </div>
                                    <div style="padding-left: 45px; float: left; width: 250px;">
                                        Address 2 :
                                        <asp:Label ID="lblPACity" runat="server" Text="--"></asp:Label>
                                    </div>
                                    <div style="padding-left: 38px; float: left; width: 250px;">
                                        Landmark :
                                        <asp:Label ID="lblPALandMark" runat="server" Text="--"></asp:Label>
                                    </div>
                                    <%-- <div style="padding-left: 12px; float: left;">
                                        <asp:Label ID="lblPAPostBoxNo" runat="server" Text="--"></asp:Label>
                                    </div>--%>
                                </div>
                            </div>
                        </div>
                        <div class="pad_5 clear">
                        </div>
                        <div class="consumer_feild">
                            <%--<div class="consume_name">
                                <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, IDENTITY_TYPE%>"></asp:Literal><span>*</span>
                            </div>
                            <div class="consume_input">
                                <div style="padding-left: 12px; float: left;">
                                    <asp:Label ID="lblIdentityType" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, IDENTITY_NUMBER%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <div style="padding-left: 12px; float: left;">
                                    <asp:Label ID="lblIdentityNo" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>--%>
                        </div>
                        <div class="pad_5 clear">
                        </div>
                        <h2 class="StepTitle">
                            <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, SERVICE_DETAILS%>"></asp:Literal></h2>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <div style="padding-left: 12px; float: left;">
                                    <asp:Label ID="lblBusinessUnit" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <div style="padding-left: 12px; float: left;">
                                    <asp:Label ID="lblServiceUnit" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <div style="padding-left: 12px; float: left;">
                                    <asp:Label ID="lblServiceCenter" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="pad_5 clear">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="Literal57" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <div style="padding-left: 12px; float: left;">
                                    <asp:Label ID="lblCycle" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:Resource, BOOK_NUMBER%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <div style="padding-left: 12px; float: left;">
                                    <asp:Label ID="lblBookNumber" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                            <%--  <div class="consume_name">
                                <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:Resource, INJECTION_SUBSTATION%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <div style="padding-left: 12px; float: left;">
                                    <asp:Label ID="lblInjectionSubstation" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, FEEDER%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <div style="padding-left: 12px; float: left;">
                                    <asp:Label ID="lblFeeder" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>--%>
                        </div>
                        <div class="pad_5 clear">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <div class="consumer_postal" style="width: 748PX;">
                                    <div style="padding-left: 59px; float: left; width: 250px;">
                                        House No :
                                        <asp:Label ID="lblSAHouseNo" runat="server" Text="--"></asp:Label>
                                    </div>
                                    <div style="padding-left: 12px; float: left; width: 250px;">
                                        Address 1 :
                                        <asp:Label ID="lblSAStreet" runat="server" Text="--"></asp:Label>
                                    </div>
                                    <div class="clear">
                                        <br />
                                    </div>
                                    <div style="padding-left: 45px; float: left; width: 250px;">
                                        Address 2 :
                                        <asp:Label ID="lblSACity" runat="server" Text="--"></asp:Label>
                                    </div>
                                    <div style="padding-left: 38px; float: left; width: 250px;">
                                        LandMark :
                                        <asp:Label ID="lblSALandMark" runat="server" Text="--"></asp:Label>
                                    </div>
                                    <%--  <div style="padding-left: 12px; float: left;">
                                        <asp:Label ID="lblSAPostBoxNo" runat="server" Text="--"></asp:Label>
                                    </div>--%>
                                </div>
                            </div>
                        </div>
                        <div class="pad_5 clear">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, MOBILE_NO%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <div style="padding-left: 12px; float: left;">
                                    <asp:Label ID="lblMobileNo" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal26" runat="server" Text="<%$ Resources:Resource, ROUTE_NO%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <div style="padding-left: 12px; float: left;">
                                    <asp:Label ID="lblRouteNo" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="pad_5">
                        </div>
                        <h2 class="StepTitle">
                            <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:Resource, ACCOUNT_INFORMATION%>"></asp:Literal></h2>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <div style="padding-left: 12px; float: left;">
                                    <asp:Label ID="lblTariff" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:Resource, ACCOUNT_TYPE%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <div style="padding-left: 12px; float: left;">
                                    <asp:Label ID="lblConnectionType" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                            <%--<div class="consume_name">
                                <asp:Literal ID="Literal56" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal><span>*</span>
                                <span>*</span></div>
                            <div class="consume_input">
                                <div style="padding-left: 12px; float: left;">
                                    <asp:Label ID="lblOldAccountNo" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>--%>
                            <div class="consume_name">
                                <asp:Literal ID="Literal19" runat="server" Text="<%$ Resources:Resource, READCODE%>"></asp:Literal></div>
                            <div class="consume_input">
                                <div style="padding-left: 12px; float: left;">
                                    <asp:Label ID="lblReadCode" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="Literal20" runat="server" Text="<%$ Resources:Resource, CUSTOMER_TYPE%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <div style="padding-left: 12px; float: left;">
                                    <asp:Label ID="lblCustomerType" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal55" runat="server" Text="<%$ Resources:Resource, MIN_READING%>"></asp:Literal></div>
                            <div class="consume_input">
                                <div style="padding-left: 12px; float: left;">
                                    <asp:Label ID="lblMinReading" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div id="divfinalMinimumReading" runat="server">
                            <div class="pad_5 clear">
                            </div>
                        </div>
                        <div class="pad_5 clear">
                        </div>
                        <div id="divFinalMeterInformation" runat="server">
                            <h2 class="StepTitle">
                                <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:Resource, METER_INFORMATION%>"></asp:Literal>
                            </h2>
                            <div class="consumer_feild">
                                <div class="consume_name">
                                    <asp:Literal ID="Literal22" runat="server" Text="<%$ Resources:Resource, METER_TYPE%>"></asp:Literal>
                                </div>
                                <div class="consume_input">
                                    <div style="padding-left: 12px; float: left;">
                                        <asp:Label ID="lblMeterConnectionType" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="consume_name">
                                    <asp:Literal ID="Literal23" runat="server" Text="<%$ Resources:Resource, PHASE%>"></asp:Literal>
                                </div>
                                <div class="consume_input">
                                    <div style="padding-left: 12px; float: left;">
                                        <asp:Label ID="lblPhase" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="consume_name">
                                    <asp:Literal ID="Literal25" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Literal></div>
                                <div class="consume_input">
                                    <div style="padding-left: 12px; float: left;">
                                        <asp:Label ID="lblMeterNo" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="pad_5 clear">
                            </div>
                            <div class="consumer_feild">
                            </div>
                            <div class="pad_5 clear">
                            </div>
                            <div class="consumer_feild">
                                <%--<div class="consume_name">
                                <asp:Literal ID="Literal26" runat="server" Text="<%$ Resources:Resource, METER_SERIAL_NUMBER%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <div style="padding-left: 12px; float: left;">
                                    <asp:Label ID="lblMeterSerialNo" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>--%>
                                <div class="consume_name">
                                    <asp:Literal ID="Literal27" runat="server" Text="<%$ Resources:Resource, CONNECTION_DATE%>"></asp:Literal>
                                </div>
                                <div class="consume_input">
                                    <div style="padding-left: 12px; float: left;">
                                        <asp:Label ID="lblConnectionDate" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="consume_name">
                                    <asp:Literal ID="Literal28" runat="server" Text="<%$ Resources:Resource, METER_STATUS%>"></asp:Literal>
                                </div>
                                <div class="consume_input">
                                    <div style="padding-left: 12px; float: left;">
                                        <asp:Label ID="lblMeterStatus" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="consume_name">
                                    <asp:Literal ID="Literal29" runat="server" Text="<%$ Resources:Resource, CONNECTION_REASON%>"></asp:Literal>
                                </div>
                                <div class="consume_input">
                                    <div style="padding-left: 12px; float: left;">
                                        <asp:Label ID="lblConnectionReason" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="pad_5 clear">
                            </div>
                            <div class="consumer_feild">
                                <div class="consume_name">
                                    <asp:Literal ID="Literal30" runat="server" Text="<%$ Resources:Resource, INITIAL_READING%>"></asp:Literal>
                                </div>
                                <div class="consume_input">
                                    <div style="padding-left: 12px; float: left;">
                                        <asp:Label ID="lblInitialReading" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="consume_name">
                                    <asp:Literal ID="Literal32" runat="server" Text="<%$ Resources:Resource, AVERAGE_READING%>"></asp:Literal></div>
                                <div class="consume_input">
                                    <div style="padding-left: 12px; float: left;">
                                        <asp:Label ID="lblAverageReading" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <%--<div class="consume_name">
                                    <asp:Literal ID="Literal33" runat="server" Text="<%$ Resources:Resource, SETUP_DATE%>"></asp:Literal>
                                </div>
                                <div class="consume_input">
                                    <div style="padding-left: 12px; float: left;">
                                        <asp:Label ID="lblReadDate" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>--%>
                                <%-- <div class="consume_name">
                                <asp:Literal ID="Literal31" runat="server" Text="<%$ Resources:Resource, CURRENT_READING%>"></asp:Literal>
                                <span>*</span></div>
                            <div class="consume_input">
                                <div style="padding-left: 12px; float: left;">
                                    <asp:Label ID="lblCurrentReading" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>--%>
                            </div>
                            <div class="pad_5 clear">
                            </div>
                            <div class="consumer_feild">
                                <%-- <div class="consume_name">
                                <asp:Literal ID="Literal34" runat="server" Text="<%$ Resources:Resource, CERTIFIED_BY%>"></asp:Literal>
                                <span>*</span></div>
                            <div class="consume_input">
                                <div style="padding-left: 12px; float: left;">
                                    <asp:Label ID="lblCertifiedBy" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>--%>
                                <div id="divBEDCApplicationFinal" runat="server">
                                    <div class="consumer_feild">
                                        <div class="consume_name">
                                            <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:Resource, INSTALLED_BY%>"></asp:Literal>
                                        </div>
                                        <div class="consume_input">
                                            <asp:Label ID="lblInstallBy" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div id="divOthersFinal" runat="server">
                                    <div class="consumer_feild">
                                        <div class="consume_name">
                                            <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, AGENCY_NAME%>"></asp:Literal>
                                        </div>
                                        <div class="consume_input">
                                            <asp:Label ID="lblAgencyName" runat="server" Text="--"></asp:Label>
                                        </div>
                                        <div class="consume_name">
                                            Contact Person Name</div>
                                        <div class="consume_input">
                                            <asp:Label ID="lblContactPersonName" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clear pad_5">
                                    </div>
                                    <div class="consumer_feild">
                                        <div class="consume_name">
                                            <asp:Literal ID="litDetails" runat="server" Text="<%$ Resources:Resource, DETAILS%>"></asp:Literal>
                                        </div>
                                        <div class="consume_input">
                                            <asp:Label ID="lblDetails" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pad_5">
                            </div>
                        </div>
                    </div>
                    <div class="pad_5">
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
            <ContentTemplate>
                <asp:Button ID="btnStep1" runat="server" Style="display: none;" Text="Step1" OnClick="btnStep1_Click" />
                <asp:Button ID="btnStep2" runat="server" Style="display: none;" Text="Step2" OnClick="btnStep2_Click" />
                <asp:Button ID="btnStep3" runat="server" Style="display: none;" Text="Step3" OnClick="btnStep3_Click" />
                <asp:Button ID="btnFinish" runat="server" Style="display: none;" Text="Finish" OnClick="btnFinish_Click" />
                <asp:HiddenField ID="hfValidStep" runat="server" Value="0" />
                <asp:HiddenField ID="hftxtMeterAmt" runat="server" />
                <asp:HiddenField ID="hfEmbassytxt" runat="server" />
                <asp:HiddenField ID="hfMeterDials" runat="server" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnFinish" />
            </Triggers>
        </asp:UpdatePanel>
        <%--  End content--%>
    </div>
    <asp:ModalPopupExtender ID="mpeMessagePopup" runat="server" TargetControlID="btnpopup"
        PopupControlID="pnlConfirmation" BackgroundCssClass="popbg">
    </asp:ModalPopupExtender>
    <asp:Button ID="btnpopup" runat="server" Text="" Style="display: none;" />
    <asp:Panel runat="server" ID="pnlConfirmation" CssClass="editpopup_two" Style="left: 537px;">
        <div class="panelMessage">
            <div>
                <asp:Panel ID="pnlMsgPopup" runat="server" align="center">
                    <asp:Label ID="lblMsgPopup" runat="server" Text=""></asp:Label>
                </asp:Panel>
            </div>
            <div class="clear pad_5">
            </div>
            <div class="buttondiv">
                <asp:HyperLink ID="hlkOk" NavigateUrl="~/ConsumerManagement/ConsumerRegistration.aspx"
                    runat="server" CssClass="btn_ok" Style="margin-left: 44%;" onclick="NaviagateOk()">OK</asp:HyperLink>
                <%-- <asp:Button ID="btnOk" runat="server" Text="OK" OnClick="btnOk_Click" CssClass="btn_ok"
                                    Style="margin-left: 105px;" />--%>
                <br />
            </div>
        </div>
    </asp:Panel>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);        
    </script>
</asp:Content>
