﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for Consumer Registration
                     
 Developer        : id027-Karthik Thati
 Creation Date    : 17-02-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using System.Threading;
using System.Globalization;
using UMS_NigeriaBE;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using System.Xml;
using System.IO;
using System.Configuration;
using System.Web.Services;
using System.Data;

namespace UMS_Nigeria
{
    public partial class ConsumerRegistration : System.Web.UI.Page
    {
        #region Members

        CommonMethods objCommonMethods = new CommonMethods();
        iDsignBAL objIdsignBal = new iDsignBAL();
        BillCalculatorBAL objBillCalculatorBAL = new BillCalculatorBAL();
        ConsumerBal objConsumerBal = new ConsumerBal();
        string Key = "ConsumerRegistration";


        #endregion

        #region Properties

        private int RouteNo
        {
            get { return string.IsNullOrEmpty(ddlRouteNo.SelectedValue) ? 0 : Convert.ToInt32(ddlRouteNo.SelectedValue); }
        }

        private int RouteSequenceNo
        {
            get { return string.IsNullOrEmpty(txtRouteSequenceNo.Text) ? 0 : Convert.ToInt32(txtRouteSequenceNo.Text); }
        }

        public string IdentityTypeId
        {
            get { return string.IsNullOrEmpty(ddlIdentityType.SelectedValue) ? string.Empty : ddlIdentityType.SelectedValue; }
            set { ddlIdentityType.SelectedValue = value.ToString(); }
        }

        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            //SecurityDongle securityDongle = new SecurityDongle();
            //if (securityDongle.DongleMessage != "Success")
            //{
            //    Response.Redirect("~/ValidateUser.aspx");
            //}

            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.CssClass = lblMessage.Text = string.Empty;

            if (Page.IsPostBack)
            {
                WebControl wcICausedPostBack = (WebControl)GetControlThatCausedPostBack(sender as Page);
                int indx = wcICausedPostBack.TabIndex;
                var ctrl = from control in wcICausedPostBack.Parent.Controls.OfType<WebControl>()
                           where control.TabIndex > indx
                           select control;
                ctrl.DefaultIfEmpty(wcICausedPostBack).First().Focus();
            }

            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = objCommonMethods.GetPagePath(this.Request.Url);
                //if (objCommonMethods.IsAccess(path))
                //{
                    BindDropDowns();
                    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    {
                        ddlBusinessUnit.SelectedIndex = ddlBusinessUnit.Items.IndexOf(ddlBusinessUnit.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                        ddlBusinessUnit.Enabled = false;
                        ddlBusinessUnit_SelectedIndexChanged(ddlBusinessUnit, new EventArgs());
                    }
                    objCommonMethods.BindProcessedBy(rblApplicationProccessedBy, false);
                    //for (int i = 0; i < rblApplicationProccessedBy.Items.Count; i++)
                    //    rblApplicationProccessedBy.Items[i].Attributes.Add("onclick", "InstalledBy()");
                    //rblApplicationProccessedBy.Items[0].Selected = true;
                    //PageAccessPermissions();
                //}
                //else
                //{
                //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                //}
            }
        }

        protected Control GetControlThatCausedPostBack(Page page)
        {
            Control control = null;
            string ctrlname = page.Request.Params.Get("__EVENTTARGET");
            if (ctrlname != null && ctrlname != string.Empty)
            {
                control = page.FindControl(ctrlname);
            }
            else
            {
                foreach (string ctl in page.Request.Form)
                {
                    Control c = page.FindControl(ctl);
                    if (c is System.Web.UI.WebControls.Button || c is System.Web.UI.WebControls.ImageButton)
                    {
                        control = c;
                        break;
                    }
                }
            }
            return control;
        }



        private void PageAccessPermissions()
        {
            try
            {
                //bool IsPageAccess = false;
                //if (objCommonMethods.IsAccess(UMS_Resource.ADD_STATES_PAGE)) { hlkAddState.Visible = true; } else { hlkAddState.Visible = false; }
                //if (objCommonMethods.IsAccess(UMS_Resource.ADD_BUSINESS_UNITS_PAGE)) { hlkAddBU.Visible = true; } else { hlkAddBU.Visible = false; }
                if (objCommonMethods.IsAccess(UMS_Resource.ADD_SERVICE_UNITS_PAGE)) { hlkAddSU.Visible = true; } else { hlkAddSU.Visible = false; }
                if (objCommonMethods.IsAccess(UMS_Resource.ADD_SERVICE_CENTER_PAGE)) { hlkServiceCenter.Visible = true; } else { hlkServiceCenter.Visible = false; }
                if (objCommonMethods.IsAccess(UMS_Resource.ADD_BOOK_NUMBERS_PAGE)) { hlkAddBookNo.Visible = true; } else { hlkAddBookNo.Visible = false; }
                if (objCommonMethods.IsAccess(UMS_Resource.ADD_SUB_STATION_PAGE)) { hlkAddSubStation.Visible = true; } else { hlkAddSubStation.Visible = false; }
                if (objCommonMethods.IsAccess(UMS_Resource.ADD_FEEDER_PAGE)) { hlkAddFeeder.Visible = true; } else { hlkAddFeeder.Visible = false; }
                if (objCommonMethods.IsAccess(UMS_Resource.ADD_TRANSFORMER_PAGE)) { hlkAddTransformer.Visible = true; } else { hlkAddTransformer.Visible = false; }
                if (objCommonMethods.IsAccess(UMS_Resource.ADD_POLE_PAGE)) { hlkAddPole.Visible = true; } else { hlkAddPole.Visible = false; }
                if (objCommonMethods.IsAccess(UMS_Resource.Add_METERTYPE_PAGE)) { hlkAddMeterInfo.Visible = true; } else { hlkAddMeterInfo.Visible = false; }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void BindDropDowns()
        {
            objCommonMethods.BindIdentityTypes(ddlIdentityType, true);
            //objCommonMethods.BindInjectionSubStations(ddlInjectionSubStation, true);
            //objCommonMethods.BindStatesByDistricts(ddlState, Session[UMS_Resource.SESSION_LOGINID].ToString(), string.Empty, true);
            //objCommonMethods.BindStates(ddlState, string.Empty, true);
            objCommonMethods.BindBusinessUnits(ddlBusinessUnit, string.Empty, true);
            //objCommonMethods.BindDistricts(ddlDistrict, string.Empty, true);
            //objCommonMethods.BindStatesByBusinessUnits(ddlState, Session[UMS_Resource.SESSION_LOGINID].ToString(), string.Empty, true);
            objCommonMethods.BindPhases(ddlPhase, true);
            objCommonMethods.BindConnectionReasons(ddlConnectionReason, true);
            objCommonMethods.BindTariffSubTypes(ddlTariff, 0, true);
            objCommonMethods.BindMeterStatus(ddlMeeterStatus, true);
            objCommonMethods.BindMeterTypes(ddlMeterType, true);
            objCommonMethods.BindAccountTypes(ddlAccountType, true);
            objCommonMethods.BindReadCodes(ddlReadCode, true);
            objCommonMethods.BindCustomerTypes(ddlCustomerType, true);
            //ddlCustomerType.SelectedIndex = 1;
            //ddlCustomerType.Enabled = false;
            objCommonMethods.BindAgencies(ddlAgencyName, true);
            objCommonMethods.BindEmployeesList(ddlCertifiedBy, true);
            objCommonMethods.BindEmployeesList(ddlInstalledBy, true);
            objCommonMethods.BindRoutes(ddlRouteNo, (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString(), true);
            //ddlMeeterStatus.SelectedIndex = 1;
            //ddlMeeterStatus.Enabled = false;
            ddlConnectionReason.SelectedIndex = 2;
            ddlConnectionReason.Enabled = false;
        }

        protected void btnFinish_Click(object sender, EventArgs e)
        {
            InsertCustomer();
        }

        protected void btnStep1_Click(object sender, EventArgs e)
        {
            try
            {
                //ddlState_SelectedIndexChanged(null,null);
                //if (ddlTitle.SelectedIndex < 0)
                //{
                //    ddlTitle.Focus();
                //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate Title')", true);
                //}
                //else if (ddlState.SelectedIndex < 0)
                //{
                //    ddlState.Focus();
                //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate State')", true);
                //}
                //else if (ddlDistrict.SelectedIndex < 0)
                //{
                //    ddlDistrict.Focus();
                //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate District')", true);
                //}
                //else if (ddlIdentityType.SelectedIndex < 0)
                //{
                //    ddlIdentityType.Focus();
                //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate Identity Type')", true);
                //}
                //if (!string.IsNullOrEmpty(txtDocumentNo.Text))
                //{
                //    if (Convert.ToBoolean(IsDocumentNoExists(txtDocumentNo.Text)))
                //    {
                //        objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.DOCUMENT_NO_EXISTS_MSG, pnlMessage, lblMessage);
                //    }
                //    else
                //    {
                //        hfValidStep.Value = "1";
                //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "$('#wizard').smartWizard('goToStep',2);", true);
                //    }
                //}
                //else
                //{
                hfValidStep.Value = "1";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "$('#wizard').smartWizard('goToStep',2);", true);
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnStep2_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlBusinessUnit.SelectedIndex < 0)
                {
                    ddlBusinessUnit.Focus();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate Business Unit')", true);
                }
                else if (ddlServiceUnit.SelectedIndex < 0)
                {
                    ddlServiceUnit.Focus();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate Service Unit')", true);
                }
                else if (ddlServiceCenter.SelectedIndex < 0)
                {
                    ddlServiceCenter.Focus();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate Service Center')", true);
                }
                else if (ddlBookNumber.SelectedIndex < 0)
                {
                    ddlBookNumber.Focus();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate Book Number')", true);
                }
                else
                {
                    hfValidStep.Value = "2";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "$('#wizard').smartWizard('goToStep',3);", true);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnStep3_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlTariff.SelectedIndex < 0)
                {
                    ddlTariff.Focus();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate Tariff')", true);
                }
                else if (ddlAccountType.SelectedIndex < 0)
                {
                    ddlAccountType.Focus();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate Account Type')", true);
                }
                else if (ddlReadCode.SelectedIndex < 0)
                {
                    ddlReadCode.Focus();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate Read Code')", true);
                }
                else if (ddlCustomerType.SelectedIndex < 0)
                {
                    ddlCustomerType.Focus();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate Customer Type')", true);
                }
                else if (ddlMeterType.SelectedIndex < 0)
                {
                    ddlMeterType.Focus();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate Meter Type')", true);
                }
                else if (ddlPhase.SelectedIndex < 0)
                {
                    ddlPhase.Focus();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate Phase')", true);
                }
                else if (ddlMeeterStatus.SelectedIndex < 0)
                {
                    ddlMeeterStatus.Focus();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate Meter Status')", true);
                }
                else if (ddlConnectionReason.SelectedIndex < 0)
                {
                    ddlConnectionReason.Focus();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate Connection Reason')", true);
                }
                //else if (ddlCertifiedBy.SelectedIndex < 0)
                //{
                //    ddlCertifiedBy.Focus();
                //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate Certified By')", true);
                //}
                else if (rblApplicationProccessedBy.Items[1].Selected && ddlAgencyName.SelectedIndex < 0)
                {
                    ddlAgencyName.Focus();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate Agency')", true);
                }
                else if (rblApplicationProccessedBy.Items[0].Selected && ddlInstalledBy.SelectedIndex < 0)
                {
                    ddlInstalledBy.Focus();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate InstalledBy')", true);
                }
                else
                {
                    hfValidStep.Value = "3";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "$('#wizard').smartWizard('goToStep',4);", true);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //protected void ddlDistrict_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        objCommonMethods.BindBusinessUnits(ddlBusinessUnit, ddlDistrict.SelectedValue, false);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message,UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}




        #endregion

        #region ddlEvents

        //protected void ddlBusinessUnit_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ddlBusinessUnit.SelectedIndex > 0)
        //        {
        //            objCommonMethods.BindServiceUnits(ddlServiceUnit, ddlBusinessUnit.SelectedValue, true);
        //            ddlServiceUnit.Enabled = true;
        //        }
        //        else
        //        {
        //            ddlServiceUnit.SelectedIndex = ddlServiceCenter.SelectedIndex = ddlBookNumber.SelectedIndex = Constants.Zero;
        //            ddlServiceUnit.Enabled = ddlServiceCenter.Enabled = ddlBookNumber.Enabled = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void ddlServiceUnit_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ddlServiceUnit.SelectedIndex > 0)
        //        {
        //            objCommonMethods.BindServiceCenters(ddlServiceCenter, ddlServiceUnit.SelectedValue, true);
        //            ddlServiceCenter.Enabled = true;
        //        }
        //        else
        //        {
        //            ddlServiceCenter.SelectedIndex = ddlBookNumber.SelectedIndex = Constants.Zero;
        //            ddlServiceCenter.Enabled = ddlBookNumber.Enabled = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void ddlServiceCenter_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ddlServiceCenter.SelectedIndex > 0)
        //        {
        //            //objCommonMethods.BindInjectionSubStations(ddlInjectionSubStation, ddlServiceCenter.SelectedValue, false);
        //            objCommonMethods.BindBookNumbersByServiceDetails(ddlBookNumber, ddlBusinessUnit.SelectedValue, ddlServiceUnit.SelectedValue, ddlServiceCenter.SelectedValue, true);
        //            ddlBookNumber.Enabled = true;
        //        }
        //        else
        //        {
        //            ddlBookNumber.SelectedIndex = Constants.Zero;
        //            ddlBookNumber.Enabled = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ddlState.SelectedIndex > 0)
        //            //objCommonMethods.BindDistricts(ddlDistrict, ddlState.SelectedValue, true);
        //            objCommonMethods.BindBusinessUnits(ddlBusinessUnit, ddlState.SelectedValue, true);
        //        else
        //            //objCommonMethods.BindDistricts(ddlDistrict, string.Empty, true);
        //            objCommonMethods.BindBusinessUnits(ddlBusinessUnit, string.Empty, true);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        protected void ddlBusinessUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlBusinessUnit.SelectedIndex > 0)
                {
                    objCommonMethods.BindUserServiceUnits_BuIds(ddlServiceUnit, ddlBusinessUnit.SelectedValue, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlServiceCenter.SelectedIndex = ddlCycle.SelectedIndex = ddlBookNumber.SelectedIndex = 0;
                    ddlServiceUnit.Enabled = true;
                    ddlServiceCenter.Enabled = ddlCycle.Enabled = ddlBookNumber.Enabled = false;

                }
                else
                {
                    ddlServiceUnit.SelectedIndex = ddlServiceCenter.SelectedIndex = ddlCycle.SelectedIndex = ddlBookNumber.SelectedIndex = 0;
                    ddlServiceUnit.Enabled = ddlServiceCenter.Enabled = ddlCycle.Enabled = ddlBookNumber.Enabled = false;
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlServiceUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlServiceUnit.SelectedIndex > 0)
                {
                    objCommonMethods.BindUserServiceCenters_SuIds(ddlServiceCenter, ddlServiceUnit.SelectedValue, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlServiceCenter.SelectedIndex = ddlCycle.SelectedIndex = ddlBookNumber.SelectedIndex = 0;
                    ddlServiceCenter.Enabled = true;
                    ddlCycle.Enabled = ddlBookNumber.Enabled = false;
                }
                else
                {
                    ddlServiceCenter.SelectedIndex = ddlCycle.SelectedIndex = ddlBookNumber.SelectedIndex = 0;
                    ddlServiceCenter.Enabled = ddlCycle.Enabled = ddlBookNumber.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlServiceCenter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlServiceCenter.SelectedIndex > 0)
                {
                    objCommonMethods.BindCyclesByBUSUSC(ddlCycle, ddlBusinessUnit.SelectedValue, ddlServiceUnit.SelectedValue, ddlServiceCenter.SelectedValue, true, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlCycle.SelectedIndex = ddlBookNumber.SelectedIndex = 0;
                    ddlCycle.Enabled = true;
                    ddlBookNumber.Enabled = false;
                }

                else
                {
                    ddlCycle.SelectedIndex = ddlBookNumber.SelectedIndex = 0;
                    ddlCycle.Enabled = ddlBookNumber.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlCycle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCycle.SelectedIndex > 0)
            {
                objCommonMethods.BindAllBookNumbers(ddlBookNumber, ddlCycle.SelectedValue, true, false, UMS_Resource.DROPDOWN_SELECT);
                ddlBookNumber.Enabled = true;
            }

            else
            {
                ddlBookNumber.SelectedIndex = 0;
                ddlBookNumber.Enabled = false;
            }
        }

        protected void ddlInjectionSubStation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlInjectionSubStation.SelectedIndex > 0)
                {
                    objCommonMethods.BindFeeders(ddlFeeder, ddlInjectionSubStation.SelectedValue, UMS_Resource.DROPDOWN_SELECT, true);
                    ddlFeeder.Enabled = true;
                }
                else
                {
                    ddlFeeder.SelectedIndex = ddlTransformer.SelectedIndex = ddlPole.SelectedIndex = Constants.Zero;
                    ddlFeeder.Enabled = ddlTransformer.Enabled = ddlPole.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlFeeder_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlFeeder.SelectedIndex > 0)
                {
                    objCommonMethods.BindTransformers(ddlTransformer, ddlFeeder.SelectedValue, true);
                    ddlTransformer.Enabled = true;
                }
                else
                {
                    ddlTransformer.SelectedIndex = ddlPole.SelectedIndex = Constants.Zero;
                    ddlTransformer.Enabled = ddlPole.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlTransformer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlTransformer.SelectedIndex > 0)
                {
                    objCommonMethods.BindPoles(ddlPole, ddlTransformer.SelectedValue, true);
                    ddlPole.Enabled = true;
                }
                else
                {
                    ddlPole.SelectedIndex = Constants.Zero;
                    ddlPole.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlReadCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlReadCode.SelectedIndex == 1)
                {
                    divMeterInformation.Visible = divFinalMeterInformation.Visible = false;
                    divMinReading.Visible = divfinalMinimumReading.Visible = true;
                }
                else
                {
                    divMeterInformation.Visible = divFinalMeterInformation.Visible = true;
                    divMinReading.Visible = divfinalMinimumReading.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods
        private void BindTariffDetails()
        {
            try
            {
                XmlDocument xml = null;
                BillCalculatorBE objBillCalculatorBe = new BillCalculatorBE();
                BillCalculatorListCharges objBillCalculatorListChargesBe = new BillCalculatorListCharges();
                BillCalculatorListBE objBillCalculatorListBE = new BillCalculatorListBE();
                TimeZoneInfo IND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                DateTime currentTime = DateTime.Now;
                DateTime ourtime = TimeZoneInfo.ConvertTime(currentTime, IND_ZONE);

                objBillCalculatorBe.Date = ourtime.ToString("MM/dd/yyyy").Replace("-", "/");
                objBillCalculatorBe.Consumption = 1;
                objBillCalculatorBe.SubType = Convert.ToInt32(ddlTariff.SelectedValue);
                // _ObjBillCalculatorBE.Vat = Vat;
                objBillCalculatorBe.CustomerTypeId = Constants.ConsumptionTypeId;

                xml = objBillCalculatorBAL.CalculateBillWithConsumption(objBillCalculatorBe, ReturnType.Get);
                objBillCalculatorListBE = objIdsignBal.DeserializeFromXml<BillCalculatorListBE>(xml);
                objBillCalculatorListChargesBe = objIdsignBal.DeserializeFromXml<BillCalculatorListCharges>(xml);

                if (objBillCalculatorListChargesBe.items.Count > 0)
                {
                    lblTariffType.Text = objBillCalculatorListBE.items[0].TariffType;
                    divTariff.Visible = true;
                }
                gvTariffDetails.DataSource = objBillCalculatorListChargesBe.items;
                gvTariffDetails.DataBind();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void InsertCustomer()
        {
            try
            {
                string fileInput = string.Empty;
                ConsumerBe objConsumerBe = new ConsumerBe();

                if (Request.Files.Count > 0)
                {
                    for (int z = 0; z < Request.Files.Count; z++)//looping to save attachments
                    {
                        HttpPostedFile PostedFile = Request.Files[z];//Assigning to variable to Access File
                        if (PostedFile.ContentLength != 0)//Checking the size of file is more than 0 bytes
                        {
                            string FileName = Path.GetFileName(PostedFile.FileName).ToString();//Assinging FileName
                            string Extension = Path.GetExtension(FileName);//Storing Extension of file into variable Extension
                            TimeZoneInfo IND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                            DateTime currentTime = DateTime.Now;
                            DateTime ourtime = TimeZoneInfo.ConvertTime(currentTime, IND_ZONE);
                            string CurrentDate = ourtime.ToString("dd_MM_yyyy_hh_mm_ss");
                            string ModifiedFileName = Path.GetFileNameWithoutExtension(FileName) + "_" + CurrentDate + Extension;//Setting File Name to store it in database
                            PostedFile.SaveAs(Server.MapPath(ConfigurationManager.AppSettings[UMS_Resource.KEY_CUSTOMER_DOCUMENTS].ToString()) + ModifiedFileName);//Saving File in to the server.
                            fileInput = string.IsNullOrEmpty(fileInput) ? ModifiedFileName : fileInput + "," + ModifiedFileName;
                        }
                    }
                    objConsumerBe.CustomerDocumentNames = fileInput;
                    objConsumerBe.CustomerDocumentPath = ConfigurationManager.AppSettings[UMS_Resource.KEY_CUSTOMER_DOCUMENTS].ToString().Replace("~", "..");
                }

                decimal TariffMultiplier = string.IsNullOrEmpty(txtTariffMultiplier.Text) ? 0 : decimal.Parse(txtTariffMultiplier.Text);
                string ApplicationDate = string.IsNullOrEmpty(txtApplicationDate.Text) ? "" : objCommonMethods.Convert_MMDDYY(txtApplicationDate.Text);
                int MeterStatusId = string.IsNullOrEmpty(ddlMeeterStatus.SelectedValue) ? 0 : Convert.ToInt32(ddlMeeterStatus.SelectedValue);
                if (ddlTitle.SelectedIndex > 0)
                    objConsumerBe.Title = ddlTitle.SelectedItem.Text;
                objConsumerBe.Name = txtName.Text;
                objConsumerBe.SurName = txtSurName.Text;
                objConsumerBe.KnownAs = txtKnownAs.Text;
                objConsumerBe.EmailId = txtEmailId.Text;
                objConsumerBe.DocumentNo = txtDocumentNo.Text;
                objConsumerBe.EmployeeCode = txtEmployeeCode.Text;
                objConsumerBe.HomeContactNo = txtHome.Text;
                objConsumerBe.BusinessContactNo = txtBussiness.Text;
                objConsumerBe.OtherContactNo = txtOthers.Text;
                //objConsumerBe.DistrictCode = ddlDistrict.SelectedValue;
                //objConsumerBe.StateCode = ddlState.SelectedValue;
                objConsumerBe.PostalLandMark = txtLandMark.Text;
                objConsumerBe.PostalStreet = txtStreet.Text;
                objConsumerBe.PostalCity = txtCity.Text;
                objConsumerBe.PostalHouseNo = txtHouseNo.Text;
                objConsumerBe.PostalDetails = txtPostalDetails.Text;
                objConsumerBe.PostalZipCode = txtZipCode.Text;
                objConsumerBe.IdentityNo = txtIdentityNumber.Text;
                objConsumerBe.IdentityTypeId = string.IsNullOrEmpty(ddlIdentityType.SelectedValue) ? 0 : Convert.ToInt32(ddlIdentityType.SelectedValue);// Convert.ToInt32(IdentityTypeId);
                objConsumerBe.BU_ID = ddlBusinessUnit.SelectedValue;
                objConsumerBe.SU_ID = ddlServiceUnit.SelectedValue;
                objConsumerBe.ServiceCenterId = ddlServiceCenter.SelectedValue;
                objConsumerBe.BookNo = ddlBookNumber.SelectedValue;
                objConsumerBe.SubStationId = ddlInjectionSubStation.SelectedValue;
                objConsumerBe.FeederId = ddlFeeder.SelectedValue;
                objConsumerBe.TransFormerId = ddlTransformer.SelectedValue;
                objConsumerBe.PoleId = ddlPole.SelectedValue;
                objConsumerBe.ServiceLandMark = txtServiceLandMark.Text;
                objConsumerBe.ServiceStreet = txtServiceStreet.Text;
                objConsumerBe.ServiceCity = txtServiceCity.Text;
                objConsumerBe.ServiceHouseNo = txtServiceHouseNo.Text;
                objConsumerBe.ServiceDetails = txtServiceDetails.Text;
                objConsumerBe.ServiceZipCode = txtServiceZipCode.Text;
                objConsumerBe.ServiceEmailId = txtServiceEmailId.Text;
                objConsumerBe.MobileNo = txtMobileNo.Text;
                //objConsumerBe.CustomerSequenceNo =Convert.ToInt32( txtCustomerSequenceNo.Text);
                objConsumerBe.RouteSequenceNo = RouteSequenceNo;
                objConsumerBe.RouteNo = RouteNo;
                objConsumerBe.NeighborAccountNo = txtneighbor.Text;
                objConsumerBe.Latitude = txtLatitude.Text;
                objConsumerBe.Longitude = txtLongitude.Text;
                objConsumerBe.TariffId = Convert.ToInt32(ddlTariff.SelectedValue);
                objConsumerBe.MultiplicationFactor = TariffMultiplier;
                objConsumerBe.AccountTypeId = Convert.ToInt32(ddlAccountType.SelectedValue);
                objConsumerBe.ApplicationDate = ApplicationDate;
                objConsumerBe.ReadCodeId = Convert.ToInt32(ddlReadCode.SelectedValue);
                objConsumerBe.OrganizationCode = txtOrganizationCode.Text;
                objConsumerBe.CustomerTypeId = Convert.ToInt32(ddlCustomerType.SelectedValue);
                objConsumerBe.OldAccountNo = txtOldAccountNo.Text;
                objConsumerBe.IsEmbassyCustomer = chkIsEmbassyCustomer.Checked;
                objConsumerBe.EmbassyCode = hfEmbassytxt.Value = txtEmbassyCode.Text;
                //objConsumerBe.IsCapnyMeter = cbIsCapnyMeter.Checked;
                if (txtMeterAmt.Text != "")
                    objConsumerBe.MeterAmt = Convert.ToDecimal(txtMeterAmt.Text);
                objConsumerBe.IsVIPCustomer = chkIsVipCustomer.Checked;
                if (ddlReadCode.SelectedIndex != 1)
                {
                    objConsumerBe.MeterTypeId = Convert.ToInt32(ddlMeterType.SelectedValue);
                    objConsumerBe.PhaseId = Convert.ToInt32(ddlPhase.SelectedValue);
                    objConsumerBe.MeterNo = txtMeterNo.Text;
                    objConsumerBe.MeterSerialNo = txtMeeterSerialNo.Text;
                    objConsumerBe.ConnectionDate = objCommonMethods.Convert_MMDDYY(txtConnectionDate.Text);
                    objConsumerBe.MeterStatusId = MeterStatusId;
                    objConsumerBe.ConnectionReasonId = Convert.ToInt32(ddlConnectionReason.SelectedValue);
                    objConsumerBe.InitialReading = txtInitialReading.Text;
                    //objConsumerBe.CurrentReading = txtCurrentReading.Text;
                    objConsumerBe.AverageReading = txtAverageReading.Text;
                    //objConsumerBe.SetupDate = objCommonMethods.Convert_MMDDYY(txtSetupDate.Text);
                    objConsumerBe.CertifiedBy = ddlCertifiedBy.SelectedValue;
                    //objConsumerBe.CertifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objConsumerBe.ProcessedById = Convert.ToInt32(rblApplicationProccessedBy.SelectedValue);
                    if (rblApplicationProccessedBy.Items[0].Selected)
                    {
                        objConsumerBe.InstalledBy = ddlInstalledBy.SelectedValue;//
                        // objConsumerBe.InstalledBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    }
                    else
                    {
                        objConsumerBe.AgencyId = Convert.ToInt32(ddlAgencyName.SelectedValue);
                        objConsumerBe.ContactPersonName = txtContactPersonName.Text;
                        objConsumerBe.AgencyDetails = objIdsignBal.ReplaceNewLines(txtAgencyDetails.Text, true);
                    }
                }
                if (ddlReadCode.SelectedIndex == 1)
                {
                    objConsumerBe.MinimumReading = txtMinReading.Text;
                    objConsumerBe.MeterNo = objConsumerBe.MeterSerialNo = objConsumerBe.ConnectionDate = objConsumerBe.InitialReading = objConsumerBe.SetupDate = string.Empty;
                    objConsumerBe.ContactPersonName = objConsumerBe.AgencyDetails = string.Empty;
                    objConsumerBe.PhaseId = objConsumerBe.MeterTypeId = objConsumerBe.MeterStatusId = objConsumerBe.ConnectionReasonId = objConsumerBe.ProcessedById = objConsumerBe.AgencyId = 0;
                    objConsumerBe.CertifiedBy = objConsumerBe.InstalledBy = string.Empty;
                }
                objConsumerBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Insert(objConsumerBe, Statement.Insert));

                if (objConsumerBe.IsDocumentNoExists)
                {
                    objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.DOCUMENT_NO_EXISTS_MSG, pnlMessage, lblMessage);
                    GoToStepOne();
                }
                else if (objConsumerBe.IsCustomerSequenceNo)
                {
                    objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.CUSTOMER_SEQUENCE_NO_EXISTS_MSG, pnlMessage, lblMessage);
                    GoToStepOne();
                }
                else if (objConsumerBe.IsNeighborAccountNotExists)
                {
                    objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.NEIGHBOUR_ACC_NOT_EXISTS, pnlMessage, lblMessage);
                }
                else if (objConsumerBe.IsMeterNumberNotExists)
                {
                    objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.METER_NO_NOT_EXISTS, pnlMessage, lblMessage);
                }
                else if (objConsumerBe.IsOldAccountNoExists)
                {
                    Message(Resource.CUSTOMER_OLD_ACCNO_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (objConsumerBe.IsMeterNoExists)
                {
                    Message(UMS_Resource.CUSTOMER_METERNO_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (objConsumerBe.IsOldAccountNotValid)
                {
                    Message(Resource.CUSTOMER_OLD_ACCNO_NOT_VALID, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (!string.IsNullOrEmpty(objConsumerBe.AccountNo))
                {
                    //objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_SUCCESS, UMS_Resource.CUSTOMER_REGISTER_SUCCESS, pnlMessage, lblMessage);
                    GoToStepOne();
                    ScriptManager.RegisterStartupScript(UpdatePanel4, UpdatePanel4.GetType(), "resetPage", "$(window).unbind('beforeunload');", true);
                    objCommonMethods.ClearInputs(Page.Controls);
                    lblMsgPopup.Text = string.Format(UMS_Resource.CUSTOMER_REGISTER_SUCCESS, "<span style='color:#0078C5;'>" + objConsumerBe.AccountNo + "</span>");
                    mpeMessagePopup.Show();
                }
                else if (objConsumerBe.MeterDials > 0)
                {
                    Message(UMS_Resource.CUSTOMER_INITIAL_READING_NOT_CORRECT + objConsumerBe.MeterDials + " Characters", UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                    objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.CUSTOMER_REGISTER_FAIL, pnlMessage, lblMessage);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void GoToStepOne()
        {
            hfValidStep.Value = "0";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "$('#wizard').smartWizard('goToStep',1);", true);
        }

        protected override void InitializeCulture()
        {
            if (Session[UMS_Resource.CULTURE] != null)
            {
                string culture = Session[UMS_Resource.CULTURE].ToString();

                Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
            }
            base.InitializeCulture();
        }

        #endregion

        #region WebMethods

        

        protected void ddlTariff_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlTariff.SelectedIndex > 0)
                    BindTariffDetails();
                else
                {
                    divTariff.Visible = false;
                    gvTariffDetails.DataSource = new DataTable();
                    gvTariffDetails.DataBind();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //[WebMethod]
        //public static string IsDocumentNoExists(string DocumentNo)
        //{            
        //    ConsumerBe objConsumerBe = new ConsumerBe();
        //    iDsignBAL objIdsignBal = new iDsignBAL();
        //    ConsumerBal objConsumerBal = new ConsumerBal();

        //    objConsumerBe.DocumentNo = DocumentNo;
        //    objConsumerBe.CustomerUniqueNo = string.Empty;
        //    objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.GetServiceDetails(objConsumerBe, ReturnType.Retrieve));
        //    return objConsumerBe.IsDocumentNoExists.ToString();
        //}

        [WebMethod]
        public static string IsSequenceNoExists(string SequenceNo)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            iDsignBAL objIdsignBal = new iDsignBAL();
            ConsumerBal objConsumerBal = new ConsumerBal();

            objConsumerBe.CustomerSequenceNo = Convert.ToInt32(SequenceNo);
            objConsumerBe.CustomerUniqueNo = string.Empty;
            objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.GetServiceDetails(objConsumerBe, ReturnType.Set));
            return objConsumerBe.IsCustomerSequenceNo.ToString();
        }


        [WebMethod]
        public static string IsMeterNoExists(string MeterNo)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            iDsignBAL objIdsignBal = new iDsignBAL();
            ConsumerBal objConsumerBal = new ConsumerBal();

            objConsumerBe.MeterNo = MeterNo;
            objConsumerBe.CustomerUniqueNo = string.Empty;
            objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Retrieve(objConsumerBe, ReturnType.Get));
            int MeterDials = objConsumerBe.MeterDials;
            return (objConsumerBe.IsMeterNoExists.ToString());
        }

        [WebMethod]
        public static string MeterDials(string MeterNo)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            iDsignBAL objIdsignBal = new iDsignBAL();
            ConsumerBal objConsumerBal = new ConsumerBal();

            objConsumerBe.MeterNo = MeterNo;
            objConsumerBe.CustomerUniqueNo = string.Empty;
            objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Retrieve(objConsumerBe, ReturnType.Get));
            return (objConsumerBe.MeterDials.ToString());
        }

        [WebMethod]
        public static string IsMeterSerialNoExists(string MeterSerialNo)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            iDsignBAL objIdsignBal = new iDsignBAL();
            ConsumerBal objConsumerBal = new ConsumerBal();

            objConsumerBe.MeterSerialNo = MeterSerialNo;
            objConsumerBe.CustomerUniqueNo = string.Empty;
            objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Retrieve(objConsumerBe, ReturnType.Single));
            return objConsumerBe.IsMeterSerialNoExists.ToString();
        }

        [WebMethod]
        public static string IsNeighborAccountNoExists(string NeighbourAccNo)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            iDsignBAL objIdsignBal = new iDsignBAL();
            ConsumerBal objConsumerBal = new ConsumerBal();

            objConsumerBe.NeighborAccountNo = NeighbourAccNo;
            objConsumerBe.CustomerUniqueNo = string.Empty;
            objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Retrieve(objConsumerBe, ReturnType.Fetch));
            return objConsumerBe.IsNeighborAccountNoExists.ToString();
        }

        [WebMethod]
        public static string IsOldAccountNoExists(string OldAccountNo)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            iDsignBAL objIdsignBal = new iDsignBAL();
            ConsumerBal objConsumerBal = new ConsumerBal();

            objConsumerBe.OldAccountNo = OldAccountNo;
            objConsumerBe.CustomerUniqueNo = string.Empty;
            objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Retrieve(objConsumerBe, ReturnType.Bulk));
            return objConsumerBe.IsOldAccountNoExists.ToString();
        }

        protected void rblApplicationProccessedBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblApplicationProccessedBy.Items[1].Selected)
            {
                divOthersFinal.Visible = divApplicationByOthers.Visible = true;
                divBEDCApplicationFinal.Visible = divBEDCApplication.Visible = false;


            }
            else
            {
                divOthersFinal.Visible = divApplicationByOthers.Visible = false;
                divBEDCApplicationFinal.Visible = divBEDCApplication.Visible = true;
            }
        }

        protected void chkIsPaidMeter_CheckedChanged(object sender, EventArgs e)
        {
            if (cbIsPaidMeter.Checked)
            {
                divPaidMeter.Visible = true;
            }
            else
            {
                divPaidMeter.Visible = false;
            }
        }

        //protected void lbtnRefreshStep1_Click(object sender, EventArgs e)
        //{
        //    //objCommonMethods.BindStatesByDistricts(ddlState, Session[UMS_Resource.SESSION_LOGINID].ToString(), string.Empty, true);
        //    //objCommonMethods.BindDistricts(ddlDistrict, string.Empty, true);
        //}

        protected void lbtnRefreshStep2_Click(object sender, EventArgs e)
        {
            //objCommonMethods.BindBusinessUnits(ddlBusinessUnit, string.Empty, true);
            //objCommonMethods.BindBusinessUnits(ddlBusinessUnit, ddlState.SelectedValue, true);
            ddlServiceUnit.SelectedIndex = ddlServiceCenter.SelectedIndex = ddlBookNumber.SelectedIndex = Constants.Zero;
            objCommonMethods.BindInjectionSubStations(ddlInjectionSubStation, true);
            ddlFeeder.SelectedIndex = ddlTransformer.SelectedIndex = ddlPole.SelectedIndex = Constants.Zero;
        }

        #endregion
    }

}