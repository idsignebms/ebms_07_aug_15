﻿<%@ Page Title=":: Change Customer Book No ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="CustomerBookNoChange.aspx.cs"
    Inherits="UMS_Nigeria.ConsumerManagement.CustomerBookNoChange" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="head" runat="server">
    <link href="../App_Themes/Green/style.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function OnOffTenentDiv() {
            if (document.getElementById("UMSNigeriaBody_cbIsSameAsPostal").checked) {
                document.getElementById("UMSNigeriaBody_divServiceAdrsOriginal").style.display = "none";
                document.getElementById("UMSNigeriaBody_divServiceAdrsDuplicate").style.display = "block";
                //                var chkBoxList = document.getElementById("UMSNigeriaBody_rblCommunicationAddress");
                //                chkBoxList.getElementsByTagName("input")[0].checked = true;
                //                chkBoxList.getElementsByTagName("input")[1].disabled = true;
                var rdoCommunicationServ = document.getElementById("UMSNigeriaBody_rdoCommunicationServ");
                rdoCommunicationServ.getElementsByTagName("input")[0].checked = true;
                rdoCommunicationServ.getElementsByTagName("input")[1].disabled = true;
            }
            else {
                document.getElementById("UMSNigeriaBody_divServiceAdrsOriginal").style.display = "block";
                document.getElementById("UMSNigeriaBody_divServiceAdrsDuplicate").style.display = "none";
                //document.getElementById("UMSNigeriaBody_rblCommunicationAddress");
                //                var chkBoxList = document.getElementById("UMSNigeriaBody_rblCommunicationAddress");
                //                chkBoxList.getElementsByTagName("input")[0].checked = true;
                //                chkBoxList.getElementsByTagName("input")[1].checked = false;
                //                chkBoxList.getElementsByTagName("input")[1].disabled = false;
                //                var rdoCommunicationServ = document.getElementById("UMSNigeriaBody_rdoCommunicationServ");
                //                rdoCommunicationServ.getElementsByTagName("input")[0].checked = false;
                //                rdoCommunicationServ.getElementsByTagName("input")[1].checked = false;
                //                rdoCommunicationServ.getElementsByTagName("input")[1].disabled = false;
            }
            OnOfIsSameAsServiceAddress();
        }
        function OnOffTenentDivRev() {
            //            alert("Workign");
            if (!document.getElementById("UMSNigeriaBody_cbIsSameAsPostal").checked) {
                document.getElementById("UMSNigeriaBody_divServiceAdrsOriginal").style.display = "none";
                document.getElementById("UMSNigeriaBody_divServiceAdrsDuplicate").style.display = "block";
                var chkBoxList = document.getElementById("UMSNigeriaBody_rblCommunicationAddress");
                //                chkBoxList.getElementsByTagName("input")[0].checked = false;
                //                chkBoxList.getElementsByTagName("input")[1].checked = false;
                chkBoxList.getElementsByTagName("input")[1].disabled = false;
                //                var rdoCommunicationServ = document.getElementById("UMSNigeriaBody_rdoCommunicationServ");
                //                rdoCommunicationServ.getElementsByTagName("input")[0].checked = true;
                //                rdoCommunicationServ.getElementsByTagName("input")[1].checked = false;
                //                rdoCommunicationServ.getElementsByTagName("input")[1].disabled = false;
            }
            else if (document.getElementById("UMSNigeriaBody_cbIsSameAsPostal").checked) {
                document.getElementById("UMSNigeriaBody_divServiceAdrsOriginal").style.display = "block";
                document.getElementById("UMSNigeriaBody_divServiceAdrsDuplicate").style.display = "none";
                //                var chkBoxList = document.getElementById("UMSNigeriaBody_rblCommunicationAddress");
                //                chkBoxList.getElementsByTagName("input")[0].checked = true;
                //                chkBoxList.getElementsByTagName("input")[1].disabled = true;
                //                var rdoCommunicationServ = document.getElementById("UMSNigeriaBody_rdoCommunicationServ");
                //                rdoCommunicationServ.getElementsByTagName("input")[0].checked = true;
                //                rdoCommunicationServ.getElementsByTagName("input")[1].disabled = true;
            }
            OnOfIsSameAsServiceAddress();
        }
        function OnOfIsSameAsServiceAddress() {
            //            debugger;
            if ((document.getElementById("UMSNigeriaBody_hfServiceAddressID").value).length > 0) {
                if (document.getElementById("UMSNigeriaBody_cbIsSameAsPostal").checked) {
                    document.getElementById("UMSNigeriaBody_txtHouseNoServ").value = document.getElementById("UMSNigeriaBody_txtPHNo").value;
                    document.getElementById("UMSNigeriaBody_txtStreetNameServ").value = document.getElementById("UMSNigeriaBody_txtPStreet").value;
                    document.getElementById("UMSNigeriaBody_txtCityServ").value = document.getElementById("UMSNigeriaBody_txtPLGAVillage").value;
                    document.getElementById("UMSNigeriaBody_txtZipServ").value = document.getElementById("UMSNigeriaBody_txtPZip").value;
                    document.getElementById("UMSNigeriaBody_txtAreaServ").value = document.getElementById("UMSNigeriaBody_txtPArea").value;
                    document.getElementById("UMSNigeriaBody_spnServericeAreaServ").value = document.getElementById("UMSNigeriaBody_spnPostalArea").value;
                }
            }
            else {
                if (document.getElementById("UMSNigeriaBody_cbIsSameAsPostal").checked) {
                    document.getElementById("UMSNigeriaBody_txtSHNo").value = document.getElementById("UMSNigeriaBody_txtPHNo").value;
                    document.getElementById("UMSNigeriaBody_txtSStreet").value = document.getElementById("UMSNigeriaBody_txtPStreet").value;
                    document.getElementById("UMSNigeriaBody_txtSLGAVillage").value = document.getElementById("UMSNigeriaBody_txtPLGAVillage").value;
                    document.getElementById("UMSNigeriaBody_txtSZip").value = document.getElementById("UMSNigeriaBody_txtPZip").value;
                    document.getElementById("UMSNigeriaBody_txtSArea").value = document.getElementById("UMSNigeriaBody_txtPArea").value;
                    document.getElementById("UMSNigeriaBody_spnServericeArea").value = document.getElementById("UMSNigeriaBody_spnPostalArea").value;
                }
            }
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upSearchCustomer" DefaultButton="btnSearch" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div class="inner-sec">
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litSearchCustomer" runat="server" Text="<%$ Resources:Resource, CHNG_CUST_BOOK%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal20" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litAccountNo" runat="server" Text="<%$ Resources:Resource, ACCNO_OLD_ACCNO%>"></asp:Literal>
                                    <span class="span_star">*</span></label>
                                <div class="space">
                                </div>
                                <asp:TextBox ID="txtAccountNo" CssClass="text-box" runat="server" MaxLength="15"
                                    placeholder="Enter Global Account No/Old Account No" onblur="return TextBoxBlurValidationlbl(this,'spanAccNo','Global Account No/Old Account No')"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtAccountNo"
                                    FilterType="Numbers" ValidChars=" ">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanAccNo" class="span_color"></span>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box1">
                        <div class="search">
                            <asp:Button ID="btnGo" OnClientClick="return Validate();" Text="<%$ Resources:Resource, GET_CUSTOMER_DETAILS%>"
                                CssClass="box_s" OnClick="btnGo_Click" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="out-bor_a" id="divdetails" visible="false" runat="server">
                    <div class="inner-sec">
                        <div class="heading">
                            <asp:Literal ID="Literal213" runat="server" Text="<%$ Resources:Resource, CUSTOMER_DETAILS%>"></asp:Literal>
                        </div>
                        <div id="DivBasicDetails" class="out-bor_cTwo">
                            <div class="inner-box">
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblGlobalAccNoAndAccNo" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lblGlobalAccountNO" Visible="false" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblOldAccNo" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblName" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <%--<div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal29" runat="server" Text="<%$ Resources:Resource, ACC_NO%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblAccountNo" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>--%>
                                <div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblTariff" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblMeterNo" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="litOutstandingAmount" runat="server" Text="<%$ Resources:Resource, OUT_STANDING_AMT%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblOutstandingAmount" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="litExistingBook" runat="server" Text="Existing Book"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblExistingBook" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="Div1">
                            <h4 class="subHeading">
                                <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:Resource, CUSTOMER_DETAILS%>"></asp:Literal>
                            </h4>
                            <div class="inner-sec">
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal><span
                                                class="span_star">*</span></label>
                                        <div class="space">
                                        </div>
                                        <asp:DropDownList ID="ddlBU" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlBU_SelectedIndexChanged"
                                            onchange="DropDownlistOnChangelbl(this,'spanBU','Business Unit')" CssClass="text-box text-select">
                                            <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                                        </asp:DropDownList>
                                        <div class="space">
                                        </div>
                                        <span id="spanBU" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box2">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litServiceUnit" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal><span
                                                class="span_star">*</span></label>
                                        <div class="space">
                                        </div>
                                        <asp:DropDownList ID="ddlSU" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlSU_SelectedIndexChanged"
                                            onchange="DropDownlistOnChangelbl(this,'spanSU','Service Unit')" CssClass="text-box text-select">
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                        <div class="space">
                                        </div>
                                        <span id="spanSU" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box3">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litServiceCenter" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal><span
                                                class="span_star">*</span></label>
                                        <div class="space">
                                        </div>
                                        <asp:DropDownList ID="ddlSC" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSC_SelectedIndexChanged"
                                            onchange="DropDownlistOnChangelbl(this,'spanSC','Service Center')" CssClass="text-box text-select">
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                        <div class="space">
                                        </div>
                                        <span id="spanSC" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litCycles" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal><span
                                                class="span_star">*</span></label>
                                        <div class="space">
                                        </div>
                                        <asp:DropDownList ID="ddlCycle" AutoPostBack="true" OnSelectedIndexChanged="ddlCycle_SelectedIndexChanged"
                                            runat="server" CssClass="text-box text-select" onchange="DropDownlistOnChangelbl(this,'spanCycle','Book Group')">
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                        <div class="space">
                                        </div>
                                        <span id="spanCycle" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box2">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litBookNo" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal><span
                                                class="span_star">*</span></label>
                                        <div class="space">
                                        </div>
                                        <%--OnSelectedIndexChanged="ddlBookNo_SelectedIndexChanged"--%>
                                        <asp:DropDownList ID="ddlBookNo" runat="server" AutoPostBack="true" CssClass="text-box text-select"
                                            onchange="DropDownlistOnChangelbl(this,'spanBook','Book No')">
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                        <div class="space">
                                        </div>
                                        <span id="spanBook" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box3">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal31" runat="server" Text="<%$ Resources:Resource, REASON%>"></asp:Literal><span
                                                class="span_star">*</span></label>
                                        <div class="space">
                                        </div>
                                        <asp:TextBox ID="txtBookReason" CssClass="text-box" runat="server" TextMode="MultiLine"
                                            placeholder="Enter Reason for changing" onblur="return TextBoxBlurValidationlbl(this,'spanBookReason','Reason')"></asp:TextBox><br />
                                        <span id="spanBookReason" class="span_color"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="DivNewUI">
                            <%-------------------------Postal Information Start----------------------%>
                            <asp:UpdatePanel ID="updtPnlPostalAddress" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="clr">
                                    </div>
                                    <h4 class="subHeading">
                                        <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, POSTAL_ADDRESS%>"></asp:Literal>
                                    </h4>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litPHNo" runat="server" Text="<%$ Resources:Resource, H_NO%>"></asp:Literal></label>
                                            <span class="span_star">*</span><br />
                                            <asp:TextBox ID="txtPHNo" runat="server" CssClass="text-box" placeholder="Enter House No"
                                                MaxLength="10" onkeyup="OnOfIsSameAsServiceAddress();" onblur="return TextBoxBlurValidationlblZeroAccepts(this,'spanPHNo','House No')"></asp:TextBox>
                                            <span id="spanPHNo" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box2">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litPStreet" runat="server" Text="<%$ Resources:Resource, STREET%>"></asp:Literal>
                                            </label>
                                            <span class="span_star">*</span><br />
                                            <asp:TextBox ID="txtPStreet" runat="server" onkeyup="OnOfIsSameAsServiceAddress();"
                                                onblur="return TextBoxBlurValidationlbl(this,'spanPStreet','Street Name')" CssClass="text-box"
                                                placeholder="Enter Street Name" MaxLength="255"></asp:TextBox>
                                            <span id="spanPStreet" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box3">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litPLGAVillage" runat="server" Text="<%$ Resources:Resource, CITY%>"></asp:Literal></label><span
                                                    class="span_star">*</span><br />
                                            <asp:TextBox ID="txtPLGAVillage" runat="server" onkeyup="OnOfIsSameAsServiceAddress();"
                                                onblur="return TextBoxBlurValidationlbl(this,'spanPLGAVillage','City')" CssClass="text-box"
                                                placeholder="Enter City" MaxLength="50"></asp:TextBox>
                                            <span id="spanPLGAVillage" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litPZip" runat="server" Text="<%$ Resources:Resource, ZIP%>"></asp:Literal>
                                            </label>
                                            <%--<span class="span_star">*</span>--%><br />
                                            <asp:TextBox ID="txtPZip" runat="server" onkeyup="OnOfIsSameAsServiceAddress();"
                                                CssClass="text-box" placeholder="Enter Zip Code" MaxLength="10"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtPZip"
                                                FilterType="Numbers" ValidChars=" ">
                                            </asp:FilteredTextBoxExtender>
                                            <span id="spanPZip" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box2">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litPArea" runat="server" Text="<%$ Resources:Resource, AREA_CODE%>"></asp:Literal></label>
                                            <%--<span class="span_star">*</span>--%><br />
                                            <asp:TextBox ID="txtPArea" runat="server" onkeyup="OnOfIsSameAsServiceAddress();"
                                                CssClass="text-box" placeholder="Enter Area Code" AutoPostBack="true" OnTextChanged="txtPArea_TextChanged"
                                                MaxLength="6"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtPArea"
                                                FilterType="Numbers">
                                            </asp:FilteredTextBoxExtender>
                                            <span id="spanPArea" class="span_color"></span>
                                            <br />
                                            <span id="spnPostalArea" runat="server"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box3">
                                        <div class="text-inner" style="display: none;">
                                            <label for="name">
                                                <asp:Literal ID="Literal30" runat="server" Text="<%$ Resources:Resource, REASON%>"></asp:Literal></label>
                                            <span class="span_star">*</span><br />
                                            <asp:TextBox ID="txtPReason" runat="server" CssClass="text-box" placeholder="Enter Reason"
                                                onblur="return TextBoxBlurValidationlbl(this,'spanPReason','Reason')" TextMode="MultiLine"></asp:TextBox>
                                            <span id="spanPReason" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <%-------------------------Postal Information End----------------------%>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <asp:CheckBox ID="cbIsSameAsPostal" runat="server" Text="Is same as Postal Address" />
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <%-------------------------Service Information Start----------------------%>
                            <div id="divServiceAdrsOriginal" runat="server">
                                <asp:UpdatePanel ID="updtPnlServiceAddress" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <h4 class="subHeading">
                                            <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Literal>
                                        </h4>
                                        <div class="clr">
                                        </div>
                                        <div class="inner-box1">
                                            <div class="text-inner">
                                                <label for="name">
                                                    <asp:Literal ID="litSHNo" runat="server" Text="<%$ Resources:Resource, H_NO%>"></asp:Literal></label><span
                                                        class="span_star">*</span><br />
                                                <asp:TextBox ID="txtSHNo" runat="server" CssClass="text-box" placeholder="Enter House No"
                                                    onblur="return TextBoxBlurValidationlblZeroAccepts(this,'spanSHNo','House No')"
                                                    MaxLength="10"></asp:TextBox>
                                                <span id="spanSHNo" class="span_color"></span>
                                            </div>
                                        </div>
                                        <div class="inner-box2">
                                            <div class="text-inner">
                                                <label for="name">
                                                    <asp:Literal ID="litSStreet" runat="server" Text="<%$ Resources:Resource, STREET%>"></asp:Literal></label><span
                                                        class="span_star">*</span><br />
                                                <asp:TextBox ID="txtSStreet" runat="server" CssClass="text-box" placeholder="Enter Street Name"
                                                    onblur="return TextBoxBlurValidationlbl(this,'spanSStreet','Street Name')" MaxLength="255"></asp:TextBox>
                                                <span id="spanSStreet" class="span_color"></span>
                                            </div>
                                        </div>
                                        <div class="inner-box3">
                                            <div class="text-inner">
                                                <label for="name">
                                                    <asp:Literal ID="litSLGAVillage" runat="server" Text="<%$ Resources:Resource, CITY%>"></asp:Literal></label><span
                                                        class="span_star">*</span><br />
                                                <asp:TextBox ID="txtSLGAVillage" runat="server" CssClass="text-box" placeholder="Enter City"
                                                    onblur="return TextBoxBlurValidationlbl(this,'spanSLGAVillage','City')" MaxLength="50"></asp:TextBox>
                                                <span id="spanSLGAVillage" class="span_color"></span>
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                        <div class="inner-box1">
                                            <div class="text-inner">
                                                <label for="name">
                                                    <asp:Literal ID="litSZip" runat="server" Text="<%$ Resources:Resource, ZIP%>"></asp:Literal></label><%--<span
                                                        class="span_star">*</span>--%><br />
                                                <asp:TextBox ID="txtSZip" runat="server" CssClass="text-box" placeholder="Enter Zip Code"
                                                    MaxLength="10"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" TargetControlID="txtSZip"
                                                    FilterType="Numbers">
                                                </asp:FilteredTextBoxExtender>
                                                <span id="spanSZip" class="span_color"></span>
                                            </div>
                                        </div>
                                        <div class="inner-box2">
                                            <div class="text-inner">
                                                <label for="name">
                                                    <asp:Literal ID="litSArea" runat="server" Text="<%$ Resources:Resource, AREA_CODE%>"></asp:Literal></label><%--<span
                                                        class="span_star">*</span>--%><br />
                                                <asp:TextBox ID="txtSArea" runat="server" CssClass="text-box" placeholder="Enter Area Code"
                                                    AutoPostBack="true" OnTextChanged="txtSArea_TextChanged" MaxLength="6"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtSArea"
                                                    FilterType="Numbers">
                                                </asp:FilteredTextBoxExtender>
                                                <span id="spanSArea" class="span_color"></span>
                                                <br />
                                                <span id="spnServericeArea" runat="server"></span>
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                        <div class="inner-box1">
                                            <div class="text-inner">
                                                <label for="name">
                                                    <asp:Literal ID="litrblCommunicationAddress" runat="server" Text="<%$ Resources:Resource, COMMUNICATION_ADD%>"></asp:Literal></label><span
                                                        class="span_star">*</span><br />
                                                <asp:RadioButtonList ID="rblCommunicationAddress" RepeatDirection="Horizontal" runat="server">
                                                    <asp:ListItem Text="Postal" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="Service" Value="2"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                            <br />
                                            <span id="spanrblCommunicationAddress" class="span_color"></span>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div id="divServiceAdrsDuplicate" runat="server" style="display: none;">
                                <asp:UpdatePanel ID="UpdatePnlSADup" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <h4 class="subHeading">
                                            <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Literal>
                                        </h4>
                                        <div class="clr">
                                        </div>
                                        <div class="inner-box1">
                                            <div class="text-inner">
                                                <label for="name">
                                                    <asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:Resource, H_NO%>"></asp:Literal></label><span
                                                        class="span_star">*</span><br />
                                                <asp:TextBox ID="txtHouseNoServ" runat="server" CssClass="text-box" placeholder="Enter House No"
                                                    onblur="return TextBoxBlurValidationlblZeroAccepts(this,'txtHouseNoServ','House No')"
                                                    MaxLength="10"></asp:TextBox>
                                                <span id="spntxtHouseNoServ" class="span_color"></span>
                                            </div>
                                        </div>
                                        <div class="inner-box2">
                                            <div class="text-inner">
                                                <label for="name">
                                                    <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:Resource, STREET%>"></asp:Literal></label><span
                                                        class="span_star">*</span><br />
                                                <asp:TextBox ID="txtStreetNameServ" runat="server" CssClass="text-box" placeholder="Enter Street Name"
                                                    onblur="return TextBoxBlurValidationlbl(this,'spntxtStreetNameServ','Street Name')"
                                                    MaxLength="255"></asp:TextBox>
                                                <span id="spntxtStreetNameServ" class="span_color"></span>
                                            </div>
                                        </div>
                                        <div class="inner-box3">
                                            <div class="text-inner">
                                                <label for="name">
                                                    <asp:Literal ID="Literal19" runat="server" Text="<%$ Resources:Resource, CITY%>"></asp:Literal></label><span
                                                        class="span_star">*</span><br />
                                                <asp:TextBox ID="txtCityServ" runat="server" CssClass="text-box" placeholder="Enter City"
                                                    onblur="return TextBoxBlurValidationlbl(this,'spntxtCityServ','City')" MaxLength="50"></asp:TextBox>
                                                <span id="spntxtCityServ" class="span_color"></span>
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                        <div class="inner-box1">
                                            <div class="text-inner">
                                                <label for="name">
                                                    <asp:Literal ID="Literal26" runat="server" Text="<%$ Resources:Resource, ZIP%>"></asp:Literal></label><%--<span
                                                        class="span_star">*</span>--%><br />
                                                <asp:TextBox ID="txtZipServ" runat="server" CssClass="text-box" placeholder="Enter Zip Code"
                                                    MaxLength="10"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtZipServ"
                                                    FilterType="Numbers">
                                                </asp:FilteredTextBoxExtender>
                                                <span id="spntxtZipServ" class="span_color"></span>
                                            </div>
                                        </div>
                                        <div class="inner-box2">
                                            <div class="text-inner">
                                                <label for="name">
                                                    <asp:Literal ID="Literal27" runat="server" Text="<%$ Resources:Resource, AREA_CODE%>"></asp:Literal></label><%--<span
                                                        class="span_star">*</span>--%><br />
                                                <asp:TextBox ID="txtAreaServ" runat="server" CssClass="text-box" placeholder="Enter Area Code"
                                                    AutoPostBack="true" OnTextChanged="txtSAreaServ_TextChanged" MaxLength="6"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtAreaServ"
                                                    FilterType="Numbers">
                                                </asp:FilteredTextBoxExtender>
                                                <span id="spntxtAreaServ" class="span_color"></span>
                                                <br />
                                                <span id="spnServericeAreaServ" runat="server"></span>
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                        <div class="inner-box1">
                                            <div class="text-inner">
                                                <label for="name">
                                                    <asp:Literal ID="Literal28" runat="server" Text="<%$ Resources:Resource, COMMUNICATION_ADD%>"></asp:Literal></label><span
                                                        class="span_star">*</span><br />
                                                <asp:RadioButtonList ID="rdoCommunicationServ" RepeatDirection="Horizontal" runat="server">
                                                    <asp:ListItem Text="Postal" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="Service" Value="2"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                            <br />
                                            <span id="spnrdoCommunicationServ" class="span_color"></span>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <%-------------------------Service Information End----------------------%>
                            <div class="clr">
                                <br />
                                <br />
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="DivPostalAddress" style="display: none;">
                            <div class="text-heading">
                                <asp:Literal ID="litPostalAddress" runat="server" Text="<%$ Resources:Resource, POSTAL_ADDRESS%>"></asp:Literal>
                            </div>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, H_NO%>"></asp:Literal>
                                        <span class="span_star">*</span></label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox ID="txtHouseNo" CssClass="text-box" runat="server" placeholder="House No"
                                        MaxLength="10" onblur="return TextBoxBlurValidationlblZeroAccepts(this,'spanHouseNo','House No')"></asp:TextBox>
                                    <span id="spanHouseNo" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal3" runat="server" Text="Street Name"></asp:Literal>
                                        <span class="span_star">*</span></label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox ID="txtStreet" CssClass="text-box" runat="server" placeholder="Address 1"
                                        MaxLength="255" onblur="return TextBoxBlurValidationlbl(this,'spanStreet','Address 1')"></asp:TextBox>
                                    <span id="spanStreet" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box3">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal4" runat="server" Text="City"></asp:Literal>
                                    </label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox ID="txtCity" CssClass="text-box" runat="server" MaxLength="50" placeholder="Address 2"></asp:TextBox>
                                    <span id="spanCity" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, LAND_MARK%>"></asp:Literal>
                                    </label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox ID="txtLandMark" CssClass="text-box" runat="server" placeholder="Landmark"></asp:TextBox>
                                    <span id="spanLandMark" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, ZIP%>"></asp:Literal>
                                    </label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox ID="txtZipCode" CssClass="text-box" MaxLength="10" runat="server" placeholder="Zip Code"></asp:TextBox>
                                    <span id="spanZipCode" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box3">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:Resource, REASON%>"></asp:Literal>
                                        <span class="span_star">*</span></label><div class="space">
                                        </div>
                                    <asp:TextBox ID="txtReason" CssClass="text-box" runat="server" TextMode="MultiLine"
                                        placeholder="Reason" onblur="return TextBoxBlurValidationlblZeroAccepts(this,'spanReason','Reason')"></asp:TextBox>
                                    <span id="spanReason" class="span_color"></span>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="consumer_feild clear" style="margin-left: 13px; display: none;">
                            <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, COPY_ADDRESS%>"></asp:Literal>
                            <input id="rbtnPostalAddress" name="Address" type="radio" onclick="CopyAddress(true)"
                                runat="server" />
                            Yes
                            <input id="rbtnServiceAddress" name="Address" type="radio" onclick="return CopyAddress(false)"
                                runat="server" />
                            No
                        </div>
                        <div class="clr">
                        </div>
                        <div id="DivServiceAddress" style="display: none;">
                            <div class="text-heading">
                                <asp:Literal ID="litServiceAddress" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Literal>
                            </div>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal22" runat="server" Text="<%$ Resources:Resource, H_NO%>"></asp:Literal>
                                        <span class="span_star">*</span></label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox ID="txtServiceHouseNo" CssClass="text-box" runat="server" placeholder="House No"
                                        MaxLength="10" onblur="return TextBoxBlurValidationlblZeroAccepts(this,'spanServiceHouseNo','Service House No')"></asp:TextBox>
                                    <span id="spanServiceHouseNo" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal23" runat="server" Text="<%$ Resources:Resource, ADDRESS_1%>"></asp:Literal>
                                        <span class="span_star">*</span></label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox ID="txtServiceStreet" CssClass="text-box" runat="server" placeholder="Address 1"
                                        MaxLength="255" onblur="return TextBoxBlurValidationlbl(this,'spanServiceStreet','Service  Address 1')"></asp:TextBox>
                                    <span id="spanServiceStreet" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box3">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal24" runat="server" Text="<%$ Resources:Resource, ADDRESS_2%>"></asp:Literal>
                                    </label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox ID="txtServiceCity" CssClass="text-box" placeholder="Address 2" MaxLength="50"
                                        runat="server"></asp:TextBox>
                                    <span id="spanServiceCity" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal25" runat="server" Text="<%$ Resources:Resource, LAND_MARK%>"></asp:Literal>
                                    </label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox ID="txtServiceLandMark" CssClass="text-box" runat="server" placeholder="Landmark"></asp:TextBox>
                                    <span id="spanServiceLandMark" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:Resource, ZIP%>"></asp:Literal></label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox ID="txtServiceZipCode" CssClass="text-box" MaxLength="10" runat="server"
                                        placeholder="ZipCode"></asp:TextBox>
                                    <span id="spanServiceZipCode" class="span_color"></span>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="inner-box1">
                            <div class="search">
                                <asp:Button ID="btnSave" Text="<%$ Resources:Resource, UPDATE%>" CssClass="box_s"
                                    OnClick="btnUpdate_Click" runat="server" OnClientClick="return UpdateValidate();" /><%-- OnClientClick="return UpdateValidate();"--%>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="rnkland">
                    <%-- Confirm popup Start--%>
                    <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                        TargetControlID="btnActivate" BackgroundCssClass="modalBackground">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelActivate" runat="server" DefaultButton="btnActiveCancel" CssClass="modalPopup"
                        Style="display: none; border-color: #639B00;">
                        <div class="popheader popheaderlblred" runat="server" id="popheader">
                            <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                        </div>
                        <br />
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Confirm popup Closed--%>
                    <asp:ModalPopupExtender ID="mpeActivate1" runat="server" PopupControlID="PanelActivate1"
                        TargetControlID="btnActivate1" BackgroundCssClass="modalBackground">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnActivate1" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelActivate1" runat="server" DefaultButton="btnActiveCancel1" CssClass="modalPopup"
                        Style="display: none; border-color: #639B00;">
                        <div class="popheader popheaderlblred">
                            <asp:Label ID="lblActiveMsg1" runat="server"></asp:Label>
                        </div>
                        <br />
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnActiveCancel1" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="PanelAlert"
                        TargetControlID="btnAlertDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnAlertOk">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnAlertDeActivate" runat="server" Text="Button" CssClass="popbtn" />
                    <asp:Panel ID="PanelAlert" runat="server" CssClass="modalPopup poppnl" Style="display: none;">
                        <div class="popheader popheaderlblred">
                            <asp:Label ID="lblAlertMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnAlertOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <asp:HiddenField ID="hfIsServiceAddress" runat="server" Value="false" />
                <asp:HiddenField ID="hfPostalAddressID" runat="server" />
                <asp:HiddenField ID="hfServiceAddressID" runat="server" />
                <asp:HiddenField ID="hfBookNo" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".rnkland").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../Javascript/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/CustomerBookNoChange.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            //AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);

        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            //AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);
        });
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
