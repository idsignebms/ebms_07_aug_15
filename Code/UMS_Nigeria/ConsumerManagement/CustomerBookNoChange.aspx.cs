﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for ChangeCustomerAddress
                     
 Developer        : id0775-NEERAJ KANOJIYA
 Creation Date    : 2-APRIL-2015
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using iDSignHelper;
using UMS_NigeriaBAL;
using Resources;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Xml;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class CustomerBookNoChange : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        ReportsBal objReportsBal = new ReportsBal();
        ConsumerBal objConsumerBal = new ConsumerBal();
        string Key = "ChangeCustomerAddress";
        AreaBe objAreaBe = new AreaBe();
        AreaBAL objAreaBAL = new AreaBAL();
        #endregion

        #region Properties
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                lblMessage.Text = pnlMessage.CssClass = string.Empty;
                if (!IsPostBack)
                {
                    string path = string.Empty;
                    path = _objCommonMethods.GetPagePath(this.Request.Url);
                    if (_objCommonMethods.IsAccess(path))
                    {

                    }
                    else
                    {
                        Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    }
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }

        #region OldCode
        //protected void btnGo_Click(object sender, EventArgs e)
        //{
        //    Clear();
        //    RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
        //    objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
        //    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
        //        objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
        //    else
        //        objLedgerBe.BUID = string.Empty;
        //    objLedgerBe = _objiDsignBal.DeserializeFromXml<RptCustomerLedgerBe>(objReportsBal.GetLedger(objLedgerBe, ReturnType.Single));
        //    if (objLedgerBe.IsSuccess)
        //    {
        //        if (objLedgerBe.IsCustExistsInOtherBU)
        //        {
        //            lblActiveMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
        //            mpeActivate.Show();
        //        }
        //        else
        //        {
        //            #region New code
        //            CustomerRegistrationBE objCustomerRegistrationBE = new CustomerRegistrationBE();

        //            CustomerRegistrationList_1BE objCustomerRegistrationList_1BE = new CustomerRegistrationList_1BE();
        //            CustomerRegistrationList_2BE objCustomerRegistrationList_2BE = new CustomerRegistrationList_2BE();
        //            XmlDocument xml = new XmlDocument();
        //            ConsumerBal objConsumerBal = new ConsumerBal();
        //            objCustomerRegistrationBE.GolbalAccountNumber = txtAccountNo.Text;
        //            xml = objConsumerBal.GetCustomerAddress(objCustomerRegistrationBE);
        //            if (xml != null)
        //            {
        //                objCustomerRegistrationList_1BE = _objiDsignBal.DeserializeFromXml<CustomerRegistrationList_1BE>(xml);
        //                objCustomerRegistrationList_2BE = _objiDsignBal.DeserializeFromXml<CustomerRegistrationList_2BE>(xml);
        //                if (objCustomerRegistrationList_1BE.items.Count > 0)
        //                {
        //                    divdetails.Visible = true;
        //                    lblGlobalAccNoAndAccNo.Text = objCustomerRegistrationList_1BE.items[0].GlobalAccNoAndAccNo;
        //                    lblGlobalAccountNO.Text = objCustomerRegistrationList_1BE.items[0].GolbalAccountNumber;
        //                    //lblAccountNo.Text = objCustomerRegistrationList_1BE.items[0].AccountNo;
        //                    lblOldAccNo.Text = objCustomerRegistrationList_1BE.items[0].OldAccountNo;
        //                    lblName.Text = objCustomerRegistrationList_1BE.items[0].ContactName;
        //                    lblTariff.Text = objCustomerRegistrationList_1BE.items[0].Tariff;
        //                    lblMeterNo.Text = objCustomerRegistrationList_1BE.items[0].MeterNumber;
        //                    lblExistingBook.Text = objCustomerRegistrationList_1BE.items[0].ExistingBook;//Faiz-ID103
        //                    lblOutstandingAmount.Text = _objCommonMethods.GetCurrencyFormat(objCustomerRegistrationList_1BE.items[0].OutStandingAmount, 2, 2); 
        //                    rblCommunicationAddress.Items[0].Selected = false;
        //                    rblCommunicationAddress.Items[1].Selected = false;


        //                    rdoCommunicationServ.Items[1].Selected = false;
        //                    rdoCommunicationServ.Items[0].Selected = true;


        //                    _objCommonMethods.BindBusinessUnits(ddlBU, objCustomerRegistrationList_1BE.items[0].StateCode, true);
        //                    ddlBU.SelectedIndex = ddlBU.Items.IndexOf(ddlBU.Items.FindByValue(objCustomerRegistrationList_1BE.items[0].BU_ID.ToString()));
        //                    _objCommonMethods.BindUserServiceUnits_BuIds(ddlSU, ddlBU.SelectedValue, UMS_Resource.DROPDOWN_SELECT, false);
        //                    ddlSU.SelectedIndex = ddlSU.Items.IndexOf(ddlSU.Items.FindByValue(objCustomerRegistrationList_1BE.items[0].SU_ID.ToString()));
        //                    _objCommonMethods.BindUserServiceCenters_SuIds(ddlSC, ddlSU.SelectedValue, UMS_Resource.DROPDOWN_SELECT, false);
        //                    ddlSC.SelectedIndex = ddlSC.Items.IndexOf(ddlSC.Items.FindByValue(objCustomerRegistrationList_1BE.items[0].SC_ID.ToString()));
        //                    _objCommonMethods.BindCyclesByBUSUSC(ddlCycle, ddlBU.SelectedValue, ddlSU.SelectedValue, ddlSC.SelectedValue, true, UMS_Resource.DROPDOWN_SELECT, false);
        //                    ddlCycle.SelectedIndex = ddlCycle.Items.IndexOf(ddlCycle.Items.FindByValue(objCustomerRegistrationList_1BE.items[0].CycleId.ToString()));
        //                    _objCommonMethods.BindAllBookNumbers(ddlBookNo, ddlCycle.SelectedValue, true, false, UMS_Resource.DROPDOWN_SELECT);
        //                    if (ddlBookNo.Items.Count > 0)
        //                        ddlBookNo.SelectedIndex = ddlBookNo.Items.IndexOf(ddlBookNo.Items.FindByValue(objCustomerRegistrationList_1BE.items[0].BookNo.ToString()));
        //                    hfBookNo.Value = objCustomerRegistrationList_1BE.items[0].BookNo.ToString();

        //                }
        //                if (objCustomerRegistrationList_2BE.items.Count > 0)
        //                {
        //                    hfPostalAddressID.Value = objCustomerRegistrationList_2BE.items[0].PostalAddressID.ToString();
        //                    txtPHNo.Text = objCustomerRegistrationList_2BE.items[0].HouseNoPostal;
        //                    txtPStreet.Text = objCustomerRegistrationList_2BE.items[0].StreetPostal;
        //                    txtPLGAVillage.Text = objCustomerRegistrationList_2BE.items[0].CityPostaL;
        //                    txtPZip.Text = objCustomerRegistrationList_2BE.items[0].ZipCodePostal;
        //                    txtPArea.Text = objCustomerRegistrationList_2BE.items[0].AreaPostal;
        //                    if (objCustomerRegistrationList_2BE.items[0].IsCommunication)
        //                        rblCommunicationAddress.Items[0].Selected = true;

        //                    if (objCustomerRegistrationList_2BE.items[0].IsCommunication)
        //                        rdoCommunicationServ.Items[0].Selected = true;


        //                    if (objCustomerRegistrationList_2BE.items.Count == 1)
        //                    {
        //                        cbIsSameAsPostal.Checked = true;
        //                        rblCommunicationAddress.Items[0].Selected = true;
        //                        rblCommunicationAddress.Items[1].Enabled = false;
        //                        txtSHNo.Text = objCustomerRegistrationList_2BE.items[0].HouseNoPostal;
        //                        txtSStreet.Text = objCustomerRegistrationList_2BE.items[0].StreetPostal;
        //                        txtSLGAVillage.Text = objCustomerRegistrationList_2BE.items[0].CityPostaL;
        //                        txtSZip.Text = objCustomerRegistrationList_2BE.items[0].ZipCodePostal;
        //                        txtSArea.Text = objCustomerRegistrationList_2BE.items[0].AreaPostal;

        //                        txtHouseNoServ.Text = "";
        //                        txtStreetNameServ.Text = "";
        //                        txtCityServ.Text = "";
        //                        txtZipServ.Text = "";
        //                        txtAreaServ.Text = "";
        //                        hfIsServiceAddress.Value = "false";
        //                        //divServiceAdrsDuplicate.Visible = false;
        //                        //divServiceAdrsOriginal.Visible = true;
        //                        cbIsSameAsPostal.Attributes.Add("onchange", "OnOffTenentDivRev()");

        //                        txtSHNo.Enabled = false;
        //                        txtSStreet.Enabled = false;
        //                        txtSLGAVillage.Enabled = false;
        //                        txtSZip.Enabled = false;
        //                        txtSArea.Enabled = false;

        //                        txtHouseNoServ.Enabled = true;
        //                        txtStreetNameServ.Enabled = true;
        //                        txtCityServ.Enabled = true;
        //                        txtZipServ.Enabled = true;
        //                        txtAreaServ.Enabled = true;
        //                        rdoCommunicationServ.Items[0].Selected = true;
        //                    }
        //                }
        //                if (objCustomerRegistrationList_2BE.items.Count > 1)
        //                {
        //                    hfServiceAddressID.Value = objCustomerRegistrationList_2BE.items[1].PostalAddressID.ToString();
        //                    rblCommunicationAddress.Items[1].Enabled = true;
        //                    cbIsSameAsPostal.Checked = false;
        //                    //rblCommunicationAddress.Items[0].Selected = false;
        //                    //rblCommunicationAddress.Items[1].Enabled = true;
        //                    txtSHNo.Text = objCustomerRegistrationList_2BE.items[1].HouseNoPostal;
        //                    txtSStreet.Text = objCustomerRegistrationList_2BE.items[1].StreetPostal;
        //                    txtSLGAVillage.Text = objCustomerRegistrationList_2BE.items[1].CityPostaL;
        //                    txtSZip.Text = objCustomerRegistrationList_2BE.items[1].ZipCodePostal;
        //                    txtSArea.Text = objCustomerRegistrationList_2BE.items[1].AreaPostal;
        //                    if (objCustomerRegistrationList_2BE.items[1].IsCommunication)
        //                        rblCommunicationAddress.Items[1].Selected = true;

        //                    txtHouseNoServ.Text = objCustomerRegistrationList_2BE.items[0].HouseNoPostal;
        //                    txtStreetNameServ.Text = objCustomerRegistrationList_2BE.items[0].StreetPostal;
        //                    txtCityServ.Text = objCustomerRegistrationList_2BE.items[0].CityPostaL;
        //                    txtZipServ.Text = objCustomerRegistrationList_2BE.items[0].ZipCodePostal;
        //                    txtAreaServ.Text = objCustomerRegistrationList_2BE.items[0].AreaPostal;
        //                    if (objCustomerRegistrationList_2BE.items[0].IsCommunication)
        //                        rdoCommunicationServ.Items[0].Selected = true;
        //                    //divServiceAdrsDuplicate.Visible = true;
        //                    //divServiceAdrsOriginal.Visible = false;
        //                    hfIsServiceAddress.Value = "true";
        //                    cbIsSameAsPostal.Attributes.Add("onchange", "OnOffTenentDiv()");

        //                    txtHouseNoServ.Enabled = false;
        //                    txtStreetNameServ.Enabled = false;
        //                    txtCityServ.Enabled = false;
        //                    txtZipServ.Enabled = false;
        //                    txtAreaServ.Enabled = false;

        //                    txtSHNo.Enabled = true;
        //                    txtSStreet.Enabled = true;
        //                    txtSLGAVillage.Enabled = true;
        //                    txtSZip.Enabled = true;
        //                    txtSArea.Enabled = true;
        //                    divServiceAdrsDuplicate.Style.Add("display", "none");
        //                    divServiceAdrsOriginal.Style.Add("display", "inline");
        //                }
        //            }


        //            #endregion

        //            #region old code
        //            //ChangeBookNoBe _objBookNoBe = new ChangeBookNoBe();
        //            //_objBookNoBe.AccountNo = txtAccountNo.Text.Trim();
        //            //_objBookNoBe = _objiDsignBal.DeserializeFromXml<ChangeBookNoBe>(_objBookNoBal.Get(_objBookNoBe, ReturnType.Get));
        //            //if (!_objBookNoBe.IsAccountNoNotExists)
        //            //{
        //            //    divdetails.Visible = true;
        //            //    lblAccountNo.Text = _objBookNoBe.AccountNo;
        //            //    lblOldAccNo.Text = _objBookNoBe.OldAccountNo;
        //            //    lblName.Text = _objBookNoBe.Name;
        //            //    lblTariff.Text = _objBookNoBe.Tariff;
        //            //    lblMeterNo.Text = _objBookNoBe.MeterNo;
        //            //    lblSurName.Text = _objBookNoBe.SurName;
        //            //    lblKnownAs.Text = _objBookNoBe.KnownAs;

        //            //    txtLandMark.Text = _objBookNoBe.PostalLandMark;
        //            //    txtStreet.Text = _objBookNoBe.PostalStreet;
        //            //    txtCity.Text = _objBookNoBe.PostalCity;
        //            //    txtHouseNo.Text = _objBookNoBe.PostalHouseNo;
        //            //    txtZipCode.Text = _objBookNoBe.PostalZipCode;
        //            //    txtServiceLandMark.Text = _objBookNoBe.ServiceLandMark;
        //            //    txtServiceStreet.Text = _objBookNoBe.ServiceStreet;
        //            //    txtServiceCity.Text = _objBookNoBe.ServiceCity;
        //            //    txtServiceHouseNo.Text = _objBookNoBe.ServiceHouseNo;
        //            //    txtServiceZipCode.Text = _objBookNoBe.ServiceZipCode;

        //            //    if (_objBookNoBe.PostalHouseNo == _objBookNoBe.ServiceHouseNo)
        //            //        rbtnPostalAddress.Checked = true;
        //            //    else
        //            //        rbtnServiceAddress.Checked = true;

        //            //}
        //            //else if (_objBookNoBe.IsAccountNoNotExists)
        //            //{
        //            //    CustomerNotFound();
        //            //}
        //            #endregion
        //        }
        //    }
        //    else
        //    {
        //        CustomerNotFound();
        //    }
        //    //txtAccountNo.Text = string.Empty;
        //}
        #endregion
        #region NewCode Go Click
        protected void btnGo_Click(object sender, EventArgs e)
        {
            XmlDocument xml = new XmlDocument();
            XmlDocument XmlCustomerRegistrationBE = new XmlDocument();
            CustomerRegistrationList_1BE objCustomerRegistrationList_1BE = new CustomerRegistrationList_1BE();
            CustomerRegistrationList_2BE objCustomerRegistrationList_2BE = new CustomerRegistrationList_2BE();
            Clear();
            RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
            objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                objLedgerBe.BUID = string.Empty;

            xml = objConsumerBal.GetCustomerBook_ByCheck(objLedgerBe);
            if (xml.SelectSingleNode("/*").Name == "RptCustomerLedgerBe")
            {
                objLedgerBe = _objiDsignBal.DeserializeFromXml<RptCustomerLedgerBe>(xml);
                if (objLedgerBe.IsSuccess)
                {
                    if (objLedgerBe.IsCustExistsInOtherBU)
                    {
                        divdetails.Visible = false;
                        txtAccountNo.Text = string.Empty;
                        lblActiveMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                        mpeActivate.Show();
                    }
                    else if (objLedgerBe.ActiveStatusId == (int)CustomerStatus.InActive)
                    {
                        divdetails.Visible = false;
                        txtAccountNo.Text = string.Empty;
                        lblActiveMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.InActive, "Book No change");
                        mpeActivate.Show();
                    }
                    else if (objLedgerBe.ActiveStatusId == (int)CustomerStatus.Hold)
                    {
                        divdetails.Visible = false;
                        txtAccountNo.Text = string.Empty;
                        lblActiveMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.Hold, "Book No change");
                        mpeActivate.Show();
                    }
                    else if (objLedgerBe.ActiveStatusId == (int)CustomerStatus.Closed)
                    {
                        divdetails.Visible = false;
                        txtAccountNo.Text = string.Empty;
                        lblActiveMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.Closed, "Book No change");
                        mpeActivate.Show();
                    }
                }
                else
                {
                    CustomerNotFound();
                }
            }
            else if (xml.SelectSingleNode("/*").Name == "CustomerRegistrationInfoByXml")
            {
                objCustomerRegistrationList_1BE = _objiDsignBal.DeserializeFromXml<CustomerRegistrationList_1BE>(xml);
                objCustomerRegistrationList_2BE = _objiDsignBal.DeserializeFromXml<CustomerRegistrationList_2BE>(xml);
                if (objCustomerRegistrationList_1BE.items.Count > 0)
                {
                    divdetails.Visible = true;
                    lblGlobalAccountNO.Text = objCustomerRegistrationList_1BE.items[0].GolbalAccountNumber;
                    lblGlobalAccNoAndAccNo.Text = objCustomerRegistrationList_1BE.items[0].GlobalAccNoAndAccNo;
                    //lblAccountNo.Text = objCustomerRegistrationList_1BE.items[0].AccountNo;
                    lblOldAccNo.Text = objCustomerRegistrationList_1BE.items[0].OldAccountNo;
                    lblName.Text = objCustomerRegistrationList_1BE.items[0].ContactName;
                    lblTariff.Text = objCustomerRegistrationList_1BE.items[0].Tariff;
                    lblMeterNo.Text = objCustomerRegistrationList_1BE.items[0].MeterNumber;
                    lblExistingBook.Text = objCustomerRegistrationList_1BE.items[0].ExistingBook;//Faiz-ID103
                    lblOutstandingAmount.Text = _objCommonMethods.GetCurrencyFormat(objCustomerRegistrationList_1BE.items[0].OutStandingAmount, 2, 2); 
                    rblCommunicationAddress.Items[0].Selected = false;
                    rblCommunicationAddress.Items[1].Selected = false;


                    rdoCommunicationServ.Items[1].Selected = false;
                    rdoCommunicationServ.Items[0].Selected = true;



                    _objCommonMethods.BindBusinessUnits(ddlBU, objCustomerRegistrationList_1BE.items[0].StateCode, true);
                    ddlBU.SelectedIndex = ddlBU.Items.IndexOf(ddlBU.Items.FindByValue(objCustomerRegistrationList_1BE.items[0].BU_ID.ToString()));
                    _objCommonMethods.BindUserServiceUnits_BuIds(ddlSU, ddlBU.SelectedValue, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlSU.SelectedIndex = ddlSU.Items.IndexOf(ddlSU.Items.FindByValue(objCustomerRegistrationList_1BE.items[0].SU_ID.ToString()));
                    _objCommonMethods.BindUserServiceCenters_SuIds(ddlSC, ddlSU.SelectedValue, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlSC.SelectedIndex = ddlSC.Items.IndexOf(ddlSC.Items.FindByValue(objCustomerRegistrationList_1BE.items[0].SC_ID.ToString()));
                    _objCommonMethods.BindCyclesByBUSUSC(ddlCycle, ddlBU.SelectedValue, ddlSU.SelectedValue, ddlSC.SelectedValue, true, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlCycle.SelectedIndex = ddlCycle.Items.IndexOf(ddlCycle.Items.FindByValue(objCustomerRegistrationList_1BE.items[0].CycleId.ToString()));
                    _objCommonMethods.BindAllBookNumbers(ddlBookNo, ddlCycle.SelectedValue, true, false, UMS_Resource.DROPDOWN_SELECT);
                    ddlBookNo.SelectedIndex = ddlBookNo.Items.IndexOf(ddlBookNo.Items.FindByValue(objCustomerRegistrationList_1BE.items[0].BookNo.ToString()));
                    hfBookNo.Value = objCustomerRegistrationList_1BE.items[0].BookNo.ToString();

                }
                if (objCustomerRegistrationList_2BE.items.Count > 0)
                {
                    hfPostalAddressID.Value = objCustomerRegistrationList_2BE.items[0].PostalAddressID.ToString();
                    txtPHNo.Text = objCustomerRegistrationList_2BE.items[0].HouseNoPostal;
                    txtPStreet.Text = objCustomerRegistrationList_2BE.items[0].StreetPostal;
                    txtPLGAVillage.Text = objCustomerRegistrationList_2BE.items[0].CityPostaL;
                    txtPZip.Text = objCustomerRegistrationList_2BE.items[0].ZipCodePostal;
                    txtPArea.Text = objCustomerRegistrationList_2BE.items[0].AreaPostal;
                    if (objCustomerRegistrationList_2BE.items[0].IsCommunication)
                        rblCommunicationAddress.Items[0].Selected = true;

                    if (objCustomerRegistrationList_2BE.items[0].IsCommunication)
                        rdoCommunicationServ.Items[0].Selected = true;


                    if (objCustomerRegistrationList_2BE.items.Count == 1)
                    {
                        cbIsSameAsPostal.Checked = true;
                        rblCommunicationAddress.Items[0].Selected = true;
                        rblCommunicationAddress.Items[1].Enabled = false;
                        txtSHNo.Text = objCustomerRegistrationList_2BE.items[0].HouseNoPostal;
                        txtSStreet.Text = objCustomerRegistrationList_2BE.items[0].StreetPostal;
                        txtSLGAVillage.Text = objCustomerRegistrationList_2BE.items[0].CityPostaL;
                        txtSZip.Text = objCustomerRegistrationList_2BE.items[0].ZipCodePostal;
                        txtSArea.Text = objCustomerRegistrationList_2BE.items[0].AreaPostal;

                        txtHouseNoServ.Text = "";
                        txtStreetNameServ.Text = "";
                        txtCityServ.Text = "";
                        txtZipServ.Text = "";
                        txtAreaServ.Text = "";
                        hfIsServiceAddress.Value = "false";
                        //divServiceAdrsDuplicate.Visible = false;
                        //divServiceAdrsOriginal.Visible = true;
                        cbIsSameAsPostal.Attributes.Add("onchange", "OnOffTenentDivRev()");

                        txtSHNo.Enabled = false;
                        txtSStreet.Enabled = false;
                        txtSLGAVillage.Enabled = false;
                        txtSZip.Enabled = false;
                        txtSArea.Enabled = false;

                        txtHouseNoServ.Enabled = true;
                        txtStreetNameServ.Enabled = true;
                        txtCityServ.Enabled = true;
                        txtZipServ.Enabled = true;
                        txtAreaServ.Enabled = true;
                        rdoCommunicationServ.Items[0].Selected = true;
                    }
                }
                if (objCustomerRegistrationList_2BE.items.Count > 1)
                {
                    hfServiceAddressID.Value = objCustomerRegistrationList_2BE.items[1].PostalAddressID.ToString();
                    rblCommunicationAddress.Items[1].Enabled = true;
                    cbIsSameAsPostal.Checked = false;
                    //rblCommunicationAddress.Items[0].Selected = false;
                    //rblCommunicationAddress.Items[1].Enabled = true;
                    txtSHNo.Text = objCustomerRegistrationList_2BE.items[1].HouseNoPostal;
                    txtSStreet.Text = objCustomerRegistrationList_2BE.items[1].StreetPostal;
                    txtSLGAVillage.Text = objCustomerRegistrationList_2BE.items[1].CityPostaL;
                    txtSZip.Text = objCustomerRegistrationList_2BE.items[1].ZipCodePostal;
                    txtSArea.Text = objCustomerRegistrationList_2BE.items[1].AreaPostal;
                    if (objCustomerRegistrationList_2BE.items[1].IsCommunication)
                        rblCommunicationAddress.Items[1].Selected = true;

                    txtHouseNoServ.Text = objCustomerRegistrationList_2BE.items[0].HouseNoPostal;
                    txtStreetNameServ.Text = objCustomerRegistrationList_2BE.items[0].StreetPostal;
                    txtCityServ.Text = objCustomerRegistrationList_2BE.items[0].CityPostaL;
                    txtZipServ.Text = objCustomerRegistrationList_2BE.items[0].ZipCodePostal;
                    txtAreaServ.Text = objCustomerRegistrationList_2BE.items[0].AreaPostal;
                    if (objCustomerRegistrationList_2BE.items[0].IsCommunication)
                        rdoCommunicationServ.Items[0].Selected = true;
                    //divServiceAdrsDuplicate.Visible = true;
                    //divServiceAdrsOriginal.Visible = false;
                    hfIsServiceAddress.Value = "true";
                    cbIsSameAsPostal.Attributes.Add("onchange", "OnOffTenentDiv()");

                    txtHouseNoServ.Enabled = false;
                    txtStreetNameServ.Enabled = false;
                    txtCityServ.Enabled = false;
                    txtZipServ.Enabled = false;
                    txtAreaServ.Enabled = false;

                    txtSHNo.Enabled = true;
                    txtSStreet.Enabled = true;
                    txtSLGAVillage.Enabled = true;
                    txtSZip.Enabled = true;
                    txtSArea.Enabled = true;
                    divServiceAdrsDuplicate.Style.Add("display", "none");
                    divServiceAdrsOriginal.Style.Add("display", "inline");
                }
            }
        }
        #endregion

        private void CustomerNotFound()
        {
            divdetails.Visible = false;
            txtAccountNo.Text = string.Empty;
            Message(UMS_Resource.CUSTOMER_NOT_FOUND_MSG, UMS_Resource.MESSAGETYPE_ERROR);
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            UpdateAddress();
            Clear();
            txtAccountNo.Text = string.Empty;
            //ChangeBookNoBe _objBookNoBe = new ChangeBookNoBe();
            //_objBookNoBe.AccountNo = txtAccountNo.Text.Trim();
            //_objBookNoBe.PostalLandMark = txtLandMark.Text;
            //_objBookNoBe.PostalStreet = txtStreet.Text;
            //_objBookNoBe.PostalCity = txtCity.Text;
            //_objBookNoBe.PostalHouseNo = txtHouseNo.Text;
            //_objBookNoBe.PostalZipCode = txtZipCode.Text;
            //_objBookNoBe.ServiceLandMark = txtServiceLandMark.Text;
            //_objBookNoBe.ServiceStreet = txtServiceStreet.Text;
            //_objBookNoBe.ServiceCity = txtServiceCity.Text;
            //_objBookNoBe.ServiceHouseNo = txtServiceHouseNo.Text;
            //_objBookNoBe.ServiceZipCode = txtServiceZipCode.Text;
            //_objBookNoBe.Details = _objiDsignBal.ReplaceNewLines(txtReason.Text, true);
            //_objBookNoBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();


            //bool IsApproval = _objCommonMethods.IsApproval(UMS_Resource.FEATURE_CUST_ADD_CHANGE);
            //if (IsApproval)
            //{
            //    _objBookNoBe.Flag = Convert.ToInt32(Resources.Resource.FLAG_TEMP);
            //    _objBookNoBe = _objiDsignBal.DeserializeFromXml<ChangeBookNoBe>(_objBookNoBal.Insert(_objBookNoBe, Statement.Change));
            //    if (_objBookNoBe.IsSuccess)
            //    {
            //        txtAccountNo.Text = string.Empty;
            //        divdetails.Visible = false;
            //        lblActiveMsg.Text = Resource.APPROVAL_TARIFF_CHANGE;
            //        mpeActivate.Show();
            //        //Message(UMS_Resource.CUSTOMER_UPDATED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
            //    }
            //    else
            //    {
            //        divdetails.Visible = true;
            //        Message(UMS_Resource.CUSTOMER_UPDATED_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            //    }
            //}
            //else
            //{
            //    _objBookNoBe = _objiDsignBal.DeserializeFromXml<ChangeBookNoBe>(_objBookNoBal.Insert(_objBookNoBe, Statement.Change));
            //    if (_objBookNoBe.IsSuccess)
            //    {
            //        txtAccountNo.Text = string.Empty;
            //        divdetails.Visible = false;
            //        lblActiveMsg.Text = Resource.MODIFICATIONS_UPDATED;
            //        mpeActivate.Show();
            //        //Message(UMS_Resource.CUSTOMER_UPDATED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
            //    }
            //    else
            //    {
            //        divdetails.Visible = true;
            //        Message(UMS_Resource.CUSTOMER_UPDATED_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            //    }
            //}

        }
        protected void txtPArea_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPArea.Text)) if (Convert.ToInt32(txtPArea.Text) == 0) txtPArea.Text = string.Empty;
            if (!string.IsNullOrEmpty(txtPArea.Text))
            {
                try
                {
                    objAreaBe.AreaCode = Convert.ToInt32(txtPArea.Text);
                    objAreaBe = _objiDsignBal.DeserializeFromXml<AreaBe>(objAreaBAL.IsAreaExists(objAreaBe));
                    if (!objAreaBe.IsExists)
                    {
                        spnPostalArea.InnerText = "Area code " + txtPArea.Text + " is not available.";
                        spnPostalArea.Attributes.Add("class", "span_color");
                        txtPArea.Text = string.Empty;
                    }
                    else
                    {
                        spnPostalArea.InnerText = "Area code " + txtPArea.Text + " is available.";
                        spnPostalArea.Attributes.Add("class", "span_color_green");
                    }
                }
                catch (Exception ex)
                {
                    Message("Exception in postal area existence check.", UMS_Resource.MESSAGETYPE_ERROR);
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
            }
            else txtPArea.Text = spnPostalArea.InnerHtml = string.Empty;
            if (hfServiceAddressID.Value != "") AddressLayoutStability2();
            else AddressLayoutStability1();
        }
        protected void txtSArea_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtSArea.Text)) if (Convert.ToInt32(txtSArea.Text) == 0) txtSArea.Text = string.Empty;
            if (!string.IsNullOrEmpty(txtSArea.Text))
            {
                try
                {
                    objAreaBe.AreaCode = Convert.ToInt32(txtSArea.Text);
                    objAreaBe = _objiDsignBal.DeserializeFromXml<AreaBe>(objAreaBAL.IsAreaExists(objAreaBe));
                    if (!objAreaBe.IsExists)
                    {
                        spnServericeArea.InnerText = "Area code " + txtSArea.Text + " is not available.";
                        spnServericeArea.Attributes.Add("class", "span_color");
                        txtSArea.Text = string.Empty;
                    }
                    else
                    {
                        spnServericeArea.InnerText = "Area code " + txtSArea.Text + " is available.";
                        spnServericeArea.Attributes.Add("class", "span_color_green");
                    }
                    if (cbIsSameAsPostal.Checked)
                        divServiceAdrsOriginal.Visible = true;
                    else
                        divServiceAdrsDuplicate.Visible = true;
                }
                catch (Exception ex)
                {
                    Message("Exception in service area existence check.", UMS_Resource.MESSAGETYPE_ERROR);
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
            }
            else txtSArea.Text = spnServericeArea.InnerHtml = string.Empty;
            if (hfServiceAddressID.Value != "") AddressLayoutStability2();
            else AddressLayoutStability1();
        }
        protected void txtSAreaServ_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtAreaServ.Text)) if (Convert.ToInt32(txtAreaServ.Text) == 0) txtAreaServ.Text = string.Empty;
            if (!string.IsNullOrEmpty(txtAreaServ.Text))
            {
                try
                {
                    objAreaBe.AreaCode = Convert.ToInt32(txtAreaServ.Text);
                    objAreaBe = _objiDsignBal.DeserializeFromXml<AreaBe>(objAreaBAL.IsAreaExists(objAreaBe));
                    if (!objAreaBe.IsExists)
                    {
                        spnServericeAreaServ.InnerText = "Area code " + txtAreaServ.Text + " is not available.";
                        spnServericeAreaServ.Attributes.Add("class", "span_color");
                        txtAreaServ.Text = string.Empty;
                        rdoCommunicationServ.Items[1].Enabled = true;
                    }
                    else
                    {
                        spnServericeAreaServ.InnerText = "Area code " + txtAreaServ.Text + " is available.";
                        spnServericeAreaServ.Attributes.Add("class", "span_color_green");
                        rdoCommunicationServ.Items[1].Enabled = true;
                    }
                }
                catch (Exception ex)
                {
                    Message("Exception in service area existence check.", UMS_Resource.MESSAGETYPE_ERROR);
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
            }
            else txtAreaServ.Text = spnServericeAreaServ.InnerHtml = string.Empty;
            if (hfServiceAddressID.Value != "") AddressLayoutStability2();
            else AddressLayoutStability1();
        }
        #endregion

        #region Methods
        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public void UpdateAddress()
        {
            CustomerRegistrationBE objCustomerRegistrationBE = new CustomerRegistrationBE();
            objCustomerRegistrationBE.GolbalAccountNumber = lblGlobalAccountNO.Text;
            objCustomerRegistrationBE.HouseNoPostal = txtPHNo.Text;
            objCustomerRegistrationBE.StreetPostal = txtPStreet.Text;
            objCustomerRegistrationBE.CityPostaL = txtPLGAVillage.Text;
            objCustomerRegistrationBE.PZipCode = txtPZip.Text;
            objCustomerRegistrationBE.AreaPostal = txtPArea.Text;
            objCustomerRegistrationBE.Reason = _objiDsignBal.ReplaceNewLines(txtBookReason.Text, true);
            objCustomerRegistrationBE.IsSameAsService = cbIsSameAsPostal.Checked;
            objCustomerRegistrationBE.BookNo = ddlBookNo.SelectedItem.Value;
            objCustomerRegistrationBE.ModifedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            objCustomerRegistrationBE.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerBookNo;
            objCustomerRegistrationBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            if (Convert.ToBoolean(hfIsServiceAddress.Value))
            {
                objCustomerRegistrationBE.HouseNoService = txtSHNo.Text;
                objCustomerRegistrationBE.StreetService = txtSStreet.Text;
                objCustomerRegistrationBE.CityService = txtSLGAVillage.Text;
                objCustomerRegistrationBE.SZipCode = txtSZip.Text;
                objCustomerRegistrationBE.AreaService = txtSArea.Text;
                if (cbIsSameAsPostal.Checked)
                {
                    objCustomerRegistrationBE.IsCommunicationPostal = rdoCommunicationServ.Items[0].Selected;
                    objCustomerRegistrationBE.IsCommunicationService = rdoCommunicationServ.Items[1].Selected;
                }
                else
                {
                    objCustomerRegistrationBE.IsCommunicationPostal = rblCommunicationAddress.Items[0].Selected;
                    objCustomerRegistrationBE.IsCommunicationService = rblCommunicationAddress.Items[1].Selected;
                }
            }
            if (!Convert.ToBoolean(hfIsServiceAddress.Value))
            {
                objCustomerRegistrationBE.HouseNoService = txtHouseNoServ.Text;
                objCustomerRegistrationBE.StreetService = txtStreetNameServ.Text;
                objCustomerRegistrationBE.CityService = txtCityServ.Text;
                objCustomerRegistrationBE.SZipCode = txtZipServ.Text;
                objCustomerRegistrationBE.AreaService = txtAreaServ.Text;
                if (cbIsSameAsPostal.Checked)
                {
                    objCustomerRegistrationBE.IsCommunicationPostal = rblCommunicationAddress.Items[0].Selected;
                    objCustomerRegistrationBE.IsCommunicationService = rblCommunicationAddress.Items[1].Selected;
                }
                else
                {
                    objCustomerRegistrationBE.IsCommunicationPostal = rdoCommunicationServ.Items[0].Selected;
                    objCustomerRegistrationBE.IsCommunicationService = rdoCommunicationServ.Items[1].Selected;
                }
            }

            if (_objCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.ChangeCustomerBookNo, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString()))
            {
                objCustomerRegistrationBE.ApprovalStatusId = (int)EnumApprovalStatus.Approved;
                objCustomerRegistrationBE.IsFinalApproval = true;
                objCustomerRegistrationBE = _objiDsignBal.DeserializeFromXml<CustomerRegistrationBE>(objConsumerBal.ChangeCustomersBookNo(objCustomerRegistrationBE));
                lblActiveMsg.Text = Resource.MODIFICATIONS_UPDATED;
            }
            else
            {
                objCustomerRegistrationBE.ApprovalStatusId = (int)EnumApprovalStatus.Process;
                objCustomerRegistrationBE = _objiDsignBal.DeserializeFromXml<CustomerRegistrationBE>(objConsumerBal.ChangeCustomersBookNo(objCustomerRegistrationBE));
                lblActiveMsg.Text = Resource.APPROVAL_TARIFF_CHANGE;
            }


            if (objCustomerRegistrationBE.IsSuccess)
            {
                txtAccountNo.Text = string.Empty;
                popheader.Attributes.Add("class", "popheader popheaderlblgreen");
                divdetails.Visible = false;
                Clear();
                mpeActivate.Show();
            }
            else if (objCustomerRegistrationBE.IsApprovalInProcess)
            {
                txtAccountNo.Text = string.Empty;
                popheader.Attributes.Add("class", "popheader popheaderlblred");
                divdetails.Visible = false;
                Clear();
                lblActiveMsg.Text = Resource.REQ_STILL_PENDING;
                mpeActivate.Show();
            }
            else if (!objCustomerRegistrationBE.IsSuccess)
            {
                divdetails.Visible = false;
                Message("You should change your BookNo here." + objCustomerRegistrationBE.StatusText, UMS_Resource.MESSAGETYPE_ERROR);
            }
            else
            {
                divdetails.Visible = false;
                Message("There is some problem in " + objCustomerRegistrationBE.StatusText, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        public void AddressLayoutStability1()
        {
            if (cbIsSameAsPostal.Checked)
            {
                divServiceAdrsOriginal.Style.Add("display", "inline");
                divServiceAdrsDuplicate.Style.Add("display", "none");
            }
            else
            {
                divServiceAdrsOriginal.Style.Add("display", "none");
                divServiceAdrsDuplicate.Style.Add("display", "inline");
            }
        }

        public void AddressLayoutStability2()
        {
            if (cbIsSameAsPostal.Checked)
            {
                divServiceAdrsOriginal.Style.Add("display", "none");
                divServiceAdrsDuplicate.Style.Add("display", "inline");
            }
            else
            {
                divServiceAdrsOriginal.Style.Add("display", "inline");
                divServiceAdrsDuplicate.Style.Add("display", "none");
            }
        }
        public void Clear()
        {
            txtBookReason.Text = spnPostalArea.InnerHtml = spnServericeArea.InnerHtml = spnServericeAreaServ.InnerHtml = hfIsServiceAddress.Value =
               hfIsServiceAddress.Value = txtAreaServ.Text = txtCity.Text = txtCityServ.Text = txtHouseNo.Text = txtHouseNoServ.Text = txtLandMark.Text =
               txtPArea.Text = txtPHNo.Text = txtPLGAVillage.Text = txtPReason.Text = txtPStreet.Text = txtPZip.Text = txtReason.Text =
               txtSArea.Text = txtServiceCity.Text = txtServiceHouseNo.Text = txtServiceLandMark.Text = txtServiceStreet.Text =
               txtServiceZipCode.Text = txtSHNo.Text = txtSLGAVillage.Text = txtSStreet.Text = txtStreet.Text = txtStreetNameServ.Text =
               txtSZip.Text = txtZipCode.Text = txtZipServ.Text = string.Empty;
            divServiceAdrsDuplicate.Style.Add("display", "none");
            divServiceAdrsOriginal.Visible = true;
            cbIsSameAsPostal.Checked = false;
            hfIsServiceAddress.Value = "false";
            hfPostalAddressID.Value = "";
            hfServiceAddressID.Value = "";
            divServiceAdrsOriginal.Style.Add("display", "inline");
            divServiceAdrsDuplicate.Style.Add("display", "none");
            ddlBookNo.SelectedIndex = ddlBU.SelectedIndex = ddlCycle.SelectedIndex = ddlSC.SelectedIndex = ddlSU.SelectedIndex = 0;
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region ddlEvents
        //protected void ddlBU_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ddlSU.Items.Clear();
        //        ddlSC.Items.Clear();
        //        ddlCycle.Items.Clear();
        //        ddlBookNo.Items.Clear();
        //        if (ddlBU.SelectedIndex > 0)
        //        {
        //            _objCommonMethods.BindUserServiceUnits_BuIds(ddlSU, ddlBU.SelectedValue, UMS_Resource.DROPDOWN_SELECT, false);
        //            ddlSC.SelectedIndex = ddlCycle.SelectedIndex = ddlBookNo.SelectedIndex = 0;
        //            ddlSU.Enabled = true;
        //            ddlSC.Enabled = ddlCycle.Enabled = ddlBookNo.Enabled = false;

        //        }
        //        else
        //        {
        //            ddlSU.SelectedIndex = ddlSC.SelectedIndex = ddlCycle.SelectedIndex = ddlBookNo.SelectedIndex = 0;
        //            ddlSU.Enabled = ddlSC.Enabled = ddlCycle.Enabled = ddlBookNo.Enabled = false;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void ddlSU_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ddlSC.Items.Clear();
        //        ddlCycle.Items.Clear();
        //        ddlBookNo.Items.Clear();
        //        if (ddlSU.SelectedIndex > 0)
        //        {
        //            _objCommonMethods.BindUserServiceCenters_SuIds(ddlSC, ddlSU.SelectedValue, UMS_Resource.DROPDOWN_SELECT, false);
        //            ddlSC.SelectedIndex = ddlCycle.SelectedIndex = ddlBookNo.SelectedIndex = 0;
        //            ddlSC.Enabled = true;
        //            ddlCycle.Enabled = ddlBookNo.Enabled = false;
        //        }
        //        else
        //        {
        //            ddlSC.SelectedIndex = ddlCycle.SelectedIndex = ddlBookNo.SelectedIndex = 0;
        //            ddlSC.Enabled = ddlCycle.Enabled = ddlBookNo.Enabled = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void ddlSC_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ddlCycle.Items.Clear();
        //        ddlBookNo.Items.Clear();
        //        if (ddlSC.SelectedIndex > 0)
        //        {
        //            _objCommonMethods.BindCyclesByBUSUSC(ddlCycle, ddlBU.SelectedValue, ddlSU.SelectedValue, ddlSC.SelectedValue, true, UMS_Resource.DROPDOWN_SELECT, false);
        //            ddlCycle.SelectedIndex = ddlBookNo.SelectedIndex = 0;
        //            ddlCycle.Enabled = true;
        //            ddlBookNo.Enabled = false;
        //        }

        //        else
        //        {
        //            ddlCycle.SelectedIndex = ddlBookNo.SelectedIndex = 0;
        //            ddlCycle.Enabled = ddlBookNo.Enabled = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void ddlCycle_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ddlBookNo.Items.Clear();
        //    if (ddlCycle.SelectedIndex > 0)
        //    {
        //        _objCommonMethods.BindAllBookNumbers(ddlBookNo, ddlCycle.SelectedValue, true, false, UMS_Resource.DROPDOWN_SELECT);
        //        ddlBookNo.Enabled = true;
        //    }

        //    else
        //    {
        //        ddlBookNo.SelectedIndex = 0;
        //        ddlBookNo.Enabled = false;
        //    }
        //}
        //protected void ddlBookNo_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (ddlCycle.SelectedIndex > 0)
        //    {
        //        txtLandMark.Text = txtStreet.Text = txtCity.Text = txtHouseNo.Text = txtZipCode.Text = string.Empty;
        //        txtServiceLandMark.Text = txtServiceStreet.Text = txtServiceCity.Text = txtServiceHouseNo.Text = txtServiceZipCode.Text = string.Empty;
        //        rbtnPostalAddress.Checked = rbtnServiceAddress.Checked = false;
        //    }
        //}

        protected void ddlBU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlSC.SelectedIndex = ddlCycle.SelectedIndex = ddlBookNo.SelectedIndex = 0;
                if (ddlBU.SelectedIndex > 0)
                {
                    _objCommonMethods.BindUserServiceUnits_BuIds(ddlSU, ddlBU.SelectedValue, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlSU.Enabled = true;
                    ddlSC.Enabled = ddlCycle.Enabled = ddlBookNo.Enabled = false;
                    if (ddlSU.Items.Count > 0)
                        ddlSU.SelectedIndex = 0;
                }
                else
                {
                    if (ddlSU.Items.Count > 0)
                        ddlSU.SelectedIndex = 0;
                    if (ddlSC.Items.Count > 0)
                        ddlSC.SelectedIndex = 0;
                    if (ddlCycle.Items.Count > 0)
                        ddlCycle.SelectedIndex = 0;
                    if (ddlBookNo.Items.Count > 0)
                        ddlBookNo.SelectedIndex = 0;
                    ddlSU.Enabled = ddlSC.Enabled = ddlCycle.Enabled = ddlBookNo.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlSU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlCycle.SelectedIndex = ddlBookNo.SelectedIndex = 0;
                if (ddlSU.SelectedIndex > 0)
                {
                    _objCommonMethods.BindUserServiceCenters_SuIds(ddlSC, ddlSU.SelectedValue, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlSC.Enabled = true;
                    ddlCycle.Enabled = ddlBookNo.Enabled = false;
                }
                else
                {
                    if (ddlSC.Items.Count > 0)
                        ddlSC.SelectedIndex = 0;
                    if (ddlCycle.Items.Count > 0)
                        ddlCycle.SelectedIndex = 0;
                    if (ddlBookNo.Items.Count > 0)
                        ddlBookNo.SelectedIndex = 0;
                    ddlSC.Enabled = ddlCycle.Enabled = ddlBookNo.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlBookNo.SelectedIndex = 0;
                if (ddlSC.SelectedIndex > 0)
                {
                    _objCommonMethods.BindCyclesBySC_HavingBooks(ddlCycle, ddlSC.SelectedValue, true, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlCycle.Enabled = true;
                    ddlBookNo.Enabled = false;
                }
                else
                {
                    if (ddlCycle.Items.Count > 0)
                        ddlCycle.SelectedIndex = 0;
                    if (ddlBookNo.Items.Count > 0)
                        ddlBookNo.SelectedIndex = 0;
                    ddlCycle.Enabled = ddlBookNo.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlCycle_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlCycle.SelectedIndex > 0)
                {
                    _objCommonMethods.BindAllBookNumbersWithDetails(ddlBookNo, ddlCycle.SelectedValue, true, false, UMS_Resource.DROPDOWN_SELECT);
                    ddlBookNo.Enabled = true;
                }
                else
                {
                    if (ddlBookNo.Items.Count > 0)
                        ddlBookNo.SelectedIndex = 0;
                    ddlBookNo.SelectedIndex = 0;
                    ddlBookNo.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}