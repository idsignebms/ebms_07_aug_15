﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using iDSignHelper;
using UMS_NigeriaBAL;
using System.Xml;
using System.Configuration;
using System.IO;
using UMS_NigeriaBE;
using System.Data.SqlClient;
using System.Data.OleDb;
using Resources;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class CustomerBulkUpload : System.Web.UI.Page
    {
        #region Members
        CommonMethods _objCommonMethods = new CommonMethods();
        iDsignBAL _objIdsignBal = new iDsignBAL();
        UserManagementBAL objUserManagementBAL = new UserManagementBAL();

        string Key = "AddEmployee";
        XmlDocument xml = null;
        public int PageNum;
        string Filename;
        DataTable dtExcel = new DataTable();
        DataTable dtValid = new DataTable();
        DataTable dtInvalid = new DataTable();
        BillingBE objBillingBE = new BillingBE();
        ConsumerBal objConsumerBal = new ConsumerBal();
        DataTable dtAccountNo = new DataTable();
        DataSet ds = new DataSet("ConsumerDetails");
        DataTable dtBulkCopy = new DataTable();
        // DataTable dtCustDetails = new DataTable();
        DataSet dsValidationTables = new DataSet();

        #endregion

        #region Properties
        public string BatchNo
        {
            get { return txtBatchNo.Text.Trim(); }
            set { txtBatchNo.Text = value; }
        }
        public string BatchName
        {
            get { return txtBatchName.Text.Trim(); }
            set { txtBatchName.Text = value; }
        }
        public string Details
        {
            get { return txtDetails.Text.Trim(); }
            set { txtDetails.Text = value; }
        }
        public string BatchDate
        {
            get { return txtBatchDate.Text.Trim(); }
            set { txtBatchDate.Text = value; }
        }
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[Resources.UMS_Resource.SESSION_LOGINID] != null)
            {
                lblMessage.Text = pnlMessage.CssClass = string.Empty;
                if (!IsPostBack)
                {
                    divBrowseDoc.Visible = false;
                    divValidateData.Visible = false;
                }
            }
            else
                Response.Redirect(Resources.UMS_Resource.DEFAULT_PAGE);
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            divBrowseDoc.Visible = true;
            divValidateData.Visible = false;
            divInValidPrint.Visible = false;
        }

        protected void btnUploadNext_Click(object sender, EventArgs e)
        {
            ReadUploads();
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            ds.Tables.Add(dtBulkCopy);
            dtBulkCopy = null;
            if (Session["dtValid"] != null)
            {
                dtBulkCopy = (DataTable)Session["dtValid"];
                if (dtBulkCopy.Rows.Count > 0)
                {
                    if (AddBatchDetailsToDT().Rows.Count > 0)
                    {
                        ds.Tables.Add(dtBulkCopy);
                        ds.Tables.Add(AddBatchDetailsToDT());
                        //BulkCopy(dtBulkCopy, "Tbl_CustomerDetails", 500);
                        try
                        {
                            if (CustomerBulkUploads(ds) > 0)
                            {
                                UploadFinsish();
                                Message(Resources.Resource.CUST_SUCSF_UPL, Resources.UMS_Resource.MESSAGETYPE_SUCCESS);
                            }
                            else
                            {
                                Message(Resources.Resource.CUST_NOT_UPLD, Resources.UMS_Resource.MESSAGETYPE_ERROR);
                            }
                        }
                        catch
                        {
                            Message(Resources.Resource.EXCP_OCRD, Resources.UMS_Resource.MESSAGETYPE_ERROR);
                        }
                    }
                    else
                    {
                        Message(Resources.Resource.BATCH_CRE_EXCP, Resources.UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
                else
                {
                    lblGenMessage.Text = Resources.Resource.NO_ROWS_FOR_UPLD;
                    mpeGenMessage.Show();
                }
            }
            else
            {
                Message(Resources.Resource.UPL_SES_EXP, Resources.UMS_Resource.MESSAGETYPE_ERROR);
            }

        }

        protected void txtBatchNo_TextChanged(object sender, EventArgs e)
        {
            objBillingBE.BatchNo = Convert.ToInt32(BatchNo);
            objBillingBE = _objIdsignBal.DeserializeFromXml<BillingBE>(objConsumerBal.IsExistsBatchCustBulkUploadBAL(objBillingBE));
            if (objBillingBE.IsExists)
            {
                //UploadFinsish();
                Message(Resources.Resource.BTCH_EXISTS, Resources.UMS_Resource.MESSAGETYPE_SUCCESS);
                txtBatchName.Focus();
            }
            else
                txtBatchName.Focus();
        }

        #endregion

        #region Methods

        public void ReadUploads()
        {
            int count = 0;
            DataColumn Comments = new DataColumn("Comments", typeof(string));
            DataColumn IsValid = new DataColumn("IsValid", typeof(bool));
            string fileName = InsertExcel();

            if (Path.GetExtension(fileName) == ".xls" || Path.GetExtension(fileName) == ".xlsx")
            {
                //dtExcel = _objCommonMethods.ExcelImport(Server.MapPath(ConfigurationManager.AppSettings["BulkCustomerUpload"].ToString()) + fileName);
                DataSet dsHeaders = new DataSet();
                dsHeaders.ReadXml(Server.MapPath(ConfigurationSettings.AppSettings["CustomerBulkUploadHeaders"]));
                //Import excel with oledb starts
                string FolderPath = ConfigurationManager.AppSettings["BulkCustomerUpload"];
                string FilePath = Server.MapPath(FolderPath + fileName);
                //string cnstr = "Provider=Microsoft.Jet.Oledb.4.0;Data Source=" + FilePath + "; Extended Properties=Excel 4.0";///upto 2007
                String cnstr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FilePath + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";//2007 Onwards


                OleDbConnection oledbConn = new OleDbConnection(cnstr);
                String[] s = _objCommonMethods.GetExcelSheetNames(FilePath);
                string strSQL = "SELECT * FROM [" + s[0] + "]";
                OleDbCommand cmd = new OleDbCommand(strSQL, oledbConn);
                DataSet ds = new DataSet();
                OleDbDataAdapter odapt = new OleDbDataAdapter(cmd);
                odapt.Fill(ds);
                dtExcel = ds.Tables[0];
                //Import excel with oledb ends


                for (int i = 0; i < dtExcel.Columns.Count; i++)
                {
                    if (dtExcel.Columns.Count == dsHeaders.Tables[0].Rows.Count)
                    {
                        string HeaderName = dtExcel.Columns[i].ColumnName;
                        var result = (from x in dsHeaders.Tables[0].AsEnumerable()
                                      where x.Field<string>("Names").Trim() == HeaderName.Trim()
                                      select new
                                      {
                                          Names = x.Field<string>("Names")
                                      }).SingleOrDefault();
                        if (result == null)
                        {
                            count++;
                            //lblMsgPopup.Text = Resource.COLUMN_NOT_EXISTS;
                            lblGenMessage.Text =Resources.Resource.COL_DSNT_MATCH;
                            mpeGenMessage.Show();
                            break;
                        }
                    }
                    else
                    {
                        count++;
                        lblGenMessage.Text = Resources.Resource.COL_DSNT_MATCH;
                        mpeGenMessage.Show();
                    }
                }
                if (count == 0)
                {
                    //for (int i = dtExcel.Rows.Count - 1; i >= 0; i--)
                    //    if (dtExcel.Rows[i]["BookNo"].ToString().Trim() == "")
                    //        dtExcel.Rows.RemoveAt(i);

                    if (dtExcel.Rows.Count > 0)
                    {
                        dtExcel.Columns.Add(Comments);
                        dtExcel.Columns.Add(IsValid);
                    }

                    #region Rows validation loop

                    ConsumerListBe buConsumerListBE = new ConsumerListBe();

                    CustomerBulkBe objCustomerBulkBe = new CustomerBulkBe();
                    objCustomerBulkBe.BUID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                    dsValidationTables = objConsumerBal.GetCustDetailBlkUpldBAL(objCustomerBulkBe);
                    //dsValidationTables = objConsumerBal.GetCustDetailBlkUpldBAL();

                    foreach (DataRow dr in dtExcel.Rows)
                    {
                        RowValidation(dr);
                    }
                    #endregion

                    if (dtExcel.Rows.Count > 0)
                    {
                        #region converting to ienumerable for filter valid and invalid data by linq
                        IEnumerable<ConsumerBe> result = (from lists in dtExcel.AsEnumerable()
                                                          where lists.Field<bool>("IsValid") == true
                                                          select new ConsumerBe
                                                          {
                                                              Name = lists.Field<string>("Name")
                                                          }
                                                         );
                        #endregion

                        #region Preparation of list for grid binding

                        var listOfValidData = from dtLIst in dtExcel.AsEnumerable()
                                              where dtLIst.Field<bool>("IsValid") == true
                                              select dtLIst;


                        var listOfInvalidData = from dtLIst in dtExcel.AsEnumerable()
                                                where dtLIst.Field<bool>("IsValid") == false
                                                select dtLIst;
                        #endregion

                        #region Datatable preparation for bulkcopy

                        if (listOfValidData.Count() > 0)
                        {
                            dtValid = listOfValidData.CopyToDataTable();

                            Session["dtValid"] = dtValid;

                            #region Bind of valid grid
                            gvValidData.DataSource = listOfValidData.CopyToDataTable();
                            gvValidData.DataBind();
                            #endregion

                            btnUpload.Visible = true;
                        }
                        else
                        {
                            gvValidData.DataSource = new DataTable();
                            gvValidData.DataBind();
                            btnUpload.Visible = false;
                        }

                        #endregion

                        #region Bind of invalid grid
                        if (listOfInvalidData.Count() > 0)
                        {
                            gvInValidData.DataSource = listOfInvalidData.CopyToDataTable();
                            gvInValidData.DataBind();
                        }
                        else
                        {
                            gvInValidData.DataSource = new DataTable();
                            gvInValidData.DataBind();
                        }

                        #endregion

                        UploadStart();
                    }
                    else
                    {
                        lblGenMessage.Text = Resources.Resource.NO_DATA;
                        mpeGenMessage.Show();
                    }

                }
                else
                {
                    lblGenMessage.Text = Resources.Resource.COL_DSNT_MATCH; ;
                    mpeGenMessage.Show();
                }
            }
            else
            {
                lblGenMessage.Text = Resources.Resource.XLS_VALID;
                mpeGenMessage.Show();
            }
        }

        public string InsertExcel()
        {
            try
            {
                string ExcelModifiedFileName;

                if (upDOCExcel.HasFile)
                {
                    string ExcelFileName = System.IO.Path.GetFileName(upDOCExcel.FileName).ToString();//Assinging FileName
                    string ExcelExtension = Path.GetExtension(ExcelFileName);//Storing Extension of file into variable Extension
                    TimeZoneInfo ExcelIND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                    DateTime ExcelcurrentTime = DateTime.Now;
                    DateTime Exceloourtime = TimeZoneInfo.ConvertTime(ExcelcurrentTime, ExcelIND_ZONE);
                    string ExcelCurrentDate = Exceloourtime.ToString("dd-MM-yyyy_hh-mm-ss");
                    ExcelModifiedFileName = Path.GetFileNameWithoutExtension(ExcelFileName) + "_" + ExcelCurrentDate + ExcelExtension;//Setting File Name to store it in database
                    Filename = Path.GetFileNameWithoutExtension(ExcelFileName) + "_" + ExcelCurrentDate + ExcelExtension;//Setting File Name to store it in database
                    upDOCExcel.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["BulkCustomerUpload"].ToString()) + ExcelModifiedFileName);//Saving File in to the server.

                }
                return Filename;

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public DataRow RowValidation(DataRow dr)
        {
            dr["IsValid"] = "true";
            if (!IsInt(dr["MobileNo"].ToString()))
            {
                dr["Comments"] = "Mobile no incorrect." + Environment.NewLine;
                dr["IsValid"] = "false";
            }
            if (string.IsNullOrEmpty(dr["Name"].ToString().Trim()))
            {
                dr["Comments"] += "Name is empty." + Environment.NewLine;
                dr["IsValid"] = "false";

            }
            if (string.IsNullOrEmpty(dr["PostalStreet"].ToString().Trim()))
            {
                dr["Comments"] += "PostalStreet is empty.." + Environment.NewLine;
                dr["IsValid"] = "false";

            }
            if (string.IsNullOrEmpty(dr["PostalHouseNo"].ToString().Trim()))
            {
                dr["Comments"] += "PostalHouseNo is empty.." + Environment.NewLine;
                dr["IsValid"] = "false";

            }
            if (string.IsNullOrEmpty(dr["BookNo"].ToString().Trim()))
            {
                dr["Comments"] += "BookNo is empty.." + Environment.NewLine;
                dr["IsValid"] = "false";

            }
            if (string.IsNullOrEmpty(dr["ServiceStreet"].ToString().Trim()))
            {
                dr["Comments"] += "ServiceStreet is empty.." + Environment.NewLine;
                dr["IsValid"] = "false";

            }
            if (string.IsNullOrEmpty(dr["ServiceHouseNo"].ToString().Trim()))
            {
                dr["Comments"] += "ServiceHouseNo is empty.." + Environment.NewLine;
                dr["IsValid"] = "false";

            }
            if (string.IsNullOrEmpty(dr["MobileNo"].ToString().Trim()))
            {
                dr["Comments"] += "MobileNo is empty.." + Environment.NewLine;
                dr["IsValid"] = "false";
            }
            if (string.IsNullOrEmpty(dr["AccountTypeId"].ToString().Trim()))
            {
                dr["Comments"] += "AccountTypeId is empty.." + Environment.NewLine;
                dr["IsValid"] = "false";
            }
            if (string.IsNullOrEmpty(dr["ReadCodeId"].ToString().Trim()))
            {
                dr["Comments"] += "ReadCodeId is empty.." + Environment.NewLine;
                dr["IsValid"] = "false";
            }
            //string ReadCode = string.IsNullOrEmpty(dr["ReadCodeId"].ToString().Trim()).ToString();
            //if (ReadCode=="2")
            //{
                if (string.IsNullOrEmpty(dr["MeterNo"].ToString().Trim()))
                {
                    dr["Comments"] += "MeterNo is empty.." + Environment.NewLine;
                    dr["IsValid"] = "false";
                }
                if (string.IsNullOrEmpty(dr["MeterTypeId"].ToString().Trim()))
                {
                    dr["Comments"] += "MeterTypeId is empty.." + Environment.NewLine;
                    dr["IsValid"] = "false";
                }
                if (string.IsNullOrEmpty(dr["PhaseId"].ToString().Trim()))
                {
                    dr["Comments"] += "PhaseId is empty.." + Environment.NewLine;
                    dr["IsValid"] = "false";
                }
                if (!string.IsNullOrEmpty(dr["OldAccountNo"].ToString().Trim()))
                {
                    if (IsOldAccountNoExists(dr["OldAccountNo"].ToString().Trim()))
                    {
                        dr["Comments"] += "Old account no. is exists." + Environment.NewLine;
                        dr["IsValid"] = "false";
                    }
                }
                if (!string.IsNullOrEmpty(dr["MeterNo"].ToString().Trim()))
                {
                    if (IsMeterNoExists(dr["MeterNo"].ToString().Trim()))
                    {
                        dr["Comments"] += "Meter no. is exists." + Environment.NewLine;
                        dr["IsValid"] = "false";
                    }
                }

                if (!string.IsNullOrEmpty(dr["PhaseId"].ToString().Trim()))
                {
                    if (!IsPhaseIdValid(dr["PhaseId"].ToString().Trim()))
                    {
                        dr["Comments"] += "PhaseId is not exists." + Environment.NewLine;
                        dr["IsValid"] = "false";
                    }
                }

                if (!string.IsNullOrEmpty(dr["MeterStatusId"].ToString().Trim()))
                {
                    if (!IsMeterStatusIdValid(dr["MeterStatusId"].ToString().Trim()))
                    {
                        dr["Comments"] += "MeterStatusId is not exists." + Environment.NewLine;
                        dr["IsValid"] = "false";
                    }
                }
                if (!string.IsNullOrEmpty(dr["IdentityId"].ToString().Trim()))
                {
                    if (!IsIdentityIdValid(dr["IdentityId"].ToString().Trim()))
                    {
                        dr["Comments"] += "IdentityId is not exists." + Environment.NewLine;
                        dr["IsValid"] = "false";
                    }
                }
                if (!string.IsNullOrEmpty(dr["MeterTypeId"].ToString().Trim()))
                {
                    if (!IsMeterTypeIdValid(dr["MeterTypeId"].ToString().Trim()))
                    {
                        dr["Comments"] += "MeterTypeId is not exists." + Environment.NewLine;
                        dr["IsValid"] = "false";
                    }
                }
            //}
            if (!string.IsNullOrEmpty(dr["AccountTypeId"].ToString().Trim()))
            {
                if (!IsAccountTypeIdValid(dr["AccountTypeId"].ToString().Trim()))
                {
                    dr["Comments"] += "AccountTypeId is not exists." + Environment.NewLine;
                    dr["IsValid"] = "false";
                }
            }
            if (!string.IsNullOrEmpty(dr["TariffId"].ToString().Trim()))
            {
                if (!IsTariffIdValid(dr["TariffId"].ToString().Trim()))
                {
                    dr["Comments"] += "TariffId is not exists." + Environment.NewLine;
                    dr["IsValid"] = "false";
                }
            }
           
            return dr;
        }

        public bool IsInt(string str)
        {
            try
            {
                Convert.ToInt64(str);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void BulkCopy(DataTable dataTable, string DestinationTbl, int batchSize)
        {

            if (dataTable.Rows.Count > 0)
            {
                // Get the DataTable 
                string sqlCon = ConfigurationManager.AppSettings["Sqlcon"].ToString();
                DataTable dtInsertRows = dataTable;

                using (SqlBulkCopy sbc = new SqlBulkCopy(sqlCon, SqlBulkCopyOptions.KeepIdentity))
                {
                    sbc.DestinationTableName = DestinationTbl;

                    // Number of records to be processed in one go
                    sbc.BatchSize = batchSize;
                    sbc.ColumnMappings.Capacity = 100;
                    //sbc.ColumnMappings.Add("", "");

                    // Add your column mappings here
                    sbc.ColumnMappings.Add("Name", "Name");
                    sbc.ColumnMappings.Add("SurName", "SurName");
                    sbc.ColumnMappings.Add("EmailId", "EmailId");
                    sbc.ColumnMappings.Add("DocumentNo", "DocumentNo");
                    sbc.ColumnMappings.Add("OtherContactNo", "OtherContactNo");
                    sbc.ColumnMappings.Add("PostalLandMark", "PostalLandMark");
                    sbc.ColumnMappings.Add("PostalStreet", "PostalStreet");
                    sbc.ColumnMappings.Add("PostalCity", "PostalCity");
                    sbc.ColumnMappings.Add("PostalHouseNo", "PostalHouseNo");
                    sbc.ColumnMappings.Add("PostalDetails", "PostalDetails");
                    sbc.ColumnMappings.Add("PostalZipCode", "PostalZipCode");
                    sbc.ColumnMappings.Add("IdentityNo", "IdentityNo");
                    sbc.ColumnMappings.Add("IdentityId", "IdentityId");
                    sbc.ColumnMappings.Add("BookNo", "BookNo");
                    sbc.ColumnMappings.Add("ServiceLandMark", "ServiceLandMark");
                    sbc.ColumnMappings.Add("ServiceStreet", "ServiceStreet");
                    sbc.ColumnMappings.Add("ServiceCity", "ServiceCity");
                    sbc.ColumnMappings.Add("ServiceHouseNo", "ServiceHouseNo");
                    sbc.ColumnMappings.Add("ServiceDetails", "ServiceDetails");
                    sbc.ColumnMappings.Add("ServiceZipCode", "ServiceZipCode");
                    sbc.ColumnMappings.Add("ServiceEmailId", "ServiceEmailId");
                    sbc.ColumnMappings.Add("MobileNo", "MobileNo");
                    sbc.ColumnMappings.Add("TariffId", "TariffId");
                    sbc.ColumnMappings.Add("MultiplicationFactor", "MultiplicationFactor");
                    sbc.ColumnMappings.Add("AccountTypeId", "AccountTypeId");
                    sbc.ColumnMappings.Add("ApplicationDate", "ApplicationDate");
                    sbc.ColumnMappings.Add("ReadCodeId", "ReadCodeId");
                    sbc.ColumnMappings.Add("OldAccountNo", "OldAccountNo");
                    sbc.ColumnMappings.Add("ConnectionDate", "ConnectionDate");
                    sbc.ColumnMappings.Add("MeterNo", "MeterNo");
                    sbc.ColumnMappings.Add("MeterSerialNo", "MeterSerialNo");
                    sbc.ColumnMappings.Add("MeterTypeId", "MeterTypeId");
                    sbc.ColumnMappings.Add("PhaseId", "PhaseId");
                    sbc.ColumnMappings.Add("MeterStatusId", "MeterStatusId");
                    sbc.ColumnMappings.Add("InitialReading", "InitialReading");
                    sbc.ColumnMappings.Add("CurrentReading", "CurrentReading");
                    sbc.ColumnMappings.Add("MinimumReading", "MinimumReading");
                    //sbc.ColumnMappings.Add("CustomerUniqueNo", "CustomerUniqueNo");


                    // Finally write to server
                    sbc.WriteToServer(dtInsertRows);
                }
            }
            else
            {
                lblGenMessage.Text = Resources.Resource.UPL_SES_EXP;
                mpeGenMessage.Show();
            }
        }

        public int InsertBatch()
        {
            objBillingBE.BatchNo = Convert.ToInt32(BatchNo);
            objBillingBE.BatchName = BatchName;
            objBillingBE.Description = Details;
            objBillingBE.BatchDate = _objCommonMethods.Convert_MMDDYY(BatchDate);
            objBillingBE.CreatedBy = Session[Resources.UMS_Resource.SESSION_LOGINID].ToString();
            objBillingBE = _objIdsignBal.DeserializeFromXml<BillingBE>(objConsumerBal.InsertBatchCustBulkUploadBAL(objBillingBE));
            return objBillingBE.RowCount;
        }

        public int CustomerBulkUploads(DataSet ds)
        {
            objBillingBE = _objIdsignBal.DeserializeFromXml<BillingBE>(objConsumerBal.CustomerBulkUploadBAL(ds));
            return objBillingBE.RowCount;
        }

        public void UploadFinsish()
        {
            BatchDate = string.Empty;
            BatchName = string.Empty;
            BatchNo = string.Empty;
            Details = string.Empty;
            divBatchDetails.Visible = true;
            divBrowseDoc.Visible = false;
            divValidateData.Visible = false;
            divInValidPrint.Visible = false;

        }

        public void UploadStart()
        {
            divBrowseDoc.Visible = false;
            divBatchDetails.Visible = true;
            divValidateData.Visible = true;
            divInValidPrint.Visible = true;
        }

        public DataTable AddBatchDetailsToDT()
        {
            DataTable dtBatch = new DataTable();
            dtBatch.Columns.Add("BatchNo", typeof(int));
            dtBatch.Columns.Add("BatchName", typeof(string));
            dtBatch.Columns.Add("Description", typeof(string));
            dtBatch.Columns.Add("BatchDate", typeof(string));
            dtBatch.Columns.Add("CreatedBy", typeof(string));
            DataRow dr = dtBatch.NewRow();
            dr["BatchNo"] = Convert.ToInt32(BatchNo);
            dr["BatchName"] = BatchName;
            dr["Description"] = Details;
            dr["BatchDate"] = _objCommonMethods.Convert_MMDDYY(BatchDate);
            dr["CreatedBy"] = Session[Resources.UMS_Resource.SESSION_LOGINID].ToString();
            dtBatch.Rows.Add(dr);
            return dtBatch;
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {

                throw Ex;
            }
        }

        public bool IsOldAccountNoExists(string OldAccountNo)
        {
            if (OldAccountNo == "NULL")
                OldAccountNo = string.Empty;
            DataTable dt = dsValidationTables.Tables[0];
            bool exists = false;
            if (dt.Rows.Count > 0)
            {

                var lists = (from x in dt.AsEnumerable() where x.Field<string>("OldAccountNo") == (OldAccountNo) select x);
                if (lists.Count() > 0)
                {
                    exists = true;
                }
            }
            return exists;
        }

        #region Validation Method Starts

        public bool IsMeterNoExists(string MeterNo)
        {
            if (MeterNo == "NULL")
                MeterNo = string.Empty;
            DataTable dt = dsValidationTables.Tables[0];
            bool exists = false;
            if (dt.Rows.Count > 0)
            {
                try
                {
                    var lists = from x in dt.AsEnumerable() where x.Field<string>("MeterNo") == (MeterNo) select x;
                    if (lists.Count() > 0)
                    {
                        exists = true;
                    }
                }
                catch
                {
                    exists = false;
                }
            }
            return exists;
        }

        public bool IsPhaseIdValid(string PhaseID)
        {
            DataTable dt = null;
            if (PhaseID == "NULL")
                PhaseID = "0";
            if (dsValidationTables.Tables.Count >= 2)
                dt = dsValidationTables.Tables[1];
            bool exists = false;
            if (dt.Rows.Count > 0)
            {
                try
                {
                    var lists = from x in dt.AsEnumerable() where x.Field<int>("PhaseID") == Convert.ToInt32((PhaseID)) select x;
                    if (lists.Count() > 0)
                    {
                        exists = true;
                    }
                }
                catch
                {
                    exists = false;
                }
            }
            return exists;
        }

        public bool IsMeterStatusIdValid(string MeterStatusId)
        {
            DataTable dt = null;
            if (MeterStatusId == "NULL")
                MeterStatusId = "0";
            if (dsValidationTables.Tables.Count >= 3)
                dt = dsValidationTables.Tables[2];
            bool exists = false;
            if (dt.Rows.Count > 0)
            {
                try
                {
                    var lists = from x in dt.AsEnumerable() where x.Field<int>("MeterStatusId") == Convert.ToInt32((MeterStatusId)) select x;
                    if (lists.Count() > 0)
                    {
                        exists = true;
                    }
                }
                catch
                {
                    exists = false;
                }
            }
            return exists;
        }

        public bool IsIdentityIdValid(string IdentityId)
        {
            DataTable dt = null;
            if (IdentityId == "NULL")
                IdentityId = "0";
            if (dsValidationTables.Tables.Count >= 4)
                dt = dsValidationTables.Tables[3];
            bool exists = false;
            if (dt.Rows.Count > 0)
            {
                try
                {
                    var lists = from x in dt.AsEnumerable() where x.Field<int>("IdentityId") == Convert.ToInt32((IdentityId)) select x;
                    if (lists.Count() > 0)
                    {
                        exists = true;
                    }
                }
                catch
                {
                    exists = false;
                }
            }
            return exists;
        }

        public bool IsAccountTypeIdValid(string AccountTypeId)
        {
            DataTable dt = null;
            if (AccountTypeId == "NULL")
                AccountTypeId = string.Empty;
            if (dsValidationTables.Tables.Count >= 5)
                dt = dsValidationTables.Tables[4];
            bool exists = false;
            if (dt.Rows.Count > 0)
            {
                try
                {
                    var lists = from x in dt.AsEnumerable() where x.Field<int>("AccountTypeId") == Convert.ToInt32((AccountTypeId)) select x;
                    if (lists.Count() > 0)
                    {
                        exists = true;
                    }
                }
                catch
                {
                    exists = false;
                }

            }
            return exists;
        }

        public bool IsTariffIdValid(string TariffId)
        {
            DataTable dt = null;
            if (TariffId == "NULL")
                TariffId = "0";
            if (dsValidationTables.Tables.Count >= 6)
                dt = dsValidationTables.Tables[5];
            bool exists = false;
            if (dt.Rows.Count > 0)
            {
                try
                {
                    var lists = from x in dt.AsEnumerable() where x.Field<int>("TariffId") == Convert.ToInt32((TariffId)) select x;
                    if (lists.Count() > 0)
                    {
                        exists = true;
                    }
                }
                catch
                {
                    exists = false;
                }
            }
            return exists;
        }

        public bool IsMeterTypeIdValid(string MeterTypeId)
        {
            DataTable dt = null;
            if (MeterTypeId == "NULL")
                MeterTypeId = "0";
            if (dsValidationTables.Tables.Count >= 7)
                dt = dsValidationTables.Tables[6];
            bool exists = false;
            if (dt.Rows.Count > 0)
            {
                try
                {
                    var lists = from x in dt.AsEnumerable() where x.Field<int>("MeterTypeId") == Convert.ToInt32((MeterTypeId)) select x;
                    if (lists.Count() > 0)
                    {
                        exists = true;
                    }
                }
                catch
                {
                    exists = false;
                }
            }
            return exists;
        }

        #endregion

        #endregion

    }
}