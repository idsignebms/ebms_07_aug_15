﻿<%@ Page Title="::Customer Details::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    AutoEventWireup="true" CodeBehind="CustomerDetails.aspx.cs" Inherits="UMS_Nigeria.ConsumerManagement.CustomerDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../App_Themes/Green/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function pageLoad() {
            DisplayMessage('UMSNigeriaBody_pnlMessage');
        }; 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:UpdateProgress ID="UpdateProgress" runat="server">
                <ProgressTemplate>
                    <center>
                        <div id="loading-div" class="pbloading">
                            <div id="title-loading" class="pbtitle">
                                BEDC</div>
                            <center>
                                <img src="../images/loading.GIF" class="pbimg">
                            </center>
                            <p class="pbtxt">
                                Loading Please wait.....</p>
                        </div>
                    </center>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
                PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="pnlMessage" runat="server">
                <asp:Label ID="lblMessage" runat="server" Text="--"></asp:Label>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="wizard" class="swMain">
        <%----------------------------------------------------------Step 1----------------------------------------- --%>
        <div>
            <div class="out-bor">
                <div class="inner-sec">
                    <div class="text_total" style="margin-bottom: -9px;">
                        <div class="text-heading">
                            <asp:Literal ID="litAddState" runat="server" Text="CUSTOMER DETAILS"></asp:Literal>
                        </div>
                        <div style="width: 21px; float: right; font-size: 12px; position: relative; top: -20px;">
                            <a href="SearchCustomer.aspx" title="Back" style="display: none;">
                                <asp:Label ID="lblBack" runat="server" Text='<%$ Resources:Resource, BACK%>'></asp:Label></a></div>
                        <div class="star_text">
                            <%-- <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>--%>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <br />
                    <div class="dot-line">
                    </div>
                    <div class="previous">
                        <a href="AdvancedSearchCustomer.aspx">
                            <asp:Label ID="lbtnBack" runat="server" Text='<%$ Resources:Resource, BACK %>'></asp:Label></a>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-sec">
                        <h4 class="subHeading">
                            <asp:Literal ID="Literal35" runat="server" Text="BOOK INFORMATION"></asp:Literal>
                        </h4>
                        <div class="out-bor_aDetails">
                            <table class="customerdetailsTamle">
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal37" runat="server" Text="Business Unit"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblBuDsp" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal36" runat="server" Text="Service Unit"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblSUDsp" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal38" runat="server" Text="Service Center"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblSCDsp" runat="server" Text="--"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal39" runat="server" Text="Book Group"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblBookGroupDsp" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="Literal40" runat="server" Text="Book Number"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblBookCodeDsp" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                </td>
                                            </tr>
                                        </table>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <%--<div class="inner-box1">--%>
                      <h4 class="subHeading">
                                        <asp:Literal ID="Literal41" runat="server" Text="ACCOUNT STATUS DETAILS"></asp:Literal>
                                    </h4>
                    <div class="out-bor_aDetails">
                        <table class="customerdetailsTamle" ><%--style="margin-left: -20px;"--%>
                            <tr>
                                <td class="customerdetailstamleTd-two" ><%--style="width: 50%"--%>
                                    <asp:Literal ID="Literal4" runat="server" Text="Global Account No"></asp:Literal>
                                </td>
                                <td class="customerdetailstamleTd-dot">
                                    :
                                </td>
                                <td class="customerdetailstamleTdsize-two"><%--class="customerdetailstamleTdsize"--%>
                                    <asp:Label ID="lblGlobalAccountNo" runat="server" Text="--"></asp:Label>
                                </td>
                                <td class="customerdetailstamleTd-two" ><%--style="width: 50%"--%>
                                    <asp:Literal ID="litAccountNo" runat="server" Text="Account No"></asp:Literal>
                                </td>
                                <td class="customerdetailstamleTd-dot">
                                    :
                                </td>
                                <td class="customerdetailstamleTdsize-two">
                                    <asp:Label ID="lblAccountNo" runat="server" Text="--"></asp:Label>
                                </td>
                                <td class="customerdetailstamleTd-two" ><%--style="width: 50%"--%>
                                    <%--<asp:Literal ID="litAccountNo" runat="server" Text="Account No"></asp:Literal>--%>
                                </td>
                                <td class="customerdetailstamleTd-dot">
                                    <%--:--%>
                                </td>
                                <td class="customerdetailstamleTdsize-two">
                                    <%--<asp:Label ID="lblAccountNo" runat="server" Text="--"></asp:Label>--%>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clr">
                    </div>
                    <%--<div class="inner-box1">--%>
                    <div class="out-bor_aDetails">
                        <table class="customerdetailsTamle">
                            <tr>
                                <td class="customerdetailstamleTd-two">
                                    <asp:Literal ID="litCustomerType" runat="server" Text="<%$ Resources:Resource, CUSTOMER_TYPE%>"></asp:Literal>
                                </td>
                                <td class="customerdetailstamleTd-dot">
                                    :
                                </td>
                                <%--<td class="customerdetailstamleTdsize">--%>
                                <td class="customerdetailstamleTdsize-two">
                                    <asp:Label ID="lblCustomerType" runat="server" Text="--"></asp:Label>
                                </td>
                                <td class="customerdetailstamleTd-two">
                                    <asp:Literal ID="litCustomerStatus" runat="server" Text="<%$ Resources:Resource, STATUS%>"></asp:Literal>
                                </td>
                                <td class="customerdetailstamleTd-dot">
                                    :
                                </td>
                                <td class="customerdetailstamleTdsize-two">
                                    <asp:Label ID="lblCustomerStatus" runat="server" Text="--"></asp:Label>
                                </td>
                                <td class="customerdetailstamleTd-two">
                                    <asp:Literal ID="litOutStanding" runat="server" Text="<%$ Resources:Resource, OUT_STANDING_AMT%>"></asp:Literal>
                                </td>
                                <td class="customerdetailstamleTd-dot">
                                    :
                                </td>
                                <td class="customerdetailstamleTdsize-two">
                                    <asp:Label ID="lblOutStanding" runat="server" Text="--"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <%-------------------------Land Lord Information Start----------------------%>
                    <div class="clr">
                    </div>
                    <div class="inner-sec">
                        <h4 class="subHeading">
                            <asp:Literal ID="Literal213" runat="server" Text="<%$ Resources:Resource, LAND_LORD_INFO%>"></asp:Literal>
                        </h4>
                        <div class="out-bor_aDetails">
                            <table class="customerdetailsTamle">
                                <tr>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:Resource, TITLE%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblLInfoTitle" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, F_NAME%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblLInfoFirstname" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, M_NAME%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblLInfoMiddleName" runat="server" Text="--"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, L_NAME%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblLInfoLastName" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, KNOWN_AS%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblLInfoKnownAs" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, EMAIL_ID%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblLInfoEmail" runat="server" Text="--"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, HOME%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblLInfoHomePhone" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:Resource, BUSINESS%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblLInfoBussPhone" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:Resource, OTHERS%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblLInfoOtherPhone" runat="server" Text="--"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <%-------------------------Land Lord Information End----------------------%>
                    <%-------------------------Tenent Information Start----------------------%>
                    <table class="customerdetailsTamle" style="width: 34%">
                        <tr>
                            <td class="customerdetailstamleTd-two">
                                <asp:Label ID="Label1" runat="server" Text="Is Tenant"></asp:Label>
                            </td>
                            <td class="customerdetailstamleTd-dot">
                                :
                            </td>
                            <td class="customerdetailstamleTdsize-two">
                                <asp:Label ID="lblIsTenant" runat="server" Text="--"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <div class="clr">
                    </div>
                    <div id="divTenent" runat="server" visible="false">
                        <div class="inner-sec">
                            <h4 class="subHeading">
                                <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:Resource, TENENT_INFO%>"></asp:Literal>
                            </h4>
                            <div class="out-bor_aDetails">
                                <table class="customerdetailsTamle">
                                    <tr>
                                        <td class="customerdetailstamleTd-two">
                                            <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, TITLE%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd-dot">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize-two">
                                            <asp:Label ID="lblTanentTitle" runat="server" Text="--"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd-two">
                                            <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:Resource, F_NAME%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd-dot">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize-two">
                                            <asp:Label ID="lblTanentFirstName" runat="server" Text="--"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd-two">
                                            <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, M_NAME%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd-dot">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize-two">
                                            <asp:Label ID="lblTanentMiddleName" runat="server" Text="--"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="customerdetailstamleTd-two">
                                            <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:Resource, L_NAME%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd-dot">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize-two">
                                            <asp:Label ID="lblTanentLastName" runat="server" Text="--"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:Resource, EMAIL_ID%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd-dot">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize-two">
                                            <asp:Label ID="lblTanentEmail" runat="server" Text="--"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd-two">
                                            <asp:Literal ID="litTHPhone" runat="server" Text="<%$ Resources:Resource, HOME%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd-dot">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize-two">
                                            <asp:Label ID="lblTanentHomeContactNo" runat="server" Text="--"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="customerdetailstamleTd-two">
                                            <asp:Literal ID="Literal19" runat="server" Text="<%$ Resources:Resource, ANOTHER_CONTACT_NO%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd-dot">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize-two">
                                            <asp:Label ID="lblTanentOtherContactNo" runat="server" Text="--"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                    </div>
                    <%-------------------------Tenent Information End----------------------%>
                    <%-------------------------Postal Information Start----------------------%>
                    <div class="clr">
                    </div>
                    <div class="inner-sec">
                        <h4 class="subHeading">
                            <asp:Literal ID="litPostalAddress" runat="server" Text="Postal Address"></asp:Literal>
                        </h4>
                        <div class="out-bor_aDetails">
                            <table class="customerdetailsTamle">
                                <tr>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litPHNo" runat="server" Text="<%$ Resources:Resource, H_NO%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblPostalHouseNo" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litPStreet" runat="server" Text="<%$ Resources:Resource, STREET%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblPostalStreet" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litPLGAVillage" runat="server" Text="<%$ Resources:Resource, CITY%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblPostalVillage" runat="server" Text="--"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litPZip" runat="server" Text="<%$ Resources:Resource, ZIP%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblPostalZip" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, AREA_CODE%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblPostalAreaCode" runat="server" Text="--"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <%-------------------------Postal Information End----------------------%>
                    <table class="customerdetailsTamle" style="width: 50%">
                        <tr>
                            <td class="customerdetailstamleTd-two">
                                <asp:Label ID="Label4" runat="server" Text="Is Same As Postal Address "></asp:Label>
                            </td>
                            <td class="customerdetailstamleTd-dot">
                                :
                            </td>
                            <td class="customerdetailstamleTdsize-two">
                                <asp:Label ID="lblsSameAsService" runat="server" Text="Yes"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <div class="clr">
                    </div>
                    <%-------------------------Service Information Start----------------------%>
                    <div class="inner-sec">
                        <h4 class="subHeading">
                            <asp:Literal ID="litServiceAddress" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Literal>
                        </h4>
                        <div class="out-bor_aDetails">
                            <table class="customerdetailsTamle">
                                <tr>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litSHNo" runat="server" Text="<%$ Resources:Resource, H_NO%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblServiceHouseNo" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd">
                                        <asp:Literal ID="litSStreet" runat="server" Text="<%$ Resources:Resource, STREET%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblServiceStreet" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litSLGAVillage" runat="server" Text="<%$ Resources:Resource, CITY%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblServiceVillage" runat="server" Text="--"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litSZip" runat="server" Text="<%$ Resources:Resource, ZIP%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblServiceZip" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litSArea" runat="server" Text="<%$ Resources:Resource, AREA_CODE%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblServiceAreaCode" runat="server" Text="--"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <table class="customerdetailsTamle" style="width: 41%">
                                <tr>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:Resource, COMMUNICATION_ADD%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblCommunicationAddress" runat="server" Text="--"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <%-------------------------Service Information End----------------------%>
                        <div class="clr">
                        </div>
                        <h4 class="subHeading">
                            Other Details</h4>
                        <table class="customerdetailsTamle">
                            <tr>
                                <td class="customerdetailstamleTd-two">
                                    <asp:Label ID="lblIsBedc" runat="server" Text="Is BEDC Employee"></asp:Label>
                                </td>
                                <td class="customerdetailstamleTd-dot">
                                    :
                                </td>
                                <td style="width: 2%;" class="cDfontsize">
                                    <asp:Label ID="lblIsBedcEmployee" runat="server" Text="No"></asp:Label>
                                </td>
                                <td>
                                    <table>
                                        <tr id="divEmployeeCode" runat="server" visible="false">
                                            <td style="width: 1%; color: #1a6807; font-size: 14px; font-weight: bold;">
                                                <asp:Literal ID="litEmployeCode" runat="server" Text="<%$ Resources:Resource, EMPLOYEE_CODE%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd-dot">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize-two">
                                                <asp:Label ID="lblEmployeeCode" runat="server" Text="--"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="customerdetailstamleTd-two" style="width: 2%;">
                                    <asp:Label ID="Label3" runat="server" Text="Is Embassy"></asp:Label>
                                </td>
                                <td class="customerdetailstamleTd-dot">
                                    :
                                </td>
                                <td class="cDfontsize">
                                    <asp:Label ID="lblIsEmbassyCustomer" runat="server" Text="No"></asp:Label>
                                </td>
                                <td>
                                    <table id="tblEmbassyCode" runat="server" visible="false">
                                        <tr>
                                            <td style="width: 1%; color: #1a6807; font-size: 14px; font-weight: bold;">
                                                <asp:Literal ID="litEmbassy" runat="server" Text="<%$ Resources:Resource, EMBASSY_CODE%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd-dot">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize-two">
                                                <asp:Label ID="lblEmbassyCode" runat="server" Text="--"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="customerdetailstamleTd-two">
                                    <asp:Label ID="Label5" runat="server" Text="Is VIP"></asp:Label>
                                </td>
                                <td class="customerdetailstamleTd-dot">
                                    :
                                </td>
                                <td colspan="2" class="cDfontsize">
                                    <asp:Label ID="lblIsVipCustomer" runat="server" Text="No"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <div class="clr">
                        </div>
                    </div>
                </div>
                <%----------------------------------------------------------Step 2----------------------------------------- --%>
                <div class="">
                    <div class="clr">
                    </div>
                    <div class="inner-sec">
                        <h4 class="subHeading">
                            Account Information</h4>
                        <div class="out-bor_aDetails">
                            <table class="customerdetailsTamle">
                                <tr>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litAccountType" runat="server" Text="<%$ Resources:Resource, ACCOUNT_TYPE%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblAccountType" runat="server" Text="--"></asp:Label>
                                        <asp:Label ID="lblGovtAccountTypeName" runat="server" Visible="false" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litBook" runat="server" Text="<%$ Resources:Resource, BOOK_CODE%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblBookCode" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litPole" runat="server" Text="<%$ Resources:Resource, POLE%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblPoleNo" runat="server" Text="--"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litTariff" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblTariff" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litClusterType" runat="server" Text="<%$ Resources:Resource, CLUSTER_TYPE%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblClusterType" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litPhase" runat="server" Text="<%$ Resources:Resource, PHASE%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblPhase" runat="server" Text="--"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litRouteNo" runat="server" Text="<%$ Resources:Resource, ROUTE_NO%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblRouteNo" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litInitialBilling" runat="server" Text="<%$ Resources:Resource, INITIAL_BILLING_KWH%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblInitialBillingkWh" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, READCODE%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblReadCode" runat="server" Text="--"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="divReadCustomer" runat="server" visible="false">
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litMeterNoDisplay" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblMeterNo" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Label ID="lblcm" runat="server" Text="Is CAPMI : "></asp:Label>
                                        <%--<asp:Literal ID="Literal20" runat="server" Text="Present Reading"></asp:Literal>--%>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblIsCapmy" runat="server" Text="No"></asp:Label>
                                        <%--  <asp:Label ID="lblPresentReading" runat="server"Text="--"></asp:Label>--%>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litAmount" runat="server" Text="<%$ Resources:Resource, AMOUNT%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblMeterAmount" runat="server" Text="--"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="divDirectCustomer" runat="server" visible="false">
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="Literal23" runat="server" Text="<%$ Resources:Resource, AVERAGE_READING%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblAverageReading" runat="server" Text="--"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trPresentReading" runat="server" visible="false">
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litCapmiOutstandingAmount" runat="server" Text="CAPMI Outstanding"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblCapmiOutstandingAmount" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="Literal20" runat="server" Text="Initial Meter Reading"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize">
                                        <asp:Label ID="lblPresentReading" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                    </td>
                                    <td class="customerdetailstamleTd">
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <%--Popus Govt Account Type Ends--%>
                    <div class="clr">
                    </div>
                    <div class="inner-sec">
                        <h4 class="subHeading">
                            Setup Information</h4>
                        <div class="out-bor_aDetails">
                            <table class="customerdetailsTamle">
                                <tr>
                                    <td class="customerdetailstamleTd">
                                        <asp:Literal ID="litApplicationDate" runat="server" Text="<%$ Resources:Resource, APPLICATION_DATE%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblApplicationDate" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litConnectionDate" runat="server" Text="<%$ Resources:Resource, CONNECTION_DATE%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblConnectionDate" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litSetupDate" runat="server" Text="<%$ Resources:Resource, SETUP_DATE%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblSetUpDate" runat="server" Text="--"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litCertifiedBy" runat="server" Text="<%$ Resources:Resource, CERTIFIED_BY%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblCertifiedBy" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litSeal1" runat="server" Text="<%$ Resources:Resource, SEAL_1%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblSeal1" runat="server" Text="--"></asp:Label>
                                    </td>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litSeal2" runat="server" Text="<%$ Resources:Resource, SEAL_2%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblSeal2" runat="server" Text="--"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="customerdetailstamleTd-two">
                                        <asp:Literal ID="litOldAccountNo" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                    </td>
                                    <td class="customerdetailstamleTd-dot">
                                        :
                                    </td>
                                    <td class="customerdetailstamleTdsize-two">
                                        <asp:Label ID="lblOldAccount" runat="server" Text="--"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <table class="customerdetailsTamle" style="margin-left: 6px; width: 50%;">
                        <tr>
                            <td class="customerdetailstamleTd-change">
                                <asp:Literal ID="litApplicationProcessedBy" runat="server" Text="<%$ Resources:Resource, APPLICATION_PROCCESED_BY%>"></asp:Literal>
                            </td>
                            <td class="customerdetailstamleTd-dot">
                                :
                            </td>
                            <td class="customerdetailstamleTdsize-twochange">
                                <asp:Label ID="lblAppProcessedBy" runat="server" Text="--"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="customerdetailstamleTd-two">
                                <asp:Literal ID="litInstalledBy" runat="server" Text="<%$ Resources:Resource, INSTALLED_BY%>"></asp:Literal>
                            </td>
                            <td class="customerdetailstamleTd-dot">
                                :
                            </td>
                            <td class="customerdetailstamleTdsize-two">
                                <asp:Label ID="lblInstallByName" runat="server" Text="--"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="clr">
            </div>
            <div class="out-bor" id="divIdentityInfo" runat="server">
                <div class="clr">
                </div>
                <div class="inner-sec">
                    <h4 class="subHeading">
                        Identity Information</h4>
                    <div class="out-bor_aDetails">
                        <asp:Repeater ID="rptrIdentityDetails" runat="server">
                            <ItemTemplate>
                                <table class="customerdetailsTamle">
                                    <tr>
                                        <td class="customerdetailstamleTd-two">
                                            <asp:Literal ID="litIdentityType" runat="server" Text="Type"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd-dot">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize-two">
                                            <%#Eval("IdentityTypeIdList")%>
                                        </td>
                                        <td class="customerdetailstamleTd-two">
                                            <asp:Literal ID="litIdentityNo" runat="server" Text="Number"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd-dot">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize-two">
                                            <%#Eval("IdentityNumberList")%>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
            <div class="clr">
            </div>
            <div class="out-bor" id="divUploadsList" runat="server">
                <div class="clr">
                </div>
                <div class="inner-sec">
                    <h4 class="subHeading">
                        Uploads</h4>
                    <div class="out-bor_aDetails">
                        <asp:Repeater ID="rptrGetCustomerDocuments" runat="server">
                            <ItemTemplate>
                                <table class="customerdetailsTamle">
                                    <tr>
                                        <td class="customerdetailstamleTd-two">
                                            <asp:Literal ID="Literal22" runat="server" Text="Document"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd-dot">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize-two">
                                            <%#Eval("DocumentName")%>
                                        </td>
                                    </tr>
                                </table>
                                <%-- <%#Eval("Path")%>--%>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
            <div class="clr">
            </div>
            <div class="out-bor" id="divUDFList" runat="server">
                <div class="clr">
                </div>
                <div class="inner-sec">
                    <h4 class="subHeading">
                        User Defined Fields</h4>
                    <div class="out-bor_aDetails">
                        <asp:Repeater ID="rptrGetCustomerUDFValues" runat="server">
                            <ItemTemplate>
                                <table class="customerdetailsTamle">
                                    <tr>
                                        <td class="customerdetailstamleTd-two">
                                            <asp:Literal ID="Literal24" runat="server" Text='<%#Eval("UDFTypeIdList")%>'></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd-dot">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize-two">
                                            <%#Eval("UDFValueList")%>
                                        </td>
                                        <%--   <td class="customerdetailstamleTd-two">
                                            <asp:Literal ID="Literal25" runat="server" Text="Ref Doc"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd-dot">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize-two">
                                            <%#Eval("UDFTypeIdList")%>
                                        </td>--%>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
