﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for Consumer Registration
                     
 Developer        : id077-Neeraj Kanojiya
 Creation Date    : 27-MARCH-2015
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using System.Threading;
using System.Globalization;
using UMS_NigeriaBE;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using System.Xml;
using System.IO;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.Text;
using System.Collections;
using System.Web.UI.HtmlControls;
using UMS_Nigeria.UserControls;


namespace UMS_Nigeria.ConsumerManagement
{
    public partial class CustomerDetails : System.Web.UI.Page
    {
        #region Members
        ConsumerBal objConsumerBal = new ConsumerBal();
        CustomerRegistrationBE objCustomerRegistrationBE = new CustomerRegistrationBE();
        XmlDocument xml = new XmlDocument();
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        string Key = "SearchCustomer";
        #endregion
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ac"])) GetCustomerDetail();
                else Response.Redirect(UMS_Resource.SRCH_CUST);

            }
        }
        public void GetCustomerDetail()
        {
            try
            {
                string GlobalAccountNo = objiDsignBAL.Decrypt(Request.QueryString["ac"]);
                objCustomerRegistrationBE.GlobalAccountNumber = GlobalAccountNo;
                xml = objConsumerBal.GetCustomerDetails(objCustomerRegistrationBE);
                if (xml != null)
                {
                    objCustomerRegistrationBE = objiDsignBAL.DeserializeFromXml<CustomerRegistrationBE>(xml);
                    lblGlobalAccountNo.Text = lblPostalHouseNo.Text = objCustomerRegistrationBE.GlobalAccountNumber;

                    lblBuDsp.Text = objCustomerRegistrationBE.BusinessUnit;
                    lblSUDsp.Text = objCustomerRegistrationBE.ServiceUnitName;
                    lblSCDsp.Text = objCustomerRegistrationBE.ServiceCenterName;
                    lblBookGroupDsp.Text = objCustomerRegistrationBE.CycleName;
                    lblBookCodeDsp.Text = objCustomerRegistrationBE.BookCode;

                    #region Postal AddressDetails

                    #region Postal
                    lblServiceHouseNo.Text = lblPostalHouseNo.Text = objCustomerRegistrationBE.HouseNoPostal;
                    lblServiceStreet.Text = lblPostalStreet.Text = objCustomerRegistrationBE.StreetPostal;
                    lblServiceVillage.Text = lblPostalVillage.Text = objCustomerRegistrationBE.CityPostaL;
                    lblServiceZip.Text = lblPostalZip.Text = objCustomerRegistrationBE.PZipCode;

                    if (objCustomerRegistrationBE.AreaPostal != "0" && !string.IsNullOrEmpty(objCustomerRegistrationBE.AreaPostal)) lblPostalAreaCode.Text = objCustomerRegistrationBE.AreaPostal;
                    if (objCustomerRegistrationBE.AreaService != "0" && !string.IsNullOrEmpty(objCustomerRegistrationBE.AreaService)) lblServiceAreaCode.Text = objCustomerRegistrationBE.AreaService;
                    if (objCustomerRegistrationBE.IsSameAsService) lblServiceAreaCode.Text = lblPostalAreaCode.Text = objCustomerRegistrationBE.AreaPostal;
                    if (objCustomerRegistrationBE.IsCommunicationPostal) lblCommunicationAddress.Text = "Postal";
                    #endregion

                    #region Service
                    if (objCustomerRegistrationBE.ServiceAddressID > 0)
                    {
                        if (objCustomerRegistrationBE.IsCommunicationService) lblCommunicationAddress.Text = "Service";
                        lblServiceHouseNo.Text = objCustomerRegistrationBE.HouseNoService;
                        lblServiceStreet.Text = objCustomerRegistrationBE.StreetService;
                        lblServiceVillage.Text = objCustomerRegistrationBE.CityService;
                        lblServiceZip.Text = objCustomerRegistrationBE.SZipCode;
                        lblServiceAreaCode.Text = objCustomerRegistrationBE.AreaService;
                        lblsSameAsService.Text = "No";
                    }
                    #endregion


                    #endregion

                    #region Tenent Details
                    if (objCustomerRegistrationBE.TenentId > 0)
                    {
                        divTenent.Visible = true;
                        //objCustomerRegistrationBE.TitleTanent = ddlTTitle.SelectedItem.Text;
                        lblTanentTitle.Text = objCustomerRegistrationBE.TitleTanent;
                        lblTanentFirstName.Text = objCustomerRegistrationBE.FirstNameTanent;
                        lblTanentMiddleName.Text = objCustomerRegistrationBE.MiddleNameTanent;
                        lblTanentLastName.Text = objCustomerRegistrationBE.LastNameTanent;
                        lblTanentHomeContactNo.Text = objCustomerRegistrationBE.PhoneNumberTanent;
                        lblTanentOtherContactNo.Text = objCustomerRegistrationBE.AlternatePhoneNumberTanent;
                        lblTanentEmail.Text = objCustomerRegistrationBE.EmailIdTanent;
                    };
                    #endregion

                    #region Customer Details
                    lblCustomerStatus.Text = objCustomerRegistrationBE.Status;
                    lblOutStanding.Text = objCommonMethods.GetCurrencyFormat(objCustomerRegistrationBE.OutStandingAmount, 2, Constants.MILLION_Format);
                    lblCustomerType.Text = objCustomerRegistrationBE.CustomerType;
                    lblLInfoTitle.Text = objCustomerRegistrationBE.TitleLandlord;
                    //objCustomerRegistrationBE.TitleLandlord = ddlOTitle.SelectedItem.Text;
                    lblLInfoFirstname.Text = objCustomerRegistrationBE.FirstNameLandlord;
                    lblLInfoMiddleName.Text = objCustomerRegistrationBE.MiddleNameLandlord;
                    lblLInfoLastName.Text = objCustomerRegistrationBE.LastNameLandlord;
                    lblLInfoHomePhone.Text = objCustomerRegistrationBE.HomeContactNumberLandlord;
                    lblLInfoBussPhone.Text = objCustomerRegistrationBE.BusinessPhoneNumberLandlord;
                    lblLInfoOtherPhone.Text = objCustomerRegistrationBE.OtherPhoneNumberLandlord;
                    lblLInfoEmail.Text = objCustomerRegistrationBE.EmailIdLandlord;
                    lblLInfoKnownAs.Text = objCustomerRegistrationBE.KnownAs;
                    //objCustomerRegistrationBE.IsSameAsService;
                    //objCustomerRegistrationBE.IsSameAsTenent ;
                    //if (fupDocument.HasFile) objCustomerRegistrationBE.DocumentNo;
                    if (!string.IsNullOrEmpty(objCustomerRegistrationBE.ApplicationDate)) lblApplicationDate.Text = objCustomerRegistrationBE.ApplicationDate;
                    if (!string.IsNullOrEmpty(objCustomerRegistrationBE.ConnectionDate)) lblConnectionDate.Text = objCustomerRegistrationBE.ConnectionDate;
                    if (!string.IsNullOrEmpty(objCustomerRegistrationBE.SetupDate)) lblSetUpDate.Text = objCustomerRegistrationBE.SetupDate;
                    if (objCustomerRegistrationBE.IsBEDCEmployee) { lblIsBedcEmployee.Text = "Yes"; divEmployeeCode.Visible = true; lblEmployeeCode.Text = objCustomerRegistrationBE.EmployeeName; }
                    if (objCustomerRegistrationBE.IsVIPCustomer) lblIsVipCustomer.Text = "Yes";
                    if (objCustomerRegistrationBE.IsEmbassyCustomer) { lblIsEmbassyCustomer.Text = "Yes"; tblEmbassyCode.Visible = true; }
                    lblEmployeeCode.Text = objCustomerRegistrationBE.EmployeeName;
                    //objCustomerRegistrationBE.IsBEDCEmployee = cbIsBEDCEmployee.Checked;
                    //objCustomerRegistrationBE.IsVIPCustomer = cbIsVIP.Checked;
                    lblOldAccount.Text = objCustomerRegistrationBE.OldAccountNo;
                    lblAccountNo.Text = objCustomerRegistrationBE.AccountNo;
                    //lblEmployeeCode.Text = objCustomerRegistrationBE.EmployeeCode;
                    #endregion

                    #region Procedural Details
                    //objCustomerRegistrationBE.CustomerTypeId;
                    //objCustomerRegistrationBE.BookNo = 
                    lblPoleNo.Text = objCustomerRegistrationBE.PoleID;
                    //if (Convert.ToInt32(ReadCodeType) != (int)ReadCode.Direct) objCustomerRegistrationBE.MeterNumber;
                    //objCustomerRegistrationBE.TariffClassID;
                    //objCustomerRegistrationBE.IsEmbassyCustomer = cbIsEmbassy.Checked;
                    lblEmbassyCode.Text = objCustomerRegistrationBE.EmbassyCode;
                    //objCustomerRegistrationBE.PhaseId;
                    //objCustomerRegistrationBE.ReadCodeID
                    //objCustomerRegistrationBE.AccountTypeId;
                    ////objCustomerRegistrationBE.ClusterCategoryId;
                    //objCustomerRegistrationBE.ClusterCategoryId;
                    //objCustomerRegistrationBE.RouteSequenceNumber;
                    //if (!string.IsNullOrEmpty(hfGovtAccountTypeID.Value)) objCustomerRegistrationBE.MGActTypeID = Convert.ToInt32(hfGovtAccountTypeID.Value);



                    //lblPresentReading.Text = Convert.ToString(objCustomerRegistrationBE.PresentReading);
                    lblPresentReading.Text = Convert.ToString(objCustomerRegistrationBE.InitialReading);
                    #endregion

                    #region Identity Details
                    //if (hfIdentityIdList.Value != "" && hfIdentityNumberList.Value != "")
                    //{
                    //    objCustomerRegistrationBE.IdentityTypeIdList = hfIdentityIdList.Value; //Convert.ToInt32(ddlIdentityType.SelectedItem.Value);
                    //    objCustomerRegistrationBE.IdentityNumberList = hfIdentityNumberList.Value;
                    //}
                    //objCustomerRegistrationBE.UploadDocumentID = fupDocument.FileName;
                    #endregion

                    #region Application Process Details
                    //objCustomerRegistrationBE.CertifiedBy
                    lblCertifiedBy.Text = objCustomerRegistrationBE.CertifiedBy;
                    lblSeal1.Text = objCustomerRegistrationBE.Seal1;
                    lblSeal2.Text = objCustomerRegistrationBE.Seal2;
                    lblAppProcessedBy.Text = objCustomerRegistrationBE.ApplicationProcessedBy;
                    if (objCustomerRegistrationBE.ApplicationProcessedBy != null)
                        if (objCustomerRegistrationBE.ApplicationProcessedBy.ToLower() == "bedc")
                        {
                            litInstalledBy.Text = Resource.INSTALLED_BY;
                            lblInstallByName.Text = objCustomerRegistrationBE.EmployeeNameProcessBy;
                        }
                        else if (objCustomerRegistrationBE.ApplicationProcessedBy.ToLower() == "others")
                        {
                            litInstalledBy.Text = "Agency Name";
                            lblInstallByName.Text = objCustomerRegistrationBE.AgencyName;
                        }
                    #endregion

                    #region Application Process Person Details
                    //if (rblApplicationProcessedBy.Items[0].Selected) objCustomerRegistrationBE.InstalledBy;
                    //if (rblApplicationProcessedBy.Items[1].Selected) objCustomerRegistrationBE.AgencyId;
                    #endregion

                    #region Customer Active Details
                    //objCustomerRegistrationBE.IsCAPMI;
                    //if (Convert.ToInt32(ReadCodeType) != (int)ReadCode.Direct)
                    //{
                    //    if (cbIsCAPMY.Checked) objCustomerRegistrationBE.MeterAmount = Convert.ToDecimal(txtAmount.Text.Replace(",", string.Empty));
                    //    objCustomerRegistrationBE.PresentReading = Convert.ToInt32(txtPresentReading.Text);
                    //}
                    lblInitialBillingkWh.Text = Convert.ToString(objCustomerRegistrationBE.InitialBillingKWh);
                    lblAccountType.Text = objCustomerRegistrationBE.AccountType;
                    lblBookCode.Text = objCustomerRegistrationBE.BookCode;
                    lblTariff.Text = objCustomerRegistrationBE.ClassName;
                    lblClusterType.Text = objCustomerRegistrationBE.ClusterCategoryName;
                    lblPhase.Text = objCustomerRegistrationBE.Phase;
                    lblRouteNo.Text = objCustomerRegistrationBE.RouteName;
                    lblReadCode.Text = objCustomerRegistrationBE.ReadType;
                    if (objCustomerRegistrationBE.ReadType != null)
                        if (objCustomerRegistrationBE.ReadType.ToLower() == "read") { divReadCustomer.Visible = true; lblMeterNo.Text = objCustomerRegistrationBE.MeterNumber; trPresentReading.Visible = true; }
                        else if (objCustomerRegistrationBE.ReadType.ToLower() == "direct") { divDirectCustomer.Visible = true; lblAverageReading.Text = objCustomerRegistrationBE.AverageReading.ToString(); }
                    if (objCustomerRegistrationBE.TenentId != null)
                        if (objCustomerRegistrationBE.TenentId == 0) lblIsTenant.Text = "No";
                        else lblIsTenant.Text = "Yes";
                    if (objCustomerRegistrationBE.IsCAPMI != null)
                        if (objCustomerRegistrationBE.IsCAPMI) { lblIsCapmy.Text = "Yes"; lblMeterAmount.Text = objCommonMethods.GetCurrencyFormat(objCustomerRegistrationBE.MeterAmount, 2, Constants.MILLION_Format); lblCapmiOutstandingAmount.Text = objCommonMethods.GetCurrencyFormat(objCustomerRegistrationBE.CapmiOutstandingAmount, 2, Constants.MILLION_Format); }
                    //divMeterAmoount.Visible = true; 
                    #endregion

                    #region UDF Values List

                    //if (GetUDFValues())
                    //{
                    //    objCustomerRegistrationBE.UDFTypeIdList = hfUserControlFieldsID.Value;
                    //    objCustomerRegistrationBE.UDFValueList = hfUserControlFieldsValue.Value;
                    //}

                    #endregion

                    GetIdentityDetails(GlobalAccountNo);
                    GetCustomerDocuments(GlobalAccountNo);
                    GetCustomerUDFValues(GlobalAccountNo);
                }
                else
                {
                    lblMessage.Text = Resource.CST_NOT_FND;
                    //mpeMessage.Show();
                }
            }
            catch (Exception ex)
            {
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                Message(Resource.CST_SRCH_FAILS, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }
        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        private void GetIdentityDetails(string GlobalAccountNo)
        {
            try
            {
                CustomerRegistrationList_1BE objCustomerRegistrationList_1BE = new CustomerRegistrationList_1BE();
                objCustomerRegistrationBE.GlobalAccountNumber = GlobalAccountNo;
                xml = objConsumerBal.GetIdentityDetails(objCustomerRegistrationBE);
                if (xml != null)
                {
                    objCustomerRegistrationList_1BE = objiDsignBAL.DeserializeFromXml<CustomerRegistrationList_1BE>(xml);
                    if (objCustomerRegistrationList_1BE.items.Count > 0)
                    {
                        divIdentityInfo.Visible = true;
                        rptrIdentityDetails.DataSource = objCustomerRegistrationList_1BE.items;
                        rptrIdentityDetails.DataBind();
                    }
                    else
                    {
                        divIdentityInfo.Visible = false;
                    }
                }
                else
                {
                    rptrIdentityDetails.Visible = false;
                }
            }
            catch (Exception ex)
            {
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                Message(Resource.CST_SRCH_FAILS, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }
        private void GetCustomerDocuments(string GlobalAccountNo)
        {
            try
            {
                CustomerRegistrationList_1BE objCustomerRegistrationList_1BE = new CustomerRegistrationList_1BE();
                objCustomerRegistrationBE.GlobalAccountNumber = GlobalAccountNo;
                xml = objConsumerBal.GetCustomerDocuments(objCustomerRegistrationBE);
                if (xml != null)
                {
                    objCustomerRegistrationList_1BE = objiDsignBAL.DeserializeFromXml<CustomerRegistrationList_1BE>(xml);
                    if (objCustomerRegistrationList_1BE.items.Count > 0)
                    {
                        divUploadsList.Visible = true;
                        rptrGetCustomerDocuments.DataSource = objCustomerRegistrationList_1BE.items;
                        rptrGetCustomerDocuments.DataBind();
                    }
                    else
                    {
                        divUploadsList.Visible = false;
                    }
                }
                else
                {
                    rptrGetCustomerDocuments.Visible = false;
                }
            }
            catch (Exception ex)
            {
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                Message(Resource.CST_SRCH_FAILS, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }
        private void GetCustomerUDFValues(string GlobalAccountNo)
        {
            try
            {
                CustomerRegistrationList_1BE objCustomerRegistrationList_1BE = new CustomerRegistrationList_1BE();
                objCustomerRegistrationBE.GlobalAccountNumber = GlobalAccountNo;
                xml = objConsumerBal.GetCustomerUDFValues(objCustomerRegistrationBE);
                if (xml != null)
                {
                    objCustomerRegistrationList_1BE = objiDsignBAL.DeserializeFromXml<CustomerRegistrationList_1BE>(xml);
                    if (objCustomerRegistrationList_1BE.items.Count > 0)
                    {
                        divUDFList.Visible = true;
                        rptrGetCustomerUDFValues.DataSource = objCustomerRegistrationList_1BE.items;
                        rptrGetCustomerUDFValues.DataBind();
                    }
                    else
                    {
                        divUDFList.Visible = false;
                    }
                }
                else
                {
                    rptrGetCustomerUDFValues.Visible = false;
                }
            }
            catch (Exception ex)
            {
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                Message(Resource.CST_SRCH_FAILS, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }
    }
}