﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddEmployee
                     
 Developer        : id027-T.Karthik
 Creation Date    : 
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using System.Data;
using org.in2bits.MyXls;
using System.Configuration;
using System.IO;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class CustomerExportBookWise : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        ReportsBal objReportsBal = new ReportsBal();
        public int PageNum;
        string Key = "CustomerExportBookWise";
        #endregion

        #region Properties

        public string BU
        {
            get { return string.IsNullOrEmpty(ddlBU.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlBU, ",") : ddlBU.SelectedValue; }
            set { ddlBU.SelectedValue = value.ToString(); }
        }
        public string SU
        {
            get { return string.IsNullOrEmpty(ddlSU.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlSU, ",") : ddlSU.SelectedValue; }
            set { ddlSU.SelectedValue = value.ToString(); }
        }
        public string SC
        {
            get { return string.IsNullOrEmpty(ddlSC.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlSC, ",") : ddlSC.SelectedValue; }
            set { ddlSC.SelectedValue = value.ToString(); }
        }
        public string Cycle
        {
            get { return string.IsNullOrEmpty(ddlCycle.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlCycle, ",") : ddlCycle.SelectedValue; }
            set { ddlCycle.SelectedValue = value.ToString(); }
        }
        public string BookNo
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblBooks, ","); }
        }

        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session[UMS_Resource.SESSION_LOGINID] != null)
                {
                    pnlMessage.CssClass = lblMessage.Text = string.Empty;
                    if (!IsPostBack)
                    {
                        string path = string.Empty;
                        _objCommonMethods.BindUserBusinessUnits(ddlBU, string.Empty, false, UMS_Resource.DROPDOWN_SELECT);
                        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        {
                            ddlBU.SelectedIndex = ddlBU.Items.IndexOf(ddlBU.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                            ddlBU.Enabled = false;
                            ddlBU_SelectedIndexChanged(ddlBU, new EventArgs());
                            ddlSU_SelectedIndexChanged(ddlSU, new EventArgs());
                            _objCommonMethods.BindCyclesByBUSUSC(ddlCycle, BU, string.Empty, string.Empty, true, "ALL", false);
                        }
                    }
                }
                else
                {
                    Response.Redirect(UMS_Resource.DEFAULT_PAGE);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #region ddlEvents
        protected void ddlBU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _objCommonMethods.BindUserServiceUnits_BuIds(ddlSU, BU, Resource.DDL_ALL, false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlSU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblBooks.Items.Clear();
                divBookNos.Visible = false;
                _objCommonMethods.BindUserServiceCenters_SuIds(ddlSC, SU, Resource.DDL_ALL, false);
                if (ddlSU.SelectedIndex >= 0)
                    _objCommonMethods.BindCyclesByBUSUSC(ddlCycle, BU, string.Empty, string.Empty, true, "ALL", false);

                //_objCommonMethods.BindUserServiceCenters_SuIds(ddlSC, SU, Resource.DDL_ALL, false);
                //if (ddlSU.SelectedIndex == 0)
                //    _objCommonMethods.BindCyclesByBUSUSC(ddlCycle, BU, string.Empty, string.Empty, true, "ALL", false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblBooks.Items.Clear();
                divBookNos.Visible = false;
                if (ddlSC.SelectedIndex > 0)
                {
                    _objCommonMethods.BindCyclesByBUSUSC(ddlCycle, BU, SU, SC, true, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlCycle.SelectedIndex = 0;
                    ddlCycle.Enabled = true;
                }
                else
                {
                    ddlCycle.SelectedIndex = 0;
                    ddlCycle.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlCycle_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlCycle.SelectedIndex > 0)
                {
                    //_objCommonMethods.BindBooksForBookwiseCustomerExport(cblBooks, ddlCycle.SelectedValue, true, false, UMS_Resource.DROPDOWN_SELECT);
                    _objCommonMethods.BindAllBookNumbersWithDetails_MeterReadings(cblBooks, ddlCycle.SelectedValue, true, false, UMS_Resource.DROPDOWN_SELECT);
                    if (cblBooks.Items.Count > 0)
                        divBookNos.Visible = true;
                    else
                        divBookNos.Visible = false;

                }
                else
                {
                    cblBooks.Items.Clear();
                    divBookNos.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        //protected void btnExport_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        RptCheckMetersBe objReportBe = new RptCheckMetersBe();
        //        //objReportBe.BU_ID = string.Empty;
        //        //objReportBe.SU_ID = string.Empty;
        //        //objReportBe.ServiceCenterId = string.Empty;
        //        //objReportBe.CycleId = string.Empty;
        //        //objReportBe.BookNo = string.Empty;
        //        objReportBe.BU_ID = BU;
        //        objReportBe.SU_ID = SU;
        //        objReportBe.ServiceCenterId = SC;
        //        objReportBe.CycleId = Cycle;
        //        objReportBe.BookNo = BookNo;
        //        DataSet ds = new DataSet();
        //        ds = objReportsBal.GetCustomersExportByBookWise(objReportBe);

        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            var BUlist = (from list in ds.Tables[0].AsEnumerable()
        //                          orderby list["CustomerSortOrder"] ascending
        //                          orderby list["BookSortOrder"] ascending
        //                          orderby list["CycleName"] ascending
        //                          group list by list.Field<string>("BusinessUnitName") into g
        //                          select new { BusinessUnit = g.Key }).ToList();

        //            var SUlist = (from list in ds.Tables[0].AsEnumerable()
        //                          orderby list["CustomerSortOrder"] ascending
        //                          orderby list["BookSortOrder"] ascending
        //                          orderby list["CycleName"] ascending
        //                          group list by list.Field<string>("ServiceUnitName") into g
        //                          select new { ServiceUnit = g.Key }).ToList();

        //            var SClist = (from list in ds.Tables[0].AsEnumerable()
        //                          orderby list["CustomerSortOrder"] ascending
        //                          orderby list["BookSortOrder"] ascending
        //                          orderby list["CycleName"] ascending
        //                          group list by list.Field<string>("ServiceCenterName") into g
        //                          select new { ServiceCenter = g.Key }).ToList();

        //            var Cyclelist = (from list in ds.Tables[0].AsEnumerable()
        //                             orderby list["CustomerSortOrder"] ascending
        //                             orderby list["BookSortOrder"] ascending
        //                             orderby list["CycleName"] ascending
        //                             group list by list.Field<string>("CycleName") into g
        //                             select new { Cycle = g.Key }).ToList();

        //            var Booklist = (from list in ds.Tables[0].AsEnumerable()
        //                            orderby list["CustomerSortOrder"] ascending
        //                            orderby list["BookSortOrder"] ascending
        //                            orderby list["CycleName"] ascending
        //                            group list by list.Field<string>("BookCode") into g
        //                            select new { Book = g.Key }).ToList();

        //            if (BUlist.Count() > 0)
        //            {
        //                XlsDocument xls = new XlsDocument();
        //                Worksheet sheet = xls.Workbook.Worksheets.Add("sheet");
        //                Cells cells = sheet.Cells;
        //                Cell _objCell = null;
        //                _objCell = cells.Add(1, 4, Resource.BEDC + "\n" + Resource.BOOKWISE_CUSTOMERS_REPORT_EXCEL_HEAD + DateTime.Today.ToShortDateString());
        //                sheet.AddMergeArea(new MergeArea(1, 2, 4, 6));

        //                int DetailsRowNo = 5;
        //                int CustomersRowNo = 10;

        //                for (int i = 0; i < BUlist.Count; i++)//BU
        //                {
        //                    for (int j = 0; j < SUlist.Count; j++)//SU
        //                    {
        //                        for (int k = 0; k < SClist.Count; k++)//SC
        //                        {
        //                            for (int l = 0; l < Cyclelist.Count; l++)//Cycle
        //                            {
        //                                for (int m = 0; m < Booklist.Count; m++)//Book
        //                                {

        //                                    var Customers = (from list in ds.Tables[0].AsEnumerable()
        //                                                     where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
        //                                                     && list.Field<string>("ServiceUnitName") == SUlist[j].ServiceUnit
        //                                                     && list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
        //                                                     && list.Field<string>("CycleName") == Cyclelist[l].Cycle
        //                                                     && list.Field<string>("BookCode") == Booklist[m].Book
        //                                                     orderby list["CustomerSortOrder"] ascending
        //                                                     orderby list["BookSortOrder"] ascending
        //                                                     orderby list["CycleName"] ascending
        //                                                     select list).ToList();

        //                                    if (Customers.Count() > 0)
        //                                    {
        //                                        cells.Add(DetailsRowNo, 1, "Business Unit");
        //                                        cells.Add(DetailsRowNo, 4, "Service Unit");
        //                                        cells.Add(DetailsRowNo, 6, "Service Center");
        //                                        cells.Add(DetailsRowNo + 2, 1, "BookGroup");
        //                                        cells.Add(DetailsRowNo + 2, 4, "Book Code");

        //                                        cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
        //                                        cells.Add(DetailsRowNo, 5, SUlist[j].ServiceUnit).Font.Bold = true;
        //                                        cells.Add(DetailsRowNo, 7, SClist[k].ServiceCenter).Font.Bold = true;
        //                                        cells.Add(DetailsRowNo + 2, 2, Cyclelist[l].Cycle).Font.Bold = true;
        //                                        cells.Add(DetailsRowNo + 2, 5, Booklist[m].Book).Font.Bold = true;

        //                                        cells.Add(DetailsRowNo + 4, 1, "Sno").Font.Bold = true;
        //                                        cells.Add(DetailsRowNo + 4, 2, "Account No").Font.Bold = true;
        //                                        cells.Add(DetailsRowNo + 4, 3, "Name").Font.Bold = true;
        //                                        cells.Add(DetailsRowNo + 4, 4, "Old Account No").Font.Bold = true;
        //                                        cells.Add(DetailsRowNo + 4, 5, "Meter No").Font.Bold = true;
        //                                        cells.Add(DetailsRowNo + 4, 6, "Previous Reading").Font.Bold = true;
        //                                        cells.Add(DetailsRowNo + 4, 7, "Current Reading").Font.Bold = true;
        //                                        cells.Add(DetailsRowNo + 4, 8, "Address").Font.Bold = true;
        //                                        cells.Add(DetailsRowNo + 4, 9, "Remarks").Font.Bold = true;

        //                                        CustomersRowNo++;
        //                                        for (int n = 0; n < Customers.Count; n++)
        //                                        {
        //                                            DataRow dr = (DataRow)Customers[n];
        //                                            cells.Add(CustomersRowNo, 1, (n + 1).ToString());
        //                                            cells.Add(CustomersRowNo, 2, dr["AccountNo"].ToString());
        //                                            cells.Add(CustomersRowNo, 3, dr["Name"].ToString());
        //                                            cells.Add(CustomersRowNo, 4, dr["OldAccountNo"].ToString());
        //                                            cells.Add(CustomersRowNo, 5, dr["MeterNo"].ToString());
        //                                            cells.Add(CustomersRowNo, 6, dr["PreviousReading"].ToString());
        //                                            cells.Add(CustomersRowNo, 7, dr["CurrentReading"].ToString());
        //                                            cells.Add(CustomersRowNo, 8, dr["Address"].ToString());
        //                                            cells.Add(CustomersRowNo, 9, dr["Remarks"].ToString());
        //                                            CustomersRowNo++;
        //                                        }

        //                                        DetailsRowNo = CustomersRowNo + 1;
        //                                        CustomersRowNo = CustomersRowNo + 5;
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                }

        //                string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
        //                fileName = "Customers" + fileName;
        //                string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
        //                xls.FileName = filePath;
        //                xls.Save();
        //                _objiDsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
        //            }
        //            else
        //                Message("Customers Not Found", UMS_Resource.MESSAGETYPE_ERROR);
        //        }
        //        else
        //            Message("Customers Not Found", UMS_Resource.MESSAGETYPE_ERROR);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                DivLoading.Style.Add("display", "block");
                RptCheckMetersBe objReportBe = new RptCheckMetersBe();
                objReportBe.BU_ID = BU;
                objReportBe.SU_ID = SU;
                objReportBe.ServiceCenterId = SC;
                objReportBe.CycleId = Cycle;
                objReportBe.BookNo = BookNo;
                DataSet ds = new DataSet();
                ds = objReportsBal.GetCustomersExportByBookWise(objReportBe);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        var BUlist = (from list in ds.Tables[0].AsEnumerable()
                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                      group list by list.Field<string>("BusinessUnitName") into g
                                      select new { BusinessUnit = g.Key }).ToList();

                        if (BUlist.Count() > 0)
                        {
                            XlsDocument xls = new XlsDocument();
                            Worksheet mainSheet = xls.Workbook.Worksheets.Add("Summary");
                            Cells mainCells = mainSheet.Cells;
                            Cell _objMainCells = null;


                            _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
                            mainCells.Add(4, 2, "Report Code ");
                            mainCells.Add(4, 3, Constants.BookWiseCustomerExportReport_Code).Font.Bold = true;
                            mainCells.Add(4, 4, "Report Name");
                            mainCells.Add(4, 5, Constants.BookWiseCustomerExportReport_Name).Font.Bold = true;
                            mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
                            _objMainCells.Font.Weight = FontWeight.ExtraBold;
                            _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objMainCells.Font.FontFamily = FontFamilies.Roman;

                            int MainSheetDetailsRowNo = 6;
                            mainCells.Add(MainSheetDetailsRowNo, 1, "S.No.").Font.Bold = true;
                            mainCells.Add(MainSheetDetailsRowNo, 2, "Service Center").Font.Bold = true;
                            mainCells.Add(MainSheetDetailsRowNo, 3, "Customer Count").Font.Bold = true;

                            var SClist = (from list in ds.Tables[0].AsEnumerable()
                                          orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                          orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                          group list by list.Field<string>("ServiceCenterName") into g
                                          select new { ServiceCenter = g.Key }).ToList();

                            for (int k = 0; k < SClist.Count; k++)//SC
                            {
                                var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                 where list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                                 select list).ToList();

                                MainSheetDetailsRowNo++;
                                mainCells.Add(MainSheetDetailsRowNo, 1, k + 1);
                                mainCells.Add(MainSheetDetailsRowNo, 2, SClist[k].ServiceCenter);
                                mainCells.Add(MainSheetDetailsRowNo, 3, Customers.Count);
                            }

                            for (int i = 0; i < BUlist.Count; i++)//BU
                            {
                                var SUlistByBU = (from list in ds.Tables[0].AsEnumerable()
                                                  where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                  group list by list.Field<string>("ServiceUnitName") into g
                                                  select new { ServiceUnitName = g.Key }).ToList();

                                for (int j = 0; j < SUlistByBU.Count; j++)//SU
                                {
                                    var SClistBySU = (from list in ds.Tables[0].AsEnumerable()
                                                      where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                      && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                      group list by list.Field<string>("ServiceCenterName") into g
                                                      select new { ServiceCenter = g.Key }).ToList();

                                    for (int k = 0; k < SClistBySU.Count; k++)//SC
                                    {

                                        var CyclelistBySC = (from list in ds.Tables[0].AsEnumerable()
                                                             where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                             && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                             && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                             orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                             orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                             group list by list.Field<string>("CycleName") into g
                                                             select new { CycleName = g.Key }).ToList();

                                        if (CyclelistBySC.Count > 0)
                                        {
                                            int DetailsRowNo = 2;
                                            int CustomersRowNo = 6;

                                            Worksheet sheet = xls.Workbook.Worksheets.Add(SClistBySU[k].ServiceCenter);
                                            Cells cells = sheet.Cells;
                                            Cell _objCell = null;

                                            for (int l = 0; l < CyclelistBySC.Count; l++)//Cycle
                                            {
                                                var BooklistByCycle = (from list in ds.Tables[0].AsEnumerable()
                                                                       where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                       && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                       && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                       && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                       orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                       orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                       group list by list.Field<string>("BookNumber") into g
                                                                       select new { BookNumber = g.Key }).ToList();

                                                for (int m = 0; m < BooklistByCycle.Count; m++)//Book
                                                {
                                                    var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                                     where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                     && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                     && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                     && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                     && list.Field<string>("BookNumber") == BooklistByCycle[m].BookNumber
                                                                     orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                     orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                     orderby Convert.ToString(list["CycleName"]) ascending
                                                                     select list).ToList();

                                                    if (Customers.Count() > 0)
                                                    {
                                                        _objCell = cells.Add(DetailsRowNo, 1, "Business Unit");
                                                        cells.Add(DetailsRowNo, 3, "Service Unit");
                                                        cells.Add(DetailsRowNo, 5, "Service Center");
                                                        cells.Add(DetailsRowNo + 1, 1, "Book Group");
                                                        cells.Add(DetailsRowNo + 1, 3, "Book Name");

                                                        cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                                        cells.Add(DetailsRowNo, 4, SUlistByBU[j].ServiceUnitName).Font.Bold = true;
                                                        cells.Add(DetailsRowNo, 6, SClistBySU[k].ServiceCenter).Font.Bold = true;
                                                        cells.Add(DetailsRowNo + 1, 2, CyclelistBySC[l].CycleName).Font.Bold = true;
                                                        cells.Add(DetailsRowNo + 1, 4, BooklistByCycle[m].BookNumber).Font.Bold = true;

                                                        _objCell = cells.Add(DetailsRowNo + 3, 1, "Sno");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 2, "Global Account No");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 3, "Old Account No");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 4, "Name");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 5, "Meter Number");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 6, "Service Address");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 7, "Tariff");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 8, "Previous Reading");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 9, "Current Reading");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 10, "Remarks");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;

                                                        for (int n = 0; n < Customers.Count; n++)
                                                        {
                                                            DataRow dr = (DataRow)Customers[n];
                                                            cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                                                            cells.Add(CustomersRowNo, 2, dr["GlobalAccountNumber"].ToString());
                                                            cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
                                                            cells.Add(CustomersRowNo, 4, dr["Name"].ToString());
                                                            cells.Add(CustomersRowNo, 5, dr["MeterNo"].ToString());
                                                            cells.Add(CustomersRowNo, 6, dr["Address"].ToString());
                                                            cells.Add(CustomersRowNo, 7, dr["ClassName"].ToString());
                                                            cells.Add(CustomersRowNo, 8, dr["PreviousReading"].ToString());
                                                            cells.Add(CustomersRowNo, 9, dr["CurrentReading"].ToString());
                                                            cells.Add(CustomersRowNo, 10, dr["Remarks"].ToString());

                                                            CustomersRowNo++;
                                                        }
                                                        DetailsRowNo = CustomersRowNo + 2;
                                                        CustomersRowNo = CustomersRowNo + 6;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                            fileName = Constants.BookWiseCustomerExportReport_Name.Replace(" ", string.Empty) + "_" + fileName;
                            string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                            //xls.FileName = filePath;
                            //xls.Save();
                            //_objiDsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");

                            if (!File.Exists(filePath))
                            {
                                xls.FileName = filePath;
                                xls.Save();
                            }
                            _objiDsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                        }
                    }
                    else
                    {
                        Message("No Customers are found", UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
                else
                {
                    Message("No Customers are found", UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            finally
            {
                DivLoading.Style.Add("display", "none");
            }
        }
    }
}