﻿<%@ Page Title=":: Customer Bulk Import ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    AutoEventWireup="true" Theme="Green" CodeBehind="CustomerImport.aspx.cs" Inherits="UMS_Nigeria.ConsumerManagement.CustomerImport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <%--  <asp:UpdatePanel ID="upBulkUpload1" runat="server">
            <ContentTemplate>--%>
        <asp:Panel ID="pnlMessage" runat="server">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <div class="text_total">
            <div class="text-heading">
                <asp:Literal ID="litAddBookNumber" runat="server" Text="Customer Bulk Upload"></asp:Literal>
            </div>
        </div>
        <div class="clear">
        </div>
        <div class="dot-line">
        </div>
        <div id="divBatchDetails" runat="server">
            <h3>
                <asp:Literal ID="litReadMethod" runat="server" Text=""></asp:Literal>
            </h3>
            <div class="consumer_feild" style="margin-left: 10px;">
                <div class="consume_name">
                    <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, BATCHDATE%>"></asp:Literal>
                    <span>*</span>
                </div>
                <div class="consume_input c_left" style="width: 175px">
                    <asp:TextBox ID="txtBatchDate" AutoComplete="off" CssClass="boxWidth" MaxLength="10"
                        onchange="DoBlur(this);" onblur="DoBlur(this);" Width="132px" runat="server"></asp:TextBox>
                    <asp:Image ID="imgDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                        vertical-align: middle" AlternateText="Calender" runat="server" />
                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtBatchDate"
                        FilterType="Custom,Numbers" ValidChars="/">
                    </asp:FilteredTextBoxExtender>
                    <span id="spanBatchDate" class="spancolor"></span>
                </div>
                <div class="clear pad_5">
                </div>
                <div class="consume_name">
                    <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, BATCHNUM%>"></asp:Literal>
                    <span>*</span>
                </div>
                <div class="consume_input c_left">
                    <%--<asp:TextBox ID="txtBatchNo" runat="server" CssClass="noinput" MaxLength="6" Style="width: 158px;"
                        onblur="return TextBoxBlurValidationlbl(this,'spanBatchNo','Batch No')" AutoPostBack="True"
                        OnTextChanged="txtBatchNo_TextChanged"></asp:TextBox>--%>
                    <asp:TextBox ID="txtBatchNo" runat="server" CssClass="noinput" MaxLength="6" Style="width: 158px;"
                        onblur="return TextBoxBlurValidationlbl(this,'spanBatchNo','Batch No')"></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtBatchNo"
                        FilterType="Numbers">
                    </asp:FilteredTextBoxExtender>
                    <span id="spanBatchNo" class="spancolor"></span>
                </div>
                <div class="consume_name" style="margin-left: 39px;">
                    <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource,  BATCHNAME%>"></asp:Literal>
                    <span>*</span>
                </div>
                <div class="consume_input c_left">
                    <asp:TextBox ID="txtBatchName" runat="server" MaxLength="16" onkeyup="GetCurrencyFormate(this);"
                        onblur="return TextBoxBlurValidationlbl(this,'spanBatchName','Batch Name')" CssClass="noinput"
                        Style="width: 158px;"></asp:TextBox>
                    <span id="spanBatchName" class="spancolor"></span>
                </div>
                <div class="clear pad_5">
                </div>
                <div class="consume_name">
                    <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, DETAILS%>"> </asp:Literal>
                </div>
                <div class="consume_input c_left">
                    <asp:TextBox ID="txtDetails" runat="server" TextMode="MultiLine" placeholder="Enter Details"></asp:TextBox>
                    <span id="spanPaymentMode" class="spancolor"></span>
                </div>
                <div class="clear pad_5">
                </div>
                <div style="margin-left: 163px; margin-top: 4px;">
                    <div class=" fltl">
                        <asp:Button ID="btnNext" CssClass="box_s" Text="<%$ Resources:Resource, NEXT%>" runat="server"
                            OnClick="btnNext_Click" OnClientClick="return Validate();" />
                    </div>
                </div>
            </div>
        </div>
        <div class="clear pad_10">
        </div>
        <div id="divBrowseDoc" runat="server">
            <div class="consumer_total">
                <div class="pad_5">
                </div>
                <h3>
                    <asp:Literal ID="Literal6" runat="server" Text="Customer Bulk Upload"></asp:Literal>
                </h3>
                <div class="pad_10">
                </div>
            </div>
            <div class="consumer_feild">
                <div class="consume_name" style="width: 163px;">
                    <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, UPLOAD_DOCUMENT%>"></asp:Literal>
                    <span>*</span>
                </div>
                <div class="consume_input c_left">
                    <asp:FileUpload ID="upDOCExcel" runat="server" />
                </div>
                <div class="pad_10">
                    <a href="../Uploads/BulkCustomerUpload/SampleCustomerDetails.rar" target="_blank">Download
                        Sample Document</a>
                </div>
                <div class="clear pad_10">
                </div>
                <div style="color: #939393; font-size: 12px; display: none; margin-left: 14px;">
                    <b class="spancolor">Note:</b> <span>Please provide customers data only in uploaded
                        Excel.</span>
                </div>
                <div class="clear pad_10">
                </div>
                <div style="margin-left: 199px;">
                    <div class=" fltl">
                        <asp:Button ID="btnUploadNext" CssClass="box_s" Text="<%$ Resources:Resource, NEXT%>"
                            runat="server" OnClick="btnUploadNext_Click" />
                    </div>
                </div>
            </div>
        </div>
        <div class="clear pad_10">
        </div>
        <div id="divValidateData" runat="server" class="validprint-grid">
            <h3 class="fltl">
                <%--<asp:Literal ID="Literal7" runat="server" Text="Valid Data"></asp:Literal>--%>
                <asp:Literal ID="Literal7" runat="server" Text=""></asp:Literal>
            </h3>
            <div class="clear">
            </div>
            <div class="grid fltl" id="divValidPrint" runat="server" align="center" style="overflow: auto;
                min-height: 95px; max-height: 400px;">
                <div class="consumer_total">
                </div>
                <asp:GridView ID="gvValidData" runat="server" AutoGenerateColumns="false" Visible="false"
                    AlternatingRowStyle-BackColor="#E8FAEA">
                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                    <EmptyDataTemplate>
                        No records found.
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField HeaderText="Name" DataField="Name" />
                        <asp:BoundField HeaderText="SurName" DataField="SurName" />
                        <asp:BoundField HeaderText="EmailId" DataField="EmailId" />
                        <asp:BoundField HeaderText="DocumentNo" DataField="DocumentNo" />
                        <asp:BoundField HeaderText="OtherContactNo" DataField="OtherContactNo" />
                        <asp:BoundField HeaderText="PostalLandMark" DataField="PostalLandMark" />
                        <asp:BoundField HeaderText="PostalStreet" DataField="PostalStreet" />
                        <asp:BoundField HeaderText="PostalCity" DataField="PostalCity" />
                        <asp:BoundField HeaderText="PostalHouseNo" DataField="PostalHouseNo" />
                        <asp:BoundField HeaderText="PostalDetails" DataField="PostalDetails" />
                        <asp:BoundField HeaderText="PostalZipCode" DataField="PostalZipCode" />
                        <asp:BoundField HeaderText="IdentityNo" DataField="IdentityNo" />
                        <asp:BoundField HeaderText="IdentityId" DataField="IdentityId" />
                        <asp:BoundField HeaderText="BookNo" DataField="BookNo" />
                        <asp:BoundField HeaderText="ServiceLandMark" DataField="ServiceLandMark" />
                        <asp:BoundField HeaderText="ServiceStreet" DataField="ServiceStreet" />
                        <asp:BoundField HeaderText="ServiceCity" DataField="ServiceCity" />
                        <asp:BoundField HeaderText="ServiceHouseNo" DataField="ServiceHouseNo" />
                        <asp:BoundField HeaderText="ServiceDetails" DataField="ServiceDetails" />
                        <asp:BoundField HeaderText="ServiceZipCode" DataField="ServiceZipCode" />
                        <asp:BoundField HeaderText="ServiceEmailId" DataField="ServiceEmailId" />
                        <asp:BoundField HeaderText="MobileNo" DataField="MobileNo" />
                        <asp:BoundField HeaderText="TariffId" DataField="TariffId" />
                        <asp:BoundField HeaderText="MultiplicationFactor" DataField="MultiplicationFactor" />
                        <asp:BoundField HeaderText="AccountTypeId" DataField="AccountTypeId" />
                        <asp:BoundField HeaderText="ApplicationDate" DataField="ApplicationDate" />
                        <asp:BoundField HeaderText="ReadCodeId" DataField="ReadCodeId" />
                        <asp:BoundField HeaderText="OldAccountNo" DataField="OldAccountNo" />
                        <asp:BoundField HeaderText="ConnectionDate" DataField="ConnectionDate" />
                        <asp:BoundField HeaderText="MeterNo" DataField="MeterNo" />
                        <asp:BoundField HeaderText="MeterSerialNo" DataField="MeterSerialNo" />
                        <asp:BoundField HeaderText="MeterTypeId" DataField="MeterTypeId" />
                        <asp:BoundField HeaderText="PhaseId" DataField="PhaseId" />
                        <asp:BoundField HeaderText="MeterStatusId" DataField="MeterStatusId" />
                        <asp:BoundField HeaderText="InitialReading" DataField="InitialReading" />
                        <asp:BoundField HeaderText="CurrentReading" DataField="CurrentReading" />
                        <%-- <asp:BoundField HeaderText="AgencyId" DataField="AgencyId" />
                        <asp:BoundField HeaderText="AgencyDetails" DataField="AgencyDetails" />--%>
                        <asp:BoundField HeaderText="MinimumReading" DataField="MinimumReading" />
                    </Columns>
                </asp:GridView>
                <asp:GridView ID="gvExcelRecords" runat="server" AutoGenerateColumns="false" AlternatingRowStyle-BackColor="#E8FAEA"
                    OnRowDataBound="gvExcelRecords_RowDataBound">
                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                    <EmptyDataTemplate>
                        No records present.
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField HeaderText="SNO" DataField="SNO" />
                        <asp:BoundField HeaderText="Title" DataField="Title" />
                        <asp:BoundField HeaderText="FirstName" DataField="FirstName" />
                        <asp:BoundField HeaderText="MiddleName" DataField="MiddleName" />
                        <asp:BoundField HeaderText="LastName" DataField="LastName" />
                        <asp:BoundField HeaderText="KnownAs" DataField="KnownAs" />
                        <asp:BoundField HeaderText="EmailId" DataField="EmailId" />
                        <asp:BoundField HeaderText="HomeContactNumber" DataField="HomeContactNumber" />
                        <asp:BoundField HeaderText="BusinessContactNumber" DataField="BusinessContactNumber" />
                        <asp:BoundField HeaderText="OtherContactNumber" DataField="OtherContactNumber" />
                        <asp:BoundField HeaderText="DocumentNo" DataField="DocumentNo" />
                        <asp:BoundField HeaderText="ApplicationDate" DataField="ApplicationDate" />
                        <asp:BoundField HeaderText="ConnectionDate" DataField="ConnectionDate" />
                        <asp:BoundField HeaderText="SetupDate" DataField="SetupDate" />
                        <asp:BoundField HeaderText="OldAccountNo" DataField="OldAccountNo" />
                        <asp:BoundField HeaderText="CustomerType" DataField="CustomerType" />
                        <asp:BoundField HeaderText="BookCode" DataField="BookCode" />
                        <asp:BoundField HeaderText="PoleID" DataField="PoleID" />
                        <asp:BoundField HeaderText="MeterNumber" DataField="MeterNumber" />
                        <asp:BoundField HeaderText="Tariff" DataField="Tariff" />
                        <asp:BoundField HeaderText="IsEmbassyCustomer" DataField="IsEmbassyCustomer" />
                        <asp:BoundField HeaderText="EmbassyCode" DataField="EmbassyCode" />
                        <asp:BoundField HeaderText="Phase" DataField="Phase" />
                        <asp:BoundField HeaderText="ReadType" DataField="ReadType" />
                        <asp:BoundField HeaderText="ServiceAddress_HouseNo" DataField="ServiceAddress_HouseNo" />
                        <asp:BoundField HeaderText="ServiceAddress_StreetName" DataField="ServiceAddress_StreetName" />
                        <asp:BoundField HeaderText="ServiceAddress_City" DataField="ServiceAddress_City" />
                        <asp:BoundField HeaderText="ServiceAddress_Landmark" DataField="ServiceAddress_Landmark" />
                        <asp:BoundField HeaderText="ServiceAddress_Details" DataField="ServiceAddress_Details" />
                        <asp:BoundField HeaderText="ServiceAddress_AreaCode" DataField="ServiceAddress_AreaCode" />
                        <asp:BoundField HeaderText="ServiceAddress_Latitude" DataField="ServiceAddress_Latitude" />
                        <asp:BoundField HeaderText="ServiceAddress_Longitude" DataField="ServiceAddress_Longitude" />
                        <asp:BoundField HeaderText="ServiceAddress_ZipCode" DataField="ServiceAddress_ZipCode" />
                        <asp:BoundField HeaderText="PostalAddress_HouseNo" DataField="PostalAddress_HouseNo" />
                        <asp:BoundField HeaderText="PostalAddress_StreetName" DataField="PostalAddress_StreetName" />
                        <asp:BoundField HeaderText="PostalAddress_City" DataField="PostalAddress_City" />
                        <asp:BoundField HeaderText="PostalAddress_Landmark" DataField="PostalAddress_Landmark" />
                        <asp:BoundField HeaderText="PostalAddress_Details" DataField="PostalAddress_Details" />
                        <asp:BoundField HeaderText="Postal_AreaCode" DataField="Postal_AreaCode" />
                        <asp:BoundField HeaderText="PostalAddress_Latitude" DataField="PostalAddress_Latitude" />
                        <asp:BoundField HeaderText="PostalAddress_Longitude" DataField="PostalAddress_Longitude" />
                        <asp:BoundField HeaderText="PostalAddress_ZipCode" DataField="PostalAddress_ZipCode" />
                        <asp:BoundField HeaderText="IsCAPMI" DataField="IsCAPMI" />
                        <asp:BoundField HeaderText="MeterAmount" DataField="MeterAmount" />
                        <asp:BoundField HeaderText="InitialBillingKWh" DataField="InitialBillingKWh" />
                        <asp:BoundField HeaderText="InitialReading" DataField="InitialReading" />
                        <asp:BoundField HeaderText="PresentReading" DataField="PresentReading" />
                        <asp:BoundField HeaderText="AverageReading" DataField="AverageReading" />
                        <asp:BoundField HeaderText="Highestconsumption" DataField="Highestconsumption" />
                        <asp:BoundField HeaderText="OutStandingAmount" DataField="OutStandingAmount" />
                        <asp:BoundField HeaderText="OpeningBalance" DataField="OpeningBalance" />
                        <%--<asp:BoundField HeaderText="IsValid" DataField="IsValid" />--%>
                        <asp:TemplateField HeaderText="OpeningBalance">
                            <ItemTemplate>
                                <asp:Label ID="lblIsValid" Visible="true" runat="server" Text='<%#Eval("IsValid") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField HeaderText="Comments" DataField="Comments" />--%>
                        <asp:TemplateField HeaderText="Comments">
                            <ItemTemplate>
                                <div><%#Eval("Comments")%></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <div class="clear pad_10">
            </div>
            <h3 class="fltl">
                <%--<asp:Literal ID="Literal8" runat="server" Text="In Valid Data"></asp:Literal>--%>
                <asp:Literal ID="Literal8" runat="server" Text=""></asp:Literal>
            </h3>
            <div class="grid " id="divInValidPrint" runat="server" align="center" style="overflow: auto;
                min-height: 95px; max-height: 400px;">
                <div class="consumer_total">
                </div>
                <asp:GridView ID="gvInValidData" runat="server" AutoGenerateColumns="false" AlternatingRowStyle-BackColor="#E8FAEA">
                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                    <EmptyDataTemplate>
                        Zero Invalid Records found.
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField HeaderText="Name" DataField="Name" />
                        <asp:BoundField HeaderText="Surname" DataField="SurName" />
                        <asp:BoundField HeaderText="EmailId" DataField="EmailId" />
                        <asp:BoundField HeaderText="DocumentNo" DataField="DocumentNo" />
                        <asp:BoundField HeaderText="Other ContactNo" DataField="OtherContactNo" />
                        <asp:BoundField HeaderText="Postal LandMark" DataField="PostalLandMark" />
                        <asp:BoundField HeaderText="Postal Street" DataField="PostalStreet" />
                        <asp:BoundField HeaderText="Postal City" DataField="PostalCity" />
                        <asp:BoundField HeaderText="Postal HouseNo" DataField="PostalHouseNo" />
                        <asp:BoundField HeaderText="Postal Details" DataField="PostalDetails" />
                        <asp:BoundField HeaderText="Postal ZipCode" DataField="PostalZipCode" />
                        <asp:BoundField HeaderText="IdentityNo" DataField="IdentityNo" />
                        <asp:BoundField HeaderText="IdentityId" DataField="IdentityId" />
                        <asp:BoundField HeaderText="BookNo" DataField="BookNo" />
                        <asp:BoundField HeaderText="Service Landmark" DataField="ServiceLandMark" />
                        <asp:BoundField HeaderText="Service Street" DataField="ServiceStreet" />
                        <asp:BoundField HeaderText="Service City" DataField="ServiceCity" />
                        <asp:BoundField HeaderText="Service HouseNo" DataField="ServiceHouseNo" />
                        <asp:BoundField HeaderText="Service Details" DataField="ServiceDetails" />
                        <asp:BoundField HeaderText="Service Zipcode" DataField="ServiceZipCode" />
                        <asp:BoundField HeaderText="Service EmailId" DataField="ServiceEmailId" />
                        <asp:BoundField HeaderText="MobileNo" DataField="MobileNo" />
                        <asp:BoundField HeaderText="TariffId" DataField="TariffId" />
                        <asp:BoundField HeaderText="Multiplication Factor" DataField="MultiplicationFactor" />
                        <asp:BoundField HeaderText="Account TypeId" DataField="AccountTypeId" />
                        <asp:BoundField HeaderText="Application Date" DataField="ApplicationDate" />
                        <asp:BoundField HeaderText="ReadCodeId" DataField="ReadCodeId" />
                        <asp:BoundField HeaderText="Old AccountNo" DataField="OldAccountNo" />
                        <asp:BoundField HeaderText="Connection Date" DataField="ConnectionDate" />
                        <asp:BoundField HeaderText="MeterNo" DataField="MeterNo" />
                        <asp:BoundField HeaderText="Meter SerialNo" DataField="MeterSerialNo" />
                        <asp:BoundField HeaderText="Meter TypeId" DataField="MeterTypeId" />
                        <asp:BoundField HeaderText="PhaseId" DataField="PhaseId" />
                        <asp:BoundField HeaderText="Meter StatusId" DataField="MeterStatusId" />
                        <asp:BoundField HeaderText="Initial Reading" DataField="InitialReading" />
                        <asp:BoundField HeaderText="Current Reading" DataField="CurrentReading" />
                        <%--       <asp:BoundField HeaderText="AgencyId" DataField="AgencyId" />
                        <asp:BoundField HeaderText="AgencyDetails" DataField="AgencyDetails" />--%>
                        <asp:BoundField HeaderText="Minimum Reading" DataField="MinimumReading" />
                        <asp:BoundField HeaderText="Comments" DataField="Comments" />
                    </Columns>
                </asp:GridView>
            </div>
            <div class="pad_10 clear">
                <div class="pad_10 clear">
                    <div class=" fltr">
                        <asp:Button ID="btnUpload" CssClass="box_s" Text="Upload" Visible="false" runat="server" OnClick="btnUpload_Click" />
                    </div>
                </div>
            </div>
        </div>
        <%--            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnUploadNext" />
            </Triggers>
        </asp:UpdatePanel>--%>
        <asp:ModalPopupExtender ID="mpeGenMessage" runat="server" PopupControlID="pnlGenMessage"
            TargetControlID="hfGenMessage" BackgroundCssClass="modalBackground" CancelControlID="BtnGenMessageClose">
        </asp:ModalPopupExtender>
        <asp:HiddenField ID="hfGenMessage" runat="server" />
        <asp:Panel ID="pnlGenMessage" runat="server" CssClass="modalPopup" Style="display: none;
            border-color: #CD3333;">
            <div class="popheader" style="background-color: #CD3333;">
                <asp:Label ID="lblGenMessage" runat="server" Text=""></asp:Label>
            </div>
            <div class="footer" align="right">
                <asp:Button ID="BtnGenMessageClose" runat="server" Text="<%$ Resources:Resource, OK%>"
                    CssClass="no" />
            </div>
        </asp:Panel>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).ready(function () {
            Calendar('<%=txtBatchDate.ClientID %>', '<%=imgDate.ClientID %>', "-2:+8");
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            Calendar('<%=txtBatchDate.ClientID %>', '<%=imgDate.ClientID %>', "-2:+8");
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/CustomerBulkUpload.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
