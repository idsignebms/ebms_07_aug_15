﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using iDSignHelper;
using UMS_NigeriaBAL;
using System.Xml;
using System.Configuration;
using System.IO;
using UMS_NigeriaBE;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Globalization;
using Resources;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class CustomerImport : System.Web.UI.Page
    {
        #region Members
        CommonMethods _objCommonMethods = new CommonMethods();
        iDsignBAL _objIdsignBal = new iDsignBAL();
        UserManagementBAL objUserManagementBAL = new UserManagementBAL();

        string Key = "AddEmployee";
        XmlDocument xml = null;
        public int PageNum;
        string Filename;
        DataTable dtExcel = new DataTable();
        DataTable dtValid = new DataTable();
        DataTable dtInvalid = new DataTable();
        BillingBE objBillingBE = new BillingBE();
        ConsumerBal objConsumerBal = new ConsumerBal();
        DataTable dtAccountNo = new DataTable();
        DataSet ds = new DataSet("ConsumerDetails");
        DataTable dtBulkCopy = new DataTable("dtBulkCopy");
        // DataTable dtCustDetails = new DataTable();
        DataSet dsValidationTables = new DataSet();

        #endregion

        #region Properties
        public string BatchNo
        {
            get { return txtBatchNo.Text.Trim(); }
            set { txtBatchNo.Text = value; }
        }
        public string BatchName
        {
            get { return txtBatchName.Text.Trim(); }
            set { txtBatchName.Text = value; }
        }
        public string Details
        {
            get { return txtDetails.Text.Trim(); }
            set { txtDetails.Text = value; }
        }
        public string BatchDate
        {
            get { return txtBatchDate.Text.Trim(); }
            set { txtBatchDate.Text = value; }
        }
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[Resources.UMS_Resource.SESSION_LOGINID] != null)
            {
                lblMessage.Text = pnlMessage.CssClass = string.Empty;
                if (!IsPostBack)
                {
                    divBrowseDoc.Visible = false;
                    divValidateData.Visible = false;
                }
            }
            else
                Response.Redirect(Resources.UMS_Resource.DEFAULT_PAGE);
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            divBrowseDoc.Visible = true;
            divValidateData.Visible = false;
            divInValidPrint.Visible = false;
        }

        protected void btnUploadNext_Click(object sender, EventArgs e)
        {
            ReadUploads();
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            ds.Tables.Add(dtBulkCopy);
            dtBulkCopy = null;
            if (Session["dtValid"] != null)
            {
                dtBulkCopy = (DataTable)Session["dtValid"];
                if (dtBulkCopy.Rows.Count > 0)
                {
                    if (AddBatchDetailsToDT().Rows.Count > 0)
                    {
                        ds.Tables.Add(dtBulkCopy);
                        ds.Tables.Add(AddBatchDetailsToDT());
                        //BulkCopy(dtBulkCopy, "Tbl_CustomerDetails", 500);
                        try
                        {
                            if (CustomerBulkUploads(ds) > 0)
                            {
                                UploadFinsish();
                                Message(Resources.Resource.CUST_SUCSF_UPL, Resources.UMS_Resource.MESSAGETYPE_SUCCESS);
                            }
                            else
                            {
                                Message(Resources.Resource.CUST_NOT_UPLD, Resources.UMS_Resource.MESSAGETYPE_ERROR);
                            }
                        }
                        catch
                        {
                            Message(Resources.Resource.EXCP_OCRD, Resources.UMS_Resource.MESSAGETYPE_ERROR);
                        }
                    }
                    else
                    {
                        Message(Resources.Resource.BATCH_CRE_EXCP, Resources.UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
                else
                {
                    lblGenMessage.Text = Resources.Resource.NO_ROWS_FOR_UPLD;
                    mpeGenMessage.Show();
                }
            }
            else
            {
                Message(Resources.Resource.UPL_SES_EXP, Resources.UMS_Resource.MESSAGETYPE_ERROR);
            }

        }

        protected void txtBatchNo_TextChanged(object sender, EventArgs e)
        {
            objBillingBE.BatchNo = Convert.ToInt32(BatchNo);
            objBillingBE = _objIdsignBal.DeserializeFromXml<BillingBE>(objConsumerBal.IsExistsBatchCustBulkUploadBAL(objBillingBE));
            if (objBillingBE.IsExists)
            {
                //UploadFinsish();
                Message(Resources.Resource.BTCH_EXISTS, Resources.UMS_Resource.MESSAGETYPE_SUCCESS);
                txtBatchName.Focus();
            }
            else
                txtBatchName.Focus();
        }

        protected void gvExcelRecords_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblIsValid = (Label)e.Row.FindControl("lblIsValid");
                if (!Convert.ToBoolean(lblIsValid.Text))
                    e.Row.BackColor = System.Drawing.Color.Tomato;
            }
        }

        #endregion

        #region Methods

        public void ReadUploads()
        {
            int count = 0;
            DataColumn Comments = new DataColumn("Comments", typeof(string));
            DataColumn IsValid = new DataColumn("IsValid", typeof(bool));

            DataColumn PhaseId = new DataColumn("PhaseId", typeof(string));
            DataColumn CustomerTypeId = new DataColumn("CustomerTypeId", typeof(string));
            DataColumn BookCodeId = new DataColumn("BookCodeId", typeof(string));
            DataColumn TariffClassID = new DataColumn("TariffClassID", typeof(string));
            DataColumn ReadCodeID = new DataColumn("ReadCodeID", typeof(string));

            DataColumn RowCount = new DataColumn("RowCount", typeof(string));

            string fileName = InsertExcel();

            if (Path.GetExtension(fileName) == ".xls" || Path.GetExtension(fileName) == ".xlsx")
            {
                //dtExcel = _objCommonMethods.ExcelImport(Server.MapPath(ConfigurationManager.AppSettings["BulkCustomerUpload"].ToString()) + fileName);
                DataSet dsHeaders = new DataSet();
                dsHeaders.ReadXml(Server.MapPath(ConfigurationSettings.AppSettings["NewCustomerBulkUploadHeaders"]));
                //Import excel with oledb starts
                string FolderPath = ConfigurationManager.AppSettings["BulkCustomerUpload"];
                string FilePath = Server.MapPath(FolderPath + "/" + fileName);
                //string cnstr = "Provider=Microsoft.Jet.Oledb.4.0;Data Source=" + FilePath + "; Extended Properties=Excel 4.0";///upto 2007
                String cnstr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FilePath + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";//2007 Onwards


                OleDbConnection oledbConn = new OleDbConnection(cnstr);
                String[] s = _objCommonMethods.GetExcelSheetNames(FilePath);
                string strSQL = "SELECT * FROM [" + s[0] + "]";
                OleDbCommand cmd = new OleDbCommand(strSQL, oledbConn);
                DataSet ds = new DataSet();
                OleDbDataAdapter odapt = new OleDbDataAdapter(cmd);
                odapt.Fill(ds);
                dtExcel = ds.Tables[0];
                //Import excel with oledb ends

                //Validating Excel headers 
                #region Validating Excel headers
                for (int i = 0; i < dtExcel.Columns.Count; i++)
                {
                    if (dtExcel.Columns.Count == dsHeaders.Tables[0].Rows.Count)
                    {
                        string HeaderName = dtExcel.Columns[i].ColumnName;
                        var result = (from x in dsHeaders.Tables[0].AsEnumerable()
                                      where x.Field<string>("Names").Trim() == HeaderName.Trim()
                                      select new
                                      {
                                          Names = x.Field<string>("Names")
                                      }).SingleOrDefault();
                        if (result == null)
                        {
                            count++;
                            //lblMsgPopup.Text = Resource.COLUMN_NOT_EXISTS;
                            lblGenMessage.Text = Resources.Resource.COL_DSNT_MATCH;
                            mpeGenMessage.Show();
                            break;
                        }
                    }
                    else
                    {
                        count++;
                        lblGenMessage.Text = Resources.Resource.COL_DSNT_MATCH;
                        mpeGenMessage.Show();
                    }
                }
                #endregion

                //If valid headers and sequence i.e. Count is zero
                if (count == 0)
                {
                    //if data exists in exell then adding Comments and IsValid Field in dtExcel
                    if (dtExcel.Rows.Count > 0)
                    {
                        dtExcel.Columns.Add(Comments);
                        dtExcel.Columns.Add(IsValid);
                        dtExcel.Columns.Add(PhaseId);
                        dtExcel.Columns.Add(CustomerTypeId);
                        dtExcel.Columns.Add(BookCodeId);
                        dtExcel.Columns.Add(TariffClassID);
                        dtExcel.Columns.Add(ReadCodeID);
                        dtExcel.Columns.Add(RowCount);
                        
                    }

                    #region Reading XML for Mandatory fields.

                    DataSet dsIsMandatoryXML = new DataSet();
                    dsIsMandatoryXML.ReadXml(Server.MapPath(ConfigurationSettings.AppSettings["NewCustomer_Mandatory_Fields"]));
                    DataTable dtMandatory = dsIsMandatoryXML.Tables[0];
                    #endregion

                    #region Row validation
                    

                    //Fetching data tables from Database for master data validation
                    CustomerBulkBe objCustomerBulkBe = new CustomerBulkBe();
                    objCustomerBulkBe.BUID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                    dsValidationTables = objConsumerBal.GetCustDetailBlkUpldBAL(objCustomerBulkBe);

                    int countZero = 0;
                    string columnName = string.Empty;
                    string value = string.Empty;
                    string comment = string.Empty;

                    foreach (DataRow row in dtExcel.Rows)
                    {
                        row["IsValid"] = "true";
                        row["RowCount"] = "";
                        foreach (DataColumn item in dtExcel.Columns)
                        {
                            columnName = item.ColumnName;
                            
                            bool IsMandatory = false;
                            //Fetching list for mandatory field
                            List<string> lstResult = (from table in dtMandatory.AsEnumerable()
                                                      where table.Field<string>("ColumnName") == columnName
                                                      select table.Field<string>("IsMandatory")).ToList();

                            //Fetching list for MaxLength field
                            List<string> lstMaxlength = (from table in dsHeaders.Tables[0].AsEnumerable()
                                                         where table.Field<string>("Names") == columnName
                                                         select table.Field<string>("MaxLength")).ToList();

                            if (lstResult.Count > 0)
                                IsMandatory = Convert.ToBoolean(lstResult[0]);

                            value = row[item.ColumnName].ToString();
                            int MaxLength = 0;
                            if (lstMaxlength.Count > 0)
                                MaxLength = Convert.ToInt32(lstMaxlength[0]) > 0 ? Convert.ToInt32(lstMaxlength[0]) : Int32.MaxValue;
                            #region Validating Input data from excel
                            switch (columnName)
                            {
                                case "SNO":
                                    IsMandatory = true;
                                    comment += ValidationGroup1(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "Title":
                                    comment += ValidationGroup7(columnName, value, MaxLength, false, true, IsMandatory);
                                    break;
                                case "FirstName":
                                    comment += ValidationGroup1(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "MiddleName":
                                    comment += ValidationGroup1(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "LastName":
                                    comment += ValidationGroup1(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "KnownAs":
                                    comment += ValidationGroup1(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "EmailId":
                                    MaxLength = int.MaxValue;
                                    comment += ValidationGroup4(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "HomeContactNumber":
                                    //MaxLength = int.MaxValue;
                                    comment += ValidationGroup4(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "BusinessContactNumber":
                                    //MaxLength = int.MaxValue;
                                    comment += ValidationGroup4(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "OtherContactNumber":
                                    //MaxLength = int.MaxValue;
                                    comment += ValidationGroup4(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "ApplicationDate":
                                    comment += ValidationGroup5(columnName, value, IsMandatory);
                                    break;
                                case "ConnectionDate":
                                    comment += ValidationGroup5(columnName, value, IsMandatory);
                                    break;
                                case "SetupDate":
                                    comment += ValidationGroup6(columnName, value, row["ApplicationDate"].ToString(), IsMandatory);
                                    break;
                                case "OldAccountNo":
                                    //MaxLength = 20;
                                    comment += ValidationGroup7(columnName, value, MaxLength, true, true, IsMandatory);
                                    break;
                                case "CustomerType":
                                    //MaxLength = 50;
                                    comment += ValidationGroup7(columnName, value, MaxLength, false, true, IsMandatory);
                                    if (comment.Length == 0)
                                        row["CustomerTypeId"] = GetCustomerTypeId(value);
                                    break;
                                case "BookCode":
                                    //MaxLength = 20;
                                    comment += ValidationGroup7(columnName, value, MaxLength, false, true, IsMandatory);
                                    if (comment.Length == 0)
                                        row["BookCodeId"] = GetBookCodeId(value);
                                    break;
                                //case "PoleID":
                                //    MaxLength = 50;
                                //    comment += ValidationGroup1(columnName, value, MaxLength, 0);
                                //    break;
                                case "MeterNumber":
                                    //MaxLength = 50;
                                    comment += ValidationGroup7(columnName, value, MaxLength, false, true, IsMandatory);
                                    break;
                                case "Tariff":
                                    //MaxLength = 200;
                                    comment += ValidationGroup7(columnName, value, MaxLength, false, true, IsMandatory);
                                    if (comment.Length == 0)
                                        row["TariffClassId"] = GetTariffClassId(value);
                                    break;
                                case "IsEmbassyCustomer":
                                    comment += ValidationGroup8(columnName, value, IsMandatory);
                                    break;
                                case "EmbassyCode": 
                                    //MaxLength = 20;
                                    if (row["IsEmbassyCustomer"].ToString().ToUpper() == "YES")
                                        comment += ValidationGroup1(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "Phase": 
                                    //MaxLength = 50;
                                    comment += ValidationGroup7(columnName, value, MaxLength, false, true, IsMandatory);
                                    if (comment.Length == 0)
                                        row["PhaseId"] = GetPhaseID(value);
                                    break;
                                case "ReadType": 
                                    //MaxLength = 20;
                                    comment += ValidationGroup7(columnName, value, MaxLength, false, true, IsMandatory);
                                    if (comment.Length == 0)
                                        row["ReadCodeId"] = GetReadTypeID(value);
                                    break;
                                case "ServiceAddress_HouseNo": 
                                    //MaxLength = 200;
                                    comment += ValidationGroup1(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "ServiceAddress_StreetName": 
                                    //MaxLength = 200;
                                    comment += ValidationGroup1(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "ServiceAddress_City": 
                                    //MaxLength = 200;
                                    comment += ValidationGroup1(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "ServiceAddress_Landmark": 
                                    //MaxLength = 200;
                                    comment += ValidationGroup1(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "ServiceAddress_Details": 
                                    //MaxLength = 200;
                                    comment += ValidationGroup1(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "ServiceAddress_AreaCode": 
                                    //MaxLength = 20;
                                    comment += ValidationGroup7(columnName, value, MaxLength, true, true, IsMandatory);
                                    break;
                                case "ServiceAddress_Latitude": 
                                    //MaxLength = 50;
                                    comment += ValidationGroup1(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "ServiceAddress_Longitude": 
                                    //MaxLength = 50;
                                    comment += ValidationGroup1(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "ServiceAddress_ZipCode": 
                                    //MaxLength = 6;
                                    comment += ValidationGroup1(columnName, value, MaxLength, 6, IsMandatory);
                                    break;
                                case "PostalAddress_HouseNo": 
                                    //MaxLength = 200;
                                    comment += ValidationGroup1(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "PostalAddress_StreetName": 
                                    //MaxLength = 200;
                                    comment += ValidationGroup1(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "PostalAddress_City": 
                                    //MaxLength = 200;
                                    comment += ValidationGroup1(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "PostalAddress_Landmark": 
                                    //MaxLength = 200;
                                    comment += ValidationGroup1(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "PostalAddress_Details": 
                                    //MaxLength = 200;
                                    comment += ValidationGroup1(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "Postal_AreaCode": 
                                    //MaxLength = 20;
                                    comment += ValidationGroup7(columnName, value, MaxLength, true, true, IsMandatory);
                                    break;
                                case "PostalAddress_Latitude": 
                                    //MaxLength = 50;
                                    comment += ValidationGroup1(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "PostalAddress_Longitude": 
                                    //MaxLength = 50;
                                    comment += ValidationGroup1(columnName, value, MaxLength, 0, IsMandatory);
                                    break;
                                case "PostalAddress_ZipCode": 
                                    //MaxLength = 6;
                                    comment += ValidationGroup1(columnName, value, MaxLength, 6, IsMandatory);
                                    break;
                                case "IsCAPMI":
                                    comment += ValidationGroup8(columnName, value, IsMandatory);
                                    break;
                                case "MeterAmount":
                                    if (!string.IsNullOrEmpty(value))
                                        comment += ValidationGroup9(columnName, value, IsMandatory);
                                    break;
                                case "InitialBillingKWh":
                                    if (!string.IsNullOrEmpty(value))
                                        comment += ValidationGroup7(columnName, value, MaxLength, true, false, IsMandatory);
                                    break;
                                case "InitialReading":
                                    if (!string.IsNullOrEmpty(value))
                                        comment += ValidationGroup7(columnName, value, MaxLength, true, false, IsMandatory);
                                    break;
                                case "PresentReading":
                                    if (!string.IsNullOrEmpty(value))
                                        comment += ValidationGroup7(columnName, value, MaxLength, true, false, IsMandatory);
                                    break;
                                case "AverageReading":
                                    if (!string.IsNullOrEmpty(value))
                                        comment += ValidationGroup7(columnName, value, MaxLength, true, false, IsMandatory);
                                    break;
                                case "Highestconsumption":
                                    if (!string.IsNullOrEmpty(value))
                                        comment += ValidationGroup7(columnName, value, MaxLength, true, false, IsMandatory);
                                    break;
                                case "OutStandingAmount":
                                    if (!string.IsNullOrEmpty(value))
                                        comment += ValidationGroup9(columnName, value, IsMandatory);
                                    break;
                                case "OpeningBalance":
                                    if (!string.IsNullOrEmpty(value))
                                        comment += ValidationGroup9(columnName, value, IsMandatory);
                                    break;


                            }
                            #endregion
                            if (comment.Length > 0)
                            {
                                row["IsValid"] = "false";
                                row["Comments"] = comment;
                            }
                        }
                        comment = string.Empty;
                    }
                    var listOfValidData = from dtLIst in dtExcel.AsEnumerable()
                                          where dtLIst.Field<bool>("IsValid") == true
                                          select dtLIst;
                    if (listOfValidData.Count() > 0)
                    {
                        dtValid = listOfValidData.CopyToDataTable();
                        Session["dtValid"] = dtValid;
                        if (listOfValidData.Count() == dtExcel.Rows.Count)
                            btnUpload.Visible = true;
                    }

                    gvExcelRecords.DataSource = dtExcel;
                    gvExcelRecords.DataBind();


                    #endregion

                    //#region Rows validation loop

                    //ConsumerListBe buConsumerListBE = new ConsumerListBe();
                    //dsValidationTables = objConsumerBal.GetCustDetailBlkUpldBAL();

                    //foreach (DataRow dr in dtExcel.Rows)
                    //{
                    //    RowValidation(dr);
                    //}
                    //#endregion

                    //if (dtExcel.Rows.Count > 0)
                    //{
                    //    #region converting to ienumerable for filter valid and invalid data by linq
                    //    IEnumerable<ConsumerBe> result = (from lists in dtExcel.AsEnumerable()
                    //                                      where lists.Field<bool>("IsValid") == true
                    //                                      select new ConsumerBe
                    //                                      {
                    //                                          Name = lists.Field<string>("Name")
                    //                                      }
                    //                                     );
                    //    #endregion

                    //    #region Preparation of list for grid binding

                    //    var listOfValidData = from dtLIst in dtExcel.AsEnumerable()
                    //                          where dtLIst.Field<bool>("IsValid") == true
                    //                          select dtLIst;


                    //    var listOfInvalidData = from dtLIst in dtExcel.AsEnumerable()
                    //                            where dtLIst.Field<bool>("IsValid") == false
                    //                            select dtLIst;
                    //    #endregion

                    //    #region Datatable preparation for bulkcopy

                    //    if (listOfValidData.Count() > 0)
                    //    {
                    //        dtValid = listOfValidData.CopyToDataTable();

                    //        Session["dtValid"] = dtValid;

                    //        #region Bind of valid grid
                    //        gvValidData.DataSource = listOfValidData.CopyToDataTable();
                    //        gvValidData.DataBind();
                    //        #endregion

                    //        btnUpload.Visible = true;
                    //    }
                    //    else
                    //    {
                    //        gvValidData.DataSource = new DataTable();
                    //        gvValidData.DataBind();
                    //        btnUpload.Visible = false;
                    //    }

                    //    #endregion

                    //    #region Bind of invalid grid
                    //    if (listOfInvalidData.Count() > 0)
                    //    {
                    //        gvInValidData.DataSource = listOfInvalidData.CopyToDataTable();
                    //        gvInValidData.DataBind();
                    //    }
                    //    else
                    //    {
                    //        gvInValidData.DataSource = new DataTable();
                    //        gvInValidData.DataBind();
                    //    }

                    //    #endregion

                    UploadStart();
                    //}
                    //else
                    //{
                    //    lblGenMessage.Text = Resources.Resource.NO_DATA;
                    //    mpeGenMessage.Show();
                    //}

                }
                else
                {
                    lblGenMessage.Text = Resources.Resource.COL_DSNT_MATCH; ;
                    mpeGenMessage.Show();
                }
            }
            else
            {
                lblGenMessage.Text = Resources.Resource.XLS_VALID;
                mpeGenMessage.Show();
            }
        }

        private string GetReadTypeID(string ReadType)
        {
            string ReadCodeId = string.Empty;
            DataTable dt = null;
            if (dsValidationTables.Tables.Count >= 10)
                dt = dsValidationTables.Tables[9];
            List<Int32> lstResult = (from table in dt.AsEnumerable()
                                     where table.Field<string>("ReadType") == ReadType
                                     select table.Field<Int32>("ReadCodeId")).ToList();
            if (lstResult.Count > 0)
                ReadCodeId = lstResult[0].ToString();
            return ReadCodeId;
        }

        private string GetTariffClassId(string Tariff)
        {
            string TariffClassId = string.Empty;
            DataTable dt = null;
            if (dsValidationTables.Tables.Count >= 6)
                dt = dsValidationTables.Tables[5];
            List<Int32> lstResult = (from table in dt.AsEnumerable()
                                      where table.Field<string>("Tariff") == Tariff
                                      select table.Field<Int32>("TariffClassId")).ToList();
            if (lstResult.Count > 0)
                TariffClassId = lstResult[0].ToString();
            return TariffClassId;
        }

        private string GetBookCodeId(string BookCode)
        {
            string BookCodeId = string.Empty;
            DataTable dt = null;
            if (dsValidationTables.Tables.Count >= 12)
                dt = dsValidationTables.Tables[11];
            List<string> lstResult = (from table in dt.AsEnumerable()
                                     where table.Field<string>("BookCode") == BookCode
                                     select table.Field<string>("BookCodeId")).ToList();
            if (lstResult.Count > 0)
                BookCodeId = lstResult[0];
            return BookCodeId;
        }

        private string GetPhaseID(string Phase)
        {
            string PhaseId = string.Empty;
            DataTable dt = null;
            if (dsValidationTables.Tables.Count >= 2)
                dt = dsValidationTables.Tables[1];
            List<Int32> lstResult = (from table in dt.AsEnumerable()
                                      where table.Field<string>("Phase") == Phase
                                      select table.Field<Int32>("PhaseId")).ToList();
            if (lstResult.Count > 0)
                PhaseId = lstResult[0].ToString();
            return PhaseId;
        }

        private string GetCustomerTypeId(string CustomerType)
        {
            string CustomerTypeId = string.Empty;
            DataTable dt = null;
            if (dsValidationTables.Tables.Count >= 8)
                dt = dsValidationTables.Tables[7];
            List<Int32> lstResult = (from table in dt.AsEnumerable()
                                     where table.Field<string>("CustomerType") == CustomerType
                                     select table.Field<Int32>("CustomerTypeId")).ToList();
            if (lstResult.Count > 0)
                CustomerTypeId = lstResult[0].ToString();
            return CustomerTypeId;
        }

        public string InsertExcel()
        {
            try
            {
                string ExcelModifiedFileName;

                if (upDOCExcel.HasFile)
                {
                    string ExcelFileName = System.IO.Path.GetFileName(upDOCExcel.FileName).ToString();//Assinging FileName
                    string ExcelExtension = Path.GetExtension(ExcelFileName);//Storing Extension of file into variable Extension
                    TimeZoneInfo ExcelIND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                    DateTime ExcelcurrentTime = DateTime.Now;
                    DateTime Exceloourtime = TimeZoneInfo.ConvertTime(ExcelcurrentTime, ExcelIND_ZONE);
                    string ExcelCurrentDate = Exceloourtime.ToString("dd-MM-yyyy_hh-mm-ss");
                    ExcelModifiedFileName = Path.GetFileNameWithoutExtension(ExcelFileName) + "_" + ExcelCurrentDate + ExcelExtension;//Setting File Name to store it in database
                    Filename = Path.GetFileNameWithoutExtension(ExcelFileName) + "_" + ExcelCurrentDate + ExcelExtension;//Setting File Name to store it in database
                    upDOCExcel.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["BulkCustomerUpload"].ToString()) + "/" + ExcelModifiedFileName);//Saving File in to the server.

                }
                return Filename;

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public DataRow RowValidation(DataRow dr)
        {
            dr["IsValid"] = "true";
            if (!IsInt(dr["MobileNo"].ToString()))
            {
                dr["Comments"] = "Mobile no incorrect." + Environment.NewLine;
                dr["IsValid"] = "false";
            }
            if (string.IsNullOrEmpty(dr["Name"].ToString().Trim()))
            {
                dr["Comments"] += "Name is empty." + Environment.NewLine;
                dr["IsValid"] = "false";

            }
            if (string.IsNullOrEmpty(dr["PostalStreet"].ToString().Trim()))
            {
                dr["Comments"] += "PostalStreet is empty.." + Environment.NewLine;
                dr["IsValid"] = "false";

            }
            if (string.IsNullOrEmpty(dr["PostalHouseNo"].ToString().Trim()))
            {
                dr["Comments"] += "PostalHouseNo is empty.." + Environment.NewLine;
                dr["IsValid"] = "false";

            }
            if (string.IsNullOrEmpty(dr["BookNo"].ToString().Trim()))
            {
                dr["Comments"] += "BookNo is empty.." + Environment.NewLine;
                dr["IsValid"] = "false";

            }
            if (string.IsNullOrEmpty(dr["ServiceStreet"].ToString().Trim()))
            {
                dr["Comments"] += "ServiceStreet is empty.." + Environment.NewLine;
                dr["IsValid"] = "false";

            }
            if (string.IsNullOrEmpty(dr["ServiceHouseNo"].ToString().Trim()))
            {
                dr["Comments"] += "ServiceHouseNo is empty.." + Environment.NewLine;
                dr["IsValid"] = "false";

            }
            if (string.IsNullOrEmpty(dr["MobileNo"].ToString().Trim()))
            {
                dr["Comments"] += "MobileNo is empty.." + Environment.NewLine;
                dr["IsValid"] = "false";
            }
            if (string.IsNullOrEmpty(dr["AccountTypeId"].ToString().Trim()))
            {
                dr["Comments"] += "AccountTypeId is empty.." + Environment.NewLine;
                dr["IsValid"] = "false";
            }
            if (string.IsNullOrEmpty(dr["ReadCodeId"].ToString().Trim()))
            {
                dr["Comments"] += "ReadCodeId is empty.." + Environment.NewLine;
                dr["IsValid"] = "false";
            }
            //string ReadCode = string.IsNullOrEmpty(dr["ReadCodeId"].ToString().Trim()).ToString();
            //if (ReadCode=="2")
            //{
            if (string.IsNullOrEmpty(dr["MeterNo"].ToString().Trim()))
            {
                dr["Comments"] += "MeterNo is empty.." + Environment.NewLine;
                dr["IsValid"] = "false";
            }
            if (string.IsNullOrEmpty(dr["MeterTypeId"].ToString().Trim()))
            {
                dr["Comments"] += "MeterTypeId is empty.." + Environment.NewLine;
                dr["IsValid"] = "false";
            }
            if (string.IsNullOrEmpty(dr["PhaseId"].ToString().Trim()))
            {
                dr["Comments"] += "PhaseId is empty.." + Environment.NewLine;
                dr["IsValid"] = "false";
            }
            if (!string.IsNullOrEmpty(dr["OldAccountNo"].ToString().Trim()))
            {
                if (IsOldAccountNoExists(dr["OldAccountNo"].ToString().Trim()))
                {
                    dr["Comments"] += "Old account no. is exists." + Environment.NewLine;
                    dr["IsValid"] = "false";
                }
            }
            if (!string.IsNullOrEmpty(dr["MeterNo"].ToString().Trim()))
            {
                if (IsMeterNoExists(dr["MeterNo"].ToString().Trim()))
                {
                    dr["Comments"] += "Meter no. is exists." + Environment.NewLine;
                    dr["IsValid"] = "false";
                }
            }

            if (!string.IsNullOrEmpty(dr["PhaseId"].ToString().Trim()))
            {
                if (!IsPhaseIdValid(dr["PhaseId"].ToString().Trim()))
                {
                    dr["Comments"] += "PhaseId is not exists." + Environment.NewLine;
                    dr["IsValid"] = "false";
                }
            }

            if (!string.IsNullOrEmpty(dr["MeterStatusId"].ToString().Trim()))
            {
                if (!IsMeterStatusIdValid(dr["MeterStatusId"].ToString().Trim()))
                {
                    dr["Comments"] += "MeterStatusId is not exists." + Environment.NewLine;
                    dr["IsValid"] = "false";
                }
            }
            if (!string.IsNullOrEmpty(dr["IdentityId"].ToString().Trim()))
            {
                if (!IsIdentityIdValid(dr["IdentityId"].ToString().Trim()))
                {
                    dr["Comments"] += "IdentityId is not exists." + Environment.NewLine;
                    dr["IsValid"] = "false";
                }
            }
            if (!string.IsNullOrEmpty(dr["MeterTypeId"].ToString().Trim()))
            {
                if (!IsMeterTypeIdValid(dr["MeterTypeId"].ToString().Trim()))
                {
                    dr["Comments"] += "MeterTypeId is not exists." + Environment.NewLine;
                    dr["IsValid"] = "false";
                }
            }
            //}
            if (!string.IsNullOrEmpty(dr["AccountTypeId"].ToString().Trim()))
            {
                if (!IsAccountTypeIdValid(dr["AccountTypeId"].ToString().Trim()))
                {
                    dr["Comments"] += "AccountTypeId is not exists." + Environment.NewLine;
                    dr["IsValid"] = "false";
                }
            }
            if (!string.IsNullOrEmpty(dr["TariffId"].ToString().Trim()))
            {
                if (!IsTariffIdValid(dr["TariffId"].ToString().Trim()))
                {
                    dr["Comments"] += "TariffId is not exists." + Environment.NewLine;
                    dr["IsValid"] = "false";
                }
            }

            return dr;
        }

        public bool IsInt(string str)
        {
            try
            {
                Convert.ToInt64(str);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool IsDecimal(string str)
        {
            try
            {
                string value = str.Replace(",", "");
                Convert.ToDecimal(value);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void BulkCopy(DataTable dataTable, string DestinationTbl, int batchSize)
        {

            if (dataTable.Rows.Count > 0)
            {
                // Get the DataTable 
                string sqlCon = ConfigurationManager.AppSettings["Sqlcon"].ToString();
                DataTable dtInsertRows = dataTable;

                using (SqlBulkCopy sbc = new SqlBulkCopy(sqlCon, SqlBulkCopyOptions.KeepIdentity))
                {
                    sbc.DestinationTableName = DestinationTbl;

                    // Number of records to be processed in one go
                    sbc.BatchSize = batchSize;
                    sbc.ColumnMappings.Capacity = 100;
                    //sbc.ColumnMappings.Add("", "");

                    // Add your column mappings here
                    sbc.ColumnMappings.Add("Name", "Name");
                    sbc.ColumnMappings.Add("SurName", "SurName");
                    sbc.ColumnMappings.Add("EmailId", "EmailId");
                    sbc.ColumnMappings.Add("DocumentNo", "DocumentNo");
                    sbc.ColumnMappings.Add("OtherContactNo", "OtherContactNo");
                    sbc.ColumnMappings.Add("PostalLandMark", "PostalLandMark");
                    sbc.ColumnMappings.Add("PostalStreet", "PostalStreet");
                    sbc.ColumnMappings.Add("PostalCity", "PostalCity");
                    sbc.ColumnMappings.Add("PostalHouseNo", "PostalHouseNo");
                    sbc.ColumnMappings.Add("PostalDetails", "PostalDetails");
                    sbc.ColumnMappings.Add("PostalZipCode", "PostalZipCode");
                    sbc.ColumnMappings.Add("IdentityNo", "IdentityNo");
                    sbc.ColumnMappings.Add("IdentityId", "IdentityId");
                    sbc.ColumnMappings.Add("BookNo", "BookNo");
                    sbc.ColumnMappings.Add("ServiceLandMark", "ServiceLandMark");
                    sbc.ColumnMappings.Add("ServiceStreet", "ServiceStreet");
                    sbc.ColumnMappings.Add("ServiceCity", "ServiceCity");
                    sbc.ColumnMappings.Add("ServiceHouseNo", "ServiceHouseNo");
                    sbc.ColumnMappings.Add("ServiceDetails", "ServiceDetails");
                    sbc.ColumnMappings.Add("ServiceZipCode", "ServiceZipCode");
                    sbc.ColumnMappings.Add("ServiceEmailId", "ServiceEmailId");
                    sbc.ColumnMappings.Add("MobileNo", "MobileNo");
                    sbc.ColumnMappings.Add("TariffId", "TariffId");
                    sbc.ColumnMappings.Add("MultiplicationFactor", "MultiplicationFactor");
                    sbc.ColumnMappings.Add("AccountTypeId", "AccountTypeId");
                    sbc.ColumnMappings.Add("ApplicationDate", "ApplicationDate");
                    sbc.ColumnMappings.Add("ReadCodeId", "ReadCodeId");
                    sbc.ColumnMappings.Add("OldAccountNo", "OldAccountNo");
                    sbc.ColumnMappings.Add("ConnectionDate", "ConnectionDate");
                    sbc.ColumnMappings.Add("MeterNo", "MeterNo");
                    sbc.ColumnMappings.Add("MeterSerialNo", "MeterSerialNo");
                    sbc.ColumnMappings.Add("MeterTypeId", "MeterTypeId");
                    sbc.ColumnMappings.Add("PhaseId", "PhaseId");
                    sbc.ColumnMappings.Add("MeterStatusId", "MeterStatusId");
                    sbc.ColumnMappings.Add("InitialReading", "InitialReading");
                    sbc.ColumnMappings.Add("CurrentReading", "CurrentReading");
                    sbc.ColumnMappings.Add("MinimumReading", "MinimumReading");
                    //sbc.ColumnMappings.Add("CustomerUniqueNo", "CustomerUniqueNo");


                    // Finally write to server
                    sbc.WriteToServer(dtInsertRows);
                }
            }
            else
            {
                lblGenMessage.Text = Resources.Resource.UPL_SES_EXP;
                mpeGenMessage.Show();
            }
        }

        public int InsertBatch()
        {
            objBillingBE.BatchNo = Convert.ToInt32(BatchNo);
            objBillingBE.BatchName = BatchName;
            objBillingBE.Description = Details;
            objBillingBE.BatchDate = _objCommonMethods.Convert_MMDDYY(BatchDate);
            objBillingBE.CreatedBy = Session[Resources.UMS_Resource.SESSION_LOGINID].ToString();
            objBillingBE = _objIdsignBal.DeserializeFromXml<BillingBE>(objConsumerBal.InsertBatchCustBulkUploadBAL(objBillingBE));
            return objBillingBE.RowCount;
        }

        public int CustomerBulkUploads(DataSet ds)
        {
            DataTable dt = ds.Tables["dtBulkCopy"];
            BatchInsert(dt, dt.Rows.Count);
            //objBillingBE = _objIdsignBal.DeserializeFromXml<BillingBE>(objConsumerBal.CustomerBulkUploadBAL(ds));
            //return objBillingBE.RowCount;
            return 0;
        }

        public void BatchInsert(DataTable dataTable, Int32 batchSize)
        {
            // Assumes GetConnectionString() returns a valid connection string.
            string connectionString = ConfigurationManager.AppSettings["Sqlcon"].ToString();

            // Connect to the AdventureWorks database.
            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                // Create a SqlDataAdapter.
                SqlDataAdapter adapter = new SqlDataAdapter();

                // Set the INSERT command and parameter.
                adapter.InsertCommand = new SqlCommand("USP_CustomerBulkImport", connection);
                adapter.InsertCommand.CommandType = CommandType.StoredProcedure;
                
                adapter.InsertCommand.Parameters.Add("@SNO", SqlDbType.VarChar, 10, "SNO");
                adapter.InsertCommand.Parameters.Add("@Title", SqlDbType.VarChar, 10, "Title");
                adapter.InsertCommand.Parameters.Add("@FirstName", SqlDbType.VarChar, 50, "FirstName");
                adapter.InsertCommand.Parameters.Add("@MiddleName", SqlDbType.VarChar, 50, "MiddleName");
                adapter.InsertCommand.Parameters.Add("@LastName", SqlDbType.VarChar, 50, "LastName");
                adapter.InsertCommand.Parameters.Add("@KnownAs", SqlDbType.VarChar, 150, "KnownAs");
                adapter.InsertCommand.Parameters.Add("@EmailId", SqlDbType.VarChar, -1, "EmailId");
                adapter.InsertCommand.Parameters.Add("@HomeContactNumber", SqlDbType.VarChar, 20, "HomeContactNumber");
                adapter.InsertCommand.Parameters.Add("@BusinessContactNumber", SqlDbType.VarChar, 20, "BusinessContactNumber");
                adapter.InsertCommand.Parameters.Add("@OtherContactNumber", SqlDbType.VarChar, 20, "OtherContactNumber");
                adapter.InsertCommand.Parameters.Add("@DocumentNo", SqlDbType.VarChar, 50, "DocumentNo");
                adapter.InsertCommand.Parameters.Add("@ApplicationDate", SqlDbType.VarChar, 50, "ApplicationDate");
                adapter.InsertCommand.Parameters.Add("@ConnectionDate", SqlDbType.VarChar, 50, "ConnectionDate");
                adapter.InsertCommand.Parameters.Add("@SetupDate", SqlDbType.VarChar, 50, "SetupDate");
                adapter.InsertCommand.Parameters.Add("@OldAccountNo", SqlDbType.VarChar, 50, "OldAccountNo");
                adapter.InsertCommand.Parameters.Add("@CustomerType", SqlDbType.VarChar, 1000, "CustomerType");
                adapter.InsertCommand.Parameters.Add("@BookCode", SqlDbType.VarChar, 20, "BookCode");
                adapter.InsertCommand.Parameters.Add("@PoleID", SqlDbType.VarChar, 50, "PoleID");
                adapter.InsertCommand.Parameters.Add("@MeterNumber", SqlDbType.VarChar, 50, "MeterNumber");
                adapter.InsertCommand.Parameters.Add("@Tariff", SqlDbType.VarChar, 200, "Tariff");
                adapter.InsertCommand.Parameters.Add("@IsEmbassyCustomer", SqlDbType.VarChar, 50, "IsEmbassyCustomer");
                adapter.InsertCommand.Parameters.Add("@EmbassyCode", SqlDbType.VarChar, 50, "EmbassyCode");
                adapter.InsertCommand.Parameters.Add("@Phase", SqlDbType.VarChar, 500, "Phase");
                adapter.InsertCommand.Parameters.Add("@ReadType", SqlDbType.VarChar, 100, "ReadType");
                adapter.InsertCommand.Parameters.Add("@ServiceAddress_HouseNo", SqlDbType.VarChar, 200, "ServiceAddress_HouseNo");
                adapter.InsertCommand.Parameters.Add("@ServiceAddress_StreetName", SqlDbType.VarChar, 200, "ServiceAddress_StreetName");
                adapter.InsertCommand.Parameters.Add("@ServiceAddress_City", SqlDbType.VarChar, 200, "ServiceAddress_City");
                adapter.InsertCommand.Parameters.Add("@ServiceAddress_Landmark", SqlDbType.VarChar, 200, "ServiceAddress_Landmark");
                adapter.InsertCommand.Parameters.Add("@ServiceAddress_Details", SqlDbType.VarChar, 200, "ServiceAddress_Details");
                adapter.InsertCommand.Parameters.Add("@ServiceAddress_AreaCode", SqlDbType.Int,10, "ServiceAddress_AreaCode");
                adapter.InsertCommand.Parameters.Add("@ServiceAddress_Latitude", SqlDbType.VarChar, 50, "ServiceAddress_Latitude");
                adapter.InsertCommand.Parameters.Add("@ServiceAddress_Longitude", SqlDbType.VarChar, 50, "ServiceAddress_Longitude");
                adapter.InsertCommand.Parameters.Add("@ServiceAddress_ZipCode", SqlDbType.Int, 6, "ServiceAddress_ZipCode");
                adapter.InsertCommand.Parameters.Add("@PostalAddress_HouseNo", SqlDbType.VarChar, 200, "PostalAddress_HouseNo");
                adapter.InsertCommand.Parameters.Add("@PostalAddress_StreetName", SqlDbType.VarChar, 200, "PostalAddress_StreetName");
                adapter.InsertCommand.Parameters.Add("@PostalAddress_City", SqlDbType.VarChar, 200, "PostalAddress_City");
                adapter.InsertCommand.Parameters.Add("@PostalAddress_Landmark", SqlDbType.VarChar, 200, "PostalAddress_Landmark");
                adapter.InsertCommand.Parameters.Add("@PostalAddress_Details", SqlDbType.VarChar, 200, "PostalAddress_Details");
                adapter.InsertCommand.Parameters.Add("@Postal_AreaCode", SqlDbType.Int, 10, "Postal_AreaCode");
                adapter.InsertCommand.Parameters.Add("@PostalAddress_Latitude", SqlDbType.VarChar, 50, "PostalAddress_Latitude");
                adapter.InsertCommand.Parameters.Add("@PostalAddress_Longitude", SqlDbType.VarChar, 50, "PostalAddress_Longitude");
                adapter.InsertCommand.Parameters.Add("@PostalAddress_ZipCode", SqlDbType.Int, 6, "PostalAddress_ZipCode");
                adapter.InsertCommand.Parameters.Add("@IsCAPMI", SqlDbType.VarChar, 10, "IsCAPMI");
                adapter.InsertCommand.Parameters.Add("@MeterAmount", SqlDbType.VarChar, 50, "MeterAmount");
                adapter.InsertCommand.Parameters.Add("@InitialBillingKWh", SqlDbType.VarChar, 50, "InitialBillingKWh");
                adapter.InsertCommand.Parameters.Add("@InitialReading", SqlDbType.VarChar, 50, "InitialReading");
                adapter.InsertCommand.Parameters.Add("@PresentReading", SqlDbType.VarChar, 50, "PresentReading");
                adapter.InsertCommand.Parameters.Add("@AverageReading", SqlDbType.VarChar, 50, "AverageReading");
                adapter.InsertCommand.Parameters.Add("@Highestconsumption", SqlDbType.VarChar, 50, "Highestconsumption");
                adapter.InsertCommand.Parameters.Add("@OutStandingAmount", SqlDbType.VarChar, 50, "OutStandingAmount");
                adapter.InsertCommand.Parameters.Add("@OpeningBalance", SqlDbType.VarChar, 50, "OpeningBalance");

                adapter.InsertCommand.Parameters.Add("@PhaseId", SqlDbType.Int, 50, "PhaseId");
                adapter.InsertCommand.Parameters.Add("@CustomerTypeId", SqlDbType.Int, 50, "CustomerTypeId");
                adapter.InsertCommand.Parameters.Add("@BookCodeId", SqlDbType.VarChar, 50, "BookCodeId");
                adapter.InsertCommand.Parameters.Add("@TariffClassID", SqlDbType.Int, 50, "TariffClassID");
                adapter.InsertCommand.Parameters.Add("@ReadCodeID", SqlDbType.Int, 50, "ReadCodeID");

                adapter.InsertCommand.Parameters.Add("@RowCount", SqlDbType.Int, 50, "RowCount");

                adapter.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

                // Set the batch size.
                adapter.UpdateBatchSize = batchSize;

                // Execute the update.
                adapter.Update(dataTable);
            }
        }

        public void UploadFinsish()
        {
            BatchDate = string.Empty;
            BatchName = string.Empty;
            BatchNo = string.Empty;
            Details = string.Empty;
            divBatchDetails.Visible = true;
            divBrowseDoc.Visible = false;
            divValidateData.Visible = false;
            divInValidPrint.Visible = false;

        }

        public void UploadStart()
        {
            divBrowseDoc.Visible = false;
            divBatchDetails.Visible = true;
            divValidateData.Visible = true;
            divInValidPrint.Visible = true;
        }

        public DataTable AddBatchDetailsToDT()
        {
            DataTable dtBatch = new DataTable();
            dtBatch.Columns.Add("BatchNo", typeof(int));
            dtBatch.Columns.Add("BatchName", typeof(string));
            dtBatch.Columns.Add("Description", typeof(string));
            dtBatch.Columns.Add("BatchDate", typeof(string));
            dtBatch.Columns.Add("CreatedBy", typeof(string));
            DataRow dr = dtBatch.NewRow();
            dr["BatchNo"] = Convert.ToInt32(BatchNo);
            dr["BatchName"] = BatchName;
            dr["Description"] = Details;
            dr["BatchDate"] = _objCommonMethods.Convert_MMDDYY(BatchDate);
            dr["CreatedBy"] = Session[Resources.UMS_Resource.SESSION_LOGINID].ToString();
            dtBatch.Rows.Add(dr);
            return dtBatch;
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {

                throw Ex;
            }
        }

        #region Validation Method Starts

        public bool IsMeterNoExists(string MeterNo)
        {
            DataTable dt = null;
            if (MeterNo == "NULL")
                MeterNo = string.Empty;
            if (dsValidationTables.Tables.Count >= 9)
                dt = dsValidationTables.Tables[8];
            bool exists = false;
            if (dt.Rows.Count > 0 && !(string.IsNullOrEmpty(MeterNo)))
            {
                try
                {
                    var lists = from x in dt.AsEnumerable() where x.Field<string>("MeterNumber") == (MeterNo) select x;
                    List<string> excelLists = (from table in dtExcel.AsEnumerable()
                                               where table.Field<string>("MeterNumber") == MeterNo
                                               select table.Field<string>("MeterNumber")).ToList();
                    if (lists.Count() > 0 || excelLists.Count > 1)
                    {
                        exists = true;
                    }
                }
                catch
                {
                    exists = false;
                }
            }
            return exists;
        }

        public bool IsPhaseIdValid(string Phase)
        {
            DataTable dt = null;
            if (Phase == "NULL")
                Phase = string.Empty;
            if (dsValidationTables.Tables.Count >= 2)
                dt = dsValidationTables.Tables[1];
            bool exists = false;
            if (dt.Rows.Count > 0)
            {
                try
                {
                    //var lists = from x in dt.AsEnumerable() where x.Field<int>("PhaseID") == Convert.ToInt32((PhaseID)) select x;
                    var lists = from x in dt.AsEnumerable() where x.Field<string>("Phase") == (Phase) select x;
                    if (lists.Count() > 0)
                    {
                        exists = true;
                    }
                }
                catch
                {
                    exists = false;
                }
            }
            return exists;
        }

        public bool IsReadTypeValid(string ReadType)
        {
            DataTable dt = null;
            if (ReadType == "NULL")
                ReadType = string.Empty;
            if (dsValidationTables.Tables.Count >= 10)
                dt = dsValidationTables.Tables[9];
            bool exists = false;
            if (dt.Rows.Count > 0)
            {
                try
                {
                    //var lists = from x in dt.AsEnumerable() where x.Field<int>("PhaseID") == Convert.ToInt32((PhaseID)) select x;
                    var lists = from x in dt.AsEnumerable() where x.Field<string>("ReadType") == (ReadType) select x;
                    if (lists.Count() > 0)
                    {
                        exists = true;
                    }
                }
                catch
                {
                    exists = false;
                }
            }
            return exists;
        }

        public bool IsAreaCodeValid(string AreaCode)
        {
            DataTable dt = null;
            if (AreaCode == "NULL")
                AreaCode = "0";
            if (dsValidationTables.Tables.Count >= 11)
                dt = dsValidationTables.Tables[10];
            bool exists = false;
            if (dt.Rows.Count > 0)
            {
                try
                {
                    //var lists = from x in dt.AsEnumerable() where x.Field<int>("PhaseID") == Convert.ToInt32((PhaseID)) select x;
                    var lists = from x in dt.AsEnumerable() where x.Field<int>("AreaCode") == Convert.ToInt32((AreaCode)) select x;
                    if (lists.Count() > 0)
                    {
                        exists = true;
                    }
                }
                catch
                {
                    exists = false;
                }
            }
            return exists;
        }

        public bool IsMeterStatusIdValid(string MeterStatusId)
        {
            DataTable dt = null;
            if (MeterStatusId == "NULL")
                MeterStatusId = "0";
            if (dsValidationTables.Tables.Count >= 3)
                dt = dsValidationTables.Tables[2];
            bool exists = false;
            if (dt.Rows.Count > 0)
            {
                try
                {
                    var lists = from x in dt.AsEnumerable() where x.Field<int>("MeterStatusId") == Convert.ToInt32((MeterStatusId)) select x;
                    if (lists.Count() > 0)
                    {
                        exists = true;
                    }
                }
                catch
                {
                    exists = false;
                }
            }
            return exists;
        }

        public bool IsIdentityIdValid(string IdentityId)
        {
            DataTable dt = null;
            if (IdentityId == "NULL")
                IdentityId = "0";
            if (dsValidationTables.Tables.Count >= 4)
                dt = dsValidationTables.Tables[3];
            bool exists = false;
            if (dt.Rows.Count > 0)
            {
                try
                {
                    var lists = from x in dt.AsEnumerable() where x.Field<int>("IdentityId") == Convert.ToInt32((IdentityId)) select x;
                    if (lists.Count() > 0)
                    {
                        exists = true;
                    }
                }
                catch
                {
                    exists = false;
                }
            }
            return exists;
        }

        public bool IsAccountTypeIdValid(string AccountTypeId)
        {
            DataTable dt = null;
            if (AccountTypeId == "NULL")
                AccountTypeId = string.Empty;
            if (dsValidationTables.Tables.Count >= 5)
                dt = dsValidationTables.Tables[4];
            bool exists = false;
            if (dt.Rows.Count > 0)
            {
                try
                {
                    var lists = from x in dt.AsEnumerable() where x.Field<int>("AccountTypeId") == Convert.ToInt32((AccountTypeId)) select x;
                    if (lists.Count() > 0)
                    {
                        exists = true;
                    }
                }
                catch
                {
                    exists = false;
                }

            }
            return exists;
        }

        public bool IsTariffIdValid(string Tariff)
        {
            DataTable dt = null;
            if (Tariff == "NULL")
                Tariff = string.Empty;
            if (dsValidationTables.Tables.Count >= 6)
                dt = dsValidationTables.Tables[5];
            bool exists = false;
            if (dt.Rows.Count > 0)
            {
                try
                {
                    //var lists = from x in dt.AsEnumerable() where x.Field<int>("TariffId") == Convert.ToInt32((TariffId)) select x;
                    var lists = from x in dt.AsEnumerable() where x.Field<string>("Tariff") == (Tariff) select x;
                    if (lists.Count() > 0)
                    {
                        exists = true;
                    }
                }
                catch
                {
                    exists = false;
                }
            }
            return exists;
        }

        public bool IsMeterTypeIdValid(string MeterTypeId)
        {
            DataTable dt = null;
            if (MeterTypeId == "NULL")
                MeterTypeId = "0";
            if (dsValidationTables.Tables.Count >= 7)
                dt = dsValidationTables.Tables[6];
            bool exists = false;
            if (dt.Rows.Count > 0)
            {
                try
                {
                    var lists = from x in dt.AsEnumerable() where x.Field<int>("MeterTypeId") == Convert.ToInt32((MeterTypeId)) select x;
                    if (lists.Count() > 0)
                    {
                        exists = true;
                    }
                }
                catch
                {
                    exists = false;
                }
            }
            return exists;
        }

        public bool IsOldAccountNoExists(string OldAccountNo)
        {
            if (OldAccountNo == "NULL")
                OldAccountNo = string.Empty;
            DataTable dt = dsValidationTables.Tables[0];
            bool exists = false;
            if (dt.Rows.Count > 0 && !(string.IsNullOrEmpty(OldAccountNo)))
            {

                //var lists = (from x in dt.AsEnumerable() where x.Field<string>("OldAccountNo") == (OldAccountNo) select x);
                //if (lists.Count() > 0)
                //{
                //    exists = true;
                //}
                var lists = (from x in dt.AsEnumerable() where x.Field<string>("OldAccountNo") == (OldAccountNo) select x);
                //var excelLists = (from x in dtExcel.AsEnumerable() where x.Field<string>("OldAccountNo") == (OldAccountNo) select x);

                List<string> excelLists = (from table in dtExcel.AsEnumerable()
                                           where table.Field<string>("OldAccountNo") == OldAccountNo
                                           select table.Field<string>("OldAccountNo")).ToList();
                if (lists.Count() > 0 || excelLists.Count() > 1)
                {
                    exists = true;
                }
            }
            return exists;
        }

        public bool IsCustomerTypeValid(string CustomerType)
        {
            DataTable dt = null;
            if (CustomerType == "NULL")
                CustomerType = string.Empty;
            if (dsValidationTables.Tables.Count >= 8)
                dt = dsValidationTables.Tables[7];
            bool exists = false;
            if (dt.Rows.Count > 0)
            {
                try
                {
                    var lists = from x in dt.AsEnumerable() where x.Field<string>("CustomerType") == (CustomerType) select x;
                    if (lists.Count() > 0)
                    {
                        exists = true;
                    }
                }
                catch
                {
                    exists = false;
                }
            }
            return exists;
        }

        public bool IsBookCodeValid(string BookCode)
        {
            DataTable dt = null;
            if (BookCode == "NULL")
                BookCode = string.Empty;
            if (dsValidationTables.Tables.Count >= 12)
                dt = dsValidationTables.Tables[11];
            bool exists = false;
            if (dt.Rows.Count > 0)
            {
                try
                {
                    var lists = from x in dt.AsEnumerable() where x.Field<string>("BookCode") == (BookCode) select x;
                    if (lists.Count() > 0)
                    {
                        exists = true;
                    }
                }
                catch
                {
                    exists = false;
                }
            }
            return exists;
        }

        #endregion

        #region Validation Methods

        public string IsRequiered(string ColumnName, string Value)
        {
            string comments = string.Empty;
            if (string.IsNullOrEmpty(Value))
                comments += "-" + ColumnName + " Should not be Empty or null" + Environment.NewLine;

            return comments;
        }

        public string IsNumber(string ColumnName, string value)
        {
            string comments = string.Empty;
            if (!IsInt(value))
                comments += "-" + ColumnName + " Should be numeric" + Environment.NewLine;
            return comments;
        }

        public string IsLengthValid(string ColumnName, string Value, int MaxLength, int MinLength)
        {
            string comments = string.Empty;
            if (Value.Length > MaxLength)
                comments += "-" + ColumnName + " Should be maximum " + MaxLength + " characters" + Environment.NewLine;
            else if (Value.Length < MinLength)
                comments += "-" + ColumnName + " Should be minimum " + MinLength + " characters" + Environment.NewLine;
            return comments;
        }

        public string IsEmailIdValid(string ColumnName, string value)
        {
            string comments = string.Empty;
            System.Text.RegularExpressions.Regex rex = new System.Text.RegularExpressions.Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

            if (!rex.IsMatch(value))
                comments += "-" + ColumnName + " Should be valid Email" + Environment.NewLine;
            return comments;
        }


        // To  check given string is in 000-0000000000 (Code-ContactNo) format or not with code min 1 max 3 and Contact no min 6 and Max 10 digit
        public string IsContactNoValid(string ColumnName, string value)
        {
            string comments = string.Empty;
            string CodeError = string.Empty;
            string ContactNoError = string.Empty;
            string[] s = value.Split('-');
            if (!string.IsNullOrEmpty(value))
            {
                if (s.Length > 1)
                {
                    CodeError = IsNumber(ColumnName + " Code", s[0]);
                    if (!(CodeError.Length > 0))
                        comments += IsLengthValid(ColumnName + " Code", s[0], 3, 1);
                    else
                        comments += CodeError;

                    ContactNoError = IsNumber(ColumnName + " ContactNo", s[1]);
                    if (!(ContactNoError.Length > 0))
                        comments += IsLengthValid(ColumnName + " ContactNo", s[1], 10, 6);
                    else
                        comments += ContactNoError;
                }
                else
                {
                    comments += "-Contact No should be in 000-0000000000 (Code-ContactNo) format" + Environment.NewLine;
                }
            }
            return comments;
        }

        //To check input string is as per dateformat
        public string IsDateValid(string ColumnName, string value, string DateFormat)
        {
            string comments = string.Empty;
            string[] formats = { DateFormat };

            DateTime parsedDateTime;
            if (!(DateTime.TryParseExact(value, formats, new CultureInfo("en-US"),
                                           DateTimeStyles.None, out parsedDateTime)))
            {
                comments += "-" + ColumnName + " Should be valid date in MM/DD/YYYY format" + Environment.NewLine;
            }
            return comments;
        }

        public string IsDateGreaterThanToday(string ColumnName, string value, string DateFormat)
        {
            string comments = string.Empty;
            DateTime date = DateTime.ParseExact(value, "dd/MM/yyyy",
                                                System.Globalization.CultureInfo.InvariantCulture);
            if (date > DateTime.Today)
            {
                comments += "-" + ColumnName + " date should be greater than current date" + Environment.NewLine;
            }
            return comments;
        }


        //To check wether the date is greater than given application date
        public string IsDateGreaterThan(string ColumnName, string value, string DateFormat, string ApplicationDate)
        {
            string comments = string.Empty;
            DateTime date = DateTime.ParseExact(value, "dd/MM/yyyy",
                                                System.Globalization.CultureInfo.InvariantCulture);
            DateTime AppDate = DateTime.ParseExact(ApplicationDate, "dd/MM/yyyy",
                                                System.Globalization.CultureInfo.InvariantCulture);
            if (date < AppDate)
            {
                comments += "-" + ColumnName + " date should be greater than application date" + Environment.NewLine;
            }
            return comments;
        }

        #endregion

        #region Validation Group Methods 

        //Validation group for Mandatory,and length validation
        public string ValidationGroup1(string ColumnName, string Value, int MaxLength, int MinLength, bool IsMandatory)
        {
            string comments = string.Empty;
            if (IsMandatory)
                comments += IsRequiered(ColumnName, Value);
            if (comments.Length == 0)
                comments += IsLengthValid(ColumnName, Value, MaxLength, MinLength);
            return comments;
        }

        //public string ValidationGroup2(string ColumnName, string Value, int MaxLength, int MinLength)
        //{
        //    string comments = string.Empty;
        //    comments += IsLengthValid(ColumnName, Value, MaxLength, MinLength);
        //    return comments;
        //}

        //public string ValidationGroup3(string ColumnName, string Value, int MaxLength, int MinLength)
        //{
        //    string comments = string.Empty;
        //    comments += IsRequiered(ColumnName, Value);
        //    comments += IsNumber(ColumnName, Value);
        //    comments += IsLengthValid(ColumnName, Value, MaxLength, MinLength);
        //    return comments;
        //}

        //Validation group for Email and Contact no validation
        public string ValidationGroup4(string ColumnName, string Value, int MaxLength, int MinLength, bool IsMandatory)
        {
            string comments = string.Empty;
            if (IsMandatory)
                comments += IsRequiered(ColumnName, Value);

            if (comments.Length == 0)
            {
                if (ColumnName == "EmailId")
                {
                    comments += IsEmailIdValid(ColumnName, Value);
                }
                else if (ColumnName == "HomeContactNumber" || ColumnName == "BusinessContactNumber" || ColumnName == "OtherContactNumber")
                {
                    comments += IsContactNoValid(ColumnName, Value);
                }
            }

            return comments;
        }

        //Valdiation group for Date format validation
        public string ValidationGroup5(string ColumnName, string Value, bool IsMandatory)
        {
            string comments = string.Empty;
            if (IsMandatory)
                comments += IsRequiered(ColumnName, Value);
            if (!(comments.Length > 0))
                comments += IsDateValid(ColumnName, Value, "dd/MM/yyyy");
            if (!(comments.Length > 0))
                comments += IsDateGreaterThanToday(ColumnName, Value, "dd/MM/yyyy");
            return comments;
        }

        //validation group for application date validation
        public string ValidationGroup6(string ColumnName, string Value, string ApplicationDateValue, bool IsMandatory)
        {
            string comments = string.Empty;
            comments += ValidationGroup5(ColumnName, Value, IsMandatory);
            if (!(comments.Length > 0))
                comments += IsDateGreaterThan(ColumnName, Value, "dd/MM/yyyy", ApplicationDateValue);
            return comments;
        }

        //Validation group for IsMandatory,IsNumeric & LengthValidation
        public string ValidationGroup7(string ColumnName, string Value, int MaxLength, bool IsNumeric, bool IsMandatory)
        {
            string comments = string.Empty;
            if (IsMandatory)
                comments += IsRequiered(ColumnName, Value);
            if (!(comments.Length > 0) && IsNumeric)
                comments += IsNumber(ColumnName, Value);
            if (!(comments.Length > 0))
                comments += IsLengthValid(ColumnName, Value, MaxLength, 0);

            return comments;
        }

        //Validation group for IsMandatory,IsNumeric, LengthValidation & IsMaster Record
        public string ValidationGroup7(string ColumnName, string Value, int MaxLength, bool IsNumeric, bool IsMaster, bool IsMandatory)
        {
            string comments = string.Empty;
            comments += ValidationGroup7(ColumnName, Value, MaxLength, IsNumeric, IsMandatory);
            if (!(comments.Length > 0) && IsMaster)
            {
                switch (ColumnName)
                {
                    case "OldAccountNo":
                        if (IsOldAccountNoExists(Value))
                            comments += "-Account No already exists" + Environment.NewLine;
                        break;
                    case "CustomerType":
                        if (!IsCustomerTypeValid(Value))
                            comments += "-Invalid customer type." + Environment.NewLine;
                        break;
                    case "BookCode":
                        if (!IsBookCodeValid(Value))
                            comments += "-Invalid Book Code." + Environment.NewLine;
                        break;
                    case "MeterNumber":
                        if (IsMeterNoExists(Value))
                            comments += "-Meter Number already exists" + Environment.NewLine;
                        break;
                    case "Tariff":
                        if (!IsTariffIdValid(Value))
                            comments += "-Invalid Tariff Name" + Environment.NewLine;
                        break;
                    case "Phase":
                        if (!IsPhaseIdValid(Value))
                            comments += "-Invalid Phase Name" + Environment.NewLine;
                        break;
                    case "ReadType":
                        if (!IsReadTypeValid(Value))
                            comments += "-Invalid Read Type" + Environment.NewLine;
                        break;
                    case "ServiceAddress_AreaCode":
                        if (!IsAreaCodeValid(Value))
                            comments += "-Invalid Service Address Area Code" + Environment.NewLine;
                        break;
                    case "Postal_AreaCode":
                        if (!IsAreaCodeValid(Value))
                            comments += "-Invalid Postal Address Area Code" + Environment.NewLine;
                        break;
                    case "Title":
                        if (!(Value == "Mr" || Value == "Miss" || Value == "Mrs"))
                            comments += "-Invalid Title. It should be Mr or Miss or Mrs" + Environment.NewLine;
                        break;

                }
            }

            return comments;
        }

        //Validation group for Boolean value i.e for YES or NO validation
        public string ValidationGroup8(string ColumnName, string Value, bool IsMandatory)
        {
            string comments = string.Empty;
            if (IsMandatory)
                comments += IsRequiered(ColumnName, Value);
            if (!(Value.ToUpper() == "YES" || Value.ToUpper() == "NO") && !(comments.Length>0))
                comments += "-" + ColumnName + " should be either YES or NO" + Environment.NewLine;
            return comments;
        }


        //Validation group for amount validation i.e. decimal value with commas. (e.g. 1,250.50)
        public string ValidationGroup9(string ColumnName, string Value, bool IsMandatory)
        {
            string comments = string.Empty;
            if (IsMandatory)
                comments += IsRequiered(ColumnName, Value);
            if (!IsDecimal(Value) && !(comments.Length > 0))
                comments += "-" + ColumnName + " should be decimal value." + Environment.NewLine;
            return comments;
        }
        #endregion

        #endregion

    }
}