﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using iDSignHelper;
using UMS_NigeriaBAL;
using Resources;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Data;
using System.Configuration;
using System.Text;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class DirectCustomerAverageUpload : System.Web.UI.Page
    {
        # region Members

        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        ChangeBookNoBal _objBookNoBal = new ChangeBookNoBal();
        ReportsBal objReportsBal = new ReportsBal();
        ConsumerBal objConsumerBal = new ConsumerBal();
        string Key = "Direct Customer Average Upload";

        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                lblMessage.Text = pnlMessage.CssClass = string.Empty;
                if (!IsPostBack)
                {
                    divdetails.Attributes.Add("style", "display:none;");
                    //string path = string.Empty;
                    //path = _objCommonMethods.GetPagePath(this.Request.Url);
                    //if (_objCommonMethods.IsAccess(path))
                    //{

                    //}
                    //else
                    //{
                    //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    //}
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            try
            {

                DirectCustomerAverageUploadBE _objDirectCustomerAverageUploadBE = new DirectCustomerAverageUploadBE();
                _objDirectCustomerAverageUploadBE.AccountNo = txtAccountNo.Text.Trim();
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    _objDirectCustomerAverageUploadBE.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    _objDirectCustomerAverageUploadBE.BUID = string.Empty;

                _objDirectCustomerAverageUploadBE = _objiDsignBal.DeserializeFromXml<DirectCustomerAverageUploadBE>(_objBookNoBal.GetDirectCustomerAverageUploadBE(_objDirectCustomerAverageUploadBE));

                if (_objDirectCustomerAverageUploadBE.IsSuccess)
                {
                    divdetails.Attributes.Add("style", "display:block;");
                    lblGlobalAccountNo.Text = _objDirectCustomerAverageUploadBE.GlobalAccountNumber;
                    lblAccNoAndGlobalAccNo.Text = _objDirectCustomerAverageUploadBE.AccNoAndGlobalAccNo;
                    lblAccountNo.Text = _objDirectCustomerAverageUploadBE.AccountNo;
                    lblOldAccNo.Text = _objDirectCustomerAverageUploadBE.OldAccountNo;
                    lblName.Text = _objDirectCustomerAverageUploadBE.Name;
                    lblServiceAddress.Text = _objDirectCustomerAverageUploadBE.ServiceAddress;
                    lblTariff.Text = _objDirectCustomerAverageUploadBE.Tariff;
                    lblCluster.Text = _objDirectCustomerAverageUploadBE.ClusterType;
                    lblAverageReading.Text = Convert.ToString(_objDirectCustomerAverageUploadBE.AverageReading);
                    lblStatus.Text = _objDirectCustomerAverageUploadBE.CustomerStatus;
                    lblOutstandingAmount.Text = _objCommonMethods.GetCurrencyFormat(_objDirectCustomerAverageUploadBE.OutStandingAmount, 2, Constants.MILLION_Format);
                }
                else if (_objDirectCustomerAverageUploadBE.IsAccountNoNotExists)
                {
                    CustomerNotFound("Customer not found");
                }
                else if (_objDirectCustomerAverageUploadBE.IsCustExistsInOtherBU)
                {
                    CustomerNotFound("Customer exists in another Biusiness Unit");
                }
                else if (_objDirectCustomerAverageUploadBE.IsReadCustomer)
                {
                    CustomerNotFound("This is read customer");
                }
                else if (_objDirectCustomerAverageUploadBE.IsHoldOrClosed)
                {
                    CustomerNotFound("Customer status is Hold/closed");
                }
                else
                {
                    CustomerNotFound("Failure in search of direct customer.");
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e) 
        {
            if (_objCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.DirectCustomerAverageUpload, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString()))
            {
                lblConfirmMsg.Text = "Are you sure you want to upload average for direct customer?";
                mpeCofirm.Show();
            }
            else
            {
                DirectCustomerAverageUploadBE _objDirectCustomerAverageUploadBE = new DirectCustomerAverageUploadBE();
                _objDirectCustomerAverageUploadBE.GlobalAccountNumber = lblGlobalAccountNo.Text;
                _objDirectCustomerAverageUploadBE.AccountNo = lblAccountNo.Text;
                _objDirectCustomerAverageUploadBE.AverageReading = Convert.ToDecimal(txtAverageReading.Text.Trim());
                _objDirectCustomerAverageUploadBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objDirectCustomerAverageUploadBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                _objDirectCustomerAverageUploadBE.ApprovalStatusId = (int)EnumApprovalStatus.Process;
                _objDirectCustomerAverageUploadBE.FunctionId = (int)EnumApprovalFunctions.DirectCustomerAverageUpload;
                _objDirectCustomerAverageUploadBE.Details = "";

                _objDirectCustomerAverageUploadBE = _objiDsignBal.DeserializeFromXml<DirectCustomerAverageUploadBE>(_objBookNoBal.UpdateDirectCustomerAverageUpload(_objDirectCustomerAverageUploadBE));

                if (_objDirectCustomerAverageUploadBE.IsSuccess)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblgreen");
                    lblActiveMsg.Text = Resource.APPROVAL_TARIFF_CHANGE;
                    divdetails.Attributes.Add("style", "display:none;");
                    txtAverageReading.Text = txtAccountNo.Text = string.Empty;
                    mpeActivate.Show();
                }
                else if (_objDirectCustomerAverageUploadBE.IsApprovalInProcess)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblred");
                    divdetails.Visible = false;
                    txtAverageReading.Text = txtAccountNo.Text = string.Empty;
                    lblActiveMsg.Text = Resource.REQ_STILL_PENDING;
                    mpeActivate.Show();
                }
                else
                {
                    divdetails.Visible = true;
                    Message(UMS_Resource.CUSTOMER_UPDATED_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                DirectCustomerAverageUploadBE _objDirectCustomerAverageUploadBE = new DirectCustomerAverageUploadBE();
                _objDirectCustomerAverageUploadBE.GlobalAccountNumber = lblGlobalAccountNo.Text;
                _objDirectCustomerAverageUploadBE.AccountNo = lblAccountNo.Text;
                _objDirectCustomerAverageUploadBE.AverageReading = Convert.ToDecimal(txtAverageReading.Text.Trim());
                _objDirectCustomerAverageUploadBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objDirectCustomerAverageUploadBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                _objDirectCustomerAverageUploadBE.ApprovalStatusId = (int)EnumApprovalStatus.Approved;  
                _objDirectCustomerAverageUploadBE.FunctionId = (int)EnumApprovalFunctions.DirectCustomerAverageUpload;
                _objDirectCustomerAverageUploadBE.IsFinalApproval = true;
                _objDirectCustomerAverageUploadBE.Details = "";

                _objDirectCustomerAverageUploadBE = _objiDsignBal.DeserializeFromXml<DirectCustomerAverageUploadBE>(_objBookNoBal.UpdateDirectCustomerAverageUpload(_objDirectCustomerAverageUploadBE));
                if (_objDirectCustomerAverageUploadBE.IsSuccess)
                {
                    lblActiveMsg.Text = "Average reading uploaded successfully";
                    divdetails.Attributes.Add("style", "display:none;");
                    txtAverageReading.Text = txtAccountNo.Text = string.Empty;
                    mpeActivate.Show();
                }
                else if (_objDirectCustomerAverageUploadBE.IsApprovalInProcess)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblred");
                    divdetails.Visible = false;
                    txtAverageReading.Text = txtAccountNo.Text = string.Empty;
                    lblActiveMsg.Text = Resource.REQ_STILL_PENDING;
                    mpeActivate.Show();
                }
                else
                {
                    divdetails.Attributes.Add("style", "display:block;");
                    Message(UMS_Resource.CUSTOMER_UPDATED_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void CustomerNotFound(string ErrorMessage)
        {
            divdetails.Attributes.Add("style", "display:none;");
            txtAccountNo.Text = txtAverageReading.Text = string.Empty;
            lblAccountNo.Text = lblGlobalAccountNo.Text = lblServiceAddress.Text = lblTariff.Text = lblOldAccNo.Text = lblName.Text = string.Empty;
            Message(ErrorMessage, UMS_Resource.MESSAGETYPE_ERROR);
        }

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}