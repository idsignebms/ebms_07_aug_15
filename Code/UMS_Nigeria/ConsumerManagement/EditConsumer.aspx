﻿<%@ Page Title="::Edit Consumer::" Language="C#" MasterPageFile="~/MasterPages/EDMUMS.Master"
    AutoEventWireup="true" CodeBehind="EditConsumer.aspx.cs" Inherits="UMS_Nigeria.ConsumerManagement.EditConsumer" %>

<%@ Register Assembly="EditableDropDownList" Namespace="EditableControls" TagPrefix="editable" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--    <script src="scripts/jquery-2.0.0.min.js" type="text/javascript"></script>--%>
    <%--<script src="scripts/jquery-1.6.4.min.js" type="text/javascript"></script>--%>
    <script src="../scripts/jquery.smartWizard.js" type="text/javascript"></script>
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../scripts/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="../scripts/jquery.ui.button.js" type="text/javascript"></script>
    <%--  <script src="../scripts/jquery.ui.position.js" type="text/javascript"></script>
    <script src="../scripts/jquery.ui.autocomplete.js" type="text/javascript"></script>
    <script src="../scripts/jquery.ui.combobox.js" type="text/javascript"></script>--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <script src="../scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <%--<link href="../Styles/style.css" rel="stylesheet" type="text/css" />--%>
    <script type="text/javascript">
        //        function IsDocumentNoExists(inputData) {
        //            var exists = true;
        //            var lblConsumerNo = document.getElementById('<%= lblConsumerNo.ClientID %>');
        //            var documentNo = $('#spanDocumentNo');
        //            //            if (TextBoxBlurValidationlbl(inputData, 'spanDocumentNo', 'Document No') == false) exists = false;
        //            //            else {
        //            var pagePath = window.location.pathname;
        //            $.ajax({
        //                type: "POST",
        //                url: pagePath + "/IsDocumentNoExists",
        //                data: "{ 'DocumentNo': '" + $.trim(inputData.value) + "','ConsumerNo':'" + $.trim(lblConsumerNo.innerHTML) + "' }",
        //                dataType: "json",
        //                contentType: "application/json; charset=utf-8",
        //                success: function (data) {
        //                    if (data['d'] == "True") {
        //                        exists = false;
        //                        // alert("DocumentNo already exists.");
        //                        documentNo.text("Document No already exists.");
        //                        documentNo.css("display", "block");
        //                        inputData.focus();
        //                        //return false;
        //                    }
        //                    else
        //                        documentNo.css("display", "none");
        //                }
        //            });
        //            //}
        //            if (!exists) {
        //                return false;
        //            }
        //        }

        function IsSequenceNoExists(inputData) {
            var exists = true;
            var spanCustomerSequenceNo = $('#spanCustomerSequenceNo');
            var lblConsumerNo = document.getElementById('<%= lblConsumerNo.ClientID %>');
            if (TextBoxBlurValidationlbl(inputData, 'spanCustomerSequenceNo', 'Customer SequenceNo') == false) exists = false;
            else {
                var pagePath = window.location.pathname;
                $.ajax({
                    type: "POST",
                    url: pagePath + "/IsSequenceNoExists",
                    data: "{ 'SequenceNo': '" + $.trim(inputData.value) + "' ,'ConsumerNo':'" + $.trim(lblConsumerNo.innerHTML) + "' }",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data['d'] == "True") {
                            exists = false;
                            // alert("DocumentNo already exists.");
                            spanCustomerSequenceNo.text("Sequence No already exists.");
                            spanCustomerSequenceNo.css("display", "block");
                            inputData.focus();
                            //return false;
                        }
                    }
                });
            }
            if (!exists) {
                return false;
            }
        }

        function IsOldAccountNoExists(inputData) {
            var exists = true;
            var spanOldAccountNo = $('#spanOldAccountNo');
            var lblConsumerNo = document.getElementById('<%= lblConsumerNo.ClientID %>');
            //            if (TextBoxBlurValidationlbl(inputData, 'spanOldAccountNo', 'Old Account No') == false) exists = false;
            //            else {
            var pagePath = window.location.pathname;
            $.ajax({
                type: "POST",
                url: pagePath + "/IsOldAccountNoExists",
                data: "{ 'OldAccountNo': '" + $.trim(inputData.value) + "' ,'ConsumerNo':'" + $.trim(lblConsumerNo.innerHTML) + "' }",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data['d'] == "True") {
                        exists = false;
                        spanOldAccountNo.text("Old Account No already exists.");
                        spanOldAccountNo.css("display", "block");
                        inputData.focus();
                    }
                }
            });
            //            }
            if (!exists) {
                return false;
            }
        }

        function IsMeterNoExists(inputData) {
            var exists = true;
            var spanMeterNo = $('#spanMeterNo');
            var lblConsumerNo = document.getElementById('<%= lblConsumerNo.ClientID %>');
            //var txtInitialReading = document.getElementById('<%= txtInitialReading.ClientID %>').setAttribute("disabled", true);
            var txtInitialReading = document.getElementById('<%= txtInitialReading.ClientID %>').disabled = false;

            if (TextBoxBlurValidationlbl(inputData, 'spanMeterNo', 'Meter No') == false) exists = false;
            else {
                var pagePath = window.location.pathname;
                $.ajax({
                    type: "POST",
                    url: pagePath + "/IsMeterNoExists",
                    data: "{ 'MeterNo': '" + $.trim(inputData.value) + "' ,'ConsumerNo':'" + $.trim(lblConsumerNo.innerHTML) + "' }",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data['d'] == "True") {
                            exists = false;
                            spanMeterNo.text("Meter No Is InValid.");
                            spanMeterNo.css("display", "block");
                            //var txtInitialReading = document.getElementById('<%= txtInitialReading.ClientID %>').setAttribute("disabled", false);
                            var txtInitialReading = document.getElementById('<%= txtInitialReading.ClientID %>').disabled = true;
                            inputData.focus();
                        }
                    }
                });
            }
            if (!exists) {
                var txtInitialReading = document.getElementById('<%= txtInitialReading.ClientID %>').disabled = true;
                return false;
            }
        }

        function MeterDials(inputData) {
            var exists = true;
            var txtMeterNo = document.getElementById('<%= txtMeterNo.ClientID %>');
            var spanMeterNo = $('#spanInitialReading');
            var txtInitialReading = document.getElementById('<%= txtInitialReading.ClientID %>');
            var lblConsumerNo = document.getElementById('<%= lblConsumerNo.ClientID %>');

            if (TextBoxValidationlblZeroAccepts(txtInitialReading, document.getElementById("spanInitialReading"), "Initial Reading") == false) exists = false;
            else {
                var pagePath = window.location.pathname;
                $.ajax({
                    type: "POST",
                    url: pagePath + "/MeterDials",
                    data: "{ 'MeterNo': '" + $.trim(txtMeterNo.value) + "' ,'ConsumerNo':'" + $.trim(lblConsumerNo.innerHTML) + "' }",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data['d'] == txtInitialReading.value.length) {
                            spanMeterNo.css("display", "none");
                        }
                        else {
                            exists = false;
                            spanMeterNo.text("Initial reading must be " + data['d'] + " characters");
                            spanMeterNo.css("display", "block");
                            inputData.focus();
                        }
                    }
                });
            }
            if (!exists) {
                return false;
            }
        }

        //        function IsNeighborAccountNoExists(inputData) {

        //            //var exists = true;
        //            var spanneighbor = $('#spanneighbor');
        //            spanneighbor.text("");
        //            var lblConsumerNo = document.getElementById('<%= lblConsumerNo.ClientID %>');
        //            //            if (TextBoxBlurValidationlbl(inputData, 'spanneighbor', 'Neighbour account no') == false) exists = false;
        //            //            else {
        //            var pagePath = window.location.pathname;
        //            $.ajax({
        //                type: "POST",
        //                url: pagePath + "/IsNeighborAccountNoExists",
        //                data: "{ 'NeighbourAccNo': '" + $.trim(inputData.value) + "' ,'ConsumerNo':'" + $.trim(lblConsumerNo.innerHTML) + "' }",
        //                dataType: "json",
        //                contentType: "application/json; charset=utf-8",
        //                success: function (data) {
        //                    if (inputData.value != "") {
        //                        if (data['d'] == "False") {
        //                            spanneighbor.text("Neighbour account no not exists.");
        //                            spanneighbor.css("display", "block");
        //                            inputData.focus();
        //                            var exists = false;
        //                            //alert(spanneighbor.text());

        //                        }
        //                        else {
        //                            spanneighbor.text("");
        //                            spanneighbor.css("display", "1px solid #8E8E8E");
        //                            // alert("2");
        //                        }
        //                    }
        //                }
        //            });
        //            //alert(exists);
        //            //}
        //            //            if (!exists) {
        //            return exists;
        //            //            }
        //        }


        function IsNeighborAccountNoExists(inputData) {
            var exists = true;
            var spanneighbor = $('#spanneighbor');
            var lblConsumerNo = document.getElementById('<%= lblConsumerNo.ClientID %>');
            spanneighbor.text("");
            //            if (TextBoxBlurValidationlbl(inputData, 'spanneighbor', 'Neighbour account no') == false) exists = false;
            //            else {
            if (Trim(inputData.value) != "") {
                var pagePath = window.location.pathname;
                $.ajax({
                    type: "POST",
                    url: pagePath + "/IsNeighborAccountNoExists",
                    data: "{ 'NeighbourAccNo': '" + $.trim(inputData.value) + "' ,'ConsumerNo':'" + $.trim(lblConsumerNo.innerHTML) + "' }",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (inputData.value != "") {
                            if (data['d'] == "False") {
                                exists = false;
                                spanneighbor.text("Neighbour account no does not exists.");
                                spanneighbor.css("display", "block");
                                inputData.focus();
                            }
                            else {
                                spanneighbor.text("");
                                spanneighbor.css("display", "1px solid #8E8E8E");
                            }
                        }
                    }
                });
                //}
            }
            if (!exists) {
                return false;
            }
        }

        function IsMeterSerialNoExists(inputData) {
            var exists = true;
            var spanMeterSerialNo = $('#spanMeterSerialNo');
            var lblConsumerNo = document.getElementById('<%= lblConsumerNo.ClientID %>');
            //if (TextBoxBlurValidationlbl(inputData, 'spanMeterSerialNo', 'Meterserialno') == false) exists = false;
            if (Trim(inputData.value) != "") {
                var pagePath = window.location.pathname;
                $.ajax({
                    type: "POST",
                    url: pagePath + "/IsMeterSerialNoExists",
                    data: "{ 'MeterSerialNo': '" + $.trim(inputData.value) + "' ,'ConsumerNo':'" + $.trim(lblConsumerNo.innerHTML) + "' }",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data['d'] == "True") {
                            exists = false;
                            spanMeterSerialNo.text("Meter serialno already exists.");
                            spanMeterSerialNo.css("display", "block");
                            inputData.focus();
                        }
                    }
                });
            }
            if (!exists) {
                return false;
            }
        }
    </script>
    <script type="text/javascript">
        function pageLoad() {
            Calendar('<%=txtApplicationDate.ClientID %>', '<%=imgDate.ClientID %>', "-2:+8");
            Calendar('<%=txtConnectionDate.ClientID %>', '<%=imgDate2.ClientID %>', "-2:+8");
            // Calendar('txtSetupDate.ClientID ', 'imgDate3.ClientID ', "-2:+8");

            // Smart Wizard 	
            $('#wizard').smartWizard({
                //                                onLeaveStep: leaveAStepCallback,
                //                                onFinish: onFinishCallback,
                //                                onShowStep: ShowStep
                enableAllSteps: true,
                keyNavigation: false
            });
            //actionBar Deletion
            var array = document.getElementsByClassName("actionBar");
            array[0].style.display = "none";

            //        $(window).bind('beforeunload', function () {
            //            return 'Are you sure you want to leave? All unsaved information will be lost';
            //        });
            DisplayMessage('<%= pnlMessage.ClientID %>');
        };
        
    </script>
    <script type="text/javascript">
        var i = 1;
        function AddFile() {
            i++;
            var div = document.createElement('DIV');
            div.innerHTML = '<input id="file' + i + '" name="file' + i + '" type="file" /><a href="#" style="font-weight:bold;" onclick="return RemoveFile(this)">Remove</a>';
            document.getElementById("divFile").appendChild(div);
            return false;
        }
        function RemoveFile(file) {
            document.getElementById("divFile").removeChild(file.parentNode);
            return false;
        }
    </script>
    <script type="text/javascript">
        function CopyNames() {
            var txtName = document.getElementById('<%= txtName.ClientID %>');
            var txtSurName = document.getElementById('<%= txtSurName.ClientID %>');
            var txtKnownAs = document.getElementById('<%= txtKnownAs.ClientID %>');
            txtKnownAs.value = txtName.value + " " + txtSurName.value;
        }

        function Step1Editblock() {
            //var spanColorRed = document.getElementsByClassName("spanColorRed");
            //var spanColorRedClass = document.getElementsByClassName("spancolorRed");
            var spanColorRed = document.getElementsByClassName("dummy1");
            for (var i = 0; i < spanColorRed.length; i++) {
                spanColorRed[i].style.display = "block";
            }
        }
        function Step1Editnone() {
            //var spanColorRed = document.getElementsByClassName("spanColorRed");
            //var spanColorRedClass = document.getElementsByClassName("spancolorRed");
            var spanColorRed = document.getElementsByClassName("dummy1");
            for (var i = 0; i < spanColorRed.length; i++) {
                spanColorRed[i].style.display = "none";
            }
        }
        function Step2Editblock() {
            //var spanColorRed = document.getElementsByClassName("spanColorRed");
            //var spanColorRedClass = document.getElementsByClassName("spancolorRed");
            var spanColorRed = document.getElementsByClassName("dummy2");
            for (var i = 0; i < spanColorRed.length; i++) {
                spanColorRed[i].style.display = "block";
            }
        }
        function Step2Editnone() {
            //var spanColorRed = document.getElementsByClassName("spanColorRed");
            //var spanColorRedClass = document.getElementsByClassName("spancolorRed");
            var spanColorRed = document.getElementsByClassName("dummy2");
            for (var i = 0; i < spanColorRed.length; i++) {
                spanColorRed[i].style.display = "none";
            }
        }
        function Step3Editblock() {
            //var spanColorRed = document.getElementsByClassName("spanColorRed");
            //var spanColorRedClass = document.getElementsByClassName("spancolorRed");
            var spanColorRed = document.getElementsByClassName("dummy3");
            for (var i = 0; i < spanColorRed.length; i++) {
                spanColorRed[i].style.display = "block";
            }
        }
        function Step3Editnone() {
            //var spanColorRed = document.getElementsByClassName("spanColorRed");
            //var spanColorRedClass = document.getElementsByClassName("spancolorRed");
            var spanColorRed = document.getElementsByClassName("dummy3");
            for (var i = 0; i < spanColorRed.length; i++) {
                spanColorRed[i].style.display = "none";
            }
        }

    </script>
    <script type="text/javascript">
        function Step1Update() {
            var ddlTitle = document.getElementById('<%= ddlTitle.ClientID %>');
            var txtName = document.getElementById('<%= txtName.ClientID %>');
            var txtSurName = document.getElementById('<%= txtSurName.ClientID %>');
            var txtDocumentNo = document.getElementById('<%= txtDocumentNo.ClientID %>');

            var txtHome = document.getElementById('<%= txtHome.ClientID %>');
            var txtBusiness = document.getElementById('<%= txtBussiness.ClientID %>');
            var txtOthers = document.getElementById('<%= txtOthers.ClientID %>');
            //var txtLandMark = document.getElementById('<%= txtLandMark.ClientID %>');
            var txtHouseNo = document.getElementById('<%= txtHouseNo.ClientID %>');
            //var txtCity = document.getElementById('<%= txtCity.ClientID %>');
            var txtStreet = document.getElementById('<%= txtStreet.ClientID %>');
            var ddlState = document.getElementById('<%= ddlState.ClientID %>');
            //var txtPostalDetails = document.getElementById('<%= txtPostalDetails.ClientID %>');
            //var txtZipCode = document.getElementById('<%= txtZipCode.ClientID %>');
            //var txtIdentityNumber = document.getElementById('<%= txtIdentityNumber.ClientID %>');
            var txtEmailId = document.getElementById('<%= txtEmailId.ClientID %>');
            //var ddlIdentityType = document.getElementById('<%= ddlIdentityType.ClientID %>');
            var lblConsumerNo = document.getElementById('<%= lblConsumerNo.ClientID %>');

            var IsValid = true;
            //if (TextBoxValidationlbl(ddlTitle, document.getElementById("spanTitle"), "Title") == false) IsValid = false;
            //if (DropDownlistOnChangelbl(ddlTitle, "spanTitle", "Title") == false) IsValid = false;
            if (TextBoxValidationlbl(txtName, document.getElementById("spanName"), "Name") == false) IsValid = false;
            //if (TextBoxValidationlbl(txtSurName, document.getElementById("spanSurName"), "Surname") == false) IsValid = false;
            if (txtDocumentNo.value != "")
                if (IsDocumentNoExists(txtDocumentNo) == false) IsValid = false;
            //if (IsSequenceNoExists(txtCustomerSequenceNo) == false) IsValid = false;
            //Contact Fields are not restricted with length
            //            if (Trim(txtHome.value) != "") {
            //                var ContactNoLength = "<%= Resources.GlobalConstants.CONTACT_NO_LENGTH %>";
            //                ContactNoLength = parseInt(ContactNoLength);
            //                if (TextBoxValidContactLbl(txtHome, "Contact No", document.getElementById("spanHome"), ContactNoLength) == false) IsValid = false;
            //                else {
            //                    txtHome.style.border = "1px solid #8E8E8E";
            //                    document.getElementById("spanHome").innerHTML = "";
            //                }
            //            }
            //            else {
            //                txtHome.style.border = "1px solid #8E8E8E";
            //                document.getElementById("spanHome").innerHTML = "";
            //            }
            //            if (Trim(txtBusiness.value) != "") {
            //                var ContactNoLength = "<%= Resources.GlobalConstants.CONTACT_NO_LENGTH %>";
            //                ContactNoLength = parseInt(ContactNoLength);
            //                if (TextBoxValidContactLbl(txtBusiness, "Contact No", document.getElementById("spanBusiness"), ContactNoLength) == false) IsValid = false;
            //                else {
            //                    txtBusiness.style.border = "1px solid #8E8E8E";
            //                    document.getElementById("spanBusiness").innerHTML = "";
            //                }
            //            }
            //            else {
            //                txtBusiness.style.border = "1px solid #8E8E8E";
            //                document.getElementById("spanBusiness").innerHTML = "";
            //            }
            //            if (Trim(txtOthers.value) != "") {
            //                var ContactNoLength = "<%= Resources.GlobalConstants.CONTACT_NO_LENGTH %>";
            //                ContactNoLength = parseInt(ContactNoLength);
            //                if (TextBoxValidContactLbl(txtOthers, "Contact No", document.getElementById("spanOthers"), ContactNoLength) == false) IsValid = false;
            //                else {
            //                    txtOthers.style.border = "1px solid #8E8E8E";
            //                    document.getElementById("spanOthers").innerHTML = "";
            //                }
            //            }
            //            else {
            //                txtOthers.style.border = "1px solid #8E8E8E";
            //                document.getElementById("spanOthers").innerHTML = "";
            //            }
            //Contact Fields are not restricted with length
            //if (TextBoxValidationlbl(txtLandMark, document.getElementById("spanLandMark"), "Landmark") == false) IsValid = false;
            if (TextBoxValidationlblZeroAccepts(txtHouseNo, document.getElementById("spanHouseNo"), "House No") == false) IsValid = false;
            //if (TextBoxValidationlbl(txtPostalDetails, document.getElementById("spanPostDetails"), "Postal Details") == false) IsValid = false;
            //if (TextBoxValidationlbl(txtZipCode, document.getElementById("spanZipCode"), "ZipCode") == false) IsValid = false;
            if (TextBoxValidationlbl(txtStreet, document.getElementById("spanStreet"), "Address 1") == false) IsValid = false;
            //if (TextBoxValidationlbl(ddlState, document.getElementById("spanState"), "State") == false) IsValid = false;
            //if (DropDownlistOnChangelbl(ddlState, "spanState", "State") == false) IsValid = false;
            //if (TextBoxValidationlbl(ddlDistrict, document.getElementById("spanDistrict"), "District") == false) IsValid = false;
            //if (DropDownlistOnChangelbl(ddlDistrict, "spanDistrict", "District") == false) IsValid = false;
            //if (TextBoxValidationlbl(txtCity, document.getElementById("spanCity"), "LGA/City/Village") == false) IsValid = false;
            //            if (TextBoxValidationlbl(txtEmailId, document.getElementById("spanemail"), "Email Id") == false) IsValid = false;
            //            else
            if (Trim(txtEmailId.value) != "") {
                if (EmailValidation(txtEmailId) == false) {
                    var spanEmail = document.getElementById("spanemail");
                    spanEmail.innerHTML = "Please Enter Valid Email Id";
                    spanEmail.style.display = "block";
                    txtEmailId.style.border = "1px solid red";
                    IsValid = false;
                }
                else {
                    txtEmailId.style.border = "1px solid #8E8E8E";
                    document.getElementById("spanemail").innerHTML = "";
                }
            }
            else {
                txtEmailId.style.border = "1px solid #8E8E8E";
                document.getElementById("spanemail").innerHTML = "";
            }
            //if (TextBoxValidationlbl(txtIdentityNumber, document.getElementById("spanIdentityNo"), "Identity Number") == false) IsValid = false;
            //if (TextBoxValidationlbl(ddlIdentityType, document.getElementById("spanIdentityType"), "Identity Type") == false) IsValid = false;
            //if (DropDownlistOnChangelbl(ddlIdentityType, "spanIdentityType", "Identity Type") == false) IsValid = false;

            //            var rbtnPostalAddress = document.getElementById("rbtnPostalAddress");
            //            var rbtnServiceAddress = document.getElementById("rbtnServiceAddress");
            //            if (rbtnPostalAddress.checked) {
            //                CopyAddress(true);
            //            }
            //                else {
            //                    rbtnServiceAddress.checked = true;
            //                    CopyAddress(false);
            //                }

            if (!IsValid) {
                return false;
            }
        }
    </script>
    <script type="text/javascript">
        function Step2Update() {

            //            var ddlBusinessUnit = document.getElementById('<%= ddlBusinessUnit.ClientID %>');
            //            var ddlServiceUnit = document.getElementById('<%= ddlServiceUnit.ClientID %>');
            //            var ddlServiceCenter = document.getElementById('<%= ddlServiceCenter.ClientID %>');
            //            var ddlBookNumber = document.getElementById('<%= ddlBookNumber.ClientID %>');
            var txtInjectionSubStation = document.getElementById('<%= ddlInjectionSubStation.ClientID %>');
            var txtFeeder = document.getElementById('<%= ddlFeeder.ClientID %>');
            var txtServiceLandMark = document.getElementById('<%= txtServiceLandMark.ClientID %>');
            var txtServiceHouseNo = document.getElementById('<%= txtServiceHouseNo.ClientID %>');
            var txtServiceDetails = document.getElementById('<%= txtServiceDetails.ClientID %>');
            var txtServiceCity = document.getElementById('<%= txtServiceCity.ClientID %>');
            var txtServiceStreet = document.getElementById('<%= txtServiceStreet.ClientID %>');
            var txtServiceZipCode = document.getElementById('<%= txtServiceZipCode.ClientID %>');
            var txtServiceEmailId = document.getElementById('<%= txtServiceEmailId.ClientID %>');
            var txtMobileNo = document.getElementById('<%= txtMobileNo.ClientID %>');
            var txtneighbor = document.getElementById('<%= txtneighbor.ClientID %>');
            var txtRouteSequenceNo = document.getElementById('<%= txtRouteSequenceNo.ClientID %>');
            var ddlRouteNo = document.getElementById('<%= ddlRouteNo.ClientID %>');

            var IsValid = true;

            //            //if (TextBoxValidationlbl(ddlBusinessUnit, document.getElementById("spanBusinessUnit"), "Busniess Unit") == false) IsValid = false;
            //            if (DropDownlistOnChangelbl(ddlBusinessUnit, "spanBusinessUnit", "Busniess Unit") == false) IsValid = false;
            //            //if (TextBoxValidationlbl(ddlServiceUnit, document.getElementById("spanServiceUnit"), "Service Unit") == false) IsValid = false;
            //            if (DropDownlistOnChangelbl(ddlServiceUnit, "spanServiceUnit", "Service Unit") == false) IsValid = false;
            //            //if (TextBoxValidationlbl(ddlServiceCenter, document.getElementById("spanServiceCenter"), "Service Center") == false) IsValid = false;
            //            if (DropDownlistOnChangelbl(ddlServiceCenter, "spanServiceCenter", "Service Center") == false) IsValid = false;
            //            //if (TextBoxValidationlbl(ddlBookNumber, document.getElementById("spanBookNumber"), "Book Number") == false) IsValid = false;
            //            if (DropDownlistOnChangelbl(ddlBookNumber, "spanBookNumber", "Book Number") == false) IsValid = false;

            //            if (IsNeighborAccountNoExists(txtneighbor) == true) {
            //                alert("123");

            if (TextBoxValidationlbl(txtServiceLandMark, document.getElementById("spanServiceLandMark"), "Service LandMark") == false) IsValid = false;

            if (TextBoxValidationlbl(txtServiceHouseNo, document.getElementById("spanServiceHouseNo"), "Service House Number") == false) IsValid = false;

            //if (TextBoxValidationlbl(txtServiceDetails, document.getElementById("spanServiceDetails"), "Service Details") == false) IsValid = false;
            if (TextBoxValidationlbl(txtServiceCity, document.getElementById("spanServiceCity"), "Service LGA/City/Village") == false) IsValid = false;
            if (TextBoxValidationlbl(txtServiceStreet, document.getElementById("spanServiceStreet"), "Service Street") == false) IsValid = false;
            //if (TextBoxValidationlbl(txtServiceEmailId, document.getElementById("spanemailadd"), "Service Email Address") == false) IsValid = false;
            if (IsNeighborAccountNoExists(txtneighbor) == false) IsValid = false;

            //            if ($('#spanneighbor').text() != "") {
            //                IsValid = false;
            ////                alert("yes");
            //            }
            //            else {
            //                alert("No");

            //            }
            //            alert(IsValid);


            //            if (document.getElementById("spanneighbor").innerHTML != "") {
            //                IsValid = false;
            //            }
            if (Trim(txtServiceEmailId.value) != "") {
                if (EmailValidation(txtServiceEmailId) == false) {
                    var spanemailadd = document.getElementById("spanemailadd")
                    spanemailadd.innerHTML = "Please Enter Valid Service Email Id";
                    spanemailadd.style.display = "block";
                    txtServiceEmailId.style.border = "1px solid red";
                    IsValid = false;
                }
                else {
                    txtServiceEmailId.style.border = "1px solid #8E8E8E";
                    document.getElementById("spanemailadd").innerHTML = "";
                }
            }
            else {
                txtServiceEmailId.style.border = "1px solid #8E8E8E";
                document.getElementById("spanemailadd").innerHTML = "";
            }
            //if (TextBoxValidationlbl(txtServiceZipCode, document.getElementById("spanServiceZipCode"), "Service ZipCode") == false) IsValid = false;
            //if (ValidateMobileNo() == false) IsValid = false;
            //if (Trim(txtMobileNo.value) != "") {
            //Contact Fields are not restricted with length
            if (TextBoxValidationlbl(txtMobileNo, document.getElementById("spanMobileNo"), "Mobile Number") == false) return false;
            //            var ContactNoLength = "<%= Resources.GlobalConstants.CONTACT_NO_LENGTH %>";
            //            ContactNoLength = parseInt(ContactNoLength);
            //            if (TextBoxValidContactLbl(txtMobileNo, "Mobile No", document.getElementById("spanMobileNo"), ContactNoLength) == false) IsValid = false;
            //            else {
            //                txtMobileNo.style.border = "1px solid #8E8E8E";
            //                document.getElementById("spanMobileNo").innerHTML = "";
            //            }
            //Contact Fields are not restricted with length
            //            }
            //            else {
            //                txtMobileNo.style.border = "1px solid #8E8E8E";
            //                document.getElementById("spanMobileNo").innerHTML = "";
            //            }
            //if (TextBoxValidationlbl(txtRouteSequenceNo, document.getElementById("spanRouteSequenceNo"), "Route Sequence Number") == false) IsValid = false;
            if (DropDownlistOnChangelbl(ddlRouteNo, "spanRouteNo", "Route No") == false) IsValid = false;


            if ($('#spanneighbor').text() != "") {
                IsValid = false;
            }
            //}
            if (!IsValid) {
                return false;
            }

            //return IsValid;
        }
    </script>
    <script type="text/javascript">
        function Step3Update() {
            var ddlTariff = document.getElementById('<%= ddlTariff.ClientID %>');
            var txtMeterNo = document.getElementById('<%= txtMeterNo.ClientID %>');
            var txtOldAccountNo = document.getElementById('<%= txtOldAccountNo.ClientID %>');
            var txtApplicationDate = document.getElementById('<%= txtApplicationDate.ClientID %>');
            var ddlAccountType = document.getElementById('<%= ddlAccountType.ClientID %>');
            var ddlReadCode = document.getElementById('<%= ddlReadCode.ClientID %>');
            var ddlCustomerType = document.getElementById('<%= ddlCustomerType.ClientID %>');
            var ddlMeterType = document.getElementById('<%= ddlMeterType.ClientID %>');
            var ddlPhase = document.getElementById('<%= ddlPhase.ClientID %>');
            var txtMeeterSerialNo = document.getElementById('<%= txtMeeterSerialNo.ClientID %>');
            var txtConnectionDate = document.getElementById('<%= txtConnectionDate.ClientID %>');
            var ddlMeeterStatus = document.getElementById('<%= ddlMeeterStatus.ClientID %>');
            var ddlConnectionReason = document.getElementById('<%= ddlConnectionReason.ClientID %>');
            var txtInitialReading = document.getElementById('<%= txtInitialReading.ClientID %>');
            var divMeterInformation = document.getElementById('<%= divMeterInformation.ClientID %>');
            var txtAverageReading = document.getElementById('<%= txtAverageReading.ClientID %>');
            //var txtSetupDate = document.getElementById(' txtSetupDate.ClientID ');
            var ddlCertifiedBy = document.getElementById('<%= ddlCertifiedBy.ClientID %>');
            var rblApplicationProccessedBy = document.getElementById('<%= rblApplicationProccessedBy.ClientID %>');
            var ddlAgencyName = document.getElementById('<%= ddlAgencyName.ClientID %>');
            var ddlInstalledBy = document.getElementById('<%= ddlInstalledBy.ClientID %>');
            var txtEmbassyCode = document.getElementById('<%= txtEmbassyCode.ClientID %>');
            var chkIsEmbassyCustomer = document.getElementById('<%= chkIsEmbassyCustomer.ClientID %>');
            var divMinReading = document.getElementById('<%=divMinReading.ClientID %>');
            var txtMinReading = document.getElementById('<%= txtMinReading.ClientID %>');

            var IsValid = true;

            //if (TextBoxValidationlbl(ddlTariff, document.getElementById("spanTariff"), "Tariff") == false) IsValid = false;
            if (DropDownlistOnChangelbl(ddlTariff, "spanTariff", "Tariff") == false) IsValid = false;
            //if (TextBoxValidationlbl(ddlAccountType, document.getElementById("spanAccountType"), "Account Type") == false) IsValid = false;
            //if (TextBoxValidationlbl(txtOldAccountNo, document.getElementById("spanOldAccountNo"), "Old AccountNo") == false) IsValid = false;

            if (DropDownlistOnChangelbl(ddlAccountType, "spanAccountType", "Account Type") == false) IsValid = false;
            if (Trim(txtApplicationDate.value) != "") {
                if (isDateLbl(txtApplicationDate.value, txtApplicationDate, "spanApplicationDate") == false) IsValid = false;
                if (GreaterToday(txtApplicationDate, "Application Date should be Less than or equal to Today's Date") == false) IsValid = false;
            }
            else {
                document.getElementById("spanApplicationDate").innerHTML = "";
                txtApplicationDate.style.border = "1px solid #8E8E8E";
            }
            //if (TextBoxValidationlbl(ddlReadCode, document.getElementById("spanReadCode"), "Read Code") == false) IsValid = false;
            if (DropDownlistOnChangelbl(ddlReadCode, "spanReadCode", "Read Code") == false) IsValid = false;
            if (TextBoxValidationlbl(ddlCustomerType, document.getElementById("spanCustomerType"), "Customer Type") == false) IsValid = false;
            if (DropDownlistOnChangelbl(ddlCustomerType, "spanCustomerType", "Customer Type") == false) IsValid = false;
            if (chkIsEmbassyCustomer.checked) {
                if (TextBoxValidationlbl(txtEmbassyCode, document.getElementById("spanEmbassyCode"), "Embassy Code") == false) IsValid = false;
            }

            if (divMinReading != null) {
                if (TextBoxValidationlbl(txtMinReading, document.getElementById("spanMinReading"), "Min Reading") == false) IsValid = false;
            }

            if (divMeterInformation != null) {
                //if (TextBoxValidationlbl(ddlMeterType, document.getElementById("spanMeterType"), "Meter Type") == false) IsValid = false;
                if (DropDownlistOnChangelbl(ddlMeterType, "spanMeterType", "Meter Type") == false) IsValid = false;
                //if (TextBoxValidationlbl(ddlPhase, document.getElementById("spanPhase"), "Phase") == false) IsValid = false;
                if (DropDownlistOnChangelbl(ddlPhase, "spanPhase", "Phase") == false) IsValid = false;
                //if (TextBoxValidationlbl(txtMeterNo, document.getElementById("spanMeterNo"), "Meter Number") == false) IsValid = false;
                if (IsMeterNoExists(txtMeterNo) == false) IsValid = false;
                //if (TextBoxValidationlbl(txtMeeterSerialNo, document.getElementById("spanMeterSerialNo"), "Serial Number") == false) IsValid = false;
                if (Trim(txtMeeterSerialNo.value) != "") {
                    if (IsMeterSerialNoExists(txtMeeterSerialNo) == false) IsValid = false;
                }
                else {
                    txtMeeterSerialNo.style.border = "1px solid #8E8E8E";
                    document.getElementById("spanMeterSerialNo").innerHTML = "";
                }
                if (TextBoxValidationlbl(txtConnectionDate, document.getElementById("spanConnectionDate"), "Connection Date") == false) IsValid = false;
                if (isDateLbl(txtConnectionDate.value, txtConnectionDate, "spanConnectionDate") == false) IsValid = false;
                if (GreaterToday(txtConnectionDate, "Connection Date should be Less than or equal to Today's Date") == false) IsValid = false;
                //if (TextBoxValidationlbl(ddlMeeterStatus, document.getElementById("spanMeterStatus"), "Meter Status") == false) IsValid = false;
                //if (DropDownlistOnChangelbl(ddlMeeterStatus, "spanMeterStatus", "Meter Status") == false) IsValid = false;
                //if (TextBoxValidationlbl(ddlConnectionReason, document.getElementById("spanConnectionReason"), "Connection Reason") == false) IsValid = false;
                if (DropDownlistOnChangelbl(ddlConnectionReason, "spanConnectionReason", "Connection Reason") == false) IsValid = false;
                //if (TextBoxValidationlblZeroAccepts(txtInitialReading, document.getElementById("spanInitialReading"), "Initial Reading") == false) IsValid = false;
                if (MeterDials(txtInitialReading) == false) IsValid = false;
                //if (TextBoxValidationlbl(txtCurrentReading, document.getElementById("spanCurrentReading"), "Current Reading") == false) IsValid = false;
                if (TextBoxValidationlbl(txtAverageReading, document.getElementById("spanAverageReading"), "Average Reading") == false) IsValid = false;
                //if (TextBoxValidationlbl(txtSetupDate, document.getElementById("spanSetupDate"), "Setup Date") == false) IsValid = false;
                //if (isDateLbl(txtSetupDate.value, txtSetupDate, "spanSetupDate") == false) IsValid = false;
                //                if (TextBoxValidationlbl(ddlCertifiedBy, document.getElementById("spanCertifiedBy"), "Certified By") == false) IsValid = false;

                var rblApplicationProccessedByList = rblApplicationProccessedBy.getElementsByTagName("input");
                if (RadioButtonListlbl(rblApplicationProccessedBy, "spanApplicationProcessedBy", " One") == false) IsValid = false;
                else {

                    if (rblApplicationProccessedByList[1].checked) {
                        // if (TextBoxValidationlbl(ddlAgencyName, document.getElementById("spanAgency"), "Agency") == false) IsValid = false;
                        if (DropDownlistOnChangelbl(ddlAgencyName, "spanAgency", "Agency Name") == false) IsValid = false;
                    }
                    else {
                        //if (TextBoxValidationlbl(ddlInstalledBy, document.getElementById("spanInstalledBy"), "InstalledBy") == false) IsValid = false;
                        if (DropDownlistOnChangelbl(ddlInstalledBy, "spanInstalledBy", "Installed By") == false) IsValid = false;
                    }
                }
            }
            //Document validation
            var divFile = document.getElementById("divFile");
            var Docitems = divFile.getElementsByTagName("input");
            for (var i = 0; i < Docitems.length; i++) {
                var spanDocuments = document.getElementById("spanDocuments").innerHTML = "";
                if (!Trim(Docitems[i].value) == "") {
                    if (!/(\.doc|\.docx|\.pdf|\.xlsx|\.xls)$/i.test(Docitems[i].value)) {
                        var spanDocuments = document.getElementById("spanDocuments");
                        spanDocuments.innerHTML = "Please select .doc, .docx, .xlsx , .pdf and .xls File Formats only";
                        spanDocuments.style.display = "block";
                        IsValid = false;
                        break;
                    }
                    //                    if (Docitems[i].size > 1024000) {
                    //                        //alert("scanned photo should be upto 1 MB only");
                    //                        var spanDocuments = document.getElementById("spanDocuments");
                    //                        spanDocuments.innerHTML = "Document should be upto 1 MB only";
                    //                        spanDocuments.style.display = "block";
                    //                        IsValid = false;
                    //                    }
                }
                else {
                    var spanDocuments = document.getElementById("spanDocuments").innerHTML = "";
                }
            }

            if (!IsValid) {
                return false;
            }
        }

        function ValidateMobileNo() {
            var txtMobileNo = document.getElementById('<%= txtMobileNo.ClientID %>');
            var ContactNoLength = "<%= Resources.GlobalConstants.CONTACT_NO_LENGTH %>";
            ContactNoLength = parseInt(ContactNoLength);
            if (TextBoxValidationlbl(txtMobileNo, document.getElementById("spanMobileNo"), "Mobile Number") == false) return false;
            if (TextBoxValidContactLbl(txtMobileNo, "Mobile No", document.getElementById("spanMobileNo"), ContactNoLength) == false) return false;
        }

        //        function InstalledBy() {
        //            var ddlAgencyName = document.getElementById('<%= ddlAgencyName.ClientID %>');
        //            var ddlInstalledBy = document.getElementById('<%= ddlInstalledBy.ClientID %>');
        //            var rblApplicationProccessedBy = document.getElementById('<%= rblApplicationProccessedBy.ClientID %>');
        //            var rblApplicationProccessedByList = rblApplicationProccessedBy.getElementsByTagName("input");
        //            if (rblApplicationProccessedByList[1].checked) {
        //                document.getElementById('<%= divApplicationByOthers.ClientID %>').style.display = "block";
        //                document.getElementById('<%= divBEDCApplication.ClientID %>').style.display = "none";

        //                //                document.getElementById('<%= divApplicationByOthers.ClientID %>').style.visibility = true;
        //                //                document.getElementById('<%= divBEDCApplication.ClientID %>').style.visibility = false;
        //            }
        //            else {
        //                document.getElementById('<%= divApplicationByOthers.ClientID %>').style.display = "none";
        //                document.getElementById('<%= divBEDCApplication.ClientID %>').style.display = "block";
        //                ddlInstalledBy.value = 0;
        //                //                document.getElementById('<%= divApplicationByOthers.ClientID %>').style.visibility = false;
        //                //                document.getElementById('<%= divBEDCApplication.ClientID %>').style.visibility = true;
        //            }
        //        }

        function DocsVisible() {
            var divDocs = document.getElementById("divDocs");
            divDocs.style.display = "block";
        }
        function DocsInVisible() {
            var divDocs = document.getElementById("divDocs");
            divDocs.style.display = "none";
        }

        function Embassy() {
            var chkIsEmbassyCustomer = document.getElementById('<%= chkIsEmbassyCustomer.ClientID %>');
            var txtEmbassyCode = document.getElementById('<%= txtEmbassyCode.ClientID %>');
            var divEmbassyCode = document.getElementById('<%= divEmbassyCode.ClientID %>');
            if (chkIsEmbassyCustomer.checked) {
                txtEmbassyCode.value = "";
                divEmbassyCode.style.display = "block";

            }
            else {
                txtEmbassyCode.value = "";

                divEmbassyCode.style.display = "none";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:ModalPopupExtender ID="mpeBillInProcess" runat="server" TargetControlID="hdnCancel1"
        PopupControlID="pnlBillInProcess" BackgroundCssClass="popbg" CancelControlID="btnBPOK">
    </asp:ModalPopupExtender>
    <asp:HiddenField ID="hdnCancel1" runat="server"></asp:HiddenField>
    <asp:Panel runat="server" ID="pnlBillInProcess" CssClass="modalPopup" Style="display: none;
        border-color: #639B00;">
        <div class="popheader" style="background-color: red;">
            <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, BILL_IN_PROCESS_EDIT%>"></asp:Label>
        </div>
        <div class="footer" align="right">
            <div class="fltr popfooterbtn">
                <br />
                <asp:Button ID="btnBPOK" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="btn_ok"
                    Style="margin-left: 10px;" />
            </div>
            <div class="clear pad_5">
            </div>
        </div>
    </asp:Panel>
    <div id="wizard" class="swMain">
        <ul>
            <li><a href="#step-1">
                <label class="stepNumber">
                    1</label>
                <span class="stepDesc"><small>
                    <asp:Literal ID="litPersonal" runat="server" Text="<%$ Resources:Resource, PERSONAL%>"></asp:Literal></small>
                </span></a></li>
            <li><a href="#step-2">
                <label class="stepNumber">
                    2</label>
                <span class="stepDesc"><small>
                    <asp:Literal ID="litService" runat="server" Text="<%$ Resources:Resource, SERVICE%>"></asp:Literal>
                </small></span></a></li>
            <li><a href="#step-3">
                <label class="stepNumber">
                    3</label>
                <span class="stepDesc"><small>
                    <asp:Literal ID="litAccount" runat="server" Text="<%$ Resources:Resource, ACCOUNT%>"></asp:Literal>
                </small></span></a></li>
            <li class="head">
                <h2 class="consume_head">
                    <asp:Literal ID="Literal50" runat="server" Text="<%$ Resources:Resource, CONSUMER_NO%>"></asp:Literal>
                    <asp:Label ID="lblAccountNo" runat="server"></asp:Label><span id="spanBookNoChanged"
                        runat="server" visible="false" style="color: Red">*</span>
                    <asp:Label ID="lblConsumerNo" Style="display: none;" runat="server"></asp:Label>
                </h2>
            </li>
        </ul>
        <%----------------------------------------------------------Step 1----------------------------------------- --%>
        <div id="step-1" style="height: 740px;">
            <asp:UpdatePanel ID="upnlStep1" runat="server">
                <ContentTemplate>
                    <h2 class="StepTitle">
                        <asp:Literal ID="litCustomerDetails" runat="server" Text="<%$ Resources:Resource, CUSTOMER_DETAILS%>"></asp:Literal>
                        <asp:LinkButton ID="lbtnStep1Edit" Visible="false" Style="float: right" OnClick="lbtnStep1Edit_Click"
                            runat="server" Text="<%$ Resources:Resource, EDIT%>"></asp:LinkButton>
                        <asp:LinkButton ID="lbtnStep1Cancel" Style="float: right" Visible="false" OnClick="lbtnStep1Cancel_Click"
                            runat="server" Text="<%$ Resources:Resource, CANCEL%>"></asp:LinkButton>&nbsp
                        &nbsp &nbsp &nbsp
                        <asp:LinkButton ID="lbtnStep1Update" Style="float: right; padding-right: 15px;" Visible="false"
                            OnClick="lbtnStep1Update_Click" runat="server" Text="<%$ Resources:Resource, UPDATE_EDIT%>"
                            OnClientClick="return Step1Update();"></asp:LinkButton></h2>
                    <div class="consumer_total">
                        <div class="pad_5">
                        </div>
                        <h3>
                            <asp:Literal ID="litPersonalDetails" runat="server" Text="<%$ Resources:Resource, PERSONAL_DETAILS%>"></asp:Literal>
                        </h3>
                        <div class="pad_10">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="litTitle" runat="server" Text="<%$ Resources:Resource, TITLE%>"></asp:Literal></div>
                            <div class="consume_input">
                                <%--<editable:EditableDropDownList ID="ddlTitle" Visible="false" runat="server" Sorted="false" AutoselectFirstItem="False"
                            placeholder="Select Title">
                            <asp:ListItem Text="Mr" Value="1">
                            </asp:ListItem>
                            <asp:ListItem Text="Miss" Value="2">
                            </asp:ListItem>
                        </editable:EditableDropDownList>--%>
                                <asp:Label ID="lblTitle" runat="server"></asp:Label>
                                <asp:DropDownList ID="ddlTitle" runat="server" Visible="false">
                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Mr" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Miss" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Mrs" Value="3"></asp:ListItem>
                                </asp:DropDownList>
                                <span id="spanTitle" class="spancolor"></span>
                            </div>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="litName" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal></div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtName" runat="server" Visible="false" onkeyup="CopyNames()" onblur="return TextBoxBlurValidationlbl(this,'spanName','Name')"></asp:TextBox>
                                <div class="dummy1">
                                    <span class="spancolorRed">*</span></div>
                                <%--<asp:FilteredTextBoxExtender ID="ftbname" runat="server" TargetControlID="txtName"
                                    ValidChars=" " FilterMode="ValidChars" FilterType="LowercaseLetters,UppercaseLetters,Custom">
                                </asp:FilteredTextBoxExtender>--%>
                                <span id="spanName" class="spancolor"></span>
                                <asp:Label ID="lblName" runat="server"></asp:Label>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="litSurName" runat="server" Text="<%$ Resources:Resource, SURNAME%>"></asp:Literal></div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtSurName" Visible="false" onkeyup="CopyNames()" runat="server"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbsurname" runat="server" TargetControlID="txtSurName"
                                    ValidChars=" " FilterMode="ValidChars" FilterType="LowercaseLetters,UppercaseLetters,Custom">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanSurName" class="spancolor"></span>
                                <asp:Label ID="lblSurName" runat="server"></asp:Label>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal49" runat="server" Text="<%$ Resources:Resource, EMAIL_ID%>"></asp:Literal></div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtEmailId" Visible="false" runat="server" placeholder="Email Id"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbEditSEmail" runat="server" TargetControlID="txtEmailId"
                                    ValidChars="@._" FilterType="LowercaseLetters,UppercaseLetters,Numbers,Custom">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanemail" class="spancolor"></span>
                                <asp:Label ID="lblEmailId" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="litKnownAs" runat="server" Text="<%$ Resources:Resource, KNOWN_AS%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtKnownAs" Visible="false" runat="server"></asp:TextBox>
                                <span id="spanKnownAs" class="spancolor"></span>
                                <asp:Label ID="lblKnownAs" runat="server"></asp:Label>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="litDocumentNo" runat="server" Text="<%$ Resources:Resource, DOCUMENT_NO%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtDocumentNo" Visible="false" runat="server" onblur="return IsDocumentNoExists(this)"></asp:TextBox>
                                <%--<div class="dummy1">
                                    <span class="spancolorRed">*</span></div>--%>
                                <span id="spanDocumentNo" class="spancolor"></span>
                                <asp:Label ID="lblDocumentNo" runat="server"></asp:Label>
                            </div>
                            <%--<div class="consume_name c_left">
                                <asp:Literal ID="Literal55" runat="server" Text="<%$ Resources:Resource, CUSTOMER_SEQUENCE_NO%>"></asp:Literal>
                                <span>*</span></div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtCustomerSequenceNo" MaxLength="7" Visible="false" runat="server"
                                    onblur="return IsSequenceNoExists(this)"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" TargetControlID="txtCustomerSequenceNo"
                                    runat="server" FilterType="Numbers">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanCustomerSequenceNo" class="spancolor"></span>
                                <asp:Label ID="lblCustomerSequenceNo" runat="server"></asp:Label>
                            </div>--%>
                            <div class="consume_name">
                                Employee code</div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtEmployeeCode" Visible="false" runat="server"></asp:TextBox>
                                <span id="spanEmployeeCode" class="spancolor"></span>
                                <asp:Label ID="lblEmployeeCode" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="pad_5 clear">
                        </div>
                        <h3>
                            <asp:Literal ID="litContactPhoneNos" runat="server" Text="<%$ Resources:Resource, CONTACT_PHONE_NUMBERS%>"></asp:Literal>
                        </h3>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="litHome" runat="server" Text="<%$ Resources:Resource, HOME%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtHome" Visible="false" MaxLength="20" runat="server"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" TargetControlID="txtHome"
                                    runat="server" FilterType="Numbers" FilterMode="ValidChars">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanHome" class="spancolor"></span>
                                <asp:Label ID="lblHome" runat="server"></asp:Label>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="litBusiness" runat="server" Text="<%$ Resources:Resource, BUSINESS%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtBussiness" Visible="false" runat="server" MaxLength="20"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" TargetControlID="txtBussiness"
                                    runat="server" FilterType="Numbers" FilterMode="ValidChars">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanBusiness" class="spancolor"></span>
                                <asp:Label ID="lblBussiness" runat="server"></asp:Label>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="litOthers" runat="server" Text="<%$ Resources:Resource, OTHERS%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtOthers" Visible="false" runat="server" MaxLength="20"></asp:TextBox>
                                <asp:Label ID="lblOthers" runat="server"></asp:Label>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" TargetControlID="txtOthers"
                                    runat="server" FilterType="Numbers" FilterMode="ValidChars">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanOthers" class="spancolor"></span>
                            </div>
                            <div class="pad_5 clear">
                            </div>
                        </div>
                        <div class="pad_5 clear">
                        </div>
                        <div class="consumer_feild">
                            <asp:UpdatePanel ID="upConsumer" runat="server">
                                <ContentTemplate>
                                    <div class="consume_name">
                                        State
                                    </div>
                                    <div class="consume_input">
                                        <%--<editable:EditableDropDownList ID="ddlState" Visible="false" runat="server" Sorted="false" AutoselectFirstItem="False"
                                    placeholder="Select State" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged">
                                </editable:EditableDropDownList>--%>
                                        <asp:DropDownList ID="ddlState" runat="server" Visible="false">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblState" runat="server"></asp:Label>
                                        <br />
                                        <div class="error">
                                            <span id="spanState" class="spancolor"></span>
                                        </div>
                                    </div>
                                    <%--<div class="consume_name c_left">
                                        District
                                    </div>
                                    <div class="consume_input">                                        
                                        <asp:DropDownList ID="ddlDistrict" Visible="false" runat="server" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlDistrict_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblDistrict" runat="server"></asp:Label>
                                        <br />
                                        <div class="error">
                                            <span id="spanDistrict" class="spancolor"></span>
                                        </div>
                                    </div>--%>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="pad_5 clear">
                        </div>
                        <h3>
                            <asp:Literal ID="litPostalAddress" runat="server" Text="<%$ Resources:Resource, POSTAL_ADDRESS%>"></asp:Literal></h3>
                        <div class="consumer_feild">
                            <div class="dummy1">
                                <span class="spancolorRed">*</span></div>
                            <div class="consume_name">
                                House No
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtHouseNo" runat="server" Visible="false" placeholder="House No"
                                    onblur="return TextBoxBlurValidationlblZeroAccepts(this,'spanHouseNo','House No')"></asp:TextBox>
                                <asp:Label ID="lblHouseNo" runat="server"></asp:Label>
                                <div class="dummy1">
                                    <span class="spancolorRed">*</span></div>
                                <div class="error">
                                    <span id="spanHouseNo" style="color: Red; margin-left: 77px;"></span>
                                </div>
                            </div>
                            <div class="consume_name">
                                Address 1
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtStreet" runat="server" Visible="false" placeholder="Address 1"
                                    onblur="return TextBoxBlurValidationlbl(this,'spanStreet','Address 1')"></asp:TextBox>
                                <asp:Label ID="lblStreet" runat="server"></asp:Label>
                                <div class="dummy1">
                                    <span class="spancolorRed">*</span></div>
                                <div class="error">
                                    <span id="spanStreet" style="color: Red; margin-left: 77px;"></span>
                                </div>
                            </div>
                            <div class="consume_name">
                                Address 2</div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtCity" runat="server" Visible="false" placeholder="Address 2"></asp:TextBox>
                                <asp:Label ID="lblCity" runat="server"></asp:Label>
                                <div class="error">
                                    <span id="spanCity" style="color: Red; margin-left: 115px;"></span>
                                </div>
                            </div>
                            <div class="clear pad_5">
                            </div>
                            <div class="consume_name">
                                LandMark
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtLandMark" runat="server" Visible="false" placeholder="LandMark"></asp:TextBox>
                                <asp:Label ID="lblLandMark" runat="server"></asp:Label>
                                <div class="error">
                                    <span id="spanLandMark" style="color: Red; margin-left: 77px;"></span>
                                </div>
                            </div>
                            <%--  <div class="text_box">
                                <asp:TextBox ID="txtLineNo" runat="server" placeholder="Line No 1" onblur="return TextBoxBlurValidationlbl(this,'spanLineNo','Line No')"></asp:TextBox><br />
                                <div class="error">
                                    <span id="spanLineNo" class="spancolor"></span>
                                </div>
                            </div>--%>
                            <div class="consume_name">
                                Details
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtPostalDetails" Visible="false" TextMode="MultiLine" runat="server"
                                    placeholder="Details"></asp:TextBox>
                                <asp:Label ID="lblPostalDetails" runat="server"></asp:Label>
                                <div class="error">
                                    <span id="spanPostDetails" class="spancolor"></span>
                                </div>
                            </div>
                            <div class="consume_name">
                                Zip Code</div>
                            <div class="consume_input" style="text-align: left;">
                                <asp:TextBox ID="txtZipCode" Visible="false" runat="server" placeholder="Zip Code"></asp:TextBox>
                                <asp:Label ID="lblZipCode" runat="server"></asp:Label>
                                <div class="error">
                                    <span id="spanZipCode" class="spancolor"></span>
                                </div>
                            </div>
                        </div>
                        <%--<div class="consumer_feild" style="margin-left: 13px;">
                    Copy this address to service address
                    <input id="rbtnPostalAddress" name="Address" type="radio" onclick="CopyAddress(true)" />
                    Yes
                    <input id="rbtnServiceAddress" name="Address" type="radio" onclick="return CopyAddress(false)" />
                    No
                </div>--%>
                        <div class="clear pad_5">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="litIdentityType" runat="server" Text="<%$ Resources:Resource, IDENTITY_TYPE%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <%--<editable:EditableDropDownList ID="ddlIdentityType" Visible="false" runat="server" Sorted="false"
                            placeholder="Select IdentityType" AutoselectFirstItem="False">
                        </editable:EditableDropDownList>--%>
                                <asp:DropDownList ID="ddlIdentityType" Visible="false" runat="server">
                                </asp:DropDownList>
                                <asp:Label ID="lblIdentityType" runat="server" Text="--"></asp:Label>
                                <%--<div class="dummy1">
                                    <span class="spancolorRed">*</span></div>--%>
                                <div class="error">
                                    <span id="spanIdentityType" class="spancolor"></span>
                                </div>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="litIdentityNo" runat="server" Text="<%$ Resources:Resource, IDENTITY_NUMBER%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtIdentityNumber" runat="server" Visible="false"></asp:TextBox>
                                <asp:Label ID="lblIdentityNumber" runat="server"></asp:Label>
                                <%-- <div class="dummy1">
                                    <span class="spancolorRed">*</span></div>--%>
                                <div class="error">
                                    <span id="spanIdentityNo" class="spancolor"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <%--  End content--%>
        <%----------------------------------------------------------Step 2----------------------------------------- --%>
        <div id="step-2" style="height: 604px;">
            <asp:UpdatePanel ID="upnlStep2" runat="server">
                <ContentTemplate>
                    <h2 class="StepTitle">
                        <asp:Literal ID="litServiceDetails" runat="server" Text="<%$ Resources:Resource, SERVICE_DETAILS%>"></asp:Literal>
                        <asp:LinkButton ID="lbtnStep2Edit" Visible="false" Style="float: right" runat="server"
                            OnClick="lbtnStep2Edit_Click" Text="<%$ Resources:Resource, EDIT%>"></asp:LinkButton>
                        <asp:LinkButton ID="lbtnStep2Cancel" Style="float: right" Visible="false" OnClick="lbtnStep2Cancel_Click"
                            runat="server" Text="<%$ Resources:Resource, CANCEL%>"></asp:LinkButton>&nbsp
                        &nbsp &nbsp &nbsp
                        <asp:LinkButton ID="lbtnStep2Update" Style="float: right; padding-right: 15px;" Visible="false"
                            runat="server" Text="<%$ Resources:Resource, UPDATE_EDIT%>" OnClientClick="return Step2Update();"
                            OnClick="lbtnStep2Update_Click"></asp:LinkButton></h2>
                    <div class="consumer_total">
                        <div class="pad_5">
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div class="consumer_feild">
                                    <div class="consume_name">
                                        <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal></div>
                                    <div class="consume_input">
                                        <%--<editable:EditableDropDownList ID="ddlBusinessUnit" Visible="false" runat="server" Sorted="false"
                                    placeholder="Select Business Unit" AutoselectFirstItem="False" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlBusinessUnit_SelectedIndexChanged">
                                </editable:EditableDropDownList>--%>
                                        <asp:DropDownList ID="ddlBusinessUnit" Visible="false" runat="server" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlBusinessUnit_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblBusinessUnit" runat="server"></asp:Label>
                                        <div class="error">
                                            <span id="spanBusinessUnit" class="spancolor"></span>
                                        </div>
                                    </div>
                                    <div class="consume_name">
                                        <asp:Literal ID="litUnderTaking" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal>
                                    </div>
                                    <div class="consume_input">
                                        <%--<editable:EditableDropDownList ID="ddlServiceUnit" Visible="false" runat="server" AutoPostBack="true"
                                    placeholder="Select Service Unit" Sorted="false" AutoselectFirstItem="False"
                                    OnSelectedIndexChanged="ddlServiceUnit_SelectedIndexChanged">
                                </editable:EditableDropDownList>--%>
                                        <asp:DropDownList ID="ddlServiceUnit" Visible="false" runat="server" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlServiceUnit_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblServiceUnit" runat="server"></asp:Label>
                                        <div class="error">
                                            <span id="spanServiceUnit" class="spancolor"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="consumer_feild">
                                    <div class="consume_name">
                                        <asp:Literal ID="litServiceCenter" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal>
                                    </div>
                                    <div class="consume_input">
                                        <%--<editable:EditableDropDownList ID="ddlServiceCenter" Visible="false" runat="server" Sorted="false"
                                    placeholder="Select Service Center" AutoselectFirstItem="False" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlServiceCenter_SelectedIndexChanged">
                                </editable:EditableDropDownList>--%>
                                        <asp:DropDownList ID="ddlServiceCenter" Visible="false" runat="server" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlServiceCenter_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblServiceCenter" runat="server"></asp:Label>
                                        <div class="error">
                                            <span id="spanServiceCenter" class="spancolor"></span>
                                        </div>
                                    </div>
                                    <div class="pad_5 clear ">
                                    </div>
                                    <div class="consume_name">
                                        <asp:Literal ID="litBookNumber" runat="server" Text="<%$ Resources:Resource, BOOK_NUMBER%>"></asp:Literal></div>
                                    <div class="consume_input">
                                        <%--<editable:EditableDropDownList ID="ddlBookNumber" Visible="false" runat="server" Sorted="false" AutoselectFirstItem="False"
                                    placeholder="Select Book Number">
                                </editable:EditableDropDownList>--%>
                                        <asp:DropDownList ID="ddlBookNumber" Visible="false" runat="server">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblBookNumber" runat="server"></asp:Label>
                                        <div class="error">
                                            <span id="spanBookNumber" class="spancolor"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="pad_5 clear ">
                                </div>
                                <h3>
                                    <asp:Literal ID="Literal35" runat="server" Text="<%$ Resources:Resource, TRANSFORMER_DETAILS%>"></asp:Literal></h3>
                                <div class="consumer_feild">
                                    <div class="consume_name">
                                        <asp:Literal ID="Literal36" runat="server" Text="<%$ Resources:Resource, INJECTION_SUBSTATION%>"></asp:Literal>
                                    </div>
                                    <div class="consume_input">
                                        <asp:DropDownList ID="ddlInjectionSubStation" Visible="false" runat="server" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlInjectionSubStation_SelectedIndexChanged">
                                            <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:Label ID="lblInjectionSubStation" runat="server"></asp:Label>
                                    </div>
                                    <div class="consume_name">
                                        <asp:Literal ID="Literal37" runat="server" Text="<%$ Resources:Resource, FEEDER%>"></asp:Literal>
                                    </div>
                                    <div class="consume_input">
                                        <%--<editable:EditableDropDownList ID="ddlFeeder" Visible="false" runat="server" Sorted="false" AutoselectFirstItem="False"
                                            placeholder="Select Feeder" AutoPostBack="true" OnSelectedIndexChanged="ddlFeeder_SelectedIndexChanged">
                                        </editable:EditableDropDownList>--%>
                                        <asp:DropDownList ID="ddlFeeder" Visible="false" runat="server" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlFeeder_SelectedIndexChanged">
                                            <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:Label ID="lblFeeder" runat="server"></asp:Label>
                                    </div>
                                    <div class="pad_5 clear">
                                    </div>
                                    <div class="consume_name">
                                        <asp:Literal ID="Literal38" runat="server" Text="<%$ Resources:Resource, TRANSFORMER%>"></asp:Literal>
                                    </div>
                                    <div class="consume_input">
                                        <%--<editable:EditableDropDownList ID="ddlTransformer" Visible="false" runat="server" Sorted="false"
                                            placeholder="Select Transformer" AutoselectFirstItem="False" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlTransformer_SelectedIndexChanged">
                                        </editable:EditableDropDownList>--%>
                                        <asp:DropDownList ID="ddlTransformer" Visible="false" runat="server" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlTransformer_SelectedIndexChanged">
                                            <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:Label ID="lblTransformer" runat="server"></asp:Label>
                                    </div>
                                    <div class="consume_name">
                                        <asp:Literal ID="Literal39" runat="server" Text="<%$ Resources:Resource, POLE%>"></asp:Literal>
                                    </div>
                                    <div class="consume_input">
                                        <%--<editable:EditableDropDownList ID="ddlPole" Visible="false" runat="server" Sorted="false" AutoselectFirstItem="False"
                                            placeholder="Select Pole">
                                        </editable:EditableDropDownList>--%>
                                        <asp:DropDownList ID="ddlPole" Visible="false" runat="server">
                                            <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:Label ID="lblPole" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="pad_5 clear">
                        </div>
                        <h3>
                            <asp:Literal ID="litServiceAddress" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Literal></h3>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                House No
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtServiceHouseNo" runat="server" Visible="false" placeholder="House No"
                                    onblur="return TextBoxBlurValidationlblZeroAccepts(this,'spanServiceHouseNo','Service House No')"></asp:TextBox>
                                <asp:Label ID="lblServiceHouseNo" runat="server"></asp:Label>
                                <div class="dummy2">
                                    <span class="spancolorRed">*</span></div>
                                <div class="error">
                                    <span id="spanServiceHouseNo" style="color: Red; margin-left: 77px;"></span>
                                </div>
                            </div>
                            <div class="consume_name">
                                Address 1
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtServiceStreet" runat="server" Visible="false" placeholder="Address 1"
                                    onblur="return TextBoxBlurValidationlbl(this,'spanServiceStreet','Service Address 1')"></asp:TextBox>
                                <asp:Label ID="lblServiceStreet" runat="server"></asp:Label>
                                <div class="dummy2">
                                    <span class="spancolorRed">*</span></div>
                                <br />
                                <div class="error">
                                    <span id="spanServiceStreet" style="color: Red; margin-left: 77px;"></span>
                                </div>
                            </div>
                            <div class="consume_name">
                                Address 2
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtServiceCity" placeholder="Address 2" Visible="false" runat="server"></asp:TextBox>
                                <asp:Label ID="lblServiceCity" runat="server"></asp:Label>
                                <div class="error">
                                    <span id="spanServiceCity" style="color: Red; margin-left: 115px;"></span>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="consume_name">
                                Land Mark
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtServiceLandMark" runat="server" Visible="false" placeholder="LandMark"
                                    onblur="return TextBoxBlurValidationlbl(this,'spanServiceLandMark','Service LandMark')"></asp:TextBox>
                                <asp:Label ID="lblServiceLandMark" runat="server"></asp:Label>
                                <div class="error">
                                    <span id="spanServiceLandMark" style="color: Red; margin-left: 77px;"></span>
                                </div>
                            </div>
                            <%-- <div class="text_box">
                                        <asp:TextBox ID="txtServiceLineNo" runat="server" placeholder="Line No 1" onblur="return TextBoxBlurValidationlbl(this,'spanServiceLineNo','Service Line No')"></asp:TextBox><br />
                                        <div class="error">
                                            <span id="spanServiceLineNo" class="spancolor"></span>
                                        </div>
                                    </div>--%>
                            <div class="consume_name">
                                Details
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtServiceDetails" TextMode="MultiLine" Visible="false" runat="server"
                                    placeholder="Details"></asp:TextBox>
                                <asp:Label ID="lblServiceDetails" runat="server"></asp:Label>
                                <div class="error">
                                    <span id="spanServiceDetails" class="spancolor"></span>
                                </div>
                            </div>
                            <div class="consume_name">
                                Zip Code
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtServiceZipCode" runat="server" Visible="false" placeholder="ZipCode"></asp:TextBox>
                                <asp:Label ID="lblServiceZipCode" runat="server"></asp:Label>
                                <div class="error">
                                    <span id="spanServiceZipCode" class="spancolor"></span>
                                </div>
                            </div>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="litMobileNo" runat="server" Text="<%$ Resources:Resource, MOBILE_NO%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtMobileNo" runat="server" Visible="false" MaxLength="20" onblur="return ValidateMobileNo()"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtMobileNo"
                                    runat="server" FilterType="Numbers" FilterMode="ValidChars">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanMobileNo" class="spancolor"></span>
                                <asp:Label ID="lblMobileNo" runat="server"></asp:Label>
                                <div class="dummy2">
                                    <span class="spancolorRed">*</span></div>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal51" runat="server" Text="<%$ Resources:Resource, EMAIL_ID%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtServiceEmailId" runat="server" Visible="false" placeholder="Email Id"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="txtServiceEmailId"
                                    ValidChars="@._" FilterType="LowercaseLetters,UppercaseLetters,Numbers,Custom">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanemailadd" class="spancolor"></span>
                                <asp:Label ID="lblServiceEmailId" runat="server"></asp:Label>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal47" runat="server" Text="<%$ Resources:Resource, LATITUDE%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtLatitude" Visible="false" runat="server"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" TargetControlID="txtLatitude"
                                    runat="server" FilterType="Numbers,Custom" FilterMode="ValidChars" ValidChars=".">
                                </asp:FilteredTextBoxExtender>
                                <asp:Label ID="lblLatitude" runat="server"></asp:Label>
                            </div>
                            <div class="pad_5 clear">
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="litLongitude" runat="server" Text="<%$ Resources:Resource, LONGITUDE%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtLongitude" Visible="false" runat="server"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" TargetControlID="txtLongitude"
                                    runat="server" FilterType="Custom,Numbers" FilterMode="ValidChars" ValidChars=".">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanLongitude" class="spancolor"></span>
                                <asp:Label ID="lblLongitude" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="pad_5">
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, ROUTE_NO%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <asp:DropDownList ID="ddlRouteNo" Visible="false" runat="server">
                                </asp:DropDownList>
                                <span id="spanRouteNo" class="spancolor"></span>
                                <asp:Label ID="lblRouteNo" runat="server"></asp:Label>
                                <div class="dummy2">
                                    <span class="spancolorRed">*</span></div>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="litRouteNo" runat="server" Text="<%$ Resources:Resource, ROUTE_SEQUENCE_NO%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtRouteSequenceNo" runat="server" Visible="false" MaxLength="50"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" TargetControlID="txtRouteSequenceNo"
                                    runat="server" FilterType="Numbers" FilterMode="ValidChars">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanRouteSequenceNo" class="spancolor"></span>
                                <asp:Label ID="lblRouteSequenceNo" runat="server"></asp:Label>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal40" runat="server" Text="<%$ Resources:Resource, NEIGHBOR_ACCOUNT_NO%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtneighbor" Visible="false" runat="server" MaxLength="14" onblur="return IsNeighborAccountNoExists(this)"></asp:TextBox>
                                <span id="spanneighbor" class="spancolor"></span>
                                <asp:Label ID="lblneighbor" runat="server"></asp:Label>
                            </div>
                            <%--  End content--%>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <%----------------------------------------------------------Step 3----------------------------------------- --%>
        <div id="step-3" style="height: 930px;">
            <asp:UpdatePanel ID="upnlstep3" runat="server">
                <ContentTemplate>
                    <div class="tariff_Details" style="position: absolute; right: 4px; z-index: 1000;">
                        <div id="divTariff" runat="server" visible="false">
                            Tariff Type :
                            <asp:Label ID="lblTariffType" runat="server" Text=""></asp:Label></div>
                        <asp:GridView ID="gvTariffDetails" AutoGenerateColumns="false" runat="server" Visible="true"
                            CssClass="charges_bind">
                            <Columns>
                                <asp:BoundField HeaderText="Charge" DataField="ChargeName" NullDisplayText="--" />
                                <asp:BoundField HeaderText="Amount" DataField="Amount" NullDisplayText="--" />
                                <asp:BoundField HeaderText="Tax" Visible="false" DataField="Tax" NullDisplayText="--" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <h2 class="StepTitle">
                        <asp:Literal ID="litAccountInformation" runat="server" Text="<%$ Resources:Resource, ACCOUNT_INFORMATION%>"></asp:Literal>
                        <asp:LinkButton ID="lbtnStep3Edit" Visible="false" Style="float: right" runat="server"
                            OnClientClick="return DocsVisible();" OnClick="lbtnStep3Edit_Click" Text="<%$ Resources:Resource, EDIT%>"></asp:LinkButton>
                        <asp:LinkButton ID="lbtnStep3Cancel" Style="float: right" Visible="false" OnClick="lbtnStep3Cancel_Click"
                            runat="server" OnClientClick="return DocsInVisible();" Text="<%$ Resources:Resource, CANCEL%>"></asp:LinkButton>&nbsp
                        &nbsp &nbsp &nbsp
                        <asp:LinkButton ID="lbtnStep3Update" Style="float: right; padding-right: 15px;" Visible="false"
                            runat="server" Text="<%$ Resources:Resource, UPDATE_EDIT%>" OnClientClick="return Step3Update();"
                            OnClick="lbtnStep3Update_Click"></asp:LinkButton>
                    </h2>
                    <div class="consumer_total">
                        <div class="pad_5">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="litTariff" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                            </div>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                    <div class="consume_input">
                                        <%--<editable:EditableDropDownList ID="ddlTariff" Visible="false" runat="server" Sorted="false" AutoPostBack="true"
                                    AutoselectFirstItem="False" placeholder="Select Tariff" OnSelectedIndexChanged="ddlTariff_SelectedIndexChanged">
                                </editable:EditableDropDownList>--%>
                                        <asp:DropDownList ID="ddlTariff" Visible="false" runat="server" AutoPostBack="true"
                                            Enabled="false" OnSelectedIndexChanged="ddlTariff_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblTariff" runat="server"></asp:Label>
                                        <div class="error">
                                            <span id="spanTariff" class="spancolor"></span>
                                        </div>
                                        <div class="dummy3">
                                            <span class="spancolorRed">*</span></div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div class="consume_name">
                                <asp:Literal ID="Literal42" runat="server" Text="<%$ Resources:Resource, MULTIPLICATION_FACTOR%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <asp:TextBox ID="txtTariffMultiplier" Visible="false" runat="server"></asp:TextBox>
                                <asp:Label ID="lblTariffMultiplier" runat="server"></asp:Label></div>
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="litConnectionType" runat="server" Text="<%$ Resources:Resource, ACCOUNT_TYPE%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <%--<editable:EditableDropDownList ID="ddlAccountType" Visible="false" runat="server" Sorted="false"
                            placeholder="Select Account Type" AutoselectFirstItem="False">
                        </editable:EditableDropDownList>--%>
                                <asp:DropDownList ID="ddlAccountType" Visible="false" runat="server">
                                </asp:DropDownList>
                                <asp:Label ID="lblAccountType" runat="server"></asp:Label>
                                <div class="error">
                                    <span id="spanAccountType" class="spancolor"></span>
                                </div>
                                <div class="dummy3">
                                    <span class="spancolorRed">*</span></div>
                            </div>
                            <div class="pad_5 clear">
                            </div>
                            <div class="consumer_feild">
                                <div class="consume_name">
                                    <asp:Literal ID="Literal48" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                </div>
                                <div class="consume_input">
                                    <asp:TextBox ID="txtOldAccountNo" MaxLength="14" Visible="false" onblur="return IsOldAccountNoExists(this)"
                                        runat="server"></asp:TextBox>
                                    <asp:Label ID="lblOldAccountNo" runat="server"></asp:Label>
                                    <span id="spanOldAccountNo" class="spancolor"></span>
                                </div>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="litApplicationDate" runat="server" Text="<%$ Resources:Resource, APPLICATION_DATE%>"></asp:Literal>
                            </div>
                            <div class="consume_input">
                                <asp:TextBox runat="server" Style="width: 130px;" CssClass="calender_total" Visible="false"
                                    ID="txtApplicationDate" AutoComplete="off"></asp:TextBox>
                                <asp:Label ID="lblApplicationDate" runat="server"></asp:Label>
                                <asp:Image ID="imgDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                    vertical-align: middle" AlternateText="Calender" runat="server" />
                                <asp:FilteredTextBoxExtender ID="ftxtApplicationDate" runat="server" TargetControlID="txtApplicationDate"
                                    FilterType="Custom,Numbers" ValidChars="/">
                                </asp:FilteredTextBoxExtender>
                                <div class="error">
                                    <span id="spanApplicationDate" class="spancolor"></span>
                                </div>
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="litReadCode" runat="server" Text="<%$ Resources:Resource, READCODE%>"></asp:Literal></div>
                            <div class="consume_input">
                                <asp:UpdatePanel ID="upnlReadCode" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlReadCode" AutoPostBack="true" OnSelectedIndexChanged="ddlReadCode_SelectedIndexChanged"
                                            Visible="false" runat="server">
                                            <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:Label ID="lblReadCode" runat="server"></asp:Label>
                                        <div class="error">
                                            <span id="spanReadCode" class="spancolor"></span>
                                        </div>
                                        <div class="dummy3">
                                            <span class="spancolorRed">*</span></div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="pad_5 clear">
                            </div>
                            <div class="consumer_feild">
                                <div class="consume_name">
                                    <asp:Literal ID="litInstitueCode" runat="server" Text="<%$ Resources:Resource, ORGANIZATION_INSTITUTION_CODE%>"></asp:Literal>
                                </div>
                                <div class="consume_input">
                                    <asp:TextBox runat="server" Visible="false" ID="txtOrganizationCode"></asp:TextBox>
                                    <asp:Label ID="lblOrganizationCode" runat="server"></asp:Label></div>
                                <div class="consume_name">
                                    <asp:Literal ID="litCustomerType" runat="server" Text="<%$ Resources:Resource, CUSTOMER_TYPE%>"></asp:Literal>
                                </div>
                                <div class="consume_input">
                                    <%--<editable:EditableDropDownList ID="ddlCustomerType" Visible="false" runat="server" Sorted="false"
                            placeholder="Select Customer Type" AutoselectFirstItem="False">
                        </editable:EditableDropDownList>--%>
                                    <asp:DropDownList ID="ddlCustomerType" Visible="false" runat="server" onchange="DropDownlistOnChangelbl(this,'spanCustomerType','Customer Type')">
                                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Label ID="lblCustomerType" runat="server"></asp:Label>
                                    <div class="error">
                                        <span id="spanCustomerType" class="spancolor"></span>
                                    </div>
                                    <div class="dummy3">
                                        <span class="spancolorRed">*</span></div>
                                </div>
                                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                    <ContentTemplate>
                                        <div id="divMinReading" visible="false" runat="server">
                                            <div class="consume_name">
                                                <asp:Literal ID="Literal34" runat="server" Text="<%$ Resources:Resource, MIN_READING%>"></asp:Literal>
                                            </div>
                                            <div class="consume_input">
                                                <asp:Label ID="lblMinReading" runat="server"></asp:Label>
                                                <asp:TextBox runat="server" ID="txtMinReading" Visible="false" onblur="return TextBoxBlurValidationlbl(this,'spanMinReading','Min Reading')"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" TargetControlID="txtMinReading"
                                                    FilterType="Custom,Numbers">
                                                </asp:FilteredTextBoxExtender>
                                                <div class="error">
                                                    <span id="spanMinReading" class="spancolor"></span>
                                                </div>
                                                <div class="dummy3">
                                                    <span class="spancolorRed">*</span></div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div class="pad_5 clear">
                                </div>
                                <div class="consume_name">
                                    <asp:Literal ID="Literal52" runat="server" Text="<%$ Resources:Resource, IS_EMBASSY_CUSTOMER%>"></asp:Literal>
                                </div>
                                <div class="consume_check">
                                    <asp:CheckBox ID="chkIsEmbassyCustomer" OnCheckedChanged="chkIsEmbassyCustomer_Checked"
                                        AutoPostBack="true" runat="server" Enabled="false" />
                                    <asp:Label ID="lblIsEmbassyCustomer" runat="server"></asp:Label>
                                </div>
                                <div id="divEmbassyCode" visible="false" runat="server">
                                    <div class="consume_name" style="width: 257px;">
                                        <asp:Literal ID="Literal53" runat="server" Text="<%$ Resources:Resource, EMBASSY_CODE%>"></asp:Literal>
                                    </div>
                                    <div class="consume_input">
                                        <asp:TextBox runat="server" Visible="false" ID="txtEmbassyCode" onblur="return TextBoxBlurValidationlbl(this,'spanEmbassyCode','Embassy Code')"></asp:TextBox>
                                        <span id="spanEmbassyCode" class="spancolor"></span>
                                        <asp:Label ID="lblEmbassyChecked" Visible="false" runat="server"></asp:Label>
                                        <asp:Label ID="lblEmbassyCode" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="clear pad_5">
                            </div>
                            <div class="consumer_feild">
                                <div class="consume_name">
                                    <asp:Literal ID="Literal54" runat="server" Text="<%$ Resources:Resource, VIP_CUSTOMER%>"></asp:Literal>
                                </div>
                                <div class="consume_check">
                                    <asp:CheckBox ID="chkIsVipCustomer" Enabled="false" runat="server" /></div>
                                <asp:Label ID="lblIsVipCustomer" runat="server"></asp:Label>
                                <div class="pad_5 clear">
                                </div>
                                <div id="divMeterInformation" runat="server">
                                    <h2 class="StepTitle">
                                        <asp:Literal ID="litMeterInformation" runat="server" Text="<%$ Resources:Resource, METER_INFORMATION%>"></asp:Literal>
                                    </h2>
                                    <div class="pad_5">
                                    </div>
                                    <div class="consumer_feild">
                                        <div class="consume_name">
                                            <asp:Literal ID="litConnectionType2" runat="server" Text="<%$ Resources:Resource, METER_TYPE%>"></asp:Literal></div>
                                        <div class="consume_input">
                                            <%--<editable:EditableDropDownList ID="ddlMeterType" Visible="false" runat="server" Sorted="false" placeholder="Select Meter Type"
                                AutoselectFirstItem="False">
                            </editable:EditableDropDownList>--%>
                                            <asp:DropDownList ID="ddlMeterType" Visible="false" runat="server">
                                                <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                            &nbsp&nbsp
                                            <asp:Label ID="lblMeterType" runat="server"></asp:Label>
                                            <div class="error">
                                                <span id="spanMeterType" class="spancolor"></span>
                                            </div>
                                            <div class="dummy3">
                                                <span class="spancolorRed">*</span></div>
                                        </div>
                                        <div class="consume_name">
                                            <asp:Literal ID="litPhase" runat="server" Text="<%$ Resources:Resource, PHASE%>"></asp:Literal>
                                        </div>
                                        <div class="consume_input">
                                            <%--<editable:EditableDropDownList ID="ddlPhase" Visible="false" runat="server" Sorted="false" AutoselectFirstItem="False"
                                placeholder="Select Phase">
                            </editable:EditableDropDownList>--%>
                                            <asp:DropDownList ID="ddlPhase" Visible="false" runat="server">
                                                <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:Label ID="lblPhase" runat="server"></asp:Label>
                                            <div class="error">
                                                <span id="spanPhase" class="spancolor"></span>
                                            </div>
                                            <div class="dummy3">
                                                <span class="spancolorRed">*</span></div>
                                        </div>
                                        <%--  <div class="consume_name">
                                    <asp:Literal ID="litMeterModel" runat="server" Text="<%$ Resources:Resource, METER_MODEL%>"></asp:Literal>
                                    <span>*</span></div>
                                <div class="fltl">
                                    <editable:EditableDropDownList ID="ddlMeterModel" runat="server" Sorted="false" AutoselectFirstItem="False"
                                        placeholder="Select Meter Model">
                                    </editable:EditableDropDownList>
                                    <div class="error">
                                        <span id="spanMeterModel" class="spancolor"></span>
                                    </div>
                                </div>--%>
                                        <div class="consume_name">
                                            <asp:Literal ID="litMeterNo" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Literal>
                                        </div>
                                        <div class="consume_input">
                                            <asp:TextBox ID="txtMeterNo" runat="server" Visible="false" onblur="return IsMeterNoExists(this)"></asp:TextBox>
                                            <span id="spanMeterNo" class="spancolor"></span>
                                            <asp:Label ID="lblMeterNo" runat="server"></asp:Label>
                                            <div class="dummy3">
                                                <span class="spancolorRed">*</span></div>
                                        </div>
                                    </div>
                                    <div class="clear pad_5">
                                    </div>
                                    <div class="consumer_feild">
                                        <div class="consume_name">
                                            <asp:Literal ID="litMeterSerialNo" runat="server" Text="<%$ Resources:Resource, METER_SERIAL_NUMBER%>"></asp:Literal>
                                        </div>
                                        <div class="consume_input">
                                            <asp:TextBox ID="txtMeeterSerialNo" runat="server" Visible="false" onblur="return IsMeterSerialNoExists(this)"></asp:TextBox>
                                            <span id="spanMeterSerialNo" class="spancolor"></span>&nbsp&nbsp
                                            <asp:Label ID="lblMeeterSerialNo" runat="server"></asp:Label>
                                        </div>
                                        <div class="consume_name">
                                            <asp:Literal ID="litConnectionDate" runat="server" Text="<%$ Resources:Resource, CONNECTION_DATE%>"></asp:Literal>
                                        </div>
                                        <div class="consume_input">
                                            <asp:TextBox Style="width: 134px;" ID="txtConnectionDate" CssClass="calender_total"
                                                Visible="false" runat="server" AutoComplete="off" onblur="return TextBoxBlurValidationlbl(this,'spanConnectionDate','Connection Date')"
                                                onchange="return TextBoxBlurValidationlbl(this,'spanConnectionDate','Connection Date')"></asp:TextBox>
                                            <asp:Label ID="lblConnectionDate" runat="server"></asp:Label>
                                            <asp:Image ID="imgDate2" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                                vertical-align: middle" AlternateText="Calender" runat="server" />
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtConnectionDate"
                                                FilterType="Custom,Numbers" ValidChars="/">
                                            </asp:FilteredTextBoxExtender>
                                            <div class="error">
                                                <span id="spanConnectionDate" class="spancolor"></span>
                                            </div>
                                            <div class="dummy3">
                                                <span class="spancolorRed">*</span></div>
                                        </div>
                                    </div>
                                    <div class="consumer_feild">
                                        <div class="consume_name">
                                            <asp:Literal ID="litMeterStatus" runat="server" Text="<%$ Resources:Resource, METER_STATUS%>"></asp:Literal>
                                        </div>
                                        <div class="consume_input">
                                            <%--<editable:EditableDropDownList ID="ddlMeeterStatus" Visible="false" runat="server" Sorted="false"
                                placeholder="Select Meter Status" AutoselectFirstItem="False">
                            </editable:EditableDropDownList>--%>
                                            <asp:DropDownList ID="ddlMeeterStatus" Visible="false" runat="server">
                                            </asp:DropDownList>
                                            &nbsp&nbsp
                                            <asp:Label ID="lblMeeterStatus" runat="server"></asp:Label>
                                            <div class="error">
                                                <span id="spanMeterStatus" class="spancolor"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear pad_5">
                                    </div>
                                    <div class="consumer_feild">
                                        <div class="consume_name">
                                            <asp:Literal ID="litConnectionReason" runat="server" Text="<%$ Resources:Resource, CONNECTION_REASON%>"></asp:Literal>
                                        </div>
                                        <div class="consume_input">
                                            <%--<editable:EditableDropDownList ID="ddlConnectionReason" runat="server" Sorted="false" Visible="false"
                                placeholder="Select ConnectionReason" AutoselectFirstItem="False">
                                <asp:ListItem Text="New" Value="New"></asp:ListItem>
                                <asp:ListItem Text="Disconnected" Value="Disconnected"></asp:ListItem>
                                <asp:ListItem Text="ReplaceMent" Value="ReplaceMent"></asp:ListItem>
                            </editable:EditableDropDownList>--%>
                                            <asp:DropDownList ID="ddlConnectionReason" runat="server" Visible="false">
                                                <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:Label ID="lblConnectionReason" runat="server"></asp:Label>
                                            <div class="error">
                                                <span id="spanConnectionReason" class="spancolor"></span>
                                            </div>
                                            <div class="dummy3">
                                                <span class="spancolorRed">*</span></div>
                                        </div>
                                        <div class="consume_name">
                                            <asp:Literal ID="litInitialReading" runat="server" Text="<%$ Resources:Resource, INITIAL_READING%>"></asp:Literal>
                                        </div>
                                        <div class="consume_input">
                                            <%--onblur="return TextBoxValidationlblZeroAccepts(this,'spanInitialReading','Initial Reading')"--%>
                                            <asp:TextBox ID="txtInitialReading" runat="server" Enabled="false" Visible="false"
                                                onblur="return MeterDials(this)"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" TargetControlID="txtInitialReading"
                                                runat="server" FilterType="Numbers,Custom" FilterMode="ValidChars" ValidChars=".">
                                            </asp:FilteredTextBoxExtender>
                                            <asp:Label ID="lblInitialReading" runat="server"></asp:Label>
                                            <span id="spanInitialReading" class="spancolor"></span>
                                            <div class="dummy3">
                                                <span class="spancolorRed">*</span></div>
                                        </div>
                                        <%--<div style="width: 221px;" class="consume_name">
                            <asp:Literal ID="litCurrentReading" runat="server" Text="<%$ Resources:Resource, CURRENT_READING%>"></asp:Literal>
                            <span>*</span></div>
                        <div class="consume_input">
                            <asp:TextBox ID="txtCurrentReading" Visible="false" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanCurrentReading','Current Reading')"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" TargetControlID="txtCurrentReading"
                                runat="server" FilterType="Numbers,Custom" FilterMode="ValidChars" ValidChars=".">
                            </asp:FilteredTextBoxExtender>
                            <asp:Label ID="lblCurrentReading" runat="server"></asp:Label>
                            <span id="spanCurrentReading" class="spancolor"></span>
                        </div>--%>
                                        <%--<div class="consume_name">
                                            <asp:Literal ID="litReadDate" runat="server" Text="<%$ Resources:Resource, SETUP_DATE%>"></asp:Literal>
                                        </div>
                                        <div class="consume_input" style="width: 198px;">
                                            <asp:TextBox ID="txtSetupDate" runat="server" Visible="false" AutoComplete="off"
                                                CssClass="calender_total" onblur="return TextBoxBlurValidationlbl(this,'spanSetupDate','Setup Date')"
                                                onchange="return TextBoxBlurValidationlbl(this,'spanSetupDate','Setup Date')"></asp:TextBox>
                                            <asp:Label ID="lblSetupDate" runat="server"></asp:Label>
                                            <asp:Image ID="imgDate3" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                                vertical-align: middle" AlternateText="Calender" runat="server" />
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="txtSetupDate"
                                                FilterType="Custom,Numbers" ValidChars="/">
                                            </asp:FilteredTextBoxExtender>
                                            <div class="error">
                                                <span id="spanSetupDate" class="spancolor"></span>
                                            </div>
                                            <div class="dummy3">
                                                <span class="spancolorRed">*</span></div>
                                        </div>--%>
                                    </div>
                                    <div class="clear pad_5">
                                    </div>
                                    <div class="consumer_feild">
                                        <div class="consume_name">
                                            <asp:Literal ID="litAverageReading" runat="server" Text="<%$ Resources:Resource, AVERAGE_READING%>"></asp:Literal>
                                        </div>
                                        <div class="consume_input">
                                            <asp:TextBox ID="txtAverageReading" Visible="false" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanAverageReading','Average Reading')"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" TargetControlID="txtAverageReading"
                                                runat="server" FilterType="Numbers,Custom" FilterMode="ValidChars" ValidChars=".">
                                            </asp:FilteredTextBoxExtender>
                                            <asp:Label ID="lblAverageReading" runat="server"></asp:Label>
                                            <span id="spanAverageReading" class="spancolor"></span>
                                            <div class="dummy3">
                                                <span class="spancolorRed">*</span></div>
                                        </div>
                                        <%--<div style="width: 221px;" class="consume_name">
                            <asp:Literal ID="litReadDate" runat="server" Text="<%$ Resources:Resource, SETUP_DATE%>"></asp:Literal>
                            <span>*</span></div>
                        <div class="consume_input">
                            <asp:TextBox ID="txtSetupDate" runat="server" Visible="false" AutoComplete="off"
                                CssClass="calender_total" onblur="return TextBoxBlurValidationlbl(this,'spanSetupDate','Setup Date')"></asp:TextBox>
                            <asp:Label ID="lblSetupDate" runat="server"></asp:Label>
                            <asp:Image ID="imgDate3" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                vertical-align: middle" AlternateText="Calender" runat="server" />
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="txtSetupDate"
                                FilterType="Custom,Numbers" ValidChars="/">
                            </asp:FilteredTextBoxExtender>
                            <div class="error">
                                <span id="spanSetupDate" class="spancolor"></span>
                            </div>
                        </div>--%>
                                        <div class="consume_name">
                                            <asp:Literal ID="litCertifiedBy" runat="server" Text="<%$ Resources:Resource, CERTIFIED_BY%>"></asp:Literal>
                                        </div>
                                        <div class="consume_input" style="width: 198px;">
                                            <%--<editable:EditableDropDownList ID="ddlCertifiedBy" runat="server" Sorted="false" Visible="false"
                                placeholder="Select CertifiedBy" AutoselectFirstItem="False">
                            </editable:EditableDropDownList>--%>
                                            <asp:DropDownList ID="ddlCertifiedBy" runat="server" Visible="false">
                                                <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                            &nbsp&nbsp&nbsp&nbsp
                                            <asp:Label ID="lblCertifiedBy" runat="server"></asp:Label>
                                            <div class="error">
                                                <span id="spanCertifiedBy" class="spancolor"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear pad_5">
                                    </div>
                                    <asp:UpdatePanel ID="pnlrbl" runat="server">
                                        <ContentTemplate>
                                            <div class="consumer_feild">
                                                <div class="consume_name">
                                                    <asp:Literal ID="Literal43" runat="server" Text="<%$ Resources:Resource, APPLICATION_PROCCESED_BY%>"></asp:Literal>
                                                </div>
                                                <div class="consume_input consume_radio">
                                                    <asp:RadioButtonList ID="rblApplicationProccessedBy" AutoPostBack="true" RepeatDirection="Horizontal"
                                                        OnSelectedIndexChanged="rblApplicationProccessedBy_SelectedIndexChanged" Enable="false"
                                                        runat="server">
                                                    </asp:RadioButtonList>
                                                    <asp:Label ID="lblApplicationProccessedBy" Visible="false" runat="server"></asp:Label>
                                                    <%--                                    <asp:RadioButton ID="rbtnBEDC" Text="BEDC" GroupName="AppBy" runat="server" Checked="True"
                                        onclick="return InstalledBy(false)" />
                                    <asp:RadioButton ID="rbtnOthers" Text="Others" GroupName="AppBy" runat="server" onclick="return InstalledBy(true)" />
                                                    --%>
                                                    <span id="spanApplicationProcessedBy" class="spancolor"></span>
                                                </div>
                                            </div>
                                            <div class="clear pad_5">
                                            </div>
                                            <div id="divBEDCApplication" runat="server">
                                                <div class="consumer_feild">
                                                    <div class="consume_name">
                                                        <asp:Literal ID="Literal44" runat="server" Text="<%$ Resources:Resource, INSTALLED_BY%>"></asp:Literal>
                                                    </div>
                                                    <div class="consume_input">
                                                        <%--<editable:EditableDropDownList ID="ddlInstalledBy1" placeholder="Select InstalledBy" Visible="false"
                                    runat="server" Sorted="false" AutoselectFirstItem="False">
                                </editable:EditableDropDownList>--%>
                                                        <asp:DropDownList ID="ddlInstalledBy" runat="server" Visible="false">
                                                            <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <span style="color: Red; display: block;" id="spanInstalledBy"></span>
                                                        <asp:Label ID="lblInstalledBy" runat="server"></asp:Label>
                                                        <div class="dummy3">
                                                            <span class="spancolorRed">*</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divApplicationByOthers" runat="server" visible="false">
                                                <div class="consumer_feild">
                                                    <div class="consume_name">
                                                        <asp:Literal ID="Literal45" runat="server" Text="<%$ Resources:Resource, AGENCY_NAME%>"></asp:Literal>
                                                    </div>
                                                    <div class="consume_input">
                                                        <%--<editable:EditableDropDownList ID="ddlAgencyName" runat="server" Sorted="false" AutoselectFirstItem="False"
                                    placeholder="Select Agency Name" Visible="false">
                                </editable:EditableDropDownList>--%>
                                                        <asp:DropDownList ID="ddlAgencyName" Visible="false" runat="server">
                                                            <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <span style="color: Red; display: block;" id="spanAgency"></span>
                                                        <asp:Label ID="lblAgencyName" runat="server"></asp:Label>
                                                        <div class="dummy3">
                                                            <span class="spancolorRed">*</span></div>
                                                    </div>
                                                </div>
                                                <div class="clear pad_5">
                                                </div>
                                                <div class="consumer_feild">
                                                    <div class="consume_name">
                                                        Contact Person Name</div>
                                                    <div class="consume_input">
                                                        <asp:TextBox ID="txtContactPersonName" Visible="false" runat="server"></asp:TextBox>
                                                        <asp:Label ID="lblContactPersonName" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="clear pad_5">
                                                </div>
                                                <div class="consumer_feild">
                                                    <div class="consume_name">
                                                        <asp:Literal ID="Literal46" runat="server" Text="<%$ Resources:Resource, DETAILS%>"></asp:Literal>
                                                    </div>
                                                    <div class="consume_input">
                                                        <asp:TextBox ID="txtAgencyDetails" runat="server" Visible="false" TextMode="MultiLine"></asp:TextBox>
                                                        <asp:Label ID="lblAgencyDetails" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <%--  End content--%>
                            <%--<div id="divDocuments" runat="server">
                            <div class="consume_name" >--%>
                            <div class="clear pad_5">
                            </div>
                            <div class="consume_input editlist" id="divDoclabel" visible="false" runat="server"
                                align="center" style="width: 98%">
                                <h2 class="StepTitle">
                                    <asp:Literal ID="litDownloadDocs" Visible="false" runat="server" Text="<%$ Resources:Resource, DOWNLOAD_DOCUMENTS%>"></asp:Literal>
                                </h2>
                                <%--</div>
                            <div class="pad_10 clear">
                            </div>
                            <div class="consume_input">--%>
                                <div class="pad_5 clear">
                                </div>
                                <asp:Repeater ID="rptrDocuments" runat="server">
                                    <ItemTemplate>
                                        <%--OnItemDataBound="rptrDocuments_ItemDataBound"--%>
                                        <asp:UpdatePanel ID="upnlDocs" runat="server">
                                            <ContentTemplate>
                                                <ol>
                                                    <li>
                                                        <div class="downdocum">
                                                            <div class="fltl">
                                                                <asp:Label runat="server" Visible="false" ID="lblDocId" Text='<%#Eval("CustomerDocumentId") %>'></asp:Label>
                                                                <asp:Label runat="server" ID="lblDocName" Text='<%#Eval("CustomerDocumentNames") %>'></asp:Label>&nbsp&nbsp</div>
                                                            <%--<asp:Label runat="server" ID="lblDocPath" Text='<%#Eval("CustomerDocumentPath") %>'></asp:Label> --%>
                                                            <div class="fltr">
                                                                <asp:LinkButton ID="lbtnDocName" ForeColor="Green" runat="server" OnClick="lbtnDocName_Click"
                                                                    CommandArgument='<%#Eval("CustomerDocumentNames") %>'>Download</asp:LinkButton>&nbsp&nbsp
                                                                <asp:LinkButton ID="lbtnDocDelete" ForeColor="Red" OnClick="lbtnDocDelete_Click"
                                                                    runat="server" OnClientClick="return confirm('Are you sure you want to Delete this Document?')"
                                                                    CommandArgument='<%#Eval("CustomerDocumentId") %>'>Delete</asp:LinkButton></div>
                                                            <br />
                                                        </div>
                                                    </li>
                                                </ol>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lbtnDocName" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                            <%--</div>
                            </div>--%>
                            <%--<div class="pad_5">
                        </div>
                        <div class="consumer_feild" id="Docdiv" visible="false" runat="server">
                            <div class="consume_name" style="width: 220px;">
                                <asp:Literal ID="litDocs"  runat="server" Text="<%$ Resources:Resource, UPLOAD_DOCUMENT%>"></asp:Literal>
                            </div>
                            <div class="consume_input" id="divFile" style="width: 280px;">
                                <asp:FileUpload ID="fupDocument"  runat="server" CssClass="fltl" />&nbsp;
                                <asp:LinkButton ID="lnkAddFiles"  runat="server" CssClass="fltl" OnClientClick="return AddFile()"
                                    Font-Bold="True">Add</asp:LinkButton>
                                <span id="spanDocuments" style="color: Red; width: 252px;"></span>
                            </div>
                        </div>--%>
                            <div class="pad_5">
                            </div>
                        </div>
                    </div>
                    <%--</div>--%>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="lbtnStep3Update" />
                </Triggers>
            </asp:UpdatePanel>
            <div class="pad_5">
            </div>
            <div class="consumer_feild" id="divDocs" style="display: none">
                <%--<div class="consume_name" style="width: 220px;">--%>
                <h2 class="StepTitle">
                    <asp:Literal ID="litDocs" runat="server" Text="<%$ Resources:Resource, UPLOAD_DOCUMENT%>"></asp:Literal>
                </h2>
                <%--</div>--%>
                <div class="pad_5">
                </div>
                <div id="divFile" style="width: 280px; padding-left: 500px;">
                    <asp:FileUpload ID="fupDocument" runat="server" CssClass="fltl" />&nbsp;
                    <asp:LinkButton ID="lnkAddFiles" runat="server" CssClass="fltl" OnClientClick="return AddFile()"
                        Font-Bold="True">Add</asp:LinkButton>
                    <span id="spanDocuments" style="color: Red; width: 252px;"></span>
                </div>
                <div class="pad_5">
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnAccountNO" runat="server"></asp:HiddenField>
    <%--</div>--%>
    <%--==============Escape button script starts==============--%>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
