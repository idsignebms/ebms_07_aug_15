﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for EditConsumer
                     
 Developer        : id065-Bhimaraju Vanka
 Creation Date    : 07-03-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using System.Threading;
using System.Globalization;
using UMS_NigeriaBE;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using System.Xml;
using System.IO;
using System.Configuration;
using System.Web.Services;
using System.Data;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class EditConsumer : System.Web.UI.Page
    {
        #region Members

        CommonMethods _ObjCommonMethods = new CommonMethods();
        iDsignBAL objIdsignBal = new iDsignBAL();
        BillCalculatorBAL objBillCalculatorBAL = new BillCalculatorBAL();
        ConsumerBal objConsumerBal = new ConsumerBal();
        string Key = "EditConsumer";
        string CustomerUniqueNumber = string.Empty;

        #endregion

        #region Properties

        private int RouteNo
        {
            get { return string.IsNullOrEmpty(ddlRouteNo.SelectedValue) ? 0 : Convert.ToInt32(ddlRouteNo.SelectedValue); }
        }

        private int RouteSequenceNo
        {
            get { return string.IsNullOrEmpty(txtRouteSequenceNo.Text) ? 0 : Convert.ToInt32(txtRouteSequenceNo.Text); }
        }

        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.CssClass = lblMessage.Text = string.Empty;
            if (!IsPostBack)
            {
                BindDropDowns();
                //ControlsInVisible();
                if (!string.IsNullOrEmpty(Request.QueryString[UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO]))
                {
                    CustomerUniqueNumber = objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO]).ToString();
                    lblConsumerNo.Text = CustomerUniqueNumber;                    
                    BindCustomerDetails();
                    ConsumerBe objConsumerBe = new ConsumerBe();
                    objConsumerBe.CustomerUniqueNo = CustomerUniqueNumber;
                    objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Retrieve(objConsumerBe, ReturnType.Check));
                    if (objConsumerBe.IsBillGenerated > 0)
                    {
                        txtAverageReading.Enabled = txtInitialReading.Enabled = txtMeterNo.Enabled = false;
                    }
                }
                //
                //objCommonMethods.BindProcessedBy(rblApplicationProccessedBy, false);                
            }
        }

        protected void rblApplicationProccessedBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                if (rblApplicationProccessedBy.Items[1].Selected)
                {
                    divApplicationByOthers.Visible = true;
                    divBEDCApplication.Visible = false;
                }
                else
                {
                    divApplicationByOthers.Visible = false;
                    divBEDCApplication.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlReadCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //divApplicationByOthers.Visible = false;
                if (ddlReadCode.SelectedIndex == 1)
                {
                    divMeterInformation.Visible = false;
                    divMinReading.Visible = true;
                }
                else
                {
                    //divApplicationByOthers.Visible = true;
                    divMeterInformation.Visible = true;
                    divMinReading.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void chkIsEmbassyCustomer_Checked(object sender, EventArgs e)
        {
            try
            {
                if (chkIsEmbassyCustomer.Checked)
                    divEmbassyCode.Visible = true;
                else
                    divEmbassyCode.Visible = false;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void lbtnDocDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtnDocDelete = (LinkButton)sender;
                ConsumerBe objConsumerBe = new ConsumerBe();
                objConsumerBe.CustomerDocumentId = Convert.ToInt32(lbtnDocDelete.CommandArgument);
                objConsumerBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Retrieve(objConsumerBe, ReturnType.Retrieve));

                if (objConsumerBe.IsSuccess)
                {
                    Message(UMS_Resource.DOCUMENT_DELETED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    BindCustomerDetails();
                    lbtnStep3Cancel_Click(null, null);
                }
                else
                    Message(UMS_Resource.DOCUMENT_DELETED_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lbtnDocName_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtnDocName = (LinkButton)sender;
                string fileLocation = Server.MapPath(ConfigurationManager.AppSettings["CustomerDocuments"].ToString()) + lbtnDocName.CommandArgument;
                if (File.Exists(fileLocation))
                    _ObjCommonMethods.DownloadFile(fileLocation, "");
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lbtnStep1Edit_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.AccountNo = hdnAccountNO.Value;
                objMastersBE.Flag = Convert.ToInt16(Resource.FLAG_ACCOUNT_NO);
                objMastersBE = _ObjCommonMethods.CustomerBillProcessDetails(objMastersBE);
                if (!objMastersBE.Status)
                {
                    BindCustomerDetails();
                     

                    lbtnStep1Edit.Visible = false;
                    lbtnStep1Cancel.Visible = lbtnStep1Update.Visible = true;
                    //Step-1 Edit Controls txtCustomerSequenceNo.Visible = lblCustomerSequenceNo.Visible =
                    ddlTitle.Visible = txtName.Visible = txtSurName.Visible = txtDocumentNo.Visible = txtHome.Visible = txtKnownAs.Visible = txtEmployeeCode.Visible = true;
                    txtBussiness.Visible = txtOthers.Visible = txtLandMark.Visible = txtHouseNo.Visible = txtCity.Visible = txtStreet.Visible = true;
                    txtPostalDetails.Visible = txtZipCode.Visible = txtIdentityNumber.Visible = txtEmailId.Visible = ddlIdentityType.Visible = true;
                    //step-1 Label Controls
                    lblTitle.Visible = lblName.Visible = lblSurName.Visible = lblDocumentNo.Visible = lblHome.Visible = lblKnownAs.Visible = lblEmployeeCode.Visible = false;
                    lblBussiness.Visible = lblOthers.Visible = lblLandMark.Visible = lblHouseNo.Visible = lblCity.Visible = lblStreet.Visible = false;
                    lblPostalDetails.Visible = lblZipCode.Visible = lblIdentityNumber.Visible = lblEmailId.Visible = lblIdentityType.Visible = false;
                    //lblState.Visible = lblDistrict.Visible =
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Reset4", "Step1Editblock();", true);
                }
                else
                {
                    mpeBillInProcess.Show();
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lbtnStep1Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Reset5", "Step1Editnone();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Reset14", "Step2Editnone();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Reset15", "Step3Editnone();", true); 

                lbtnStep1Edit.Visible = true;
                lbtnStep1Cancel.Visible = lbtnStep1Update.Visible = false;
                //Step-1 Edit Controls txtCustomerSequenceNo.Visible =lblCustomerSequenceNo.Visible =
                ddlTitle.Visible = txtName.Visible = txtSurName.Visible = txtDocumentNo.Visible = txtHome.Visible = txtKnownAs.Visible = txtEmployeeCode.Visible = false;
                txtBussiness.Visible = txtOthers.Visible = txtLandMark.Visible = txtHouseNo.Visible = txtCity.Visible = txtStreet.Visible = false;
                txtPostalDetails.Visible = txtZipCode.Visible = txtIdentityNumber.Visible = txtEmailId.Visible = ddlIdentityType.Visible = false;
                //step-1 Label Controls
                lblTitle.Visible = lblName.Visible = lblSurName.Visible = lblDocumentNo.Visible = lblHome.Visible = lblKnownAs.Visible = lblEmployeeCode.Visible = true;
                lblBussiness.Visible = lblOthers.Visible = lblLandMark.Visible = lblHouseNo.Visible = lblCity.Visible = lblStreet.Visible = true;
                lblPostalDetails.Visible = lblZipCode.Visible = lblIdentityNumber.Visible = lblEmailId.Visible = lblIdentityType.Visible = true;

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lbtnStep1Update_Click(object sender, EventArgs e)
        {
            try
            {
                ConsumerBe objConsumerBe = new ConsumerBe();
                objConsumerBe.CustomerUniqueNo = lblConsumerNo.Text;
                if (ddlTitle.SelectedIndex > 0)
                    objConsumerBe.Title = ddlTitle.SelectedItem.Text;
                objConsumerBe.Name = txtName.Text;
                objConsumerBe.SurName = txtSurName.Text;
                objConsumerBe.KnownAs = txtKnownAs.Text;
                objConsumerBe.EmailId = txtEmailId.Text;
                objConsumerBe.DocumentNo = txtDocumentNo.Text;
                objConsumerBe.EmployeeCode = txtEmployeeCode.Text;
                objConsumerBe.HomeContactNo = txtHome.Text;
                objConsumerBe.BusinessContactNo = txtBussiness.Text;
                objConsumerBe.OtherContactNo = txtOthers.Text;
                objConsumerBe.PostalLandMark = txtLandMark.Text;
                objConsumerBe.PostalStreet = txtStreet.Text;
                objConsumerBe.PostalCity = txtCity.Text;
                objConsumerBe.PostalHouseNo = txtHouseNo.Text;
                objConsumerBe.PostalDetails = objIdsignBal.ReplaceNewLines(txtPostalDetails.Text, true);
                objConsumerBe.PostalZipCode = txtZipCode.Text;
                objConsumerBe.IdentityNo = txtIdentityNumber.Text;
                //objConsumerBe.CustomerSequenceNo = Convert.ToInt32(txtCustomerSequenceNo.Text);
                objConsumerBe.IdentityTypeId = string.IsNullOrEmpty(ddlIdentityType.SelectedValue) ?0: Convert.ToInt32(ddlIdentityType.SelectedValue);
                objConsumerBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objConsumerBe.Step = 1;
                objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Insert(objConsumerBe, Statement.Update));
                if (objConsumerBe.IsDocumentNoExists)
                {
                    Message(UMS_Resource.DOCUMENT_NO_EXISTS_MSG, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (objConsumerBe.IsCustomerSequenceNo)
                {
                    Message(UMS_Resource.CUSTOMER_SEQUENCE_NO_EXISTS_MSG, UMS_Resource.MESSAGETYPE_ERROR);

                }
                else if (objConsumerBe.IsSuccess)
                {
                    Message(UMS_Resource.CUSTOMER_UPDATED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    BindCustomerDetails();
                    lbtnStep1Cancel_Click(null, null);
                }
                else
                    Message(UMS_Resource.CUSTOMER_UPDATED_FAIL, UMS_Resource.MESSAGETYPE_ERROR);

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void lbtnStep2Edit_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.AccountNo = hdnAccountNO.Value;
                objMastersBE.Flag = Convert.ToInt16(Resource.FLAG_ACCOUNT_NO);
                objMastersBE = _ObjCommonMethods.CustomerBillProcessDetails(objMastersBE);
                if (!objMastersBE.Status)
                {
                    BindCustomerDetails();
                    lbtnStep2Edit.Visible = false;
                    lbtnStep2Cancel.Visible = lbtnStep2Update.Visible = true;
                    //Step-2 Edit Controls
                    ddlTransformer.Visible = ddlPole.Visible = txtneighbor.Visible = txtLatitude.Visible = txtLongitude.Visible = true;
                    //ddlBusinessUnit.Visible = ddlServiceUnit.Visible = ddlServiceCenter.Visible = ddlBookNumber.Visible = true;
                    ddlInjectionSubStation.Visible = true;
                    ddlFeeder.Visible = txtServiceLandMark.Visible = txtServiceHouseNo.Visible = txtServiceDetails.Visible = txtServiceCity.Visible = true;
                    txtServiceStreet.Visible = txtServiceZipCode.Visible = txtServiceEmailId.Visible = txtMobileNo.Visible = txtRouteSequenceNo.Visible = ddlRouteNo.Visible = true;
                    //step-2 Label Controls
                    lblTransformer.Visible = lblPole.Visible = lblneighbor.Visible = lblLatitude.Visible = lblLongitude.Visible = false;
                    //lblBusinessUnit.Visible = lblServiceUnit.Visible = lblServiceCenter.Visible = lblBookNumber.Visible = false;
                    lblInjectionSubStation.Visible = false;
                    lblFeeder.Visible = lblServiceLandMark.Visible = lblServiceHouseNo.Visible = lblServiceDetails.Visible = lblServiceCity.Visible = false;
                    lblServiceStreet.Visible = lblServiceZipCode.Visible = lblServiceEmailId.Visible = lblMobileNo.Visible = lblRouteSequenceNo.Visible = lblRouteNo.Visible = false;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Reset6", "Step2Editblock();", true);

                }
                else
                {
                    mpeBillInProcess.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lbtnStep2Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Reset7", "Step2Editnone();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Reset12", "Step3Editnone();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Reset13", "Step1Editnone();", true); 

                lbtnStep2Edit.Visible = true;
                lbtnStep2Cancel.Visible = lbtnStep2Update.Visible = false;
                //Step-2 Edit Controls
                ddlTransformer.Visible = ddlPole.Visible = txtneighbor.Visible = txtLatitude.Visible = txtLongitude.Visible = false;
                ddlBusinessUnit.Visible = ddlServiceUnit.Visible = ddlServiceCenter.Visible = ddlBookNumber.Visible = ddlInjectionSubStation.Visible = false;
                ddlFeeder.Visible = txtServiceLandMark.Visible = txtServiceHouseNo.Visible = txtServiceDetails.Visible = txtServiceCity.Visible = false;
                txtServiceStreet.Visible = txtServiceZipCode.Visible = txtServiceEmailId.Visible = txtMobileNo.Visible = txtRouteSequenceNo.Visible = ddlRouteNo.Visible = false;
                //step-2 Label Controls
                lblTransformer.Visible = lblPole.Visible = lblneighbor.Visible = lblLatitude.Visible = lblLongitude.Visible = true;
                lblBusinessUnit.Visible = lblServiceUnit.Visible = lblServiceCenter.Visible = lblBookNumber.Visible = lblInjectionSubStation.Visible = true;
                lblFeeder.Visible = lblServiceLandMark.Visible = lblServiceHouseNo.Visible = lblServiceDetails.Visible = lblServiceCity.Visible = true;
                lblServiceStreet.Visible = lblServiceZipCode.Visible = lblServiceEmailId.Visible = lblMobileNo.Visible = lblRouteSequenceNo.Visible = lblRouteNo.Visible = true;

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void lbtnStep2Update_Click(object sender, EventArgs e)
        {
            try
            {
                ConsumerBe objConsumerBe = new ConsumerBe();

                objConsumerBe.CustomerUniqueNo = lblConsumerNo.Text;
                objConsumerBe.SubStationId = ddlInjectionSubStation.SelectedValue;
                objConsumerBe.FeederId = ddlFeeder.SelectedValue;
                objConsumerBe.TransFormerId = ddlTransformer.SelectedValue;
                objConsumerBe.PoleId = ddlPole.SelectedValue;
                objConsumerBe.ServiceLandMark = txtServiceLandMark.Text;
                objConsumerBe.ServiceStreet = txtServiceStreet.Text;
                objConsumerBe.ServiceCity = txtServiceCity.Text;
                objConsumerBe.ServiceHouseNo = txtServiceHouseNo.Text;
                objConsumerBe.ServiceDetails = objIdsignBal.ReplaceNewLines(txtServiceDetails.Text, true);
                objConsumerBe.ServiceZipCode = txtServiceZipCode.Text;
                objConsumerBe.ServiceEmailId = txtServiceEmailId.Text;
                objConsumerBe.MobileNo = txtMobileNo.Text;
                objConsumerBe.RouteSequenceNo = RouteSequenceNo;
                objConsumerBe.RouteNo = RouteNo;
                objConsumerBe.NeighborAccountNo = txtneighbor.Text;
                objConsumerBe.Latitude = txtLatitude.Text;
                objConsumerBe.Longitude = txtLongitude.Text;
                objConsumerBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objConsumerBe.Step = 2;
                objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Insert(objConsumerBe, Statement.Update));
                if (!string.IsNullOrEmpty(txtneighbor.Text) && objConsumerBe.IsNeighborAccountNotExists)
                {
                    _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.NEIGHBOUR_ACC_NOT_EXISTS, pnlMessage, lblMessage);
                }
                else if (objConsumerBe.IsSuccess)
                {
                    Message(UMS_Resource.CUSTOMER_UPDATED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    BindCustomerDetails();
                    lbtnStep2Cancel_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void lbtnStep3Edit_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.AccountNo = hdnAccountNO.Value;
                objMastersBE.Flag = Convert.ToInt16(Resource.FLAG_ACCOUNT_NO);
                objMastersBE = _ObjCommonMethods.CustomerBillProcessDetails(objMastersBE);
                if (!objMastersBE.Status)
                {
                    BindCustomerDetails();
                    lbtnStep3Edit.Visible = false;
                    lbtnStep3Cancel.Visible = lbtnStep3Update.Visible = true;
                    //Step-3 Edit Controls
                    //litDocs.Visible = fupDocument.Visible = lnkAddFiles.Visible = true;
                    //Docdiv.Visible = true;txtSetupDate.Visible =lblSetupDate.Visible =

                    ddlTariff.Visible = txtMeterNo.Visible = txtApplicationDate.Visible = ddlAccountType.Visible = ddlReadCode.Visible = ddlCustomerType.Visible = txtOrganizationCode.Visible = true;
                    ddlMeterType.Visible = ddlPhase.Visible = txtMeeterSerialNo.Visible = txtConnectionDate.Visible = ddlMeeterStatus.Visible = ddlConnectionReason.Visible = true;
                    txtInitialReading.Visible =  ddlCertifiedBy.Visible = rblApplicationProccessedBy.Enabled = true;
                    ddlAgencyName.Visible = ddlInstalledBy.Visible = txtEmbassyCode.Visible = chkIsEmbassyCustomer.Enabled = txtTariffMultiplier.Visible = txtOldAccountNo.Visible = chkIsVipCustomer.Enabled = true;
                    txtAgencyDetails.Visible = txtContactPersonName.Visible = txtAverageReading.Visible = txtMinReading.Visible = true;
                    //txtCurrentReading.Visible = 
                    //step-3 Label Controls
                    lblTariff.Visible = lblMeterNo.Visible = lblApplicationDate.Visible = lblAccountType.Visible = lblReadCode.Visible = lblCustomerType.Visible = lblOrganizationCode.Visible = false;
                    lblMeterType.Visible = lblPhase.Visible = lblMeeterSerialNo.Visible = lblConnectionDate.Visible = lblMeeterStatus.Visible = lblConnectionReason.Visible = false;
                    lblInitialReading.Visible =  lblCertifiedBy.Visible = false;
                    lblAgencyName.Visible = lblInstalledBy.Visible = lblEmbassyCode.Visible = lblTariffMultiplier.Visible = lblOldAccountNo.Visible = false;
                    lblAgencyDetails.Visible = lblContactPersonName.Visible = lblAverageReading.Visible = lblMinReading.Visible = false;
                    //lblCurrentReading.Visible = 
                    if (lblEmbassyChecked.Text == 1.ToString())
                    {
                        chkIsEmbassyCustomer.Checked = true;
                        divEmbassyCode.Visible = true;
                    }
                    else
                    {
                        chkIsEmbassyCustomer.Checked = false;
                        divEmbassyCode.Visible = false;
                    }
                    //for (int i = 0; i < rblApplicationProccessedBy.Items.Count; i++)
                    //    rblApplicationProccessedBy.Items[i].Attributes.Add("onclick", "InstalledBy()");
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Reset8", "Step3Editblock();", true);

                }
                else
                {
                    mpeBillInProcess.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lbtnStep3Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Reset9", "Step3Editnone();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Reset10", "Step2Editnone();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Reset11", "Step1Editnone();", true); 

                lbtnStep3Edit.Visible = true;
                lbtnStep3Cancel.Visible = lbtnStep3Update.Visible = false;
                //Step-3 Edit Controls
                //litDocs.Visible = fupDocument.Visible = lnkAddFiles.Visible = false;
                //Docdiv.Visible = false;imgDate3.Visible =txtSetupDate.Visible =imgDate3.Visible =lblSetupDate.Visible =
                ddlTariff.Visible =  imgDate2.Visible = imgDate.Visible = txtMeterNo.Visible = txtApplicationDate.Visible = ddlAccountType.Visible = ddlReadCode.Visible = ddlCustomerType.Visible = txtOrganizationCode.Visible = false;
                ddlMeterType.Visible = ddlPhase.Visible = txtMeeterSerialNo.Visible = txtConnectionDate.Visible = ddlMeeterStatus.Visible = ddlConnectionReason.Visible = false;
                txtInitialReading.Visible =  ddlCertifiedBy.Visible = rblApplicationProccessedBy.Enabled = false;
                ddlAgencyName.Visible = ddlInstalledBy.Visible = txtEmbassyCode.Visible = txtAgencyDetails.Visible = txtTariffMultiplier.Visible = txtOldAccountNo.Visible = false;
                chkIsEmbassyCustomer.Enabled = chkIsVipCustomer.Enabled = txtContactPersonName.Visible = txtAverageReading.Visible = txtMinReading.Visible = false;
                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Reset", "DocsInVisible();", true);
                //txtCurrentReading.Visible = 
                //step-3 Label Controls
                lblTariff.Visible =  imgDate2.Visible = imgDate.Visible = lblMeterNo.Visible = lblApplicationDate.Visible = lblAccountType.Visible = lblReadCode.Visible = lblCustomerType.Visible = lblOrganizationCode.Visible = true;
                lblMeterType.Visible = lblPhase.Visible = lblMeeterSerialNo.Visible = lblConnectionDate.Visible = lblMeeterStatus.Visible = lblConnectionReason.Visible = true;
                lblInitialReading.Visible =  lblContactPersonName.Visible = lblAgencyDetails.Visible = lblCertifiedBy.Visible = lblMinReading.Visible = true;
                lblAgencyName.Visible = lblInstalledBy.Visible = lblEmbassyCode.Visible = lblTariffMultiplier.Visible = lblAverageReading.Visible = lblOldAccountNo.Visible = true;
                chkIsEmbassyCustomer.Enabled = rblApplicationProccessedBy.Enabled = false;
                ddlReadCode.SelectedIndex = ddlReadCode.Items.IndexOf(ddlReadCode.Items.FindByText(lblReadCode.Text));
                ddlReadCode_SelectedIndexChanged(ddlReadCode, new EventArgs());
                rblApplicationProccessedBy.SelectedIndex = rblApplicationProccessedBy.Items.IndexOf(rblApplicationProccessedBy.Items.FindByValue(lblApplicationProccessedBy.Text));
                rblApplicationProccessedBy_SelectedIndexChanged(rblApplicationProccessedBy, new EventArgs());
                if (lblEmbassyChecked.Text == 1.ToString())
                {
                    chkIsEmbassyCustomer.Checked = true;
                    divEmbassyCode.Visible = true;
                }
                else
                {
                    chkIsEmbassyCustomer.Checked = false;
                    divEmbassyCode.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void lbtnStep3Update_Click(object sender, EventArgs e)
        {
            try
            {
                string fileInput = string.Empty;
                ConsumerBe objConsumerBe = new ConsumerBe();

                if (Request.Files.Count > 0)
                {
                    for (int z = 0; z < Request.Files.Count; z++)//looping to save attachments
                    {
                        HttpPostedFile PostedFile = Request.Files[z];//Assigning to variable to Access File
                        if (PostedFile.ContentLength != 0)//Checking the size of file is more than 0 bytes
                        {
                            string FileName = Path.GetFileName(PostedFile.FileName).ToString();//Assinging FileName
                            string Extension = Path.GetExtension(FileName);//Storing Extension of file into variable Extension
                            TimeZoneInfo IND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                            DateTime currentTime = DateTime.Now;
                            DateTime ourtime = TimeZoneInfo.ConvertTime(currentTime, IND_ZONE);
                            string CurrentDate = ourtime.ToString("dd_MM_yyyy_hh_mm_ss");
                            string ModifiedFileName = Path.GetFileNameWithoutExtension(FileName) + "_" + CurrentDate + Extension;//Setting File Name to store it in database
                            PostedFile.SaveAs(Server.MapPath(ConfigurationManager.AppSettings[UMS_Resource.KEY_CUSTOMER_DOCUMENTS].ToString()) + ModifiedFileName);//Saving File in to the server.
                            fileInput = string.IsNullOrEmpty(fileInput) ? ModifiedFileName : fileInput + "," + ModifiedFileName;
                        }
                    }
                    objConsumerBe.CustomerDocumentNames = fileInput;
                    objConsumerBe.CustomerDocumentPath = ConfigurationManager.AppSettings[UMS_Resource.KEY_CUSTOMER_DOCUMENTS].ToString().Replace("~", "..");
                }
                decimal TariffMultiplier = string.IsNullOrEmpty(txtTariffMultiplier.Text) ? 0 : decimal.Parse(txtTariffMultiplier.Text);
                string ApplicationDate = string.IsNullOrEmpty(txtApplicationDate.Text) ? "" : _ObjCommonMethods.Convert_MMDDYY(txtApplicationDate.Text);
                objConsumerBe.CustomerUniqueNo = lblConsumerNo.Text;
                objConsumerBe.TariffId = Convert.ToInt32(ddlTariff.SelectedValue);
                objConsumerBe.MultiplicationFactor = TariffMultiplier;
                objConsumerBe.AccountTypeId = Convert.ToInt32(ddlAccountType.SelectedValue);
                objConsumerBe.ApplicationDate = ApplicationDate;
                objConsumerBe.ReadCodeId = Convert.ToInt32(ddlReadCode.SelectedValue);
                objConsumerBe.OrganizationCode = txtOrganizationCode.Text;
                objConsumerBe.CustomerTypeId = Convert.ToInt32(ddlCustomerType.SelectedValue);
                objConsumerBe.OldAccountNo = txtOldAccountNo.Text;


                objConsumerBe.IsEmbassyCustomer = chkIsEmbassyCustomer.Checked;
                if (chkIsEmbassyCustomer.Checked)
                {
                    objConsumerBe.EmbassyCode = txtEmbassyCode.Text;
                }
                else
                {
                    objConsumerBe.EmbassyCode = string.Empty;
                }
                objConsumerBe.IsVIPCustomer = chkIsVipCustomer.Checked;
                if (ddlReadCode.SelectedIndex != 1)
                {
                    objConsumerBe.PhaseId = Convert.ToInt32(ddlPhase.SelectedValue);
                    objConsumerBe.MeterTypeId = Convert.ToInt32(ddlMeterType.SelectedValue);
                    objConsumerBe.MeterNo = txtMeterNo.Text;
                    objConsumerBe.MeterSerialNo = txtMeeterSerialNo.Text;
                    objConsumerBe.ConnectionDate = _ObjCommonMethods.Convert_MMDDYY(txtConnectionDate.Text);
                    objConsumerBe.MeterStatusId = Convert.ToInt32(ddlMeeterStatus.SelectedValue);
                    objConsumerBe.ConnectionReasonId = Convert.ToInt32(ddlConnectionReason.SelectedValue);
                    objConsumerBe.InitialReading = txtInitialReading.Text;
                    //objConsumerBe.CurrentReading = txtCurrentReading.Text;
                    objConsumerBe.AverageReading = txtAverageReading.Text;
                    //objConsumerBe.SetupDate = _ObjCommonMethods.Convert_MMDDYY(txtSetupDate.Text);
                    objConsumerBe.CertifiedBy = ddlCertifiedBy.SelectedValue;
                    //objConsumerBe.CertifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objConsumerBe.ProcessedById = Convert.ToInt32(rblApplicationProccessedBy.SelectedValue);
                    if (rblApplicationProccessedBy.Items[0].Selected)
                    {
                        objConsumerBe.InstalledBy = ddlInstalledBy.SelectedValue;//
                        //objConsumerBe.InstalledBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    }
                    else
                    {
                        objConsumerBe.AgencyId = Convert.ToInt32(ddlAgencyName.SelectedValue);
                        objConsumerBe.ContactPersonName = txtContactPersonName.Text;
                        objConsumerBe.AgencyDetails = objIdsignBal.ReplaceNewLines(txtAgencyDetails.Text, true);
                    }
                }
                if (ddlReadCode.SelectedIndex == 1)
                {
                    objConsumerBe.MinimumReading = txtMinReading.Text;
                    objConsumerBe.MeterNo = objConsumerBe.MeterSerialNo = objConsumerBe.ConnectionDate = objConsumerBe.InitialReading = objConsumerBe.SetupDate = string.Empty;
                    objConsumerBe.ContactPersonName = objConsumerBe.AgencyDetails = string.Empty;
                    objConsumerBe.PhaseId = objConsumerBe.MeterTypeId = objConsumerBe.MeterStatusId = objConsumerBe.ConnectionReasonId = objConsumerBe.ProcessedById = objConsumerBe.AgencyId = 0;
                    objConsumerBe.CertifiedBy = objConsumerBe.InstalledBy = string.Empty;
                }
                objConsumerBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objConsumerBe.Step = 3;
                objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Insert(objConsumerBe, Statement.Update));
                chkIsEmbassyCustomer.Checked = false;
                txtEmbassyCode.Text = string.Empty;
                chkIsEmbassyCustomer_Checked(null, null);
                if (objConsumerBe.IsSuccess)
                {
                    Message(UMS_Resource.CUSTOMER_UPDATED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    BindCustomerDetails();
                    lbtnStep3Cancel_Click(null, null);
                }
                else if (objConsumerBe.IsMeterNumberNotExists)
                {
                    Message(UMS_Resource.METER_NO_NOT_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (objConsumerBe.IsMeterNoExists)
                {
                    Message(UMS_Resource.CUSTOMER_METERNO_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (objConsumerBe.IsMeterSerialNoExists)
                {
                    Message(UMS_Resource.CUSTOMER_METER_SERIALNO_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (objConsumerBe.IsOldAccountNoExists)
                {
                    Message(Resource.CUSTOMER_OLD_ACCNO_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (objConsumerBe.IsOldAccountNotValid)
                {
                    Message(Resource.CUSTOMER_OLD_ACCNO_NOT_VALID, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (objConsumerBe.MeterDials > 0)
                {
                    Message(UMS_Resource.CUSTOMER_INITIAL_READING_NOT_CORRECT + objConsumerBe.MeterDials + " Charecteres", UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        private void BindDropDowns()
        {
            try
            {
                _ObjCommonMethods.BindIdentityTypes(ddlIdentityType, true);
                _ObjCommonMethods.BindInjectionSubStations(ddlInjectionSubStation, true);
                _ObjCommonMethods.BindStates(ddlState, string.Empty, true);
                //objCommonMethods.BindDistricts(ddlDistrict, string.Empty, true);
                _ObjCommonMethods.BindPhases(ddlPhase, true);
                _ObjCommonMethods.BindConnectionReasons(ddlConnectionReason, true);
                _ObjCommonMethods.BindTariffSubTypes(ddlTariff, 0, true);
                _ObjCommonMethods.BindMeterStatus(ddlMeeterStatus, true);
                _ObjCommonMethods.BindMeterTypes(ddlMeterType, true);
                _ObjCommonMethods.BindAccountTypes(ddlAccountType, true);
                _ObjCommonMethods.BindReadCodes(ddlReadCode, true);
                _ObjCommonMethods.BindCustomerTypes(ddlCustomerType, true);
                _ObjCommonMethods.BindAgencies(ddlAgencyName, true);
                _ObjCommonMethods.BindEmployeesList(ddlCertifiedBy, true);
                _ObjCommonMethods.BindEmployeesList(ddlInstalledBy, true);
                _ObjCommonMethods.BindProcessedBy(rblApplicationProccessedBy, true);
                _ObjCommonMethods.BindRoutes(ddlRouteNo, (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString(), true);
                //ddlCustomerType.SelectedIndex = 1;
                //ddlCustomerType.Enabled = false;
                //ddlMeeterStatus.SelectedIndex = 1;
                //ddlMeeterStatus.Enabled = false;
                ddlConnectionReason.SelectedIndex = 2;
                ddlConnectionReason.Enabled = false;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindDocuments()
        {
            try
            {
                litDownloadDocs.Visible = divDoclabel.Visible = false;
                ConsumerBe objConsumerBe = new ConsumerBe();
                ConsumerListBe objConsumerListBe = new ConsumerListBe();
                objConsumerBe.CustomerUniqueNo = lblConsumerNo.Text;
                objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Retrieve(objConsumerBe, ReturnType.Set));
                if (objConsumerListBe.items.Count > 0)
                {
                    litDownloadDocs.Visible = divDoclabel.Visible = true;
                    rptrDocuments.DataSource = objConsumerListBe.items;
                    rptrDocuments.DataBind();
                }
                else
                {

                    rptrDocuments.DataSource = new DataTable();
                    rptrDocuments.DataBind();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }



        protected void ddlBusinessUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlBusinessUnit.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindServiceUnits(ddlServiceUnit, ddlBusinessUnit.SelectedValue, true);
                    ddlServiceUnit.Enabled = true;
                }
                else
                {
                    //ddlServiceUnit.SelectedIndex = Constants.Zero;
                    //ddlServiceUnit.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlServiceUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlServiceUnit.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindServiceCenters(ddlServiceCenter, ddlServiceUnit.SelectedValue, true);
                }
                else
                {
                    //ddlServiceCenter.SelectedIndex = Constants.Zero;
                    //ddlServiceCenter.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ddlState.SelectedIndex > 0)
        //            objCommonMethods.BindDistricts(ddlDistrict, ddlState.SelectedValue, true);
        //        else
        //            objCommonMethods.BindDistricts(ddlDistrict, string.Empty, true);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void ddlDistrict_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        objCommonMethods.BindBusinessUnits(ddlBusinessUnit, ddlDistrict.SelectedValue, true);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void ddlServiceCenter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlServiceCenter.SelectedIndex > 0)
                {
                    //objCommonMethods.BindInjectionSubStations(ddlInjectionSubStation, ddlServiceCenter.SelectedValue, false);
                    _ObjCommonMethods.BindBookNumbersByServiceDetails(ddlBookNumber, ddlBusinessUnit.SelectedValue, ddlServiceUnit.SelectedValue, ddlServiceCenter.SelectedValue, true);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlInjectionSubStation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlInjectionSubStation.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindFeeders(ddlFeeder, ddlInjectionSubStation.SelectedValue, UMS_Resource.DROPDOWN_SELECT, true);
                    ddlFeeder.Enabled = true;
                    ddlTransformer.Enabled = ddlPole.Enabled = false;
                }
                else
                {
                    ddlFeeder.SelectedIndex = ddlTransformer.SelectedIndex = ddlPole.SelectedIndex = Constants.Zero;
                    ddlFeeder.Enabled = ddlTransformer.Enabled = ddlPole.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlFeeder_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlFeeder.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindTransformers(ddlTransformer, ddlFeeder.SelectedValue, true);
                    ddlTransformer.Enabled = true;
                }
                else
                {
                    ddlTransformer.SelectedIndex = ddlPole.SelectedIndex = Constants.Zero;
                    ddlTransformer.Enabled = ddlPole.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlTransformer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlTransformer.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindPoles(ddlPole, ddlTransformer.SelectedValue, true);
                    ddlPole.Enabled = true;
                }
                else
                {
                    ddlPole.SelectedIndex = Constants.Zero;
                    ddlPole.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //protected void rptrDocuments_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        Label lblDocName = ((Label)e.Item.FindControl("lblDocName"));
        //        lblDocName.Text = lblDocName.Text.Remove((lblDocName.Text.Length - 25), 25);
        //    }
        //}

        #endregion

        #region Methods
        private void BindCustomerDetails()
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Reset1", "Step1Editnone();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Reset2", "Step2Editnone();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Reset3", "Step3Editnone();", true);
                ConsumerBe objConsumerBe = new ConsumerBe();
                objConsumerBe.CustomerUniqueNo = lblConsumerNo.Text;
                objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Get(objConsumerBe, ReturnType.Set));

                //Step-1
                //lblConsumerNo.Text = CustomerUniqueNumber;
                lblAccountNo.Text = objConsumerBe.AccountNo;
                hdnAccountNO.Value = objConsumerBe.AccountNo;
                if (objConsumerBe.IsBookNoChanged)
                    spanBookNoChanged.Visible = true;
                else
                    spanBookNoChanged.Visible = false;
                lblTitle.Text = objConsumerBe.Title;
                ddlTitle.SelectedIndex = ddlTitle.Items.IndexOf(ddlTitle.Items.FindByText(objConsumerBe.Title));
                lblName.Text = txtName.Text = objConsumerBe.Name;
                lblSurName.Text = txtSurName.Text = objConsumerBe.SurName;
                lblEmailId.Text = txtEmailId.Text = objConsumerBe.EmailId;
                lblKnownAs.Text = txtKnownAs.Text = objConsumerBe.KnownAs;
                lblDocumentNo.Text = txtDocumentNo.Text = objConsumerBe.DocumentNo;
                //lblCustomerSequenceNo.Text = txtCustomerSequenceNo.Text = objConsumerBe.CustomerSequenceNo.ToString();
                lblEmployeeCode.Text = txtEmployeeCode.Text = objConsumerBe.EmployeeCode;
                lblHome.Text = txtHome.Text = objConsumerBe.HomeContactNo;
                lblBussiness.Text = txtBussiness.Text = objConsumerBe.BusinessContactNo;
                lblOthers.Text = txtOthers.Text = objConsumerBe.OtherContactNo;
                lblState.Text = objConsumerBe.StateName;
                //lblDistrict.Text = objConsumerBe.DistrictName;
                lblLandMark.Text = txtLandMark.Text = objConsumerBe.PostalLandMark;
                lblStreet.Text = txtStreet.Text = objConsumerBe.PostalStreet;
                lblCity.Text = txtCity.Text = objConsumerBe.PostalCity;
                lblHouseNo.Text = txtHouseNo.Text = objConsumerBe.PostalHouseNo;
                lblPostalDetails.Text = txtPostalDetails.Text = objIdsignBal.ReplaceNewLines(objConsumerBe.PostalDetails, false);
                lblZipCode.Text = txtZipCode.Text = objConsumerBe.PostalZipCode;
                lblIdentityNumber.Text = txtIdentityNumber.Text = objConsumerBe.IdentityNo;
                lblIdentityType.Text = objConsumerBe.IdentityType;
                ddlIdentityType.SelectedIndex = ddlIdentityType.Items.IndexOf(ddlIdentityType.Items.FindByValue(objConsumerBe.IdentityTypeId.ToString()));
                //Step-2                
                lblBusinessUnit.Text = objConsumerBe.BusinessUnitName;
                lblServiceUnit.Text = objConsumerBe.ServiceUnitName;
                lblServiceCenter.Text = objConsumerBe.ServiceCenterName;
                lblBookNumber.Text = objConsumerBe.BookNo;
                lblInjectionSubStation.Text = objConsumerBe.SubStationName;
                _ObjCommonMethods.BindInjectionSubStations(ddlInjectionSubStation, true);
                ddlInjectionSubStation.SelectedIndex = ddlInjectionSubStation.Items.IndexOf(ddlInjectionSubStation.Items.FindByValue(objConsumerBe.SubStationId));
                _ObjCommonMethods.BindFeeders(ddlFeeder, ddlInjectionSubStation.SelectedValue, UMS_Resource.DROPDOWN_SELECT, true);
                lblFeeder.Text = objConsumerBe.FeederName;
                ddlFeeder.SelectedIndex = ddlFeeder.Items.IndexOf(ddlFeeder.Items.FindByValue(objConsumerBe.FeederId));
                _ObjCommonMethods.BindTransformers(ddlTransformer, ddlFeeder.SelectedValue, true);
                lblTransformer.Text = objConsumerBe.TransFormerName;
                ddlTransformer.SelectedIndex = ddlTransformer.Items.IndexOf(ddlTransformer.Items.FindByValue(objConsumerBe.TransFormerId));
                _ObjCommonMethods.BindPoles(ddlPole, ddlTransformer.SelectedValue, true);
                lblPole.Text = objConsumerBe.PoleName;
                ddlPole.SelectedIndex = ddlPole.Items.IndexOf(ddlPole.Items.FindByValue(objConsumerBe.PoleId));
                lblServiceLandMark.Text = txtServiceLandMark.Text = objConsumerBe.ServiceLandMark;
                lblServiceStreet.Text = txtServiceStreet.Text = objConsumerBe.ServiceStreet;
                lblServiceCity.Text = txtServiceCity.Text = objConsumerBe.ServiceCity;
                lblServiceDetails.Text = txtServiceDetails.Text = objIdsignBal.ReplaceNewLines(objConsumerBe.ServiceDetails, false);
                lblServiceZipCode.Text = txtServiceZipCode.Text = objConsumerBe.ServiceZipCode;
                lblServiceHouseNo.Text = txtServiceHouseNo.Text = objConsumerBe.ServiceHouseNo;
                lblMobileNo.Text = txtMobileNo.Text = objConsumerBe.MobileNo;
                lblRouteSequenceNo.Text = txtRouteSequenceNo.Text = objConsumerBe.RouteSequenceNo == 0 ? "--" : objConsumerBe.RouteSequenceNo.ToString();
                ddlRouteNo.SelectedIndex = objConsumerBe.RouteNo == 0 ? 0 : ddlRouteNo.Items.IndexOf(ddlRouteNo.Items.FindByValue(objConsumerBe.RouteNo.ToString()));
                lblRouteNo.Text = objConsumerBe.RouteNo == 0 ? "--" : objConsumerBe.Route;
                lblServiceEmailId.Text = txtServiceEmailId.Text = objConsumerBe.ServiceEmailId;
                if (objConsumerBe.NeighborAccountNo == "--")
                {
                    lblneighbor.Text = objConsumerBe.NeighborAccountNo;
                    txtneighbor.Text = string.Empty;
                }
                else
                    lblneighbor.Text = txtneighbor.Text = objConsumerBe.NeighborAccountNo;
                lblLatitude.Text = txtLatitude.Text = objConsumerBe.Latitude;
                lblLongitude.Text = txtLongitude.Text = objConsumerBe.Longitude;
                //Step-3

                lblTariff.Text = objConsumerBe.Tariff;
                ddlTariff.SelectedIndex = ddlTariff.Items.IndexOf(ddlTariff.Items.FindByValue(objConsumerBe.TariffId.ToString()));
                lblTariffMultiplier.Text = txtTariffMultiplier.Text = Convert.ToString(objConsumerBe.MultiplicationFactor);
                BindTariffDetails();
                lblAccountType.Text = objConsumerBe.AccountType;
                ddlAccountType.SelectedIndex = ddlAccountType.Items.IndexOf(ddlAccountType.Items.FindByValue(objConsumerBe.AccountTypeId.ToString()));
                lblOldAccountNo.Text = txtOldAccountNo.Text = objConsumerBe.OldAccountNo;
                lblApplicationDate.Text = txtApplicationDate.Text = objConsumerBe.ApplicationDate;
                lblReadCode.Text = objConsumerBe.ReadCode;
                ddlReadCode.SelectedIndex = ddlReadCode.Items.IndexOf(ddlReadCode.Items.FindByValue(objConsumerBe.ReadCodeId.ToString()));
                ddlReadCode_SelectedIndexChanged(null, null);
                lblOrganizationCode.Text = txtOrganizationCode.Text = objConsumerBe.OrganizationCode;
                lblCustomerType.Text = objConsumerBe.CustomerType;
                ddlCustomerType.SelectedIndex = ddlCustomerType.Items.IndexOf(ddlCustomerType.Items.FindByValue(objConsumerBe.CustomerTypeId.ToString()));
                //lblIsEmbassyCustomer.Text = objConsumerBe.IsEmbassyCustomer;
                //chkIsEmbassyCustomer.Checked = false;
                //txtEmbassyCode.Text = string.Empty;
                //chkIsEmbassyCustomer_Checked(null, null);
                if (objConsumerBe.IsEmbassyCustomer)
                {
                    chkIsEmbassyCustomer.Checked = true;
                    lblEmbassyChecked.Text = 1.ToString();
                    divEmbassyCode.Visible = true;
                    lblEmbassyCode.Text = txtEmbassyCode.Text = objConsumerBe.EmbassyCode;
                }
                else
                {
                    chkIsEmbassyCustomer.Checked = false;
                    lblEmbassyChecked.Text = 0.ToString();
                    divEmbassyCode.Visible = false;
                }
                chkIsVipCustomer.Checked = objConsumerBe.IsVIPCustomer;
                lblMinReading.Text = txtMinReading.Text = objConsumerBe.MinimumReading;

                lblMeterNo.Text = lblPhase.Text = lblMeterType.Text = lblMeeterSerialNo.Text = lblConnectionDate.Text = lblInitialReading.Text = lblCertifiedBy.Text = lblInstalledBy.Text = lblAgencyName.Text = lblContactPersonName.Text = lblAgencyDetails.Text = string.Empty;
                txtMeterNo.Text = txtMeeterSerialNo.Text = txtConnectionDate.Text = txtInitialReading.Text = txtContactPersonName.Text = txtAgencyDetails.Text = string.Empty;
                ddlMeterType.SelectedIndex = ddlPhase.SelectedIndex = ddlCertifiedBy.SelectedIndex = ddlInstalledBy.SelectedIndex = ddlAgencyName.SelectedIndex = 0;

                if (ddlReadCode.SelectedIndex != 1)
                {
                    lblMinReading.Text = string.Empty;
                    lblMeterNo.Text = txtMeterNo.Text = objConsumerBe.MeterNo;
                    lblPhase.Text = objConsumerBe.Phase;
                    ddlPhase.SelectedIndex = ddlPhase.Items.IndexOf(ddlPhase.Items.FindByValue(objConsumerBe.PhaseId.ToString()));
                    lblMeterType.Text = objConsumerBe.MeterType;
                    ddlMeterType.SelectedIndex = ddlMeterType.Items.IndexOf(ddlMeterType.Items.FindByValue(objConsumerBe.MeterTypeId.ToString()));
                    lblMeeterSerialNo.Text = txtMeeterSerialNo.Text = objConsumerBe.MeterSerialNo;
                    lblConnectionDate.Text = txtConnectionDate.Text = objConsumerBe.ConnectionDate;
                    lblMeeterStatus.Text = objConsumerBe.MeterStatus;
                    ddlMeeterStatus.SelectedIndex = objConsumerBe.MeterStatusId == 0 ? 0 : ddlMeeterStatus.Items.IndexOf(ddlMeeterStatus.Items.FindByValue(objConsumerBe.MeterStatusId.ToString()));
                    lblConnectionReason.Text = objConsumerBe.Reason;
                    ddlConnectionReason.SelectedIndex = ddlConnectionReason.Items.IndexOf(ddlConnectionReason.Items.FindByValue(objConsumerBe.ConnectionReasonId.ToString()));
                    lblInitialReading.Text = txtInitialReading.Text = objConsumerBe.InitialReading;
                    //lblCurrentReading.Text = txtCurrentReading.Text = objConsumerBe.CurrentReading;
                    lblAverageReading.Text = txtAverageReading.Text = objConsumerBe.AverageReading;
                    //lblSetupDate.Text = txtSetupDate.Text = objConsumerBe.SetupDate;
                    lblCertifiedBy.Text = objConsumerBe.CertifiedBy;
                    ddlCertifiedBy.SelectedIndex = ddlCertifiedBy.Items.IndexOf(ddlCertifiedBy.Items.FindByValue(objConsumerBe.CertifiedBy));
                    rblApplicationProccessedBy.SelectedIndex = rblApplicationProccessedBy.Items.IndexOf(rblApplicationProccessedBy.Items.FindByValue(objConsumerBe.ProcessedById.ToString()));
                    lblApplicationProccessedBy.Text = objConsumerBe.ProcessedById.ToString();
                    rblApplicationProccessedBy_SelectedIndexChanged(null, null);
                    if (objConsumerBe.ProcessedById == 1)
                    {
                        //divBEDCApplication.Visible = true;
                        //divApplicationByOthers.Style.Add("display", "none"); 
                        divApplicationByOthers.Visible = false;
                        rblApplicationProccessedBy.Enabled = false;
                    }
                    else
                    {
                        //divBEDCApplication.Style.Add("display", "none");
                        divBEDCApplication.Visible = false;
                        rblApplicationProccessedBy.Enabled = false;
                    }
                    lblInstalledBy.Text = objConsumerBe.InstalledBy;
                    ddlInstalledBy.SelectedIndex = ddlInstalledBy.Items.IndexOf(ddlInstalledBy.Items.FindByValue(objConsumerBe.InstalledBy));
                    lblAgencyName.Text = objConsumerBe.AgencyName;
                    ddlAgencyName.SelectedIndex = ddlAgencyName.Items.IndexOf(ddlAgencyName.Items.FindByValue(objConsumerBe.AgencyId.ToString()));
                    lblContactPersonName.Text = txtContactPersonName.Text = objConsumerBe.ContactPersonName;
                    lblAgencyDetails.Text = txtAgencyDetails.Text = objIdsignBal.ReplaceNewLines(objConsumerBe.AgencyDetails, false);
                }
                BindDocuments();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindTariffDetails()
        {
            try
            {
                XmlDocument xml = null;
                BillCalculatorBE objBillCalculatorBe = new BillCalculatorBE();
                BillCalculatorListCharges objBillCalculatorListChargesBe = new BillCalculatorListCharges();
                BillCalculatorListBE objBillCalculatorListBE = new BillCalculatorListBE();
                TimeZoneInfo IND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                DateTime currentTime = DateTime.Now;
                DateTime ourtime = TimeZoneInfo.ConvertTime(currentTime, IND_ZONE);

                objBillCalculatorBe.Date = ourtime.ToString("MM/dd/yyyy").Replace("-", "/");
                objBillCalculatorBe.Consumption = 1;
                objBillCalculatorBe.SubType = Convert.ToInt32(ddlTariff.SelectedValue);
                // _ObjBillCalculatorBE.Vat = Vat;
                objBillCalculatorBe.CustomerTypeId = Constants.ConsumptionTypeId;

                xml = objBillCalculatorBAL.CalculateBillWithConsumption(objBillCalculatorBe, ReturnType.Get);
                objBillCalculatorListBE = objIdsignBal.DeserializeFromXml<BillCalculatorListBE>(xml);
                objBillCalculatorListChargesBe = objIdsignBal.DeserializeFromXml<BillCalculatorListCharges>(xml);

                if (objBillCalculatorListChargesBe.items.Count > 0)
                {
                    lblTariffType.Text = objBillCalculatorListBE.items[0].TariffType;
                    divTariff.Visible = true;
                }
                gvTariffDetails.DataSource = objBillCalculatorListChargesBe.items;
                gvTariffDetails.DataBind();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        protected override void InitializeCulture()
        {
            if (Session[UMS_Resource.CULTURE] != null)
            {
                string culture = Session[UMS_Resource.CULTURE].ToString();

                Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
            }
            base.InitializeCulture();
        }

        #endregion

        #region web methods
        protected void ddlTariff_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlTariff.SelectedIndex > 0)
                    BindTariffDetails();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //[WebMethod]
        //public static string IsDocumentNoExists(string DocumentNo, string ConsumerNo)
        //{
        //    ConsumerBe objConsumerBe = new ConsumerBe();
        //    iDsignBAL objIdsignBal = new iDsignBAL();
        //    ConsumerBal objConsumerBal = new ConsumerBal();

        //    objConsumerBe.DocumentNo = DocumentNo;
        //    //objConsumerBe.CustomerUniqueNo = string.Empty;
        //    objConsumerBe.CustomerUniqueNo = ConsumerNo;
        //    objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.GetServiceDetails(objConsumerBe, ReturnType.Retrieve));
        //    return objConsumerBe.IsDocumentNoExists.ToString();
        //}

        [WebMethod]
        public static string IsSequenceNoExists(string SequenceNo, string ConsumerNo)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            iDsignBAL objIdsignBal = new iDsignBAL();
            ConsumerBal objConsumerBal = new ConsumerBal();

            objConsumerBe.CustomerSequenceNo = Convert.ToInt32(SequenceNo);
            objConsumerBe.CustomerUniqueNo = ConsumerNo;
            objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.GetServiceDetails(objConsumerBe, ReturnType.Set));
            return objConsumerBe.IsCustomerSequenceNo.ToString();
        }

        [WebMethod]
        public static string IsMeterNoExists(string MeterNo, string ConsumerNo)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            iDsignBAL objIdsignBal = new iDsignBAL();
            ConsumerBal objConsumerBal = new ConsumerBal();

            objConsumerBe.MeterNo = MeterNo;
            objConsumerBe.CustomerUniqueNo = ConsumerNo;
            objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Retrieve(objConsumerBe, ReturnType.Get));
            return objConsumerBe.IsMeterNoExists.ToString();
        }

        [WebMethod]
        public static string MeterDials(string MeterNo, string ConsumerNo)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            iDsignBAL objIdsignBal = new iDsignBAL();
            ConsumerBal objConsumerBal = new ConsumerBal();

            objConsumerBe.MeterNo = MeterNo;
            objConsumerBe.CustomerUniqueNo = ConsumerNo;
            objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Retrieve(objConsumerBe, ReturnType.Get));
            return (objConsumerBe.MeterDials.ToString());
        }

        [WebMethod]
        public static string IsMeterSerialNoExists(string MeterSerialNo, string ConsumerNo)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            iDsignBAL objIdsignBal = new iDsignBAL();
            ConsumerBal objConsumerBal = new ConsumerBal();

            objConsumerBe.MeterSerialNo = MeterSerialNo;
            objConsumerBe.CustomerUniqueNo = ConsumerNo;
            objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Retrieve(objConsumerBe, ReturnType.Single));
            return objConsumerBe.IsMeterSerialNoExists.ToString();
        }
        [WebMethod]
        public static string IsNeighborAccountNoExists(string NeighbourAccNo, string ConsumerNo)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            iDsignBAL objIdsignBal = new iDsignBAL();
            ConsumerBal objConsumerBal = new ConsumerBal();

            objConsumerBe.NeighborAccountNo = NeighbourAccNo;
            objConsumerBe.CustomerUniqueNo = ConsumerNo;
            objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Retrieve(objConsumerBe, ReturnType.Fetch));
            return objConsumerBe.IsNeighborAccountNoExists.ToString();
        }

        [WebMethod]
        public static string IsOldAccountNoExists(string OldAccountNo, string ConsumerNo)
        {
            ConsumerBe objConsumerBe = new ConsumerBe();
            iDsignBAL objIdsignBal = new iDsignBAL();
            ConsumerBal objConsumerBal = new ConsumerBal();

            objConsumerBe.OldAccountNo = OldAccountNo;
            objConsumerBe.CustomerUniqueNo = ConsumerNo;
            objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Retrieve(objConsumerBe, ReturnType.Bulk));
            return objConsumerBe.IsOldAccountNoExists.ToString();
        }
        #endregion
    }
}