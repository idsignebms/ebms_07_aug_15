﻿<%@ Page Title="::Generate Customer Bill::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    AutoEventWireup="true" CodeBehind="GenerateBillByCustomer.aspx.cs" Inherits="UMS_Nigeria.ConsumerManagement.GenerateBillByCustomer"
    Theme="Green" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControls/Authentication.ascx" TagName="Authentication" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <%--  <link href="../App_Themes/Green/style.css" rel="stylesheet" type="text/css" />--%>
    <script>
        function Validation() {
            var IsValid = true;
            var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo');

            if (TextBoxValidationlbl(txtAccountNo, document.getElementById("spanAccountNo"), "Global Account No / Old Account No") == false) IsValid = false;
            return IsValid;
        }
        function HideDownload() {
            var mpeAlert = $find('UMSNigeriaBody_mpeDownload');
            mpeAlert.hide();
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out_bor_Payment">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upPaymentEntry" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div id="divBillInput" runat="server">
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddState" runat="server" Text="Individual Customer Bill Generation"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="litCountryName" runat="server" Text="<%$ Resources:Resource, ACCNO_OLD_ACCNO%>"></asp:Literal>
                                <span class="span_star">*</span><div class="space">
                                </div>
                                <asp:TextBox CssClass="text-box" runat="server" ID="txtAccountNo" MaxLength="20"
                                    onblur="return TextBoxBlurValidationlbl(this,'spanAccountNo','Global Account No / Old Account No')"
                                    onkeypress="GetAccountDetails(event)" placeholder="Enter Global Account No / Old Account No"></asp:TextBox>
                                &nbsp
                                <asp:FilteredTextBoxExtender ID="ftbAccNo" runat="server" TargetControlID="txtAccountNo"
                                    FilterType="Numbers" ValidChars=" ">
                                </asp:FilteredTextBoxExtender>
                                <asp:LinkButton runat="server" Visible="false" ID="lblSearch" OnClick="lblSearch_Click"
                                    CssClass="search_img"></asp:LinkButton>
                                <br />
                                <span id="spanAccountNo" class="span_color"></span>
                            </div>
                        </div>
                    </div>
                    <div class="box_total">
                        <div class="box_total_a" style="margin-left: 15px;">
                            <asp:Button ID="btnGo" Text="<%$ Resources:Resource, GET_CUSTOMER_DETAILS%>" CssClass="box_s"
                                runat="server" OnClick="btnGo_Click" OnClientClick="return Validation();" />
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <br />
                    <div id="divCustomerDetails" runat="server" visible="false">
                        <div class="out-bor_a">
                            <div class="inner-sec">
                                <div class="heading" style="padding-left: 16px; padding-top: 17px;">
                                    <asp:Literal ID="Literal213" runat="server" Text="<%$ Resources:Resource, CUSTOMER_DETAILS%>"></asp:Literal>
                                </div>
                                <div class="out-bor_aTwo">
                                    <table class="customerdetailsTamle">
                                        <tr>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label runat="server" ID="lblAccNoAndGlobalAccNo"></asp:Label>
                                                <asp:Label runat="server" Visible="false" ID="lblAccountno"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal9" runat="server" Text='<%$ Resources:Resource, OLD_ACCOUNT_NO %>'></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label ID="lblOldAccountNo" runat="server"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label runat="server" ID="lblServiceAddress"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal3" runat="server" Text="Name"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label runat="server" ID="lblName"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, ROUTE_SEQUENCE_NO%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Literal ID="litRouteSequenceNo" runat="server"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal6" runat="server" Text="Read Code"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Literal ID="lblReadType" runat="server" Text=""></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal2" runat="server" Text="Service Unit"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label runat="server" ID="lblServiceUnit"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal4" runat="server" Text="Service Center"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label ID="lblServiceCenter" runat="server"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal5" runat="server" Text="Book Group"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Literal ID="lblBookGroup" runat="server"></asp:Literal>
                                                <asp:Literal ID="lblBookGroupID" runat="server" Visible="false"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal11" runat="server" Text='<%$ Resources:Resource, BOOK_NAME %>'></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Literal ID="lblBookName" runat="server"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, OUT_STANDING_AMT%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label ID="lblTotalDueAmount" runat="server"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                            </td>
                                            <td class="customerdetailstamleTd">
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div id="divAssignMeter" runat="server" visible="false" style="width: auto;">
                                <div class="consume_name" style="font-weight: bold; padding-left: 15px;">
                                    <asp:Literal ID="Literal1" runat="server" Text="Bill will generate for "></asp:Literal>&nbsp;:&nbsp;<asp:Label
                                        ID="lblYear" runat="server"></asp:Label>&nbsp;
                                    <asp:Label ID="lblMonth" runat="server"></asp:Label>
                                </div>
                                <div>
                                    <asp:Button ID="btnGoToBilling" runat="server" Text="<%$ Resources:Resource, GENERATE_CUST_BILL%>"
                                        CssClass="box_s" OnClick="btnGoToBilling_Click" /></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="divAlert" runat="server" visible="false">
                    <div class="info">
                        Bill Generate</div>
                    <div class="error">
                        Before generate bill kindly follow below points:<br />
                        <br />
                        1. Open Bill Month and Year.<br />
                        2. Add reading to Read Customers.<br />
                        3. Do Estimation for Direct Customers.<br />
                    </div>
                    <%--   Click To Go Back&nbsp;
                    <asp:LinkButton ID="lnkNewRegstration" runat="server" Text="Here" OnClick="lnkNewRegstration_Click"></asp:LinkButton>.--%>
                </div>
                <asp:ModalPopupExtender ID="mpeAlert" runat="server" TargetControlID="hdnCancelCNA"
                    PopupControlID="pnlAlert" BackgroundCssClass="popbg" CancelControlID="btnBPOK">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hdnCancelCNA" runat="server"></asp:HiddenField>
                <asp:Panel runat="server" ID="pnlAlert" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblAlertMsg" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnBPOK" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="btn_ok"
                                Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeMeterDetails" runat="server" TargetControlID="hdnMeterDetails"
                    PopupControlID="pnlMeterDetials" BackgroundCssClass="popbg" CancelControlID="btnAssignNew">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hdnMeterDetails" runat="server"></asp:HiddenField>
                <asp:Panel runat="server" ID="pnlMeterDetials" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblMeterDetails" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="clear pad_10">
                    </div>
                    <div class="consumer_feild">
                        <div class="consume_name">
                            <asp:Literal ID="Literal13" runat="server" Text="Global Account No."></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblGlobalAccountNo" runat="server"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="Literal14" runat="server" Text="Meter No."></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblMeterNo" runat="server"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="Literal15" runat="server" Text="Change Date"></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblChangeDate" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btnAssignNew" runat="server" Text="Assign New" CssClass="btn_ok"
                                Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeDownload" runat="server" TargetControlID="hfDwnload"
                    PopupControlID="pnlDownload" BackgroundCssClass="popbg" CancelControlID="btnCancelDwnld">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hfDwnload" runat="server"></asp:HiddenField>
                <asp:Panel runat="server" ID="pnlDownload" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblDwnldMessage" runat="server" Text="Do you want to download Bill file?"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnCancelDwnld" runat="server" Text="<%$ Resources:Resource, NO%>"
                                CssClass="btn_ok" Style="margin-left: 10px;" />
                            <asp:Button ID="btnDownload" runat="server" Text="<%$ Resources:Resource, YES%>"
                                OnClientClick="return HideDownload();" CssClass="btn_ok" OnClick="btnDownload_Click"
                                Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="hdnCustomerTypeID" runat="server" Value="" />
                <asp:HiddenField ID="hfBillfileName" runat="server" Value="" />
                <asp:HiddenField ID="hfBillfilePath" runat="server" Value="" />
                </label>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnDownload" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:HiddenField ID="hfMonthID" runat="server" />
        <asp:HiddenField ID="hfYearID" runat="server" />
        <uc1:Authentication ID="ucAuthentication" runat="server" />
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        //        $(document).keydown(function (e) {
        //            // ESCAPE key pressed 
        //            if (e.keyCode == 27) {
        //                $(".rnkland").fadeOut("slow");
        //                $(".mpeConfirmDelete").fadeOut("slow");
        //                document.getElementById("overlay").style.display = "none";
        //            }
        //        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        //        function pageLoad() {
        //            $get("<%=txtAccountNo.ClientID%>").focus();
        //        };


        $(document).ready(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
            // AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
            //AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);
        });
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../Javascript/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/AssignMeters.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
</asp:Content>
