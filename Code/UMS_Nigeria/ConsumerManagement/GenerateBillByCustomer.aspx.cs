﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Xml;
using System.Data;
using Resources;
using System.IO;
using System.Globalization;
using System.Configuration;
using System.Xml.Xsl;
using System.Text;
using Communication;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class GenerateBillByCustomer : System.Web.UI.Page
    {
        #region Members
        CustomerDetailsBAL _ObjCustomerDetailsBAL = new CustomerDetailsBAL();
        CustomerDetailsBe _ObjCustomerDetailsBe = new CustomerDetailsBe();
        MastersBAL _ObjMastersBAL = new MastersBAL();
        ReportsBal objReportsBal = new ReportsBal();
        MastersBE _ObjeMastersBE = new MastersBE();
        XmlDocument _objXmlDocument = new XmlDocument();
        MastersListBE objMastersListBE = new MastersListBE();
        iDsignBAL _ObjIdsignBal = new iDsignBAL();
        ConsumerBal objConsumerBal = new ConsumerBal();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        CustomerRegistrationBE objCustomerRegistrationBE = new CustomerRegistrationBE();
        BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
        BillGenerationBe objBillGenerationBe = new BillGenerationBe();
        public string Key = "AssignMeter";
        string AccountNo;
        #endregion

        #region Properties
        private int YearId
        {
            //get { return string.IsNullOrEmpty(ddlYear.SelectedValue) ? 0 : Convert.ToInt32(ddlYear.SelectedValue); }
            get { return string.IsNullOrEmpty(hfYearID.Value) ? 0 : Convert.ToInt32(hfYearID.Value); }
        }

        private int MonthId
        {
            //get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? 0 : Convert.ToInt32(ddlMonth.SelectedValue); }
            get { return string.IsNullOrEmpty(hfMonthID.Value) ? 0 : Convert.ToInt32(hfMonthID.Value); }
        }

        private string Cycles
        {
            get { return lblBookGroupID.Text; }

        }
        #endregion

        #region Methods

        private void BindUserDetails()
        {
            XmlDocument xml = new XmlDocument();
            try
            {
                objCustomerRegistrationBE.GolbalAccountNumber = txtAccountNo.Text;
                xml = objConsumerBal.GetCustomerForBillGen(objCustomerRegistrationBE);
                if (xml != null)
                {
                    objCustomerRegistrationBE = _ObjIdsignBal.DeserializeFromXml<CustomerRegistrationBE>(xml);
                    if (objCustomerRegistrationBE.IsSuccessful)
                    {
                        divAssignMeter.Visible = divCustomerDetails.Visible = true;
                        lblAccountno.Text = objCustomerRegistrationBE.AccountNo;
                        lblAccNoAndGlobalAccNo.Text = objCustomerRegistrationBE.GlobalAccNoAndAccNo;
                        lblServiceAddress.Text = objCustomerRegistrationBE.FullServiceAddress;
                        lblName.Text = objCustomerRegistrationBE.FirstNameLandlord;
                        //lblName.Text = objCustomerRegistrationBE.Name;
                        litRouteSequenceNo.Text = objCustomerRegistrationBE.RouteName;// Convert.ToString(objCustomerRegistrationBE.RouteSequenceNumber);
                        lblTotalDueAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(objCustomerRegistrationBE.OutStandingAmount.ToString()), 2, Constants.MILLION_Format).ToString(); //Raja-ID065 For Getting amt with comma seperated
                        hdnCustomerTypeID.Value = Convert.ToString(objCustomerRegistrationBE.CustomerTypeId);
                        lblServiceUnit.Text = objCustomerRegistrationBE.ServiceUnitName;
                        lblServiceCenter.Text = objCustomerRegistrationBE.ServiceCenterName;
                        lblBookGroup.Text = objCustomerRegistrationBE.CycleName;
                        lblBookName.Text = objCustomerRegistrationBE.BookNo;
                        lblOldAccountNo.Text = objCustomerRegistrationBE.OldAccountNo;
                        lblBookGroupID.Text = objCustomerRegistrationBE.CycleId;
                        if (objCustomerRegistrationBE.ReadCodeID == (int)ReadCode.Read)
                        {
                            lblReadType.Text = ReadCode.Read.ToString();
                        }
                        else if (objCustomerRegistrationBE.ReadCodeID == (int)ReadCode.Direct)
                        {
                            lblReadType.Text = ReadCode.Direct.ToString();
                        }
                    }
                    else
                    {
                        divAssignMeter.Visible = divCustomerDetails.Visible = false;
                        ClearFields();
                        Message("No customer found.", UMS_Resource.MESSAGETYPE_SUCCESS);
                    }
                }
            }
            catch (Exception ex)
            {
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                Message("Error in customer search.", UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void ClearFields()
        {
            txtAccountNo.Text = lblAccountno.Text = lblName.Text = lblServiceAddress.Text = lblTotalDueAmount.Text = litRouteSequenceNo.Text = lblReadType.Text = string.Empty;
        }

        private void BindYears()
        {
            try
            {

                XmlDocument xmlResult = new XmlDocument();
                xmlResult = _objBillGenerationBal.GetDetails(ReturnType.Multiple);
                BillGenerationListBe objBillsListBE = _ObjIdsignBal.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                if (objBillsListBE.Items.Count > 0)
                {
                    lblYear.Text = objBillsListBE.Items[0].Year.ToString();
                    hfYearID.Value = objBillsListBE.Items[0].Year.ToString();
                    // _objIdsignBal.FillDropDownList(_objIdsignBal.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlYear, "Year", "Year", "Year", true);
                    BindMonths();
                    divAlert.Visible = false;
                    divBillInput.Visible = true;
                }
                else
                {
                    divAlert.Visible = true;
                    divBillInput.Visible = false;
                }

                /// Old Code
                //int CurrentYear = DateTime.Now.Year;
                //int FromYear = Convert.ToInt32(GlobalConstants.TARIFF_ENTRY_NO_OF_LAST_YEARS);
                //for (int i = FromYear; i < CurrentYear; i++)
                //{
                //    ListItem item = new ListItem();
                //    item.Text = item.Value = i.ToString();
                //    ddlYear.Items.Add(item);

                //}
                //for (int i = 0; i <= Convert.ToInt32(GlobalConstants.TARIFF_ENTRY_NO_OF_PRESENT_AND_FUTURE_YEARS); i++)
                //{
                //    ListItem item = new ListItem();
                //    item.Text = item.Value = (CurrentYear + i).ToString();
                //    ddlYear.Items.Add(item);
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindMonths()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                _objBillGenerationBe.Year = this.YearId;
                xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.Get);
                BillGenerationListBe objBillsListBE = _ObjIdsignBal.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                if (objBillsListBE.Items.Count > 0)
                {
                    lblMonth.Text = objBillsListBE.Items[0].MonthName;
                    hfMonthID.Value = objBillsListBE.Items[0].Month.ToString();
                    divAlert.Visible = false;
                    divBillInput.Visible = true;
                    // _objIdsignBal.FillDropDownList(_objIdsignBal.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlMonth, "MonthName", "Month", "Month", true);
                }
                else
                {
                    divAlert.Visible = true;
                    divBillInput.Visible = false;
                }
                //DataSet dsMonths = new DataSet();
                //dsMonths.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["Months"]));
                //objIdsignBal.FillDropDownList(dsMonths.Tables[0], ddlMonth, "name", "value", UMS_Resource.DROPDOWN_SELECT, false);

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void CreateCustomersBillFile(string GlobalAccountNumber, int MonthId, int YearId)
        {
            GetPDF(GlobalAccountNumber, MonthId, YearId);
        }
        private void GetPDF(string GlobalAccountNumber, int MonthId, int YearId)
        {
            CustomerBillPDFBE objBE = new CustomerBillPDFBE();
            CustomerBillDetailsListBE objListBE = new CustomerBillDetailsListBE();

            objBE.BillMonth = string.IsNullOrEmpty(MonthId.ToString()) ? 0 : Convert.ToInt32(MonthId);
            objBE.BillYear = string.IsNullOrEmpty(YearId.ToString()) ? 0 : Convert.ToInt32(YearId);
            objBE.GlobalAccountNo = string.IsNullOrEmpty(GlobalAccountNumber) ? string.Empty : GlobalAccountNumber;

            XmlDocument xml = _objBillGenerationBal.GetCustomerBillDetailsForPDF(objBE);

            if (xml.InnerXml != null)
            {
                string strXSLTFile = Server.MapPath(ConfigurationManager.AppSettings["CustomerBillPDF"].ToString());

                //string strXMLFile = Server.MapPath("XSLT/PDFOutput_Test.xml");
                //XmlReader reader = XmlReader.Create(strXMLFile);
                //XmlReader reader = XmlReader.Create(xml);

                XslCompiledTransform objXSLTransform = new XslCompiledTransform();
                objXSLTransform.Load(strXSLTFile);
                StringBuilder htmlOutput = new StringBuilder();
                TextWriter htmlWriter = new StringWriter(htmlOutput);
                objXSLTransform.Transform(xml, null, htmlWriter);
                string htmltext = htmlOutput.ToString();

                //reader.Close();

                objListBE = _ObjIdsignBal.DeserializeFromXml<CustomerBillDetailsListBE>(xml);

                if (objListBE.Items.Count > 0)
                {
                    string BusinessUnit = objListBE.Items[0].BusinessUnit;
                    string ServiceUnit = objListBE.Items[0].ServiceUnit;
                    string ServiceCenter = objListBE.Items[0].ServiceCenter;
                    string BookGroup = objListBE.Items[0].BookGroup;
                    string BookNo = objListBE.Items[0].BookNo;
                    string GlobalAccountNo = objListBE.Items[0].GlobalAccountNo;
                    string MonthName = objListBE.Items[0].MonthName;
                    int Year = objListBE.Items[0].Year;

                    string path = Server.MapPath(ConfigurationManager.AppSettings["BillGenerationPDF"].ToString());

                    // Determine whether the directory exists. 
                    if (!Directory.Exists(path))
                    {
                        // Try to create the directory.
                        DirectoryInfo di = Directory.CreateDirectory(path);
                    }
                    if (!Directory.Exists(path + "\\" + BusinessUnit))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path + "\\" + BusinessUnit);
                    }
                    if (!Directory.Exists(path + "\\" + BusinessUnit + "\\" + ServiceUnit))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path + "\\" + BusinessUnit + "\\" + ServiceUnit);
                    }
                    if (!Directory.Exists(path + "\\" + BusinessUnit + "\\" + ServiceUnit + "\\" + ServiceCenter))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path + "\\" + BusinessUnit + "\\" + ServiceUnit + "\\" + ServiceCenter);
                    }
                    if (!Directory.Exists(path + "\\" + BusinessUnit + "\\" + ServiceUnit + "\\" + ServiceCenter + "\\" + BookGroup))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path + "\\" + BusinessUnit + "\\" + ServiceUnit + "\\" + ServiceCenter + "\\" + BookGroup);
                    }
                    if (!Directory.Exists(path + "\\" + BusinessUnit + "\\" + ServiceUnit + "\\" + ServiceCenter + "\\" + BookGroup + "\\" + BookNo))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path + "\\" + BusinessUnit + "\\" + ServiceUnit + "\\" + ServiceCenter + "\\" + BookGroup + "\\" + BookNo);
                    }

                    string FileName = GlobalAccountNo + "-" + MonthName + "-" + Year + ".pdf";

                    if (File.Exists(path + "\\" + BusinessUnit + "\\" + ServiceUnit + "\\" + ServiceCenter + "\\" + BookGroup + "\\" + BookNo + "\\" + FileName))
                    {
                        File.Delete(path + "\\" + BusinessUnit + "\\" + ServiceUnit + "\\" + ServiceCenter + "\\" + BookGroup + "\\" + BookNo + "\\" + FileName);
                    }

                    string PDFFilePath = "\\GeneratedBills\\CustomerPDFBills\\" + BusinessUnit + "\\" + ServiceUnit + "\\" + ServiceCenter + "\\" + BookGroup + "\\" + BookNo + "\\" + FileName;
                    GeneratePDF _objGeneratePDF = new GeneratePDF();
                    string pdfFileName = Request.PhysicalApplicationPath + PDFFilePath;// "\\XSLT\\" + "PDFSample4.pdf";
                    _objGeneratePDF.ConvertHTMLToPDF(htmltext, pdfFileName);

                    #region SaveFilePath
                    CustomerBillPDFDetailsBE _objCustomerBillPDFDetailsBE = new CustomerBillPDFDetailsBE();
                    _objCustomerBillPDFDetailsBE.GlobalAccountNo = objListBE.Items[0].GlobalAccountNo;
                    _objCustomerBillPDFDetailsBE.BillMonth = MonthId;
                    _objCustomerBillPDFDetailsBE.BillYear = YearId;
                    _objCustomerBillPDFDetailsBE.BillNo = objListBE.Items[0].BillNo;
                    _objCustomerBillPDFDetailsBE.FilePath = PDFFilePath;
                    _objCustomerBillPDFDetailsBE.CustomerBillId = objListBE.Items[0].CustomerBillId;
                    _objCustomerBillPDFDetailsBE.EmailId = objListBE.Items[0].BulkEmails;
                    _objCustomerBillPDFDetailsBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();

                    _objBillGenerationBal.InsertCustomerBillDetailsForPDF(_objCustomerBillPDFDetailsBE);

                    hfBillfilePath.Value = PDFFilePath;
                    hfBillfileName.Value = FileName;
                    mpeDownload.Show();
                    #endregion

                    #region Mail&SMS
                    string SuccessMessage = string.Empty;
                    SuccessMessage = "Bill generated successfully.";
                    CommunicationFeaturesBE objCommunicationFeaturesBE = new CommunicationFeaturesBE();
                    objCommunicationFeaturesBE.CommunicationFeaturesID = (int)CommunicationFeatures.BillGeneration;

                    try
                    {
                        if (_ObjCommonMethods.IsEmailFeatureEnabled(objCommunicationFeaturesBE))
                        {

                            CommunicationFeaturesBE _objMailBE = new CommunicationFeaturesBE();
                            _objMailBE.GlobalAccountNumber = objListBE.Items[0].GlobalAccountNo;
                            XmlDocument xmlDoc = _ObjCommonMethods.GetDetails(_objMailBE, ReturnType.Fetch);
                            _objMailBE = _ObjIdsignBal.DeserializeFromXml<CommunicationFeaturesBE>(xmlDoc);

                            if (_objMailBE.EmailId == "0")
                            {
                                SuccessMessage += "Customer not having Mail Id.";
                            }
                            else if (!string.IsNullOrEmpty(_objMailBE.EmailId))
                            {
                                //decimal BillAmtWithTax = objListBE.Items[0].BillAmount;
                                //string totalBillamount = _ObjCommonMethods.GetCurrencyFormat(BillAmtWithTax, 2, Constants.MILLION_Format); //vat+BillAmount -TotalBillwithtax
                                string htmlBody = string.Empty;
                                htmlBody = _ObjCommonMethods.GetMailCommTemp(CommunicationFeatures.BillGeneration, objListBE.Items[0].Name, objListBE.Items[0].MonthName + "-" + Convert.ToString(objListBE.Items[0].Year), objListBE.Items[0].GlobalAccountNo, objListBE.Items[0].MeterNo, objListBE.Items[0].BillAmount, objListBE.Items[0].NetAmountPayable, objListBE.Items[0].DueDate, string.Empty, string.Empty, string.Empty, "THIS BILLING IS FOR " + objListBE.Items[0].MonthName + "-" + Convert.ToString(objListBE.Items[0].Year) + " CONSUMPTION.");

                                _ObjCommonMethods.sendmail(htmlBody, ConfigurationManager.AppSettings["SMTPUsername"].ToString(), "Bill Generated for acc " + objListBE.Items[0].GlobalAccountNo, _objMailBE.EmailId, pdfFileName);
                                SuccessMessage += "Mail sent successfully. ";
                            }
                            else
                            {
                                SuccessMessage += "Mail not sent to this customer.";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                        try
                        {
                            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                        }
                        catch (Exception logex)
                        {

                        }
                    }

                    try
                    {
                        if (_ObjCommonMethods.IsSMSFeatureEnabled(objCommunicationFeaturesBE))
                        {
                            CommunicationFeaturesBE _objSmsBE = new CommunicationFeaturesBE();
                            _objSmsBE.GlobalAccountNumber = objListBE.Items[0].GlobalAccountNo;
                            XmlDocument xmlSMS = _ObjCommonMethods.GetDetails(_objSmsBE, ReturnType.Get);
                            _objSmsBE = _ObjIdsignBal.DeserializeFromXml<CommunicationFeaturesBE>(xmlSMS);

                            if (_objSmsBE.MobileNo == "0")
                            {
                                SuccessMessage += "Customer not having Mobile No.";
                            }
                            else if (!string.IsNullOrEmpty(_objSmsBE.MobileNo) && _objSmsBE.MobileNo.Length == 10)
                            {
                                //decimal BillAmtWithTax = totalBillAmount + vatAmount;
                                //string totalBillamount = _ObjCommonMethods.GetCurrencyFormat(BillAmtWithTax, 2, Constants.MILLION_Format); //vat+BillAmount -TotalBillwithtax
                                string Messsage = string.Empty;
                                Messsage = _ObjCommonMethods.GetSMSCommTemp(CommunicationFeatures.BillGeneration, objListBE.Items[0].GlobalAccountNo, objListBE.Items[0].MonthName + "-" + Convert.ToString(objListBE.Items[0].Year), Convert.ToString(objListBE.Items[0].BillAmount), objListBE.Items[0].DueDate, string.Empty, string.Empty);
                                //Messsage = _ObjCommonMethods.GetSMSCommTemp(CommunicationFeatures.BillGeneration, Convert.ToString(dr["AccountNo"]), Convert.ToString(dr["MonthName"]) + "-" + Convert.ToString(dr["BillYear"]), Convert.ToString(dr["BillAmount"]), Convert.ToString(dr["BillDueDate"]), string.Empty);//Parameter has to be check
                                _ObjCommonMethods.sendSMS(_objSmsBE.MobileNo, Messsage, ConfigurationManager.AppSettings["SMTPUsername"].ToString());
                                SuccessMessage += "Message sent successfully.";
                            }
                            else
                            {
                                SuccessMessage += "Message not sent to this customer.";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                        try
                        {
                            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                        }
                        catch (Exception logex)
                        {

                        }
                    }
                    Message(SuccessMessage, UMS_Resource.MESSAGETYPE_SUCCESS);

                    divCustomerDetails.Visible = false;
                    txtAccountNo.Text = string.Empty;
                    #endregion
                }
            }
        }

        //private void CreateCustomersBillFile(DataSet ds)
        //{
        //    try
        //    {
        //        string filePath = null;
        //        string filePathforDB = null;
        //        filePath = Server.MapPath(ConfigurationManager.AppSettings["BillGeneration"].ToString());
        //        if (ds.Tables.Count > 0)
        //        {
        //            if (ds.Tables[0].Rows.Count > 0)
        //            {
        //                string str = null;
        //                string[] strArr = null;
        //                char[] splitchar = { ' ' };
        //                string adjustment = "0.00";
        //                int rowCount = 0;
        //                string billDetails = string.Empty;
        //                string totalPayment = "0.00";
        //                decimal totalPaymentAmount = 0;
        //                string FileName = string.Empty;



        //                foreach (DataRow dr in ds.Tables[0].Rows)
        //                {
        //                    rowCount = rowCount + 1;

        //                    decimal previouseBalanceAmount = Convert.ToDecimal(dr["PreviousBalance"]);
        //                    string previouseBalnce = _ObjCommonMethods.GetCurrencyFormat(previouseBalanceAmount, 2, Constants.MILLION_Format);// string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", previouseBalanceAmount);
        //                    FileName = Convert.ToString(dr["AccountNo"]) + "-" + Convert.ToString(dr["MonthName"]) + "-" + Convert.ToString(dr["BillYear"]) + ".txt";
        //                    //filePath += FileName;
        //                    filePath += FileName;
        //                    filePathforDB = FileName;
        //                    if (File.Exists(filePath))
        //                    {
        //                        try
        //                        {
        //                            File.Delete(filePath);
        //                        }
        //                        catch (Exception ex)
        //                        {
        //                            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //                        }
        //                    }
        //                    FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
        //                    //set up a streamwriter for adding text
        //                    StreamWriter sw = new StreamWriter(fs);

        //                    //find the end of the underlying filestream
        //                    sw.BaseStream.Seek(0, SeekOrigin.End);
        //                    //string accountNumbers = string.Empty;
        //                    str = dr["AdjustmentAmmount"].ToString().TrimEnd();
        //                    strArr = str.Split(splitchar);
        //                    decimal adjustmentAmount = Convert.ToDecimal(strArr[0]);


        //                    if (adjustmentAmount != 0)
        //                        adjustment = _ObjCommonMethods.GetCurrencyFormat(adjustmentAmount, 2, Constants.MILLION_Format); //string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", adjustmentAmount) + " " + strArr[1];

        //                    if (!string.IsNullOrWhiteSpace(dr["TotalPayment"].ToString()))
        //                    {
        //                        totalPaymentAmount = Convert.ToDecimal(dr["TotalPayment"]);
        //                        totalPayment = _ObjCommonMethods.GetCurrencyFormat(totalPaymentAmount, 2, Constants.MILLION_Format);  //string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", totalPaymentAmount);
        //                    }

        //                    decimal netFixedChargesAmount = Convert.ToDecimal(dr["NetFixedCharges"]);
        //                    string netFixedCharges = _ObjCommonMethods.GetCurrencyFormat(netFixedChargesAmount, 2, Constants.MILLION_Format); //string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", netFixedChargesAmount);

        //                    decimal netArrearsAmount = previouseBalanceAmount - adjustmentAmount - totalPaymentAmount;
        //                    string netArrears = _ObjCommonMethods.GetCurrencyFormat(netArrearsAmount, 2, Constants.MILLION_Format); //string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", netArrearsAmount);

        //                    decimal netEnergyChargesAmount = Convert.ToDecimal(dr["NetEnergyCharges"]);
        //                    string netEnergyCharges = _ObjCommonMethods.GetCurrencyFormat(netEnergyChargesAmount, 2, Constants.MILLION_Format); // string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", netEnergyChargesAmount);

        //                    //double lastPaidAmount = Convert.ToDouble(dr["LastPaidAmount"]);
        //                    //string lastPaidAmountValue = string.Format(CultureInfo.InvariantCulture, "{0:#,#}", netEnergyChargesAmount);

        //                    decimal vatAmount = Convert.ToDecimal(dr["VAT"]);
        //                    string vat = _ObjCommonMethods.GetCurrencyFormat(vatAmount, 2, Constants.MILLION_Format); //string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", vatAmount);

        //                    decimal totalBillAmount = Convert.ToDecimal(dr["TotalBillAmount"]);
        //                    string totalBill = _ObjCommonMethods.GetCurrencyFormat(totalBillAmount, 2, Constants.MILLION_Format); //string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", totalBillAmount);

        //                    decimal totalBillAmountWithTaxAmount = totalBillAmount + netArrearsAmount + vatAmount;
        //                    string totalBillAmountWithTax = _ObjCommonMethods.GetCurrencyFormat(totalBillAmountWithTaxAmount, 2, Constants.MILLION_Format); //string.Format(CultureInfo.InvariantCulture, "{0:#,#.00}", totalBillAmountWithTaxAmount);


        //                    //accountNumbers = string.IsNullOrEmpty(accountNumbers) ? dr["AccountNo"].ToString() : accountNumbers + "," + dr["AccountNo"].ToString();
        //                    billDetails += string.Format("{0,-6}#{1,-107}#{2,-11}", string.Empty, rowCount, rowCount).Replace('#', ' ') +
        //                                            Environment.NewLine + string.Format("{0,-15}#{1,-74}#{2,-11}#{3,-6}", "Route: 0", "Seq.", "Route: 0", "Seq.").Replace('#', ' ') +
        //                                            Environment.NewLine + Environment.NewLine +
        //                                            Environment.NewLine + string.Format("{0,-34}#{1,-63}#{2,-25}", string.Empty, dr["BusinessUnitName"].ToString() + " BUSINESS UNIT", dr["BusinessUnitName"].ToString() + " Business Unit").Replace('#', ' ') +
        //                                            Environment.NewLine + string.Format("{0,-23}#{1,-74}#{2,-9}", string.Empty, "PLS PAY AT ANY BEDC OFFICE OR DESIGNATED BANKS", dr["MonthName"].ToString() + dr["BillYear"].ToString()).Replace('#', ' ') +
        //                        //Environment.NewLine + string.Format("{0,-31}#{1,-27}#{2,-36}#{3,-22}", string.Empty, "PRIVATE ACCOUNT", dr["MonthName"].ToString() + dr["BillYear"].ToString(), "Private Account", dr["MonthName"].ToString() + dr["BillYear"].ToString()).Replace('#', ' ') +
        //                                            Environment.NewLine + string.Format("{0,-31}#{1,-27}#{2,-36}#{3,-22}", string.Empty, dr["ClassName"] + " Account", dr["MonthName"].ToString() + dr["BillYear"].ToString(), dr["ClassName"] + " Account", dr["MonthName"].ToString() + dr["BillYear"].ToString()).Replace('#', ' ') +
        //                                             Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", "Bill No ", dr["BillNo"].ToString(), string.Empty, "Bill No", dr["BillNo"].ToString()).Replace('#', ' ') +
        //                                            Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, dr["AccountNo"].ToString(), string.Empty, string.Empty, dr["AccountNo"].ToString()).Replace('#', ' ') +
        //                                            Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, dr["Name"].ToString(), dr["LastDueDate"].ToString(), previouseBalnce, dr["Name"].ToString()).Replace('#', ' ') +
        //                                            Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, Convert.ToString(dr["ServiceAddress"]).Replace("\n", Environment.NewLine) + " ", dr["MeterNo"].ToString(), totalPayment, dr["ServiceAddress"].ToString()).Replace('#', ' ') +
        //                        //Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, Convert.ToString(dr["ServiceAddress"]).Replace("\n", Environment.NewLine) + " ", dr["MeterNo"].ToString(), totalPayment, dr["PostalAddress"].ToString()).Replace('#', ' ') +
        //                        //Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, dr["PostalZipCode"].ToString(), netFixedCharges, adjustment, dr["ServiceHouseNo"].ToString() + " " + dr["ServiceStreet"].ToString()).Replace('#', ' ') +
        //                        //Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, dr["PostalZipCode"].ToString(), dr["ADC"], adjustment, dr["ServiceHouseNo"].ToString() + " " + dr["ServiceStreet"].ToString()).Replace('#', ' ') +
        //                                            Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, dr["PostalZipCode"].ToString(), dr["ADC"], adjustment, string.Empty + " " + string.Empty).Replace('#', ' ') +
        //                                            Environment.NewLine + string.Format("{0,8}#{1,-38}#{2,-28}#{3,-22}#{4,-30}", string.Empty, string.Empty, dr["Dials"].ToString(), netArrears, dr["ServiceZipCode"].ToString()).Replace('#', ' ') +
        //                                            Environment.NewLine + string.Format("{0,8}#{1,-90}#{2,-15}", string.Empty, string.Format("THIS BILLING IS FOR {0} CONSUMPTION.", dr["BillingMonthName"]), dr["MeterNo"].ToString()).Replace('#', ' ') +
        //                                            Environment.NewLine + Environment.NewLine + Environment.NewLine +
        //                                            Environment.NewLine + string.Format("{0,-13}#{1,-7}#{2,-16}#{3,-12}#{4,-7}#{5,-7}#{6,7}#{7,11}", "ENERGY", dr["TariffName"].ToString(), dr["ReadDate"].ToString(), dr["PresentReading"].ToString(), dr["PreviousReading"].ToString(), dr["Multiplier"].ToString(), dr["Usage"].ToString(), netEnergyCharges).Replace('#', ' ') +
        //                                            Environment.NewLine +
        //                                            Environment.NewLine + string.Format("{0,-13}#{1,-7}#{2,-16}#{3,-12}#{4,-7}#{5,-7}#{6,7}#{7,11}", "FIXED", dr["TariffName"].ToString(), "----", "----", "----", "----", "----", netFixedCharges).Replace('#', ' ') +
        //                                            Environment.NewLine + string.Format("{0,-22}#{1,-19}", string.Empty, "Billing Periods: 01").Replace('#', ' ') +
        //                                            Environment.NewLine +
        //                                            Environment.NewLine + string.Format("{0,-33}#{1,-22}", "LAST PAYMENT DATE  " + dr["LastPaymentDate"].ToString(), "AMOUNT      " + dr["LastPaidAmount"].ToString()).Replace('#', ' ') +
        //                                            Environment.NewLine + string.Format("{0,-100}#{1,-11}", "RECONNECTION FEE IS NOW =N=5000", dr["LastDueDate"].ToString()).Replace('#', ' ') +
        //                                            Environment.NewLine + string.Format("{0,-70}#{1,17}#{2,27}", "PAY WITHIN " + dr["LastDueDays"].ToString() + " DAYS TO AVOID DISCONNECTION", netArrears, netArrears).Replace('#', ' ') +
        //                                            Environment.NewLine + string.Format("{0,-70}#{1,17}#{2,27}", string.Empty, totalBill, totalBill).Replace('#', ' ') +
        //                                            Environment.NewLine + string.Format("{0,-70}#{1,17}#{2,27}", string.Empty, vat, vat).Replace('#', ' ') +
        //                                            Environment.NewLine +
        //                                            Environment.NewLine + string.Format("{0,-70}#{1,17}#{2,27}", string.Empty, totalBillAmountWithTax, totalBillAmountWithTax).Replace('#', ' ') +
        //                                            Environment.NewLine +
        //                                            Environment.NewLine + string.Format("{0,8}#{1,-19}#{2,-77}#{3,-16}", string.Empty, dr["OldAccountNo"].ToString(), "EN" + dr["TariffName"].ToString() + "- Rate:=N= " + (!string.IsNullOrEmpty(Convert.ToString(dr["EnergyCharges"])) ? dr["EnergyCharges"].ToString() : "--") + "", dr["OldAccountNo"].ToString()).Replace('#', ' ') +
        //                        //Environment.NewLine + string.Format("{0,-2}#{1,-60}", string.Empty, "THANKS FOR YOUR PATRONAGE").Replace('#', ' ') +
        //                                            Environment.NewLine + string.Format("{0,-2}#{1,-60}", string.Empty, dr["Messages"]).Replace("&lt;\\n&gt;", Environment.NewLine) +
        //                                            Environment.NewLine + string.Format("{0,-5}#{1,-26}#{2,-61}", string.Empty, "- EN" + dr["TariffName"].ToString() + "- Rate:=N= " + (!string.IsNullOrEmpty(Convert.ToString(dr["EnergyCharges"])) ? dr["EnergyCharges"].ToString() : "--") + "", " ").Replace('#', ' ') +
        //                                            Environment.NewLine;
        //                    //+ Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine +
        //                    // "-------------------------------------------------------------------------------------------------------------------" + Environment.NewLine;
        //                    //add the text
        //                    sw.WriteLine(billDetails.ToUpper());
        //                    //add the text to the underlying filestream

        //                    sw.Flush();
        //                    //close the writer
        //                    sw.Close();
        //                    fs.Close();
        //                    divCustomerDetails.Visible = false;
        //                    //Message("Bill generated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
        //                    txtAccountNo.Text = string.Empty;
        //                    hfBillfilePath.Value = filePath;
        //                    hfBillfileName.Value = FileName;
        //                    mpeDownload.Show();
        //                    UpdateBillFile(Convert.ToInt32(dr["CustomerBillId"]), filePathforDB);
        //                    string SuccessMessage = string.Empty;
        //                    SuccessMessage = "Bill generated successfully.";
        //                    CommunicationFeaturesBE objCommunicationFeaturesBE = new CommunicationFeaturesBE();
        //                    objCommunicationFeaturesBE.CommunicationFeaturesID = (int)CommunicationFeatures.BillGeneration;

        //                    try
        //                    {
        //                        if (_ObjCommonMethods.IsEmailFeatureEnabled(objCommunicationFeaturesBE))
        //                        {

        //                            CommunicationFeaturesBE _objMailBE = new CommunicationFeaturesBE();
        //                            _objMailBE.GlobalAccountNumber = Convert.ToString(dr["AccountNo"]);
        //                            XmlDocument xml = _ObjCommonMethods.GetDetails(_objMailBE, ReturnType.Fetch);
        //                            _objMailBE = _ObjIdsignBal.DeserializeFromXml<CommunicationFeaturesBE>(xml);

        //                            if (_objMailBE.EmailId == "0")
        //                            {
        //                                SuccessMessage += "Customer not having Mail Id.";
        //                            }
        //                            else if (!string.IsNullOrEmpty(_objMailBE.EmailId))
        //                            {
        //                                decimal BillAmtWithTax = totalBillAmount + vatAmount;
        //                                string totalBillamount = _ObjCommonMethods.GetCurrencyFormat(BillAmtWithTax, 2, Constants.MILLION_Format); //vat+BillAmount -TotalBillwithtax
        //                                string htmlBody = string.Empty;
        //                                htmlBody = _ObjCommonMethods.GetMailCommTemp(CommunicationFeatures.BillGeneration, Convert.ToString(dr["Name"]), Convert.ToString(dr["MonthName"]) + "-" + Convert.ToString(dr["BillYear"]), Convert.ToString(dr["AccountNo"]), Convert.ToString(dr["MeterNo"]), Convert.ToString(totalBillamount), Convert.ToString(totalBillAmountWithTax), Convert.ToString(dr["LastDueDate"]), string.Empty, string.Empty, string.Empty, "THIS BILLING IS FOR " + Convert.ToString(dr["MonthName"]) + "-" + Convert.ToString(dr["BillYear"]) + " CONSUMPTION.");

        //                                _ObjCommonMethods.sendmail(htmlBody, ConfigurationManager.AppSettings["SMTPUsername"].ToString(), "Bill Generated for acc " + Convert.ToString(dr["AccountNo"]), _objMailBE.EmailId, filePath);
        //                                SuccessMessage += "Mail sent successfully. ";
        //                            }
        //                            else
        //                            {
        //                                SuccessMessage += "Mail not sent to this customer.";
        //                            }
        //                        }
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //                        try
        //                        {
        //                            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //                        }
        //                        catch (Exception logex)
        //                        {

        //                        }
        //                    }

        //                    try
        //                    {
        //                        if (_ObjCommonMethods.IsSMSFeatureEnabled(objCommunicationFeaturesBE))
        //                        {
        //                            CommunicationFeaturesBE _objSmsBE = new CommunicationFeaturesBE();
        //                            _objSmsBE.GlobalAccountNumber = Convert.ToString(dr["AccountNo"]);
        //                            XmlDocument xml = _ObjCommonMethods.GetDetails(_objSmsBE, ReturnType.Get);
        //                            _objSmsBE = _ObjIdsignBal.DeserializeFromXml<CommunicationFeaturesBE>(xml);

        //                            if (_objSmsBE.MobileNo == "0")
        //                            {
        //                                SuccessMessage += "Customer not having Mobile No.";
        //                            }
        //                            else if (!string.IsNullOrEmpty(_objSmsBE.MobileNo) && _objSmsBE.MobileNo.Length == 10)
        //                            {
        //                                decimal BillAmtWithTax = totalBillAmount + vatAmount;
        //                                string totalBillamount = _ObjCommonMethods.GetCurrencyFormat(BillAmtWithTax, 2, Constants.MILLION_Format); //vat+BillAmount -TotalBillwithtax
        //                                string Messsage = string.Empty;
        //                                Messsage = _ObjCommonMethods.GetSMSCommTemp(CommunicationFeatures.BillGeneration, Convert.ToString(dr["AccountNo"]), Convert.ToString(dr["MonthName"]) + "-" + Convert.ToString(dr["BillYear"]), Convert.ToString(totalBillamount), Convert.ToString(dr["LastDueDate"]), string.Empty, string.Empty);
        //                                //Messsage = _ObjCommonMethods.GetSMSCommTemp(CommunicationFeatures.BillGeneration, Convert.ToString(dr["AccountNo"]), Convert.ToString(dr["MonthName"]) + "-" + Convert.ToString(dr["BillYear"]), Convert.ToString(dr["BillAmount"]), Convert.ToString(dr["BillDueDate"]), string.Empty);//Parameter has to be check
        //                                _ObjCommonMethods.sendSMS(_objSmsBE.MobileNo, Messsage, ConfigurationManager.AppSettings["SMTPUsername"].ToString());
        //                                SuccessMessage += "Message sent successfully.";
        //                            }
        //                            else
        //                            {
        //                                SuccessMessage += "Message not sent to this customer.";
        //                            }
        //                        }
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //                        try
        //                        {
        //                            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //                        }
        //                        catch (Exception logex)
        //                        {

        //                        }
        //                    }
        //                    Message(SuccessMessage, UMS_Resource.MESSAGETYPE_SUCCESS);
        //                }

        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message("Error in file creation.", UMS_Resource.MESSAGETYPE_ERROR);
        //        //_ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //    }
        //}

        private void UpdateBillFile(int CustomerBillId, string FilePath)
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                _objBillGenerationBe.CustomerBillId = CustomerBillId;
                _objBillGenerationBe.FilePath = FilePath;
                xmlResult = _objBillGenerationBal.UpdateBillGenByCustFile(_objBillGenerationBe);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private bool CheckDisableBookDetails()
        {
            BillGenerationBe ObjBillGenerationBe = new BillGenerationBe();
            ObjBillGenerationBe.AccountNo = lblAccountno.Text;
            XmlDocument xmlResult = _objBillGenerationBal.GetBillCycleDetails(ObjBillGenerationBe, ReturnType.Fetch);
            ObjBillGenerationBe = _ObjIdsignBal.DeserializeFromXml<BillGenerationBe>(xmlResult);
            if (ObjBillGenerationBe.IsExists)
            {
                lblAlertMsg.Text = "Book is disable. Bill cannot generate.";
                return true;
            }
            else
                return false;
        }

        //private void sendmail(string EmailBody, string FromEmailId, string Subject, string ToEmailId)
        //{
        //    Communication.EmailInput.EmailInput objEmailInput = new Communication.EmailInput.EmailInput();
        //    Communication.Mail.Email objEmail = new Communication.Mail.Email();
        //    objEmailInput.EmailBody = EmailBody + DateTime.Now.ToShortDateString();
        //    objEmailInput.FromEmail = FromEmailId;
        //    objEmailInput.Subject = Subject;
        //    objEmailInput.To = ToEmailId;
        //    objEmailInput.SMTPServer = ConfigurationManager.AppSettings["SMTPServer"];
        //    objEmailInput.SMTPPort = Convert.ToInt16(ConfigurationManager.AppSettings["SMTPPort"]);
        //    objEmailInput.SMTPUsername = ConfigurationManager.AppSettings["SMTPUsername"];
        //    objEmailInput.SMTPPassword = ConfigurationManager.AppSettings["SMTPPassword"];
        //    objEmailInput.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
        //    objEmailInput.UserDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]);
        //    objEmail.SendMail(objEmailInput);
        //}        

        //private void sendSMS(string MobileNo, string SMSMessage, string DeliveryEmailId)
        //{
        //    try
        //    {
        //        Communication.SmsInput.SmsInput _objSmsInput = new Communication.SmsInput.SmsInput();
        //        Communication.Sms.Sms _objsms = new Communication.Sms.Sms();
        //        _objSmsInput.SMSLoginId = ConfigurationManager.AppSettings["SMSLoginId"];
        //        _objSmsInput.SMSLoginPassword = ConfigurationManager.AppSettings["SMSLoginPassword"];
        //        _objSmsInput.SMSCallBack = ConfigurationManager.AppSettings["SMSCallBack"];
        //        _objSmsInput.SMSSiteTokenId = ConfigurationManager.AppSettings["SMSSiteTokenId"];
        //        _objSmsInput.SMSMobileNo = MobileNo;
        //        _objSmsInput.SMSMessage = SMSMessage;
        //        _objSmsInput.SMSDeliveryEmailId = DeliveryEmailId;
        //        _objsms.SendSMS(_objSmsInput);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString[UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO]))
                {
                    string AccountNo = _ObjIdsignBal.Decrypt(Request.QueryString[UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO]).ToString();
                    txtAccountNo.Text = AccountNo;

                }
                BindYears();
            }
        }

        protected void lblSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADVANCE_SEARCH_PAGE + "?" + UMS_Resource.FROM_PAGE + "=" + _ObjIdsignBal.Encrypt(Constants.CustomerBillPayment), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
            objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                objLedgerBe.BUID = string.Empty;
            //objLedgerBe = _ObjIdsignBal.DeserializeFromXml<RptCustomerLedgerBe>(objReportsBal.GetLedger(objLedgerBe, ReturnType.Check));
            objLedgerBe = _ObjIdsignBal.DeserializeFromXml<RptCustomerLedgerBe>(objReportsBal.IsCustomerExistsInBU_Billing_INDV(objLedgerBe));
            if (objLedgerBe.IsSuccess)
            {
                if (objLedgerBe.IsCustExistsInOtherBU)
                {
                    lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                    mpeAlert.Show();
                    divAssignMeter.Visible = false;
                }
                //else if (objLedgerBe.CustIsNotActive)
                //{
                //    lblAlertMsg.Text = "Customer is not in Active State."; //Defaul msg.
                //    if (objLedgerBe.ActiveStatusId == (int)ActiveStatus.Closed)
                //        lblAlertMsg.Text = "Customer status is " + ActiveStatus.Closed + ".  Cannot proceed for bill generation.";
                //    if (objLedgerBe.ActiveStatusId == (int)ActiveStatus.Hold)
                //        lblAlertMsg.Text = "Customer status is " + ActiveStatus.Hold + ".  Cannot proceed for bill generation.";
                //    mpeAlert.Show();
                //    divAssignMeter.Visible = false;
                //}
                else if (!objLedgerBe.IsNotCreditMeter)
                {
                    lblAlertMsg.Text = "Customer doesn't has credit meter. Cannot proceed for bill generation.";
                    mpeAlert.Show();
                    divAssignMeter.Visible = false;
                }
                else if (objLedgerBe.IsPrepaidCustomer)
                {
                    lblAlertMsg.Text = "This customer is prepaid. Cannot proceed for bill generation.";
                    mpeAlert.Show();
                    divAssignMeter.Visible = false;
                }
                else
                {
                    BindUserDetails();
                    BindYears();
                }
            }
            else
            {
                string Status = UMS_Resource.CUSTOMER_NOT_FOUND_MSG;//default

                switch (objLedgerBe.ActiveStatusId)
                {
                    case (int)ActiveStatus.Closed:
                        Status = string.Format(Resource.BILL_CNOT_GEN_STATUS, ActiveStatus.Closed.ToString());
                        break;
                    case (int)ActiveStatus.Hold:
                        Status = string.Format(Resource.BILL_CNOT_GEN_STATUS, ActiveStatus.Hold.ToString());
                        break;
                }
                divCustomerDetails.Visible = false;
                divAssignMeter.Visible = false;
                ClearFields();
                _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, Status, pnlMessage, lblMessage);
            }
        }

        protected void btnGoToBilling_Click(object sender, EventArgs e)
        {
            if (!CheckDisableBookDetails())
            {
                DataSet ds = new DataSet();
                string date = "01-" + lblMonth.Text + "-" + lblYear.Text;
                objBillGenerationBe.AccountNo = lblAccountno.Text;
                objBillGenerationBe.BillMonth = Convert.ToDateTime(date).Month;
                objBillGenerationBe.BillYear = Convert.ToDateTime(date).Year;
                ds = _objBillGenerationBal.GenerateBillForCustomer(objBillGenerationBe);
                if (ds.Tables.Count == 1)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //CreateCustomersBillFile(ds);
                        CreateCustomersBillFile(objBillGenerationBe.AccountNo, objBillGenerationBe.BillMonth, objBillGenerationBe.BillYear);
                    }
                    else
                        Message("Error in Bill Generation.", UMS_Resource.MESSAGETYPE_ERROR);
                }
                if (ds.Tables.Count == 2)
                {
                    if (Convert.ToDecimal(ds.Tables[1].Rows[0]["TotalPaidAmount"]) > 0 && Convert.ToDecimal(ds.Tables[1].Rows[0]["AdjustmentAmountEffected"]) > 0)
                        lblAlertMsg.Text = "Bill cannot be generate. Payment and Bill adjustment have already made for this bill month and account no.";
                    else if (Convert.ToDecimal(ds.Tables[1].Rows[0]["TotalPaidAmount"]) > 0)
                        lblAlertMsg.Text = "Bill cannot be generate. Payment have already made for this bill month and account no.";
                    else if (Convert.ToDecimal(ds.Tables[1].Rows[0]["AdjustmentAmountEffected"]) > 0)
                        lblAlertMsg.Text = "Bill cannot be generate. Bill adjustment have already made for this bill month and account no.";
                    mpeAlert.Show();
                }
            }
            else
            {
                lblAlertMsg.Text = "Book is disable. Bill cannot generate.";
                mpeAlert.Show();
            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            Response.ContentType = "pdf/text/plain";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + hfBillfileName.Value + "");
            Response.TransmitFile(hfBillfilePath.Value);
            Response.End();
            mpeDownload.Hide();
        }

        #endregion
    }
}