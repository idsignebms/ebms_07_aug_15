﻿<%@ Page Title="::Consumer Registration::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    AutoEventWireup="true" Theme="Green" CodeBehind="NewCustomer.aspx.cs" Inherits="UMS_Nigeria.ConsumerManagement.NewCustomer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControls/UserDefinedControls.ascx" TagName="UserDefinedControls"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type='text/javascript'>
        //    $(function () {
        //        // To disable f5
        //        $(document).bind("keydown", disableF5);
        //    });

        //    //Function to handle disabling F5
        //    function disableF5(e) {
        //        if ((e.which || e.keyCode) == 116)
        //            e.preventDefault();
        //        };

        //    var ctrlKeyDown = false;

        //    $(document).ready(function () {
        //        $(document).on("keydown", keydown);
        //        $(document).on("keyup", keyup);
        //    });

        //    function keydown(e) {

        //        if ((e.which || e.keyCode) == 116 || ((e.which || e.keyCode) == 82 && ctrlKeyDown)) {
        //            // Pressing F5 or Ctrl+R
        //            e.preventDefault();
        //        } else if ((e.which || e.keyCode) == 17) {
        //            // Pressing  only Ctrl
        //            ctrlKeyDown = true;
        //        }
        //    };

        //    function keyup(e) {
        //        // Key up Ctrl
        //        if ((e.which || e.keyCode) == 17)
        //            ctrlKeyDown = false;
        //    };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:UpdateProgress ID="UpdateProgress" runat="server">
                <ProgressTemplate>
                    <center>
                        <div id="loading-div" class="pbloading">
                            <div id="title-loading" class="pbtitle">
                                BEDC</div>
                            <center>
                                <img src="../images/loading.GIF" class="pbimg"></center>
                            <p class="pbtxt">
                                Loading Please wait.....</p>
                        </div>
                    </center>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
                PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="pnlMessage" runat="server">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divstep1" runat="server">
        <div id="wizard" class="swMain">
            <%----------------------------------------------------------Step 1----------------------------------------- --%>
            <div>
                <div class="out-bor">
                    <div class="inner-sec">
                        <div class="text_total">
                            <div class="text-heading">
                                <asp:Literal ID="litAddState" runat="server" Text="<%$ Resources:Resource, CUST_REGIST%>"></asp:Literal>
                            </div>
                            <div class="star_text">
                                <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="dot-line">
                        </div>
                        <asp:UpdatePanel ID="updtPnlCustType" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="inner-box1">
                                    <div class="text-inner" style="margin-bottom: 20px;">
                                        <asp:Literal ID="litCustomerType" runat="server" Text="<%$ Resources:Resource, CUSTOMER_TYPE%>"></asp:Literal>
                                        <br />
                                        <asp:DropDownList ID="ddlCustomerType" TabIndex="1" runat="server" CssClass="text-box text-select"
                                            onchange="return ifPrepaidMeter();">
                                            <%--OnSelectedIndexChanged="ddlCustomerType_SelectedIndexChanged" AutoPostBack="true"--%>
                                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                        <div class="space">
                                        </div>
                                        <span id="spanCustomerType" class="span_color"></span>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <%-------------------------Land Lord Information Start----------------------%>
                        <asp:UpdatePanel ID="updtPnlLandlord" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="clr">
                                </div>
                                <h4 class="subHeading">
                                    <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, LAND_LORD_INFO%>"></asp:Literal></h4>
                                <div class="inner-box">
                                    <div class="inner-box1">
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litOTitle" runat="server" Text="<%$ Resources:Resource, TITLE%>"></asp:Literal>
                                        </label>
                                        <br />
                                        <asp:DropDownList ID="ddlOTitle" runat="server" TabIndex="2" CssClass="text-box text-select">
                                            <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Mr" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Miss" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Mrs" Value="3"></asp:ListItem>
                                        </asp:DropDownList>
                                        <div class="space">
                                        </div>
                                        <span id="spanOTitle" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box2">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litOFName" runat="server" Text="<%$ Resources:Resource, F_NAME%>"></asp:Literal></label>
                                        <br />
                                        <asp:TextBox ID="txtOFName" runat="server" TabIndex="3" CssClass="text-box" placeholder="Enter First Name"
                                            MaxLength="50" onKeyUp="CopyToKnownAsOwner()"></asp:TextBox>
                                        <%--<asp:FilteredTextBoxExtender ID="ftbTxtOFName" runat="server" TargetControlID="txtOFName"
                                            FilterType="LowercaseLetters, UppercaseLetters, Custom" ValidChars=" .&-:@,#$()'">
                                        </asp:FilteredTextBoxExtender>--%>
                                        <span id="spanOFName" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box3">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litOMName" runat="server" Text="<%$ Resources:Resource, M_NAME%>"></asp:Literal></label>
                                        <br />
                                        <asp:TextBox ID="txtOMName" runat="server" TabIndex="4" CssClass="text-box" placeholder="Enter Middle Name"
                                            MaxLength="50" onKeyUp="CopyToKnownAsOwner()"></asp:TextBox>
                                        <span id="spanOMName" class="span_color"></span>
                                        <%--<asp:FilteredTextBoxExtender ID="ftbTxtOMName" runat="server" TargetControlID="txtOMName"
                                            FilterType="LowercaseLetters, UppercaseLetters, Custom" ValidChars=" .&-:@,#$()'">
                                        </asp:FilteredTextBoxExtender>--%>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litOLName" runat="server" Text="<%$ Resources:Resource, L_NAME%>"></asp:Literal></label>
                                        <br />
                                        <asp:TextBox ID="txtOLName" runat="server" TabIndex="5" CssClass="text-box" onblur="return TextBoxBlurValidationlbl(this,'spanLName','Last Name')"
                                            placeholder="Enter Last Name" MaxLength="50" onKeyUp="CopyToKnownAsOwner()"></asp:TextBox>
                                        <%--<asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" TargetControlID="txtOLName"
                                            FilterType="LowercaseLetters, UppercaseLetters, Custom" ValidChars=" .&-:@,#$()'">
                                        </asp:FilteredTextBoxExtender>--%>
                                        <span id="spanOLName" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box2">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litOKnownAs" runat="server" Text="<%$ Resources:Resource, KNOWN_AS%>"></asp:Literal></label>
                                        <br />
                                        <asp:TextBox ID="txtOKnownAs" runat="server" TabIndex="6" CssClass="text-box" placeholder="Enter Known As"
                                            MaxLength="150"></asp:TextBox>
                                        <%--<asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" TargetControlID="txtOKnownAs"
                                            FilterType="LowercaseLetters, UppercaseLetters, Custom" ValidChars=" .&-:@,#$()'">
                                        </asp:FilteredTextBoxExtender>--%>
                                        <span id="spanOKnownAs" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box3">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litOEmailId" runat="server" Text="<%$ Resources:Resource, EMAIL_ID%>"></asp:Literal></label>
                                        <br />
                                        <asp:TextBox ID="txtOEmailId" runat="server" TabIndex="7" CssClass="text-box" placeholder="Enter EmailId"
                                            OnTextChanged="txtOEmailId_TextChanged" onblur="return OnlyEmailValidation(this,'spanOEmailId','Email Id')"></asp:TextBox>
                                        <span id="spanOEmailId" class="span_color"></span><span id="spanOEMailExists" runat="server">
                                        </span>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litOHPhone" runat="server" Text="<%$ Resources:Resource, HOME%>"></asp:Literal>
                                        </label>
                                        <br />
                                        <asp:TextBox ID="txtOHPhoneCode" Width="10%" runat="server" TabIndex="8" CssClass="text-box"
                                            placeholder="Code" MaxLength="3"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtOHPhoneCode"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:TextBox ID="txtOHPhone" Width="40%" runat="server" TabIndex="9" CssClass="text-box"
                                            placeholder="Enter Home Phone" MaxLength="10"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="txtOHPhone"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanOHPhone" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box2">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litOBPhone" runat="server" Text="<%$ Resources:Resource, BUSINESS%>"></asp:Literal></label>
                                        <br />
                                        <asp:TextBox ID="txtOBPhoneCode" Width="10%" runat="server" TabIndex="10" CssClass="text-box"
                                            placeholder="Code" MaxLength="3"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="txtOBPhoneCode"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:TextBox ID="txtOBPhone" Width="40%" runat="server" TabIndex="11" CssClass="text-box"
                                            placeholder="Enter Business Phone" MaxLength="10"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="txtOBPhone"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanOBPhone" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box3">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litOOPhone" runat="server" Text="<%$ Resources:Resource, OTHERS%>"></asp:Literal></label>
                                        <br />
                                        <asp:TextBox ID="txtOOPhoneCode" Width="10%" runat="server" TabIndex="12" CssClass="text-box"
                                            placeholder="Code" MaxLength="3"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="txtOOPhoneCode"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:TextBox ID="txtOOPhone" Width="40%" runat="server" TabIndex="13" CssClass="text-box"
                                            placeholder="Enter Others Phone" MaxLength="10"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" TargetControlID="txtOOPhone"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanOOPhone" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <%-------------------------Land Lord Information End----------------------%>
                        <%-------------------------Tenent Information Start----------------------%>
                        <asp:UpdatePanel ID="updtPnlTenent" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <asp:CheckBox ID="cbIsTenent" TabIndex="14" runat="server" onchange="OnOffTenentDiv();"
                                            Text="Is Tenant" Checked="false" />
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="clr">
                                </div>
                                <div id="divTenent" style="display: none;">
                                    <h4 class="subHeading">
                                        <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, TENENT_INFO%>"></asp:Literal>
                                    </h4>
                                    <%--  <div class="inner-box">
                                        <div class="inner-box1">
                                            <div class="grid_head">
                                            </div>
                                        </div>
                                    </div>--%>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litTTitle" runat="server" Text="<%$ Resources:Resource, TITLE%>"></asp:Literal></label><br />
                                            <asp:DropDownList ID="ddlTTitle" runat="server" TabIndex="15" CssClass="text-box text-select">
                                                <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Mr" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Miss" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Mrs" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="space">
                                            </div>
                                            <span class="span_color" id="spanTTitle"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box2">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litTFName" runat="server" Text="<%$ Resources:Resource, F_NAME%>"></asp:Literal></label><br />
                                            <asp:TextBox ID="txtTFName" runat="server" TabIndex="16" CssClass="text-box" placeholder="Enter First Name"
                                                MaxLength="50"></asp:TextBox>
                                            <%--<asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" TargetControlID="txtTFName"
                                                FilterType="LowercaseLetters, UppercaseLetters, Custom" ValidChars=" .&-">
                                            </asp:FilteredTextBoxExtender>--%>
                                            <span id="spanTFName" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box3">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litTMName" runat="server" Text="<%$ Resources:Resource, M_NAME%>"></asp:Literal></label><br />
                                            <asp:TextBox ID="txtTMName" runat="server" TabIndex="17" CssClass="text-box" placeholder="Enter Middle Name"
                                                MaxLength="50"></asp:TextBox>
                                            <%--<asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" TargetControlID="txtTMName"
                                                FilterType="LowercaseLetters, UppercaseLetters, Custom" ValidChars=" .&-">
                                            </asp:FilteredTextBoxExtender>--%>
                                            <span id="spanTMName" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litTLName" runat="server" Text="<%$ Resources:Resource, L_NAME%>"></asp:Literal></label><br />
                                            <asp:TextBox ID="txtTLName" runat="server" TabIndex="18" CssClass="text-box" placeholder="Enter Last Name"
                                                MaxLength="50"></asp:TextBox>
                                            <%--<asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server" TargetControlID="txtTLName"
                                                FilterType="LowercaseLetters, UppercaseLetters, Custom" ValidChars=" .&-">
                                            </asp:FilteredTextBoxExtender>--%>
                                            <span id="spanTLName" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box2">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litTEmailId" runat="server" Text="<%$ Resources:Resource, EMAIL_ID%>"></asp:Literal></label><br />
                                            <asp:TextBox ID="txtTEmailId" runat="server" TabIndex="19" CssClass="text-box" placeholder="Enter EmailId"
                                                OnTextChanged="txtTEmailId_TextChanged" onblur="return OnlyEmailValidation(this,'spanTEmailId','Email Id')"></asp:TextBox>
                                            <span id="spanTEmailId" class="span_color"></span><span id="spanTEMailExists" runat="server">
                                            </span>
                                        </div>
                                    </div>
                                    <div class="inner-box3">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litTHPhone" runat="server" Text="<%$ Resources:Resource, HOME%>"></asp:Literal></label><br />
                                            <asp:TextBox ID="txtTHPhoneCode" Width="10%" runat="server" TabIndex="20" CssClass="text-box"
                                                placeholder="Code" MaxLength="3"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" TargetControlID="txtTHPhoneCode"
                                                FilterType="Numbers">
                                            </asp:FilteredTextBoxExtender>
                                            <asp:TextBox ID="txtTHPhone" Width="40%" runat="server" TabIndex="21" CssClass="text-box"
                                                placeholder="Enter Home Phone" MaxLength="10"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" TargetControlID="txtTHPhone"
                                                FilterType="Numbers">
                                            </asp:FilteredTextBoxExtender>
                                            <span id="spanTHPhone" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litTAPhone" runat="server" Text="<%$ Resources:Resource, ANOTHER_CONTACT_NO%>"></asp:Literal></label><br />
                                            <asp:TextBox ID="txtTAPhoneCode" Width="10%" runat="server" TabIndex="22" CssClass="text-box"
                                                placeholder="Code" MaxLength="3"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" TargetControlID="txtTAPhoneCode"
                                                FilterType="Numbers">
                                            </asp:FilteredTextBoxExtender>
                                            <asp:TextBox ID="txtTAPhone" Width="40%" runat="server" TabIndex="23" CssClass="text-box"
                                                placeholder="Enter Alternate Contact No" MaxLength="10"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" TargetControlID="txtTAPhone"
                                                FilterType="Numbers">
                                            </asp:FilteredTextBoxExtender>
                                            <span id="spanTAPhone" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <%-------------------------Tenent Information End----------------------%>
                        <%-------------------------Postal Information Start----------------------%>
                        <asp:UpdatePanel ID="updtPnlPostalAddress" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="clr">
                                </div>
                                <h4 class="subHeading">
                                    <asp:Literal ID="litPostalAddress" runat="server" Text="Postal Address"></asp:Literal>
                                </h4>
                                <div class="clr">
                                </div>
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litPHNo" runat="server" Text="<%$ Resources:Resource, H_NO%>"></asp:Literal></label><br />
                                        <asp:TextBox ID="txtPHNo" runat="server" TabIndex="24" CssClass="text-box" placeholder="Enter House No"
                                            MaxLength="10" onKeyUp="OnOfIsSameAsServiceAddress()"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server" TargetControlID="txtPHNo"
                                            FilterType="LowercaseLetters, UppercaseLetters, Custom, Numbers" ValidChars=" -/\#.,">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanPHNo" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box2">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litPStreet" runat="server" Text="<%$ Resources:Resource, STREET%>"></asp:Literal>
                                        </label>
                                        <br />
                                        <asp:TextBox ID="txtPStreet" runat="server" TabIndex="25" CssClass="text-box" placeholder="Enter Street Name"
                                            MaxLength="255" onKeyUp="OnOfIsSameAsServiceAddress()"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server" TargetControlID="txtPStreet"
                                            FilterType="LowercaseLetters, UppercaseLetters, Custom, Numbers" ValidChars=" -/\#.,">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanPStreet" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box3">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litPLGAVillage" runat="server" Text="<%$ Resources:Resource, CITY%>"></asp:Literal></label><br />
                                        <asp:TextBox ID="txtPLGAVillage" runat="server" TabIndex="26" CssClass="text-box"
                                            placeholder="Enter City" MaxLength="50" onKeyUp="OnOfIsSameAsServiceAddress()"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender28" runat="server" TargetControlID="txtPLGAVillage"
                                            FilterType="LowercaseLetters, UppercaseLetters, Custom, Numbers" ValidChars=" -/\#.,">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanPLGAVillage" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litPZip" runat="server" Text="<%$ Resources:Resource, ZIP%>"></asp:Literal>
                                        </label>
                                        <br />
                                        <asp:TextBox ID="txtPZip" runat="server" TabIndex="27" CssClass="text-box" placeholder="Enter Zip Code"
                                            MaxLength="10" onKeyUp="OnOfIsSameAsServiceAddress()"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtPZip"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanPZip" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box2">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litPArea" runat="server" Text="<%$ Resources:Resource, AREA_CODE%>"></asp:Literal></label><br />
                                        <asp:TextBox ID="txtPArea" runat="server" TabIndex="28" CssClass="text-box" placeholder="Enter Area Code"
                                            AutoPostBack="true" OnTextChanged="txtPArea_TextChanged" MaxLength="6" onKeyUp="OnOfIsSameAsServiceAddress()"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtPArea"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanPArea" class="span_color"></span>
                                        <br />
                                        <span id="spnPostalArea" runat="server"></span>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <%-------------------------Postal Information End----------------------%>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:CheckBox ID="cbIsSameAsPostal" runat="server" TabIndex="29" onchange="OnOfIsSameAsServiceAddress()"
                                    Text="Is same as Postal Address" />
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <%-------------------------Service Information Start----------------------%>
                        <asp:UpdatePanel ID="updtPnlServiceAddress" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <h4 class="subHeading">
                                    <asp:Literal ID="litServiceAddress" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Literal>
                                </h4>
                                <div class="clr">
                                </div>
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litSHNo" runat="server" Text="<%$ Resources:Resource, H_NO%>"></asp:Literal></label><br />
                                        <asp:TextBox ID="txtSHNo" runat="server" TabIndex="30" CssClass="text-box" placeholder="Enter House No"
                                            MaxLength="10"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender29" runat="server" TargetControlID="txtSHNo"
                                            FilterType="LowercaseLetters, UppercaseLetters, Custom, Numbers" ValidChars=" -/\#.,">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanSHNo" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box2">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litSStreet" runat="server" Text="<%$ Resources:Resource, STREET%>"></asp:Literal></label><br />
                                        <asp:TextBox ID="txtSStreet" runat="server" TabIndex="31" CssClass="text-box" placeholder="Enter Street Name"
                                            MaxLength="255"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server" TargetControlID="txtSStreet"
                                            FilterType="LowercaseLetters, UppercaseLetters, Custom, Numbers" ValidChars=" -/\#.,">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanSStreet" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box3">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litSLGAVillage" runat="server" Text="<%$ Resources:Resource, CITY%>"></asp:Literal></label><br />
                                        <asp:TextBox ID="txtSLGAVillage" runat="server" TabIndex="32" CssClass="text-box"
                                            placeholder="Enter City" MaxLength="50"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender31" runat="server" TargetControlID="txtSLGAVillage"
                                            FilterType="LowercaseLetters, UppercaseLetters, Custom, Numbers" ValidChars=" -/\#.,">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanSLGAVillage" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litSZip" runat="server" Text="<%$ Resources:Resource, ZIP%>"></asp:Literal></label><br />
                                        <asp:TextBox ID="txtSZip" runat="server" TabIndex="33" CssClass="text-box" placeholder="Enter Zip Code"
                                            MaxLength="10"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" TargetControlID="txtSZip"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanSZip" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box2">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litSArea" runat="server" Text="<%$ Resources:Resource, AREA_CODE%>"></asp:Literal></label><br />
                                        <asp:TextBox ID="txtSArea" runat="server" TabIndex="34" CssClass="text-box" placeholder="Enter Area Code"
                                            AutoPostBack="true" OnTextChanged="txtSArea_TextChanged" MaxLength="6"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtSArea"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanSArea" class="span_color"></span>
                                        <br />
                                        <span id="spnServericeArea" runat="server"></span>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litrblCommunicationAddress" runat="server" Text="<%$ Resources:Resource, COMMUNICATION_ADD%>"></asp:Literal></label><br />
                                        <asp:RadioButtonList ID="rblCommunicationAddress" TabIndex="35" RepeatDirection="Horizontal"
                                            runat="server" Width="300">
                                            <asp:ListItem Text="Postal" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Service" Value="2"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                    <br />
                                    <span id="spanrblCommunicationAddress" class="span_color"></span>
                                </div>
                                <div class="clr">
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <%-------------------------Service Information End----------------------%>
                        <div class="clr">
                            <br />
                            <br />
                        </div>
                        <asp:UpdatePanel ID="updtPnlOtherDetails" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <h4 class="subHeading">
                                    Other Details</h4>
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <asp:CheckBox ID="cbIsBEDCEmployee" runat="server" TabIndex="36" onchange="OnOffIsEmployee()"
                                            Text="Is BEDC Employee" Checked="false" />
                                    </div>
                                </div>
                                <div class="inner-box2">
                                    <div id="divEmployeeCode" class="text-inner" style="display: none;">
                                        <label for="name">
                                            <asp:Literal ID="litEmployeCode" runat="server" Text="<%$ Resources:Resource, EMPLOYEE_CODE%>"></asp:Literal></label><br />
                                        <asp:DropDownList ID="ddlIsBedcEmp" runat="server" TabIndex="37" CssClass="text-box text-select">
                                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                        <div class="space">
                                        </div>
                                        <span id="spanIsBedcEmp" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <asp:CheckBox ID="cbIsEmbassy" runat="server" TabIndex="38" Text="Tax Free(Is Embassy)"
                                            onchange="OnOffIsEmbessy()" Checked="false" />
                                    </div>
                                    <div class="text-inner">
                                        <asp:CheckBox ID="cbIsVIP" runat="server" TabIndex="39" Text="Is VIP" />
                                    </div>
                                </div>
                                <div id="divEmbessy" class="inner-box2" style="display: none;">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litEmbassy" runat="server" Text="<%$ Resources:Resource, EMBASSY_CODE%>"></asp:Literal></label><br />
                                        <asp:TextBox ID="txtEmbassy" runat="server" TabIndex="40" CssClass="text-box" placeholder="Enter Embassy Code"
                                            MaxLength="50"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender32" runat="server" TargetControlID="txtEmbassy"
                                            FilterType="LowercaseLetters, UppercaseLetters, Numbers">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanEmbassy" class="span_color"></span>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="clr">
                        </div>
                    </div>
                </div>
                <%----------------------------------------------------------Step 2----------------------------------------- --%>
                <div class="out-bor">
                    <div class="clr">
                    </div>
                    <h4 class="subHeading">
                        Account Information</h4>
                    <asp:UpdatePanel ID="updtPnlAccountInfo" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <asp:Literal ID="litAccountType" runat="server" Text="<%$ Resources:Resource, ACCOUNT_TYPE%>"></asp:Literal>
                                    <br />
                                    <asp:DropDownList ID="ddlAccountType" runat="server" TabIndex="41" CssClass="text-box text-select"
                                        OnSelectedIndexChanged="ddlAccountType_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                    <div style="display: none;">
                                        <asp:Button ID="btnGovtAccountType" Text="btn" runat="server" OnClick="btnGovtAccountType_Click" /></div>
                                    <div class="space">
                                    </div>
                                    <span id="spanAccountType" class="span_color"></span><span id="spanAccountType1"
                                        runat="server" class="span_color"></span>
                                    <asp:Label ID="lblGovtAccountTypeName" runat="server"></asp:Label>
                                    <asp:HiddenField ID="hfGovtAccountTypeID" runat="server" />
                                </div>
                            </div>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litBook" runat="server" Text="<%$ Resources:Resource, BOOK_CODE%>"></asp:Literal></label><br />
                                    <asp:TextBox ID="txtBook" runat="server" TabIndex="42" CssClass="text-box" ReadOnly="true"
                                        placeholder="Enter Book Code" atuocomplete="off"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" TargetControlID="txtBook"
                                        FilterType="LowercaseLetters, UppercaseLetters, Custom, Numbers" ValidChars=" -()">
                                    </asp:FilteredTextBoxExtender>
                                    <asp:HiddenField ID="hfBookNo" runat="server" />
                                    <div class="box_total_b">
                                        <div class="search_divTwo">
                                            <asp:LinkButton ID="idlnkSearchBookNo" TabIndex="43" runat="server">                                            
                                             <%--   <span class="search_img"><span class="search_img"></span>--%>
                                                  Search
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="space">
                                    </div>
                                    <span id="spanBook" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box3">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litPole" runat="server" Text="<%$ Resources:Resource, POLE%>"></asp:Literal></label><br />
                                    <asp:TextBox ID="txtPole" runat="server" TabIndex="44" CssClass="text-box" placeholder="Enter Pole Number"
                                        autocomplete="off" AutoPostBack="True" OnTextChanged="txtPole_TextChanged" MaxLength="15"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtPole"
                                        FilterType="Numbers, UppercaseLetters, LowercaseLetters">
                                    </asp:FilteredTextBoxExtender>
                                    <div class="box_total_b">
                                        <div class="search_divTwo">
                                            <asp:LinkButton ID="lnkSearchPole" runat="server" TabIndex="45" OnClick="lnkSearchPole_Click">
                                           <%--<span class="search_img"></span>--%>Search
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="space">
                                    </div>
                                    <span id="spanPole" class="span_color"></span><span id="spnPoleStatus" runat="server">
                                    </span>
                                    <asp:HiddenField ID="hfPoleID" runat="server" />
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litTariff" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal></label><br />
                                    <asp:DropDownList ID="ddlTariff" runat="server" TabIndex="46" CssClass="text-box text-select">
                                        <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                    <div class="space">
                                    </div>
                                    <span id="spanTariff" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litClusterType" runat="server" Text="<%$ Resources:Resource, CLUSTER_TYPE%>"></asp:Literal></label><br />
                                    <asp:DropDownList ID="ddlClusterType" runat="server" TabIndex="47" CssClass="text-box text-select">
                                        <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                    <div class="space">
                                    </div>
                                    <span id="spanClusterType" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box3">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litPhase" runat="server" Text="<%$ Resources:Resource, PHASE%>"></asp:Literal></label><br />
                                    <asp:DropDownList ID="ddlPhase" runat="server" TabIndex="48" CssClass="text-box text-select">
                                        <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                    <div class="space">
                                    </div>
                                    <span id="spanPhase" class="span_color"></span>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litRouteNo" runat="server" Text="<%$ Resources:Resource, ROUTE_NO%>"></asp:Literal></label><br />
                                    <asp:DropDownList ID="ddlRouteNo" runat="server" TabIndex="49" CssClass="text-box text-select">
                                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                    <div class="space">
                                    </div>
                                    <span id="spanRouteNo" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litInitialBilling" runat="server" Text="<%$ Resources:Resource, INITIAL_BILLING_KWH%>"></asp:Literal></label><br />
                                    <asp:TextBox ID="txtInitialBilling" runat="server" MaxLength="9" TabIndex="50" CssClass="text-box"
                                        placeholder="Enter Initial Billing kWh"></asp:TextBox>
                                    <div class="space">
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender34" runat="server" TargetControlID="txtInitialBilling"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>
                                    </div>
                                    <span id="spanInitialBilling" class="span_color"></span>
                                </div>
                            </div>
                            <%--Popus Book Starts--%>
                            <asp:ModalPopupExtender ID="mpeBookSearch" runat="server" TargetControlID="idlnkSearchBookNo"
                                PopupControlID="pnlBookSearch" BackgroundCssClass="popbg" CancelControlID="btnBookSearchCancel">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="pnlBookSearch" runat="server" CssClass="editpopup_two" Style="display: none">
                                <span style="position: fixed;"><a id="btnBookSearchCancel" runat="server" class="close"
                                    href="#close"></a></span>
                                <div class="inner-boxOne">
                                    <h4 class="smHeading">
                                        Book Code</h4>
                                    <div class="inner-box1" style="display: none;">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litCountryCode" runat="server" Text="<%$ Resources:Resource, BOOK_CODE%>"></asp:Literal>
                                                <span class="span_star">*</span></label><br />
                                            <asp:TextBox ID="txtBookNumber" onblur="return TextBoxBlurValidationlbl(this,'spanDisplayCode','Display Code')"
                                                autocomplete="off" ReadOnly="true" runat="server" MaxLength="20" CssClass="text-boxTwo"
                                                TabIndex="4" placeholder="Enter Display Code"></asp:TextBox>
                                            <span id="span1" class="span_color"></span><span class="text-heading_2"></span><span
                                                id="spanCountryCode" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box2" style="display: none;">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litDisplayCode" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal>
                                                <span class="span_star">*</span></label><br />
                                            <asp:TextBox ID="txtbookCode" onblur="return TextBoxBlurValidationlbl(this,'spanDisplayCode','Display Code')"
                                                runat="server" MaxLength="20" TabIndex="4" placeholder="Enter Display Code" CssClass="text-boxTwo"></asp:TextBox>
                                            <span id="spanDisplayCode" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="box_total" style="margin-top: -29px; display: none;">
                                        <div class="box_total_aTwo">
                                            <asp:Button ID="Button1" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s popupOne"
                                                runat="server" />
                                        </div>
                                    </div>
                                    <div class="space">
                                    </div>
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal></label><br />
                                            <asp:DropDownList ID="ddlBusinessUnitName" AutoPostBack="true" runat="server" CssClass="text-boxpopupOne select_box"
                                                OnSelectedIndexChanged="ddlBusinessUnitName_SelectedIndexChanged">
                                                <asp:ListItem Value="" Text="All"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="inner-box2">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litServiceUnit" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal></label><br />
                                            <asp:DropDownList ID="ddlServiceUnitName" AutoPostBack="true" runat="server" CssClass="text-boxpopupOne select_box"
                                                OnSelectedIndexChanged="ddlServiceUnitName_SelectedIndexChanged">
                                                <asp:ListItem Text="All" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="inner-box3">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litServiceCenter" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal></label><br />
                                            <asp:DropDownList ID="ddlServiceCenterName" runat="server" AutoPostBack="true" CssClass="text-boxpopupOne select_box"
                                                OnSelectedIndexChanged="ddlServiceCenterName_SelectedIndexChanged">
                                                <asp:ListItem Text="All" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="clear">
                                    </div>
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <asp:Literal ID="litCycles" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal><br />
                                            <asp:DropDownList ID="ddlCycle" runat="server" AutoPostBack="true" CssClass="text-boxpopupOne select_box"
                                                OnSelectedIndexChanged="ddlCycle_SelectedIndexChanged">
                                                <asp:ListItem Text="All" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="inner-box2">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litBookNo" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal></label><br />
                                            <asp:DropDownList ID="ddlBookNo" runat="server" AutoPostBack="true" CssClass="text-boxpopupOne select_box"
                                                OnSelectedIndexChanged="ddlBookNo_SelectedIndexChanged">
                                                <asp:ListItem Text="All" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="box_total" style="margin-top: -29px;">
                                        <div class="box_total_aTwo">
                                            <asp:Button ID="btnSerachBook" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s popupOne"
                                                runat="server" OnClick="btnSerachBook_Click" />
                                            <%--  <asp:Button ID="btnBookSearchCancel" Text="<%$ Resources:Resource, CANCEL%>" CssClass="box_s popupOne"
                                    runat="server" />--%>
                                        </div>
                                    </div>
                                    <div style="clear: both;">
                                    </div>
                                    <asp:GridView ID="grdBookNumber" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                        HeaderStyle-CssClass="grid_tb_hd" HeaderStyle-BackColor="#558D00" OnRowCommand="grdBookNumber_rowCommand"
                                        Font-Size="14px">
                                        <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                        <EmptyDataTemplate>
                                            There is no data.
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, GRID_SNO%>"
                                                runat="server">
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                HeaderText="<%$ Resources:Resource, BUSINESS_UNIT%>" DataField="BusinessUnitName" />
                                            <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                HeaderText="<%$ Resources:Resource, SERVICE_UNIT%>" DataField="ServiceUnitName" />
                                            <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                HeaderText="<%$ Resources:Resource, SERVICE_CENTER%>" DataField="ServiceCenterName" />
                                            <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                HeaderText="BookGroup" DataField="CycleName" />
                                            <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                HeaderText="<%$ Resources:Resource, BOOK_NO%>" DataField="BookCode" />
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Select" runat="server">
                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lstAdd" runat="server" CommandArgument='<%#Eval("BookCode") %>'
                                                        Text="Select" CommandName="select"></asp:LinkButton>
                                                    <asp:Label ID="lblBookNo" runat="server" Text='<%#Eval("BookNo") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle HorizontalAlign="center"></RowStyle>
                                    </asp:GridView>
                                </div>
                                <div style="clear: both;">
                                </div>
                            </asp:Panel>
                            <%--Popus Book Ends--%>
                            <%--Popus Pole Starts--%>
                            <asp:ModalPopupExtender ID="mpePole" runat="server" TargetControlID="hfSearchPole"
                                PopupControlID="pnlPole" BackgroundCssClass="popbg" CancelControlID="btnPoleSearchCancel">
                            </asp:ModalPopupExtender>
                            <asp:HiddenField ID="hfSearchPole" runat="server" />
                            <asp:Panel ID="pnlPole" runat="server" CssClass="editpopup_two" Style="display: none">
                                <span style="position: fixed;"><a id="btnPoleSearchCancel" runat="server" class="close"
                                    href="#close"></a></span>
                                <h4 class="smHeading">
                                    Pole</h4>
                                <div class="inner-box">
                                    <div class="inner-box">
                                        <div class="inner-box1">
                                            <table width="100%" style="border: 1px solid #7fbf4d; display: none;">
                                                <asp:Repeater ID="rptPoleSearch" runat="server" OnItemDataBound="rptPoleSearch_ItemDataBound"
                                                    OnItemCommand="rptPoleSearch_ItemCommand">
                                                    <ItemTemplate>
                                                        <div class="text-inner">
                                                            <tr>
                                                                <td>
                                                                    <%#Container.ItemIndex+1 %>
                                                                </td>
                                                                <td>
                                                                    <label for="name">
                                                                        <asp:Literal ID="litPoleMasterID" runat="server" Text='<%#Eval("PoleMasterId") %>'
                                                                            Visible="false"></asp:Literal>
                                                                        <asp:Label ID="lblPoleMasterName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlPoleSearchDesc" runat="server">
                                                                    </asp:DropDownList>
                                                                    <span id="span2" class="span_color"></span>
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="lstAdd" runat="server" Text="Select" CommandArgument='<%#Eval("PoleMasterId") %>'
                                                                        CommandName="polenumber"></asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="Literal6" runat="server" Text="TCN"></asp:Literal></label><br />
                                            <asp:DropDownList ID="ddlPoleParent" AutoPostBack="true" runat="server" CssClass="text-boxpopupOne select_box"
                                                OnSelectedIndexChanged="ddlPoleParent_SelectedIndexChanged" TabIndex="1">
                                                <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="space">
                                            </div>
                                            <span id="spnddlPoleParent" class="span_color"></span>
                                        </div>
                                        <div class="text-inner">
                                            <label for="name" style="margin-bottom: 5px;">
                                                <asp:Literal ID="Literal9" runat="server" Text="33/11 Inj. S/S"></asp:Literal></label><br />
                                            <asp:DropDownList ID="dllPoleLevelChild3" AutoPostBack="true" runat="server" CssClass="text-boxpopupOne select_box"
                                                OnSelectedIndexChanged="dllPoleLevelChild3_SelectedIndexChanged" TabIndex="2">
                                                <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="space">
                                            </div>
                                            <span id="spndllPoleLevelChild3" class="span_color"></span>
                                        </div>
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="Literal12" runat="server" Text="11/0.45KV TR"></asp:Literal></label><br />
                                            <asp:DropDownList ID="dllPoleLevelChild6" AutoPostBack="true" runat="server" CssClass="text-boxpopupOne select_box"
                                                OnSelectedIndexChanged="dllPoleLevelChild6_SelectedIndexChanged" TabIndex="7">
                                                <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="space">
                                            </div>
                                            <span id="spndllPoleLevelChild6" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box2">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="Literal7" runat="server" Text="TCN Power TR"></asp:Literal></label><br />
                                            <asp:DropDownList ID="dllPoleLevelChild1" AutoPostBack="true" runat="server" CssClass="text-boxpopupOne select_box"
                                                OnSelectedIndexChanged="dllPoleLevelChild1_SelectedIndexChanged" TabIndex="2">
                                                <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="space">
                                            </div>
                                            <span id="spndllPoleLevelChild1" class="span_color"></span>
                                        </div>
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="Literal10" runat="server" Text="33/11 Power TR"></asp:Literal></label><br />
                                            <asp:DropDownList ID="dllPoleLevelChild4" AutoPostBack="true" runat="server" CssClass="text-boxpopupOne select_box"
                                                OnSelectedIndexChanged="dllPoleLevelChild4_SelectedIndexChanged" TabIndex="5">
                                                <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="space">
                                            </div>
                                            <span id="spndllPoleLevelChild4" class="span_color"></span>
                                        </div>
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="Literal13" runat="server" Text="LV Upraiser"></asp:Literal></label><br />
                                            <asp:DropDownList ID="dllPoleLevelChild7" AutoPostBack="true" runat="server" CssClass="text-boxpopupOne select_box"
                                                OnSelectedIndexChanged="dllPoleLevelChild7_SelectedIndexChanged" TabIndex="8">
                                                <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="space">
                                            </div>
                                            <span id="spndllPoleLevelChild7" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box3">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="Literal8" runat="server" Text="33 KV Feeder"></asp:Literal></label><br />
                                            <asp:DropDownList ID="dllPoleLevelChild2" AutoPostBack="true" runat="server" CssClass="text-boxpopupOne select_box"
                                                OnSelectedIndexChanged="dllPoleLevelChild2_SelectedIndexChanged" TabIndex="3">
                                                <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="space">
                                            </div>
                                            <span id="spndllPoleLevelChild2" class="span_color"></span>
                                        </div>
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="Literal11" runat="server" Text="11 KV Feeder"></asp:Literal></label><br />
                                            <asp:DropDownList ID="dllPoleLevelChild5" AutoPostBack="true" runat="server" CssClass="text-boxpopupOne select_box"
                                                OnSelectedIndexChanged="dllPoleLevelChild5_SelectedIndexChanged" TabIndex="6">
                                                <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="space">
                                            </div>
                                            <span id="spndllPoleLevelChild5" class="span_color"></span>
                                        </div>
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="Literal14" runat="server" Text="Pole No"></asp:Literal></label><br />
                                            <asp:DropDownList ID="dllPoleLevelChild8" runat="server" AutoPostBack="true" CssClass="text-boxpopupOne select_box"
                                                TabIndex="9" OnSelectedIndexChanged="dllPoleLevelChild8_SelectedIndexChanged">
                                                <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="space">
                                            </div>
                                            <span id="spndllPoleLevelChild8" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div style="clear: both;">
                                    </div>
                                    <div class="box_total" style="margin-top: -29px;">
                                        <div class="selection">
                                            <asp:Button ID="btnSelectPole" Text="Select Pole" CssClass="box_s popupOne" runat="server"
                                                OnClick="btnSelectPole_click" OnClientClick="return ValidatePole();" Visible="false" />
                                        </div>
                                    </div>
                                    <div style="clear: both;">
                                    </div>
                                    <span id="spnPoleStatusPopup" runat="server"></span>
                                </div>
                            </asp:Panel>
                            <%--Popus Pole Ends--%>
                            <%--Popus Govt Account Type Starts--%>
                            <asp:ModalPopupExtender ID="mpeGovtAccType" runat="server" TargetControlID="hfGovtAccType"
                                PopupControlID="pnlGovtAccType" BackgroundCssClass="popbg" CancelControlID="btnGovtAccTypeCancel">
                            </asp:ModalPopupExtender>
                            <asp:HiddenField ID="hfGovtAccType" runat="server" />
                            <asp:Panel ID="pnlGovtAccType" runat="server" CssClass="editpopup_two" Style="display: none">
                                <span style="position: fixed;"><a class="close" id="btnGovtAccTypeCancel" href="#close"
                                    tabindex="0"></a></span>
                                <h4 class="smHeading">
                                    Govt Account Type</h4>
                                <div class="inner-box">
                                    <div class="inner-box1">
                                        <asp:Literal ID="litFullPath" runat="server"></asp:Literal>
                                        <div class="text-inner" style="width: 150px; float: left; margin-bottom: 20px;">
                                            <asp:TreeView ID="tvGovtType" ExpandDepth="0" runat="server" OnSelectedNodeChanged="tvGovtType_SelectedNodeChanged"
                                                OnTreeNodePopulate="tvGovtType_TreeNodePopulate">
                                                <RootNodeStyle Font-Bold="True" />
                                                <SelectedNodeStyle BackColor="#7FBF4D" Font-Bold="True" Font-Italic="True" ForeColor="White" />
                                            </asp:TreeView>
                                            <asp:Button ID="btnSelectGovt" runat="server" Text="Select" OnClick="btnSelectGovt_Click"
                                                Visible="false" CssClass="box_s popupOne" />
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <%--Popus Govt Account Type Ends--%>
                            <asp:HiddenField ID="hfPoleCode" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="updtPnlMeterDetails" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="inner-box3">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litReadCode" runat="server" Text="<%$ Resources:Resource, READCODE%>"></asp:Literal></label><br />
                                    <asp:DropDownList ID="ddlReadCode" runat="server" TabIndex="51" onChange="IfReadedCustomer()"
                                        CssClass="text-box text-select">
                                        <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                    <div class="space">
                                    </div>
                                    <span id="spanReadCode" class="span_color"></span>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div class="inner-box1">
                            </div>
                            <div class="inner-box2">
                            </div>
                            <div class="clr">
                            </div>
                            <div id="divReadCustomer" style="display: none;">
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:UpdatePanel ID="updtPnlMeterNo" runat="server">
                                                <ContentTemplate>
                                                    <asp:Literal ID="litMeterNoDisplay" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Literal></label><br />
                                                    <asp:TextBox ID="txtMeterNo" runat="server" TabIndex="52" CssClass="text-box" placeholder="Enter Meter No"
                                                        ReadOnly="true" autocomplete="off"></asp:TextBox>
                                                    <span id="spanMeterNo" class="span_color"></span>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdatePanel ID="updtPnlSearchMeterNo" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div class="box_total_b">
                                                        <div class="search_div" style="margin-top: -25px;">
                                                            <asp:LinkButton ID="lnkSearchMeterNo" runat="server" TabIndex="53"> <%--<span class="search_img"></span>--%>Search
                                                            </asp:LinkButton>
                                                        </div>
                                                        <asp:ModalPopupExtender ID="mpeMeterList" BehaviorID="mpeMeterList" runat="server"
                                                            TargetControlID="lnkSearchMeterNo" PopupControlID="pnlMeterList" BackgroundCssClass="popbg"
                                                            CancelControlID="btnCancleMpeMeter">
                                                        </asp:ModalPopupExtender>
                                                        <asp:HiddenField ID="hfMeterList" runat="server" />
                                                        <asp:Panel ID="pnlMeterList" runat="server" CssClass="editpopup_two" Style="display: none">
                                                            <span style="position: fixed;"><a id="btnCancleMpeMeter" runat="server" class="close"
                                                                href="#close"></a></span>
                                                            <h4 class="smHeading">
                                                                Meter No</h4>
                                                            <div style="clear: both;">
                                                            </div>
                                                            <div class="inner-box1">
                                                                <div class="text-inner">
                                                                    <label for="name">
                                                                        <asp:Literal ID="Literal15" runat="server" Text="Meter Number"></asp:Literal></label><br />
                                                                    <asp:TextBox ID="txtMeterNoSearch" runat="server" CssClass="text-box" placeholder="Enter Meter Number"
                                                                        MaxLength="100"></asp:TextBox>
                                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender35" runat="server" TargetControlID="txtMeterNoSearch"
                                                                        FilterType="LowercaseLetters, UppercaseLetters, Numbers">
                                                                    </asp:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <div class="inner-box2">
                                                                <div class="text-inner">
                                                                    <label for="name">
                                                                        <asp:Literal ID="Literal16" runat="server" Text="Meter Serial No"></asp:Literal></label><br />
                                                                    <asp:TextBox ID="txtMeterSerialNo" runat="server" CssClass="text-box" placeholder="Enter Meter Serial No"
                                                                        MaxLength="100"></asp:TextBox>
                                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender36" runat="server" TargetControlID="txtMeterSerialNo"
                                                                        FilterType="LowercaseLetters, UppercaseLetters, Numbers">
                                                                    </asp:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                            <div class="box_total" style="margin-top: -29px;">
                                                                <div class="box_total_aTwo">
                                                                    <asp:Button ID="btnSearchMeter" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s popupOne"
                                                                        runat="server" OnClick="btnSearchMeter_Click" />
                                                                </div>
                                                            </div>
                                                            <div style="clear: both;">
                                                            </div>
                                                            <asp:GridView ID="gvMeterList" runat="server" CssClass="gvMeterList" AutoGenerateColumns="False"
                                                                AlternatingRowStyle-CssClass="color" HeaderStyle-CssClass="grid_tb_hd" HeaderStyle-BackColor="#558D00"
                                                                OnRowCommand="gvMeterList_rowCommand">
                                                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                                                <EmptyDataTemplate>
                                                                    There is no data.
                                                                </EmptyDataTemplate>
                                                                <Columns>
                                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, GRID_SNO%>"
                                                                        runat="server">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                        <ItemTemplate>
                                                                            <%# Container.DataItemIndex+1 %>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                        HeaderText="Meter No" DataField="MeterNo" />
                                                                    <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                        HeaderText="Meter Serial No" DataField="MeterSerialNo" />
                                                                    <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                        HeaderText="Type" DataField="MeterType" />
                                                                    <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                        HeaderText="MeterSize" DataField="MeterSize" />
                                                                    <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                        HeaderText="MeterSize" DataField="MeterBrand" />
                                                                    <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                        HeaderText="Multiplier" DataField="MeterMultiplier" />
                                                                    <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                        HeaderText="BU Name" DataField="BusinessUnitName" />
                                                                    <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                        HeaderText="IsCAPMI Meter" DataField="IsCAPMIMeterString" />
                                                                    <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                        HeaderText="CAPMI Amount" DataField="CAPMIAmount" />
                                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="select" runat="server">
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lstAdd" runat="server" CommandArgument='<%#Eval("MeterNo") %>'
                                                                                Text="Select" CommandName="select" OnClientClick="return MeterReadVisible();"></asp:LinkButton>
                                                                            <asp:Literal ID="litMeterNo" runat="server" Text='<%#Eval("MeterId") %>' Visible="false"></asp:Literal>
                                                                            <asp:Literal ID="litMeterDials" runat="server" Text='<%#Eval("MeterDials") %>' Visible="false"></asp:Literal>
                                                                            <asp:Literal ID="IsCAPMIMeterString" runat="server" Text='<%#Eval("IsCAPMIMeterString") %>' Visible="false"></asp:Literal>
                                                                            <asp:Literal ID="litGridCAPMIAmount" runat="server" Text='<%#Eval("CAPMIAmount") %>' Visible="false"></asp:Literal>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <RowStyle HorizontalAlign="center"></RowStyle>
                                                            </asp:GridView>
                                                        </asp:Panel>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <div class="space">
                                            </div>
                                    </div>
                                    <div class="text-inner">
                                        <asp:HiddenField ID="hfMeterDials" runat="server" Value="0" />
                                        <asp:Button ID="btnDummyMeterDials" runat="server" Visible="false" OnClientClick="alert('');"
                                            OnClick="btnDummyMeterDials_click" />
                                        <asp:Literal ID="lblPresentReading" runat="server" Text="Initial Meter Reading"></asp:Literal><br />
                                        <asp:TextBox ID="txtPresentReading" runat="server" TabIndex="56" CssClass="text-box"
                                            placeholder="Enter Initial Meter Reading"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" TargetControlID="txtPresentReading"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanPresentReading" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box2">
                                    <div class="text-inner">
                                        <label id="lblIsCapmi" for="name">
                                            <asp:CheckBox ID="cbIsCAPMY" runat="server" TabIndex="54" Checked="false" onchange="OnOffIsCammy()" />
                                            <asp:Literal ID="litIsCapmi" runat="server" Text="Is CAPMI"></asp:Literal></label><br />
                                    </div>
                                </div>
                                <div class="inner-box3">
                                    <div id="divAmount" class="text-inner" style="display: none;">
                                        <label for="name">
                                            <asp:Literal ID="litAmount" runat="server" Text="<%$ Resources:Resource, AMOUNT%>"></asp:Literal></label><br />
                                        <asp:TextBox ID="txtAmount" runat="server" TabIndex="55" CssClass="text-box" placeholder="Enter CAPMI Amount"
                                            MaxLength="7" onkeypress="return isNumberKey(event,this)" onkeyup="GetCurrencyFormate(this);"></asp:TextBox>
                                        <%--<asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" TargetControlID="txtAmount"
                                            FilterType="Numbers,Custom" ValidChars="," FilterMode="ValidChars">--%>
                                        </asp:FilteredTextBoxExtender>
                                        <%--       <div class="space">
                                            </div>--%>
                                        <span id="spanAmount" class="span_color"></span>
                                    </div>
                                </div>
                            </div>
                            </label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="clr">
                </div>
                <div class="out-bor">
                    <asp:UpdatePanel ID="updtPnlSetupInformation" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <h4 class="subHeading">
                                Setup Information</h4>
                            <div class="clr">
                            </div>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litApplicationDate" runat="server" Text="<%$ Resources:Resource, APPLICATION_DATE%>"></asp:Literal></label><br />
                                    <asp:TextBox ID="txtApplicationDate" Style="width: 186px;" runat="server" TabIndex="57"
                                        CssClass="text-box" placeholder="Enter Application Date" AutoComplete="off" MaxLength="10"
                                        onchange="DoBlur(this,'spanApplicationDate','Application Date');" onblur="DoBlur(this,'ApplicationDate','');"></asp:TextBox>
                                    <asp:Image ID="imgDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                        vertical-align: middle" AlternateText="Calender" runat="server" />
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtApplicationDate"
                                        FilterType="Custom,Numbers" ValidChars="/">
                                    </asp:FilteredTextBoxExtender>
                                    <div class="space">
                                    </div>
                                    <span id="spanApplicationDate" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litConnectionDate" runat="server" Text="<%$ Resources:Resource, CONNECTION_DATE%>"></asp:Literal></label><br />
                                    <asp:TextBox ID="txtConnectionDate" Style="width: 186px;" runat="server" TabIndex="58"
                                        CssClass="text-box" placeholder="Enter Connection Date" AutoComplete="off" MaxLength="10"
                                        onchange="DoBlur(this,'spanConnectionDate','Connection Date');" onblur="DoBlur(this,'Connection Date','');"></asp:TextBox>
                                    <asp:Image ID="Image1" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                        vertical-align: middle" AlternateText="Calender" runat="server" />
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtConnectionDate"
                                        FilterType="Custom,Numbers" ValidChars="/">
                                    </asp:FilteredTextBoxExtender>
                                    <span id="spanConnectionDate" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box3">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litSetupDate" runat="server" Text="<%$ Resources:Resource, SETUP_DATE%>"></asp:Literal></label><br />
                                    <asp:TextBox ID="txtSetupDate" Style="width: 186px;" runat="server" TabIndex="59"
                                        CssClass="text-box" placeholder="Enter Setup Date" AutoComplete="off" MaxLength="10"
                                        onchange="DoBlur(this,'spanSetupDate','Setup Date');" onblur="DoBlur(this,'SetupDate','');"></asp:TextBox>
                                    <asp:Image ID="Image2" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                        vertical-align: middle" AlternateText="Calender" runat="server" />
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtSetupDate"
                                        FilterType="Custom,Numbers" ValidChars="/">
                                    </asp:FilteredTextBoxExtender>
                                    <div class="space">
                                    </div>
                                    <span id="spanSetupDate" class="span_color"></span>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litCertifiedBy" runat="server" Text="<%$ Resources:Resource, CERTIFIED_BY%>"></asp:Literal></label><br />
                                    <asp:DropDownList ID="ddlCertifiedBy" runat="server" TabIndex="60" CssClass="text-box text-select">
                                        <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                    <div class="space">
                                    </div>
                                    <span id="spanCertifiedBy" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litSeal1" runat="server" Text="<%$ Resources:Resource, SEAL_1%>"></asp:Literal></label><br />
                                    <asp:TextBox ID="txtSeal1" runat="server" TabIndex="61" CssClass="text-box" placeholder="Enter Seal 1"
                                        MaxLength="50"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender38" runat="server" TargetControlID="txtSeal1"
                                        FilterType="LowercaseLetters, UppercaseLetters, Numbers">
                                    </asp:FilteredTextBoxExtender>
                                    <div class="space">
                                    </div>
                                    <span id="spanSeal1" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box3">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litSeal2" runat="server" Text="<%$ Resources:Resource, SEAL_2%>"></asp:Literal></label><br />
                                    <asp:TextBox ID="txtSeal2" runat="server" TabIndex="62" CssClass="text-box" placeholder="Enter Seal 2"
                                        MaxLength="50"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender39" runat="server" TargetControlID="txtSeal2"
                                        FilterType="LowercaseLetters, UppercaseLetters, Numbers">
                                    </asp:FilteredTextBoxExtender>
                                    <div class="space">
                                    </div>
                                    <span id="spanSeal2" class="span_color"></span>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div class="inner-box1">
                            </div>
                            <div class="inner-box2">
                            </div>
                            <div class="inner-box3">
                            </div>
                            <div class="clr">
                            </div>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litOldAccountNo" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal></label><br />
                                    <asp:TextBox ID="txtOldAccountNo" runat="server" TabIndex="63" CssClass="text-box"
                                        placeholder="Enter Old AccountNo" MaxLength="20" AutoPostBack="True" OnTextChanged="txtOldAccountNo_TextChanged"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender37" runat="server" TargetControlID="txtOldAccountNo"
                                        FilterType="Numbers">
                                    </asp:FilteredTextBoxExtender>
                                    <div class="space">
                                    </div>
                                    <span id="spanOldAccountNo" class="span_color"></span><span id="spnOldACStatus" runat="server">
                                    </span>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litApplicationProcessedBy" runat="server" Text="<%$ Resources:Resource, APPLICATION_PROCCESED_BY%>"></asp:Literal>
                                    </label>
                                    <br />
                                    <asp:RadioButtonList ID="rblApplicationProcessedBy" RepeatDirection="Horizontal"
                                        onclick="OnOffAppProcessBy();" runat="server" TabIndex="64">
                                        <asp:ListItem Text="BEDC" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Others" Value="2"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <br />
                                <span id="spanApplicationProcessedBy" class="span_color"></span>
                            </div>
                            <div class="inner-box2">
                                <div id="divInstalledBy" class="text-inner" style="display: none;">
                                    <label for="name">
                                        <asp:Literal ID="litInstalledBy" runat="server" Text="<%$ Resources:Resource, INSTALLED_BY%>"></asp:Literal></label><br />
                                    <asp:DropDownList ID="ddlInstalledBy" runat="server" TabIndex="65" CssClass="text-box text-select">
                                        <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                    <div class="space">
                                    </div>
                                    <span id="spanInstalledBy" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box3">
                                <div id="divAgencyName" class="text-inner" style="display: none;">
                                    <label for="name">
                                        <asp:Literal ID="litAgency" runat="server" Text="<%$ Resources:Resource, AGENCY_NAME%>"></asp:Literal></label><br />
                                    <asp:DropDownList ID="ddlAgency" runat="server" TabIndex="66" CssClass="text-box text-select">
                                        <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                    <div class="space">
                                    </div>
                                    <span id="spanAgency" class="span_color"></span>
                                </div>
                            </div>
                            <div class="clr">
                                <br />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="clr">
                </div>
                <asp:UpdatePanel ID="updPnlIdentity" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="out-bor" id="divIdentityInfo">
                            <div class="clr">
                            </div>
                            <h4 class="subHeading">
                                Identity Information</h4>
                            <div class="clr">
                            </div>
                            <div id="divIdentity">
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litIdentityType" runat="server" Text="Type"></asp:Literal></label><br />
                                        <asp:DropDownList ID="ddlIdentityType" runat="server" TabIndex="67" class="text-box text-select identityIDClass">
                                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                        <div class="space">
                                        </div>
                                        <span id="spanIdentityType" style="margin-left: 2px;" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box2">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litIdentityNo" runat="server" Text="Number"></asp:Literal></label><br />
                                        <asp:TextBox ID="txtIdentityNo" runat="server" TabIndex="68" placeholder="Enter Identity No"
                                            CssClass="text-box identityNumClass" MaxLength="100"></asp:TextBox>
                                        <div class="space">
                                        </div>
                                        <span id="spanIdentityNo" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                            </div>
                        </div>
                        <div class="box_total_b">
                            <div class="search_div">
                                <asp:LinkButton ID="btnAddIdntity" runat="server" TabIndex="69" CssClass="addButton"
                                    OnClientClick="return AddIdentityDiv();">Add</asp:LinkButton></div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="out-bor">
                    <div class="clr">
                    </div>
                    <h4 class="subHeading">
                        Uploads</h4>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="IdentityTitle">
                                <asp:Literal ID="Literal2" runat="server" Text="Uploads"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <div id="divFile" class="fltl">
                                <asp:FileUpload ID="fupDocument" runat="server" TabIndex="70" CssClass="fltl" onchange="return filevalidate(this,this.value,'Photo')" />
                            </div>
                            <div class="space">
                            </div>
                            <br />
                        </div>
                        <div class="text-inner">
                            <br />
                            <div style="width: 720px; color: Gray;">
                                *Note:- Selected documents will be uploaded. Click on Add Button to add more Documents.</div>
                        </div>
                    </div>
                    <br />
                    <span id="spanPhoto" class="span_color"></span>
                </div>
            </div>
            <div class="box_total_b">
                <div class="search_div">
                    <asp:LinkButton ID="lnkAddFiles" runat="server" TabIndex="71" CssClass="addButton"
                        OnClientClick="return AddFile();">Add</asp:LinkButton></div>
                <asp:LinkButton ID="lnkBackToRegistration" runat="server" OnClick="lnkBackToRegistration_Click"></asp:LinkButton>
            </div>
            <div id="divUserDefineFields" runat="server" class="out-bor">
                <div class="clr">
                </div>
                <h4 class="subHeading">
                    User Defined Fields</h4>
                <div class="clr">
                </div>
                <uc2:UserDefinedControls ID="UserDefinedControls1" runat="server" TabIndex="72" />
            </div>
            <div id="divFinishActions" runat="server" class="clr">
                <asp:Button ID="btnFinish" runat="server" TabIndex="73" CssClass="box_s finesh" Text="<%$ Resources:Resource, SAVE_CUSTOMER%>"
                    OnClick="btnFinish_Click" />
                <%--Popus Message Starts--%>
                <asp:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="hfMessage"
                    PopupControlID="pnlMessageValidation" BackgroundCssClass="popbg" CancelControlID="btnMessageOk">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hfMessage" runat="server" />
                <asp:Panel ID="pnlMessageValidation" runat="server" DefaultButton="" CssClass="modalPopup"
                    Style="display: none; border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblMessageValidation" runat="server"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnMessageOk" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%--Popus Pole Ends--%>
            </div>
        </div>
        <asp:HiddenField ID="hfCurrentIdentity" runat="server" Value="1" />
        <asp:HiddenField ID="hfTotalIdentity" runat="server" Value="1" />
        <asp:HiddenField ID="hfIdentityIdList" runat="server" Value="1" />
        <asp:HiddenField ID="hfIdentityNumberList" runat="server" Value="1" />
        <asp:HiddenField ID="hfMeterNo" runat="server" Value="0" />
        <asp:HiddenField ID="hfControlsIDParent" runat="server" />
        <asp:HiddenField ID="hfUserControlFieldsID" runat="server" />
        <asp:HiddenField ID="hfUserControlFieldsValue" runat="server" />
        <asp:Literal ID="litText" runat="server" Visible="false"></asp:Literal>
    </div>
    </div>
    <div id="divSuccessMessage" runat="server" visible="false" style="margin-bottom: 190px;">
        <div class="out-bor">
            <div class="info">
                Registration</div>
            <div class="success" id="divStatusSuccess" runat="server">
                <asp:Label ID="lblStatusSuccess" runat="server"></asp:Label>
            </div>
            <div class="error" id="divStatusError" runat="server">
                <asp:Label ID="lblStatusError" runat="server"></asp:Label>
            </div>
            &nbsp;Click For New Registration&nbsp;
            <asp:LinkButton ID="lnkNewRegstration" runat="server" Text="Here" OnClick="lnkNewRegstration_Click"></asp:LinkButton>.
        </div>
    </div>
    <%--==============Validation script ref starts==============--%>
    <%--<script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>--%>
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/NewCustomer.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <script type="text/javascript">
        $(document).ready(function () {
            Calendar('<%=txtApplicationDate.ClientID %>', '<%=imgDate.ClientID %>', "-2:+8");
            Calendar('<%=txtConnectionDate.ClientID %>', '<%=Image1.ClientID %>', "-2:+8");
            Calendar('<%=txtSetupDate.ClientID %>', '<%=Image2.ClientID %>', "-2:+8");
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            Calendar('<%=txtApplicationDate.ClientID %>', '<%=imgDate.ClientID %>', "-2:+8");
            Calendar('<%=txtConnectionDate.ClientID %>', '<%=Image1.ClientID %>', "-2:+8");
            Calendar('<%=txtSetupDate.ClientID %>', '<%=Image2.ClientID %>', "-2:+8");
        });     
    
    </script>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
</asp:Content>
