﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for Consumer Registration
                     
 Developer        : id077-Neeraj Kanojiya
 Creation Date    : 24-DEC-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using System.Threading;
using System.Globalization;
using UMS_NigeriaBE;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using System.Xml;
using System.IO;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.Text;
using System.Collections;
using System.Web.UI.HtmlControls;
using UMS_Nigeria.UserControls;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class NewCustomer : System.Web.UI.Page
    {
        #region Members

        CommonMethods objCommonMethods = new CommonMethods();
        iDsignBAL objIdsignBal = new iDsignBAL();
        BillCalculatorBAL objBillCalculatorBAL = new BillCalculatorBAL();
        ConsumerBal objConsumerBal = new ConsumerBal();
        CustomerRegistrationBE objCustomerRegistrationBE = new CustomerRegistrationBE();
        ConsumerBe objConsumerBe = new ConsumerBe();
        PoleBAL objPoleBAL = new PoleBAL();
        DataSet dsPoleDetails = new DataSet();
        MastersBE objMastersBE = new MastersBE();
        MastersListBE objMastersListBE = new MastersListBE();
        MastersBAL objMastersBAL = new MastersBAL();
        PoleBE objPoleBE = new PoleBE();
        AreaBe objAreaBe = new AreaBe();
        AreaBAL objAreaBAL = new AreaBAL();
        string Key = "NewCustomers";
        string DocumentNameList = string.Empty;
        string DocumentPathList = string.Empty;
        GovtAccountTypeListBE objGovtAccountTypeListBe = new GovtAccountTypeListBE();
        GovtAccountTypeBAL objGovtAccountTypeBAL = new GovtAccountTypeBAL();

        bool IsIdentityMandatory = false;
        #endregion

        #region Properties
        public string BusinessUnitName
        {
            get { return string.IsNullOrEmpty(ddlBusinessUnitName.SelectedValue) ? objCommonMethods.CollectAllValues(ddlBusinessUnitName, ",") : ddlBusinessUnitName.SelectedValue; }
            set { ddlBusinessUnitName.SelectedValue = value.ToString(); }
        }
        public string ServiceUnitName
        {
            get { return string.IsNullOrEmpty(ddlServiceUnitName.SelectedValue) ? objCommonMethods.CollectAllValues(ddlServiceUnitName, ",") : ddlServiceUnitName.SelectedValue; }
            set { ddlServiceUnitName.SelectedValue = value.ToString(); }
        }
        public string ServiceCenterName
        {
            get { return string.IsNullOrEmpty(ddlServiceCenterName.SelectedValue) ? objCommonMethods.CollectAllValues(ddlServiceCenterName, ",") : ddlServiceCenterName.SelectedValue; }
            set { ddlServiceCenterName.SelectedValue = value.ToString(); }
        }
        public string Cycle
        {
            get { return string.IsNullOrEmpty(ddlCycle.SelectedValue) ? objCommonMethods.CollectAllValues(ddlCycle, ",") : ddlCycle.SelectedValue; }
            set { ddlCycle.SelectedValue = value.ToString(); }
        }
        public string BookNo
        {
            get { return string.IsNullOrEmpty(ddlBookNo.SelectedValue) ? objCommonMethods.CollectAllValues(ddlBookNo, ",") : ddlBookNo.SelectedValue; }
            set { ddlBookNo.SelectedValue = value.ToString(); }
        }


        public string CustomerType
        {
            get { return string.IsNullOrEmpty(ddlCustomerType.SelectedItem.Value) ? "0" : ddlCustomerType.SelectedItem.Value; }
        }
        public string OTitle
        {
            get { return string.IsNullOrEmpty(ddlOTitle.SelectedValue) ? "" : ddlOTitle.SelectedItem.Text; }
        }

        public string TTitle
        {
            get { return string.IsNullOrEmpty(ddlTTitle.SelectedValue) ? "" : ddlTTitle.SelectedItem.Text; }
        }

        public string Employee
        {
            get { return string.IsNullOrEmpty(ddlIsBedcEmp.SelectedValue) ? "" : ddlIsBedcEmp.SelectedItem.Value; }
        }

        public string AccountType
        {
            get { return string.IsNullOrEmpty(ddlAccountType.SelectedValue) ? "0" : ddlAccountType.SelectedValue; }
            //set { ddlAccountType.SelectedValue = value.ToString(); }
        }

        public string Tariff
        {
            get { return string.IsNullOrEmpty(ddlTariff.SelectedValue) ? "0" : ddlTariff.SelectedValue; }
        }

        public string ClusterType
        {
            get { return string.IsNullOrEmpty(ddlClusterType.SelectedValue) ? "0" : ddlClusterType.SelectedValue; }
        }

        public string Phase
        {
            get { return string.IsNullOrEmpty(ddlPhase.SelectedValue) ? "0" : ddlPhase.SelectedValue; }
        }

        public string RouteNo
        {
            get { return string.IsNullOrEmpty(ddlRouteNo.SelectedValue) ? "0" : ddlRouteNo.SelectedValue; }
        }

        public string ReadCodeType
        {
            get { return string.IsNullOrEmpty(ddlReadCode.SelectedValue) ? "0" : ddlReadCode.SelectedValue; }
        }

        public string CertifiedBy
        {
            get { return string.IsNullOrEmpty(ddlCertifiedBy.SelectedValue) ? "0" : ddlCertifiedBy.SelectedValue; }
        }

        public string InstalledBy
        {
            get { return string.IsNullOrEmpty(ddlInstalledBy.SelectedValue) ? "0" : ddlInstalledBy.SelectedValue; }
        }

        public string Agency
        {
            get { return string.IsNullOrEmpty(ddlAgency.SelectedValue) ? "0" : ddlAgency.SelectedValue; }
        }

        public string IdentityType
        {
            get { return string.IsNullOrEmpty(ddlIdentityType.SelectedValue) ? "0" : ddlIdentityType.SelectedValue; }
        }

        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {

            //SecurityDongle securityDongle = new SecurityDongle();
            //if (securityDongle.DongleMessage != "Success")
            //{
            //    Response.Redirect("~/ValidateUser.aspx");
            //}

            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                BindDropDowns();
                BindMandatory();
                BindBusinessUnits();
            }
        }

        protected void btnFinish_Click(object sender, EventArgs e)
        {
            CustomerRegistration();
        }

        protected void btnRegistration_Click(object sender, EventArgs e)
        {
            CustomerRegistration();
        }

        protected void idlnkSearchBookNo_Click(object sender, EventArgs e)
        {
            mpeBookSearch.Show();
            ddlServiceUnitName.Focus();
        }

        protected void rptPoleSearch_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    int PoleMasterId = Convert.ToInt32(((Literal)(e.Item.FindControl("litPoleMasterID"))).Text);
                    DropDownList ddlPoleSearchDesc = (DropDownList)(e.Item.FindControl("ddlPoleSearchDesc"));
                    if (PoleMasterId != 0)
                    {

                        var newRowDesc = (from x in dsPoleDetails.Tables[1].AsEnumerable()
                                          where x.Field<int>("PoleMasterId") == PoleMasterId
                                          select new
                                          {
                                              PoleId = x.Field<int>("PoleId"),
                                              Name = x.Field<string>("Name")
                                          });
                        ddlPoleSearchDesc.DataSource = newRowDesc;
                        ddlPoleSearchDesc.DataTextField = "Name";
                        ddlPoleSearchDesc.DataValueField = "PoleId";
                        ddlPoleSearchDesc.DataBind();


                    }

                }
            }
            catch (Exception ex)
            {
                Message("Exception in pole list binding.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        protected void lnkSearchPole_Click(object sender, EventArgs e)
        {
            dllPoleLevelChild1.SelectedIndex
              = dllPoleLevelChild2.SelectedIndex
              = dllPoleLevelChild3.SelectedIndex
              = dllPoleLevelChild4.SelectedIndex
              = dllPoleLevelChild5.SelectedIndex
              = dllPoleLevelChild6.SelectedIndex
              = dllPoleLevelChild7.SelectedIndex
              = dllPoleLevelChild8.SelectedIndex = 0;
            spnPoleStatusPopup.InnerHtml = "";
            spnPoleStatus.InnerHtml = "";
            BindParentPole();
            mpePole.Show();
        }

        protected void rptPoleSearch_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "polenumber")
                {
                    txtPole.Text = ((DropDownList)e.Item.FindControl("ddlPoleSearchDesc")).SelectedItem.Value;
                }
            }
            catch (Exception ex)
            {
                Message("Exception in pole select.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        protected void lnkSearchMeterNo_Click(object sender, EventArgs e)
        {
            mpeMeterList.Show();
        }

        protected void lnkSelectMeterNo_Click(object sender, EventArgs e)
        {
            // txtMeterNo.Text = ddlMeterList.SelectedItem.Value;

            //divMeterNo.Attributes.Add("style", "display:block");
            //divInitialReading.Attributes.Add("style", "display:block");
            //divIsCapmy.Attributes.Add("style", "display:block");
            //divIsCapmyAmount.Attributes.Add("style", "display:block");
        }

        protected void btnSaveFiles_Click(object sender, EventArgs e)
        {
            SaveDocument();
        }

        protected void lnkBackToRegistration_Click(object sender, EventArgs e)
        {
            Response.Redirect(UMS_Resource.NEW_CUST_REG_PAGE);
        }

        protected void lnkNewRegstration_Click(object sender, EventArgs e)
        {
            divstep1.Visible = divFinishActions.Visible = true;
            divSuccessMessage.Visible = false;
            Response.Redirect(UMS_Resource.NEW_CUST_REG_PAGE);
        }

        protected void ddlPoleParent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                dllPoleLevelChild1.SelectedIndex
               = dllPoleLevelChild2.SelectedIndex
               = dllPoleLevelChild3.SelectedIndex
               = dllPoleLevelChild4.SelectedIndex
               = dllPoleLevelChild5.SelectedIndex
               = dllPoleLevelChild6.SelectedIndex
               = dllPoleLevelChild7.SelectedIndex
               = dllPoleLevelChild8.SelectedIndex = 0;
                spnPoleStatusPopup.InnerHtml = "";
                spnPoleStatus.InnerHtml = "";

                objPoleBE.IsParentPole = !Constants.IsParentPole; ;
                objPoleBE.PoleMasterId = Convert.ToInt32(ddlPoleParent.SelectedItem.Value);
                objPoleBE.Name = ddlPoleParent.SelectedItem.Text;
                objCommonMethods.BindPoleByOrderID(dllPoleLevelChild1, objPoleBE, true);
                dllPoleLevelChild1.SelectedIndex = 0;
                //hfPoleCode.Value += getPoleCode(ddlPoleParent.SelectedItem.Text);
                dllPoleLevelChild1.Visible = true;
                mpePole.Show();
            }
            catch (Exception ex)
            {
                Message("Exception in selection of parent pole.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        protected void dllPoleLevelChild1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objPoleBE.IsParentPole = !Constants.IsParentPole; ;
                objPoleBE.PoleMasterId = Convert.ToInt32(dllPoleLevelChild1.SelectedItem.Value);
                objPoleBE.Name = dllPoleLevelChild1.SelectedItem.Text;
                objCommonMethods.BindPoleByOrderID(dllPoleLevelChild2, objPoleBE, true);
                //hfPoleCode.Value += getPoleCode(dllPoleLevelChild1.SelectedItem.Text);
                dllPoleLevelChild2.Visible = true;
                mpePole.Show();
            }
            catch (Exception ex)
            {
                Message("Exception in selection of level 1 pole.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        protected void dllPoleLevelChild2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objPoleBE.IsParentPole = !Constants.IsParentPole; ;
                objPoleBE.PoleMasterId = Convert.ToInt32(dllPoleLevelChild2.SelectedItem.Value);
                objPoleBE.Name = dllPoleLevelChild2.SelectedItem.Text;
                objCommonMethods.BindPoleByOrderID(dllPoleLevelChild3, objPoleBE, true);
                //hfPoleCode.Value += getPoleCode(dllPoleLevelChild2.SelectedItem.Text);
                dllPoleLevelChild3.Visible = true;
                mpePole.Show();
            }
            catch (Exception ex)
            {
                Message("Exception in selection of level 2 pole.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        protected void dllPoleLevelChild3_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objPoleBE.IsParentPole = !Constants.IsParentPole; ;
                objPoleBE.PoleMasterId = Convert.ToInt32(dllPoleLevelChild3.SelectedItem.Value);
                objPoleBE.Name = dllPoleLevelChild3.SelectedItem.Text;
                objCommonMethods.BindPoleByOrderID(dllPoleLevelChild4, objPoleBE, true);
                //hfPoleCode.Value += getPoleCode(dllPoleLevelChild3.SelectedItem.Text);
                dllPoleLevelChild4.Visible = true;
                mpePole.Show();
            }
            catch (Exception ex)
            {
                Message("Exception in selection of level 3 pole.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        protected void dllPoleLevelChild4_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objPoleBE.IsParentPole = !Constants.IsParentPole; ;
                objPoleBE.PoleMasterId = Convert.ToInt32(dllPoleLevelChild4.SelectedItem.Value);
                objPoleBE.Name = dllPoleLevelChild4.SelectedItem.Text;
                objCommonMethods.BindPoleByOrderID(dllPoleLevelChild5, objPoleBE, true);
                //hfPoleCode.Value += getPoleCode(dllPoleLevelChild4.SelectedItem.Text);
                dllPoleLevelChild5.Visible = true;
                mpePole.Show();
            }
            catch (Exception ex)
            {
                Message("Exception in selection of level 4 pole.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        protected void dllPoleLevelChild5_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objPoleBE.IsParentPole = !Constants.IsParentPole; ;
                objPoleBE.PoleMasterId = Convert.ToInt32(dllPoleLevelChild5.SelectedItem.Value);
                objPoleBE.Name = dllPoleLevelChild5.SelectedItem.Text;
                objCommonMethods.BindPoleByOrderID(dllPoleLevelChild6, objPoleBE, true);
                //hfPoleCode.Value += getPoleCode(dllPoleLevelChild5.SelectedItem.Text);
                dllPoleLevelChild6.Visible = true;
                mpePole.Show();
            }
            catch (Exception ex)
            {
                Message("Exception in selection of level 5 pole.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        protected void dllPoleLevelChild6_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objPoleBE.IsParentPole = !Constants.IsParentPole; ;
                objPoleBE.PoleMasterId = Convert.ToInt32(dllPoleLevelChild6.SelectedItem.Value);
                objPoleBE.Name = dllPoleLevelChild6.SelectedItem.Text;
                objCommonMethods.BindPoleByOrderID(dllPoleLevelChild7, objPoleBE, true);
                //hfPoleCode.Value += getPoleCode(dllPoleLevelChild6.SelectedItem.Text);
                dllPoleLevelChild7.Visible = true;
                mpePole.Show();

            }
            catch (Exception ex)
            {
                Message("Exception in selection of level 6 pole.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        protected void dllPoleLevelChild7_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objPoleBE.IsParentPole = !Constants.IsParentPole; ;
                objPoleBE.PoleMasterId = Convert.ToInt32(dllPoleLevelChild7.SelectedItem.Value);
                objPoleBE.Name = dllPoleLevelChild7.SelectedItem.Text;
                objCommonMethods.BindPoleByOrderID(dllPoleLevelChild8, objPoleBE, true);
                //hfPoleCode.Value += getPoleCode(dllPoleLevelChild7.SelectedItem.Text);
                dllPoleLevelChild8.Visible = true;
                mpePole.Show();
            }
            catch (Exception ex)
            {
                Message("Exception in selection of level 7 pole.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        protected void dllPoleLevelChild8_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPoleCode.Value = getPoleCode(ddlPoleParent.SelectedItem.Text)
                                + getPoleCode(dllPoleLevelChild1.SelectedItem.Text)
                                + getPoleCode(dllPoleLevelChild2.SelectedItem.Text)
                                + getPoleCode(dllPoleLevelChild3.SelectedItem.Text)
                                + getPoleCode(dllPoleLevelChild4.SelectedItem.Text)
                                + getPoleCode(dllPoleLevelChild5.SelectedItem.Text)
                                + getPoleCode(dllPoleLevelChild6.SelectedItem.Text)
                                + getPoleCode(dllPoleLevelChild7.SelectedItem.Text)
                                + getPoleCode(dllPoleLevelChild8.SelectedItem.Text);

                spnPoleStatusPopup.InnerHtml = "";
                string poleNumber = string.Empty;
                //hfPoleCode.Value += getPoleCode(dllPoleLevelChild8.SelectedItem.Text);
                poleNumber = IsPoleExists(hfPoleCode.Value);
                if (!string.IsNullOrEmpty(poleNumber))
                {
                    hfPoleID.Value = dllPoleLevelChild8.SelectedItem.Value;
                    spnPoleStatusPopup.Attributes.Add("class", "span_color_green");
                    spnPoleStatusPopup.InnerHtml = "Pole No.: " + hfPoleCode.Value + " is available.";

                }
                else
                {
                    spnPoleStatusPopup.Attributes.Add("class", "span_color");
                    spnPoleStatusPopup.InnerHtml = "Pole No.: " + hfPoleCode.Value + " is not available.";
                }
                mpePole.Show();
            }
            catch (Exception ex)
            {
                spnPoleStatusPopup.InnerHtml = "";
                spnPoleStatus.InnerHtml = "";
                Message("Exception in selection of level 8 pole.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        protected void btnSelectPole_click(object sender, EventArgs e)
        {
            try
            {
                txtPole.Text = string.Empty;
                objPoleBE.IsParentPole = !Constants.IsParentPole; ;
                objPoleBE.PoleMasterId = Convert.ToInt32(dllPoleLevelChild8.SelectedItem.Value);
                objPoleBE.Name = dllPoleLevelChild8.SelectedItem.Text;
                txtPole.Text = hfPoleCode.Value;//dllPoleLevelChild8.SelectedItem.Value;
                hfPoleCode.Value += getPoleCode(dllPoleLevelChild8.SelectedItem.Text);
                dllPoleLevelChild8.Visible = true;

                mpePole.Hide();
            }
            catch (Exception ex)
            {
                Message("Exception in selection of level 9  pole.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        protected void btnDummyMeterDials_click(object sender, EventArgs e)
        {

        }

        protected void gvMeterList_rowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "select")
                {
                    Control ctrl = e.CommandSource as Control;
                    if (ctrl != null)
                    {
                        GridViewRow row = ctrl.Parent.NamingContainer as GridViewRow;
                        string MeterNo = ((Literal)row.FindControl("litMeterNo")).Text;
                        string IsCAPMIMeterString = ((Literal)row.FindControl("IsCAPMIMeterString")).Text;
                        string litGridCAPMIAmount = ((Literal)row.FindControl("litGridCAPMIAmount")).Text;
                        hfMeterDials.Value = ((Literal)row.FindControl("litMeterDials")).Text;
                        int MeterDials = Convert.ToInt32(((Literal)row.FindControl("litMeterDials")).Text);
                        cbIsCAPMY.Checked = (IsCAPMIMeterString == "Yes");
                        //cbIsCAPMY.Style.Add(HtmlTextWriterStyle.Visibility, (IsCAPMIMeterString == "Yes").ToString());
                        if (IsCAPMIMeterString != "Yes")
                        {
                            litIsCapmi.Visible = false;
                            cbIsCAPMY.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
                        }
                        else
                        {
                            cbIsCAPMY.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "inline";
                            litIsCapmi.Visible = true;
                        }
                        txtAmount.Text = litGridCAPMIAmount;
                        cbIsCAPMY.Enabled = txtAmount.Enabled = false;
                        txtPresentReading.MaxLength = MeterDials;
                        updtPnlMeterDetails.Update();
                        txtMeterNo.Text = e.CommandArgument.ToString();
                        hfMeterNo.Value = MeterNo;
                        //Page.ClientScript.RegisterStartupScript(Page.GetType(), "my", "alert('');", true);

                    }
                    gvMeterList.Visible = false;
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "Script1", "MeterReadVisible();", true);
                }
            }
            catch (Exception ex)
            {
                Message("Exception in selection of meter no.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        protected void txtPArea_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtPArea.Text))
                {
                    objAreaBe.AreaCode = Convert.ToInt32(txtPArea.Text);
                    objAreaBe = objIdsignBal.DeserializeFromXml<AreaBe>(objAreaBAL.IsAreaExists(objAreaBe));
                    if (!objAreaBe.IsExists)
                    {
                        spnPostalArea.InnerText = "Area code " + txtPArea.Text + " is not available.";
                        spnPostalArea.Attributes.Add("class", "span_color");
                        txtPArea.Text = string.Empty;
                        TextBox txtSArea1 = (TextBox)updtPnlPostalAddress.FindControl("txtSArea");
                        txtSArea1.Text = string.Empty;
                        updtPnlServiceAddress.UpdateMode = UpdatePanelUpdateMode.Conditional;
                        txtSArea.Text = "";
                        updtPnlServiceAddress.Update();
                        updtPnlServiceAddress.UpdateMode = UpdatePanelUpdateMode.Always;
                    }
                    else
                    {
                        spnPostalArea.InnerText = "Area code " + txtPArea.Text + " is available.";
                        spnPostalArea.Attributes.Add("class", "span_color_green");
                    }
                }
            }
            catch (Exception ex)
            {
                Message("Exception in postal area existence check.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        protected void txtSArea_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtSArea.Text))
                {
                    txtSArea.Text = string.Empty;

                    //ClientScript.RegisterStartupScript(GetType(), "id", "callMyJSFunction()", true); 
                    //Response.Write("<script>document.getElementById('UMSNigeriaBody_txtSArea').value = '';</script>"); 
                    return;
                }
                objAreaBe.AreaCode = Convert.ToInt32(txtSArea.Text);
                objAreaBe = objIdsignBal.DeserializeFromXml<AreaBe>(objAreaBAL.IsAreaExists(objAreaBe));
                if (!objAreaBe.IsExists)
                {
                    spnServericeArea.InnerText = "Area code " + txtSArea.Text + " is not available.";
                    spnServericeArea.Attributes.Add("class", "span_color");
                    txtSArea.Text = string.Empty;
                }
                else
                {
                    spnServericeArea.InnerText = "Area code " + txtSArea.Text + " is available.";
                    spnServericeArea.Attributes.Add("class", "span_color_green");
                }
            }
            catch (Exception ex)
            {
                Message("Exception in service area existence check.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        protected void txtPole_TextChanged(object sender, EventArgs e)
        {
            dllPoleLevelChild1.SelectedIndex
             = dllPoleLevelChild2.SelectedIndex
             = dllPoleLevelChild3.SelectedIndex
             = dllPoleLevelChild4.SelectedIndex
             = dllPoleLevelChild5.SelectedIndex
             = dllPoleLevelChild6.SelectedIndex
             = dllPoleLevelChild7.SelectedIndex
             = dllPoleLevelChild8.SelectedIndex = 0;
            BindParentPole();
            string poleNumber = IsPoleExists(txtPole.Text);
            if (poleNumber.Length == 14)
            {
                spnPoleStatus.InnerHtml = "Pole no. " + poleNumber + " is available!";
                spnPoleStatus.Attributes.Add("class", "span_color_green");
            }
            else
            {
                spnPoleStatus.InnerHtml = "Pole no. " + txtPole.Text + " does not available!";
                spnPoleStatus.Attributes.Add("class", "span_color");
                txtPole.Text = string.Empty;
            }
        }

        protected void ddlAccountType_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblGovtAccountTypeName.Text = hfGovtAccountTypeID.Value = string.Empty;
            btnSelectGovt.Visible = false;
            if (!string.IsNullOrEmpty(ddlAccountType.SelectedValue))
            {
                if (Convert.ToInt32(ddlAccountType.SelectedValue) == 2)
                {
                    tvGovtType.Nodes.Clear();
                    PopulateRootLevel();
                    TreeNode tn = tvGovtType.Nodes[0];
                    tn.Selected = litFullPath.Visible = true;
                    tn.Expand();
                    mpeGovtAccType.Show();
                }
            }
        }

        protected void btnGovtAccountType_Click(object sender, EventArgs e)
        {
            //event to fire ddlAccountType_SelectedIndexChanged from NewCustomer.js
        }

        protected void ddlCustomerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtMeterNo.Text = string.Empty;
        }


        protected void tvGovtType_TreeNodePopulate(object sender, TreeNodeEventArgs e)
        {
            objGovtAccountTypeListBe =
                objIdsignBal.DeserializeFromXml<GovtAccountTypeListBE>(
                    objGovtAccountTypeBAL.GetGovtAccountTypeList());
            PopulateSubLevel(int.Parse(e.Node.Value), e.Node);
            mpeGovtAccType.Show();
        }

        protected void tvGovtType_SelectedNodeChanged(object sender, EventArgs e)
        {
            litFullPath.Text = getFullNodepath();
            mpeGovtAccType.Show();
            Session["selectedNode"] = tvGovtType.SelectedNode;
            tvGovtType.SelectedNode.SelectAction = TreeNodeSelectAction.None;
            TreeNode rootNode = tvGovtType.Nodes[0];
            DisableSelectionForNonSelectedNodes(rootNode);
            if (tvGovtType.SelectedNode.ChildNodes.Count == 0)
                btnSelectGovt.Visible = true;
            else
                btnSelectGovt.Visible = false;

        }

        //Calling this function for non selected nodes as double clicking the same node raises an issue in popup
        public void DisableSelectionForNonSelectedNodes(TreeNode tn)
        {
            if (tvGovtType.SelectedNode.Value != tn.Value)
                tn.SelectAction = TreeNodeSelectAction.SelectExpand;
            foreach (TreeNode sublevel in tn.ChildNodes)
                DisableSelectionForNonSelectedNodes(sublevel);
        }
        protected void btnSelectGovt_Click(object sender, EventArgs e)
        {
            TreeNode selectedNode = (TreeNode)Session["selectedNode"];
            lblGovtAccountTypeName.Text = selectedNode.Text;
            hfGovtAccountTypeID.Value = selectedNode.Value;
        }

        protected void txtOldAccountNo_TextChanged(object sender, EventArgs e)
        {
            IsOldAccountNoExists();
        }

        protected void txtOEmailId_TextChanged(object sender, EventArgs e)
        {
            //IsEmailAlreadyExists(txtOEmailId, true);
        }

        protected void txtTEmailId_TextChanged(object sender, EventArgs e)
        {
            //IsEmailAlreadyExists(txtTEmailId, false);
        }

        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);

            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void CustomerRegistration()
        {
            bool IsInputValid = true;
            try
            {
                objCustomerRegistrationBE = new CustomerRegistrationBE();
                string BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                objCustomerRegistrationBE.BU_ID = BU_ID;
                objCustomerRegistrationBE.FunctionId = (int)EnumApprovalFunctions.CustomerRegistration;

                #region Postal AddressDetails

                #region Postal
                objCustomerRegistrationBE.HouseNoPostal = txtPHNo.Text;
                objCustomerRegistrationBE.StreetPostal = txtPStreet.Text;
                objCustomerRegistrationBE.CityPostaL = txtPLGAVillage.Text;
                objCustomerRegistrationBE.PZipCode = txtPZip.Text;
                objCustomerRegistrationBE.IsServiceAddress = cbIsSameAsPostal.Checked;
                objCustomerRegistrationBE.AreaPostal = txtPArea.Text;
                objCustomerRegistrationBE.IsCommunicationPostal = rblCommunicationAddress.Items[0].Selected;
                #endregion

                #region Service
                if (!cbIsSameAsPostal.Checked)
                {
                    objCustomerRegistrationBE.HouseNoService = txtSHNo.Text;
                    objCustomerRegistrationBE.StreetService = txtSStreet.Text;
                    objCustomerRegistrationBE.CityService = txtSLGAVillage.Text;
                    objCustomerRegistrationBE.SZipCode = txtSZip.Text;
                    objCustomerRegistrationBE.IsServiceAddress = cbIsSameAsPostal.Checked;
                    objCustomerRegistrationBE.AreaService = txtSArea.Text;
                    objCustomerRegistrationBE.IsCommunicationService = rblCommunicationAddress.Items[1].Selected;
                }
                #endregion


                #endregion

                #region Tenent Details.
                if (cbIsTenent.Checked)
                {
                    //objCustomerRegistrationBE.TitleTanent = ddlTTitle.SelectedItem.Text;
                    objCustomerRegistrationBE.TitleTanent = TTitle;
                    objCustomerRegistrationBE.FirstNameTanent = txtTFName.Text;
                    objCustomerRegistrationBE.MiddleNameTanent = txtTMName.Text;
                    objCustomerRegistrationBE.LastNameTanent = txtTLName.Text;
                    objCustomerRegistrationBE.PhoneNumberTanent = txtTHPhoneCode.Text + "-" + txtTHPhone.Text;
                    objCustomerRegistrationBE.AlternatePhoneNumberTanent = txtTAPhoneCode.Text + "-" + txtTAPhone.Text;
                    objCustomerRegistrationBE.EmailIdTanent = txtTEmailId.Text;
                }
                #endregion

                #region Customer Details
                objCustomerRegistrationBE.TitleLandlord = OTitle;
                //objCustomerRegistrationBE.TitleLandlord = ddlOTitle.SelectedItem.Text;
                objCustomerRegistrationBE.FirstNameLandlord = txtOFName.Text;
                objCustomerRegistrationBE.MiddleNameLandlord = txtOMName.Text;
                objCustomerRegistrationBE.LastNameLandlord = txtOLName.Text;
                objCustomerRegistrationBE.HomeContactNumberLandlord = txtOHPhoneCode.Text + "-" + txtOHPhone.Text;
                objCustomerRegistrationBE.BusinessPhoneNumberLandlord = txtOBPhoneCode.Text + "-" + txtOBPhone.Text;
                objCustomerRegistrationBE.OtherPhoneNumberLandlord = txtOOPhoneCode.Text + "-" + txtOOPhone.Text;
                objCustomerRegistrationBE.EmailIdLandlord = txtOEmailId.Text;
                objCustomerRegistrationBE.KnownAs = txtOKnownAs.Text;
                objCustomerRegistrationBE.IsSameAsService = cbIsSameAsPostal.Checked;
                objCustomerRegistrationBE.IsSameAsTenent = !cbIsTenent.Checked;
                if (fupDocument.HasFile) objCustomerRegistrationBE.DocumentNo = fupDocument.FileName;

                if (!string.IsNullOrEmpty(txtApplicationDate.Text.Trim()))
                    objCustomerRegistrationBE.ApplicationDate = objCommonMethods.Convert_MMDDYY(txtApplicationDate.Text);
                else
                    objCustomerRegistrationBE.ApplicationDate = string.Empty;

                if (!string.IsNullOrEmpty(txtConnectionDate.Text.Trim()))
                    objCustomerRegistrationBE.ConnectionDate = objCommonMethods.Convert_MMDDYY(txtConnectionDate.Text);
                else
                    objCustomerRegistrationBE.ConnectionDate = string.Empty;

                if (!string.IsNullOrEmpty(txtSetupDate.Text.Trim()))
                    objCustomerRegistrationBE.SetupDate = objCommonMethods.Convert_MMDDYY(txtSetupDate.Text);
                else
                    objCustomerRegistrationBE.SetupDate = string.Empty;

                objCustomerRegistrationBE.IsBEDCEmployee = cbIsBEDCEmployee.Checked;
                objCustomerRegistrationBE.IsVIPCustomer = cbIsVIP.Checked;
                objCustomerRegistrationBE.OldAccountNo = txtOldAccountNo.Text;
                if (cbIsBEDCEmployee.Checked) objCustomerRegistrationBE.EmployeeCode = Convert.ToInt32(Employee);
                objCustomerRegistrationBE.CreatedBy = Convert.ToString(Session[UMS_Resource.SESSION_LOGINID]);
                #endregion

                #region Procedural Details
                objCustomerRegistrationBE.CustomerTypeId = Convert.ToInt32(CustomerType);
                objCustomerRegistrationBE.BookNo = hfBookNo.Value;
                objCustomerRegistrationBE.PoleID = hfPoleID.Value; // txtPole.Text;
                //if (Convert.ToInt32(ddlReadCode.SelectedItem.Value) != (int)ReadCode.Direct) objCustomerRegistrationBE.MeterNumber = txtMeterNo.Text;//hfMeterNo.Value
                if (Convert.ToInt32(ReadCodeType) != (int)ReadCode.Direct) objCustomerRegistrationBE.MeterNumber = txtMeterNo.Text;//hfMeterNo.Value
                //objCustomerRegistrationBE.TariffClassID = Convert.ToInt32(ddlTariff.SelectedItem.Value);
                objCustomerRegistrationBE.TariffClassID = Convert.ToInt32(Tariff);
                objCustomerRegistrationBE.IsEmbassyCustomer = cbIsEmbassy.Checked;
                if (cbIsEmbassy.Checked) objCustomerRegistrationBE.EmbassyCode = txtEmbassy.Text;
                //objCustomerRegistrationBE.PhaseId = Convert.ToInt32(ddlPhase.SelectedItem.Value);
                objCustomerRegistrationBE.PhaseId = Convert.ToInt32(Phase);
                //objCustomerRegistrationBE.ReadCodeID = Convert.ToInt32(ddlReadCode.SelectedItem.Value);
                objCustomerRegistrationBE.ReadCodeID = Convert.ToInt32(ReadCodeType);
                objCustomerRegistrationBE.AccountTypeId = Convert.ToInt32(AccountType);
                //objCustomerRegistrationBE.ClusterCategoryId = Convert.ToInt32(ddlClusterType.SelectedItem.Value);
                objCustomerRegistrationBE.ClusterCategoryId = Convert.ToInt32(ClusterType);
                //objCustomerRegistrationBE.RouteSequenceNumber = Convert.ToInt32(ddlRouteNo.SelectedItem.Value);
                objCustomerRegistrationBE.RouteSequenceNumber = Convert.ToInt32(RouteNo);
                if (!string.IsNullOrEmpty(hfGovtAccountTypeID.Value)) objCustomerRegistrationBE.MGActTypeID = Convert.ToInt32(hfGovtAccountTypeID.Value);
                #endregion

                #region Identity Details
                if (hfIdentityIdList.Value != "" && hfIdentityNumberList.Value != "")
                {
                    objCustomerRegistrationBE.IdentityTypeIdList = hfIdentityIdList.Value; //Convert.ToInt32(ddlIdentityType.SelectedItem.Value);
                    objCustomerRegistrationBE.IdentityNumberList = hfIdentityNumberList.Value;
                }
                objCustomerRegistrationBE.UploadDocumentID = fupDocument.FileName;
                #endregion

                #region Application Process Details
                //objCustomerRegistrationBE.CertifiedBy = ddlCertifiedBy.SelectedItem.Value;
                objCustomerRegistrationBE.CertifiedBy = CertifiedBy;
                objCustomerRegistrationBE.Seal1 = txtSeal1.Text;
                objCustomerRegistrationBE.Seal2 = txtSeal2.Text;
                if (rblApplicationProcessedBy.SelectedItem != null)
                    objCustomerRegistrationBE.ApplicationProcessedBy = rblApplicationProcessedBy.SelectedItem.Text;
                #endregion

                #region Application Process Person Details
                //if (rblApplicationProcessedBy.Items[0].Selected) objCustomerRegistrationBE.InstalledBy = ddlInstalledBy.SelectedItem.Value;
                if (rblApplicationProcessedBy.Items[0].Selected) objCustomerRegistrationBE.InstalledBy = InstalledBy;
                //if (rblApplicationProcessedBy.Items[1].Selected) objCustomerRegistrationBE.AgencyId = ddlAgency.SelectedItem.Value;
                if (rblApplicationProcessedBy.Items[1].Selected) objCustomerRegistrationBE.AgencyId = Agency;
                #endregion

                #region Customer Active Details
                //if (Convert.ToInt32(ddlReadCode.SelectedItem.Value) == (int)ReadCode.Read) objCustomerRegistrationBE.IsCAPMI = cbIsCAPMY.Checked;
                //if (Convert.ToInt32(ddlReadCode.SelectedItem.Value) != (int)ReadCode.Direct)
                if (Convert.ToInt32(ReadCodeType) == (int)ReadCode.Read) objCustomerRegistrationBE.IsCAPMI = cbIsCAPMY.Checked;
                if (Convert.ToInt32(ReadCodeType) != (int)ReadCode.Direct)
                {
                    if (cbIsCAPMY.Checked) objCustomerRegistrationBE.MeterAmount = Convert.ToDecimal(txtAmount.Text.Replace(",", string.Empty));
                    objCustomerRegistrationBE.PresentReading = Convert.ToInt32(txtPresentReading.Text);
                }
                objCustomerRegistrationBE.InitialBillingKWh = Convert.ToInt32(txtInitialBilling.Text);
                if (!string.IsNullOrEmpty(txtPresentReading.Text)) objCustomerRegistrationBE.InitialReading = Convert.ToInt32(txtPresentReading.Text);//Faiz-ID103
                #endregion

                #region UDF Values List

                if (GetUDFValues())
                {
                    objCustomerRegistrationBE.UDFTypeIdList = hfUserControlFieldsID.Value;
                    objCustomerRegistrationBE.UDFValueList = hfUserControlFieldsValue.Value;
                }

                #endregion
            }
            catch (Exception ex)
            {
                IsInputValid = false;
                Message("Exception in registration input.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
            #region Registration
            try
            {
                if (IsInputValid)
                {

                    SaveDocument();
                    objCustomerRegistrationBE.DocumentName = DocumentNameList;
                    objCustomerRegistrationBE.Path = DocumentPathList;
                    
                    bool IsFinalApproval = false;
                    IsFinalApproval = objCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.CustomerRegistration, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());
                    if (!IsFinalApproval)
                    {
                        objCustomerRegistrationBE.ApprovalStatusId = (int)EnumApprovalStatus.Process;
                        objCustomerRegistrationBE = objIdsignBal.DeserializeFromXml<CustomerRegistrationBE>(objConsumerBal.CustomerRegistrationByApproval(objCustomerRegistrationBE));
                        if (objCustomerRegistrationBE.IsSuccessful)
                        {
                            divstep1.Visible = divFinishActions.Visible = divStatusError.Visible = false;
                            divSuccessMessage.Visible = divStatusSuccess.Visible = true;
                            lblStatusSuccess.Text = "Customer registration successfully sent for approval. Once it is approved customer will be activate.";
                            Message("Registration Queued for Approval!", UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        else
                        {
                            divstep1.Visible = divFinishActions.Visible = false;
                            divSuccessMessage.Visible = divStatusSuccess.Visible = false;
                            divSuccessMessage.Visible = divStatusError.Visible = true;
                            lblStatusError.Text = "Sorry!!!<br /> Registration Was Not Successful.<br />Problem in : " + objCustomerRegistrationBE.StatusText;
                            Message(objCustomerRegistrationBE.StatusText, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                    }
                    else
                    {
                        objCustomerRegistrationBE.ApprovalStatusId = (int)EnumApprovalStatus.Approved;
                        objCustomerRegistrationBE.IsFinalApproval = true;
                        //objCustomerRegistrationBE = objIdsignBal.DeserializeFromXml<CustomerRegistrationBE>(objConsumerBal.CustomerRegistrationBAL(objCustomerRegistrationBE));
                        objCustomerRegistrationBE = objIdsignBal.DeserializeFromXml<CustomerRegistrationBE>(objConsumerBal.CustomerRegistrationByApproval(objCustomerRegistrationBE));
                        if (objCustomerRegistrationBE.IsSuccessful)
                        {
                            divstep1.Visible = divFinishActions.Visible = divStatusError.Visible = false;
                            divSuccessMessage.Visible = divStatusSuccess.Visible = true;
                            lblStatusSuccess.Text = "Congratulations!!!<br /> Registration Was Successful<br />New Global Account No. Is: " + objCustomerRegistrationBE.GolbalAccountNumber;
                            Message("Registration Succeeded!", UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        else
                        {
                            divstep1.Visible = divFinishActions.Visible = false;
                            divSuccessMessage.Visible = divStatusSuccess.Visible = false;
                            divSuccessMessage.Visible = divStatusError.Visible = true;
                            lblStatusError.Text = "Sorry!!!<br /> Registration Was Not Successful.<br />Problem in : " + objCustomerRegistrationBE.StatusText;
                            Message(objCustomerRegistrationBE.StatusText, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Message("Exception in registration.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
            #endregion
        }

        private void BindDropDowns()
        {
            try
            {
                objCommonMethods.BindIdentityTypes(ddlIdentityType, true);
                hfTotalIdentity.Value = (ddlIdentityType.Items.Count - 1).ToString();
                objCommonMethods.BindPhases(ddlPhase, true);
                objCommonMethods.BindTariffSubTypes(ddlTariff, 0, true);
                objCommonMethods.BindAccountTypes(ddlAccountType, true);
                objCommonMethods.BindReadCodes(ddlReadCode, true);
                objCommonMethods.BindCustomerTypes(ddlCustomerType, true);
                objCommonMethods.BindAgencies(ddlAgency, true);
                objCommonMethods.BindEmployeeList(ddlCertifiedBy, true);
                objCommonMethods.BindEmployeeList(ddlInstalledBy, true);
                objCommonMethods.BindEmployeeList(ddlIsBedcEmp, true);
                objCommonMethods.BindClusterCategories(ddlClusterType, true);

                string BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                objCommonMethods.BindRoutes(ddlRouteNo, BU_ID, true);
                //objMastersBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                //if (objMastersBE.BU_ID != null) objCommonMethods.BindMeterList(objMastersBE, ddlMeterList, true);
            }
            catch (Exception ex)
            {
                Message("Exception in binding masters.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        private void BindBusinessUnits()
        {
            try
            {
                objCommonMethods.BindUserBusinessUnits(ddlBusinessUnitName, string.Empty, false, Resource.DDL_ALL);
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                {
                    string BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                    ddlBusinessUnitName.SelectedIndex = ddlBusinessUnitName.Items.IndexOf(ddlBusinessUnitName.Items.FindByValue(BUID));
                    ddlBusinessUnitName.Enabled = false;
                    ddlBusinessUnitName_SelectedIndexChanged(ddlBusinessUnitName, new EventArgs());
                    //ddlServiceUnitName_SelectedIndexChanged(ddlServiceUnitName, new EventArgs());
                    //objCommonMethods.BindCyclesByBUSUSC(ddlCycle, BUID, string.Empty, string.Empty, true, Resource.DDL_ALL, false);
                    //objCommonMethods.BindAllBookNumbers(ddlBookNo, Cycle, true, false, Resource.DDL_ALL);
                    mpeBookSearch.Hide();
                }
            }
            catch (Exception ex)
            {
                Message("Exception in registration input.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        protected void btnSerachBook_Click(object sender, EventArgs e)
        {
            BindBookGrid();
            grdBookNumber.Visible = true;
        }

        protected void btnSearchMeter_Click(object sender, EventArgs e)
        {
            BindAvailableMeterList();
            mpeMeterList.Show();
        }

        #region ddlEvents
        protected void ddlBusinessUnitName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objCommonMethods.BindUserServiceUnits_BuIds(ddlServiceUnitName, BusinessUnitName, Resource.DDL_ALL, false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                    mpeBookSearch.Show();
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlServiceUnitName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objCommonMethods.BindUserServiceCenters_SuIds(ddlServiceCenterName, ServiceUnitName, Resource.DDL_ALL, false);
                InsertALLDdl(ddlCycle);
                InsertALLDdl(ddlBookNo);
                grdBookNumber.Visible = false;
                mpeBookSearch.Show();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void InsertALLDdl(DropDownList ddl)
        {
            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("All", ""));
        }

        protected void ddlServiceCenterName_SelectedIndexChanged(object sender, EventArgs e)
        {
            objCommonMethods.BindCyclesByBUSUSC(ddlCycle, BusinessUnitName, ServiceUnitName, ServiceCenterName, true, Resource.DDL_ALL, false);
            InsertALLDdl(ddlBookNo);
            grdBookNumber.Visible = false;
            mpeBookSearch.Show();
        }
        protected void ddlCycle_SelectedIndexChanged(object sender, EventArgs e)
        {
            objCommonMethods.BindAllBookNumbers(ddlBookNo, Cycle, true, false, Resource.DDL_ALL);
            mpeBookSearch.Show();
        }
        #endregion

        protected void ddlBookNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            mpeBookSearch.Show();
        }

        protected void grdBookNumber_rowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                Label lblBookNo = (Label)row.FindControl("lblBookNo");
                if (e.CommandName == "select")
                {
                    txtBook.Text = e.CommandArgument.ToString();
                    hfBookNo.Value = lblBookNo.Text;
                }
            }
            catch (Exception ex)
            {
                Message("Exception in book number select.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        public void BindPoleRptr()
        {
            try
            {
                objPoleBAL = new PoleBAL();
                dsPoleDetails = objPoleBAL.PoleNumberSearch();
                if (dsPoleDetails.Tables.Count > 0)
                {
                    if (dsPoleDetails.Tables[0].Rows.Count > 0)
                    {
                        rptPoleSearch.DataSource = dsPoleDetails.Tables[0];
                        rptPoleSearch.DataBind();
                    }
                    else
                    {
                        rptPoleSearch.DataSource = dsPoleDetails;
                        rptPoleSearch.DataBind();
                    }
                    mpePole.Show();
                }
            }
            catch (Exception ex)
            {
                Message("Exception in pole infromation binding.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        public void BindBookGrid()
        {
            try
            {
                DataSet dsBookSearch = new DataSet();
                objConsumerBe = new ConsumerBe();
                objConsumerBe.BU_ID = ddlBusinessUnitName.SelectedItem.Value;
                objConsumerBe.SU_ID = ddlServiceUnitName.SelectedItem.Value;
                objConsumerBe.ServiceCenterId = ddlServiceCenterName.SelectedItem.Value;
                objConsumerBe.CycleId = ddlCycle.SelectedItem.Value;
                objConsumerBe.BookNo = ddlBookNo.SelectedItem.Value;
                dsBookSearch = objConsumerBal.BookNoSearchBAL(objConsumerBe);
                if (dsBookSearch.Tables.Count > 0)
                {
                    if (dsBookSearch.Tables[0].Rows.Count > 0)
                    {
                        grdBookNumber.DataSource = dsBookSearch.Tables[0];
                        grdBookNumber.DataBind();
                    }
                    else
                    {
                        grdBookNumber.DataSource = dsBookSearch;
                        grdBookNumber.DataBind();
                    }
                    mpeBookSearch.Show();
                }
            }
            catch (Exception ex)
            {
                Message("Exception in registration input.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        #region Customer Management With new table structure

        public XmlDocument CustomerRegistrationDAL(XmlDocument XmlDoc)//Neeraj-ID077
        {
            XmlDocument xml = null;
            try
            {
                //xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CustomerRegistration, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }
        #endregion

        public void SaveDocument()
        {
            // string fileInput = string.Empty;
            try
            {

                if (Request.Files.Count > 0)
                {
                    for (int z = 0; z < Request.Files.Count; z++)//looping to save attachments
                    {
                        HttpPostedFile PostedFile = Request.Files[z];//Assigning to variable to Access File
                        if (PostedFile.ContentLength != 0)//Checking the size of file is more than 0 bytes
                        {
                            string FileName = Path.GetFileName(PostedFile.FileName).ToString();//Assinging FileName
                            string Extension = Path.GetExtension(FileName);//Storing Extension of file into variable Extension
                            TimeZoneInfo IND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                            DateTime currentTime = DateTime.Now;
                            DateTime ourtime = TimeZoneInfo.ConvertTime(currentTime, IND_ZONE);
                            string CurrentDate = ourtime.ToString("dd_MM_yyyy_hh_mm_ss");
                            string ModifiedFileName = Path.GetFileNameWithoutExtension(FileName) + "_" + CurrentDate + Extension;//Setting File Name to store it in database
                            DocumentPathList += ConfigurationManager.AppSettings[UMS_Resource.KEY_CUSTOMER_DOCUMENTS].ToString() + "|";
                            //PostedFile.SaveAs(Server.MapPath(ConfigurationManager.AppSettings[UMS_Resource.KEY_CUSTOMER_DOCUMENTS].ToString()) + ModifiedFileName);//Saving File in to the server.
                            //DocumentNameList += string.IsNullOrEmpty(DocumentNameList) ? ModifiedFileName : DocumentNameList + "|" + ModifiedFileName;
                            DocumentNameList += ModifiedFileName + "|";

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Message("Exception in registration input.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        private void BindMandatory()
        {
            DataSet dsControls = new DataSet();
            dsControls.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["NewCustomer_Mandatory_Fields"]));
            StringBuilder jscript = new StringBuilder();

            UpdatePanel[] updtPanles = new UpdatePanel[12];
            updtPanles[0] = updtPnlLandlord;
            updtPanles[1] = updtPnlTenent;
            updtPanles[2] = updtPnlPostalAddress;
            updtPanles[3] = updtPnlServiceAddress;
            updtPanles[4] = updtPnlOtherDetails;
            updtPanles[5] = updtPnlAccountInfo;
            updtPanles[6] = updtPnlMeterDetails;
            updtPanles[7] = updtPnlSetupInformation;
            updtPanles[8] = updPnlIdentity;
            updtPanles[9] = updtPnlSearchMeterNo;
            updtPanles[10] = updtPnlCustType;
            updtPanles[11] = updtPnlMeterNo;

            ControlCollection Pagecontrols = null;

            jscript.Append("<script type=" + "'text/javascript'" + ">");
            jscript.Append("function FinalValidate(){");
            jscript.Append("var IsValid = true;");
            jscript.Append("var IsZipValid = true;");
            jscript.Append("var IsContactValid = true;");
            foreach (UpdatePanel upd in updtPanles)
            {
                Pagecontrols = upd.ContentTemplateContainer.Controls;

                for (int i = 0; i < Pagecontrols.Count; i++)
                {
                    foreach (DataRow dr in dsControls.Tables[0].Rows)
                    {
                        if (dr["ControlId"].ToString() == "txtIdentityControl")
                        {
                            if (Convert.ToBoolean(dr["IsMandatory"]))
                            {
                                IsIdentityMandatory = true;
                            }
                        }
                        if (Pagecontrols[i].ID == dr["ControlId"].ToString())
                        {
                            if (Convert.ToBoolean(dr["IsMandatory"]))
                            {
                                Literal LiteralId = (Literal)divstep1.FindControl(dr["LiteralId"].ToString());
                                LiteralId.Text += "<span class=" + "span_star" + ">*</span>";
                                #region If TextBox
                                if (Pagecontrols[i].GetType().Name.ToString() == "TextBox")
                                {
                                    TextBox txt = (TextBox)Pagecontrols[i];
                                    if (txt.ID == "txtApplicationDate" || txt.ID == "txtConnectionDate" || txt.ID == "txtSetupDate")
                                    {
                                        txt.Attributes.Add("onchange", "DoBlur(this,'" + dr["SpanId"] + "','" + dr["SpanText"] + "');");
                                        txt.Attributes.Add("onblur", "DoBlur(this,'" + dr["SpanId"] + "','" + dr["SpanText"] + "');");
                                        jscript.Append("if(TextBoxValidationlbl(document.getElementById('UMSNigeriaBody_" + dr["ControlId"] + "'), document.getElementById('" + dr["SpanId"] + "'),'" + dr["SpanText"] + "') == false) IsValid = false;");
                                    }
                                    else if (txt.ID == "txtOEmailId")
                                    {
                                        txt.Attributes.Add("onblur", "emailValidation(this,'" + dr["SpanId"] + "','" + dr["SpanText"] + "')");
                                        jscript.Append("if(emailValidation(document.getElementById('UMSNigeriaBody_" + dr["ControlId"] + "'),'" + dr["SpanId"] + "','" + dr["SpanText"] + "') == false) IsValid = false;");
                                    }
                                    else if (txt.ID == "txtTEmailId")
                                    {
                                        if (cbIsTenent.Checked)
                                        {
                                            txt.Attributes.Add("onblur", "emailValidation(this,'" + dr["SpanId"] + "','" + dr["SpanText"] + "')");
                                            jscript.Append("if(emailValidation(document.getElementById('UMSNigeriaBody_" + dr["ControlId"] + "'),'" + dr["SpanId"] + "','" + dr["SpanText"] + "') == false) IsValid = false;");
                                        }
                                    }
                                    else if (txt.ID == "txtPHNo" || txt.ID == "txtSHNo")
                                    {
                                        jscript.Append("if(TextBoxValidationlblZeroAccepts(document.getElementById('UMSNigeriaBody_" + dr["ControlId"] + "'), document.getElementById('" + dr["SpanId"] + "'),'" + dr["SpanText"] + "') == false) IsValid = false;");
                                    }
                                    else
                                    {
                                        if (txt.ID == "txtPresentReading")
                                        {
                                            txt.Attributes.Add("onblur", "return TextBoxValidationlblZeroAccepts(this,document.getElementById('" + dr["SpanId"] + "'),'" + dr["SpanText"] + "')");
                                        }
                                        else
                                        {
                                            txt.Attributes.Add("onblur", "return TextBoxBlurValidationlbl(this,'" + dr["SpanId"] + "','" + dr["SpanText"] + "')");
                                        }
                                        if (txt.ID == "txtTHPhoneCode" || txt.ID == "txtTAPhoneCode" || txt.ID == "txtTFName" || txt.ID == "txtTMName" || txt.ID == "txtTLName" || txt.ID == "txtTEmailId" || txt.ID == "txtTHPhone" || txt.ID == "txtTAPhone")
                                            jscript.Append("if (document.getElementById('UMSNigeriaBody_cbIsTenent').checked)");
                                        else if (txt.ID == "txtEmbassy")
                                            jscript.Append("if (document.getElementById('UMSNigeriaBody_cbIsEmbassy').checked)");
                                        else if (txt.ID == "txtMeterNo" || txt.ID == "txtPresentReading")
                                            jscript.Append("var e = document.getElementById('UMSNigeriaBody_ddlReadCode'); var ReadText = e.options[e.selectedIndex].value;if (ReadText == '2')");
                                        else if (txt.ID == "txtAmount")
                                            jscript.Append("if (document.getElementById('UMSNigeriaBody_cbIsCAPMY').checked)");

                                        if (txt.ID == "txtPresentReading")
                                        {
                                            jscript.Append("if(TextBoxValidationlblZeroAccepts(document.getElementById('UMSNigeriaBody_" + dr["ControlId"] + "'), document.getElementById('" + dr["SpanId"] + "'),'" + dr["SpanText"] + "') == false) IsValid = false;");
                                        }
                                        else
                                        {
                                            jscript.Append("if(TextBoxValidationlbl(document.getElementById('UMSNigeriaBody_" + dr["ControlId"] + "'), document.getElementById('" + dr["SpanId"] + "'),'" + dr["SpanText"] + "') == false) IsValid = false;");
                                        }
                                    }
                                }
                                #endregion
                                #region If DropDownList
                                else if (Pagecontrols[i].GetType().Name.ToString() == "DropDownList")
                                {
                                    if (Pagecontrols[i].ID == "ddlTTitle") jscript.Append("if (document.getElementById('UMSNigeriaBody_cbIsTenent').checked)");
                                    else if (Pagecontrols[i].ID == "ddlIsBedcEmp") jscript.Append("if (document.getElementById('UMSNigeriaBody_cbIsBEDCEmployee').checked)");
                                    else if (Pagecontrols[i].ID == "ddlInstalledBy")
                                        jscript.Append("if (document.getElementById('UMSNigeriaBody_rblApplicationProcessedBy_0').checked)");
                                    else if (Pagecontrols[i].ID == "ddlAgency")
                                        jscript.Append("if (document.getElementById('UMSNigeriaBody_rblApplicationProcessedBy_1').checked)");
                                    DropDownList ddl = (DropDownList)Pagecontrols[i];

                                    if (Pagecontrols[i].ID == "ddlAccountType") ddl.Attributes.Add("onchange", "return funGovType();");
                                    if (Pagecontrols[i].ID == "ddlCustomerType")
                                    {
                                        ddl.Attributes.Add("onchange", "DropDownlistOnChangelbl(this,'" + dr["SpanId"] + "','" + dr["SpanText"] + "');");
                                        ddl.Attributes.Add("onchange", "return ifPrepaidMeter();");
                                    }
                                    else ddl.Attributes.Add("onchange", "DropDownlistOnChangelbl(this,'" + dr["SpanId"] + "','" + dr["SpanText"] + "')");
                                    jscript.Append("if (DropDownlistValidationlbl(document.getElementById('UMSNigeriaBody_" + dr["ControlId"] + "'), document.getElementById('" + dr["SpanId"] + "'), '" + dr["SpanText"] + "') == false) IsValid = false;");

                                    if (Pagecontrols[i].ID == "ddlReadCode")
                                    {
                                        ddl.Attributes.Add("onchange", "DropDownlistOnChangelbl(this,'" + dr["SpanId"] + "','" + dr["SpanText"] + "');");
                                        ddl.Attributes.Add("onchange", "IfReadedCustomer();");
                                    }
                                }
                                #endregion
                                else if (Pagecontrols[i].GetType().Name.ToString() == "RadioButtonList")
                                {
                                    RadioButtonList rbl = (RadioButtonList)Pagecontrols[i];
                                    rbl.Attributes.Add("onchange", "return RadioButtonListlbl(this,'" + dr["SpanId"] + "','" + dr["SpanText"] + "')");
                                    jscript.Append("if (RadioButtonListlbl(document.getElementById('UMSNigeriaBody_" + dr["ControlId"] + "'), '" + dr["SpanId"] + "', '" + dr["SpanText"] + "') == false) IsValid = false;");
                                }
                            }
                            break;
                        }
                    }
                }

            }
            jscript.Append("if (document.getElementById('UMSNigeriaBody_txtApplicationDate').value != '') {  if(ValidateDate(document.getElementById('UMSNigeriaBody_txtApplicationDate'), 'spanApplicationDate', 'spanApplicationDate')==false){ IsValid=false;} }");
            jscript.Append("if (document.getElementById('UMSNigeriaBody_txtConnectionDate').value != '') { if(ValidateDate(document.getElementById('UMSNigeriaBody_txtConnectionDate'), 'spanConnectionDate', 'spanConnectionDate')==false){ IsValid=false;} }");
            jscript.Append("if (document.getElementById('UMSNigeriaBody_txtSetupDate').value != '') { if(ValidateDate(document.getElementById('UMSNigeriaBody_txtSetupDate'), 'spanSetupDate', 'spanSetupDate')==false){ IsValid=false;} }");
            jscript.Append("if (document.getElementById('UMSNigeriaBody_txtOEmailId').value != '') { if (OnlyEmailValidation(document.getElementById('UMSNigeriaBody_txtOEmailId'),'spanOEmailId','Please Enter Valid Email Id')==false){ IsValid = false; } }");
            jscript.Append("if (document.getElementById('UMSNigeriaBody_txtTEmailId').value != '') { if (OnlyEmailValidation(document.getElementById('UMSNigeriaBody_txtTEmailId'),'spanTEmailId','Please Enter Valid Email Id')==false){ IsValid = false; } }");
            jscript.Append("if (document.getElementById('UMSNigeriaBody_txtOHPhone').value != '' || document.getElementById('UMSNigeriaBody_txtOHPhoneCode').value != '') { if (CustomerContactNoLengthMinMax(document.getElementById('UMSNigeriaBody_txtOHPhone'),6,10,document.getElementById('spanOHPhone'),document.getElementById('UMSNigeriaBody_txtOHPhoneCode'),3)==false){ IsValid = false; } }");
            jscript.Append("if (document.getElementById('UMSNigeriaBody_txtOBPhone').value != '' || document.getElementById('UMSNigeriaBody_txtOBPhoneCode').value != '') { if (CustomerContactNoLengthMinMax(document.getElementById('UMSNigeriaBody_txtOBPhone'),6,10,document.getElementById('spanOBPhone'),document.getElementById('UMSNigeriaBody_txtOBPhoneCode'),3)==false){ IsValid = false; } }");
            jscript.Append("if (document.getElementById('UMSNigeriaBody_txtOOPhone').value != '' || document.getElementById('UMSNigeriaBody_txtOOPhoneCode').value != '') { if (CustomerContactNoLengthMinMax(document.getElementById('UMSNigeriaBody_txtOOPhone'),6,10,document.getElementById('spanOOPhone'),document.getElementById('UMSNigeriaBody_txtOOPhoneCode'),3)==false){ IsValid = false; } }");
            jscript.Append("if (document.getElementById('UMSNigeriaBody_txtTHPhone').value != '' || document.getElementById('UMSNigeriaBody_txtTHPhoneCode').value != '') { if (CustomerContactNoLengthMinMax(document.getElementById('UMSNigeriaBody_txtTHPhone'),6,10,document.getElementById('spanTHPhone'),document.getElementById('UMSNigeriaBody_txtTHPhoneCode'),3)==false){ IsValid = false; } }");
            jscript.Append("if (document.getElementById('UMSNigeriaBody_txtTAPhone').value != '' || document.getElementById('UMSNigeriaBody_txtTAPhoneCode').value != '') { if (CustomerContactNoLengthMinMax(document.getElementById('UMSNigeriaBody_txtTAPhone'),6,10,document.getElementById('spanTAPhone'),document.getElementById('UMSNigeriaBody_txtTAPhoneCode'),3)==false){ IsValid = false; } }");
            jscript.Append("if (IsSameEmail() == false) { IsValid = false;}");
            jscript.Append("if (CustomerTextboxZipCodelbl(document.getElementById('UMSNigeriaBody_txtSZip'), document.getElementById('spanSZip'), 10,6) == false) { IsZipValid = false;}");
            jscript.Append("if (CustomerTextboxZipCodelbl(document.getElementById('UMSNigeriaBody_txtPZip'), document.getElementById('spanPZip'), 10,6) == false) { IsZipValid = false;}");
            jscript.Append("if (!IsValid){document.getElementById('UMSNigeriaBody_lblMessageValidation').innerHTML = 'Please enter all mandatory fields.'; ShowMessage()}");
            jscript.Append("if (IsValid && !IsZipValid){  IsValid = false; document.getElementById('UMSNigeriaBody_lblMessageValidation').innerHTML = 'Please enter 6 digit zip code.'; ShowMessage()}");
            jscript.Append("if (!IsValid){return false;}");
            //jscript.Append("else {if(GetIdentityFields()==true && GovtAccountSubType()==true){IsValid= true;} else IsValid= false; }");
            if (IsIdentityMandatory)
            {
                //jscript.Append("else {if(GetIdentityFields()==true) {IsValid= true;} else if GovtAccountSubType()==true) {IsValid= true;} else IsValid= false;}");
                jscript.Append("else {if(GetIdentityFields()==true && GovtAccountSubType()==true){IsValid= true;} else IsValid= false; }");
                ((Literal)divstep1.FindControl("litIdentityType")).Text += "<span class=" + "span_star" + ">*</span>";
                ((Literal)divstep1.FindControl("litIdentityNo")).Text += "<span class=" + "span_star" + ">*</span>";
                //jscript.Append("document.getElementById('spanIdentityType').style.display = 'block';");
                //jscript.Append("document.getElementById('spanIdentityNo').style.display = 'block';");
            }
            //jscript.Append("debugger;");
            //jscript.Append("if (TextBoxValidationlbl(txtOHPhoneCode, document.getElementById('spanOHPhone'), 'Code') == false) IsValid = false;");
            //jscript.Append("if (TextBoxValidationlbl(txtOHPhone, document.getElementById('spanOHPhone'), 'Contact No') == false) IsValid = false;");
            //jscript.Append("else if (ContactNoLengthMinMax(txtOHPhone, 6, 10, document.getElementById('spanOHPhone'), txtOHPhoneCode, 3) == false) { debugger; IsValid = false;}");
            //jscript.Append("if(validationContactNo('txtOBPhoneCode','txtOBPhone','spanOBPhone','Code','Contact No')==false) IsValid = false;");
            jscript.Append("return IsValid;}");
            jscript.Append("</script>");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "ConfirmSubmit", jscript.ToString(), false);
            btnFinish.Attributes.Add("onclick", "return FinalValidate()");


        }

        public void BindParentPole()
        {
            objPoleBE.IsParentPole = Constants.IsParentPole;
            objCommonMethods.BindPoleByOrderID(ddlPoleParent, objPoleBE, true);
        }

        public void BindAvailableMeterList()
        {
            try
            {
                objMastersBE.MeterNo = txtMeterNoSearch.Text;
                objMastersBE.MeterSerialNo = txtMeterSerialNo.Text;
                //objMastersBE.CustomerTypeId = string.IsNullOrEmpty(ddlCustomerType.SelectedValue) ? Constants.Zero : Convert.ToInt32(ddlCustomerType.SelectedValue);
                objMastersBE.CustomerTypeId = Convert.ToInt32(CustomerType);
                //Convert.ToInt32(ddlAccountType.SelectedValue);
                if (Session[UMS_Resource.SESSION_USER_BUID] != null) objMastersBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else objMastersBE.BU_ID = string.Empty;
                objMastersListBE = objIdsignBal.DeserializeFromXml<MastersListBE>(objMastersBAL.GetAvailableMeterList(objMastersBE));
                if (objMastersListBE.Items.Count > 0)
                {
                    gvMeterList.Visible = true;
                    gvMeterList.DataSource = objMastersListBE.Items;
                    gvMeterList.DataBind();
                }
                else
                {
                    gvMeterList.DataSource = new DataTable();
                    gvMeterList.DataBind();
                }
            }
            catch (Exception ex)
            {
                Message("Exception in meter list binding.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }

        public string IsPoleExists(string poleNumber)
        {
            string poleNum = string.Empty;
            try
            {

                objPoleBE = new PoleBE();
                objPoleBE.PoleInputCode = poleNumber;

                objPoleBE = objIdsignBal.DeserializeFromXml<PoleBE>(objPoleBAL.IsPoleNoExists(objPoleBE));
                if (objPoleBE != null && !objPoleBE.IsExists)
                {
                    poleNum = string.Empty;
                    //spnPoleStatus.InnerHtml = "Pole no. " + poleNumber + " does not available!";
                    //spnPoleStatus.Attributes.Add("class", "span_color");
                    spnPoleStatusPopup.InnerHtml = "Pole no. " + poleNumber + " does not available!";
                    spnPoleStatusPopup.Attributes.Add("class", "span_color");
                    //BindParentPole();
                    btnSelectPole.Visible = false;
                    mpePole.Show();
                }
                else
                {
                    //spnPoleStatus.InnerHtml = "Pole no. " + poleNumber + " is available!";
                    //spnPoleStatus.Attributes.Add("class", "span_color_green");
                    spnPoleStatusPopup.InnerHtml = "Pole no. " + poleNumber + " is available!";
                    spnPoleStatusPopup.Attributes.Add("class", "span_color_green");
                    poleNum = poleNumber;
                    hfPoleID.Value = objPoleBE.PoleId.ToString();
                    btnSelectPole.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Message("Exception in pole existence verification.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
            return poleNum;
        }

        public string getPoleCode(string PoleText)
        {
            string poleCode = string.Empty;
            try
            {
                int lastIndex = PoleText.LastIndexOf('-');
                poleCode = PoleText.Substring(lastIndex + 1);
            }
            catch (Exception ex)
            {
                Message("Exception in pole number.", UMS_Resource.MESSAGETYPE_ERROR);
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
            return poleCode;
        }

        public bool GetUDFValues()
        {
            hfUserControlFieldsID.Value = hfUserControlFieldsValue.Value = string.Empty;
            string[] strArray = null;
            string userInput = string.Empty;
            bool IsValid = false;
            if (!string.IsNullOrEmpty(hfControlsIDParent.Value))
            {
                strArray = hfControlsIDParent.Value.Split('|');
                UserDefinedControls udControl = (UserDefinedControls)UserDefinedControls1;
                HtmlGenericControl GrandParent = (HtmlGenericControl)udControl.FindControl("GrandParent");
                foreach (string x in strArray)
                {
                    if (x != "")
                    {
                        switch (x.Substring(0, 3))
                        {
                            case "txt":
                                userInput = ((TextBox)GrandParent.FindControl(x.Substring(0, x.Length))).Text;
                                if (userInput != "")
                                {
                                    hfUserControlFieldsValue.Value += userInput + "|";
                                    //hfUserControlFieldsID.Value += x.Substring(x.Length - 1, 1) + "|";
                                    hfUserControlFieldsID.Value += x.Split('_').Last() + "|";//Faiz-ID103
                                }
                                break;
                            case "ddl":

                                userInput = ((DropDownList)GrandParent.FindControl(x.Substring(0, x.Length))).SelectedItem.Value; if (userInput != "0")
                                {
                                    hfUserControlFieldsValue.Value += userInput + "|";
                                    //hfUserControlFieldsID.Value += x.Substring(x.Length - 1, 1) + "|";
                                    hfUserControlFieldsID.Value += x.Split('_').Last() + "|";//Faiz-ID103
                                }
                                break;
                            case "rdo":
                                userInput = ((RadioButtonList)GrandParent.FindControl(x.Substring(0, x.Length))).SelectedValue;
                                if (userInput != "")
                                {
                                    hfUserControlFieldsValue.Value += userInput + "|";
                                    //hfUserControlFieldsID.Value += x.Substring(x.Length - 1, 1) + "|";
                                    hfUserControlFieldsID.Value += x.Split('_').Last() + "|";//Faiz-ID103
                                }
                                break;
                        }
                    }
                }
                if (hfUserControlFieldsID.Value.Length > 0 && hfUserControlFieldsValue.Value.Length > 0)
                {
                    if (hfUserControlFieldsID.Value.Split('|').Count() == hfUserControlFieldsValue.Value.Split('|').Count())
                    {
                        IsValid = true;
                    }
                }
            }
            return IsValid;
        }

        private void PopulateRootLevel()
        {
            objGovtAccountTypeListBe =
                    objIdsignBal.DeserializeFromXml<GovtAccountTypeListBE>(
                        objGovtAccountTypeBAL.GetGovtAccountTypeList());
            var _objGovtAccountTypeListBe =
                objGovtAccountTypeListBe.Items.Where(x => x.RefActTypeID == 0).ToList();
            PopulateNodes(_objGovtAccountTypeListBe, tvGovtType.Nodes);

        }

        private void PopulateNodes(List<GovtAccountTypeBE> objGovtAccountTypeBe, TreeNodeCollection nodes)
        {

            foreach (var Item in objGovtAccountTypeBe)
            {
                TreeNode tn = new TreeNode();
                tn.Text = Item.AccountType;
                tn.Value = Item.GActTypeID.ToString();
                tn.SelectAction = TreeNodeSelectAction.SelectExpand;
                nodes.Add(tn);
                // If node has child nodes, then enable on-demand populating
                tn.PopulateOnDemand = objGovtAccountTypeListBe.Items.Exists(x => x.RefActTypeID == Item.GActTypeID);
            }
        }

        private void PopulateSubLevel(int parentid, TreeNode parentNode)
        {
            var _objgoGovtAccountTypeBe =
                objGovtAccountTypeListBe.Items.Where(x => x.RefActTypeID == parentid).ToList();
            PopulateNodes(_objgoGovtAccountTypeBe, parentNode.ChildNodes);
        }

        public string getFullNodepath()
        {
            string FullPath = string.Empty;
            int depth = tvGovtType.SelectedNode.Depth;
            List<string> s = new List<string>();
            TreeNode node = tvGovtType.SelectedNode;
            for (int i = depth; i > -1; i--)
            {
                s.Add(node.Text);
                //if (!string.IsNullOrEmpty(FullPath))
                //    FullPath +=  " --> " + node.Text;
                //else
                //    FullPath = node.Text;

                node = node.Parent;
            }
            for (int i = s.Count; i > 0; i--)
            {
                if (!string.IsNullOrEmpty(FullPath))
                    FullPath += " --> " + s[i - 1];
                else
                    FullPath = s[i - 1];
            }
            return FullPath;
        }

        public void IsOldAccountNoExists()
        {
            ConsumerBal objConsumerBal = new ConsumerBal();
            objCustomerRegistrationBE.OldAccountNo = txtOldAccountNo.Text;
            objCustomerRegistrationBE = objIdsignBal.DeserializeFromXml<CustomerRegistrationBE>(objConsumerBal.IsOldAccountNoExists(objCustomerRegistrationBE));
            if (objCustomerRegistrationBE.IsSuccessful)
            {

                spnOldACStatus.InnerText = string.Empty;
                spnOldACStatus.InnerText = txtOldAccountNo.Text + " Old account no. already exists.";
                spnOldACStatus.Attributes.Add("class", "span_color");
                txtOldAccountNo.Text = string.Empty;
            }
            else
            {
                spnOldACStatus.InnerText = "You can use this old account no.";
                spnOldACStatus.Attributes.Add("class", "span_color_green");
            }
        }

        public void IsEmailAlreadyExists(TextBox emailID, bool IsLandLoardEmail)
        {
            ConsumerBal objConsumerBal = new ConsumerBal();
            objCustomerRegistrationBE.EmailID = emailID.Text.Trim();
            objCustomerRegistrationBE = objIdsignBal.DeserializeFromXml<CustomerRegistrationBE>(objConsumerBal.IsEmailExists(objCustomerRegistrationBE));
            if (objCustomerRegistrationBE.IsSuccessful)
            {
                if (IsLandLoardEmail)
                {
                    spanOEMailExists.InnerText = string.Empty;
                    spanOEMailExists.InnerText = emailID.Text + " Email already Exists.";
                    spanOEMailExists.Attributes.Add("class", "span_color");
                    emailID.Text = string.Empty;
                }
                else
                {
                    spanTEMailExists.InnerText = string.Empty;
                    spanTEMailExists.InnerText = emailID.Text + " Email already Exists.";
                    spanTEMailExists.Attributes.Add("class", "span_color");
                    emailID.Text = string.Empty;
                }
            }
            else
            {
                if (IsLandLoardEmail)
                {
                    spanOEMailExists.InnerText = "You can use this EmailId.";
                    spanOEMailExists.Attributes.Add("Display", "Bloack");
                    spanOEMailExists.Attributes.Add("class", "span_color_green");
                }
                else
                {
                    spanTEMailExists.InnerText = "You can use this EmailId.";
                    spanTEMailExists.Attributes.Add("class", "span_color_green");
                }
            }
        }

        [System.Web.Services.WebMethod]
        public static bool IsEmailExists(string EmailId)
        {
            bool IsExists = false;
            if (!string.IsNullOrEmpty(EmailId))
            {
                iDsignBAL _objiDsignBal = new iDsignBAL();
                ConsumerBal objConsumerBal = new ConsumerBal();
                CustomerRegistrationBE objCustomerRegistrationBE = new CustomerRegistrationBE();
                objCustomerRegistrationBE.EmailID = EmailId.Trim();
                objCustomerRegistrationBE = _objiDsignBal.DeserializeFromXml<CustomerRegistrationBE>(objConsumerBal.IsEmailExists(objCustomerRegistrationBE));
                if (objCustomerRegistrationBE.IsSuccessful)
                {
                    IsExists = true;
                }
            }
            return IsExists;
        }

        public bool IsApproval()
        {
            bool IsApproval = false;
            IsApproval = objCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.CustomerRegistration, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());
            return IsApproval;
        }
        #endregion

    }
}