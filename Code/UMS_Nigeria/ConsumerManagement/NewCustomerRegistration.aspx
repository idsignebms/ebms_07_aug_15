﻿<%@ Page Title=":: Consumer Registration ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="NewCustomerRegistration.aspx.cs"
    Inherits="UMS_Nigeria.ConsumerManagement.NewCustomerRegistration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="wizard" class="swMain">
        <%----------------------------------------------------------Step 1----------------------------------------- --%>
        <div id="divstep1" runat="server">
            <div class="out-bor">
                <div class="inner-sec">
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddState" runat="server" Text="<%$ Resources:Resource, CUST_REGIST%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <asp:Literal ID="litCustomerType" runat="server" Text="<%$ Resources:Resource, CUSTOMER_TYPE%>"></asp:Literal>
                            <br />
                            <asp:DropDownList ID="ddlCustomerType" TabIndex="1" runat="server" CssClass="text-box text-select">
                                <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                            <div class="space">
                            </div>
                            <span id="spanCustomerType" class="span_color"></span>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <%-------------------------Land Lord Information Start----------------------%>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="grid_head">
                                <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, LAND_LORD_INFO%>"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litOTitle" runat="server" Text="<%$ Resources:Resource, TITLE%>"></asp:Literal>
                            </label>
                            <br />
                            <asp:DropDownList ID="ddlOTitle" runat="server" CssClass="text-box text-select">
                                <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                            <div class="space">
                            </div>
                            <span id="spanOTitle" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box2">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litOFName" runat="server" Text="<%$ Resources:Resource, F_NAME%>"></asp:Literal></label>
                            <br />
                            <asp:TextBox ID="txtOFName" runat="server" CssClass="text-box" placeholder="Enter First Name"></asp:TextBox>
                            <span id="spanOFName" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box3">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litOMName" runat="server" Text="<%$ Resources:Resource, M_NAME%>"></asp:Literal></label>
                            <br />
                            <asp:TextBox ID="txtOMName" runat="server" CssClass="text-box" placeholder="Enter Middle Name"></asp:TextBox>
                            <span id="spanOMName" class="span_color"></span>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litOLName" runat="server" Text="<%$ Resources:Resource, L_NAME%>"></asp:Literal></label>
                            <br />
                            <asp:TextBox ID="txtOLName" runat="server" CssClass="text-box" onblur="return TextBoxBlurValidationlbl(this,'spanLName','Last Name')"
                                placeholder="Enter Last Name"></asp:TextBox>
                            <span id="spanOLName" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box2">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litOKnownAs" runat="server" Text="<%$ Resources:Resource, KNOWN_AS%>"></asp:Literal></label>
                            <br />
                            <asp:TextBox ID="txtOKnownAs" runat="server" CssClass="text-box" placeholder="Enter Known As"></asp:TextBox>
                            <span id="spanOKnownAs" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box3">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litOEmailId" runat="server" Text="<%$ Resources:Resource, EMAIL_ID%>"></asp:Literal></label>
                            <br />
                            <asp:TextBox ID="txtOEmailId" runat="server" CssClass="text-box" placeholder="Enter EmailId"></asp:TextBox>
                            <span id="spanOEmailId" class="span_color"></span>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litOHPhone" runat="server" Text="<%$ Resources:Resource, HOME%>"></asp:Literal>
                            </label>
                            <br />
                            <asp:TextBox ID="txtOHPhone" runat="server" CssClass="text-box" placeholder="Enter Home Phone"></asp:TextBox>
                            <span id="spanOHPhone" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box2">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litOBPhone" runat="server" Text="<%$ Resources:Resource, BUSINESS%>"></asp:Literal></label>
                            <br />
                            <asp:TextBox ID="txtOBPhone" runat="server" CssClass="text-box" placeholder="Enter Business Phone"></asp:TextBox>
                            <span id="spanOBPhone" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box3">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litOOPhone" runat="server" Text="<%$ Resources:Resource, OTHERS%>"></asp:Literal></label>
                            <br />
                            <asp:TextBox ID="txtOOPhone" runat="server" CssClass="text-box" placeholder="Enter Others Phone"></asp:TextBox>
                            <span id="spanOOPhone" class="span_color"></span>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <%-------------------------Land Lord Information End----------------------%>
                    <%-------------------------Tenent Information Start----------------------%>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <asp:CheckBox ID="cbIsTenent" runat="server" Text="Is Tenent" />
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="grid_head">
                                <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, TENENT_INFO%>"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litTTitle" runat="server" Text="<%$ Resources:Resource, TITLE%>"></asp:Literal></label><br />
                            <asp:DropDownList ID="ddlTTitle" runat="server" CssClass="text-box text-select">
                                <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                            <div class="space">
                            </div>
                            <span class="span_color" id="spanTTitle"></span>
                        </div>
                    </div>
                    <div class="inner-box2">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litTFName" runat="server" Text="<%$ Resources:Resource, F_NAME%>"></asp:Literal></label><br />
                            <asp:TextBox ID="txtTFName" runat="server" placeholder="Enter First Name"></asp:TextBox>
                            <span id="spanTFName" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box3">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litTMName" runat="server" Text="<%$ Resources:Resource, M_NAME%>"></asp:Literal></label><br />
                            <asp:TextBox ID="txtTMName" runat="server" placeholder="Enter Middle Name"></asp:TextBox>
                            <span id="spanTMName" class="span_color"></span>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litTLName" runat="server" Text="<%$ Resources:Resource, L_NAME%>"></asp:Literal></label><br />
                            <asp:TextBox ID="txtTLName" runat="server" placeholder="Enter Last Name"></asp:TextBox>
                            <span id="spanTLName" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box2">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litTEmailId" runat="server" Text="<%$ Resources:Resource, EMAIL_ID%>"></asp:Literal></label><br />
                            <asp:TextBox ID="txtTEmailId" runat="server" placeholder="Enter EmailId"></asp:TextBox>
                            <span id="spanTEmailId" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box3">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litTHPhone" runat="server" Text="<%$ Resources:Resource, HOME%>"></asp:Literal></label><br />
                            <asp:TextBox ID="txtTHPhone" runat="server" placeholder="Enter Home Phone"></asp:TextBox>
                            <span id="spanTHPhone" class="span_color"></span>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litTAPhone" runat="server" Text="<%$ Resources:Resource, ANOTHER_CONTACT_NO%>"></asp:Literal></label><br />
                            <asp:TextBox ID="txtTAPhone" runat="server" placeholder="Enter Alternate Contact No"></asp:TextBox>
                            <span id="spanTAPhone" class="span_color"></span>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <%-------------------------Tenent Information End----------------------%>
                    <%-------------------------Postal Information Start----------------------%>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="grid_head">
                                <asp:Literal ID="litPostalAddress" runat="server" Text="<%$ Resources:Resource, POSTAL_ADDRESS%>"></asp:Literal></div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litPHNo" runat="server" Text="<%$ Resources:Resource, H_NO%>"></asp:Literal></label><br />
                            <asp:TextBox ID="txtPHNo" runat="server" placeholder="Enter House No"></asp:TextBox>
                            <span id="spanPHNo" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box2">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litPStreet" runat="server" Text="<%$ Resources:Resource, STREET%>"></asp:Literal>Street
                                Name</label><br />
                            <asp:TextBox ID="txtPStreet" runat="server" placeholder="Enter Street Name"></asp:TextBox>
                            <span id="spanPStreet" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box3">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litPLGAVillage" runat="server" Text="<%$ Resources:Resource, LGA_VILLAGE%>"></asp:Literal>LGA/Village</label><br />
                            <asp:TextBox ID="txtPLGAVillage" runat="server" placeholder="Enter LGA/Village"></asp:TextBox>
                            <span id="spanPLGAVillage" class="span_color"></span>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litPZip" runat="server" Text="<%$ Resources:Resource, ZIP%>"></asp:Literal>Zip
                                Code</label><br />
                            <asp:TextBox ID="txtPZip" runat="server" placeholder="Enter Zip Code"></asp:TextBox>
                            <span id="spanPZip" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box2">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litPArea" runat="server" Text="<%$ Resources:Resource, AREA_CODE%>"></asp:Literal></label><br />
                            <asp:TextBox ID="txtPArea" runat="server" placeholder="Enter Area Code"></asp:TextBox>
                            <span id="spanPArea" class="span_color"></span>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <%-------------------------Postal Information End----------------------%>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <asp:CheckBox ID="cbIsSameAsPostal" runat="server" Text="Is same as Postal Address" />
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <%-------------------------Service Information Start----------------------%>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="grid_head">
                                <asp:Literal ID="litServiceAddress" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Literal></div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litSHNo" runat="server" Text="<%$ Resources:Resource, H_NO%>"></asp:Literal></label><br />
                            <asp:TextBox ID="txtSHNo" runat="server" placeholder="Enter House No"></asp:TextBox>
                            <span id="spanSHNo" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box2">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litSStreet" runat="server" Text="<%$ Resources:Resource, STREET%>"></asp:Literal></label><br />
                            <asp:TextBox ID="txtSStreet" runat="server" placeholder="Enter Street Name"></asp:TextBox>
                            <span id="spanSStreet" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box3">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litSLGAVillage" runat="server" Text="<%$ Resources:Resource, LGA_VILLAGE%>"></asp:Literal></label><br />
                            <asp:TextBox ID="txtSLGAVillage" runat="server" placeholder="Enter LGA/Village"></asp:TextBox>
                            <span id="spanSLGAVillage" class="span_color"></span>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litSZip" runat="server" Text="<%$ Resources:Resource, ZIP%>"></asp:Literal></label><br />
                            <asp:TextBox ID="txtSZip" runat="server" placeholder="Enter Zip Code"></asp:TextBox>
                            <span id="spanSZip" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box2">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litSArea" runat="server" Text="<%$ Resources:Resource, AREA_CODE%>"></asp:Literal></label><br />
                            <asp:TextBox ID="txtSArea" runat="server" placeholder="Enter Area Code"></asp:TextBox>
                            <span id="spanSArea" class="span_color"></span>
                        </div>
                    </div>
                    <%-------------------------Service Information End----------------------%>
                    <div class="clr">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litrblCommunicationAddress" runat="server" Text="<%$ Resources:Resource, COMMUNICATION_ADD%>"></asp:Literal></label><br />
                            <asp:RadioButtonList ID="rblCommunicationAddress" RepeatDirection="Horizontal" runat="server">
                                <asp:ListItem Text="Postal" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Service" Value="2"></asp:ListItem>
                            </asp:RadioButtonList>
                            <div class="space">
                            </div>
                            <span id="spanrblCommunicationAddress" class="span_color"></span>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <asp:CheckBox ID="cbIsBEDCEmployee" runat="server" Text="Is BEDC Employee" />
                        </div>
                    </div>
                    <div class="inner-box2">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litEmployeCode" runat="server" Text="<%$ Resources:Resource, EMPLOYEE_CODE%>"></asp:Literal></label><br />
                            <asp:TextBox ID="txtEmployeCode" runat="server" placeholder="Enter Employee Code"></asp:TextBox>
                            <span id="spanEmployeCode" class="span_color"></span>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <asp:CheckBox ID="cbIsEmbassy" runat="server" Text="Tax Free(Is Embassy)" />
                        </div>
                    </div>
                    <div class="inner-box2">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litEmbassy" runat="server" Text="<%$ Resources:Resource, EMBASSY_CODE%>"></asp:Literal></label><br />
                            <asp:TextBox ID="txtEmbassy" runat="server" placeholder="Enter Embassy Code"></asp:TextBox>
                            <span id="spanEmbassy" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box3">
                        <div class="text-inner">
                            <asp:CheckBox ID="cbIsVIP" runat="server" Text="Is VIP" />
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                </div>
            </div>
            <%----------------------------------------------------------Step 2----------------------------------------- --%>
            <div class="out-bor">
                <div class="clr">
                </div>
                <br />
                <div class="inner-box1">
                    <div class="text-inner">
                        <asp:Literal ID="litAccountType" runat="server" Text="<%$ Resources:Resource, ACCOUNT_TYPE%>"></asp:Literal>
                        <br />
                        <asp:DropDownList ID="ddlAccountType" runat="server" CssClass="text-box text-select">
                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        <div class="space">
                        </div>
                        <span id="spanAccountType" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box2">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litBook" runat="server" Text="<%$ Resources:Resource, BOOK_CODE%>"></asp:Literal></label><br />
                        <asp:TextBox ID="txtBook" runat="server" placeholder="Enter Book Code"></asp:TextBox>
                        <div class="box_total_b">
                            <div class="search_div">
                                <a href="#"><span class="search_img"></span>
                                    <asp:Literal ID="litSearch" runat="server" Text="Search"></asp:Literal></a></div>
                        </div>
                        <div class="space">
                        </div>
                        <span id="spanBook" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box3">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litPole" runat="server" Text="<%$ Resources:Resource, POLE%>"></asp:Literal></label><br />
                        <asp:TextBox ID="txtPole" runat="server" placeholder="Enter Pole Number"></asp:TextBox>
                        <div class="box_total_b">
                            <div class="search_div">
                                <a href="#"><span class="search_img"></span>
                                    <asp:Literal ID="Literal1" runat="server" Text="Search"></asp:Literal></a></div>
                        </div>
                        <div class="space">
                        </div>
                        <span id="spanPole" class="span_color"></span>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litTariff" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal></label><br />
                        <asp:DropDownList ID="ddlTariff" runat="server" CssClass="text-box text-select">
                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        <div class="space">
                        </div>
                        <span id="spanTariff" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box2">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litClusterType" runat="server" Text="<%$ Resources:Resource, CLUSTER_TYPE%>"></asp:Literal></label><br />
                        <asp:DropDownList ID="ddlClusterType" runat="server" CssClass="text-box text-select">
                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        <div class="space">
                        </div>
                        <span id="spanClusterType" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box3">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litReadCode" runat="server" Text="<%$ Resources:Resource, READCODE%>"></asp:Literal></label><br />
                        <asp:DropDownList ID="ddlReadCode" runat="server" CssClass="text-box text-select">
                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        <div class="space">
                        </div>
                        <span id="spanReadCode" class="span_color"></span>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litMeterNo" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Literal></label><br />
                        <asp:TextBox ID="txtMeterNo" runat="server" placeholder="Enter Meter No"></asp:TextBox>
                        <div class="box_total_b">
                            <div class="search_div">
                                <a href="#"><span class="search_img"></span>
                                    <asp:Literal ID="Literal2" runat="server" Text="Search"></asp:Literal></a></div>
                        </div>
                        <div class="space">
                        </div>
                        <span id="spanMeterNo" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box2">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litPhase" runat="server" Text="<%$ Resources:Resource, PHASE%>"></asp:Literal></label><br />
                        <asp:DropDownList ID="ddlPhase" runat="server" CssClass="text-box text-select">
                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="space">
                    </div>
                    <span id="spanPhase" class="span_color"></span>
                </div>
                <div class="inner-box3">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litInitialBilling" runat="server" Text="<%$ Resources:Resource, INITIAL_BILLING_KWH%>"></asp:Literal></label><br />
                        <asp:TextBox ID="txtInitialBilling" runat="server" placeholder="Enter Initial Billing kWh"></asp:TextBox>
                        <div class="space">
                        </div>
                        <span id="spanInitialBilling" class="span_color"></span>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litApplicationDate" runat="server" Text="<%$ Resources:Resource, APPLICATION_DATE%>"></asp:Literal></label><br />
                        <asp:TextBox ID="txtApplicationDate" runat="server" placeholder="Enter Application Date"></asp:TextBox>
                        <div class="space">
                        </div>
                        <span id="spanApplicationDate" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box2">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litConnectionDate" runat="server" Text="<%$ Resources:Resource, CONNECTION_DATE%>"></asp:Literal></label><br />
                        <asp:TextBox ID="txtConnectionDate" runat="server" placeholder="Enter Connection Date"></asp:TextBox>
                        <span id="spanConnectionDate" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box3">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litSetupDate" runat="server" Text="<%$ Resources:Resource, SETUP_DATE%>"></asp:Literal></label><br />
                        <asp:TextBox ID="txtSetupDate" runat="server" placeholder="Enter Setup Date"></asp:TextBox>
                        <div class="space">
                        </div>
                        <span id="spanSetupDate" class="span_color"></span>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litCertifiedBy" runat="server" Text="<%$ Resources:Resource, CERTIFIED_BY%>"></asp:Literal></label><br />
                        <asp:DropDownList ID="ddlCertifiedBy" runat="server" CssClass="text-box text-select">
                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        <div class="space">
                        </div>
                        <span id="spanCertifiedBy" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box2">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litSeal1" runat="server" Text="<%$ Resources:Resource, SEAL_1%>"></asp:Literal></label><br />
                        <asp:TextBox ID="txtSeal1" runat="server" placeholder="Enter Seal 1"></asp:TextBox>
                        <div class="space">
                        </div>
                        <span id="spanSeal1" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box3">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litSeal2" runat="server" Text="<%$ Resources:Resource, SEAL_2%>"></asp:Literal></label><br />
                        <asp:TextBox ID="txtSeal2" runat="server" placeholder="Enter Seal 2"></asp:TextBox>
                        <div class="space">
                        </div>
                        <span id="spanSeal2" class="span_color"></span>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner">
                        <asp:CheckBox ID="cbIsCAPMY" runat="server" Text="Is CAPMY" />
                    </div>
                </div>
                <div class="inner-box2">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litAmount" runat="server" Text="<%$ Resources:Resource, AMOUNT%>"></asp:Literal></label><br />
                        <asp:TextBox ID="txtAmount" runat="server" placeholder="Enter CAPMY Amount"></asp:TextBox>
                        <div class="space">
                        </div>
                        <span id="spanAmount" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box3">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litMinimumReading" runat="server" Text="<%$ Resources:Resource, MIN_READING%>"></asp:Literal></label><br />
                        <asp:TextBox ID="txtMinimumReading" runat="server" placeholder="Enter Minimum Reading"></asp:TextBox>
                        <div class="space">
                        </div>
                        <span id="spanMinimumReading" class="span_color"></span>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litOldAccountNo" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal></label><br />
                        <asp:TextBox ID="txtOldAccountNo" runat="server" placeholder="Enter Old AccountNo"></asp:TextBox>
                        <div class="space">
                        </div>
                        <span id="spanOldAccountNo" class="span_color"></span>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litApplicationProcessedBy" runat="server" Text="<%$ Resources:Resource, APPLICATION_PROCCESED_BY%>"></asp:Literal></label><br />
                        <asp:RadioButtonList ID="rblApplicationProcessedBy" RepeatDirection="Horizontal"
                            runat="server">
                            <asp:ListItem Text="BEDC" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Others" Value="2"></asp:ListItem>
                        </asp:RadioButtonList>
                        <div class="space">
                        </div>
                        <span id="spanApplicationProcessedBy" class="span_color"></span>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litInstalledBy" runat="server" Text="<%$ Resources:Resource, INSTALLED_BY%>"></asp:Literal></label><br />
                        <asp:DropDownList ID="ddlInstalledBy" runat="server" CssClass="text-box text-select">
                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        <div class="space">
                        </div>
                        <span id="spanInstalledBy" class="span_color"></span>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litAgency" runat="server" Text="<%$ Resources:Resource, AGENCY_NAME%>"></asp:Literal></label><br />
                        <asp:DropDownList ID="ddlAgency" runat="server" CssClass="text-box text-select">
                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        <div class="space">
                        </div>
                        <span id="spanAgency" class="span_color"></span>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litIdentityType" runat="server" Text="<%$ Resources:Resource, IDENTITY_TYPE%>"></asp:Literal></label><br />
                        <asp:DropDownList ID="ddlIdentityType" runat="server" CssClass="text-box text-select">
                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        <div class="space">
                        </div>
                        <span id="spanIdentityType" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box2">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litIdentityNo" runat="server" Text="<%$ Resources:Resource, IDENTITY_NUMBER%>"></asp:Literal></label><br />
                        <asp:TextBox ID="txtIdentityNo" runat="server" placeholder="Enter Identity No"></asp:TextBox>
                        <div class="space">
                        </div>
                        <span id="spanIdentityNo" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box3">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="Literal24" runat="server" Text="<%$ Resources:Resource, BROWSE%>"></asp:Literal></label><br />
                        <asp:FileUpload ID="fupDocument" runat="server" CssClass="fltl" />
                        <div class="box_total_b">
                            <div class="search_div">
                                <asp:LinkButton ID="lnkAddFiles" runat="server">Add</asp:LinkButton></div>
                        </div>
                    </div>
                </div>
                <div class="clr">
                    <asp:Button ID="Button1" runat="server" Text="Finish" />
                </div>
            </div>
            <div class="out-bor" runat="server" id="divDynamic" visible="false">                
                    <asp:PlaceHolder ID="phDynamic" runat="server"></asp:PlaceHolder>
            </div>
        </div>
        <asp:HiddenField ID="hfValidStep" runat="server" Value="0" />
        <%--<div id="dynamicDiv" runat="server"></div>--%>
    </div>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);        
    </script>
    <script type="text/C#" runat="server">
        System.Data.DataSet dscontrols = new System.Data.DataSet();
        UMS_NigeriaBAL.ApprovalBal _objApprovalBal = new UMS_NigeriaBAL.ApprovalBal();
        
    </script>
</asp:Content>
