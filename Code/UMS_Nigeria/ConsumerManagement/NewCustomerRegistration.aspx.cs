﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for Consumer Registration
                     
 Developer        : id027-Karthik Thati
 Creation Date    : 17-02-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using System.Threading;
using System.Globalization;
using UMS_NigeriaBE;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using System.Xml;
using System.IO;
using System.Configuration;
using System.Web.Services;
using System.Data;
using System.Text;
using UMS_Nigeria.UserControls;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class NewCustomerRegistration : System.Web.UI.Page
    {
        #region Members

        CommonMethods objCommonMethods = new CommonMethods();
        iDsignBAL objIdsignBal = new iDsignBAL();
        BillCalculatorBAL objBillCalculatorBAL = new BillCalculatorBAL();
        ConsumerBal objConsumerBal = new ConsumerBal();
        string Key = "ConsumerRegistration";


        #endregion
        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            //SecurityDongle securityDongle = new SecurityDongle();
            //if (securityDongle.DongleMessage != "Success")
            //{
            //    Response.Redirect("~/ValidateUser.aspx");
            //}

            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnFinish_Click(object sender, EventArgs e)
        {

        }

        protected void btnStep1_Click(object sender, EventArgs e)
        {
            try
            {
                //ddlState_SelectedIndexChanged(null,null);
                //if (ddlTitle.SelectedIndex < 0)
                //{
                //    ddlTitle.Focus();
                //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate Title')", true);
                //}
                //else if (ddlState.SelectedIndex < 0)
                //{
                //    ddlState.Focus();
                //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate State')", true);
                //}
                //else if (ddlDistrict.SelectedIndex < 0)
                //{
                //    ddlDistrict.Focus();
                //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate District')", true);
                //}
                //else if (ddlIdentityType.SelectedIndex < 0)
                //{
                //    ddlIdentityType.Focus();
                //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "alert('Please select appropriate Identity Type')", true);
                //}
                //if (!string.IsNullOrEmpty(txtDocumentNo.Text))
                //{
                //    if (Convert.ToBoolean(IsDocumentNoExists(txtDocumentNo.Text)))
                //    {
                //        objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.DOCUMENT_NO_EXISTS_MSG, pnlMessage, lblMessage);
                //    }
                //    else
                //    {
                //        hfValidStep.Value = "1";
                //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "$('#wizard').smartWizard('goToStep',2);", true);
                //    }
                //}
                //else
                //{
                hfValidStep.Value = "1";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "$('#wizard').smartWizard('goToStep',2);", true);
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnStep2_Click(object sender, EventArgs e)
        {
            try
            {
                hfValidStep.Value = "2";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "$('#wizard').smartWizard('goToStep',3);", true);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnStep3_Click(object sender, EventArgs e)
        {
            hfValidStep.Value = "3";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "$('#wizard').smartWizard('goToStep',4);", true);

        }
        #endregion
        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void GoToStepOne()
        {
            hfValidStep.Value = "0";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "step1validate", "$('#wizard').smartWizard('goToStep',1);", true);
        }
        #endregion
    }
}