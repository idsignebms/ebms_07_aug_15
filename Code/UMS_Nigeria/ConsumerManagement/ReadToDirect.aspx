﻿<%@ Page Title=":: Change Customer Read Type ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="ReadToDirect.aspx.cs" Inherits="UMS_Nigeria.ConsumerManagement.ReadToDirect" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControls/Authentication.ascx" TagName="Authentication" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upPaymentEntry" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div class="text_total">
                    <div class="text-heading">
                        <asp:Literal ID="litAddState" runat="server" Text="Change Customer Read Type"></asp:Literal>
                    </div>
                    <div class="star_text">
                        <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div class="dot-line">
                </div>
                <div class="inner-box">
                    <div class="inner-box1">
                        <div class="text-inner">
                            <asp:Literal ID="litCountryName" runat="server" Text="<%$ Resources:Resource, ACCNO_OLD_ACCNO%>"></asp:Literal>
                            <span class="span_star">*</span><div class="space">
                            </div>
                            <asp:TextBox CssClass="text-box" runat="server" ID="txtAccountNo" MaxLength="20"
                                onblur="return TextBoxBlurValidationlbl(this,'spanAccountNo','Global Account No / Old Account No')"
                                onkeypress="GetAccountDetails(event)" placeholder="Global Account No / Old Account No"></asp:TextBox>
                            &nbsp
                            <asp:FilteredTextBoxExtender ID="ftbAccNo" runat="server" TargetControlID="txtAccountNo"
                                FilterType="Numbers" ValidChars=" ">
                            </asp:FilteredTextBoxExtender>
                            <asp:LinkButton runat="server" Visible="false" ID="lblSearch" OnClick="lblSearch_Click"
                                CssClass="search_img"></asp:LinkButton>
                            <br />
                            <span id="spanAccountNo" class="span_color"></span>
                        </div>
                    </div>
                </div>
                <div class="box_total">
                    <div class="box_total_a" style="margin-left: 15px;">
                        <asp:Button ID="btnGo" Text="<%$ Resources:Resource, GET_CUSTOMER_DETAILS%>" CssClass="box_s"
                            runat="server" OnClientClick="return ValidateGo();" OnClick="btnGo_Click" />
                    </div>
                </div>
                <div class="clr">
                </div>
                <br />
                <div id="divCustomerDetails" runat="server" visible="false">
                    <div class="out-bor_a">
                        <div class="inner-sec">
                            <div class="heading" style="padding-left: 16px; padding-top: 17px;">
                                <asp:Literal ID="Literal213" runat="server" Text="<%$ Resources:Resource, CUSTOMER_DETAILS%>"></asp:Literal>
                            </div>
                            <div class="out-bor_aTwo">
                                <table class="customerdetailsTamle">
                                    <tr>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label runat="server" ID="lblGlobalAccNoAndAccNo"></asp:Label>
                                            <asp:Label runat="server" Visible="false" ID="lblAccountno"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, ROUTE_SEQUENCE_NO%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Literal ID="litRouteSequenceNo" runat="server"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label runat="server" ID="lblServiceAddress"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal3" runat="server" Text="Name"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label runat="server" ID="lblName"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, OUT_STANDING_AMT%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Label ID="lblTotalDueAmount" runat="server"></asp:Label>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal6" runat="server" Text="Read Type"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Literal ID="lblReadType" runat="server" Text=""></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Literal ID="MeterNo" runat="server" Text=""></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Literal ID="litOldAccoutnNo" runat="server" Text=""></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal5" runat="server" Text="Last Meter Reading"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Literal ID="lblMeterDails" runat="server" Text="" Visible="false"></asp:Literal>
                                            <asp:Literal ID="lblLastMeterReading" runat="server" Text=""></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            <asp:Literal ID="Literal11" runat="server" Text="Last Reading Date"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize">
                                            <asp:Literal ID="lblLastReadingDate" runat="server" Text=""></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="divConfirmation" runat="server" visible="false" style="width: auto;">
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, REASON%>"></asp:Literal>
                                        <span class="span_star">*</span></label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox CssClass="text-box" ID="txtReason" runat="server" onblur="return TextBoxBlurValidationlbl(this,'SpanReason','Reason')"
                                        placeholder="Enter Reason" TextMode="MultiLine"></asp:TextBox>
                                    <span id="SpanReason" class="span_color"></span>
                                </div>
                            </div>
                            <div class="space">
                            </div>
                            <div class="box_total">
                                <div class="box_total_a">
                                    <asp:Button ID="btnDisconnectMeter" OnClientClick="return UpdateValidate();" TabIndex="8"
                                        Text="Disconnect Meter" CssClass="box_s" OnClick="btnDisconnectMeter_Click" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div id="divAssignMeter" runat="server" visible="false" style="width: auto;">
                            <div class="box_total">
                                <div class="box_total_a">
                                    <div style="margin: 0 10px 10px 10px">
                                        Do you want to shift this customer from Read to Direct connection?
                                    </div>
                                    <asp:Button ID="btnNo" TabIndex="8" Text="<%$ Resources:Resource, CANCEL%>" CssClass="box_s"
                                        runat="server" OnClick="btnNo_Click" />
                                    <asp:Button ID="btnSave" TabIndex="8" Text="<%$ Resources:Resource, PROCEED%>" CssClass="box_s"
                                        runat="server" OnClick="btnSave_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="rnkland">
                    <%-- Confirm popup Start--%>
                    <asp:ModalPopupExtender ID="mpeCofirm" runat="server" PopupControlID="PanelCofirm"
                        TargetControlID="btnCofirm" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnCofirm" runat="server" Text="Button" CssClass="popbtn" />
                    <asp:Panel ID="PanelCofirm" runat="server" DefaultButton="btnYes" CssClass="modalPopup"
                        class="poppnl" Style="display: none;">
                        <div class="popheader popheaderlblred" id="popup" runat="server">
                            <asp:Label ID="lblConfirmMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnYes" runat="server" OnClientClick="return closePopup();" OnClick="btnYes_Click"
                                    Text="<%$ Resources:Resource, YES%>" CssClass="ok_btn" />
                                <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, NO%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                        TargetControlID="btnActivate" BackgroundCssClass="modalBackground">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelActivate" runat="server" DefaultButton="btnActiveCancel" CssClass="modalPopup"
                        Style="display: none; border-color: #639B00;">
                        <div class="popheader" id="popActivate" runat="server" style="background-color: #639B00;">
                            <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                        </div>
                        <div class="space">
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Confirm popup Closed--%>
                </div>
                <asp:ModalPopupExtender ID="mpeAlert" runat="server" TargetControlID="hdnCancelCNA"
                    PopupControlID="pnlAlert" BackgroundCssClass="popbg" CancelControlID="btnBPOK">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hdnCancelCNA" runat="server"></asp:HiddenField>
                <asp:Panel runat="server" ID="pnlAlert" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblAlertMsg" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnBPOK" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="btn_ok"
                                Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="hdnCustomerTypeID" runat="server" Value="" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <uc1:Authentication ID="ucAuthentication" runat="server" />
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        //        $(document).keydown(function (e) {
        //            // ESCAPE key pressed 
        //            if (e.keyCode == 27) {
        //                $(".rnkland").fadeOut("slow");
        //                $(".mpeConfirmDelete").fadeOut("slow");
        //                document.getElementById("overlay").style.display = "none";
        //            }
        //        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        //        function pageLoad() {
        //            $get("<%=txtAccountNo.ClientID%>").focus();
        //        };


        $(document).ready(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
            //AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
            //AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);
        });
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/ReadToDirectCustomers.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
</asp:Content>
