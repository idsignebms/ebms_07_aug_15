﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Xml;
using System.Data;
using Resources;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class ReadToDirect : System.Web.UI.Page
    {
        #region Members
        CustomerDetailsBAL _ObjCustomerDetailsBAL = new CustomerDetailsBAL();
        CustomerDetailsBe _ObjCustomerDetailsBe = new CustomerDetailsBe();
        MastersBAL _ObjMastersBAL = new MastersBAL();
        ReportsBal objReportsBal = new ReportsBal();
        MastersBE _ObjeMastersBE = new MastersBE();
        XmlDocument _objXmlDocument = new XmlDocument();
        MastersListBE objMastersListBE = new MastersListBE();
        iDsignBAL _ObjIdsignBal = new iDsignBAL();
        ConsumerBal objConsumerBal = new ConsumerBal();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        CustomerRegistrationBE objCustomerRegistrationBE = new CustomerRegistrationBE();
        MastersBE objMastersBE = new MastersBE();
        public string Key = "AssignMeter";
        string AccountNo;
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString[UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO]))
                {
                    string AccountNo = _ObjIdsignBal.Decrypt(Request.QueryString[UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO]).ToString();
                    txtAccountNo.Text = AccountNo;
                }
            }
        }

        protected void lblSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADVANCE_SEARCH_PAGE + "?" + UMS_Resource.FROM_PAGE + "=" + _ObjIdsignBal.Encrypt(Constants.CustomerBillPayment), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnGo_Click1(object sender, EventArgs e)
        {
            RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
            objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                objLedgerBe.BUID = string.Empty;
            objLedgerBe = _ObjIdsignBal.DeserializeFromXml<RptCustomerLedgerBe>(objReportsBal.GetLedger(objLedgerBe, ReturnType.Set));
            if (objLedgerBe.IsSuccess)
            {
                if (objLedgerBe.IsCustExistsInOtherBU)
                {
                    lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                    mpeAlert.Show();
                    divAssignMeter.Visible = false;
                }
                else if (objLedgerBe.CustIsNotActive)
                {
                    lblAlertMsg.Text = "Customer is not active";
                    mpeAlert.Show();
                    divAssignMeter.Visible = false;
                }
                else
                {
                    BindUserDetails();
                }
            }
            else
            {
                divCustomerDetails.Visible = false;
                divAssignMeter.Visible = false;
                ClearFields();
                _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, "No customer found with this account no.", pnlMessage, lblMessage);
            }
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            XmlDocument xml = new XmlDocument();
            XmlDocument XmlCustomerRegistrationBE = new XmlDocument();
            RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
            ChangeBookNoBal objChangeBookNoBal = new ChangeBookNoBal();
            ChangeBookNoBe _objBookNoBe = new ChangeBookNoBe();
            CustomerRegistrationBE objCustomerRegistrationBE = new CustomerRegistrationBE();
            objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                objLedgerBe.BUID = string.Empty;

            xml = objConsumerBal.GetCustomerDetailsForMeter_ByCheck(objLedgerBe);
            if (xml.SelectSingleNode("/*").Name == "RptCustomerLedgerBe")
            {
                objLedgerBe = _ObjIdsignBal.DeserializeFromXml<RptCustomerLedgerBe>(xml);
                if (objLedgerBe.IsSuccess)
                {
                    if (objLedgerBe.IsCustExistsInOtherBU)
                    {
                        lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                        mpeAlert.Show();
                        divAssignMeter.Visible = false;
                    }
                    else if (objLedgerBe.ActiveStatusId == (int)CustomerStatus.Hold)
                    {
                        divAssignMeter.Visible = false;
                        lblAlertMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.Hold, "Read to Direct Change");
                        mpeAlert.Show();
                    }
                    else if (objLedgerBe.ActiveStatusId == (int)CustomerStatus.Closed)
                    {
                        divAssignMeter.Visible = false;
                        lblAlertMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.Closed, "Read to Direct Change");
                        mpeAlert.Show();
                    }
                    else if (objLedgerBe.ActiveStatusId == (int)CustomerStatus.InActive)
                    {
                        divAssignMeter.Visible = false;
                        lblAlertMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.InActive, "Read to Direct Change");
                        mpeAlert.Show();
                    }
                }
                else
                {
                    //CustomerNotFound();
                    divCustomerDetails.Visible = false;
                    divAssignMeter.Visible = false;
                    ClearFields();
                    _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.CUSTOMER_NOT_FOUND_MSG, pnlMessage, lblMessage);
                }
            }
            else if (xml.SelectSingleNode("/*").Name == "CustomerRegistrationBE")
            {

                objCustomerRegistrationBE = _ObjIdsignBal.DeserializeFromXml<CustomerRegistrationBE>(xml);
                lblAccountno.Text = objCustomerRegistrationBE.AccountNo;
                lblGlobalAccNoAndAccNo.Text = objCustomerRegistrationBE.GlobalAccountNumber;
                lblServiceAddress.Text = objCustomerRegistrationBE.FullServiceAddress;
                lblName.Text = objCustomerRegistrationBE.FirstNameLandlord;
                litRouteSequenceNo.Text = objCustomerRegistrationBE.RouteName;// Convert.ToString(objCustomerRegistrationBE.RouteSequenceNumber);
                lblTotalDueAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(objCustomerRegistrationBE.OutStandingAmount.ToString()), 2, Constants.MILLION_Format).ToString(); //Raja-ID065 For Getting amt with comma seperated
                hdnCustomerTypeID.Value = Convert.ToString(objCustomerRegistrationBE.CustomerTypeId);
                if (objCustomerRegistrationBE.ReadCodeID == (int)ReadCode.Direct)
                {
                    lblReadType.Text = ReadCode.Direct.ToString();
                    divCustomerDetails.Visible = true;
                    divConfirmation.Visible = false;
                    lblAlertMsg.Text = "This is direct customer.";
                    mpeAlert.Show();
                }
                else if (objCustomerRegistrationBE.ReadCodeID == (int)ReadCode.Read)
                {
                    divConfirmation.Visible = true;
                    divCustomerDetails.Visible = true;
                    lblReadType.Text = ReadCode.Read.ToString();
                    MeterNo.Text = objCustomerRegistrationBE.MeterNumber;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            ChangeReadToDirect();
        }

        protected void btnDisconnectMeter_Click(object sender, EventArgs e)
        {
            divAssignMeter.Visible = true;
            divConfirmation.Visible = false;
        }

        protected void btnNo_Click(object sender, EventArgs e)
        {
            divAssignMeter.Visible = false;
            divConfirmation.Visible = false;
            divCustomerDetails.Visible = false;
            ClearFields();
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                objMastersBE.AccountNo = lblAccountno.Text;
                objMastersBE.MeterNo = MeterNo.Text;
                objMastersBE.Remarks = _ObjIdsignBal.ReplaceNewLines(txtReason.Text, true);
                objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.ApprovalStatusId = (int)EnumApprovalStatus.Approved;
                objMastersBE.FunctionId = (int)EnumApprovalFunctions.ChangeReadToDirect;
                objMastersBE.IsFinalApproval = true;
                objMastersBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                objMastersBE = _ObjIdsignBal.DeserializeFromXml<MastersBE>(_ObjMastersBAL.ChangeReadCustToDirect(objMastersBE));
                if (objMastersBE.IsSuccess)
                {
                    lblActiveMsg.Text = Resource.MODIFICATIONS_UPDATED;
                    divAssignMeter.Visible = divCustomerDetails.Visible = false;
                    txtAccountNo.Text = txtReason.Text = string.Empty;
                    mpeActivate.Show();
                }
                else if (objMastersBE.IsApprovalInProcess)
                {
                    divAssignMeter.Visible = divCustomerDetails.Visible = false;
                    txtReason.Text = string.Empty;
                    lblAlertMsg.Text = Resource.REQ_STILL_PENDING;
                    mpeAlert.Show();
                }
                else if (objMastersBE.IsMeterChangeExist)
                {
                    divAssignMeter.Visible = divCustomerDetails.Visible = false;
                    txtReason.Text = string.Empty;
                    lblAlertMsg.Text = "Meter Change request is pending for this Customer.";
                    mpeAlert.Show();
                }
                else if (objMastersBE.IsReadingsExist)
                {
                    divAssignMeter.Visible = divCustomerDetails.Visible = false;
                    txtReason.Text = string.Empty;
                    lblAlertMsg.Text = "Meter Readings request is pending for this Customer.";
                    mpeAlert.Show();
                }
                else if (objMastersBE.IsHavingCapmiAmt)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblred");
                    lblAlertMsg.Text = "Previous Meter is CAPMI Meter and Outstanding amount is not adjusted.";
                    mpeAlert.Show();
                } 
                else
                {
                    divAssignMeter.Visible = divCustomerDetails.Visible = true;
                    Message(UMS_Resource.CUSTOMER_UPDATED_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods

        private void BindUserDetails()
        {
            divAssignMeter.Visible = false;

            XmlDocument xml = new XmlDocument();
            try
            {
                objCustomerRegistrationBE.GolbalAccountNumber = txtAccountNo.Text;
                xml = objConsumerBal.GetCustomerForAssignMeter(objCustomerRegistrationBE);
                if (xml != null)
                {
                    objCustomerRegistrationBE = _ObjIdsignBal.DeserializeFromXml<CustomerRegistrationBE>(xml);
                    if (objCustomerRegistrationBE != null)
                    {
                        if (objCustomerRegistrationBE.IsSuccessful)
                        {
                            lblAccountno.Text = objCustomerRegistrationBE.AccountNo;
                            lblGlobalAccNoAndAccNo.Text = objCustomerRegistrationBE.GlobalAccountNumber;
                            lblServiceAddress.Text = objCustomerRegistrationBE.FullServiceAddress;
                            lblName.Text = objCustomerRegistrationBE.FirstNameLandlord;
                            litOldAccoutnNo.Text = objCustomerRegistrationBE.OldAccountNo;//Faiz-ID103
                            litRouteSequenceNo.Text = objCustomerRegistrationBE.RouteName;// Convert.ToString(objCustomerRegistrationBE.RouteSequenceNumber);
                            lblTotalDueAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(objCustomerRegistrationBE.OutStandingAmount.ToString()), 2, Constants.MILLION_Format).ToString(); //Raja-ID065 For Getting amt with comma seperated
                            hdnCustomerTypeID.Value = Convert.ToString(objCustomerRegistrationBE.CustomerTypeId);
                            lblMeterDails.Text = objCustomerRegistrationBE.MeterDials.ToString();
                            lblLastMeterReading.Text = AppendZeros(string.IsNullOrEmpty(objCustomerRegistrationBE.LastMeterReading) ? "0" : objCustomerRegistrationBE.LastMeterReading, !string.IsNullOrEmpty(lblMeterDails.Text) ? Convert.ToInt32(lblMeterDails.Text) : 0);
                            lblLastReadingDate.Text = objCustomerRegistrationBE.LastReadingDate;

                            if (objCustomerRegistrationBE.ReadCodeID == (int)ReadCode.Direct)
                            {
                                lblReadType.Text = ReadCode.Direct.ToString();
                                divCustomerDetails.Visible = true;
                                divConfirmation.Visible = false;
                                lblAlertMsg.Text = "This is direct customer.";
                                mpeAlert.Show();
                            }
                            else if (objCustomerRegistrationBE.ReadCodeID == (int)ReadCode.Read)
                            {
                                divConfirmation.Visible = true;
                                divCustomerDetails.Visible = true;
                                lblReadType.Text = ReadCode.Read.ToString();
                                MeterNo.Text = objCustomerRegistrationBE.MeterNumber;
                            }
                        }
                        else
                        {
                            divCustomerDetails.Visible = false;
                            divAssignMeter.Visible = false;
                            ClearFields();
                            Message("No customer found with this account no.", UMS_Resource.MESSAGETYPE_ERROR);
                        }
                    }
                    else
                    {
                        divCustomerDetails.Visible = false;
                        divAssignMeter.Visible = false;
                        ClearFields();
                        Message("No customer found with this account no.", UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
            }
            catch (Exception ex)
            {
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                Message("Error in customer search.", UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        //Append Zeros Before the String Passed
        //Get Max Number of Given Dials
        private string AppendZeros(string Reading, int Dails)
        {
            string Str = "";
            if (Reading.Length < Dails)
            {
                for (int i = 0; i < (Dails - Reading.Length); i++)
                {
                    Str += "0";
                }
            }
            return Str + Reading;
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void ClearFields()
        {
            txtAccountNo.Text = lblAccountno.Text = lblName.Text = lblServiceAddress.Text = lblTotalDueAmount.Text = litRouteSequenceNo.Text = lblReadType.Text = MeterNo.Text = txtReason.Text = litOldAccoutnNo.Text = string.Empty;
        }

        public void ChangeReadToDirect()
        {
            if (_ObjCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.ChangeReadToDirect, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString()))
            {
                lblConfirmMsg.Text = "Are you sure you want to shift this customer from Read to Direct connection?";
                mpeCofirm.Show();
            }
            else
            {
                objMastersBE.AccountNo = lblAccountno.Text;
                objMastersBE.MeterNo = MeterNo.Text;
                objMastersBE.Remarks = _ObjIdsignBal.ReplaceNewLines(txtReason.Text, true);
                objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.ApprovalStatusId = (int)EnumApprovalStatus.Process;
                objMastersBE.FunctionId = (int)EnumApprovalFunctions.ChangeReadToDirect;
                objMastersBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                objMastersBE.IsFinalApproval = false;

                objMastersBE = _ObjIdsignBal.DeserializeFromXml<MastersBE>(_ObjMastersBAL.ChangeReadCustToDirect(objMastersBE));

                if (objMastersBE.IsSuccess)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblgreen");
                    lblActiveMsg.Text = Resource.APPROVAL_TARIFF_CHANGE;
                    divAssignMeter.Visible = divCustomerDetails.Visible = false;
                    txtAccountNo.Text = txtReason.Text = string.Empty;
                    mpeActivate.Show();

                }
                else if (objMastersBE.IsApprovalInProcess)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblred");
                    divAssignMeter.Visible = divCustomerDetails.Visible = false;
                    txtReason.Text = string.Empty;
                    lblAlertMsg.Text = Resource.REQ_STILL_PENDING;
                    mpeAlert.Show();
                }
                else if (objMastersBE.IsMeterChangeExist)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblred");
                    divAssignMeter.Visible = divCustomerDetails.Visible = false;
                    txtReason.Text = string.Empty;
                    lblAlertMsg.Text = "Meter Change request is pending for this Customer.";
                    mpeAlert.Show();
                }
                else if (objMastersBE.IsReadingsExist)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblred");
                    divAssignMeter.Visible = divCustomerDetails.Visible = false;
                    txtReason.Text = string.Empty;
                    lblAlertMsg.Text = "Meter Readings request is pending for this Customer.";
                    mpeAlert.Show();
                }
                else if (objMastersBE.IsHavingCapmiAmt)
                {
                    popActivate.Attributes.Add("class", "popheader popheaderlblred");
                    lblAlertMsg.Text = "Previous Meter is CAPMI Meter and Outstanding amount is not adjusted.";
                    mpeAlert.Show();
                } 
                else
                {
                    divAssignMeter.Visible = divCustomerDetails.Visible = true;
                    Message(UMS_Resource.CUSTOMER_UPDATED_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
        }

        #endregion
    }
}