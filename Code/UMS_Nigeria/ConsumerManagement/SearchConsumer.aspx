﻿<%@ Page Title="Search Customer" Language="C#" MasterPageFile="~/MasterPages/EDMUMS.Master"
    AutoEventWireup="true" CodeBehind="SearchConsumer.aspx.cs" Inherits="UMS_Nigeria.ConsumerManagement.SearchConsumer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="contentBody" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="edmcontainer">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upSearchCustomer" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <h3>
                    <asp:Literal ID="litSearchCustomer" runat="server" Text="<%$ Resources:Resource, SEARCH_CUSTOMER%>"></asp:Literal>
                </h3>
                <div class="clear">
                    &nbsp;</div>
                <div class="consumer_feild">
                    <div class="consume_name">
                        <asp:Literal ID="litAccountNo" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal></div>
                    <div class="consume_input" style="width: 215px;">
                        <asp:TextBox ID="txtAccountNo" runat="server" MaxLength="50" placeholder="Account No"></asp:TextBox>
                        <asp:LinkButton ID="lbtnSearch" OnClick="lbtnSearch_Click" Text="<%$ Resources:Resource, SEARCH%>"
                            runat="server"></asp:LinkButton>
                    </div>
                    <div class="consume_name c_left">
                        (OR)
                    </div>
                    <div class="consume_name c_left">
                        <asp:Literal ID="litDocumentNo" runat="server" Text="<%$ Resources:Resource, DOCUMENT_NO%>"></asp:Literal>
                    </div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtDocumentNo" runat="server" MaxLength="100" placeholder="Document No"></asp:TextBox>
                    </div>
                    <div class="clear pad_5">
                        <div style="margin-left: 653px;">
                            <asp:Button ID="btnSearch" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SEARCH%>"
                                CssClass="calculate_but" runat="server" OnClick="btnSearch_Click" ToolTip="Search Customer" />
                        </div>
                    </div>
                </div>
                <div class="consumer_total">
                    <div class="pad_10 clear">
                    </div>
                </div>
                <div id="divCustomerDetails" runat="server" visible="false">
                    <div class="edmcontainer">
                        <h3>
                            <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, CUSTOMER_DETAILS%>"></asp:Literal>
                        </h3>
                        <div class="pad_10">
                        </div>
                        <div class="consumer_feild search_unique_total">
                            <div style="text-align: left; width: 160px;" class="consume_name c_left">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, CUSTOMER_UNIQUE_NO%>"></asp:Literal>
                            </div>
                            <div class="fltl">
                                :</div>
                            <div class="consume_input">
                                <asp:Label ID="lblCustomerUniqueNo" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="consume_name c_left" style="text-align: left; width: 160px;">
                                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                            </div>
                            <div class="fltl">
                                :</div>
                            <div class="consume_input">
                                <asp:Label ID="lblAccountNo" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="clear pad_5">
                            </div>
                            <div class="consume_name c_left" style="text-align: left; width: 160px;">
                                <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                            </div>
                            <div class="fltl">
                                :</div>
                            <div class="consume_input">
                                <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="consume_name c_left" style="text-align: left; width: 160px;">
                                <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, EMAIL_ID%>"></asp:Literal>
                            </div>
                            <div class="fltl">
                                :</div>
                            <div class="consume_input">
                                <asp:Label ID="lblEmailId" runat="server" Text="---"></asp:Label>
                            </div>
                            <div class="clear pad_5">
                            </div>
                            <div class="consume_name c_left" style="text-align: left; width: 160px;">
                                <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, DOCUMENT_NO%>"></asp:Literal>
                            </div>
                            <div class="fltl">
                                :</div>
                            <div class="consume_input">
                                <asp:Label ID="lblDocumentNo" runat="server" Text=""></asp:Label>
                            </div>
                            <%--<div class="clear pad_5">
                            </div>
                            <div class="consume_name c_left" style="text-align: left; width: 197px; font-weight: bold;">
                                <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, CUSTOMER_SEQUENCE_NO%>"></asp:Literal>
                                :
                            </div>
                            <div class="consume_input">
                                <asp:Label ID="lblCustomerSequenceNo" runat="server" Text=""></asp:Label>
                            </div>--%>
                            <div class="consume_name c_left" style="text-align: left; width: 160px;">
                                <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, HOME_CONTACT_NO%>"></asp:Literal>
                            </div>
                            <div class="fltl">
                                :</div>
                            <div class="consume_input">
                                <asp:Label ID="lblHomeContactNo" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="clear pad_5">
                            </div>
                            <div class="consume_name c_left" style="text-align: left; width: 160px;">
                                <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, BUSINESS_CONTACT_NO%>"></asp:Literal>
                            </div>
                            <div class="fltl">
                                :</div>
                            <div class="consume_input">
                                <asp:Label ID="lblBusinessContactNo" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="consume_name c_left" style="text-align: left; width: 160px;">
                                <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:Resource, OTHER_CONTACT_NO%>"></asp:Literal>
                            </div>
                            <div class="fltl">
                                :</div>
                            <div class="consume_input">
                                <asp:Label ID="lblOtherContactNo" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="clear pad_5">
                            </div>
                            <div class="consume_name c_left" style="text-align: left; width: 160px;">
                                <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, MOBILE_NO%>"></asp:Literal>
                            </div>
                            <div class="fltl">
                                :</div>
                            <div class="consume_input">
                                <asp:Label ID="lblMobileNo" runat="server" Text=""></asp:Label>
                            </div>
                            <%--<div class="clear pad_5">
                            </div>
                            <div class="consume_name c_left" style="text-align: left; width: 197px; font-weight: bold;">
                                <asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:Resource, DISTRICT_NAME%>"></asp:Literal>
                                :
                            </div>
                            <div class="consume_input">
                                <asp:Label ID="lblDistrictName" runat="server" Text=""></asp:Label>
                            </div>--%>
                            <div class="consume_name c_left" style="text-align: left; width: 160px;">
                                <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal>
                            </div>
                            <div class="fltl">
                                :</div>
                            <div class="consume_input">
                                <asp:Label ID="lblBookNo" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="clear pad_10">
                                <div style="margin-left: 705px;">
                                    <asp:Button ID="btnEdit" Text="View" ToolTip="Edit Customer" CssClass="calculate_but"
                                        runat="server" OnClick="btnEdit_Click" />
                                </div>
                            </div>
                            <div class="consumer_total">
                                <div class="clear pad_10">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).ready(function () {
            //AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            //AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Javascript/jquery.min.js" type="text/javascript"></script>
    <script src="../Javascript/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/SearchConsumer.js" type="text/javascript"></script>
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
