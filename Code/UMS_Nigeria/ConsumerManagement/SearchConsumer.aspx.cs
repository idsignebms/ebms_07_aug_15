﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for Search consumer
                     
 Developer        : id027-Karthik Thati
 Creation Date    : 07-03-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using Resources;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class SearchConsumer : System.Web.UI.Page
    {
        #region Members

        iDsignBAL objIdsignBal = new iDsignBAL();
        ConsumerBal objConsumerBal = new ConsumerBal();
        CommonMethods objCommonMethods = new CommonMethods();
        public string Key = "SearchCustomer";

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.CssClass = lblMessage.Text = string.Empty;
            if (!IsPostBack)
            {
                string path = string.Empty;
                path = objCommonMethods.GetPagePath(this.Request.Url);
                if (objCommonMethods.IsAccess(path))
                {

                    if (!string.IsNullOrEmpty(Request.QueryString[UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO]))
                    {
                        txtAccountNo.Text = objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO]);
                    }
                }
                else
                {
                    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                }
            }

        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.EDIT_CONSUMER_PAGE + "?" + UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO + "=" + objIdsignBal.Encrypt(lblCustomerUniqueNo.Text), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GetCustomerDetails();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lbtnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADVANCE_SEARCH_PAGE + "?" + UMS_Resource.FROM_PAGE + "=" + objIdsignBal.Encrypt(Constants.SearchCustomerPageName), false);
                //Response.Redirect(UMS_Resource.EDIT_CONSUMER_PAGE + "?" + UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO + "=" + objIdsignBal.Encrypt(lblCustomerUniqueNo.Text), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods

        private void GetCustomerDetails()
        {
            try
            {
                ConsumerBe objConsumerBe = new ConsumerBe();
                objConsumerBe.DocumentNo = txtDocumentNo.Text;
                objConsumerBe.AccountNo = txtAccountNo.Text;
                objConsumerBe.OldAccountNo = "";
                objConsumerBe.MeterNo = "";
                objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.GetServiceDetails(objConsumerBe, ReturnType.Group));
                if (objConsumerBe != null)
                {
                    lblCustomerUniqueNo.Text = objConsumerBe.CustomerUniqueNo.ToString();
                    lblAccountNo.Text = objConsumerBe.AccountNo;
                    lblName.Text = objConsumerBe.Name;
                    lblEmailId.Text = objConsumerBe.EmailId;
                    lblDocumentNo.Text = objConsumerBe.DocumentNo;
                    //lblCustomerSequenceNo.Text = objConsumerBe.CustomerSequenceNo.ToString();
                    lblCustomerUniqueNo.Text = objConsumerBe.CustomerUniqueNo;
                    lblHomeContactNo.Text = objConsumerBe.HomeContactNo;
                    lblBusinessContactNo.Text = objConsumerBe.BusinessContactNo;
                    lblOtherContactNo.Text = objConsumerBe.OtherContactNo;
                    lblMobileNo.Text = objConsumerBe.MobileNo;
                    //lblDistrictName.Text = objConsumerBe.DistrictName;
                    lblBookNo.Text = objConsumerBe.BookNo;
                    divCustomerDetails.Visible = true;
                }
                else
                {
                    divCustomerDetails.Visible = false;
                    Message(UMS_Resource.CUSTOMER_NOT_FOUND_MSG, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

    }
}