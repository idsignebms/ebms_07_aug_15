﻿<%@ Page Title=":: Search Customer ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master" AutoEventWireup="true"
    CodeBehind="SearchCustomer.aspx.cs" Inherits="UMS_Nigeria.ConsumerManagement.SearchCustomer" Theme="Green" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControls/Authentication.ascx" TagName="Authentication" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script>
        function Validation() {
            var IsValid = true;
            var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo');

            if (TextBoxValidationlbl(txtAccountNo, document.getElementById("spanAccountNo"), "Global Account No./ Old Account No") == false) IsValid = false;
            return IsValid;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out_bor_Payment">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="pnlSearchCustomerGO" DefaultButton="btnGo" runat="server">
        <asp:UpdatePanel ID="upPaymentEntry" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div class="text_total">
                    <div class="text-heading">
                        <asp:Literal ID="litAddState" runat="server" Text="Search Customer"></asp:Literal>
                    </div>
                    <div class="star_text">
                        <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div class="dot-line">
                </div>
                <div class="inner-box">
                    <div class="inner-box1">
                        <div class="text-inner">
                            <asp:Literal ID="litCountryName" runat="server" Text="<%$ Resources:Resource, ACCNO_OLD_ACCNO%>"></asp:Literal>
                            <span class="span_star">*</span><div class="space">
                            </div>
                            <asp:TextBox CssClass="text-box" runat="server" ID="txtAccountNo" MaxLength="20"
                                onblur="return TextBoxBlurValidationlbl(this,'spanAccountNo','Account No')" onkeypress="GetAccountDetails(event)"
                                placeholder="Enter Global Account No./ Old Account No"></asp:TextBox>
                            &nbsp
                            <asp:FilteredTextBoxExtender ID="ftbAccNo" runat="server" TargetControlID="txtAccountNo"
                                FilterType="Numbers" ValidChars=" ">
                            </asp:FilteredTextBoxExtender>
                            <br />
                            <span id="spanAccountNo" class="span_color"></span>
                        </div>
                    </div>
                </div>
                <div class="box_total">
                    <div class="box_total_a" style="margin-left: 15px;">
                        <asp:Button ID="btnGo" Text="<%$ Resources:Resource, GET_CUSTOMER_DETAILS%>" CssClass="box_s"
                            runat="server" OnClick="btnGo_Click" OnClientClick="return Validation();" />
                    </div>
                </div>
                <div class="clr">
                </div>
                <br />
                <asp:Panel runat="server" ID="pnlAlert" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblAlertMsg" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnBPOK" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="btn_ok"
                                Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlMeterDetials" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblMeterDetails" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="clear pad_10">
                    </div>
                    <div class="consumer_feild">
                        <div class="consume_name">
                            <asp:Literal ID="Literal13" runat="server" Text="Global Account No."></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblGlobalAccountNo" runat="server"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="Literal14" runat="server" Text="Meter No."></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblMeterNo" runat="server"></asp:Label>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="Literal15" runat="server" Text="Change Date"></asp:Literal>
                        </div>
                        <div class="consume_input c_left">
                            <asp:Label ID="lblChangeDate" runat="server"></asp:Label>
                        </div>
                    </div>
                 
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeAlert" runat="server" TargetControlID="hdnCancelCNA"
                    PopupControlID="pnlAlert" BackgroundCssClass="popbg" CancelControlID="btnBPOK">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hdnCancelCNA" runat="server"></asp:HiddenField>
                <asp:Panel runat="server" ID="Panel1" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="Button1" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="btn_ok"
                                Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        </asp:Panel>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        //        $(document).keydown(function (e) {
        //            // ESCAPE key pressed 
        //            if (e.keyCode == 27) {
        //                $(".rnkland").fadeOut("slow");
        //                $(".mpeConfirmDelete").fadeOut("slow");
        //                document.getElementById("overlay").style.display = "none";
        //            }
        //        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        //        function pageLoad() {
        //            $get("<%=txtAccountNo.ClientID%>").focus();
        //        };


        $(document).ready(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
            // AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
            //AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);
        });
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../Javascript/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/AssignMeters.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
</asp:Content>
