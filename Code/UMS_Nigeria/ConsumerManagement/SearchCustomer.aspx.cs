﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Xml;
using System.Data;
using Resources;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class SearchCustomer : System.Web.UI.Page
    {
        #region Members
        CustomerDetailsBAL _ObjCustomerDetailsBAL = new CustomerDetailsBAL();
        CustomerDetailsBe _ObjCustomerDetailsBe = new CustomerDetailsBe();
        MastersBAL _ObjMastersBAL = new MastersBAL();
        ReportsBal objReportsBal = new ReportsBal();
        MastersBE _ObjeMastersBE = new MastersBE();
        XmlDocument _objXmlDocument = new XmlDocument();
        MastersListBE objMastersListBE = new MastersListBE();
        iDsignBAL _ObjIdsignBal = new iDsignBAL();
        ConsumerBal objConsumerBal = new ConsumerBal();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        CustomerRegistrationBE objCustomerRegistrationBE = new CustomerRegistrationBE();

        public string Key = "SearchCustomer";
        string AccountNo;
        #endregion

        #region Methods

        
        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
        }


        protected void btnGo_Click(object sender, EventArgs e)
        {
            RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
            objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                objLedgerBe.BUID = string.Empty;
            objLedgerBe = _ObjIdsignBal.DeserializeFromXml<RptCustomerLedgerBe>(objReportsBal.GetLedger(objLedgerBe, ReturnType.Retrieve));
            if (objLedgerBe.IsSuccess)
            {
                if (objLedgerBe.IsCustExistsInOtherBU)
                {
                    lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                    mpeAlert.Show();
                }
                else if (objLedgerBe.CustIsNotActive)
                {
                    lblAlertMsg.Text = "Customer is not active";
                    mpeAlert.Show();
                }
                else
                {
                    Response.Redirect(UMS_Resource.CST_DETAILS_PAGE + "?ac=" + _ObjIdsignBal.Encrypt(txtAccountNo.Text));
                }
            }
            else
            {
                _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.CUSTOMER_NOT_FOUND_MSG, pnlMessage, lblMessage);
            }
        }
        #endregion
    }
}