﻿<%@ Page Title="::View Approvals::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master" Theme="Green"
AutoEventWireup="true" CodeBehind="ViewApproval.aspx.cs" Inherits="UMS_Nigeria.ConsumerManagement.ViewApproval" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upStates" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddCycle" runat="server" Text="View Customer Change Approvals"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="litBusinessUnit" runat="server" Text="Function"></asp:Literal>
                                <span class="span_star">*</span><br />
                                <asp:DropDownList ID="ddlFunctions" AutoPostBack="true" OnSelectedIndexChanged="ddlFunctions_SelectedIndexChanged"
                                    runat="server" CssClass="text-box select_box">
                                </asp:DropDownList>
                                <br />
                                <span id="spanBU" class="span_color"></span>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div id="divTarrifApprovalsList" runat="server" visible="false">
                        <div id="divgrid" class="grid marg_5" runat="server" style="width: 99% !important;
                            float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="litBuList" runat="server" Text="Customer Tariff Change Logs"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="divPrint" runat="server" align="center" style="float: left;
                            width: 97%; margin-left: 20px;">
                            <asp:GridView ID="gvTarrifApprovals" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%" ><%--OnRowDataBound="gvTarrifApprovals_RowDataBound"--%>
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOriGlobalAccNo" Visible="false" runat="server" Text='<%#Eval("AccountNo") %>'></asp:Label>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                            
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-Width="8%"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Tarrif" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldTarrifId" runat="server" Text='<%#Eval("OldClassID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblOldTarrif" runat="server" Text='<%#Eval("PreviousTariffName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Tarrif" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewTarrifId" runat="server" Text='<%#Eval("NewClassID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblNewTarrif" runat="server" Text='<%#Eval("ChangeRequestedTariffName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Cluster Type" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldClusterTypeId" runat="server" Text='<%#Eval("OldClusterTypeId") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblOldClusterType" runat="server" Text='<%#Eval("PreviousClusterName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Cluster Type" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewClusterTypeId" runat="server" Text='<%#Eval("NewClusterTypeId") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblNewClusterType" runat="server" Text='<%#Eval("ChangeRequestedClusterName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField ItemStyle-Width="10%" HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("TariffChangeRequestId") %>'
                                                    CommandName="approve" ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>"
                                                    OnClick="lnkApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkReject" CommandArgument='<%#Eval("TariffChangeRequestId") %>'
                                                    CommandName="reject" ForeColor="Green" runat="server" Text="Other" OnClick="lnkReject_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divNameApprovalsList" runat="server" visible="false">
                        <div id="div2" class="grid marg_5" runat="server" style="width: 99% !important; float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal1" runat="server" Text="Customer Name Change Logs"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="div3" runat="server" align="center" style="float: left; width: 97%;
                            margin-left: 20px;">
                            <asp:GridView ID="gvNameApprovals" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%" ><%--OnRowDataBound="gvNameApprovals_RowDataBound"--%>
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Name" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldTitle" runat="server" Text='<%#Eval("OldName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Name" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewTitle" runat="server" Text='<%#Eval("NewName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Known As" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldKnownAs" runat="server" Text='<%#Eval("OldKnownAs") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Known As" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewKnownAs" runat="server" Text='<%#Eval("KnownAs") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("NameChangeLogId") %>' CommandName="approve"
                                                    ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>" OnClick="lnkNameApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("NameChangeLogId") %>' CommandName="reject"
                                                    ForeColor="Green" runat="server" Text="Other" OnClick="lnkNameOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divTypeApprovalsList" runat="server" visible="false">
                        <div id="div4" class="grid marg_5" runat="server" style="width: 99% !important; float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal3" runat="server" Text="Customer Type Change Logs"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="div5" runat="server" align="center" style="float: left; width: 97%;
                            margin-left: 20px;">
                            <asp:GridView ID="gvTypeApprovals" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%" ><%--OnRowDataBound="gvTypeApprovals_RowDataBound"--%>
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Customer Type" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldTypeId" runat="server" Text='<%#Eval("OldCustomerTypeId") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblOldType" runat="server" Text='<%#Eval("OldCustomerType") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Customer Type" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewTypeId" runat="server" Text='<%#Eval("NewCustomerTypeId") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblNewType" runat="server" Text='<%#Eval("NewCustomerType") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("CustomerTypeChangeLogId") %>'
                                                    CommandName="approve" ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>"
                                                    OnClick="lnkTypeApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("CustomerTypeChangeLogId") %>'
                                                    CommandName="reject" ForeColor="Green" runat="server" Text="Other" OnClick="lnkTypeOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divStatusApprovalList" runat="server" visible="false">
                        <div id="div6" class="grid marg_5" runat="server" style="width: 99% !important; float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal4" runat="server" Text="Customer Status Change Logs"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="div7" runat="server" align="center" style="float: left; width: 97%;
                            margin-left: 20px;">
                            <asp:GridView ID="gvStatusApprovals" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%" ><%--OnRowDataBound="gvStatusApprovals_RowDataBound"--%>
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldStatusd" runat="server" Text='<%#Eval("OldStatusId") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblOldStatus" runat="server" Text='<%#Eval("OldStatus") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewStatusId" runat="server" Text='<%#Eval("NewStatusId") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblNewStatus" runat="server" Text='<%#Eval("NewStatus") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("ActiveStatusChangeLogId") %>'
                                                    CommandName="approve" ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>"
                                                    OnClick="lnkStatusApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("ActiveStatusChangeLogId") %>'
                                                    CommandName="reject" ForeColor="Green" runat="server" Text="Other" OnClick="lnkStatusOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divMeterApprovalList" runat="server" visible="false">
                        <div id="div8" class="grid marg_5" runat="server" style="width: 99% !important; float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal5" runat="server" Text="Customer Meter Number Change Logs"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="div9" runat="server" align="center" style="float: left; width: 97%;
                            margin-left: 20px;">
                            <asp:GridView ID="gvMeterApprovals" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%" ><%--OnRowDataBound="gvStatusApprovals_RowDataBound"--%>
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Meter Number" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldMeterNo" runat="server" Text='<%#Eval("OldMeterNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Meter Number" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewMeterNo" runat="server" Text='<%#Eval("NewMeterNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Meter Reading" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMeterReading" runat="server" Text='<%#Eval("OldMeterReading") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Meter Reading" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewMeterReading" runat="server" Text='<%#Eval("NewMeterInitialReading") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Meter Change Date" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMeterChangeDate" runat="server" Text='<%#Eval("MeterChangedDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("MeterInfoChangeLogId") %>'
                                                    CommandName="approve" ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>"
                                                    OnClick="lnkMeterApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("MeterInfoChangeLogId") %>'
                                                    CommandName="reject" ForeColor="Green" runat="server" Text="Other" OnClick="lnkMeterOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divAddressApprovalsList" runat="server" visible="false">
                        <div id="div1" class="grid marg_5" runat="server" style="width: 99% !important; float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="litAddress" runat="server" Text="Customer Address Change Logs"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="divAddress" runat="server" align="center" style="float: left;
                            width: 97%; margin-left: 20px;">
                            <asp:GridView ID="gvAddressApprovals" runat="server" AutoGenerateColumns="False"
                                AlternatingRowStyle-CssClass="color" Width="100%" ><%--OnRowDataBound="gvAddressApprovals_RowDataBound"--%>
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Postal Address" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldPAddress" runat="server" Text='<%#Eval("OldPostalAddress") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Postal Address" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewPAddress" runat="server" Text='<%#Eval("NewPostalAddress") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Service Address" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldSAddress" runat="server" Text='<%#Eval("OldServiceAddress") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Service Address" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewSAddress" runat="server" Text='<%#Eval("NewServiceAddress") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("AddressChangeLogId") %>'
                                                    CommandName="approve" ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>"
                                                    OnClick="lnkAddressApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("AddressChangeLogId") %>'
                                                    CommandName="reject" ForeColor="Green" runat="server" Text="Other" OnClick="lnkAddressOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divReadToDirectApprovalsList" runat="server" visible="false">
                        <div id="div10" class="grid marg_5" runat="server" style="width: 99% !important;
                            float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal6" runat="server" Text="Customer Read To Direct Change Logs"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="div11" runat="server" align="center" style="float: left; width: 97%;
                            margin-left: 20px;">
                            <asp:GridView ID="gvReadToDirectCust" runat="server" AutoGenerateColumns="False"
                                AlternatingRowStyle-CssClass="color" Width="100%" ><%--OnRowDataBound="gvStatusApprovals_RowDataBound"--%>
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Meter No" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMeterNo" runat="server" Text='<%#Eval("MeterNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("ReadToDirectId") %>' CommandName="approve"
                                                    ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>" OnClick="lnkReadToDirectApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("ReadToDirectId") %>' CommandName="reject"
                                                    ForeColor="Green" runat="server" Text="Other" OnClick="lnkReadToDirectOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divAssignMeterApprovalsList" runat="server" visible="false">
                        <div id="div13" class="grid marg_5" runat="server" style="width: 99% !important;
                            float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal7" runat="server" Text="Customer Assigned Meter Change Logs"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="div14" runat="server" align="center" style="float: left; width: 97%;
                            margin-left: 20px;">
                            <asp:GridView ID="gvAssignMeter" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%" ><%--OnRowDataBound="gvAssignMeter_RowDataBound"--%>
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Meter No" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMeterNo" runat="server" Text='<%#Eval("MeterNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Initial Reading" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInitialReading" runat="server" Text='<%#Eval("InitialReading") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Meter Assigned Date" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMeterAssignedDate" runat="server" Text='<%#Eval("MeterAssignedDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("AssignedMeterId") %>' CommandName="approve"
                                                    ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>" OnClick="lnkAssignApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("AssignedMeterId") %>' CommandName="reject"
                                                    ForeColor="Green" runat="server" Text="Other" OnClick="lnkAssignOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divBookNoApprovalList" runat="server" visible="false">
                        <div id="div15" class="grid marg_5" runat="server" style="width: 99% !important;
                            float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal8" runat="server" Text="Customer Book No Change Logs"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="div16" runat="server" align="center" style="float: left; width: 97%;
                            margin-left: 20px;">
                            <asp:GridView ID="gvBookNoApproval" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%" ><%--OnRowDataBound="gvBookNoApproval_RowDataBound"--%>
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Postal Address" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldPAddress" runat="server" Text='<%#Eval("OldPostalAddress") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Postal Address" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewPAddress" runat="server" Text='<%#Eval("NewPostalAddress") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Service Address" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldSAddress" runat="server" Text='<%#Eval("OldServiceAddress") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Service Address" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewSAddress" runat="server" Text='<%#Eval("NewServiceAddress") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Old Book Details" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldBook" runat="server" Text='<%#Eval("OldBookNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Book Details" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <div id="lblNewBook" runat="server">
                                                <%#Eval("NewBookNo") %></div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("BookNoChangeLogId") %>'
                                                    CommandName="approve" ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>"
                                                    OnClick="lnkBookApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("BookNoChangeLogId") %>' CommandName="reject"
                                                    ForeColor="Green" runat="server" Text="Other" OnClick="lnkBookOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divMeterReadingApproval" runat="server" visible="false">
                        <div id="div17" class="grid marg_5" runat="server" style="width: 99% !important;
                            float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal9" runat="server" Text="Customer Meter Readings"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="div18" runat="server" align="center" style="float: left; width: 97%;
                            margin-left: 20px;">
                            <asp:GridView ID="gvMeterReadings" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%"><%-- OnRowDataBound="gvMeterReadingsApproval_RowDataBound"--%>
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAccountNumber" runat="server" Text='<%#Eval("AccountNumber") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Meter No" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMeterNo" runat="server" Text='<%#Eval("MeterNo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Previous Reading" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPreviousReading" runat="server" Text='<%#Eval("PreviousReading") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Present Reading" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentReading" runat="server" Text='<%#Eval("PresentReading") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Average Usage" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAverageUsage" runat="server" Text='<%#Eval("AverageUsage") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Usage" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUsage" runat="server" Text='<%#Eval("Usage") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Read Date" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblReadDate" runat="server" Text='<%#Eval("ReadDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Meter Reading From" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMeterReadingFromId" runat="server" Visible="false" Text='<%#Eval("MeterReadingFromId") %>'></asp:Label>
                                            <asp:Label ID="lblMeterReadingFrom" runat="server" Text='<%#Eval("MeterReadingFrom") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsRollover" runat="server" Text='<%#Eval("IsRollOver") %>' Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("MeterReadingLogId") %>'
                                                    CommandName="approve" ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>"
                                                    OnClick="lnkMeterReadingApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("MeterReadingLogId") %>' CommandName="reject"
                                                    ForeColor="Green" runat="server" Text="Other" OnClick="lnkMeterReadingOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divPaymentsApproval" runat="server" visible="false">
                        <div id="div19" class="grid marg_5" runat="server" style="width: 99% !important;
                            float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal10" runat="server" Text="Customer Payments"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="div20" runat="server" align="center" style="float: left; width: 97%;
                            margin-left: 20px;">
                            <asp:GridView ID="gvPayments" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%" OnRowDataBound="gvPaymentsApproval_RowDataBound"><%--OnRowDataBound="gvPaymentsApproval_RowDataBound"--%>
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Receipt Number" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblReceiptNumber" runat="server" Text='<%#Eval("ReceiptNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Payment Mode" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewPAddress" runat="server" Text='<%#Eval("PaymentMode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Paid Amount" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPaidAmount" runat="server" Text='<%#Eval("PaidAmount") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Paid Date" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPaidDate" runat="server" Text='<%#Eval("PaidDate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("PaymentLogId") %>' CommandName="approve"
                                                    ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>" OnClick="lnkPaymentsApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("PaymentLogId") %>' CommandName="reject"
                                                    ForeColor="Green" runat="server" Text="Other" OnClick="lnkPaymentsOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divAvgChangeApproval" runat="server" visible="false">
                        <div id="div21" class="grid marg_5" runat="server" style="width: 99% !important;
                            float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal11" runat="server" Text="Direct Customer Average upload change"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="div22" runat="server" align="center" style="float: left; width: 97%;
                            margin-left: 20px;">
                            <asp:GridView ID="gvAvgChange" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%" OnRowDataBound="gvAvgChange_RowDataBound"><%-- OnRowDataBound="gvAvgChange_RowDataBound"--%>
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Previous Average" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPreviousAverage" runat="server" Text='<%#Eval("PreviousAverage") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Average" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewAverage" runat="server" Text='<%#Eval("NewAverage") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("AverageChangeLogId") %>'
                                                    CommandName="approve" ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>"
                                                    OnClick="lnkAvgChangeApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("AverageChangeLogId") %>'
                                                    CommandName="reject" ForeColor="Green" runat="server" Text="Other" OnClick="lnkAvgChangeOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="divAdjustmentApproval" runat="server" visible="false">
                        <div id="div23" class="grid marg_5" runat="server" style="width: 99% !important;
                            float: left;">
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="Literal12" runat="server" Text="Change Customer Adjustment Approval"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid" id="divAdjustmentApprovalgrid" runat="server" align="center" style="float: left;
                            width: 97%; margin-left: 20px;">
                            <asp:GridView ID="gvAdjustment" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                Width="100%" ><%--OnRowDataBound="gvAdjustment_RowDataBound"--%>
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No pending approvals found for selected Function.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                        HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGlobalACNo" runat="server" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOldAccountNumber" runat="server" Text='<%#Eval("OldAccountNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bill Number" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBillNumber" runat="server" Text='<%#Eval("BillNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bill Adjustment Type Id" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBillAdjustmentTypeId" runat="server" Text='<%#Eval("BillAdjustmentType") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Meter No" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMeterNumber" runat="server" Text='<%#Eval("MeterNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Previous Reading" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPreviousReading" runat="server" Text='<%#Eval("PreviousReading") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Current Reading After Adjustment" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCurrentReadingAfterAdjustment" runat="server" Text='<%#Eval("CurrentReadingAfterAdjustment") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Current Reading Before Adjustment" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCurrentReadingBeforeAdjustment" runat="server" Text='<%#Eval("CurrentReadingBeforeAdjustment") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Adjusted Units" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAdjustedUnits" runat="server" Text='<%#Eval("AdjustedUnits") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tax Effected" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTaxEffected" runat="server" Text='<%#Eval("TaxEffected") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total Amount Effected" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotalAmountEffected" runat="server" Text='<%#Eval("TotalAmountEffected") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Energy Charges" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEnergyCharges" runat="server" Text='<%#Eval("EnergryCharges") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Additional Charges" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAdditionalCharges" runat="server" Text='<%#Eval("AdditionalCharges") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPresentRoleId" runat="server" Text='<%#Eval("PresentApprovalRole") %>'
                                                Visible="false"></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Text='<%#Eval("IsPerformAction") %>'
                                                Visible="false"></asp:Label>
                                            <div id="divAction" runat="server">
                                                <asp:LinkButton ID="lnkApprove" CommandArgument='<%#Eval("AdjustmentLogId") %>' CommandName="approve"
                                                    ForeColor="Green" runat="server" Text="<%$ Resources: Resource, APPROVE %>" OnClick="lnkAdjustmentApprove_Click"></asp:LinkButton>
                                                <span style="margin: 0 5px;">/</span>
                                                <asp:LinkButton ID="lnkOther" CommandArgument='<%#Eval("AdjustmentLogId") %>' CommandName="reject"
                                                    ForeColor="Green" runat="server" Text="Other" OnClick="lnkAdjustmentOther_Click"></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="consumer_total">
                        <div class="pad_10">
                        </div>
                    </div>
                </div>
            </div>
            <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
            </asp:ModalPopupExtender>
            <asp:Button ID="btnActivate" runat="server" Text="Button" CssClass="popbtn" />
            <asp:Panel ID="PanelActivate" runat="server" DefaultButton="btnActiveOk" CssClass="modalPopup"
                class="poppnl" Style="display: none;">
                <div class="popheader popheaderlblgreen" id="pop" runat="server">
                    <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                </div>
                <br />
                <div class="footer popfooterbtnrt">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="btnActiveOk" runat="server" OnClick="btnActiveOk_Click" Text="<%$ Resources:Resource, PROCEED%>"
                            CssClass="ok_btn" />
                        <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                            CssClass="cancel_btn" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
            </asp:ModalPopupExtender>
            <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
            <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                <div class="popheader popheaderlblred">
                    <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                </div>
                <div class="footer popfooterbtnrt">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                            CssClass="ok_btn" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="mpeRequestAction" runat="server" PopupControlID="pnlRequestAction"
                TargetControlID="HiddenField1" BackgroundCssClass="modalBackground" CancelControlID="btnCancel">
            </asp:ModalPopupExtender>
            <asp:HiddenField ID="HiddenField1" runat="server" />
            <asp:Panel ID="pnlRequestAction" runat="server" CssClass="modalPopup" Style="display: none;
                min-width: 362px;" DefaultButton="btnOK">
                <div class="consumer_feild">
                    <div class="Dflt_pop">
                        Request Action</div>
                    <div class="consume_name" style="margin-left: 10px;">
                        <asp:Literal ID="LiteralUnits" runat="server" Text="Select Status"></asp:Literal>
                        <span class="span_star">*</span></div>
                    <div class="consume_input">
                        <asp:DropDownList ID="ddlApprovalStatus" TabIndex="1" runat="server" onchange="DropDownlistOnChangelbl(this,'spanApprovalStatus',' Status')">
                            <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        </asp:DropDownList>
                        <span id="spanApprovalStatus" class="span_color"></span>
                    </div>
                    <div class="clear pad_5">
                    </div>
                    <div class="consume_name" style="margin-left: 10px;">
                        Remarks <span class="span_star">*</span>
                    </div>
                    <div class="consume_input">
                        <asp:TextBox CssClass="text-box" Style="width: 91%;" ID="txtRemarks" runat="server"
                            TextMode="MultiLine" onblur="return TextBoxBlurValidationlbl(this,'spanRemarks','Remarks')"
                            placeholder="Enter Remarks Here"></asp:TextBox>
                        <span id="spanRemarks" class="span_color"></span>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="clear pad_10">
                    </div>
                    <div class="consume_name c_left">
                    </div>
                    <div class="fltl">
                        <asp:Button ID="btnOK" runat="server" TabIndex="2" Text="<%$ Resources:Resource, OK%>"
                            CssClass="ok_btn" OnClientClick="return ValidateRequest()" OnClick="btnActiveOk_Click" />
                        <asp:Button ID="btnCancel" runat="server" TabIndex="3" Text="<%$ Resources:Resource, CANCEL%>"
                            CssClass="ok_btn" />
                    </div>
                    <div class="clear pad_10">
                    </div>
                </div>
            </asp:Panel>
            <asp:HiddenField ID="hfRecognize" runat="server" />
            <asp:HiddenField ID="hfAccountNo" runat="server" />
            <asp:HiddenField ID="hfTariffChangeRequestId" runat="server" />
            <asp:HiddenField ID="hfClusterTypeId" runat="server" />
            <asp:HiddenField ID="hfclassId" runat="server" />
            <asp:HiddenField ID="hfNameChangeLogId" runat="server" />
            <asp:HiddenField ID="hfTypeChangeLogId" runat="server" />
            <asp:HiddenField ID="hfStatusChangeLogId" runat="server" />
            <asp:HiddenField ID="hfAddressChangeLogId" runat="server" />
            <asp:HiddenField ID="hfReadToDirectChangeLogId" runat="server" />
            <asp:HiddenField ID="hfAssignedMeterId" runat="server" />
            <asp:HiddenField ID="hfBookNoChangeLogId" runat="server" />
            <asp:HiddenField ID="hfMeterReadingLogId" runat="server" />
            <asp:HiddenField ID="hfPaymentLogId" runat="server" />
            <asp:HiddenField ID="hfAdjustmentLogId" runat="server" />
            <asp:HiddenField ID="hfAverageChangeLogId" runat="server" />
            <asp:HiddenField ID="hfMeterNumberChangeLogId" runat="server" />
            <asp:HiddenField ID="hfMeterReadingFromId" runat="server" />
            <asp:HiddenField ID="hfIsRollOver" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--==============Ajax loading script starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/ChangesApproval.js" type="text/javascript"></script>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <script type="text/javascript">
        function pageLoad() {
            DisplayMessage('UMSNigeriaBody_pnlMessage');
        }; 
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
