﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using iDSignHelper;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using System.Data;
using System.Web.UI.HtmlControls;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class ViewApproval : System.Web.UI.Page
    {
        # region Members

        iDsignBAL _objiDsignBAL = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        ApprovalBal objApprovalBal = new ApprovalBal();
        TariffManagementBal objTariffBal = new TariffManagementBal();
        ChangeBookNoBal objChangeBookNoBal = new ChangeBookNoBal();
        ChangeCusotmerTypeBAL objChangeCusotmerTypeBAL = new ChangeCusotmerTypeBAL();
        public int PageNum;
        string Key = "CustomerInfoViewApproval";

        #endregion

        #region Properties

        public int Function
        {
            get { return string.IsNullOrEmpty(ddlFunctions.SelectedValue) ? 0 : Convert.ToInt32(ddlFunctions.SelectedValue); }
            set { ddlFunctions.SelectedValue = value.ToString(); }
        }

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                try
                {
                    BindFunctions();
                    _objCommonMethods.BindMoidfyApprovalStatusList(ddlApprovalStatus, UMS_Resource.SELECT);
                }
                catch (Exception ex)
                {
                    _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                    try
                    {
                        _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                    }
                    catch (Exception logex)
                    {

                    }
                }
            }
        }

        protected void ddlFunctions_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divTarrifApprovalsList.Visible = divNameApprovalsList.Visible = divTypeApprovalsList.Visible = divPaymentsApproval.Visible =
                divStatusApprovalList.Visible = divMeterApprovalList.Visible = divAddressApprovalsList.Visible = divMeterReadingApproval.Visible =
                   divAvgChangeApproval.Visible = divReadToDirectApprovalsList.Visible = divAssignMeterApprovalsList.Visible = divBookNoApprovalList.Visible =
                   divAdjustmentApproval.Visible = false;

                if (Function != 0)
                {
                    BindChangeLogInfoGrid();
                }
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                switch (Function)
                {
                    //Customer Tarrif Approval Start
                    case (int)EnumApprovalFunctions.ChangeCustomerTariff:
                        TariffManagementBE objTariffBe = new TariffManagementBE();
                        objTariffBe.NewClassID = Convert.ToInt32(hfclassId.Value);
                        objTariffBe.ClusterTypeId = string.IsNullOrEmpty(hfClusterTypeId.Value) ? 0 : Convert.ToInt32(hfClusterTypeId.Value);
                        objTariffBe.AccountNo = hfAccountNo.Value;
                        objTariffBe.TariffChangeRequestId = Convert.ToInt32(hfTariffChangeRequestId.Value);
                        objTariffBe.Flag = Constants.DeActive; //means From Approval Page
                        objTariffBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objTariffBe.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerTariff;
                        objTariffBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                        if (hfRecognize.Value == UMS_Resource.APPROVED)
                        {
                            hfRecognize.Value = string.Empty;
                            objTariffBe.ApprovalStatusId = Constants.APPROVED;
                            objTariffBe = _objiDsignBAL.DeserializeFromXml<TariffManagementBE>(objTariffBal.Insert(objTariffBe, Statement.Change));
                            if (objTariffBe.IsSuccess)
                            {
                                BindTarrifApprovals();
                                Message(Resource.TARIFF_APPROVED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                            }
                        }
                        else if (hfRecognize.Value == UMS_Resource.REJECTED) //means other than Approve
                        {
                            hfRecognize.Value = string.Empty;
                            objTariffBe.ApprovalStatusId = Convert.ToInt32(ddlApprovalStatus.SelectedValue);
                            objTariffBe.Details = _objiDsignBAL.ReplaceNewLines(txtRemarks.Text, true);
                            objTariffBe = _objiDsignBAL.DeserializeFromXml<TariffManagementBE>(objTariffBal.Insert(objTariffBe, Statement.Change));
                            if (objTariffBe.IsSuccess)
                            {
                                BindTarrifApprovals();
                                Message("Tariff Change Request updated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                                txtRemarks.Text = string.Empty;
                            }
                        }
                        break;
                    //Customer Tarrif Approval End
                    //Customer Name Approval Start
                    case (int)EnumApprovalFunctions.ChangeCustomerName:
                        ChangeBookNoBe objChangeBookNoBe = new ChangeBookNoBe();
                        objChangeBookNoBe.NameChangeLogId = Convert.ToInt32(hfNameChangeLogId.Value);
                        objChangeBookNoBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objChangeBookNoBe.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerName;
                        objChangeBookNoBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                        if (hfRecognize.Value == UMS_Resource.APPROVED)
                        {
                            hfRecognize.Value = string.Empty;
                            objChangeBookNoBe.ApprovalStatusId = Constants.APPROVED;
                            objChangeBookNoBe = _objiDsignBAL.DeserializeFromXml<ChangeBookNoBe>(objChangeBookNoBal.Insert(objChangeBookNoBe, Statement.Modify));
                            if (objChangeBookNoBe.IsSuccess)
                            {
                                BindNameApprovals();
                                Message("Customer Name Change approved successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                            }
                        }
                        else if (hfRecognize.Value == UMS_Resource.REJECTED) //means other than Approve
                        {
                            hfRecognize.Value = string.Empty;
                            objChangeBookNoBe.ApprovalStatusId = Convert.ToInt32(ddlApprovalStatus.SelectedValue);
                            objChangeBookNoBe.Details = _objiDsignBAL.ReplaceNewLines(txtRemarks.Text, true);
                            objChangeBookNoBe = _objiDsignBAL.DeserializeFromXml<ChangeBookNoBe>(objChangeBookNoBal.Insert(objChangeBookNoBe, Statement.Modify));
                            if (objChangeBookNoBe.IsSuccess)
                            {
                                BindNameApprovals();
                                Message("Customer Name Change Request updated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                                txtRemarks.Text = string.Empty;
                            }
                        }
                        break;
                    //Customer Name Approval End
                    //Customer Type Approval Start
                    case (int)EnumApprovalFunctions.CustomerTypeChange:
                        ChangeCustomerTypeBE objChangeCustomerTypeBE = new ChangeCustomerTypeBE();
                        objChangeCustomerTypeBE.CustomerTypeChangeLogId = Convert.ToInt32(hfTypeChangeLogId.Value);
                        objChangeCustomerTypeBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objChangeCustomerTypeBE.FunctionId = (int)EnumApprovalFunctions.CustomerTypeChange;
                        objChangeCustomerTypeBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                        if (hfRecognize.Value == UMS_Resource.APPROVED)
                        {
                            hfRecognize.Value = string.Empty;
                            objChangeCustomerTypeBE.ApprovalStatusId = Constants.APPROVED;
                            objChangeCustomerTypeBE = _objiDsignBAL.DeserializeFromXml<ChangeCustomerTypeBE>(objChangeCusotmerTypeBAL.Insert(objChangeCustomerTypeBE, Statement.Modify));
                            if (objChangeCustomerTypeBE.IsSuccess)
                            {
                                BindTypeApprovals();
                                Message("Customer Type Change approved successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                            }
                        }
                        else if (hfRecognize.Value == UMS_Resource.REJECTED) //means other than Approve
                        {
                            hfRecognize.Value = string.Empty;
                            objChangeCustomerTypeBE.ApprovalStatusId = Convert.ToInt32(ddlApprovalStatus.SelectedValue);
                            objChangeCustomerTypeBE.Reason = _objiDsignBAL.ReplaceNewLines(txtRemarks.Text, true);
                            objChangeCustomerTypeBE = _objiDsignBAL.DeserializeFromXml<ChangeCustomerTypeBE>(objChangeCusotmerTypeBAL.Insert(objChangeCustomerTypeBE, Statement.Modify));
                            if (objChangeCustomerTypeBE.IsSuccess)
                            {
                                BindTypeApprovals();
                                Message("Customer Type Change Request updated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                                txtRemarks.Text = string.Empty;
                            }
                        }
                        break;
                    //Customer Type Approval End
                    //Customer Status Approval Start
                    case (int)EnumApprovalFunctions.ChangeCustomerStatus:
                        ChangeBookNoBe objChangeStatusBE = new ChangeBookNoBe();
                        objChangeStatusBE.ActiveStatusChangeLogId = Convert.ToInt32(hfStatusChangeLogId.Value);
                        objChangeStatusBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objChangeStatusBE.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerStatus;
                        objChangeStatusBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                        if (hfRecognize.Value == UMS_Resource.APPROVED)
                        {
                            hfRecognize.Value = string.Empty;
                            objChangeStatusBE.ApprovalStatusId = Constants.APPROVED;
                            objChangeStatusBE = _objiDsignBAL.DeserializeFromXml<ChangeBookNoBe>(objChangeBookNoBal.Insert(objChangeStatusBE, Statement.Insert));
                            if (objChangeStatusBE.IsSuccess)
                            {
                                BindStatusApprovals();
                                Message("Customer Status Change approved successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                            }
                        }
                        else if (hfRecognize.Value == UMS_Resource.REJECTED) //means other than Approve
                        {
                            hfRecognize.Value = string.Empty;
                            if (!string.IsNullOrEmpty(ddlApprovalStatus.SelectedValue))
                            {
                                objChangeStatusBE.ApprovalStatusId = Convert.ToInt32(ddlApprovalStatus.SelectedValue);
                                objChangeStatusBE.Details = _objiDsignBAL.ReplaceNewLines(txtRemarks.Text, true);
                                objChangeStatusBE = _objiDsignBAL.DeserializeFromXml<ChangeBookNoBe>(objChangeBookNoBal.Insert(objChangeStatusBE, Statement.Insert));
                                if (objChangeStatusBE.IsSuccess)
                                {
                                    BindStatusApprovals();
                                    Message("Customer Status Change Request updated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                                    ddlApprovalStatus.SelectedIndex = Constants.Zero;
                                    txtRemarks.Text = string.Empty;
                                }
                            }
                        }
                        break;
                    //Customer Status Approval End
                    //Customer Meter No Approval Start
                    case (int)EnumApprovalFunctions.ChangeCustomerMeterNo:
                        ChangeBookNoBe objChangeMeterBE = new ChangeBookNoBe();
                        objChangeMeterBE.MeterNumberChangeLogId = Convert.ToInt32(hfMeterNumberChangeLogId.Value);
                        objChangeMeterBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objChangeMeterBE.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerMeterNo;
                        objChangeMeterBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                        if (hfRecognize.Value == UMS_Resource.APPROVED)
                        {
                            hfRecognize.Value = string.Empty;
                            objChangeMeterBE.ApprovalStatusId = Constants.APPROVED;
                            objChangeMeterBE = _objiDsignBAL.DeserializeFromXml<ChangeBookNoBe>(objChangeBookNoBal.Insert(objChangeMeterBE, Statement.Service));
                            if (objChangeMeterBE.IsSuccess)
                            {
                                BindMeterNoApprovals();
                                Message("Customer Meter Number Change approved successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                            }
                        }
                        else if (hfRecognize.Value == UMS_Resource.REJECTED) //means other than Approve
                        {
                            hfRecognize.Value = string.Empty;
                            objChangeMeterBE.ApprovalStatusId = Convert.ToInt32(ddlApprovalStatus.SelectedValue);
                            objChangeMeterBE.Details = _objiDsignBAL.ReplaceNewLines(txtRemarks.Text, true);
                            objChangeMeterBE = _objiDsignBAL.DeserializeFromXml<ChangeBookNoBe>(objChangeBookNoBal.Insert(objChangeMeterBE, Statement.Service));
                            if (objChangeMeterBE.IsSuccess)
                            {
                                BindMeterNoApprovals();
                                Message("Customer Meter Number Change Request updated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                                txtRemarks.Text = string.Empty;
                            }
                        }
                        break;
                    //Customer Meter No End
                    //Customer ReadToDirect No Approval Start
                    case (int)EnumApprovalFunctions.ChangeReadToDirect:
                        ChangeBookNoBe objChangeRTDBE = new ChangeBookNoBe();
                        objChangeRTDBE.ReadToDirectChangeLogId = Convert.ToInt32(hfReadToDirectChangeLogId.Value);
                        objChangeRTDBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objChangeRTDBE.FunctionId = (int)EnumApprovalFunctions.ChangeReadToDirect;
                        objChangeRTDBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                        if (hfRecognize.Value == UMS_Resource.APPROVED)
                        {
                            hfRecognize.Value = string.Empty;
                            objChangeRTDBE.ApprovalStatusId = Constants.APPROVED;
                            objChangeRTDBE = _objiDsignBAL.DeserializeFromXml<ChangeBookNoBe>(objChangeBookNoBal.InsertApprovalDetails(objChangeRTDBE, Statement.Insert));
                            if (objChangeRTDBE.IsSuccess)
                            {
                                BindReadToDirectNoApprovals();
                                Message("Customer Read To Direct Change approved successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                            }
                        }
                        else if (hfRecognize.Value == UMS_Resource.REJECTED) //means other than Approve
                        {
                            hfRecognize.Value = string.Empty;
                            objChangeRTDBE.ApprovalStatusId = Convert.ToInt32(ddlApprovalStatus.SelectedValue);
                            objChangeRTDBE.Details = _objiDsignBAL.ReplaceNewLines(txtRemarks.Text, true);
                            objChangeRTDBE = _objiDsignBAL.DeserializeFromXml<ChangeBookNoBe>(objChangeBookNoBal.InsertApprovalDetails(objChangeRTDBE, Statement.Insert));
                            if (objChangeRTDBE.IsSuccess)
                            {
                                BindReadToDirectNoApprovals();
                                Message("Customer Read To Direct Change Request updated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                                txtRemarks.Text = string.Empty;
                            }
                        }
                        break;
                    //Customer ReadToDirect No End
                    //Customer Address Change Start
                    case (int)EnumApprovalFunctions.ChangeCustomerAddress:
                        ChangeBookNoBe objChangeAddressBE = new ChangeBookNoBe();
                        objChangeAddressBE.AddressChangeLogId = Convert.ToInt32(hfAddressChangeLogId.Value);
                        objChangeAddressBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objChangeAddressBE.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerAddress;
                        objChangeAddressBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                        if (hfRecognize.Value == UMS_Resource.APPROVED)
                        {
                            hfRecognize.Value = string.Empty;
                            objChangeAddressBE.ApprovalStatusId = Constants.APPROVED;
                            objChangeAddressBE = _objiDsignBAL.DeserializeFromXml<ChangeBookNoBe>(objChangeBookNoBal.Insert(objChangeAddressBE, Statement.Generate));
                            if (objChangeAddressBE.IsSuccess)
                            {
                                BindAddressApprovals();
                                Message("Customer Address Change approved successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                            }
                        }
                        else if (hfRecognize.Value == UMS_Resource.REJECTED) //means other than Approve
                        {
                            hfRecognize.Value = string.Empty;
                            objChangeAddressBE.ApprovalStatusId = Convert.ToInt32(ddlApprovalStatus.SelectedValue);
                            objChangeAddressBE.Details = _objiDsignBAL.ReplaceNewLines(txtRemarks.Text, true);
                            objChangeAddressBE = _objiDsignBAL.DeserializeFromXml<ChangeBookNoBe>(objChangeBookNoBal.Insert(objChangeAddressBE, Statement.Generate));
                            if (objChangeAddressBE.IsSuccess)
                            {
                                BindAddressApprovals();
                                Message("Customer Address Change Request updated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                                txtRemarks.Text = string.Empty;
                            }
                        }
                        break;
                    //Customer Address Change End
                    //Customer Assign Meter Change Start
                    case (int)EnumApprovalFunctions.AssignMeter:
                        ChangeCustomerTypeBE objAssignMeterBe = new ChangeCustomerTypeBE();
                        objAssignMeterBe.AssignMeterChangeId = Convert.ToInt32(hfAssignedMeterId.Value);
                        objAssignMeterBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objAssignMeterBe.FunctionId = (int)EnumApprovalFunctions.AssignMeter;
                        objAssignMeterBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                        if (hfRecognize.Value == UMS_Resource.APPROVED)
                        {
                            hfRecognize.Value = string.Empty;
                            objAssignMeterBe.ApprovalStatusId = Constants.APPROVED;
                            objAssignMeterBe = _objiDsignBAL.DeserializeFromXml<ChangeCustomerTypeBE>(objChangeCusotmerTypeBAL.InsertApproveList(objAssignMeterBe, Statement.Insert));
                            if (objAssignMeterBe.IsSuccess)
                            {
                                BindAssignMeterApprovals();
                                Message("Customer Assigned Meter Change approved successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                            }
                        }
                        else if (hfRecognize.Value == UMS_Resource.REJECTED) //means other than Approve
                        {
                            hfRecognize.Value = string.Empty;
                            objAssignMeterBe.ApprovalStatusId = Convert.ToInt32(ddlApprovalStatus.SelectedValue);
                            objAssignMeterBe.Reason = _objiDsignBAL.ReplaceNewLines(txtRemarks.Text, true);
                            objAssignMeterBe = _objiDsignBAL.DeserializeFromXml<ChangeCustomerTypeBE>(objChangeCusotmerTypeBAL.InsertApproveList(objAssignMeterBe, Statement.Insert));
                            if (objAssignMeterBe.IsSuccess)
                            {
                                BindAssignMeterApprovals();
                                Message("Customer Assigned Meter Request updated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                                txtRemarks.Text = string.Empty;
                            }
                        }
                        break;
                    //Customer Assign Meter Change End
                    //Customer Book No Change Start
                    case (int)EnumApprovalFunctions.ChangeCustomerBookNo:
                        ChangeCustomerTypeBE objBookNoBe = new ChangeCustomerTypeBE();
                        objBookNoBe.BookNoChangeLogId = Convert.ToInt32(hfBookNoChangeLogId.Value);
                        objBookNoBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objBookNoBe.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerBookNo;
                        objBookNoBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                        if (hfRecognize.Value == UMS_Resource.APPROVED)
                        {
                            hfRecognize.Value = string.Empty;
                            objBookNoBe.ApprovalStatusId = Constants.APPROVED;
                            objBookNoBe = _objiDsignBAL.DeserializeFromXml<ChangeCustomerTypeBE>(objChangeCusotmerTypeBAL.InsertApproveList(objBookNoBe, Statement.Change));
                            if (objBookNoBe.IsSuccess)
                            {
                                BindBookChangeApprovals();
                                Message("Customer Book No Change approved successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                            }
                        }
                        else if (hfRecognize.Value == UMS_Resource.REJECTED) //means other than Approve
                        {
                            hfRecognize.Value = string.Empty;
                            objBookNoBe.ApprovalStatusId = Convert.ToInt32(ddlApprovalStatus.SelectedValue);
                            objBookNoBe.Reason = _objiDsignBAL.ReplaceNewLines(txtRemarks.Text, true);
                            objBookNoBe = _objiDsignBAL.DeserializeFromXml<ChangeCustomerTypeBE>(objChangeCusotmerTypeBAL.InsertApproveList(objBookNoBe, Statement.Change));
                            if (objBookNoBe.IsSuccess)
                            {
                                BindBookChangeApprovals();
                                Message("Customer Book No Request updated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                                txtRemarks.Text = string.Empty;
                            }
                        }
                        break;
                    //Customer Book No Change End
                    //Customer Meter Reading Start
                    case (int)EnumApprovalFunctions.MeterReadings:
                        ChangeCustomerTypeBE objMeterReadingBe = new ChangeCustomerTypeBE();
                        objMeterReadingBe.MeterReadingLogId = Convert.ToInt32(hfMeterReadingLogId.Value);
                        objMeterReadingBe.MeterReadingFromId = Convert.ToInt32(hfMeterReadingFromId.Value);
                        objMeterReadingBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objMeterReadingBe.FunctionId = (int)EnumApprovalFunctions.MeterReadings;
                        objMeterReadingBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                        objMeterReadingBe.IsRollOver = Convert.ToBoolean(hfIsRollOver.Value);
                        objMeterReadingBe.GlobalAccountNumber = hfAccountNo.Value;

                        if (hfRecognize.Value == UMS_Resource.APPROVED)
                        {
                            hfRecognize.Value = string.Empty;
                            objMeterReadingBe.ApprovalStatusId = Constants.APPROVED;
                            objMeterReadingBe = _objiDsignBAL.DeserializeFromXml<ChangeCustomerTypeBE>(objChangeCusotmerTypeBAL.InsertApproveList(objMeterReadingBe, Statement.Check));
                            if (objMeterReadingBe.IsSuccess)
                            {
                                BindMeterReadingApprovals();
                                Message("Customer Meter Reading approved successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                            }
                        }
                        else if (hfRecognize.Value == UMS_Resource.REJECTED) //means other than Approve
                        {
                            hfRecognize.Value = string.Empty;
                            objMeterReadingBe.ApprovalStatusId = Convert.ToInt32(ddlApprovalStatus.SelectedValue);
                            objMeterReadingBe.Reason = _objiDsignBAL.ReplaceNewLines(txtRemarks.Text, true);
                            objMeterReadingBe = _objiDsignBAL.DeserializeFromXml<ChangeCustomerTypeBE>(objChangeCusotmerTypeBAL.InsertApproveList(objMeterReadingBe, Statement.Check));
                            if (objMeterReadingBe.IsSuccess)
                            {
                                BindMeterReadingApprovals();
                                Message("Customer Meter Reading Request updated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                                txtRemarks.Text = string.Empty;
                            }
                        }
                        break;
                    //Customer Meter Reading End
                    //Customer Payments Start
                    case (int)EnumApprovalFunctions.PaymentEntry:
                        ChangeCustomerTypeBE objPaymentsBe = new ChangeCustomerTypeBE();
                        objPaymentsBe.PaymentLogId = Convert.ToInt32(hfPaymentLogId.Value);
                        objPaymentsBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objPaymentsBe.FunctionId = (int)EnumApprovalFunctions.PaymentEntry;
                        objPaymentsBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                        if (hfRecognize.Value == UMS_Resource.APPROVED)
                        {
                            hfRecognize.Value = string.Empty;
                            objPaymentsBe.ApprovalStatusId = Constants.APPROVED;
                            objPaymentsBe = _objiDsignBAL.DeserializeFromXml<ChangeCustomerTypeBE>(objChangeCusotmerTypeBAL.InsertApproveList(objPaymentsBe, Statement.Delete));
                            if (objPaymentsBe.IsSuccess)
                            {
                                BindPaymentsApprovals();
                                Message("Customer Payment approved successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                            }
                        }
                        else if (hfRecognize.Value == UMS_Resource.REJECTED) //means other than Approve
                        {
                            hfRecognize.Value = string.Empty;
                            objPaymentsBe.ApprovalStatusId = Convert.ToInt32(ddlApprovalStatus.SelectedValue);
                            objPaymentsBe.Reason = _objiDsignBAL.ReplaceNewLines(txtRemarks.Text, true);
                            objPaymentsBe = _objiDsignBAL.DeserializeFromXml<ChangeCustomerTypeBE>(objChangeCusotmerTypeBAL.InsertApproveList(objPaymentsBe, Statement.Delete));
                            if (objPaymentsBe.IsSuccess)
                            {
                                BindPaymentsApprovals();
                                Message("Customer Payment Request updated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                                txtRemarks.Text = string.Empty;
                            }
                        }
                        break;
                    //Customer Payments End
                    //Customer Payments DirectCustomerAverageUpload
                    case (int)EnumApprovalFunctions.DirectCustomerAverageUpload:
                        ChangeCustomerTypeBE objAvgChangeBe = new ChangeCustomerTypeBE();
                        objAvgChangeBe.AverageChangeLogId = Convert.ToInt32(hfAverageChangeLogId.Value);
                        objAvgChangeBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objAvgChangeBe.FunctionId = (int)EnumApprovalFunctions.DirectCustomerAverageUpload;
                        objAvgChangeBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                        if (hfRecognize.Value == UMS_Resource.APPROVED)
                        {
                            hfRecognize.Value = string.Empty;
                            objAvgChangeBe.ApprovalStatusId = Constants.APPROVED;
                            objAvgChangeBe = _objiDsignBAL.DeserializeFromXml<ChangeCustomerTypeBE>(objChangeCusotmerTypeBAL.InsertApproveList(objAvgChangeBe, Statement.Edit));
                            if (objAvgChangeBe.IsSuccess)
                            {
                                BindAvgChangeApprovals();
                                Message("Direct Customer Average upload change approved successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                            }
                        }
                        else if (hfRecognize.Value == UMS_Resource.REJECTED) //means other than Approve
                        {
                            hfRecognize.Value = string.Empty;
                            objAvgChangeBe.ApprovalStatusId = Convert.ToInt32(ddlApprovalStatus.SelectedValue);
                            objAvgChangeBe.Reason = _objiDsignBAL.ReplaceNewLines(txtRemarks.Text, true);
                            objAvgChangeBe = _objiDsignBAL.DeserializeFromXml<ChangeCustomerTypeBE>(objChangeCusotmerTypeBAL.InsertApproveList(objAvgChangeBe, Statement.Edit));
                            if (objAvgChangeBe.IsSuccess)
                            {
                                BindAvgChangeApprovals();
                                Message("Direct Customer Average upload change Request updated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                                txtRemarks.Text = string.Empty;
                            }
                        }
                        break;
                    //Customer Payments DirectCustomerAverageUpload
                    //Customer Adjustments
                    case (int)EnumApprovalFunctions.BillAdjustment:
                        ChangeCustomerTypeBE objAdjustmentsBe = new ChangeCustomerTypeBE();
                        objAdjustmentsBe.AdjustmentLogId = Convert.ToInt32(hfAdjustmentLogId.Value);
                        objAdjustmentsBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objAdjustmentsBe.FunctionId = (int)EnumApprovalFunctions.BillAdjustment;
                        objAdjustmentsBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();

                        if (hfRecognize.Value == UMS_Resource.APPROVED)
                        {
                            hfRecognize.Value = string.Empty;
                            objAdjustmentsBe.ApprovalStatusId = Constants.APPROVED;
                            objAdjustmentsBe = _objiDsignBAL.DeserializeFromXml<ChangeCustomerTypeBE>(objChangeCusotmerTypeBAL.InsertApproveList(objAdjustmentsBe, Statement.Generate));
                            if (objAdjustmentsBe.IsSuccess)
                            {
                                BindAdjustmentApprovals();
                                Message("Bill Adjustment change approved successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                            }
                        }
                        else if (hfRecognize.Value == UMS_Resource.REJECTED) //means other than Approve
                        {
                            hfRecognize.Value = string.Empty;
                            objAdjustmentsBe.ApprovalStatusId = Convert.ToInt32(ddlApprovalStatus.SelectedValue);
                            objAdjustmentsBe.Reason = _objiDsignBAL.ReplaceNewLines(txtRemarks.Text, true);
                            objAdjustmentsBe = _objiDsignBAL.DeserializeFromXml<ChangeCustomerTypeBE>(objChangeCusotmerTypeBAL.InsertApproveList(objAdjustmentsBe, Statement.Generate));
                            if (objAdjustmentsBe.IsSuccess)
                            {
                                BindAdjustmentApprovals();
                                Message("Bill Adjustment change Request updated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                                txtRemarks.Text = string.Empty;
                            }
                        }
                        break;
                    //Customer Adjustments

                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        private void BindFunctions()
        {
            ApprovalBe objApprovalBe = new ApprovalBe();
            objApprovalBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
            objApprovalBe.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
            ApprovalListBe objApprovalList = _objiDsignBAL.DeserializeFromXml<ApprovalListBe>(objApprovalBal.Get(objApprovalBe, ReturnType.Set));
            _objiDsignBAL.FillDropDownList(_objiDsignBAL.ConvertListToDataSet<ApprovalBe>(objApprovalList.items), ddlFunctions, "Function", "FunctionId", UMS_Resource.DROPDOWN_SELECT, false);
        }

        private void BindChangeLogInfoGrid()
        {
            switch (Function)
            {
                case (int)EnumApprovalFunctions.ChangeCustomerTariff:
                    BindTarrifApprovals();
                    break;
                case (int)EnumApprovalFunctions.ChangeCustomerName:
                    BindNameApprovals();
                    break;
                case (int)EnumApprovalFunctions.CustomerTypeChange:
                    BindTypeApprovals();
                    break;
                case (int)EnumApprovalFunctions.ChangeCustomerStatus:
                    BindStatusApprovals();
                    break;
                case (int)EnumApprovalFunctions.ChangeCustomerMeterNo:
                    BindMeterNoApprovals();
                    break;
                case (int)EnumApprovalFunctions.ChangeReadToDirect:
                    BindReadToDirectNoApprovals();
                    break;
                case (int)EnumApprovalFunctions.ChangeCustomerAddress:
                    BindAddressApprovals();
                    break;
                case (int)EnumApprovalFunctions.AssignMeter:
                    BindAssignMeterApprovals();
                    break;
                case (int)EnumApprovalFunctions.ChangeCustomerBookNo:
                    BindBookChangeApprovals();
                    break;
                case (int)EnumApprovalFunctions.MeterReadings:
                    BindMeterReadingApprovals();
                    break;
                case (int)EnumApprovalFunctions.PaymentEntry:
                    BindPaymentsApprovals();
                    break;
                case (int)EnumApprovalFunctions.DirectCustomerAverageUpload:
                    BindAvgChangeApprovals();
                    break;
                case (int)EnumApprovalFunctions.BillAdjustment:
                    BindAdjustmentApprovals();
                    break;

                default:
                    Message("No Approvals for selected Function.", UMS_Resource.MESSAGETYPE_ERROR);
                    break;
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

        #region Function Bindings
        #region Customer Tarrif

        private void BindTarrifApprovals()
        {
            try
            {
                TariffManagementBE objTariffBe = new TariffManagementBE();
                objTariffBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                objTariffBe.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objTariffBe.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerTariff;
                DataSet ds = objTariffBal.GetTariffChangesApproveList(objTariffBe);

                if (ds != null)
                {
                    divTarrifApprovalsList.Visible = true;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvTarrifApprovals.DataSource = ds.Tables[0];
                        gvTarrifApprovals.DataBind();
                    }
                    else
                    {
                        gvTarrifApprovals.DataSource = new DataTable();
                        gvTarrifApprovals.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkApprove_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                GridViewRow row = lnkApprove.NamingContainer as GridViewRow;
                Label lblNewTarrifId = (Label)row.FindControl("lblNewTarrifId");
                Label lblNewClusterTypeId = (Label)row.FindControl("lblNewClusterTypeId");
                Label lblPresentRoleId = (Label)row.FindControl("lblPresentRoleId");
                Label lblGlobalACNo = (Label)row.FindControl("lblGlobalACNo");
                Label lblOriGlobalAccNo = (Label)row.FindControl("lblOriGlobalAccNo");


                hfclassId.Value = lblNewTarrifId.Text;
                hfClusterTypeId.Value = lblNewClusterTypeId.Text;
                hfTariffChangeRequestId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.APPROVED;
                hfAccountNo.Value = lblOriGlobalAccNo.Text;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = Resource.CNF_APPROVED_POPUP_TEXT;
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkReject_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkReject = (LinkButton)sender;
                GridViewRow row = lnkReject.NamingContainer as GridViewRow;
                Label lblNewTarrifId = (Label)row.FindControl("lblNewTarrifId");
                Label lblNewClusterTypeId = (Label)row.FindControl("lblNewClusterTypeId");
                Label lblPresentRoleId = (Label)row.FindControl("lblPresentRoleId");
                Label lblGlobalACNo = (Label)row.FindControl("lblGlobalACNo");
                Label lblOriGlobalAccNo = (Label)row.FindControl("lblOriGlobalAccNo");

                hfclassId.Value = lblNewTarrifId.Text;
                hfClusterTypeId.Value = lblNewClusterTypeId.Text;
                hfTariffChangeRequestId.Value = lnkReject.CommandArgument;
                hfAccountNo.Value = lblOriGlobalAccNo.Text;
                hfRecognize.Value = UMS_Resource.REJECTED;
                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                txtRemarks.Text = string.Empty;
                mpeRequestAction.Show();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvTarrifApprovals_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIsPerformAction = (Label)e.Row.FindControl("lblIsPerformAction");
                    HtmlControl divAction = (HtmlControl)e.Row.FindControl("divAction");
                    if (lblIsPerformAction.Text == "1")
                    {
                        divAction.Visible = true;
                    }
                    else
                    {
                        divAction.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Customer Name

        private void BindNameApprovals()
        {
            try
            {
                ChangeBookNoBe objChangeBookNoBe = new ChangeBookNoBe();
                objChangeBookNoBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                objChangeBookNoBe.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objChangeBookNoBe.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerName;
                DataSet ds = objChangeBookNoBal.GetNameChangesApproveList(objChangeBookNoBe);

                if (ds != null)
                {
                    divNameApprovalsList.Visible = true;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvNameApprovals.DataSource = ds.Tables[0];
                        gvNameApprovals.DataBind();
                    }
                    else
                    {
                        gvNameApprovals.DataSource = new DataTable();
                        gvNameApprovals.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvNameApprovals_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIsPerformAction = (Label)e.Row.FindControl("lblIsPerformAction");
                    HtmlControl divAction = (HtmlControl)e.Row.FindControl("divAction");
                    if (lblIsPerformAction.Text == "1")
                    {
                        divAction.Visible = true;
                    }
                    else
                    {
                        divAction.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkNameApprove_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfNameChangeLogId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.APPROVED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Customer Name Change?";
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkNameOther_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfNameChangeLogId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.REJECTED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Customer Name Change?";
                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                txtRemarks.Text = string.Empty;
                mpeRequestAction.Show();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Customer Type

        private void BindTypeApprovals()
        {
            try
            {
                ChangeCustomerTypeBE objChangeCustomerTypeBE = new ChangeCustomerTypeBE();
                objChangeCustomerTypeBE.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                objChangeCustomerTypeBE.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objChangeCustomerTypeBE.FunctionId = (int)EnumApprovalFunctions.CustomerTypeChange;
                DataSet ds = objChangeCusotmerTypeBAL.GetTypeChangesApproveList(objChangeCustomerTypeBE);

                if (ds != null)
                {
                    divTypeApprovalsList.Visible = true;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvTypeApprovals.DataSource = ds.Tables[0];
                        gvTypeApprovals.DataBind();
                    }
                    else
                    {
                        gvTypeApprovals.DataSource = new DataTable();
                        gvTypeApprovals.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvTypeApprovals_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIsPerformAction = (Label)e.Row.FindControl("lblIsPerformAction");
                    HtmlControl divAction = (HtmlControl)e.Row.FindControl("divAction");
                    if (lblIsPerformAction.Text == "1")
                    {
                        divAction.Visible = true;
                    }
                    else
                    {
                        divAction.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkTypeApprove_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfTypeChangeLogId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.APPROVED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Customer Type Change?";
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkTypeOther_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfTypeChangeLogId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.REJECTED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Customer Type Change?";
                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                txtRemarks.Text = string.Empty;
                mpeRequestAction.Show();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Customer Address

        private void BindAddressApprovals()
        {
            try
            {
                ChangeCustomerTypeBE objChangeCustomerTypeBE = new ChangeCustomerTypeBE();
                objChangeCustomerTypeBE.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                objChangeCustomerTypeBE.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objChangeCustomerTypeBE.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerAddress;
                DataSet ds = objChangeCusotmerTypeBAL.GetBindApproveList(objChangeCustomerTypeBE, ReturnType.Get);

                if (ds != null)
                {
                    divAddressApprovalsList.Visible = true;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvAddressApprovals.DataSource = ds.Tables[0];
                        gvAddressApprovals.DataBind();
                    }
                    else
                    {
                        gvAddressApprovals.DataSource = new DataTable();
                        gvAddressApprovals.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvAddressApprovals_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIsPerformAction = (Label)e.Row.FindControl("lblIsPerformAction");
                    HtmlControl divAction = (HtmlControl)e.Row.FindControl("divAction");
                    if (lblIsPerformAction.Text == "1")
                    {
                        divAction.Visible = true;
                    }
                    else
                    {
                        divAction.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkAddressApprove_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfAddressChangeLogId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.APPROVED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Customer Address Changes?";
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkAddressOther_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfAddressChangeLogId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.REJECTED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Customer Address Changes?";
                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                txtRemarks.Text = string.Empty;
                mpeRequestAction.Show();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Customer Status

        private void BindStatusApprovals()
        {
            try
            {
                ChangeBookNoBe objChangeBookNoBe = new ChangeBookNoBe();
                objChangeBookNoBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                objChangeBookNoBe.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objChangeBookNoBe.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerStatus;

                DataSet ds = objChangeBookNoBal.GetStatusChangesApproveList(objChangeBookNoBe);

                if (ds != null)
                {
                    divStatusApprovalList.Visible = true;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvStatusApprovals.DataSource = ds.Tables[0];
                        gvStatusApprovals.DataBind();
                    }
                    else
                    {
                        gvStatusApprovals.DataSource = new DataTable();
                        gvStatusApprovals.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvStatusApprovals_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIsPerformAction = (Label)e.Row.FindControl("lblIsPerformAction");
                    HtmlControl divAction = (HtmlControl)e.Row.FindControl("divAction");
                    if (lblIsPerformAction.Text == "1")
                    {
                        divAction.Visible = true;
                    }
                    else
                    {
                        divAction.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkStatusApprove_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfStatusChangeLogId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.APPROVED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Customer Staus Change?";
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkStatusOther_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfStatusChangeLogId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.REJECTED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Customer Status Change?";
                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                txtRemarks.Text = string.Empty;
                mpeRequestAction.Show();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Customer Meter Number

        private void BindMeterNoApprovals()
        {
            try
            {
                ChangeBookNoBe objChangeBookNoBe = new ChangeBookNoBe();
                objChangeBookNoBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                objChangeBookNoBe.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objChangeBookNoBe.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerMeterNo;
                DataSet ds = objChangeBookNoBal.GetMeterNoChangesApproveList(objChangeBookNoBe);

                if (ds != null)
                {
                    divMeterApprovalList.Visible = true;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvMeterApprovals.DataSource = ds.Tables[0];
                        gvMeterApprovals.DataBind();
                    }
                    else
                    {
                        gvMeterApprovals.DataSource = new DataTable();
                        gvMeterApprovals.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvMeterApprovals_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIsPerformAction = (Label)e.Row.FindControl("lblIsPerformAction");
                    HtmlControl divAction = (HtmlControl)e.Row.FindControl("divAction");
                    if (lblIsPerformAction.Text == "1")
                    {
                        divAction.Visible = true;
                    }
                    else
                    {
                        divAction.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkMeterApprove_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfMeterNumberChangeLogId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.APPROVED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Customer Meter Number Change?";
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkMeterOther_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfMeterNumberChangeLogId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.REJECTED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Customer Meter Number Change?";
                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                txtRemarks.Text = string.Empty;
                mpeRequestAction.Show();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Customer ReadToDirect

        private void BindReadToDirectNoApprovals()
        {
            try
            {
                ChangeCustomerTypeBE objChangeBookNoBe = new ChangeCustomerTypeBE();
                objChangeBookNoBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                objChangeBookNoBe.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objChangeBookNoBe.FunctionId = (int)EnumApprovalFunctions.ChangeReadToDirect;
                DataSet ds = objChangeCusotmerTypeBAL.GetBindApproveList(objChangeBookNoBe, ReturnType.Bulk);

                if (ds != null)
                {
                    divReadToDirectApprovalsList.Visible = true;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvReadToDirectCust.DataSource = ds.Tables[0];
                        gvReadToDirectCust.DataBind();
                    }
                    else
                    {
                        gvReadToDirectCust.DataSource = new DataTable();
                        gvReadToDirectCust.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvReadToDirectCust_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIsPerformAction = (Label)e.Row.FindControl("lblIsPerformAction");
                    HtmlControl divAction = (HtmlControl)e.Row.FindControl("divAction");
                    if (lblIsPerformAction.Text == "1")
                    {
                        divAction.Visible = true;
                    }
                    else
                    {
                        divAction.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkReadToDirectApprove_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfReadToDirectChangeLogId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.APPROVED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Customer Read To Direct?";
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkReadToDirectOther_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfReadToDirectChangeLogId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.REJECTED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Customer  Read To Direct?";
                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                txtRemarks.Text = string.Empty;
                mpeRequestAction.Show();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Assign Meter

        private void BindAssignMeterApprovals()
        {
            try
            {
                ChangeCustomerTypeBE objChangeCustomerTypeBE = new ChangeCustomerTypeBE();
                objChangeCustomerTypeBE.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                objChangeCustomerTypeBE.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objChangeCustomerTypeBE.FunctionId = (int)EnumApprovalFunctions.AssignMeter;
                DataSet ds = objChangeCusotmerTypeBAL.GetBindApproveList(objChangeCustomerTypeBE, ReturnType.Check);

                if (ds != null)
                {
                    divAssignMeterApprovalsList.Visible = true;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvAssignMeter.DataSource = ds.Tables[0];
                        gvAssignMeter.DataBind();
                    }
                    else
                    {
                        gvAssignMeter.DataSource = new DataTable();
                        gvAssignMeter.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvAssignMeter_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIsPerformAction = (Label)e.Row.FindControl("lblIsPerformAction");
                    HtmlControl divAction = (HtmlControl)e.Row.FindControl("divAction");
                    if (lblIsPerformAction.Text == "1")
                    {
                        divAction.Visible = true;
                    }
                    else
                    {
                        divAction.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkAssignApprove_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfAssignedMeterId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.APPROVED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Assign Meter Change?";
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkAssignOther_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfAssignedMeterId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.REJECTED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Assign Meter Change?";
                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                txtRemarks.Text = string.Empty;
                mpeRequestAction.Show();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Book No Change

        private void BindBookChangeApprovals()
        {
            try
            {
                ChangeCustomerTypeBE objChangeCustomerTypeBE = new ChangeCustomerTypeBE();
                objChangeCustomerTypeBE.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                objChangeCustomerTypeBE.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objChangeCustomerTypeBE.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerBookNo;
                DataSet ds = objChangeCusotmerTypeBAL.GetBindApproveList(objChangeCustomerTypeBE, ReturnType.Data);

                if (ds != null)
                {
                    divBookNoApprovalList.Visible = true;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvBookNoApproval.DataSource = ds.Tables[0];
                        gvBookNoApproval.DataBind();
                    }
                    else
                    {
                        gvBookNoApproval.DataSource = new DataTable();
                        gvBookNoApproval.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvBookNoApproval_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIsPerformAction = (Label)e.Row.FindControl("lblIsPerformAction");
                    HtmlControl divAction = (HtmlControl)e.Row.FindControl("divAction");
                    if (lblIsPerformAction.Text == "1")
                    {
                        divAction.Visible = true;
                    }
                    else
                    {
                        divAction.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkBookApprove_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfBookNoChangeLogId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.APPROVED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Book No Change?";
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkBookOther_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfBookNoChangeLogId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.REJECTED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Book No Change?";
                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                txtRemarks.Text = string.Empty;
                mpeRequestAction.Show();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region MeterReading

        private void BindMeterReadingApprovals()
        {
            try
            {
                ChangeCustomerTypeBE objChangeCustomerTypeBE = new ChangeCustomerTypeBE();
                objChangeCustomerTypeBE.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                objChangeCustomerTypeBE.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objChangeCustomerTypeBE.FunctionId = (int)EnumApprovalFunctions.MeterReadings;
                DataSet ds = objChangeCusotmerTypeBAL.GetBindApproveList(objChangeCustomerTypeBE, ReturnType.Double);

                if (ds != null)
                {
                    divMeterReadingApproval.Visible = true;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvMeterReadings.DataSource = ds.Tables[0];
                        gvMeterReadings.DataBind();
                        MergeMeterReadingsRows();
                    }
                    else
                    {
                        gvMeterReadings.DataSource = new DataTable();
                        gvMeterReadings.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvMeterReadingsApproval_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIsPerformAction = (Label)e.Row.FindControl("lblIsPerformAction");
                    HtmlControl divAction = (HtmlControl)e.Row.FindControl("divAction");
                    if (lblIsPerformAction.Text == "1")
                    {
                        divAction.Visible = true;
                    }
                    else
                    {
                        divAction.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void MergeMeterReadingsRows()
        {
            for (int rowIndex = gvMeterReadings.Rows.Count - 2; rowIndex >= 0; rowIndex--)
            {
                GridViewRow row = gvMeterReadings.Rows[rowIndex];
                GridViewRow previousRow = gvMeterReadings.Rows[rowIndex + 1];

                Label lblIsRollover = (Label)row.FindControl("lblIsRollover");
                Label lblPreviousIsRollover = (Label)previousRow.FindControl("lblIsRollover");

                Label lblGlobalACNo = (Label)row.FindControl("lblGlobalACNo");
                Label lblPreviousGlobalACNo = (Label)previousRow.FindControl("lblGlobalACNo");

                Label lblOldAccountNumber = (Label)row.FindControl("lblOldAccountNumber");
                Label lblPreviousOldAccountNumber = (Label)previousRow.FindControl("lblOldAccountNumber");

                Label lblName = (Label)row.FindControl("lblName");
                Label lblPreviousName = (Label)previousRow.FindControl("lblName");

                Label lblMeterNo = (Label)row.FindControl("lblMeterNo");
                Label lblPreviousMeterNo = (Label)previousRow.FindControl("lblMeterNo");

                Label lblUsage = (Label)row.FindControl("lblUsage");
                Label lblPreviousUsage = (Label)previousRow.FindControl("lblUsage");

                for (int i = 0; i < row.Cells.Count; i++)
                {
                    if (Convert.ToBoolean(lblIsRollover.Text) && Convert.ToBoolean(lblPreviousIsRollover.Text))
                    {
                        if (lblGlobalACNo.Text == lblPreviousGlobalACNo.Text)
                        {
                            if (i != 0 && i != 5 && i != 6)//For SNo, previous Reading & Present Reading
                            {
                                row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                                previousRow.Cells[i].Visible = false;
                            }
                            if (i == 8)//For Usage
                            {
                                if (!string.IsNullOrEmpty(lblUsage.Text) && !string.IsNullOrEmpty(lblPreviousUsage.Text))
                                {
                                    lblUsage.Text = (Convert.ToInt64(lblUsage.Text) + Convert.ToInt64(lblPreviousUsage.Text)).ToString();
                                }
                            }
                        }
                    }
                    if (lblGlobalACNo.Text == lblPreviousGlobalACNo.Text)
                    {
                        if (i == 1)//For Global Account No
                        {
                            row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                            previousRow.Cells[i].Visible = false;
                        }
                        if (lblOldAccountNumber.Text == lblPreviousOldAccountNumber.Text)
                        {
                            if (i == 2)//For Account No
                            {
                                row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                                previousRow.Cells[i].Visible = false;
                            }
                        }
                        if (lblName.Text == lblPreviousName.Text)
                        {
                            if (i == 3)//For Name
                            {
                                row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                                previousRow.Cells[i].Visible = false;
                            }
                        }
                        if (lblMeterNo.Text == lblPreviousMeterNo.Text)
                        {
                            if (i == 4)//For Meter no
                            {
                                row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                                previousRow.Cells[i].Visible = false;
                            }
                        }
                    }
                }
            }
        }

        protected void lnkMeterReadingApprove_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                Label lblIsRollover = (Label)lnkApprove.Parent.FindControl("lblIsRollover");
                Label lblMeterReadingFromId = (Label)lnkApprove.Parent.FindControl("lblMeterReadingFromId");
                Label lblAccountNumber = (Label)lnkApprove.Parent.FindControl("lblAccountNumber");

                if (IsValidApproval(lblAccountNumber.Text))
                {
                    hfAccountNo.Value = lblAccountNumber.Text;
                    hfMeterReadingFromId.Value = lblMeterReadingFromId.Text;
                    hfIsRollOver.Value = lblIsRollover.Text;
                    hfMeterReadingLogId.Value = lnkApprove.CommandArgument;
                    hfRecognize.Value = UMS_Resource.APPROVED;
                    pop.Attributes.Add("class", "popheader popheaderlblgreen");
                    lblActiveMsg.Text = "Are you sure you want to approve this Meter Reading?";
                    mpeActivate.Show();
                    btnActiveOk.Focus();
                }
                else
                {
                    pop.Attributes.Add("class", "popheader popheaderlblred");
                    lblgridalertmsg.Text = "Already Readings available for this Customer. You can not Approve the Readings of this Customer.";
                    mpegridalert.Show();
                }
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkMeterReadingOther_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                Label lblIsRollover = (Label)lnkApprove.Parent.FindControl("lblIsRollover");
                Label lblMeterReadingFromId = (Label)lnkApprove.Parent.FindControl("lblMeterReadingFromId");
                Label lblAccountNumber = (Label)lnkApprove.Parent.FindControl("lblAccountNumber");

                hfAccountNo.Value = lblAccountNumber.Text;
                hfMeterReadingFromId.Value = lblMeterReadingFromId.Text;
                hfIsRollOver.Value = lblIsRollover.Text;
                hfMeterReadingLogId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.REJECTED;
                //pop.Attributes.Add("class", "popheader popheaderlblgreen");
                //lblActiveMsg.Text = "Are you sure you want to approve this Meter Reading?";
                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                txtRemarks.Text = string.Empty;
                mpeRequestAction.Show();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private bool IsValidApproval(string AccountNo)
        {
            bool isProcess = false;
            ChangeCustomerTypeBE objMeterReadingBe = new ChangeCustomerTypeBE();
            objMeterReadingBe.GlobalAccountNumber = AccountNo;

            objMeterReadingBe = _objiDsignBAL.DeserializeFromXml<ChangeCustomerTypeBE>(objChangeCusotmerTypeBAL.Get(objMeterReadingBe, ReturnType.Check));
            isProcess = objMeterReadingBe.IsApprovalInProcess;
            return isProcess;
        }

        #endregion

        #region Payments

        private void BindPaymentsApprovals()
        {
            try
            {
                ChangeCustomerTypeBE objChangeCustomerTypeBE = new ChangeCustomerTypeBE();
                objChangeCustomerTypeBE.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                objChangeCustomerTypeBE.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objChangeCustomerTypeBE.FunctionId = (int)EnumApprovalFunctions.PaymentEntry;
                DataSet ds = objChangeCusotmerTypeBAL.GetBindApproveList(objChangeCustomerTypeBE, ReturnType.Fetch);

                if (ds != null)
                {
                    divPaymentsApproval.Visible = true;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvPayments.DataSource = ds.Tables[0];
                        gvPayments.DataBind();
                    }
                    else
                    {
                        gvPayments.DataSource = new DataTable();
                        gvPayments.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvPaymentsApproval_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIsPerformAction = (Label)e.Row.FindControl("lblIsPerformAction");
                    HtmlControl divAction = (HtmlControl)e.Row.FindControl("divAction");
                    if (lblIsPerformAction.Text == "1")
                    {
                        divAction.Visible = true;
                    }
                    else
                    {
                        divAction.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkPaymentsApprove_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfPaymentLogId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.APPROVED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Payment?";
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkPaymentsOther_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfPaymentLogId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.REJECTED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Payment?";
                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                txtRemarks.Text = string.Empty;
                mpeRequestAction.Show();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region AverageChange

        private void BindAvgChangeApprovals()
        {
            try
            {
                ChangeCustomerTypeBE objChangeCustomerTypeBE = new ChangeCustomerTypeBE();
                objChangeCustomerTypeBE.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                objChangeCustomerTypeBE.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objChangeCustomerTypeBE.FunctionId = (int)EnumApprovalFunctions.DirectCustomerAverageUpload;
                DataSet ds = objChangeCusotmerTypeBAL.GetBindApproveList(objChangeCustomerTypeBE, ReturnType.Group);

                if (ds != null)
                {
                    divAvgChangeApproval.Visible = true;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvAvgChange.DataSource = ds.Tables[0];
                        gvAvgChange.DataBind();
                    }
                    else
                    {
                        gvAvgChange.DataSource = new DataTable();
                        gvAvgChange.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvAvgChange_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIsPerformAction = (Label)e.Row.FindControl("lblIsPerformAction");
                    HtmlControl divAction = (HtmlControl)e.Row.FindControl("divAction");
                    if (lblIsPerformAction.Text == "1")
                    {
                        divAction.Visible = true;
                    }
                    else
                    {
                        divAction.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkAvgChangeApprove_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfAverageChangeLogId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.APPROVED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Direct Customer Average Change?";
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkAvgChangeOther_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfAverageChangeLogId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.REJECTED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Direct Customer Average Change?";
                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                txtRemarks.Text = string.Empty;
                mpeRequestAction.Show();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Bill Adjustments

        private void BindAdjustmentApprovals()
        {
            try
            {
                ChangeCustomerTypeBE objChangeCustomerTypeBE = new ChangeCustomerTypeBE();
                objChangeCustomerTypeBE.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                objChangeCustomerTypeBE.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objChangeCustomerTypeBE.FunctionId = (int)EnumApprovalFunctions.BillAdjustment;
                DataSet ds = objChangeCusotmerTypeBAL.GetBindApproveList(objChangeCustomerTypeBE, ReturnType.List);

                if (ds != null)
                {
                    divAdjustmentApproval.Visible = true;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvAdjustment.DataSource = ds.Tables[0];
                        gvAdjustment.DataBind();
                    }
                    else
                    {
                        gvAdjustment.DataSource = new DataTable();
                        gvAdjustment.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvAdjustment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIsPerformAction = (Label)e.Row.FindControl("lblIsPerformAction");
                    HtmlControl divAction = (HtmlControl)e.Row.FindControl("divAction");
                    if (lblIsPerformAction.Text == "1")
                    {
                        divAction.Visible = true;
                    }
                    else
                    {
                        divAction.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkAdjustmentApprove_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfAdjustmentLogId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.APPROVED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Bill Adjustment Change?";
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkAdjustmentOther_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfAdjustmentLogId.Value = lnkApprove.CommandArgument;
                hfRecognize.Value = UMS_Resource.REJECTED;
                pop.Attributes.Add("class", "popheader popheaderlblgreen");
                lblActiveMsg.Text = "Are you sure you want to approve this Bill Adjustment Change?";
                ddlApprovalStatus.SelectedIndex = Constants.Zero;
                txtRemarks.Text = string.Empty;
                mpeRequestAction.Show();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #endregion Function Bindings
    }
}