﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for ViewNewCustomersLocator
                     
 Developer        : Neeraj.K
 Creation Date    : 18-Sep-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using Resources;
using UMS_NigriaDAL;
using System.Data;
using iDSignHelper;
using UMS_NigeriaBAL;
using System.Drawing;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Globalization;

namespace UMS_Nigeria.ConsumerManagement
{
    public partial class ViewNewCustomersLocator : System.Web.UI.Page
    {
        #region Members
        CommonMethods _ObjCommonMethods = new CommonMethods();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CustomerDetailsBAL _ObjCustomerDetailsBAL = new CustomerDetailsBAL();
        NewCustomerLocatorBE _objNewCustomerLocatorBE = new NewCustomerLocatorBE();
        NewCustomerLocatorListBE _objNewCustomerLocatorListBE = new NewCustomerLocatorListBE();
        int PageNum;
        string Key = UMS_Resource.ADVANCEDSEARCHCUSTOMER;
        #endregion

        #region Properties

        public int PageSize
        {
            get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                bindNewCustomerLocatorBE();
                BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
        }

        protected void btnApproveOk_Click(object sender, EventArgs e)
        {
            ApproveCustomer(hfNewCustomerLocator.Value);
            bindNewCustomerLocatorBE();
        }

        protected void gvSearchCustomers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "approve")
            {
                hfNewCustomerLocator.Value = e.CommandArgument.ToString();
                lblApprovalConfirmation.Text = Resource.ARE_SURE_TO_APPRV_NEW_CUST_LCTR;
                mpeApprovalConfirmation.Show();
            }
        }
        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void bindNewCustomerLocatorBE()
        {
            try
            {
                _objNewCustomerLocatorBE.PageSize = PageSize;
                _objNewCustomerLocatorBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    _objNewCustomerLocatorBE.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    _objNewCustomerLocatorBE.BUID = string.Empty;
                _objNewCustomerLocatorListBE = _ObjiDsignBAL.DeserializeFromXml<NewCustomerLocatorListBE>(_ObjCustomerDetailsBAL.GetCustLocatorForApprovalBAL(_objNewCustomerLocatorBE));
                if (_objNewCustomerLocatorListBE != null && _objNewCustomerLocatorListBE.Items.Count > 0)
                {
                    hfTotalRecords.Value = _objNewCustomerLocatorListBE.Items[0].TotalRecords.ToString();
                    //gvSearchCustomers.DataSource = _ObjiDsignBAL.ConvertListToDataSet<ConsumerBe>(_objlist.items).Tables[0];
                    gvSearchCustomers.DataSource = _objNewCustomerLocatorListBE.Items;
                    gvSearchCustomers.DataBind();
                    divpaging.Visible = true;
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                }
                else
                {
                    gvSearchCustomers.DataSource = new DataTable();
                    gvSearchCustomers.DataBind();
                    divpaging.Visible = false;
                    hfTotalRecords.Value = Constants.Zero.ToString();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        public void ApproveCustomer(string NewCustomerLocatorId)
        {
            _objNewCustomerLocatorBE.NewCustomerLocatorId = Convert.ToInt32(NewCustomerLocatorId);
            _objNewCustomerLocatorBE.PageNo = Convert.ToInt32(hfPageNo.Value);
            _objNewCustomerLocatorBE = _ObjiDsignBAL.DeserializeFromXml<NewCustomerLocatorBE>(_ObjCustomerDetailsBAL.ApproveCustLocatorBAL(_objNewCustomerLocatorBE));
            if (_objNewCustomerLocatorBE != null && _objNewCustomerLocatorBE.RowEffected > 0)
            {
                Message(Resource.CST_SUCCES_APPROVED, UMS_Resource.MESSAGETYPE_SUCCESS);
            }
            else
            {
                Message(Resource.CST_NOT_APRVD, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        #endregion

        #region Paging

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                _ObjiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; }
                else
                {
                    lkbtnNext.Enabled = lkbtnLast.Enabled = true;
                    lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5");
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                bindNewCustomerLocatorBE();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                bindNewCustomerLocatorBE();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                bindNewCustomerLocatorBE();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                bindNewCustomerLocatorBE();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                bindNewCustomerLocatorBE();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                _ObjCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion
    }
}