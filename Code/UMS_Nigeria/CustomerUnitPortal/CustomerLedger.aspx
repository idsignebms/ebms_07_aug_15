﻿<%@ Page Title=":: Customer Ledger ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="CustomerLedger.aspx.cs" Inherits="UMS_Nigeria.CustomerUnitPortal.CustomerLedger" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="contentBody" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upSearchCustomer" DefaultButton="btnSearch" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litSearchCustomer" runat="server" Text="Customer Ledger Report"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                                <asp:Literal ID="Literal7" runat="server" Text="/ Old Account No"></asp:Literal>
                                <span class="span_star">*</span><br />
                                <asp:TextBox ID="txtAccountNo" runat="server" MaxLength="50" onchange="GetRecords()"
                                    placeholder="Account No" CssClass="text-box"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender37" runat="server" TargetControlID="txtAccountNo"
                                        FilterType="Numbers">
                                    </asp:FilteredTextBoxExtender>
                                <br />
                                <span id="spanAccountNo" class="spancolor"></span>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <asp:Button ID="btnGo" OnClientClick="return Validate();" Text="<%$ Resources:Resource, GET_CUSTOMER_DETAILS%>"
                                OnClick="btnGo_Click" CssClass="box_s" runat="server" />
                        </div>
                    </div>
                    <div class="grid_boxes">
                        <div class="grid_paging_top">
                            <div id="divgrid" runat="server">
                                <div id="divpaging" runat="server">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="litBuList" runat="server" Text="Transactions List"></asp:Literal>
                                    </div>
                                    <div style="font-size: 13px;">
                                        <div class="consume_name c_left" style="text-align: left; float: left;">
                                            <asp:Label ID="lblheader" runat="server" Font-Bold="true" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Label>
                                            -
                                            <asp:Label ID="lblAccountNo" runat="server" Text="---"></asp:Label>
                                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                                        </div>
                                        <div style="text-align: left; float: left;" class="consume_name c_left">
                                            <asp:Label ID="lbl1" runat="server" Font-Bold="true" Text="<%$ Resources:Resource, NAME%>"></asp:Label>
                                            -
                                            <asp:Label ID="lblName" runat="server" Text="---"></asp:Label>
                                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                                        </div>
                                        <div style="text-align: left; float: left;" class="consume_name c_left">
                                            <asp:Label ID="lbl2" runat="server" Font-Bold="true" Text="<%$ Resources:Resource, METER_NO%>"></asp:Label>
                                            -
                                            <asp:Label ID="lblMeterNo" runat="server" Text="---"></asp:Label>
                                            &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                                        </div>
                                        <div class="consume_name c_left" style="text-align: left; float: left;">
                                            <asp:Label ID="lbl3" runat="server" Font-Bold="true" Text="<%$ Resources:Resource, TARIFF%>"></asp:Label>
                                            -
                                            <asp:Label ID="lblTariff" runat="server" Text="---"></asp:Label>
                                        </div>
                                        <div class="clear pad_5">
                                        </div>
                                        <div class="consume_name c_left" style="text-align: left; float: left;">
                                            <asp:Label ID="Labeladdress" runat="server" Font-Bold="true" Text="<%$ Resources:Resource, ADDRESS%>"></asp:Label>
                                            -
                                            <asp:Label ID="lblAddress" runat="server" Text="---"></asp:Label>
                                        </div>
                                    </div>
                                    <div align="right" class="fltr" runat="server" id="divExport" visible="true">
                                        <asp:Button ID="btnExportExcel" OnClick="btnExportExcel_Click" runat="server" CssClass="excel"
                                            Text="Export to excel" /></div>
                                </div>
                            </div>
                            <%-- <div class="clear pad_10">
                        </div>--%>
                        </div>
                    </div>
                    <div class="grid_tb" id="divPrint" runat="server" align="center" style="overflow-x: auto;">
                        <asp:GridView ID="gvCustomerLedger" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                            ShowFooter="true" OnRowDataBound="gvCustomerLedger_RowDataBound" HeaderStyle-CssClass="grid_tb_hd">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="S.No">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Date" DataField="TransactionDate" />
                                <asp:TemplateField HeaderText="Reference No" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkReferenceNo" runat="server" CommandArgument='<%#Eval("ReferenceNo") %>'
                                            Text='<%#Eval("ReferenceNo") %>' OnClick="lnkReferenceNo_Click"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Description" DataField="Particulars" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField HeaderText="Remarks" DataField="Remarks" ItemStyle-HorizontalAlign="Left" />
                                <asp:TemplateField HeaderText="Debit" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCredit" runat="server" Text='<%#Eval("Credit") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Credit" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDebit" runat="server" Text='<%#Eval("Debit") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Balance" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBalance" runat="server" Text='<%#Eval("Balance") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle HorizontalAlign="center"></RowStyle>
                            <FooterStyle HorizontalAlign="Right" Font-Bold="true" />
                        </asp:GridView>
                    </div>
                    <asp:HiddenField ID="hfTotalCredit" runat="server" />
                    <asp:HiddenField ID="hfTotalDebit" runat="server" />
                    <%-- <asp:HiddenField ID="hfPageSize" runat="server" />
                <asp:HiddenField ID="hfPageNo" runat="server" />
                <asp:HiddenField ID="hfLastPage" runat="server" />
                <asp:HiddenField ID="hfTotalRecords" runat="server" />--%>
                    <%-- Popup --%>
                    <asp:ModalPopupExtender ID="mpeRefDetails" runat="server" TargetControlID="btnpopup"
                        PopupControlID="pnlChangePwd" BackgroundCssClass="popbg">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnpopup" runat="server" Text="" Style="display: none;" />
                    <asp:Panel ID="pnlChangePwd" runat="server" CssClass="editpopup_two" Style="display: none;
                        width: 720px; left: 259.5px; position: fixed; z-index: 100001; top: 111.5px;">
                        <h3 class="edithead" align="left" style="background: none repeat scroll 0 0 #008000;
                            border-bottom: 1px solid #CCCCCC; color: #FFFFFF; margin-bottom: 20px; padding: 10px 10px 8px 14px;">
                            <asp:Literal ID="Literal26" runat="server" Text="Bill Details"></asp:Literal>
                            &nbsp;:&nbsp;
                            <asp:Label ID="lblBillId" runat="server"></asp:Label>
                        </h3>
                        <div class="adjust-meter">
                            <ul style="width: 357px; float: left;">
                                <li class="prevleft fltl">
                                    <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal></li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <asp:Label runat="server" ID="lblPopAccNo"></asp:Label>
                                </li>
                                <li class="clear pad_5"></li>
                                <li class="prevleft fltl">
                                    <asp:Literal ID="Literal19" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal></li><li
                                        class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <asp:Label runat="server" ID="lblPopFullName"></asp:Label>
                                </li>
                                <li class="clear pad_5"></li>
                                <li class="prevleft fltl">
                                    <asp:Literal ID="Literal25" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT_NAME%>"></asp:Literal>
                                </li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <asp:Label ID="lblPopBU" runat="server"></asp:Label></li>
                                <li class="clear pad_5"></li>
                                <li class="prevleft fltl">
                                    <asp:Literal ID="Literal37" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT_NAME%>"></asp:Literal>
                                </li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <asp:Label ID="lblPopSU" runat="server"></asp:Label></li>
                                <li class="clear pad_5"></li>
                                <li class="prevleft fltl">
                                    <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER_NAME%>"></asp:Literal>
                                </li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <asp:Label ID="lblPopSC" runat="server"></asp:Label></li>
                                <li class="clear pad_5"></li>
                                <li class="prevleft fltl">
                                    <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, CYCLE_NAME%>"></asp:Literal>
                                </li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <asp:Label ID="lblPopCycle" runat="server"></asp:Label></li>
                                <li class="clear pad_5"></li>
                                <li class="prevleft fltl">
                                    <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal>
                                </li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <asp:Label ID="lblPopBookNo" runat="server"></asp:Label></li>
                                <li class="clear pad_5"></li>
                            </ul>
                            <ul style="width: 357px; float: left;">
                                <li class="prevleft fltl">
                                    <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, ADDRESS%>"></asp:Literal></li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <asp:Label runat="server" ID="lblPopAddress"></asp:Label>
                                    <li class=" clear pad_5"></li>
                                    <li class="prevleft fltl">
                                        <asp:Literal ID="Literal57" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Literal>
                                    </li>
                                    <li class="middle fltl">:</li>
                                    <li class="prevright fltl">
                                        <asp:Label ID="lblPopMeterNo" runat="server"></asp:Label>
                                    </li>
                                    <li class="clear pad_5"></li>
                                    <li class="prevleft fltl">
                                        <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                    </li>
                                    <li class="middle fltl">:</li>
                                    <li class="prevright fltl">
                                        <asp:Label ID="lblPopOldAccNo" runat="server"></asp:Label>
                                    </li>
                                    <li class=" clear pad_5"></li>
                                    <li class="prevleft fltl">
                                        <asp:Literal ID="Literal56" runat="server" Text='<%$ Resources:Resource, MOBILE_NO %>'></asp:Literal>
                                    </li>
                                    <li class="middle fltl">:</li>
                                    <li class="prevright fltl">
                                        <asp:Label ID="lblPopMobileNo" runat="server"></asp:Label></li>
                                    <li class="clear pad_5"></li>
                                    <li class="prevleft fltl">
                                        <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                                    </li>
                                    <li class="middle fltl">:</li>
                                    <li class="prevright fltl">
                                        <asp:Label ID="lblPopTariff" runat="server"></asp:Label></li>
                                    <li class="clear pad_5"></li>
                                    <li class="prevleft fltl">
                                        <asp:Literal ID="Literal39" runat="server" Text="Total Before Adjustment"></asp:Literal>
                                    </li>
                                    <li class="middle fltl">:</li>
                                    <li class="prevright fltl">
                                        <asp:Label ID="lblTotalBill" runat="server"></asp:Label></li>
                                    <li class="clear pad_5"></li>
                                    <li class="prevleft fltl" style="line-height: 13px;">
                                        <asp:Literal ID="Literal40" runat="server" Text="Total After Adjustment"></asp:Literal>
                                    </li>
                                    <li class="middle fltl">:</li>
                                    <li class="prevright fltl">
                                        <asp:Label ID="lblTotalAfterAdjustment" runat="server"></asp:Label></li>
                                    <li class="clear pad_5"></li>
                                    <asp:HiddenField ID="hdnCustomerBillID" runat="server" Visible="false" />
                                    <asp:HiddenField ID="hdnCustomerID" runat="server" Visible="false" />
                        </div>
                        <div class="clear pad_10">
                        </div>
                        <div class="consumer_feild">
                            <div style="margin-left: 556px; overflow: auto; float: left; height: 43px;">
                                <asp:Button ID="btnCancel" Text="<%$ Resources:Resource, CANCEL%>" CssClass="calculate_but"
                                    runat="server" />
                            </div>
                        </div>
                    </asp:Panel>
                    <%--Popup--%>
                    <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="PanelAlert"
                        TargetControlID="btnAlertDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnAlertOk">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnAlertDeActivate" runat="server" Text="Button" CssClass="popbtn" />
                    <asp:Panel ID="PanelAlert" runat="server" CssClass="modalPopup" class="poppnl" Style="display: none;">
                        <div class="popheader popheaderlblred" id="pop" runat="server">
                            <asp:Label ID="lblAlertMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnAlertOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
    <%--<script type="text/javascript">
        function pageLoad() {
            DisplayMessage('<%= pnlMessage.ClientID %>');
            var table = '<%= gvCustomerLedger.ClientID %>';
            $("#<%= btnExportExcel.ClientID%>").click(function () {

                $("#<%= gvCustomerLedger.ClientID %>").btechco_excelexport({
                    containerid: table
               , datatype: $datatype.Table
                });
            });
        }
    </script>--%>
    <%--==============Escape button script starts==============--%>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <%-- <script src="../scripts/jquery.btechco.excelexport.js" type="text/javascript"></script>
    <script src="../scripts/jquery.base64.js" type="text/javascript"></script>--%>
    <script src="../JavaScript/PageValidations/CustomerLedger.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
