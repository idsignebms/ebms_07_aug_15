﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using iDSignHelper;
using UMS_NigeriaBE;
using System.Globalization;
using System.Threading;
using UMS_NigriaDAL;
using UMS_NigeriaBAL;
using System.Data;
using System.Xml;
using System.Drawing;
using org.in2bits.MyXls;
using org.in2bits.MyXls.ByteUtil;
using System.Configuration;

namespace UMS_Nigeria.CustomerUnitPortal
{
    public partial class CustomerLedger : System.Web.UI.Page
    {
        #region Members
        iDsignBAL _objIdsignBal = new iDsignBAL();
        ReportsBal _objReportsBal = new ReportsBal();
        CommonMethods _objCommonMethods = new CommonMethods();
        public int PageNum;
        XmlDocument xml = null;
        public string Key = UMS_Resource.CUSTOMER_CHANGE_TARIFF;

        #endregion

        #region Properties
        //public int PageSize
        //{
        //    get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
        //}
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.CssClass = lblMessage.Text = string.Empty;
            if (!IsPostBack)
            {
                string path = string.Empty;
                //path = objCommonMethods.GetPagePath(this.Request.Url);
                //if (objCommonMethods.IsAccess(path))
                //{
                if (!string.IsNullOrEmpty(Request.QueryString[UMS_Resource.QSTR_ACC_NO]))
                {
                    txtAccountNo.Text = _objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.QSTR_ACC_NO]);
                }
                //hfPageNo.Value = Constants.pageNoValue.ToString();
                //getCustomerLedger();
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                //}
                //else
                //{
                //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                //}
                //divCustomerDetails.Visible = false;
                divgrid.Visible = false;
                divExport.Visible = false;
                divPrint.Visible = false;
            }

        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                getCustomerLedger();

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lbtnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADVANCE_SEARCH_PAGE + "?" + UMS_Resource.FROM_PAGE + "=" + _objIdsignBal.Encrypt(Constants.CustomerLedger), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void txtAccountNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                getCustomerLedger();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvCustomerLedger_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblDebit = (Label)e.Row.FindControl("lblDebit");
                Label lblCredit = (Label)e.Row.FindControl("lblCredit");
                Label lblBalance = (Label)e.Row.FindControl("lblBalance");

                if (Convert.ToDecimal(lblDebit.Text) == 0)
                    lblDebit.Text = string.Empty;
                else
                    lblDebit.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblDebit.Text), 2, Constants.MILLION_Format).ToString();
                if (Convert.ToDecimal(lblCredit.Text) == 0)
                    lblCredit.Text = string.Empty;
                else
                    lblCredit.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblCredit.Text), 2, Constants.MILLION_Format).ToString();
                if (Convert.ToDecimal(lblBalance.Text) == 0)
                    lblBalance.Text = string.Empty;
                else
                    lblBalance.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblBalance.Text), 2, Constants.MILLION_Format).ToString();
            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[5].Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(hfTotalCredit.Value), 2, Constants.MILLION_Format).ToString();
                e.Row.Cells[6].Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(hfTotalDebit.Value), 2, Constants.MILLION_Format).ToString();
                decimal credit = string.IsNullOrEmpty(hfTotalCredit.Value) ? 0 : Convert.ToDecimal(hfTotalCredit.Value);
                decimal debit = string.IsNullOrEmpty(hfTotalDebit.Value) ? 0 : Convert.ToDecimal(hfTotalDebit.Value);
                e.Row.Cells[7].Text = _objCommonMethods.GetCurrencyFormat((credit - debit), 2, Constants.MILLION_Format).ToString();
            }
        }

        protected void lnkReferenceNo_Click(object sender, EventArgs e)
        {

            try
            {
                RptCustomerLedgerBe _objLedgerBe = new RptCustomerLedgerBe();
                RptCustomerLedgerListBe _objRptCustomerLedgerListBe = new RptCustomerLedgerListBe();
                RptCustomerLedgerDetailsListBe _objRptCustomerLedgerDetailsListBe = new RptCustomerLedgerDetailsListBe();
                _objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
                // _objLedgerBe.OldAccountNo = txtOldAcountNo.Text.Trim();
                _objLedgerBe.ReportMonths = Constants.ReportMonths;
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    _objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    _objLedgerBe.BUID = string.Empty;
                LinkButton lnkReferenceNo = (LinkButton)sender;
                _objLedgerBe.ReferenceNo = lnkReferenceNo.CommandArgument;

                XmlDocument xmlResult = _objReportsBal.GetLedger(_objLedgerBe, ReturnType.User);
                _objRptCustomerLedgerListBe = _objIdsignBal.DeserializeFromXml<RptCustomerLedgerListBe>(xmlResult);
                _objRptCustomerLedgerDetailsListBe = _objIdsignBal.DeserializeFromXml<RptCustomerLedgerDetailsListBe>(xmlResult);
                if (_objRptCustomerLedgerDetailsListBe.items.Count > 0)
                {
                    _objLedgerBe = _objRptCustomerLedgerDetailsListBe.items[0];
                }
                if (_objLedgerBe.IsSuccess)
                {
                    lblBillId.Text = lnkReferenceNo.CommandArgument;
                    lblPopAccNo.Text = _objLedgerBe.AccountNo;
                    lblPopFullName.Text = _objLedgerBe.Name;
                    lblPopAddress.Text = _objLedgerBe.ServiceAddress;
                    lblPopMeterNo.Text = _objLedgerBe.MeterNo;
                    lblPopTariff.Text = _objLedgerBe.ClassName;
                    lblPopOldAccNo.Text = _objLedgerBe.OldAccountNo;
                    lblPopMobileNo.Text = _objLedgerBe.MobileNo;
                    lblPopBU.Text = _objLedgerBe.BusinessUnitName;
                    lblPopSU.Text = _objLedgerBe.ServiceUnitName;
                    lblPopSC.Text = _objLedgerBe.ServiceCenterName;
                    lblPopCycle.Text = _objLedgerBe.CycleName;
                    lblPopBookNo.Text = _objLedgerBe.BookNo;
                    mpeRefDetails.Show();



                    hfTotalDebit.Value = (from x in _objRptCustomerLedgerListBe.items.AsEnumerable()
                                          select x.Debit).Sum().ToString();
                    hfTotalCredit.Value = (from x in _objRptCustomerLedgerListBe.items.AsEnumerable()
                                           select x.Credit).Sum().ToString();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods

        private void getCustomerLedger()
        {
            try
            {
                RptCustomerLedgerBe _objLedgerBe = new RptCustomerLedgerBe();
                RptCustomerLedgerListBe _objRptCustomerLedgerListBe = new RptCustomerLedgerListBe();
                RptCustomerLedgerDetailsListBe _objRptCustomerLedgerDetailsListBe = new RptCustomerLedgerDetailsListBe();
                _objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
                // _objLedgerBe.OldAccountNo = txtOldAcountNo.Text.Trim();
                _objLedgerBe.ReportMonths = Constants.ReportMonths;
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    _objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    _objLedgerBe.BUID = string.Empty;
                XmlDocument xmlResult = _objReportsBal.GetLedger(_objLedgerBe, ReturnType.Bulk);
                _objRptCustomerLedgerListBe = _objIdsignBal.DeserializeFromXml<RptCustomerLedgerListBe>(xmlResult);
                _objRptCustomerLedgerDetailsListBe = _objIdsignBal.DeserializeFromXml<RptCustomerLedgerDetailsListBe>(xmlResult);
                if (_objRptCustomerLedgerDetailsListBe.items.Count > 0)
                {
                    _objLedgerBe = _objRptCustomerLedgerDetailsListBe.items[0];
                }
                if (_objLedgerBe.IsSuccess)
                {
                    if (_objLedgerBe.IsCustExistsInOtherBU)
                    {
                        lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                        mpeAlert.Show();
                    }
                    else
                    {
                        lblAccountNo.Text = _objLedgerBe.AccountNo;
                        lblAddress.Text = _objLedgerBe.ServiceAddress;
                        lblName.Text = _objLedgerBe.Name;
                        lblMeterNo.Text = _objLedgerBe.MeterNo;
                        lblTariff.Text = _objLedgerBe.ClassName;
                        //lblOldAccNo.Text = _objLedgerBe.OldAccountNo;
                        //lblMobileNo.Text = _objLedgerBe.MobileNo;
                        //lblBU.Text = _objLedgerBe.BusinessUnitName;
                        //lblSU.Text = _objLedgerBe.ServiceUnitName;
                        //lblSC.Text = _objLedgerBe.ServiceCenterName;
                        //lblCycle.Text = _objLedgerBe.CycleName;
                        //lblBookNo.Text = _objLedgerBe.BookNo;

                        hfTotalDebit.Value = (from x in _objRptCustomerLedgerListBe.items.AsEnumerable()
                                              select x.Debit).Sum().ToString();
                        hfTotalCredit.Value = (from x in _objRptCustomerLedgerListBe.items.AsEnumerable()
                                               select x.Credit).Sum().ToString();

                        if (_objRptCustomerLedgerListBe.items.Count > 1)
                        {
                            gvCustomerLedger.DataSource = _objRptCustomerLedgerListBe.items;
                            gvCustomerLedger.DataBind();
                            //divCustomerDetails.Visible = true;
                            divgrid.Visible = true;
                            divExport.Visible = true;
                            divPrint.Visible = true;
                        }
                        else
                        {
                            //divCustomerDetails.Visible = false;
                            CustomerNotFound();
                            Message("No payment made by this customer so far.", UMS_Resource.MESSAGETYPE_ERROR);
                        }
                    }
                }
                else
                {
                    CustomerNotFound();
                    Message("Customer Account number doesn't exist. Please check account number and try again.", UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void CustomerNotFound()
        {
            divgrid.Visible = false;
            divExport.Visible = false;
            divPrint.Visible = false;
            //lblMessage.Text = "Customer Account number doesn't exist. Please check account number and try again.";
            //lblMessage.ForeColor = Color.Red;            
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region ExportToExcel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            RptCustomerLedgerBe _objLedgerBe = new RptCustomerLedgerBe();
            RptCustomerLedgerListBe _objRptCustomerLedgerListBe = new RptCustomerLedgerListBe();
            RptCustomerLedgerDetailsListBe _objRptCustomerLedgerDetailsListBe = new RptCustomerLedgerDetailsListBe();
            _objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
            _objLedgerBe.ReportMonths = Constants.ReportMonths;
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                _objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                _objLedgerBe.BUID = string.Empty;

            XmlDocument xmlResult = _objReportsBal.GetLedger(_objLedgerBe, ReturnType.Bulk);
            _objRptCustomerLedgerListBe = _objIdsignBal.DeserializeFromXml<RptCustomerLedgerListBe>(xmlResult);
            _objRptCustomerLedgerDetailsListBe = _objIdsignBal.DeserializeFromXml<RptCustomerLedgerDetailsListBe>(xmlResult);

            XlsDocument xls = new XlsDocument();
            Worksheet sheet = xls.Workbook.Worksheets.Add("sheet");
            //state the name of the column headings 
            Cells cells = sheet.Cells;
            int i = 5, j = 1;
            Cell _objCell = null;

            cells.Add(1, 1, "Account No");
            cells.Add(1, 4, "Name");
            cells.Add(2, 1, "Meter No");
            cells.Add(2, 4, "Tariff");
            cells.Add(3, 1, "Address");

            _objCell = cells.Add(1, 2, _objRptCustomerLedgerDetailsListBe.items[0].AccountNo);
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell = cells.Add(1, 5, _objRptCustomerLedgerDetailsListBe.items[0].Name);
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell = cells.Add(2, 2, _objRptCustomerLedgerDetailsListBe.items[0].MeterNo);
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell = cells.Add(2, 5, _objRptCustomerLedgerDetailsListBe.items[0].ClassName);
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell = cells.Add(3, 2, _objRptCustomerLedgerDetailsListBe.items[0].ServiceAddress);
            _objCell.Font.Weight = FontWeight.ExtraBold;

            _objCell = cells.Add(5, 1, "S.No");
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell = cells.Add(5, 2, "Date");
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell = cells.Add(5, 3, "Reference No");
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell = cells.Add(5, 4, "Description");
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell = cells.Add(5, 5, "Remarks");
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell = cells.Add(5, 6, "Debit");
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell = cells.Add(5, 7, "Credit");
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell = cells.Add(5, 8, "Balance");
            _objCell.Font.Weight = FontWeight.ExtraBold;

            foreach (RptCustomerLedgerBe item in _objRptCustomerLedgerListBe.items)
            {
                i++;
                cells.Add(i, 1, j);
                cells.Add(i, 2, item.TransactionDate);
                cells.Add(i, 3, item.ReferenceNo);
                cells.Add(i, 4, item.Particulars);
                cells.Add(i, 5, item.Remarks);
                cells.Add(i, 6, _objCommonMethods.GetCurrencyFormat(item.Credit, 2, Constants.MILLION_Format));
                cells.Add(i, 7, _objCommonMethods.GetCurrencyFormat(item.Debit, 2, Constants.MILLION_Format));
                cells.Add(i, 8, _objCommonMethods.GetCurrencyFormat(item.Balance, 2, Constants.MILLION_Format));

                j++;
            }

            string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
            fileName = _objLedgerBe.AccountNo + fileName;
            string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
            xls.FileName = filePath;
            xls.Save();
            _objIdsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
        }
        #endregion
    }
}