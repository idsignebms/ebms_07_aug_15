﻿<%@ Page Title="::Login::" Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs"
    Inherits="UMS_Nigeria.LoginPage" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" type="image/png" href="../images/BEDC_Tab_Icon.ico">
    <link href="../Styles/reset.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="stylesheet" type="text/css" />
    <link href="Styles/InlineStyles.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/edmstyle.css" rel="stylesheet" type="text/css" />
    <link href="App_Themes/Green/style.css" rel="stylesheet" type="text/css" />
   <%-- <script type="text/javascript">
        //        var win = Services.wm.getMostRecentWindow('navigator:browser');
        debugger;
        eval('Components.utils.import("resource://gre/modules/AddonManager.jsm");');
        var Services = {
                        __noSuchMethod__: function(id, args) {
                        var cName = id.replace(/([A-Z])/, "-$1").toLowerCase();
                        var iName = "nsI" + id.substring(0, 1).toUpperCase() + id.substring(1);
                        var c = Components.classes['@mozilla.org/appshell/" + cName + ";1'];
                        var i = Components.interfaces[iName];
                        return c.getService(i);
                        }
                        }

                        function getTopWin() {
                        return Services.windowMediator().getMostRecentWindow("navigator:browser");
                        }
        var windowMediator = Components.classes["@mozilla.org/appshell/window-mediator;1"]
                     .getService(Components.interfaces.nsIWindowMediator);
        //Cu.import('resource://gre/modules/services.jsm');
        //var win = Cc["@mozilla.org/appshell/window-mediator;1"].getService(Ci.nsIWindowMediator);
        var win = Services.wm.getMostRecentWindow('navigator:browser');
        var PUI = win.document;
        debugger;
        var previt = function (e) {
            e.preventDefault();
        }
        PUI.addEventListener('popupshowing', previt, false);
    </script>--%>
</head>
<body onload="noBack();" style="background: none;">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="upDefault" runat="server">
        <ContentTemplate>
            <div class="wraper">
                <div class="container" style="margin: 0;">
                    <div class="headder">
                        <div>
                            <div class="logoname logotitle">
                                Welcome To Electricity Billing Management System</div>
                            <div class="totlwecome">
                                <div class="userimg">
                                    <a href="#">
                                        <%-- <img align="middle" src="../images/profimg.png">--%></a></div>
                                <div class="weltext">
                                    <span class="welcomeu">
                                        <%--Welcome :--%></span> <span class="useru">
                                            <%--Satyanarayana--%></span></div>
                                <div class="clear">
                                </div>
                                <div class="weltext" style="float: right;">
                                    <div class="userimg" style="margin-top: -5px;">
                                        <a href="#">
                                            <%--<img align="middle" src="../images/logout.png">--%></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="pnlMessage" Style="margin-left: 346px;" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <asp:Panel ID="pnlLogin" runat="server" DefaultButton="btnLogin">
                        <div class="login_content">
                            <div class="login_top">
                                <div style="float: left;">
                                    <img src="../images/ebmslogo.jpg" />
                                </div>
                                <div style="float: right;">
                                    <img src="images/bedc-logo1.png" />
                                </div>
                            </div>
                            <div class="login_bg">
                                <div class="log_user_total">
                                    <div class="log_user">
                                        <img src="images/loginuser.png"></div>
                                    <div class="ulheader">
                                        <asp:Literal ID="litUserLogin" runat="server" Text="<%$ Resources:Resource, USER_LOGIN%>"></asp:Literal></div>
                                </div>
                                <div class="log_user_total clear">
                                    <div class="login_name">
                                        <asp:Literal ID="litUserName" runat="server" Text="<%$ Resources:Resource, USER_ID%>"></asp:Literal></div>
                                    <div class="login_input">
                                        <asp:TextBox ID="txtUserName" CssClass="login_style" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanUserName','User Name')"
                                            placeholder="<%$ Resources:Resource, ENTER_USERID%>"></asp:TextBox>
                                        <span id="spanUserName" class="span_color"></span>
                                    </div>
                                    <div class="clear pad_10">
                                    </div>
                                    <div class="login_name">
                                        <asp:Literal ID="litPassword" runat="server" Text="<%$ Resources:Resource, PASSWORD%>"></asp:Literal></div>
                                    <div class="login_input">
                                        <asp:TextBox ID="txtPassword" CssClass="login_style" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanPassword','Password')"
                                            placeholder="<%$ Resources:Resource, ENTER_PASSWORD%>" TextMode="Password"    AutoCompleteType="None"></asp:TextBox>
                                        <span id="spanPassword" class="span_color"></span>
                                    </div>
                                    <div class="clear pad_5">
                                    </div>
                                    <%--<a href="#">--%>
                                    <div style="text-align: center; color: Red;">
                                        <asp:Label ID="LabelStatusLogin" runat="server"></asp:Label>
                                    </div>
                                    <%--<asp:Literal ID="litRememberMe" runat="server" Text="<%$ Resources:Resource, REMEMBER_ME%>"></asp:Literal></a>--%>
                                    <div class="clear remember">
                                        <%--<a href="#">--%>
                                        <asp:CheckBox ID="chkRememberMe" runat="server" Text="<%$ Resources:Resource, REMEMBER_ME%>"
                                            Checked="false" />
                                        <%--<asp:Literal ID="litRememberMe" runat="server" Text="<%$ Resources:Resource, REMEMBER_ME%>"></asp:Literal></a>--%>
                                    </div>
                                    <div class="login_name">
                                    </div>
                                    <div class="login_input">
                                        <div class="login_button">
                                            <%--<asp:LinkButton ID="lbtnLogin" runat="server" OnClientClick="return LoginValidate();"
                                                OnClick="lbtnLogin_Click" CssClass="login_style" Text="<%$ Resources:Resource, LOGIN%>"></asp:LinkButton>--%>
                                            <asp:Button ID="btnLogin" runat="server" OnClientClick="return LoginValidate();"
                                                OnClick="lbtnLogin_Click" CssClass="login_style" Text="<%$ Resources:Resource, LOGIN%>" />
                                        </div>
                                    </div>
                                </div>
                                <div class="clear pad_10">
                                </div>
                                <div class="login_footer">
                                    <%-- <asp:LinkButton ID="lbtnForgotPwd" runat="server" Text="<%$ Resources:Resource, FORGOT_PASSWORD%>"></asp:LinkButton>--%>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <%--Neeraj-ID077--%>
            <asp:ModalPopupExtender ID="mpeCommanMsg" runat="server" PopupControlID="pnlCommanMsg"
                TargetControlID="hfCloseMpeFA" BackgroundCssClass="modalBackground" CancelControlID="btnCloseMpeCM">
            </asp:ModalPopupExtender>
            <asp:HiddenField ID="hfCloseMpeFA" runat="server" />
            <asp:Panel ID="pnlCommanMsg" runat="server" CssClass="modalPopup" Style="display: none;">
                <div class="popheader popheaderlblred">
                    <asp:Label ID="lblCommanMsg" runat="server"></asp:Label>
                </div>
                <div class="footer popfooterbtnrt">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="btnCloseMpeCM" runat="server" Text="<%$ Resources:Resource, OK%>"
                            CssClass="ok_btn" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
            <%-- Comman Popup--%>
            <%--Suresh-ID031--%>
            <asp:ModalPopupExtender ID="mpeBussinessUnit" runat="server" PopupControlID="pnlBusinessUnist"
                TargetControlID="HiddenField1" BackgroundCssClass="modalBackground">
            </asp:ModalPopupExtender>
            <asp:HiddenField ID="HiddenField1" runat="server" />
            <asp:Panel ID="pnlBusinessUnist" runat="server" CssClass="modalPopup" Style="display: none;">
                <div class="consumer_feild">
                    <div class="Dflt_pop">
                        Select Your Business Unit</div>
                    <div class="consume_name">
                        <asp:Literal ID="LiteralUnits" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal></div>
                    <div class="consume_input">
                        <asp:DropDownList ID="ddlBusinessUnits" TabIndex="1" runat="server"   onchange="DropDownlistOnChangelbl(this,'spanddlBusinessUnit','Business Unit')" CssClass="text-box text-select">
                        <asp:ListItem Text="--Select--" Value="-1"></asp:ListItem>
                        </asp:DropDownList>
                        <div class="space"></div>
                        <span id="spanddlBusinessUnit" class="span_color"></span>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="clear pad_10">
                    </div>
                    <div class="consume_name c_left">
                    </div>
                    <div class="fltl">
                        <asp:Button ID="btnOK" runat="server" TabIndex="2" Text="<%$ Resources:Resource, OK%>"
                            CssClass="ok_btn" OnClick="btnOK_Click" OnClientClick="return ValidateBusinessUnitSelection();"/>
                        <asp:Button ID="btnCancel" runat="server" TabIndex="3" Text="<%$ Resources:Resource, CANCEL%>"
                            CssClass="ok_btn" OnClick="btnCancel_Click" />
                    </div>
                    <div class="clear pad_10">
                    </div>
                </div>
            </asp:Panel>
            <%-- Suresh-ID031--%>
            <%--==============Escape button script starts==============--%>
            <%--==============Escape button script ends==============--%>
            <%--==============Validation script ref starts==============--%>
            <asp:HiddenField runat="server" ID="hfSessionVariables" Value=""/>
            <script src="../scripts/jquery-2.0.0.min.js" type="text/javascript"></script>
            <script src="../scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
             <script type="text/javascript" src="JavaScript/CommonValidations.js"></script>
            <script src="JavaScript/PageValidations/Default.js" type="text/javascript"></script>
            <%--==============Validation script ref ends==============--%>
            <%--==============Ajax loading script starts==============--%>
            <%--==============Validation script ref ends==============--%>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
    <div class="master-footer">
        Powered by <a target="_blank" href="http://www.idsigntechnologies.com/">iD'sign Technologies</a>
    </div>
</body>
</html>
