﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for DefaultPage
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 03-MAR-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBE;
using System.Xml;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using Resources;
using System.Threading;
using System.Globalization;
using System.IO;
using System.Configuration;

namespace UMS_Nigeria
{
    public partial class LoginPage : System.Web.UI.Page
    {
        #region Members
        iDsignBAL objIDsignBAL = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        LoginPageBAL objLoginPageBAL = new LoginPageBAL();
        UMS_Service _ObjUMS_Service = new UMS_Service();
        string Key = "LoginPage";
        //string[] sessionArrayValues = new string[] { "UserName", "LoginId", "RoleId", "Designation", "EmailId" };
        #endregion

        #region Properties

        private string UserName
        {
            get { return txtUserName.Text.Trim(); }
            set { txtUserName.Text = value; }
        }

        private string Password
        {
            get { return txtPassword.Text.Trim(); }
            set { txtPassword.Text = value; }
        }

        #endregion

        #region Events
        protected void Page_PreInit()
        {
            //SecurityDongle securityDongle = new SecurityDongle();
            //if (securityDongle.DongleMessage != "Success")
            //{
            //    Response.Redirect("~/ValidateUser.aspx");
            //}

            if (Session[UMS_Resource.SESSION_LOGINID] != null)
                Response.Redirect(UMS_Resource.HomePage, false);
        }
        //    try
        //    {
        //        if (!HttpContext.Current.Request.Url.AbsoluteUri.Contains("localhost"))
        //        {
        //            if (!HttpContext.Current.Request.Url.AbsoluteUri.Contains("https://www."))//FOR LIVE  
        //            {
        //                string UrlRedirect = HttpContext.Current.Request.Url.AbsoluteUri;
        //                if (UrlRedirect.Contains("http://www."))
        //                {
        //                    UrlRedirect = UrlRedirect.Replace("http://www.", "https://www.");
        //                }
        //                else
        //                    UrlRedirect = UrlRedirect.Replace("http://", "https://www.");
        //                Response.Redirect(UrlRedirect, false);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message,UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = "";
            try
            {
                if (Request.Cookies["UserDetails"] != null)
                {
                    HttpCookie UserDetails = Request.Cookies["UserDetails"];
                    if (!IsPostBack)
                    {
                        (from f in new DirectoryInfo(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString())).GetFiles()
                         where f.CreationTime < DateTime.Now.Subtract(TimeSpan.FromHours(3))
                         select f
                       ).ToList()
                       .ForEach(f => f.Delete());

                        if (UserDetails != null)
                        {
                            txtUserName.Text = UserDetails[Constants.COOKIE_USERNAME];
                            txtPassword.Attributes.Add("value", UserDetails[Constants.COOKIE_PASSWORD].ToString());
                        }
                    }
                }
                txtUserName.Focus();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lbtnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                //Response.Cookies["objHttpCookieLogAtmpt"].Expires = DateTime.Now.AddDays(-1);//Neeraj-ID077
                XmlDocument xml = new XmlDocument();
                LoginPageBE objLoginPageBE = new LoginPageBE();
                objLoginPageBE.UserName = UserName;
                objLoginPageBE.Password = objIDsignBAL.AESEncryptString(Password);
                xml = objLoginPageBAL.Login(objLoginPageBE, ReturnType.Get);
                objLoginPageBE = objIDsignBAL.DeserializeFromXml<LoginPageBE>(xml);

                if (objLoginPageBE.IsLogin)
                {
                    Response.Cookies["objHttpCookieLogAtmpt"].Expires = DateTime.Now.AddDays(-1);//Neeraj-ID077
                    LabelStatusLogin.Visible = false;

                    hfSessionVariables.Value = objLoginPageBE.UserName + "^" + txtUserName.Text + "^" + objLoginPageBE.RoleId + "^" + objLoginPageBE.Designation + "^" + objLoginPageBE.PrimaryEmailId;
                    //Session[UMS_Resource.SESSION_UserName] = objLoginPageBE.UserName;
                    //Session[UMS_Resource.SESSION_LOGINID] = txtUserName.Text;
                    //Session[UMS_Resource.SESSION_ROLEID] = objLoginPageBE.RoleId;
                    //Session[UMS_Resource.SESSION_DESIGNATION] = objLoginPageBE.Designation;
                    //Session[UMS_Resource.SESSION_USER_EMAILID] = objLoginPageBE.PrimaryEmailId;

                    RememberMe();
                    //if (objLoginPageBE.RoleId != Convert.ToInt32(Roles.SuperAdmin))
                    if (!IsSuperAdmin(objLoginPageBE.UserId))
                    {
                        FetchBusinessUnits(objLoginPageBE.UserId);
                    }
                    else
                    {
                        sessionVariables();
                        Response.Redirect(UMS_Resource.HomePage, false);
                    }
                }
                else//Neeraj-ID077
                {
                    if (!objLoginPageBE.IsDisabled && !objLoginPageBE.IsDeleted && objLoginPageBE.IsExists)
                    {
                        int LoginAttempt = GetLoginAttempt(txtUserName.Text);
                        int AttemptLeft = 3 - LoginAttempt;
                        if (LoginAttempt < Constants.MaxLoginAttempts)
                        {
                            LabelStatusLogin.Visible = true;
                            if (AttemptLeft > 0)
                                LabelStatusLogin.Text = string.Format(Resource.LOGIN_ATMPT_WRN_MSG, AttemptLeft);
                            else
                                LabelStatusLogin.Text = string.Format(Resource.LOGIN_ATMPT_BLOCK_MSG);
                            txtUserName.Focus();
                        }
                        else
                        {
                            Session.Abandon();
                            _objCommonMethods.DisableUser(txtUserName.Text);
                            Response.Cookies["objHttpCookieLogAtmpt"].Expires = DateTime.Now.AddDays(-1);
                            LabelStatusLogin.Text = "";
                            lblCommanMsg.Text = Resource.NO_ATMPT;
                        }
                    }

                    else if (objLoginPageBE.IsDisabled)
                    {
                        Message(Resource.YOUR_AC_IS_INACTIVE, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else if (objLoginPageBE.IsDeleted)
                    {
                        Message(Resource.USER_DELETED, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else
                    {
                        Message(Resource.INVALID_USER_NM, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                Message(Resource.NETWORK_ISSUE, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void FetchBusinessUnits(string userId)
        {
            MastersBE _objMastersBE = new MastersBE();
            MastersListBE _objMastersListBE = new MastersListBE();
            MastersBAL _objMastersBAL = new MastersBAL();
            _objMastersBE.UserId = userId;
            XmlDocument xml = _objMastersBAL.GetBusinessUnits(_objMastersBE, ReturnType.User);
            _objMastersListBE = objIDsignBAL.DeserializeFromXml<MastersListBE>(xml);
            if (_objMastersListBE.Items.Count > 1)
            {
                objIDsignBAL.FillDropDownList(objIDsignBAL.ConvertListToDataSet<MastersBE>(_objMastersListBE.Items), ddlBusinessUnits, "BusinessUnitName", "BU_ID", UMS_Resource.DROPDOWN_SELECT, true);
                ddlBusinessUnits.Focus();
                mpeBussinessUnit.Show();
                //btnOK.Focus();
            }
            else
                if (_objMastersListBE.Items.Count == 1)
                {
                    sessionVariables();
                    objIDsignBAL.FillDropDownList(objIDsignBAL.ConvertListToDataSet<MastersBE>(_objMastersListBE.Items), ddlBusinessUnits, "BusinessUnitName", "BU_ID", UMS_Resource.DROPDOWN_SELECT, true);
                    Session[UMS_Resource.SESSION_USER_BUNAME] = _objMastersListBE.Items[0].BusinessUnitName;
                    Session[UMS_Resource.SESSION_USER_BUID] = _objMastersListBE.Items[0].BU_ID;
                    Response.Redirect(UMS_Resource.HomePage, false);
                }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                sessionVariables();
                Session[UMS_Resource.SESSION_USER_BUNAME] = ddlBusinessUnits.SelectedItem.Text;
                Session[UMS_Resource.SESSION_USER_BUID] = ddlBusinessUnits.SelectedValue;
                Response.Redirect(UMS_Resource.HomePage, false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void sessionVariables()
        {
            if (!string.IsNullOrEmpty(hfSessionVariables.Value))
            {
                string[] sessionArrayValues;
                sessionArrayValues = hfSessionVariables.Value.Split('^');

                Session[UMS_Resource.SESSION_UserName] = sessionArrayValues[0];
                Session[UMS_Resource.SESSION_LOGINID] = sessionArrayValues[1];
                Session[UMS_Resource.SESSION_ROLEID] = sessionArrayValues[2];
                Session[UMS_Resource.SESSION_DESIGNATION] = sessionArrayValues[3];
                Session[UMS_Resource.SESSION_USER_EMAILID] = sessionArrayValues[4];
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Abandon();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods
        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void RememberMe()
        {
            try
            {
                if (chkRememberMe.Checked)
                {
                    HttpCookie UserDetails = new HttpCookie("UserDetails");
                    UserDetails[Constants.COOKIE_USERNAME] = txtUserName.Text.Trim();
                    UserDetails[Constants.COOKIE_PASSWORD] = txtPassword.Text.Trim();
                    UserDetails.Expires = DateTime.Now.AddDays(7);
                    Response.Cookies.Add(UserDetails);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void AddLoginAttempt(string UserId)//Neeraj-ID077
        {
            HttpCookie objHttpCookie = Request.Cookies[UMS_Resource.LOGIN_ATMPT_COOKIE];

            if (objHttpCookie != null)
            {
                string[] LoginDetails = new string[2];
                LoginDetails[0] = objHttpCookie[Resources.UMS_Resource.USERID];
                LoginDetails[1] = objHttpCookie[UMS_Resource.LOGIN_ATMPT];
                if (LoginDetails[0] == UserId)
                {
                    objHttpCookie[UMS_Resource.LOGIN_ATMPT] = (Convert.ToInt16(LoginDetails[1]) + 1).ToString();
                    Response.Cookies.Add(objHttpCookie);
                }
            }

            else
            {
                objHttpCookie = new HttpCookie(UMS_Resource.LOGIN_ATMPT_COOKIE);
                objHttpCookie[UMS_Resource.USERID] = UserId;
                objHttpCookie[UMS_Resource.LOGIN_ATMPT] = "1";
                objHttpCookie.Expires = DateTime.Now.AddMinutes(15);
                Response.Cookies.Add(objHttpCookie);
            }
        }

        private int GetLoginAttempt(string UserId)//Neeraj-ID077
        {
            AddLoginAttempt(txtUserName.Text);
            HttpCookie objHttpCookie = Request.Cookies[UMS_Resource.LOGIN_ATMPT_COOKIE];
            string[] LoginDetails = new string[2];
            int LoginAttempt = 0;
            if (objHttpCookie != null)
            {
                LoginDetails[0] = objHttpCookie[Resources.UMS_Resource.USERID];
                LoginDetails[1] = objHttpCookie[UMS_Resource.LOGIN_ATMPT];
                if (LoginDetails[0] == UserId)
                {
                    LoginAttempt = Convert.ToInt16(LoginDetails[1]);
                }
            }
            return LoginAttempt;
        }

        public bool IsSuperAdmin(string UserId)
        {
            LoginPageBE objLoginPageBE = new LoginPageBE();
            objLoginPageBE.UserId = UserId;
            XmlDocument xmlResult = objLoginPageBAL.Login(objLoginPageBE, ReturnType.Single);
            objLoginPageBE = objIDsignBAL.DeserializeFromXml<LoginPageBE>(xmlResult);
            return objLoginPageBE.IsSuccess;
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}