﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Resources;
using UMS_NigeriaBAL;
using iDSignHelper;
using UMS_NigeriaBE;
using System.Xml;
using System.Data;
using System.Configuration;

namespace UMS_Nigeria.EBill
{
    public partial class EBill : System.Web.UI.Page
    {
        #region Members

        MastersBE _ObjMastersBE = new MastersBE();
        MastersListBE _ObjMastersListBE = new MastersListBE();
        MastersBAL _ObjMastersBAL = new MastersBAL();
        XmlDocument _ObjXmlDocument = new XmlDocument();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public static void GenerateTxtEBillFile(string str, string fileName)
        {
            try
            {
                FileStream fs = default(FileStream);
                fs = new FileStream(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["EBillPayFiles"]) + fileName + "_" + DateTime.Now.ToString(Resource.EBILL_DATE_FRMT1) + Resource.Formate_Text, FileMode.Append);
                StreamWriter sw = new StreamWriter(fs);
                sw.Write(str);
                sw.Close();
                fs.Close();
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnGBill_Click(object sender, EventArgs e)
        {
            string space = "";
            string eBillFormate = string.Empty;
            string TempBU = string.Empty;
            int Frmtcounter = 0;
            int itemCounter = 0;
            string fileNameTxt = string.Empty;
            string fileNameXls = string.Empty;
            string BUS_Unit = string.Empty;
            DataSet WorkBook = new DataSet();
            List<MastersBE> _ObjMastersBEFileGen = new List<MastersBE>();
            _ObjXmlDocument = _ObjMastersBAL.GetEBillDetailsBAL(_ObjMastersBE);
            _ObjMastersListBE = _ObjiDsignBAL.DeserializeFromXml<MastersListBE>(_ObjXmlDocument);
            if (_ObjMastersListBE != null)
            {
                if (_ObjMastersListBE.Items.Count > 0)
                {
                    TempBU = _ObjMastersListBE.Items[0].DistName;

                    foreach (MastersBE Mas in _ObjMastersListBE.Items)
                    {
                        if (TempBU == Mas.DistName)
                        {
                            if (Frmtcounter == 0)
                            {
                                BUS_Unit = Mas.DistName;
                                eBillFormate += Mas.TrnDate.ToString(Resource.EBILL_DATE_FRMT2) + Mas.BatchNo + Mas.BatchTotal.ToString(Resource.EBILL_CURRENCY_FRMT1) + Resource.PAYDIRECT_PAYMENT + space.PadRight(33) + Mas.BatchNo;
                            }
                            eBillFormate += Environment.NewLine + Mas.AccountNo + Mas.TrnAmount.ToString(Resource.EBILL_CURRENCY_FRMT2) + Resource.EBILL_CASH_ID + space.PadRight(5) + Mas.TrnDate.ToString(Resource.EBILL_DATE_FRMT1);
                            _ObjMastersBEFileGen.Add(Mas);
                            Frmtcounter++;
                            if (itemCounter == _ObjMastersListBE.Items.Count - 1)
                            {
                                fileNameXls = BUS_Unit + "_" + DateTime.Now.ToString(Resource.EBILL_DATE_FRMT1);
                                WorkBook.Tables.Add(_ObjCommonMethods.SaveExcel(_ObjMastersBEFileGen, fileNameXls));

                                fileNameTxt = _ObjCommonMethods.GenerateTxtEBillFile(eBillFormate, BUS_Unit);
                                _ObjCommonMethods.SendMailToBUEBillFiles(fileNameTxt, fileNameXls);
                                _ObjMastersBEFileGen = new List<MastersBE>();
                            }
                        }
                        else
                        {
                            fileNameXls = BUS_Unit + "_" + DateTime.Now.ToString(Resource.EBILL_DATE_FRMT1);
                            WorkBook.Tables.Add(_ObjCommonMethods.SaveExcel(_ObjMastersBEFileGen, fileNameXls));
                            _ObjMastersBEFileGen = new List<MastersBE>();
                            fileNameTxt = _ObjCommonMethods.GenerateTxtEBillFile(eBillFormate, BUS_Unit);
                            _ObjCommonMethods.SendMailToBUEBillFiles(fileNameTxt, fileNameXls);
                            TempBU = Mas.DistName;
                            eBillFormate = string.Empty;
                            BUS_Unit = Mas.DistName;
                            eBillFormate += Mas.TrnDate.ToString(Resource.EBILL_DATE_FRMT2) + Mas.BatchNo + Mas.BatchTotal.ToString(Resource.EBILL_CURRENCY_FRMT1) + Resource.PAYDIRECT_PAYMENT + space.PadRight(33) + Mas.BatchNo;
                            eBillFormate += Environment.NewLine + Mas.AccountNo + Mas.TrnAmount.ToString(Resource.EBILL_CURRENCY_FRMT2) + Resource.EBILL_CASH_ID + space.PadRight(5) + Mas.TrnDate.ToString(Resource.EBILL_DATE_FRMT1);
                            _ObjMastersBEFileGen.Add(Mas);
                            Frmtcounter++;
                            if (itemCounter == _ObjMastersListBE.Items.Count - 1)
                            {
                                fileNameXls = BUS_Unit + "_" + DateTime.Now.ToString(Resource.EBILL_DATE_FRMT1);
                                WorkBook.Tables.Add(_ObjCommonMethods.SaveExcel(_ObjMastersBEFileGen, fileNameXls));
                                _ObjMastersBEFileGen = new List<MastersBE>();
                                _ObjCommonMethods.GenerateTxtEBillFile(eBillFormate, BUS_Unit);
                                _ObjCommonMethods.SendMailToBUEBillFiles(fileNameTxt, fileNameXls);
                            }
                        }
                        itemCounter++;
                    }
                    //_ObjCommonMethods.CreateExeclWithMultiWorksheets(WorkBook);

                }
            }
        }

    }
}