﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Globalization;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
using System.Configuration;
using System.IO;
using System.Security.AccessControl;
using Ionic.Zip;

namespace UMS_Nigeria
{
    /// <summary>
    /// Priority of log
    /// </summary>
    public enum LogPriority
    {
        None = 0,
        High = 1,
        Medium = 2,
        Normal = 3,
        Low = 4
    }

    public static class ErrorLog
    {
        /// <summary>
        /// Log type for logging 
        /// </summary>
        public enum LogType
        {
            None = 0,
            Information = 4,
            Warning = 3,
            Success = 2,
            Error = 1
        }

        /// <summary>
        /// Log the activity in to event viewer
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="methodName"></param>
        /// <param name="lp"></param>
        /// <param name="title"></param>
        public static void Log(string message, LogPriority lp, string title)
        {
            LogEntry le = new LogEntry();
            le.Title = title;
            le.TimeStamp = DateTime.Now;
            le.Message = message;
            le.Priority = Convert.ToInt32(lp, CultureInfo.InvariantCulture.NumberFormat);

        }
        /// <summary>
        /// Log the activity in to log file
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="methodName"></param>
        /// <param name="lt"></param>
        public static void LoggingIntoText(string message, string methodName, LogType lt)
        {

            try
            {
                string logTypes = "1";
                if (logTypes.IndexOf(Convert.ToInt32(lt).ToString()) >= 0)
                {
                    LogEntry le = new LogEntry();
                    le.Title = methodName;
                    le.AddErrorMessage(message);
                    le.Message = message;
                    le.TimeStamp = DateTime.Now;
                    if (lt == LogType.Error)
                    {
                        le.Severity = System.Diagnostics.TraceEventType.Error;
                        le.Categories.Add(System.Diagnostics.TraceEventType.Error.ToString());
                    }
                    else if (lt == LogType.Warning)
                    {
                        le.Severity = System.Diagnostics.TraceEventType.Warning;
                        le.Categories.Add(System.Diagnostics.TraceEventType.Warning.ToString());
                    }
                    else if (lt == LogType.Information)
                    {
                        le.Severity = System.Diagnostics.TraceEventType.Information;
                        le.Categories.Add(System.Diagnostics.TraceEventType.Information.ToString());
                    }
                    else if (lt == LogType.Success)
                    {
                        le.Severity = System.Diagnostics.TraceEventType.Information;
                        le.Categories.Add(System.Diagnostics.TraceEventType.Information.ToString());
                    }
                    Logger.Write(le);
                }
            }
            catch
            {
                throw;
            }
        }
        /// <summary>
        /// Log the activity in to log file
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="methodName"></param>
        /// <param name="lt"></param>
        /// <param name="ex"></param>
        public static void LoggingIntoText(string message, string methodName, LogType lt, Exception ex)
        {
            try
            {
                string logTypes = "1";
                if (logTypes.IndexOf(Convert.ToInt32(lt).ToString()) >= 0)
                {
                    LogEntry le = new LogEntry();

                    le.Title = methodName;

                    le.AddErrorMessage(message);
                    le.TimeStamp = DateTime.Now;
                    if (lt == LogType.Error)
                    {
                        le.Severity = System.Diagnostics.TraceEventType.Error;
                        le.Categories.Add(System.Diagnostics.TraceEventType.Error.ToString());
                        le.Message = message + Environment.NewLine + "Error Message: " + ex.Message + Environment.NewLine + "Error Source: " + ex.Source + Environment.NewLine + "Error Inner Exception: " + ex.StackTrace + Environment.NewLine + "Error data: " + ex.Data;
                    }
                    else if (lt == LogType.Warning)
                    {
                        le.Severity = System.Diagnostics.TraceEventType.Warning;
                        le.Categories.Add(System.Diagnostics.TraceEventType.Warning.ToString());
                        le.Message = message + Environment.NewLine;
                    }
                    else if (lt == LogType.Information)
                    {
                        le.Severity = System.Diagnostics.TraceEventType.Information;
                        le.Categories.Add(System.Diagnostics.TraceEventType.Information.ToString());
                        le.Message = message + Environment.NewLine;
                    }
                    else if (lt == LogType.Success)
                    {
                        le.Severity = System.Diagnostics.TraceEventType.Information;
                        le.Categories.Add(System.Diagnostics.TraceEventType.Information.ToString());
                        le.Message = message + Environment.NewLine;
                    }
                    Logger.Write(le);
                }
            }
            catch
            {
                throw;
            }
        }
        /// <summary>
        /// Delete the Log files
        /// </summary>
        public static void DeleteLogFile()
        {
            try
            {
                String logFolder = string.Empty;
                string path = AppDomain.CurrentDomain.BaseDirectory.ToString();
                var entLibConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
                //var entLibConfig = System.Configuration.ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                LoggingSettings loggingSettings = (LoggingSettings)entLibConfig.GetSection(LoggingSettings.SectionName);
                TraceListenerData traceListenerData = loggingSettings.TraceListeners.Get("Rolling Flat File Trace Listener");
                ElementInformation listener = traceListenerData.ElementInformation;
                string filename = listener.Properties["fileName"].Value.ToString();

                int index = filename.LastIndexOf("\\", StringComparison.OrdinalIgnoreCase);
                string logFileName = filename.Substring(index + 1);
                int index2 = logFileName.LastIndexOf(".", StringComparison.OrdinalIgnoreCase);
                string logFileNamewithoutExt = logFileName.Substring(0, index2);


                if (File.Exists(filename))
                {
                    FileInfo logPath = new FileInfo(filename);
                    logFolder = logPath.DirectoryName;
                }
                else
                {
                    string filePath = path + filename;
                    FileInfo logPath = new FileInfo(filePath);
                    logFolder = logPath.DirectoryName;
                }

                SetPermissions(logFolder);

                if (File.Exists(logFolder))
                {
                    // File path
                    ProcessFile(path);
                }

                else if (Directory.Exists(logFolder))
                {
                    // This path is a directory
                    ProcessDirectory(logFolder, logFileNamewithoutExt);
                }

            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, "DeleteLogFile", ErrorLog.LogType.Error);
                throw;
            }
        }
        /// <summary>
        /// Give the permission of Log file Directory
        /// </summary>
        /// <param name="logFolder"></param>
        private static void SetPermissions(String logFolder)
        {
            DirectoryInfo info = new DirectoryInfo(logFolder);
            DirectorySecurity security = info.GetAccessControl();

            security.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow));
            security.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));

            info.SetAccessControl(security);
        }
        /// <summary>
        /// Get the log files
        /// </summary>
        /// <param name="targetDirectory"></param>
        public static void ProcessDirectory(string targetDirectory, string logFileName)
        {
            //XmlOperations appValue = new XmlOperations();
            //string nameofLogfile = XmlOperations.GetConfigurationValueFromXml("NameofLogFile");
            // Process the list of files found in the directory.

            string[] fileEntries = Directory.GetFiles(targetDirectory, logFileName + ".*.log", SearchOption.AllDirectories);

            foreach (string fileName in fileEntries)
            {
                ProcessFile(fileName);
            }

            // Recurse into subdirectories of this directory.

            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);

            foreach (string subdirectory in subdirectoryEntries)

                ProcessDirectory(subdirectory, logFileName);

            //deleting zip files
            DeleteZipFiles(targetDirectory, logFileName);
        }

        /// <summary>
        /// Delete the log file
        /// </summary>
        /// <param name="path"></param>
        public static void ProcessFile(string path)
        {
            //XmlOperations appValue = new XmlOperations();
            int numberofDays = 8;

            FileInfo fi = new FileInfo(path);
            DateTime presentTime = DateTime.UtcNow;
            DateTime fileModifiedTime = fi.LastWriteTimeUtc;

            int lastindex = path.LastIndexOf("\\", StringComparison.OrdinalIgnoreCase);
            string zipfilename = path.Substring(lastindex + 1);
            string folderpath = path.Substring(0, lastindex + 1);

            if (fileModifiedTime.AddDays(numberofDays) < presentTime)
            {
                string deleteLogfile = "true";

                if (string.Compare(deleteLogfile, "true", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    File.Delete(path);
                }
                else
                {
                    using (ZipFile zip = new ZipFile())
                    {
                        //input file with folder path/application path
                        zip.AddFile(path, @"\");
                        // Save to output filename with folder path
                        zip.Save(folderpath + zipfilename + ".zip");
                    }
                    File.Delete(path);
                }
            }
            else
            {
                // Response.Write("Path: " + path + " ---> Last Access Time : " + fi.LastWriteTimeUtc.ToString() + "<br/>");
            }

        }

        /// <summary>
        /// delete zip log files
        /// </summary>
        /// <returns></returns>
        public static void DeleteZipFiles(string targetDirectory, string logFileName)
        {
            //XmlOperations appValue = new XmlOperations();
            int numberofMonths = 2;

            string[] fileEntries = Directory.GetFiles(targetDirectory, logFileName + ".*.zip", SearchOption.AllDirectories);
            foreach (string fileName in fileEntries)
            {
                FileInfo fi = new FileInfo(fileName);
                DateTime presentTime = DateTime.UtcNow;
                DateTime fileModifiedTime = fi.LastWriteTimeUtc;
                if (fileModifiedTime.AddMonths(numberofMonths) < presentTime)
                {
                    File.Delete(fileName);
                }
            }
        }

    }

}