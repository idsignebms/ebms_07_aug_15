﻿<%@ Page Title="::Home::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="UMS_Nigeria.Home"
    Theme="Green" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<script src="../scripts/vTicker1.15.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('#latest').vTicker();
        });
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="container">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <div class="content-total">
            <div class="fltl news_home">
                <div class="news-text">
                    News</div>
                <div class="fltl hmenews">
                    <div id="divLatestNewsDynamic" runat="server" class="dashboard">
                        <div class="learpoem">
                            <div id="latest">
                                <ul class=" clear">
                                    <asp:Repeater ID="rptrNews" runat="server">
                                        <ItemTemplate>
                                            <li class=" clear">
                                                <p class=" clear">
                                                    <a href='UserManagement/LatestNews.aspx?nw=<%#Eval("NoticeID") %>'>
                                                        <%#Eval("Title") %></a></p>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
                        </div>
                        <div class="readmore">
                            <%--<asp:LinkButton ID="litBusinessUnit" runat="server" Visible="false" Text="<%$Resources:Resource ,READMORE %>"
                        PostBackUrl="~/UserManagement/LatestNews.aspx"></asp:LinkButton>--%>
                        </div>
                    </div>
                    <div id="divLatestNewsStatic" runat="server" class="dashboard">
                        <div class="learpoem">
                            <div id="latest1">
                                <ul style="margin: -5px 0 0;">
                                    <li class="clear">
                                        <p>
                                            <a href='UserManagement/LatestNews.aspx?static=1'>
                                                <asp:Literal ID="litStaticNewsTitle" runat="server" Text="<%$Resources:Resource ,NEWS_STATIC_TITLE %>"></asp:Literal></a>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fltr more" style="">
                    <asp:HyperLink ID="litBusinessUnit" runat="server" Visible="false" Text="<%$Resources:Resource ,READMORE %>"
                        NavigateUrl="~/UserManagement/LatestNews.aspx"></asp:HyperLink></div>
            </div>
            <div>
                <%= UserNavigation() %>
                <%--  <div class="bildblock">
        <h2 class="billing">Billing</h2>
        <ul>
        <li><a href="#">Back Office Bulk Bill HT</a></li>
		<li><a href="#">Bulk bill Generation</a></li>
		<li><a href="#">Complaint Management - Entry</a></li>
		<li><a href="#">HT bill Generation</a></li>
		<li><a href="#">JV Import From Excel</a></li>
		<li><a href="#">Other Changes Impoert Excel</a></li>
		<li><a href="#">L-T4 Cosumm Update</a></li>
		<li><a href="#">LT Bill BackOffice</a></li>
		<li><a href="#">Month Close</a></li>
		
        <li class="readmore"><a href="#">Readmore</a></li>
        
        </ul>
        </div>
        <div class="bildblock">
        <h2 class="spotbilling">Spot Billing</h2>
        <ul>
        <li><a href="#">Back Office Bulk Bill HT</a></li>
		<li><a href="#">Bulk bill Generation</a></li>
		<li><a href="#">Complaint Management - Entry</a></li>
		<li><a href="#">HT bill Generation</a></li>
		<li><a href="#">JV Import From Excel</a></li>
		<li><a href="#">Other Changes Impoert Excel</a></li>
		<li><a href="#">L-T4 Cosumm Update</a></li>
		<li><a href="#">LT Bill BackOffice</a></li>
		<li><a href="#">Month Close</a></li>
		
        <li class="readmore"><a href="#">Readmore</a></li>
        
        </ul>
        </div>
        <div class="bildblock" style="margin:0;">
        <h2 class="setting">Settings</h2>
        <ul>
        <li><a href="#">Back Office Bulk Bill HT</a></li>
		<li><a href="#">Bulk bill Generation</a></li>
		<li><a href="#">Complaint Management - Entry</a></li>
		<li><a href="#">HT bill Generation</a></li>
		<li><a href="#">JV Import From Excel</a></li>
		<li><a href="#">Other Changes Impoert Excel</a></li>
		<li><a href="#">L-T4 Cosumm Update</a></li>
		<li><a href="#">LT Bill BackOffice</a></li>
		<li><a href="#">Month Close</a></li>
		
        <li class="readmore"><a href="#">Readmore</a></li>
        
        </ul>
        </div>
        <div class="clear pad_10">
</div>
        
        <div class="bildblock">
        <h2 class="colelction">Collection</h2>
        <ul>
        <li><a href="#">Back Office Bulk Bill HT</a></li>
		<li><a href="#">Bulk bill Generation</a></li>
		<li><a href="#">Complaint Management - Entry</a></li>
		<li><a href="#">HT bill Generation</a></li>
		<li><a href="#">JV Import From Excel</a></li>
		<li><a href="#">Other Changes Impoert Excel</a></li>
		<li><a href="#">L-T4 Cosumm Update</a></li>
		<li><a href="#">LT Bill BackOffice</a></li>
		<li><a href="#">Month Close</a></li>
		<li><a href="#">Other Adgusments</a></li>
		<li><a href="#">Other Changes</a></li>
        <li class="readmore"><a href="#">Readmore</a></li>
        
        </ul>
        
        </div>
        <div class="bildblock">
        <h2 class="connection">Connection</h2>
        <ul>
        <li><a href="#">Back Office Bulk Bill HT</a></li>
		<li><a href="#">Bulk bill Generation</a></li>
		<li><a href="#">Complaint Management - Entry</a></li>
		<li><a href="#">HT bill Generation</a></li>
		<li><a href="#">JV Import From Excel</a></li>
		<li><a href="#">Other Changes Impoert Excel</a></li>
		<li><a href="#">L-T4 Cosumm Update</a></li>
		<li><a href="#">LT Bill BackOffice</a></li>
		<li><a href="#">Month Close</a></li>
		<li><a href="#">Other Adgusments</a></li>
		<li><a href="#">Other Changes</a></li>
        <li class="readmore"><a href="#">Readmore</a></li>
        
        </ul>
        </div>
        <div class="bildblock" style="margin:0;">
        <h2 class="meeter">Metering</h2>
        <ul>
        <li><a href="#">Back Office Bulk Bill HT</a></li>
		<li><a href="#">Bulk bill Generation</a></li>
		<li><a href="#">Complaint Management - Entry</a></li>
		<li><a href="#">HT bill Generation</a></li>
		<li><a href="#">JV Import From Excel</a></li>
		<li><a href="#">Other Changes Impoert Excel</a></li>
		<li><a href="#">L-T4 Cosumm Update</a></li>
		<li><a href="#">LT Bill BackOffice</a></li>
		<li><a href="#">Month Close</a></li>
		<li><a href="#">Other Adgusments</a></li>
		<li><a href="#">Other Changes</a></li>
        <li class="readmore"><a href="#">Readmore</a></li>
        
        </ul>
        </div>--%>
                <div class="clear pad_10">
                </div>
            </div>
        </div>
        <%--<div class="right-content">
            <div class="pendingapproval">
                <h2>
                    Pending for Approval
                </h2>
                <ul>
                    <asp:Repeater ID="rptrPendingList" runat="server" OnItemDataBound="rptrPendingList_ItemDataBound">
                        <ItemTemplate>
                            <li>
                                <asp:LinkButton ID="lbtnFeature" runat="server" CommandArgument='<%#Eval("Feature")%>'
                                    CommandName='<%#Eval("PENDING_KEY")%>' Text="--"></asp:LinkButton></li>
                            
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
            <div id="lblMsgApproval" style="text-align: center;" visible="false" runat="server">
                <asp:Label runat="server" ID="lblNoApprovals" Text="No pending approvals are here."></asp:Label></div>
            <div class="fltr more" runat="server" visible="false" id="divPendingmore" style="">
                <asp:HyperLink ID="HyperLink1" runat="server" Text="<%$Resources:Resource ,READMORE %>"></asp:HyperLink></div>
            <div class="clear pad_10">
            </div>
            <h2>
                Dash Board
            </h2>
            <div class="dashboard">
                <ul>
                    <li class="dashleft">New Connection </li>
                    <li>:</li>
                    <li class="dashright">-</li>
                    <li class="clear"></li>
                    <li class="dashleft">New disconnections</li><li>:</li>
                    <li class="dashright">-</li>
                    <li class="clear"></li>
                    <li class="dashleft">Billing Figures </li>
                    <li>:</li>
                    <li class="dashright"></li>
                    <li class="clear"></li>
                    <li class="dashleft">Restored</li><li>:</li>
                    <li class="dashright"></li>
                    <li class="clear"></li>
                    <li class="dashleft">Todays Collection</li><li>:</li>
                    <li class="dashright">-</li>
                    <li class="clear"></li>
                    <li class="dashleft">Todays Adjustments</li><li>:</li>
                    <li class="dashright"></li>
                    <li class="clear"></li>
                </ul>
            </div>
            <div class="clear pad_10">
            </div>
            <div class="clear pad_10">
            </div>
            <div class="clear pad_10">
            </div>
        </div>--%>
    </div>
    <%--<script type="text/javascript">
        $(function () {
            $('#latest').vTicker({
                speed: 700,
                pause: 4000,
                showItems: 1,
                mousePause: true,
                height: 0,
                animate: true,
                margin: 0,
                padding: 0,
                startPaused: false
            });
        });
    </script>--%>
</asp:Content>
