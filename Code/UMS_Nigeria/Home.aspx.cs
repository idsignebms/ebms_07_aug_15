﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using UMS_NigeriaBAL;
using iDSignHelper;
using UMS_NigeriaBE;
using System.Xml;
using UMS_NigriaDAL;

namespace UMS_Nigeria
{
    public partial class Home : System.Web.UI.Page
    {
        #region Members
        UMS_Service _ObjUMS_Service = new UMS_Service();
        SideMenusBAL _objMenuBAL = new SideMenusBAL();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        string Key = UMS_Resource.HOMEPAGE_AFTERLOGIN;
        NotificationsBE _ObjNotificationsBE = new NotificationsBE();
        NotificationsBAL _objNotificationsBAL = new NotificationsBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        MastersBAL _ObjMastersBAL = new MastersBAL();
        MastersBE _ObjMastersBE = new MastersBE();
        MastersListBE _ObjMastersListBE = new MastersListBE();
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            try
            {

                if (Session[UMS_Resource.SESSION_LOGINID] == null)
                    Response.Redirect(UMS_Resource.DEFAULT_PAGE);


            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack && Session[UMS_Resource.SESSION_LOGINID] != null)
            {                
                    BindNews();                
                //GetApprovalsList();
            }
        }
        #endregion

        #region Methods

        public string UserNavigation()
        {
            try
            {
                SideMenusBE _ObjSideMenusBE = new SideMenusBE();

                //  _ObjSideMenusBe.Role_Id = 8;
                XmlDocument resultXml = new XmlDocument();
                _ObjSideMenusBE.Role_Id = Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID]);
                _ObjSideMenusBE.PageName = "HomePage";
                resultXml = _objMenuBAL.GetNewMenusBAL(_ObjSideMenusBE);

                SideMenusListBE _ObjSideMenusListBE = new SideMenusListBE();
                _ObjSideMenusListBE = _ObjiDsignBAL.DeserializeFromXml<SideMenusListBE>(resultXml);

                MainMenuListBE _ObjMainMenuListBE = new MainMenuListBE();
                _ObjMainMenuListBE = _ObjiDsignBAL.DeserializeFromXml<MainMenuListBE>(resultXml);

                int PageCount = 0;
                string strNavigation = string.Empty;
                for (var i = 0; i < _ObjMainMenuListBE.items.Count; i++)
                {
                    _ObjSideMenusBE = (SideMenusBE)_ObjMainMenuListBE.items[i];
                    string path = _ObjSideMenusBE.Path;
                    string name = _ObjSideMenusBE.Main_Menu_Text;
                    int MainMenuId = _ObjSideMenusBE.Main_Menu_Id;
                    string cssclassAdvance = "margin:0px;";
                    string cssclass = _ObjSideMenusBE.CssClassName;
                    if (cssclass == "")
                    {
                        cssclass = "nav-home";
                    }
                    if (PageCount <= 2)
                    {
                        cssclassAdvance = (PageCount == 2) ? cssclassAdvance : "";
                    }
                    else
                    {
                        strNavigation += "<div class='clear pad_10'></div>";
                        PageCount = 0; cssclassAdvance = "";
                    }
                    if (_ObjSideMenusBE.HasSubMenu == false)
                    {
                        //strNavigation += "<li><a>" + name + "</a></li>";
                        //strNavigation += "<div class='bildblock' style='" + cssclassAdvance + "'><h2 class='" + cssclass + "'>" + name + "</h2><ul><li><a href='" + path + "?" + UMS_Resource.PageId + "=" + _ObjiDsignBAL.AESEncryptString(_ObjSideMenusBE.Page_Id.ToString()) + "'>" + name + "</a></li></ul></div> ";//ID-065 Bhimaraju
                        strNavigation += "<div class='bildblock' style='" + cssclassAdvance + "'><h2 class='" + cssclass + "'>" + name + "</h2><ul><li><a href='" + path + "?" + UMS_Resource.PageId + "=" + _ObjiDsignBAL.Encrypt(_ObjSideMenusBE.Page_Id.ToString()) + "'>" + name + "</a></li></ul></div> ";
                    }
                    else if (_ObjSideMenusListBE.items.Count > 0)
                    {
                        int SubMenuCount = Convert.ToInt32(UMS_Resource.SUBMENUCOUNT_HOMEPAGE);
                        int Result = 0, ReadMore = 0;
                        string subname = string.Empty;
                        strNavigation += "<div class='bildblock' style='" + cssclassAdvance + "'><h2 class='" + cssclass + "'>" + name + "</h2><ul> ";

                        for (int item = 0; item < _ObjSideMenusListBE.items.Count; item++)
                        {
                            subname = _ObjSideMenusListBE.items[item].SubMenu_Text;
                            string path1 = _ObjSideMenusListBE.items[item].Path;
                            int submenuid = _ObjSideMenusListBE.items[item].Main_Menu_Id;
                            string page_Id = _ObjSideMenusListBE.items[item].Page_Id.ToString();
                            if (MainMenuId == submenuid)
                            {
                                if (Result < SubMenuCount)
                                {
                                    if (name.Contains("Forums"))
                                        //strNavigation += "<li><a>" + subname + "</a></li>";
                                        strNavigation += "<li><a href='" + path1 + "' target='_blank'>" + subname + "</a></li>";
                                    else
                                    {
                                        if (page_Id != "")
                                            //strNavigation += "<li><a>" + subname + "</a></li>";                                    
                                            strNavigation += "<li ><a href='" + path1 + "'>" + subname + "</a></li>";
                                        //strNavigation += "<li class='radius'><a href='" + path1 + "?" + UMS_Resource.PageId + "=" + _ObjiDsignBAL.Encrypt(page_Id) + "'>" + subname + "</a></li>";
                                        else
                                            //strNavigation += "<li><a>" + subname + "</a></li>";
                                            strNavigation += "<li ><a href='" + path1 + "'>" + subname + "</a></li>";
                                    }
                                    Result += 1;
                                }
                                else
                                {
                                    if (ReadMore == 0)
                                    {
                                        // strNavigation += "<li class='readmore'><a href='" + UMS_Resource.READMOREPAGE_HOME + "?" + UMS_Resource.MENUID_HOME + "=" + _ObjiDsignBAL.AESEncryptString(Convert.ToString(MainMenuId)) + "'>" + Resource.READMORE + "</a></li>";//ID-065 Bhimaraju
                                        strNavigation += "<li class='readmore'><a href='" + UMS_Resource.READMOREPAGE_HOME + "?" + UMS_Resource.MENUID_HOME + "=" + _ObjiDsignBAL.Encrypt(Convert.ToString(MainMenuId)) + "'>" + Resource.READMORE + "</a></li>";
                                        ReadMore++;
                                    }

                                }
                            }

                        }
                        strNavigation += "</ul></div>";
                    }
                    PageCount++;

                }
                return strNavigation;

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            return string.Empty;
        }
        private void Message(string Message, string MessageType)
        {
            try
            {
                //_ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        private void BindNews()
        {
            _ObjNotificationsBE.PageSize = Convert.ToInt32(GlobalConstants.HOME_NEWS_COUNT);
            _ObjNotificationsBE.PageNo = 1;
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
            {
                _ObjNotificationsBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            }
            _ObjNotificationsBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            XmlDocument ResultantXml = _objNotificationsBAL.GetNotificationsBAL(_ObjNotificationsBE);
            NotificationsListBE objNotificationList = _ObjiDsignBAL.DeserializeFromXml<NotificationsListBE>(ResultantXml);
            rptrNews.DataSource = _ObjiDsignBAL.ConvertListToDataSet<NotificationsBE>(objNotificationList.Items).Tables[0];
            rptrNews.DataBind();
            if (objNotificationList.Items.Count > 0)
            {
                divLatestNewsDynamic.Visible = true;
                divLatestNewsStatic.Visible = false;
                if (objNotificationList.Items[0].TotalRecords > Convert.ToInt32(GlobalConstants.HOME_NEWS_LIMIT))
                    litBusinessUnit.Visible = true;
            }
            else
            {
                divLatestNewsDynamic.Visible = false;
                divLatestNewsStatic.Visible = true;
            }
        }
        //public void GetApprovalsList()
        //{
        //    _ObjMastersListBE = _ObjiDsignBAL.DeserializeFromXml<MastersListBE>(_ObjMastersBAL.GetApprovalsListBAL(_ObjMastersBE));

        //    var query = (from item in _ObjMastersListBE.Items.AsEnumerable()
        //                 select item).ToList().Take(5);
        //    //var topTwo = query.Take(5);

        //    //if (_ObjMastersListBE != null)
        //    //{
        //    if (query.Count() > 0)
        //    {
        //        lblMsgApproval.Visible = false;                
        //        rptrPendingList.DataSource = query;
        //        rptrPendingList.DataBind();
        //        if (_ObjMastersListBE.Items.Count > Convert.ToInt32(GlobalConstants.PENDING_APPROVAL_LIMIT))
        //            divPendingmore.Visible = true;
        //    }
        //    else
        //    {
        //        lblMsgApproval.Visible = true;
        //    }
        //    //}
        //}
        protected void rptrPendingList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lbtnFeature = (LinkButton)e.Item.FindControl("lbtnFeature");
                if (lbtnFeature.CommandArgument == NotificationFeature.Bill.ToString())
                {
                    lbtnFeature.Text = string.Format(Resource.BILL_PENDING, lbtnFeature.CommandName);
                }
                else if (lbtnFeature.CommandArgument == NotificationFeature.Batch.ToString())
                {
                    lbtnFeature.Text = string.Format(Resource.BATCH_PENDING, lbtnFeature.CommandName);
                }
            }
        }
        #endregion
    }
}