﻿function Trim(str) {
    while (str.substring(0, 1) == ' ') // check for white spaces from beginning
    {
        str = str.substring(1, str.length);
    }
    while (str.substring(str.length - 1, str.length) == ' ') // check white space from end
    {
        str = str.substring(0, str.length - 1);
    }
    return str;
}

//DropDownList Validation
function DropDownlistValidations(DropDownvalue, Field) {
    if (DropDownvalue.selectedIndex == 0) {
        alert("Select " + Field + "");
        DropDownvalue.focus();
        return false;
    }
}

//Empty TextBox validation 
function TextBoxValidation(Textboxvalues, Field) {

    if (Trim(Textboxvalues.value) == "") {
        alert("Enter " + Field + "");
        Textboxvalues.focus();
        return false;
    }
}

//Empty TextBox validation displays in pop up Raja-ID065 
function TextBoxValidationInPopup(popup, lblmsg, Textboxvalues, Field) {

    if (Trim(Textboxvalues.value) == "") {
        //alert("Enter " + Field + "");
        lblmsg.innerHTML = "Enter " + Field + "";
        lblmsg.style.display = "block";
        popup.show();
        Textboxvalues.focus();
        return false;
    }
}

function DropDownlistValidationsInPopup(popup, lblmsg, DropDownvalue, Field) {
    if (DropDownvalue.selectedIndex == 0) {
        //alert("Select " + Field + "");
        lblmsg.innerHTML = "Please select " + Field + "";
        popup.show();
        DropDownvalue.focus();
        return false;
    }
}

//Empty TextBox Contact Length validation displays in pop up Raja-ID065 

function ContactNoLengthInPopup(popup, lblmsg, TextBox, TextLength) {
    if (TextBox.value.length < TextLength) {
        lblmsg.innerHTML = "Contact no should be " + TextLength + " characters.";
        popup.show();
        TextBox.focus();
        return false;
    }
}

function ContactNoLengthMinMax(TextBox, MinLength, MaxLength, lblmsg, txtCode, CodeLength) {
    lblmsg.innerHTML = "";
    if (TextBox.value.length < MinLength) {
        lblmsg.innerHTML = "Contact no should be Minimum " + MinLength + " characters.";
        lblmsg.style.display = "block";
        TextBox.style.border = "1px solid red";
        //TextBox.focus();
        return false;
    }
    if (TextBox.value.length > MaxLength) {
        lblmsg.innerHTML = "Contact no should be Maximum " + MaxLength + " characters.";
        lblmsg.style.display = "block";
        TextBox.style.border = "1px solid red";
        //TextBox.focus();
        return false;
    }
    if (TextBoxValidationlbl(txtCode, lblmsg, 'Code') == false) {
        return false;
    }
    else if (txtCode.value.length > CodeLength) {
        lblmsg.innerHTML = "Code should be Maximum " + CodeLength + " characters.";
        lblmsg.style.display = "block";
        txtCode.style.border = "1px solid red";
        //txtCode.focus();
        return false;
    }
}

function ContactNoLengthMinMaxInPopup(popup, lblmsg, TextBox, MinLength, MaxLength, txtCode, CodeLength) {
    if (TextBox.value.length < MinLength) {
        lblmsg.innerHTML = "Contact no should be Minimum " + MinLength + " characters.";
        lblmsg.style.display = "block";
        popup.show();
        //TextBox.focus();
        return false;
    }
    if (TextBox.value.length > MaxLength) {
        lblmsg.innerHTML = "Contact no should be Maximum " + MaxLength + " characters.";
        lblmsg.style.display = "block";
        popup.show();
        //TextBox.focus();
        return false;
    }
    if (TextBoxValidationInPopup(popup, lblmsg, txtCode, 'Code') == false) {
        return false;
    }
    else if (txtCode.value.length > CodeLength) {
        lblmsg.innerHTML = "Code should be Maximum " + CodeLength + " characters.";
        lblmsg.style.display = "block";
        popup.show();
        //txtCode.focus();
        return false;
    }
}

function TextboxZipCodelbl(TextBox, lblmsg, MaxLength) {
    lblmsg.innerHTML = "";
    if (TextBox.value.length != MaxLength) {
        lblmsg.innerHTML = "Zip code should be " + MaxLength + " characters.";
        lblmsg.style.display = "block";
        TextBox.style.border = "1px solid red";
        //        TextBox.focus();
        return false;
    }
    else {
        TextBox.style.border = "1px solid #8E8E8E";
    }
}

function CustomerTextboxZipCodelbl(TextBox, lblmsg, MaxLength, MinLength) {
    if (TextBox.value.length > MaxLength && TextBox.value.length != "") {
        lblmsg.innerHTML = "Zip code should be Maximum " + MaxLength + " characters.";
        lblmsg.style.display = "block";
        TextBox.style.border = "1px solid red";
        //        TextBox.focus();
        return false;
    }
    else if (TextBox.value.length < MinLength && TextBox.value.length != "") {
        lblmsg.innerHTML = "Zip code should be atleast " + MinLength + " characters.";
        lblmsg.style.display = "block";
        TextBox.style.border = "1px solid red";
        //        TextBox.focus();
        return false;
    }
    else {
        return true;
    }
}

function TextboxZipCodelblInPopup(popup, lblmsg, TextBox, MaxLength) {
    lblmsg.innerHTML = "";
    if (TextBox.value.length != MaxLength) {
        lblmsg.innerHTML = "Zip code should be " + MaxLength + " characters.";
        popup.show();
        return false;
    }
}

function CustomerTextboxZipCodelblInPopup(popup, lblmsg, TextBox, MaxLength, MinLength) {
    lblmsg.innerHTML = "";

    if (TextBox.value.length > MaxLength && TextBox.value.length != "") {
        lblmsg.innerHTML = "Zip code should be Maximum " + MaxLength + " characters.";
        popup.show();
        return false;
    }
    else if (TextBox.value.length < MinLength && TextBox.value.length != "") {
        lblmsg.innerHTML = "Zip code should be atleast " + MinLength + " characters.";
        popup.show();
        return false;
    }
    else {
        return true;
    }
}

function TextmsgInPopup(popup, lblmsg, TextBox, TextLength, TextMsg) {
    if (TextBox.value.length < TextLength) {
        lblmsg.innerHTML = TextMsg;
        popup.show();
        TextBox.focus();
        return false;
    }
}

function IsTextBoxEmpty(txt) {
    if (Trim(txt.value) == "") {
        return false;
    }
}

function TextBoxValidationlbl(txt, lbl, field) {
    //txt.className = "normalfld";
    lbl.innerHTML = "";
    lbl.style.display = "none";
    if (Trim(txt.value) == "" || Trim(txt.value) == "0") {
        lbl.innerHTML = "Enter " + field;
        lbl.style.display = "block";
        txt.style.border = "1px solid red";
        //txt.focus();
        return false;
    }
    else {
        txt.style.border = "1px solid #8E8E8E";
    }
}
//Empty TextBox validation displays in pop up Raja-ID065 

function TrimLeadingZeros(s) {
    while (s.substr(0, 1) == '0' && s.length > 1) { s = s.substr(1, 9999); }
    return s;
}

function TextBoxValidationlblZeroAccepts(txt, lbl, field) {
    //txt.className = "normalfld";
    lbl.innerHTML = "";
    lbl.style.display = "none";
    if (Trim(txt.value) == "") {
        lbl.innerHTML = "Enter " + field;
        lbl.style.display = "block";
        txt.style.border = "1px solid red";
        return false;
    }
    else {
        txt.style.border = "1px solid #8E8E8E";
    }
}

function TextBoxBlurValidationlbl(txt, lblId, field) {
    var lbl = document.getElementById(lblId);
    //txt.className = "normalfld";
    lbl.innerHTML = "";
    lbl.style.display = "none";
    if (Trim(txt.value) == "" || Trim(txt.value) == "0") {
        lbl.innerHTML = "Enter " + field;
        lbl.style.display = "block";
        txt.style.border = "1px solid red";
        //txt.focus();
        return false;
    }
    else {
        txt.style.border = "1px solid #8E8E8E";
        lbl.style.display = "none";
    }
}

function TextBoxBlurValidationlblWithNoZero(txt, lblId, field) {
    var lbl = document.getElementById(lblId);
    //txt.className = "normalfld";
    lbl.innerHTML = "";
    lbl.style.display = "none";
    if (Trim(txt.value) == "" || Trim(txt.value) == "0") {
        lbl.innerHTML = "Enter " + field;
        lbl.style.display = "block";
        txt.style.border = "1px solid red";
        //txt.focus();
        return false;
    }
    else if (parseFloat(txt.value) == 0) {
        lbl.innerHTML = "Enter valid " + field;
        lbl.style.display = "block";
        txt.style.border = "1px solid red";
        //txt.focus();
        return false;
    }
    else {
        txt.style.border = "1px solid #8E8E8E";
        lbl.style.display = "none";
    }
}

function TextBoxBlurValidationlblZeroAccepts(txt, lblId, field) {
    var lbl = document.getElementById(lblId);
    //txt.className = "normalfld";
    lbl.innerHTML = "";
    lbl.style.display = "none";
    if (Trim(txt.value) == "") {
        lbl.innerHTML = "Enter " + field;
        lbl.style.display = "block";
        txt.style.border = "1px solid red";
        //txt.focus();
        return false;
    }
    else {
        txt.style.border = "1px solid #8E8E8E";

    }
}

//onchange Event For drop downlist
function DropDownlistOnChangelbl(ddl, lblId, field) {
    var lbl = document.getElementById(lblId);
    lbl.innerHTML = "";
    lbl.style.display = "none";

    
    if (ddl.selectedIndex == 0) {
        //alert(ddl.selectedIndex);
        lbl.innerHTML = "Select " + field;
        lbl.style.display = "inline";
        return false;
    }
}
//RadioButtonList lbl Check
function RadioButtonListlbl(rbl, lblId, field) {
    var lbl = document.getElementById(lblId);
    var listitems = rbl.getElementsByTagName("input");
    var IsSelected = false;
    for (var i = 0; i < listitems.length; i++) {
        if (listitems[i].checked) {
            IsSelected = true;
            break;
        }
    }
    lbl.innerHTML = "";
    lbl.style.display = "none";
    if (!IsSelected) {
        lbl.innerHTML = "Select" + field;
        lbl.style.display = "inline";
        return false;
    }
}

//RadioButtonList lbl Check Value
function RadioButtonListlblValue(rbl, lblId, field) {

    var lbl = document.getElementById(lblId);
    var listitems = rbl.getElementsByTagName("input");

    var IsSelected = false;
    for (var i = 0; i < listitems.length; i++) {
        if (listitems[0].checked) {
            IsSelected = true;
            break;
        }
    }
    lbl.innerHTML = "";
    lbl.style.display = "none";
    if (!IsSelected) {
        lbl.innerHTML = "" + field;
        lbl.style.display = "inline";
        return false;
    }
}

function DropDownlistValidationlbl(ddl, lbl, field) {
    lbl.innerHTML = "";
    lbl.style.display = "none";
    if (ddl.length > 0) {
        if (ddl.selectedIndex == 0) {
            lbl.innerHTML = "Select " + field;
            lbl.style.display = "inline";
            return false;
        }
    }
    else {
        lbl.innerHTML = "Select " + field;
        lbl.style.display = "inline";
        return false;
    }
}

function TextAreaValidation(Textboxvalues, Field) {

    if (Trim(Textboxvalues.value.split('\n').join('')) == "") {
        alert("Enter " + Field + "");
        Textboxvalues.focus();
        return false;
    }
}

//ListBox Validation
function ValidateListBox(ListBox, Field) {
    var count = 0;
    var Options = ListBox.getElementsByTagName('option');
    if (Options.length == 0) {
        alert("There is no " + Field);
        return false;
    }
    else {
        for (var i = 0; i < Options.length; i++) {
            if (Options[i].selected == true) {
                count = 1;
                break;
            }
        }
        if (count == 0) {
            alert("Select atleast one" + Field);
            return false;
        }
    }
}


function RadioButtonValidation(elementRef, Field) {
    var input = elementRef.getElementsByTagName("input");
    var countrd = 0;
    for (var i = 0; i < input.length; i++) {
        switch (input[i].type) {
            case "radio": //Radiio Button
                if (input[i].checked) {
                    countrd = 1;
                }
                break;
        }
    }
    if (countrd == 0 || input == null) {
        alert("Select " + Field + "");
        return false;
    }
}

//--------------validate Uploading Files---------------------
function OnUpload(FileUpload, validFiles, alertMessage) {
    var source = FileUpload.value;
    var IsValid = false;
    var ext = source.substring(source.lastIndexOf(".") + 1, source.length).toLowerCase();
    for (var i = 0; i < validFiles.length; i++) {
        if (validFiles[i] == ext) {
            IsValid = true;
            break;
        }
    }
    if (!IsValid) {
        alert("Select appropriate" + alertMessage);
        FileUpload.focus();
        return false;
    }
}
//--------------validate Uploading Files---------------------
function OnUploadpopup(mpeAlert, lblAlertmsg, FileUpload, validFiles, alertMessage) {
    var source = FileUpload.value;
    var IsValid = false;
    var ext = source.substring(source.lastIndexOf(".") + 1, source.length).toLowerCase();
    for (var i = 0; i < validFiles.length; i++) {
        if (validFiles[i] == ext) {
            IsValid = true;
            break;
        }
    }
    if (!IsValid) {
        //alert("Select appropriate" + alertMessage);
        lblAlertmsg.innerHTML = "Select appropriate " + alertMessage;
        mpeAlert.show();
        FileUpload.focus();
        return false;
    }
}

function OnAsyncUpload(source, validFiles) {

    //var source = FileUpload.value;

    var ext = source.substring(source.lastIndexOf(".") + 1, source.length).toLowerCase();
    for (var i = 0; i < validFiles.length; i++) {
        if (validFiles[i] == ext)
            return false;
        break;
    }
    if (i >= validFiles.length) {
        if (!/(\.pdf)$/i.test(source)) {
            alert("Please upload PDF format file only.");
        }

        FileUpload.focus();
        return false;
    }

}
//----------------Check Box List Validation-----------------------

function ValidateCbl(CHK, Field) {
    if (CHK != null) {
        var checkbox = CHK.getElementsByTagName("input");
        var counter = 0;
        for (var i = 0; i < checkbox.length; i++) {
            if (checkbox[i].checked) {
                counter = 1;
                break;
            }
        }
        if (1 > counter) {
            alert("Select atleast One " + Field);
            return false;
        }
    }
    else {
        alert("Select atleast One " + Field);
        return false;
    }
    return true;
}

function ValidateCblInPopup(CHK, Field) {
    if (CHK != null) {
        var checkbox = CHK.getElementsByTagName("input");
        var counter = 0;
        for (var i = 0; i < checkbox.length; i++) {
            if (checkbox[i].checked) {
                counter = 1;
                break;
            }
        }
        if (1 > counter) {
            alert("Select atleast One " + Field);
            return false;
        }
    }
    else {
        alert("Select atleast One " + Field);
        return false;
    }
    return true;
}

function ValidateCblInPopup(mpe, lbl, CHK, Field) {
    if (CHK != null) {
        var checkbox = CHK.getElementsByTagName("input");
        var counter = 0;
        for (var i = 0; i < checkbox.length; i++) {
            if (checkbox[i].checked) {
                counter = 1;
                break;
            }
        }
        if (1 > counter) {
            //alert("Select atleast One " + Field);
            mpe.show();
            lbl.innerHTML = "Select atleast one " + Field;
            return false;
        }
    }
    else {
        //alert("Select atleast One " + Field);
        mpe.show();
        lbl.innerHTML = "Select atleast one" + Field;
        return false;
    }
    return true;
}

//---------------------Contact Number----------------
function ContactNoLength(TextBox) {
    if (TextBox.value.length < 10) {
        alert("Enter correct phone number");
        TextBox.focus();
        return false;
    }
}

function MoblieNoLength(TextBox, length, value) {
    if (TextBox.value.length < length) {
        alert("Enter valid " + value);
        TextBox.focus();
        return false;
    }
}
//---------------------------------------------------------------
function UserIdLength(TextBox) {
    if (TextBox.value.length < 6) {
        alert("Enter userid atleast 6 characters");
        TextBox.focus();
        return false;
    }
}

function TextBoxValidLength(TextBox, value) {
    if (TextBox.value.length != 6) {
        alert("Enter valid " + value);
        TextBox.focus();
        return false;
    }
}

function TextBoxValidLengthValue(TextBox, value, msg) {
    if (TextBox.value.length != value) {
        alert("Enter" + " " + value + " " + msg);
        TextBox.focus();
        return false;
    }
}

function TextBoxValidContactLbl(TextBox, value, lbl, charLength) {
    //TextBox.className = "normalfld";
    lbl.innerHTML = "";
    lbl.style.display = "none";
    if (TextBox.value.length < charLength) {
        lbl.innerHTML = "Enter valid " + value;
        lbl.style.display = "block";
        TextBox.style.border = "1px solid red";
        //TextBox.focus();
        return false;
    }
    else {
        TextBox.style.border = "1px solid #8E8E8E";

    }
}

function InitialMeterReading(TextBox, lbl, charLength) {
    //TextBox.className = "normalfld";
    lbl.innerHTML = "";
    lbl.style.display = "none";
    if (TextBox.value.length != charLength) {
//        alert(charLength);
//        alert(lbl);
//        alert(TextBox);
        lbl.innerHTML = "Initial reading must be" + charLength + "Charecters";
        lbl.style.display = "block";
        TextBox.style.border = "1px solid red";
        //TextBox.focus();
        return false;
    }
    else {
        TextBox.style.border = "1px solid #8E8E8E";
    }
}

function TextBoxLengthValidationLbl(TextBox, value, lbl, charLength) {
    lbl.innerHTML = ""; //TextBox.className = "normalfld";
    lbl.style.display = "none";
    if (TextBox.value.length < charLength) {
        lbl.innerHTML = "Enter " + value;
        lbl.style.display = "block";
        TextBox.style.border = "1px solid red";
        //TextBox.focus();
        return false;
    }
    else {
        TextBox.style.border = "1px solid #8E8E8E";

    }
}

function TextBoxValidLength(TextBox, value) {
    if (TextBox.value.length != 6) {
        alert("Enter valid " + value);
        TextBox.focus();
        return false;
    }
}
//---------------validate Email Id---------------ID065-Raja
function EmailValidation(txtEmailId) {

    var reg = /^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i; ;

    if (reg.test(txtEmailId.value) == false) {
        //output.innerHTML = "Invalid Email Id";
        //alert("Invalid Email Id");
        return false;
    }
}

//---------------validate Email Id---------------
function validateEmail(str) {
    var at = "@"
    var dot = "."
    var lat = str.indexOf(at)
    var lstr = str.length
    var ldot = str.indexOf(dot)
    if (str.indexOf(at) == -1) {
        //alert("Please enter valid E-mail ID")
        return false
    }

    if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr) {
        //alert("Please enter valid E-mail ID")
        return false
    }

    if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr) {
        //alert("Please enter valid E-mail ID")
        return false
    }

    if (str.indexOf(at, (lat + 1)) != -1) {
        // alert("Please enter valid E-mail ID")
        return false
    }

    if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot) {
        //alert("Please enter valid E-mail ID")
        return false
    }

    if (str.indexOf(dot, (lat + 2)) == -1) {
        //alert("Please enter valid E-mail ID")
        return false
    }

    if (str.indexOf(" ") != -1) {
        //alert("Please enter valid E-mail ID")
        return false
    }

    return true
}

//---------------validate Email Id with text---------------//{Vineela-ID017}
function validateEmailWithCustomText(str, Text) {

    var at = "@"
    var dot = "."
    var lat = str.indexOf(at)
    var lstr = str.length
    var ldot = str.indexOf(dot)
    if (str.indexOf(at) == -1) {
        alert("Invalid " + Text + " E-mail ID")
        return false
    }

    if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr) {
        alert("Invalid " + Text + " E-mail ID")
        return false
    }

    if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr) {
        alert("Invalid " + Text + " E-mail ID")
        return false
    }

    if (str.indexOf(at, (lat + 1)) != -1) {
        alert("Invalid " + Text + " E-mail ID")
        return false
    }

    if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot) {
        alert("Invalid " + Text + " E-mail ID")
        return false
    }

    if (str.indexOf(dot, (lat + 2)) == -1) {
        alert("Invalid " + Text + " E-mail ID")
        return false
    }

    if (str.indexOf(" ") != -1) {
        alert("Invalid " + Text + " E-mail ID")
        return false
    }

    return true
}

//------------CheckBox List Validation-----------------

function ListValidationForEnabledItems(cblRef, Field) {
    var input = cblRef.getElementsByTagName("input");
    var count = 0;

    for (var i = 0; i < input.length; i++) {
        switch (input[i].type) {
            case "checkbox": //CheckBox
                if (input[i].checked) {
                    if (input[i].disabled == false) {
                        count = 1;
                    }
                }
                break;
            case "radio": //Radiio Button
                if (input[i].checked) {
                    if (input[i].disabled == false) {
                        count = 1;
                    }
                }
                break;
        }
    }
    if (count == 0) {
        alert("Select " + Field);
        return false;
    }
}


//Date Validation
var dtCh = "/";
var minYear = 1961;
var maxYear = 2050;
function isInteger(s) {
    var i;
    for (i = 0; i < s.length; i++) {
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag) {
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++) {
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary(year) {
    // February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ((!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28);
}
function DaysArray(n) {
    var DaysCurrentArray = new Array();
    for (var i = 1; i <= n; i++) {
        DaysCurrentArray[i] = 31
        if (i == 4 || i == 6 || i == 9 || i == 11) { DaysCurrentArray[i] = 30 }
        if (i == 2) { DaysCurrentArray[i] = 29 }
    }
    return DaysCurrentArray;
}



function isDateLbl(dtStr, txt, lblId) {
    var lbl = document.getElementById(lblId);
    lbl.innerHTML = "";
    lbl.style.display = "none";
    var IsValidDate = true;
    var daysInMonth = DaysArray(12)
    var pos1 = dtStr.indexOf(dtCh)
    var pos2 = dtStr.indexOf(dtCh, pos1 + 1)
    var strDay = dtStr.substring(0, pos1)
    var strMonth = dtStr.substring(pos1 + 1, pos2)
    var strYear = dtStr.substring(pos2 + 1)
    strYr = strYear
    if (strDay.charAt(0) == "0" && strDay.length > 1)
        strDay = strDay.substring(1)
    if (strMonth.charAt(0) == "0" && strMonth.length > 1)
        strMonth = strMonth.substring(1)
    for (var i = 1; i <= 3; i++) {
        if (strYr.charAt(0) == "0" && strYr.length > 1) strYr = strYr.substring(1)
    }
    month = parseInt(strMonth)
    day = parseInt(strDay)
    year = parseInt(strYr)
    if (pos1 == -1 || pos2 == -1) {
        IsValidDate = false;
    }
    else if (strMonth.length < 1 || month < 1 || month > 12) {
        IsValidDate = false;
    }
    else if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month]) {
        IsValidDate = false;
    }
    else if (strYear.length != 4 || year == 0 || year < minYear || year > maxYear) {
        IsValidDate = false;
    }
    else if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || isInteger(stripCharsInBag(dtStr, dtCh)) == false) {
        IsValidDate = false;
    }

    if (!IsValidDate) {
        //txt.className = "normalfld";
        txt.style.border = "1px solid red";
        //txt.focus();
        lbl.innerHTML = "Enter a valid date";
        lbl.style.display = "block";
        return false;
    }
    else {
        txt.style.border = "1px solid #8E8E8E";

    }
}
/*---------isdatevalidation----------------*/
function isDate(dtStr) {
    var daysInMonth = DaysArray(12)
    var pos1 = dtStr.indexOf(dtCh)
    var pos2 = dtStr.indexOf(dtCh, pos1 + 1)
    var strDay = dtStr.substring(0, pos1)
    var strMonth = dtStr.substring(pos1 + 1, pos2)
    var strYear = dtStr.substring(pos2 + 1)
    strYr = strYear
    if (strDay.charAt(0) == "0" && strDay.length > 1)
        strDay = strDay.substring(1)
    if (strMonth.charAt(0) == "0" && strMonth.length > 1)
        strMonth = strMonth.substring(1)
    for (var i = 1; i <= 3; i++) {
        if (strYr.charAt(0) == "0" && strYr.length > 1) strYr = strYr.substring(1)
    }
    month = parseInt(strMonth)
    day = parseInt(strDay)
    year = parseInt(strYr)

    if (pos1 == -1 || pos2 == -1) {
        alert("The date format should be : dd/mm/yyyy")
        return false
    }
    if (strMonth.length < 1 || month < 1 || month > 12) {
        alert("Enter a valid month")
        return false
    }
    if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month]) {
        alert("Enter a valid date")
        return false
    }
    if (strYear.length != 4 || year == 0 || year < minYear || year > maxYear) {
        alert("Enter a valid year between " + minYear + " and " + maxYear)
        return false
    }
    if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || isInteger(stripCharsInBag(dtStr, dtCh)) == false) {
        alert("Enter a valid date")
        return false
    }
    return true
}

/*---------isdatevalidation----------------*/
function isDateInPopup(mpeAlert, lblAlertMsg, dtStr) {
    var daysInMonth = DaysArray(12)
    var pos1 = dtStr.indexOf(dtCh)
    var pos2 = dtStr.indexOf(dtCh, pos1 + 1)
    var strDay = dtStr.substring(0, pos1)
    var strMonth = dtStr.substring(pos1 + 1, pos2)
    var strYear = dtStr.substring(pos2 + 1)
    strYr = strYear
    if (strDay.charAt(0) == "0" && strDay.length > 1)
        strDay = strDay.substring(1)
    if (strMonth.charAt(0) == "0" && strMonth.length > 1)
        strMonth = strMonth.substring(1)
    for (var i = 1; i <= 3; i++) {
        if (strYr.charAt(0) == "0" && strYr.length > 1) strYr = strYr.substring(1)
    }
    month = parseInt(strMonth)
    day = parseInt(strDay)
    year = parseInt(strYr)

    if (pos1 == -1 || pos2 == -1) {
        //alert("The date format should be : dd/mm/yyyy")
        lblAlertMsg.innerHTML = "The date format should be : dd/mm/yyyy";
        mpeAlert.show();
        return false
    }
    if (strMonth.length < 1 || month < 1 || month > 12) {
        //alert("Enter a valid month")
        lblAlertMsg.innerHTML = "Enter a valid month";
        mpeAlert.show();
        return false
    }
    if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month]) {
        //alert("Enter a valid date")
        lblAlertMsg.innerHTML = "Enter a valid date";
        mpeAlert.show();
        return false
    }
    if (strYear.length != 4 || year == 0 || year < minYear || year > maxYear) {
        //alert("Enter a valid year between " + minYear + " and " + maxYear)
        lblAlertMsg.innerHTML = "Enter a valid year between " + minYear + " and " + maxYear;
        mpeAlert.show();
        return false
    }
    if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || isInteger(stripCharsInBag(dtStr, dtCh)) == false) {
        //alert("Enter a valid date")
        lblAlertMsg.innerHTML = "Enter a valid date";
        mpeAlert.show();
        return false
    }
    return true
}

function isDateInPopupForMerterInfo(mpeAlert, lblAlertMsg, dtStr) {
    var daysInMonth = DaysArray(12)
    var pos1 = dtStr.indexOf(dtCh)
    var pos2 = dtStr.indexOf(dtCh, pos1 + 1)
    var strDay = dtStr.substring(0, pos1)
    var strMonth = dtStr.substring(pos1 + 1, pos2)
    var strYear = dtStr.substring(pos2 + 1)
    strYr = strYear
    if (strDay.charAt(0) == "0" && strDay.length > 1)
        strDay = strDay.substring(1)
    if (strMonth.charAt(0) == "0" && strMonth.length > 1)
        strMonth = strMonth.substring(1)
    for (var i = 1; i <= 3; i++) {
        if (strYr.charAt(0) == "0" && strYr.length > 1) strYr = strYr.substring(1)
    }
    month = parseInt(strMonth)
    day = parseInt(strDay)
    year = parseInt(strYr)
    var CDate = new Date();
    var CYear = CDate.getFullYear();
    if (pos1 == -1 || pos2 == -1) {
        //alert("The date format should be : dd/mm/yyyy")
        lblAlertMsg.innerHTML = "The date format should be : dd/mm/yyyy";
        mpeAlert.show();
        return false
    }
    if (strMonth.length < 1 || month < 1 || month > 12) {
        //alert("Enter a valid month")
        lblAlertMsg.innerHTML = "Enter a valid month";
        mpeAlert.show();
        return false
    }
    if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month]) {
        //alert("Enter a valid date")
        lblAlertMsg.innerHTML = "Enter a valid date";
        mpeAlert.show();
        return false
    }
    if (strYear.length != 4 || year == 0 || year < minYear || year > maxYear) {
        //alert("Enter a valid year between " + minYear + " and " + maxYear)
        lblAlertMsg.innerHTML = "Enter a valid year between " + CYear + " and " + maxYear;
        mpeAlert.show();
        return false
    }
    if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || isInteger(stripCharsInBag(dtStr, dtCh)) == false) {
        alert("Enter a valid date")
        lblAlertMsg.innerHTML = "Enter a valid date";
        mpeAlert.show();
        return false
    }
    return true
}


function isDateFromToYear(dtStr, FromYear, ToYear) {
    var daysInMonth = DaysArray(12)
    var pos1 = dtStr.indexOf(dtCh)
    var pos2 = dtStr.indexOf(dtCh, pos1 + 1)
    var strDay = dtStr.substring(0, pos1)
    var strMonth = dtStr.substring(pos1 + 1, pos2)
    var strYear = dtStr.substring(pos2 + 1)
    strYr = strYear
    if (strDay.charAt(0) == "0" && strDay.length > 1)
        strDay = strDay.substring(1)
    if (strMonth.charAt(0) == "0" && strMonth.length > 1)
        strMonth = strMonth.substring(1)
    for (var i = 1; i <= 3; i++) {
        if (strYr.charAt(0) == "0" && strYr.length > 1) strYr = strYr.substring(1)
    }
    month = parseInt(strMonth)
    day = parseInt(strDay)
    year = parseInt(strYr)
    if (pos1 == -1 || pos2 == -1) {
        alert("The date format should be : dd/mm/yyyy")
        return false
    }
    if (strMonth.length < 1 || month < 1 || month > 12) {
        alert("Enter a valid month")
        return false
    }
    if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month]) {
        alert("Enter a valid date")
        return false
    }
    if (strYear.length != 4 || year == 0 || year < FromYear || year > ToYear) {
        alert("Enter a valid year between " + FromYear + " and " + ToYear)
        return false
    }
    if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || isInteger(stripCharsInBag(dtStr, dtCh)) == false) {
        alert("Enter a valid date")
        return false
    }
    return true
}

function GreaterToday(TDate, Message) {
    var now = new Date();
    var entDate = TDate.value.split('/');
    var xdate = entDate[1] + '/' + entDate[0] + '/' + entDate[2];

    var date2 = new Date(xdate);
    if (now <= date2) {
        alert(Message);
        TDate.focus();
        return false;
    }
}

function GreaterTodayInPopup(mpeAlert, lblAlertMsg, TDate, Message) {
    var now = new Date();
    var entDate = TDate.value.split('/');
    var xdate = entDate[1] + '/' + entDate[0] + '/' + entDate[2];

    var date2 = new Date(xdate);
    if (now <= date2) {
        //alert(Message);
        lblAlertMsg.innerHTML = Message;
        mpeAlert.show();
        TDate.value = "";
        TDate.focus();
        return false;
    }
}

function LesserTodayInPopup(mpeAlert, lblAlertMsg, TDate, Message) {
    var now = new Date();
    var entDate = TDate.value.split('/');
    var xdate = entDate[1] + '/' + entDate[0] + '/' + entDate[2];

    var date2 = new Date(xdate);
    if (now >= date2) {
        //alert(Message);
        lblAlertMsg.innerHTML = Message;
        mpeAlert.show();
        TDate.value = "";
        TDate.focus();
        return false;
    }
}

function FromToDate(input1, input2) {
    var inpu1 = input1.split('/');
    var frmDate = inpu1[1] + '/' + inpu1[0] + '/' + inpu1[2]

    var inpu2 = input2.split('/');
    var todate = inpu2[1] + '/' + inpu2[0] + '/' + inpu2[2]


    var date1 = new Date(frmDate);
    var date2 = new Date(todate);

    if (date1 > date2) {
        alert("To date should be greater than or equal to From date");
        return false;
    }
}

function FromToDateInPopup(mpeAlert, lblAlertMsg, input1, input2, lblmsg) {
    var inpu1 = input1.split('/');
    var frmDate = inpu1[1] + '/' + inpu1[0] + '/' + inpu1[2]

    var inpu2 = input2.split('/');
    var todate = inpu2[1] + '/' + inpu2[0] + '/' + inpu2[2]


    var date1 = new Date(frmDate);
    var date2 = new Date(todate);

    if (date1 > date2) {
        //alert("To date should be greater than or equal to From date");
        lblAlertMsg.innerHTML = lblmsg;
        mpeAlert.show();
        return false;
    }
}

function Validate30DaysInPopup(mpeAlert, lblAlertMsg, input1, input2, lblmsg) {
    var inpu1 = input1.split('/');
    var frmDate = inpu1[1] + '/' + inpu1[0] + '/' + inpu1[2]

    var inpu2 = input2.split('/');
    var todate = inpu2[1] + '/' + inpu2[0] + '/' + inpu2[2]
    
    var date1 = new Date(frmDate);
    var date2 = new Date(todate);

    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

    if (diffDays > 30) {
        lblAlertMsg.innerHTML = lblmsg;
        mpeAlert.show();
        return false;
    }
}

//------------JQuery Date Picker-----------------

function DatePicker(txtDate, imgCal) {
    $(txtDate).datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-2:+8"
    });
    $(txtDate).datepicker({ autoSize: true });
    //getter
    var autoSize = $(txtDate).datepicker("option", "autoSize");
    //setter
    $(txtDate).datepicker("option", "autoSize", true);
    $(imgCal).css({ 'cursor': 'pointer', "vertical-align": 'middle' });
    $(imgCal).click(function () {
        $(txtDate).datepicker('show');
    });
}

//---------To clear controls---------------

function ClearControls() {
    for (i = 0; i < document.forms[0].length; i++) {
        doc = document.forms[0].elements[i];
        switch (doc.type) {
            case "text":
                doc.value = "";
                break;

            case "textarea":
                doc.value = "";
                break;

            case "checkbox":
                doc.checked = false;
                break;

            case "radio":
                doc.checked = false;
                break;

            case "select-one":
                doc.options[doc.selectedIndex].selected = false;
                break;

            case "password":
                doc.value = "";
                break;

            case "select-multiple":
                while (doc.selectedIndex != -1) {
                    indx = doc.selectedIndex;
                    doc.options[indx].selected = false;
                }
                doc.selected = false;
                break;

            case "file":
                doc.value = "";
                break;
        }
    }
    return false;
}


function RemoveScrapImages(container) {
    var content = document.getElementById(container);
    var Images = content.getElementsByTagName("IMG");
    for (var j = 0; j < Images.length; j++) {
        if (Images[j].src.search("http://") == -1 || Images[j].src == document.URL) {
            Images[j].src = "http://gateiespsu.com/images/spacer.gif";
            Images[j].height = "1";
            Images[j].width = "1";
        }
        else {
            if (Images[j].src.search("../QuestionImages/") == -1) {

            }
            else {
                var url = Images[j].src.split("/");
                var location = document.location.href;
                if (location.search("Staging") == -1) {
                    if (url[4] != undefined)
                        Images[j].src = "http://gateiespsu.com/QuestionImages/" + url[4];
                }
                else {
                    if (url[5] != undefined)
                        Images[j].src = "http://gateiespsu.com/QuestionImages/" + url[5];
                }
            }
        }
    }
    return false;
}

function IndianCurrencyFormate(x) {
    x = x.toString();
    var afterPoint = '';
    if (x.indexOf('.') > 0)
        afterPoint = x.substring(x.indexOf('.'), x.length);
    x = Math.floor(x);
    x = x.toString();
    var lastThree = x.substring(x.length - 3);
    var otherNumbers = x.substring(0, x.length - 3);
    if (otherNumbers != '')
        lastThree = ',' + lastThree;
    return otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
}

function MillionCurrencyFormate(x) {
    x = x.toString();
    var afterPoint = '';
    if (x.indexOf('.') > 0)
        afterPoint = x.substring(x.indexOf('.'), x.length);
    x = Math.floor(x);
    x = x.toString();
    var lastThree = x.substring(x.length - 3);
    var otherNumbers = x.substring(0, x.length - 3);
    if (otherNumbers != '')
        lastThree = ',' + lastThree;
    return otherNumbers.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + lastThree + afterPoint;
}

function MillionCurrencyFormateWith2Decimals(x) {
    x = x.toString();
    var afterPoint = '';
    if (x.indexOf('.') > 0)
        afterPoint = x.substring(x.indexOf('.'), x.length);
    x = Math.floor(x);
    x = x.toString();
    var lastThree = x.substring(x.length - 3);
    var otherNumbers = x.substring(0, x.length - 3);
    if (otherNumbers != '')
        lastThree = ',' + lastThree;
    return otherNumbers.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + lastThree + afterPoint.substring(afterPoint.indexOf('.'), 3);
}

function Calendar(TextBoxId, ImageCalendarId, YearRange) {
    TextBoxId = "#" + TextBoxId;
    ImageCalendarId = "#" + ImageCalendarId;
    $(TextBoxId).datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: YearRange
        //defaultDate: new Date(1941, 00, 00)
    });
    $(TextBoxId).datepicker({ autoSize: true });
    //getter
    var autoSize = $(TextBoxId).datepicker("option", "autoSize");
    //setter
    $(TextBoxId).datepicker("option", "autoSize", true);
    $(ImageCalendarId).css({ "cursor": "pointer", "vertical-align": "middle" });
    $(ImageCalendarId).click(function () {
        $(TextBoxId).datepicker('show');
    });
    //    $(TextBoxId).attr('tabindex', 0);
}

function CalendarWithClass(TextBox, ImageCalendar, YearRange) {
    TextBox = "." + TextBox;
    ImageCalendar = "." + ImageCalendar;
    $(TextBox).datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: YearRange
        //defaultDate: new Date(1941, 00, 00)
    });
    $(TextBox).datepicker({ autoSize: true });
    //getter
    var autoSize = $(TextBox).datepicker("option", "autoSize");
    //setter
    $(TextBox).datepicker("option", "autoSize", true);
    $(ImageCalendar).css({ "cursor": "pointer", "vertical-align": "middle" });
    $(ImageCalendar).click(function () {
        $(TextBox).datepicker('show');
    });
}

function DisplayMessage(ControlId) {
    ControlId = '#' + ControlId;
    $(ControlId).fadeOut(5000);

}

/*------------------------------Check All check Boxes and Un Check ;-------------------------------------------------Bhimaraju*/


function CheckBoxListSelect(cblCheckAllId, cblListId) {
    var chkBoxCount = cblListId.getElementsByTagName("input");
    for (var i = 0; i < chkBoxCount.length; i++) {
        chkBoxCount[i].checked = cblCheckAllId.checked;
    }
}

function Uncheck(cblCheckAllId, cblListId) {
    var flag = 1;
    var chkBoxCount = cblListId.getElementsByTagName("input");
    for (var i = 0; i < chkBoxCount.length; i++) {
        if (!chkBoxCount[i].checked) {
            flag = 0;
        }
    }

    if (flag == 0)
        cblCheckAllId.checked = false;
    else
        cblCheckAllId.checked = true;
}

/*------------------------------Amount Should seperated with , on TextBox-------------------------------------------------Bhimaraju*/
function IndianCurrencyFormate(X) {
    x = X.toString();
    var afterPoint = '';
    if (x.indexOf('.') > 0)
        afterPoint = x.substring(x.indexOf('.'), x.length);
    x = Math.floor(X);
    x = x.toString();
    var lastThree = x.substring(x.length - 3);
    var otherNumbers = x.substring(0, x.length - 3);
    if (otherNumbers != '')
        lastThree = ',' + lastThree;
    return otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
}

function NewText(obj) {
    var str = "";
    str = obj.value.replace(/,/g, "");
    return obj.value = MillionCurrencyFormate(str);
}

function NewTextWith2Decimals(obj) {
    var str = "";
    str = obj.value.replace(/,/g, "");
    return obj.value = MillionCurrencyFormateWith2Decimals(str);
}

function isNumberKey(evt, txt) {

    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    if ((charCode == 46 && txt.value.split('.').length > 1)) {
        return false;
    }
}
/*------------------------------Autocomplete functionality-------------------------------------------------Neeraj-077*/
function AutoComplete(inputControl, FilterType) {

    // if ($(inputControl).val().length >= 3) {
    var pagePath = window.location.pathname;
    inputControl.autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/UMS_Service.asmx/GetAutocompleteList",
                data: "{ 'SearchValue': '" + inputControl.val() + "','FilterType':" + FilterType + " }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return { value: item }
                    }))
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                    //alert(textStatus);
                }
            });
        },
        minLength: 1    // MINIMUM 1 CHARACTER TO START WITH.
    });
    //  }
}
function MinimumLengthValidation(TextBox, lblmsg, MaxLength, Name) {
    lblmsg.innerHTML = "";
    if (TextBox.value.length < parseInt(MaxLength)) {
        lblmsg.innerHTML = Name + " minimum of " + MaxLength + " characters.";
        lblmsg.style.display = "block";
        TextBox.style.border = "1px solid red";
        //        TextBox.focus();
        return false;
    }
    else {
        TextBox.style.border = "1px solid #8E8E8E";
        return true;
    }
}