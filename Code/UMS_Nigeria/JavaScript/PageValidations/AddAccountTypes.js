﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {

    var txtAccountType = document.getElementById('UMSNigeriaBody_txtAccountType');
    var txtAccountCode = document.getElementById('UMSNigeriaBody_txtAccountCode');

    var IsValid = true;

    if (TextBoxValidationlbl(txtAccountType, document.getElementById("spanAccountType"), "Account Type") == false) IsValid = false;
    if (TextBoxValidationlbl(txtAccountCode, document.getElementById("spanAccountCode"), "Account Code") == false) IsValid = false;

    if (!IsValid) {

        return false;
    }
}

function UpdateValidation(AccountType,AccountCode) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var txtGridAccountType = document.getElementById(AccountType);
    var txtGridAccountCode = document.getElementById(AccountCode);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridAccountType, "Account Type") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridAccountCode, "Account Code") == false) { return false; }
}

function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};    