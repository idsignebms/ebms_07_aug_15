﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {
    var txtAgencyName = document.getElementById('UMSNigeriaBody_txtAgencyName');
    var txtContactName = document.getElementById('UMSNigeriaBody_txtContactName');
    var txtContactNo = document.getElementById('UMSNigeriaBody_txtContactNo');
    var txtAnotherContactNo = document.getElementById('UMSNigeriaBody_txtAnotherContactNo');
    var txtCode1 = document.getElementById('UMSNigeriaBody_txtCode1');
    var txtCode2 = document.getElementById('UMSNigeriaBody_txtCode2');

    var TextLength = "12";
    TextLength = parseInt(TextLength);

    var IsValid = true;

    if (TextBoxValidationlbl(txtAgencyName, document.getElementById("spanAgencyName"), "Agency Name") == false) IsValid = false;
    if (TextBoxValidationlbl(txtContactName, document.getElementById("spanContactName"), "Contact Name") == false) IsValid = false;

    if (TextBoxValidationlbl(txtCode1, document.getElementById("spanContactNo"), "Code") == false) IsValid = false;
    if (TextBoxValidationlbl(txtContactNo, document.getElementById("spanContactNo"), "Contact No") == false) IsValid = false;
    else if (ContactNoLengthMinMax(txtContactNo, 6, 10, document.getElementById("spanContactNo"), txtCode1, 3) == false) {
        IsValid = false;
    }
    else {
        txtContactNo.style.border = "1px solid #8E8E8E";
        txtCode1.style.border = "1px solid #8E8E8E";
        document.getElementById("spanContactNo").innerHTML = "";
    }

    if (Trim(txtAnotherContactNo.value) != "" || Trim(txtCode2.value) != "") {
        if (ContactNoLengthMinMax(txtAnotherContactNo, 6, 10, document.getElementById("spanAnotherNo"), txtCode2, 3) == false) IsValid = false;
        else {
            txtAnotherContactNo.style.border = "1px solid #8E8E8E";
            txtCode2.style.border = "1px solid #8E8E8E";
            document.getElementById("spanAnotherNo").innerHTML = "";
        }
    }
    else {
        txtAnotherContactNo.style.border = "1px solid #8E8E8E";
        txtCode2.style.border = "1px solid #8E8E8E";
        document.getElementById("spanAnotherNo").innerHTML = "";
    }

    //    if (TextBoxValidContactLbl(txtContactNo, "Contact No", document.getElementById("spanContactNo"), TextLength) == false) IsValid = false;
    //    else {
    //        txtContactNo.style.border = "1px solid #8E8E8E";
    //        document.getElementById("spanContactNo").innerHTML = "";
    //    }

    //    if (Trim(txtAnotherContactNo.value) != "") {
    //        if (TextBoxValidContactLbl(txtAnotherContactNo, "Contact No", document.getElementById("spanAnotherNo"), TextLength) == false) IsValid = false;
    //    }

    if (!IsValid) {
        return false;
    }
}

function UpdateValidation(AgencyName, ContactPersonName, ContactNo2, ContactNo1, Code1, Code2) {

    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var txtGridAgencyName = document.getElementById(AgencyName);
    var txtGridContactPersonName = document.getElementById(ContactPersonName);
    var txtGridContactNo1 = document.getElementById(ContactNo1);
    var txtGridContactNo2 = document.getElementById(ContactNo2);
    var Code1 = document.getElementById(Code1);
    var Code2 = document.getElementById(Code2);
    var TextLength = "12";
    TextLength = parseInt(TextLength);


    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridAgencyName, "Agency Name") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridContactPersonName, "Contact Name") == false) { return false; }
    //    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridContactNo1, "Contact No") == false) { return false; }
    //    if (ContactNoLengthInPopup(mpeAlert, lblAlertMsg, txtGridContactNo1, TextLength) == false) { return false; }
    //    if (Trim(txtGridContactNo2.value) != "") {
    //        if (ContactNoLengthInPopup(mpeAlert, lblAlertMsg, txtGridContactNo2, TextLength) == false) { return false; }
    //    }

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, Code1, "Code") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridContactNo1, "Contact No") == false) { return false; }
    else if (ContactNoLengthMinMaxInPopup(mpeAlert, lblAlertMsg, txtGridContactNo1, 6, 10, Code1, 3) == false) { return false; }

    if (Trim(txtGridContactNo2.value) != "" || Trim(Code2.value) != "") {
        if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, Code2, "Code") == false) { return false; }
        if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridContactNo2, "Contact No") == false) { return false; }
        else if (ContactNoLengthMinMaxInPopup(mpeAlert, lblAlertMsg, txtGridContactNo2, 6, 10, Code2, 3) == false) { return false; }
    }
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
}; 