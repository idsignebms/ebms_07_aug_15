﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {
    var txtAddress1 = document.getElementById('UMSNigeriaBody_txtAddress1');
    var txtAddress2 = document.getElementById('UMSNigeriaBody_txtAddress2');
    var txtCity = document.getElementById('UMSNigeriaBody_txtCity');
    var txtZipCode = document.getElementById('UMSNigeriaBody_txtZipCode');
    //var txtNotes = document.getElementById('UMSNigeriaBody_txtNotes');
    var IsValid = true;

    if (TextBoxValidationlbl(txtAddress1, document.getElementById("spanAddress1"), "Address1") == false) IsValid = false;
    if (TextBoxValidationlbl(txtAddress2, document.getElementById("spanAddress2"), "Address2") == false) IsValid = false;
    if (TextBoxValidationlbl(txtCity, document.getElementById("spanCity"), "City") == false) IsValid = false;
//    if (TextBoxValidationlbl(txtZipCode, document.getElementById("spanZipCode"), "Zip Code") == false) IsValid = false;else
     if (CustomerTextboxZipCodelbl(txtZipCode, document.getElementById("spanZipCode"), 10, 6) == false) {
        IsValid = false;
    }
    //if (TextBoxValidationlbl(txtNotes, document.getElementById("spanNotes"), "Notes") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
}

function UpdateValidation(Address1, Address2, City, ZipCode, Notes) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var txtGvAddress1 = document.getElementById(Address1);
    var txtGvAddress2 = document.getElementById(Address2);
    var txtGvCity = document.getElementById(City);
    var txtGvZipCode = document.getElementById(ZipCode);
    var txtGvNotes = document.getElementById(Notes);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGvAddress1, "Address1") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGvAddress2, "Address2") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGvCity, "City") == false) { return false; }
//    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGvZipCode, "Zip Code") == false) { return false; }else
     if (CustomerTextboxZipCodelblInPopup(mpeAlert, lblAlertMsg, txtGvZipCode, 10, 6) == false) { return false; }
    //if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGvNotes, "Notes") == false) { return false; }
}

function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};    