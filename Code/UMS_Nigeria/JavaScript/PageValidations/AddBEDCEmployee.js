﻿function Validate() {
    var txtName = document.getElementById('UMSNigeriaBody_txtName');
    //var txtDesignation = document.getElementById('UMSNigeriaBody_txtDesignation');
    var ddlDesignation = document.getElementById('UMSNigeriaBody_ddlDesignation');
    var txtContactNo = document.getElementById('UMSNigeriaBody_txtContactNo');
    var txtAnotherContactNo = document.getElementById('UMSNigeriaBody_txtAnotherContactNo');
    var txtCode1 = document.getElementById('UMSNigeriaBody_txtCode1');
    var txtCode2 = document.getElementById('UMSNigeriaBody_txtCode2');
    var txtAddress = document.getElementById('UMSNigeriaBody_txtAddress');
    var txtLocation = document.getElementById('UMSNigeriaBody_txtLocation');

    var IsValid = true;

    if (TextBoxValidationlbl(txtName, document.getElementById("spanName"), "Name") == false) IsValid = false;
    //if (TextBoxValidationlbl(txtDesignation, document.getElementById("spanDesignation"), "Designation") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlDesignation, document.getElementById("spanDesignation"), "Designation") == false) IsValid = false;
    if (TextBoxValidationlbl(txtLocation, document.getElementById("spanLocation"), "Location") == false) IsValid = false;
    if (TextBoxValidationlbl(txtAddress, document.getElementById("spanAddress"), "Address") == false) IsValid = false;

    if (TextBoxValidationlbl(txtCode1, document.getElementById("spanContactNo"), "Code") == false) IsValid = false;
    if (TextBoxValidationlbl(txtContactNo, document.getElementById("spanContactNo"), "Contact No") == false) IsValid = false;
    else if (ContactNoLengthMinMax(txtContactNo, 6, 10, document.getElementById("spanContactNo"), txtCode1, 3) == false) {
        IsValid = false;
    }
    else {
        txtContactNo.style.border = "1px solid #8E8E8E";
        txtCode1.style.border = "1px solid #8E8E8E";
        document.getElementById("spanContactNo").innerHTML = "";
    }

    if (Trim(txtAnotherContactNo.value) != "" || Trim(txtCode2.value) != "") {
        if (ContactNoLengthMinMax(txtAnotherContactNo, 6, 10, document.getElementById("spanAnotherNo"), txtCode2, 3) == false) IsValid = false;
        else {
            txtAnotherContactNo.style.border = "1px solid #8E8E8E";
            txtCode2.style.border = "1px solid #8E8E8E";
            document.getElementById("spanAnotherNo").innerHTML = "";
        }
    }
    else {
        txtAnotherContactNo.style.border = "1px solid #8E8E8E";
        txtCode2.style.border = "1px solid #8E8E8E";
        document.getElementById("spanAnotherNo").innerHTML = "";
    }
    if (!IsValid) {
        return false;
    }
}

function UpdateValidation(Name, Designation, Location, Code1, ContactNo1, Code2, ContactNo2, Address) {

    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var Name = document.getElementById(Name);
    var Designation = document.getElementById(Designation);
    var Location = document.getElementById(Location);
    var txtGridContactNo1 = document.getElementById(ContactNo1);
    var txtGridContactNo2 = document.getElementById(ContactNo2);
    var Code1 = document.getElementById(Code1);
    var Code2 = document.getElementById(Code2);
    var Address = document.getElementById(Address);
    var TextLength = "12";
    TextLength = parseInt(TextLength);


    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, Name, "Name") == false) { return false; }
    //if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, Designation, "Designation") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, Designation, "Designation") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, Location, "Location") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, Code1, "Code") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridContactNo1, "Contact No") == false) { return false; }
    else if (ContactNoLengthMinMaxInPopup(mpeAlert, lblAlertMsg, txtGridContactNo1, 6, 10, Code1, 3) == false) { return false; }

    if (Trim(txtGridContactNo2.value) != "" || Trim(Code2.value) != "") {
        if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, Code2, "Code") == false) { return false; }
        if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridContactNo2, "Contact No") == false) { return false; }
        else if (ContactNoLengthMinMaxInPopup(mpeAlert, lblAlertMsg, txtGridContactNo2, 6, 10, Code2, 3) == false) { return false; }
    }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, Address, "Address") == false) { return false; }
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
}; 