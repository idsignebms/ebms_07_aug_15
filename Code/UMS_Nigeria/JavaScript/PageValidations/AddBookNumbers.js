﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');

function Validate() {
    var ddlBusinessUnitName = document.getElementById('UMSNigeriaBody_ddlBusinessUnitName');
    var ddlServiceUnitName = document.getElementById('UMSNigeriaBody_ddlServiceUnitName');
    var ddlServiceCenterName = document.getElementById('UMSNigeriaBody_ddlServiceCenterName');
    var ddlCycle = document.getElementById('UMSNigeriaBody_ddlCycle');
    var ddlMarketer = document.getElementById('UMSNigeriaBody_ddlMarketer');
    var txtBookNo = document.getElementById('UMSNigeriaBody_txtBookNo');
    //var txtBookCode = document.getElementById('UMSNigeriaBody_txtBookCode');
    //var hfBookCodeLength = document.getElementById('UMSNigeriaBody_hfBookCodeLength');
    var IsValid = true;

    if (DropDownlistValidationlbl(ddlBusinessUnitName, document.getElementById("spanddlBusinessUnitName"), "Business Unit") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlServiceUnitName, document.getElementById("spanddlServiceUnitName"), "Service Unit") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlServiceCenterName, document.getElementById("spanddlServiceCenterName"), "Service Center") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlCycle, document.getElementById("spanddlCycle"), "BookGroup") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlMarketer, document.getElementById("spanMarketer"), "Marketer") == false) IsValid = false;
    if (TextBoxValidationlbl(txtBookNo, document.getElementById("spanBookNo"), "Book Name") == false) IsValid = false;
//    if (TextBoxValidationlbl(txtBookCode, document.getElementById("spanBookCode"), "Book Code") == false) IsValid = false;
//    if (txtBookCode.value != "") {
//        if (TextBoxLengthValidationLbl(txtBookCode, hfBookCodeLength.value + " Chars", document.getElementById("spanBookCode"), hfBookCodeLength.value) == false) IsValid = false;
//    }
    if (!IsValid) {
        return false;
    }
    else {

        //                var mpeActivate = $find('UMSNigeriaBody_mpeActivate');
        //                var lblActiveMsg = document.getElementById('UMSNigeriaBody_lblActiveMsg');
        //                lblActiveMsg.innerHTML = "Please check your details before save!";
        //                mpeActivate.show();
        //                var r = confirm("Please check your Book Number?" + "\n"
        //                                                 + "Once it is saved you cannot edit" + "\n"
        //                                                 + "If you want to modify the details click on cancel");
        //                if (r == true) { return true; } else { return false; }
    }
}



function UpdateValidation(BUID, SUID, ServiceCenter, Cycle,Marketer) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var ddlGridBusinessUnitName = document.getElementById(BUID);
    var ddlGridServiceUnitName = document.getElementById(SUID);
    var ddlGridServiceCenterName = document.getElementById(ServiceCenter);
    var ddlGridCycle = document.getElementById(Cycle);
    var ddlGridMarketer = document.getElementById(Marketer);

    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlGridBusinessUnitName, "Business Unit") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlGridServiceUnitName, "Service Unit") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlGridServiceCenterName, "Service Center") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlGridCycle, "BookGroup") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlGridMarketer, "Marketer") == false) { return false; }
    //if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, BookCode, "Book Code") == false) { return false; }
    //if (TextmsgInPopup(mpeAlert, lblAlertMsg, BookCode, hfBookCodeLength.value, "Book code must be " + hfBookCodeLength.value + " characters") == false) { return false; }

}
function pageLoad(sender, args) {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};