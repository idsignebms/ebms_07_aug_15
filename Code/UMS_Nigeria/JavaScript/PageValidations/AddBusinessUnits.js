﻿
function Validate() {
    var txtBusinessUnitName = document.getElementById('UMSNigeriaBody_txtBusinessUnitName');
    var ddlStateName = document.getElementById('UMSNigeriaBody_ddlStateName');
    var hfBUCodeLength = document.getElementById('UMSNigeriaBody_hfBUCodeLength');
    var txtCity = document.getElementById('UMSNigeriaBody_txtCity');
    var txtAddress1 = document.getElementById('UMSNigeriaBody_txtAddress1');
    var txtAddress2 = document.getElementById('UMSNigeriaBody_txtAddress2');
    var txtZipCode = document.getElementById('UMSNigeriaBody_txtZipCode');

    var IsValid = true;

    if (DropDownlistValidationlbl(ddlStateName, document.getElementById("spanddlStateName"), "State") == false) IsValid = false;
    if (TextBoxValidationlbl(txtBusinessUnitName, document.getElementById("spanBusinessUnitName"), "Business Unit Name") == false) IsValid = false;
    if (TextBoxValidationlbl(txtCity, document.getElementById("spanCity"), "City Name") == false) IsValid = false;
    if (TextBoxValidationlbl(txtAddress1, document.getElementById("spanAddress1"), "Address 1") == false) IsValid = false;
    if (TextBoxValidationlbl(txtAddress2, document.getElementById("spanAddress2"), "Address 2") == false) IsValid = false;
    if (TextBoxValidationlbl(txtZipCode, document.getElementById("spanZip"), "Zip Code") == false) {
        IsValid = false;
    }
    else if (CustomerTextboxZipCodelbl(txtZipCode, document.getElementById("spanZip"), 10, 6) == false) {
        IsValid = false;
    }
    if (!IsValid) {
        return false;
    }
}

function UpdateValidation(BusinessUnitName, StateName, Address1, Address2, City, Zip) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var txtGridBusinessUnitName = document.getElementById(BusinessUnitName);
    var txtGridAddress1 = document.getElementById(Address1);
    var txtGridAddress2 = document.getElementById(Address2);
    var txtGridCity = document.getElementById(City);
    var txtGridZip = document.getElementById(Zip);
    var ddlGridStateName = document.getElementById(StateName);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridBusinessUnitName, "Business Unit Name") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlGridStateName, "State Name") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridAddress1, "Address1") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridAddress2, "Address2") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridCity, "City") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridZip, "Zip Code") == false) { return false; }
    else if (CustomerTextboxZipCodelblInPopup(mpeAlert, lblAlertMsg, txtGridZip, 10, 6) == false) { return false; }
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};   