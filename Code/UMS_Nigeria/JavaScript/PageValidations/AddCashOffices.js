﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');

function Validate() {
    var OfficeName = document.getElementById('UMSNigeriaBody_txtOfficeName');
    var ddlBusinessUnit = document.getElementById('UMSNigeriaBody_ddlBusinessUnit');
    var ddlServiceUnit = document.getElementById('UMSNigeriaBody_ddlServiceUnit');
    var ddlServiceCenter = document.getElementById('UMSNigeriaBody_ddlServiceCenter');
    var txtContactNo = document.getElementById('UMSNigeriaBody_txtContactNo');
    var txtAnotherContactNo = document.getElementById('UMSNigeriaBody_txtAnotherContactNo');
    var txtCode1 = document.getElementById('UMSNigeriaBody_txtCode1');
    var txtCode2 = document.getElementById('UMSNigeriaBody_txtCode2');

    var IsValid = true;

    if (TextBoxValidationlbl(OfficeName, document.getElementById("spanOfficeName"), "Office Name") == false) IsValid = false;
    if (ddlBusinessUnit.value != "") {
        if (DropDownlistValidationlbl(ddlServiceUnit, document.getElementById("spanSU"), "Service Unit") == false) IsValid = false;
    }
    if (ddlServiceUnit.value != "") {
        if (DropDownlistValidationlbl(ddlServiceCenter, document.getElementById("spanSC"), "Service Center") == false) IsValid = false;
    }

    if (TextBoxValidationlbl(txtCode1, document.getElementById("spanContactNo"), "Code") == false) IsValid = false;
    if (TextBoxValidationlbl(txtContactNo, document.getElementById("spanContactNo"), "Contact No") == false) IsValid = false;
    else if (ContactNoLengthMinMax(txtContactNo, 6, 10, document.getElementById("spanContactNo"), txtCode1, 3) == false) {
        IsValid = false;
    }
    else {
        txtContactNo.style.border = "1px solid #8E8E8E";
        txtCode1.style.border = "1px solid #8E8E8E";
        document.getElementById("spanContactNo").innerHTML = "";
    }

    if (Trim(txtAnotherContactNo.value) != "" || Trim(txtCode2.value) != "") {
        if (ContactNoLengthMinMax(txtAnotherContactNo, 6, 10, document.getElementById("spanAnotherNo"), txtCode2, 3) == false) IsValid = false;
        else {
            txtAnotherContactNo.style.border = "1px solid #8E8E8E";
            txtCode2.style.border = "1px solid #8E8E8E";
            document.getElementById("spanAnotherNo").innerHTML = "";
        }
    }
    else {
        txtAnotherContactNo.style.border = "1px solid #8E8E8E";
        txtCode2.style.border = "1px solid #8E8E8E";
        document.getElementById("spanAnotherNo").innerHTML = "";
    }

    // if (ValidateMobileNo() == false) IsValid = false;
    if (!IsValid) {
        return false;
    }
}

//function ValidateMobileNo() {
//    var ContactNo = document.getElementById('UMSNigeriaBody_txtContactNo');
//    var AnotherContact = document.getElementById('UMSNigeriaBody_txtAnotherContactNo');
//    var ContactNoLength = "12";
//    ContactNoLength = parseInt(ContactNoLength);
//    if (TextBoxValidationlbl(ContactNo, document.getElementById("spanContactNo"), "Contact No") == false) return false;

//    if (TextBoxValidContactLbl(ContactNo, "Contact No", document.getElementById("spanContactNo"), ContactNoLength) == false) return false;
//    if (AnotherContact.value != "") {
//        if (TextBoxValidContactLbl(AnotherContact, "Contact No", document.getElementById("spanAnotherNo"), ContactNoLength) == false) return false;
//    }
//}

function UpdateValidation(CashOffice, ContactNo2, BU, SU, SC, ContactNo, Code1, Code2) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var CashOffice = document.getElementById(CashOffice);
    var txtGridContactNo1 = document.getElementById(ContactNo);
    var txtGridContactNo2 = document.getElementById(ContactNo2);
    var BU = document.getElementById(BU);
    var SU = document.getElementById(SU);
    var SC = document.getElementById(SC);
    var Code1 = document.getElementById(Code1);
    var Code2 = document.getElementById(Code2);
    var TextLength = "12";
    TextLength = parseInt(TextLength);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, CashOffice, "Cash Office") == false) { return false; }
    //    if (ContactNoLengthInPopup(mpeAlert, lblAlertMsg, ContactNo, TextLength) == false) { return false; }
    //    if (Trim(ContactNo2.value) != "") {
    //        if (ContactNoLengthInPopup(mpeAlert, lblAlertMsg, ContactNo2, TextLength) == false) { return false; }
    //    }
    //if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, State, "State") == false) { return false; }

    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, BU, "Business Unit") == false) { return false; }
    //if (DropDownlistValidations(District, "District") == false) { return false; }

    if (BU.value != "") {
        if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, SU, "Service Unit") == false) { return false; }
    }
    if (SU.value != "") {
        if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, SC, "Service Center") == false) { return false; }
    }

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, Code1, "Code") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridContactNo1, "Contact No") == false) { return false; }
    else if (ContactNoLengthMinMaxInPopup(mpeAlert, lblAlertMsg, txtGridContactNo1, 6, 10, Code1, 3) == false) { return false; }

    if (Trim(txtGridContactNo2.value) != "" || Trim(Code2.value) != "") {
        if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, Code2, "Code") == false) { return false; }
        if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridContactNo2, "Contact No") == false) { return false; }
        else if (ContactNoLengthMinMaxInPopup(mpeAlert, lblAlertMsg, txtGridContactNo2, 6, 10, Code2, 3) == false) { return false; }
    }
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};   