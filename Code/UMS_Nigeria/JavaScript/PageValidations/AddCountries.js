﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {

    var txtCountryName = document.getElementById('UMSNigeriaBody_txtCountryName');
//    var txtCountryCode = document.getElementById('UMSNigeriaBody_txtCountryCode');
    var txtCurrency = document.getElementById('UMSNigeriaBody_txtCurrency');
    var txtCurrencySymbol = document.getElementById('UMSNigeriaBody_txtCurrencySymbol');

    var IsValid = true;
       
    if (TextBoxValidationlbl(txtCountryName, document.getElementById("spanCountryName"), "Country Name") == false) IsValid = false;
//    if (TextBoxValidationlbl(txtCountryCode, document.getElementById("spanCountryCode"), "Country Code") == false) IsValid = false;
    if (TextBoxValidationlbl(txtCurrency, document.getElementById("spanCurrency"), "Currency") == false) IsValid = false;
    if (TextBoxValidationlbl(txtCurrencySymbol, document.getElementById("spanCurrencySymbol"), "Currency Symbol") == false) IsValid = false;

    if (!IsValid) {

        return false;
    }
    //            else {

    //                var r = confirm("Please check your country code?" + "\n"
    //                                 + "Once is saved you cannot edit" + "\n"
    //                                 + "If you want to modify the details click on cancel");
    //                if (r == true) { return true; } else { return false; }
    //            }
}

function UpdateValidation(CountryName,CurrencyName,CurrencySymbol) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var txtGridCountryName = document.getElementById(CountryName);
    var txtGridCurrencyName = document.getElementById(CurrencyName);
    var txtGridCurrencySymbol = document.getElementById(CurrencySymbol);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridCountryName, "Country Name") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridCurrencyName, "Currency Name") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridCurrencySymbol, "Currency Symbol") == false) { return false; }

}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');    
};    