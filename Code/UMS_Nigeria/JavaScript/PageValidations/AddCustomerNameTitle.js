﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {

    var txtTitle = document.getElementById('UMSNigeriaBody_txtTitle');

    var IsValid = true;

    if (TextBoxValidationlbl(txtTitle, document.getElementById("spanTitle"), "Title") == false) IsValid = false;

    if (!IsValid) {

        return false;
    }
}

function UpdateValidation(Title) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var txtGridTitle = document.getElementById(Title);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridTitle, "Title") == false) {
        return false;
    }
}

function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};    