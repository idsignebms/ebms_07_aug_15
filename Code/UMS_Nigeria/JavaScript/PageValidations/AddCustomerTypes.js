﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {

    var txtCustomerType = document.getElementById('UMSNigeriaBody_txtCustomerType');

    var IsValid = true;

    if (TextBoxValidationlbl(txtCustomerType, document.getElementById("spanCustomerType"), "Customer Type") == false) IsValid = false;

    if (!IsValid) {

        return false;
    }
}

function UpdateValidation(CustomerType) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var txtGridCustomerType = document.getElementById(CustomerType);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridCustomerType, "Customer Type") == false) {
        return false;
    }
}

function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};    