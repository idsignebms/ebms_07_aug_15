﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');


function Validate() {
    var ddlBusinessUnitName = document.getElementById('UMSNigeriaBody_ddlBusinessUnitName');
    var ddlServiceUnitName = document.getElementById('UMSNigeriaBody_ddlServiceUnitName');
    var ddlServiceCenterName = document.getElementById('UMSNigeriaBody_ddlServiceCenterName');
    var txtCycleName = document.getElementById('UMSNigeriaBody_txtCycleName');
    var txtCycleCode = document.getElementById('UMSNigeriaBody_txtCycleCode');
    var hfCycleCodeLength = document.getElementById('UMSNigeriaBody_hfCycleCodeLength');

    var IsValid = true;

    if (DropDownlistValidationlbl(ddlBusinessUnitName, document.getElementById("spanddlBusinessUnitName"), "Business Unit") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlServiceUnitName, document.getElementById("spanddlServiceUnitName"), "Service Unit") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlServiceCenterName, document.getElementById("spanddlServiceCenterName"), "Service Center") == false) IsValid = false;
    if (TextBoxValidationlbl(txtCycleName, document.getElementById("spanCycleName"), "BookGroup Name") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
}

function UpdateValidation(CycleName, BUID, SUID, ServiceCenter) {

    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var txtGridCycleName = document.getElementById(CycleName);
    var ddlGridBusinessUnitName = document.getElementById(BUID);
    var ddlGridServiceUnitName = document.getElementById(SUID);
    var ddlGridServiceCenterName = document.getElementById(ServiceCenter);
    var hfCycleCodeLength = document.getElementById('UMSNigeriaBody_hfCycleCodeLength');

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridCycleName, "BookGroup Name") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlGridBusinessUnitName, "Business Unit") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlGridServiceUnitName, "Service Unit") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlGridServiceCenterName, "Service Center") == false) { return false; }
}
function pageLoad() {

    DisplayMessage('UMSNigeriaBody_pnlMessage');
};      