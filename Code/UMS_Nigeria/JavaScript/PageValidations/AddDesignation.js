﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {

    var txtDesignation = document.getElementById('UMSNigeriaBody_txtDesignation');

    var IsValid = true;

    if (TextBoxValidationlbl(txtDesignation, document.getElementById("spanDesignation"), "Designation") == false) IsValid = false;

    if (!IsValid) {

        return false;
    }
}

function UpdateValidation(Designation) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var txtGridDesignation = document.getElementById(Designation);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridDesignation, "Designation") == false) {
        return false;
    }
}

function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};    