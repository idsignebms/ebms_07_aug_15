﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {

    var txtFirst = document.getElementById('UMSNigeriaBody_txtMessage1');
    var txtSecond = document.getElementById('UMSNigeriaBody_txtMessage2');
    var txtThird = document.getElementById('UMSNigeriaBody_txtMessage3');
    var ddlBusinessUnit = document.getElementById('UMSNigeriaBody_ddlBusinessUnit');
    var rblBillType = document.getElementById('UMSNigeriaBody_rblMessageType');

    var IsValid = true;

    if (DropDownlistValidationlbl(ddlBusinessUnit, document.getElementById("spanddlBusinessUnit"), "Business Unit") == false) IsValid = false;
    if (MultipleTextBoxValidationlbl(txtFirst, txtSecond, txtThird, document.getElementById("spanFirstMessage"), "any one of the message") == false) IsValid = false;
    if (RadioButtonListlbl(rblBillType, "spanBillType", " Bill Type ") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }

    else {

        return true;
    }
}


function MultipleTextBoxValidationlbl(txt1, txt2, txt3, lbl, field) {
    lbl.innerHTML = "";
    lbl.style.display = "none";
    if ((Trim(txt1.value) == "" || Trim(txt1.value) == "0") && (Trim(txt2.value) == "" || Trim(txt2.value) == "0") && (Trim(txt3.value) == "" || Trim(txt3.value) == "0")) {
        lbl.innerHTML = "Enter " + field;
        lbl.style.display = "block";
        txt1.style.border = txt2.style.border = txt3.style.border = "1px solid red";
        txt1.focus();
        return false;
    }
    else {

        return true;
    }
}
function Clearall() {
    var txtFirst = document.getElementById('UMSNigeriaBody_txtMessage1');
    var btnViewMsg = document.getElementById('UMSNigeriaBody_btnViewGMsg');
    var txtSecond = document.getElementById('UMSNigeriaBody_txtMessage2');
    var txtThird = document.getElementById('UMSNigeriaBody_txtMessage3');
    var ddlBusinessUnit = document.getElementById('UMSNigeriaBody_ddlBusinessUnit');
    var rblBillType = document.getElementById('UMSNigeriaBody_rblMessageType');
    $("#UMSNigeriaBody_rblMessageType input[type=radio]").prop('checked', false);
    txtFirst.value = txtSecond.value = txtThird.value = "";
    ddlBusinessUnit.selectedIndex = 0;
    btnViewMsg.style.visibility = "hidden";
}
function StrictSymbol(evt) {

    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 124)

        return false;

    return true;
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};       