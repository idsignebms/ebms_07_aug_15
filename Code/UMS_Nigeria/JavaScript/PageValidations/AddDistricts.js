﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {
    var txtDistrictName = document.getElementById('UMSNigeriaBody_txtDistrictName');
    var txtDistrictCode = document.getElementById('UMSNigeriaBody_txtDistrictCode');
    var ddlStateName = document.getElementById('UMSNigeriaBody_ddlStateName');
    var ddlCountryName = document.getElementById('UMSNigeriaBody_ddlCountryName');

    var IsValid = true;

    if (DropDownlistValidationlbl(ddlCountryName, document.getElementById("spanCountryCode"), "Country") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlStateName, document.getElementById("spanStateCode"), "State") == false) IsValid = false;
    if (TextBoxValidationlbl(txtDistrictName, document.getElementById("spanDistrictName"), "District Name ") == false) IsValid = false;
    if (TextBoxValidationlbl(txtDistrictCode, document.getElementById("spanDistrictCode"), "District Code") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }

    else {

        var r = confirm("Please check your District code?" + "\n"
                                 + "Once is saved you cannot edit" + "\n"
                                 + "If you want to modify the details click on cancel");
        if (r == true) { return true; } else { return false; }
    }
}

function UpdateValidation(DistrictName, CountryName, StateName) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var txtGridDistrictName = document.getElementById(DistrictName);
    var ddlGridCountryName = document.getElementById(CountryName);
    var ddlGridStateName = document.getElementById(StateName);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridDistrictName, "District Name") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlGridCountryName, "Country") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlGridStateName, "State") == false) { return false; }
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};       