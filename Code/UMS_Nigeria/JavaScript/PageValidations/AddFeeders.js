﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {

    var txtFeederName = document.getElementById('UMSNigeriaBody_txtFeederName');
    var ddlSubStationName = document.getElementById('UMSNigeriaBody_ddlSubStationName');


    var IsValid = true;

    if (DropDownlistValidationlbl(ddlSubStationName, document.getElementById("spanddlSubStationName"), "Injection SubStation Name") == false) IsValid = false;
    if (TextBoxValidationlbl(txtFeederName, document.getElementById("spanFeederName"), "Feeder Name ") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
}
function UpdateValidation(FeederName, SubStationName) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var txtGridFeederName = document.getElementById(FeederName);
    var ddlGridSubStationName = document.getElementById(SubStationName);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridFeederName, "Feeder Name") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlGridSubStationName, "Injection SubStation") == false) { return false; }
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};      