﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {

    var txtIdentityType = document.getElementById('UMSNigeriaBody_txtIdentityType');

    var IsValid = true;

    if (TextBoxValidationlbl(txtIdentityType, document.getElementById("spanIdentityType"), "Identity Type") == false) IsValid = false;

    if (!IsValid) {

        return false;
    }
}

function UpdateValidation(IdentityType) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var txtGridCustomerIdentityType = document.getElementById(IdentityType);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridCustomerIdentityType, "Identity Type") == false) {
        return false;
    }
}

function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};    