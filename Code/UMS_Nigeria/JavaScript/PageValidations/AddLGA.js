﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {
    var ddlBusinessUnitName = document.getElementById('UMSNigeriaBody_ddlBusinessUnitName');
    var txtLGAName = document.getElementById('UMSNigeriaBody_txtLGAName');
    var IsValid = true;

    if (DropDownlistValidationlbl(ddlBusinessUnitName, document.getElementById("spanddlBusinessUnitName"), "Business Unit") == false) IsValid = false;
    if (TextBoxValidationlbl(txtLGAName, document.getElementById("spanLGAName"), "LGA Name") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
}

function UpdateValidation(RegionName, ddlBU) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var ddlGridBusinessUnitName = document.getElementById(ddlBU);
    var txtGridLGAName = document.getElementById(RegionName);
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlGridBusinessUnitName, "Business Unit") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridLGAName, "LGA Name") == false) { return false; }
}

function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};