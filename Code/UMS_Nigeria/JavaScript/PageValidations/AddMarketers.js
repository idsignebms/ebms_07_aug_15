﻿
function Validate() {
    var ddlBusinessUnitName = document.getElementById('UMSNigeriaBody_ddlBusinessUnitName');
    var txtFName = document.getElementById('UMSNigeriaBody_txtFName');
    var txtLName = document.getElementById('UMSNigeriaBody_txtLName');
    var txtContactNo = document.getElementById('UMSNigeriaBody_txtContactNo');
    var txtCode1 = document.getElementById('UMSNigeriaBody_txtCode1');
    var txtCode2 = document.getElementById('UMSNigeriaBody_txtCode2');

    var txtAnotherContactNo = document.getElementById('UMSNigeriaBody_txtAnotherContactNo');
    var txtEmail = document.getElementById('UMSNigeriaBody_txtEmail');
    var txtAddress1 = document.getElementById('UMSNigeriaBody_txtAddress1');
    var txtZip = document.getElementById('UMSNigeriaBody_txtZip');
    var txtCity = document.getElementById('UMSNigeriaBody_txtCity');
    var fupPhoto = document.getElementById('UMSNigeriaBody_fupPhoto');
    var ContactNoLength = "12";
    ContactNoLength = parseInt(ContactNoLength);
    var IsValid = true;

    if (DropDownlistValidationlbl(ddlBusinessUnitName, document.getElementById("spanddlBusinessUnitName"), "Business Unit") == false) IsValid = false;
    if (TextBoxValidationlbl(txtFName, document.getElementById("spanFName"), "First Name") == false) IsValid = false;
    if (TextBoxValidationlbl(txtLName, document.getElementById("spanLName"), "Last Name") == false) IsValid = false;

    if (TextBoxValidationlbl(txtEmail, document.getElementById("spanEmail"), "Email Id") == false) IsValid = false;
    else
        if (EmailValidation(txtEmail) == false) {
            var spanEmail = document.getElementById("spanEmail");
            spanEmail.innerHTML = "Please Enter Valid Email Id";
            spanEmail.style.display = "block";
            txtEmail.style.border = "1px solid red";
            IsValid = false;
        }
        else {
            txtEmail.style.border = "1px solid #8E8E8E";
            document.getElementById("spanEmail").innerHTML = "";
        }

    if (TextBoxValidationlbl(txtAddress1, document.getElementById("spanAddress1"), "Address1") == false) IsValid = false;
    if (TextBoxValidationlbl(txtCity, document.getElementById("spanCity"), "City") == false) IsValid = false;
//    if (TextBoxValidationlbl(txtZip, document.getElementById("spanZip"), "Zip Code") == false) {
//        IsValid = false;
//    }else
     if (CustomerTextboxZipCodelbl(txtZip, document.getElementById("spanZip"), 10, 6) == false) {
        IsValid = false;
    }
    if (filevalidate(fupPhoto.value, "Photo") == false) IsValid = false;

    if (TextBoxValidationlbl(txtCode1, document.getElementById("spanContactNo"), "Code") == false) IsValid = false;

    if (TextBoxValidationlbl(txtContactNo, document.getElementById("spanContactNo"), "Contact No") == false) IsValid = false;
    else if (ContactNoLengthMinMax(txtContactNo, 6, 10, document.getElementById("spanContactNo"), txtCode1, 3) == false) {
        IsValid = false;
    }
    else {
        txtContactNo.style.border = "1px solid #8E8E8E";
        txtCode1.style.border = "1px solid #8E8E8E";
        document.getElementById("spanContactNo").innerHTML = "";
    }

    if (Trim(txtAnotherContactNo.value) != "" || Trim(txtCode2.value) != "") {
        if (ContactNoLengthMinMax(txtAnotherContactNo, 6, 10, document.getElementById("spanAnotherNo"), txtCode2, 3) == false) IsValid = false;
        else {
            txtAnotherContactNo.style.border = "1px solid #8E8E8E";
            txtCode2.style.border = "1px solid #8E8E8E";
            document.getElementById("spanAnotherNo").innerHTML = "";
        }
    }
    else {
        txtAnotherContactNo.style.border = "1px solid #8E8E8E";
        txtCode2.style.border = "1px solid #8E8E8E";
        document.getElementById("spanAnotherNo").innerHTML = "";
    }

    if (!IsValid) {
        return false;
    }
}

function filevalidate(filename, Message) {
    document.getElementById("spanPhoto").innerHTML = "";
    if (Trim(filename) != "") {
        if (!/(\.jpeg|\.jpg|\.png|\.gif)$/i.test(filename)) {
            var spanPhoto = document.getElementById("spanPhoto");
            spanPhoto.innerHTML = "Please select .jpeg, .jpg, .png and .gif File Formats only";
            document.getElementById("UMSNigeriaBody_fupPhoto").value = ''; //--faiz id103
            spanPhoto.style.display = "block";
            return false;
        }
    }
}

function UpdateValidation(FName, LName, EmailId, ContactNo1, ContactNo2, Code1, Code2, Address1, City, ZipCode, fupPhoto) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var txtGridFirstName = document.getElementById(FName);
    var txtGridLastName = document.getElementById(LName);
    var txtGridEmailId = document.getElementById(EmailId);
    var txtGridContactNo1 = document.getElementById(ContactNo1);
    var txtGridContactNo2 = document.getElementById(ContactNo2);
    var Code1 = document.getElementById(Code1);
    var Code2 = document.getElementById(Code2);
    var txtGridAddress1 = document.getElementById(Address1);
    var txtGridCity = document.getElementById(City);
    var txtGridZip = document.getElementById(ZipCode);
    var fupGridPhoto = document.getElementById(fupPhoto);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridFirstName, "First Name") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridLastName, "Last Name") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridEmailId, "Email Id") == false) { return false; }
    else if (EmailValidation(txtGridEmailId) == false) {
        lblAlertMsg.innerHTML = "Please enter valid Email Address";
        mpeAlert.show();
        return false;
    }
//    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridContactNo1, "Contact Number") == false) { return false; }
//    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridContactNo2, "Alternate Contact Number") == false) { return false; }
//    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridCode1, "Code for Contact Number") == false) { return false; }
//    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridCode2, "Code for Alternate Contact Number") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridAddress1, "Address1") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridCity, "City") == false) { return false; }
//    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridZip, "Zip Code") == false) { return false; }else
     if (CustomerTextboxZipCodelblInPopup(mpeAlert, lblAlertMsg, txtGridZip, 10, 6) == false) { return false; }
    //if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, fupGridPhoto, "First Name") == false) { return false; }

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, Code1, "Code") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridContactNo1, "Contact No") == false) { return false; }
    else if (ContactNoLengthMinMaxInPopup(mpeAlert, lblAlertMsg, txtGridContactNo1, 6, 10, Code1, 3) == false) { return false; }

    if (Trim(txtGridContactNo2.value) != "" || Trim(Code2.value) != "") {
        if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, Code2, "Alternate Contact NO Code") == false) { return false; }
        if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridContactNo2, "Alternate Contact No") == false) { return false; }
        else if (ContactNoLengthMinMaxInPopup(mpeAlert, lblAlertMsg, txtGridContactNo2, 6, 10, Code2, 3) == false) { return false; }
    }

    if (filevalidate(fupGridPhoto.value, "Photo") == false) {
        lblAlertMsg.innerHTML = "Only image files are allowed.";
        mpeAlert.show();
        return false;
    }
}

function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};