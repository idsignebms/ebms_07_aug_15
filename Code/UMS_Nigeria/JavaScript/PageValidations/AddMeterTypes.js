﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {

    var txtMeterName = document.getElementById('UMSNigeriaBody_txtMeterName');

    var IsValid = true;

    if (TextBoxValidationlbl(txtMeterName, document.getElementById("spanMeterName"), "Meter Name") == false) IsValid = false;

    if (!IsValid) {

        return false;
    }
}

function UpdateValidation(MeterType) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var txtGridMeterType = document.getElementById(MeterType);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridMeterType, "Meter Type") == false) {
        return false;
    }
}

function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};    