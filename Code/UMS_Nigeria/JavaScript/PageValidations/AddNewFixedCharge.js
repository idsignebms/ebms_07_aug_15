﻿function Validate() {
    var txtChargeName = document.getElementById('UMSNigeriaBody_txtChargeName');
    var IsValid = true;
    if (TextBoxValidationlbl(txtChargeName, document.getElementById("spanChargeName"), "Charge Name") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
}

function UpdateValidation(ChargeName) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var ChargeName = document.getElementById(ChargeName);
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, ChargeName, "Charge Name") == false) { return false; }
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};       