﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {

    var txtName = document.getElementById('UMSNigeriaBody_txtName');
    var ddlPoleName = document.getElementById("UMSNigeriaBody_ddlPoleName");

    var divParent = document.getElementById("UMSNigeriaBody_divParent");
    
    var IsValid = true;

    if (TextBoxValidationlbl(txtName, document.getElementById("spanName"), "Name") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlPoleName, document.getElementById("spanddlPoleName"), "Pole Name") == false) IsValid = false;
    if (divParent != null) {
        var ddlParent = document.getElementById("UMSNigeriaBody_ddlParent");
        if (DropDownlistValidationlbl(ddlParent, document.getElementById("spanddlParent"), "Parent Name") == false) IsValid = false;
    }
        

    if (!IsValid) {

        return false;
    }
    //            else {

    //                var r = confirm("Please check your country code?" + "\n"
    //                                 + "Once is saved you cannot edit" + "\n"
    //                                 + "If you want to modify the details click on cancel");
    //                if (r == true) { return true; } else { return false; }
    //            }
}

function UpdateValidation(Name) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var txtGridName = document.getElementById(Name);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridName, "Name") == false) { return false; }
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};    