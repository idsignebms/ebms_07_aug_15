﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {

    var txtPoleName = document.getElementById('UMSNigeriaBody_txtPoleName');
    var txtPoleOrderId = document.getElementById('UMSNigeriaBody_txtPoleOrderId');
    var txtCodeLength = document.getElementById('UMSNigeriaBody_txtCodeLength');

    var IsValid = true;

    if (TextBoxValidationlbl(txtPoleName, document.getElementById("spanPoleName"), "Pole Name") == false) IsValid = false;
    if (TextBoxValidationlbl(txtPoleOrderId, document.getElementById("spanPoleOrderId"), "Pole Order Id") == false) IsValid = false;
    if (TextBoxValidationlbl(txtCodeLength, document.getElementById("spanCodeLength"), "Code Length") == false) IsValid = false;

    if (!IsValid) {

        return false;
    }
    //            else {

    //                var r = confirm("Please check your country code?" + "\n"
    //                                 + "Once is saved you cannot edit" + "\n"
    //                                 + "If you want to modify the details click on cancel");
    //                if (r == true) { return true; } else { return false; }
    //            }
}

function UpdateValidation(PoleMasterName) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var txtGridPoleMasterName = document.getElementById(PoleMasterName);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridPoleMasterName, "Pole Master Name") == false) { return false; }
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};    