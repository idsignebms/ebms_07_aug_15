﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {

    var txtPoleName = document.getElementById('UMSNigeriaBody_txtPoleName');
    var ddlTransformerName = document.getElementById('UMSNigeriaBody_ddlTransformerName');


    var IsValid = true;

    if (DropDownlistValidationlbl(ddlTransformerName, document.getElementById("spanddlTransformerName"), "Transformer Name") == false) IsValid = false;
    if (TextBoxValidationlbl(txtPoleName, document.getElementById("spanPoleName"), "Pole Name ") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
}

function UpdateValidation(PoleName, TransFormerName) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var txtGridPoleName = document.getElementById(PoleName);
    var ddlGridTransFormerName = document.getElementById(TransFormerName);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridPoleName, "Pole Name") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlGridTransFormerName, "Transformer") == false) { return false; }
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};       