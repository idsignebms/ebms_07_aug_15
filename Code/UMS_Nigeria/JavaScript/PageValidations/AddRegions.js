﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {
    var txtRegionName = document.getElementById('UMSNigeriaBody_txtRegionName');
    var ddlStateName = document.getElementById('UMSNigeriaBody_ddlCountry');
    var IsValid = true;

    if (TextBoxValidationlbl(txtRegionName, document.getElementById("spanRegionName"), "Region Name") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlStateName, document.getElementById("spanddlCountry"), "Country") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
}

function UpdateValidation(RegionName,State) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var txtGridRegionName = document.getElementById(RegionName);
    var ddlStateName = document.getElementById(State);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridRegionName, "Region Name") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlStateName, "Country") == false) { return false; }
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};    