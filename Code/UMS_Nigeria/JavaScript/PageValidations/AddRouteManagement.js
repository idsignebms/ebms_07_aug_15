﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {
    var ddlBusinessUnit = document.getElementById('UMSNigeriaBody_ddlBusinessUnit');
    var txtRouteName = document.getElementById('UMSNigeriaBody_txtRouteName');
    var txtRouteCode = document.getElementById('UMSNigeriaBody_txtRouteCode');
    var hfRouteCodeLength = document.getElementById('UMSNigeriaBody_hfRouteCodeLength');


    var IsValid = true;

    if (DropDownlistValidationlbl(ddlBusinessUnit, document.getElementById("spanBusinessUnits"), "Business Unit") == false) IsValid = false;
    if (TextBoxValidationlbl(txtRouteName, document.getElementById("spanRouteName"), "Route Name") == false) IsValid = false;
    if (TextBoxValidationlbl(txtRouteCode, document.getElementById("spanRouteCode"), "Route Code") == false) IsValid = false;
    //if (TextBoxValidationlbl(txtAvgMaxLimit, document.getElementById("spanAvgMaxLimit"), "Range") == false) IsValid = false;
    if (txtRouteCode.value != "") {
        if (TextBoxLengthValidationLbl(txtRouteCode, hfRouteCodeLength.value + " Chars", document.getElementById("spanRouteCode"), hfRouteCodeLength.value) == false) IsValid = false;
    }
    if (!IsValid) {
        return false;
    }
}

function UpdateValidation(BUID, RouteName, RouteCode) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var BUID = document.getElementById(BUID);
    var RouteName = document.getElementById(RouteName);
    var RouteCode = document.getElementById(RouteCode);
    var hfRouteCodeLength = document.getElementById('UMSNigeriaBody_hfRouteCodeLength');

    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, BUID, "Business Unit") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, RouteName, "Route Name") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, RouteCode, "Route Code") == false) { return false; }
    if (TextmsgInPopup(mpeAlert, lblAlertMsg, RouteCode, hfRouteCodeLength.value, "Route code must be " + hfRouteCodeLength.value + " characters") == false) { return false; }
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};    