﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {

    var txtServiceCenterName = document.getElementById('UMSNigeriaBody_txtServiceCenterName');
    //var txtSCCode = document.getElementById('UMSNigeriaBody_txtSCCode');
    var ddlServiceUnitName = document.getElementById('UMSNigeriaBody_ddlServiceUnitName');
    var hfSCCodeLength = document.getElementById('UMSNigeriaBody_hfSCCodeLength');

    var txtCity = document.getElementById('UMSNigeriaBody_txtCity');
    var txtAddress1 = document.getElementById('UMSNigeriaBody_txtAddress1');
    var txtAddress2 = document.getElementById('UMSNigeriaBody_txtAddress2');
    var txtZipCode = document.getElementById('UMSNigeriaBody_txtZipCode');
    
    var IsValid = true;

    if (DropDownlistValidationlbl(ddlServiceUnitName, document.getElementById("spanddlServiceUnitName"), "Service Unit") == false) IsValid = false;
    if (TextBoxValidationlbl(txtServiceCenterName, document.getElementById("spanServiceCenterName"), "Service Center Name ") == false) IsValid = false;
//    if (TextBoxValidationlbl(txtSCCode, document.getElementById("spanSCCode"), "Service Center Code") == false) IsValid = false;
    if (TextBoxValidationlbl(txtCity, document.getElementById("spanCity"), "City Name") == false) IsValid = false;
    if (TextBoxValidationlbl(txtAddress1, document.getElementById("spanAddress1"), "Address 1") == false) IsValid = false;
    if (TextBoxValidationlbl(txtAddress2, document.getElementById("spanAddress2"), "Address 2") == false) IsValid = false;
    //if (TextBoxValidationlbl(txtZipCode, document.getElementById("spanZip"), "Zipcode") == false) IsValid = false;

//    if (TextBoxValidationlbl(txtZipCode, document.getElementById("spanZip"), "Zip Code") == false) {
//        IsValid = false;
//    } else
    if (CustomerTextboxZipCodelbl(txtZipCode, document.getElementById("spanZip"), 10, 6) == false) {
        IsValid = false;
    }

    //    if (txtSCCode.value != "") {
    //        if (TextBoxLengthValidationLbl(txtSCCode, hfSCCodeLength.value + " Chars", document.getElementById("spanSCCode"), hfSCCodeLength.value) == false) IsValid = false;
    //    }
    if (!IsValid) {
        return false;
    }
}

function UpdateValidation(ServiceCenterName, ServiceUnitName, Address1, Address2, City, Zip) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var txtGridServiceCenterName = document.getElementById(ServiceCenterName);

    var txtGridAddress1 = document.getElementById(Address1);
    var txtGridAddress2 = document.getElementById(Address2);
    var txtGridCity = document.getElementById(City);
    var txtGridZip = document.getElementById(Zip);

    //var SCCode = document.getElementById(SCCode);
    var ddlGridServiceUnitName = document.getElementById(ServiceUnitName);
    //var hfSCCodeLength = document.getElementById('UMSNigeriaBody_hfSCCodeLength');

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridServiceCenterName, "Service Center Name") == false) { return false; }
    //if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, SCCode, "Service Center Code") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlGridServiceUnitName, "Service Unit") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridAddress1, "Address1") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridAddress2, "Address2") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridCity, "City") == false) { return false; }
//    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridZip, "Zip Code") == false) { return false; }else
     if (CustomerTextboxZipCodelblInPopup(mpeAlert, lblAlertMsg, txtGridZip, 10, 6) == false) { return false; }
    //if (TextmsgInPopup(mpeAlert, lblAlertMsg, SCCode, hfSCCodeLength.value, "Service center code must be " + hfSCCodeLength.value + " characters") == false) { return false; }
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};       