﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {

    var txtServiceUnitName = document.getElementById('UMSNigeriaBody_txtServiceUnitName');
    //var txtSUCode = document.getElementById('UMSNigeriaBody_txtSUCode');
    var ddlBusinessUnitName = document.getElementById('UMSNigeriaBody_ddlBusinessUnitName');
    var hfSUCodeLength = document.getElementById('UMSNigeriaBody_hfSUCodeLength');
    var txtCity = document.getElementById('UMSNigeriaBody_txtCity');
    var txtAddress1 = document.getElementById('UMSNigeriaBody_txtAddress1');
    var txtAddress2 = document.getElementById('UMSNigeriaBody_txtAddress2');
    var txtZipCode = document.getElementById('UMSNigeriaBody_txtZipCode');
    var IsValid = true;

    if (DropDownlistValidationlbl(ddlBusinessUnitName, document.getElementById("spanddlBusinessUnitName"), "Business Unit") == false) IsValid = false;
    if (TextBoxValidationlbl(txtServiceUnitName, document.getElementById("spanServiceUnitName"), "Service Unit Name ") == false) IsValid = false;

    //    if (TextBoxValidationlbl(txtSUCode, document.getElementById("spanSUCode"), "Service Unit Code") == false) IsValid = false;
    if (TextBoxValidationlbl(txtCity, document.getElementById("spanCity"), "City Name") == false) IsValid = false;
    if (TextBoxValidationlbl(txtAddress1, document.getElementById("spanAddress1"), "Address 1") == false) IsValid = false;
    if (TextBoxValidationlbl(txtAddress2, document.getElementById("spanAddress2"), "Address 2") == false) IsValid = false;
    //if (TextBoxValidationlbl(txtZipCode, document.getElementById("spanZip"), "Zipcode") == false) IsValid = false;

//    if (TextBoxValidationlbl(txtZipCode, document.getElementById("spanZip"), "Zip Code") == false) {
//        IsValid = false;
//    }else
     if (CustomerTextboxZipCodelbl(txtZipCode, document.getElementById("spanZip"), 10, 6) == false) {
        IsValid = false;
    }

    //    if (txtSUCode.value != "") {
    //        if (TextBoxLengthValidationLbl(txtSUCode, hfSUCodeLength.value + " Chars", document.getElementById("spanSUCode"), hfSUCodeLength.value) == false) IsValid = false;
    //    }
    if (!IsValid) {
        return false;
    }
}

function UpdateValidation(ServiceUnitName, BusinessUnitName, Address1, Address2, City, Zip) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var txtGridServiceUnitName = document.getElementById(ServiceUnitName);

    var txtGridAddress1 = document.getElementById(Address1);
    var txtGridAddress2 = document.getElementById(Address2);
    var txtGridCity = document.getElementById(City);
    var txtGridZip = document.getElementById(Zip);

    //var SUCode = document.getElementById(SUCode);
    var ddlGridBusinessUnitName = document.getElementById(BusinessUnitName);
    //var hfSUCodeLength = document.getElementById('UMSNigeriaBody_hfSUCodeLength');

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridServiceUnitName, "Service Unit Name") == false) { return false; }
    //if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, SUCode, "Service Unit Code") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlGridBusinessUnitName, "Business Unit") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridAddress1, "Address1") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridAddress2, "Address2") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridCity, "City") == false) { return false; }
//    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridZip, "Zip Code") == false) { return false; }else
     if (CustomerTextboxZipCodelblInPopup(mpeAlert, lblAlertMsg, txtGridZip, 10, 6) == false) { return false; }
    //if (TextmsgInPopup(mpeAlert, lblAlertMsg, SUCode, hfSUCodeLength.value, "Service unit code must be " + hfSUCodeLength.value + " characters") == false) { return false; }
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};    