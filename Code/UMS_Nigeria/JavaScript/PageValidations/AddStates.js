﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {

    var txtStateName = document.getElementById('UMSNigeriaBody_txtStateName');
    var txtDisplayCode = document.getElementById('UMSNigeriaBody_txtDisplayCode');
    var ddlCountryName = document.getElementById("UMSNigeriaBody_ddlCountryName");
    var ddlRegion = document.getElementById("UMSNigeriaBody_ddlRegion");

    var IsValid = true;

    if (DropDownlistValidationlbl(ddlCountryName, document.getElementById("spanCountryCode"), "Country") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlRegion, document.getElementById("spanRegion"), "Region") == false) IsValid = false;
    if (TextBoxValidationlbl(txtStateName, document.getElementById("spanStateName"), "State Name") == false) IsValid = false;
    if (TextBoxValidationlbl(txtDisplayCode, document.getElementById("spanDisplayCode"), "Display Code") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
}

function UpdateValidation(StateName, DisplayCode, CountryCode, RegionId) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var txtGridStateName = document.getElementById(StateName);
    var txtGridDisplayCode = document.getElementById(DisplayCode);
    var ddlGridCountryName = document.getElementById(CountryCode);
    var ddlRegionId = document.getElementById(RegionId);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridStateName, "State Name") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridDisplayCode, "Display Code") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlGridCountryName, "Country") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlRegionId, "Region") == false) { return false; }

}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};       