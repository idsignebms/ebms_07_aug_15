﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {
    var txtSubCategoryName = document.getElementById('UMSNigeriaBody_txtSubCategoryName');
    var ddlCategory = document.getElementById('UMSNigeriaBody_ddlCategory');

    var IsValid = true;

    if (TextBoxValidationlbl(txtSubCategoryName, document.getElementById("spanSubCategoryName"), "Sub Category Name") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlCategory, document.getElementById("spanddlCategory"), "Tariff Category") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
}

function UpdateValidation(CategoryName, SubCategoryName) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var CategoryName = document.getElementById(CategoryName);
    var SubCategoryName = document.getElementById(SubCategoryName);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, SubCategoryName, "Sub Category Name") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, CategoryName, "Category Name") == false) { return false; }
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};     