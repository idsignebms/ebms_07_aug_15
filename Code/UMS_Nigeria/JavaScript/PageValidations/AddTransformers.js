﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function Validate() {

    var txtTransformerName = document.getElementById('UMSNigeriaBody_txtTransformerName');
    var ddlFeederName = document.getElementById('UMSNigeriaBody_ddlFeederName');


    var IsValid = true;

    if (DropDownlistValidationlbl(ddlFeederName, document.getElementById("spanddlFeederName"), "Feeder Name") == false) IsValid = false;
    if (TextBoxValidationlbl(txtTransformerName, document.getElementById("spanTransformerName"), "Transformer Name ") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
}
function UpdateValidation(TransFormerName, FeederName) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var txtGridTransFormerName = document.getElementById(TransFormerName);
    var ddlGridFeederName = document.getElementById(FeederName);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridTransFormerName, "Transformer Name") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlGridFeederName, "Feeder") == false) { return false; }
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};       