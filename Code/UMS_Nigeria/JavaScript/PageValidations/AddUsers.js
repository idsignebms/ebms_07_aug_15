﻿var i = 1;
function AddFile() {
    i++;
    var div = document.createElement('DIV');
    div.innerHTML = '<input id="file' + i + '" name="file' + i + '" type="file" /><a href="#" style="font-weight:bold;" onclick="return RemoveFile(this)">Remove</a>';
    document.getElementById("divFile").appendChild(div);
    return false;
}
function AddEditFile() {
    i++;
    var div = document.createElement('DIV');
    div.innerHTML = '<input id="file' + i + '" name="file' + i + '" type="file" /><a href="#" style="font-weight:bold;" onclick="return RemoveEditFile(this)">Remove</a>';
    document.getElementById("divEditFile").appendChild(div);
    return false;
}
function RemoveFile(file) {
    document.getElementById("divFile").removeChild(file.parentNode);
    return false;
}
function RemoveEditFile(file) {
    document.getElementById("divEditFile").removeChild(file.parentNode);
    return false;
}
/*Check Box List Check All Starts*/
/*For Insert*/
function BUCheckAll() {
    var cbAll = document.getElementById('UMSNigeriaBody_BUcbAll');
    var chkBoxList = document.getElementById('UMSNigeriaBody_cblBusinessUnits');
    CheckBoxListSelect(cbAll, chkBoxList);
}
function BUUnCheckAll() {
    var chkBoxList = document.getElementById('UMSNigeriaBody_cblBusinessUnits');
    var cbAll = document.getElementById('UMSNigeriaBody_BUcbAll');
    Uncheck(cbAll, chkBoxList);
}
function SUCheckAll() {
    var cbAll = document.getElementById('UMSNigeriaBody_SUcbAll');
    var cblServiceUnits = document.getElementById('UMSNigeriaBody_cblServiceUnits');
    CheckBoxListSelect(cbAll, cblServiceUnits);
}
function SUUnCheckAll() {
    var cblServiceUnits = document.getElementById('UMSNigeriaBody_cblServiceUnits');
    var cbAll = document.getElementById('UMSNigeriaBody_SUcbAll');
    Uncheck(cbAll, cblServiceUnits);
}
function SCCheckAll() {
    var cbAll = document.getElementById('UMSNigeriaBody_SCcbAll');
    var cblServiceCenters = document.getElementById('UMSNigeriaBody_cblServiceCenters');
    CheckBoxListSelect(cbAll, cblServiceCenters);
}
function SCUnCheckAll() {
    var cblServiceCenters = document.getElementById('UMSNigeriaBody_cblServiceCenters');
    var cbAll = document.getElementById('UMSNigeriaBody_SCcbAll');
    Uncheck(cbAll, cblServiceCenters);
}
function COCheckAll() {
    var COcbAll = document.getElementById('UMSNigeriaBody_COcbAll');
    var cblCashOffices = document.getElementById('UMSNigeriaBody_cblCashOffices');
    CheckBoxListSelect(COcbAll, cblCashOffices);
}
function COUnCheckAll() {
    var cblCashOffices = document.getElementById('UMSNigeriaBody_cblCashOffices');
    var cbAll = document.getElementById('UMSNigeriaBody_COcbAll');
    Uncheck(cbAll, cblCashOffices);
}

/*For Edit*/
function BUEditCheckAll() {
    var cbAll = document.getElementById('UMSNigeriaBody_BUcbEditAll');
    var chkBoxList = document.getElementById('UMSNigeriaBody_cblEditBusinessUnits');
    CheckBoxListSelect(cbAll, chkBoxList);
}
function BUEditUnCheckAll() {
    var chkBoxList = document.getElementById('UMSNigeriaBody_cblEditBusinessUnits');
    var cbAll = document.getElementById('UMSNigeriaBody_BUcbEditAll');
    Uncheck(cbAll, chkBoxList);
}
function SUEditCheckAll() {
    var cbAll = document.getElementById('UMSNigeriaBody_SUcbEditAll');
    var cblEditServiceUnits = document.getElementById('UMSNigeriaBody_cblEditServiceUnits');
    CheckBoxListSelect(cbAll, cblEditServiceUnits);
}
function SUEditUnCheckAll() {
    var cblEditServiceUnits = document.getElementById('UMSNigeriaBody_cblEditServiceUnits');
    var cbAll = document.getElementById('UMSNigeriaBody_SUcbEditAll');
    Uncheck(cbAll, cblEditServiceUnits);
}
function SCEditCheckAll() {
    var cbAll = document.getElementById('UMSNigeriaBody_SCcbEditAll');
    var cblServiceCenters = document.getElementById('UMSNigeriaBody_cblEditServiceCenters');
    CheckBoxListSelect(cbAll, cblServiceCenters);
}
function SCEditUnCheckAll() {
    var cblServiceCenters = document.getElementById('UMSNigeriaBody_cblEditServiceCenters');
    var cbAll = document.getElementById('UMSNigeriaBody_SCcbEditAll');
    Uncheck(cbAll, cblServiceCenters);
}
function COEditCheckAll() {
    var cbAll = document.getElementById('UMSNigeriaBody_COcbEditAll');
    var cblEditCashOffices = document.getElementById('UMSNigeriaBody_cblEditCashOffices');
    CheckBoxListSelect(cbAll, cblEditCashOffices);
}
function COEditUnCheckAll() {
    var cblEditCashOffices = document.getElementById('UMSNigeriaBody_cblEditCashOffices');
    var cbAll = document.getElementById('UMSNigeriaBody_COcbEditAll');
    Uncheck(cbAll, cblEditCashOffices);
}
function Validate() {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var ddlDesignation = document.getElementById('UMSNigeriaBody_ddlDesignation');
    var txtEmpId = document.getElementById('UMSNigeriaBody_txtEmpId');
    var txtPassword = document.getElementById('UMSNigeriaBody_txtPassword');
    var txtReEnterPassword = document.getElementById('UMSNigeriaBody_txtReEnterPassword');
    var txtName = document.getElementById('UMSNigeriaBody_txtName');
    var txtSurName = document.getElementById('UMSNigeriaBody_txtSurName');
    var txtPrimaryEmail = document.getElementById('UMSNigeriaBody_txtPrimaryEmail');
    var txtSecondaryEmail = document.getElementById('UMSNigeriaBody_txtSecondaryEmail');
    var rblGender = document.getElementById('UMSNigeriaBody_rblGender');
    var txtAddress = document.getElementById('UMSNigeriaBody_txtAddress');
    var txtContactNo = document.getElementById('UMSNigeriaBody_txtContactNo');
    var txtCode1 = document.getElementById('UMSNigeriaBody_txtCode1');
    var txtCode2 = document.getElementById('UMSNigeriaBody_txtCode2');
    var txtAnotherContactNo = document.getElementById('UMSNigeriaBody_txtAnotherContactNo');
    var ddlRoleId = document.getElementById('UMSNigeriaBody_ddlRoleId');
    var fupHT = document.getElementById("UMSNigeriaBody_fupDocument");
    var fupPhoto = document.getElementById("UMSNigeriaBody_fupPhoto");
    var divBusinessUnits = document.getElementById('UMSNigeriaBody_divBusinessUnits');
    var divServiceCenters = document.getElementById('UMSNigeriaBody_divServiceCenters');
    var divServiceUnits = document.getElementById('UMSNigeriaBody_divServiceUnits');
    var divCashOffices = document.getElementById('UMSNigeriaBody_divCashOffices');
    var hfmailId = document.getElementById('UMSNigeriaBody_hfmailId').value;
    var rblIsMobileAccess = document.getElementById('UMSNigeriaBody_rblIsMobileAccess');
    var ddlServiceUnits = document.getElementById('UMSNigeriaBody_ddlServiceUnits');


    var IsValid = true;

    if (TextBoxValidationlbl(txtEmpId, document.getElementById("spanEmpId"), "User Id") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlDesignation, document.getElementById("spanDisgnation"), "Designation") == false) IsValid = false;

    if (TextBoxValidationlbl(txtPassword, document.getElementById("spanPwd"), "Password") == false) IsValid = false;

    if (TextBoxValidationlbl(txtReEnterPassword, document.getElementById("spanReEnterPassword"), "Re-Enter Password") == false) IsValid = false;

    if (txtReEnterPassword != "") {
        if (txtPassword.value != txtReEnterPassword.value) {

            var spanReEnterPassword = document.getElementById("spanReEnterPassword");

            spanReEnterPassword.innerHTML = "Passwords are Mismatch";

            spanReEnterPassword.style.display = "block";

            txtReEnterPassword.style.border = "1px solid red";
            IsValid = false;
        }
    }
    if (TextBoxValidationlbl(txtName, document.getElementById("spanName"), "Name") == false) IsValid = false;
    if (TextBoxValidationlbl(txtSurName, document.getElementById("spanSurName"), "Surname") == false) IsValid = false;
    if (hfmailId == 1 || txtPrimaryEmail.value != "") {
        if (TextBoxValidationlbl(txtPrimaryEmail, document.getElementById("spanPrimaryEmail"), "Primary Email Id") == false) IsValid = false;
        else
            if (EmailValidation(txtPrimaryEmail) == false) {
                var spanEmail = document.getElementById("spanPrimaryEmail");
                spanEmail.innerHTML = "Please Enter Valid Email Id";
                spanEmail.style.display = "block";
                txtPrimaryEmail.style.border = "1px solid red";
                IsValid = false;
            }
            else {
                txtPrimaryEmail.style.border = "1px solid #8E8E8E";
                document.getElementById("spanPrimaryEmail").innerHTML = "";
            }
    }
    else {
        txtPrimaryEmail.style.border = "1px solid #8E8E8E";
        document.getElementById("spanPrimaryEmail").innerHTML = "";
    }

    if (Trim(txtSecondaryEmail.value) != "") {
        if (EmailValidation(txtSecondaryEmail) == false) {
            var spanEmail = document.getElementById("spanSecondaryEmail");
            spanEmail.innerHTML = "Please Enter Valid Email Id";
            spanEmail.style.display = "block";
            txtSecondaryEmail.style.border = "1px solid red";
            IsValid = false;
        }
        else {
            txtSecondaryEmail.style.border = "1px solid #8E8E8E";
            document.getElementById("spanSecondaryEmail").innerHTML = "";
        }
    }
    else {
        txtSecondaryEmail.style.border = "1px solid #8E8E8E";
        document.getElementById("spanSecondaryEmail").innerHTML = "";
    }
    if (RadioButtonListlbl(rblGender, "spanGender", " Gender") == false) IsValid = false;
    if (TextBoxValidationlbl(txtAddress, document.getElementById("spanAddress"), "Address") == false) IsValid = false;

    if (TextBoxValidationlbl(txtCode1, document.getElementById("spanContactNo"), "Code") == false) IsValid = false;
    if (TextBoxValidationlbl(txtContactNo, document.getElementById("spanContactNo"), "Contact No") == false) IsValid = false;
    else if (ContactNoLengthMinMax(txtContactNo, 6, 10, document.getElementById("spanContactNo"), txtCode1, 3) == false) {
        IsValid = false;
    }
    else {
        txtContactNo.style.border = "1px solid #8E8E8E";
        txtCode1.style.border = "1px solid #8E8E8E";
        document.getElementById("spanContactNo").innerHTML = "";
    }

    if (Trim(txtAnotherContactNo.value) != "" || Trim(txtCode2.value) != "") {
        if (ContactNoLengthMinMax(txtAnotherContactNo, 6, 10, document.getElementById("spanAnotherNo"), txtCode2, 3) == false) IsValid = false;
        else {
            txtAnotherContactNo.style.border = "1px solid #8E8E8E";
            txtCode2.style.border = "1px solid #8E8E8E";
            document.getElementById("spanAnotherNo").innerHTML = "";
        }
    }
    else {
        txtAnotherContactNo.style.border = "1px solid #8E8E8E";
        txtCode2.style.border = "1px solid #8E8E8E";
        document.getElementById("spanAnotherNo").innerHTML = "";
    }
    if (RadioButtonListlbl(rblIsMobileAccess, "spanIsMobileAccess", " One") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlRoleId, document.getElementById("spanRoleId"), "Role Id") == false) IsValid = false;

    //Photo validation           

    if (filevalidate(fupPhoto.value, "Photo") == false) return false;


    //Document validation
    var divFile = document.getElementById("divFile");
    var Docitems = divFile.getElementsByTagName("input");
    for (var i = 0; i < Docitems.length; i++) {
        var spanDocuments = document.getElementById("spanDocuments").innerHTML = "";
        if (!Trim(Docitems[i].value) == "") {
            if (!/(\.doc|\.docx|\.pdf|\.xlsx|\.xls)$/i.test(Docitems[i].value)) {
                var spanDocuments = document.getElementById("spanDocuments");
                spanDocuments.innerHTML = "Please select .doc, .docx, .xlsx , .pdf and .xls File Formats only";
                spanDocuments.style.display = "block";
                IsValid = false;
                break;
            }

        }
        else {
            var spanDocuments = document.getElementById("spanDocuments").innerHTML = "";
        }
    }

    if (divBusinessUnits != null) {
        var cblBusinessUnits = document.getElementById('UMSNigeriaBody_cblBusinessUnits');
        if (ValidateCblInPopup(mpeAlert, lblAlertMsg, cblBusinessUnits, ' Business Unit') == false) return false;
    }


    if (divServiceCenters != null) {
        var ddlBusinessUnits = document.getElementById('UMSNigeriaBody_ddlBusinessUnits');
        var ddlServiceUnits = document.getElementById('UMSNigeriaBody_ddlServiceUnits');
        var cblServiceCenters = document.getElementById('UMSNigeriaBody_cblServiceCenters');
        var cblCashOffices = document.getElementById('UMSNigeriaBody_cblCashOffices');
        if (DropDownlistValidationlbl(ddlBusinessUnits, document.getElementById("spanddlBusinessUnits"), "Business Unit") == false) IsValid = false;
        else if (DropDownlistValidationlbl(ddlServiceUnits, document.getElementById("spanddlServiceUnits"), "Service Unit") == false) IsValid = false;
        else if (ValidateCblInPopup(mpeAlert, lblAlertMsg, cblServiceCenters, ' Service Center') == false) return false;
        else if (ValidateCblInPopup(mpeAlert, lblAlertMsg, cblCashOffices, ' Cash Office') == false) return false;
    }
    if (divCashOffices != null) {
        var cblCashOffices = document.getElementById('UMSNigeriaBody_cblCashOffices');
        if (ValidateCblInPopup(mpeAlert, lblAlertMsg, cblCashOffices, ' Cash Office') == false) return false;
    }

    if (divServiceUnits != null) {
        var cblServiceUnits = document.getElementById('UMSNigeriaBody_cblServiceUnits');
        if (ValidateCblInPopup(mpeAlert, lblAlertMsg, cblServiceUnits, ' Service Unit') == false) return false;
    }

    if (!IsValid) {
        return false;
    }

}

function filevalidate(filename, Message) {
    document.getElementById("spanPhoto").innerHTML = "";

    if (Trim(filename) != "") {
        if (!/(\.jpeg|\.jpg|\.png|\.gif)$/i.test(filename)) {
            var spanPhoto = document.getElementById("spanPhoto");
            spanPhoto.innerHTML = "Please select .jpeg, .jpg, .png and .gif File Formats only";
            document.getElementById("UMSNigeriaBody_fupPhoto").value = ''; //--faiz id103
            spanPhoto.style.display = "block";
            return false;
        }
    }
}

function pdffilevalidate(filename, Message) {
    document.getElementById("spanDocuments").innerHTML = "";
    if (Trim(filename) != "") {
        if (!/(\.doc|\.docx|\.pdf|\.xlsx|\.xls)$/i.test(filename)) {
            var spanDocuments = document.getElementById("spanDocuments");
            spanDocuments.innerHTML = "Please select .doc, .docx, .xlsx , .pdf and .xls File Formats only";
            document.getElementById("UMSNigeriaBody_fupDocument").value = ''; //--faiz id103
            spanDocuments.style.display = "block";
            return false;
        }
    }
}

function pdffileEditvalidate(filename, Message) {
    document.getElementById("spanEditDocuments").innerHTML = "";
    if (Trim(filename) != "") {
        if (!/(\.doc|\.docx|\.pdf|\.xlsx|\.xls)$/i.test(filename)) {
            var spanDocuments = document.getElementById("spanEditDocuments");
            spanDocuments.innerHTML = "Please select .doc, .docx, .xlsx , .pdf and .xls File Formats only";
            document.getElementById("UMSNigeriaBody_fupEditDocument").value = ''; //--faiz id103
            spanDocuments.style.display = "block";
            return false;
        }
    }
}

function EditValidate() {

    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var ddlEditDesignation = document.getElementById('UMSNigeriaBody_ddlEditDesignation');

    var txtEditName = document.getElementById('UMSNigeriaBody_txtEditName');
    var txtEditSurName = document.getElementById('UMSNigeriaBody_txtEditSurName');
    var txtEditPrimaryEmail = document.getElementById('UMSNigeriaBody_txtEditPrimaryEmail');
    var txtEditSecondaryEmail = document.getElementById('UMSNigeriaBody_txtEditSecondaryEmail');
    var rblEditGender = document.getElementById('UMSNigeriaBody_rblEditGender');
    var txtEditAddress = document.getElementById('UMSNigeriaBody_txtEditAddress');
    var txtEditContactNo1 = document.getElementById('UMSNigeriaBody_txtEditContactNo1');
    var txtEditContactNo2 = document.getElementById('UMSNigeriaBody_txtEditContactNo2');
    var txtEditCode1 = document.getElementById('UMSNigeriaBody_txtEditCode1');
    var txtEditCode2 = document.getElementById('UMSNigeriaBody_txtEditCode2');
    var ddlEditRoleId = document.getElementById('UMSNigeriaBody_ddlEditRoleId');
    var fupHT = document.getElementById("UMSNigeriaBody_fupEditDocument");
    var fupPhoto = document.getElementById("UMSNigeriaBody_fupEditPhoto");
    var divEditBusinessUnits = document.getElementById('UMSNigeriaBody_divEditBusinessUnits');
    var divEditServiceCenters = document.getElementById('UMSNigeriaBody_divEditServiceCenters');
    var divEditServiceUnits = document.getElementById('UMSNigeriaBody_divEditServiceUnits');
    var divEditCashOffices = document.getElementById('UMSNigeriaBody_divEditCashOffices');
    var hfEditmailId = document.getElementById('UMSNigeriaBody_hfEditmailId').value;
    var rblEditIsMobileAccess = document.getElementById('UMSNigeriaBody_rblEditIsMobileAccess');

    var IsValid = true;

    if (TextBoxValidationlbl(txtEditName, document.getElementById("spanEditName"), "Name") == false) IsValid = false;
    if (TextBoxValidationlbl(txtEditSurName, document.getElementById("spanEditSurName"), "Surname") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlEditDesignation, document.getElementById("spanEditDesignation"), "Designation") == false) IsValid = false;

    if (hfEditmailId == 1 || txtEditPrimaryEmail.value != "") {
        if (TextBoxValidationlbl(txtEditPrimaryEmail, document.getElementById("spanEditPrimaryEmail"), "Primary Email Id") == false) IsValid = false;
        if (EmailValidation(txtEditPrimaryEmail) == false) {
            var spanEmail = document.getElementById("spanEditPrimaryEmail");
            spanEmail.innerHTML = "Please Enter Valid Email Id";
            spanEmail.style.display = "block";
            txtEditPrimaryEmail.style.border = "1px solid red";
            IsValid = false;
        }
        else {
            txtEditPrimaryEmail.style.border = "1px solid #8E8E8E";
            document.getElementById("spanEditPrimaryEmail").innerHTML = "";
        }
    }
    else {
        txtEditPrimaryEmail.style.border = "1px solid #8E8E8E";
        document.getElementById("spanEditPrimaryEmail").innerHTML = "";
    }
    if (Trim(txtEditSecondaryEmail.value) != "") {
        if (EmailValidation(txtEditSecondaryEmail) == false) {
            var spanEmail = document.getElementById("spanEditSecondaryEmail");
            spanEmail.innerHTML = "Please Enter Valid Email Id";
            spanEmail.style.display = "block";
            txtEditSecondaryEmail.style.border = "1px solid red";
            IsValid = false;
        }
        else {
            txtEditSecondaryEmail.style.border = "1px solid #8E8E8E";
            document.getElementById("spanEditSecondaryEmail").innerHTML = "";
        }
    }
    else {
        txtEditSecondaryEmail.style.border = "1px solid #8E8E8E";
        document.getElementById("spanEditSecondaryEmail").innerHTML = "";
    }

    if (RadioButtonListlbl(rblEditGender, "spanEditGender", " Gender") == false) IsValid = false;
    if (TextBoxValidationlbl(txtEditAddress, document.getElementById("spanEditAddress"), "Address") == false) IsValid = false;

    if (TextBoxValidationlbl(txtEditCode1, document.getElementById("spanEditContactNo"), "Code") == false) IsValid = false;
    if (TextBoxValidationlbl(txtEditContactNo1, document.getElementById("spanEditContactNo"), "Contact No") == false) IsValid = false;

    if (Trim(txtEditContactNo1.value) != "") {
        if (ContactNoLengthMinMax(txtEditContactNo1, 6, 10, document.getElementById("spanEditContactNo"), txtEditCode1, 3) == false) {
            alert('a');
            IsValid = false;
        }
        else {
            txtEditContactNo1.style.border = "1px solid #8E8E8E";
            txtEditCode1.style.border = "1px solid #8E8E8E";
            document.getElementById("spanEditContactNo").innerHTML = "";
        }
    }


    if (Trim(txtEditContactNo2.value) != "" || Trim(txtEditCode2.value) != "") {
        if (ContactNoLengthMinMax(txtEditContactNo2, 6, 10, document.getElementById("spanEditContactNo2"), txtEditCode2, 3) == false) {
            alert('b');
            IsValid = false;
        }
        else {
            txtEditContactNo2.style.border = "1px solid #8E8E8E";
            txtEditCode2.style.border = "1px solid #8E8E8E";
            document.getElementById("spanEditContactNo2").innerHTML = "";
        }
    }
    else {
        txtEditContactNo2.style.border = "1px solid #8E8E8E";
        txtEditCode2.style.border = "1px solid #8E8E8E";
        document.getElementById("spanEditContactNo2").innerHTML = "";
    }

    if (RadioButtonListlbl(rblEditIsMobileAccess, "spanEditIsMobileAccess", " One") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlEditRoleId, document.getElementById("spanEditRoleId"), "Role Id") == false) IsValid = false;

    //Photo validation           

    if (fileEditvalidate(fupPhoto.value, "Photo") == false) return false;


    //Document validation
    var divFile = document.getElementById("divEditFile");
    var Docitems = divFile.getElementsByTagName("input");
    for (var i = 0; i < Docitems.length; i++) {
        var spanDocuments = document.getElementById("spanEditDocuments").innerHTML = "";
        if (!Trim(Docitems[i].value) == "") {
            if (!/(\.doc|\.docx|\.pdf|\.xlsx|\.xls)$/i.test(Docitems[i].value)) {
                var spanDocuments = document.getElementById("spanEditDocuments");
                spanDocuments.innerHTML = "Please select .doc, .docx, .xlsx , .pdf and .xls File Formats only";
                spanDocuments.style.display = "block";
                IsValid = false;
                break;
            }

        }
        else {
            var spanDocuments = document.getElementById("spanEditDocuments").innerHTML = "";
        }
    }

    if (divEditBusinessUnits != null) {
        var cblBusinessUnits = document.getElementById('UMSNigeriaBody_cblEditBusinessUnits');
        if (ValidateCblInPopup(mpeAlert, lblAlertMsg, cblBusinessUnits, ' Business Unit') == false) return false;
    }

    if (divEditServiceCenters != null) {
        var ddlEditBusinessUnits = document.getElementById('UMSNigeriaBody_ddlEditBusinessUnits');
        var ddlEditServiceUnits = document.getElementById('UMSNigeriaBody_ddlEditServiceUnits');
        var cblEditCashOffices = document.getElementById('UMSNigeriaBody_cblEditCashOffices');
        var cblEditServiceCenters = document.getElementById('UMSNigeriaBody_cblEditServiceCenters');
        if (DropDownlistValidationlbl(ddlEditBusinessUnits, document.getElementById("spanEditddlBusinessUnits"), "Business Unit") == false) { IsValid = false; }
        else if (DropDownlistValidationlbl(ddlEditServiceUnits, document.getElementById("spanEditddlServiceUnits"), "Service Unit") == false) { IsValid = false; }
        else if (ValidateCblInPopup(mpeAlert, lblAlertMsg, cblEditServiceCenters, ' Service Center') == false) { return false; }
        else if (ValidateCblInPopup(mpeAlert, lblAlertMsg, cblEditCashOffices, ' Cash Office') == false) { return false; }
    }
    if (divEditCashOffices != null) {
        var cblEditCashOffices = document.getElementById('UMSNigeriaBody_cblEditCashOffices');
        if (ValidateCblInPopup(mpeAlert, lblAlertMsg, cblEditCashOffices, ' Cash Office') == false) return false;
    }

    if (divEditServiceUnits != null) {
        var cblServiceUnits = document.getElementById('UMSNigeriaBody_cblEditServiceUnits');
        if (ValidateCblInPopup(mpeAlert, lblAlertMsg, cblServiceUnits, ' Service Unit') == false) return false;
    }

    if (!IsValid) {
        return false;
    }
    else {
        var divFile = document.getElementById("divEditFile");
        var files = divFile.getElementsByTagName("input");
        var hfDocuments = document.getElementById('UMSNigeriaBody_hfEditDocuments');
        for (var i = 0; i < files.length; i++) {
            hfDocuments.value = hfDocuments.value + files[i].value;
        }
    }
}

function fileEditvalidate(filename, Message) {
    document.getElementById("spanfupEditPhoto").innerHTML = "";
    if (Trim(filename) != "") {

        if (!/(\.jpeg|\.jpg|\.png|\.gif)$/i.test(filename)) {
            var spanPhoto = document.getElementById("spanfupEditPhoto");
            spanPhoto.innerHTML = "Please select .jpeg, .jpg, .png and .gif File Formats only";
            spanPhoto.style.display = "block";
            return false;
        }
    }
}
function EditPasswordValidate() {
    var txtEditReEnterPassword = document.getElementById('UMSNigeriaBody_txtEditReEnterPassword');
    var txtEditPassword = document.getElementById('UMSNigeriaBody_txtEditPassword');

    var IsValid = true;
    if (TextBoxValidationlbl(txtEditPassword, document.getElementById("spanEditPassword"), "Password") == false) IsValid = false;
    if (TextBoxValidationlbl(txtEditReEnterPassword, document.getElementById("spanEditReEnterPassword"), "Re-Enter Password") == false) IsValid = false;

    if (txtEditPassword.value != txtEditReEnterPassword.value) {
        var spanEditPassword = document.getElementById("spanEditPassword");
        var spanEditReEnterPassword = document.getElementById("spanEditReEnterPassword");

        spanEditReEnterPassword.innerHTML = "Passwords are Mismatch";

        spanEditReEnterPassword.style.display = "block";

        txtEditReEnterPassword.style.border = "1px solid red";
        IsValid = false;
    }
    if (!IsValid) {
        return false;
    }
}



//onchange Event For drop downlist
function RoleDropDownlistOnChangelbl(ddl, lblId, field) {
    var lbl = document.getElementById(lblId);
    lbl.innerHTML = "";
    lbl.style.display = "none";
    if (ddl.selectedIndex == 0) {
        lbl.innerHTML = "Select " + field;
        lbl.style.display = "inline";
        return false;
    }
    else {
        //document.getElementById('UMSNigeriaBody_btnDummyRoleId').click();
        return true;
    }
}

function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlEditMessage');
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};     