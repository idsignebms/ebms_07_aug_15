﻿function Validate() {
    var IsValid = true;
    var BatchDate = document.getElementById('UMSNigeriaBody_txtBatchDate');
    var BatchNo = document.getElementById('UMSNigeriaBody_txtBatchNo');
    var txtBatchTotal = document.getElementById('UMSNigeriaBody_txtBatchTotal');
    var ddlReasonList = document.getElementById('UMSNigeriaBody_ddlReasonList');

    if (TextBoxValidationlbl(BatchDate, document.getElementById('spanBatchDate'), " Batch Date") == false) IsValid = false;
    if (isDateLbl(BatchDate.value, BatchDate, "spanBatchDate") == false) IsValid = false;
    if (TextBoxValidationlbl(BatchNo, document.getElementById('spanBatchNo'), " Batch No") == false) IsValid = false;
    if (TextBoxValidationlbl(txtBatchTotal, document.getElementById('spanBatchTotal'), " Batch Total") == false) IsValid = false;
    else if (TextBoxBlurValidationlblWithNoZero(txtBatchTotal, 'spanBatchTotal', " Batch Total") == false) IsValid = false;
//    else if (parseFloat(txtBatchTotal.value) == 0) {//        
//        document.getElementById('spanBatchTotal').innerHTML = "Enter Valid Batch Total";
//        document.getElementById('spanBatchTotal').style.display = "block";
//        txtBatchTotal.style.border = "1px solid red";
//        IsValid = false;
//    }
    //            if (DropDownlistValidationlbl(ddlCashier, document.getElementById('spanddlCashier'), "Cashier") == false) IsValid = false;
    //            if (DropDownlistValidationlbl(ddlCashOffice, document.getElementById('spanddlCashOffice'), "Cash Office") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlReasonList, document.getElementById('spanReason'), "Reason") == false) IsValid = false;
    if (!IsValid) {
        return false;
    }
}

function ValidateDate() {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    document.getElementById('spanBatchDate').innerHTML = "";
    var txtBatchDate = document.getElementById('UMSNigeriaBody_txtBatchDate');
    if (TextBoxValidationlbl(txtBatchDate, document.getElementById('spanBatchDate'), "Batch Date") == false)
        return false;
    else
        if (isDateLbl(txtBatchDate.value, txtBatchDate, "spanBatchDate") == false) {
            return false;
        }
        else {
            if (GreaterTodayInPopup(mpeAlert, lblAlertMsg, txtBatchDate, "Batch Date should not be Greater than the Today's Date") == false) {
                return false;
            }
        }
}
function DoBlur(fld) {
    ValidateDate();
}
function GetCurrencyFormate(obj) {
    obj.value = NewText(obj);
}
function isNumberKey(evt, txt) {

    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    if ((charCode == 46 && txt.value.split('.').length > 1)) {
        return false;
    }
}