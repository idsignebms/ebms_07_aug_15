﻿function Validate() {
    var IsValid = true;
    var mpeAlert = $find('UMSNigeriaBody_mpeAlert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblAlertMsg');
    var txtCredit = document.getElementById('UMSNigeriaBody_txtCreditAmount').value;
    var txtDebitAmount = document.getElementById('UMSNigeriaBody_txtDebitAmount').value;

    if (parseFloat(txtCredit) == 0)
        txtCredit = "";
    if (parseFloat(txtDebitAmount) == 0)
        txtDebitAmount = "";
    if (txtCredit.length == 0 && txtDebitAmount.length == 0) {
        IsValid = false;
        lblAlertMsg.innerHTML = 'Please enter Credit OR Debit Amount.';
        mpeAlert.show();

    }
    else if (txtCredit.length > 0 && txtDebitAmount.length > 0) {
        IsValid = false;
        lblAlertMsg.innerHTML = 'Please enter Credit OR Debit Amount.';
        mpeAlert.show();

    }
    if (IsValid)
        return true
    else
        return false;
}
function ValidateEdit() {
    var IsValid = true;
    var mpeAlert = $find('UMSNigeriaBody_mpeAlert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblAlertMsg');
    var txtCredit = document.getElementById('UMSNigeriaBody_txtAmoutCreditEdit').value;
    var txtDebitAmount = document.getElementById('UMSNigeriaBody_txtAmountDebitEdit').value;

    if (parseFloat(txtCredit) == 0)
        txtCredit = "";
    if (parseFloat(txtDebitAmount) == 0)
        txtDebitAmount = "";

    if (txtCredit.length == 0 && txtDebitAmount.length == 0) {
        IsValid = false;
        lblAlertMsg.innerHTML = 'Please enter Credit OR Debit Amount.';
        mpeAlert.show();

    }
    else if (txtCredit.length > 0 && txtDebitAmount.length > 0) {
        IsValid = false;
        lblAlertMsg.innerHTML = 'Please enter Credit OR Debit Amount.';
        mpeAlert.show();

    }
    if (IsValid)
        return true
    else
        return false;
}
function GetCurrencyFormate(obj) {
    obj.value = NewText(obj);
}
function isNumberKey(evt, txt) {

    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    if ((charCode == 46 && txt.value.split('.').length > 1)) {
        return false;
    }
}