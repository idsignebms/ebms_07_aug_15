﻿
function DoBlur(fld) {
    fld.className = 'normalfld';
}

function DoFocus(fld) {
    fld.className = 'focusfld';
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}


$(document).ready(function () {
    AutoComplete($('#UMSNigeriaBody_txtAccNo'), 1);
    AutoComplete($('#UMSNigeriaBody_txtMeterNo'), 2);
    DisplayMessage('UMSNigeriaBody_pnlMessage');
});

var prm = Sys.WebForms.PageRequestManager.getInstance();

prm.add_endRequest(function () {
    AutoComplete($('#UMSNigeriaBody_txtAccNo'), 1);
    AutoComplete($('#UMSNigeriaBody_txtMeterNo'), 2);
    DisplayMessage('UMSNigeriaBody_pnlMessage');
});

Sys.Application.add_init(function () {
    $create(Sys.Extended.UI.ModalPopupBehavior, { "BackgroundCssClass": "modalBackground", "CancelControlID": "btnDeActiveCancel", "PopupControlID": "PanelDeActivate", "dynamicServicePath": "/ConsumerManagement/AdvancedSearchCustomer.aspx", "id": "mpeDeActive" }, null, null, $get("btnDeActivate"));
});
Sys.Application.add_init(function () {
    $create(Sys.UI._UpdateProgress, { "associatedUpdatePanelId": null, "displayAfter": 500, "dynamicLayout": true }, null, null, $get("UMSNigeriaBody_UpdateProgress"));
});
Sys.Application.add_init(function () {
    $create(Sys.Extended.UI.ModalPopupBehavior, { "BackgroundCssClass": "modalBackground", "PopupControlID": "UMSNigeriaBody_UpdateProgress", "dynamicServicePath": "/ConsumerManagement/AdvancedSearchCustomer.aspx", "id": "UMSNigeriaBody_modalPopupLoading" }, null, null, $get("UMSNigeriaBody_UpdateProgress"));
});
Sys.Application.add_init(function () {
    $create(Sys.Extended.UI.ModalPopupBehavior, { "BackgroundCssClass": "modalBackground", "CancelControlID": "UMSNigeriaBody_btnAlertOk", "PopupControlID": "UMSNigeriaBody_PanelAlert", "dynamicServicePath": "/ConsumerManagement/AdvancedSearchCustomer.aspx", "id": "UMSNigeriaBody_mpeAlert" }, null, null, $get("UMSNigeriaBody_btnAlertDeActivate"));
});