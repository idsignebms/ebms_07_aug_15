﻿function CheckSUAll() {
    var cbAll = document.getElementById('UMSNigeriaBody_cbSUAll');
    var chkBoxList = document.getElementById('UMSNigeriaBody_cblSU');
    CheckBoxListSelect(cbAll, chkBoxList);
}
function UnCheckSUAll() {
    var chkBoxList = document.getElementById('UMSNigeriaBody_cblSU');
    var cbAll = document.getElementById('UMSNigeriaBody_cbSUAll');
    Uncheck(cbAll, chkBoxList);
}
function CheckSCAll() {
    var cbAll = document.getElementById('UMSNigeriaBody_cbSCAll');
    var chkBoxList = document.getElementById('UMSNigeriaBody_cblSC');
    CheckBoxListSelect(cbAll, chkBoxList);
}
function UnCheckSCAll() {
    var chkBoxList = document.getElementById('UMSNigeriaBody_cblSC');
    var cbAll = document.getElementById('UMSNigeriaBody_cbSCAll');
    Uncheck(cbAll, chkBoxList);
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};