﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function CheckAll(cbAll) {
    var chkBoxList = cbAll.parentNode.getElementsByTagName('table');
    CheckBoxListSelect(cbAll, chkBoxList[0]);
}
function UnCheckAll(cbAllId, cblId) {
    var chkBoxList = document.getElementById(cblId);
    var cbAll = document.getElementById(cbAllId);
    Uncheck(cbAll, chkBoxList);
}

function Validate() {
    var ddlFunctions = document.getElementById('UMSNigeriaBody_ddlFunctions');
    var ddlLevels = document.getElementById('UMSNigeriaBody_ddlLevels');
    var ddlBusinessUnit = document.getElementById('UMSNigeriaBody_ddlBusinessUnit');
    var gvLevelsSettings = document.getElementById('UMSNigeriaBody_gvLevelsSettings');
    var IsValid = true;

    if (DropDownlistValidationlbl(ddlFunctions, document.getElementById("spanFunction"), "Function") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlLevels, document.getElementById("spanLevels"), "No.of Levels") == false) IsValid = false;
    if (ddlBusinessUnit)
        if (DropDownlistValidationlbl(ddlBusinessUnit, document.getElementById("spanBusinessUnit"), "Business Unit") == false) IsValid = false;

    if (!IsValid)
        return false;
    var ddlRoles = gvLevelsSettings.getElementsByClassName("ddlRoles");
    var ddlAllUsers = gvLevelsSettings.getElementsByClassName("ddlAllUsers");
    var divUsersCheckBoxes = gvLevelsSettings.getElementsByClassName("divUsersCheckBoxes");
    var mpeMessage = $find('UMSNigeriaBody_mpegridalert');
    var lblMessage = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    for (var i = 0; i < ddlRoles.length; i++) {
        if (DropDownlistValidationsInPopup(mpeMessage, lblMessage, ddlRoles[i], "Role") == false) return false;
        if (ddlAllUsers[i].selectedIndex == 1) {
            var Users = divUsersCheckBoxes[i].getElementsByTagName("input");
            if (Users == null || Users.length == 0) {
                lblMessage.innerHTML = "There are no users available.";
                mpeMessage.show();
                return false;
            }
            else {
                var IsAtleastOneUser = false;
                for (var j = 0; j < Users.length; j++) {
                    if (Users[j].type == "checkbox") {
                        if (Users[j].checked) {
                            IsAtleastOneUser = true;
                            break;
                        }
                    }
                }
                if (!IsAtleastOneUser) {
                    lblMessage.innerHTML = "Please select atleast one user.";
                    mpeMessage.show();
                    return false;
                }
            }
        }
    }
    for (var i = 0; i < ddlRoles.length; i++) {
        for (var j = i + 1; j < ddlRoles.length; j++) {
            if (ddlRoles[i].selectedIndex == ddlRoles[j].selectedIndex) {
                if (ddlAllUsers[i].selectedIndex == 0 || ddlAllUsers[j].selectedIndex == 0) {
                    lblMessage.innerHTML = "One user can not be selected in multiple levels.";
                    mpeMessage.show();
                    return false;
                }
                else {
                    if (ddlAllUsers[i].selectedIndex == 1 && ddlAllUsers[j].selectedIndex == 1) {
                        var Users1 = divUsersCheckBoxes[i].getElementsByTagName("input");
                        var Users2 = divUsersCheckBoxes[j].getElementsByTagName("input");
                        if (Users1 != null && Users2 != null) {
                            for (var k = 0; k < Users1.length; k++) {
                                if (Users1[k].type == "checkbox" && Users2[k].type == "checkbox") {
                                    if (Users1[k].value == Users2[k].value && Users1[k].checked && Users2[k].checked) {
                                        lblMessage.innerHTML = "One user can not be selected in multiple levels.";
                                        mpeMessage.show();
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};      