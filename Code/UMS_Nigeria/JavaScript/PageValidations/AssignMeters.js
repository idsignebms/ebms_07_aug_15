﻿function ValidateGo() {
    var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo');
    var IsValid = true;

    if (TextBoxValidationlbl(txtAccountNo, document.getElementById("spanAccountNo"), "Global Account No / Old Account No") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
}

function Validate() {
    var IsValid = true;
    var txtMeterNo = document.getElementById('UMSNigeriaBody_txtMeterNo');
    var txtReason = document.getElementById('UMSNigeriaBody_txtReason');
    var txtReadingDate = document.getElementById('UMSNigeriaBody_txtMeterAssignedDate');
    //    if (parseInt(document.getElementById('UMSNigeriaBody_txtInitialReading').value) == 0)
    //        document.getElementById('UMSNigeriaBody_txtInitialReading').value = "0";
    var txtInitialReading = document.getElementById('UMSNigeriaBody_txtInitialReading');
    //txtInitialReading = parseFloat(txtInitialReading.value);
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    document.getElementById('UMSNigeriaBody_spnMeterNoStatus').innerHTML = "";
    if (TextBoxValidationlbl(txtMeterNo, document.getElementById('spnMeterNo'), 'Meter No') == false) IsValid = false;
    //if (TextBoxBlurMeterValidationlbl(txtInitialReading, document.getElementById('spnInitialReading'), ' Initial Meter Reading') == false) IsValid = false;
    if (TextBoxValidationlblZeroAccepts(txtInitialReading, document.getElementById('spnInitialReading'), ' Initial Meter Reading') == false) IsValid = false;

    if (TextBoxValidationlbl(txtReadingDate, document.getElementById('spanMeterAssignedDate'), " Meter Assigned Date") == false) {
        IsValid = false;
    }
    else if (isDateLbl(txtReadingDate.value, txtReadingDate, "spanMeterAssignedDate") == false) {
        IsValid = false;
    }
    else {
        if (GreaterTodayInPopup(mpeAlert, lblAlertMsg, txtReadingDate, "Meter Assigned Date should not be Greater than the Today's Date") == false) {
            IsValid = false;
        }
    }

    if (TextBoxValidationlbl(txtReason, document.getElementById('SpanReason'), 'Reason') == false) IsValid = false;
    if (!IsValid)
        return false;
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};

function DoBlur(fld) {
    fld.className = 'text-box hasDatepicker';
    ValidateDate();
}

function DoFocus(fld) {
    fld.className = 'text-box hasDatepicker';
}

function ValidateDate() {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    document.getElementById('spanMeterAssignedDate').innerHTML = "";
    var txtReadingDate = document.getElementById('UMSNigeriaBody_txtMeterAssignedDate');
    if (TextBoxValidationlbl(txtReadingDate, document.getElementById('spanMeterAssignedDate'), "Meter Assigned Date") == false)
        return false;
    else
        if (isDateLbl(txtReadingDate.value, txtReadingDate, "spanMeterAssignedDate") == false) {
            return false;
        }
        else {
            if (GreaterTodayInPopup(mpeAlert, lblAlertMsg, txtReadingDate, "Meter Assigned Date should not be Greater than the Today's Date") == false) {
                return false;
            }
        }
}


function setMeterDials(MeterDials) {
    $("#UMSNigeriaBody_txtInitialReading").attr('maxlength', MeterDials);
}