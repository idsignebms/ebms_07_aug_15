﻿function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};

function Validate() {
    var divFilterType = document.getElementById('UMSNigeriaBody_divFilterType');
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var IsValid = true;

    if (divFilterType != null) {
        var cbl = document.getElementById('UMSNigeriaBody_cblBatchNos');
        if (ValidateCblInPopup(mpeAlert, lblAlertMsg, cbl, 'BatchNo') == false) return false;
    }
    if (!IsValid) {
        return false;
    }
}        