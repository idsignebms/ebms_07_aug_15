﻿function pageLoad() {

    //Calendar('UMSNigeriaBody_txtNextDate', 'UMSNigeriaBody_imgDate', "-2:+8");
    //DatePicker($('#UMSNigeriaBody_txtBatchDate'), $('#UMSNigeriaBody_imgDate'));

    $(document).ready(function () {
        DisplayMessage('UMSNigeriaBody_pnlMessage');
        Calendar('UMSNigeriaBody_txtBatchDate', 'UMSNigeriaBody_imgDate', "-2:+8");
        AutoComplete($('#UMSNigeriaBody_txtBatchNo'), 4);
    });
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(function () {
        Calendar('UMSNigeriaBody_txtBatchDate', 'UMSNigeriaBody_imgDate', "-2:+8");
        AutoComplete($('#UMSNigeriaBody_txtBatchNo'), 4);
    });
};

function Validate() {
    var IsValid = true;
    var BatchDate = document.getElementById('UMSNigeriaBody_txtBatchDate');
    var BatchNo = document.getElementById('UMSNigeriaBody_txtBatchNo');
    var txtBatchTotal = document.getElementById('UMSNigeriaBody_txtBatchTotal');
    var ddlPaymentMode = document.getElementById('UMSNigeriaBody_ddlPaymentMode');
    if (TextBoxValidationlbl(BatchDate, document.getElementById('spanBatchDate'), " Batch Date") == false) IsValid = false;
    if (isDateLbl(BatchDate.value, BatchDate, "spanBatchDate") == false) IsValid = false;
    else if (ValidateDate() == false) IsValid = false;
    if (TextBoxValidationlbl(BatchNo, document.getElementById('spanBatchNo'), " Batch No") == false) IsValid = false;
    if (TextBoxValidationlbl(txtBatchTotal, document.getElementById('spanBatchTotal'), " Batch Total") == false) IsValid = false;
    else if (TextBoxBlurValidationlblWithNoZero(txtBatchTotal, 'spanBatchTotal', " Batch Total") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlPaymentMode, document.getElementById('spanPaymentMode'), "Payment Mode") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
    //    else {
    //        return ValidationBatchNo();
    //    }
}

function ValidateDate() {
    debugger;
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    document.getElementById('spanBatchDate').innerHTML = "";
    var txtBatchDate = document.getElementById('UMSNigeriaBody_txtBatchDate');
    if (TextBoxValidationlbl(txtBatchDate, document.getElementById('spanBatchDate'), "Batch Date") == false)
        return false;
    else
        if (isDateLbl(txtBatchDate.value, txtBatchDate, "spanBatchDate") == false) {
            return false;
        }
        else {
            if (GreaterTodayInPopup(mpeAlert, lblAlertMsg, txtBatchDate, "Batch Date should not be Greater than the Today's Date") == false) {
                return false;
            }
        }
}
function DoBlur(fld) {
    ValidateDate();
}
function isNumberKey(evt, txt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode == 46 && txt.value.split('.').length > 1) {
        return false;
    }
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
}
function onlyNos(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        } else { return true; }
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    catch (err) {
        alert(err.Description);
    }
}

function GetCurrencyFormate(obj) {

    if (obj.value != "") {
        document.getElementById('spanBatchTotal').style.display = "none";
        obj.style.border = "1px solid #8E8E8E";
    }
    obj.value = NewTextWith2Decimals(obj);
}
function GetBatchDetails() {
    var btndummy = document.getElementById('UMSNigeriaBody_btnGetBatchDetails');
    btndummy.click();
    //    setTimeout(btndummy.click(), 1000);
}
function ValidationBatchNo() {
    var IsZipValid = 1;
    if (MinimumLengthValidation(document.getElementById('UMSNigeriaBody_txtBatchNo'), document.getElementById('spanBatchNo'), 6, 'Batch no.') == false) { IsZipValid = false; }
    return IsZipValid;
}