﻿function ConsumptionByPrReading(obj) {
    var pagePath = window.location.pathname;
    var PresentReading = document.getElementById('UMSNigeriaBody_txtPresentReading');
    var PreviousReading = document.getElementById('UMSNigeriaBody_lblPreviousReading');
    var Consumption = document.getElementById('UMSNigeriaBody_lblRConsumption');
    var lbtnBillNo = document.getElementById('UMSNigeriaBody_lbtnBillNo');
    var lblAccountno = document.getElementById('UMSNigeriaBody_lblAccountno');
    var PreviousBalanceUsage = document.getElementById('UMSNigeriaBody_lblConsumptionToAdjust').innerHTML;

    if (parseInt(PresentReading.value) >= parseInt(PreviousReading.innerHTML)) {
        if (parseFloat(PresentReading.value) != 0) {
            var total = parseFloat(PresentReading.value) - parseFloat(PreviousReading.innerHTML);
            total = total - parseInt(PreviousBalanceUsage);
            if (total < 0)
                total = 0;
            Consumption.innerHTML = Math.round(total).toString();
            PageMethods.GetMeterReadingAdjustment(Consumption.innerHTML, lbtnBillNo.innerHTML, lblAccountno.innerHTML, Incase_Success, Incase_Failure);
        }
    }
}
function Incase_Success(results) {
    document.getElementById('UMSNigeriaBody_lblREnergyCharges').innerHTML = IndianCurrencyFormate(results[0]);
    document.getElementById('UMSNigeriaBody_lblTotalAfterAdjustment').innerHTML = IndianCurrencyFormate(results[3]);
}
function Incase_Failure(errors) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    if (error) {
        //alert(error.get_message());
        lblAlertMsg.innerHTML = error.get_message();
        mpeAlert.show();
    }
    else {
        //alert("An unexpeceted error occurred");
        lblAlertMsg.innerHTML = "An unexpeceted error occurred";
        mpeAlert.show();
    }
}
function ConsumptionAdjustmentDetails(obj) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var pagePath = window.location.pathname;
    var PresentReading = document.getElementById('UMSNigeriaBody_txtPresentReading').value.replace(/,/g, "");
    var PreviousReading = document.getElementById('UMSNigeriaBody_lblPreviousReading').innerHTML.replace(/,/g, "");
    var Consumption = document.getElementById('UMSNigeriaBody_lblCurrentConsumption').innerHTML.replace(/,/g, "");
    var TotalAdjustedConsumption = document.getElementById('UMSNigeriaBody_lblTotalAdjustedConsumption').innerHTML.replace(/,/g, "");
    var AdjustedConsumption = document.getElementById('UMSNigeriaBody_txtConsumptionCA').value.replace(/,/g, "");
    var lbtnBillNo = document.getElementById('UMSNigeriaBody_lbtnBillNo');
    var lblAccountno = document.getElementById('UMSNigeriaBody_lblAccountno');
    var lblTaxEffectedConAdj = document.getElementById('UMSNigeriaBody_lblTaxEffectedConAdj');
    var lblTaxPercentCA = document.getElementById('UMSNigeriaBody_lblTaxPercentCA').innerHTML.replace(/,/g, "");
    var lblREnergyChargesCAForCal = document.getElementById('UMSNigeriaBody_lblREnergyChargesCAForCal').innerHTML.replace(/,/g, "");
    var lblAdjustedAmountCAForCal = document.getElementById('UMSNigeriaBody_lblAdjustedAmountCAForCal').innerHTML.replace(/,/g, "");

    var tarrif = (parseFloat(lblREnergyChargesCAForCal) / parseInt(Consumption)).toFixed(2);

    if (AdjustedConsumption == "")
        AdjustedConsumption = 0;

    var ConsumptionAfterAdjustment = 0;
    ConsumptionAfterAdjustment = parseInt(Consumption) - (parseInt(TotalAdjustedConsumption) + parseInt(AdjustedConsumption));
    if (parseFloat(ConsumptionAfterAdjustment) >= 0) {
        document.getElementById('UMSNigeriaBody_txtConsumptionCA').value = AdjustedConsumption;
        //var total = parseFloat(PresentReading.value) - parseFloat(PreviousReading.innerHTML);
        var adjustedAmount = (AdjustedConsumption * tarrif);
        var tax = (adjustedAmount) * parseFloat(lblTaxPercentCA) / 100;
        lblTaxEffectedConAdj.innerHTML = tax.toFixed(2);

        document.getElementById('UMSNigeriaBody_lblConsumptionAfterAdjst').innerHTML = ConsumptionAfterAdjustment;

        document.getElementById('UMSNigeriaBody_lblREnergyChargesCA').innerHTML = IndianCurrencyFormate((parseFloat(ConsumptionAfterAdjustment) * parseFloat(tarrif)).toFixed(2));
        document.getElementById('UMSNigeriaBody_lblAdjustedAmountCA').innerHTML = IndianCurrencyFormate((parseFloat(lblAdjustedAmountCAForCal) + (parseFloat(adjustedAmount) + parseFloat(tax))).toFixed(2));

        var billAmountAfterAdjustment = (document.getElementById('UMSNigeriaBody_lblTotalBillCA').innerHTML.replace(/,/g, "") - document.getElementById('UMSNigeriaBody_lblAdjustedAmountCA').innerHTML.replace(/,/g, ""));
        document.getElementById('UMSNigeriaBody_lblTotalAfterAdjustmentCA').innerHTML = IndianCurrencyFormate(billAmountAfterAdjustment.toFixed(2));

        //                    Consumption.value = Math.round(total).toString();
        //if (parseInt(AdjustedConsumption) > 0) {
        //        PageMethods.GetMeterReadingAdjustment(ConsumptionAfterAdjustment, lbtnBillNo.innerHTML, lblAccountno.innerHTML, Incase_SuccessCA, Incase_FailureCA);
        //        PageMethods.GetMeterReadingAdjustment(AdjustedConsumption, lbtnBillNo.innerHTML, lblAccountno.innerHTML, Incase_SuccessCA1, Incase_FailureCA1);
        //}
    }
    else {
        document.getElementById('UMSNigeriaBody_txtConsumptionCA').value = "0";
        document.getElementById('UMSNigeriaBody_txtConsumptionCA').blur();
        document.getElementById('UMSNigeriaBody_lblConsumptionAfterAdjst').innerHTML = "0";
        document.getElementById('UMSNigeriaBody_lblREnergyChargesCA').innerHTML = document.getElementById('UMSNigeriaBody_lblREnergyChargesCAForCal').innerHTML;
        document.getElementById('UMSNigeriaBody_lblAdjustedAmountCA').innerHTML = document.getElementById('UMSNigeriaBody_lblAdjustedAmountCAForCal').innerHTML;
        document.getElementById('UMSNigeriaBody_lblTotalAfterAdjustmentCA').innerHTML = document.getElementById('UMSNigeriaBody_lblTotalAfterAdjustmentCAForCal').innerHTML;
        lblAlertMsg.innerHTML = "Adjustment cannot be greater than consumption.";
        lblTaxEffectedConAdj.innerHTML = "0.00";
        lblAlertMsg.focus();
        mpeAlert.show();
        //                alert("Adjustment cannot be greater then current consumption!");
    }
}
function Incase_SuccessCA(results) {
    document.getElementById('UMSNigeriaBody_lblREnergyChargesCA').innerHTML = IndianCurrencyFormate(results[0]);
    document.getElementById('UMSNigeriaBody_lblTotalAfterAdjustmentCA').innerHTML = IndianCurrencyFormate(results[3]);
}
function Incase_FailureCA(errors) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    if (error) {
        //alert(error.get_message());
        lblAlertMsg.innerHTML = error.get_message();
        mpeAlert.show();
    }
    else {
        //alert("An unexpeceted error occurred");
        lblAlertMsg.innerHTML = "An unexpeceted error occurred";
        mpeAlert.show();
    }
}


//Amount for adjestedConsumbtion
function Incase_SuccessCA1(results) {
    //    debugger;
    //    alert('' + IndianCurrencyFormate(results[3]));
    //    alert('' + IndianCurrencyFormate(results[2]));
    //    debugger;
    var result = IndianCurrencyFormate((parseFloat(results[3]) - parseFloat(results[2])));
    //document.getElementById('UMSNigeriaBody_lblTaxEffectedConAdj').innerHTML = parseFloat(result).toFixed(2);
    //    debugger;
    //Total amount after adjustment calculation -- Neeraj ID077
    var lblTotalAfterAdjustmentCA = document.getElementById('UMSNigeriaBody_lblTotalAfterAdjustmentCA').innerHTML.replace(/,/g, "");
    var lblTotalBillCA = document.getElementById('UMSNigeriaBody_lblTotalBillCA').innerHTML.replace(/,/g, "");
    var adjutResult = 0.00;
    adjutResult = parseFloat(lblTotalBillCA) - parseFloat(lblTotalAfterAdjustmentCA);
    document.getElementById('UMSNigeriaBody_lblAdjustedAmountCA').innerHTML = IndianCurrencyFormate(adjutResult.toFixed(2));
}
function Incase_FailureCA1(errors) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    if (error) {
        //alert(error.get_message());
        lblAlertMsg.innerHTML = error.get_message();
        mpeAlert.show();
    }
    else {
        //alert("An unexpeceted error occurred");
        lblAlertMsg.innerHTML = "An unexpeceted error occurred";
        mpeAlert.show();
    }
}


function PrReadingByConsumption(obj) {

    var PresentReading = document.getElementById('UMSNigeriaBody_txtPresentReading');
    var PreviousReading = document.getElementById('UMSNigeriaBody_lblPreviousReading');
    var Consumption = document.getElementById('UMSNigeriaBody_lblRConsumption');
    if (parseFloat(PresentReading.value) != 0) {
        var total = parseFloat(PreviousReading.innerHTML) + parseFloat(Consumption.innerHTML);
        PresentReading.value = Math.round(total).toString();
    }

    GetEnergyCharges(obj);
}
function GetEnergyCharges(Consumption) {
    var lbtnBillNo = document.getElementById('UMSNigeriaBody_lbtnBillNo'); lblTariff
    var lblTariff = document.getElementById('UMSNigeriaBody_lblTariff');
    PageMethods.GetEnergyPerKW(Consumption.value, lbtnBillNo.innerHTML, lblTariff.innerHTML, Incase_GraphSuccess, Incase_GraphFailure);
}
function Incase_GraphSuccess(results) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    //alert(results);
    lblAlertMsg.innerHTML = results;
    mpeAlert.show();

}
function Incase_GraphFailure(errors) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    if (error) {
        //alert(error.get_message());
        lblAlertMsg.innerHTML = error.get_message();
        mpeAlert.show();
    }
    else {
        //alert("An unexpeceted error occurred");
        lblAlertMsg.innerHTML = "An unexpeceted error occurred";
        mpeAlert.show();
    }
}

//function GetCurrencyFormate(obj) {
//    obj.value = NewText(obj);
//}
function GetCurrencyFormate(obj) {
    obj.value = NewTextWith2Decimals(obj);
}
function ValidatePayment() {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var IsValid = true;
    var PresentReading = document.getElementById('UMSNigeriaBody_txtPresentReading');
    var PreviousReading = document.getElementById('UMSNigeriaBody_lblPreviousReading');


    if (TextBoxValidationlbl(PresentReading, document.getElementById('spanPresentReading'), " Current Reading") == false) IsValid = false;

    else {
        if (parseInt(PresentReading.value) < parseInt(PreviousReading.innerHTML)) {
            //alert("Present reading cannot be less than previous reading!");
            IsValid = false;
            lblAlertMsg.innerHTML = "Present reading cannot be less than previous reading!";
            mpeAlert.show();
        }
    }
    if (!IsValid) {
        return false;
    }
}
function ValidateBillAdjustment() {
    var txtAdditionalReason = document.getElementById('UMSNigeriaBody_txtAdditionalReason');
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var IsValid;

    var inputs = $(".fetchCharges"); //Finding additional charges repeated text box by css class name.
    var count = 0;

    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].value != '') {
            if (parseFloat(inputs[i].value) != 0) {
                count++;
            }
        }
    }
    if (count == 0) {
        lblAlertMsg.innerHTML = "Additional charges cannot be empty or zero.";
        mpeAlert.show();
        return false;
    }
    else if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtAdditionalReason, "Reason") == false) { return false; }

}


function IndianCurrencyFormate(x) {
    x = x.toString();
    var afterPoint = '';
    if (x.indexOf('.') > 0)
        afterPoint = x.substring(x.indexOf('.'), x.length);
    x = Math.floor(x);
    x = x.toString();
    var lastThree = x.substring(x.length - 3);
    var otherNumbers = x.substring(0, x.length - 3);
    if (otherNumbers != '')
        lastThree = ',' + lastThree;
    return otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;

}
function Validate() {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var IsValid = false;
    var txtAccountNo = document.getElementById('UMSNigeriaBody_txtccountNo').value;

    if (txtAccountNo.length > 0)

        IsValid = true;
    else {
        IsValid = false;
        //alert("Please enter account number or bill number.");
        lblAlertMsg.innerHTML = "Please enter Global Account No / Old Account No";
        //lblAlertMsg.innerHTML = "Please enter account number/bill number/Old Account No/Meter No.";
        mpeAlert.show();
    }
    return IsValid;
}
function BillAdjustment() {
    var IsValid = false;
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var lblTalAfterAdjust = document.getElementById('UMSNigeriaBody_lblTalAfterAdjust');

    var totalEnergyCharges = document.getElementById('UMSNigeriaBody_lblEnergyCharges').innerHTML;
    var totalBillAmountBeforeAdjustment = document.getElementById('UMSNigeriaBody_lblReadingTotalBill').innerHTML;
    var taxBofeAdjustment = document.getElementById('UMSNigeriaBody_lblTaxEffected').innerHTML;
    var totalBillAmountBeforeAdjustment_ForCal = document.getElementById('UMSNigeriaBody_lblDTotalBillAfterAdjustment_ForCal').innerHTML;

    var additionalChargesBA = parseFloat(totalBillAmountBeforeAdjustment.replace(/,/g, "")) - (parseFloat(totalEnergyCharges.replace(/,/g, "")) + parseFloat(taxBofeAdjustment.replace(/,/g, "")))

    var lblDTax = document.getElementById('UMSNigeriaBody_lblDTax').innerHTML;

    var fixedCharges = 0;

    var ActualChargesInputs = $(".ActualCharges"); //Finding Actual additional charges repeated text box by css class name.   
    var AdjustedChargesInputs = $(".AdjustedCharges"); //Finding Adjusted additional charges repeated text box by css class name.

    var inputs = $(".fetchCharges"); //Finding additional charges repeated text box by css class name.
    var litInputs = $(".litFetchCharges");
    var inputsText = $(".fetchAddCharge"); //Finding additional charges name repeated text box by css class name.
    var inputsTextTax = $(".fetchChargesTax"); //Finding additional charges name repeated text box by css class name.
    var lblAdjestedAmounts = $(".lblAdjestedAmounts"); //Finding additional charges name repeated text box by css class name.
    var AmountForCal = 0;

    for (i = 0; i < inputs.length; i++) {
        if (inputs[i].value != '') {
            var InputValue = parseFloat(inputs[i].value.replace(/,/g, ""));
            var ActualValue = parseFloat(ActualChargesInputs[i].innerHTML.replace(/,/g, ""));
            var AdjustedValue = parseFloat(AdjustedChargesInputs[i].innerHTML.replace(/,/g, ""));
            //fixedCharges += InputValue;
            var Calvalue = ActualValue - (InputValue + AdjustedValue);

            if (Calvalue < 0) {
                lblAlertMsg.innerHTML = "Adjustment Amount should not be greater than Actual Additional Charges.";
                mpeAlert.show();
                inputs[i].value = "0";
                AmountForCal = 0;
                fixedCharges += (ActualValue - AdjustedValue);
                IsValid = false;
                break;
            }
            else {
                AmountForCal += InputValue;
                fixedCharges += Calvalue;
                IsValid = true;
            }
        }
    }

    if (IsValid) {
        //var adjustedAmount = parseFloat(additionalChargesBA) - parseFloat(fixedCharges);
        var adjustedAmount = parseFloat(fixedCharges);
        //var taxAfterAdjustment = (parseFloat(totalEnergyCharges.replace(/,/g, "")) + parseFloat(fixedCharges)) * parseFloat(lblDTax) / 100;
        var taxAfterAdjustment = (parseFloat(fixedCharges) * parseFloat(lblDTax)) / 100;
        //var taxEffected = taxBofeAdjustment - taxAfterAdjustment;
        //var totalAdjustedAmountWithTax = parseFloat(adjustedAmount) + parseFloat(taxBofeAdjustment) - parseFloat(taxAfterAdjustment);
        var totalAdjustedAmountWithTax = parseFloat(adjustedAmount) + parseFloat(taxAfterAdjustment);
        var totalBillAfterAdjustment = parseFloat(totalEnergyCharges.replace(/,/g, "")) + parseFloat(fixedCharges);
        var energyChargesTax = (parseFloat(totalEnergyCharges.replace(/,/g, "")) * parseFloat(lblDTax)) / 100;

        //        var totalBillWithTaxAA = parseFloat(totalBillAfterAdjustment) + parseFloat(taxAfterAdjustment) + parseFloat(energyChargesTax)
        //        totalBillWithTaxAA = totalBillWithTaxAA.toFixed(2);

        var TaxAmountForCal = (parseFloat(AmountForCal) * parseFloat(lblDTax)) / 100;

        var totalBillWithTaxAA = parseFloat(totalBillAmountBeforeAdjustment_ForCal) - (parseFloat(AmountForCal) + parseFloat(TaxAmountForCal));
        totalBillWithTaxAA = totalBillWithTaxAA.toFixed(2);

        document.getElementById('UMSNigeriaBody_lblTaxEffectedTotal').innerHTML = IndianCurrencyFormate(taxAfterAdjustment.toFixed(2));
        document.getElementById('UMSNigeriaBody_lblAdjestedAmountTotal').innerHTML = IndianCurrencyFormate(adjustedAmount.toFixed(2));
        document.getElementById('UMSNigeriaBody_lblTotalAdjestment').innerHTML = IndianCurrencyFormate(totalAdjustedAmountWithTax.toFixed(2));
        document.getElementById('UMSNigeriaBody_lblDTotalBillAfterAdjustment').innerHTML = IndianCurrencyFormate(totalBillWithTaxAA);
    }
    else {
        document.getElementById('UMSNigeriaBody_lblTaxEffectedTotal').innerHTML = "0.00";
        document.getElementById('UMSNigeriaBody_lblAdjestedAmountTotal').innerHTML = "0.00";
        document.getElementById('UMSNigeriaBody_lblTotalAdjestment').innerHTML = "0.00";
        document.getElementById('UMSNigeriaBody_lblDTotalBillAfterAdjustment').innerHTML = IndianCurrencyFormate(parseFloat(totalBillAmountBeforeAdjustment_ForCal).toFixed(2));
    }
}



function NewText1(obj) {
    var str = "";
    var count = 0;
    str = obj.value.replace(/,/g, "");
    if (str.indexOf('-') != -1) {
        str = str.replace('-', '');
        count = 1;
    }
    if (str != '-') {
        if (count == 1)
            obj.value = "-" + IndianCurrencyFormate(str);
        else
            obj.value = IndianCurrencyFormate(str);
    }
    else { obj.value = "-"; }
}
//function GetCurrencyFormate(obj) {
//    obj.value = NewText(obj);
//}
function isNumberKey(evt, txt) {

    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    if ((charCode == 46 && txt.value.split('.').length > 1)) {
        return false;
    }
}
function ValidateCustOutstanding() {
    var txtAmountToAdjuste = document.getElementById('UMSNigeriaBody_txtAmountToAdjuste').value;
    if (txtAmountToAdjuste.length == 0) {

        alert("Please enter amount to adjust.");
        return false;
    }

}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};

function GetAccountDetails(e) {
    var key = e.keyCode || e.which;
    if (key == 13) {
        var btnGo = document.getElementById('UMSNigeriaBody_btnGo');
        btnGo.click();
    }
}
function ValidateConsumptionAdjustment() {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var IsValid = true;
    var txtConsumptionCA = document.getElementById('UMSNigeriaBody_txtConsumptionCA');
    var lblCurrentConsumption = document.getElementById('UMSNigeriaBody_lblCurrentConsumption');
    var lblTotalAdjustedConsumption = document.getElementById('UMSNigeriaBody_lblTotalAdjustedConsumption');
    var lblConsumptionAfterAdjst = document.getElementById('UMSNigeriaBody_lblConsumptionAfterAdjst');
    var txtMeterReson = document.getElementById('UMSNigeriaBody_txtMeterReson');

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtConsumptionCA, "Consumption To Adjust.") == false) {
        return false;
    }
    else if (parseInt(txtConsumptionCA.value) == 0) {
        lblAlertMsg.innerHTML = "Enter valid Consumption";
        mpeAlert.show();
        return false;
    }
    else if (parseInt(lblCurrentConsumption.innerHTML) < parseInt(lblConsumptionAfterAdjst.value)) {
        lblAlertMsg.innerHTML = "Consumption cannot be current consumption!";
        mpeAlert.show();
        return false;
    }
    else if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtMeterReson, "Reason") == false) {
        return false;
    }
}

//Neeraj ID-077 19-May-15
function ValidateAdjustmentInput() {
    //    debugger;
    var IsValid = true;
    var mpeAlert = $find('UMSNigeriaBody_mpeAlert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblAlertMsg');
    var txtCredit = document.getElementById('UMSNigeriaBody_txtCredit').value;
    var txtDebitAmount = document.getElementById('UMSNigeriaBody_txtDebit').value;
    var txtBillAdjustReason = document.getElementById('UMSNigeriaBody_txtBillAdjustReason');

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtBillAdjustReason, "Reason") == false) { return false; }

    if (parseFloat(txtCredit) == 0)
        txtCredit = "";
    if (parseFloat(txtDebitAmount) == 0)
        txtDebitAmount = "";
    if (txtCredit.length == 0 && txtDebitAmount.length == 0) {
        IsValid = false;
        lblAlertMsg.innerHTML = 'Please enter amount to adjust.';
        mpeAlert.show();
    }
    else if (txtCredit.length > 0 && txtDebitAmount.length > 0) {
        IsValid = false;
        lblAlertMsg.innerHTML = 'Please enter Credit or Debit Amount.';
        mpeAlert.show();
    }
    if (IsValid)
        return true
    else
        return false;
}
function ValidateAdjustmentInputNew() {
    var IsValid = true;
    var mpeAlert = $find('UMSNigeriaBody_mpeAlert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblAlertMsg');
    var txtCrdDbtAmount = document.getElementById('UMSNigeriaBody_txtCrdDbtAmount').value;
    var txtBillAdjustReason = document.getElementById('UMSNigeriaBody_txtBillAdjustReason');

    if (txtCrdDbtAmount.length > 0) {
        if (parseFloat(txtCrdDbtAmount) == 0) {
            lblAlertMsg.innerHTML = 'Amount cannot be zero.';
            mpeAlert.show();
            return false;
        }
        else if (!ifCreditExceed()) {
            return false;
        }
        else if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtBillAdjustReason, "Reason") == false) { return false; }
    }
    else if (txtCrdDbtAmount == "") {
        lblAlertMsg.innerHTML = 'Please enter Amount.';
        mpeAlert.show();
        return false;
    }
    else if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtBillAdjustReason, "Reason") == false) { return false; }
}
function ifCreditExceed() {
    var IsValid = true;
    var mpeAlert = $find('UMSNigeriaBody_mpeCrdConfirmation');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_Label3')
    var radioButtons = document.getElementById("UMSNigeriaBody_rdoAdjustmentTypeBA_0");
    var lblTotalAmountPending = document.getElementById('UMSNigeriaBody_lblTotalAmountPending').innerHTML.replace(/,/g, "");
    var txtCrdDbtAmount = document.getElementById('UMSNigeriaBody_txtCrdDbtAmount').value.replace(/,/g, "");

    if (radioButtons.checked) {
        if (parseFloat(txtCrdDbtAmount) > parseFloat(lblTotalAmountPending)) {
            IsValid = false;
            lblAlertMsg.innerHTML = 'Adjustment amount is exceeding Outstanding amount. Do you want to continue adjustment?';
            mpeAlert.show();
        }
    }
    if (IsValid)
        return true
    else
        return false;
}
