﻿function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};

function CheckAll(cbAllId, cblId) {
    var cbAll = document.getElementById(cbAllId);
    var chkBoxList = document.getElementById(cblId);
    CheckBoxListSelect(cbAll, chkBoxList);

}
function UnCheckAll(cbAllId, cblId) {
    var chkBoxList = document.getElementById(cblId);
    var cbAll = document.getElementById(cbAllId);
    Uncheck(cbAll, chkBoxList);
    document.getElementById("UMSNigeriaBody_divInValidPaymentBatches").style.display = 'none';
}

function PageaValidation() {
    var IsValid = true;
    var rblReadings = document.getElementById('UMSNigeriaBody_rblReadings');
    var rblBatches = document.getElementById('UMSNigeriaBody_rblBatches');
    var rblAdjustments = document.getElementById('UMSNigeriaBody_rblAdjustments');
    //    var ddlMonth = document.getElementById('UMSNigeriaBody_ddlMonth');
    //    var ddlYear = document.getElementById('UMSNigeriaBody_ddlYear');
    var ddlServiceUnits = document.getElementById('UMSNigeriaBody_ddlServiceUnits');
    var ddlServiceCenters = document.getElementById('UMSNigeriaBody_ddlServiceCenters');

    var cblServiceUnits = document.getElementById('UMSNigeriaBody_cblServiceUnits');
    var cblServiceCenters = document.getElementById('UMSNigeriaBody_cblServiceCenters');
    var cblCycles = document.getElementById('UMSNigeriaBody_cblCycles');
    //var rblEstimation = document.getElementById('UMSNigeriaBody_rblEstimation');
    //var rblBillSendingMode = document.getElementById('UMSNigeriaBody_rblBillSendingMode');

    //    if (DropDownlistValidationlbl(ddlYear, document.getElementById('spanYear'), "Year") == false) {
    //        document.getElementById('spanYear').innerHTML = "*Please select year.";
    //        IsValid = false;
    //    }
    //    if (DropDownlistValidationlbl(ddlMonth, document.getElementById('spanMonth'), "Month") == false) {
    //        document.getElementById('spanMonth').innerHTML = "*Please select Month.";
    //        IsValid = false;
    //    }
    //if (DropDownlistValidationlbl(ddlServiceUnits, document.getElementById('spanServiceUnit'), "Service Unit") == false) IsValid = false;
    //if (DropDownlistValidationlbl(ddlServiceCenters, document.getElementById('spanServiceCenter'), "Service Centers") == false) IsValid = false;

    var spanCyclesFeeders = document.getElementById('spanCyclesFeeders');
    if (spanCyclesFeeders != null)
        document.getElementById('spanCyclesFeeders').innerHTML = " ";
    var atLeast = 1;
    var counter = 0;

    if (cblServiceUnits != null) {
        var IsCheckedOneSU = false;
        var cSU = cblServiceUnits.getElementsByTagName("input");
        for (var i = 0; i < cSU.length; i++) {
            if (cSU[i].checked) {
                IsCheckedOneSU = true;
                break;
            }
        }

        if (!IsCheckedOneSU) {
            document.getElementById('spanServiceUnit').innerHTML = "*Please select atleast " + atLeast + " Service Unit.";
            IsValid = false;
        }
    }

    if (cblServiceCenters != null) {
        var IsCheckedOneSC = false;
        var cSC = cblServiceCenters.getElementsByTagName("input");
        for (var i = 0; i < cSC.length; i++) {
            if (cSC[i].checked) {
                IsCheckedOneSC = true;
                break;
            }
        }

        if (!IsCheckedOneSC) {
            document.getElementById('spanServiceCenter').innerHTML = "*Please select atleast " + atLeast + " Service Center.";
            IsValid = false;
        }
    }

    if (cblCycles != null) {
        var checkbox = cblCycles.getElementsByTagName("input");

        for (var i = 0; i < checkbox.length; i++) {
            if (checkbox[i].checked) {
                counter++;
            }
        }
    }

    if (atLeast > counter) {
        document.getElementById('spanCyclesFeeders').innerHTML = "*Please select atleast " + atLeast + " Cycle(s)";
        IsValid = false;
    }

    //    if (RadioButtonListlbl(rblReadings, 'spanReadings', " Readings") == false) {
    //        document.getElementById("spanReadings").innerHTML="*Please select readings are completed or not."
    //        IsValid = false;
    //    } else {
    //        if (RadioButtonListlblValue(rblReadings, 'spanReadings', "*Please Complete All The Readings") == false) IsValid = false;
    //    }

    //    if (RadioButtonListlbl(rblBatches, 'spanBatches', " Batches") == false) {
    //        document.getElementById("spanBatches").innerHTML="*Please select payment batches are closed or not."
    //        IsValid = false;
    //    }
    //    else {
    //        if (RadioButtonListlblValue(rblBatches, 'spanBatches', "*Please Close All The Batches") == false) IsValid = false;

    //    }
    //    if (RadioButtonListlbl(rblAdjustments, 'spanAdjustments', " Adjustments") == false) {
    //        document.getElementById("spanAdjustments").innerHTML ="*Please select Adjustments are closed or not."
    //        IsValid = false;
    //    }
    //    else {
    //        if (RadioButtonListlblValue(rblAdjustments, 'spanAdjustments', "*Please Close All The Adjustments") == false) IsValid = false;

    //    }
    //    if (RadioButtonListlbl(rblEstimation, 'spanEstimation', " Estimation") == false) {
    //        document.getElementById("spanEstimation").innerHTML = "*Please select estimations are closed or not.";
    //        IsValid = false;
    //    }
    //    else {
    //        if (RadioButtonListlblValue(rblEstimation, 'spanEstimation', "*Please Close All The Estimations") == false) IsValid = false;

    //    }

    // if (DropDownlistValidationlbl(ddlCycleList, document.getElementById('spanCyclesFeeders'), " Cycle / Feeder ") == false) IsValid = false;

    //    if (RadioButtonListlbl(rblBillSendingMode, 'spanBillSendingMode', "Bill Sending Mode") == false) {
    //        IsValid = false;
    //    }
    if (!IsValid) {
        return false;
    }

}


function Selectall(obj) {
    var btnDummyforCycle = document.getElementById('UMSNigeriaBody_btnDummyforCycle');
    var cblCycles = document.getElementById('UMSNigeriaBody_cblCycles');

    var checkbox = cblCycles.getElementsByTagName("input");

    for (var i = 0; i < checkbox.length; i++) {
        checkbox[i].checked = obj.checked;
    }
    btnDummyforCycle.click();
}

function CheckSelectAll() {
    var cblCycles = document.getElementById('UMSNigeriaBody_cblCycles');
    var cbAll = document.getElementById('UMSNigeriaBody_cbAll');
    var btnDummyforCycle = document.getElementById('UMSNigeriaBody_btnDummyforCycle');

    var checkbox = cblCycles.getElementsByTagName("input");
    cbAll.checked = true;
    for (var i = 0; i < checkbox.length; i++) {
        if (checkbox[i].checked == false) {
            cbAll.checked = false;
            break;
        }
    }
    btnDummyforCycle.click();
}


function CheckReadValidation() {
    var IsValid = true;
    var RB1 = document.getElementById("UMSNigeriaBody_rblReadings");
    var cblCycles = document.getElementById('UMSNigeriaBody_cblCycles');

    document.getElementById('spanCyclesFeeders').innerHTML = " ";
    var atLeast = 1;
    var counter = 0;
    if (cblCycles != null) {
        var checkbox = cblCycles.getElementsByTagName("input");

        for (var i = 0; i < checkbox.length; i++) {
            if (checkbox[i].checked) {
                counter++;
            }
        }
    }

    if (atLeast > counter) {
        document.getElementById('spanCyclesFeeders').innerHTML = "*Please select atleast " + atLeast + " Cycle(s)";
        IsValid = false;
    }

    if (RadioButtonListlbl(RB1, 'spanReadings', " Readings") == false) {
        document.getElementById("spanReadings").innerHTML = "*Please select readings are completed or not."
        IsValid = false;
    } else {
        if (RadioButtonListlblValue(RB1, 'spanReadings', "*Please Complete All The Readings") == false) IsValid = false;
    }
    if (IsValid) {
        //var ddlMonth = document.getElementById('UMSNigeriaBody_ddlMonth');
        //var ddlYear = document.getElementById('UMSNigeriaBody_ddlYear');
        var MonthId = document.getElementById('UMSNigeriaBody_hfMonthID');
        var YearId = document.getElementById('UMSNigeriaBody_hfYearID');
        var cblCycles = document.getElementById('UMSNigeriaBody_cblCycles');
        var selectedCycles = "";
        var checkbox = cblCycles.getElementsByTagName("input");
        for (var i = 0; i < checkbox.length; i++) {
            if (checkbox[i].checked) {
                if (Trim(selectedCycles) == "")
                    selectedCycles = checkbox[i].value;
                else
                    selectedCycles = selectedCycles + "," + checkbox[i].value;
            }
        }

        var radio = RB1.getElementsByTagName("input");
        var label = RB1.getElementsByTagName("label");
        for (var i = 0; i < radio.length; i++) {
            if (radio[i].checked) {
                if (radio[i].value == 1) {
                    radio[i].checked = false;
                    //PageMethods.CheckIsReadingDone(ddlMonth[ddlMonth.selectedIndex].value, ddlYear[ddlYear.selectedIndex].value, selectedCycles, Incase_ReadSuccess, Incase_ReadFailure);
                    PageMethods.CheckIsReadingDone(MonthId.value, YearId.value, selectedCycles, Incase_ReadSuccess, Incase_ReadFailure);
                }
            }
        }
    }
    else {
        var RB1 = document.getElementById("UMSNigeriaBody_rblReadings");
        var radio = RB1.getElementsByTagName("input");
        radio[0].checked = false;
    }
}

function Incase_ReadSuccess(results) {
    var lblReadingsCycles = document.getElementById("UMSNigeriaBody_lblReadingsCycles");
    var divInValidReadings = document.getElementById("UMSNigeriaBody_divInValidReadings");
    var divAfterRead = document.getElementById("UMSNigeriaBody_divAfterRead");
    var divInValidReadings = document.getElementById("UMSNigeriaBody_divInValidReadings");
    var btnNext = document.getElementById("UMSNigeriaBody_btnNext");

    lblReadingsCycles.innerHTML = results;
    var RB1 = document.getElementById("UMSNigeriaBody_rblReadings");
    var radio = RB1.getElementsByTagName("input");

    if (Trim(results) == "") {
        radio[0].checked = true;
        divAfterRead.style.display = "block"
        divInValidReadings.style.display = "none"
        btnNext.style.display = "block"
    }
    else {
        radio[0].checked = false;
        divAfterRead.style.display = "none"
        btnNext.style.display = "none"
        divInValidReadings.style.display = "block"
    }
}
function Incase_ReadFailure(errors) {
}


function CheckPaymentBatchClose() {

    var RB1 = document.getElementById("UMSNigeriaBody_rblBatches");

    if (RadioButtonListlbl(RB1, 'spanBatches', " Batches") == false) {
        document.getElementById("spanBatches").innerHTML = "*Please select payment batches are closed or not.";
        IsValid = false;
    }
    else {
        if (RadioButtonListlblValue(RB1, 'spanBatches', "*Please close all the payments batches") == false) IsValid = false;
    }

//    var ddlMonth = document.getElementById('UMSNigeriaBody_ddlMonth');
    //    var ddlYear = document.getElementById('UMSNigeriaBody_ddlYear');
    var MonthId = document.getElementById('UMSNigeriaBody_hfMonthID');
    var YearId = document.getElementById('UMSNigeriaBody_hfYearID');
    var radio = RB1.getElementsByTagName("input");
    var label = RB1.getElementsByTagName("label");
    for (var i = 0; i < radio.length; i++) {
        if (radio[i].checked) {
            if (radio[i].value == 1) {
                radio[i].checked = false;
//                PageMethods.CheckPaymentBatchClose(ddlMonth[ddlMonth.selectedIndex].value, ddlYear[ddlYear.selectedIndex].value, Incase_PaymentSuccess, Incase_PaymentFailure);
                PageMethods.CheckPaymentBatchClose(MonthId.value, YearId.value, Incase_PaymentSuccess, Incase_PaymentFailure);
            }
        }
    }
}

function Incase_PaymentSuccess(results) {
    var divAfterPaymentBatch = document.getElementById("UMSNigeriaBody_divAfterPaymentBatch");
    var spanBatches = document.getElementById('spanBatches');
    var btnNext = document.getElementById("UMSNigeriaBody_btnNext");

    var RB1 = document.getElementById("UMSNigeriaBody_rblBatches");
    var radio = RB1.getElementsByTagName("input");

    var Values = results.split("|");

    if (Values[0] == "false") {
        radio[0].checked = true;
        divAfterPaymentBatch.style.display = "block"
        btnNext.style.display = "block"
        spanBatches.style.display = "none";
    }
    else {
        radio[0].checked = false;
        spanBatches.innerHTML = "*Please close all the payments batches having BatchNo's-" + Values[1];
        spanBatches.style.display = "block";
        divAfterPaymentBatch.style.display = "none"
        btnNext.style.display = "none"
    }
}
function Incase_PaymentFailure(errors) {
}



function CheckAdjustmentBatchClose() {

//    var ddlMonth = document.getElementById('UMSNigeriaBody_ddlMonth');
    //    var ddlYear = document.getElementById('UMSNigeriaBody_ddlYear');
    var MonthId = document.getElementById('UMSNigeriaBody_hfMonthID');
    var YearId = document.getElementById('UMSNigeriaBody_hfYearID');
    var RB1 = document.getElementById("UMSNigeriaBody_rblAdjustments");

    if (RadioButtonListlbl(RB1, 'spanAdjustments', " Adjustments") == false) {
        document.getElementById("spanAdjustments").innerHTML = "*Please select Adjustments are closed or not."
        IsValid = false;
    }
    else {
        if (RadioButtonListlblValue(RB1, 'spanAdjustments', "*Please Close All The Adjustment batches") == false) IsValid = false;
    }

    var radio = RB1.getElementsByTagName("input");
    var label = RB1.getElementsByTagName("label");
    for (var i = 0; i < radio.length; i++) {
        if (radio[i].checked) {
            if (radio[i].value == 1) {
                radio[i].checked = false;
//                PageMethods.CheckAdjustmentBatchClose(ddlMonth[ddlMonth.selectedIndex].value, ddlYear[ddlYear.selectedIndex].value, Incase_AdjustmentSuccess, Incase_AdjustmentFailure);
                PageMethods.CheckAdjustmentBatchClose(MonthId.value, YearId.value, Incase_AdjustmentSuccess, Incase_AdjustmentFailure);
            }
        }
    }
}

function Incase_AdjustmentSuccess(results) {
    var divAfterAdjustment = document.getElementById("UMSNigeriaBody_divAfterAdjustment");
    var spanAdjustments = document.getElementById('spanAdjustments');
    var btnNext = document.getElementById("UMSNigeriaBody_btnNext");

    var RB1 = document.getElementById("UMSNigeriaBody_rblAdjustments");
    var radio = RB1.getElementsByTagName("input");

    var Values = results.split("|");

    if (Values[0] == "false") {
        radio[0].checked = true;
        divAfterAdjustment.style.display = "block"
        spanAdjustments.style.display = "none";
        btnNext.style.display = "block"
    }
    else {
        radio[0].checked = false;
        spanAdjustments.innerHTML = "*Please close all the Adjustment batches having BatchNo's-" + Values[1];
        spanAdjustments.style.display = "block";
        btnNext.style.display = "none"
        divAfterAdjustment.style.display = "none"
    }
}
function Incase_AdjustmentFailure(errors) {
}


function CheckEstimationValidation() {
//    var ddlMonth = document.getElementById('UMSNigeriaBody_ddlMonth');
    //    var ddlYear = document.getElementById('UMSNigeriaBody_ddlYear');
    var MonthId = document.getElementById('UMSNigeriaBody_hfMonthID');
    var YearId = document.getElementById('UMSNigeriaBody_hfYearID');
    var cblCycles = document.getElementById('UMSNigeriaBody_cblCycles');
    var RB1 = document.getElementById("UMSNigeriaBody_rblEstimation");

    if (RadioButtonListlbl(RB1, 'spanEstimation', " Estimation") == false) {
        document.getElementById("spanEstimation").innerHTML = "*Please select estimations are closed or not."
        IsValid = false;
    }
    else {
        if (RadioButtonListlblValue(RB1, 'spanEstimation', "*Please Close All The Estimations") == false) IsValid = false;
    }
    var selectedCycles = "";
    var checkbox = cblCycles.getElementsByTagName("input");
    for (var i = 0; i < checkbox.length; i++) {
        if (checkbox[i].checked) {
            if (Trim(selectedCycles) == "")
                selectedCycles = checkbox[i].value;
            else
                selectedCycles = selectedCycles + "," + checkbox[i].value;
        }
    }

    var radio = RB1.getElementsByTagName("input");
    var label = RB1.getElementsByTagName("label");
    for (var i = 0; i < radio.length; i++) {
        if (radio[i].checked) {
            if (radio[i].value == 1) {
                radio[i].checked = false;
//                PageMethods.CheckIsEstimationDone(ddlMonth[ddlMonth.selectedIndex].value, ddlYear[ddlYear.selectedIndex].value, selectedCycles, Incase_EstimationSuccess, Incase_EstimationFailure);
                PageMethods.CheckIsEstimationDone(MonthId.value, YearId.value, selectedCycles, Incase_EstimationSuccess, Incase_EstimationFailure);
            }
        }
    }
}

function Incase_EstimationSuccess(results) {

    var lblInValidEstimation = document.getElementById("UMSNigeriaBody_lblInValidEstimation");
    var divEstimationValid = document.getElementById("UMSNigeriaBody_divEstimationValid");
    var btnNext = document.getElementById("UMSNigeriaBody_btnNext");

    lblInValidEstimation.innerHTML = results;
    var RB1 = document.getElementById("UMSNigeriaBody_rblEstimation");
    var radio = RB1.getElementsByTagName("input");

    if (Trim(results) == "") {
        radio[0].checked = true;
        divEstimationValid.style.display = "none"
        btnNext.style.display = "block"
    }
    else {
        radio[0].checked = false;
        divEstimationValid.style.display = "block"
        btnNext.style.display = "none"
    }
}
function Incase_EstimationFailure(errors) {
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');

};       