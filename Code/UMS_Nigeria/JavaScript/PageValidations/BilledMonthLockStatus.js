﻿function Validate() {
    var ddlYear = document.getElementById('UMSNigeriaBody_ddlYear');
    var ddlMonth = document.getElementById('UMSNigeriaBody_ddlMonth');

    var IsValid = true;

    if (DropDownlistValidationlbl(ddlYear, document.getElementById("spanYear"), "Year") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlMonth, document.getElementById("spanMonth"), "Month") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
}
function ReasonValidate() {
    var txtReason = document.getElementById('UMSNigeriaBody_txtReason');
    var IsValid = true;
    if (TextBoxValidationlbl(txtReason, document.getElementById("spanReason"), "Reason") == false) IsValid = false;
    if (!IsValid) {
        return false;
    }
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};