﻿function pageLoad(sender, args) {
    DisplayMessage('UMSNigeriaBody_pnlMessage');


    $(document).ready(function () {
        DisplayMessage('UMSNigeriaBody_pnlMessage');
        Calendar('UMSNigeriaBody_txtDisableDate', 'UMSNigeriaBody_imgDate', "-2:+8");

    });
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(function () {
        Calendar('UMSNigeriaBody_txtDisableDate', 'UMSNigeriaBody_imgDate', "-2:+8");

    });
};
function Validate() {
    var mpeAlert = $find('UMSNigeriaBody_mpeAlert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblAlertMsg');

//    var ddlYear = document.getElementById('UMSNigeriaBody_ddlYear');
//    var ddlMonth = document.getElementById('UMSNigeriaBody_ddlMonth');
    var ddlBU = document.getElementById('UMSNigeriaBody_ddlBU');
    var ddlSU = document.getElementById('UMSNigeriaBody_ddlSU');
    var ddlSC = document.getElementById('UMSNigeriaBody_ddlSC');
    var ddlCycle = document.getElementById('UMSNigeriaBody_ddlCycle');
    var ddlBookNo = document.getElementById('UMSNigeriaBody_ddlBookNo');
    var txtDetails = document.getElementById('UMSNigeriaBody_txtDetails');
    var ddlDisableType = document.getElementById('UMSNigeriaBody_ddlDisableType');
    var txtDisableDate = document.getElementById('UMSNigeriaBody_txtDisableDate');
    var IsValid = true;

//    if (DropDownlistValidationlbl(ddlYear, document.getElementById("spanYear"), "Year") == false) IsValid = false;
//    if (DropDownlistValidationlbl(ddlMonth, document.getElementById("spanMonth"), "Month") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlDisableType, document.getElementById("spanddlDisableType"), "Disable Type") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlBU, document.getElementById("spanBU"), "Business Unit") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlSU, document.getElementById("spanSU"), "Service Unit") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlSC, document.getElementById("spanSC"), "Service Center") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlCycle, document.getElementById("spanCycle"), "Book Group") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlBookNo, document.getElementById("spanBook"), "Book Number") == false) IsValid = false;
    if (TextBoxValidationlbl(txtDetails, document.getElementById("spanDetails"), "Details") == false) IsValid = false;

    if (TextBoxValidationlbl(txtDisableDate, document.getElementById("spanDisableDate"), "Disable Date") == false)
        IsValid = false;
    else if (isDateLbl(txtDisableDate.value, txtDisableDate, "spanDisableDate") == false) IsValid = false;
    else if (GreaterTodayInPopup(mpeAlert, lblAlertMsg, txtDisableDate, "Disable Date should not be Greater than the Today's Date") == false)
        IsValid = false;

    if (!IsValid) {
        return false;
    }
}