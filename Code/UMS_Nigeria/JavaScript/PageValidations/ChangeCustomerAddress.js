﻿function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};

function Validate() {
    var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo');
    var IsValid = true;

    if (TextBoxValidationlbl(txtAccountNo, document.getElementById("spanAccNo"), " Global Account No / Old Account No") == false) IsValid = false;

    if (txtAccountNo.value.length = 0)
        document.getElementById('UMSNigeriaBody_divdetails').style.display = "none";

    if (!IsValid) {
        return false;
    }
}
function CopyAddress(IsCopy) {
    var txtLandMark = document.getElementById('UMSNigeriaBody_txtLandMark');
    var txtHouseNo = document.getElementById('UMSNigeriaBody_txtHouseNo');
    var txtCity = document.getElementById('UMSNigeriaBody_txtCity');
    var txtStreet = document.getElementById('UMSNigeriaBody_txtStreet');
    var txtZipCode = document.getElementById('UMSNigeriaBody_txtZipCode');


    var txtServiceLandMark = document.getElementById('UMSNigeriaBody_txtServiceLandMark');
    var txtServiceHouseNo = document.getElementById('UMSNigeriaBody_txtServiceHouseNo');
    var txtServiceCity = document.getElementById('UMSNigeriaBody_txtServiceCity');
    var txtServiceStreet = document.getElementById('UMSNigeriaBody_txtServiceStreet');
    var txtServiceZipCode = document.getElementById('UMSNigeriaBody_txtServiceZipCode');


    if (IsCopy) {
        txtServiceLandMark.value = txtLandMark.value;
        txtServiceHouseNo.value = txtHouseNo.value;
        txtServiceCity.value = txtCity.value;
        txtServiceStreet.value = txtStreet.value;
        txtServiceZipCode.value = txtZipCode.value;
    }
    else {
        txtServiceLandMark.value = txtServiceHouseNo.value = txtServiceCity.value = txtServiceStreet.value = txtServiceZipCode.value = "";
    }
}
function UpdateValidate() {
    var txtPHNo = document.getElementById('UMSNigeriaBody_txtPHNo');
    var txtPStreet = document.getElementById('UMSNigeriaBody_txtPStreet');
    var txtPLGAVillage = document.getElementById('UMSNigeriaBody_txtPLGAVillage');
    var txtPZip = document.getElementById('UMSNigeriaBody_txtPZip');
    var txtPArea = document.getElementById('UMSNigeriaBody_txtPArea');
    var txtPReason = document.getElementById('UMSNigeriaBody_txtPReason');

    var txtSHNo = document.getElementById('UMSNigeriaBody_txtSHNo');
    var txtSStreet = document.getElementById('UMSNigeriaBody_txtSStreet');
    var txtSLGAVillage = document.getElementById('UMSNigeriaBody_txtSLGAVillage');
    var txtSZip = document.getElementById('UMSNigeriaBody_txtSZip');
    var txtSArea = document.getElementById('UMSNigeriaBody_txtSArea');
    var rblCommunicationAddress = document.getElementById('UMSNigeriaBody_rblCommunicationAddress');

    var txtHouseNoServ = document.getElementById('UMSNigeriaBody_txtHouseNoServ');
    var txtStreetNameServ = document.getElementById('UMSNigeriaBody_txtStreetNameServ');
    var txtCityServ = document.getElementById('UMSNigeriaBody_txtCityServ');
    var txtZipServ = document.getElementById('UMSNigeriaBody_txtZipServ');
//    var txtAreaServ = document.getElementById('UMSNigeriaBody_txtAreaServ');
    var rdoCommunicationServ = document.getElementById('UMSNigeriaBody_rdoCommunicationServ');
    var hfIsServiceAddress = document.getElementById('UMSNigeriaBody_hfIsServiceAddress');
    var IsValid = true;
//    if (TextBoxValidationlbl(txtPHNo, document.getElementById("spanPHNo"), "House No") == false) IsValid = false;
    if (TextBoxBlurValidationlblZeroAccepts(txtPHNo, 'spanPHNo', "House No") == false) IsValid = false;
    if (TextBoxValidationlbl(txtPStreet, document.getElementById("spanPStreet"), "Street Name") == false) IsValid = false;
    if (TextBoxValidationlbl(txtPLGAVillage, document.getElementById("spanPLGAVillage"), "City") == false) IsValid = false;
    //if (TextBoxValidationlbl(txtPZip, document.getElementById("spanPZip"), "Postal Zip") == false) IsValid = false;
    //if (TextBoxValidationlbl(txtPArea, document.getElementById("spanPArea"), "Posat Area") == false) IsValid = false;
    if (TextBoxValidationlbl(txtPReason, document.getElementById("spanPReason"), "Reason") == false) IsValid = false;

    if (document.getElementById("UMSNigeriaBody_cbIsSameAsPostal").checked) {
        if (hfIsServiceAddress.value == "false") {
//            if (TextBoxValidationlbl(txtSHNo, document.getElementById("spanSHNo"), "House No") == false) IsValid = false;
            if (TextBoxBlurValidationlblZeroAccepts(txtSHNo,'spanSHNo', "House No") == false) IsValid = false;
            if (TextBoxValidationlbl(txtSStreet, document.getElementById("spanSStreet"), "Street Name") == false) IsValid = false;
            if (TextBoxValidationlbl(txtSLGAVillage, document.getElementById("spanSLGAVillage"), "City") == false) IsValid = false;
            //if (TextBoxValidationlbl(txtSZip, document.getElementById("spanSZip"), "Service Zip") == false) IsValid = false;
            //        if (RadioButtonListlbl(rblCommunicationAddress, document.getElementById("spanrblCommunicationAddress"), "Service Area") == false) IsValid = false;
        }
        else {
//            if (TextBoxValidationlbl(txtHouseNoServ, document.getElementById("spntxtHouseNoServ"), "House No") == false) IsValid = false;
            if (TextBoxBlurValidationlblZeroAccepts(txtHouseNoServ, 'spntxtHouseNoServ', "House No") == false) IsValid = false;
            if (TextBoxValidationlbl(txtStreetNameServ, document.getElementById("spntxtStreetNameServ"), "Street Name") == false) IsValid = false;
            if (TextBoxValidationlbl(txtCityServ, document.getElementById("spntxtCityServ"), "City") == false) IsValid = false;
            if (TextBoxValidationlbl(txtZipServ, document.getElementById("spntxtZipServ"), "Service Zip") == false) IsValid = false;
//            if (TextBoxValidationlbl(txtAreaServ, document.getElementById("spntxtAreaServ"), "Service Area") == false) IsValid = false;
            //if (RadioButtonListlbl(rdoCommunicationServ, document.getElementById("spnrdoCommunicationServ"), "Service Area") == false) IsValid = false;
        }
    }
    else {
        if (hfIsServiceAddress.value == "false") {
//            if (TextBoxValidationlbl(txtHouseNoServ, document.getElementById("spntxtHouseNoServ"), "House No") == false) IsValid = false;
            if (TextBoxBlurValidationlblZeroAccepts(txtHouseNoServ, 'spntxtHouseNoServ', "House No") == false) IsValid = false;
            if (TextBoxValidationlbl(txtStreetNameServ, document.getElementById("spntxtStreetNameServ"), "Street Name") == false) IsValid = false;
            if (TextBoxValidationlbl(txtCityServ, document.getElementById("spntxtCityServ"), "City") == false) IsValid = false;
            //if (TextBoxValidationlbl(txtZipServ, document.getElementById("spntxtZipServ"), "Service Zip") == false) IsValid = false;
            //if (TextBoxValidationlbl(txtAreaServ, document.getElementById("spntxtAreaServ"), "Service Area") == false) IsValid = false;
        }
        else {
//            if (TextBoxValidationlbl(txtSHNo, document.getElementById("spanSHNo"), "House No") == false) IsValid = false;
            if (TextBoxBlurValidationlblZeroAccepts(txtSHNo, 'spanSHNo', "House No") == false) IsValid = false;
            if (TextBoxValidationlbl(txtSStreet, document.getElementById("spanSStreet"), "Street Name") == false) IsValid = false;
            if (TextBoxValidationlbl(txtSLGAVillage, document.getElementById("spanSLGAVillage"), "City") == false) IsValid = false;
            //if (TextBoxValidationlbl(txtSZip, document.getElementById("spanSZip"), "Service Zip") == false) IsValid = false;
        }
        //        if (RadioButtonListlbl(rblCommunicationAddress, document.getElementById("spanrblCommunicationAddress"), "Service Area") == false) IsValid = false;

    }

    if (!IsValid) {
        return false;
    }
}