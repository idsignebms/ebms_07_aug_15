﻿
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
}

function Validate() {
    var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo');

    var IsValid = true;

    if (TextBoxValidationlbl(txtAccountNo, document.getElementById("spanAccNo"), "Global Account No / Old Account No") == false) IsValid = false;

    if (txtAccountNo.value.length == 0)
        document.getElementById('UMSNigeriaBody_divdetails').style.display = "none";
    if (!IsValid) {
        return false;
    }
}

function ShowMessage() {
    var mpee = $find('UMSNigeriaBody_mpeMessage');
    mpee.show();
    return false;
}

function emailValidation(txtEmailID, spanId, spanText) {
    var Valid = true;
    document.getElementById('UMSNigeriaBody_spanOEMailExists').innerHTML = '';
    if (TextBoxValidationlbl(txtEmailID, document.getElementById(spanId), spanText) == false)
        Valid = false;
    else if (OnlyEmailValidation(txtEmailID, spanId, spanText) == false)
        Valid = false;
    return Valid;
}

function OnlyEmailValidation(txtEmailID, spanId, spanText) {
    var Valid = true;
    document.getElementById('UMSNigeriaBody_spanOEMailExists').innerHTML = '';
    if (txtEmailID.value != "" && EmailValidation(txtEmailID) == false) {
        var spanEmail = document.getElementById(spanId);
        spanEmail.innerHTML = "Please Enter Valid Email Id";
        spanEmail.style.display = "block";
        txtEmailID.style.border = "1px solid red";
        Valid = false;
    }
//    else if (IsEmailAlreadyExists(txtEmailID.value)) {
//        document.getElementById(spanId).innerHTML = "EmailId already exists.";
//        document.getElementById(spanId).style.display = "block";
//        txtEmailID.style.border = "1px solid red";
//        Valid = false;
//    }
    else {
        txtEmailID.style.border = "1px solid #8E8E8E";
        document.getElementById(spanId).innerHTML = "";
    }
    return Valid;
}

function CustomerContactNoLengthMinMax(TextBox, MinLength, MaxLength, lblmsg, txtCode, CodeLength) {
    lblmsg.innerHTML = "";
    var IsValid = true;
    if (TextBox.value.length < MinLength) {
        lblmsg.innerHTML = "Contact no should be Minimum " + MinLength + " characters.";
        lblmsg.style.display = "block";
        TextBox.style.border = "1px solid red";
        //TextBox.focus();
        IsValid = false;
    }
    else if (TextBox.value.length > MaxLength) {
        lblmsg.innerHTML = "Contact no should be Maximum " + MaxLength + " characters.";
        lblmsg.style.display = "block";
        TextBox.style.border = "1px solid red";
        //TextBox.focus();
        IsValid = false;
    }
    else if (TextBoxValidationlbl(txtCode, lblmsg, 'Code') == false) {
        IsValid = false;
    }
    else if (txtCode.value.length > CodeLength) {
        lblmsg.innerHTML = "Code should be Maximum " + CodeLength + " characters.";
        lblmsg.style.display = "block";
        txtCode.style.border = "1px solid red";
        //txtCode.focus();
        IsValid = false;
    }
    return IsValid;
}


function IsSameEmail() {
    var IsValid = true;
    if (document.getElementById('UMSNigeriaBody_txtOEmailId').value != "" &&
    document.getElementById('UMSNigeriaBody_txtTEmailId').value != "") {
        if (document.getElementById('UMSNigeriaBody_txtOEmailId').value == document.getElementById('UMSNigeriaBody_txtTEmailId').value != "") {
            var spanEmail = document.getElementById('spanTEmailId');
            spanEmail.innerHTML = "Mail Id can not be same as Landlord email id";
            spanEmail.style.display = "block";
            document.getElementById('UMSNigeriaBody_txtTEmailId').style.border = "1px solid red";
            IsValid = false;
        }
    }
    return IsValid;
}

function CustomerEmailValidate(txtEmailID, spanId, spanText) {
    var Valid = true;
    document.getElementById('UMSNigeriaBody_spanOEMailExists').innerHTML = '';
    if (EmailValidation(txtEmailID) == false) {
        var spanEmail = document.getElementById(spanId);
        spanEmail.innerHTML = "Please Enter Valid Email Id";
        spanEmail.style.display = "block";
        txtEmailID.style.border = "1px solid red";
        Valid = false;
    }
    else {
        var spanEmail = document.getElementById(spanId);
        spanEmail.innerHTML = "";
        txtEmailID.style.border = "1px solid #8E8E8E";
        Valid = false;
    }
    return Valid;
}

//function UpdateValidate() {
//    var Valid = true;
//    var txtOEmailId = document.getElementById('UMSNigeriaBody_txtOEmailId');
//    var txtOHPhoneCode = document.getElementById('UMSNigeriaBody_txtOHPhoneCode');
//    var txtOHPhone = document.getElementById('UMSNigeriaBody_txtOHPhone');
//    var txtOBPhoneCode = document.getElementById('UMSNigeriaBody_txtOBPhoneCode');
//    var txtOBPhone = document.getElementById('UMSNigeriaBody_txtOBPhone');
//    var txtOOPhoneCode = document.getElementById('UMSNigeriaBody_txtOOPhoneCode');
//    var txtOOPhone = document.getElementById('UMSNigeriaBody_txtOOPhone');
//    var txtReason = document.getElementById('UMSNigeriaBody_txtReason');

//    return Valid;
////    if(mailValidation(txtOEmailId, 'spanOEmailId', 'Email Id')
//}



function IsEmailAlreadyExists(EmailId) {
    var flag = false;
    $.ajax({
        type: "POST",
        url: "/ConsumerManagement/ChangeCustomerContactInfo.aspx/IsEmailExists",
        data: '{EmailId: "' + EmailId + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            flag = response.d;
        },
        failure: function (response) {
            alert(response.d);
        }
    });
    return flag;
}
function OnSuccess(data) {
    if (data.d) {
        var spanEmail = document.getElementById('spanOEmailId');
        spanEmail.innerHTML = "Email already exists";
        spanEmail.style.display = "block";
        document.getElementById('UMSNigeriaBody_txtOEmailId').style.border = "1px solid red";
    }
}