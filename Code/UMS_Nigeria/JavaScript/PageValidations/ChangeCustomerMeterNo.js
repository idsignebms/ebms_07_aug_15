﻿
//function pageLoad() {
//    $(document).ready(function () {
//        DisplayMessage('UMSNigeriaBody_pnlMessage');
//        Calendar('UMSNigeriaBody_txtReadingDate', 'UMSNigeriaBody_imgReadingDate', "-2:+8");
//        Calendar('UMSNigeriaBody_txtMeterChangedDate', 'UMSNigeriaBody_imgMeterChangedDate', "-2:+8");
//        //DatePicker($('#UMSNigeriaBody_txtReadingDate'), $('#UMSNigeriaBody_imgReadingDate'));
//        //DatePicker($('#UMSNigeriaBody_txtMeterChangedDate'), $('#UMSNigeriaBody_imgMeterChangedDate'));
//        AutoComplete($('#UMSNigeriaBody_txtAccountNo'), 1);

//    });
//    var prm = Sys.WebForms.PageRequestManager.getInstance();
//    prm.add_endRequest(function () {
//        Calendar('UMSNigeriaBody_txtReadingDate', 'UMSNigeriaBody_imgReadingDate', "-2:+8");
//        Calendar('UMSNigeriaBody_txtMeterChangedDate', 'UMSNigeriaBody_imgMeterChangedDate', "-2:+8");
//        DisplayMessage('UMSNigeriaBody_pnlMessage');
//        //        DatePicker($('#UMSNigeriaBody_txtReadingDate'), $('#UMSNigeriaBody_imgReadingDate'));
//        //        DatePicker($('#UMSNigeriaBody_txtMeterChangedDate'), $('#UMSNigeriaBody_imgMeterChangedDate'));
//        AutoComplete($('#UMSNigeriaBody_txtAccountNo'), 1);
//    });
//};

function Validate() {
    var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo');
    var IsValid = true;

    if (TextBoxValidationlbl(txtAccountNo, document.getElementById("spanAccNo"), "Global Account No / Old Account No") == false) IsValid = false;

    if (txtAccountNo.value.length = 0)
        document.getElementById('UMSNigeriaBody_divdetails').style.display = "none";

    if (!IsValid) {
        return false;
    }
}

function UpdateValidate() {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var txtMeterNo = document.getElementById('UMSNigeriaBody_txtMeterNo');
    var txtInitialReading = document.getElementById('UMSNigeriaBody_txtInitialReading');
    var txtOldMeterReading = document.getElementById('UMSNigeriaBody_txtOldMeterReading');
    var txtMeterChangedDate = document.getElementById('UMSNigeriaBody_txtMeterChangedDate');
    var txtMeterInitialReading = document.getElementById('UMSNigeriaBody_txtMeterInitialReading');
    var txtReadingDate = document.getElementById('UMSNigeriaBody_txtReadingDate');
    var lblLastReadDate = document.getElementById('UMSNigeriaBody_lblLastReadDate');

    var txtReason = document.getElementById('UMSNigeriaBody_txtReason');

    var IsValid = true;

    if (TextBoxValidationlbl(txtMeterNo, document.getElementById("UMSNigeriaBody_spanMeterNo"), "Meter No") == false)
        IsValid = false;
    else
        if (MeterNoValidation(txtMeterNo, 'UMSNigeriaBody_spanMeterNo', "Meter No") == false) {
            IsValid = false;
        }

    if (TextBoxValidationlblZeroAccepts(txtOldMeterReading, document.getElementById("spanOldMeterReading"), "Current Reading") == false) IsValid = false;
    if (TextBoxValidationlbl(txtMeterChangedDate, document.getElementById("spanMeterChangedDate"), "Meter Changed Date") == false) {
        IsValid = false;
    }
    else if (isDateLbl(txtMeterChangedDate.value, txtMeterChangedDate, "spanMeterChangedDate") == false) { IsValid = false; }
    else if (FromToDateInPopup(mpeAlert, lblAlertMsg, lblLastReadDate.innerHTML, txtMeterChangedDate.value, 'Meter Changed Date should be greater than Last Read Date') == false) {
        IsValid = false;
    }

    if (TextBoxValidationlblZeroAccepts(txtMeterInitialReading, document.getElementById("spanMeterInitialReading"), "Meter Initial Reading") == false) IsValid = false;

    if (txtInitialReading.value != "") {
        if (TextBoxValidationlblZeroAccepts(txtInitialReading, document.getElementById("spanInitialReading"), "Present Meter Reading") == false) IsValid = false;
        if (TextBoxValidationlbl(txtReadingDate, document.getElementById("spanReadingDate"), "Reading Date") == false) {
            IsValid = false;
        }
        else if (isDateLbl(txtReadingDate.value, txtReadingDate, "spanReadingDate") == false) { IsValid = false; }
        else if (FromToDateInPopup(mpeAlert, lblAlertMsg, txtMeterChangedDate.value, txtReadingDate.value, 'Reading Date should be greater than Meter Changed Date') == false) {
            IsValid = false;
        }
    }
    if (TextBoxValidationlbl(txtReason, document.getElementById("spanReason"), "Reason") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
    else {
        if (GreaterTodayInPopup(mpeAlert, lblAlertMsg, txtMeterChangedDate, "Meter Changed Date should not be Greater than the Today's Date") == false) {
            return false;
        }
        else if (GreaterTodayInPopup(mpeAlert, lblAlertMsg, txtReadingDate, "Reading Date should not be Greater than the Today's Date") == false) {
            return false;
        }
    }
}

function MeterNoValidation(txt, span, field) {
    if (TextBoxBlurValidationlbl(txt, span, field) != false) {
        var lblMeterNo = document.getElementById('UMSNigeriaBody_lblMeterNo');
        var lbl = document.getElementById(span);
        lbl.innerHTML = "";
        lbl.style.display = "none";
        if (lblMeterNo.innerHTML == Trim(txt.value)) {
            lbl.innerHTML = "Meter No should not be same as existing one";
            lbl.style.display = "block";
            txt.style.border = "1px solid red";
            return false;
        }
        else {
            txt.style.border = "1px solid #8E8E8E";
        }
    }
}