﻿function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};

function Validate() {
    var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo');

    var IsValid = true;

    if (TextBoxValidationlbl(txtAccountNo, document.getElementById("spanAccNo"), "Account No / Old Account No") == false) IsValid = false;

    if (txtAccountNo.value.length = 0)
        document.getElementById('UMSNigeriaBody_divdetails').style.display = "none";
    if (!IsValid) {
        return false;
    }
}

function UpdateValidate() {   
    var txtReason = document.getElementById('UMSNigeriaBody_txtReason');
    //var ddlOTitle = document.getElementById('UMSNigeriaBody_ddlOTitle');
    var txtOFName = document.getElementById('UMSNigeriaBody_txtOFName');
    //var txtOMName = document.getElementById('UMSNigeriaBody_txtOMName');
    var txtOLName = document.getElementById('UMSNigeriaBody_txtOLName');
    //var txtOKnownAs = document.getElementById('UMSNigeriaBody_txtOKnownAs');

    var IsValid = true;
    //if (DropDownlistValidationlbl(ddlOTitle, document.getElementById('spanOTitle'), "Title") == false) IsValid = false;
    if (TextBoxValidationlbl(txtOFName, document.getElementById("spanOFName"), "First Name") == false) IsValid = false;
    //if (TextBoxValidationlbl(txtOMName, document.getElementById("spanOMName"), "Middle Name") == false) IsValid = false;
    if (TextBoxValidationlbl(txtOLName, document.getElementById("spanOLName"), "Last Name") == false) IsValid = false;
    //if (TextBoxValidationlbl(txtOKnownAs, document.getElementById("spanOKnownAs"), "Known As") == false) IsValid = false;
    if (TextBoxValidationlbl(txtReason, document.getElementById("spanReason"), "Reason") == false) IsValid = false;
    if (!IsValid) {
        return false;
    }
}
function CopyNames() {
    var txtOFName = document.getElementById('UMSNigeriaBody_txtOFName');
    var txtOMName = document.getElementById('UMSNigeriaBody_txtOMName');
    var txtOLName = document.getElementById('UMSNigeriaBody_txtOLName');
    var txtOKnownAs = document.getElementById('UMSNigeriaBody_txtOKnownAs');
    txtOKnownAs.value = txtOFName.value + " " + txtOMName.value + " " + txtOLName.value;
}