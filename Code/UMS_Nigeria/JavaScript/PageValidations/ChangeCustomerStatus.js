﻿function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');



    $(document).ready(function () {
        DisplayMessage('UMSNigeriaBody_pnlMessage');
        Calendar('UMSNigeriaBody_txtChangeDate', 'UMSNigeriaBody_imgDate', "-2:+8");

    });
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(function () {
        Calendar('UMSNigeriaBody_txtChangeDate', 'UMSNigeriaBody_imgDate', "-2:+8");

    });
};

function Validate() {
    var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo');
    //var txtChangeDate = document.getElementById('UMSNigeriaBody_txtChangeDate');
    var IsValid = true;

    if (TextBoxValidationlbl(txtAccountNo, document.getElementById("spanAccNo"), "Global Account No / Old Account No") == false) IsValid = false;
    //    if (txtAccountNo.value.length = 0)
    //        document.getElementById('UMSNigeriaBody_divdetails').style.display = "none";

    //if (TextBoxValidationlbl(txtChangeDate, document.getElementById("spanChangeDate"), "Change Date") == false) IsValid = false;
    if (!IsValid) {
        return false;
    }
}

function UpdateValidate() {
    var txtReason = document.getElementById('UMSNigeriaBody_txtReason');
    var ddlStatus = document.getElementById('UMSNigeriaBody_ddlStatus');
    //var txtChangeDate = document.getElementById('UMSNigeriaBody_txtChangeDate');
    var mpeAlert = $find('UMSNigeriaBody_mpeAlert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblAlertMsg');

    var IsValid = true;

    if (DropDownlistValidationlbl(ddlStatus, document.getElementById("spanStatus"), "Status") == false) IsValid = false;
    if (TextBoxValidationlbl(txtReason, document.getElementById("spanReason"), "Reason") == false) IsValid = false;

    //    if (TextBoxValidationlbl(txtChangeDate, document.getElementById("spanChangeDate"), "Change Date") == false) {
    //        IsValid = false;
    //    }
    //    else if (isDateLbl(txtChangeDate.value, txtChangeDate, "spanChangeDate") == false) {
    //        IsValid = false;
    //    }

    //    if (IsValid) {

    //        if (GreaterTodayInPopup(mpeAlert, lblAlertMsg, txtChangeDate, "Change Date should not be Greater than Today's Date") == false) return false;
    //    }
    //    else {
    //        return false;
    //    }
    if (!IsValid) {
        return false;
    }
}
function GetAccountDetails(e) {

    var key = e.keyCode || e.which;
    if (key == 13) {
        var btnGo = document.getElementById('UMSNigeriaBody_btnGo');
        btnGo.click();
    }
}