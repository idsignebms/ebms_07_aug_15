﻿function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};


function Validate() {
    var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo');

    var IsValid = true;

    if (TextBoxValidationlbl(txtAccountNo, document.getElementById("spanAccNo"), "Account No / Old Account No") == false) IsValid = false;

    if (txtAccountNo.value.length = 0)
        document.getElementById('UMSNigeriaBody_divdetails').style.display = "none";
    if (!IsValid) {
        return false;
    }
}

function UpdateValidate() {
    var txtReason = document.getElementById('UMSNigeriaBody_txtReason');
    var ddlCustomerType = document.getElementById('UMSNigeriaBody_ddlCustomerType');
   
    var IsValid = true;
    if (DropDownlistValidationlbl(ddlCustomerType, document.getElementById('spanCustomerType'), "Customer Type") == false) IsValid = false;
    if (TextBoxValidationlbl(txtReason, document.getElementById("spanReason"), "Reason") == false) IsValid = false;
    if (!IsValid) {
        return false;
    }
}