﻿function UpdateValidation(GridBU) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var GridBU = document.getElementById(GridBU);

    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, GridBU, "Business Unit") == false) { return false; }
}

function pageLoad() {
    $(document).ready(function () {
        DisplayMessage('UMSNigeriaBody_pnlMessage');
    });
};