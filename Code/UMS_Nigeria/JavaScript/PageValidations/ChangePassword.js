﻿function Validate() {
    var txtOldPwd = document.getElementById('UMSNigeriaBody_txtOldPwd');
    var txtNewPwd = document.getElementById('UMSNigeriaBody_txtNewPwd');
    var txtReEnterPwd = document.getElementById('UMSNigeriaBody_txtReEnterPwd');

    var IsValid = true;

    if (TextBoxValidationlbl(txtOldPwd, document.getElementById("spanOldPwd"), "Old Password") == false) IsValid = false;
    if (TextBoxValidationlbl(txtNewPwd, document.getElementById("spanNewPwd"), "New Password") == false) IsValid = false;
    if (TextBoxValidationlbl(txtReEnterPwd, document.getElementById("spanReEnterPwd"), "New Password Again") == false) IsValid = false;
    if (txtNewPwd.value != "") {
        if (TextBoxLengthValidationLbl(txtNewPwd, "Min 6 Chars", document.getElementById("spanNewPwd"), 6) == false) IsValid = false;
    }
    if (txtNewPwd.value != txtReEnterPwd.value) {
        var spanReEnterPwd = document.getElementById("spanReEnterPwd");
        spanReEnterPwd.innerHTML = "Password Mismatch";
        spanReEnterPwd.style.display = "block";
        txtReEnterPwd.style.border = "1px solid red";
        IsValid = false;
    }

    if (!IsValid) {
        return false;
    }
}

function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
}; 