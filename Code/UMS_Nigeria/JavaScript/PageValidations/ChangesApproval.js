﻿function ValidateRequest() {
    var ddlStatus = document.getElementById('UMSNigeriaBody_ddlApprovalStatus');
    var txtRemarks = document.getElementById('UMSNigeriaBody_txtRemarks');
          var IsValid = true;

          if (DropDownlistValidationlbl(ddlStatus, document.getElementById("spanApprovalStatus"), "Status") == false) IsValid = false;
          if (TextBoxValidationlbl(txtRemarks, document.getElementById("spanRemarks"), "Reason") == false) IsValid = false;

        if (!IsValid) {
            return false;
        }
}
