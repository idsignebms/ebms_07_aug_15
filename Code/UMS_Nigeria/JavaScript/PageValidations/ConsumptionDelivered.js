﻿
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};

function Validate() {
    var ddlBusinessUnitName = document.getElementById('UMSNigeriaBody_ddlBusinessUnitName');
    var ddlServiceUnitName = document.getElementById('UMSNigeriaBody_ddlServiceUnitName');
    var txtCapacity = document.getElementById('UMSNigeriaBody_txtCapacity');
    var txtSUPPMConsumption = document.getElementById('UMSNigeriaBody_txtSUPPMConsumption');
    var txtSUCreditConsumption = document.getElementById('UMSNigeriaBody_txtSUCreditConsumption');
    var lblTotalConsumption = document.getElementById('UMSNigeriaBody_lblTotalConsumption');
    
    var IsValid = true;

    if (DropDownlistValidationlbl(ddlBusinessUnitName, document.getElementById("spanddlBusinessUnitName"), "Business Unit") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlServiceUnitName, document.getElementById("spanddlServiceUnitName"), "Service Unit") == false) IsValid = false;
    if (TextBoxBlurValidationlblNonZero(txtCapacity, "spanCapacity", "Capacity") == false) IsValid = false;
    if (TextBoxBlurValidationlblNonZero(txtSUPPMConsumption, "spanSUPPMConsumption", "Service Unit PPM Consumption") == false) IsValid = false;
    if (TextBoxBlurValidationlblNonZero(txtSUCreditConsumption, "spanSUCreditConsumption", "Service Unit Credit Consumption") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
}

function UpdateValidation(Capacity, SUCreditConsumption, SUPPMConsumption, TotalConsumption) {

    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var txtGridCapacity = document.getElementById(Capacity);
    var txtGridSUCreditConsumption = document.getElementById(SUCreditConsumption);
    var txtGridSUPPMConsumption = document.getElementById(SUPPMConsumption);
    var lblGridTotalConsumption = document.getElementById(TotalConsumption);

    var IsValid = true;
    debugger;
    if (TextBoxValidationNonZeroInPopup(mpeAlert, lblAlertMsg, txtGridCapacity, "Capacity") == false) {
        IsValid = false; 
    }
    if (TextBoxValidationNonZeroInPopup(mpeAlert, lblAlertMsg, txtGridSUPPMConsumption, "Service Unit PPM Consumption") == false) {
        IsValid = false;
    }
    if (TextBoxValidationNonZeroInPopup(mpeAlert, lblAlertMsg, txtGridSUCreditConsumption, "Service Unit Credit Consumption") == false) {
        IsValid = false;
    }
    if (!IsValid) {
        return false;
    }
    else {

        lblGridTotalConsumption.innerHTML = parseFloat(txtGridSUCreditConsumption.value) + parseFloat(txtGridSUPPMConsumption.value);
        return true;
    }
}

function TextBoxBlurValidationlblNonZero(txt, lblId, field) {
    var lbl = document.getElementById(lblId);
    //txt.className = "normalfld";
    lbl.innerHTML = "";
    lbl.style.display = "none";
    if (Trim(txt.value) == "" || Trim(txt.value) == "." || parseFloat(Trim(txt.value)) == 0) {
        lbl.innerHTML = "Enter " + field;
        lbl.style.display = "block";
        txt.style.border = "1px solid red";
        //txt.focus();
        return false;
    }
    else {
        txt.style.border = "1px solid #8E8E8E";
        var txtSUPPMConsumption = document.getElementById('UMSNigeriaBody_txtSUPPMConsumption');
        var txtSUCreditConsumption = document.getElementById('UMSNigeriaBody_txtSUCreditConsumption');
        var lblTotalConsumption = document.getElementById('UMSNigeriaBody_lblTotalConsumption');
        if (txt.id == txtSUPPMConsumption.id || txt.id == txtSUCreditConsumption.id) {
            var ppm = (txtSUPPMConsumption.value == "" || Trim(txtSUPPMConsumption.value) == ".") ? 0 : parseFloat(Trim(txtSUPPMConsumption.value));
            var credit = (txtSUCreditConsumption.value == "" || Trim(txtSUCreditConsumption.value) == ".") ? 0 : parseFloat(Trim(txtSUCreditConsumption.value));
            lblTotalConsumption.innerHTML = ppm + credit;
        
        }
    }
}


function TextBoxValidationNonZeroInPopup(popup, lblmsg, Textboxvalues, Field) {

    if (Trim(Textboxvalues.value) == "" || Trim(Textboxvalues.value) == "." || parseFloat(Trim(Textboxvalues.value)) == 0) {
        //alert("Enter " + Field + "");
        lblmsg.innerHTML = "Enter " + Field + "";
        lblmsg.style.display = "block";
        popup.show();
        Textboxvalues.focus();
        return false;
    }
}