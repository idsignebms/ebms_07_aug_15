﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');

function Validate() {
    document.getElementById('spanAccountNo').innerHTML = "";
    var IsValid = false;
    var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo').value;
    if (txtAccountNo.length > 0)
        IsValid = true;
    else {
        IsValid = false;
        document.getElementById('spanAccountNo').innerHTML = "Enter Global Account No / Old Account No.";
    }
    return IsValid;
}
function printGrid() { 

    var printContent = document.getElementById('UMSNigeriaBody_divPrint');

    var windowUrl = 'about:blank';
    var uniqueName = new Date();
    var windowName = 'Print' + uniqueName.getTime();
    var printWindow = window.open();
    printWindow.document.write(printContent.innerHTML);
    printWindow.document.close();
    printWindow.focus();
    printWindow.print();
    printWindow.close();
}

function GetAccountDetails(e) {
    var key = e.keyCode || e.which;
    if (key == 13) {
        var btnGo = document.getElementById('UMSNigeriaBody_btnGo');
        btnGo.click();
    }
}
function GetCurrencyFormate(obj) {
    obj.value = NewTextWith2Decimals(obj);
}
function isNumberKey(evt, txt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    if ((charCode == 46 && txt.value.split('.').length > 1)) {
        return false;
    }
}
function ValidatePayment() {
    var IsValid = true;
    var txtAmount = document.getElementById('UMSNigeriaBody_txtAmount');
    var txtPaidDate = document.getElementById('UMSNigeriaBody_txtPaidDate');
    var mpeAlert = $find('UMSNigeriaBody_mpeAlert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblAlertMsg');

    if (TextBoxValidationlbl(txtAmount, document.getElementById('spanPaidAmount'), " Paid Amount") == false) IsValid = false;
    else 
    {
        if (txtAmount.value != "") {
            if (txtAmount.value.charAt(0) == "." || txtAmount.value < 1) {
                var lbl = document.getElementById('spanPaidAmount');
                lbl.innerHTML = "Enter Valid Amount";
                lbl.style.display = "block";
                txtAmount.style.border = "1px solid red";
                IsValid = false;
            }
            else
                txtAmount.value = TrimLeadingZeros(txtAmount.value);
        }
    }
    if (TextBoxValidationlbl(txtPaidDate, document.getElementById('spanPaidDate'), "Payment Received Date") == false) IsValid = false;
    else if (isDateLbl(txtPaidDate.value, txtPaidDate, "spanPaidDate") == false) IsValid = false;

    if (IsValid) {
        if (GreaterTodayInPopup(mpeAlert, lblAlertMsg, txtPaidDate, "Payment Received Date should not be Greater than the Today's Date") == false) return false;
    }
    else
        return false;
}
function ValidatePaidAmount() {
    var txt = document.getElementById('UMSNigeriaBody_txtAmount');
    if (txt.value.charAt(0) == ".") {
        var lbl = document.getElementById('spanPaidAmount');
        lbl.innerHTML = "Enter Valid Amount";
        lbl.style.display = "block";
        txt.style.border = "1px solid red";
        return false;
    }
    else {
        txt.style.border = "1px solid #8E8E8E";
        document.getElementById('spanPaidAmount').innerHTML = "";
        return TextBoxBlurValidationlbl(obj, 'spanPaidAmount', 'Paid Amount');
        return true;
    }
}
