﻿function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};

//function Validate() {
//    var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo');
//    var IsValid = true;

//    if (TextBoxValidationlbl(txtAccountNo, document.getElementById("spanAccNo"), "Account No / Old AccountNo") == false) IsValid = false;

//    if (txtAccountNo.value == "")
//        document.getElementById('UMSNigeriaBody_divdetails').style.display = "none";

//    if (!IsValid) {
//        return false;
//    }
//}

function Validate() {
    var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo');

    var IsValid = true;

    if (TextBoxValidationlbl(txtAccountNo, document.getElementById("spanAccNo"), "Global Account No / Old Account No") == false) IsValid = false;

    if (txtAccountNo.value.length = 0)
        document.getElementById('UMSNigeriaBody_divdetails').style.display = "none";
    if (!IsValid) {
        return false;
    }
}

function CopyAddress(IsCopy) {
    var txtLandMark = document.getElementById('UMSNigeriaBody_txtLandMark');
    var txtHouseNo = document.getElementById('UMSNigeriaBody_txtHouseNo');
    var txtCity = document.getElementById('UMSNigeriaBody_txtCity');
    var txtStreet = document.getElementById('UMSNigeriaBody_txtStreet');
    var txtZipCode = document.getElementById('UMSNigeriaBody_txtZipCode');


    var txtServiceLandMark = document.getElementById('UMSNigeriaBody_txtServiceLandMark');
    var txtServiceHouseNo = document.getElementById('UMSNigeriaBody_txtServiceHouseNo');
    var txtServiceCity = document.getElementById('UMSNigeriaBody_txtServiceCity');
    var txtServiceStreet = document.getElementById('UMSNigeriaBody_txtServiceStreet');
    var txtServiceZipCode = document.getElementById('UMSNigeriaBody_txtServiceZipCode');


    if (IsCopy) {
        txtServiceLandMark.value = txtLandMark.value;
        txtServiceHouseNo.value = txtHouseNo.value;
        txtServiceCity.value = txtCity.value;
        txtServiceStreet.value = txtStreet.value;
        txtServiceZipCode.value = txtZipCode.value;
    }
    else {
        txtServiceLandMark.value = txtServiceHouseNo.value = txtServiceCity.value = txtServiceStreet.value = txtServiceZipCode.value = "";
    }
}
function UpdateValidate() {

    var ddlBU = document.getElementById('UMSNigeriaBody_ddlBU');
    var ddlSU = document.getElementById('UMSNigeriaBody_ddlSU');
    var ddlSC = document.getElementById('UMSNigeriaBody_ddlSC');
    var ddlCycle = document.getElementById('UMSNigeriaBody_ddlCycle');
    var ddlBookNo = document.getElementById('UMSNigeriaBody_ddlBookNo');
    var txtBookReason = document.getElementById('UMSNigeriaBody_txtBookReason');

    var txtPHNo = document.getElementById('UMSNigeriaBody_txtPHNo');
    var txtPStreet = document.getElementById('UMSNigeriaBody_txtPStreet');
    var txtPLGAVillage = document.getElementById('UMSNigeriaBody_txtPLGAVillage');
    var txtPZip = document.getElementById('UMSNigeriaBody_txtPZip');
    var txtPArea = document.getElementById('UMSNigeriaBody_txtPArea');

    var txtSHNo = document.getElementById('UMSNigeriaBody_txtSHNo');
    var txtSStreet = document.getElementById('UMSNigeriaBody_txtSStreet');
    var txtSLGAVillage = document.getElementById('UMSNigeriaBody_txtSLGAVillage');
    var txtSZip = document.getElementById('UMSNigeriaBody_txtSZip');
    var txtSArea = document.getElementById('UMSNigeriaBody_txtSArea');
    var rblCommunicationAddress = document.getElementById('UMSNigeriaBody_rblCommunicationAddress');

    var txtHouseNoServ = document.getElementById('UMSNigeriaBody_txtHouseNoServ');
    var txtStreetNameServ = document.getElementById('UMSNigeriaBody_txtStreetNameServ');
    var txtCityServ = document.getElementById('UMSNigeriaBody_txtCityServ');
    var txtZipServ = document.getElementById('UMSNigeriaBody_txtZipServ');
    var txtAreaServ = document.getElementById('UMSNigeriaBody_txtAreaServ');
    var rdoCommunicationServ = document.getElementById('UMSNigeriaBody_rdoCommunicationServ');
    var hfBookNo = document.getElementById('UMSNigeriaBody_hfBookNo');
    var IsValid = true;

    if (DropDownlistValidationlbl(ddlBU, document.getElementById("spanBU"), "Business Unit") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlSU, document.getElementById("spanSU"), "Service Unit") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlSC, document.getElementById("spanSC"), "Service Center") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlCycle, document.getElementById("spanCycle"), "BookGroup") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlBookNo, document.getElementById("spanBook"), "Book Number") == false) IsValid = false;
    if (TextBoxValidationlbl(txtBookReason, document.getElementById("spanBookReason"), "Reason") == false) IsValid = false;

    // Commented by karteek
//    if (TextBoxValidationlblZeroAccepts(txtPHNo, document.getElementById("spanPHNo"), "House No") == false) IsValid = false;
//    if (TextBoxValidationlbl(txtPStreet, document.getElementById("spanPStreet"), "Street Name") == false) IsValid = false;
//    if (TextBoxValidationlbl(txtPLGAVillage, document.getElementById("spanPLGAVillage"), "City") == false) IsValid = false;
    // End by karteek

    //if (TextBoxValidationlbl(txtPZip, document.getElementById("spanPZip"), "Postal Zip") == false) IsValid = false;
    //if (TextBoxValidationlbl(txtPArea, document.getElementById("spanPArea"), "Posat Area") == false) IsValid = false;

    if (document.getElementById("UMSNigeriaBody_cbIsSameAsPostal").checked) {
        //        if (TextBoxValidationlbl(txtHouseNoServ, document.getElementById("spntxtHouseNoServ"), "House No") == false) IsValid = false;


        // Commented by karteek
        //        if (TextBoxBlurValidationlblZeroAccepts(txtHouseNoServ, "spntxtHouseNoServ", "House No") == false) IsValid = false;
        //        if (TextBoxValidationlbl(txtStreetNameServ, document.getElementById("spntxtStreetNameServ"), "Street Name") == false) IsValid = false;
        //        if (TextBoxValidationlbl(txtCityServ, document.getElementById("spntxtCityServ"), "City") == false) IsValid = false;
        if (TextBoxValidationlblZeroAccepts(txtPHNo, document.getElementById("spanPHNo"), "House No") == false) IsValid = false;
        if (TextBoxValidationlbl(txtPStreet, document.getElementById("spanPStreet"), "Street Name") == false) IsValid = false;
        if (TextBoxValidationlbl(txtPLGAVillage, document.getElementById("spanPLGAVillage"), "City") == false) IsValid = false;
        // End by karteek

        //if (TextBoxValidationlbl(txtZipServ, document.getElementById("spntxtZipServ"), "Service Zip") == false) IsValid = false;
        //if (TextBoxValidationlbl(txtAreaServ, document.getElementById("spntxtAreaServ"), "Service Area") == false) IsValid = false;

        //        if (RadioButtonListlbl(rdoCommunicationServ, document.getElementById("spnrdoCommunicationServ"), "Service Area") == false) IsValid = false;

    }
    else {
        //        if (TextBoxValidationlbl(txtSHNo, document.getElementById("spanSHNo"), "House No") == false) IsValid = false;
        if (TextBoxBlurValidationlblZeroAccepts(txtSHNo, "spanSHNo", "House No") == false) IsValid = false;
        if (TextBoxValidationlbl(txtSStreet, document.getElementById("spanSStreet"), "Street Name") == false) IsValid = false;
        if (TextBoxValidationlbl(txtSLGAVillage, document.getElementById("spanSLGAVillage"), "City") == false) IsValid = false;
        //if (TextBoxValidationlbl(txtSZip, document.getElementById("spanSZip"), "Service Zip") == false) IsValid = false;

        //        if (RadioButtonListlbl(rblCommunicationAddress, document.getElementById("spanrblCommunicationAddress"), "Service Area") == false) IsValid = false;
    }

    if (!IsValid) {
        return false;
    }
    else {
        if (hfBookNo.value == ddlBookNo.options[ddlBookNo.selectedIndex].value) {
            var mpeAlert = $find('UMSNigeriaBody_mpeActivate1');
            var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblActiveMsg1');
            lblAlertMsg.innerHTML = "You should change your Book here.";
            mpeAlert.show();
            return false;
        }
    }
}

//function ConfirmValidate() {
//    //            var r = confirm("Are you sure you want to logout?");
//    //            if (r == true) { return true; } else { return false; }
//    var mpeDeActive = $find('mpeDeActive');
//    var lblDeActiveMsg = document.getElementById('lblDeActiveMsg');
//    lblDeActiveMsg.innerHTML = "Are you sure you want to logout?";
//    mpeDeActive.show();
//    var btnDeActiveOk = document.getElementById('btnDeActiveOk');
//    btnDeActiveOk.focus();
//    return false;

//}
//function DoBlur(fld) {
//    fld.className = 'text-box';
//}

//function DoFocus(fld) {
//    fld.className = 'text-box';
//}
//function pageLoad() {
//    DisplayMessage('UMSNigeriaBody_pnlMessage');
//};

//function Validate() {
//    var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo');
//    var IsValid = true;

//    if (TextBoxValidationlbl(txtAccountNo, document.getElementById("spanAccNo"), "Account No / Old Account No") == false) IsValid = false;

//    if (txtAccountNo.value == "")
//        document.getElementById('UMSNigeriaBody_divdetails').style.display = "none";

//    if (!IsValid) {
//        return false;
//    }
//}
//function UpdateValidate() {
//    var ddlBU = document.getElementById('UMSNigeriaBody_ddlBU');
//    var ddlSU = document.getElementById('UMSNigeriaBody_ddlSU');
//    var ddlSC = document.getElementById('UMSNigeriaBody_ddlSC');
//    var ddlCycle = document.getElementById('UMSNigeriaBody_ddlCycle');
//    var ddlBookNo = document.getElementById('UMSNigeriaBody_ddlBookNo');

//    var txtLandMark = document.getElementById('UMSNigeriaBody_txtLandMark');
//    var txtStreet = document.getElementById('UMSNigeriaBody_txtStreet');
//    var txtCity = document.getElementById('UMSNigeriaBody_txtCity');
//    var txtHouseNo = document.getElementById('UMSNigeriaBody_txtHouseNo');
//    var txtZipCode = document.getElementById('UMSNigeriaBody_txtZipCode');
//    var txtServiceLandMark = document.getElementById('UMSNigeriaBody_txtServiceLandMark');
//    var txtServiceStreet = document.getElementById('UMSNigeriaBody_txtServiceStreet');
//    var txtServiceCity = document.getElementById('UMSNigeriaBody_txtServiceCity');
//    var txtServiceHouseNo = document.getElementById('UMSNigeriaBody_txtServiceHouseNo');
//    var txtServiceZipCode = document.getElementById('UMSNigeriaBody_txtServiceZipCode');
//    var txtReason = document.getElementById('UMSNigeriaBody_txtReason');

//    var IsValid = true;
//    if (DropDownlistValidationlbl(ddlBU, document.getElementById("spanBU"), "Business Unit") == false) IsValid = false;
//    if (DropDownlistValidationlbl(ddlSU, document.getElementById("spanSU"), "Service Unit") == false) IsValid = false;
//    if (DropDownlistValidationlbl(ddlSC, document.getElementById("spanSC"), "Service Center") == false) IsValid = false;
//    if (DropDownlistValidationlbl(ddlCycle, document.getElementById("spanCycle"), "BookGroup") == false) IsValid = false;
//    if (DropDownlistValidationlbl(ddlBookNo, document.getElementById("spanBook"), "Book Number") == false) IsValid = false;
//    if (TextBoxValidationlbl(txtReason, document.getElementById("spanReason"), "Reason") == false) IsValid = false;

//    if (TextBoxValidationlbl(txtHouseNo, document.getElementById("spanHouseNo"), "House No") == false) IsValid = false;
//    if (TextBoxValidationlbl(txtStreet, document.getElementById("spanStreet"), "Address 1") == false) IsValid = false;

//    if (TextBoxValidationlbl(txtServiceHouseNo, document.getElementById("spanServiceHouseNo"), "Service House No") == false) IsValid = false;
//    if (TextBoxValidationlbl(txtServiceStreet, document.getElementById("spanServiceStreet"), "Service Address 1") == false) IsValid = false;

//    if (!IsValid) {
//        return false;
//    }
//}

//function CopyAddress(IsCopy) {
//    var txtLandMark = document.getElementById('UMSNigeriaBody_txtLandMark');
//    var txtHouseNo = document.getElementById('UMSNigeriaBody_txtHouseNo');
//    var txtCity = document.getElementById('UMSNigeriaBody_txtCity');
//    var txtStreet = document.getElementById('UMSNigeriaBody_txtStreet');
//    var txtZipCode = document.getElementById('UMSNigeriaBody_txtZipCode');


//    var txtServiceLandMark = document.getElementById('UMSNigeriaBody_txtServiceLandMark');
//    var txtServiceHouseNo = document.getElementById('UMSNigeriaBody_txtServiceHouseNo');
//    var txtServiceCity = document.getElementById('UMSNigeriaBody_txtServiceCity');
//    var txtServiceStreet = document.getElementById('UMSNigeriaBody_txtServiceStreet');
//    var txtServiceZipCode = document.getElementById('UMSNigeriaBody_txtServiceZipCode');


//    if (IsCopy) {
//        txtServiceLandMark.value = txtLandMark.value;
//        txtServiceHouseNo.value = txtHouseNo.value;
//        txtServiceCity.value = txtCity.value;
//        txtServiceStreet.value = txtStreet.value;
//        txtServiceZipCode.value = txtZipCode.value;
//    }
//    else {
//        txtServiceLandMark.value = txtServiceHouseNo.value = txtServiceCity.value = txtServiceStreet.value = txtServiceZipCode.value = "";
//    }
//}
//$(document).keydown(function (e) {
//    // ESCAPE key pressed 
//    if (e.keyCode == 27) {
//        $(".rnkland").fadeOut("slow");
//        $(".modalPopup").fadeOut("slow");
//        document.getElementById("overlay").style.display = "none";
//    }
//});