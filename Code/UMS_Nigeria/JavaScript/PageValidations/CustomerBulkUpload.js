﻿function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
}

function Validate() {
    var IsValid = true;
    var BatchDate = document.getElementById('UMSNigeriaBody_txtBatchDate');
    var BatchNo = document.getElementById('UMSNigeriaBody_txtBatchNo');
    var txtBatchName = document.getElementById('UMSNigeriaBody_txtBatchName');
    if (TextBoxValidationlbl(BatchDate, document.getElementById('spanBatchDate'), " Batch Date") == false) IsValid = false;
    if (isDateLbl(BatchDate.value, BatchDate, "spanBatchDate") == false) IsValid = false;
    if (TextBoxValidationlbl(BatchNo, document.getElementById('spanBatchNo'), " Batch No") == false) IsValid = false;
    if (TextBoxValidationlbl(txtBatchName, document.getElementById('spanBatchName'), " Batch Name") == false) IsValid = false;
    if (!IsValid) {
        return false;
    }
}

function ValidateExcel() {
    var IsValid = true;
    var fupExcel = document.getElementById('UMSNigeriaBody_upDOCExcel');
    if (OnUpload(fupExcel, ['xls', 'xlsx'], " Excel File") == false)
        return false;
}      