﻿function Validate() {
    var ddlTariffSubTypes = document.getElementById('UMSNigeriaBody_ddlTariffSubTypes');
    var ddlTariff = document.getElementById('UMSNigeriaBody_ddlTariff');
    var txtRemarks = document.getElementById('UMSNigeriaBody_txtRemarks');
    var IsValid = true;
    if (DropDownlistValidationlbl(ddlTariff, document.getElementById("spanTariff"), "Tariff") == false) {
        IsValid = false;
    }
    else {
        if (DropDownlistValidationlbl(ddlTariffSubTypes, document.getElementById("spanTariffSubTypes"), "Tariff SubTypes") == false)
            IsValid = false;
    }

    if (TextBoxValidationlbl(txtRemarks, document.getElementById("spanRemarks"), "Remarks") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};