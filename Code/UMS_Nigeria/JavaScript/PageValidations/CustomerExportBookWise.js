﻿function CheckAll() {
    var cbAll = document.getElementById('UMSNigeriaBody_cbAll');
    var chkBoxList = document.getElementById('UMSNigeriaBody_cblBooks');
    CheckBoxListSelect(cbAll, chkBoxList);
}
function UnCheckAll() {
    var chkBoxList = document.getElementById('UMSNigeriaBody_cblBooks');
    var cbAll = document.getElementById('UMSNigeriaBody_cbAll');
    Uncheck(cbAll, chkBoxList);
}

function Validate() {
    var ddlBU = document.getElementById('UMSNigeriaBody_ddlBU');
    var ddlSU = document.getElementById('UMSNigeriaBody_ddlSU');
    var ddlSC = document.getElementById('UMSNigeriaBody_ddlSC');
    var ddlCycle = document.getElementById('UMSNigeriaBody_ddlCycle');
    var divBookNos = document.getElementById('UMSNigeriaBody_divBookNos');

    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var IsValid = true;

    if (DropDownlistValidationlbl(ddlBU, document.getElementById("spanddlBU"), "Business Unit") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlSU, document.getElementById("spanddlSU"), "Service Unit") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlSC, document.getElementById("spanddlSC"), "Service Center") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlCycle, document.getElementById("spanddlCycle"), "Book Group") == false) IsValid = false;

    if (divBookNos != null) {
        var cbl = document.getElementById('UMSNigeriaBody_cblBooks');
        if (ValidateCblInPopup(mpeAlert, lblAlertMsg, cbl, 'Book No') == false) return false;
    }
    if (!IsValid) {
        return false;
    }
}