﻿function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};


function Validate() {
    var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo');

    var IsValid = true;

    if (TextBoxValidationlbl(txtAccountNo, document.getElementById("spanAccNo"), "Account No / Old Account No") == false) IsValid = false;

    if (txtAccountNo.value.length = 0)
        document.getElementById('UMSNigeriaBody_divdetails').style.display = "none";
    if (!IsValid) {
        return false;
    }
}

function UpdateValidate() {
    var txtCurrentReading = document.getElementById('UMSNigeriaBody_txtCurrentReading');
    var lblLastPreviousReading = document.getElementById('UMSNigeriaBody_lblLastPreviousReading');
    var lblLastPresentReading = document.getElementById('UMSNigeriaBody_lblLastPresentReading');
    var lblTotalReadings = document.getElementById('UMSNigeriaBody_lblTotalReadings');
    var mpeAlert = $find('UMSNigeriaBody_mpeAlert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblAlertMsg');

    var IsValid = true;
    if (TextBoxValidationlblZeroAccepts(txtCurrentReading, document.getElementById("spanCurrentReading"), "Current Reading") == false) {
        IsValid = false;
    }
    else if (parseInt(lblTotalReadings.innerHTML) > 0) {
        if (parseInt(lblLastPresentReading.innerHTML) < parseInt(txtCurrentReading.value)) {
            lblAlertMsg.innerHTML = "Current Reading should not be greater than the Last Present Reading.";
            mpeAlert.show();
            return false;
        }
        else if (parseInt(lblLastPreviousReading.innerHTML) > parseInt(txtCurrentReading.value)) {
            lblAlertMsg.innerHTML = "Current Reading should not be less than the Last Previous Reading.";
            mpeAlert.show();
            return false;
        }
    }
    if (!IsValid) {
        return false;
    }
}