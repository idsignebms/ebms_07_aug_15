﻿function Validate() {
    var ddlBU = document.getElementById('UMSNigeriaBody_ddlBU');
    var ddlBook = document.getElementById('UMSNigeriaBody_ddlBook');

    var IsValid = true;

    if (DropDownlistValidationlbl(ddlBU, document.getElementById("spanBU"), "Business Unit") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlBook, document.getElementById("spanBook"), "Book No") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
}

function ValidateRequest() {

    var ddlApprovalStatus = document.getElementById('UMSNigeriaBody_ddlApprovalStatus');
    var txtRemarks = document.getElementById('UMSNigeriaBody_txtRemarks');

    var IsValid = true;

    if (DropDownlistValidationlbl(ddlApprovalStatus, document.getElementById("spanApprovalStatus"), "Status") == false) IsValid = false;
    if (TextBoxValidationlbl(txtRemarks, document.getElementById("spanRemarks"), "Remarks") == false) IsValid = false;

    if (!IsValid) {

        return false;
    }
}

function ValidateMessage(txtMessageId) {
    var txtMessage = document.getElementById(txtMessageId);
    var popup = $find('UMSNigeriaBody_mpegridalert');
    var lblMessage = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    if (TextBoxValidationInPopup(popup, lblMessage, txtMessage, "Message") == false) return false;
}


function RefreshAccordian() {
    $(function () {
        $("#accordion").accordion({
            autoHeight: false,
            event: "mousedown",
            active: parseInt($('input[id$=hidAccordionIndex]').val()),
            collapsible: true,
            change: function (event, ui) {
                var index = $(this).accordion("option", "active");
                $('#UMSNigeriaBody_hidAccordionIndex').val(index);
            }
        });
    });
}


function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
    RefreshAccordian();
};
