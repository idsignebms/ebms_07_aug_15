﻿function CheckAll(cbAllId, cblId) {
    var cbAll = document.getElementById(cbAllId);
    var chkBoxList = document.getElementById(cblId);
    CheckBoxListSelect(cbAll, chkBoxList);
}
function UnCheckAll(cbAllId, cblId) {
    var chkBoxList = document.getElementById(cblId);
    var cbAll = document.getElementById(cbAllId);
    Uncheck(cbAll, chkBoxList);
}