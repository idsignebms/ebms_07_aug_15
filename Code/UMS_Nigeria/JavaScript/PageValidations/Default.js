﻿function TextBoxValidationlbl(txt, lbl, field) {
    lbl.innerHTML = "";
    lbl.style.display = "none";
    if (Trim(txt.value) == "" || Trim(txt.value) == "0") {
        lbl.innerHTML = "Enter " + field;
        lbl.style.display = "block";
        txt.style.border = "1px solid red";

        return false;
    }
    else {
        txt.style.border = "1px solid #8E8E8E";

    }
}
function TextBoxBlurValidationlbl(txt, lblId, field) {
    var lbl = document.getElementById(lblId);
    lbl.innerHTML = "";
    lbl.style.display = "none";
    if (Trim(txt.value) == "" || Trim(txt.value) == "0") {
        lbl.innerHTML = "Enter " + field;
        lbl.style.display = "block";
        txt.style.border = "1px solid red";

        return false;
    }
    else {
        txt.style.border = "1px solid #8E8E8E";

    }
}
function LoginValidate() {
    var txtUserName = document.getElementById('txtUserName');
    var txtPassword = document.getElementById('txtPassword');
    var IsValid = true;

    if (TextBoxValidationlbl(txtUserName, document.getElementById("spanUserName"), "User Id") == false) IsValid = false;
    if (TextBoxValidationlbl(txtPassword, document.getElementById("spanPassword"), "Password") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
}
function Trim(str) {
    while (str.substring(0, 1) == ' ') // check for white spaces from beginning
    {
        str = str.substring(1, str.length);
    }
    while (str.substring(str.length - 1, str.length) == ' ') // check white space from end
    {
        str = str.substring(0, str.length - 1);
    }
    return str;
}
function pageLoad() {
    DisplayMessage('pnlMessage');
};

function DisplayMessage(ControlId) {
    ControlId = '#' + ControlId;
    $(ControlId).fadeOut(5000);
}
window.history.forward();
function noBack() { window.history.forward(); }

function ValidateBusinessUnitSelection() {
    var ddlBusinessUnits = document.getElementById('ddlBusinessUnits');
    var IsValid = true;
    IsValid = DropDownlistValidationlbl(ddlBusinessUnits, document.getElementById("spanddlBusinessUnit"), "Business Unit Name");
    return IsValid;
}