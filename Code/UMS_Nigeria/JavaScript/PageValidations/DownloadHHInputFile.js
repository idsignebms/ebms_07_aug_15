﻿function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};

var prm = Sys.WebForms.PageRequestManager.getInstance();
//Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
prm.add_beginRequest(BeginRequestHandler);
// Raised after an asynchronous postback is finished and control has been returned to the browser.
prm.add_endRequest(EndRequestHandler);
function ValidateStepOne() {
    var ddlYear = document.getElementById('UMSNigeriaBody_ddlYear');
    var ddlMonth = document.getElementById('UMSNigeriaBody_ddlMonth');
    var ddlCycleList = document.getElementById('UMSNigeriaBody_ddlCycleList');

    var IsValid = true;

    if (DropDownlistValidationlbl(ddlYear, document.getElementById("spanYear"), "Year") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlMonth, document.getElementById("spanMonth"), "Month") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlCycleList, document.getElementById("spanCyclesFeeders"), "BookGroup/Feeder") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
}