﻿var i = 1;
function AddFile() {
    i++;
    var div = document.createElement('DIV');
    div.innerHTML = '<input id="file' + i + '" name="file' + i + '" type="file" /><a href="#" style="font-weight:bold;" onclick="return RemoveFile(this)">Remove</a>';
    document.getElementById("divFile").appendChild(div);
    return false;
}

function RemoveFile(file) {
    document.getElementById("divFile").removeChild(file.parentNode);
    return false;
}
function Validate() {
    var txtName = document.getElementById('UMSNigeriaBody_txtName');
    var txtSurName = document.getElementById('UMSNigeriaBody_txtSurName');
    var txtPrimaryEmail = document.getElementById('UMSNigeriaBody_txtPrimaryEmail');
    var txtSecondaryEmail = document.getElementById('UMSNigeriaBody_txtSecondaryEmail');
    var rblGender = document.getElementById('UMSNigeriaBody_rblGender');
    var txtAddress = document.getElementById('UMSNigeriaBody_txtAddress');
    var txtContactNo = document.getElementById('UMSNigeriaBody_txtContactNo');
    var txtAnotherContactNo = document.getElementById('UMSNigeriaBody_txtAnotherContactNo');
    var fupHT = document.getElementById("UMSNigeriaBody_fupDocument");
    var fupPhoto = document.getElementById("UMSNigeriaBody_fupPhoto");
    var txtCode1 = document.getElementById('UMSNigeriaBody_txtCode1');
    var txtCode2 = document.getElementById('UMSNigeriaBody_txtCode2');
    var hfmailId = document.getElementById('UMSNigeriaBody_hfmailId').value;
    var ContactNoLength = "12";
    ContactNoLength = parseInt(ContactNoLength);

    var IsValid = true;

    if (TextBoxValidationlbl(txtName, document.getElementById("spanName"), "Name") == false) IsValid = false;
    if (TextBoxValidationlbl(txtSurName, document.getElementById("spanSurName"), "Surname") == false) IsValid = false;

    if (hfmailId == 1 || txtPrimaryEmail.value != "") {
        if (TextBoxValidationlbl(txtPrimaryEmail, document.getElementById("spanPrimaryEmail"), "Primary Email Id") == false) IsValid = false;
        else
            if (EmailValidation(txtPrimaryEmail) == false) {
                var spanEmail = document.getElementById("spanPrimaryEmail");
                spanEmail.innerHTML = "Please Enter Valid Email Id";
                spanEmail.style.display = "block";
                txtPrimaryEmail.style.border = "1px solid red";
                IsValid = false;
            }
            else {
                txtPrimaryEmail.style.border = "1px solid #8E8E8E";
                document.getElementById("spanPrimaryEmail").innerHTML = "";
            }
    }
    else {
        txtPrimaryEmail.style.border = "1px solid #8E8E8E";
        document.getElementById("spanPrimaryEmail").innerHTML = "";
    }

    if (Trim(txtSecondaryEmail.value) != "") {
        if (EmailValidation(txtSecondaryEmail) == false) {
            var spanEmail = document.getElementById("spanSecondaryEmail");
            spanEmail.innerHTML = "Please Enter Valid Email Id";
            spanEmail.style.display = "block";
            txtSecondaryEmail.style.border = "1px solid red";
            IsValid = false;
        }
        else {
            txtSecondaryEmail.style.border = "1px solid #8E8E8E";
            document.getElementById("spanSecondaryEmail").innerHTML = "";
        }
    }
    else {
        txtSecondaryEmail.style.border = "1px solid #8E8E8E";
        document.getElementById("spanSecondaryEmail").innerHTML = "";
    }
    if (RadioButtonListlbl(rblGender, "spanGender", " Gender") == false) IsValid = false;
    if (TextBoxValidationlbl(txtAddress, document.getElementById("spanAddress"), "Address") == false) IsValid = false;
    //    if (TextBoxValidationlbl(txtContactNo, document.getElementById("spanContactNo"), "Contact No") == false) IsValid = false;
    //    if (Trim(txtContactNo.value) != "") {
    //        if (TextBoxValidContactLbl(txtContactNo, "Contact No", document.getElementById("spanContactNo"), ContactNoLength) == false) IsValid = false;
    //        else {
    //            txtContactNo.style.border = "1px solid #8E8E8E";
    //            document.getElementById("spanContactNo").innerHTML = "";
    //        }
    //    }

    //    if (Trim(txtAnotherContactNo.value) != "") {
    //        if (TextBoxValidContactLbl(txtAnotherContactNo, "Contact No", document.getElementById("spanAnotherContactNo"), ContactNoLength) == false) IsValid = false;
    //        else {
    //            txtAnotherContactNo.style.border = "1px solid #8E8E8E";
    //            document.getElementById("spanAnotherContactNo").innerHTML = "";
    //        }
    //    }
    //    else {
    //        txtAnotherContactNo.style.border = "1px solid #8E8E8E";
    //        document.getElementById("spanAnotherContactNo").innerHTML = "";
    //    }

    if (TextBoxValidationlbl(txtCode1, document.getElementById("spanContactNo"), "Code") == false) IsValid = false;
    if (TextBoxValidationlbl(txtContactNo, document.getElementById("spanContactNo"), "Contact No") == false) IsValid = false;
    else if (ContactNoLengthMinMax(txtContactNo, 6, 10, document.getElementById("spanContactNo"), txtCode1, 3) == false) {
        IsValid = false;
    }
    else {
        txtContactNo.style.border = "1px solid #8E8E8E";
        txtCode1.style.border = "1px solid #8E8E8E";
        document.getElementById("spanContactNo").innerHTML = "";
    }

    if (Trim(txtAnotherContactNo.value) != "" || Trim(txtCode2.value) != "") {
        if (ContactNoLengthMinMax(txtAnotherContactNo, 6, 10, document.getElementById("spanAnotherNo"), txtCode2, 3) == false) IsValid = false;
        else {
            txtAnotherContactNo.style.border = "1px solid #8E8E8E";
            txtCode2.style.border = "1px solid #8E8E8E";
            document.getElementById("spanAnotherNo").innerHTML = "";
        }
    }
    else {
        txtAnotherContactNo.style.border = "1px solid #8E8E8E";
        txtCode2.style.border = "1px solid #8E8E8E";
        document.getElementById("spanAnotherNo").innerHTML = "";
    }

    //Photo validation           

    if (filevalidate(fupPhoto.value, "Photo") == false) return false;
    //            var fileValid = fupPhoto.files[0];
    //            if (fileValid.size > 1024000) {
    //                //alert("scanned photo should be upto 1 MB only");
    //                var spanPhoto = document.getElementById("spanPhoto");
    //                spanPhoto.innerHTML = "scanned photo should be upto 1 MB only";
    //                spanPhoto.style.display = "block";
    //                IsValid = false;
    //            }

    //Document validation
    var divFile = document.getElementById("divFile");
    var Docitems = divFile.getElementsByTagName("input");
    for (var i = 0; i < Docitems.length; i++) {
        var spanDocuments = document.getElementById("spanDocuments").innerHTML = "";
        if (!Trim(Docitems[i].value) == "") {
            if (!/(\.doc|\.docx|\.pdf|\.xlsx|\.xls)$/i.test(Docitems[i].value)) {
                var spanDocuments = document.getElementById("spanDocuments");
                spanDocuments.innerHTML = "Please select .doc, .docx, .xlsx , .pdf and .xls File Formats only";
                spanDocuments.style.display = "block";
                IsValid = false;
                break;
            }
            //                    if (Docitems[i].size > 1024000) {
            //                        //alert("scanned photo should be upto 1 MB only");
            //                        var spanDocuments = document.getElementById("spanDocuments");
            //                        spanDocuments.innerHTML = "Document should be upto 1 MB only";
            //                        spanDocuments.style.display = "block";
            //                        IsValid = false;
            //                    }
        }
        else {
            var spanDocuments = document.getElementById("spanDocuments").innerHTML = "";
        }
    }

    if (!IsValid) {
        return false;
    }
    //            else {
    //                var divFile = document.getElementById("divFile");
    //                var files = divFile.getElementsByTagName("input");
    //                var hfDocuments = document.getElementById('UMSNigeriaBody_hfDocuments');
    //                for (var i = 0; i < files.length; i++) {
    //                    hfDocuments.value = hfDocuments.value + files[i].value + ",";
    //                }
    //}            
}

function filevalidate(filename, Message) {
    document.getElementById("spanPhoto").innerHTML = "";
    //            if (Trim(filename) == "") {
    //                var spanPhoto = document.getElementById("spanPhoto");
    //                spanPhoto.innerHTML = "Please select " + Message;
    //                spanPhoto.style.display = "block";
    //                return false;
    //            }
    //            else
    if (Trim(filename) != "") {
        if (!/(\.jpeg|\.jpg|\.png|\.gif)$/i.test(filename)) {
            var spanPhoto = document.getElementById("spanPhoto");
            spanPhoto.innerHTML = "Please select .jpeg, .jpg, .png and .gif File Formats only";
            spanPhoto.style.display = "block";
            return false;
        }
    }
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};      