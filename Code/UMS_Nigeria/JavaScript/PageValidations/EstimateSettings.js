﻿//function pageLoad() {
//    DisplayMessage('UMSNigeriaBody_pnlMessage');
//};

function EnergyChange(obj, tariffName, cycleId, ClusterCategoryId) {

    if (obj.value == 0) {
        obj.style.backgroundColor = "red";
        obj.style.color = "white";
    }
    else {
        obj.style.backgroundColor = "green";
        obj.style.color = "white";
    }
    PageMethods.SaveEnergyDetails(obj.value, tariffName, cycleId, ClusterCategoryId, Incase_Success, Incase_Failure);
    //  obj.onchange();
}

function Incase_Success(results) {
}
function Incase_Failure(errors) {
}

function CaluculateEnergy(obj, customers, lbltotalEnergy) {
    var labels = obj.parentNode.getElementsByTagName("span");
    if (labels.length > 1) {
        if (obj.value == "") {
            obj.value = "0";
        }
        labels[3].innerHTML = (parseFloat(obj.value) * parseFloat(customers));
    }

    var lblTotalDirectConsumption = document.getElementById("UMSNigeriaBody_lblTotalDirectConsumption");


    var inputs = $(".totalestusageCSS");
    var sum = 0;
    for (i = 0; i < inputs.length; i++) {
        if (inputs[i].innerHTML != 0 && inputs[i].innerHTML != '') {
            sum += parseFloat(inputs[i].innerHTML);
        }
    }
    lblTotalDirectConsumption.innerHTML = sum;

}

function isNumberKey(evt, txt) {
    var IsValid = true;
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        IsValid = false;
    if ((charCode == 46 && txt.value.split('.').length > 1)) {
        IsValid = false;
    }
    if (charCode == 8)
        IsValid = true;
    if (IsValid == false)
        return false;
}



function PageaValidation() {
    var IsValid = true;
    //    var ddlMonth = document.getElementById('UMSNigeriaBody_ddlMonth');
    //    var ddlYear = document.getElementById('UMSNigeriaBody_ddlYear');

    //    if (DropDownlistValidationlbl(ddlYear, document.getElementById('spanYear'), "Year") == false) IsValid = false;
    //    if (DropDownlistValidationlbl(ddlMonth, document.getElementById('spanMonth'), "Month") == false) IsValid = false;

    return IsValid;
}