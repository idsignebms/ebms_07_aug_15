﻿function pageLoad() {

    DisplayMessage('UMSNigeriaBody_pnlMessage');
};


function CheckBoxListSelect(cbControl) {
    var chkBoxList = document.getElementById('UMSNigeriaBody_cblTariff');
    var chkBoxCount = chkBoxList.getElementsByTagName("input");
    for (var i = 0; i < chkBoxCount.length; i++) {
        chkBoxCount[i].checked = cbControl.checked;
    }
}

function Uncheck() {
    var flag = 1;
    var chkBoxList = document.getElementById('UMSNigeriaBody_cblTariff');
    var chkBoxCount = chkBoxList.getElementsByTagName("input");

    for (var i = 0; i < chkBoxCount.length; i++) {
        if (!chkBoxCount[i].checked) {
            flag = 0;
        }
    }

    if (flag == 0)
        document.getElementById('UMSNigeriaBody_cbAll').checked = false;
    else
        document.getElementById('UMSNigeriaBody_cbAll').checked = true;

}

function Validate() {

    var IsValid = true;
    var ddlMonth = document.getElementById('UMSNigeriaBody_ddlMonth');
    var ddlYear = document.getElementById('UMSNigeriaBody_ddlYear');
    var cblTariff = document.getElementById('UMSNigeriaBody_cblTariff');
    var chkBoxCount = chkBoxList.getElementsByTagName("input");
    var counter = 0;
    var atLeast = 1
    if (DropDownlistValidationlbl(ddlMonth, document.getElementById('spanddlCashier'), "Cashier") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlYear, document.getElementById('spanddlCashOffice'), "Cash Office") == false) IsValid = false;

    for (var i = 0; i < chkBoxCount.length; i++) {

        if (chkBoxCount[i].checked) {

            counter++;

        }
    }

    if (atLeast > counter) {
        IsValid = false;
    }

    if (!IsValid) {
        return false;
    }
}