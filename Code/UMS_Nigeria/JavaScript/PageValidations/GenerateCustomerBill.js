﻿function ValidateDetails() {
    var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo');
    var ddlYear = document.getElementById('UMSNigeriaBody_ddlYear');
    var ddlMonth = document.getElementById('UMSNigeriaBody_ddlMonth');
    if (TextBoxValidationlbl(txtAccountNo, document.getElementById("spanAccountNo"), "Account Number") == false) {
        return false;
    }
    if (DropDownlistValidationlbl(ddlYear, document.getElementById('spanYear'), "Year") == false) return false;
    if (DropDownlistValidationlbl(ddlMonth, document.getElementById('spanMonth'), "Month") == false) return false;

    var btnGenerateBill = document.getElementById('UMSNigeriaBody_btnGenerateBill');
    btnGenerateBill.style.display = 'inline';
}

function IsAccountNoChange() {
    var btnGenerateBill = document.getElementById('UMSNigeriaBody_btnGenerateBill');
    btnGenerateBill.style.display = 'none';
}

function ddl_changed() {
    IsAccountNoChange();
}