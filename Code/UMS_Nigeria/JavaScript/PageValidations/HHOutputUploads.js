﻿function Validate() {
    var ddlBusinessUnits = document.getElementById('UMSNigeriaBody_ddlBusinessUnits');
    var ddlServiceUnits = document.getElementById('UMSNigeriaBody_ddlServiceUnits');
    var ddlServiceCenters = document.getElementById('UMSNigeriaBody_ddlServiceCenters');
    var ddlCycleList = document.getElementById('UMSNigeriaBody_ddlCycleList');
    var ddlYear = document.getElementById('UMSNigeriaBody_ddlYear');
    var ddlMonth = document.getElementById('UMSNigeriaBody_ddlMonth');

    var IsValid = true;
    if (DropDownlistValidationlbl(ddlBusinessUnits, document.getElementById("spanddlBusinessUnit"), "Business Unit") == false)
        IsValid = false;
    if (DropDownlistValidationlbl(ddlServiceUnits, document.getElementById("spanServiceUnit"), "Service Unit") == false)
        IsValid = false;
    if (DropDownlistValidationlbl(ddlServiceCenters, document.getElementById("spanServiceCenter"), "Service Center") == false)
        IsValid = false;
    if (DropDownlistValidationlbl(ddlCycleList, document.getElementById("spanCycles"), "BookGroups") == false)
        IsValid = false;
    if (DropDownlistValidationlbl(ddlYear, document.getElementById("spanYear"), "Year") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlMonth, document.getElementById("spanMonth"), "Month") == false) IsValid = false;


    return IsValid;
}