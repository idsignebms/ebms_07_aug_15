﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');
function pageLoad() {

    //Calendar('UMSNigeriaBody_txtNextDate', 'UMSNigeriaBody_imgDate', "-2:+8");

    $(document).ready(function () {
        DisplayMessage('UMSNigeriaBody_pnlMessage');
        DatePicker($('#UMSNigeriaBody_txtNextDate'), $('#UMSNigeriaBody_imgDate'));
    });
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(function () {
        DatePicker($('#UMSNigeriaBody_txtNextDate'), $('#UMSNigeriaBody_imgDate'));
    });
};

function Validate() {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var txtMeterNo = document.getElementById('UMSNigeriaBody_txtMeterNo');
    var txtSerialNo = document.getElementById('UMSNigeriaBody_txtSerialNo');

    var ddlMeterType = document.getElementById('UMSNigeriaBody_ddlMeterType');
    var txtSize = document.getElementById('UMSNigeriaBody_txtSize');
    var txtMultiplier = document.getElementById('UMSNigeriaBody_txtMultiplier');
    var txtBrand = document.getElementById('UMSNigeriaBody_txtBrand');
    var txtMeterRating = document.getElementById('UMSNigeriaBody_txtMeterRating');
    var txtNextDate = document.getElementById('UMSNigeriaBody_txtNextDate');
    //var txtServiceBeforeCalibration = document.getElementById('UMSNigeriaBody_txtServiceBeforeCalibration');
    var txtDials = document.getElementById('UMSNigeriaBody_txtDials');
    var ddlBusinessUnitName = document.getElementById('UMSNigeriaBody_ddlBusinessUnitName');

    var rblIsCAPMI = document.getElementById('UMSNigeriaBody_rblIsCAPMI');
    var txtAmount = document.getElementById('UMSNigeriaBody_txtAmount');

    var IsValid = true;

    if (DropDownlistValidationlbl(ddlBusinessUnitName, document.getElementById("spanddlBusinessUnitName"), "Business Unit") == false) IsValid = false;
    if (TextBoxValidationlbl(txtMeterNo, document.getElementById("spanMeterNo"), "Meter No") == false) IsValid = false;
    if (TextBoxValidationlbl(txtSerialNo, document.getElementById("spanSerialNo"), "Meter Serial Number") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlMeterType, document.getElementById("spanMeterType"), "Meter Type") == false) IsValid = false;
    if (TextBoxValidationlbl(txtSize, document.getElementById("spanMeterSize"), "Meter Size") == false) IsValid = false;
    if (TextBoxValidationlbl(txtMultiplier, document.getElementById("spanMultiplier"), "Multiplier") == false) IsValid = false;
    if (TextBoxValidationlbl(txtBrand, document.getElementById("spanBrand"), "Meter Brand") == false) IsValid = false;
    if (TextBoxValidationlbl(txtMeterRating, document.getElementById("spanMeterRating"), "Meter Rating") == false) IsValid = false;
    if (txtNextDate.value != "") {
        if (TextBoxValidationlbl(txtNextDate, document.getElementById("spanNextDate"), "Next Calibration Date") == false) IsValid = false;
        if (txtNextDate.value != "")
            ValidateDate();
        if (isDateLbl(txtNextDate.value, txtNextDate, "spanNextDate") == false) IsValid = false;
    }
    else {
        document.getElementById("spanNextDate").innerHTML = "";
        txtNextDate.style.border = "1px solid #8E8E8E";
    }
    //if (TextBoxValidationlbl(txtServiceBeforeCalibration, document.getElementById("spanServiceHours"), "Service Hours") == false) IsValid = false;
    var DialsLength = "9";
    DialsLength = parseInt(DialsLength);
    if (TextBoxValidationlbl(txtDials, document.getElementById("spanDials"), "Meter Dials") == false) IsValid = false;
    else {
        //                if (txtDials.value > DialsLength) {
        //                    alert("Dials limit is " + DialsLength.toString() + ". Please enter valid dials");
        //                    IsValid = false;
        //                }
        //                if (txtDials.value == 0) {
        //                    alert("Zero not allowing!");
        //                    IsValid = false;
        //                }


        if (txtDials.value > DialsLength) {
            // alert("Dials limit is " + DialsLength.toString() + ". Please enter valid dials");
            lblAlertMsg.innerHTML = "Dials limit is " + DialsLength.toString() + ". Please enter valid dials";
            mpeAlert.show();
            GridDials.focus();
            return false;
        }
        if (parseInt(txtDials.value) == 0) {
            //alert("Zero not allowing!");
            lblAlertMsg.innerHTML = "Zero not allowing! ";
            mpeAlert.show();
            GridDials.focus();
            return false;
        }

    }


    if (rblIsCAPMI.getElementsByTagName('input')[0].checked) {
        if (TextBoxValidationlbl(txtAmount, document.getElementById("spanAmount"), "CAPMI Amount") == false) IsValid = false;
        else if (parseFloat(txtAmount.value) == 0) {
            //alert("Zero not allowing!");
            lblAlertMsg.innerHTML = "Capmi amount should be greater than zero ";
            //            document.getElementById("spanAmount").innerHTML = "amount should be greater than zero ";
            //            document.getElementById("spanAmount").display = "block";
            mpeAlert.show();
            txtAmount.focus();
            return false;
        }
    }

    if (!IsValid) {
        return false;
    }
}
function GetCurrencyFormate(obj) {
    obj.value = NewTextWith2Decimals(obj);
}
function UpdateValidation(GridMeterNo, GridSerialNo, ddlGridMeterType, GridSize, GridMultiplier, GridBrand, Gridradio, GridCapmiAmt, GridDials) {
    debugger;
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var GridMeterNo = document.getElementById(GridMeterNo);
    var GridSerialNo = document.getElementById(GridSerialNo);
    var ddlGridMeterType = document.getElementById(ddlGridMeterType);
    var GridSize = document.getElementById(GridSize);
    var GridMultiplier = document.getElementById(GridMultiplier);
    var GridBrand = document.getElementById(GridBrand);
    var GridCapmiAmt = document.getElementById(GridCapmiAmt);
    var Gridradio = document.getElementById(Gridradio);
    //var GridMeterRating = document.getElementById(GridMeterRating);
    //var GridNextDate = document.getElementById(GridNextDate);
    var GridDials = document.getElementById(GridDials);   
   // if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, GridMeterNo, "Meter No") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, GridSerialNo, "Meter Serial Number") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlGridMeterType, "Meter Type") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, GridMultiplier, "Multiplier") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, GridSize, "Meter Size") == false) { return false; }
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, GridBrand, "Meter Brand") == false) { return false; }


        if (Gridradio.getElementsByTagName('input')[0].checked) {
            if (TextBoxValidationInPopupNotAllowZero(mpeAlert, lblAlertMsg, GridCapmiAmt, "Capmi Amount Should be grater than zero") == false) { return false; }
        }



    //if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, GridMeterRating, "Meter Rating") == false) { return false; }

    //    if (GridNextDate.value != "") {
    //        if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, GridNextDate, "Next Calibration Date") == false) { return false; }
    //        else
    //            if (isDateInPopupForMerterInfo(mpeAlert, lblAlertMsg, GridNextDate.value) == false) {
    //                return false;
    //            }
    //            else {
    //                if (LesserTodayInPopup(mpeAlert, lblAlertMsg, GridNextDate, "Date should be Greater than Today's Date") == false) {
    //                    return false;
    //                }
    //            }
    //    }

    //if (isDateInPopup(mpeAlert, lblAlertMsg, GridNextDate.value) == false) { return false; }

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, GridDials, "Meter Dials") == false) { return false; }
    if (GridDials.value != "") {
        var DialsLength = "9";
        DialsLength = parseInt(DialsLength);
        if (GridDials.value > DialsLength) {
            // alert("Dials limit is " + DialsLength.toString() + ". Please enter valid dials");
            lblAlertMsg.innerHTML = "Dials limit is " + DialsLength.toString() + ". Please enter valid dials";
            mpeAlert.show();
            GridDials.focus();
            return false;
        }
        if (GridDials.value == 0) {
            //alert("Zero not allowing!");
            lblAlertMsg.innerHTML = "Zero not allowing! ";
            mpeAlert.show();
            GridDials.focus();
            return false;
        }
    }
}

function TextBoxValidationInPopupNotAllowZero(popup, lblmsg, Textboxvalues, Field) {
    if (Trim(Textboxvalues.value) == 0) {     
        lblmsg.innerHTML = Field;
        lblmsg.style.display = "block";
        popup.show();
        Textboxvalues.focus();
        return false;
    }
}

function ValidateDate() {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    document.getElementById('spanNextDate').innerHTML = "";
    var txtNextDate = document.getElementById('UMSNigeriaBody_txtNextDate');
    //if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtNextDate, "Next Calibration Date") == false)
    if (TextBoxValidationlbl(txtNextDate, document.getElementById('spanNextDate'), "Next Calibration Date") == false)
        return false;
    else
        if (isDateLbl(txtNextDate.value, txtNextDate, "spanNextDate") == false) {
            return false;
        }
        else {
            if (LesserTodayInPopup(mpeAlert, lblAlertMsg, txtNextDate, "Date should be Greater than Today's Date") == false) {
                return false;
            }
        }
}

function IsCAPMIMeterAmountShow() {
    var rblIsCAPMI = document.getElementById('UMSNigeriaBody_rblIsCAPMI');
    var divCAPMIAmount = document.getElementById('divCAPMIAmount');

    if (rblIsCAPMI.getElementsByTagName('input')[0].checked) {
        divCAPMIAmount.style.display = "block";
    }
    else {
        divCAPMIAmount.style.display = "none";
    }
}

