﻿//        function printGrid() {
//            var printContent = document.getElementById('UMSNigeriaBody_Div1');
//            var uniqueName = new Date();

//            var gridrows = document.getElementById('UMSNigeriaBody_gvMeterReadingList');

//            var windowName = 'Print' + uniqueName.getTime();

//            var printWindow = window.open();
//            printWindow.document.write('<html><head>');
//            printWindow.document.write('</head><body><h3>');
//            printWindow.document.write('Meter Reading Entry List');
//            printWindow.document.write('</h3>');
//            printWindow.document.write("<link href='../css/style.css' rel='stylesheet' type='text/css' />");
//            printWindow.document.write('<link href="../css/reset.css" rel="stylesheet" type="text/css" />');
//            printWindow.document.write(printContent.innerHTML);
//            printWindow.document.write('</body></html>');

//            printWindow.document.close();
//            printWindow.focus();
//            printWindow.print();
//            printWindow.close();

//            return false;

//        }

function IsTamperReasonValidate() {
    var txtIsTamperReason = document.getElementById('UMSNigeriaBody_txtIsTamperReason');
    var IsValid = true;
    if (TextBoxValidationlbl(txtIsTamperReason, document.getElementById('spanIsTamperReason'), "Reason") == false) IsValid = false;
    ClearTamperReading();
    if (!IsValid) {
        return false;
    }
}

function controlEnter(obj, event) {

    var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;

    if (keyCode == 13) {
        document.getElementById(obj).focus();
        return false;
    }
    else {
        return true;
    }
}

function enableRouteWise() {

    var ddlRoutes = document.getElementById('UMSNigeriaBody_ddlRoutes');
    var divAccountNo = document.getElementById('divAccountNo');
    var DivAccount = document.getElementById('divAccountWise');
    var DivRouteType = document.getElementById('divRouteSequenceWise');
    ddlRoutes.style.display = "block";
    divAccountNo.style.display = "none";
    //           DivRouteType.style.display = "block";
    //                     DivAccount.style.display = "none";


}

function enableAccountNo() {
    var divAccountNo = document.getElementById('divAccountNo');
    var ddlRoutes = document.getElementById('UMSNigeriaBody_ddlRoutes');
    var DivAccount = document.getElementById('divAccountWise');
    var DivRouteType = document.getElementById('divRouteSequenceWise');

    divAccountNo.style.display = "block";
    ddlRoutes.style.display = "none";

    //            DivAccount.style.display = "block";
    //            DivRouteType.style.display = "none";
}

function ValidateDate() {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    document.getElementById('spanReadingDate').innerHTML = "";
    var txtReadingDate = document.getElementById('UMSNigeriaBody_txtReadingDate');
    if (TextBoxValidationlbl(txtReadingDate, document.getElementById('spanReadingDate'), "Reading Date") == false)
        return false;
    else
        if (isDateLbl(txtReadingDate.value, txtReadingDate, "spanReadingDate") == false) {
            return false;
        }
        else {
            if (GreaterTodayInPopup(mpeAlert, lblAlertMsg, txtReadingDate, "Reading Date should not be Greater than the Today's Date") == false) {
                return false;
            }
        }
}

function Validate() {
    var radios = document.getElementById('UMSNigeriaBody_rblAccountType');
    var radiosItems = radios.getElementsByTagName("input");
    var txtReadingDate = document.getElementById('UMSNigeriaBody_txtReadingDate');
    var ddlMeterReader = document.getElementById('UMSNigeriaBody_ddlMeterReader');
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');


    var IsValid = true;

    if (RadioButtonListlbl(radios, "spanAccountType", " Read Method ") == false) IsValid = false;
    else {
        var length = 0;
        for (var i = 0, length = radiosItems.length; i < length; i++) {
            if (i == 0 && radiosItems[i].checked) {
                var count = 0;
                document.getElementById('spanAccNum').style.display = "block";

                var txtAccountNumber = document.getElementById('UMSNigeriaBody_txtAccountNumber');
                //                        var txtOldAcountNo = document.getElementById('txtOldAcountNo.ClientID ');
                //                        var txtMeterNo = document.getElementById('txtMeterNo.ClientID ');
                //                        if (TextBoxValidation(txtAccountNumber, document.getElementById('spanAccNum'), " Account Number") == false) {
                if (Trim(txtAccountNumber.value) != "") {
                    count = count + 1;
                }
                //                        if (Trim(txtOldAcountNo.value) != "") {
                //                            count = count + 1;
                //                        }
                //                        if (Trim(txtMeterNo.value) != "") {
                //                            count = count + 1;
                //                        }

                if (count == 0) {
                    document.getElementById('spanAccNum').innerHTML = "Enter Global Account No OR Old Account No OR Meter No";
                    txtAccountNumber.style.border = "1px solid red";
                    //                            txtOldAcountNo.style.border = "1px solid red";
                    //                            txtMeterNo.style.border = "1px solid red";
                    IsValid = false;
                }
                else if (count > 1) {
                    document.getElementById('spanAccNum').innerHTML = "Please Enter any one field only from Global Account No OR Old Account No OR Meter No";
                    txtAccountNumber.style.border = "1px solid red";
                    //                            txtOldAcountNo.style.border = "1px solid red";
                    //                            txtMeterNo.style.border = "1px solid red";
                    IsValid = false;
                }
                else {
                    document.getElementById('spanAccNum').style.display = "none";
                    txtAccountNumber.className = "normalfld";
                    //                            txtOldAcountNo.className = "normalfld";
                    //                            txtMeterNo.className = "normalfld";
                    txtAccountNumber.style.border = "1px solid #8E8E8E";
                    //                            txtOldAcountNo.style.border = "1px solid #8E8E8E";
                    //                            txtMeterNo.style.border = "1px solid #8E8E8E";
                    IsValid = true;
                }
                break;
            }
            else if (i == 1 && radiosItems[i].checked) {

                var ddlBU = document.getElementById('UMSNigeriaBody_ddlBU');
                var ddlSU = document.getElementById('UMSNigeriaBody_ddlSU');
                var ddlSC = document.getElementById('UMSNigeriaBody_ddlSC');
                var ddlCycle = document.getElementById('UMSNigeriaBody_ddlCycle');
                var ddlRoutes = document.getElementById('UMSNigeriaBody_ddlRoutes');
                if (DropDownlistValidationlbl(ddlBU, document.getElementById('spanBU'), " Business Unit ") == false) IsValid = false;
                if (DropDownlistValidationlbl(ddlSU, document.getElementById('spanSU'), " Service Unit ") == false) IsValid = false;
                if (DropDownlistValidationlbl(ddlSC, document.getElementById('spanSC'), " Service Center ") == false) IsValid = false;
                if (DropDownlistValidationlbl(ddlCycle, document.getElementById('spanCycle'), " Book Group ") == false) IsValid = false;
                if (DropDownlistValidationlbl(ddlRoutes, document.getElementById('spanRoute'), " Book Number ") == false) IsValid = false;
                break;
            }
            else if (i == 2 && radiosItems[i].checked) {

                var ddlBU = document.getElementById('UMSNigeriaBody_ddlBU');
                var ddlSU = document.getElementById('UMSNigeriaBody_ddlSU');
                var ddlSC = document.getElementById('UMSNigeriaBody_ddlSC');
                var ddlCycle = document.getElementById('UMSNigeriaBody_ddlCycle');
                var ddlRoutes = document.getElementById('UMSNigeriaBody_ddlRoutes');
                if (DropDownlistValidationlbl(ddlBU, document.getElementById('spanBU'), " Business Unit ") == false) IsValid = false;
                if (DropDownlistValidationlbl(ddlSU, document.getElementById('spanSU'), " Service Unit ") == false) IsValid = false;
                if (DropDownlistValidationlbl(ddlSC, document.getElementById('spanSC'), " Service Center ") == false) IsValid = false;
                if (DropDownlistValidationlbl(ddlCycle, document.getElementById('spanCycle'), " Book Group ") == false) IsValid = false;
                if (DropDownlistValidationlbl(ddlRoutes, document.getElementById('spanRoute'), " Book Number ") == false) IsValid = false;
                break;
            }
        }
    }
    if (TextBoxValidationlbl(txtReadingDate, document.getElementById('spanReadingDate'), " Reading Date") == false) IsValid = false;
    else if (isDateLbl(txtReadingDate.value, txtReadingDate, "spanReadingDate") == false) {
        IsValid = false;
    } else {
        if (GreaterTodayInPopup(mpeAlert, lblAlertMsg, txtReadingDate, "Reading Date should not be Greater than the Today's Date") == false) {
            IsValid = false;
        }
    }

    if (DropDownlistValidationlbl(ddlMeterReader, document.getElementById('spanMeterReader'), " Marketer ") == false) IsValid = false;
    if (!IsValid) {
        return false;
    }

}

/***********Selection change Of Account Type ************/

function AccountChange(selectObj) {

    var radios = document.getElementsByName('ctl00$UMSNigeriaBody$rblAccountType');

    for (var i = 0, length = radios.length; i < length; i++) {
        if (i == 0 && radios[i].checked) {
            enableAccountNo();

        }
        else if (i == 1 && radios[i].checked) {
            enableRouteWise();

        }
    }
}
function isNumberKey(evt, txt) {
    var IsValid = true;
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        IsValid = false;
    if ((charCode == 46 && txt.value.split('.').length > 1)) {
        IsValid = false;
    }
    if (charCode == 8)
        IsValid = true;
    if (IsValid == false)
        return false;
}
function GetUsage(txt) {
    if (txt.value != "") {
        $get("UMSNigeriaBody_lblUsageDetails").innerHTML = parseFloat(txt.value) - parseFloat($get("UMSNigeriaBody_lblPreviousReading").innerHTML);

        if (parseFloat($get("UMSNigeriaBody_lblUsageDetails").innerHTML) < 0) {
            $get("UMSNigeriaBody_lblUsageDetails").style.color = "Red";
        }
        else if (parseFloat($get("UMSNigeriaBody_lblUsageDetails").innerHTML) > 0) {
            $get("UMSNigeriaBody_lblUsageDetails").style.color = "Green";
        }
        else if (parseFloat($get("UMSNigeriaBody_lblUsageDetails").innerHTML) < 0) {
            $get("UMSNigeriaBody_lblUsageDetails").style.color = "Black";
        }
    }
    else {
        $get("UMSNigeriaBody_lblUsageDetails").innerHTML = "";
    }

}
function GetAllNines(Dails) {
    var Str = "";
    for (var i = 0; i < Dails; i++) {
        Str = Str + "9";
    }
    return Str;
}

function GetUsagePopup(txt) {
    if (txt.value != "") {
        $get("UMSNigeriaBody_lblpopGoUsage").innerHTML = parseFloat(txt.value) - parseFloat($get("UMSNigeriaBody_lblpopGoPreviousReading").innerHTML);

        if (parseFloat($get("UMSNigeriaBody_lblpopGoUsage").innerHTML) < 0) {
            $get("UMSNigeriaBody_lblpopGoUsage").style.color = "Red";
        }
        else if (parseFloat($get("UMSNigeriaBody_lblpopGoUsage").innerHTML) > 0) {
            $get("UMSNigeriaBody_lblpopGoUsage").style.color = "Green";
        }
        else if (parseFloat($get("UMSNigeriaBody_lblpopGoUsage").innerHTML) == 0) {
            $get("UMSNigeriaBody_lblpopGoUsage").style.color = "Black";
        }
    }
    else {
        $get("UMSNigeriaBody_lblpopGoUsage").innerHTML = "";
    }

}
function CalculateUsage(lblPreviousReading, lblUsage, txtCurrentReading, Dials) {
    var Dial = document.getElementById(Dials);
    lblPreviousReading = document.getElementById(lblPreviousReading);
    lblUsage = document.getElementById(lblUsage);
    if (txtCurrentReading.value != "") {
        if (lblPreviousReading.innerHTML.substring(0, 1) == "9") {
            var StringValue = GetAllNines(Dial.innerHTML);
            lblUsage.innerHTML = ((parseFloat(StringValue) - parseFloat(lblPreviousReading.innerHTML)) + parseFloat(txtCurrentReading.value) + 1).toString();
            document.getElementById('UMSNigeriaBody_hfIsReset').value = "1";
        }
        else {
            lblUsage.innerHTML = (parseFloat(txtCurrentReading.value) - parseFloat(lblPreviousReading.innerHTML)).toString();
            document.getElementById('UMSNigeriaBody_hfIsReset').value = "0";
        }
    }
    else
        lblUsage.innerHTML = "0";
    return false;
}
function CalculateNewUsage(lblPreviousReading, lblUsage, txtCurrentReading, Dials) {
    var Dial = document.getElementById(Dials);
    var DialValue = "";
    var chkRollOver = document.getElementById('UMSNigeriaBody_chkRollOver');
    if (Dial.tagName == "SPAN")
        DialValue = Dial.innerHTML;
    else
        DialValue = Dial.value;
    lblPreviousReading = document.getElementById(lblPreviousReading);
    lblUsage = document.getElementById(lblUsage);
    if (txtCurrentReading.value != "") {
        //        if (lblPreviousReading.innerHTML.substring(0, 1) == "9" && txtCurrentReading.value.substring(0, 1) == "0") {
        if (chkRollOver.checked) {
            var StringValue = GetAllNines(DialValue);
            lblUsage.innerHTML = ((parseFloat(StringValue) - parseFloat(lblPreviousReading.innerHTML)) + parseFloat(txtCurrentReading.value) + 1).toString();
            document.getElementById('UMSNigeriaBody_hfIsReset').value = "1";
            //                    alert(document.getElementById('UMSNigeriaBody_hfIsReset').value);
        }
        else {
            lblUsage.innerHTML = (parseFloat(txtCurrentReading.value) - parseFloat(lblPreviousReading.innerHTML)).toString();
            document.getElementById('UMSNigeriaBody_hfIsReset').value = "0";
        }
    }
    else
        lblUsage.innerHTML = "0";
    return false;
}
function CalculateNewUsageGV(lblPreviousReading, lblUsage, txtCurrentReading, Dials) {
    var Dial = document.getElementById(Dials);
    var DialValue = "";
    if (Dial.tagName == "SPAN")
        DialValue = Dial.innerHTML;
    else
        DialValue = Dial.value;
    lblPreviousReading = document.getElementById(lblPreviousReading);
    lblUsage = document.getElementById(lblUsage);
    if (txtCurrentReading.value != "") {
        lblUsage.innerHTML = (parseFloat(txtCurrentReading.value) - parseFloat(lblPreviousReading.innerHTML)).toString();
    }
    else
        lblUsage.innerHTML = "0";
    return false;
}
function CalculateNewUsagePop(lblPreviousReading, lblUsage, txtCurrentReading, Dials) {
    var Dial = document.getElementById(Dials);
    var DialValue = "";
    var chkRollOver = document.getElementById('UMSNigeriaBody_chkPopRolleOver');
    if (Dial.tagName == "SPAN")
        DialValue = Dial.innerHTML;
    else
        DialValue = Dial.value;
    lblPreviousReading = document.getElementById(lblPreviousReading);
    lblUsage = document.getElementById(lblUsage);
    if (txtCurrentReading.value != "") {
        if (chkRollOver.checked) {
            var StringValue = GetAllNines(DialValue);
            lblUsage.innerHTML = ((parseFloat(StringValue) - parseFloat(lblPreviousReading.innerHTML)) + parseFloat(txtCurrentReading.value) + 1).toString();
            document.getElementById('UMSNigeriaBody_hfIsResetPopup').value = "1";
        }
        else {
            lblUsage.innerHTML = (parseFloat(txtCurrentReading.value) - parseFloat(lblPreviousReading.innerHTML)).toString();
            document.getElementById('UMSNigeriaBody_hfIsResetPopup').value = "0";
        }
    }
    else
        lblUsage.innerHTML = "0";
    return false;
}
function ValidateRoute() {
    var gvMeterReadingList = document.getElementById('UMSNigeriaBody_gvMeterReadingList');
    var txtCurrentReadings = gvMeterReadingList.getElementsByTagName("input");
    var lblReadingsCount = gvMeterReadingList.getElementsByClassName("lblReadingsCount");
    var lblgvReadingAverage = gvMeterReadingList.getElementsByClassName("lblgvReadingAverage");
    var lblgvUsage = gvMeterReadingList.getElementsByClassName("lblgvUsage");
    var lblgvAccountNos = gvMeterReadingList.getElementsByClassName("lblgvAccountNos");
    var hfRouteAverage = document.getElementById('UMSNigeriaBody_hfRouteAverage');

    var EmptyAccounts = "";
    var NegativeAccounts = "";
    var MaxAvgAccounts = "";

    for (var i = 0; i < txtCurrentReadings.length; i++) {
        if (txtCurrentReadings[i].type = "text" && txtCurrentReadings[i].className == "txtgvCurrentReading") {
            if (Trim(txtCurrentReadings[i].value) == "")
                EmptyAccounts = EmptyAccounts + lblgvAccountNos[i].innerHTML + ",";
            else if (Trim(txtCurrentReadings[i].value) != "") {
                if (parseFloat(lblgvUsage[i].innerHTML) < 0)
                    NegativeAccounts = NegativeAccounts + lblgvAccountNos[i].innerHTML + ",";
                if (
                        (
                        (
                        (parseFloat(lblgvReadingAverage[i].innerHTML) * parseFloat(lblReadingsCount.innerHTML)) + parseFloat(lblgvUsage.innerHTML)
                        )
                        / (parseFloat(lblReadingsCount.innerHTML) + 1)) > parseFloat(hfRouteAverage.value)) {
                    MaxAvgAccounts = MaxAvgAccounts + lblgvAccountNos[i].innerHTML + ",";
                }
            }
        }
    }
    //            alert(EmptyAccounts);
    //            alert(NegativeAccounts);
    //            alert(MaxAvgAccounts);
}
//        } var gvMeterReadingList = document.getElementById('UMSNigeriaBody_gvMeterReadingList');
function AllowNumbers(Dials, Decimals, txt, evt) {

    var IsValid = true;
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        IsValid = false;
    if ((charCode == 46 && txt.value.split('.').length > 1)) {
        IsValid = false;
    }

    var Dial = document.getElementById(Dials);
    var DecimalValue = document.getElementById(Decimals);

    if (txt.value.length >= parseInt(Dial.value)) {

        IsValid = false;
    }
    else if (txt.value.indexOf('.') > -1) {

        var Str = txt.value.split('.');

        if (Str[1].length >= parseInt(DecimalValue.value) || Str[0].length >= (parseInt(Dial.value) - parseInt(DecimalValue.value))) {

            IsValid = false;
        }
    }
    if (charCode == 8)
        IsValid = true;
    if (IsValid == false) {

        return false;
    }
}
function GVAllowNumbers(Dials, Decimals, txt, evt) {
    var IsValid = true;
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        IsValid = false;
    if ((charCode == 46 && txt.value.split('.').length > 1)) {
        IsValid = false;
    }

    var Dial = document.getElementById(Dials);
    var DecimalValue = document.getElementById(Decimals);

    if (txt.value.length >= parseInt(Dial.innerHTML)) {

        IsValid = false;
    }
    else if (txt.value.indexOf('.') > -1) {

        var Str = txt.value.split('.');

        if (Str[1].length >= parseInt(DecimalValue.innerHTML) || Str[0].length >= (parseInt(Dial.innerHTML) - parseInt(DecimalValue.innerHTML))) {

            IsValid = false;
        }
    }
    if (charCode == 8)
        IsValid = true;
    if (IsValid == false) {

        return false;
    }
}
function ValidPopup() {
    var Txt = document.getElementById('UMSNigeriaBody_txtpopGoCurrent');
    var IsValid = true;
    if (TextBoxValidationlbl(Txt, document.getElementById('spanpopGoCurReading'), "Current Reading") == false)
        IsValid = false;
    else/// written by Suresh for not allowing negative values. 
        if (parseFloat($get("UMSNigeriaBody_lblpopGoUsage").innerHTML) < 0) {
            var span = document.getElementById('spanpopGoCurReading');
            span.style.display = "block";
            span.innerHTML = "Enter Valid Current Reading";
            Txt.focus();
            IsValid = false;
        }

    if (IsValid == false)
        return false;

}

function ValidSaveReading() {
    var Txt = document.getElementById('UMSNigeriaBody_txtCurrentReading');
    var span = document.getElementById('spanCurrentReading');
    var lblUsageDetails = document.getElementById('UMSNigeriaBody_lblUsageDetails');
    //    var chkRollOver = document.getElementById('UMSNigeriaBody_chkRollOver');
    //    if (!chkRollOver.checked) {
    if (Trim(Txt.value) == "") {
        span.innerHTML = "Enter Current Reading";
        span.style.display = "block";
        Txt.focus();
        return false;
    }
    else if (parseFloat(lblUsageDetails.innerHTML) < 0) {
        span.innerHTML = "Enter Valid Reading";
        span.style.display = "block";
        Txt.focus();
        return false;
    }
    //    }
}

function GreaterToday(TDate) {
    var now = new Date();
    var entDate = TDate.value.split('/');
    var xdate = entDate[1] + '/' + entDate[0] + '/' + entDate[2];

    var date2 = new Date(xdate);
    if (now <= date2) {
        alert("Date should not be Greater than Current Date");
        return false;
    }
}

function AuthenticationLogin(txt, spanvalue) {
    var pwd = document.getElementById('UMSNigeriaBody_txtPassword');
    var spanauth = document.getElementById('UMSNigeriaBody_spanAuthentication');
    var isValid = true;
    var popup = $find('UMSNigeriaBody_ModalPopupExtenderLogin');

    if (TextBoxValidationlbl(pwd, spanauth, "Password") == false) IsValid = false;
    if (isValid == false) {

        popup.show();
        return false;
    }
}
//      Change TextBoxColorOnFocus
function DisableGrid() {
    var divRouteSequenceWise = document.getElementById('UMSNigeriaBody_divRouteSequenceWise');
    var divAccountWise = document.getElementById('UMSNigeriaBody_divAccountWise');
    if (divAccountWise)
        divAccountWise.style.display = "none";
    if (divRouteSequenceWise)
        divRouteSequenceWise.style.display = "none";
}
function DoBlur(fld) {
    fld.className = 'text-box hasDatepicker';
    ValidateDate();
}

function DoFocus(fld) {
    fld.className = 'text-box hasDatepicker';
}
//Default button in a division
function WebForm_FireDefaultButton(event, target) {
    if (event.keyCode == 13 && !(event.srcElement && (event.srcElement.tagName.toLowerCase() == "textarea"))) {
        var defaultButton;
        if (__nonMSDOMBrowser) {
            defaultButton = document.getElementById(target);
        }
        else {
            defaultButton = document.all[target];
        }
        if (defaultButton && typeof (defaultButton.click) != "undefined") {
            defaultButton.click();
            event.cancelBubble = true;
            if (event.stopPropagation) event.stopPropagation();
            return false;
        }
    }
    return true;
}


function ValidateRouteOrBookWise() {
    var gvMeterReadingList = document.getElementById('UMSNigeriaBody_gvMeterReadingList');
    var txtCurrentReadings = gvMeterReadingList.getElementsByTagName("input");
    if (txtCurrentReadings != null) {
        for (var i = 0; i < txtCurrentReadings.length; i++) {
            if (txtCurrentReadings[i].type == "text") {
                if (Trim(txtCurrentReadings[i].value) != '') {
                    if (txtCurrentReadings[i].textLength != txtCurrentReadings[i].maxLength) {
                        var lblInvalidUsageHeader = document.getElementById('UMSNigeriaBody_lblHeaderInvalid');
                        var lblInvalidUsageBody = document.getElementById('UMSNigeriaBody_lblInvalidBody');
                        var mpeInvalidLatest = $find('UMSNigeriaBody_ModalPopupExtenderInvalidAccount');
                        lblInvalidUsageHeader.innerHTML = "Invalid Usage Details";
                        lblInvalidUsageBody.innerHTML = "Please enter " + txtCurrentReadings[i].maxLength + " Digits valid reading.";
                        txtCurrentReadings[i].focus();
                        mpeInvalidLatest.show();
                        return false;
                        break;
                    }
                }
            }
        }
    }
}

function CalculateNewUsageTemp(lblPreviousReading, lblUsage, txtCurrentReading, Dials) {
    var chkRollOver = document.getElementById('UMSNigeriaBody_chkRollOver');
    var Dial = document.getElementById(Dials);
    var IsRollOverchecked = true;
    var DialValue = "";
    if (Dial.tagName == "SPAN")
        DialValue = Dial.innerHTML;
    else
        DialValue = Dial.value;
    lblPreviousReading = document.getElementById(lblPreviousReading);
    lblUsage = document.getElementById(lblUsage);
    if (txtCurrentReading.value != "") {
            if (chkRollOver.checked) {          //If Rollover checked
                if (lblPreviousReading.innerHTML.substring(0, 1) == "9") IsRollOverchecked = true;
                else IsRollOverchecked = false;
            }
            if (IsRollOverchecked) {
                //        if (lblPreviousReading.innerHTML.substring(0, 1) == "9" && txtCurrentReading.value.substring(0, 1) == "0") {
                var StringValue = GetAllNines(DialValue);
                lblUsage.innerHTML = ((parseFloat(StringValue) - parseFloat(lblPreviousReading.innerHTML)) + parseFloat(txtCurrentReading.value) + 1).toString();
                document.getElementById('UMSNigeriaBody_hfIsReset').value = "1";
                //                    alert(document.getElementById('UMSNigeriaBody_hfIsReset').value);
            }
            else {
                lblUsage.innerHTML = (parseFloat(txtCurrentReading.value) - parseFloat(lblPreviousReading.innerHTML)).toString();
                document.getElementById('UMSNigeriaBody_hfIsReset').value = "0";
            }
    }
    else
        lblUsage.innerHTML = "0";
    return false;
}

function CalculateNewUsageChecked() {
    //    var lblPreviousReading var lblUsage, var txtCurrentReading, var Dials
    //    alert("Working");
    var Dial = document.getElementById('UMSNigeriaBody_hfDial');
    var DialValue = "";
    var chkRollOver = document.getElementById('UMSNigeriaBody_chkRollOver');
    if (Dial.tagName == "SPAN")
        DialValue = Dial.innerHTML;
    else
        DialValue = Dial.value;
    lblPreviousReading = document.getElementById('UMSNigeriaBody_lblPreviousReading');
    lblUsage = document.getElementById('UMSNigeriaBody_lblUsageDetails');
    var txtCurrentReading = document.getElementById('UMSNigeriaBody_txtCurrentReading');
    if (txtCurrentReading.value != "") {
        //        if (lblPreviousReading.innerHTML.substring(0, 1) == "9" && txtCurrentReading.value.substring(0, 1) == "0") {
        if (chkRollOver.checked) {
            var StringValue = GetAllNines(DialValue);
            lblUsage.innerHTML = ((parseFloat(StringValue) - parseFloat(lblPreviousReading.innerHTML)) + parseFloat(txtCurrentReading.value) + 1).toString();
            document.getElementById('UMSNigeriaBody_hfIsReset').value = "1";
            //                    alert(document.getElementById('UMSNigeriaBody_hfIsReset').value);
        }
        else {
            lblUsage.innerHTML = (parseFloat(txtCurrentReading.value) - parseFloat(lblPreviousReading.innerHTML)).toString();
            document.getElementById('UMSNigeriaBody_hfIsReset').value = "0";
        }
    }
    else
        lblUsage.innerHTML = "0";
    return false;
}

function CalculateNewUsagePopupChecked() {
    //    var lblPreviousReading var lblUsage, var txtCurrentReading, var Dials
    //    alert("Working");
    var Dial = document.getElementById('UMSNigeriaBody_hfDial');
    var DialValue = "";
    var chkRollOver = document.getElementById('UMSNigeriaBody_chkPopRolleOver');
    if (Dial.tagName == "SPAN")
        DialValue = Dial.innerHTML;
    else
        DialValue = Dial.value;
    lblPreviousReading = document.getElementById('UMSNigeriaBody_lblpopGoPreviousReading');
    lblUsage = document.getElementById('UMSNigeriaBody_lblpopGoUsage');
    var txtpopGoCurrent = document.getElementById('UMSNigeriaBody_txtpopGoCurrent');
    if (txtpopGoCurrent.value != "") {
        //        if (lblPreviousReading.innerHTML.substring(0, 1) == "9" && txtCurrentReading.value.substring(0, 1) == "0") {
        if (chkRollOver.checked) {
            var StringValue = GetAllNines(DialValue);
            lblUsage.innerHTML = ((parseFloat(StringValue) - parseFloat(lblPreviousReading.innerHTML)) + parseFloat(txtpopGoCurrent.value) + 1).toString();
            document.getElementById('UMSNigeriaBody_hfIsResetPopup').value = "1";
            //                    alert(document.getElementById('UMSNigeriaBody_hfIsReset').value);
        }
        else {
            lblUsage.innerHTML = (parseFloat(txtpopGoCurrent.value) - parseFloat(lblPreviousReading.innerHTML)).toString();
            document.getElementById('UMSNigeriaBody_hfIsResetPopup').value = "0";
        }
    }
    else
        lblUsage.innerHTML = "0";
    return false;
}

function ClearTamperReading() {
    document.getElementById('UMSNigeriaBody_txtCurrentReading').value = "";
    document.getElementById('UMSNigeriaBody_txtpopGoCurrent').value = "";
}

function Routes_IndexCHanged(ddl, lblId, field) {
    if (DropDownlistOnChangelbl(ddl, lblId, field) == false) {
        return false;
    }
    else {
        document.getElementById('UMSNigeriaBody_btnRoutesDummy').click();
    }
}