﻿function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');


    $(function () {
        document.getElementById('UMSNigeriaBody_txtNotes').setAttribute("MaxLength", "200");
    });

    $(document).ready(function () {
        DisplayMessage('UMSNigeriaBody_pnlMessage');
        Calendar('UMSNigeriaBody_txtBatchDate', 'UMSNigeriaBody_imgDate', "-2:+8");
        //AutoComplete($('#UMSNigeriaBody_txtBatchNo'), 4);
    });
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(function () {
        Calendar('UMSNigeriaBody_txtBatchDate', 'UMSNigeriaBody_imgDate', "-2:+8");
        //AutoComplete($('#UMSNigeriaBody_txtBatchNo'), 4);
    });
};



function Validate() {
    var IsValid = true;
    var mpegridalert = $find('UMSNigeriaBody_mpegridalert');
    var lblgridalertmsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var BatchDate = document.getElementById('UMSNigeriaBody_txtBatchDate');
    var BatchNo = document.getElementById('UMSNigeriaBody_txtBatchNoNew');
    if (TextBoxValidationlbl(BatchNo, document.getElementById('spanBatchNo'), " Batch No") == false) IsValid = false;
    if (TextBoxValidationlbl(BatchDate, document.getElementById('spanBatchDate'), " Batch Date") == false) IsValid = false;
    else if (isDateLbl(BatchDate.value, BatchDate, "spanBatchDate") == false) IsValid = false;
    else if (GreaterTodayInPopup(mpegridalert, lblgridalertmsg, BatchDate, "Batch Date should not be Greater than the Today's Date") == false) {
        return false;
    }
    if (!IsValid) {
        return false;
    }

    var fupExcel = document.getElementById('UMSNigeriaBody_upDOCExcel');
    if (OnUploadpopup(mpegridalert, lblgridalertmsg, fupExcel, ['xls', 'xlsx'], " Excel File") == false) IsValid = false;
    //if (MinimumLengthValidation(BatchNo, document.getElementById('spanBatchNo'), 6, 'Batch No.') == false) IsValid = false;
    return IsValid;
}



function ValidateDate() {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    document.getElementById('spanBatchDate').innerHTML = "";
    var txtBatchDate = document.getElementById('UMSNigeriaBody_txtBatchDate');
    if (TextBoxValidationlbl(txtBatchDate, document.getElementById('spanBatchDate'), "Batch Date") == false)
        return false;
    else
        if (isDateLbl(txtBatchDate.value, txtBatchDate, "spanBatchDate") == false) {
            return false;
        }
        else {
            if (GreaterTodayInPopup(mpeAlert, lblAlertMsg, txtBatchDate, "Batch Date should not be Greater than the Today's Date") == false) {
                return false;
            }
        }
}
function DoBlur(fld) {
    ValidateDate();
}

function ShowLoading(value) {
    var pnlLoadingUpload = document.getElementById('UMSNigeriaBody_pnlLoadingUpload');
    if (value == "1") {
        pnlLoadingUpload.style.display = "block";
    }
    else {
        pnlLoadingUpload.style.display = "none";
    }
}

function ValidateUpload() {
    var mpegridalert = $find('UMSNigeriaBody_mpegridalert');
    var lblgridalertmsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var fupExcel = document.getElementById('UMSNigeriaBody_fupUploadTransactions');
    if (OnUploadpopup(mpegridalert, lblgridalertmsg, fupExcel, ['xls', 'xlsx'], " Excel File") == false)
        return false;
}