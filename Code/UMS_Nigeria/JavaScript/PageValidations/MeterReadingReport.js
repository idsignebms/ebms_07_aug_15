﻿function CheckAll(cbAllId, cblId) {
    var cbAll = document.getElementById(cbAllId);
    var chkBoxList = document.getElementById(cblId);
    CheckBoxListSelect(cbAll, chkBoxList);
}
function UnCheckAll(cbAllId, cblId) {
    var chkBoxList = document.getElementById(cblId);
    var cbAll = document.getElementById(cbAllId);
    Uncheck(cbAll, chkBoxList);
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};

function Validate() {
    var txtFromDate = document.getElementById('UMSNigeriaBody_txtFromDate');
    var txtToDate = document.getElementById('UMSNigeriaBody_txtToDate');
    var mpeAlert = $find('UMSNigeriaBody_mpeAlert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblAlertMsg');
    
    var IsValid = true;

    if (TextBoxValidationlbl(txtFromDate, document.getElementById("spanFromDate"), "From Date") == false) IsValid = false;

    if (isDateLbl(txtFromDate.value, txtFromDate, "spanFromDate") == false) IsValid = false;

    if (TextBoxValidationlbl(txtToDate, document.getElementById("spanToDate"), "To Date") == false) IsValid = false;

    if (isDateLbl(txtToDate.value, txtToDate, "spanToDate") == false) IsValid = false;

    if (IsValid) {
        if (GreaterTodayInPopup(mpeAlert, lblAlertMsg, txtFromDate, "From Date should not be Greater than the Today's Date") == false) return false;
        if (GreaterTodayInPopup(mpeAlert, lblAlertMsg, txtToDate, "To Date should not be Greater than the Today's Date") == false) return false;
        if (FromToDateInPopup(mpeAlert, lblAlertMsg, txtFromDate.value, txtToDate.value, 'To date should be greater than the From date') == false) return false;
        if (Validate30DaysInPopup(mpeAlert, lblAlertMsg, txtFromDate.value, txtToDate.value, 'Difference between From date and To date should be maximum 30 days only') == false) return false;
    }
    else {
        return false;
    }
}