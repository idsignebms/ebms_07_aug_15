﻿function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
    $get("UMSNigeriaBody_txtPassword").focus();
    $get("UMSNigeriaBody_txtPassword").attr('autocomplete', 'off');
}

function Validate() {
    var IsValid = true;
    var popup = $find('UMSNigeriaBody_mpePaymentypopup');
    var lblmsg = document.getElementById('UMSNigeriaBody_lblMsgPopup');
    var fupExcel = document.getElementById('UMSNigeriaBody_upDOCExcel');
    if (OnUploadpopup(popup, lblmsg, fupExcel, ['xls', 'xlsx'], " Excel File") == false)
        return false;
}


function AuthenticationLogin(txt, spanvalue) {
    var pwd = document.getElementById('UMSNigeriaBody_txtPassword');
    var spanauth = document.getElementById('UMSNigeriaBody_spanAuthentication');
    var isValid = true;
    var popup = $find('UMSNigeriaBody_ModalPopupExtenderLogin');

    if (TextBoxValidationlbl(pwd, spanauth, "Password") == false) IsValid = false;
    if (isValid == false) {

        popup.show();
        return false;
    }
}