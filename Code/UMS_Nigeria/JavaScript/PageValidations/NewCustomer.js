﻿function AddIdentityDiv() {
    if (IdentityValidate() == true) {
        var hfTotalIdentity = document.getElementById("UMSNigeriaBody_hfTotalIdentity").value;
        var index = document.getElementById("UMSNigeriaBody_hfCurrentIdentity").value;
        index++;
        if (index > 1) {
            if (index <= hfTotalIdentity) {

                var div = document.createElement('DIV');
                div.innerHTML = '<div class="inner-box1">'
                                    + '<div class="text-inner">'
                                        + '<label for="name">'
                                          + '  Type<span class="span_star">*</span></label></br>'
                                        + '<select name="ctl00$UMSNigeriaBody$ddlIdentityType" id="UMSNigeriaBody_ddlIdentityType' + index + '"'
                                            + 'class="text-box text-select identityIDClass" onchange="return DropDownlistOnChangelbl(this,&#39;spanIdentityType&#39;,&#39;Identity Type&#39;)">'
                                            + '<option value="">--Select--</option>'
                                            + '<option value="1">License</option>'
                                            + '<option value="3">PanCard</option>'
                                          + '  <option value="2">Passport</option>'
                                        + '</select>'
                                        + '<div class="space">'
                                        + '</div>'
                                      + '  <span id="span3" class="span_color"></span>'
                                    + '</div>'
                                + '</div>'
                                 + '<div class="inner-box2">'
                                    + '<div class="text-inner">'
                                        + '<label for="name">Number<span class="span_star">*</span></label></br>'
                                        + '<input name="ctl00$UMSNigeriaBody$txtIdentityNo" type="text" id="UMSNigeriaBody_txtIdentityNo" class="text-box identityNumClass" placeholder="Enter Identity No" maxlength="100" onblur="return TextBoxBlurValidationlbl(this,&#39;spanIdentityNo&#39;,&#39;Identity No&#39;)" onfocus="DoFocus(this);" />'
                                        + '<div class="space">'
                                        + '</div>'
                                        + '<span id="span4" class="span_color"></span>'
                                    + '</div>'
                                + '</div>'
                                + '<a onclick="return RemoveIdentity(this);" class="removeLink" id="UMSNigeriaBody_linkRemoveTest" href="javascript:__doPostBack(&#39;ctl00$UMSNigeriaBody$linkRemoveTest&#39;,&#39;&#39;)"><img src="../images/cancel.png" alt="Remove" /></a>'
                               + '<div class="clr"></div>';
                document.getElementById("divIdentity").appendChild(div);
                document.getElementById("UMSNigeriaBody_hfCurrentIdentity").value = index;
            }
            else {
                document.getElementById('UMSNigeriaBody_lblMessageValidation').innerHTML = "Only " + parseInt(hfTotalIdentity) + " identity allowed.";
                ShowMessage();
                //                alert("Only " + parseInt(hfTotalIdentity) + " identity allowed.");
            }
        }
        else {
            //            alert("Cannot add new identity.");
            document.getElementById('UMSNigeriaBody_lblMessageValidation').innerHTML = "Cannot add new identity.";
            ShowMessage();
        }
    }

    return false;
}
function RemoveIdentity(file) {
    document.getElementById("divIdentity").removeChild(file.parentNode);
    document.getElementById("UMSNigeriaBody_hfCurrentIdentity").value = document.getElementById("UMSNigeriaBody_hfCurrentIdentity").value - 1;
    return false;
}
function GetIdentityFields() {
    debugger;
    var divIdentityInfo = document.getElementById("divIdentityInfo");
    var identityIDClass = divIdentityInfo.getElementsByTagName("select");
    var identityNumberClass = divIdentityInfo.getElementsByTagName("input");
    var IdentityID = "";
    var IdentityNumbers = "";
    var IsVld = true;
    var message = "";
    for (var idnNo = 0; idnNo <= identityIDClass.length - 1; idnNo++) {
        if (identityIDClass[idnNo].value > 0) {
            IdentityID = IdentityID + identityIDClass[idnNo].value + ",";
            IdentityID = IdentityID.replace(/,/g, "") + "|";
        }
        else {
            IsVld = false;
            message = "Please select identity.";
            break;
        }
    }
    document.getElementById("UMSNigeriaBody_hfIdentityIdList").value = IdentityID;
    for (var idnNum = 0; idnNum <= identityNumberClass.length - 1; idnNum++) {
        if (identityNumberClass[idnNum].value.length > 0) {
            IdentityNumbers = IdentityNumbers + identityNumberClass[idnNum].value + ",";
            IdentityNumbers = IdentityNumbers.replace(/,/g, "") + "|";
        }
        else {
            IsVld = false;
            message = message + "\nPlease enter identity number.";
            break;
        }

    }
    if (IsVld == true) {

        if (IdentityValidate() == true) {
            document.getElementById("UMSNigeriaBody_hfIdentityNumberList").value = IdentityNumbers;
            document.getElementById("UMSNigeriaBody_hfCurrentIdentity").value = 0;
        }
        else {
            IsVld = false;
        }
    }
    else {
        document.getElementById('UMSNigeriaBody_lblMessageValidation').innerHTML = message;
        ShowMessage()
    }

    return IsVld;
}
function OnOffTenentDiv() {
    //            alert("Workign");
    if (!document.getElementById("UMSNigeriaBody_cbIsTenent").checked)
        document.getElementById("divTenent").style.display = "none";
    else if (document.getElementById("UMSNigeriaBody_cbIsTenent").checked)
        document.getElementById("divTenent").style.display = "inherit";
}
function OnOfIsSameAsServiceAddress() {
    if (document.getElementById("UMSNigeriaBody_cbIsSameAsPostal").checked) {
        document.getElementById("UMSNigeriaBody_txtSHNo").value = document.getElementById("UMSNigeriaBody_txtPHNo").value;
        document.getElementById("UMSNigeriaBody_txtSStreet").value = document.getElementById("UMSNigeriaBody_txtPStreet").value;
        document.getElementById("UMSNigeriaBody_txtSLGAVillage").value = document.getElementById("UMSNigeriaBody_txtPLGAVillage").value;
        document.getElementById("UMSNigeriaBody_txtSZip").value = document.getElementById("UMSNigeriaBody_txtPZip").value;
        document.getElementById("UMSNigeriaBody_txtSArea").value = document.getElementById("UMSNigeriaBody_txtPArea").value;
        var chkBoxList = document.getElementById("UMSNigeriaBody_rblCommunicationAddress");
        var chkBoxCount = chkBoxList.getElementsByTagName("input")[0].checked = true;
        var chkBoxCount = chkBoxList.getElementsByTagName("input")[1].disabled = true;
        DisableServiceAddress(true);
    }
    else if (!document.getElementById("UMSNigeriaBody_cbIsSameAsPostal").checked) {
        document.getElementById("UMSNigeriaBody_txtSHNo").value = "";
        document.getElementById("UMSNigeriaBody_txtSStreet").value = "";
        document.getElementById("UMSNigeriaBody_txtSLGAVillage").value = "";
        document.getElementById("UMSNigeriaBody_txtSZip").value = "";
        document.getElementById("UMSNigeriaBody_txtSArea").value = "";
        var chkBoxList = document.getElementById("UMSNigeriaBody_rblCommunicationAddress");
        var chkBoxCount = chkBoxList.getElementsByTagName("input")[0].checked = false;
        var chkBoxCount = chkBoxList.getElementsByTagName("input")[1].checked = false;
        var chkBoxCount = chkBoxList.getElementsByTagName("input")[1].disabled = false;
        DisableServiceAddress(false);
    }
    return false;
}

function DisableServiceAddress(IsEnabledServiceAddress) {
    document.getElementById("UMSNigeriaBody_txtSHNo").disabled = IsEnabledServiceAddress;
    document.getElementById("UMSNigeriaBody_txtSStreet").disabled = IsEnabledServiceAddress;
    document.getElementById("UMSNigeriaBody_txtSLGAVillage").disabled = IsEnabledServiceAddress;
    document.getElementById("UMSNigeriaBody_txtSZip").disabled = IsEnabledServiceAddress;
    document.getElementById("UMSNigeriaBody_txtSArea").disabled = IsEnabledServiceAddress;
    document.getElementById("UMSNigeriaBody_rblCommunicationAddress").disabled = IsEnabledServiceAddress;
}

function OnOffIsEmployee() {
    if (!document.getElementById("UMSNigeriaBody_cbIsBEDCEmployee").checked)
        document.getElementById("divEmployeeCode").style.display = "none";
    else if (document.getElementById("UMSNigeriaBody_cbIsBEDCEmployee").checked)
        document.getElementById("divEmployeeCode").style.display = "inherit";
}
function OnOffIsEmbessy() {
    if (!document.getElementById("UMSNigeriaBody_cbIsEmbassy").checked)
        document.getElementById("divEmbessy").style.display = "none";
    else if (document.getElementById("UMSNigeriaBody_cbIsEmbassy").checked)
        document.getElementById("divEmbessy").style.display = "inherit";
}
function IfReadedCustomer() {
    var e = document.getElementById("UMSNigeriaBody_ddlReadCode");
    var ReadText = e.options[e.selectedIndex].text;
    if (ReadText == 'Read') {
        document.getElementById("divReadCustomer").style.display = "block";
        document.getElementById("lblIsCapmi").style.display = "none";
//        $("#UMSNigeriaBody_txtMeterNo").focus();
        document.getElementById("UMSNigeriaBody_lnkSearchMeterNo").focus();
        //                document.getElementById("UMSNigeriaBody_divMeterNo").style.display = "inherit";
        //                document.getElementById("UMSNigeriaBody_divIsCapmy").style.display = "inherit";
        //                document.getElementById("UMSNigeriaBody_divAmount").style.display = "inherit";
    }
    else {
        document.getElementById("divReadCustomer").style.display = "none";
        document.getElementById("UMSNigeriaBody_txtAmount").value = "";
        document.getElementById("UMSNigeriaBody_txtMeterNo").value = "";
        document.getElementById("UMSNigeriaBody_txtPresentReading").value = "";
        document.getElementById("divAmount").style.display = "none";
        //                document.getElementById("UMSNigeriaBody_divMeterNo").style.display = "none";
        //                document.getElementById("UMSNigeriaBody_divIsCapmy").style.display = "none";
        //                document.getElementById("UMSNigeriaBody_divAmount").style.display = "none";
    }
}
function OnOffIsCammy() {
    //            alert(document.getElementById("UMSNigeriaBody_cbIsCAPMY").checked);
    if (!document.getElementById("UMSNigeriaBody_cbIsCAPMY").checked) {
        document.getElementById("UMSNigeriaBody_txtAmount").value = "";
        document.getElementById("divAmount").style.display = "none";
    }
    else if (document.getElementById("UMSNigeriaBody_cbIsCAPMY").checked) {
        document.getElementById("divAmount").style.display = "block";
    }
}
function OnOffAppProcessBy() {
    var ProcessedBy = document.getElementById("UMSNigeriaBody_rblApplicationProcessedBy");
    var ProcessedByList = ProcessedBy.getElementsByTagName("input");
    for (var i = 0; i < ProcessedByList.length; i++) {
        if (ProcessedByList[i].checked) {
            switch (i) {
                case 0:
                    document.getElementById("divInstalledBy").style.display = "block";
                    document.getElementById("divAgencyName").style.display = "none";
                    break;
                case 1:
                    document.getElementById("divInstalledBy").style.display = "none";
                    document.getElementById("divAgencyName").style.display = "block";
                    break;
            }
        }
    }
}
function ValidatePole() {
    var IsValid = true;
    if (DropDownlistValidationlbl(document.getElementById("UMSNigeriaBody_ddlPoleParent"), document.getElementById("spnddlPoleParent"), "TCN") == false) IsValid = false;
    if (DropDownlistValidationlbl(document.getElementById("UMSNigeriaBody_dllPoleLevelChild1"), document.getElementById("spndllPoleLevelChild1"), "TCN Power TR") == false) IsValid = false;
    if (DropDownlistValidationlbl(document.getElementById("UMSNigeriaBody_dllPoleLevelChild2"), document.getElementById("spndllPoleLevelChild2"), "33 KV Feeder") == false) IsValid = false;
    if (DropDownlistValidationlbl(document.getElementById("UMSNigeriaBody_dllPoleLevelChild3"), document.getElementById("spndllPoleLevelChild3"), "33/11 Inj. S/S") == false) IsValid = false;
    if (DropDownlistValidationlbl(document.getElementById("UMSNigeriaBody_dllPoleLevelChild4"), document.getElementById("spndllPoleLevelChild4"), "33/11 Power TR") == false) IsValid = false;
    if (DropDownlistValidationlbl(document.getElementById("UMSNigeriaBody_dllPoleLevelChild5"), document.getElementById("spndllPoleLevelChild5"), "11 KV Feeder") == false) IsValid = false;
    if (DropDownlistValidationlbl(document.getElementById("UMSNigeriaBody_dllPoleLevelChild6"), document.getElementById("spndllPoleLevelChild6"), "11/0.45KV TR") == false) IsValid = false;
    if (DropDownlistValidationlbl(document.getElementById("UMSNigeriaBody_dllPoleLevelChild7"), document.getElementById("spndllPoleLevelChild7"), "LV Upraiser") == false) IsValid = false;
    if (DropDownlistValidationlbl(document.getElementById("UMSNigeriaBody_dllPoleLevelChild8"), document.getElementById("spndllPoleLevelChild8"), "Pole No") == false) IsValid = false;
    return IsValid;
}
function MeterPopup() {
    var mpee = $find('UMSNigeriaBody_mpeMeterList');
    mpee.show();
    return false;
}
//function DoBlur(fld) {
//    fld.className = 'text-box hasDatepicker';
//}

//function DoFocus(fld) {
//    fld.className = 'text-box hasDatepicker';
//}

function ValidateDate(fld, spanId, spanText) {
    document.getElementById(spanId).innerHTML = "";
    var ApplicationDate = document.getElementById('UMSNigeriaBody_txtApplicationDate');
    var ConnectionDate = document.getElementById('UMSNigeriaBody_txtConnectionDate');
    var SetupDate = document.getElementById('UMSNigeriaBody_txtSetupDate');
    /*if (TextBoxValidationlbl(fld, document.getElementById(spanId), spanText) == false) {
        return false;
    }
    else */
    if (isDateLbl(fld.value, fld, spanId) == false) {
        return false;
    }
    else {
        var mpeAlert = $find('UMSNigeriaBody_mpeMessage');
        var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblMessageValidation');
        //Date should not be greater than today's date.
        if (GreaterTodayInPopup(mpeAlert, lblAlertMsg, fld, spanText + " should not be Greater than the Today's Date") == false) {
            return false;
        }
        //Connection Date Should be less then Setup Date
        if (fld.id == ApplicationDate.id && ConnectionDate.value != '') {
            if (CompareDates(ApplicationDate, ConnectionDate, "Application Date Should be less then connection Date") == false) {
                fld.value = '';
                fld.style.border = "1px solid red";
                return false;
            }
        }
        //Connection Date Should be less then Setup Date
        if (fld.id == ConnectionDate.id && ApplicationDate.value != '') {
            if (CompareDates(ApplicationDate, ConnectionDate, "Application Date Should be less then connection Date") == false) {
                fld.value = '';
                fld.style.border = "1px solid red";
                return false;
            }
        }
        //Connection Date Should be less then Setup Date
        if (fld.id == ConnectionDate.id && SetupDate.value != '') {
            if (CompareDates(ConnectionDate, SetupDate, "Connection Date Should be less then Setup Date") == false) {
                fld.value = '';
                fld.style.border = "1px solid red";
                return false;
            }
        }
        //Connection Date Should be less then Setup Date
        if (fld.id == SetupDate.id && ConnectionDate.value != '') {
            if (CompareDates(ConnectionDate, SetupDate, "Connection Date Should be less then Setup Date") == false) {
                fld.value = '';
                fld.style.border = "1px solid red";
                return false;
            }
        }
        if (fld.id == SetupDate.id && ApplicationDate.value != '') {
            if (CompareDates(ApplicationDate, SetupDate, "Application Date Should be less then Setup Date") == false) {
                fld.value = '';
                fld.style.border = "1px solid red";
                return false;
            }
        }
    }
}


function DoBlur(fld, spanId, spanText) {
    var SpanText = spanId;
    ValidateDate(fld, spanId, spanText);
}

//        if (TextBoxValidationlbl(txtPZip, document.getElementById("spanPZip"), "Zip Code") == false) {
//            IsValid = false;
//        }
//        else if (TextboxZipCodelbl(txtPZip, document.getElementById("spanPZip"), 6) == false) {
//            IsValid = false;
//        }

function AddFile() {
    var divFileCount = document.getElementById('divFile').childNodes.length;
    var searchEles = document.getElementById('divFile').getElementsByTagName("input");
    for (var i = 0; i < searchEles.length; i++) {
        if (searchEles[i].tagName == 'INPUT') {
            if (!(IsCurrentFileValid(searchEles[i]))) {
                document.getElementById('UMSNigeriaBody_lblMessageValidation').innerHTML = "Please select .jpeg, .jpg, .png, .gif, .doc, .docx and .pdf File Formats";
                ShowMessage();
                searchEles[i].focus();
                return false;
            }
        }
    }    
    var i = 1;
    //            if (FinalValidate()) {
    i++;
    

    var div = document.createElement('DIV');
    div.innerHTML = //'<div id="div1" class="fltl">'
                                '<input type="file" name="ctl00$UMSNigeriaBody$fupDocument" id="UMSNigeriaBody_fupDocument' + divFileCount + '" onchange="return filevalidate(this,this.value,\'Photo\')" class="text-box updateV" />'
                                + '<a onclick="return RemoveFile(this);" id="UMSNigeriaBody_lnkRemovefile" href="javascript:__doPostBack(&#39;ctl00$UMSNigeriaBody$lnkRemoveIdentityFields&#39;,&#39;&#39;)"><img src="../images/cancel.png" alt="Remove" /></a>'
    //+ '</div>';

    document.getElementById("divFile").appendChild(div);
    return false;
    //            }
    //            else
    //                alert("Please enter mandatory fields.");
}

function IsCurrentFileValid(fup) {
    if (!(filevalidate(fup, fup.value, 'Photo'))) {
        return false;
    }
    else {
        return true;
    }
}

function RemoveFile(file) {
    document.getElementById("divFile").removeChild(file.parentNode);
    return false;
}

function SearchBookFocus() {
    var ddl = document.getElementById('UMSNigeriaBody_ddlServiceUnitName');
    ddl.focus();
}

function IdentityValidate() {
    var divIdentityInfo = document.getElementById("divIdentityInfo");
    var identityIDClass = divIdentityInfo.getElementsByTagName("select");
    var identityNumberClass = divIdentityInfo.getElementsByTagName("input");
    var status = true;
    var isMessaged = false;
    if ((identityIDClass[identityIDClass.length - 1].value != "") || (identityIDClass[identityIDClass.length - 1].value != 0)) {
        var identityLast = null;
        var identityNumLast = null;
        var Message = "";

        if (identityIDClass.length > 1) {
            identityLast = identityIDClass[identityIDClass.length - 1].value;
            for (var ValueIndex = 0; ValueIndex < identityIDClass.length - 1; ValueIndex++) {
                if (identityLast == identityIDClass[ValueIndex].value) {
                    status = false;
                    isMessaged = true;
                    Message = "Identity type cannot be same.\n";
                    break;
                }
            }
        }
        if (status == false) {
            document.getElementById('UMSNigeriaBody_lblMessageValidation').innerHTML = Message;
            ShowMessage()
        }
    }
    if (((identityNumberClass[identityIDClass.length - 1].value != "") || (identityNumberClass[identityIDClass.length - 1].value != 0)) && isMessaged == false) {
        var identityLast = null;
        var identityNumLast = null;
        var Message = "";
        if (identityNumberClass.length > 1) {
            identityLast = identityNumberClass[identityNumberClass.length - 1].value;
            for (var ValueIndex = 0; ValueIndex < identityNumberClass.length - 1; ValueIndex++) {
                if (identityLast == identityNumberClass[ValueIndex].value) {
                    status = false;
                    isMessaged = true;
                    Message = "Identity number cannot be same.";
                    break;
                }
            }
        }
        if (status == false) {
            document.getElementById('UMSNigeriaBody_lblMessageValidation').innerHTML = Message;
            ShowMessage()
        }
    }


    else {
        status = false;
        if (!isMessaged) {
            if ((identityIDClass[identityIDClass.length - 1].value == "") || (identityIDClass[identityIDClass.length - 1].value == 0)) {
                document.getElementById('UMSNigeriaBody_lblMessageValidation').innerHTML = "Select identity type.";
            }
            else if ((identityNumberClass[identityIDClass.length - 1].value == "" || identityNumberClass[identityIDClass.length - 1].value == 0)) {
                document.getElementById('UMSNigeriaBody_lblMessageValidation').innerHTML = "Enter identity number.";
            }
            ShowMessage()
        }
    }
    return status;
}
function filevalidate(control, filename, Message) {
    document.getElementById("spanPhoto").innerHTML = "";
    if (Trim(filename) != "") {
        if (!/(\.jpeg|\.jpg|\.png|\.gif|\.doc|\.docx|\.pdf)$/i.test(filename)) {
            var spanPhoto = document.getElementById("spanPhoto");
            spanPhoto.innerHTML = "Only .jpeg, .jpg, .png, .gif, .doc, .docx and .pdf formats are supported.";
            //document.getElementById('control').value = '';
            control.value = ''; 
            spanPhoto.style.display = "block";
            IsValid = false;
            return false;
        }
        else {
            return true;
        }
    }
}
var lastDPI;
var dpiAwareImageEl;
var zoomSettingEl;
var imageNameEl;
var titleEl;
var image96dpi;
var imag120dpi;
var image144dpi;
var image192dpi;

window.onload = onLoad;

function onLoad() {
    dpiAwareImageEl = document.getElementById('dpi_aware');
    zoomSettingEl = document.getElementById('zoom_setting');
    imageNameEl = document.getElementById('image_name');
    titleEl = document.getElementById('title');

    image96dpi = 'IE_Logo_150.png';
    image120dpi = 'IE_Logo_180.png';
    image144dpi = 'IE_Logo_225.png';
    image192dpi = 'IE_Logo_300.png';


    if (screen.deviceXDPI) { // To avoid throwing JavaScript errors in other browsers
        lastDPI = screen.deviceXDPI;
        window.onresize = onResize;
        zoomSettingEl.innerHTML = lastDPI / 96 * 100;

    }


    assignImage();
}

function onResize() {
    if (lastDPI != screen.deviceXDPI) { // Only take action if zoom factor has changed (won't be triggered by actual resize of window)
        lastDPI = screen.deviceXDPI;
        zoomSettingEl.innerHTML = lastDPI / 96 * 100;
        assignImage();

    }
}

function assignImage() {
    if (screen.deviceXDPI <= '96' || !screen.deviceXDPI) { // 100% Zoom or less, or non-IE browser
        dpiAwareImageEl.src = image96dpi;
        imageNameEl.innerHTML = image96dpi;
    }
    else if (screen.deviceXDPI <= '120') { // 125% Zoom
        dpiAwareImageEl.src = image120dpi;
        imageNameEl.innerHTML = image120dpi;

    }
    else if (screen.deviceXDPI <= '144') { // 150% Zoom
        dpiAwareImageEl.src = image144dpi;
        imageNameEl.innerHTML = image144dpi;

    }
    else { // Greater than 150% Zoom
        dpiAwareImageEl.src = image192dpi;
        imageNameEl.innerHTML = image192dpi;
    }
}

//        function DoBlur(fld) {
//            fld.className = 'text-box hasDatepicker';
//        }

//        function DoFocus(fld) {
//            fld.className = 'text-box hasDatepicker';
//        }
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};
function ShowMessage() {
    var mpee = $find('UMSNigeriaBody_mpeMessage');
    mpee.show();
    return false;
}


//function disableBackButton() {
//    window.history.forward()
//}
//disableBackButton();
//window.onload = disableBackButton();
//window.onpageshow = function (evt) { if (evt.persisted) disableBackButton() }
//window.onunload = function () { void (0) }

function GovtAccountSubType() {
    var ddlAccountType = document.getElementById('UMSNigeriaBody_ddlAccountType');
    var hfGovtAccountTypeID = document.getElementById('UMSNigeriaBody_hfGovtAccountTypeID').value;
    var ReadText = ddlAccountType.options[ddlAccountType.selectedIndex].value;
    if (ReadText == '2') {
        if (hfGovtAccountTypeID == '') {
            document.getElementById('UMSNigeriaBody_lblMessageValidation').innerHTML = "Please select govt account sub type.";
            ShowMessage();
            return false;
        }
        else
            return true;
    }
    else
        return true;

}

function emailValidation(txtEmailID, spanId, spanText) {
    var Valid = true;
    if (TextBoxValidationlbl(txtEmailID, document.getElementById(spanId), spanText) == false) Valid = false;
    else
        Valid = OnlyEmailValidation(txtEmailID, spanId, spanText);
    return Valid;
}

function OnlyEmailValidation(txtEmailID, spanId, spanText) {
    var Valid = true;
    if (txtEmailID.value !="" && EmailValidation(txtEmailID) == false) {
        var spanEmail = document.getElementById(spanId);
        spanEmail.innerHTML = "Please Enter Valid Email Id";
        spanEmail.style.display = "block";
        txtEmailID.style.border = "1px solid red";
        Valid = false;
    }
//    else if (IsEmailAlreadyExists(txtEmailID.value)) {
//        document.getElementById(spanId).innerHTML = "EmailId already exists.";
//        document.getElementById(spanId).style.display = "block";
//        txtEmailID.style.border = "1px solid red";
//        Valid = false;
//    }
    else {
        txtEmailID.style.border = "1px solid #8E8E8E";
        document.getElementById(spanId).innerHTML = "";
    }
    return Valid;
}

//This funciton is ruquired to make read type by default for prepaid customer.
function ifPrepaidMeter() {
    var ddlCustomerType = document.getElementById('UMSNigeriaBody_ddlCustomerType');
    var ddlReadCode = document.getElementById('UMSNigeriaBody_ddlReadCode');
    var ReadText = ddlCustomerType.options[ddlCustomerType.selectedIndex].value;
    if (ReadText == '3') {
        ddlReadCode.selectedIndex = 2;
        ddlReadCode.disabled = true;
        ddlReadCode.title = "Prepaid Customer.";
        IfReadedCustomer();
    }
    else {
        ddlReadCode.selectedIndex = 0;
        ddlReadCode.disabled = false;
        ddlReadCode.title = "";
        IfReadedCustomer();
    }
    //    remove below line from .cs
    document.getElementById('UMSNigeriaBody_txtMeterNo').value = "";
    return false;
}

function CustomerContactNoLengthMinMax(TextBox, MinLength, MaxLength, lblmsg, txtCode, CodeLength) {
    lblmsg.innerHTML = "";
    var IsValid = true;
    if (TextBox.value.length < MinLength) {
        lblmsg.innerHTML = "Contact no should be Minimum " + MinLength + " characters.";
        lblmsg.style.display = "block";
        TextBox.style.border = "1px solid red";
        //TextBox.focus();
        IsValid = false;
    }
    else if (TextBox.value.length > MaxLength) {
        lblmsg.innerHTML = "Contact no should be Maximum " + MaxLength + " characters.";
        lblmsg.style.display = "block";
        TextBox.style.border = "1px solid red";
        //TextBox.focus();
        IsValid = false;
    }
    else if (TextBoxValidationlbl(txtCode, lblmsg, 'Code') == false) {
        IsValid = false;
    }
    else if (txtCode.value.length > CodeLength) {
        lblmsg.innerHTML = "Code should be Maximum " + CodeLength + " characters.";
        lblmsg.style.display = "block";
        txtCode.style.border = "1px solid red";
        //txtCode.focus();
        IsValid = false;
    }
    return IsValid;
}


function IsSameEmail() {
    var IsValid = true;
    if (document.getElementById('UMSNigeriaBody_txtOEmailId').value != "" &&
    document.getElementById('UMSNigeriaBody_txtTEmailId').value != "") {
        if (document.getElementById('UMSNigeriaBody_txtOEmailId').value == document.getElementById('UMSNigeriaBody_txtTEmailId').value != "") {
            var spanEmail = document.getElementById('spanTEmailId');            
            spanEmail.innerHTML = "Mail Id can not be same as Landlord email id";
            spanEmail.style.display = "block";
            document.getElementById('UMSNigeriaBody_txtTEmailId').style.border = "1px solid red";
            IsValid = false; 
        }
    }
    return IsValid;
}

function CopyToKnownAsOwner() {
    var fName = document.getElementById('UMSNigeriaBody_txtOFName').value + ' ';
    var mName = document.getElementById('UMSNigeriaBody_txtOMName').value + ' ';
    var lName = document.getElementById('UMSNigeriaBody_txtOLName').value;

    document.getElementById('UMSNigeriaBody_txtOKnownAs').value = fName + mName + lName;
}

function funGovType() {
    var IsValid = true;
    var ddlAccountType = document.getElementById('UMSNigeriaBody_ddlAccountType');
    //alert(' --Select--');
    if (ddlAccountType.value == '') {
        //alert('value');
        document.getElementById('spanAccountType').innerHTML = 'Select Account Type';
        IsValid = false;
    }
    else {
        document.getElementById('spanAccountType').innerHTML = '';
        //document.getElementById('UMSNigeriaBody_ddlAccountType').onchange();
        document.getElementById('UMSNigeriaBody_btnGovtAccountType').click();
        
    }
    return IsValid;
}

//function CheckApplicationProcessDate(Date) {
//    var IsValid = true;
//    var ApplicationDate = document.getElementById('UMSNigeriaBody_txtApplicationDate');
//    var ConnectionDate = document.getElementById('UMSNigeriaBody_txtConnectionDate');
//    var SetupDate = document.getElementById('UMSNigeriaBody_txtSetupDate');
//    //alert(Date.id + ' - ');
//    var ThisDate = Date.value.split('/');
//    var xdate = ThisDate[1] + '/' + ThisDate[0] + '/' + ThisDate[2];
//    var date2 = new Date(xdate);
//    var AppDate2, StUpDate2, ConnDate2;
//    if (ApplicationDate.value != "") {
//        var ThisAppDate = ApplicationDate.value.split('/');
//        var xAppDate = ThisAppDate[1] + '/' + ThisAppDate[0] + '/' + ThisAppDate[2];
//        AppDate2 = new Date(xAppDate);
//    }

//    if (SetupDate.value != "") {
//        var ThisSetupDate = SetupDate.value.split('/');
//        var xStUpDate = ThisStUpDate[1] + '/' + ThisStUpDate[0] + '/' + ThisStUpDate[2];
//        StUpDate2 = new Date(xStUpDate);
//    }

//    if (ConnectionDate.value != "") {
//        var ThisConnDate = ConnectionDate.value.split('/');
//        var xConnDate = ThisConnDate[1] + '/' + ThisConnDate[0] + '/' + ThisConnDate[2];
//        ConnDate2 = new Date(xConnDate);
//    }        

//    if (Date.id == ApplicationDate.id) {
//        if (date2>ConnDate2) {
//            document.getElementById('UMSNigeriaBody_lblMessageValidation').innerHTML = "Application date should not be greater then Connection date";
//            ShowMessage();
//        }
//    }
//    else if (Date.id == ConnectionDate.id){
//        //alert('Connection Date');
//        }
//    else if (Date.id == SetupDate.id){
//       // alert('Setup Date');
//       }
//    
//    return IsValid;
//}


//------------1st Date Greater than 2nd date-----
function CompareDates(input1, input2, lblmsg) {
    var inpu1 = input1.value.split('/');
    var frmDate = inpu1[1] + '/' + inpu1[0] + '/' + inpu1[2]

    var inpu2 = input2.value.split('/');
    var todate = inpu2[1] + '/' + inpu2[0] + '/' + inpu2[2]


    var date1 = new Date(frmDate);
    var date2 = new Date(todate);

    if (date1 > date2) {
        //alert("To date should be greater than or equal to From date");
        document.getElementById('UMSNigeriaBody_lblMessageValidation').innerHTML = lblmsg;
        ShowMessage();
        return false;
    }
}

function GetCurrencyFormate(obj) {
    obj.value = NewText(obj);
}

function isNumberKey(evt, txt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode == 46 && txt.value.split('.').length > 1) {
        return false;
    }
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
}

function onlyNos(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        } else { return true; }
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    catch (err) {
        alert(err.Description);
    }
}

function CustomerEmailValidate(txtEmailID, spanId, spanText) {
    var Valid = true;
    if (EmailValidation(txtEmailID) == false) {
        var spanEmail = document.getElementById(spanId);
        spanEmail.innerHTML = "Please Enter Valid Email Id";
        spanEmail.style.display = "block";
        txtEmailID.style.border = "1px solid red";
        Valid = false;
    }
    else {
        var spanEmail = document.getElementById(spanId);
        spanEmail.innerHTML = "";
        txtEmailID.style.border = "1px solid #8E8E8E";
        Valid = false;
    }
    return Valid;
}

function MeterReadVisible() {
    document.getElementById("divReadCustomer").style.display = "block";
    if(document.getElementById('UMSNigeriaBody_cbIsCAPMY').checked)
        document.getElementById("divAmount").style.display = "block";
    
//    var MeterDials = document.getElementById("UMSNigeriaBody_hfMeterDials");
//    
//    $("#UMSNigeriaBody_txtPresentReading").attr('maxlength', MeterDials.value);
}

function IsEmailAlreadyExists(EmailId) {
    var flag = false;
    $.ajax({
        type: "POST",
        url: "/ConsumerManagement/NewCustomer.aspx/IsEmailExists",
        data: '{EmailId: "' + EmailId + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            flag = response.d;
        },
        failure: function (response) {
            alert(response.d);
        }
    });
    return flag;
}
function OnSuccess(data) {
    if (data.d) {
        var spanEmail = document.getElementById('spanOEmailId');
        spanEmail.innerHTML = "Email already exists";
        spanEmail.style.display = "block";
        document.getElementById('UMSNigeriaBody_txtOEmailId').style.border = "1px solid red";
    }
}