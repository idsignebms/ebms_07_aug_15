﻿function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};

function Validate() {
    var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo');
    var IsValid = true;

    if (TextBoxValidationlbl(txtAccountNo, document.getElementById("spanAccNo"), "Account No / Old Account No") == false) IsValid = false;

    if (txtAccountNo.value == "")
        document.getElementById('UMSNigeriaBody_divdetails').style.display = "none";

    if (!IsValid) {
        return false;
    }
}
function GetCurrencyFormate(obj) {
    obj.value = NewText(obj);
}
function isNumberKey(evt, txt) {

    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    if ((charCode == 46 && txt.value.split('.').length > 1)) {
        return false;
    }
}
function UpdateValidate() {

    var txtAmount = document.getElementById('UMSNigeriaBody_txtAmount');
    var txtReason = document.getElementById('UMSNigeriaBody_txtReason');
    var hfOutStandingAmount = document.getElementById('UMSNigeriaBody_hfOutStandingAmount');
    var mpeAlert = $find('UMSNigeriaBody_mpeAlert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblAlertMsg');
    var IsValid = true;

    if (TextBoxValidationlbl(txtReason, document.getElementById("spanReason"), "Reason") == false) IsValid = false;
    if (TextBoxValidationlbl(txtAmount, document.getElementById("spanAmount"), "Amount") == false) IsValid = false;
    else if (TextBoxBlurValidationlblWithNoZero(txtAmount, "spanAmount", "Amount") == false) IsValid = false;
    else if (txtAmount.value != "") {
        if (parseInt(hfOutStandingAmount.value) < parseFloat(txtAmount.value.replace(",", ""))) {
            lblAlertMsg.innerHTML = "Amount should not be grater than the outstanding amount";
            mpeAlert.show();
            IsValid = false;
        }
    }
    if (!IsValid) {
        return false;
    }
}     