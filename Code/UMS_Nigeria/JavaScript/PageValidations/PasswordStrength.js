﻿

function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};

//function Validate() {
//    var txtMinPasswordLength = document.getElementById('UMSNigeriaBody_txtMinPasswordLength');
//    var IsValid = true;

//    if (TextBoxValidationlbl(txtMinPasswordLength, document.getElementById("spanMinPasswordLength"), "Min Password Length") == false) IsValid = false;
//    return IsValid;
//}

function Validate() {
    var txtMinNoOfCapitalLetters = document.getElementById('UMSNigeriaBody_txtMinNoOfCapitalLetters');
    var txtMinNoOfSmallLetters = document.getElementById('UMSNigeriaBody_txtMinNoOfSmallLetters');
    var txtMinSpecialCharecters = document.getElementById('UMSNigeriaBody_txtMinSpecialCharecters');
    var txtMinNoOfNumericValue = document.getElementById('UMSNigeriaBody_txtMinNoOfNumericValue');
    var txtMinPasswordLength = document.getElementById('UMSNigeriaBody_txtMinPasswordLength');


    var IsValid = true;


    if (LengthMinMaxlbl(txtMinNoOfCapitalLetters, 1, 6, document.getElementById("spanMinNoOfCapitalLetters"), " Enter Min Capital Letters between 1 to 6") == false) IsValid = false;
    if (LengthMinMaxlbl(txtMinNoOfSmallLetters, 1, 6, document.getElementById("spanMinNoOfSmallLetters"), " Enter Min Small Letters between 1 to 6") == false) IsValid = false;
    if (LengthMinMaxlbl(txtMinSpecialCharecters, 1, 6, document.getElementById("spanMinNoOfSpecialCharecters"), " Enter Special characters between 1 to 6") == false) IsValid = false;
    if (LengthMinMaxlbl(txtMinNoOfNumericValue, 1, 6, document.getElementById("spanMinNoOfNumericValue"), " Enter Min Numeric Letters between 1 to 6") == false) IsValid = false;
    if (LengthMinMaxlbl(txtMinPasswordLength, 6, 20, document.getElementById("spanMinPasswordLength"), " Enter Min Password Length between 6 to 20") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
}


function LengthMinMaxlbl(TextBox, MinLength, MaxLength, lblmsg, msg) {
    lblmsg.innerHTML = "";
    lblmsg.style.display = "none";
    if (parseInt(TextBox.value) < parseInt(MinLength) || parseInt(TextBox.value) > parseInt(MaxLength)) {
        lblmsg.innerHTML = msg;
        lblmsg.style.display = "block";
        TextBox.style.border = "1px solid red";
        return false;
    }
    else
        TextBox.style.border = "1px solid #8E8E8E";
}
