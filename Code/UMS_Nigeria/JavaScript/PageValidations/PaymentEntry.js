﻿
function controlEnter(obj, event) {
    var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;

    if (keyCode == 13) {
        var txt = document.getElementById(obj);
        txt.focus();
    }
    else {
        return true;
    }
}

function Validate() {
    var IsValid = true;
    var IsPaymentValid = true;
    var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo');
    var txtPaidAmount = document.getElementById('UMSNigeriaBody_txtPaidAmount');
    var txtReciptNo = document.getElementById('UMSNigeriaBody_txtReciptNo');
    //var ddlBillNos = document.getElementById('UMSNigeriaBody_ddlBillNos');
    var ddlPaymentMode = document.getElementById('UMSNigeriaBody_ddlPaymentMode');
    var txtPaymentRecievedDate = document.getElementById('UMSNigeriaBody_txtPaymentRecievedDate');

    if (TextBoxValidationlbl(txtAccountNo, document.getElementById('spanAccountNo'), " Account No") == false) IsValid = false;
    if (txtPaidAmount.value != "") {
        txtPaidAmount.value = TrimLeadingZeros(txtPaidAmount.value);
    }
    if (TextBoxValidationlbl(txtPaidAmount, document.getElementById('spanPaidAmount'), " Paid Amount") == false) IsValid = false;
    //if (DropDownlistValidationlbl(ddlBillNos, document.getElementById('spanBillNos'), "Bill No") == false) IsValid = false;
    if (TextBoxValidationlbl(txtReciptNo, document.getElementById('spanReciptNo'), " Receipt No") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlPaymentMode, document.getElementById('spanPaymentMode'), "Payment Mode") == false) IsValid = false;
    if (TextBoxValidationlbl(txtPaymentRecievedDate, document.getElementById('spanPaymentRecievedDate'), " Payment Received Date") == false) IsValid = false;
    if (isDateLbl(txtPaymentRecievedDate.value, txtPaymentRecievedDate, "spanPaymentRecievedDate") == false) IsValid = false;


    //Document validation
    var divFile = document.getElementById("divFile");
    var Docitems = divFile.getElementsByTagName("input");
    for (var i = 0; i < Docitems.length; i++) {
        var spanDocuments = document.getElementById("spanDocuments").innerHTML = "";
        if (!Trim(Docitems[i].value) == "") {
            if (!/(\.doc|\.docx|\.pdf|\.xlsx|\.xls)$/i.test(Docitems[i].value)) {
                var spanDocuments = document.getElementById("spanDocuments");
                spanDocuments.innerHTML = "Please select .doc, .docx, .xlsx , .pdf and .xls File Formats only";
                spanDocuments.style.display = "block";
                IsValid = false;
                break;
            }
            //                    if (Docitems[i].size > 1024000) {
            //                        //alert("scanned photo should be upto 1 MB only");
            //                        var spanDocuments = document.getElementById("spanDocuments");
            //                        spanDocuments.innerHTML = "Document should be upto 1 MB only";
            //                        spanDocuments.style.display = "block";
            //                        IsValid = false;
            //                    }
        }
        else {
            var spanDocuments = document.getElementById("spanDocuments").innerHTML = "";
        }
    }
    var txt = document.getElementById('UMSNigeriaBody_txtPaidAmount');
    if (txt.value.charAt(0) == "." || txt.value < 1) {
        var lbl = document.getElementById('spanPaidAmount');
        lbl.innerHTML = "Enter Valid Amount";
        lbl.style.display = "block";
        txt.style.border = "1px solid red";
        IsValid = false;
    }
    //            else {
    //                txt.style.border = "1px solid #8E8E8E";
    //                document.getElementById('spanPaidAmount').innerHTML = "";
    //                return TextBoxBlurValidationlbl(obj, 'spanPaidAmount', 'Paid Amount');
    //                IsValid = true;
    //            }
    if (!IsValid) {
        return false;
    }
}


function isNumberKey(evt, txt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode == 46 && txt.value.split('.').length > 1) {
        return false;
    }
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
}

function GetCurrencyFormate(obj) {
    obj.value = NewTextWith2Decimals(obj);
}

function ValidateDate() {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    document.getElementById('spanPaymentRecievedDate').innerHTML = "";
    var txtPaymentRecievedDate = document.getElementById('UMSNigeriaBody_txtPaymentRecievedDate');
    if (TextBoxValidationlbl(txtPaymentRecievedDate, document.getElementById('spanPaymentRecievedDate'), "a Valid Date") == false)
        return false;
    else
        if (isDateLbl(txtPaymentRecievedDate.value, txtPaymentRecievedDate, "spanPaymentRecievedDate") == false) {
            return false;
        }
        else {
            if (GreaterTodayInPopup(mpeAlert, lblAlertMsg, txtPaymentRecievedDate, "Payment Date should not be Greater than the Today's Date") == false) {
                return false;
            }
        }
}
function DoBlur(fld) {
    ValidateDate();
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
    Calendar('UMSNigeriaBody_txtPaymentRecievedDate', 'UMSNigeriaBody_imgDate', "-2:+8");

    //            $(window).bind('beforeunload', function () {
    //                return 'Please click on "Batch finish" before leaving the page.';
    //            });
};

function ConfirmValidate1() {
    var mpeDeActive = document.getElementById('UMSNigeriaBody_mpeConfirmDelete');
    return false;
    mpeDeActive.show();
}
window.onload = WindowLoad;
function WindowLoad(event) {
    LoadStartTime();
}

var Loadsecs = 60;
var t1;
function LoadStartTime() {
    clearTimeout(t1);
    Loadsecs = Loadsecs - 1;
    t1 = setTimeout("LoadStartTime()", 1000);
    if (Loadsecs == 0) {
        PageMethods.UpdateTimeElapsed(TimerSuccess, Incase_Failure);
    }
}
function FocusAccountNo() {
    document.getElementById('UMSNigeriaBody_txtAccountNo').focus();
}
function ValidateAccountNo() {
    var IsValid = true;
    var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo');
    if (TextBoxValidationlbl(txtAccountNo, document.getElementById('spanAccountNo'), " Account No") == false) IsValid = false;
    return IsValid;
}