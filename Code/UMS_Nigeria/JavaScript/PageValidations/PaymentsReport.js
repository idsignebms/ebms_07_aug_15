﻿function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};

function CheckAll() {
    var cbAll = document.getElementById('UMSNigeriaBody_cbAll');
    var chkBoxList = document.getElementById('UMSNigeriaBody_cbl');
    CheckBoxListSelect(cbAll, chkBoxList);
}
function UnCheckAll() {
    var chkBoxList = document.getElementById('UMSNigeriaBody_cbl');
    var cbAll = document.getElementById('UMSNigeriaBody_cbAll');
    Uncheck(cbAll, chkBoxList);
}

function Validate() {
    debugger;
    var ddlFilterType = document.getElementById('UMSNigeriaBody_ddlFilterType');
    var divFilterType = document.getElementById('UMSNigeriaBody_divFilterType');
    var txtFromDate = document.getElementById('UMSNigeriaBody_txtFromDate');
    var txtToDate = document.getElementById('UMSNigeriaBody_txtToDate');

    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var IsValid = true;

    if (DropDownlistValidationlbl(ddlFilterType, document.getElementById("spanFilterType"), "Filter Type") == false) IsValid = false;

    if (divFilterType != null) {
        var cbl = document.getElementById('UMSNigeriaBody_cbl');
        if (ddlFilterType.selectedIndex == 1)
            if (ValidateCblInPopup(mpeAlert, lblAlertMsg, cbl, 'Business Unit') == false) return false;
        if (ddlFilterType.selectedIndex == 2)
            if (ValidateCblInPopup(mpeAlert, lblAlertMsg, cbl, 'Book Group') == false) return false;

        if (txtFromDate.value != "") {
            if (isDateLbl(txtFromDate.value, txtFromDate, "spanFromDate") == false) IsValid = false;
        }

        if (txtToDate.value != "") {
            if (isDateLbl(txtToDate.value, txtToDate, "spanToDate") == false) IsValid = false;
        }
    }


    if (IsValid) {
        if (GreaterTodayInPopup(mpeAlert, lblAlertMsg, txtFromDate, "From Date should not be Greater than the Today's Date") == false) return false;
        if (GreaterTodayInPopup(mpeAlert, lblAlertMsg, txtToDate, "To Date should not be Greater than the Today's Date") == false) return false;
        if (FromToDateInPopup(mpeAlert, lblAlertMsg, txtFromDate.value, txtToDate.value, 'To date should be greater than the From date') == false) return false;
    }
    else {
        return false;
    }
}

function printGrid() {


    var printContent = document.getElementById('UMSNigeriaBody_divPrint');
    var uniqueName = new Date();

    var gridrows = document.getElementById('UMSNigeriaBody_gvPaymentsRptList');

    var windowName = 'Print' + uniqueName.getTime();

    var printWindow = window.open();
    printWindow.document.write('<html><head>');
    printWindow.document.write('</head><body><h3>');
    printWindow.document.write('Payments Received Report');
    printWindow.document.write('</h3>');
    printWindow.document.write("<link href='../css/style.css' rel='stylesheet' type='text/css' />");
    printWindow.document.write('<link href="../css/reset.css" rel="stylesheet" type="text/css" />');
    printWindow.document.write(printContent.innerHTML);
    printWindow.document.write('</body></html>');

    printWindow.document.close();
    printWindow.focus();
    printWindow.print();
    printWindow.close();

    return false;

}