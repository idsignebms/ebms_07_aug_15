﻿function ValidatePaidAmount() {
    var txt = document.getElementById('UMSNigeriaBody_txtAmount');
    if (txt.value.charAt(0) == ".") {
        var lbl = document.getElementById('spanPaidAmount');
        lbl.innerHTML = "Enter Valid Amount";
        lbl.style.display = "block";
        txt.style.border = "1px solid red";
        return false;
    }
    else {
        txt.style.border = "1px solid #8E8E8E";
        document.getElementById('spanPaidAmount').innerHTML = "";
        return TextBoxBlurValidationlbl(obj, 'spanPaidAmount', 'Paid Amount');
        return true;
    }
}


function validateGo() {
    var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo');
    var IsValid = true;

    if (TextBoxValidationlbl(txtAccountNo, document.getElementById('spanAccountNo'), "Global Account No/Old Account No") == false) IsValid = false;
    if (!IsValid)
        return false;
}

function Validate() {

    var txtAmount = document.getElementById('UMSNigeriaBody_txtAmount');
    var txtPaidDate = document.getElementById('UMSNigeriaBody_txtPaidDate');
     var mpeAlert = $find('UMSNigeriaBody_mpeAlert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblAlertMsg');

    var IsValid = true;
    if (TextBoxValidationlbl(txtAmount, document.getElementById('spanPaidAmount'), "Paid Amount") == false) IsValid = false;
    if (TextBoxValidationlbl(txtPaidDate, document.getElementById('spanPaidDate'), "Payment Received Date") == false) IsValid = false;
    else if (isDateLbl(txtPaidDate.value, txtPaidDate, "spanPaidDate") == false) IsValid = false;

    if (IsValid) {
        if (GreaterTodayInPopup(mpeAlert, lblAlertMsg, txtPaidDate, "Payment Received Date should not be Greater than the Today's Date") == false) return false;
    }
    else
        return false;

}
function GetCurrencyFormate(obj) {
    obj.value = NewTextWith2Decimals(obj);
}