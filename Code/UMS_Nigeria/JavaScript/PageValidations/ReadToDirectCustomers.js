﻿function ValidateGo() {
    var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo');
    var IsValid = true;

    if (TextBoxValidationlbl(txtAccountNo, document.getElementById("spanAccountNo"), "Global Account No / Old Account No") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
}

function UpdateValidate() {
    var IsValid = true;
    var txtReason = document.getElementById('UMSNigeriaBody_txtReason');
    if (TextBoxValidationlbl(txtReason, document.getElementById('SpanReason'), 'Reason') == false) IsValid = false;
    if (!IsValid)
        return false;
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};