﻿function ValidateUpload() {
    var mpegridalert = $find('UMSNigeriaBody_mpegridalert');
    var lblgridalertmsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var fupExcel = document.getElementById('UMSNigeriaBody_fupUploadTransactions');
    if (OnUploadpopup(mpegridalert, lblgridalertmsg, fupExcel, ['xls', 'xlsx'], " Excel File") == false)
        return false;
}