﻿document.write('<script type="text/javascript" src="' + "../JavaScript/CommonValidations.js" + '"></script>');

function Validate() {

    if (ItemValidate() == true) {
        var txtFieldName = document.getElementById('UMSNigeriaBody_txtFieldName');
        var ddlControlType = document.getElementById('UMSNigeriaBody_ddlControlType');
        var IsValid = true;
        if (TextBoxValidationlbl(txtFieldName, document.getElementById("spanFieldName"), "Field Name") == false) IsValid = false;
        if (DropDownlistValidationlbl(ddlControlType, document.getElementById("spanddlControlType"), "Control Type") == false) IsValid = false;

        if (!IsValid) {
            return false;
        }
    } else {
        ShowMessage();
        return false;
    }
}

function UpdateValidation(FieldName, ControlType) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var txtFieldName = document.getElementById(FieldName);
    var ddlControlType = document.getElementById(ControlType);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtFieldName, "Field Name") == false) { return false; }
    if (DropDownlistValidationsInPopup(mpeAlert, lblAlertMsg, ddlControlType, "Control Type") == false) { return false; }
}

//function AddItems() {
//    if (ItemValidate() == true) {
//        var div = document.createElement('DIV');
//        div.innerHTML = '<div class="text-inner">'
//            + '<label for="name">'
//            + 'Item1'
//            + '</label><br>'
//            + '<input type="text" onfocus="DoFocus(this);" class="text-box">'
//            + '</div>'
//            + '<a onclick="return RemoveItem(this);" class="removeLink" id="UMSNigeriaBody_linkRemoveTest" href="javascript:__doPostBack(&#39;ctl00$UMSNigeriaBody$linkRemoveTest&#39;,&#39;&#39;)"><img src="../images/cancel.png" alt="Remove" /></a>';
//        document.getElementById("UMSNigeriaBody_DivDdlItems").appendChild(div);
//    } else {
//        ShowMessage();
//    }
//    return false;
//}

function AddItems() {
    if (ItemValidate() == true) {
        var div = document.createElement('DIV');
        div.innerHTML = '<div class="text-inner">'
            + '<label for="name">'
            + 'Item1'
            + '</label><br>'
            + '<input type="text" onfocus="DoFocus(this);" class="text-box">'
            + '</div>'
            + '<a onclick="return RemoveItem(this);" class="removeLinktwo" id="UMSNigeriaBody_linkRemoveTest" href="javascript:__doPostBack(&#39;ctl00$UMSNigeriaBody$linkRemoveTest&#39;,&#39;&#39;)"><img src="../images/cancel.png" alt="Remove" /></a>';
        document.getElementById("UMSNigeriaBody_DivDdlItems").appendChild(div);
    } else {
        ShowMessage();
    }
    return false;
}


function ItemValidate() {
    var IsVld = true;
    var DivDdlItems = document.getElementById("UMSNigeriaBody_DivDdlItems");
    if (DivDdlItems != null) {
        var itemIDClass = DivDdlItems.getElementsByTagName("input");
        var ItemID = "";
        for (var idnNo = 0; idnNo <= itemIDClass.length - 1; idnNo++) {
            if (itemIDClass[idnNo].value == "") {
                document.getElementById('UMSNigeriaBody_lblMessageValidation').innerHTML = "Please enter the Item name";
                IsVld = false;
                break;
            } else {
                ItemID = ItemID + itemIDClass[idnNo].value + ",";
                ItemID = ItemID.replace(/,/g, "") + "|";
            }

        }
        document.getElementById("UMSNigeriaBody_hfItemIdList").value = ItemID;
    } else {
        IsVld = true;
    }

    return IsVld;
}
function RemoveItem(item) {
    document.getElementById("UMSNigeriaBody_DivDdlItems").removeChild(item.parentNode);
    //document.getElementById("UMSNigeriaBody_hfCurrentIdentity").value = document.getElementById("UMSNigeriaBody_hfCurrentIdentity").value - 1;
    return false;
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};

function ShowMessage()   {
    var mpee = $find('UMSNigeriaBody_mpeMessage');
    mpee.show();
    return false;
}


