﻿function Validation() {
    var rblCalCulatorType = document.getElementById('UMSNigeriaBody_rblCalCulatorType');

    var txtKw = document.getElementById('UMSNigeriaBody_txtKw');
    var txtDate = document.getElementById('UMSNigeriaBody_txtBillDate');
    var txtAccountNo = document.getElementById('UMSNigeriaBody_txtAccountNo');

    var IsValid = true;

    if (RadioButtonListlbl(rblCalCulatorType, "spanCalculatorType", " Customer wise or Tariff wise") == false) IsValid = false;
    var radioItems = rblCalCulatorType.getElementsByTagName("input");
    if (radioItems != null) {
        if (radioItems[0].checked) {
            if (TextBoxValidationlbl(txtAccountNo, document.getElementById("spanAccountNo"), "Account No") == false) IsValid = false;
        }
        else if (radioItems[1].checked) {

        }
    }

    if (TextBoxValidationlbl(txtKw, document.getElementById("spanKw"), "kWh") == false) IsValid = false;
    if (TextBoxValidationlbl(txtDate, document.getElementById("spanBillDate"), "Bill Date") == false) IsValid = false;
    if (txtDate.value != "") {
        if (isDateLbl(txtDate.value, txtDate, "spanBillDate") == false) IsValid = false;
    }
    if (!IsValid)
        return false;
}

function CalculatorType() {
    var rblCalCulatorType = document.getElementById('UMSNigeriaBody_rblCalCulatorType');
    var radioItems = rblCalCulatorType.getElementsByTagName("input");
    var divAccountNo = document.getElementById("divAccountNo");
    var divTariffClass = document.getElementById("divTariffClass");
    var divTariffDetails = document.getElementById("divTariffDetails");
    if (radioItems != null) {
        if (radioItems[0].checked) {
            divAccountNo.style.display = "block";
            divTariffClass.style.display = "none";
            divTariffDetails.style.display = "none";
        }
        else if (radioItems[1].checked) {
            divAccountNo.style.display = "none";
            divTariffClass.style.display = "block";
        }
    }
}