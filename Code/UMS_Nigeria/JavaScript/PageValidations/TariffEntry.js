﻿
var i = 1;
function AddCharge() {
    if (ValidateCharges(true) == false) {
        return false;
    }
    else {

        i++;
        var txtToKWs = document.getElementsByClassName("ToKW");
        var ToKW = (parseInt(txtToKWs[txtToKWs.length - 1].value) + 1).toString();
        var div = document.createElement('DIV');
        div.innerHTML = ' <div class="consume_name" style="width:108px;">' +
                        'From kWh <span>*</span>' +
                    '</div>' +
                    '<div class="consume_input" >' +
                    '<input style="background:#fff;" id="txtFromKw' + i + '" name="txtFromKw' + i + '" type="textbox" onkeypress="return onlyNos(event,this)" placeholder="From kWh" value="' + ToKW + '" class="FromKW" disabled="true" maxlength="7" />' +
                    '</div>' +
                    '<div class="consume_name" style="width:84px; text-align:left;margin-left: 15px;">' +
                        'To kWh <span>*</span>' +
                    '</div>' +
                     '<div class="consume_input">' +
                        '<input style="background:#fff;"  id="txtToKw' + i + '" name="txtToKw' + i + '" type="textbox" placeholder="To kWh" onkeypress="return onlyNos(event,this)" class="ToKW" maxlength="7"/>' +
                    '</div>' +
                    '<div class="consume_name" style="width:81px; text-align:left;margin-left: 15px;">' +
                        'Charge <span>*</span>' +
                    '</div>' +
                    '<div class="consume_input">' +
                        '<input style="background:#fff; " id="txtCharge' + i + '" name="txtCharge' + i + '" type="textbox" placeholder="Enter Charge"  onkeypress="return isNumberKey(event,this)" class="Charges" maxlength="16"/></div><div class="clear"></div>';

        document.getElementById("divCharges").appendChild(div);
        //document.getElementById(div).appendChild('<div class="clear"></div>');
        initialSetup();
    }
    return false;
}
function RemoveCharge(Charge, RowNo) {
    //            if (RowNo > 2) {
    //                //document.getElementById("txtFromKw" + (RowNo-1).toString()).disabled = false;
    //                //                if (document.getElementById("txtFromKw" + (RowNo + 1).toString()) != null) {
    //                //                    
    //                //                }
    //                document.getElementById("txtToKw" + (RowNo - 1).toString()).disabled = false;
    //            }
    //            else {
    //                document.getElementById('UMSNigeriaBody_txtToKw').disabled = false;
    //            }
    document.getElementById("divCharges").removeChild(Charge.parentNode);
    return false;
}

function ValidatePaidAmount(txt) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    //var txt = document.getElementById('<%= txtPaidAmount.ClientID %>');
    if (txt.value.charAt(0) == "." || txt.value < 1) {
        //var lbl = document.getElementById('spanPaidAmount');
        //lbl.innerHTML = "Enter Valid Amount";
        lblAlertMsg.innerHTML = "Enter Valid Charges Amount";
        mpeAlert.show();
        //lbl.style.display = "block";
        //txt.style.border = "1px solid red";
        return false;
    }
    //    else {
    //        txt.style.border = "1px solid #8E8E8E";
    //        document.getElementById('spanPaidAmount').innerHTML = "";
    //        return TextBoxBlurValidationlbl(obj, 'spanPaidAmount', 'Paid Amount');
    //        return true;
    //    }
}

function ValidateCharges(IsAdd) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var txtFromKWs = document.getElementsByClassName("FromKW");
    var txtToKWs = document.getElementsByClassName("ToKW");
    var txtCharges = document.getElementsByClassName("Charges");
    for (var i = 0; i < txtFromKWs.length; i++) {
        if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtFromKWs[i], "From kWh") == false) {
            return false;
        }
        if (IsAdd) {
            if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtToKWs[i], "To kWh") == false) {
                return false;
            }
            if (parseFloat(txtToKWs[i].value) < parseFloat(txtFromKWs[i].value)) {
                //alert("To kWh must be greater than or equal to From kWh");
                txtToKWs[i].focus();
                lblAlertMsg.innerHTML = "To kWh must be greater than From kWh";
                mpeAlert.show();
                return false;
            }
            //txtFromKWs[i].disabled = true;
            txtToKWs[i].disabled = true;
        }
        else {
            if (i != txtToKWs.length - 1) {
                if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtToKWs[i], "To kWh") == false) {
                    return false;
                }
            }

            if (Trim(txtToKWs[i].value) != '') {
                if (parseFloat(txtToKWs[i].value) < parseFloat(txtFromKWs[i].value)) {
                    //alert("To kWh must be greater than or equal to From kWh");
                    txtToKWs[i].focus();
                    lblAlertMsg.innerHTML = "To kWh must be greater than From kWh";
                    mpeAlert.show();
                    return false;
                }
            }
        }

        if (ValidatePaidAmount(txtCharges[i]) == false) {
            return false;
        }

        if (i > 0) {
            for (var x = 0; x < i; x++) {
                if (Trim(txtCharges[i].value) != '') {
                    if (parseFloat(txtCharges[i].value) <= parseFloat(txtCharges[x].value)) {
                        lblAlertMsg.innerHTML = "Charge should be in increasing order.";
                        mpeAlert.show();
                        return false;
                    }
                }
            }
        }

        //        if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtCharges[i], "Charge") == false) {
        //            return false;
        //        }
    }
}

function DoFocus(fld) {
    if (fld.className == 'FromKW bgwhite')
        fld.className = 'FromKW bgwhite';
    else if (fld.className == 'ToKW bgwhite')
        fld.className = 'ToKW bgwhite';
    else if (fld.className == 'Charges bgwhite')
        fld.className = 'Charges bgwhite';
}

function Validate() {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');

    var divTariffClasses = document.getElementById('divTariffClasses');
    var Tariffs = divTariffClasses.getElementsByTagName('select');
    var ddlFromMonth = document.getElementById('UMSNigeriaBody_ddlFromMonth');
    var ddlFromYear = document.getElementById('UMSNigeriaBody_ddlFromYear');
    var ddlToMonth = document.getElementById('UMSNigeriaBody_ddlToMonth');
    var ddlToYear = document.getElementById('UMSNigeriaBody_ddlToYear');
    var hfCharges = document.getElementById('UMSNigeriaBody_hfCharges'); //Get charges.
    var hfTariffClass = document.getElementById('UMSNigeriaBody_hfTariffClass');
    var IsValid = true;
    for (var i = 0; i < Tariffs.length; i++) {
        if (DropDownlistValidationlbl(Tariffs[i], document.getElementById("spanTariff"), "Tariff Class") == false) IsValid = false;
    }
    if (DropDownlistValidationlbl(ddlFromMonth, document.getElementById("spanFromMonth"), "From Month") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlFromYear, document.getElementById("spanFromYear"), "From Year") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlToMonth, document.getElementById("spanToMonth"), "To Month") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlToYear, document.getElementById("spanToYear"), "To Year") == false) IsValid = false;
    var FromDate = '01/' + ddlFromMonth.options[ddlFromMonth.selectedIndex].value + '/' + ddlFromYear.options[ddlFromYear.selectedIndex].value;
    var ToDateTotalDays = daysInMonth(parseInt(ddlToMonth.options[ddlToMonth.selectedIndex].value), parseInt(ddlToYear.options[ddlToYear.selectedIndex].value));
    var ToDate = ToDateTotalDays.toString() + '/' + ddlToMonth.options[ddlToMonth.selectedIndex].value + '/' + ddlToYear.options[ddlToYear.selectedIndex].value;

    if (FromToDateInPopup(mpeAlert, lblAlertMsg, FromDate, ToDate, 'To date should be greater than the From date') == false) return false;

    var FromDateFormat = new Date(parseInt(ddlFromYear.options[ddlFromYear.selectedIndex].value), parseInt(ddlFromMonth.options[ddlFromMonth.selectedIndex].value) - 1, 1);
    var ToDateFormat = new Date(parseInt(ddlToYear.options[ddlToYear.selectedIndex].value), parseInt(ddlToMonth.options[ddlToMonth.selectedIndex].value) - 1, ToDateTotalDays)
    var TotalMonths = monthDiff(FromDateFormat, ToDateFormat);

    var MinNoMonthsLength = "12";
    MinNoMonthsLength = parseInt(MinNoMonthsLength);

    if (TotalMonths < MinNoMonthsLength) {
        //alert("Tariff Entry period must be atleast of " + MinNoMonthsLength + " months.");
        lblAlertMsg.innerHTML = "Tariff Entry period must be atleast of " + MinNoMonthsLength + " months.";
        mpeAlert.show();
        return false;
    }

    if (!IsValid)
        return false;
    else
        if (ValidateCharges(false) == false) IsValid = false;

    if (!IsValid)
        return false;
    else {
        var txtFromKWs = document.getElementsByClassName("FromKW");
        var txtToKWs = document.getElementsByClassName("ToKW");
        var txtCharges = document.getElementsByClassName("Charges");
        var cont = 0;
        var Charges = '';
        for (var i = 0; i < txtFromKWs.length; i++) {
            cont++;
            Charges = Charges + txtFromKWs[i].value + ",";
            if (Trim(txtToKWs[i].value) == "")
                Charges = Charges + txtToKWs[i].value + " ,";
            else
                Charges = Charges + txtToKWs[i].value + ",";
            Charges = Charges + txtCharges[i].value.replace(/,/g, "") + "|";
        }
        if (cont == 0) {
            Charges = document.getElementById('UMSNigeriaBody_txtFromKw').value + ",";
            Charges += document.getElementById('UMSNigeriaBody_txtToKw').value + ",";
            Charges += document.getElementById('UMSNigeriaBody_txtChargeAmount').value + ",";
        }

        hfCharges.value = Charges;
        var Tariff = Tariffs[Tariffs.length - 1];
        hfTariffClass.value = Tariff.options[Tariff.selectedIndex].value;
    }
}

function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};

function GetCurrencyFormate(obj) {
    obj.value = NewText(obj);
}

function isNumberKey(evt, txt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode == 46 && txt.value.split('.').length > 1) {
        return false;
    }
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
}

function onlyNos(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        } else { return true; }
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    catch (err) {
        alert(err.Description);
    }
}

function monthDiff(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth() + 1;
    months += d2.getMonth() + 1;
    // edit: increment months if d2 comes later in its month than d1 in its month
    if (d2.getDate() >= d1.getDate())
        months++
    // end edit
    return months <= 0 ? 0 : months;
}

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

function UpdateValidation(Amount) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var txtGridAmount = document.getElementById(Amount);

    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, txtGridAmount, "Charge Amount") == false) {
        return false;
    }
    if (ValidatePaidAmount(txtGridAmount) == false) {
        return false;
    }
}
function printGrid() {
    var printContent = document.getElementById('UMSNigeriaBody_divPrint');
    var uniqueName = new Date();

    var gridrows = document.getElementById('UMSNigeriaBody_gvTariffEntries');
    for (var i = 0; i < gridrows.rows.length; i++) {
        if (i == 0)
            gridrows.rows[i].cells[6].style.display = "none";
    }
    var GridAction = document.getElementsByClassName("GridAction");
    for (var i = 0; i < GridAction.length; i++) {
        GridAction[i].style.display = "none";
    }

    var windowName = 'Print' + uniqueName.getTime();

    var printWindow = window.open();
    printWindow.document.write('<html><head>');
    printWindow.document.write('x</head><body><h3>');
    printWindow.document.write('Tariff Energy Charges List');
    printWindow.document.write('</h3>');
    printWindow.document.write("<link href='../css/style.css' rel='stylesheet' type='text/css' />");
    printWindow.document.write('<link href="../css/reset.css" rel="stylesheet" type="text/css" />');
    printWindow.document.write(printContent.innerHTML);
    printWindow.document.write('</body></html>');

    printWindow.document.close();
    printWindow.focus();
    printWindow.print();
    printWindow.close();

    //            for (var i = 0; i < gridrows.rows.length; i++) {
    //                gridrows.rows[i].cells[6].style.display = "block";
    //                gridrows.rows[i].cells[6].HeaderStyle.width = "10px";
    //            }

    for (var i = 0; i < gridrows.rows.length; i++) {
        if (i == 0) {
            gridrows.rows[i].cells[6].style.display = "block";
            gridrows.rows[i].cells[6].HeaderStyle.width = "10px";
        }
    }
    var GridAction = document.getElementsByClassName("GridAction");
    for (var i = 0; i < GridAction.length; i++) {
        GridAction[i].style.display = "block";
        GridAction[i].HeaderStyle.width = "10px";
    }
    return false;

}
function removeChargs() {
    var totalChilds = $(".FromKW");
    if (totalChilds.length > 1) {
        var divChild = totalChilds[totalChilds.length - 1].parentNode.parentNode;
        document.getElementById("divCharges").removeChild(divChild);
    }
    var ToKws = $(".ToKW");

    ToKws[ToKws.length - 1].disabled = false; //enable current ToKw textbox.
    initialSetup();

    return false;
}
function initialSetup() {
    var totalChilds = $(".FromKW");
    if (totalChilds.length == 1) {
        document.getElementById('UMSNigeriaBody_lnkRemove').style.display = "none";
        document.getElementById('UMSNigeriaBody_txtFromKw').value = '1';
    }
    else
        document.getElementById('UMSNigeriaBody_lnkRemove').style.display = "inline";
}


