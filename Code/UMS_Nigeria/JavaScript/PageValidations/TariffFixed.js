﻿function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};

function Validate() {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var divTariffClasses = document.getElementById('divTariffClasses');
    var Tariffs = divTariffClasses.getElementsByTagName('select');
    var ddlFromMonth = document.getElementById('UMSNigeriaBody_ddlFromMonth');
    var ddlFromYear = document.getElementById('UMSNigeriaBody_ddlFromYear');
    var ddlToMonth = document.getElementById('UMSNigeriaBody_ddlToMonth');
    var ddlToYear = document.getElementById('UMSNigeriaBody_ddlToYear');
    var hfTariffClass = document.getElementById('UMSNigeriaBody_hfTariffClass');
    //var ddlTypeOfCharge = document.getElementById('ddlTypeOfCharge.ClientID');
    // var txtChargeAmount = document.getElementById('txtChargeAmount.ClientID');


    var IsValid = true;
    //            if (Tariffs.length > 0) {
    for (var i = 0; i < Tariffs.length; i++) {
        if (DropDownlistValidationlbl(Tariffs[i], document.getElementById("spanTariff"), "Tariff Class") == false) IsValid = false;
    }
    //            }
    //            divTariffClasses.visibility = false;
    if (DropDownlistValidationlbl(ddlFromMonth, document.getElementById("spanFromMonth"), "From Month") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlFromYear, document.getElementById("spanFromYear"), "From Year") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlToMonth, document.getElementById("spanToMonth"), "To Month") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlToYear, document.getElementById("spanToYear"), "To Year") == false) IsValid = false;

    var FromDate = '01/' + ddlFromMonth.options[ddlFromMonth.selectedIndex].value + '/' + ddlFromYear.options[ddlFromYear.selectedIndex].value;
    var ToDateTotalDays = daysInMonth(parseInt(ddlToMonth.options[ddlToMonth.selectedIndex].value), parseInt(ddlToYear.options[ddlToYear.selectedIndex].value));
    var ToDate = ToDateTotalDays.toString() + '/' + ddlToMonth.options[ddlToMonth.selectedIndex].value + '/' + ddlToYear.options[ddlToYear.selectedIndex].value;

    if (FromToDateInPopup(mpeAlert, lblAlertMsg, FromDate, ToDate, 'To date should be greater than the From date') == false) return false;

    var FromDateFormat = new Date(parseInt(ddlFromYear.options[ddlFromYear.selectedIndex].value), parseInt(ddlFromMonth.options[ddlFromMonth.selectedIndex].value) - 1, 1);
    var ToDateFormat = new Date(parseInt(ddlToYear.options[ddlToYear.selectedIndex].value), parseInt(ddlToMonth.options[ddlToMonth.selectedIndex].value) - 1, ToDateTotalDays)

    var TotalMonths = monthDiff(FromDateFormat, ToDateFormat);

    var MinNoMonthsLength = "12";
    MinNoMonthsLength = parseInt(MinNoMonthsLength);

    if (TotalMonths < MinNoMonthsLength) {
        //alert("Tariff Fixed period must be atleast of " + MinNoMonthsLength + " months.");
        lblAlertMsg.innerHTML = "Tariff Fixed period must be atleast of " + MinNoMonthsLength + " months.";
        mpeAlert.show();
        return false;
    }

    //if (DropDownlistValidationlbl(ddlTypeOfCharge, document.getElementById("spanTypeOfCharge"), "Type Of Charge") == false) IsValid = false;
    // if (TextBoxValidationlblZeroAccepts(txtChargeAmount, document.getElementById("spanAmount"), "Charge Amount") == false) IsValid = false;

    //if (FromToDate('01/' + ddlFromMonth.options[ddlFromMonth.selectedIndex].value + '/' + ddlFromYear.options[ddlFromYear.selectedIndex].value, '01/' + ddlToMonth.options[ddlToMonth.selectedIndex].value + '/' + ddlToYear.options[ddlToYear.selectedIndex].value) == false) return false;
    if (!IsValid)
        return false;

    var Tariff = Tariffs[Tariffs.length - 1];
    hfTariffClass.value = Tariff.options[Tariff.selectedIndex].value;
}

function UpdateValidation(Amount) {
    var mpeAlert = $find('UMSNigeriaBody_mpegridalert');
    var lblAlertMsg = document.getElementById('UMSNigeriaBody_lblgridalertmsg');
    var Amount = document.getElementById(Amount);
    if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, Amount, "Amount") == false) { return false; }
}

function isNumberKey(evt, txt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode == 46 && txt.value.split('.').length > 1) {
        return false;
    }
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
}

function monthDiff(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth() + 1;
    months += d2.getMonth() + 1;
    // edit: increment months if d2 comes later in its month than d1 in its month
    if (d2.getDate() >= d1.getDate())
        months++
    // end edit
    return months <= 0 ? 0 : months;
}

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

function GetCurrencyFormate(obj) {
    obj.value = NewText(obj);
}      