﻿function ValidateSearch() {
    var ddlYearFilter = document.getElementById('UMSNigeriaBody_ddlYearFilter');
    var ddlMonthFilter = document.getElementById('UMSNigeriaBody_ddlMonthFilter');
    var ddlCycleFilter = document.getElementById('UMSNigeriaBody_ddCycleFilter');

    var IsValid = true;

    if (DropDownlistValidationlbl(ddlYearFilter, document.getElementById("spanyearfilter"), "Year") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlMonthFilter, document.getElementById("spanmonthfilter"), "Month") == false) IsValid = false;
    if (DropDownlistValidationlbl(ddlCycleFilter, document.getElementById("spancyclefilter"), "BookGroup/Feeder") == false) IsValid = false;

    if (!IsValid) {
        return false;
    }
}
function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};

function CheckAll(cbAllId, cblId) {
    var cbAll = document.getElementById(cbAllId);
    var chkBoxList = document.getElementById(cblId);
    CheckBoxListSelect(cbAll, chkBoxList);
}
function UnCheckAll(cbAllId, cblId) {
    var chkBoxList = document.getElementById(cblId);
    var cbAll = document.getElementById(cbAllId);
    Uncheck(cbAll, chkBoxList);
}