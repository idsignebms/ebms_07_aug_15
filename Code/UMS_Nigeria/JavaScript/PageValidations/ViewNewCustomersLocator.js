﻿function ConfirmValidate() {
    //            var r = confirm("Are you sure you want to logout?");
    //            if (r == true) { return true; } else { return false; }
    var mpeDeActive = $find('mpeDeActive');
    var lblDeActiveMsg = document.getElementById('lblDeActiveMsg');
    lblDeActiveMsg.innerHTML = "Are you sure you want to logout?";
    mpeDeActive.show();
    var btnDeActiveOk = document.getElementById('btnDeActiveOk');
    btnDeActiveOk.focus();
    return false;

}
function DoBlur(fld) {
    fld.className = 'normalfld';
}

function DoFocus(fld) {
    fld.className = 'focusfld';
}

function pageLoad() {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
};

function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
$(document).ready(function () {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
});

var prm = Sys.WebForms.PageRequestManager.getInstance();

prm.add_endRequest(function () {
    DisplayMessage('UMSNigeriaBody_pnlMessage');
});