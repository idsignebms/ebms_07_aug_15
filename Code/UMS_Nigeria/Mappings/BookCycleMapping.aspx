﻿<%@ Page Title="Book BookGroup Mapping" Language="C#" MasterPageFile="~/MasterPages/EDMUMS.Master"
    AutoEventWireup="true" CodeBehind="BookCycleMapping.aspx.cs" Inherits="UMS_Nigeria.Mappings.BookCycleMapping" %>

<%@ Register Assembly="EditableDropDownList" Namespace="EditableControls" TagPrefix="editable" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../scripts/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="../scripts/jquery.ui.button.js" type="text/javascript"></script>
    <script src="../scripts/jquery.ui.position.js" type="text/javascript"></script>
    <script src="../scripts/jquery.ui.autocomplete.js" type="text/javascript"></script>
    <script src="../scripts/jquery.ui.combobox.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script type="text/javascript">
        function Validate() {
            var ddlCycle = document.getElementById('<%= ddlCycle.ClientID %>');
            var ddlBookNo = document.getElementById('<%= ddlBookNo.ClientID %>');
            var IsValid = true;
            //            if (TextBoxValidationlbl(ddlCycle, document.getElementById("spanCycle"), "Cycle") == false) IsValid = false;
            //            if (TextBoxValidationlbl(ddlBookNo, document.getElementById("spanBookNo"), "BookNo") == false) IsValid = false;
            if (DropDownlistValidationlbl(ddlCycle, document.getElementById("spanCycle"), "BookGroup Name") == false) IsValid = false;
            if (DropDownlistValidationlbl(ddlBookNo, document.getElementById("spanBookNo"), "Book No") == false) IsValid = false;

            if (!IsValid) {
                return false;
            }
        }

        function Reset() {
            ClearControls();
            document.getElementById("spanCycle").innerHTML = "";
            document.getElementById("spanBookNo").innerHTML = "";
            window.location = "BookCycleMapping.aspx";
        }
    </script>
    <script type="text/javascript">
        function pageLoad() {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        };       
    </script>
</asp:Content>
<asp:Content ID="contentBody" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <div class="edmcontainer">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upAddCountries" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <h3>
                    <asp:Literal ID="litBookCycleMap" runat="server" Text="<%$ Resources:Resource, BOOK_CYCLE_MAPPING%>"></asp:Literal>
                </h3>
                <div class="clear">
                    &nbsp;</div>
                <div style="width: 55%; float: left;">
                    <div class="consumer_feild">
                        <div class="consume_name">
                            <asp:Literal ID="litCycle" runat="server" Text="<%$ Resources:Resource, CYCLE_NAME%>"></asp:Literal><span>*</span></div>
                        <div class="consume_input fltl">
                            <%--   <editable:EditableDropDownList ID="ddlCycle" runat="server" Sorted="false" AutoselectFirstItem="False"
                            placeholder="Select Cycle" AutoPostBack="true" OnSelectedIndexChanged="ddlCycle_SelectedIndexChanged">
                        </editable:EditableDropDownList>--%>
                            <asp:DropDownList ID="ddlCycle" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCycle_SelectedIndexChanged">
                                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <span id="spanCycle" class="spancolor"></span>
                        </div>
                        <div class="consume_name c_left">
                            <asp:Literal ID="litBookNo" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal><span>*</span>
                        </div>
                        <div class="consume_input fltl">
                            <%-- <editable:EditableDropDownList ID="ddlBookNo" runat="server" Sorted="false" AutoselectFirstItem="False"
                            placeholder="Select BookNo" AutoPostBack="true" OnSelectedIndexChanged="ddlBookNo_SelectedIndexChanged">
                        </editable:EditableDropDownList>--%>
                            <asp:DropDownList ID="ddlBookNo" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlBookNo_SelectedIndexChanged">
                                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <span id="spanBookNo" class="spancolor"></span>
                        </div>
                        <div class="clear pad_10">
                            <div class="m_left456" >
                                <asp:Button ID="btnSave" Text="<%$ Resources:Resource, SAVE%>" CssClass="calculate_but"
                                    runat="server" OnClick="btnSave_Click" OnClientClick="return Validate()" />
                                <asp:Button ID="btnReset" Text="<%$ Resources:Resource, RESET%>" OnClientClick="return Reset()"
                                    CssClass="calculate_but" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear pad_10" style="margin-left: 106px;">
                    <div class="routwise cycledetail" id="divCycleDetails" runat="server" visible="false">
                        <h2>
                            BookGroup Details</h2>
                        <div>
                            <span class="consume_name">BookGroupId </span><span class="fltl">:</span> <span class="consume_input">
                                <asp:Label ID="lblCycleId" runat="server" Text=""></asp:Label></span>
                            <div class="clear">
                            </div>
                            <span class="consume_name">BookGroupNo</span> <span class="fltl">:</span> <span class="consume_input">
                                <asp:Label ID="lblCycleNo" runat="server" Text=""></asp:Label></span>
                            <div class="clear">
                            </div>
                            <span class="consume_name">BookGroupName</span> <span class="fltl">:</span> <span class="consume_input">
                                <asp:Label ID="lblCycleName" runat="server" Text=""></asp:Label></span>
                            <div class="clear">
                            </div>
                            <span class="consume_name">Business Unit</span> <span class="fltl">:</span> <span
                                class="consume_input">
                                <asp:Label ID="lblCycleBUName" runat="server" Text=""></asp:Label></span>
                            <div class="clear">
                            </div>
                            <span class="consume_name">Service Unit </span><span class="fltl">:</span> <span
                                class="consume_input">
                                <asp:Label ID="lblCycleSUName" runat="server" Text=""></asp:Label></span>
                            <div class="clear">
                            </div>
                            <span class="consume_name">Service Center </span><span class="fltl">:</span> <span
                                class="consume_input">
                                <asp:Label ID="lblCycleServiceCenter" runat="server" Text=""></asp:Label></span>
                            <div class="clear">
                            </div>
                            <span class="consume_name">Contact No :</span> <span class="fltl">:</span> <span
                                class="consume_input">
                                <asp:Label ID="lblCycleContactNo" runat="server" Text=""></asp:Label></span>
                            <div class="clear">
                            </div>
                            <span class="consume_name">Contact Name </span><span class="fltl">:</span> <span
                                class="consume_input">
                                <asp:Label ID="lblCycleContactName" runat="server" Text=""></asp:Label></span>
                        </div>
                    </div>
                    <div class="routwise cycledetail" id="divBookDetails" runat="server" visible="false">
                        <h2>
                            Book Details</h2>
                        <div>
                            <span class="consume_name">Book No :</span> <span class="fltl">:</span> <span class="consume_input">
                                <asp:Label ID="lblBookNo" runat="server" Text=""></asp:Label></span> <span class="consume_name">
                                    Business Unit </span><span class="fltl">:</span> <span class="consume_input">
                                        <asp:Label ID="lblBookBUName" runat="server" Text=""></asp:Label></span>
                            <div class="clear">
                            </div>
                            <span class="consume_name">Service Unit </span><span class="fltl">:</span> <span
                                class="consume_input">
                                <asp:Label ID="lblBookSUName" runat="server" Text=""></asp:Label></span>
                            <div class="clear">
                            </div>
                            <span class="consume_name">Service Center </span><span class="fltl">:</span> <span
                                class="consume_input">
                                <asp:Label ID="lblBookSCName" runat="server" Text=""></asp:Label></span>
                        </div>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div class="mapedcycle_total" id="divMappedList" runat="server" visible="false">
                    <h3 class="gridhead" align="left">
                        Mapped Books To The BookGroup
                    </h3>
                    <div class="consumer_total">
                        <div class="clear">
                        </div>
                    </div>
                    <%--<asp:DataList ID="dlMappedBooks" runat="server" RepeatColumns="3" OnItemCommand="dlMappedBooks_ItemCommand">
                    <ItemTemplate>
                        <div class="cylename1">
                            <div>
                                <div class="fltr">
                                    <asp:LinkButton ID="lbtnUnMapBook" CommandName="UnMap" CommandArgument='<%# Eval("BookNo") %>'
                                        runat="server"><img src="../images/cancel.png" /></asp:LinkButton>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div>
                                <p>
                                    Book No :
                                    <asp:Label ID="lblCycleBookNo" runat="server" Text='<%# Eval("BookNo") %>'></asp:Label><br />
                                    No of Accounts :
                                    <asp:Label ID="lblNoOfAccounts" runat="server" Text='<%# Eval("NoOfAccounts") %>'></asp:Label><br />
                                    Business Unit :
                                    <asp:Label ID="lblBusinessUnit" runat="server" Text='<%# Eval("BusinessUnitName") %>'></asp:Label><br />
                                    Service Unit :
                                    <asp:Label ID="lblServiceUnit" runat="server" Text='<%# Eval("ServiceUnitName") %>'></asp:Label><br />
                                    Service Center :
                                    <asp:Label ID="lblServiceCenter" runat="server" Text='<%# Eval("ServiceCenterName") %>'></asp:Label><br />
                                    CycleId :
                                    <asp:Label ID="lblBookCycleId" runat="server" Text='<%# Eval("CycleId") %>'></asp:Label><br />
                                    Cycle Name :
                                    <asp:Label ID="lblBookCycleName" runat="server" Text='<%# Eval("CycleName") %>'></asp:Label><br />
                                </p>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:DataList>--%>
                    <div class="grid" id="divPrint" runat="server" align="center" style="overflow-x: auto;">
                        <asp:GridView ID="gvMappedBooks" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                            OnRowCommand="gvMappedBooks_RowCommand">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                No Books Found.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" HeaderStyle-Width="3%"
                                    HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, BOOK_NO%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCycleBookNo" runat="server" Text='<%# Eval("BookNo") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, NO_OF_ACCOUNT%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNoOfAccounts" runat="server" Text='<%# Eval("NoOfAccounts") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, BUSINESS_UNIT_NAME%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBusinessUnit" runat="server" Text='<%# Eval("BusinessUnitName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, SERVICE_UNIT_NAME%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblServiceUnit" runat="server" Text='<%# Eval("ServiceUnitName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, SERVICE_CENTER_NAME%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblServiceCenter" runat="server" Text='<%# Eval("ServiceCenterName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, CYCLE_ID%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBookCycleId" runat="server" Text='<%# Eval("CycleId") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, CYCLE_NAME%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBookCycleName" runat="server" Text='<%# Eval("CycleName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="3%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnUnMapBook" ToolTip="Delete" CommandName="UnMap" CommandArgument='<%# Eval("BookNo") %>'
                                            runat="server"><img src="../images/delete.png" /></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <%--                <div class="cylename2">
                    <div class="fltr">
                        <a href="#">
                            <img src="../images/cancel.png" /></a></div>
                    <div class="clear">
                    </div>
                    <div>
                        <p>
                            Cycle Name : C2</p>
                    </div>
                </div>
                <div class="cylename3">
                    <div class="fltr">
                        <a href="#">
                            <img src="../images/cancel.png" /></a></div>
                    <div class="clear">
                    </div>
                    <div>
                        <p>
                            Cycle Name : C2</p>
                    </div>
                </div>
                    --%>
                    <div>
                    </div>
                    <div>
                    </div>
                </div>
                <div class="consumer_total">
                    <div class="pad_10">
                    </div>
                </div>
                <div class="rnkland">
                    <asp:ModalPopupExtender ID="mpeUnMapBook" runat="server" TargetControlID="btnpopup"
                        PopupControlID="pnlUnMapBook" BackgroundCssClass="modalBackground" CancelControlID="btnCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnpopup" runat="server" Text="" Style="display: none;" />
                    <asp:Panel runat="server" ID="pnlUnMapBook" CssClass="modalPopup" Style="display: none;">
                        <div class="popheader popheaderlblred">
                            <asp:Panel ID="pnlMsgPopup" runat="server" align="center">
                                Are you sure do you want to unmap the book details for this cycle ?
                            </asp:Panel>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnUpdate" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="btn_ok"
                                    Style="margin-left: 127px;" OnClick="btnUpdate_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="btn_ok" Style="margin-left: 9px;" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- DeActive  Btn popup Start--%>
                    <%--<asp:ModalPopupExtender ID="mpeUnMapBook" runat="server" PopupControlID="pnlUnMapBook"
                        TargetControlID="btnpopup" BackgroundCssClass="modalBackground" CancelControlID="btnCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnpopup" runat="server" Text="Button" CssClass="popbtn" />
                    <asp:Panel ID="pnlUnMapBook" runat="server" DefaultButton="btnUpdate" CssClass="modalPopup"
                        class="poppnl">
                        <div class="popheader popheaderlblred">
                            <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>--%>
                    <%-- DeActive  popup Closed--%>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".rnkland").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
</asp:Content>
