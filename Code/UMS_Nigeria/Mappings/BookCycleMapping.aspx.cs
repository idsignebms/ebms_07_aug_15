﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for BookCycleMapping
                     
 Developer        : id027-Karthik Thati
 Creation Date    : 04-03-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using Resources;

namespace UMS_Nigeria.Mappings
{
    public partial class BookCycleMapping : System.Web.UI.Page
    {
        #region Members

        iDsignBAL objIdsignBal = new iDsignBAL();
        ConsumerBal objConsumerBal = new ConsumerBal();
        CommonMethods objCommonMethods = new CommonMethods();
        string Key = "BookCycleMapping";

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.CssClass = lblMessage.Text = string.Empty;
            if (!IsPostBack)
            {
                string path = string.Empty;
                path = objCommonMethods.GetPagePath(this.Request.Url);
                if (objCommonMethods.IsAccess(path))
                {
                    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        objCommonMethods.BindCyclesByBUSUSC(ddlCycle, Session[UMS_Resource.SESSION_USER_BUID].ToString(), string.Empty, string.Empty, true, UMS_Resource.DROPDOWN_SELECT, false);
                    else
                        objCommonMethods.BindCyclesList(ddlCycle, true);
                    objCommonMethods.BindUnMappedBooksList(ddlBookNo, true);
                }
                else
                {
                    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                }
            }
        }

        protected void ddlCycle_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCycleDetails();
                BindCycleBookMapList();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlBookNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindBookDetails();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlCycle.SelectedIndex < 0)
                {
                    ddlCycle.Focus();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "validate", "alert('Please select appropriate Cycle')", true);
                }
                else if (ddlBookNo.SelectedIndex < 0)
                {
                    ddlBookNo.Focus();
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "validate", "alert('Please select appropriate BookNo')", true);
                }
                else
                {
                    if (MapBook())
                    {
                        Message(UMS_Resource.BOOK_CYCLE_MAP_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        objCommonMethods.BindUnMappedBooksList(ddlBookNo, true);
                        divBookDetails.Visible = false;
                        BindCycleBookMapList();
                    }
                    else
                    {
                        Message(UMS_Resource.BOOK_CYCLE_MAP_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        private void BindCycleBookMapList()
        {
            try
            {
                ConsumerBe objConsumerBe = new ConsumerBe();
                ConsumerListBe objConsumerListBe = new ConsumerListBe();
                objConsumerBe.CycleId = ddlCycle.SelectedValue;
                objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Get(objConsumerBe, ReturnType.Bulk));
                if (objConsumerListBe.items.Count > 0)
                {
                    gvMappedBooks.DataSource = objConsumerListBe.items;
                    gvMappedBooks.DataBind();
                    divMappedList.Visible = true;
                }
                else
                    divMappedList.Visible = false;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindCycleDetails()
        {
            try
            {
                ConsumerBe objConsumerBe = new ConsumerBe();
                objConsumerBe.CycleId = ddlCycle.SelectedValue;
                objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Get(objConsumerBe, ReturnType.Retrieve));
                if (objConsumerBe != null)
                {
                    lblCycleId.Text = objConsumerBe.CycleId;
                    lblCycleName.Text = objConsumerBe.CycleName;
                    lblCycleNo.Text = objConsumerBe.CycleNo.ToString();
                    lblCycleBUName.Text = objConsumerBe.BusinessUnitName;
                    lblCycleSUName.Text = objConsumerBe.ServiceUnitName;
                    lblCycleServiceCenter.Text = objConsumerBe.ServiceCenterName;
                    lblCycleContactNo.Text = objConsumerBe.ContactNo;
                    lblCycleContactName.Text = objConsumerBe.ContactName;
                    divCycleDetails.Visible = true;
                }
                else
                    divCycleDetails.Visible = false;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void BindBookDetails()
        {
            try
            {
                ConsumerBe objConsumerBe = new ConsumerBe();
                objConsumerBe.BookNo = ddlBookNo.SelectedValue;
                objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Get(objConsumerBe, ReturnType.Group));
                if (objConsumerBe != null)
                {
                    lblBookNo.Text = objConsumerBe.BookNo;
                    lblBookBUName.Text = objConsumerBe.BusinessUnitName;
                    lblBookSUName.Text = objConsumerBe.ServiceUnitName;
                    lblBookSCName.Text = objConsumerBe.ServiceCenterName;
                    divBookDetails.Visible = true;
                }
                else
                    divBookDetails.Visible = false;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private bool MapBook()
        {
            try
            {
                ConsumerBe objConsumerBe = new ConsumerBe();
                objConsumerBe.BookNo = ddlBookNo.SelectedValue;
                objConsumerBe.CycleId = ddlCycle.SelectedValue;
                objConsumerBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Insert(objConsumerBe, Statement.Modify));
                return objConsumerBe.IsSuccess;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
                return false;
            }
        }

        #endregion

        protected void dlMappedBooks_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                switch (e.CommandName.ToUpper())
                {
                    case "UNMAP":
                        Label lblBookCycleId = (Label)e.Item.FindControl("lblBookCycleId");
                        btnUpdate.CommandName = lblBookCycleId.Text;
                        btnUpdate.CommandArgument = e.CommandArgument.ToString();
                        mpeUnMapBook.Show();
                        break;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {
                }
            }
        }

        protected void gvMappedBooks_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                switch (e.CommandName.ToUpper())
                {
                    case "UNMAP":
                        Label lblBookCycleId = (Label)row.FindControl("lblBookCycleId");
                        btnUpdate.CommandName = lblBookCycleId.Text;
                        btnUpdate.CommandArgument = e.CommandArgument.ToString();
                        mpeUnMapBook.Show();
                        break;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {
                }
            }

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                ConsumerBe objConsumerBe = new ConsumerBe();
                objConsumerBe.BookNo = btnUpdate.CommandArgument;
                objConsumerBe.CycleId = btnUpdate.CommandName;
                objConsumerBe = objIdsignBal.DeserializeFromXml<ConsumerBe>(objConsumerBal.Insert(objConsumerBe, Statement.Delete));
                if (objConsumerBe.IsSuccess)
                {
                    objCommonMethods.BindUnMappedBooksList(ddlBookNo, true);
                    BindCycleBookMapList();
                    Message(UMS_Resource.BOOK_UNMAP_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                    Message(UMS_Resource.BOOK_UNMAP_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }


    }
}