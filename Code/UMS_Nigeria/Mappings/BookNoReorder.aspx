﻿<%@ Page Title=":: BookNumbers Re-Order ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master" Theme="Green"
    AutoEventWireup="true" CodeBehind="BookNoReorder.aspx.cs" Inherits="UMS_Nigeria.Mappings.BookNoReorder" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script type="text/javascript">
        function Validate() {

            var ddlCycle = document.getElementById('<%=ddlCycle.ClientID %>');
            var IsValid = true;

            if (DropDownlistValidationlbl(ddlCycle, document.getElementById("spanddlCycle"), "BookGroup") == false) IsValid = false;
            if (!IsValid) {
                return false;
            }
        }
        function pageLoad() { DisplayMessage('<%= pnlMessage.ClientID %>'); };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" AssociatedUpdatePanelID="upnlBookNoReorder">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdatePanel ID="upnlBookNoReorder" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="lit" runat="server" Text="<%$ Resources:Resource, BOOKNO_RE-ORDER_SEQ%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal>
                                    <span class="span_star">*</span></label><div class="space">
                                    </div>
                                <asp:DropDownList ID="ddlCycle" runat="server" CssClass="text-box select_box" AutoPostBack="true" OnSelectedIndexChanged="ddlCycle_SelectedIndexChanged">
                                    <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                                </asp:DropDownList><div class="space"></div>
                                <span id="spanddlCycle" class="span_color"></span>
                                <div class="space">
                                </div>
                                <span>
                                    <br />
                                    <asp:Button ID="btnGo" Text="<%$ Resources:Resource, GO_BUTTON%>" CssClass="box_s"
                                        runat="server" OnClick="btnGo_Click" OnClientClick="return Validate();" />
                                </span>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="grid_boxes">
                        <div class="grid_paging_top">
                            <div class="paging_top_title" style="width:40%;padding-left: 21px;">
                                <asp:Literal ID="litBookList" runat="server" Text=""></asp:Literal>
                            </div>
                            <div class="paging_top_right_content" id="divLinks" visible="false" runat="server">
                                <uc1:UCPagingV1 ID="UCPagingV1" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server" visible="false">
                        <ul class="custmrs_list">
                            <li class="th" id="liRevenueDetailsHeading" runat="server">
                                <dl>
                                    <dt class="sno" style="width: 50px; text-align: center;">
                                        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, GRID_SNO%>" /></dt>
                                    <dt class="name" style="width: 143px; text-align: center;">
                                        <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>" /></dt>
                                    <dt class="name" style="width: 183px; text-align: center;">
                                        <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, BOOK_CODE%>" /></dt>
                                    <dt class="name" style="width: 127px; text-align: center;">
                                        <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>" /></dt>
                                    <dt class="name" style="width: 147px; text-align: center;">
                                        <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>" /></dt>
                                    <dt class="addrs" style="width: 136px; text-align: center;">
                                        <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>" /></dt>
                                </dl>
                            </li>
                            <asp:ReorderList ID="rolBookNo" runat="server" CallbackCssStyle="grid"
                                BorderWidth="0" ClientIDMode="AutoID" PostBackOnReorder="true" DataKeyField="BookNo"
                                OnItemReorder="rolBookNo_ItemReorder">
                                <DragHandleTemplate>
                                    <div id="drag" class="dragHandle" runat="server">
                                    </div>
                                </DragHandleTemplate>
                                <ItemTemplate>
                                    <li class="td" style="height: auto;">
                                        <dl>
                                            <dt class="sno" style="width: 64px; text-align:center;">
                                                <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label></dt>
                                            <dt class="name" style="width: 144px; text-align:center;">
                                                <asp:Label ID="lblBookNo" runat="server" Text='<%#Eval("BookNo") %>'></asp:Label>
                                                <asp:Label ID="lblSortOrder" runat="server" Text='<%#Eval("SortOrder") %>' Visible="false"></asp:Label>
                                            </dt>
                                            <dt class="name" style="width: 183px; text-align:center;">
                                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("BookCode") %>'></asp:Label>
                                            </dt>
                                            <dt class="name" style="width: 127px; text-align:center;">
                                                <asp:Label ID="Label3" runat="server" Text='<%#Eval("BusinessUnitName") %>'></asp:Label>
                                            </dt>
                                            <dt class="name" style="width: 147px; text-align:center;">
                                                <asp:Label ID="Label5" runat="server" Text='<%#Eval("ServiceUnitName") %>'></asp:Label>
                                            </dt>
                                            <dt class="name" style="width: 136px; text-align:center;border-right:none">
                                                <asp:Label ID="Label2" runat="server" Text='<%#Eval("ServiceCenterName") %>'></asp:Label>
                                            </dt>
                                        </dl>
                                    </li>
                                </ItemTemplate>
                            </asp:ReorderList>
                        </ul>
                        <asp:HiddenField ID="hfPageNo" runat="server" />
                        <asp:HiddenField ID="hfTotalRecords" Value="10" runat="server" />
                        <asp:HiddenField ID="hfLastPage" runat="server" />
                        <asp:HiddenField ID="hfPageSize" runat="server" />
                    </div>
                    <div id="NoRec" class="out-bor" runat="server" visible="true">
                        <center>
                            <br />
                            There is no Books for Selected BookGroup.
                            <br />
                        </center>
                    </div>
                    <div class="grid_boxes">
                        <div id="divdownpaging" runat="server" visible="false">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }   
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
