﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for BookNoReorder
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 03-Sep-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Configuration;
using AjaxControlToolkit;

namespace UMS_Nigeria.Mappings
{
    public partial class BookNoReorder : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        BookNoReorderBal _objBookNoReorderBal = new BookNoReorderBal();
        public int PageNum;
        XmlDocument xml = null;
        string Key = UMS_Resource.BOOKNO_REORDER;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
        }
        public string Cycle
        {
            get { return string.IsNullOrEmpty(ddlCycle.SelectedValue) ? string.Empty : ddlCycle.SelectedValue; }
            set { ddlCycle.SelectedValue = value.ToString(); }
        }

        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                if (!IsPostBack)
                {
                    string path = string.Empty;
                    //path = _objCommonMethods.GetPagePath(this.Request.Url);
                    //if (_objCommonMethods.IsAccess(path))
                    //{
                    BindDropDowns();
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                   // BindGrid();
                    //}
                    //else
                    //{
                    //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    //}
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }
        protected void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();                
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void rolBookNo_ItemReorder(object sender, AjaxControlToolkit.ReorderListItemReorderEventArgs e)
        {
            try
            {
                if (Session[UMS_Resource.SESSIONREORDER] != null)
                {
                    DataTable DtOldSession = new DataTable();
                    DataTable DtNewSession = new DataTable();

                    Label lblOld = (Label)rolBookNo.Items[e.OldIndex].FindControl("lblSortOrder");
                    Label lblNew = (Label)rolBookNo.Items[e.NewIndex].FindControl("lblSortOrder");
                    int replacedOrder = Convert.ToInt32(lblOld.Text);
                    int selectedOrder = Convert.ToInt32(lblNew.Text);

                    DtOldSession = (DataTable)Session[UMS_Resource.SESSIONREORDER];
                    DtNewSession = DtOldSession.Copy();
                    DataRow NewRow = DtOldSession.NewRow();
                    NewRow["RowNumber"] = DtNewSession.Rows[e.OldIndex]["RowNumber"];
                    NewRow["BookNo"] = DtNewSession.Rows[e.OldIndex]["BookNo"];
                    NewRow["SortOrder"] = DtNewSession.Rows[e.OldIndex]["SortOrder"];
                    NewRow["BookCode"] = DtNewSession.Rows[e.OldIndex]["BookCode"];
                    NewRow["BusinessUnitName"] = DtNewSession.Rows[e.OldIndex]["BusinessUnitName"];
                    NewRow["ServiceUnitName"] = DtNewSession.Rows[e.OldIndex]["ServiceUnitName"];
                    NewRow["ServiceCenterName"] = DtNewSession.Rows[e.OldIndex]["ServiceCenterName"];
                    DtOldSession.Rows[e.OldIndex].Delete();
                    DtOldSession.Rows.InsertAt(NewRow, e.NewIndex);
                    Session[UMS_Resource.SESSIONREORDER] = DtOldSession;
                    rolBookNo.DataSource = DtOldSession;
                    rolBookNo.DataBind();
                    ReorderListItem SelectedItem = (ReorderListItem)rolBookNo.Items[e.NewIndex];

                    UpdateReorderedList();
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlCycle_SelectedIndexChanged(object sender, EventArgs e)
        {
            //NoRec.Visible = false;
            BindGrid();
        }
        #endregion

        #region Methods
        private void UpdateReorderedList()
        {
            try
            {
                int replacedOrder = 0;
                int pageNo = Convert.ToInt32(hfPageNo.Value);
                int pageSize = this.PageSize;

                replacedOrder = (pageNo * pageSize) - pageSize;

                string BookNo, SortOrder;
                BookNo = SortOrder = string.Empty;

                for (int i = 0; i < rolBookNo.Items.Count; i++)
                {
                    BookNo = string.IsNullOrEmpty(BookNo) ? rolBookNo.DataKeys[i].ToString() : BookNo + "," + rolBookNo.DataKeys[i].ToString();
                    SortOrder = string.IsNullOrEmpty(SortOrder) ? (i + 1 + replacedOrder).ToString() : SortOrder + "," + (i + 1 + replacedOrder).ToString();
                }

                XmlDocument xmldoc = new XmlDocument();
                BookNoReorderBe _objBookNoReorderBe = new BookNoReorderBe();
                _objBookNoReorderBe.BookNo = BookNo;
                _objBookNoReorderBe.Orders = SortOrder;
                xmldoc = _objBookNoReorderBal.Insert(_objBookNoReorderBe, Statement.Update);

                _objBookNoReorderBe = _objiDsignBal.DeserializeFromXml<BookNoReorderBe>(xmldoc);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        public void BindGrid()
        {
            try
            {
                BookNoReorderBe _objBookNoReorderBe = new BookNoReorderBe();
                _objBookNoReorderBe.CycleId = Cycle;
                _objBookNoReorderBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                _objBookNoReorderBe.PageSize = PageSize;
                DataTable dt = new DataTable();
                BookNoReorderListBe ObjList = _objiDsignBal.DeserializeFromXml<BookNoReorderListBe>(_objBookNoReorderBal.Get(_objBookNoReorderBe, ReturnType.Get));
                dt = _objiDsignBal.ConvertListToDataSet<BookNoReorderBe>(ObjList.Items).Tables[0];
                if (ObjList.Items.Count > 0)
                {
                    litBookList.Text = Resource.BOOKS_LIST_BY_CYCLE + ddlCycle.SelectedItem;
                    divgrid.Visible = divdownpaging.Visible = divLinks.Visible = true;
                    NoRec.Visible = false;
                    if (Session[UMS_Resource.SESSIONREORDER] != null)
                        Session.Remove(UMS_Resource.SESSIONREORDER);

                    Session[UMS_Resource.SESSIONREORDER] = dt;
                    hfTotalRecords.Value = ObjList.Items[0].TotalRecords.ToString();
                    rolBookNo.DataSource = _objiDsignBal.ConvertListToDataSet<BookNoReorderBe>(ObjList.Items).Tables[0];
                    rolBookNo.DataBind();
                }
                else
                {
                    divgrid.Visible = divdownpaging.Visible=divLinks.Visible = false;
                    NoRec.Visible = true;
                    hfTotalRecords.Value = ObjList.Items.Count.ToString();
                    rolBookNo.DataSource = new DataTable();
                    rolBookNo.DataBind();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void BindDropDowns()
        {
            try
            {
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                {
                    _objCommonMethods.BindCyclesByBUSUSC(ddlCycle, Session[UMS_Resource.SESSION_USER_BUID].ToString(), string.Empty, string.Empty, true, UMS_Resource.DROPDOWN_SELECT, false);
                }
                else
                    _objCommonMethods.BindAllCyclesList(ddlCycle, true, UMS_Resource.DROPDOWN_SELECT, false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion        

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}