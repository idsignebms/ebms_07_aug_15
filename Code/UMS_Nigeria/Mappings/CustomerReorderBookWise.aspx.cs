﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for CustomerReorderBookWise
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 04-Sep-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using System.Data;
using System.Xml;
using Resources;
using UMS_NigriaDAL;
using System.Drawing;
using System.Threading;
using System.Globalization;
using iDSignHelper;
using UMS_NigeriaBAL;
using AjaxControlToolkit;


namespace UMS_Nigeria.Mappings
{
    public partial class CustomerReorderBookWise : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        BookNoReorderBal _objBookNoReorderBal = new BookNoReorderBal();
        public int PageNum;
        XmlDocument xml = null;
        string Key = UMS_Resource.CUST_REORDER;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
        }
        public string BU
        {
            get { return string.IsNullOrEmpty(ddlBU.SelectedValue) ? string.Empty : ddlBU.SelectedValue; }
            set { ddlBU.SelectedValue = value.ToString(); }
        }
        public string SU
        {
            get { return string.IsNullOrEmpty(ddlSU.SelectedValue) ? string.Empty : ddlSU.SelectedValue; }
            set { ddlSU.SelectedValue = value.ToString(); }
        }
        public string SC
        {
            get { return string.IsNullOrEmpty(ddlSC.SelectedValue) ? string.Empty : ddlSC.SelectedValue; }
            set { ddlSC.SelectedValue = value.ToString(); }
        }
        public string Cycle
        {
            get { return string.IsNullOrEmpty(ddlCycle.SelectedValue) ? string.Empty : ddlCycle.SelectedValue; }
            set { ddlCycle.SelectedValue = value.ToString(); }
        }
        public string Book
        {
            get { return string.IsNullOrEmpty(ddlBookNo.SelectedValue) ? string.Empty : ddlBookNo.SelectedValue; }
            set { ddlBookNo.SelectedValue = value.ToString(); }
        }
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                if (!IsPostBack)
                {
                    string path = string.Empty;
                    //path = _objCommonMethods.GetPagePath(this.Request.Url);
                    //if (_objCommonMethods.IsAccess(path))
                    //{
                    _objCommonMethods.BindUserBusinessUnits(ddlBU, string.Empty, false, UMS_Resource.DROPDOWN_SELECT);
                    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    {
                        ddlBU.SelectedIndex = ddlBU.Items.IndexOf(ddlBU.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                        ddlBU.Enabled = false;
                        ddlBU_SelectedIndexChanged(ddlBU, new EventArgs());
                    }
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    //}
                    //else
                    //{
                    //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    //}
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void rolCustomerList_ItemReorder(object sender, AjaxControlToolkit.ReorderListItemReorderEventArgs e)
        {
            try
            {
                if (Session[UMS_Resource.SESSIONREORDER] != null)
                {
                    DataTable DtOldSession = new DataTable();
                    DataTable DtNewSession = new DataTable();

                    Label lblOld = (Label)rolCustomerList.Items[e.OldIndex].FindControl("lblSortOrder");
                    Label lblNew = (Label)rolCustomerList.Items[e.NewIndex].FindControl("lblSortOrder");
                    int replacedOrder = Convert.ToInt32(lblOld.Text);
                    int selectedOrder = Convert.ToInt32(lblNew.Text);

                    DtOldSession = (DataTable)Session[UMS_Resource.SESSIONREORDER];
                    DtNewSession = DtOldSession.Copy();
                    DataRow NewRow = DtOldSession.NewRow();
                    NewRow["RowNumber"] = DtNewSession.Rows[e.OldIndex]["RowNumber"];
                    NewRow["AccountNo"] = DtNewSession.Rows[e.OldIndex]["AccountNo"];
                    NewRow["SortOrder"] = DtNewSession.Rows[e.OldIndex]["SortOrder"];
                    NewRow["Name"] = DtNewSession.Rows[e.OldIndex]["Name"];
                    NewRow["ServiceAddress"] = DtNewSession.Rows[e.OldIndex]["ServiceAddress"];
                    NewRow["MobileNo"] = DtNewSession.Rows[e.OldIndex]["MobileNo"];
                    NewRow["BookNo"] = DtNewSession.Rows[e.OldIndex]["BookNo"];
                    DtOldSession.Rows[e.OldIndex].Delete();
                    DtOldSession.Rows.InsertAt(NewRow, e.NewIndex);
                    Session[UMS_Resource.SESSIONREORDER] = DtOldSession;
                    rolCustomerList.DataSource = DtOldSession;
                    rolCustomerList.DataBind();
                    ReorderListItem SelectedItem = (ReorderListItem)rolCustomerList.Items[e.NewIndex];

                    UpdateReorderedList();
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region ddlEvents
        protected void ddlBU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlBU.SelectedIndex > 0)
                {
                    _objCommonMethods.BindUserServiceUnits_BuIds(ddlSU, BU, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlSC.SelectedIndex = ddlCycle.SelectedIndex = ddlBookNo.SelectedIndex = 0;
                    ddlSU.Enabled = true;
                    ddlSC.Enabled = ddlCycle.Enabled = ddlBookNo.Enabled = false;

                }
                else
                {
                    ddlSU.SelectedIndex = ddlSC.SelectedIndex = ddlCycle.SelectedIndex = ddlBookNo.SelectedIndex = 0;
                    ddlSU.Enabled = ddlSC.Enabled = ddlCycle.Enabled = ddlBookNo.Enabled = false;
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlSU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlSU.SelectedIndex > 0)
                {
                    _objCommonMethods.BindUserServiceCenters_SuIds(ddlSC, SU, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlSC.SelectedIndex = ddlCycle.SelectedIndex = ddlBookNo.SelectedIndex = 0;
                    ddlSC.Enabled = true;
                    ddlCycle.Enabled = ddlBookNo.Enabled = false;
                }
                else
                {
                    ddlSC.SelectedIndex = ddlCycle.SelectedIndex = ddlBookNo.SelectedIndex = 0;
                    ddlSC.Enabled = ddlCycle.Enabled = ddlBookNo.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlSC.SelectedIndex > 0)
                {
                    _objCommonMethods.BindCyclesByBUSUSC(ddlCycle, BU, SU, SC, true, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlCycle.SelectedIndex = ddlBookNo.SelectedIndex = 0;
                    ddlCycle.Enabled = true;
                    ddlBookNo.Enabled = false;
                }

                else
                {
                    ddlCycle.SelectedIndex = ddlBookNo.SelectedIndex = 0;
                    ddlCycle.Enabled = ddlBookNo.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlCycle_SelectedIndexChanged(object sender, EventArgs e)
        {            
            if (ddlCycle.SelectedIndex > 0)
            {
                _objCommonMethods.BindAllBookNumbers(ddlBookNo, ddlCycle.SelectedValue, true, false, UMS_Resource.DROPDOWN_SELECT);
                ddlBookNo.Enabled = true;
            }

            else
            {
                ddlBookNo.SelectedIndex = 0;
                ddlBookNo.Enabled = false;
            }
        }
        #endregion

        #region Methods
        private void UpdateReorderedList()
        {
            try
            {
                int replacedOrder = 0;
                int pageNo = Convert.ToInt32(hfPageNo.Value);
                int pageSize = this.PageSize;

                replacedOrder = (pageNo * pageSize) - pageSize;

                string AccountNo, SortOrder;
                AccountNo = SortOrder = string.Empty;

                for (int i = 0; i < rolCustomerList.Items.Count; i++)
                {
                    AccountNo = string.IsNullOrEmpty(AccountNo) ? rolCustomerList.DataKeys[i].ToString() : AccountNo + "," + rolCustomerList.DataKeys[i].ToString();
                    SortOrder = string.IsNullOrEmpty(SortOrder) ? (i + 1 + replacedOrder).ToString() : SortOrder + "," + (i + 1 + replacedOrder).ToString();
                }

                XmlDocument xmldoc = new XmlDocument();
                BookNoReorderBe _objBookNoReorderBe = new BookNoReorderBe();
                _objBookNoReorderBe.AccountNo = AccountNo;
                _objBookNoReorderBe.Orders = SortOrder;
                xmldoc = _objBookNoReorderBal.Insert(_objBookNoReorderBe, Statement.Change);

                _objBookNoReorderBe = _objiDsignBal.DeserializeFromXml<BookNoReorderBe>(xmldoc);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        public void BindGrid()
        {
            try
            {
                BookNoReorderBe _objBookNoReorderBe = new BookNoReorderBe();
                _objBookNoReorderBe.CycleId = Cycle;
                _objBookNoReorderBe.BU_ID = BU;
                _objBookNoReorderBe.SU_ID = SU;
                _objBookNoReorderBe.SC_ID = SC;
                _objBookNoReorderBe.BookNo = Book;
                _objBookNoReorderBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                _objBookNoReorderBe.PageSize = PageSize;
                DataTable dt = new DataTable();
                BookNoReorderListBe ObjList = _objiDsignBal.DeserializeFromXml<BookNoReorderListBe>(_objBookNoReorderBal.Get(_objBookNoReorderBe, ReturnType.Fetch));
                dt = _objiDsignBal.ConvertListToDataSet<BookNoReorderBe>(ObjList.Items).Tables[0];
                if (ObjList.Items.Count > 0)
                {
                    litCustomersList.Text =Resource.CUSTOMER_LIST_BY_BOOK + ddlBookNo.SelectedItem;
                    divpaging.Visible = divdownpaging.Visible = divgrid.Visible = true;
                    NoRec.Visible = false;
                    if (Session[UMS_Resource.SESSIONREORDER] != null)
                        Session.Remove(UMS_Resource.SESSIONREORDER);

                    Session[UMS_Resource.SESSIONREORDER] = dt;
                    hfTotalRecords.Value = ObjList.Items[0].TotalRecords.ToString();
                    rolCustomerList.DataSource = _objiDsignBal.ConvertListToDataSet<BookNoReorderBe>(ObjList.Items).Tables[0];
                    rolCustomerList.DataBind();
                }
                else
                {
                    divpaging.Visible = divdownpaging.Visible = divgrid.Visible = false;
                    NoRec.Visible = true;
                    hfTotalRecords.Value = ObjList.Items.Count.ToString();
                    rolCustomerList.DataSource = new DataTable();
                    rolCustomerList.DataBind();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        //#region Paging
        //protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        BindCustomersList();
        //        hfPageSize.Value = ddlPageSize.SelectedItem.Text;
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void lkbtnFirst_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        BindCustomersList();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void lkbtnPrevious_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum -= Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        BindCustomersList();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void lkbtnNext_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum += Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        BindCustomersList();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void lkbtnLast_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = hfLastPage.Value;
        //        BindCustomersList();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //private void CreatePagingDT(int TotalNoOfRecs)
        //{
        //    try
        //    {
        //        double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
        //        DataTable dtPages = new DataTable();
        //        dtPages.Columns.Add("PageNo", typeof(int));
        //        int currentpage = Convert.ToInt32(hfPageNo.Value);
        //        lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
        //        _objiDsignBal.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
        //        lblCurrentPage.Text = hfPageNo.Value;
        //        if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
        //        if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //private void BindPagingDropDown(int TotalRecords)
        //{
        //    try
        //    {
        //        _objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //#endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}