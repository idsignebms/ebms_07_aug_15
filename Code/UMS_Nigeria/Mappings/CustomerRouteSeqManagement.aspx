﻿<%@ Page Title="::Customer Route Sequence  Management::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="CustomerRouteSeqManagement.aspx.cs"
    Inherits="UMS_Nigeria.Mappings.CustomerRouteSeqManagement" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/InlineStyles.css" rel="stylesheet" type="text/css" />
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script type="text/javascript">
        function pageLoad() {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        };
        function Validate() {
            //            var IsValid = true;
            //            var ddlbu = document.getElementById('<%=ddlBU.ClientID %>');
            //            var ddlroot = document.getElementById('<%=ddlRouteNo.ClientID %>');
            //            if (DropDownlistValidationlbl(ddlbu, document.getElementById('spanbu'), " Bussiness Unit ") == false) IsValid = false;
            //            if (DropDownlistValidationlbl(ddlroot, document.getElementById('spanRoute'), " Route Number ") == false) IsValid = false;
            //            if (IsValid == false) {
            //                return false;
            //            }
        }     
    </script>
    <%--GridView Drag And Drop--%>
    <%-- /** Re Ordered List styles START  **/--%>
    <style type="text/css">
        
    </style>
    <%--/** Re Ordered List styles END  **/--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <asp:UpdatePanel ID="upnlRouteSequence" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="UpdateProgress" runat="server">
                <ProgressTemplate>
                    <center>
                        <div id="loading-div" class="pbloading">
                            <div id="title-loading" class="pbtitle">
                                BEDC</div>
                            <center>
                                <img src="../images/loading.GIF" class="pbimg"></center>
                            <p class="pbtxt">
                                Loading Please wait.....</p>
                        </div>
                    </center>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
                PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="text_total">
                <div class="text-heading">
                    <asp:Literal ID="litAddGlobalMessage" runat="server" Text="<%$ Resources:Resource, MASTER_CUSTOMERROOTSEQMANAGEMENT%>"></asp:Literal>
                </div>
                <div class="star_text">
                    <asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="dot-line">
            </div>
            <div class="inner-box">
                <div class="inner-box1">
                    <div class="text-inner">
                        <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT_NAME%>"></asp:Literal>
                        <div class="space">
                        </div>
                        <asp:DropDownList ID="ddlBU" runat="server" AutoPostBack="true" CssClass="text-box text-select"
                            OnSelectedIndexChanged="ddlBU_SelectedIndexChanged">
                            <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        </asp:DropDownList>
                        <div class="space">
                        </div>
                        <span id="spanbu" class="spancolor"></span>
                    </div>
                    <div class="text-inner">
                        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER_NAME%>"></asp:Literal>
                        <div class="space">
                        </div>
                        <asp:DropDownList ID="ddlSC" Enabled="false" runat="server" CssClass="text-box text-select">
                            <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        </asp:DropDownList>
                        <div class="space">
                        </div>
                    </div>
                </div>
                <div class="inner-box2">
                    <div class="text-inner">
                        <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT_NAME%>"></asp:Literal>
                        <div class="space">
                        </div>
                        <asp:DropDownList ID="ddlSU" runat="server" Enabled="false" CssClass="text-box text-select"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlSU_SelectedIndexChanged">
                            <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        </asp:DropDownList>
                        <div class="space">
                        </div>
                    </div>
                    <div class="text-inner">
                        <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, ROUTE_NO%>"></asp:Literal>
                        <div class="space">
                        </div>
                        <asp:DropDownList ID="ddlRouteNo" runat="server" CssClass="text-box text-select">
                            <asp:ListItem Value="" Text="--Select"></asp:ListItem>
                        </asp:DropDownList>
                        <div class="space">
                        </div>
                        <span id="spanRoute" class="spancolor"></span>
                    </div>
                </div>
            </div>
            <div class="clr">
            </div>
            <div class="box_total">
                <div class="box_totalTwi_c">
                    <asp:Button ID="btnSave" Text="<%$ Resources:Resource, GO_BUTTON%>" CssClass="box_s"
                        runat="server" OnClick="btnSave_Click" OnClientClick="return Validate();" />
                </div>
            </div>
            <div class="edmcontainer">
                <%--<h3>
                    
                </h3>
                <div class="clear">
                    &nbsp;</div>--%>
                <div class="consumer_feild">
                    <%-- <div class="consume_name" style="width: 100px;">
                        <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT_NAME%>"></asp:Literal>
                    </div>
                    <div class="consume_input" style="width: 162px;">
                        <asp:DropDownList ID="ddlBU" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlBU_SelectedIndexChanged">
                            <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        </asp:DropDownList>
                        <span id="spanbu" class="spancolor"></span>
                    </div>--%>
                    <%--<div class="consume_name" style="width: 140px;">
                       <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT_NAME%>"></asp:Literal>
                    </div>
                    <div class="consume_input" style="width: 162px;">
                        <asp:DropDownList ID="ddlSU" runat="server" Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="ddlSU_SelectedIndexChanged">
                            <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        </asp:DropDownList>
                    </div>--%>
                </div>
                <div class="clear">
                    &nbsp;</div>
                <div class="consumer_feild">
                  <%--  <div class="consume_name" style="width: 100px;">
                        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER_NAME%>"></asp:Literal>
                    </div>
                    <div class="consume_input" style="width: 200px;">
                        <asp:DropDownList ID="ddlSC" Enabled="false" runat="server">
                            <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="consume_name" style="width: 100px;">
                        <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, ROUTE_NO%>"></asp:Literal>
                    </div>
                    <div class="consume_input" style="width: 200px;">
                        <asp:DropDownList ID="ddlRouteNo" runat="server">
                            <asp:ListItem Value="" Text="--Select"></asp:ListItem>
                        </asp:DropDownList>
                        <span id="spanRoute" class="spancolor"></span>
                    </div>--%>
                </div>
                <div class="consumer_feild">
                    <div style="overflow: auto; float: left; height: 42px;">
                        <%--<asp:Button ID="btnSave" Text="<%$ Resources:Resource, GO_BUTTON%>" CssClass="calculate_but"
                            runat="server" OnClick="btnSave_Click" OnClientClick="return Validate();" />--%>
                    </div>
                </div>
                <div id="divgrid" class="grid marg_5" runat="server" style="width: 99% !important;
                    float: left;">
                    <div id="divpaging" runat="server" visible="false">
                        <div class="clear pad_10">
                        </div>
                        <div align="right" class="marg_20t" runat="server" id="divLinks">
                            <h3 class="gridheading fltl" align="left">
                                <asp:Literal ID="litCountriesList" runat="server" Text="<%$ Resources:Resource, ROUTE_WISE%>"></asp:Literal>
                            </h3>
                            <asp:LinkButton ID="lkbtnFirst" runat="server" CssClass="pagingfirst" OnClick="lkbtnFirst_Click">
                                <asp:Literal ID="litFirst" runat="server" Text="<%$ Resources:Resource, FIRST%>"></asp:Literal></asp:LinkButton>
                            &nbsp;|
                            <asp:LinkButton ID="lkbtnPrevious" runat="server" CssClass="pagingfirst" OnClick="lkbtnPrevious_Click">
                                <asp:Literal ID="litPrevious" runat="server" Text="<%$ Resources:Resource, PREVIOUS%>"></asp:Literal></asp:LinkButton>
                            &nbsp;|
                            <asp:Label ID="lblCurrentPage" runat="server" Font-Bold="true"></asp:Label>
                            &nbsp;|
                            <asp:LinkButton ID="lkbtnNext" runat="server" CssClass="pagingfirst" OnClick="lkbtnNext_Click">
                                <asp:Literal ID="litNext" runat="server" Text="<%$ Resources:Resource, NEXT%>"></asp:Literal></asp:LinkButton>
                            &nbsp;|
                            <asp:LinkButton ID="lkbtnLast" runat="server" CssClass="pagingfirst" OnClick="lkbtnLast_Click">
                                <asp:Literal ID="litLast" runat="server" Text="<%$ Resources:Resource, LAST%>"></asp:Literal></asp:LinkButton>
                            &nbsp;|<asp:Literal ID="litPageSize" runat="server" Text="<%$ Resources:Resource, PAGE_SIZE%>"></asp:Literal>
                            <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" CssClass="paging-size"
                                OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="consumer_total">
                </div>
                <%--<div id="Div1" class="grid" runat="server" align="center" style="overflow-x: auto;">
                        <asp:GridView ID="gvConsumerRouteManagement" runat="server" AutoGenerateColumns="False"
                            AlternatingRowStyle-CssClass="color">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, ROUTE_RADIOBUTTON%>" runat="server"
                                    HeaderStyle-Width="16%" HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%#Eval("RouteNo") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                              <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" runat="server"
                                    HeaderStyle-Width="16%" HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                      <asp:Label ID="Label1" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, DOCUMENT_NO%>" runat="server"
                                    HeaderStyle-Width="16%" HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                       <asp:Label ID="Label1" runat="server" Text='<%#Eval("DocumentNo") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, CONTACT_NO%>" runat="server"
                                    HeaderStyle-Width="16%" HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                       <asp:Label ID="Label1" runat="server" Text='<%#Eval("MobileNo") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, SERVICE_ADDRESS%>" runat="server"
                                    HeaderStyle-Width="16%" HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                       <asp:Label ID="Label1" runat="server" Text='<%#Eval("ServiceDetails") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                               
                            </Columns>
                        </asp:GridView>
                        <div class="consumer_total">
                            <div class="pad_10">
                            </div>
                        </div>
                       
                    </div>--%>
                <div class="consumer_total">
                    <div class="pad_10">
                    </div>
                </div>
                <div class="consumer_total">
                    <div class="pad_10">
                    </div>
                </div>
                <div id="divRecords" class="marg_5" runat="server">
                    <ul class="custmrs_list" style="width: 100% !important; margin: 0px;">
                        <li class="th" id="liRevenueDetailsHeading" runat="server">
                            <dl>
                                <dt class="sno" style="width: 50px;">S.No</dt>
                                <dt class="name" style="width: 150px;">
                                    <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>" /></dt>
                                <dt class="name" style="width: 150px;">
                                    <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, CUSTOMER_UNIQUE_NO%>" /></dt>
                                <dt class="name" style="width: 150px;">
                                    <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, NAME%>" /></dt>
                                <dt class="name" style="width: 150px;">
                                    <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, DOCUMENT_NO%>" /></dt>
                                <dt class="addrs" style="width: 150px;">
                                    <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, CONTACT_NO%>" /></dt>
                                <dt class="addrs" style="border-right: medium none; width: 317px;">
                                    <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>" /></dt>
                            </dl>
                        </li>
                        <asp:ReorderList ID="rolCustomerSeq" runat="server" CallbackCssStyle="grid" DataKeyField="CustomerUniqueNo"
                            Width="100%" OnItemReorder="rolCustomer_ItemReorder" BorderWidth="0" ClientIDMode="AutoID"
                            PostBackOnReorder="true">
                            <DragHandleTemplate>
                                <div id="drag" class="dragHandle" title="Drag Record" runat="server">
                                </div>
                            </DragHandleTemplate>
                            <ItemTemplate>
                                <li class="td" style="height: auto;">
                                    <dl>
                                        <dt class="sno" style="width: 45px;">
                                            <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RouteNo")%>'></asp:Label>
                                        </dt>
                                        <dt class="name" style="width: 150px;">
                                            <asp:Label ID="Label1" runat="server" Text='<%#Eval("AccountNo") %>'></asp:Label>
                                        </dt>
                                        <dt class="name" style="width: 150px;">
                                            <asp:Label ID="Label2" runat="server" Text='<%#Eval("CustomerUniqueNo") %>'></asp:Label>
                                        </dt>
                                        <dt class="name" style="width: 150px;">
                                            <asp:Label ID="lblCode" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        </dt>
                                        <dt class="name" style="width: 150px;">
                                            <asp:Label ID="lblOrder" runat="server" Text='<%#Eval("DocumentNo") %>'></asp:Label>
                                        </dt>
                                        <dt class="name" style="width: 150px;">
                                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("MobileNo") %>'></asp:Label>
                                        </dt>
                                        <dt style="width: 310px; border-right: medium none;" class="addrs">
                                            <asp:Label ID="lblCompany" runat="server" Text='<%#Eval("ServiceDetails") %>'></asp:Label>
                                        </dt>
                                    </dl>
                                </li>
                            </ItemTemplate>
                        </asp:ReorderList>
                    </ul>
                </div>
                <div class="consumer_total">
                    <div class="pad_10">
                    </div>
                </div>
                <div id="NoRec" class="edmcontainer" runat="server" visible="false">
                    <center>
                        <br />
                        There is no routes available
                        <br />
                    </center>
                </div>
                <asp:HiddenField ID="hfPageNo" runat="server" />
                <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                <asp:HiddenField ID="hfLastPage" runat="server" />
                <asp:HiddenField ID="hfPageSize" runat="server" />
                <div class="clear">
                    &nbsp;</div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
