﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for CustomerRouteSeqManagement
                     
 Developer        : Ramadevi-ID075
 Creation Date    : 01-04-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigriaDAL;
using UMS_NigeriaBE;
using Resources;
using iDSignHelper;
using System.Data;
using System.Drawing;
using UMS_NigeriaBAL;
using System.Web.Services;
using System.Xml;
using AjaxControlToolkit;

namespace UMS_Nigeria.Mappings
{
    public partial class CustomerRouteSeqManagement : System.Web.UI.Page
    {
        #region Members
        CommonMethods _objCommonMethods = new CommonMethods();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        ConsumerBal _ObjConsumerBAL = new ConsumerBal();
        ConsumerBe _objConsumerBe = new ConsumerBe();
        string Key = UMS_Resource.CUSTOMERROUTEMANAGEMENT;
        public int PageNum;
        #endregion

        #region Properties

        public int PageSize
        {
            get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string path = string.Empty;
                path = _objCommonMethods.GetPagePath(this.Request.Url);
                if (_objCommonMethods.IsAccess(path))
                {

                    if (HttpContext.Current.Session[UMS_Resource.SESSION_LOGINID] != null)
                    {
                        hfPageNo.Value = Constants.pageNoValue.ToString();
                        _objCommonMethods.BindBusinessUnits(ddlBU, string.Empty, true);
                        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        {
                            ddlBU.SelectedIndex = ddlBU.Items.IndexOf(ddlBU.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                            ddlBU.Enabled = false;
                            ddlBU_SelectedIndexChanged(ddlBU, new EventArgs());
                        }
                        _objCommonMethods.BindRoutes(ddlRouteNo, Session[UMS_Resource.SESSION_USER_BUID].ToString(), true);
                        divRecords.Visible = false;
                        GetCustomerRouteManagement();
                        BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    }
                }
                else
                {
                    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            hfPageNo.Value = Constants.pageNoValue.ToString();
            hfTotalRecords.Value = Constants.PageSizeStarts.ToString();
            GetCustomerRouteManagement();
            CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
        }
        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                GetCustomerRouteManagement();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                GetCustomerRouteManagement();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                GetCustomerRouteManagement();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                GetCustomerRouteManagement();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                GetCustomerRouteManagement();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlBU_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlBU.SelectedItem.Value != "")
            //    _objCommonMethods.BindServiceUnits(ddlSU, ddlBU.SelectedItem.Value, true);
            //else
            //{
            //    if (ddlSU.Items.Count > 0)
            //    {
            //        ddlSU.Items.Clear();
            //        if (ddlSC.Items.Count > 0)
            //        {
            //            ddlSC.Items.Clear();
            //        }
            //    }
            //}

            if (ddlBU.SelectedIndex > 0)
            {
                _objCommonMethods.BindServiceUnits(ddlSU, ddlBU.SelectedItem.Value, true);
                ddlSU.Enabled = true;
                ddlSC.SelectedIndex = ddlSU.SelectedIndex = 0;
                ddlSC.Enabled = false;
            }
            else
            {
                ddlSU.SelectedIndex = ddlSC.SelectedIndex = 0;
                ddlSC.Enabled = ddlSU.Enabled = false;
            }

        }

        protected void ddlSU_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlSU.SelectedItem.Value != "")
            //    _objCommonMethods.BindServiceCenters(ddlSC, ddlSU.SelectedItem.Value, true);
            //else
            //{

            //    if (ddlSC.Items.Count > 0)
            //    {
            //        ddlSC.Items.Clear();
            //    }

            //}
            if (ddlSU.SelectedIndex > 0)
            {
                _objCommonMethods.BindServiceCenters(ddlSC, ddlSU.SelectedItem.Value, true);
                ddlSC.Enabled = true;
            }
            else
            {
                ddlSC.SelectedIndex = 0;
                ddlSC.Enabled = false;
            }

        }

        protected void rolCustomer_ItemReorder(object sender, AjaxControlToolkit.ReorderListItemReorderEventArgs e)
        {
            if (Session[UMS_Resource.SESSIONREORDER] != null)
            {
                DataTable DtOldSession = new DataTable();
                DataTable DtNewSession = new DataTable();

                Label lblOld = (Label)rolCustomerSeq.Items[e.OldIndex].FindControl("lblRow");
                Label lblNew = (Label)rolCustomerSeq.Items[e.NewIndex].FindControl("lblRow");
                int replacedOrder = Convert.ToInt32(lblOld.Text);
                int selectedOrder = Convert.ToInt32(lblNew.Text);

                DtOldSession = (DataTable)Session[UMS_Resource.SESSIONREORDER];
                DtNewSession = DtOldSession.Copy();
                DataRow NewRow = DtOldSession.NewRow();
                NewRow["AccountNo"] = DtNewSession.Rows[e.OldIndex]["AccountNo"];
                NewRow["CustomerUniqueNo"] = DtNewSession.Rows[e.OldIndex]["CustomerUniqueNo"];
                NewRow["RouteNo"] = DtNewSession.Rows[e.OldIndex]["RouteNo"];
                NewRow["Name"] = DtNewSession.Rows[e.OldIndex]["Name"];
                NewRow["DocumentNo"] = DtNewSession.Rows[e.OldIndex]["DocumentNo"];
                NewRow["MobileNo"] = DtOldSession.Rows[e.OldIndex]["MobileNo"];
                NewRow["ServiceDetails"] = DtOldSession.Rows[e.OldIndex]["ServiceDetails"];
                DtOldSession.Rows[e.OldIndex].Delete();
                DtOldSession.Rows.InsertAt(NewRow, e.NewIndex);
                Session[UMS_Resource.SESSIONREORDER] = DtOldSession;
                rolCustomerSeq.DataSource = DtOldSession;
                rolCustomerSeq.DataBind();
                ReorderListItem SelectedItem = (ReorderListItem)rolCustomerSeq.Items[e.NewIndex];

                UpdateReorderedList();
                GetCustomerRouteManagement();
            }


        }
        #endregion

        #region Methods
        private void UpdateReorderedList()
        {
            int replacedOrder = 0;
            int pageNo = Convert.ToInt32(hfPageNo.Value);
            int pageSize = this.PageSize;

            replacedOrder = (pageNo * pageSize) - pageSize;

            string RouteIds, Orders;
            RouteIds = Orders = string.Empty;

            for (int i = 0; i < rolCustomerSeq.Items.Count; i++)
            {
                RouteIds = string.IsNullOrEmpty(RouteIds) ? rolCustomerSeq.DataKeys[i].ToString() : RouteIds + "," + rolCustomerSeq.DataKeys[i].ToString();
                Orders = string.IsNullOrEmpty(Orders) ? (i + 1 + replacedOrder).ToString() : Orders + "," + (i + 1 + replacedOrder).ToString();
            }

            XmlDocument xmldoc = new XmlDocument();
            xmldoc = _ObjConsumerBAL.UpdateRouteSequenceBAL(RouteIds, Orders);
            _objConsumerBe = _ObjiDsignBAL.DeserializeFromXml<ConsumerBe>(xmldoc);

            _objConsumerBe = new ConsumerBe();

        }
        private void GetCustomerRouteManagement()
        {
            _objConsumerBe.BU_ID = ddlBU.Items.Count == 0 ? "" : ddlBU.SelectedItem.Value;
            _objConsumerBe.SU_ID = ddlSU.Items.Count > 0 ? ddlSU.SelectedItem.Value : "";
            _objConsumerBe.ServiceCenterId = ddlSC.Items.Count == 0 ? "" : ddlSC.SelectedItem.Value;
            _objConsumerBe.RouteNo = ddlRouteNo.Items.Count == 0 ? 0 : (ddlRouteNo.SelectedItem.Value == "" ? 0 : Convert.ToInt32(ddlRouteNo.SelectedItem.Value));
            _objConsumerBe.PageNo = Convert.ToInt32(hfPageNo.Value);
            _objConsumerBe.PageSize = PageSize;

            _objConsumerBe.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
            DataTable dt = new DataTable();

            ConsumerListBe ObjList = _ObjiDsignBAL.DeserializeFromXml<ConsumerListBe>(_ObjConsumerBAL.GetCustomerRouteManagementBAL(_objConsumerBe));
            dt = _ObjiDsignBAL.ConvertListToDataSet<ConsumerBe>(ObjList.items).Tables[0];
            if (ObjList.items.Count > 0)
            {
                divpaging.Visible = divRecords.Visible = true;
                NoRec.Visible = false;
                if (Session[UMS_Resource.SESSIONREORDER] != null)
                    Session.Remove(UMS_Resource.SESSIONREORDER);

                Session[UMS_Resource.SESSIONREORDER] = dt;
                hfTotalRecords.Value = ObjList.items[0].TotalRecords.ToString();
            }
            else
            {
                divpaging.Visible = divRecords.Visible = false;
                NoRec.Visible = true;
            }

            rolCustomerSeq.DataSource = _ObjiDsignBAL.ConvertListToDataSet<ConsumerBe>(ObjList.items).Tables[0];
            rolCustomerSeq.DataBind();
        }
        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                _ObjiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                _objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
                //ddlPageSize.Items.Clear();
                //if (TotalRecords < Constants.PageSizeStarts)
                //{
                //    ListItem item = new ListItem(Constants.PageSizeStarts.ToString(), Constants.PageSizeStarts.ToString());
                //    ddlPageSize.Items.Add(item);
                //}
                //else
                //{
                //    int previousSize = Constants.PageSizeStarts;
                //    for (int i = Constants.PageSizeStarts; i <= TotalRecords; i = i + Constants.PageSizeIncrement)
                //    {
                //        ListItem item = new ListItem(i.ToString(), i.ToString());
                //        ddlPageSize.Items.Add(item);
                //        previousSize = i;
                //    }
                //    if (previousSize < TotalRecords)
                //    {
                //        ListItem item = new ListItem((previousSize + Constants.PageSizeIncrement).ToString(), (previousSize + Constants.PageSizeIncrement).ToString());
                //        ddlPageSize.Items.Add(item);
                //    }
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion
    }
}