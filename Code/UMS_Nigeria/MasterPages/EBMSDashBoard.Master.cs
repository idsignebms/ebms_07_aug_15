﻿using System;
using iDSignHelper;
using Resources;
using UMS_NigeriaBE;
using System.Xml;
using UMS_NigeriaBAL;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using UMS_NigriaDAL;
using System.Web.Services;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web.UI.HtmlControls;
namespace UMS_Nigeria.MasterPages
{
    public partial class EBMSDashBoard : System.Web.UI.MasterPage
    {
        #region Members
        UMS_Service _ObjUMS_Service = new UMS_Service();
        SideMenusBAL _objMenuBAL = new SideMenusBAL();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        LoginPageBAL _objLoginPageBal = new LoginPageBAL();
        string Key = UMS_Resource.HOMEPAGE_AFTERLOGIN;

        DataSet dsSettingMenu = new DataSet();

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                //lblDateTime.Text = DateTime.Now.ToString("ddd,MMM,d,yyyy");
                // lblTime.Text = DateTime.Now.ToShortTimeString();
                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "SessionTimeout", "LoadStartTime();", true);
                if (!IsPostBack)
                {

                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Cache.SetExpires(DateTime.Now);
                    Response.Cache.SetNoServerCaching();
                    Response.Cache.SetNoStore();
                    if (Session[UMS_Resource.SESSION_UserName] != null)
                    {
                        lblUserName.Text = Session[UMS_Resource.SESSION_UserName].ToString();
                        //      lblDesignation.Text = Session[UMS_Resource.SESSION_DESIGNATION].ToString();
                        hfUserName.Value = Session[UMS_Resource.SESSION_UserName].ToString();
                        hfLoginId.Value = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        hfRoleId.Value = Session[UMS_Resource.SESSION_ROLEID].ToString();
                        hfDesignation.Value = Session[UMS_Resource.SESSION_DESIGNATION].ToString();
                        hfUserEmailId.Value = Session[UMS_Resource.SESSION_USER_EMAILID].ToString();
                        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                            hfBU_ID.Value = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                    }
                    if (Session[UMS_Resource.SESSION_USER_BUNAME] != null)
                        lblBusinessUnit.Text = Session[UMS_Resource.SESSION_USER_BUNAME].ToString();

                    BindSideMenuForSettings();

                    //if (Session[UMS_Resource.SESSION_LOGINID] == null)
                    //    Response.Redirect(UMS_Resource.DEFAULT_PAGE);
                }
                addBlurAtt(UMSNigeriaBody);
            }
            else
                Response.Redirect(UMS_Resource.DEFAULT_PAGE, false);
        }

        protected void lbtnLogout_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Abandon();
                //Response.Redirect(UMS_Resource.StaffLogin_Page);
                Response.Redirect(UMS_Resource.MAINTENANCE_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void imgLogout_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            Session.Abandon();
            Response.Redirect(UMS_Resource.DEFAULT_PAGE, false);
        }

        protected void lkLBtnogout_Click(object sender, EventArgs e)
        {
            mpeDeActive.Show();
        }
        protected void lkLogout_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect(UMS_Resource.DEFAULT_PAGE, false);
        }
        protected void Page_PreInit()
        {
            try
            {
                //if (Session[UMS_Resource.SESSION_ROLEID] == null || Session[UMS_Resource.SESSION_LOGINID] == null)
                //    //Response.Redirect(UMS_Resource.StaffLogin_Page);
                //    Response.Redirect(UMS_Resource.MAINTENANCE_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnReLogin_Click(object sender, EventArgs e)
        {//not in use we are using servicemetod//ID065
            try
            {
                if (hfLoginId.Value == txtUserName.Text)
                {
                    XmlDocument xml = new XmlDocument();
                    LoginPageBE objLoginPageBE = new LoginPageBE();
                    objLoginPageBE.UserName = txtUserName.Text;
                    objLoginPageBE.Password = _ObjiDsignBAL.AESEncryptString(txtPassword.Text);
                    xml = _objLoginPageBal.Login(objLoginPageBE, ReturnType.Get);
                    //xml = _ObjUMS_Service.Login(this.UserName, this.Password);
                    objLoginPageBE = _ObjiDsignBAL.DeserializeFromXml<LoginPageBE>(xml);

                    if (objLoginPageBE.IsSuccess)
                    {
                        Session[UMS_Resource.SESSION_UserName] = objLoginPageBE.UserName;
                        Session[UMS_Resource.SESSION_LOGINID] = txtUserName.Text;
                        Session[UMS_Resource.SESSION_ROLEID] = objLoginPageBE.RoleId;
                        Session[UMS_Resource.SESSION_DESIGNATION] = objLoginPageBE.Designation;
                        Session[UMS_Resource.SESSION_USER_EMAILID] = objLoginPageBE.PrimaryEmailId;
                        Session[UMS_Resource.SESSION_USER_BUID] = hfBU_ID.Value;
                    }
                    else
                    {
                        //mpeReLoginpopup.Show();                        
                        mpeReLoginWarning.Show();
                    }
                }
                else
                {
                    //mpeReLoginpopup.Show();                    
                    mpeReLoginWarning.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }


        protected void btnReLoginWarningOk_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Abandon();
                Response.Redirect(UMS_Resource.DEFAULT_PAGE, false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void btnReLoginWarningCancel_Click(object sender, EventArgs e)
        {
            try
            {
                mpeReLoginpopup.Show();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                //_ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void addBlurAtt(Control cntrl)
        {
            if (cntrl.Controls.Count > 0)
            {
                foreach (Control childControl in cntrl.Controls)
                {
                    addBlurAtt(childControl);
                }
            }
            if (cntrl.GetType() == typeof(TextBox))
            {
                TextBox TempTextBox = (TextBox)cntrl;
                TempTextBox.Attributes.Add("onFocus", "DoFocus(this);");
                //TempTextBox.Attributes.Add("onBlur", "DoBlur(this);");
            }
            if (cntrl.GetType() == typeof(ListBox))
            {
                ListBox TempTextBox = (ListBox)cntrl;
                TempTextBox.Attributes.Add("onFocus", "DoFocus(this);");
                //TempTextBox.Attributes.Add("onBlur", "DoBlur(this);");
            }
        }

        public string MenuBinding()
        {
            try
            {
                StringBuilder menuString = new StringBuilder();
                int itemsPerColumn = 10;
                int subMenuIndex = 0;
                int colCount = 1;
                int menuCount = 1;

                #region Call of menu data

                SideMenusBE _ObjSideMenusBE = new SideMenusBE();
                XmlDocument resultXml = new XmlDocument();

                _ObjSideMenusBE.Role_Id = Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID]);
                _ObjSideMenusBE.PageName = "MasterPage";

                resultXml = _objMenuBAL.GetNewMenusNewBAL(_ObjSideMenusBE);
                SideMenusListBE _ObjSideMenusListBE = new SideMenusListBE();
                _ObjSideMenusListBE = _ObjiDsignBAL.DeserializeFromXml<SideMenusListBE>(resultXml);

                MainMenuListBE _ObjMainMenuListBE = new MainMenuListBE();
                _ObjMainMenuListBE = _ObjiDsignBAL.DeserializeFromXml<MainMenuListBE>(resultXml);

                #endregion

                #region Menu binding starts

                #region Menu header
                menuString.Append("<li><img src='../images/home-icon.png'/>"
                              + "<a href='../Home.aspx'>Home</a></li>");
                #endregion

                #region Main menu loop

                foreach (SideMenusBE list in _ObjMainMenuListBE.items)
                {
                    subMenuIndex = 1;
                    colCount = 1;
                    menuString.Append("<li class='dropdown'>");

                    #region image binding
                    menuString.Append("<img src='../images/" + list.Menu_Image + "'/>");
                    #endregion

                    menuString.Append(" <a class='dropdown-toggle' data-toggle='dropdown' href='#'>" + list.Main_Menu_Text + "<b class='caret'></b></a>");

                    #region Select submenus by main menu Id

                    var drSubMenuDetails = from x in _ObjSideMenusListBE.items.AsEnumerable()
                                           where x.Main_Menu_Id == list.Main_Menu_Id
                                           select new
                                           {
                                               x.SubMenu_Text,
                                               x.Path,
                                               x.Main_Menu_Id,
                                               x.Page_Id
                                           };


                    if (menuCount == 3)
                        menuString.Append("<ul class='dropdown-menu mega-menu_one'>");
                    else if (menuCount == 1)
                        menuString.Append("<ul class='dropdown-menu mega-menu_four'>");
                    else if (menuCount == 4)
                        menuString.Append("<ul class='dropdown-menu mega-menu_three'>");
                    else if (menuCount == 5)
                        menuString.Append("<ul class='dropdown-menu mega-menu_two'>");
                    else
                        menuString.Append("<ul class='dropdown-menu mega-menu'>");


                    foreach (var SubMenuDetails in drSubMenuDetails)
                    {
                        if (colCount == 1)
                        {
                            menuString.Append("<li class='mega-menu-column'><ul>");
                        }

                        menuString.Append("<span class='span-arrow-c'></span><li><a href='" + SubMenuDetails.Path + "'> " + SubMenuDetails.SubMenu_Text + "</a></li>");
                        colCount++;

                        if (colCount == itemsPerColumn || drSubMenuDetails.Count() == subMenuIndex)
                        {
                            menuString.Append("</ul></li>");
                            colCount = 1;
                        }
                        subMenuIndex++;
                    }
                    menuString.Append("</ul>");
                    menuCount++;
                    #endregion
                }
                #endregion

                #region Menu footer
                menuString.Append("</li>");
                #endregion

                #endregion
                return menuString.ToString();

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            return string.Empty;
        }
        //public string MenuBinding()
        //{
        //    try
        //    {
        //        StringBuilder menuString = new StringBuilder();
        //        int itemsPerColumn = 10;
        //        int subMenuIndex = 0;
        //        int colCount = 1;
        //        int menuCount = 1;
        //        #region CssList

        //        string[] cssList = new string[13];
        //        //cssList[0] = "full-width";
        //        //cssList[1] = "top-heading";
        //        //cssList[2] = "caret";
        //        //cssList[3] = "dropdown";
        //        //cssList[4] = "dd-inner";
        //        //cssList[5] = "column";
        //        //cssList[6] = "span-arrow";
        //        //cssList[7] = "span-arrow-b";
        //        //cssList[8] = "dropdown right-aligned";
        //        //cssList[9] = "dropdown offset300";
        //        //cssList[10] = "span-arrow-a";
        //        cssList[0] = "full-width";
        //        cssList[1] = "top-heading";
        //        cssList[2] = "caret";
        //        cssList[3] = "dropdown";
        //        cssList[4] = "dd-inner";
        //        cssList[5] = "column";
        //        cssList[6] = "span-arrow";
        //        cssList[7] = "dropdown right_aligned_rpt";
        //        cssList[8] = "dropdown";
        //        cssList[9] = "dropdown offsetBillsubdrop";
        //        //cssList[10] = "span-arrow-a";
        //        cssList[11] = "column_a";
        //        cssList[12] = "span-arrow-c";
        //        #endregion

        //        #region Call of menu data

        //        SideMenusBE _ObjSideMenusBE = new SideMenusBE();
        //        XmlDocument resultXml = new XmlDocument();

        //        _ObjSideMenusBE.Role_Id = Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID]);
        //        _ObjSideMenusBE.PageName = "MasterPage";

        //        resultXml = _objMenuBAL.GetNewMenusNewBAL(_ObjSideMenusBE);
        //        SideMenusListBE _ObjSideMenusListBE = new SideMenusListBE();
        //        _ObjSideMenusListBE = _ObjiDsignBAL.DeserializeFromXml<SideMenusListBE>(resultXml);

        //        MainMenuListBE _ObjMainMenuListBE = new MainMenuListBE();
        //        _ObjMainMenuListBE = _ObjiDsignBAL.DeserializeFromXml<MainMenuListBE>(resultXml);

        //        #endregion

        //        #region Menu binding starts

        //        #region Menu header
        //        menuString.Append("<ul>"
        //                      + "<li class='" + cssList[0] + "'><img src='../images/home-icon.png'/>"
        //                      + "<a href='../Home.aspx'>Home</a></li>");
        //        #endregion

        //        #region Main menu loop

        //        foreach (SideMenusBE list in _ObjMainMenuListBE.items)
        //        {
        //            subMenuIndex = 1;
        //            colCount = 1;
        //            menuString.Append("<li><span class='" + cssList[1] + "'> ");

        //            #region image binding
        //            menuString.Append("<img src='../images/" + list.Menu_Image + "'/>");
        //            #endregion

        //            menuString.Append("<a href='#'>" + list.Main_Menu_Text + "</a></span>"
        //               + "<i class='" + cssList[2] + "'></i>");
        //            if (menuCount == 3)
        //                menuString.Append("<div class='" + cssList[9] + "'>");
        //            else if (menuCount == 6)
        //                menuString.Append("<div class='" + cssList[7] + "'>");
        //            else if (menuCount == 7)
        //                menuString.Append("<div class='" + cssList[8] + "'>");
        //            else
        //                menuString.Append("<div class='" + cssList[3] + "'>");

        //            menuString.Append("<div class='" + cssList[4] + "'>");



        //            #region Select submenus by main menu Id

        //            var drSubMenuDetails = from x in _ObjSideMenusListBE.items.AsEnumerable()
        //                                   where x.Main_Menu_Id == list.Main_Menu_Id
        //                                   select new
        //                                   {
        //                                       x.SubMenu_Text,
        //                                       x.Path,
        //                                       x.Main_Menu_Id,
        //                                       x.Page_Id
        //                                   };



        //            foreach (var SubMenuDetails in drSubMenuDetails)
        //            {
        //                if (colCount == 1)
        //                {
        //                    menuString.Append("<div class='" + cssList[5] + "'>");
        //                    if (menuCount == 3)
        //                        menuString.Append("<span class='" + cssList[10] + "'></span>");
        //                    else if (menuCount == 6 || menuCount == 7)
        //                        menuString.Append("<span class='" + cssList[7] + "'></span>");
        //                    if (list.Main_Menu_Id == 8)
        //                        menuString.Append("<span class='" + cssList[12] + "'></span>");
        //                    else
        //                        menuString.Append("<span class='" + cssList[6] + "'></span>");
        //                    menuString.Append("<div>");

        //                }
        //                menuString.Append("<a href='" + SubMenuDetails.Path + "'> " + SubMenuDetails.SubMenu_Text + "</a>");
        //                colCount++;
        //                if (colCount == itemsPerColumn || drSubMenuDetails.Count() == subMenuIndex)
        //                {
        //                    menuString.Append("</div>"
        //                                   + "</div>");
        //                    colCount = 1;
        //                }
        //                subMenuIndex++;
        //            }
        //            menuString.Append("</div> </div> </li>");
        //            menuCount++;
        //            #endregion
        //        }
        //        #endregion

        //        #region Menu footer
        //        menuString.Append("</ul>");
        //        #endregion

        //        #endregion

        //        return menuString.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //    return string.Empty;
        //}

        private void BindSideMenuForSettings()
        {
            SideMenusBE _ObjSideMenusBE = new SideMenusBE();
            SideMenusListBE _ObjSideMenusListBE = new SideMenusListBE();
            XmlDocument resultXml = new XmlDocument();
            DataSet ds = new DataSet();
            ds.ReadXml(Server.MapPath(ConfigurationSettings.AppSettings["ConfigurationSettingsMenu"]));

            _ObjSideMenusBE.Role_Id = Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID]);
            _ObjSideMenusBE.PageName = Constants.ConfigurationSettings;

            resultXml = _objMenuBAL.GetConfigurationSettingsMenu(_ObjSideMenusBE);
            _ObjSideMenusListBE = _ObjiDsignBAL.DeserializeFromXml<SideMenusListBE>(resultXml);
            dsSettingMenu = _ObjiDsignBAL.ConvertListToDataSet<SideMenusBE>(_ObjSideMenusListBE.items);

            if (ds.Tables[0].Rows.Count > 0)
            {
                rptrMainMasterMenu.DataSource = ds.Tables[0];
                rptrMainMasterMenu.DataBind();
            }
            else
            {
                rptrMainMasterMenu.DataSource = new DataTable();
                rptrMainMasterMenu.DataBind();
            }
            //selectMenu();
        }
        //public void selectMenu()
        //{
        //   // Repeater mainMenu = (Repeater)this.Master.FindControl("rptrMainMasterMenu");
        //    //HtmlContainerControl div156 = (HtmlContainerControl)rptrMainMasterMenu.FindControl("156");
        //    //if (div156 != null)
        //    //    div156.Style.Add("display", "block");
        //    foreach (RepeaterItem item in rptrMainMasterMenu.Items)
        //    {
        //        //Label la = (Label)Repeater1.Items[e.Item.ItemIndex].FindControl("label1");
        //        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
        //        {
        //            HtmlContainerControl div156 = (HtmlContainerControl)item.FindControl("156");
        //            div156.Style.Add("display", "block");
        //            break;
        //            //lbl.Text = "do something to your label";
        //        }
        //    }
        //}
        protected void rptrMainMasterMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    LinkButton lbtnMainMenu = (LinkButton)e.Item.FindControl("lbtnMainMenu");
                    Repeater rptrSubMenu = (Repeater)e.Item.FindControl("rptrSubMenu");
                    HtmlContainerControl divSubMenu = (HtmlContainerControl)e.Item.FindControl("divSubMenu");

                    e.Item.Visible = false;

                    string PageIds = lbtnMainMenu.CommandArgument;
                    divSubMenu.ID = PageIds.Split(',')[0];
                    //HiddenField hfMenuId = (HiddenField)this.Master.FindControl("hfMenuId");
                    //hfMenuId.Value = "156";
                    if (!string.IsNullOrEmpty(PageIds))
                    {
                        var res = (from x in dsSettingMenu.Tables[0].AsEnumerable()
                                   where PageIds.Contains(x.Field<string>("Main_Menu_Text"))
                                   select new
                                   {
                                       Path = x.Field<string>("Path"),
                                       SubMenu_Text = x.Field<string>("SubMenu_Text"),
                                       Main_Menu_Text = x.Field<string>("Main_Menu_Text"),
                                   }).ToList();

                        if (res.Count > 0)
                        {
                            e.Item.Visible = true;

                            rptrSubMenu.DataSource = res;
                            rptrSubMenu.DataBind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion
    }
}