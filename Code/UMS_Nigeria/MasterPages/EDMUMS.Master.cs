﻿using System;
using iDSignHelper;
using Resources;
using UMS_NigeriaBE;
using System.Xml;
using UMS_NigeriaBAL;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using UMS_NigriaDAL;
using System.Web.Services;

namespace UMS_Nigeria.MasterPages
{
    public partial class EDMUMS : System.Web.UI.MasterPage
    {
        #region Members
        UMS_Service _ObjUMS_Service = new UMS_Service();
        SideMenusBAL _objMenuBAL = new SideMenusBAL();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        LoginPageBAL _objLoginPageBal = new LoginPageBAL();
        string Key = UMS_Resource.HOMEPAGE_AFTERLOGIN;
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            //lblDateTime.Text = DateTime.Now.ToString("ddd,MMM,d,yyyy");
            // lblTime.Text = DateTime.Now.ToShortTimeString();
            // ScriptManager.RegisterStartupScript(Page, Page.GetType(), "SessionTimeout", "LoadStartTime();", true);
            if (!IsPostBack)
            {
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Cache.SetExpires(DateTime.Now);
                Response.Cache.SetNoServerCaching();
                Response.Cache.SetNoStore();
                if (Session[UMS_Resource.SESSION_UserName] != null)
                {
                    lblUserName.Text = Session[UMS_Resource.SESSION_UserName].ToString();
                    //      lblDesignation.Text = Session[UMS_Resource.SESSION_DESIGNATION].ToString();
                    hfUserName.Value = Session[UMS_Resource.SESSION_UserName].ToString();
                    hfLoginId.Value = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    hfRoleId.Value = Session[UMS_Resource.SESSION_ROLEID].ToString();
                    hfDesignation.Value = Session[UMS_Resource.SESSION_DESIGNATION].ToString();
                    hfUserEmailId.Value = Session[UMS_Resource.SESSION_USER_EMAILID].ToString();
                    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        hfBU_ID.Value = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                }
                if (Session[UMS_Resource.SESSION_USER_BUNAME] != null)
                    lblBusinessUnit.Text = Session[UMS_Resource.SESSION_USER_BUNAME].ToString();

                //if (Session[UMS_Resource.SESSION_LOGINID] == null)
                //    Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
            addBlurAtt(UMSNigeriaBody);
        }

        protected void lbtnLogout_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Abandon();
                //Response.Redirect(UMS_Resource.StaffLogin_Page);
                Response.Redirect(UMS_Resource.MAINTENANCE_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void Page_PreInit()
        {
            try
            {
                if (Session[UMS_Resource.SESSION_ROLEID] == null || Session[UMS_Resource.SESSION_LOGINID] == null)
                    //Response.Redirect(UMS_Resource.StaffLogin_Page);
                    Response.Redirect(UMS_Resource.MAINTENANCE_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnReLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (hfLoginId.Value == txtUserName.Text)
                {
                    XmlDocument xml = new XmlDocument();
                    LoginPageBE objLoginPageBE = new LoginPageBE();
                    objLoginPageBE.UserName = txtUserName.Text;
                    objLoginPageBE.Password = _ObjiDsignBAL.AESEncryptString(txtPassword.Text);
                    xml = _objLoginPageBal.Login(objLoginPageBE, ReturnType.Get);
                    //xml = _ObjUMS_Service.Login(this.UserName, this.Password);
                    objLoginPageBE = _ObjiDsignBAL.DeserializeFromXml<LoginPageBE>(xml);

                    if (objLoginPageBE.IsSuccess)
                    {
                        Session[UMS_Resource.SESSION_UserName] = objLoginPageBE.UserName;
                        Session[UMS_Resource.SESSION_LOGINID] = txtUserName.Text;
                        Session[UMS_Resource.SESSION_ROLEID] = objLoginPageBE.RoleId;
                        Session[UMS_Resource.SESSION_DESIGNATION] = objLoginPageBE.Designation;
                        Session[UMS_Resource.SESSION_USER_EMAILID] = objLoginPageBE.PrimaryEmailId;
                        Session[UMS_Resource.SESSION_USER_BUID] = hfBU_ID.Value;
                    }
                    else
                    {
                        //mpeReLoginpopup.Show();                        
                        mpeReLoginWarning.Show();
                    }
                }
                else
                {
                    //mpeReLoginpopup.Show();                    
                    mpeReLoginWarning.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }


        protected void btnReLoginWarningOk_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect(UMS_Resource.DEFAULT_PAGE, false);
        }
        protected void btnReLoginWarningCancel_Click(object sender, EventArgs e)
        {
            mpeReLoginpopup.Show();
        }


        protected void lkLogout_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect(UMS_Resource.DEFAULT_PAGE, false);
        }

        protected void imgLogout_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            Session.Abandon();
            Response.Redirect(UMS_Resource.DEFAULT_PAGE, false);
        }
        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                //_ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public string UserNavigation()
        {
            try
            {
                SideMenusBE _ObjSideMenusBE = new SideMenusBE();

                //  _ObjSideMenusBe.Role_Id = 8;
                XmlDocument resultXml = new XmlDocument();
                _ObjSideMenusBE.Role_Id = Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID]);
                _ObjSideMenusBE.PageName = "MasterPage";
                resultXml = _objMenuBAL.GetNewMenusBAL(_ObjSideMenusBE);

                SideMenusListBE _ObjSideMenusListBE = new SideMenusListBE();
                _ObjSideMenusListBE = _ObjiDsignBAL.DeserializeFromXml<SideMenusListBE>(resultXml);

                MainMenuListBE _ObjMainMenuListBE = new MainMenuListBE();
                _ObjMainMenuListBE = _ObjiDsignBAL.DeserializeFromXml<MainMenuListBE>(resultXml);


                string strNavigation = string.Empty;
                for (var i = 0; i < _ObjMainMenuListBE.items.Count; i++)
                {
                    _ObjSideMenusBE = (SideMenusBE)_ObjMainMenuListBE.items[i];
                    string path = _ObjSideMenusBE.Path;
                    string name = _ObjSideMenusBE.Main_Menu_Text;
                    int MainMenuId = _ObjSideMenusBE.Main_Menu_Id;
                    int submenucount = 0;

                    submenucount = (from x in _ObjSideMenusListBE.items.AsEnumerable()
                                    where x.Main_Menu_Id == MainMenuId
                                    select x).Count();

                    //string cssclass = "nav-home";
                    string cssclass = _ObjSideMenusBE.CssClassName;
                    if (cssclass == "")
                    {
                        cssclass = "nav-home";
                    }
                    if (_ObjSideMenusBE.HasSubMenu == false)
                    {
                        //strNavigation += "<li><a>" + name + "</a></li>";
                        //strNavigation += "<li class='" + cssclass + "'><a href='" + path + "?" + UMS_Resource.PageId + "=" + _ObjiDsignBAL.AESEncryptString(_ObjSideMenusBE.Page_Id.ToString()) + "'>" + name + "</a></li>";//ID-065 Bhimaraju
                        strNavigation += "<li class='" + cssclass + "'><a href='" + path + "?" + UMS_Resource.PageId + "=" + _ObjiDsignBAL.Encrypt(_ObjSideMenusBE.Page_Id.ToString()) + "'>" + name + "</a></li>";
                    }
                    else if (_ObjSideMenusListBE.items.Count > 0)
                    {
                        string subname = string.Empty;
                        //strNavigation += "<li><a href='#'>" + name + "</a><ul class='insite mint'>";
                        //strNavigation += "<li class='" + cssclass + "'><a href='#'>" + name + "</a><ul class='radius_bottom shadow'>";
                        //strNavigation += "<li class='" + cssclass + "'  ><a href='#'>" + name + "</a><ul class='sub-menu'>";
                        strNavigation += "<li class='" + cssclass + "'  ><a href='#'>" + name + "</a>";

                        //if ((Convert.ToInt32(MainMenuId) == Constants.SettingsMenu) && Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID]) == Constants.AdminRoleId)
                        //{//Bhimaraju-ID065 for sub menu style for Admin
                        //    strNavigation += "<ul class='sub-menu sub-setting'>";
                        //}

                        if (submenucount > Convert.ToInt32(UMS_Resource.SUBMENUCOUNT))//Bhimaraju-ID065 for sub menu style for more than 12
                        {
                            if (MainMenuId == 4)
                                strNavigation += "<ul class='sub-menu billing'>";
                            else if (MainMenuId == 1)
                                strNavigation += "<ul class='sub-menu CManagement'>";
                            else
                                strNavigation += "<ul class='sub-menu sub-setting'>";
                        }
                        else
                            strNavigation += "<ul class='sub-menu'>";

                        for (int item = 0; item < _ObjSideMenusListBE.items.Count; item++)
                        {
                            subname = _ObjSideMenusListBE.items[item].SubMenu_Text;
                            string path1 = _ObjSideMenusListBE.items[item].Path;
                            int submenuid = _ObjSideMenusListBE.items[item].Main_Menu_Id;
                            string page_Id = _ObjSideMenusListBE.items[item].Page_Id.ToString();

                            if (MainMenuId == submenuid)
                            {
                                //submenucount++;
                                //if (submenucount > Convert.ToInt32(UMS_Resource.SUBMENUCOUNT_HOMEPAGE)) 
                                //{ strNavigation += "<ul class='sub-menu sub-setting'>"; }
                                //else
                                //{ strNavigation += "<ul class='sub-menu'>"; }

                                if (name.Contains("Forums"))
                                    //strNavigation += "<li><a>" + subname + "</a></li>";
                                    strNavigation += "<li><a href='" + path1 + "' target='_blank'>" + subname + "</a></li>";
                                else
                                {
                                    if (page_Id != "")
                                        //strNavigation += "<li><a>" + subname + "</a></li>";                                    
                                        strNavigation += "<li ><a href='" + path1 + "'>" + subname + "</a></li>";
                                    //strNavigation += "<li class='radius'><a href='" + path1 + "?" + UMS_Resource.PageId + "=" + _ObjiDsignBAL.Encrypt(page_Id) + "'>" + subname + "</a></li>";
                                    else
                                        //strNavigation += "<li><a>" + subname + "</a></li>";
                                        strNavigation += "<li ><a href='" + path1 + "'>" + subname + "</a></li>";
                                }

                                //if (submenucount > Convert.ToInt32(UMS_Resource.SUBMENUCOUNT_HOMEPAGE)) { strNavigation += "<ul class='sub-menu sub-setting'>"; }
                            }
                        }
                        strNavigation += "</ul></li>";
                    }
                }
                return strNavigation;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            return string.Empty;
        }
        private void addBlurAtt(Control cntrl)
        {
            if (cntrl.Controls.Count > 0)
            {
                foreach (Control childControl in cntrl.Controls)
                {
                    addBlurAtt(childControl);
                }
            }
            if (cntrl.GetType() == typeof(TextBox))
            {
                TextBox TempTextBox = (TextBox)cntrl;
                TempTextBox.Attributes.Add("onFocus", "DoFocus(this);");
                //TempTextBox.Attributes.Add("onBlur", "DoBlur(this);");
            }
            if (cntrl.GetType() == typeof(ListBox))
            {
                ListBox TempTextBox = (ListBox)cntrl;
                TempTextBox.Attributes.Add("onFocus", "DoFocus(this);");
                //TempTextBox.Attributes.Add("onBlur", "DoBlur(this);");
            }
        }
        #endregion

    }
}