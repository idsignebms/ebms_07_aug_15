﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using Resources;
using UMS_NigeriaBE;
using System.Configuration;
using System.Xml;


namespace UMS_Nigeria.MasterPages
{
    public partial class UMS : System.Web.UI.MasterPage
    {

        #region Members
        UMS_Service _ObjUMS_Service = new UMS_Service();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        string Key = "Master";
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Cache.SetExpires(DateTime.Now);
                    Response.Cache.SetNoServerCaching();
                    Response.Cache.SetNoStore();
                    if (Session[UMS_Resource.SESSION_UserName] != null)
                        lblUserName.Text = Session[UMS_Resource.SESSION_UserName].ToString();
                    if (Session[UMS_Resource.SESSION_USER_BUNAME] != null)
                        lblBusinessUnit.Text = Session[UMS_Resource.SESSION_USER_BUNAME].ToString();
                    //Session[UMS_Resource.SESSION_ROLEID] = 1;
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lbtnLogout_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Abandon();
                //Response.Redirect(UMS_Resource.StaffLogin_Page);
                Response.Redirect(UMS_Resource.MAINTENANCE_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void Page_PreInit()
        {
            try
            {
                if (Session[UMS_Resource.SESSION_ROLEID] == null || Session[UMS_Resource.SESSION_LOGINID] == null)
                    //Response.Redirect(UMS_Resource.StaffLogin_Page);
                    Response.Redirect(UMS_Resource.MAINTENANCE_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public string UserNavigation()
        {
            try
            {
                SideMenusBE _ObjSideMenusBE = new SideMenusBE();

                //  _ObjSideMenusBe.Role_Id = 8;
                XmlDocument resultXml = new XmlDocument();
                resultXml = _ObjUMS_Service.GetMenus(Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID]));

                SideMenusListBE _ObjSideMenusListBE = new SideMenusListBE();
                _ObjSideMenusListBE = _ObjiDsignBAL.DeserializeFromXml<SideMenusListBE>(resultXml);

                MainMenuListBE _ObjMainMenuListBE = new MainMenuListBE();
                _ObjMainMenuListBE = _ObjiDsignBAL.DeserializeFromXml<MainMenuListBE>(resultXml);


                string strNavigation = string.Empty;
                for (var i = 0; i < _ObjMainMenuListBE.items.Count; i++)
                {
                    _ObjSideMenusBE = (SideMenusBE)_ObjMainMenuListBE.items[i];
                    string path = _ObjSideMenusBE.Path;
                    string name = _ObjSideMenusBE.Main_Menu_Text;
                    int MainMenuId = _ObjSideMenusBE.Main_Menu_Id;
                    string cssclass = "dashhhhh";
                    //string cssclass = _ObjSideMenusBE.CssClassName;
                    if (_ObjSideMenusBE.HasSubMenu == false)
                    {
                        //strNavigation += "<li><a>" + name + "</a></li>";
                        strNavigation += "<li class='" + cssclass + "'><a href='" + path + "?" + UMS_Resource.PageId + "=" + _ObjiDsignBAL.Encrypt(_ObjSideMenusBE.Page_Id.ToString()) + "'>" + name + "</a></li>";
                    }
                    else if (_ObjSideMenusListBE.items.Count > 0)
                    {
                        string subname = string.Empty;
                        //strNavigation += "<li><a href='#'>" + name + "</a><ul class='insite mint'>";
                        //strNavigation += "<li class='" + cssclass + "'><a href='#'>" + name + "</a><ul class='radius_bottom shadow'>";
                        strNavigation += "<li class='" + cssclass + "'><a href='#'>" + name + "</a><ul class='insite mint'>";

                        for (int item = 0; item < _ObjSideMenusListBE.items.Count; item++)
                        {
                            subname = _ObjSideMenusListBE.items[item].SubMenu_Text;
                            string path1 = _ObjSideMenusListBE.items[item].Path;
                            int submenuid = _ObjSideMenusListBE.items[item].Main_Menu_Id;
                            string page_Id = _ObjSideMenusListBE.items[item].Page_Id.ToString();
                            if (MainMenuId == submenuid)
                            {

                                if (name.Contains("Forums"))
                                    //strNavigation += "<li><a>" + subname + "</a></li>";
                                    strNavigation += "<li class='radius'><a href='" + path1 + "' target='_blank'>" + subname + "</a></li>";
                                else
                                {
                                    if (page_Id != "")
                                        //strNavigation += "<li><a>" + subname + "</a></li>";                                    
                                        strNavigation += "<li class='radius'><a href='" + path1 + "'>" + subname + "</a></li>";
                                    //strNavigation += "<li class='radius'><a href='" + path1 + "?" + UMS_Resource.PageId + "=" + _ObjiDsignBAL.Encrypt(page_Id) + "'>" + subname + "</a></li>";
                                    else
                                        //strNavigation += "<li><a>" + subname + "</a></li>";
                                        strNavigation += "<li class='radius'><a href='" + path1 + "'>" + subname + "</a></li>";
                                }
                            }

                        }
                        strNavigation += "</ul></li>";
                    }
                }
                return strNavigation;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            return string.Empty;
        }
        #endregion

        protected void lbtnLogout_Click1(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect(UMS_Resource.DEFAULT_PAGE, false);
        }


    }
}