﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using System.Data;
using System.Threading;
using System.Globalization;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Xml;
using iDSignHelper;
using UMS_NigeriaBAL;

namespace UMS_Nigeria.Masters
{
    public partial class AddAccountTypes : System.Web.UI.Page
    {
        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        string Key = "AddAccountTypes";
        DataTable dtData = new DataTable();
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
        }
        private string AccountType
        {
            get { return txtAccountType.Text.Trim(); }
            set { txtAccountType.Text = value; }
        }
        private string AccountCode
        {
            get { return txtAccountCode.Text.Trim(); }
            set { txtAccountCode.Text = value; }
        }
        private string Details
        {
            get { return txtDetails.Text.Trim(); }
            set { txtDetails.Text = value; }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = InsertAccountTypes();

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message("Account type added successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                    AccountType = AccountCode = Details = string.Empty;

                }
                else if (objMastersBE.IsAccountTypeExists)
                {
                    Message("Account Type already exists.", UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (objMastersBE.IsAccountCodeExists)
                {
                    Message("Account Code already exists.", UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                    Message("Failed to save account type.", UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvAccountTypeList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridAccountType = (TextBox)row.FindControl("txtGridAccountType");
                TextBox txtGridAccountCode = (TextBox)row.FindControl("txtGridAccountCode");
                TextBox txtGridDetails = (TextBox)row.FindControl("txtGridDetails");

                Label lblAccountTypeId = (Label)row.FindControl("lblAccountTypeId");
                Label lblAccountType = (Label)row.FindControl("lblAccountType");
                Label lblAccountCode = (Label)row.FindControl("lblAccountCode");

                Label lblDetails = (Label)row.FindControl("lblDetails");

                Label lblIsMaster = (Label)row.FindControl("lblIsMaster");
                switch (e.CommandName.ToUpper())
                {
                    case "EDITACCOUNT":
                        lblAccountType.Visible = lblDetails.Visible = lblAccountCode.Visible = false;
                        txtGridAccountType.Visible = txtGridDetails.Visible = txtGridAccountCode.Visible = true;
                        txtGridAccountType.Text = lblAccountType.Text;
                        txtGridAccountCode.Text = lblAccountCode.Text;
                        txtGridDetails.Text = objiDsignBAL.ReplaceNewLines(lblDetails.Text, false);
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;

                        if (Convert.ToBoolean(lblIsMaster.Text))
                            row.FindControl("lkbtnDeActive").Visible = false;
                        break;
                    case "CANCELACCOUNT":
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;

                        if (Convert.ToBoolean(lblIsMaster.Text))
                            row.FindControl("lkbtnDeActive").Visible = false;

                        txtGridAccountType.Visible = txtGridDetails.Visible = txtGridAccountCode.Visible = false;
                        lblAccountType.Visible = lblDetails.Visible = lblAccountCode.Visible = true;
                        break;
                    case "UPDATEACCOUNT":
                        objMastersBE.AccountTypeId = Convert.ToInt32(lblAccountTypeId.Text);
                        objMastersBE.AccountType = txtGridAccountType.Text;
                        objMastersBE.AccountCode = txtGridAccountCode.Text;
                        objMastersBE.Details = objiDsignBAL.ReplaceNewLines(txtGridDetails.Text, true);
                        objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        xml = objMastersBAL.AccountType(objMastersBE, Statement.Update);
                        objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                        if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        {
                            BindGrid();
                            Message("Account Type updated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);

                        }
                        else if (objMastersBE.IsAccountTypeExists)
                        {
                            Message("Account Type already exists.", UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else if (objMastersBE.IsAccountCodeExists)
                        {
                            Message("Account Code already exists.", UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else
                            Message("Account Type updation failed.", UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "ACTIVEACCOUNT":
                        mpeActivate.Show();
                        hfAccountTypeId.Value = lblAccountTypeId.Text;
                        lblActiveMsg.Text = "Are you sure to Activate this account type?";
                        btnActiveOk.Focus();

                        break;
                    case "DEACTIVEACCOUNT":
                        mpeDeActive.Show();
                        hfAccountTypeId.Value = lblAccountTypeId.Text;
                        lblDeActiveMsg.Text = "Are you sure to Deactivate this account type?";
                        btnDeActiveOk.Focus();

                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.Active;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.AccountTypeId = Convert.ToInt32(hfAccountTypeId.Value);
                xml = objMastersBAL.AccountType(objMastersBE, Statement.Delete);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    Message("Account Type Activated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else
                    Message("Account Type Activation failed.", UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.DeActive;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.AccountTypeId = Convert.ToInt32(hfAccountTypeId.Value);
                xml = objMastersBAL.AccountType(objMastersBE, Statement.Delete);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    Message("Account Type deactivated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else
                    Message("Account Type deactivation failed.", UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvAccountTypeList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    TextBox txtGridAccountType = (TextBox)e.Row.FindControl("txtGridAccountType");
                    TextBox txtGridAccountCode = (TextBox)e.Row.FindControl("txtGridAccountCode");

                    lkbtnUpdate.Attributes.Add("onclick",
                        "return UpdateValidation('" + txtGridAccountType.ClientID + "','"
                        + txtGridAccountCode.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");


                    if (Convert.ToInt32(lblActiveStatusId.Text) == Constants.DeActive)
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }

                    Label lblIsMaster = (Label)e.Row.FindControl("lblIsMaster");
                    if (Convert.ToBoolean(lblIsMaster.Text))
                        e.Row.FindControl("lkbtnDeActive").Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void BindGrid()
        {
            try
            {
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();
                XmlDocument resultedXml = new XmlDocument();

                objMastersBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                objMastersBE.PageSize = PageSize;
                resultedXml = objMastersBAL.AccountType(objMastersBE, ReturnType.Get);

                objMastersListBE = objiDsignBAL.DeserializeFromXml<MastersListBE>(resultedXml);
                if (objMastersListBE.Items.Count > 0)
                {
                    divdownpaging.Visible = divpaging.Visible = true;
                    hfTotalRecords.Value = objMastersListBE.Items[0].TotalRecords.ToString();
                    gvAccountTypeList.DataSource = objMastersListBE.Items;
                    gvAccountTypeList.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = objMastersListBE.Items.Count.ToString();
                    divdownpaging.Visible = divpaging.Visible = false;
                    gvAccountTypeList.DataSource = new DataTable(); ;
                    gvAccountTypeList.DataBind();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private MastersBE InsertAccountTypes()
        {
            MastersBE objMastersBE = new MastersBE();
            try
            {
                objMastersBE.AccountType = this.AccountType;
                objMastersBE.AccountCode = this.AccountCode;
                objMastersBE.Details = objiDsignBAL.ReplaceNewLines(this.Details, true);
                objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                xml = objMastersBAL.AccountType(objMastersBE, Statement.Insert);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                return objMastersBE;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            return objMastersBE;
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}