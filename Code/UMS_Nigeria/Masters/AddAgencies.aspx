﻿<%@ Page Title=":: New Agency ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master" Theme="Green"
    AutoEventWireup="true" CodeBehind="AddAgencies.aspx.cs" Inherits="UMS_Nigeria.Masters.AddAgencies" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upAgencies" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <asp:UpdatePanel ID="upAddAgency" runat="server">
                    <ContentTemplate>
                        <div class="inner-sec">
                            <asp:Panel ID="pnlMessage" runat="server">
                                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                            </asp:Panel>
                            <div class="text_total">
                                <div class="text-heading">
                                    <asp:Literal ID="litAddAgency" runat="server" Text="<%$ Resources:Resource, ADD_AGENCY%>"></asp:Literal>
                                </div>
                                <div class="star_text">
                                    <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="dot-line">
                            </div>
                            <div class="inner-box">
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litAgencyName" runat="server" Text="<%$ Resources:Resource, AGENCY_NAME%>"></asp:Literal>
                                            <span class="span_star">*</span></label>
                                        <div class="space">
                                        </div>
                                        <asp:TextBox CssClass="text-box" ID="txtAgencyName" TabIndex="1" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanAgencyName','Agency Name')"
                                            MaxLength="300" placeholder="Enter Agency Name"></asp:TextBox>
                                        <span id="spanAgencyName" class="span_color"></span>
                                    </div>
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litAnotherContactNo" runat="server" Text="<%$ Resources:Resource, ANOTHER_CONTACT_NO%>"></asp:Literal>
                                        </label>
                                        <div class="space">
                                        </div>
                                        <asp:TextBox CssClass="text-box" ID="txtCode2" TabIndex="5" runat="server" MaxLength="3"
                                            Width="11%" placeholder="Code"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtCode2"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:TextBox CssClass="text-box" ID="txtAnotherContactNo" Width="45%" TabIndex="6"
                                            runat="server" MaxLength="15" placeholder="Enter Alternate Contact No"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="ftbTxtAnotherContactNo" runat="server" TargetControlID="txtAnotherContactNo"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanAnotherNo" class="span_color"></span>
                                    </div>
                                    <div class="box_totalThree_a">
                                        <br />
                                        <asp:Button ID="btnSave" TabIndex="8" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                            CssClass="box_s" runat="server" OnClick="btnSave_Click" />
                                    </div>
                                </div>
                                <div class="inner-box2">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litContactName" runat="server" Text="<%$ Resources:Resource, CONTACT_NAME%>"></asp:Literal>
                                            <span class="span_star">*</span></label><div class="space">
                                            </div>
                                        <asp:TextBox CssClass="text-box" ID="txtContactName" runat="server" TabIndex="2"
                                            MaxLength="200" placeholder="Enter Contact Name" onblur="return TextBoxBlurValidationlbl(this,'spanContactName','Contact Name')"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="ftbContactName" runat="server" TargetControlID="txtContactName"
                                            ValidChars=" " FilterMode="ValidChars" FilterType="LowercaseLetters,UppercaseLetters,Custom">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanContactName" class="span_color"></span>
                                    </div>
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litNotes" runat="server" Text="<%$ Resources:Resource, Details%>"></asp:Literal>
                                        </label>
                                        <div class="space_2">
                                        </div>
                                        <asp:TextBox CssClass="text-box" ID="txtDetails" runat="server" TabIndex="7" TextMode="MultiLine"
                                            placeholder="Enter Details Here" MaxLength="5"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="inner-box3">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, CONTACT_NO%>"></asp:Literal>
                                            <span class="span_star">*</span></label>
                                        <div class="space">
                                        </div>
                                        <asp:TextBox CssClass="text-box" ID="txtCode1" TabIndex="3" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanContactNo','Code')"
                                            MaxLength="3" Width="11%" placeholder="Code"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtCode1"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:TextBox CssClass="text-box" ID="txtContactNo" Width="45%" TabIndex="4" runat="server"
                                            onblur="return TextBoxBlurValidationlbl(this,'spanContactNo','Contact No')" MaxLength="15"
                                            placeholder="Enter Contact No"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtContactNo"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanContactNo" class="span_color"></span>
                                    </div>
                                </div>
                            </div>
                            <div style="clear: both;">
                            </div>
                            <div class="grid_boxes">
                                <div class="grid_pagingTwo_top">
                                    <div class="paging_top_title">
                                        <asp:Literal ID="litCountriesList" runat="server" Text="<%$ Resources:Resource, GRID_AGENCIES_LIST%>"></asp:Literal>
                                    </div>
                                    <div class="paging_top_rightTwo_content" id="divpaging" runat="server">
                                        <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="grid_tb" id="divgrid" runat="server">
                                <asp:GridView ID="gvAgenciesList" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="grid_tb_hd"
                                    OnRowCommand="gvAgenciesList_RowCommand" OnRowDataBound="gvAgenciesList_RowDataBound">
                                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                    <EmptyDataTemplate>
                                        There is no data.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                            HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%--<%#Container.DataItemIndex+1%>--%>
                                                <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                                <asp:Label ID="lblActiveStatusId" Visible="false" runat="server" Text='<%#Eval("ActiveStatusId") %>'></asp:Label>
                                                <asp:Label ID="lblAgencyId" Visible="false" runat="server" Text='<%#Eval("AgencyId")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, AGENCY_NAME%>" ItemStyle-Width="8%"
                                            ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAgencyName" runat="server" Text='<%#Eval("AgencyName") %>'></asp:Label>
                                                <asp:TextBox CssClass="text-box" ID="txtGridAgencyName" MaxLength="300" placeholder="Enter Agency Name"
                                                    runat="server" Visible="false"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, CONTACT_NAME%>" ItemStyle-Width="10%"
                                            ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                            <ItemTemplate>
                                                <asp:Label ID="lblContactPersonName" runat="server" Text='<%#Eval("ContactName") %>'></asp:Label>
                                                <asp:TextBox CssClass="text-box" ID="txtGridContactPersonName" MaxLength="200" placeholder="Enter Contact Name"
                                                    runat="server" Visible="false"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, CONTACT_NO%>" ItemStyle-Width="10%"
                                            ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                            <ItemTemplate>
                                                <asp:Label ID="lblContactNo1" runat="server" Text='<%#Eval("ContactNo") %>'></asp:Label>
                                                <asp:TextBox CssClass="text-box" ID="txtGridCode1" runat="server" MaxLength="3" Width="11%"
                                                    placeholder="Code" Visible="false"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtGridCode1"
                                                    FilterType="Numbers" ValidChars=" ">
                                                </asp:FilteredTextBoxExtender>
                                                <asp:TextBox CssClass="text-box" ID="txtGridContactNo1" runat="server" MaxLength="15"
                                                    placeholder="Enter Contact No" Visible="false"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="ftbGridTxtContactNo7" runat="server" TargetControlID="txtGridContactNo1"
                                                    FilterType="Numbers" ValidChars=" ">
                                                </asp:FilteredTextBoxExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, ANOTHER_CONTACT_NO%>" ItemStyle-Width="10%"
                                            ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                            <ItemTemplate>
                                                <asp:Label ID="lblContactNo2" runat="server" Text='<%#Eval("ContactNo2") %>'></asp:Label>
                                                <asp:TextBox CssClass="text-box" ID="txtGridCode2" Visible="false" runat="server"
                                                    MaxLength="3" Width="11%" placeholder="Code"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtGridCode2"
                                                    FilterType="Numbers" ValidChars=" ">
                                                </asp:FilteredTextBoxExtender>
                                                <asp:TextBox CssClass="text-box" ID="txtGridContactNo2" MaxLength="15" placeholder="Enter Alternate Contact No"
                                                    runat="server" Visible="false">
                                                </asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="ftbTxtGridContactNo5" runat="server" TargetControlID="txtGridContactNo2"
                                                    FilterType="Numbers" ValidChars=" ">
                                                </asp:FilteredTextBoxExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, DETAILS%>" ItemStyle-Width="10%"
                                            ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGridDetails" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                                <asp:TextBox CssClass="text-box" ID="txtGridDetails" runat="server" Visible="false"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="10%"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lkbtnEdit" runat="server" ToolTip="Edit" Text="Edit" CommandName="EditAgency">
                                        <img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                                                <asp:LinkButton ID="lkbtnUpdate" Visible="false" runat="server" ToolTip="Update"
                                                    CommandName="UpdateAgency"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                                <asp:LinkButton ID="lkbtnCancel" Visible="false" runat="server" ToolTip="Cancel"
                                                    CommandName="CancelAgency"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                                <asp:LinkButton ID="lkbtnActive" Visible="false" runat="server" ToolTip="Activate Agency"
                                                    CommandName="ActiveAgency"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                                <asp:LinkButton ID="lkbtnDeActive" runat="server" ToolTip="DeActivate Agency" CommandName="DeActiveAgency"><img src="../images/Deactivate.gif" alt="DeActive" /></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <%--<div class="page_na">
                                    <uc2:UCPagingV2 ID="UCPagingV2" runat="server" />
                                </div>--%>
                                <asp:HiddenField ID="hfPageSize" runat="server" />
                                <asp:HiddenField ID="hfPageNo" runat="server" />
                                <asp:HiddenField ID="hfLastPage" runat="server" />
                                <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                                <asp:HiddenField ID="hfAgencyId" runat="server" />
                            </div>
                            <div class="grid_boxes">
                                <div id="divdownpaging" runat="server">
                                    <div class="grid_paging_bottom">
                                        <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="rnkland">
                <%-- Active Btn popup Start--%>
                <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                    TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnActivate" runat="server" Text="Button" CssClass="popbtn" />
                <asp:Panel ID="PanelActivate" runat="server" CssClass="modalPopup"
                    class="poppnl" Style="display: none;">
                    <div class="popheader popheaderlblgreen">
                        <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                    </div>
                    <div class="space">
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btnActiveOk" runat="server" OnClick="btnActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                            <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- Active  Btn popup Closed--%>
                <%-- DeActive  Btn popup Start--%>
                <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                    TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnDeActivate" runat="server" Text="Button" CssClass="popbtn" />
                <asp:Panel ID="PanelDeActivate" runat="server" CssClass="modalPopup"
                    Style="display: none;" class="poppnl">
                    <div class="popheader popheaderlblred">
                        <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <div class="space">
                            </div>
                            <asp:Button ID="btnDeActiveOk" runat="server" OnClick="btnDeActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                            <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- DeActive  popup Closed--%>
                <%-- Grid Alert popup Start--%>
                <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                    TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                </asp:ModalPopupExtender>
                <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                    <div class="popheader popheaderlblred">
                        <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- Grid Alert popup Closed--%>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".rnkland").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/PageValidations/AddAgencies.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
     <script language="javascript" type="text/javascript">
         $(document).ready(function () { assignDiv('rptrMainMasterMenu_156_0', '92') });
    </script>
</asp:Content>
