﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddAgencies
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 27-Feb-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;

namespace UMS_Nigeria.Masters
{
    public partial class AddAgencies : System.Web.UI.Page
    {
        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        string Key = "AddAgency";
        #endregion

        #region Properties
        public int PageSize
        {
            //get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStartsReport : Convert.ToInt32(hfPageSize.Value); }
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
        }

        private string AgencyName
        {
            get { return txtAgencyName.Text.Trim(); }
            set { txtAgencyName.Text = value; }
        }
        private string Details
        {
            get { return txtDetails.Text.Trim(); }
            set { txtDetails.Text = value; }
        }
        private string ContactName
        {
            get { return txtContactName.Text.Trim(); }
            set { txtContactName.Text = value; }
        }
        private string ContactNo
        {
            get { return txtContactNo.Text.Trim(); }
            set { txtContactNo.Text = value; }
        }
        private string ContactNo2
        {
            get { return txtAnotherContactNo.Text.Trim(); }
            set { txtAnotherContactNo.Text = value; }
        }
        private string ContactNoCode1
        {
            get { return txtCode1.Text.Trim(); }
            set { txtCode1.Text = value; }
        }
        private string ContactNoCode2
        {
            get { return txtCode2.Text.Trim(); }
            set { txtCode2.Text = value; }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                lblMessage.Text = pnlMessage.CssClass = string.Empty;
                if (!IsPostBack)
                {
                    //string path = string.Empty;
                    //path = objCommonMethods.GetPagePath(this.Request.Url);
                    //if (objCommonMethods.IsAccess(path))
                    //{
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    //}
                    //else { Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }
                }
            }
            else { Response.Redirect(UMS_Resource.DEFAULT_PAGE); }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = InsertAgenciesDetails();
                if (objMastersBE.AgencyId > 0)
                {
                    BindGrid();
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.AGENCY_INSERT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    txtAgencyName.Text = txtContactName.Text = txtContactNo.Text = txtDetails.Text = txtAnotherContactNo.Text = string.Empty;
                }
                else if (objMastersBE.IsAgencyNameExists)
                {
                    Message(UMS_Resource.AGENCY_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                {
                    Message(UMS_Resource.AGENCY_INSERT_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                }


            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvAgenciesList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridAgencyName = (TextBox)row.FindControl("txtGridAgencyName");
                TextBox txtGridContactPersonName = (TextBox)row.FindControl("txtGridContactPersonName");
                TextBox txtGridContactNo1 = (TextBox)row.FindControl("txtGridContactNo1");
                TextBox txtGridContactNo2 = (TextBox)row.FindControl("txtGridContactNo2");
                TextBox txtGridDetails = (TextBox)row.FindControl("txtGridDetails");
                TextBox txtGridCode1 = (TextBox)row.FindControl("txtGridCode1");
                TextBox txtGridCode2 = (TextBox)row.FindControl("txtGridCode2");


                Label lblAgencyName = (Label)row.FindControl("lblAgencyName");
                Label lblContactPersonName = (Label)row.FindControl("lblContactPersonName");
                Label lblContactNo1 = (Label)row.FindControl("lblContactNo1");
                Label lblContactNo2 = (Label)row.FindControl("lblContactNo2");
                Label lblGridDetails = (Label)row.FindControl("lblGridDetails");
                Label lblAgencyId = (Label)row.FindControl("lblAgencyId");
                Label lblActiveStatusId = (Label)row.FindControl("lblActiveStatusId");

                switch (e.CommandName.ToUpper())
                {
                    case "EDITAGENCY":
                        foreach (GridViewRow gvr in gvAgenciesList.Rows)
                        {
                            if (gvr.RowType == DataControlRowType.DataRow)
                            {
                                if (gvr.RowIndex == row.RowIndex)
                                {

                                    lblGridDetails.Visible = lblContactNo2.Visible = lblContactNo1.Visible = lblContactPersonName.Visible = lblAgencyName.Visible = false;
                                    txtGridCode1.Visible = txtGridCode2.Visible = txtGridDetails.Visible = txtGridContactNo2.Visible = txtGridContactNo1.Visible = txtGridContactPersonName.Visible = txtGridAgencyName.Visible = true;
                                    txtGridAgencyName.Text = lblAgencyName.Text;
                                    txtGridContactPersonName.Text = lblContactPersonName.Text;
                                    //txtGridContactNo1.Text = lblContactNo1.Text;
                                    //txtGridContactNo2.Text = lblContactNo2.Text;
                                    if (lblContactNo1.Text.Length > 0 && lblContactNo1.Text != "--")
                                    {
                                        string[] contactNo = lblContactNo1.Text.Split('-');
                                        if (contactNo.Length < 2)
                                        {
                                            txtGridCode1.Text = lblContactNo1.Text.Substring(0, 3);
                                            txtGridContactNo1.Text = lblContactNo1.Text.Remove(0, 3);
                                        }
                                        else
                                        {
                                            txtGridCode1.Text = contactNo[0].ToString();
                                            txtGridContactNo1.Text = contactNo[1].ToString();
                                        }
                                    }
                                    else
                                        txtGridCode1.Text = txtGridContactNo1.Text = string.Empty;

                                    if (lblContactNo2.Text.Length > 0 && lblContactNo2.Text != "--")
                                    {
                                        string[] contactNo = lblContactNo2.Text.Split('-');
                                        if (contactNo.Length < 2)
                                        {
                                            txtGridCode2.Text = lblContactNo2.Text.Substring(0, 3);
                                            txtGridContactNo2.Text = lblContactNo2.Text.Remove(0, 3);
                                        }
                                        else
                                        {
                                            txtGridCode2.Text = contactNo[0].ToString();
                                            txtGridContactNo2.Text = contactNo[1].ToString();
                                        }
                                    }
                                    else
                                        txtGridCode2.Text = txtGridContactNo2.Text = string.Empty;

                                    txtGridDetails.Text = objiDsignBAL.ReplaceNewLines(lblGridDetails.Text, false);
                                    row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                                    row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;
                                }
                                else
                                {
                                    Label lblActiveId = (Label)gvr.FindControl("lblActiveStatusId");
                                    if (Convert.ToInt32(lblActiveId.Text) == Constants.Active)
                                    {
                                        gvr.FindControl("lblGridDetails").Visible = gvr.FindControl("lblContactNo2").Visible = gvr.FindControl("lblContactNo1").Visible = gvr.FindControl("lblContactPersonName").Visible = gvr.FindControl("lblAgencyName").Visible = true;
                                        gvr.FindControl("txtGridDetails").Visible = gvr.FindControl("txtGridContactNo2").Visible = gvr.FindControl("txtGridContactNo1").Visible = gvr.FindControl("txtGridContactPersonName").Visible = gvr.FindControl("txtGridAgencyName").Visible = false;
                                        gvr.FindControl("lkbtnCancel").Visible = gvr.FindControl("lkbtnUpdate").Visible = false;
                                        gvr.FindControl("lkbtnDeActive").Visible = gvr.FindControl("lkbtnEdit").Visible = true;
                                    }
                                }
                            }

                        }
                        break;
                    case "CANCELAGENCY":
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                        txtGridCode1.Visible = txtGridCode2.Visible = txtGridDetails.Visible = txtGridContactNo2.Visible = txtGridContactNo1.Visible = txtGridContactPersonName.Visible = txtGridAgencyName.Visible = false;
                        lblGridDetails.Visible = lblContactNo2.Visible = lblContactNo1.Visible = lblContactPersonName.Visible = lblAgencyName.Visible = true;
                        break;
                    case "UPDATEAGENCY":
                        objMastersBE.AgencyId = Convert.ToInt32(lblAgencyId.Text);
                        objMastersBE.AgencyName = txtGridAgencyName.Text;
                        objMastersBE.Details = objiDsignBAL.ReplaceNewLines(txtGridDetails.Text, true);
                        objMastersBE.ContactName = txtGridContactPersonName.Text;
                        objMastersBE.ContactNo = txtGridCode1.Text + "-" + txtGridContactNo1.Text;
                        objMastersBE.ContactNo2 = txtGridCode2.Text + "-" + txtGridContactNo2.Text;
                        objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.Agency(objMastersBE, Statement.Update));

                        if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        {
                            BindGrid();
                            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            Message(UMS_Resource.AGENCY_UPDATED, UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        else if (objMastersBE.IsAgencyNameExists)
                        {
                            Message(UMS_Resource.AGENCY_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else
                        {
                            Message(UMS_Resource.AGENCY_INSERT_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        break;
                    case "ACTIVEAGENCY":
                        mpeActivate.Show();
                        hfAgencyId.Value = lblAgencyId.Text;
                        lblActiveMsg.Text = UMS_Resource.ACTIVE_AGENCY_POPUP_TEXT;
                        btnActiveOk.Focus();
                        //objMastersBE.ActiveStatusId = Constants.Active;
                        //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objMastersBE.AgencyId = Convert.ToInt32(lblAgencyId.Text);
                        //xml = objMastersBAL.Agency(objMastersBE, Statement.Delete);
                        //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                        //if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        //{
                        //    BindGrid();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message(UMS_Resource.AGENCY_ACTIVATE_SUCESS,UMS_Resource.MESSAGETYPE_SUCCESS);

                        //}
                        //else
                        //    Message( UMS_Resource.AGENCY_ACTIVATE_FAIL,UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "DEACTIVEAGENCY":
                        mpeDeActive.Show();
                        lblDeActiveMsg.Text = UMS_Resource.DEACTIVE_AGENCY_POPUP_TEXT;
                        hfAgencyId.Value = lblAgencyId.Text;
                        btnDeActiveOk.Focus();
                        //objMastersBE.ActiveStatusId = Constants.DeActive;
                        //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objMastersBE.AgencyId = Convert.ToInt32(lblAgencyId.Text);
                        //xml = objMastersBAL.Agency(objMastersBE, Statement.Delete);
                        //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                        //if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        //{
                        //    BindGrid();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message(UMS_Resource.AGENCY_DEACTIVATE_SUCESS,UMS_Resource.MESSAGETYPE_SUCCESS);

                        //}
                        //else
                        //    Message( UMS_Resource.AGENCY_DEACTIVATE_FAIL,UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.Active;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.AgencyId = Convert.ToInt32(hfAgencyId.Value);
                xml = objMastersBAL.Agency(objMastersBE, Statement.Delete);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.AGENCY_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else
                    Message(UMS_Resource.AGENCY_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.DeActive;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.AgencyId = Convert.ToInt32(hfAgencyId.Value);
                xml = objMastersBAL.Agency(objMastersBE, Statement.Delete);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.AGENCY_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else
                    Message(UMS_Resource.AGENCY_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvAgenciesList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    TextBox txtGridAgencyName = (TextBox)e.Row.FindControl("txtGridAgencyName");
                    TextBox txtGridContactPersonName = (TextBox)e.Row.FindControl("txtGridContactPersonName");
                    TextBox txtGridContactNo1 = (TextBox)e.Row.FindControl("txtGridContactNo1");
                    TextBox txtGridContactNo2 = (TextBox)e.Row.FindControl("txtGridContactNo2");
                    TextBox txtGridCode1 = (TextBox)e.Row.FindControl("txtGridCode1");
                    TextBox txtGridCode2 = (TextBox)e.Row.FindControl("txtGridCode2");



                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridAgencyName.ClientID + "','"
                                                                                      + txtGridContactPersonName.ClientID + "','"
                                                                                      + txtGridContactNo2.ClientID + "','"
                                                                                      + txtGridContactNo1.ClientID + "','"
                                                                                      + txtGridCode1.ClientID + "','"
                                                                                      + txtGridCode2.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");


                    if (Convert.ToInt32(lblActiveStatusId.Text) == Constants.DeActive)
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        private MastersBE InsertAgenciesDetails()
        {
            MastersBE objMastersBE = new MastersBE();
            try
            {

                objMastersBE.AgencyName = AgencyName;
                objMastersBE.Details = objiDsignBAL.ReplaceNewLines(Details, true);
                objMastersBE.ContactName = ContactName;
                objMastersBE.ContactNo = ContactNoCode1 + "-" + ContactNo;
                objMastersBE.ContactNo2 = ContactNoCode2 + "-" + ContactNo2;
                objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.Agency(objMastersBE, Statement.Insert));

                return objMastersBE;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            return objMastersBE;
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void BindGrid()
        {
            try
            {
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();
                XmlDocument resultedXml = new XmlDocument();

                objMastersBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                objMastersBE.PageSize = PageSize;
                resultedXml = objMastersBAL.Agency(objMastersBE, ReturnType.Get);

                objMastersListBE = objiDsignBAL.DeserializeFromXml<MastersListBE>(resultedXml);
                DataTable dt = new DataTable();
                //dt = objiDsignBAL.ConvertListToDataSet<MastersBE>(objMastersListBE.Items).Tables[0];

                if (objMastersListBE.Items.Count > 0)
                {
                    gvAgenciesList.DataSource = objMastersListBE.Items;
                    gvAgenciesList.DataBind();
                    divdownpaging.Visible = divpaging.Visible = true;
                    hfTotalRecords.Value = objMastersListBE.Items[0].TotalRecords.ToString();
                }
                else
                {
                    gvAgenciesList.DataSource = dt;
                    gvAgenciesList.DataBind();
                    hfTotalRecords.Value = objMastersListBE.Items.Count.ToString();
                    divdownpaging.Visible = divpaging.Visible = false;
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion       

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}