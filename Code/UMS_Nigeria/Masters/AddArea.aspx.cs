﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using System.Xml;
using System.Data;
using System.Collections;
using Resources;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using System.Threading;


namespace UMS_Nigeria.Masters
{
    public partial class AddArea : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        // MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        string Key = "AddArea";
        AreaBAL _objAreaBAL = new AreaBAL();
        #endregion

        #region Properties

        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                AreaBe objAreaBe = new AreaBe();

                objAreaBe.Address1 = txtAddress1.Text.Trim();
                objAreaBe.Address2 = txtAddress2.Text.Trim();
                objAreaBe.City = txtCity.Text.Trim();
                objAreaBe.ZipCode = txtZipCode.Text.Trim();
                objAreaBe.AreaDetails = txtNotes.Text.Trim();
                objAreaBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                xml = _objAreaBAL.Area(objAreaBe, Statement.Insert);
                objAreaBe = _ObjiDsignBAL.DeserializeFromXml<AreaBe>(xml);
                if (Convert.ToBoolean(objAreaBe.RowsEffected > 0))
                {
                    BindGrid();
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message("Area details saved successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                    txtAddress1.Text = txtAddress2.Text = txtCity.Text = txtZipCode.Text = txtNotes.Text = string.Empty;
                }
                else if (objAreaBe.IsExists)
                {
                    Message("Area details already exists", UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                {
                    Message("Failed to save area details.", UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {

                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvAreaList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                AreaListBE _ObjAreaListBE = new AreaListBE();
                AreaBe _ObjAreasBE = new AreaBe();
                DataSet ds = new DataSet();


                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                TextBox txtGvAddress1 = (TextBox)row.FindControl("txtGvAddress1");
                TextBox txtGvAddress2 = (TextBox)row.FindControl("txtGvAddress2");
                TextBox txtGvCity = (TextBox)row.FindControl("txtGvCity");
                TextBox txtGvZipCode = (TextBox)row.FindControl("txtGvZipCode");
                TextBox txtGvNotes = (TextBox)row.FindControl("txtGvNotes");

                Label lblGvAddress1 = (Label)row.FindControl("lblGvAddress1");
                Label lblGvAddress2 = (Label)row.FindControl("lblGvAddress2");
                Label lblGvCity = (Label)row.FindControl("lblGvCity");
                Label lblGvZipCode = (Label)row.FindControl("lblGvZipCode");
                Label lblGvNotes = (Label)row.FindControl("lblGvNotes");
                Label lblAreaCode = (Label)row.FindControl("lblAreaCode");

                hfAreaCode.Value = lblAreaCode.Text;

                hfRecognize.Value = string.Empty;
                switch (e.CommandName.ToUpper())
                {
                    case "EDITAREA":
                        lblGvAddress1.Visible = lblGvAddress2.Visible = lblGvCity.Visible = lblGvZipCode.Visible = lblGvNotes.Visible = false;
                        txtGvAddress1.Visible = txtGvAddress2.Visible = txtGvCity.Visible = txtGvZipCode.Visible = txtGvNotes.Visible = true;

                        txtGvAddress1.Text = lblGvAddress1.Text;
                        txtGvAddress2.Text = lblGvAddress2.Text;
                        txtGvCity.Text = lblGvCity.Text;
                        txtGvZipCode.Text = lblGvZipCode.Text;
                        txtGvNotes.Text = lblGvNotes.Text;

                        row.FindControl("lkbtnAreaCancel").Visible = row.FindControl("lkbtnAreaUpdate").Visible = true;
                        row.FindControl("lkbtnDelete").Visible = row.FindControl("lkbtnAreaEdit").Visible = false;
                        break;
                    case "CANCELAREA":
                        row.FindControl("lkbtnDelete").Visible = row.FindControl("lkbtnAreaEdit").Visible = true;
                        row.FindControl("lkbtnAreaCancel").Visible = row.FindControl("lkbtnAreaUpdate").Visible = false;

                        lblGvAddress1.Visible = lblGvAddress2.Visible = lblGvCity.Visible = lblGvZipCode.Visible = lblGvNotes.Visible = true;
                        txtGvAddress1.Visible = txtGvAddress2.Visible = txtGvCity.Visible = txtGvZipCode.Visible = txtGvNotes.Visible = false;

                        break;
                    case "UPDATEAREA":
                        //xml = _ObjUMS_Service.UpdateStates(txtGridStateName.Text, lblStateCode.Text, txtGridDisplayCode.Text, ddlGridCountryName.SelectedValue, _ObjiDsignBAL.ReplaceNewLines(txtGridNotes.Text, true));
                        _ObjAreasBE.AreaCode = Convert.ToInt32(lblAreaCode.Text);
                        _ObjAreasBE.Address1 = txtGvAddress1.Text.Trim();
                        _ObjAreasBE.Address2 = txtGvAddress2.Text.Trim();
                        _ObjAreasBE.City = txtGvCity.Text.Trim();
                        _ObjAreasBE.ZipCode = txtGvZipCode.Text.Trim();
                        _ObjAreasBE.AreaDetails = txtGvNotes.Text.Trim();
                        _ObjAreasBE.ModifedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        xml = _objAreaBAL.Area(_ObjAreasBE, Statement.Update);
                        _ObjAreasBE = _ObjiDsignBAL.DeserializeFromXml<AreaBe>(xml);

                        if (_ObjAreasBE.RowsEffected > 0)
                        {
                            BindGrid();
                            Message("Area Updated Successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        else if (_ObjAreasBE.IsExists)
                        {
                            Message("Area details already exists.", UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else
                        {
                            Message("Failed to update area details.", UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        break;
                    case "ACTIVEAREA":
                        mpeActivate.Show();
                        lblActiveMsg.Text = "Are you sure you want to Activate this Area?";
                        btnActiveOk.Focus();

                        break;
                    case "DEACTIVEAREA":
                        mpeDeActive.Show();
                        lblDeActiveArea.Text = "Are you sure you want to DeActivate this Area?";
                        btnDeActiveOk.Focus();

                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void gvAreaList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnAreaUpdate = (LinkButton)e.Row.FindControl("lkbtnAreaUpdate");

                    TextBox txtGvAddress1 = (TextBox)e.Row.FindControl("txtGvAddress1");
                    TextBox txtGvAddress2 = (TextBox)e.Row.FindControl("txtGvAddress2");
                    TextBox txtGvCity = (TextBox)e.Row.FindControl("txtGvCity");
                    TextBox txtGvZipCode = (TextBox)e.Row.FindControl("txtGvZipCode");
                    TextBox txtGvNotes = (TextBox)e.Row.FindControl("txtGvNotes");

                    lkbtnAreaUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGvAddress1.ClientID + "','" + txtGvAddress2.ClientID + "','" + txtGvCity.ClientID + "','" + txtGvZipCode.ClientID + "','" + txtGvNotes.ClientID + "')");

                    Label lblIsActive = (Label)e.Row.FindControl("lblIsActive");


                    if (!Convert.ToBoolean(lblIsActive.Text))
                    {
                        e.Row.FindControl("lkbtnDelete").Visible = e.Row.FindControl("lkbtnAreaEdit").Visible = false;
                        e.Row.FindControl("lkbtnAreaActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDelete").Visible = e.Row.FindControl("lkbtnAreaEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {

            try
            {
                AreaBe _ObjAreasBE = new AreaBe();
                _ObjAreasBE.AreaCode = Convert.ToInt32(hfAreaCode.Value);
                _ObjAreasBE.ModifedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _ObjAreasBE.IsActive = true;

                xml = _objAreaBAL.Area(_ObjAreasBE, Statement.Delete);
                _ObjAreasBE = _ObjiDsignBAL.DeserializeFromXml<AreaBe>(xml);

                if (_ObjAreasBE.RowsEffected > 0)
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message("Area activated successfully", UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                    Message("Area activation failed", UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                AreaBe _ObjAreasBE = new AreaBe();
                _ObjAreasBE.AreaCode = Convert.ToInt32(hfAreaCode.Value);
                _ObjAreasBE.ModifedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _ObjAreasBE.IsActive = false;
                xml = _objAreaBAL.Area(_ObjAreasBE, Statement.Delete);
                _ObjAreasBE = _ObjiDsignBAL.DeserializeFromXml<AreaBe>(xml);
                if (_ObjAreasBE.RowsEffected > 0)
                {
                    BindGrid();
                    Message("Area DeActivated Successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else if (_ObjAreasBE.IsRecordsAssociated)
                    Message("This area has customers. Shift Customers to different Area before deactivating.", UMS_Resource.MESSAGETYPE_ERROR);
                else
                    Message("Area DeActivation Failed.", UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        public void BindGrid()
        {
            try
            {
                AreaListBE _objAreaListBE = new AreaListBE();
                AreaBe _ObjAreaBe = new AreaBe();
                XmlDocument resultedXml = new XmlDocument();
                //resultedXml = _ObjUMS_Service.GetActiveCountriesList(Convert.ToInt32(hfPageNo.Value), PageSize);
                _ObjAreaBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                _ObjAreaBe.PageSize = PageSize;
                resultedXml = _objAreaBAL.Area(_ObjAreaBe, ReturnType.Get);
                _objAreaListBE = _ObjiDsignBAL.DeserializeFromXml<AreaListBE>(resultedXml);

                if (_objAreaListBE.items.Count > 0)
                {
                    divdownpaging.Visible = divpaging.Visible = true;
                    UCPaging1.Visible = UCPaging2.Visible = true;
                    hfTotalRecords.Value = _objAreaListBE.items[0].TotalRecords.ToString();
                    gvAreaList.DataSource = _objAreaListBE.items;
                    gvAreaList.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = _objAreaListBE.items.Count.ToString();
                    divdownpaging.Visible = divpaging.Visible = false;
                    UCPaging1.Visible = UCPaging2.Visible = false;
                    gvAreaList.DataSource = new DataTable(); ;
                    gvAreaList.DataBind();
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}