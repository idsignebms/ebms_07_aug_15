﻿<%@ Page Title="::Add Book Number::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="AddBookNumbers.aspx.cs" Inherits="UMS_Nigeria.Masters.AddBookNumbers" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upStates" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddBookNumber" runat="server" Text="<%$ Resources:Resource, ADD_BOOK_NUMBER%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                                    <span class="span_star">*</span></label><div class="space">
                                    </div>
                                <asp:DropDownList ID="ddlBusinessUnitName" TabIndex="1" onchange="DropDownlistOnChangelbl(this,'spanddlBusinessUnitName',' Business Unit')"
                                    AutoPostBack="true" runat="server" CssClass="text-box text-select" OnSelectedIndexChanged="ddlBusinessUnitName_SelectedIndexChanged">
                                </asp:DropDownList>
                                <%--<span class="text-heading_2">
                                    <asp:HyperLink ID="lkbtnAddNewBusinessUnit" NavigateUrl="~/Masters/AddBusinessUnits.aspx"
                                        runat="server" Text="<%$ Resources:Resource, ADD%>"></asp:HyperLink></span>--%>
                                <div class="space">
                                </div>
                                <span id="spanddlBusinessUnitName" class="span_color"></span>
                            </div>
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litCycles" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal>
                                    <span class="span_star">*</span>
                                </label>
                                <div class="space">
                                </div>
                                <asp:DropDownList ID="ddlCycle" TabIndex="4" Enabled="false" runat="server" CssClass="text-box text-select"
                                    onchange="DropDownlistOnChangelbl(this,'spanddlCycle',' BookGroup')">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <%--<span class="text-heading_2">
                                    <asp:HyperLink ID="lkbtnAddNewCycle" runat="server" NavigateUrl="~/Masters/AddCycle.aspx"
                                        Text="<%$ Resources:Resource, ADD%>"></asp:HyperLink></span>--%>
                                <div class="space">
                                </div>
                                <span id="spanddlCycle" class="span_color"></span>
                            </div>
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litNoOfAccount" runat="server" Text="<%$ Resources:Resource, MAX_NO_OF_ACCOUNT%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:TextBox CssClass="text-box" ID="txtNoOfAccount" runat="server" TabIndex="7"
                                    MaxLength="10" placeholder="Enter Max No Of Acccounts">
                                </asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbNoOfAcc" runat="server" TargetControlID="txtNoOfAccount"
                                    FilterType="Numbers" ValidChars=" ">
                                </asp:FilteredTextBoxExtender>
                            </div>
                            <%--  <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, BOOK_CODE%>"></asp:Literal>
                                    <span class="span_star">*</span></label><div class="space">
                                    </div>
                                <asp:TextBox CssClass="text-box"  ID="txtBookCode" runat="server" TabIndex="6" onkeyup="this.value=this.value.toUpperCase()"
                                    onblur="return TextBoxBlurValidationlbl(this,'spanBookCode','Book Code')" placeholder="Enter Book Code"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbtxtBookCode" runat="server" TargetControlID="txtBookCode"
                                    FilterType="LowercaseLetters,UppercaseLetters,Custom,Numbers" ValidChars="">
                                </asp:FilteredTextBoxExtender>
                                <div class="space">
                                </div>
   
                                <span id="spanBookCode" class="span_color"></span>
                            </div>--%>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label>
                                    <asp:Literal ID="litServiceUnit" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal>
                                    <span class="span_star">*</span></label><div class="space">
                                    </div>
                                <asp:DropDownList ID="ddlServiceUnitName" TabIndex="2" Enabled="false" onchange="DropDownlistOnChangelbl(this,'spanddlServiceUnitName',' Service Unit')"
                                    AutoPostBack="true" runat="server" CssClass="text-box text-select" OnSelectedIndexChanged="ddlServiceUnitName_SelectedIndexChanged">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <%-- <span class="text-heading_2">
                                    <asp:HyperLink ID="lkbtnAddNewServiceUnit" NavigateUrl="~/Masters/AddServiceUnits.aspx"
                                        runat="server" Text="<%$ Resources:Resource, ADD%>"></asp:HyperLink></span>--%>
                                <div class="space">
                                </div>
                                <span id="spanddlServiceUnitName" class="span_color"></span>
                            </div>
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, BOOK_NAME%>"></asp:Literal>
                                    <span class="span_star">*</span></label><div class="space">
                                    </div>
                                <asp:TextBox CssClass="text-box" ID="txtBookNo" runat="server" TabIndex="5" onblur="return TextBoxBlurValidationlbl(this,'spanBookNo','Book Number')"
                                    MaxLength="20" placeholder="Enter Book Name"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" TargetControlID="txtBookNo"
                                    FilterType="LowercaseLetters, UppercaseLetters, Custom, Numbers" ValidChars=" -">
                                </asp:FilteredTextBoxExtender>
                                <div class="space">
                                </div>
                                <span id="spanBookNo" class="span_color"></span>
                            </div>
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litDetails" runat="server" Text="<%$ Resources:Resource, DETAILS%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:TextBox CssClass="text-box" ID="txtDetails" runat="server" TabIndex="8" placeholder="Enter Details"
                                    TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litServiceCenter" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal>
                                    <span class="span_star">*</span></label><div class="space">
                                    </div>
                                <asp:DropDownList ID="ddlServiceCenterName" TabIndex="3" Enabled="false" onchange="DropDownlistOnChangelbl(this,'spanddlServiceCenterName',' Service Center')"
                                    runat="server" CssClass="text-box text-select" AutoPostBack="true" OnSelectedIndexChanged="ddlServiceCenterName_SelectedIndexChanged">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <%--<span class="text-heading_2">
                                    <asp:HyperLink ID="lkbtnAddNewServiceCenter" NavigateUrl="~/Masters/AddServiceCenters.aspx"
                                        runat="server" Text="<%$ Resources:Resource, ADD%>"></asp:HyperLink>
                                </span>--%>
                                <div class="space">
                                </div>
                                <span id="spanddlServiceCenterName" class="span_color"></span>
                            </div>
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, MARKETER%>"></asp:Literal>
                                    <span class="span_star">*</span></label><div class="space">
                                    </div>
                                <asp:DropDownList ID="ddlMarketer" CssClass="text-box text-select" runat="server"
                                    TabIndex="6" onchange="DropDownlistOnChangelbl(this,'spanMarketer','Marketer')">
                                    <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <%--<span class="text-heading_2">
                                    <asp:HyperLink ID="HyperLink1" NavigateUrl="~/Masters/AddMarketers.aspx" runat="server"
                                        Text="<%$ Resources:Resource, ADD%>"></asp:HyperLink></span>--%>
                                <div class="space">
                                </div>
                                <span id="spanMarketer" class="span_color"></span>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <asp:Button ID="btnSave" TabIndex="9" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                CssClass="box_s" OnClick="btnSave_Click" runat="server" />
                            <%--<asp:Button ID="btnSaveNext" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVENEXT%>"
                                CssClass="calculate_but" runat="server" OnClick="btnSaveNext_Click" />--%>
                            <asp:Button ID="btnSaveNext" TabIndex="10" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVENEXT%>"
                                CssClass="box_s" runat="server" Visible="false" OnClick="btnSaveNext_Click" />
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div class="box_totalTwo_b">
                            <div class="text-heading_2">
                                <a href="SearchBookNoMaster.aspx"><span class="search_img"></span>
                                    <asp:Literal ID="litSearch" runat="server" Text="Book No Search"></asp:Literal></a></div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="grid_boxes">
                        <div class="grid_pagingTwo_top">
                            <div class="paging_top_titleTwo">
                                <asp:Literal ID="litCountriesList" runat="server" Text="<%$ Resources:Resource, GRID_BOOK_NUMBERS_LIST%>"></asp:Literal>
                            </div>
                            <div class="paging_top_rightTwo_content" id="divpaging" runat="server">
                                <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvBookNumbersList" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                            HeaderStyle-CssClass="grid_tb_hd" OnRowCommand="gvBookNumbersList_RowCommand"
                            OnRowDataBound="gvBookNumbersList_RowDataBound">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                    HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, BOOK_NAME%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" Visible="True" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridBookNo" runat="server" Text='<%#Eval("BookNo") %>'></asp:Label>
                                        <asp:Label ID="lblActiveStatusId" Visible="false" runat="server" Text='<%#Eval("ActiveStatusId") %>'></asp:Label>
                                        <asp:Label ID="lblActualBookNo" runat="server" Visible="false" Text='<%#Eval("BNO") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridBookName" placeholder="Enter Book Name"
                                            runat="server" Visible="false" MaxLength="20"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, BOOK_CODE%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBookCode" runat="server" Text='<%#Eval("BookCode") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Old Book" Visible="false" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridDetails" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridDetails" placeholder="Enter Details"
                                            runat="server" Visible="false" TextMode="MultiLine"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, NO_OF_ACCOUNT%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridNoOfAccounts" runat="server" Text='<%#Eval("NoOfAccounts") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridNoOfAccounts" MaxLength="10" placeholder="Enter No Of Accounts"
                                            runat="server" Visible="false"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="ftbGridNoOfAcc" runat="server" TargetControlID="txtGridNoOfAccounts"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, STATE_NAME%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblState" runat="server" Text='<%#Eval("StateName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, BU%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridBusinessUnitId" Visible="false" runat="server" Text='<%#Eval("BU_ID") %>'></asp:Label>
                                        <asp:DropDownList ID="ddlGridBusinessUnitName" runat="server" AutoPostBack="true"
                                            Visible="false" OnSelectedIndexChanged="ddlGidBusinessUnitName_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblGridBusinessUnitName" runat="server" Text='<%#Eval("BusinessUnitName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, SU%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridServiceUnitId" Visible="false" runat="server" Text='<%#Eval("SU_ID") %>'></asp:Label>
                                        <asp:DropDownList ID="ddlGridServiceUnitName" runat="server" AutoPostBack="true"
                                            Visible="false" OnSelectedIndexChanged="ddlGridServiceUnitName_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblGridServiceUnitName" runat="server" Text='<%#Eval("ServiceUnitName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, SC%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridServiceCenterId" Visible="false" runat="server" Text='<%#Eval("ServiceCenterId") %>'></asp:Label>
                                        <asp:DropDownList ID="ddlGridServiceCenterName" runat="server" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlGridServiceCenterName_SelectedIndexChanged" Visible="false">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblGridServiceCenterName" runat="server" Text='<%#Eval("ServiceCenterName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, CYCLE%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCycleId" Visible="false" runat="server" Text='<%#Eval("CycleId") %>'></asp:Label>
                                        <asp:Label ID="lblCycleName" runat="server" Text='<%#Eval("CycleName") %>'></asp:Label>
                                        <%--<asp:TextBox CssClass="text-box"  ID="txtGridCycleName" runat="server" Visible="false"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlGridCycle" runat="server" Visible="false">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, MARKETER%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMarketerId" Visible="false" runat="server" Text='<%#Eval("MarketerId") %>'></asp:Label>
                                        <asp:Label ID="lblMarketer" runat="server" Text='<%#Eval("Marketer") %>'></asp:Label>
                                        <%--<asp:TextBox CssClass="text-box"  ID="txtGridCycleName" runat="server" Visible="false"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlGridMarketer" runat="server" Visible="false">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lkbtnEdit" runat="server" ToolTip="Edit" Text="Edit" CommandName="EditBook">
                                        <img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnUpdate" Visible="false" runat="server" ToolTip="Update"
                                            CommandName="UpdateBook"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnCancel" Visible="false" runat="server" ToolTip="Cancel"
                                            CommandName="CancelBook"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnActive" Visible="false" runat="server" ToolTip="Activate Book Number"
                                            CommandName="ActiveBook"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnDeActive" runat="server" ToolTip="DeActivate Book Number"
                                            CommandName="DeActiveBook"><img src="../images/Deactivate.gif" alt="DeActive" /></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <%--<div class="page_na">
                            <uc2:UCPagingV2 ID="UCPagingV2" runat="server" />
                        </div>--%>
                        <asp:HiddenField ID="hfPageSize" runat="server" />
                        <asp:HiddenField ID="hfPageNo" runat="server" />
                        <asp:HiddenField ID="hfLastPage" runat="server" />
                        <asp:HiddenField ID="hfTotalRecords" runat="server" />
                        <asp:HiddenField ID="hfBookCodeLength" runat="server" />
                        <asp:HiddenField ID="hfRecognize" runat="server" />
                        <asp:HiddenField ID="hfBookNumberId" runat="server" />
                    </div>
                    <div class="grid_boxes">
                        <div id="divdownpaging" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="rnkland">
                <asp:ModalPopupExtender ID="mpeCountrypopup" runat="server" TargetControlID="btnpopup"
                    PopupControlID="pnlConfirmation" BackgroundCssClass="popbg">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnpopup" runat="server" Text="" CssClass="popbtn dis_none" />
                <asp:Panel runat="server" ID="pnlConfirmation" DefaultButton="btnOk" CssClass="editpopup_two"
                    Style="display: none;">
                    <div class="panelMessage">
                        <div>
                            <asp:Panel ID="pnlMsgPopup" runat="server" CssClass="aligncenter">
                                <asp:Label ID="lblMsgPopup" runat="server" Text=""></asp:Label>
                            </asp:Panel>
                        </div>
                        <div class="clear pad_10">
                        </div>
                        <div class="buttondiv">
                            <asp:Button ID="btnOk" runat="server" Text="OK" OnClick="btnOk_Click" CssClass="btn_ok"
                                class="m_left108" />
                            <div class="space">
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <%-- Active Btn popup Start--%>
                <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                    TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnActivate" runat="server" Text="Button" CssClass="popbtn dis_none" />
                <asp:Panel ID="PanelActivate" runat="server" CssClass="modalPopup"
                    class="poppnl" Style="display: none;">
                    <div class="popheader popheaderlblgreen">
                        <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                    </div>
                    <div class="space">
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btnActiveOk" runat="server" OnClick="btnActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                            <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- Active  Btn popup Closed--%>
                <%-- DeActive  Btn popup Start--%>
                <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                    TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnDeActivate" runat="server" Text="Button" CssClass="popbtn dis_none" />
                <asp:Panel ID="PanelDeActivate" runat="server" CssClass="modalPopup"
                    Style="display: none;">
                    <div class="popheader popheaderlblred">
                        <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <div class="space">
                            </div>
                            <asp:Button ID="btnDeActiveOk" runat="server" OnClick="btnDeActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                            <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- DeActive  popup Closed--%>
                <%-- Grid Alert popup Start--%>
                <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                    TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                </asp:ModalPopupExtender>
                <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                    <div class="popheader popheaderlblred">
                        <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- Grid Alert popup Closed--%>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--<asp:ModalPopupExtender ID="mpeCountrypopup" runat="server" TargetControlID="btnpopup"
                    PopupControlID="pnlConfirmation" BackgroundCssClass="popbg">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnpopup" runat="server" Text="" Style="display: none;" />
                <asp:Panel runat="server" ID="pnlConfirmation" CssClass="editpopup_two">
                    <div class="panelMessage">
                        <div>
                            <asp:Panel ID="pnlMsgPopup" runat="server" align="center">
                                <asp:Label ID="lblMsgPopup" runat="server" Text=""></asp:Label>
                            </asp:Panel>
                        </div>
                        <div class="clear pad_10">
                        </div>
                        <div class="buttondiv">
                            <asp:Button ID="btnOk" runat="server" Text="OK" OnClick="btnOk_Click" CssClass="btn_ok"
                                Style="margin-left: 106px;" />
                        </div>
                    </div>
                </asp:Panel>--%>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".rnkland").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                $(".popbg").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/PageValidations/AddBookNumbers.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>

    <script language="javascript" type="text/javascript">
        $(document).ready(function () { assignDiv('rptrMainMasterMenu_156_0', '89') });
    </script>
</asp:Content>
