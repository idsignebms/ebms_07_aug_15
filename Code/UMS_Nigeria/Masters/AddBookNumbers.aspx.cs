﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddBookNumbers
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 27-Feb-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Collections;

namespace UMS_Nigeria.Masters
{
    public partial class AddBookNumbers : System.Web.UI.Page
    {
        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        string Key = "AddBookNumber";
        Hashtable htableMaxLength = new Hashtable();
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }

        public string BusinessUnitName
        {
            get { return string.IsNullOrEmpty(ddlBusinessUnitName.SelectedValue) ? Constants.SelectedValue : ddlBusinessUnitName.SelectedValue; }
            set { ddlBusinessUnitName.SelectedValue = value.ToString(); }
        }
        public string ServiceUnitName
        {
            get { return string.IsNullOrEmpty(ddlServiceUnitName.SelectedValue) ? Constants.SelectedValue : ddlServiceUnitName.SelectedValue; }
            set { ddlServiceUnitName.SelectedValue = value.ToString(); }
        }
        public string ServiceCenterName
        {
            get { return string.IsNullOrEmpty(ddlServiceCenterName.SelectedValue) ? Constants.SelectedValue : ddlServiceCenterName.SelectedValue; }
            set { ddlServiceCenterName.SelectedValue = value.ToString(); }
        }

        private string BookNumber
        {
            get { return txtBookNo.Text.Trim(); }
            set { txtBookNo.Text = value; }
        }
        private string Details
        {
            get { return txtDetails.Text.Trim(); }
            set { txtDetails.Text = value; }
        }
        //private string BookCode
        //{
        //    get { return txtBookCode.Text.Trim(); }
        //    set { txtBookCode.Text = value; }
        //}
        private int NoOfAccount
        {
            get { return string.IsNullOrEmpty(txtNoOfAccount.Text.Trim()) ? 0 : Convert.ToInt32(txtNoOfAccount.Text); }
        }

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = _ObjCommonMethods.GetPagePath(this.Request.Url);
                //if (_ObjCommonMethods.IsAccess(path))
                //{

                //PageAccessPermissions();
                _ObjCommonMethods.BindCyclesList(ddlCycle, true);

                _ObjCommonMethods.BindMarketers(ddlMarketer, string.Empty, UMS_Resource.DROPDOWN_SELECT, true);
                if (!string.IsNullOrEmpty(Request.QueryString[UMS_Resource.QSTR_CYCLE_ID]))
                {
                    ddlCycle.SelectedValue = objiDsignBAL.Decrypt(Request.QueryString[UMS_Resource.QSTR_CYCLE_ID]).ToString();
                }
                //AssignMaxLength();
                _ObjCommonMethods.BindBusinessUnits(ddlBusinessUnitName, string.Empty, true);
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                {
                    ddlBusinessUnitName.SelectedIndex = ddlBusinessUnitName.Items.IndexOf(ddlBusinessUnitName.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                    ddlBusinessUnitName.Enabled = false;
                    ddlBusinessUnitName_SelectedIndexChanged(ddlBusinessUnitName, new EventArgs());
                    _ObjCommonMethods.BindMarketers(ddlMarketer, Session[UMS_Resource.SESSION_USER_BUID].ToString(), UMS_Resource.DROPDOWN_SELECT, true);
                }
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                //}
                //else
                //{
                //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                //}
            }
        }

        //private void PageAccessPermissions()
        //{
        //    try
        //    {
        //        if (_ObjCommonMethods.IsAccess(UMS_Resource.ADD_BUSINESS_UNITS_PAGE)) { lkbtnAddNewBusinessUnit.Visible = true; } else { lkbtnAddNewBusinessUnit.Visible = false; }
        //        if (_ObjCommonMethods.IsAccess(UMS_Resource.ADD_SERVICE_UNITS_PAGE)) { lkbtnAddNewServiceUnit.Visible = true; } else { lkbtnAddNewServiceUnit.Visible = false; }
        //        if (_ObjCommonMethods.IsAccess(UMS_Resource.ADD_SERVICE_CENTER_PAGE)) { lkbtnAddNewServiceCenter.Visible = true; } else { lkbtnAddNewServiceCenter.Visible = false; }
        //        if (_ObjCommonMethods.IsAccess(UMS_Resource.ADD_CYCLE_PAGE)) { lkbtnAddNewCycle.Visible = true; } else { lkbtnAddNewCycle.Visible = false; }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void lkbtnAddNewServiceUnit_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_SERVICE_UNITS_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnAddNewBusinessUnit_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_BUSINESS_UNITS_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnAddNewServiceCenter_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_SERVICE_CENTER_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlBusinessUnitName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlBusinessUnitName.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindServiceUnits(ddlServiceUnitName, BusinessUnitName, true);
                    ddlServiceUnitName.Enabled = true;
                    ddlCycle.SelectedIndex = ddlServiceCenterName.SelectedIndex = ddlServiceUnitName.SelectedIndex = 0;
                    ddlServiceCenterName.Enabled = ddlCycle.Enabled = false;
                }
                else
                {
                    ddlCycle.SelectedIndex = ddlServiceUnitName.SelectedIndex = ddlServiceCenterName.SelectedIndex = 0;
                    ddlCycle.Enabled = ddlServiceCenterName.Enabled = ddlServiceUnitName.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlServiceUnitName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlServiceUnitName.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindServiceCenters(ddlServiceCenterName, ServiceUnitName, true);
                    ddlServiceCenterName.Enabled = true;
                    ddlCycle.Enabled = false;
                    ddlCycle.SelectedIndex = 0;
                }
                else
                {
                    ddlCycle.SelectedIndex = ddlServiceCenterName.SelectedIndex = 0;
                    ddlCycle.Enabled = ddlServiceCenterName.Enabled = false;
                }

                ddlServiceCenterName.Focus();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlServiceCenterName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlServiceUnitName.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindCyclesByBUSUSC(ddlCycle, BusinessUnitName, ServiceUnitName, ServiceCenterName, true, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlCycle.Enabled = true;
                    ddlCycle.SelectedIndex = 0;
                }
                else
                {
                    ddlCycle.SelectedIndex = 0;
                    ddlCycle.Enabled = false;
                }
                ddlCycle.Focus();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlGidBusinessUnitName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlGridBusinessUnitName = (DropDownList)sender;
                DropDownList ddlGridServiceUnitName = (DropDownList)ddlGridBusinessUnitName.NamingContainer.FindControl("ddlGridServiceUnitName");
                DropDownList ddlGridServiceCenterName = (DropDownList)ddlGridBusinessUnitName.NamingContainer.FindControl("ddlGridServiceCenterName");
                DropDownList ddlGridCycle = (DropDownList)ddlGridBusinessUnitName.NamingContainer.FindControl("ddlGridCycle");
                if (ddlGridBusinessUnitName.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindServiceUnits(ddlGridServiceUnitName, ddlGridBusinessUnitName.SelectedValue, true);
                    ddlGridServiceUnitName.Enabled = true;
                    ddlGridCycle.SelectedIndex = ddlGridServiceCenterName.SelectedIndex = ddlGridServiceUnitName.SelectedIndex = 0;
                    ddlGridCycle.Enabled = ddlGridServiceCenterName.Enabled = false;
                }
                else
                {
                    ddlGridCycle.SelectedIndex = ddlGridServiceUnitName.SelectedIndex = ddlGridServiceCenterName.SelectedIndex = 0;
                    ddlGridCycle.Enabled = ddlGridServiceCenterName.Enabled = ddlGridServiceUnitName.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlGridServiceUnitName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlGridServiceUnitName = (DropDownList)sender;
                DropDownList ddlGridServiceCenterName = (DropDownList)ddlGridServiceUnitName.NamingContainer.FindControl("ddlGridServiceCenterName");
                DropDownList ddlGridCycle = (DropDownList)ddlGridServiceUnitName.NamingContainer.FindControl("ddlGridCycle");
                if (ddlGridServiceUnitName.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindServiceCenters(ddlGridServiceCenterName, ddlGridServiceUnitName.SelectedValue, true);
                    ddlGridServiceCenterName.Enabled = true;
                    ddlGridCycle.Enabled = false;
                    ddlGridCycle.SelectedIndex = 0;
                }
                else
                {
                    ddlGridCycle.SelectedIndex = ddlGridServiceCenterName.SelectedIndex = 0;
                    ddlGridCycle.Enabled = ddlGridServiceCenterName.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlGridServiceCenterName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlGridServiceCenterName = (DropDownList)sender;
                DropDownList ddlGridBusinessUnitName = (DropDownList)ddlGridServiceCenterName.NamingContainer.FindControl("ddlGridBusinessUnitName");
                DropDownList ddlGridServiceUnitName = (DropDownList)ddlGridServiceCenterName.NamingContainer.FindControl("ddlGridServiceUnitName");
                DropDownList ddlGridServiceCenter = (DropDownList)ddlGridServiceCenterName.NamingContainer.FindControl("ddlGridServiceCenterName");
                DropDownList ddlGridCycle = (DropDownList)ddlGridServiceCenterName.NamingContainer.FindControl("ddlGridCycle");
                if (ddlGridServiceCenterName.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindCyclesByBUSUSC(ddlGridCycle, ddlGridBusinessUnitName.SelectedValue, ddlGridServiceUnitName.SelectedValue, ddlGridServiceCenter.SelectedValue, true, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlGridCycle.Enabled = true;
                }
                else
                {
                    ddlGridCycle.SelectedIndex = 0;
                    ddlGridCycle.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                hfRecognize.Value = UMS_Resource.SAVE;
                lblActiveMsg.Text = Resource.CHK_BEFORE_U_SAVE;
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSaveNext_Click(object sender, EventArgs e)
        {
            try
            {
                hfRecognize.Value = UMS_Resource.SAVENEXT;
                lblActiveMsg.Text = Resource.CHK_BEFORE_U_SAVE;
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                // Response.Redirect(UMS_Resource.ADD_BOOK_NUMBERS_PAGE);
                //Response.Redirect(UMS_Resource.ADD_SERVICE_UNITS_PAGE + "?" + UMS_Resource.QSTR_BUSINESS_UNIT_CODE + "=" + objiDsignBAL.Encrypt(hfBU_ID.Value), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnAddNewCycle_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_CYCLE_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvBookNumbersList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                hfRecognize.Value = string.Empty;
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridNoOfAccounts = (TextBox)row.FindControl("txtGridNoOfAccounts");
                DropDownList ddlGridBusinessUnitName = (DropDownList)row.FindControl("ddlGridBusinessUnitName");
                DropDownList ddlGridServiceUnitName = (DropDownList)row.FindControl("ddlGridServiceUnitName");
                DropDownList ddlGridServiceCenterName = (DropDownList)row.FindControl("ddlGridServiceCenterName");
                TextBox txtGridBookName = (TextBox)row.FindControl("txtGridBookName");
                TextBox txtGridDetails = (TextBox)row.FindControl("txtGridDetails");
                DropDownList ddlGridCycle = (DropDownList)row.FindControl("ddlGridCycle");
                DropDownList ddlGridMarketer = (DropDownList)row.FindControl("ddlGridMarketer");

                Label lblMarketerId = (Label)row.FindControl("lblMarketerId");
                Label lblMarketer = (Label)row.FindControl("lblMarketer");
                Label lblGridBookNo = (Label)row.FindControl("lblGridBookNo");
                Label lblActualBookNo = (Label)row.FindControl("lblActualBookNo");
                Label lblGridNoOfAccounts = (Label)row.FindControl("lblGridNoOfAccounts");
                Label lblGridBusinessUnitName = (Label)row.FindControl("lblGridBusinessUnitName");
                Label lblGridServiceUnitName = (Label)row.FindControl("lblGridServiceUnitName");
                Label lblGridServiceCenterName = (Label)row.FindControl("lblGridServiceCenterName");
                Label lblGridDetails = (Label)row.FindControl("lblGridDetails");
                Label lblGridBusinessUnitId = (Label)row.FindControl("lblGridBusinessUnitId");
                Label lblGridServiceUnitId = (Label)row.FindControl("lblGridServiceUnitId");
                Label lblGridServiceCenterId = (Label)row.FindControl("lblGridServiceCenterId");
                Label lblCycleName = (Label)row.FindControl("lblCycleName");
                Label lblCycleId = (Label)row.FindControl("lblCycleId");

                objMastersBE.Flag = Convert.ToInt32(Resource.FLAG_ACCOUNT_BOOK_NO);
                objMastersBE.BookNo = lblGridBookNo.Text;

                objMastersBE = _ObjCommonMethods.CustomerBillProcessDetails(objMastersBE);

                if (!objMastersBE.Status)
                {
                    switch (e.CommandName.ToUpper())
                    {
                        case "EDITBOOK":
                            lblMarketer.Visible = lblCycleName.Visible = lblGridDetails.Visible = lblGridNoOfAccounts.Visible = lblGridBusinessUnitName.Visible = lblGridServiceUnitName.Visible = lblGridServiceCenterName.Visible =lblGridBookNo.Visible = false;
                            ddlGridMarketer.Visible = ddlGridCycle.Visible = txtGridNoOfAccounts.Visible = ddlGridBusinessUnitName.Visible = ddlGridServiceUnitName.Visible = ddlGridServiceCenterName.Visible = txtGridDetails.Visible = txtGridBookName.Visible = true;
                            txtGridNoOfAccounts.Text = lblGridNoOfAccounts.Text;

                            _ObjCommonMethods.BindBusinessUnits(ddlGridBusinessUnitName, string.Empty, true);
                            _ObjCommonMethods.BindMarketers(ddlGridMarketer, string.Empty, UMS_Resource.DROPDOWN_SELECT, false);
                            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                            {
                                ddlGridBusinessUnitName.SelectedIndex = ddlGridBusinessUnitName.Items.IndexOf(ddlGridBusinessUnitName.Items.FindByValue(lblGridBusinessUnitId.Text));
                                ddlGridBusinessUnitName.Enabled = false;
                                _ObjCommonMethods.BindMarketers(ddlGridMarketer, ddlGridBusinessUnitName.SelectedValue, UMS_Resource.DROPDOWN_SELECT, false);
                                ddlGridMarketer.SelectedIndex = ddlGridMarketer.Items.IndexOf(ddlGridMarketer.Items.FindByValue(lblMarketerId.Text));
                            }

                            ddlGridBusinessUnitName.SelectedIndex = ddlGridBusinessUnitName.Items.IndexOf(ddlGridBusinessUnitName.Items.FindByValue(lblGridBusinessUnitId.Text));
                            ddlGridMarketer.SelectedIndex = ddlGridMarketer.Items.IndexOf(ddlGridMarketer.Items.FindByValue(lblMarketerId.Text));
                            _ObjCommonMethods.BindServiceUnits(ddlGridServiceUnitName, ddlGridBusinessUnitName.SelectedValue, true);
                            ddlGridServiceUnitName.SelectedIndex = ddlGridServiceUnitName.Items.IndexOf(ddlGridServiceUnitName.Items.FindByValue(lblGridServiceUnitId.Text));

                            _ObjCommonMethods.BindServiceCenters(ddlGridServiceCenterName, ddlGridServiceUnitName.SelectedValue, true);
                            ddlGridServiceCenterName.SelectedIndex = ddlGridServiceCenterName.Items.IndexOf(ddlGridServiceCenterName.Items.FindByValue(lblGridServiceCenterId.Text));

                            _ObjCommonMethods.BindCyclesByBUSUSC(ddlGridCycle, ddlGridBusinessUnitName.SelectedValue, ddlGridServiceUnitName.SelectedValue, ddlGridServiceCenterName.SelectedValue, true, UMS_Resource.DROPDOWN_SELECT, false);
                            ddlGridCycle.SelectedIndex = ddlGridCycle.Items.IndexOf(ddlGridCycle.Items.FindByValue(lblCycleId.Text));
                            txtGridDetails.Text = objiDsignBAL.ReplaceNewLines(lblGridDetails.Text, false);
                            txtGridBookName.Text = lblGridBookNo.Text;


                            row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                            row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;
                            break;
                        case "CANCELBOOK":
                            row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                            row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                            ddlGridMarketer.Visible = ddlGridCycle.Visible = txtGridNoOfAccounts.Visible = ddlGridBusinessUnitName.Visible = ddlGridServiceUnitName.Visible = ddlGridServiceCenterName.Visible = txtGridDetails.Visible = txtGridBookName.Visible = false;
                            lblMarketer.Visible = lblCycleName.Visible = lblGridDetails.Visible = lblGridNoOfAccounts.Visible = lblGridBusinessUnitName.Visible = lblGridServiceUnitName.Visible = lblGridServiceCenterName.Visible = lblGridBookNo.Visible = true;
                            break;
                        case "UPDATEBOOK":
                            objMastersBE.BookNo = txtGridBookName.Text.Trim();
                            objMastersBE.BU_ID = ddlGridBusinessUnitName.SelectedValue;
                            objMastersBE.SU_ID = ddlGridServiceUnitName.SelectedValue;
                            //objMastersBE.BookCode = txtGridBookCode.Text;
                            objMastersBE.BNO = lblActualBookNo.Text;
                            objMastersBE.OldSeviceCenterID = lblGridServiceCenterId.Text;
                            objMastersBE.ServiceCenterId = ddlGridServiceCenterName.SelectedValue;
                            objMastersBE.NoOfAccounts = string.IsNullOrEmpty(txtGridNoOfAccounts.Text) ? Constants.Zero : Convert.ToInt32(txtGridNoOfAccounts.Text);
                            objMastersBE.CycleId = ddlGridCycle.SelectedValue;
                            objMastersBE.Details = objiDsignBAL.ReplaceNewLines(txtGridDetails.Text, true);
                            objMastersBE.MarketerId = Convert.ToInt32(ddlGridMarketer.SelectedValue);
                            objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                            objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.BookNumber(objMastersBE, Statement.Update));

                            if (Convert.ToBoolean(objMastersBE.IsSuccess))
                            {
                                BindGrid();
                                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                                Message(UMS_Resource.BOOK_UPDATED, UMS_Resource.MESSAGETYPE_SUCCESS);
                            }
                            else if (objMastersBE.IsBookCodeExists)
                            {
                                Message(Resource.BOOK_NAME_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                            }

                            break;
                        case "ACTIVEBOOK":
                            mpeActivate.Show();
                            //hfBookNumberId.Value = lblGridBookNo.Text;
                            hfBookNumberId.Value =lblActualBookNo.Text;//Neeraj
                            lblActiveMsg.Text = UMS_Resource.ACTIVE_BOOK_POPUP_TEXT;
                            btnActiveOk.Focus();
                            //objMastersBE.ActiveStatusId = Constants.Active;
                            //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                            //objMastersBE.BookNo = lblGridBookNo.Text;
                            //xml = objMastersBAL.BookNumber(objMastersBE, Statement.Delete);
                            //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                            //if (Convert.ToBoolean(objMastersBE.IsSuccess))
                            //{
                            //    BindGridBookNumbersList();
                            //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            //    Message(UMS_Resource.BOOK_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                            //}
                            //else
                            //    Message(UMS_Resource.BOOK_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);

                            break;
                        case "DEACTIVEBOOK":
                            mpeDeActive.Show();
                            hfBookNumberId.Value = lblActualBookNo.Text;
                            lblDeActiveMsg.Text = UMS_Resource.DEACTIVE_BOOK_POPUP_TEXT;
                            btnDeActiveOk.Focus();
                            //objMastersBE.ActiveStatusId = Constants.DeActive;
                            //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                            //objMastersBE.BookNo = lblGridBookNo.Text;
                            //xml = objMastersBAL.BookNumber(objMastersBE, Statement.Delete);
                            //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                            //if (Convert.ToBoolean(objMastersBE.IsSuccess))
                            //{
                            //    BindGridBookNumbersList();
                            //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            //    Message(UMS_Resource.BOOK_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                            //}
                            //else
                            //    Message(UMS_Resource.BOOK_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);

                            break;
                    }

                }
                else
                {
                    btnDeActiveOk.Visible = false;
                    lblDeActiveMsg.Text = Resource.BILL_IN_PROCESS_BOOK_NO;
                    btnDeActiveCancel.Text = Resource.OK;
                    mpeDeActive.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (hfRecognize.Value == Constants.SAVE)
                {
                    hfRecognize.Value = string.Empty;
                    MastersBE objMastersBE = InsertBookDetails();
                    if (objMastersBE.IsSuccess)
                    {
                        BindGrid();
                        UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        Message(UMS_Resource.BOOK_INSERT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        txtNoOfAccount.Text = txtBookNo.Text = txtDetails.Text = string.Empty;
                        ddlMarketer.SelectedIndex = 0;
                    }
                    else if (objMastersBE.IsBookNumberExists)
                    {
                        Message(UMS_Resource.BOOK_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else if (objMastersBE.IsBookCodeExists)
                    {
                        Message(Resource.BOOK_CODE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else if (string.IsNullOrEmpty(objMastersBE.BookCode))
                    {
                        Message("Can not add the Book Number. Problem in Book Code Generation.", UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else
                    {
                        Message(UMS_Resource.BOOK_INSERT_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
                else if (hfRecognize.Value == Constants.SAVENEXT)
                {
                    hfRecognize.Value = string.Empty;
                    MastersBE objMastersBE = InsertBookDetails();
                    if (objMastersBE.IsSuccess)
                    {
                        BindGrid();
                        UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        txtNoOfAccount.Text = txtBookNo.Text = txtDetails.Text = string.Empty;
                        lblMsgPopup.Text = UMS_Resource.BOOK_INSERT_SUCCESS;
                        mpeCountrypopup.Show();
                    }
                    else if (objMastersBE.IsBookNumberExists)
                    {
                        Message(UMS_Resource.BOOK_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else if (objMastersBE.IsBookCodeExists)
                    {
                        Message(Resource.BOOK_CODE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else
                    {
                        Message(UMS_Resource.BOOK_INSERT_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
                else
                {
                    MastersBE objMastersBE = new MastersBE();
                    objMastersBE.ActiveStatusId = Constants.Active;
                    objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objMastersBE.BookNo = hfBookNumberId.Value;
                    xml = objMastersBAL.BookNumber(objMastersBE, Statement.Delete);
                    objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                    if (Convert.ToBoolean(objMastersBE.IsSuccess))
                    {
                        BindGrid();
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        Message(UMS_Resource.BOOK_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                    }
                    else
                        Message(UMS_Resource.BOOK_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.DeActive;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.BookNo = hfBookNumberId.Value;
                xml = objMastersBAL.BookNumber(objMastersBE, Statement.Delete);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (objMastersBE.IsSuccess)
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.BOOK_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else if (!objMastersBE.IsSuccess && objMastersBE.Count > 0)
                {
                    //Message(string.Format(UMS_Resource.BOOK_HAVE_CUSTOMERS, objMastersBE.Count), UMS_Resource.MESSAGETYPE_ERROR);
                    lblgridalertmsg.Text = UMS_Resource.BOOK_HAVE_CUSTOMERS;
                    mpegridalert.Show();
                }
                else
                    Message(UMS_Resource.BOOK_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvBookNumbersList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    DropDownList ddlGridBusinessUnitName = (DropDownList)e.Row.FindControl("ddlGridBusinessUnitName");
                    DropDownList ddlGridServiceUnitName = (DropDownList)e.Row.FindControl("ddlGridServiceUnitName");
                    DropDownList ddlGridServiceCenterName = (DropDownList)e.Row.FindControl("ddlGridServiceCenterName");
                    DropDownList ddlGridMarketer = (DropDownList)e.Row.FindControl("ddlGridMarketer");
                    DropDownList ddlGridCycle = (DropDownList)e.Row.FindControl("ddlGridCycle");
                    //htableMaxLength = _ObjCommonMethods.getApplicationSettings(UMS_Resource.SETTING_BOOK_NUM_LENGTH);
                    //txtGridBookCode.Attributes.Add("MaxLength", htableMaxLength[Constants.BookNumbers].ToString());
                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + ddlGridBusinessUnitName.ClientID + "','"
                                                                                      + ddlGridServiceUnitName.ClientID + "','"
                                                                                      + ddlGridServiceCenterName.ClientID + "','"
                                                                                      + ddlGridCycle.ClientID + "','"
                                                                                      + ddlGridMarketer.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");


                    if (Convert.ToInt32(lblActiveStatusId.Text) == Constants.DeActive)
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        private MastersBE InsertBookDetails()
        {
            MastersBE objMastersBE = new MastersBE();
            try
            {

                objMastersBE.BU_ID = BusinessUnitName;
                objMastersBE.SU_ID = ServiceUnitName;
                objMastersBE.ServiceCenterId = ServiceCenterName;
                objMastersBE.BookNo = BookNumber;
                //objMastersBE.BookCode = BookCode;
                objMastersBE.MarketerId = Convert.ToInt32(ddlMarketer.SelectedValue);
                objMastersBE.Details = objiDsignBAL.ReplaceNewLines(Details, true);
                objMastersBE.NoOfAccounts = NoOfAccount;
                objMastersBE.CycleId = ddlCycle.SelectedValue;
                objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.BookNumber(objMastersBE, Statement.Insert));

                return objMastersBE;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void BindGrid()
        {
            try
            {
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();
                XmlDocument resultedXml = new XmlDocument();

                objMastersBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                objMastersBE.PageSize = PageSize;
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    objMastersBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    objMastersBE.BU_ID = string.Empty;
                resultedXml = objMastersBAL.BookNumber(objMastersBE, ReturnType.Get);

                objMastersListBE = objiDsignBAL.DeserializeFromXml<MastersListBE>(resultedXml);
                DataTable dt = new DataTable();

                if (objMastersListBE.Items.Count > 0)
                {
                    divdownpaging.Visible = divpaging.Visible = true;
                    hfTotalRecords.Value = objMastersListBE.Items[0].TotalRecords.ToString();
                    gvBookNumbersList.DataSource = objMastersListBE.Items;
                    gvBookNumbersList.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = objMastersListBE.Items.Count.ToString();
                    divdownpaging.Visible = divpaging.Visible = false;
                    gvBookNumbersList.DataSource = dt;
                    gvBookNumbersList.DataBind();
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void AssignMaxLength()
        {
            try
            {
                htableMaxLength = _ObjCommonMethods.getApplicationSettings(UMS_Resource.SETTING_BOOK_NUM_LENGTH);
                //txtBookCode.Attributes.Add("MaxLength", htableMaxLength[Constants.BookNumbers].ToString());
                hfBookCodeLength.Value = htableMaxLength[Constants.BookNumbers].ToString();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}