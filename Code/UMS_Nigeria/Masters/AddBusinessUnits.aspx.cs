﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddBusinessUnits
                     
 Developer        : id065-Bhimaraju V
 Creation Date    : 19-Feb-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using Resources;
using iDSignHelper;
using System.Xml;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Globalization;
using System.Collections;

namespace UMS_Nigeria.Masters
{
    public partial class AddBusinessUnits : System.Web.UI.Page
    {
        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        string Key = "AddBusinessUnits";
        DataTable dtData = new DataTable();
        CustomerDetailsBe _ObjCustomerDetailsBe = new CustomerDetailsBe();
        CustomerDetailsBAL _ObjCustomerDetailsBAL = new CustomerDetailsBAL();
        static int customersCount = 0;
        Hashtable htableMaxLength = new Hashtable();
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = objCommonMethods.GetPagePath(this.Request.Url);
                //if (objCommonMethods.IsAccess(path))
                //{
                //PageAccessPermissions();
                //AssignMaxLength();
                //objCommonMethods.BindStates(ddlStateName, string.Empty, true);
                objCommonMethods.BindStatesByBusinessUnits(ddlStateName, Session[UMS_Resource.SESSION_LOGINID].ToString(), string.Empty, true, UMS_Resource.DROPDOWN_SELECT, false);
                //objCommonMethods.BindDistricts(ddlDistrictName, string.Empty, true);
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                //}
                //else
                //{
                //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                //}
            }

        }

        //private void PageAccessPermissions()
        //{
        //    try
        //    {
        //        if (objCommonMethods.IsAccess(UMS_Resource.ADD_STATES_PAGE)) { lkbtnAddNewState.Visible = true; } else { lkbtnAddNewState.Visible = false; }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void btnSaveNext_Click(object sender, EventArgs e)
        {
            try
            {
                hfRecognize.Value = UMS_Resource.SAVENEXT;
                lblActiveMsg.Text = Resource.CHK_BEFORE_U_SAVE;
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnAddNewState_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_STATES_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                btnSaveNext.Visible = true;
                txtBusinessUnitName.Text = txtNotes.Text = string.Empty;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                hfRecognize.Value = UMS_Resource.SAVE;
                lblActiveMsg.Text = Resource.CHK_BEFORE_U_SAVE;
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //protected void ddlStateName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        objCommonMethods.BindDistricts(ddlDistrictName, ddlStateName.SelectedValue, true);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message,UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void ddlGridStateName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {

        //        DropDownList ddlGridStateName = (DropDownList)sender;
        //        DropDownList ddlGridDistrictName = (DropDownList)ddlGridStateName.NamingContainer.FindControl("ddlGridDistrictName");
        //        objCommonMethods.BindDistricts(ddlGridDistrictName, ddlGridStateName.SelectedValue, true);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message,UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}        

        protected void gvBusinessUnitsList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridBusinessUnitName = (TextBox)row.FindControl("txtGridBusinessUnitName");
                //TextBox txtGridBUCode = (TextBox)row.FindControl("txtGridBUCode");
                TextBox txtGridNotes = (TextBox)row.FindControl("txtGridNotes");
                DropDownList ddlGridStateName = (DropDownList)row.FindControl("ddlGridStateName");

               // DropDownList ddlGridRegionName = (DropDownList)row.FindControl("ddlGridRegionName");

                Label lblBusinessUnitId = (Label)row.FindControl("lblBusinessUnitId");
                Label lblBusinessUnitName = (Label)row.FindControl("lblBusinessUnitName");
                Label lblStateCode = (Label)row.FindControl("lblStateCode");
                //Label lblRegionId = (Label)row.FindControl("lblRegionId");
                Label lblStateName = (Label)row.FindControl("lblStateName");
                //Label lblRegionName = (Label)row.FindControl("lblRegionName");
                Label lblNotes = (Label)row.FindControl("lblNotes");

                Label lblAddress1 = (Label)row.FindControl("lblAddress1");
                Label lblAddress2 = (Label)row.FindControl("lblAddress2");
                Label lblCity = (Label)row.FindControl("lblCity");
                Label lblZip = (Label)row.FindControl("lblZip");
                TextBox txtGridAddress1 = (TextBox)row.FindControl("txtGridAddress1");
                TextBox txtGridAddress2 = (TextBox)row.FindControl("txtGridAddress2");
                TextBox txtGridCity = (TextBox)row.FindControl("txtGridCity");
                TextBox txtGridZip = (TextBox)row.FindControl("txtGridZip");

                // Label lblBUCode = (Label)row.FindControl("lblBUCode");
                hfRecognize.Value = string.Empty;
                switch (e.CommandName.ToUpper())
                {
                    case "EDITBUSINESSUNIT":
                        lblStateName.Visible = lblStateCode.Visible = lblNotes.Visible = lblStateCode.Visible =
                         lblBusinessUnitName.Visible = lblAddress1.Visible = lblAddress2.Visible = lblCity.Visible = lblZip.Visible = false;
                        txtGridBusinessUnitName.Visible = ddlGridStateName.Visible = txtGridNotes.Visible =
                            txtGridAddress1.Visible = txtGridAddress2.Visible = txtGridCity.Visible = txtGridZip.Visible = true;
                        txtGridBusinessUnitName.Text = lblBusinessUnitName.Text;

                        txtGridAddress1.Text = lblAddress1.Text;
                        txtGridAddress2.Text = lblAddress2.Text;
                        txtGridCity.Text = lblCity.Text;
                        txtGridZip.Text = lblZip.Text;

                        //txtGridBUCode.Text = lblBUCode.Text;
                        objCommonMethods.BindStates(ddlGridStateName, string.Empty, true);
                        //objCommonMethods.BindRegions(ddlGridRegionName, lblStateCode.Text, true,
                        //    UMS_Resource.DROPDOWN_SELECT);
                        ddlGridStateName.SelectedIndex = ddlGridStateName.Items.IndexOf(ddlGridStateName.Items.FindByValue(lblStateCode.Text));
                        //ddlGridRegionName.SelectedIndex = ddlGridRegionName.Items.IndexOf(ddlGridRegionName.Items.FindByValue(lblRegionId.Text));
                        txtGridNotes.Text = objiDsignBAL.ReplaceNewLines(lblNotes.Text, false);
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;
                        break;
                    case "CANCELBUSINESSUNIT":
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                        txtGridBusinessUnitName.Visible = ddlGridStateName.Visible =  txtGridNotes.Visible = txtGridAddress1.Visible = txtGridAddress2.Visible = txtGridCity.Visible = txtGridZip.Visible = false;
                        lblStateName.Visible = lblNotes.Visible =  lblBusinessUnitName.Visible = lblAddress1.Visible = lblAddress2.Visible = lblCity.Visible = lblZip.Visible = true;
                        break;
                    case "UPDATEBUSINESSUNIT":
                        objMastersBE.BU_ID = lblBusinessUnitId.Text;
                        //objMastersBE.DistrictCode = ddlGridDistrictName.SelectedValue;
                        objMastersBE.StateCode = ddlGridStateName.SelectedValue;
                        //objMastersBE.RegionId = Convert.ToInt32(ddlGridRegionName.SelectedValue);
                        //objMastersBE.BUCode = txtGridBUCode.Text;
                        objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objMastersBE.BusinessUnitName = txtGridBusinessUnitName.Text;

                        objMastersBE.Address1 = txtGridAddress1.Text.Trim();
                        objMastersBE.Address2 = txtGridAddress2.Text.Trim();
                        objMastersBE.City = txtGridCity.Text.Trim();
                        objMastersBE.ZIP = txtGridZip.Text.Trim();

                        objMastersBE.Notes = objiDsignBAL.ReplaceNewLines(txtGridNotes.Text, true);
                        xml = objMastersBAL.BusinessUnits(objMastersBE, Statement.Update);
                        objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                        if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        {
                            BindGrid();
                            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            Message(UMS_Resource.BUSINESS_UNITS_UPDATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                        }
                        else if (objMastersBE.IsBUCodeExists)
                        {
                            Message(Resource.BUSINESS_UNIT_CODE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else
                            Message(UMS_Resource.BUSINESS_UNIT_DETAILS_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "ACTIVEBUSINESSUNIT":
                        hfBU_ID.Value = lblBusinessUnitId.Text;
                        lblActiveMsg.Text = UMS_Resource.ACTIVE_BUSINESSUNIT_POPUP_TEXT;
                        btnActiveOk.Focus();
                        mpeActivate.Show();
                        //objMastersBE.ActiveStatusId = Constants.Active;
                        //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objMastersBE.BU_ID = lblBusinessUnitId.Text;
                        //xml = objMastersBAL.BusinessUnits(objMastersBE, Statement.Delete);
                        //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                        //if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        //{
                        //    BindGridBusinessUnitsList();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message( UMS_Resource.BUSINESS_UNITS_ACTIVATE_SUCESS,UMS_Resource.MESSAGETYPE_SUCCESS);

                        //}
                        //else
                        //    Message( UMS_Resource.BUSINESS_UNITS_ACTIVATE_FAIL,UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "DEACTIVEBUSINESSUNIT":
                        //if (CustomerCount(e.CommandArgument.ToString()))
                        //{
                        //    mpeShiftCustomer.Show();
                        //    //hfBU_ID.Value = lblBusinessUnitId.Text;
                        //    lblShiftCustomer.Text = UMS_Resource.SHIFT_CUSTOMERS_BU.Insert(23, customersCount.ToString() + " ");
                        //    btnShipCustomer.Focus();
                        //}
                        //else
                        //{
                            lblDeActiveMsg.Text = UMS_Resource.DEACTIVE_BUSINESSUNIT_POPUP_TEXT;
                            mpeDeActive.Show();
                            hfBU_ID.Value = lblBusinessUnitId.Text;

                            btnDeActiveOk.Focus();
                        //}


                        //objMastersBE.ActiveStatusId = Constants.DeActive;
                        //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objMastersBE.BU_ID = lblBusinessUnitId.Text;
                        //xml = objMastersBAL.BusinessUnits(objMastersBE, Statement.Delete);
                        //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                        //if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        //{
                        //    BindGridBusinessUnitsList();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message( UMS_Resource.BUSINESS_UNITS_DEACTIVATE_SUCESS,UMS_Resource.MESSAGETYPE_SUCCESS);

                        //}
                        //else
                        //    Message(UMS_Resource.BUSINESS_UNITS_DEACTIVATE_FAIL,UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (hfRecognize.Value == Constants.SAVE)
                {
                    hfRecognize.Value = string.Empty;
                    MastersBE objMastersBE = new MastersBE();
                    objMastersBE.BusinessUnitName = txtBusinessUnitName.Text;
                    objMastersBE.StateCode = ddlStateName.SelectedValue;
                    objMastersBE.Address1 = txtAddress1.Text;
                    objMastersBE.Address2 = txtAddress2.Text;
                    objMastersBE.City = txtCity.Text;
                    objMastersBE.ZIP = txtZipCode.Text;
                    //objMastersBE.RegionId = Convert.ToInt32(ddlRegion.SelectedValue);
                    //objMastersBE.DistrictCode = ddlDistrictName.SelectedValue;
                    // objMastersBE.BUCode = txtBUCode.Text;
                    objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objMastersBE.Notes = objiDsignBAL.ReplaceNewLines(txtNotes.Text, true);
                    xml = objMastersBAL.BusinessUnits(objMastersBE, Statement.Insert);

                    objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                    if (Convert.ToBoolean(objMastersBE.IsSuccess))
                    {
                        BindGrid();
                        UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //ClearControls();
                        txtBusinessUnitName.Text = txtNotes.Text = txtAddress1.Text = txtAddress2.Text = txtCity.Text = txtZipCode.Text = string.Empty;
                        Message(UMS_Resource.BUSINESS_UNIT_DETAILS_INSERT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    }
                    else if (objMastersBE.IsBUCodeExists)
                    {
                        Message(Resource.BUSINESS_UNIT_CODE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else if (string.IsNullOrEmpty(objMastersBE.BUCode))
                    {
                        Message("Can not add the Business Unit. Problem in BU Code Generation.", UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else
                        Message(UMS_Resource.BUSINESS_UNIT_DETAILS_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (hfRecognize.Value == Constants.SAVENEXT)
                {
                    hfRecognize.Value = string.Empty;
                    InsertBusinessUnitsDetails();
                }
                else
                {
                    MastersBE objMastersBE = new MastersBE();
                    objMastersBE.ActiveStatusId = Constants.Active;
                    objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objMastersBE.BU_ID = hfBU_ID.Value;
                    objMastersBE.Address1 = txtAddress1.Text;
                    objMastersBE.Address2 = txtAddress2.Text;
                    objMastersBE.City = txtCity.Text;
                    objMastersBE.ZIP = txtZipCode.Text;
                    xml = objMastersBAL.BusinessUnits(objMastersBE, Statement.Delete);
                    objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                    if (Convert.ToBoolean(objMastersBE.IsSuccess))
                    {
                        BindGrid();
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        txtAddress1.Text = txtAddress2.Text = txtBusinessUnitName.Text = txtCity.Text = txtNotes.Text = txtZipCode.Text = string.Empty;
                        ddlStateName.Items[0].Selected = true;
                        Message(UMS_Resource.BUSINESS_UNITS_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    }
                    else
                        Message(UMS_Resource.BUSINESS_UNITS_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.DeActive;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.BU_ID = hfBU_ID.Value;
                xml = objMastersBAL.BusinessUnits(objMastersBE, Statement.Delete);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (objMastersBE.IsSuccess)
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.BUSINESS_UNITS_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else if (!objMastersBE.IsSuccess && objMastersBE.Count > 0)
                    //Message(string.Format(UMS_Resource.BUSINESS_UNITS_HAVE_CUSTOMERS, objMastersBE.Count), UMS_Resource.MESSAGETYPE_ERROR);
                    Message(UMS_Resource.BUSINESS_UNITS_HAVE_CUSTOMERS, UMS_Resource.MESSAGETYPE_ERROR);
                else
                    Message(UMS_Resource.BUSINESS_UNITS_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvBusinessUnitsList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    TextBox txtGridBusinessUnitName = (TextBox)e.Row.FindControl("txtGridBusinessUnitName");
                    //TextBox txtGridBUCode = (TextBox)e.Row.FindControl("txtGridBUCode");
                    DropDownList ddlGridStateName = (DropDownList)e.Row.FindControl("ddlGridStateName");
                    //DropDownList ddlGridRegionName = (DropDownList)e.Row.FindControl("ddlGridRegionName");

                    TextBox txtGridAddress1 = (TextBox)e.Row.FindControl("txtGridAddress1");
                    TextBox txtGridAddress2 = (TextBox)e.Row.FindControl("txtGridAddress2");
                    TextBox txtGridCity = (TextBox)e.Row.FindControl("txtGridCity");
                    TextBox txtGridZip = (TextBox)e.Row.FindControl("txtGridZip");

                    //htableMaxLength = objCommonMethods.getApplicationSettings(UMS_Resource.SETTING_BU_CODE_LENGTH);
                    //txtGridBUCode.Attributes.Add("MaxLength", htableMaxLength[Constants.BusinessUnits].ToString());
                    lkbtnUpdate.Attributes.Add("onclick",
                        "return UpdateValidation('" + txtGridBusinessUnitName.ClientID + "','" +
                                                        ddlGridStateName.ClientID + "','" +
                                                        //ddlGridRegionName.ClientID + "','" +
                                                        txtGridAddress1.ClientID + "','" +
                                                        txtGridAddress2.ClientID + "','" +
                                                        txtGridCity.ClientID + "','" +
                                                        txtGridZip.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");


                    if (Convert.ToInt32(lblActiveStatusId.Text) == Constants.DeActive)
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_SERVICE_UNITS_PAGE + "?" + UMS_Resource.QSTR_BUSINESS_UNIT_CODE + "=" + objiDsignBAL.Encrypt(hfBU_ID.Value), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //protected void ddlStateName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ddlStateName.SelectedIndex > 0)
        //        {
        //            objCommonMethods.BindRegions(ddlRegion, ddlStateName.SelectedValue, true, UMS_Resource.DROPDOWN_SELECT);
        //            ddlRegion.Enabled = true;
        //        }
        //        else
        //        {
        //            ddlRegion.SelectedIndex = 0;
        //            ddlRegion.Enabled = false;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void ddlGridStateName_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DropDownList ddlGridStateName = (DropDownList)sender;
        //    GridViewRow row = (GridViewRow)ddlGridStateName.NamingContainer;
        //    if (row != null)
        //    {
        //        try
        //        {
        //            DropDownList ddlGridRegionName = (DropDownList)row.FindControl("ddlGridRegionName");
        //            if (ddlGridStateName.SelectedIndex > 0)
        //            {
        //                Label lblStateCode = (Label)row.FindControl("lblStateCode");

        //                objCommonMethods.BindRegions(ddlGridRegionName, ddlGridStateName.SelectedValue, true, UMS_Resource.DROPDOWN_SELECT);
        //                ddlGridRegionName.Enabled = true;
        //                ddlRegion.Focus();
        //            }
        //            else
        //            {
        //                ddlGridRegionName.SelectedIndex = 0;
        //                ddlGridRegionName.Enabled = false;
        //            }

        //        }
        //        catch (Exception ex)
        //        {
        //            Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //            try
        //            {
        //                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //            }
        //            catch (Exception logex)
        //            {

        //            }
        //        }
        //    }
        //}

        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }       

        public void BindGrid()
        {
            try
            {
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();
                XmlDocument resultedXml = new XmlDocument();

                objMastersBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                objMastersBE.PageSize = PageSize;
                resultedXml = objMastersBAL.GetBusinessUnits(objMastersBE, ReturnType.Multiple);
                objMastersListBE = objiDsignBAL.DeserializeFromXml<MastersListBE>(resultedXml);
                if (objMastersListBE.Items.Count > 0)
                {
                    divdownpaging.Visible = divpaging.Visible = true;
                    hfTotalRecords.Value = objMastersListBE.Items[0].TotalRecords.ToString();
                    gvBusinessUnitsList.DataSource = objMastersListBE.Items;
                    gvBusinessUnitsList.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = objMastersListBE.Items.Count.ToString();
                    divdownpaging.Visible = divpaging.Visible = false;
                    gvBusinessUnitsList.DataSource = new DataTable();
                    gvBusinessUnitsList.DataBind();
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void InsertBusinessUnitsDetails()
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.BusinessUnitName = txtBusinessUnitName.Text;
                objMastersBE.StateCode = ddlStateName.SelectedValue;
                objMastersBE.Address1 = txtAddress1.Text;
                objMastersBE.Address2 = txtAddress2.Text;
                objMastersBE.City = txtCity.Text;
                objMastersBE.ZIP = txtZipCode.Text;
                //objMastersBE.RegionId = Convert.ToInt32(ddlRegion.SelectedValue);
                objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.Notes = objiDsignBAL.ReplaceNewLines(txtNotes.Text, true);
                xml = objMastersBAL.BusinessUnits(objMastersBE, Statement.Insert);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);
                hfBU_ID.Value = objMastersBE.BU_ID;
                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    UCPaging2.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    lblMsgPopup.Text = UMS_Resource.BUSINESS_UNIT_DETAILS_INSERT_SUCCESS;
                    mpeCountrypopup.Show();
                    pop.Attributes.Add("class", "popheader popheaderlblgreen");
                    btnOk.Focus();
                }
                else if (objMastersBE.IsBUCodeExists)
                {
                    Message(Resource.BUSINESS_UNIT_CODE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (string.IsNullOrEmpty(objMastersBE.BUCode))
                {
                    Message("Can not add the Business Unit. Problem in BU Code Generation.", UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                    Message(UMS_Resource.BUSINESS_UNIT_DETAILS_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        public bool CustomerCount(string BU_ID)
        {
            XmlDocument _xml = new XmlDocument();
            _ObjCustomerDetailsBe.BusinessUnitName = BU_ID;
            _xml = _ObjCustomerDetailsBAL.CountCustomersBAL(_ObjCustomerDetailsBe, Statement.Check);
            _ObjCustomerDetailsBe = objiDsignBAL.DeserializeFromXml<CustomerDetailsBe>(_xml);
            customersCount = _ObjCustomerDetailsBe.Count;
            if (_ObjCustomerDetailsBe.Count > 0)
                return true;
            else
                return false;
        }

        private void AssignMaxLength()
        {
            try
            {
                htableMaxLength = objCommonMethods.getApplicationSettings(UMS_Resource.SETTING_BU_CODE_LENGTH);
                //txtBUCode.Attributes.Add("MaxLength", htableMaxLength[Constants.BusinessUnits].ToString());
                hfBUCodeLength.Value = htableMaxLength[Constants.BusinessUnits].ToString();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion        

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}