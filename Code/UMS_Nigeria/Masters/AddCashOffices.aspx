﻿<%@ Page Title="::Cash Office::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master" Theme="Green"
    AutoEventWireup="true" CodeBehind="AddCashOffices.aspx.cs" Inherits="UMS_Nigeria.Masters.AddCashOffices" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upAgencies" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <asp:UpdatePanel ID="upAddAgency" runat="server">
                    <ContentTemplate>
                        <div class="inner-sec">
                            <asp:Panel ID="pnlMessage" runat="server">
                                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                            </asp:Panel>
                            <div class="text_total">
                                <div class="text-heading">
                                    <asp:Literal ID="litReadMethod" runat="server" Text="<%$ Resources:Resource, CASH_OFFICES_ENTRY%>"></asp:Literal>
                                </div>
                                <div class="star_text">
                                    <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="dot-line">
                            </div>
                            <div class="inner-box">
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                                        </label>
                                        <div class="space">
                                        </div>
                                        <asp:DropDownList ID="ddlBusinessUnit" TabIndex="1" Enabled="false" AutoPostBack="true"
                                            runat="server" OnSelectedIndexChanged="ddlBusinessUnit_SelectedIndexChanged"
                                            CssClass="text-box text-select">
                                            <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                                        </asp:DropDownList>
                                        <%--<span class="text-heading_2">
                                            <asp:HyperLink ID="lkbtnAddNewBusinessUnit" NavigateUrl="~/Masters/AddBusinessUnits.aspx"
                                                runat="server" Text="<%$ Resources:Resource, ADD%>"></asp:HyperLink>
                                        </span>--%>
                                        <div class="space">
                                        </div>
                                        <span id="spanBU" class="span_color"></span>
                                    </div>
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, OFFICE_NAME%>"></asp:Literal>
                                            <span class="span_star">*</span></label>
                                        <div class="space">
                                        </div>
                                        <asp:TextBox CssClass="text-box" ID="txtOfficeName" MaxLength="299" TabIndex="4"
                                            placeholder="Enter Office Name" onblur="return TextBoxBlurValidationlbl(this,'spanOfficeName','Office Name')"
                                            runat="server"></asp:TextBox>
                                        <span id="spanOfficeName" class="span_color"></span>
                                    </div>
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource,  NOTES%>"></asp:Literal>
                                        </label>
                                        <div class="space">
                                        </div>
                                        <asp:TextBox CssClass="text-box" ID="txtDetails" runat="server" TabIndex="9" TextMode="MultiLine"
                                            placeholder="Enter Notes"></asp:TextBox>
                                    </div>
                                    <div class="box_totalThree_a">
                                        <asp:Button ID="btnSave" OnClientClick="return Validate();" TabIndex="10" OnClick="btnSave_Click"
                                            CssClass="box_s" Text="<%$ Resources:Resource, SAVE%>" runat="server" />
                                    </div>
                                </div>
                                <div class="inner-box2">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litServiceUnit" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal>
                                        </label>
                                        <div class="space">
                                        </div>
                                        <asp:DropDownList ID="ddlServiceUnit" Enabled="false" TabIndex="2" AutoPostBack="true"
                                            runat="server" OnSelectedIndexChanged="ddlServiceUnit_SelectedIndexChanged" CssClass="text-boxTwo select_box">
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                        <%--<span class="text-heading_2">
                                            <asp:HyperLink ID="lkbtnAddNewServiceUnit" NavigateUrl="~/Masters/AddServiceUnits.aspx"
                                                runat="server" Text="<%$ Resources:Resource, ADD%>"></asp:HyperLink>
                                        </span>--%>
                                        <div class="space">
                                        </div>
                                        <span id="spanSU" class="span_color"></span>
                                    </div>
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, CONTACT_NO%>"></asp:Literal>
                                            <span class="span_star">*</span></label>
                                        <div class="space">
                                        </div>
                                        <asp:TextBox CssClass="text-box" ID="txtCode1" TabIndex="5" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanContactNo','Code')"
                                            MaxLength="3" Width="11%" placeholder="Code"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtCode1"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:TextBox CssClass="text-box" ID="txtContactNo" Width="45%" TabIndex="6" runat="server"
                                            onblur="return TextBoxBlurValidationlbl(this,'spanContactNo','Contact No')" MaxLength="15"
                                            placeholder="Enter Contact No"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtContactNo"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanContactNo" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box3">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litServiceCenter" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal>
                                        </label>
                                        <div class="space">
                                        </div>
                                        <asp:DropDownList ID="ddlServiceCenter" TabIndex="3" Enabled="false" runat="server"
                                            CssClass="text-boxTwo select_box">
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                        <%--<span class="text-heading_2">
                                            <asp:HyperLink ID="lkbtnAddNewServiceCenter" NavigateUrl="~/Masters/AddServiceCenters.aspx"
                                                runat="server" Text="<%$ Resources:Resource, ADD%>"></asp:HyperLink>
                                        </span>--%>
                                        <div class="space">
                                        </div>
                                        <span id="spanSC" class="span_color"></span>
                                    </div>
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litAnotherContactNo" runat="server" Text="<%$ Resources:Resource, ANOTHER_CONTACT_NO%>"></asp:Literal>
                                        </label>
                                        <div class="space">
                                        </div>
                                        <asp:TextBox CssClass="text-box" ID="txtCode2" TabIndex="7" runat="server" MaxLength="3"
                                            Width="11%" placeholder="Code"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtCode2"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:TextBox CssClass="text-box" ID="txtAnotherContactNo" Width="45%" TabIndex="8"
                                            runat="server" MaxLength="15" placeholder="Enter Alternate Contact No"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="ftbTxtAnotherContactNo" runat="server" TargetControlID="txtAnotherContactNo"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanAnotherNo" class="span_color"></span>
                                    </div>
                                </div>
                            </div>
                            <div style="clear: both;">
                            </div>
                            <div class="grid_boxes">
                                <div class="grid_paging_top">
                                    <div class="paging_top_title" style="margin-left: 7px;">
                                        <asp:Literal ID="litCountriesList" runat="server" Text="<%$ Resources:Resource, CASH_OFFICES_LIST%>"></asp:Literal>
                                    </div>
                                    <div class="paging_top_rightTwo_content" id="divpaging" runat="server">
                                        <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div style="clear: both;">
                            </div>
                            <div class="grid_tb" id="divgrid" runat="server">
                                <asp:GridView ID="gvCashOfficesList" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="grid_tb_hd"
                                    OnRowCommand="gvCashOfficesList_RowCommand" OnRowDataBound="gvCashOfficesList_RowDataBound">
                                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                    <EmptyDataTemplate>
                                        There is no cash offices.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                            HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                                <asp:Label ID="lblCashOfficeId" Visible="false" runat="server" Text='<%#Eval("CashOfficeId")%>'></asp:Label>
                                                <asp:Label ID="lblActiveStatusId" Visible="false" runat="server" Text='<%#Eval("ActiveStatusId")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, OFFICE_NAME%>" ItemStyle-Width="10%"
                                            ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCashOffice" runat="server" Text='<%#Eval("CashOffice") %>'></asp:Label>
                                                <asp:TextBox CssClass="text-box" ID="txtGridCashOffice" runat="server" placeholder="Enter Cash Office Name"
                                                    MaxLength="299" Visible="false"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, BUSINESS_UNIT%>" ItemStyle-Width="10%"
                                            ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBUName" runat="server" Text='<%#Eval("BusinessUnitName") %>'></asp:Label>
                                                <asp:Label ID="lblBU_ID" Visible="false" runat="server" Text='<%#Eval("BU_ID") %>'></asp:Label>
                                                <asp:DropDownList ID="ddlGridBusinessUnit" runat="server" AutoPostBack="true" Visible="false"
                                                    OnSelectedIndexChanged="ddlGridBusinessUnit_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, SERVICE_UNIT_NAME%>" ItemStyle-Width="8%"
                                            ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSU_Id" Visible="false" runat="server" Text='<%#Eval("SU_ID") %>'></asp:Label>
                                                <asp:DropDownList ID="ddlGridServiceUnit" runat="server" AutoPostBack="true" Visible="false"
                                                    OnSelectedIndexChanged="ddlGridServiceUnit_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:Label ID="lblSUName" runat="server" Text='<%#Eval("ServiceUnitName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, SERVICE_CENTER_NAME%>" ItemStyle-Width="8%"
                                            ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSC_Id" Visible="false" runat="server" Text='<%#Eval("ServiceCenterId") %>'></asp:Label>
                                                <asp:DropDownList ID="ddlGridServiceCenter" runat="server" Visible="false">
                                                </asp:DropDownList>
                                                <asp:Label ID="lblSCName" runat="server" Text='<%#Eval("ServiceCenterName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, CONTACT_NO%>" ItemStyle-Width="8%"
                                            ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                            <ItemTemplate>
                                                <asp:Label ID="lblContactNo" runat="server" Text='<%#Eval("ContactNo") %>'></asp:Label>
                                                <asp:TextBox CssClass="text-box" ID="txtGridCode1" runat="server" MaxLength="3" Width="40"
                                                    placeholder="Code" Visible="false"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtGridCode1"
                                                    FilterType="Numbers" ValidChars=" ">
                                                </asp:FilteredTextBoxExtender>
                                                <asp:TextBox CssClass="text-box" ID="txtGridContactNo" MaxLength="10" placeholder="Enter Contact Number"
                                                    runat="server" Visible="false"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="ftbTxtGridContactNo1" runat="server" TargetControlID="txtGridContactNo"
                                                    FilterType="Numbers" ValidChars=" ">
                                                </asp:FilteredTextBoxExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, ANOTHER_CONTACT_NO%>" ItemStyle-Width="10%"
                                            ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                            <ItemTemplate>
                                                <asp:Label ID="lblContactNo2" runat="server" Text='<%#Eval("ContactNo2") %>'></asp:Label>
                                                <asp:TextBox CssClass="text-box" ID="txtGridCode2" Visible="false" runat="server"
                                                    MaxLength="3" Width="40" placeholder="Code"></asp:TextBox><br />
                                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtGridCode2"
                                                    FilterType="Numbers" ValidChars=" ">
                                                </asp:FilteredTextBoxExtender>
                                                <asp:TextBox CssClass="text-box" ID="txtGridContactNo2" placeholder="Enter Alternate Contact No"
                                                    runat="server" Visible="false" MaxLength="10"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="ftbTxtGridContactNo2" runat="server" TargetControlID="txtGridContactNo2"
                                                    FilterType="Numbers" ValidChars=" ">
                                                </asp:FilteredTextBoxExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, DETAILS%>" ItemStyle-Width="10%"
                                            ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDetails" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                                <asp:TextBox CssClass="text-box" ID="txtGridDetails" placeholder="Enter Notes" runat="server"
                                                    Visible="false" TextMode="MultiLine"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="10%"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lkbtnEdit" runat="server" ToolTip="Edit" Text="Edit" CommandName="EditOffice">
                                        <img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                                                <asp:LinkButton ID="lkbtnUpdate" Visible="false" runat="server" ToolTip="Update"
                                                    CommandName="UpdateOffice"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                                <asp:LinkButton ID="lkbtnCancel" Visible="false" runat="server" ToolTip="Cancel"
                                                    CommandName="CancelOffice"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                                <asp:LinkButton ID="lkbtnActive" Visible="false" runat="server" ToolTip="Activate Cash Office"
                                                    CommandName="ActiveOffice"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                                <asp:LinkButton ID="lkbtnDeActive" runat="server" ToolTip="DeActivate Cash Office"
                                                    CommandName="DeActiveOffice"><img src="../images/Deactivate.gif" alt="DeActive" /></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <%--<div class="page_na">
                                    <uc2:UCPagingV2 ID="UCPagingV2" runat="server" />
                                </div>--%>
                                <asp:HiddenField ID="hfPageNo" runat="server" />
                                <asp:HiddenField ID="hfTotalRecords" runat="server" />
                                <asp:HiddenField ID="hfLastPage" runat="server" />
                                <asp:HiddenField ID="hfPageSize" runat="server" />
                                <asp:HiddenField ID="hfCashOfficeId" runat="server" />
                            </div>
                            <div class="grid_boxes">
                                <div id="divdownpaging" runat="server">
                                    <div class="grid_paging_bottom">
                                        <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="hidepopup">
                <%-- Active Btn popup Start--%>
                <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                    TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="PanelActivate" runat="server" DefaultButton="btnActiveOk" CssClass="modalPopup"
                    Style="display: none; border-color: #639B00;">
                    <div class="popheader" style="background-color: #639B00;">
                        <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                    </div>
                    <br />
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btnActiveOk" runat="server" OnClick="btnActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                            <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- Active  Btn popup Closed--%>
                <%-- DeActive  Btn popup Start--%>
                <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                    TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnDeActivate" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="PanelDeActivate" runat="server" DefaultButton="btnDeActiveOk" CssClass="modalPopup"
                    Style="display: none; border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnDeActiveOk" runat="server" OnClick="btnDeActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                            <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- DeActive  popup Closed--%>
                <%-- Grid Alert popup Start--%>
                <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                    TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                </asp:ModalPopupExtender>
                <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                    <div class="popheader popheaderlblred">
                        <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- Grid Alert popup Closed--%>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/PageValidations/AddCashOffices.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
