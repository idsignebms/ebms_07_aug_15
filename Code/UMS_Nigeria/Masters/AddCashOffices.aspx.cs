﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddCashOffices
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 05-Apr-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;

namespace UMS_Nigeria.Masters
{
    public partial class AddCashOffices : System.Web.UI.Page
    {
        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        string Key = UMS_Resource.ERROR_CASH_OFFICES;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
        }
        public string BusinessUnit
        {
            get { return string.IsNullOrEmpty(ddlBusinessUnit.SelectedValue) ? string.Empty : ddlBusinessUnit.SelectedValue; }
            set { ddlBusinessUnit.SelectedValue = value.ToString(); }
        }
        public string ServiceUnit
        {
            get { return string.IsNullOrEmpty(ddlServiceUnit.SelectedValue) ? string.Empty : ddlServiceUnit.SelectedValue; }
            set { ddlServiceUnit.SelectedValue = value.ToString(); }
        }
        public string ServiceCenter
        {
            get { return string.IsNullOrEmpty(ddlServiceCenter.SelectedValue) ? string.Empty : ddlServiceCenter.SelectedValue; }
            set { ddlServiceCenter.SelectedValue = value.ToString(); }
        }
        private string OfficeName
        {
            get { return txtOfficeName.Text.Trim(); }
            set { txtOfficeName.Text = value; }
        }
        private string Details
        {
            get { return txtDetails.Text.Trim(); }
            set { txtDetails.Text = value; }
        }
        private string ContactNo
        {
            get { return txtContactNo.Text.Trim(); }
            set { txtContactNo.Text = value; }
        }
        private string AnotherContactNo
        {
            get { return txtAnotherContactNo.Text.Trim(); }
            set { txtAnotherContactNo.Text = value; }
        }
        private string ContactNoCode1
        {
            get { return txtCode1.Text.Trim(); }
            set { txtCode1.Text = value; }
        }
        private string ContactNoCode2
        {
            get { return txtCode2.Text.Trim(); }
            set { txtCode2.Text = value; }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = objCommonMethods.GetPagePath(this.Request.Url);
                //if (objCommonMethods.IsAccess(path))
                //{

                //PageAccessPermissions();
                BindDropDownsList();
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                //}
                //else
                //{
                //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                //}
            }

        }

        //private void PageAccessPermissions()
        //{
        //    try
        //    {
        //        if (objCommonMethods.IsAccess(UMS_Resource.ADD_BUSINESS_UNITS_PAGE)) { lkbtnAddNewBusinessUnit.Visible = true; } else { lkbtnAddNewBusinessUnit.Visible = false; }
        //        if (objCommonMethods.IsAccess(UMS_Resource.ADD_SERVICE_UNITS_PAGE)) { lkbtnAddNewServiceUnit.Visible = true; } else { lkbtnAddNewServiceUnit.Visible = false; }
        //        if (objCommonMethods.IsAccess(UMS_Resource.ADD_SERVICE_CENTER_PAGE)) { lkbtnAddNewServiceCenter.Visible = true; } else { lkbtnAddNewServiceCenter.Visible = false; }
        //        //if (objCommonMethods.IsAccess(UMS_Resource.ADD_STATES_PAGE)) { lkbtnAddNewState.Visible = true; } else { lkbtnAddNewState.Visible = false; }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        protected void lkbtnAddNewServiceUnit_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_SERVICE_UNITS_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnAddNewBusinessUnit_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_BUSINESS_UNITS_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnAddNewServiceCenter_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_SERVICE_CENTER_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = InsertCashOffice();

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Reset", "ClearControls();", true);
                    txtCode1.Text = txtCode2.Text = txtAnotherContactNo.Text = txtContactNo.Text = txtDetails.Text = txtOfficeName.Text = string.Empty;
                    Message(UMS_Resource.CASH_OFFICE_INSERT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else if (objMastersBE.IsCashOfficeExists)
                {
                    Message(UMS_Resource.CASH_OFFICE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                    Message(UMS_Resource.CASH_OFFICE_INSERT_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnAddNewState_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_STATES_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ddlState.SelectedIndex > 0)
        //        {
        //            //objCommonMethods.BindDistricts(ddlDistrict, ddlState.SelectedValue, true);
        //            objCommonMethods.BindBusinessUnits(ddlBusinessUnit, ddlState.SelectedValue, true);
        //            ddlBusinessUnit.Enabled = true;
        //            ddlServiceUnit.Enabled = ddlServiceCenter.Enabled = false;
        //            ddlServiceUnit.SelectedIndex = ddlServiceCenter.SelectedIndex = 0;
        //        }
        //        else
        //        {
        //            //objCommonMethods.BindDistricts(ddlDistrict, string.Empty, true);
        //            //objCommonMethods.BindBusinessUnits(ddlBusinessUnit, string.Empty, true);
        //            ddlBusinessUnit.SelectedIndex = ddlServiceUnit.SelectedIndex = ddlServiceCenter.SelectedIndex = 0;
        //            ddlBusinessUnit.Enabled = ddlServiceUnit.Enabled = ddlServiceCenter.Enabled = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void ddlGridState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                DropDownList ddlGridState = (DropDownList)sender;
                //DropDownList ddlGridDistrict = (DropDownList)ddlGridState.NamingContainer.FindControl("ddlGridDistrict");
                DropDownList ddlGridBusinessUnit = (DropDownList)ddlGridState.NamingContainer.FindControl("ddlGridBusinessUnit");

                if (ddlGridState.SelectedIndex > 0)
                {
                    //objCommonMethods.BindDistricts(ddlGridDistrict, ddlGridState.SelectedValue, true);
                    objCommonMethods.BindBusinessUnits(ddlGridBusinessUnit, ddlGridState.SelectedValue, true);
                    ddlGridBusinessUnit.Enabled = true;
                }
                else
                {
                    objCommonMethods.BindBusinessUnits(ddlGridBusinessUnit, string.Empty, true);
                    ddlGridBusinessUnit.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlBusinessUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlBusinessUnit.SelectedIndex > 0)
                {
                    objCommonMethods.BindServiceUnits(ddlServiceUnit, BusinessUnit, true);
                    ddlServiceUnit.Enabled = true;
                    ddlServiceCenter.SelectedIndex = ddlServiceUnit.SelectedIndex = 0;
                    ddlServiceCenter.Enabled = false;
                }
                else
                {
                    ddlServiceUnit.SelectedIndex = ddlServiceCenter.SelectedIndex = 0;
                    ddlServiceCenter.Enabled = ddlServiceUnit.Enabled = false;
                }
                ddlServiceUnit.Focus();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlServiceUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlServiceUnit.SelectedIndex > 0)
                {
                    objCommonMethods.BindServiceCenters(ddlServiceCenter, ServiceUnit, true);
                    ddlServiceCenter.Enabled = true;
                }
                else
                {
                    ddlServiceCenter.SelectedIndex = 0;
                    ddlServiceCenter.Enabled = false;
                }
                ddlServiceCenter.Focus();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlGridBusinessUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlGridBusinessUnit = (DropDownList)sender;
                DropDownList ddlGridServiceUnit = (DropDownList)ddlGridBusinessUnit.NamingContainer.FindControl("ddlGridServiceUnit");
                DropDownList ddlGridServiceCenter = (DropDownList)ddlGridBusinessUnit.NamingContainer.FindControl("ddlGridServiceCenter");
                if (ddlGridBusinessUnit.SelectedIndex > 0)
                {
                    objCommonMethods.BindServiceUnits(ddlGridServiceUnit, ddlGridBusinessUnit.SelectedValue, true);
                    ddlGridServiceUnit.Enabled = true;
                    ddlGridServiceCenter.SelectedIndex = ddlGridServiceUnit.SelectedIndex = 0;
                    ddlGridServiceCenter.Enabled = false;
                }
                else
                {
                    ddlGridServiceUnit.SelectedIndex = ddlGridServiceCenter.SelectedIndex = 0;
                    ddlGridServiceCenter.Enabled = ddlGridServiceUnit.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlGridServiceUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlGridServiceUnit = (DropDownList)sender;
                DropDownList ddlGridServiceCenter = (DropDownList)ddlGridServiceUnit.NamingContainer.FindControl("ddlGridServiceCenter");
                if (ddlGridServiceUnit.SelectedIndex > 0)
                {
                    objCommonMethods.BindServiceCenters(ddlGridServiceCenter, ddlGridServiceUnit.SelectedValue, true);
                    ddlGridServiceCenter.Enabled = true;
                }
                else
                {
                    ddlGridServiceCenter.SelectedIndex = 0;
                    ddlGridServiceCenter.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvCashOfficesList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridCashOffice = (TextBox)row.FindControl("txtGridCashOffice");
                DropDownList ddlGridBusinessUnit = (DropDownList)row.FindControl("ddlGridBusinessUnit");
                DropDownList ddlGridServiceUnit = (DropDownList)row.FindControl("ddlGridServiceUnit");
                DropDownList ddlGridServiceCenter = (DropDownList)row.FindControl("ddlGridServiceCenter");
                TextBox txtGridContactNo = (TextBox)row.FindControl("txtGridContactNo");
                TextBox txtGridContactNo2 = (TextBox)row.FindControl("txtGridContactNo2");
                TextBox txtGridDetails = (TextBox)row.FindControl("txtGridDetails");
                TextBox txtGridCode1 = (TextBox)row.FindControl("txtGridCode1");
                TextBox txtGridCode2 = (TextBox)row.FindControl("txtGridCode2");


                Label lblCashOfficeId = (Label)row.FindControl("lblCashOfficeId");
                Label lblCashOffice = (Label)row.FindControl("lblCashOffice");
                Label lblBUName = (Label)row.FindControl("lblBUName");
                Label lblBU_ID = (Label)row.FindControl("lblBU_ID");
                Label lblSU_Id = (Label)row.FindControl("lblSU_Id");
                Label lblSUName = (Label)row.FindControl("lblSUName");
                Label lblSC_Id = (Label)row.FindControl("lblSC_Id");
                Label lblSCName = (Label)row.FindControl("lblSCName");
                Label lblContactNo = (Label)row.FindControl("lblContactNo");
                Label lblContactNo2 = (Label)row.FindControl("lblContactNo2");
                Label lblDetails = (Label)row.FindControl("lblDetails");

                switch (e.CommandName.ToUpper())
                {
                    case "EDITOFFICE":
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;
                        lblCashOffice.Visible = lblBUName.Visible = lblSUName.Visible = lblSCName.Visible = lblContactNo.Visible = lblContactNo2.Visible = lblDetails.Visible = false;
                        txtGridCode1.Visible = txtGridCode2.Visible = txtGridCashOffice.Visible = ddlGridBusinessUnit.Visible = ddlGridServiceUnit.Visible = ddlGridServiceCenter.Visible = txtGridContactNo.Visible = txtGridContactNo2.Visible = txtGridDetails.Visible = true;

                        txtGridCashOffice.Text = lblCashOffice.Text;

                        if (lblContactNo.Text.Length > 0 && lblContactNo.Text != "--")
                        {
                            string[] contactNo = lblContactNo.Text.Split('-');
                            if (contactNo.Length < 2)
                            {
                                txtGridCode1.Text = lblContactNo.Text.Substring(0, 3);
                                txtGridContactNo.Text = lblContactNo.Text.Remove(0, 3);
                            }
                            else
                            {
                                txtGridCode1.Text = contactNo[0].ToString();
                                txtGridContactNo.Text = contactNo[1].ToString();
                            }
                        }
                        else
                            txtGridCode1.Text = txtGridContactNo.Text = string.Empty;

                        if (lblContactNo2.Text.Length > 0 && lblContactNo2.Text != "--")
                        {
                            string[] contactNo = lblContactNo2.Text.Split('-');
                            if (contactNo.Length < 2)
                            {
                                txtGridCode2.Text = lblContactNo2.Text.Substring(0, 3);
                                txtGridContactNo2.Text = lblContactNo2.Text.Remove(0, 3);
                            }
                            else
                            {
                                txtGridCode2.Text = contactNo[0].ToString();
                                txtGridContactNo2.Text = contactNo[1].ToString();
                            }
                        }
                        else
                            txtGridCode2.Text = txtGridContactNo2.Text = string.Empty;

                        txtGridDetails.Text = objiDsignBAL.ReplaceNewLines(lblDetails.Text, false);
                        objCommonMethods.BindBusinessUnits(ddlGridBusinessUnit, string.Empty, true);
                        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        {
                            ddlGridBusinessUnit.SelectedIndex = ddlGridBusinessUnit.Items.IndexOf(ddlGridBusinessUnit.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                            ddlGridBusinessUnit.Enabled = false;
                        }
                        else
                            ddlGridBusinessUnit.SelectedIndex = ddlGridBusinessUnit.Items.IndexOf(ddlGridBusinessUnit.Items.FindByValue(lblBU_ID.Text));
                        objCommonMethods.BindServiceUnits(ddlGridServiceUnit, ddlGridBusinessUnit.SelectedValue, true);
                        ddlGridServiceUnit.SelectedIndex = ddlGridServiceUnit.Items.IndexOf(ddlGridServiceUnit.Items.FindByValue(lblSU_Id.Text));
                        objCommonMethods.BindServiceCenters(ddlGridServiceCenter, ddlGridServiceUnit.SelectedValue, true);
                        ddlGridServiceCenter.SelectedIndex = ddlGridServiceCenter.Items.IndexOf(ddlGridServiceCenter.Items.FindByValue(lblSC_Id.Text));
                        break;
                    case "CANCELOFFICE":
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                        lblCashOffice.Visible = lblBUName.Visible = lblSUName.Visible = lblSCName.Visible = lblContactNo.Visible = lblContactNo2.Visible = lblDetails.Visible = true;
                        txtGridCode1.Visible = txtGridCode2.Visible = txtGridCashOffice.Visible = ddlGridBusinessUnit.Visible = ddlGridServiceUnit.Visible = ddlGridServiceCenter.Visible = txtGridContactNo.Visible = txtGridContactNo2.Visible = txtGridDetails.Visible = false;
                        break;
                    case "UPDATEOFFICE":
                        //objMastersBE.StateCode = ddlGridState.SelectedValue;
                        //objMastersBE.DistrictCode = ddlGridDistrict.SelectedValue;
                        objMastersBE.BU_ID = ddlGridBusinessUnit.SelectedValue;
                        objMastersBE.SU_ID = ddlGridServiceUnit.SelectedValue;
                        objMastersBE.ServiceCenterId = ddlGridServiceCenter.SelectedValue;
                        objMastersBE.CashOfficeId = Convert.ToInt32(lblCashOfficeId.Text);
                        objMastersBE.CashOffice = txtGridCashOffice.Text;
                        objMastersBE.ContactNo = txtGridCode1.Text + "-" + txtGridContactNo.Text;
                        objMastersBE.ContactNo2 = txtGridCode2.Text + "-" + txtGridContactNo2.Text;
                        objMastersBE.Details = objiDsignBAL.ReplaceNewLines(txtGridDetails.Text, true);
                        objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.CashOffices(objMastersBE, Statement.Update));

                        if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        {
                            BindGrid();
                            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            Message(UMS_Resource.CASH_OFFICE_UPDATED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        else if (objMastersBE.IsCashOfficeExists)
                        {
                            Message(UMS_Resource.CASH_OFFICE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else
                        {
                            Message(UMS_Resource.CASH_OFFICE_UPDATE_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                        }

                        break;
                    case "ACTIVEOFFICE":
                        mpeActivate.Show();
                        hfCashOfficeId.Value = lblCashOfficeId.Text;
                        lblActiveMsg.Text = UMS_Resource.ACTIVE_OFFICE_POPUP_TEXT;
                        btnActiveOk.Focus();
                        //objMastersBE.ActiveStatusId = Constants.Active;
                        //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objMastersBE.CashOfficeId =Convert.ToInt32(lblCashOfficeId.Text);
                        //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.CashOffices(objMastersBE, Statement.Delete));

                        //if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        //{
                        //    BindCashOffices();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message(UMS_Resource.CASH_OFFICE_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                        //}
                        //else
                        //    Message(UMS_Resource.CASH_OFFICE_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "DEACTIVEOFFICE":
                        mpeDeActive.Show();
                        hfCashOfficeId.Value = lblCashOfficeId.Text;
                        lblDeActiveMsg.Text = UMS_Resource.DEACTIVE_OFFICE_POPUP_TEXT;
                        btnDeActiveOk.Focus();
                        //objMastersBE.ActiveStatusId = Constants.DeActive;
                        //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objMastersBE.CashOfficeId =Convert.ToInt32(lblCashOfficeId.Text);
                        //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.CashOffices(objMastersBE, Statement.Delete));

                        //if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        //{
                        //    BindCashOffices();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message(UMS_Resource.CASH_OFFICE_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                        //}
                        //else
                        //    Message(UMS_Resource.CASH_OFFICE_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.Active;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.CashOfficeId = Convert.ToInt32(hfCashOfficeId.Value);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.CashOffices(objMastersBE, Statement.Delete));

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.CASH_OFFICE_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else
                    Message(UMS_Resource.CASH_OFFICE_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.DeActive;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.CashOfficeId = Convert.ToInt32(hfCashOfficeId.Value);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.CashOffices(objMastersBE, Statement.Delete));

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.CASH_OFFICE_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else
                    Message(UMS_Resource.CASH_OFFICE_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvCashOfficesList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    TextBox txtGridCashOffice = (TextBox)e.Row.FindControl("txtGridCashOffice");
                    TextBox txtGridContactNo = (TextBox)e.Row.FindControl("txtGridContactNo");
                    TextBox txtGridContactNo2 = (TextBox)e.Row.FindControl("txtGridContactNo2");
                    TextBox txtGridCode1 = (TextBox)e.Row.FindControl("txtGridCode1");
                    TextBox txtGridCode2 = (TextBox)e.Row.FindControl("txtGridCode2");
                    DropDownList ddlGridBusinessUnit = (DropDownList)e.Row.FindControl("ddlGridBusinessUnit");
                    DropDownList ddlGridServiceUnit = (DropDownList)e.Row.FindControl("ddlGridServiceUnit");
                    DropDownList ddlGridServiceCenter = (DropDownList)e.Row.FindControl("ddlGridServiceCenter");

                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridCashOffice.ClientID + "','"
                                                                                      + txtGridContactNo2.ClientID + "','"
                                                                                      + ddlGridBusinessUnit.ClientID + "','"
                                                                                      + ddlGridServiceUnit.ClientID + "','"
                                                                                      + ddlGridServiceCenter.ClientID + "','"
                                                                                      + txtGridContactNo.ClientID + "','"
                                                                                      + txtGridCode1.ClientID + "','"
                                                                                      + txtGridCode2.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");


                    if (Convert.ToInt32(lblActiveStatusId.Text) == Constants.DeActive)
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods
        public void BindGrid()
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                objMastersBE.PageSize = PageSize;
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    objMastersBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    objMastersBE.BU_ID = string.Empty;
                //MastersListBE objMastersListBE = objiDsignBAL.DeserializeFromXml<MastersListBE>(objMastersBAL.CashOffices(objMastersBE, ReturnType.Get));
                DataSet ds = objMastersBAL.GetCashOfficesList(objMastersBE);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvCashOfficesList.DataSource = ds.Tables[0];
                    gvCashOfficesList.DataBind();
                    hfTotalRecords.Value = ds.Tables[0].Rows[0]["TotalRecords"].ToString();
                    UCPaging2.Visible = UCPaging1.Visible = true;
                    divdownpaging.Visible = divpaging.Visible = true;
                    //CreatePagingDT(Convert.ToInt32(ds.Tables[0].Rows[0]["TotalRecords"]));
                }
                else
                {
                    gvCashOfficesList.DataSource = new DataTable();
                    gvCashOfficesList.DataBind();
                    hfTotalRecords.Value = Constants.Zero.ToString();
                    UCPaging2.Visible = UCPaging1.Visible = false;
                    divdownpaging.Visible = divpaging.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private MastersBE InsertCashOffice()
        {
            MastersBE objMastersBE = new MastersBE();
            try
            {
                //objMastersBE.StateCode = State;
                //objMastersBE.DistrictCode = District;
                objMastersBE.BU_ID = BusinessUnit;
                objMastersBE.SU_ID = ServiceUnit;
                objMastersBE.ServiceCenterId = ServiceCenter;
                objMastersBE.CashOffice = OfficeName;
                objMastersBE.ContactNo = ContactNoCode1 + "-" + ContactNo;
                objMastersBE.ContactNo2 = ContactNoCode2 + "-" + AnotherContactNo;
                objMastersBE.Details = objiDsignBAL.ReplaceNewLines(Details, true);
                objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.CashOffices(objMastersBE, Statement.Insert));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            return objMastersBE;
        }

        private void BindDropDownsList()
        {
            try
            {
                //objCommonMethods.BindStatesByBusinessUnits(ddlState, Session[UMS_Resource.SESSION_LOGINID].ToString(), string.Empty, true);
                objCommonMethods.BindUserBusinessUnits(ddlBusinessUnit, string.Empty, false, UMS_Resource.DROPDOWN_SELECT);
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                {
                    ddlBusinessUnit.SelectedIndex = ddlBusinessUnit.Items.IndexOf(ddlBusinessUnit.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                    ddlBusinessUnit.Enabled = false;
                    ddlBusinessUnit_SelectedIndexChanged(ddlBusinessUnit, new EventArgs());
                }
                else
                {
                    ddlBusinessUnit.Enabled = true;
                    ddlBusinessUnit_SelectedIndexChanged(ddlBusinessUnit, new EventArgs());
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}