﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using Resources;
using iDSignHelper;
using System.Threading;
using System.Globalization;
using System.Xml;
using System.Configuration;
using System.IO;
using System.Data;

namespace UMS_Nigeria.Masters
{
    public partial class AddCashier : System.Web.UI.Page
    {
        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
        }
        #endregion

        #region Members
        CashierBAL _objCashierBAL = new CashierBAL();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        CommonDAL _ObjCommonDAL = new CommonDAL();
        XmlDocument xml = new XmlDocument();
        public int PageNum;
        string Key = "AddCashier";

        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE, false);

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                InsertCashiers();

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = _ObjCommonMethods.GetPagePath(this.Request.Url);
                //if (_ObjCommonMethods.IsAccess(path))
                //{
                BindDropDowns();
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
                //}
                //else
                //{
                //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                //}
            }
        }

        private void BindDropDowns()
        {
            try
            {
                string BU_ID = Convert.ToString(Session[UMS_Resource.SESSION_USER_BUID]);
                _ObjCommonMethods.BindOfficeByBU_ID(ddlCashOffice, BU_ID, true, false, UMS_Resource.DROPDOWN_SELECT);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods
        protected void InsertCashiers()
        {
            try
            {
                CashierBE _objCashierBE = new CashierBE();
                _objCashierBE.FirstName = txtFName.Text;
                _objCashierBE.MiddleName = txtMName.Text;
                _objCashierBE.LastName = txtLName.Text;
                _objCashierBE.ContactNo1 = txtCode1.Text + "-" + txtContactNo.Text;
                if (txtAnotherContactNo.Text != string.Empty)
                    _objCashierBE.ContactNo2 = txtCode2.Text + "-" + txtAnotherContactNo.Text;
                _objCashierBE.EmailId = txtEmail.Text;
                _objCashierBE.Address1 = txtAddress1.Text;
                _objCashierBE.Address2 = txtAddress2.Text;
                _objCashierBE.City = txtCity.Text;
                _objCashierBE.ZipCode = txtZip.Text;
                _objCashierBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objCashierBE.CashOfficeId =Convert.ToInt32(ddlCashOffice.SelectedValue);
                string PhotoFileName = string.Empty;
                //if (fupPhoto.FileContent.Length > 0)
                //{
                //    PhotoFileName = System.IO.Path.GetFileName(fupPhoto.FileName).ToString();//Assinging FileName
                //    string PhotoExtension = Path.GetExtension(PhotoFileName);//Storing Extension of file into variable Extension
                //    TimeZoneInfo PhotoIND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                //    DateTime PhotocurrentTime = DateTime.Now;
                //    DateTime Photoourtime = TimeZoneInfo.ConvertTime(PhotocurrentTime, PhotoIND_ZONE);
                //    string PhotoCurrentDate = Photoourtime.ToString("dd-MM-yyyy_hh-mm-ss");
                //    string PhotoModifiedFileName = Path.GetFileNameWithoutExtension(PhotoFileName) + "_" + PhotoCurrentDate + PhotoExtension;//Setting File Name to store it in database
                //    fupPhoto.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["UserPhotos"].ToString()) + PhotoModifiedFileName);//Saving File in to the server.
                //    _objMarketersBe.Photo = PhotoModifiedFileName;
                //}

                XmlDocument xml = new XmlDocument();
                xml = _objCashierBAL.InsertCashier(_objCashierBE, Statement.Insert);

                _objCashierBE = _ObjiDsignBAL.DeserializeFromXml<CashierBE>(xml);
                if (_objCashierBE.IsSuccess)
                {
                    txtCode1.Text = txtCode2.Text = txtFName.Text = txtMName.Text = txtLName.Text = txtContactNo.Text = txtAnotherContactNo.Text = txtEmail.Text = txtAddress1.Text = txtAddress2.Text = txtCity.Text = txtZip.Text = string.Empty;
                    BindGrid();
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(Resource.CASHIER_INSERT, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else if (_objCashierBE.IsEmailIdExists)
                {
                    Message(Resource.EMAIL_ID_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                {
                    Message(Resource.CASHIER_FAILD, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        public void BindGrid()
        {
            try
            {
                CashierBE _objCashierBE = new CashierBE();
                CashierListBE _objCashierListBE = new CashierListBE();

                XmlDocument resultedXml = new XmlDocument();

                _objCashierBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                _objCashierBE.PageSize = PageSize;
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    _objCashierBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    _objCashierBE.BU_ID = string.Empty;
                resultedXml = _objCashierBAL.GetCashier(_objCashierBE, ReturnType.List);
                _objCashierListBE = _ObjiDsignBAL.DeserializeFromXml<CashierListBE>(resultedXml);

                if (_objCashierListBE.Items.Count > 0)
                {
                    divdownpaging.Visible = divpaging.Visible = true;
                    UCPaging1.Visible = UCPaging2.Visible = true;
                    hfTotalRecords.Value = _objCashierListBE.Items[0].TotalRecords.ToString();
                    gvCashierList.DataSource = _objCashierListBE.Items;
                    gvCashierList.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = _objCashierListBE.Items.Count.ToString();
                    divdownpaging.Visible = divpaging.Visible = false;
                    UCPaging1.Visible = UCPaging2.Visible = false;
                    gvCashierList.DataSource = new DataTable(); ;
                    gvCashierList.DataBind();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        protected void gvCashierList_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                CashierListBE _ObjCashierListBE = new CashierListBE();
                CashierBE _ObjCashierBE = new CashierBE();
                DataSet ds = new DataSet();


                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridFirstName = (TextBox)row.FindControl("txtGridFirstName");
                TextBox txtGridMiddleName = (TextBox)row.FindControl("txtGridMiddleName");
                TextBox txtGridLastName = (TextBox)row.FindControl("txtGridLastName");
                TextBox txtGridEmailId = (TextBox)row.FindControl("txtGridEmailId");
                TextBox txtGridContactNo1 = (TextBox)row.FindControl("txtGridContactNo1");
                TextBox txtGridContactNo2 = (TextBox)row.FindControl("txtGridContactNo2");
                TextBox txtGridCode1 = (TextBox)row.FindControl("txtGridCode1");
                TextBox txtGridCode2 = (TextBox)row.FindControl("txtGridCode2");
                TextBox txtGridAddress1 = (TextBox)row.FindControl("txtGridAddress1");
                TextBox txtGridAddress2 = (TextBox)row.FindControl("txtGridAddress2");
                TextBox txtGridCity = (TextBox)row.FindControl("txtGridCity");
                TextBox txtGridZip = (TextBox)row.FindControl("txtGridZip");
                TextBox txtGridNotes = (TextBox)row.FindControl("txtGridNotes");
                DropDownList ddlGridCashOffice = (DropDownList)row.FindControl("ddlGridCashOffice");

                Label lblCashOfficeId = (Label)row.FindControl("lblCashOfficeId");
                Label lblGridCashOffice = (Label)row.FindControl("lblGridCashOffice");
                Label lblCashierId = (Label)row.FindControl("lblCashierId");
                Label lblFirstName = (Label)row.FindControl("lblFirstName");
                Label lblMiddleName = (Label)row.FindControl("lblMiddleName");
                Label lblLastName = (Label)row.FindControl("lblLastName");
                Label lblEmailId = (Label)row.FindControl("lblEmailId");
                Label lblContactNo1 = (Label)row.FindControl("lblContactNo1");
                Label lblContactNo2 = (Label)row.FindControl("lblContactNo2");
                Label lblAddress1 = (Label)row.FindControl("lblAddress1");
                Label lblAddress2 = (Label)row.FindControl("lblAddress2");
                Label lblCity = (Label)row.FindControl("lblCity");
                Label lblZip = (Label)row.FindControl("lblZip");
                Label lblGridNotes= (Label)row.FindControl("lblGridNotes");

                //Label lblGridPhoto = (Label)row.FindControl("lblGridPhoto");
                //Image imgCashier = (Image)row.FindControl("imgCashier");
                //FileUpload fupGridPhoto = (FileUpload)row.FindControl("fupGridPhoto");


                //ScriptManager.GetCurrent(this).RegisterPostBackControl(fupGridPhoto);


                hfCashierId.Value = lblCashierId.Text;

                hfRecognize.Value = string.Empty;
                switch (e.CommandName.ToUpper())
                {
                    case "EDITCASHIER":
                        lblFirstName.Visible = lblLastName.Visible = lblMiddleName.Visible = lblEmailId.Visible =
                            lblAddress1.Visible = lblAddress2.Visible = lblCity.Visible = lblZip.Visible =
                            lblContactNo1.Visible = lblContactNo2.Visible = lblGridNotes.Visible = lblGridCashOffice.Visible = false;

                        txtGridFirstName.Visible = txtGridMiddleName.Visible = txtGridLastName.Visible = txtGridEmailId.Visible =
                            txtGridAddress1.Visible = txtGridAddress2.Visible = txtGridCity.Visible = txtGridZip.Visible =
                            txtGridCode1.Visible = txtGridCode2.Visible = txtGridContactNo1.Visible = txtGridContactNo2.Visible = txtGridNotes.Visible = ddlGridCashOffice.Visible = true;

                        txtGridFirstName.Text = lblFirstName.Text;
                        txtGridMiddleName.Text = lblMiddleName.Text;
                        txtGridLastName.Text = lblLastName.Text;
                        txtGridEmailId.Text = lblEmailId.Text;

                        if (lblContactNo1.Text.Length > 0 && lblContactNo1.Text != "--")
                        {
                            string[] contactNo = lblContactNo1.Text.Split('-');
                            if (contactNo.Length < 2)
                            {
                                txtGridCode1.Text = lblContactNo1.Text.Substring(0, 3);
                                txtGridContactNo1.Text = lblContactNo1.Text.Remove(0, 3);
                            }
                            else
                            {
                                txtGridCode1.Text = contactNo[0].ToString();
                                txtGridContactNo1.Text = contactNo[1].ToString();
                            }
                        }
                        else
                            txtGridCode1.Text = txtGridContactNo1.Text = string.Empty;

                        if (lblContactNo2.Text.Length > 0 && lblContactNo2.Text != "--")
                        {
                            string[] contactNo = lblContactNo2.Text.Split('-');
                            if (contactNo.Length < 2)
                            {
                                txtGridCode2.Text = lblContactNo2.Text.Substring(0, 3);
                                txtGridContactNo2.Text = lblContactNo2.Text.Remove(0, 3);
                            }
                            else
                            {
                                txtGridCode2.Text = contactNo[0].ToString();
                                txtGridContactNo2.Text = contactNo[1].ToString();
                            }
                        }
                        else
                            txtGridCode2.Text = txtGridContactNo2.Text = string.Empty;

                        txtGridAddress1.Text = lblAddress1.Text;
                        txtGridAddress2.Text = lblAddress2.Text;
                        txtGridCity.Text = lblCity.Text;
                        txtGridZip.Text = lblZip.Text;
                        txtGridNotes.Text = lblGridNotes.Text;

                        //_ObjCommonMethods.BindOffice(ddlGridCashOffice, true);
                        string BU_ID = Convert.ToString(Session[UMS_Resource.SESSION_USER_BUID]);
                        _ObjCommonMethods.BindOfficeByBU_ID(ddlGridCashOffice, BU_ID, true, false, UMS_Resource.DROPDOWN_SELECT);

                        ddlGridCashOffice.SelectedIndex = ddlGridCashOffice.Items.IndexOf(ddlGridCashOffice.Items.FindByValue(lblCashOfficeId.Text));


                        row.FindControl("lkbtnCashierCancel").Visible = row.FindControl("lkbtnCashierUpdate").Visible = true;
                        row.FindControl("lkbtnDelete").Visible = row.FindControl("lkbtnCashierEdit").Visible = false;
                        break;
                    case "CANCELCASHIER":
                        row.FindControl("lkbtnDelete").Visible = row.FindControl("lkbtnCashierEdit").Visible = true;
                        row.FindControl("lkbtnCashierCancel").Visible = row.FindControl("lkbtnCashierUpdate").Visible = false;
                        
                        lblFirstName.Visible = lblLastName.Visible = lblMiddleName.Visible = lblEmailId.Visible =
                            lblAddress1.Visible = lblAddress2.Visible = lblCity.Visible = lblZip.Visible =
                            lblContactNo1.Visible = lblContactNo2.Visible = lblGridNotes.Visible = lblGridCashOffice.Visible = true;

                        txtGridFirstName.Visible = txtGridMiddleName.Visible = txtGridLastName.Visible = txtGridEmailId.Visible =
                            txtGridAddress1.Visible = txtGridAddress2.Visible = txtGridCity.Visible = txtGridZip.Visible =
                            txtGridCode1.Visible = txtGridCode2.Visible = txtGridContactNo1.Visible = txtGridContactNo2.Visible = txtGridNotes.Visible = ddlGridCashOffice.Visible = false;
                        break;
                    case "UPDATECASHIER":
                        //xml = _ObjUMS_Service.UpdateStates(txtGridStateName.Text, lblStateCode.Text, txtGridDisplayCode.Text, ddlGridCountryName.SelectedValue, _ObjiDsignBAL.ReplaceNewLines(txtGridNotes.Text, true));
                        _ObjCashierBE.CashOfficeId = Convert.ToInt32(ddlGridCashOffice.SelectedValue);
                        _ObjCashierBE.FirstName = txtGridFirstName.Text.Trim();
                        _ObjCashierBE.MiddleName = txtGridMiddleName.Text.Trim();
                        _ObjCashierBE.LastName = txtGridLastName.Text.Trim();
                        _ObjCashierBE.EmailId = txtGridEmailId.Text.Trim();
                        _ObjCashierBE.ContactNo1 = txtGridCode1.Text.Trim() + "-" + txtGridContactNo1.Text.Trim();
                        _ObjCashierBE.ContactNo2 = txtGridCode2.Text.Trim() + "-" + txtGridContactNo2.Text.Trim();
                        _ObjCashierBE.Address1 = txtGridAddress1.Text.Trim();
                        _ObjCashierBE.Address2 = txtGridAddress2.Text.Trim();
                        _ObjCashierBE.City = txtGridCity.Text.Trim();
                        _ObjCashierBE.ZipCode = txtGridZip.Text.Trim();
                        _ObjCashierBE.Notes = txtGridNotes.Text.Trim();
                        _ObjCashierBE.CashierId = Convert.ToInt32(lblCashierId.Text);
                        _ObjCashierBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();

                        xml = _objCashierBAL.InsertCashier(_ObjCashierBE, Statement.Update);
                        _ObjCashierBE = _ObjiDsignBAL.DeserializeFromXml<CashierBE>(xml);

                        if (_ObjCashierBE.RowsEffected > 0)
                        {
                            BindGrid();
                            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            Message("Cashier Updated Successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);

                            txtGridFirstName.Text = txtGridMiddleName.Text = txtGridLastName.Text =
                                txtGridEmailId.Text = txtGridAddress1.Text = txtGridAddress2.Text =
                                    txtGridCity.Text = txtGridZip.Text = string.Empty;
                        }
                        else if (_ObjCashierBE.IsEmailIdExists)
                        {
                            Message("Cashier Email Id already exists.", UMS_Resource.MESSAGETYPE_ERROR);
                            txtGridEmailId.Focus();
                        }
                        else
                            Message("Cashier updation failed.", UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "ACTIVECASHIER":
                        //xml = _ObjUMS_Service.DeleteActiveState(1, lblStateCode.Text);
                        mpeActivate.Show();
                        lblActiveMsg.Text = "Are you sure you want to Activate this Cashier?";
                        btnActiveOk.Focus();

                        break;
                    case "DEACTIVECASHIER":
                        //xml = _ObjUMS_Service.DeleteActiveState(0, lblStateCode.Text);
                        mpeDeActive.Show();
                        //hfStateCode.Value = lblStateCode.Text;
                        lblDeActive.Text = "Are you sure you want to DeActivate this Cashier?";
                        btnDeActiveOk.Focus();

                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvCashierList_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            Page.Form.Attributes.Add("enctype", "multipart/form-data");
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnCashierUpdate = (LinkButton)e.Row.FindControl("lkbtnCashierUpdate");
                    TextBox txtGridFirstName = (TextBox)e.Row.FindControl("txtGridFirstName");
                    TextBox txtGridLastName = (TextBox)e.Row.FindControl("txtGridLastName");
                    TextBox txtGridEmailId = (TextBox)e.Row.FindControl("txtGridEmailId");
                    TextBox txtGridContactNo1 = (TextBox)e.Row.FindControl("txtGridContactNo1");
                    TextBox txtGridContactNo2 = (TextBox)e.Row.FindControl("txtGridContactNo2");
                    TextBox txtGridCode1 = (TextBox)e.Row.FindControl("txtGridCode1");
                    TextBox txtGridCode2 = (TextBox)e.Row.FindControl("txtGridCode2");
                    TextBox txtGridAddress1 = (TextBox)e.Row.FindControl("txtGridAddress1");
                    TextBox txtGridAddress2 = (TextBox)e.Row.FindControl("txtGridAddress2");
                    TextBox txtGridCity = (TextBox)e.Row.FindControl("txtGridCity");
                    TextBox txtGridZip = (TextBox)e.Row.FindControl("txtGridZip");
                    DropDownList ddlGridCashOffice = (DropDownList)e.Row.FindControl("ddlGridCashOffice");

                    //FileUpload fupGridPhoto = (FileUpload)e.Row.FindControl("fupGridPhoto");


                    //PostBackTrigger pst = new PostBackTrigger();
                    //pst.ControlID = fupGridPhoto.UniqueID;
                    //this.upAgencies.Triggers.Add(pst);


                    //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(fupGridPhoto);
                    //upAddAgency.Update();
                    //TextBox txtGridDisplayCode = (TextBox)e.Row.FindControl("txtGridDisplayCode");

                    lkbtnCashierUpdate.Attributes.Add("onclick",
                        "return UpdateValidation('" + ddlGridCashOffice.ClientID + "','"
                                                    + txtGridFirstName.ClientID + "','"
                                                    + txtGridLastName.ClientID + "','"
                                                    + txtGridEmailId.ClientID + "','"
                                                    + txtGridContactNo1.ClientID + "','"
                                                    + txtGridContactNo2.ClientID + "','"
                                                    + txtGridCode1.ClientID + "','"
                                                    + txtGridCode2.ClientID + "','"
                                                    + txtGridAddress1.ClientID + "','"
                                                    + txtGridCity.ClientID + "','"
                                                    + txtGridZip.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");
                    
                    //Image imgCashier = (Image)e.Row.FindControl("imgCashier");
                    //if (!string.IsNullOrEmpty(lblGridPhoto.Text))
                    //    imgCashier.ImageUrl = "~/Uploads/UserPhotos/" + lblGridPhoto.Text;

                    if (Convert.ToInt32(lblActiveStatusId.Text) == 2)
                    {
                        e.Row.FindControl("lkbtnDelete").Visible = e.Row.FindControl("lkbtnCashierEdit").Visible = false;
                        e.Row.FindControl("lkbtnCashierActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDelete").Visible = e.Row.FindControl("lkbtnCashierEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnActiveOk_OnClick(object sender, EventArgs e)
        {
            try
            {
                CashierBE _ObjCashierBE = new CashierBE();
                _ObjCashierBE.CashierId = Convert.ToInt32(hfCashierId.Value);
                _ObjCashierBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _ObjCashierBE.ActiveStatusId = 1;

                xml = _objCashierBAL.InsertCashier(_ObjCashierBE, Statement.Delete);
                _ObjCashierBE = _ObjiDsignBAL.DeserializeFromXml<CashierBE>(xml);

                if (_ObjCashierBE.RowsEffected > 0)
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message("Cashier activated successfully", UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                    Message("Cashier activation failed", UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_OnClick(object sender, EventArgs e)
        {
            try
            {
                CashierBE _ObjCashierBE = new CashierBE();
                _ObjCashierBE.CashierId = Convert.ToInt32(hfCashierId.Value);
                _ObjCashierBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _ObjCashierBE.ActiveStatusId = 2;
                xml = _objCashierBAL.InsertCashier(_ObjCashierBE, Statement.Delete);
                _ObjCashierBE = _ObjiDsignBAL.DeserializeFromXml<CashierBE>(xml);
                if (_ObjCashierBE.RowsEffected > 0)
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message("Cashier Deactivated Successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                {
                    //if (_ObjMarketersBe.AssciatedStateCount > 0)
                    //{
                    //    lblgridalertmsg.Text = "LGA can not be deactivated as it contains " +
                    //                           _ObjMarketersBe.AssciatedStateCount + " States.";
                    //    mpegridalert.Show();
                    //}
                    //else
                    Message("Cashier Deactivation Failed.", UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
    }
}