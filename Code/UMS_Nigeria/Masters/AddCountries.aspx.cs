﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddCountries
                     
 Developer        : id065-Bhimaraju V
 Creation Date    : 14-Feb-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBE;
using System.Xml;
using UMS_NigeriaBAL;
using Resources;
using System.Data;
using System.Threading;
using System.Globalization;
using System.Drawing;
using UMS_NigriaDAL;
using System.Web.Services;

namespace UMS_Nigeria.Masters
{
    public partial class AddCountries : System.Web.UI.Page
    {
        #region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        CommonDAL _ObjCommonDAL = new CommonDAL();
        UMS_Service _ObjUMS_Service = new UMS_Service();
        public int PageNum;
        XmlDocument xml = null;
        MastersBAL _ObjMastersBAL = new MastersBAL();
        string Key = "AddCountries";
        #endregion

        #region Properties

        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
        }
        private string CountryName
        {
            get { return txtCountryName.Text.Trim(); }
            set { txtCountryName.Text = value; }
        }

        //private string CountryCode
        //{
        //    get { return txtCountryCode.Text.Trim(); }
        //    set { txtCountryCode.Text = value; }
        //}

        private string Notes
        {
            get { return txtNotes.Text.Trim(); }
            set { txtNotes.Text = value; }
        }

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE, false);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = _ObjCommonMethods.GetPagePath(this.Request.Url);
                //if (_ObjCommonMethods.IsAccess(path))
                //{
                //Session[SeaWaysMIS_Resources.SESSION_PAGENAME] = ConstantsBE.NewCountryPageName;
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                //}
                //else
                //{
                //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                //}
            }
        }

        protected void btnSaveNext_Click(object sender, EventArgs e)
        {
            try
            {
                hfRecognize.Value = UMS_Resource.SAVENEXT;
                lblActiveMsg.Text = Resource.CHK_BEFORE_U_SAVE;
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    btnSaveNext.Visible = txtCountryCode.Enabled = true;
            //    txtCountryCode.Text =
            //        txtCountryName.Text = txtCurrency.Text = txtCurrencySymbol.Text = txtNotes.Text = string.Empty;
            //}
            //catch (Exception ex)
            //{
            //    Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
            //    try
            //    {
            //        _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            //    }
            //    catch (Exception logex)
            //    {

            //    }
            //}
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                hfRecognize.Value = UMS_Resource.SAVE;
                lblActiveMsg.Text = Resource.CHK_BEFORE_U_SAVE;
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Constants.Region + "?" + UMS_Resource.QSTR_COUNTRY_CODE + "=" + _ObjiDsignBAL.Encrypt(hfCountryCode.Value), false);

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvCountryList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnCountryUpdate = (LinkButton)e.Row.FindControl("lkbtnCountryUpdate");
                    TextBox txtGridCountryName = (TextBox)e.Row.FindControl("txtGridCountryName");
                    TextBox txtGridCurrencyName = (TextBox)e.Row.FindControl("txtGridCurrencyName");
                    TextBox txtGridCurrencySymbol = (TextBox)e.Row.FindControl("txtGridCurrencySymbol");

                    lkbtnCountryUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridCountryName.ClientID + "','" + txtGridCurrencyName.ClientID + "','" + txtGridCurrencySymbol.ClientID + "')");

                    //LinkButton lkbtnCountryActive = (LinkButton)e.Row.FindControl("lkbtnCountryActive");
                    //Label lblCountryCode = (Label)e.Row.FindControl("lblCountryCode");
                    //hfCountryCode.Value = lblCountryCode.Text;
                    //lkbtnCountryActive.Attributes.Add("onclick", "return Activatepop()");

                    Label lblIsActive = (Label)e.Row.FindControl("lblIsActive");


                    if ((!Convert.ToBoolean(lblIsActive.Text)))
                    {
                        e.Row.FindControl("lkbtnDelete").Visible = e.Row.FindControl("lkbtnCountryEdit").Visible = false;
                        e.Row.FindControl("lkbtnCountryActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDelete").Visible = e.Row.FindControl("lkbtnCountryEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvCountryList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                MastersBE _ObjMastersBE = new MastersBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridCountryName = (TextBox)row.FindControl("txtGridCountryName");
                TextBox txtGridCurrencyName = (TextBox)row.FindControl("txtGridCurrencyName");
                TextBox txtGridCurrencySymbol = (TextBox)row.FindControl("txtGridCurrencySymbol");
                TextBox txtGridNotes = (TextBox)row.FindControl("txtGridNotes");
                Label lblCountryCode = (Label)row.FindControl("lblCountryCode");
                Label lblCountryName = (Label)row.FindControl("lblCountryName");
                Label lblCurrencyName = (Label)row.FindControl("lblCurrencyName");
                Label lblCurrencySymbol = (Label)row.FindControl("lblCurrencySymbol");
                Label lblNotes = (Label)row.FindControl("lblNotes");
                hfRecognize.Value = string.Empty;

                switch (e.CommandName.ToUpper())
                {
                    case "EDITCOUNTRY":
                        lblCountryName.Visible = lblNotes.Visible = lblCurrencyName.Visible = lblCurrencySymbol.Visible = false;
                        txtGridCountryName.Visible = txtGridNotes.Visible = txtGridCurrencyName.Visible = txtGridCurrencySymbol.Visible = true;
                        txtGridCountryName.Text = lblCountryName.Text;
                        txtGridCurrencyName.Text = lblCurrencyName.Text;
                        txtGridCurrencySymbol.Text = lblCurrencySymbol.Text;
                        txtGridNotes.Text = _ObjiDsignBAL.ReplaceNewLines(lblNotes.Text, false);
                        row.FindControl("lkbtnCountryCancel").Visible = row.FindControl("lkbtnCountryUpdate").Visible = true;
                        row.FindControl("lkbtnDelete").Visible = row.FindControl("lkbtnCountryEdit").Visible = false;
                        break;
                    case "CANCELCOUNTRY":
                        row.FindControl("lkbtnDelete").Visible = row.FindControl("lkbtnCountryEdit").Visible = true;
                        row.FindControl("lkbtnCountryCancel").Visible = row.FindControl("lkbtnCountryUpdate").Visible = false;
                        txtGridCountryName.Visible = txtGridNotes.Visible = txtGridCurrencyName.Visible = txtGridCurrencySymbol.Visible = false;
                        lblCountryName.Visible = lblNotes.Visible = lblCurrencyName.Visible = lblCurrencySymbol.Visible = true;
                        break;
                    case "UPDATECOUNTRY":
                        //xml = _ObjUMS_Service.UpdateCountries(txtGridCountryName.Text, lblCountryCode.Text, _ObjiDsignBAL.ReplaceNewLines(txtGridNotes.Text, true));
                        _ObjMastersBE.CountryName = txtGridCountryName.Text;
                        _ObjMastersBE.CountryCode = lblCountryCode.Text;
                        _ObjMastersBE.Currency = txtGridCurrencyName.Text;
                        _ObjMastersBE.CurrencySymbol = txtGridCurrencySymbol.Text;
                        _ObjMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        _ObjMastersBE.Notes = _ObjiDsignBAL.ReplaceNewLines(txtGridNotes.Text, true);
                        xml = _ObjMastersBAL.Countries(_ObjMastersBE, Statement.Update);
                        _ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(xml);
                        if (Convert.ToBoolean(_ObjMastersBE.IsSuccess))
                        {
                            // gvCountryList.EditIndex = -1;
                            BindGrid();
                            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            Message(UMS_Resource.COUNTRY_DETAILS_UPDATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        else
                            Message(UMS_Resource.COUNTRY_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                        break;
                    case "ACTIVECOUNTRY":
                        //xml = _ObjUMS_Service.DeleteActiveCountry(1, lblCountryCode.Text);
                        mpeActivate.Show();
                        hfCountryCode.Value = lblCountryCode.Text;
                        lblActiveMsg.Text = UMS_Resource.ACTIVE_COUNTRY_POPUP_TEXT;
                        btnActiveOk.Focus();
                        //_ObjMastersBE.CountryCode = lblCountryCode.Text;
                        //_ObjMastersBE.IsActive = Convert.ToBoolean(1);
                        //_ObjMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();

                        //xml = _ObjMastersBAL.Countries(_ObjMastersBE, Statement.Delete);
                        //_ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(xml);
                        //if (Convert.ToBoolean(_ObjMastersBE.IsSuccess))
                        //{
                        //    BindGridCountriesList();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message(UMS_Resource.COUNTRY_DETAILS_ACTIVATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        //}
                        //else { Message(UMS_Resource.COUNTRY_DETAILS_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR); }

                        break;
                    case "DEACTIVECOUNTRY":
                        //xml = _ObjUMS_Service.DeleteActiveCountry(0, lblCountryCode.Text);
                        mpeDeActive.Show();
                        hfCountryCode.Value = lblCountryCode.Text;
                        lblDeActiveCountry.Text = UMS_Resource.DEACTIVE_COUNTRY_POPUP_TEXT;
                        btnDeActiveOk.Focus();
                        //_ObjMastersBE.CountryCode = lblCountryCode.Text;
                        //_ObjMastersBE.IsActive = Convert.ToBoolean(0);
                        //_ObjMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();

                        //xml = _ObjMastersBAL.Countries(_ObjMastersBE, Statement.Delete);

                        //_ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(xml);
                        //if (Convert.ToBoolean(_ObjMastersBE.IsSuccess))
                        //{
                        //    BindGridCountriesList();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message(UMS_Resource.COUNTRYDETAILS_DEACTIVATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        //}
                        //else { Message(UMS_Resource.COUNTRYDETAILS_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR); }

                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (hfRecognize.Value == Constants.SAVE)
                {
                    hfRecognize.Value = string.Empty;
                    MastersBE _ObjMastersBE = new MastersBE();
                    _ObjMastersBE.CountryName = CountryName;
                    //_ObjMastersBE.CountryCode = CountryCode;
                    _ObjMastersBE.Currency = txtCurrency.Text;
                    _ObjMastersBE.CurrencySymbol = txtCurrencySymbol.Text;
                    _ObjMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    _ObjMastersBE.Notes = _ObjiDsignBAL.ReplaceNewLines(Notes, true);
                    xml = _ObjMastersBAL.Countries(_ObjMastersBE, Statement.Insert);
                    //xml = _ObjUMS_Service.Countries(this.CountryName, this.CountryCode, _ObjiDsignBAL.ReplaceNewLines(this.Notes, true));
                    _ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(xml);
                    if (Convert.ToBoolean(_ObjMastersBE.IsSuccess))
                    {
                        BindGrid();
                        UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        txtCountryName.Text = txtCurrency.Text = txtCurrencySymbol.Text = txtNotes.Text = string.Empty;
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        Message(UMS_Resource.COUNTRYDETAILS_INSERT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        //btnReset_Click(null, null);
                    }
                    else if (_ObjMastersBE.IsCountryNameExists)
                    {
                        Message(UMS_Resource.COUNTRY_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else if (_ObjMastersBE.IsCountryCodeExists)
                    {
                        Message(UMS_Resource.COUNTRY_CODE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
                else if (hfRecognize.Value == Constants.SAVENEXT)
                {
                    hfRecognize.Value = string.Empty;
                    InsertCountryDetails();
                }
                else
                {
                    MastersBE _ObjMastersBE = new MastersBE();
                    _ObjMastersBE.CountryCode = hfCountryCode.Value;
                    _ObjMastersBE.IsActive = Convert.ToBoolean(1);
                    _ObjMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();

                    xml = _ObjMastersBAL.Countries(_ObjMastersBE, Statement.Delete);
                    _ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(xml);
                    if (Convert.ToBoolean(_ObjMastersBE.IsSuccess))
                    {
                        BindGrid();
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        Message(UMS_Resource.COUNTRY_DETAILS_ACTIVATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    }
                    else { Message(UMS_Resource.COUNTRY_DETAILS_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR); }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE _ObjMastersBE = new MastersBE();
                _ObjMastersBE.CountryCode = hfCountryCode.Value;
                _ObjMastersBE.IsActive = Convert.ToBoolean(0);
                _ObjMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();

                xml = _ObjMastersBAL.Countries(_ObjMastersBE, Statement.Delete);

                _ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(xml);
                if (_ObjMastersBE.IsSuccess)
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.COUNTRYDETAILS_DEACTIVATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else if (!_ObjMastersBE.IsSuccess && _ObjMastersBE.Count > 0)
                    Message(string.Format(UMS_Resource.COUNTRYDETAILS_HAVE_CUSTOMERS, _ObjMastersBE.Count), UMS_Resource.MESSAGETYPE_ERROR);
                else
                    Message(UMS_Resource.COUNTRYDETAILS_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void BindGrid()
        {
            try
            {
                MastersListBE _objMastersListBE = new MastersListBE();
                MastersBE _ObjMastersBE = new MastersBE();
                XmlDocument resultedXml = new XmlDocument();
                //resultedXml = _ObjUMS_Service.GetActiveCountriesList(Convert.ToInt32(hfPageNo.Value), PageSize);

                _ObjMastersBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                _ObjMastersBE.PageSize = PageSize;
                resultedXml = _ObjMastersBAL.Get(_ObjMastersBE, ReturnType.Get);
                _objMastersListBE = _ObjiDsignBAL.DeserializeFromXml<MastersListBE>(resultedXml);

                if (_objMastersListBE.Items.Count > 0)
                {
                    divdownpaging.Visible = divpaging.Visible = true;
                    hfTotalRecords.Value = _objMastersListBE.Items[0].TotalRecords.ToString();
                    gvCountryList.DataSource = _objMastersListBE.Items;
                    gvCountryList.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = _objMastersListBE.Items.Count.ToString();
                    divdownpaging.Visible = divpaging.Visible = false;
                    gvCountryList.DataSource = new DataTable(); ;
                    gvCountryList.DataBind();
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void InsertCountryDetails()
        {
            try
            {
                MastersBE _ObjMastersBE = new MastersBE();
                //hfCountryCode.Value = this.CountryCode;
                //xml = _ObjUMS_Service.Countries(this.CountryName, this.CountryCode, _ObjiDsignBAL.ReplaceNewLines(this.Notes, true));
                _ObjMastersBE.CountryName = CountryName;
                _ObjMastersBE.Currency = txtCurrency.Text;
                _ObjMastersBE.CurrencySymbol = txtCurrencySymbol.Text;
                // _ObjMastersBE.CountryCode = CountryCode;
                _ObjMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _ObjMastersBE.Notes = _ObjiDsignBAL.ReplaceNewLines(Notes, true);
                xml = _ObjMastersBAL.Countries(_ObjMastersBE, Statement.Insert);
                _ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(xml);
                if (Convert.ToBoolean(_ObjMastersBE.IsSuccess))
                {
                    BindGrid();
                    txtCountryName.Text = txtCurrency.Text = txtCurrencySymbol.Text = txtNotes.Text = string.Empty;
                    lblMsgPopup.Text = UMS_Resource.COUNTRYDETAILS_INSERT_SUCCESS;
                    mpeCountrypopup.Show();
                    btnOk.Focus();
                }
                else if (_ObjMastersBE.IsCountryNameExists)
                {
                    Message(UMS_Resource.COUNTRY_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (_ObjMastersBE.IsCountryCodeExists)
                {
                    Message(UMS_Resource.COUNTRY_CODE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion       

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion


    }
}