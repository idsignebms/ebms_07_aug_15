﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using iDSignHelper;
using UMS_NigeriaBAL;
using System.Data;
using System.Globalization;
using System.Threading;
using Resources;
using UMS_NigeriaBE;
using UMS_NigriaDAL;

namespace UMS_Nigeria.Masters
{
    public partial class AddCustomerIdentityType : System.Web.UI.Page
    {

        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        string Key = "AddCustomerIdentityType";
        DataTable dtData = new DataTable();
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
        }
        private string IdentityType
        {
            get { return txtIdentityType.Text.Trim(); }
            set { txtIdentityType.Text = value; }
        }
        private string Details
        {
            get { return txtDetails.Text.Trim(); }
            set { txtDetails.Text = value; }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
            }
        }

        protected void gvIdentityTypesList_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridIdentityType = (TextBox)row.FindControl("txtGridIdentityType");
                TextBox txtGridDetails = (TextBox)row.FindControl("txtGridDetails");

                Label lblIdentityId = (Label)row.FindControl("lblIdentityId");
                Label lblType = (Label)row.FindControl("lblType");
                Label lblDetails = (Label)row.FindControl("lblDetails");

                //Label lblIsMaster = (Label)row.FindControl("lblIsMaster");
                switch (e.CommandName.ToUpper())
                {
                    case "EDITIDENTITYTYPE":
                        lblType.Visible = lblDetails.Visible = false;
                        txtGridIdentityType.Visible = txtGridDetails.Visible = true;
                        txtGridIdentityType.Text = lblType.Text;
                        txtGridDetails.Text = objiDsignBAL.ReplaceNewLines(lblDetails.Text, false);
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;

                        //if (Convert.ToBoolean(lblIsMaster.Text))
                        //    row.FindControl("lkbtnDeActive").Visible = false;
                        break;
                    case "CANCELIDENTITYTYPE":
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;

                        //if (Convert.ToBoolean(lblIsMaster.Text))
                        //    row.FindControl("lkbtnDeActive").Visible = false;
                        txtGridIdentityType.Visible = txtGridDetails.Visible = false;
                        lblType.Visible = lblDetails.Visible = true;
                        break;
                    case "UPDATEIDENTITYTYPE":
                        objMastersBE.IdentityId = Convert.ToInt32(lblIdentityId.Text);
                        objMastersBE.Type = txtGridIdentityType.Text.Trim();
                        objMastersBE.Details = objiDsignBAL.ReplaceNewLines(txtGridDetails.Text.Trim(), true);
                        objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        xml = objMastersBAL.IdentityType(objMastersBE, Statement.Update);
                        objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                        if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        {
                            BindGrid();
                            Message("Identity Type updated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        else if (objMastersBE.IsCustomerTypeExists)
                        {
                            Message("Identity Type already exists.", UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else
                            Message("Identity Type Updation Failed.", UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "ACTIVEIDENTITYTYPE":
                        mpeActivate.Show();
                        hfIdentityId.Value = lblIdentityId.Text;
                        lblActiveMsg.Text = "Are you sure you want to activate this Identity type ?";
                        btnActiveOk.Focus();
                        break;
                    case "DEACTIVEIDENTITYTYPE":
                        mpeDeActive.Show();
                        hfIdentityId.Value = lblIdentityId.Text;
                        lblDeActiveMsg.Text = "Are you sure you want to deactivate this Identity type ?";
                        btnDeActiveOk.Focus();
                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void gvIdentityTypesList_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    TextBox txtGridIdentityType = (TextBox)e.Row.FindControl("txtGridIdentityType");

                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridIdentityType.ClientID + "')");


                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");


                    if (Convert.ToInt32(lblActiveStatusId.Text) == Constants.DeActive)
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }

                    //Label lblIsMaster = (Label)e.Row.FindControl("lblIsMaster");
                    //if (Convert.ToBoolean(lblIsMaster.Text))
                    //    e.Row.FindControl("lkbtnDeActive").Visible = false;

                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = InsertCustomerIdentityTypes();

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(Resource.IDENTITY_TYPE_SAVED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    IdentityType = Details = string.Empty;

                }
                else if (objMastersBE.IsCustomerTypeExists)
                {
                    Message(Resource.IDENTITY_TYPE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                    Message(Resource.IDENTITY_TYPE_SAVE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.Active;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.IdentityId = Convert.ToInt32(hfIdentityId.Value);
                xml = objMastersBAL.IdentityType(objMastersBE, Statement.Delete);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(Resource.IDENTITY_TYPE_ACTIVATED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else
                    Message(Resource.IDENTITY_TYPE_ACTIVATION_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.DeActive;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.IdentityId = Convert.ToInt32(hfIdentityId.Value);
                xml = objMastersBAL.IdentityType(objMastersBE, Statement.Delete);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (objMastersBE.IsSuccess)
                {
                    BindGrid();
                    Message(Resource.IDENTITY_TYPE_DEACTIVATED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else if (!objMastersBE.IsSuccess && objMastersBE.Count > 0)
                {
                    //Message(UMS_Resource.IDENTITY_TYPE_HAVING_CUSTOMERS, UMS_Resource.MESSAGETYPE_ERROR);     
                    lblgridalertmsg.Text = UMS_Resource.IDENTITY_TYPE_HAVING_CUSTOMERS;
                    mpegridalert.Show();
                    btngridalertok.Focus();
                }
                else
                    Message(Resource.IDENTITY_TYPE_DEACTIVATION_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void BindGrid()
        {
            try
            {
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();
                XmlDocument resultedXml = new XmlDocument();

                objMastersBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                objMastersBE.PageSize = PageSize;
                resultedXml = objMastersBAL.IdentityType(objMastersBE, ReturnType.Get);

                objMastersListBE = objiDsignBAL.DeserializeFromXml<MastersListBE>(resultedXml);
                if (objMastersListBE.Items.Count > 0)
                {
                    divdownpaging.Visible = divpaging.Visible = true;
                    hfTotalRecords.Value = objMastersListBE.Items[0].TotalRecords.ToString();
                    gvIdentityTypesList.DataSource = objMastersListBE.Items;
                    gvIdentityTypesList.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = objMastersListBE.Items.Count.ToString();
                    divdownpaging.Visible = divpaging.Visible = false;
                    gvIdentityTypesList.DataSource = new DataTable(); ;
                    gvIdentityTypesList.DataBind();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private MastersBE InsertCustomerIdentityTypes()
        {
            MastersBE objMastersBE = new MastersBE();
            try
            {
                objMastersBE.Type = this.IdentityType;
                objMastersBE.Details = objiDsignBAL.ReplaceNewLines(this.Details, true);
                objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                xml = objMastersBAL.IdentityType(objMastersBE, Statement.Insert);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                return objMastersBE;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            return objMastersBE;
        }

        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}