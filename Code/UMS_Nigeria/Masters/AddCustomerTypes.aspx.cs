﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using iDSignHelper;
using UMS_NigeriaBAL;
using System.Data;
using System.Globalization;
using System.Threading;
using Resources;
using UMS_NigeriaBE;
using UMS_NigriaDAL;

namespace UMS_Nigeria.Masters
{
    public partial class AddCustomerTypes : System.Web.UI.Page
    {

        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        string Key = "AddCustomerTypes";
        DataTable dtData = new DataTable();
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
        }
        private string CustomerType
        {
            get { return txtCustomerType.Text.Trim(); }
            set { txtCustomerType.Text = value; }
        }
        private string Details
        {
            get { return txtDetails.Text.Trim(); }
            set { txtDetails.Text = value; }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
            }
        }

        protected void gvCustomerTypesList_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridCustomerType = (TextBox)row.FindControl("txtGridCustomerType");
                TextBox txtGridDetails = (TextBox)row.FindControl("txtGridDetails");

                Label lblCustomerTypeId = (Label)row.FindControl("lblCustomerTypeId");
                Label lblCustomerType = (Label)row.FindControl("lblCustomerType");
                Label lblDetails = (Label)row.FindControl("lblDetails");

                Label lblIsMaster = (Label)row.FindControl("lblIsMaster");
                switch (e.CommandName.ToUpper())
                {
                    case "EDITCUSTOMERTYPE":
                        lblCustomerType.Visible = lblDetails.Visible = false;
                        txtGridCustomerType.Visible = txtGridDetails.Visible = true;
                        txtGridCustomerType.Text = lblCustomerType.Text;
                        txtGridDetails.Text = objiDsignBAL.ReplaceNewLines(lblDetails.Text, false);
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;

                        if (Convert.ToBoolean(lblIsMaster.Text))
                            row.FindControl("lkbtnDeActive").Visible = false;
                        break;
                    case "CANCELCUSTOMERTYPE":
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;

                        if (Convert.ToBoolean(lblIsMaster.Text))
                            row.FindControl("lkbtnDeActive").Visible = false;
                        txtGridCustomerType.Visible = txtGridDetails.Visible = false;
                        lblCustomerType.Visible = lblDetails.Visible = true;
                        break;
                    case "UPDATECUSTOMERTYPE":
                        objMastersBE.CustomerTypeId = Convert.ToInt32(lblCustomerTypeId.Text);
                        objMastersBE.CustomerType = txtGridCustomerType.Text.Trim();
                        objMastersBE.Details = objiDsignBAL.ReplaceNewLines(txtGridDetails.Text.Trim(), true);
                        objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        xml = objMastersBAL.CustomerType(objMastersBE, Statement.Update);
                        objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                        if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        {
                            BindGrid();
                            Message("Customer Type updated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        else if (objMastersBE.IsCustomerTypeExists)
                        {
                            Message("Customer Type already exists.", UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else
                            Message("Customer Updation Failed.", UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "ACTIVECUSTOMERTYPE":
                        mpeActivate.Show();
                        hfCustomerTypeId.Value = lblCustomerTypeId.Text;
                        lblActiveMsg.Text = "Are you sure you want to activate this customer type ?";
                        btnActiveOk.Focus();
                        break;
                    case "DEACTIVECUSTOMERTYPE":
                        mpeDeActive.Show();
                        hfCustomerTypeId.Value = lblCustomerTypeId.Text;
                        lblDeActiveMsg.Text = "Are you sure you want to deactivate this customer type ?";
                        btnDeActiveOk.Focus();
                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void gvCustomerTypesList_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    TextBox txtGridCustomerType = (TextBox)e.Row.FindControl("txtGridCustomerType");

                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridCustomerType.ClientID + "')");


                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");


                    if (Convert.ToInt32(lblActiveStatusId.Text) == Constants.DeActive)
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }

                    Label lblIsMaster = (Label)e.Row.FindControl("lblIsMaster");
                    if (Convert.ToBoolean(lblIsMaster.Text))
                        e.Row.FindControl("lkbtnDeActive").Visible = false;

                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = InsertCustomerTypes();

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(Resource.CUST_TYPE_SAVED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    CustomerType = Details = string.Empty;

                }
                else if (objMastersBE.IsCustomerTypeExists)
                {
                    Message(Resource.CUST_TYPE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                    Message(Resource.CUST_TYPE_SAVE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.Active;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.CustomerTypeId = Convert.ToInt32(hfCustomerTypeId.Value);
                xml = objMastersBAL.CustomerType(objMastersBE, Statement.Delete);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(Resource.CUST_TYPE_ACTIVATED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else
                    Message(Resource.CUST_TYPE_ACTIVATION_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.DeActive;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.CustomerTypeId = Convert.ToInt32(hfCustomerTypeId.Value);
                xml = objMastersBAL.CustomerType(objMastersBE, Statement.Delete);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (objMastersBE.IsSuccess)
                {
                    BindGrid();
                    Message("Customer type deactivated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else if (!objMastersBE.IsSuccess && objMastersBE.Count > 0)
                    Message(string.Format(UMS_Resource.CUSTOMER_TYPE_HAVING_CUSTOMERS, objMastersBE.Count), UMS_Resource.MESSAGETYPE_ERROR);
                else
                    Message("Customer type activation failed.", UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void BindGrid()
        {
            try
            {
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();
                XmlDocument resultedXml = new XmlDocument();

                objMastersBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                objMastersBE.PageSize = PageSize;
                resultedXml = objMastersBAL.CustomerType(objMastersBE, ReturnType.Get);

                objMastersListBE = objiDsignBAL.DeserializeFromXml<MastersListBE>(resultedXml);
                if (objMastersListBE.Items.Count > 0)
                {
                    divdownpaging.Visible = divpaging.Visible = true;
                    hfTotalRecords.Value = objMastersListBE.Items[0].TotalRecords.ToString();
                    gvCustomerTypesList.DataSource = objMastersListBE.Items;
                    gvCustomerTypesList.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = objMastersListBE.Items.Count.ToString();
                    divdownpaging.Visible = divpaging.Visible = false;
                    gvCustomerTypesList.DataSource = new DataTable(); ;
                    gvCustomerTypesList.DataBind();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private MastersBE InsertCustomerTypes()
        {
            MastersBE objMastersBE = new MastersBE();
            try
            {
                objMastersBE.CustomerType = this.CustomerType;
                objMastersBE.Details = objiDsignBAL.ReplaceNewLines(this.Details, true);
                objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                xml = objMastersBAL.CustomerType(objMastersBE, Statement.Insert);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                return objMastersBE;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            return objMastersBE;
        }

        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}