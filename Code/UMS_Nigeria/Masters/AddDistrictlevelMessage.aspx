﻿<%@ Page Title=":: Add Business Unit Level Messages ::" Language="C#" MasterPageFile="~/MasterPages/EDMUMS.Master" Theme="Green"
    AutoEventWireup="true" CodeBehind="AddDistrictlevelMessage.aspx.cs" Inherits="UMS_Nigeria.Masters.AddDistrictlevelMessage" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="edmcontainer">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <h3>
            <asp:Literal ID="litAddGlobalMessage" runat="server" Text="<%$ Resources:Resource, ADD_DISTRICTLEVELMESSAGE%>"></asp:Literal>
        </h3>
        <div class="clear">
            &nbsp;</div>
        <div class="consumer_feild">
            <div class="consume_name">
                <asp:Literal ID="litState" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal><span>*</span></div>
            <div class="consume_input">
                <asp:UpdatePanel ID="upDDL" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <%--<asp:DropDownList ID="ddlDistrict" runat="server" AutoPostBack="true" onchange="DropDownlistOnChangelbl(this,'spanStateCode','District')"
                            OnSelectedIndexChanged="ddlState_SelectedIndexChanged">
                        </asp:DropDownList>--%>
                        <asp:DropDownList ID="ddlBusinessUnit" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlBusinessUnit_SelectedIndexChanged">
                            <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                        </asp:DropDownList>
                        <span id="spanddlBusinessUnit" class="spancolor"></span>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <%--<span id="spanStateCode" class="spancolor"></span>--%>
            </div>
            <%--<div class="consume_input" style="width: 20px">
                <asp:HyperLink ID="lkbtnAddNewBusinessUnit" NavigateUrl="~/Masters/AddBusinessUnits.aspx"
                    runat="server" Text="<%$ Resources:Resource, ADD%>"></asp:HyperLink>
            </div>--%>
            <div class="consume_name c_left">
                <asp:Literal ID="litMsgtype" runat="server" Text="<%$ Resources:Resource, MESSAGETYPE%>"></asp:Literal><span>*</span></div>
            <div class="consume_input consume_radio">
                <asp:RadioButtonList ID="rblMessageType" runat="server" RepeatDirection="Horizontal"
                    Style="width: 271px">
                </asp:RadioButtonList>
                <span id="spanBillType" class="spancolor"></span>
            </div>
            <div class="clear pad_5">
            </div>
            <div class="consume_name ">
                <asp:Literal ID="litMsg1" runat="server" Text="<%$ Resources:Resource, MESSAGE1%>"></asp:Literal><span>*</span></div>
            <div class="consume_input">
                <asp:TextBox ID="txtMessage1" runat="server" placeholder="Enter First Message" onkeypress="return StrictSymbol(event)"></asp:TextBox>
                <span id="spanFirstMessage" class="spancolor"></span>
            </div>
            <div class="consume_name c_left">
                <asp:Literal ID="litMsg2" runat="server" Text="<%$ Resources:Resource, MESSAGE2%>"></asp:Literal></div>
            <div class="consume_input" style="margin-left: 10px;">
                <asp:TextBox ID="txtMessage2" runat="server" placeholder="Enter Second Message" onkeypress="return StrictSymbol(event)"></asp:TextBox>
            </div>
            <div class="consume_name c_left">
                <asp:Literal ID="litMsg3" runat="server" Text="<%$ Resources:Resource, MESSAGE3%>"></asp:Literal></div>
            <div class="consume_input">
                <asp:TextBox ID="txtMessage3" runat="server" placeholder="Enter Third Message" onkeypress="return StrictSymbol(event)"></asp:TextBox>
            </div>
            <div class="clear pad_5">
                <div style="margin-left: 804px;">
                    <asp:Button ID="btnSave" CssClass="calculate_but" runat="server" OnClientClick="javascript:return Validate();"
                        Text="<%$ Resources:Resource, SAVE%>" OnClick="btnSave_Click" />
                    <asp:Button ID="btnReset" runat="server" CssClass="calculate_but" Text="<%$ Resources:Resource, RESET%>"
                        OnClientClick="Clearall()" OnClick="btnReset_Click" />
                </div>
            </div>
        </div>
        <%--Grid Starts Here --%>
        <div class="consumer_total">
            <div class="pad_10">
            </div>
        </div>
        <div class="consumer_total">
            <div class="pad_5 clear">
                <br />
            </div>
        </div>
        <asp:UpdatePanel ID="upGV" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
            <ContentTemplate>
                <table>
                    <tr style="vertical-align: top;">
                        <td style="border-right: 1px solid #ccc; padding-right: 50px;">
                            <div id="divgrid1" runat="server">
                                <h3 class="gridhead" align="left" style="width: 250px">
                                    <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, SPOTBILLINGMESSAGE%>"></asp:Literal>
                                </h3>
                                <div id="divpaging1" runat="server">
                                    <div align="right" class="marg_20t" runat="server" id="div2" style="width: 550px">
                                        <asp:LinkButton ID="lnkspotfirst" runat="server" CssClass="pagingfirst" OnClick="lnkspotfirst_Click">
                                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, FIRST%>"></asp:Literal></asp:LinkButton>
                                        &nbsp;|
                                        <asp:LinkButton ID="lnkspotprevious" runat="server" CssClass="pagingfirst" OnClick="lnkspotprevious_Click">
                                            <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, PREVIOUS%>"></asp:Literal></asp:LinkButton>
                                        &nbsp;|
                                        <asp:Label ID="lblSpotCurrentPage" runat="server" Font-Bold="true"></asp:Label>
                                        &nbsp;|
                                        <asp:LinkButton ID="lnkspotnext" runat="server" CssClass="pagingfirst" OnClick="lnkspotnext_Click">
                                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, NEXT%>"></asp:Literal></asp:LinkButton>
                                        &nbsp;|
                                        <asp:LinkButton ID="lnkspotlast" runat="server" CssClass="pagingfirst" OnClick="lnkspotlast_Click">
                                            <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, LAST%>"></asp:Literal></asp:LinkButton>
                                        &nbsp;|<asp:Literal ID="litSpotPageSize" runat="server" Text="<%$ Resources:Resource, PAGE_SIZE%>"></asp:Literal>
                                        <asp:DropDownList ID="ddlSpotPaze" runat="server" AutoPostBack="true" CssClass="paging-size"
                                            OnSelectedIndexChanged="ddlSpotPaze_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="consumer_total">
                                <div class="clear">
                                </div>
                            </div>
                            <asp:HiddenField ID="hfspotPageNo" runat="server" />
                            <asp:HiddenField ID="hfspotTotalRecords" runat="server" />
                            <asp:HiddenField ID="hfspotLastPage" runat="server" />
                            <asp:HiddenField ID="hfTemp" runat="server" />
                            <asp:HiddenField ID="hfRecognize" runat="server" />
                            <asp:HiddenField ID="hfspotPageSize" runat="server" />
                            <div class="clear">
                                &nbsp;</div>
                            <div class="grid">
                                <asp:GridView ID="GvSpotMSG" Width="550px" runat="server" DataKeyNames="DistrictMessageId"
                                    AutoGenerateColumns="false" EmptyDataText="No Records Found" OnRowDeleting="GvSpotMSG_RowDeleting">
                                    <Columns>
                                        <asp:BoundField DataField="Message" HeaderText="<%$ Resources:Resource, HEADER_SPOTBILLINGMESSAGE%>" />
                                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkItemDelete" runat="server" CommandName="Delete">
            <img src="../images/delete.png" alt="Delete" title="Delete" /></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </td>
                        <td style="padding-left: 40px;">
                            <div id="divgrid" runat="server">
                                <h3 class="gridhead" align="left" style="width: 250px">
                                    <asp:Literal ID="litSpotmsg" runat="server" Text="<%$ Resources:Resource, MANUALBILLINGMESSAGE%>"></asp:Literal>
                                </h3>
                                <div id="divpaging" runat="server">
                                    <div align="right" class="marg_20t" runat="server" id="divLinks" style="width: 550px">
                                        <asp:LinkButton ID="lkbtnFirst" runat="server" CssClass="pagingfirst" OnClick="lkbtnFirst_Click">
                                            <asp:Literal ID="litFirst" runat="server" Text="<%$ Resources:Resource, FIRST%>"></asp:Literal></asp:LinkButton>
                                        &nbsp;|
                                        <asp:LinkButton ID="lkbtnPrevious" runat="server" CssClass="pagingfirst" OnClick="lkbtnPrevious_Click">
                                            <asp:Literal ID="litPrevious" runat="server" Text="<%$ Resources:Resource, PREVIOUS%>"></asp:Literal></asp:LinkButton>
                                        &nbsp;|
                                        <asp:Label ID="lblCurrentPage" runat="server" Font-Bold="true"></asp:Label>
                                        &nbsp;|
                                        <asp:LinkButton ID="lkbtnNext" runat="server" CssClass="pagingfirst" OnClick="lkbtnNext_Click">
                                            <asp:Literal ID="litNext" runat="server" Text="<%$ Resources:Resource, NEXT%>"></asp:Literal></asp:LinkButton>
                                        &nbsp;|
                                        <asp:LinkButton ID="lkbtnLast" runat="server" CssClass="pagingfirst" OnClick="lkbtnLast_Click">
                                            <asp:Literal ID="litLast" runat="server" Text="<%$ Resources:Resource, LAST%>"></asp:Literal></asp:LinkButton>
                                        &nbsp;|<asp:Literal ID="litPageSize" runat="server" Text="<%$ Resources:Resource, PAGE_SIZE%>"></asp:Literal>
                                        <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" CssClass="paging-size"
                                            OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="consumer_total">
                                <div class="clear">
                                </div>
                            </div>
                            <asp:HiddenField ID="hfPageNo" runat="server" />
                            <asp:HiddenField ID="hfTotalRecords" runat="server" />
                            <asp:HiddenField ID="hfLastPage" runat="server" />
                            <asp:HiddenField ID="hfPageSize" runat="server" />
                            <div class="clear">
                                &nbsp;</div>
                            <div class="grid">
                                <asp:GridView ID="GvBillingMsg" Width="550px" runat="server" DataKeyNames="DistrictMessageId"
                                    AutoGenerateColumns="false" OnRowDeleting="GvBillingMsg_RowDeleting" EmptyDataText="No Records Found">
                                    <Columns>
                                        <asp:BoundField DataField="Message" HeaderText="<%$ Resources:Resource, HEADER_SPOTBILLINGMESSAGE%>" />
                                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkItemDelete" runat="server" CommandName="Delete">
                                               <img src="../images/delete.png" alt="Delete" title="Delete" /></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnViewGMsg" CssClass="calculate_but" runat="server" Visible="false"
                                Text="<%$ Resources:Resource, VIEW_GLOBALMESSAGE%>" OnClick="btnViewGMsg_Click" />
                            <asp:Button ID="btnPop" runat="server" Text="" Style="display: none" />
                            <asp:ModalPopupExtender ID="PopupViewMSG" runat="server" TargetControlID="btnPop"
                                PopupControlID="pnlViewMsg" BackgroundCssClass="popbg" CancelControlID="btnCancel">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="pnlViewMsg" runat="server" CssClass="editpopup_two" ScrollBars="Auto"
                                Style="display: none; overflow: auto; width: 50%; max-height: 400px;">
                                <div class="clear pad_10">
                                </div>
                                <div align="center" class="grid">
                                    <asp:GridView ID="GvViewMSG" CssClass="color" Width="100%" HorizontalAlign="Center"
                                        AlternatingRowStyle-CssClass="color" runat="server" DataKeyNames="GlobalMessageId"
                                        AutoGenerateColumns="false">
                                        <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                        <EmptyDataTemplate>
                                            No Records Founds.
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="<%$ Resources:Resource, GRID_SNO%>">
                                                <ItemTemplate>
                                                    <%#(Container.DataItemIndex+1)%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Message" HeaderText="<%$ Resources:Resource, HEADER_SPOTBILLINGMESSAGE%>"
                                                ItemStyle-HorizontalAlign="Center" />
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="<%$ Resources:Resource, GRID_ACTION%>">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkItemActivate" runat="server" CommandName="AddToDistrict" CommandArgument='<%#Eval("GlobalMessageId").ToString()+","+Eval("BillingTypeId").ToString()+","+Eval("Message").ToString() %>'
                                                        OnCommand="lnkItemActivate_Command">Add To District Messages</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                <div class="clear pad_5">
                                </div>
                                <asp:Button ID="btnCancel" runat="server" Style="margin-left: 575px; margin-top: 10px;"
                                    CssClass="cancel_btn" Text="<%$ Resources:Resource, CANCEL%>" />
                                <div class="clear pad_5">
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
                <%-- DeActive  Btn popup Start--%>
                <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                    TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnDeActivate" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="PanelDeActivate" runat="server" DefaultButton="btnDeActiveOk" CssClass="modalPopup"
                    Style="display: none; border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnDeActiveOk" runat="server" OnClick="btnDeActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                            <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- DeActive  popup Closed--%>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlBusinessUnit" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".rnkland").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".popbg").fadeOut("slow");
                $(".editpopup_two").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/PageValidations/AddDistrictlevelMessage.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
