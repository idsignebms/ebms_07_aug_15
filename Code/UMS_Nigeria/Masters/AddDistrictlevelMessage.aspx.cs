﻿
#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddDistrictlevelMessage
                     
 Developer        : Id075-RamaDevi M
 Creation Date    : 16-Mar-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using System.Xml;
using UMS_NigeriaBE;
using System.Data;
using Resources;
using System.Globalization;
using System.Threading;

namespace UMS_Nigeria.Masters
{
    public partial class AddDistrictlevelMessage : System.Web.UI.Page
    {
        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        GlobalMessagesBAL objGlobalMessagesBAL = new GlobalMessagesBAL();
        public int PageNum;
        public int PagespotNum;
        string Key = Resources.UMS_Resource.ADD_DISTRICTLEVELMESSAGE;
        CommonMethods _ObjCommonMethods = new CommonMethods();
        GlobalMessages _objGlobalMessege = new GlobalMessages();
        #endregion

        #region Properties
        private string FirstMessage
        {
            get
            { return txtMessage1.Text; }
            set { txtMessage1.Text = value; }
        }
        private string SecondMessage
        {
            get
            { return txtMessage2.Text; }
            set { txtMessage2.Text = value; }
        }
        private string ThirdMessage
        {
            get
            { return txtMessage3.Text; }
            set { txtMessage3.Text = value; }
        }
        public int PageSize
        {
            get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        public int PageSpotSize
        {
            get { return string.IsNullOrEmpty(ddlSpotPaze.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlSpotPaze.SelectedValue); }
        }
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.CssClass = lblMessage.Text = string.Empty;
            if (!IsPostBack)
            {
                string path = string.Empty;
                path = _ObjCommonMethods.GetPagePath(this.Request.Url);
                if (_ObjCommonMethods.IsAccess(path))
                {

                    //PageAccessPermissions();
                    hfspotPageNo.Value = hfPageNo.Value = Constants.pageNoValue.ToString();
                    //_ObjCommonMethods.BindDistricts(ddlDistrict, string.Empty, true);
                    _ObjCommonMethods.BindBusinessUnits(ddlBusinessUnit, string.Empty, true);
                    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    {
                        ddlBusinessUnit.SelectedIndex = ddlBusinessUnit.Items.IndexOf(ddlBusinessUnit.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                        ddlBusinessUnit.Enabled = false;
                        ddlBusinessUnit_SelectedIndexChanged(ddlBusinessUnit, new EventArgs());
                    }
                    _ObjCommonMethods.BindBillingTypes(rblMessageType, false);
                    btnViewGMsg.Visible = false;
                    BindSpotMessages(string.Empty);
                    BindManualMessages(string.Empty);
                    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    BindSpotPagingDropDown(Convert.ToInt32(hfspotTotalRecords.Value));
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    CreateSpotPagingDT(Convert.ToInt32(hfspotTotalRecords.Value));
                }
                else
                {
                    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //_objGlobalMessege.DistrictCode = ddlDistrict.SelectedItem.Value;
                _objGlobalMessege.BU_ID = ddlBusinessUnit.SelectedValue;
                _objGlobalMessege.BillingTypeId = Convert.ToInt32(rblMessageType.SelectedItem.Value);
                _objGlobalMessege.Message = FirstMessage + "|" + SecondMessage + "|" + ThirdMessage;
                _objGlobalMessege.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objGlobalMessege.IsGlobalMSG = 0;
                _objGlobalMessege.GlobalMessageId = 0;
                _objGlobalMessege = objiDsignBAL.DeserializeFromXml<GlobalMessages>(objGlobalMessagesBAL.DistrictlevelMessageActionBAL(objiDsignBAL.DoSerialize<GlobalMessages>(_objGlobalMessege)));
                if (_objGlobalMessege.ActiveStatus == 1)
                {
                    BindSpotMessages(string.Empty);
                    BindManualMessages(string.Empty);
                    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    BindSpotPagingDropDown(Convert.ToInt32(hfspotTotalRecords.Value));
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    CreateSpotPagingDT(Convert.ToInt32(hfspotTotalRecords.Value));
                    Message(UMS_Resource.GLOBALMESSAGE_INSERTSUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "Script", "javascript:Clearall();", true);
                }
                else
                {
                    Message(UMS_Resource.GLOBALMESSAGE_INSERTFAILED, UMS_Resource.MESSAGETYPE_ERROR);
                }
                upGV.Update(); upDDL.Update();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }
        }
        //private void PageAccessPermissions()
        //{
        //    try
        //    {
        //        if (_ObjCommonMethods.IsAccess(UMS_Resource.ADD_BUSINESS_UNITS_PAGE)) { lkbtnAddNewBusinessUnit.Visible = true; } else { lkbtnAddNewBusinessUnit.Visible = false; }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        protected void GvBillingMsg_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                hfTemp.Value = Convert.ToInt32(GvBillingMsg.DataKeys[e.RowIndex].Value).ToString();
                hfRecognize.Value = UMS_Resource.BILLING_MSG_DELETE;
                lblDeActiveMsg.Text = UMS_Resource.BILLING_MSG_DELETE_POPUP_TEXT;
                btnDeActiveOk.Text = UMS_Resource.YES;
                btnDeActiveCancel.Text = UMS_Resource.NO;
                mpeDeActive.Show();
                btnDeActiveOk.Focus();
            }
            catch (Exception ex)
            {
                Message(UMS_Resource.MESSAGETYPE_ERROR, ex.Message);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (hfRecognize.Value == UMS_Resource.BILLING_MSG_DELETE)
                {
                    _objGlobalMessege.DistrictId = Convert.ToInt32(hfTemp.Value);
                    _objGlobalMessege.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    _objGlobalMessege = objiDsignBAL.DeserializeFromXml<GlobalMessages>(objGlobalMessagesBAL.DeleteDisitrictlevelMessageBAL(objiDsignBAL.DoSerialize<GlobalMessages>(_objGlobalMessege)));
                    if (_objGlobalMessege.ActiveStatus == 1)
                    {
                        BindManualMessages(string.Empty);
                        Message(UMS_Resource.MESSAGE_DELETED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    }
                }
                else if (hfRecognize.Value == UMS_Resource.SPOT_MSG_DELETE)
                {
                    _objGlobalMessege.DistrictId = Convert.ToInt32(hfTemp.Value);
                    _objGlobalMessege.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    _objGlobalMessege = objiDsignBAL.DeserializeFromXml<GlobalMessages>(objGlobalMessagesBAL.DeleteDisitrictlevelMessageBAL(objiDsignBAL.DoSerialize<GlobalMessages>(_objGlobalMessege)));
                    if (_objGlobalMessege.ActiveStatus == 1)
                    {
                        BindSpotMessages(string.Empty);
                        Message(UMS_Resource.MESSAGE_DELETED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void GvSpotMSG_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                hfTemp.Value = Convert.ToInt32(GvSpotMSG.DataKeys[e.RowIndex].Value).ToString();
                hfRecognize.Value = UMS_Resource.SPOT_MSG_DELETE;
                lblDeActiveMsg.Text = UMS_Resource.SPOT_MSG_DELETE_POPUP_TEXT;
                btnDeActiveOk.Text = UMS_Resource.YES;
                btnDeActiveCancel.Text = UMS_Resource.NO;
                mpeDeActive.Show();
                btnDeActiveOk.Focus();

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        //protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    BindSpotMessages(ddlDistrict.SelectedItem.Value);
        //    BindManualMessages(string.Empty);
        //    if (ddlDistrict.SelectedIndex != 0)
        //    {
        //        btnViewGMsg.Visible = true;
        //    }
        //    else
        //    {
        //        btnViewGMsg.Visible = false;
        //    }

        //}
        protected void ddlBusinessUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlBusinessUnit.SelectedIndex > 0)
            {
                BindSpotMessages(ddlBusinessUnit.SelectedValue);
                BindManualMessages(string.Empty);
                BindGlobalMessages(string.Empty);
            }
            else
            {
                btnViewGMsg.Visible = false;
            }
            //if (ddlBusinessUnit.SelectedIndex != 0)
            //{
            //    btnViewGMsg.Visible = true;
            //}
            //else
            //{
            //    btnViewGMsg.Visible = false;
            //}
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            btnViewGMsg.Visible = false;
        }
        protected void btnViewGMsg_Click(object sender, EventArgs e)
        {
            BindGlobalMessages(ddlBusinessUnit.SelectedItem.Value);
            PopupViewMSG.Show();
            //BindGlobalMessages(ddlDistrict.SelectedItem.Value);



        }
        protected void lnkItemActivate_Command(object sender, CommandEventArgs e)
        {

            try
            {
                LinkButton lkActivate = (LinkButton)sender;
                string[] Commands = System.Text.RegularExpressions.Regex.Split(lkActivate.CommandArgument, ",");
                _objGlobalMessege.GlobalMessageId = Convert.ToInt32(Commands[0]);
                //_objGlobalMessege.DistrictCode = ddlDistrict.SelectedItem.Value;
                _objGlobalMessege.BU_ID = ddlBusinessUnit.SelectedValue;
                _objGlobalMessege.BillingTypeId = Convert.ToInt32(Commands[1]);
                _objGlobalMessege.Message = Commands[2];
                _objGlobalMessege.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objGlobalMessege.IsGlobalMSG = 1;

                _objGlobalMessege = objiDsignBAL.DeserializeFromXml<GlobalMessages>(objGlobalMessagesBAL.DistrictlevelMessageActionBAL(objiDsignBAL.DoSerialize<GlobalMessages>(_objGlobalMessege)));
                if (_objGlobalMessege.ActiveStatus == 1)
                {
                    //BindGlobalMessages(string.Empty);
                    BindSpotMessages(string.Empty);
                    BindManualMessages(string.Empty);
                    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    BindSpotPagingDropDown(Convert.ToInt32(hfspotTotalRecords.Value));
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    CreateSpotPagingDT(Convert.ToInt32(hfspotTotalRecords.Value));
                    Message(UMS_Resource.GLOBALMESSAGE_INSERTSUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                {
                    Message(UMS_Resource.GLOBALMESSAGE_INSERTFAILED, UMS_Resource.MESSAGETYPE_ERROR);
                }
                upGV.Update(); upDDL.Update();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            }

        }
        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                //BindManualMessages(ddlDistrict.SelectedItem.Value);
                BindManualMessages(ddlBusinessUnit.SelectedItem.Value);
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                //BindManualMessages(ddlDistrict.SelectedItem.Value);
                BindManualMessages(ddlBusinessUnit.SelectedItem.Value);
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                //BindManualMessages(ddlDistrict.SelectedItem.Value);
                BindManualMessages(ddlBusinessUnit.SelectedItem.Value);
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void lkbtnAddNewBusinessUnit_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_BUSINESS_UNITS_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                //BindManualMessages(ddlDistrict.SelectedItem.Value);
                BindManualMessages(ddlBusinessUnit.SelectedItem.Value);
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                //BindManualMessages(ddlDistrict.SelectedItem.Value);
                BindManualMessages(ddlBusinessUnit.SelectedItem.Value);
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void lnkspotfirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfspotPageNo.Value = Constants.pageNoValue.ToString();
                //BindSpotMessages(ddlDistrict.SelectedItem.Value);
                BindSpotMessages(ddlBusinessUnit.SelectedItem.Value);
                CreateSpotPagingDT(Convert.ToInt32(hfspotTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkspotprevious_Click(object sender, EventArgs e)
        {
            try
            {
                PagespotNum = Convert.ToInt32(lblSpotCurrentPage.Text.Trim());
                PagespotNum -= Constants.pageNoValue;
                hfspotPageNo.Value = PagespotNum.ToString();
                //BindSpotMessages(ddlDistrict.SelectedItem.Value);
                BindSpotMessages(ddlBusinessUnit.SelectedItem.Value);
                CreateSpotPagingDT(Convert.ToInt32(hfspotTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }


        protected void lnkspotnext_Click(object sender, EventArgs e)
        {
            try
            {
                PagespotNum = Convert.ToInt32(lblSpotCurrentPage.Text.Trim());
                PagespotNum += Constants.pageNoValue;
                hfspotPageNo.Value = PagespotNum.ToString();
                //BindSpotMessages(ddlDistrict.SelectedItem.Value);
                BindSpotMessages(ddlBusinessUnit.SelectedItem.Value);
                CreateSpotPagingDT(Convert.ToInt32(hfspotTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkspotlast_Click(object sender, EventArgs e)
        {
            try
            {
                hfspotPageNo.Value = hfspotLastPage.Value;
                //BindSpotMessages(ddlDistrict.SelectedItem.Value);
                BindSpotMessages(ddlBusinessUnit.SelectedItem.Value);
                CreateSpotPagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlSpotPaze_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfspotPageNo.Value = Constants.pageNoValue.ToString();
                //BindSpotMessages(ddlDistrict.SelectedItem.Value);
                BindSpotMessages(ddlBusinessUnit.SelectedItem.Value);
                hfspotPageSize.Value = ddlSpotPaze.SelectedItem.Text;
                CreateSpotPagingDT(Convert.ToInt32(hfspotTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods
        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                _ObjCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);

                //ddlPageSize.Items.Clear();
                //if (TotalRecords < Constants.PageSizeStarts)
                //{
                //    ListItem item = new ListItem(Constants.PageSizeStarts.ToString(), Constants.PageSizeStarts.ToString());
                //    ddlPageSize.Items.Add(item);
                //}
                //else
                //{
                //    int previousSize = Constants.PageSizeStarts;
                //    for (int i = Constants.PageSizeStarts; i <= TotalRecords; i = i + Constants.PageSizeIncrement)
                //    {
                //        ListItem item = new ListItem(i.ToString(), i.ToString());
                //        ddlPageSize.Items.Add(item);
                //        previousSize = i;
                //    }
                //    if (previousSize < TotalRecords)
                //    {
                //        ListItem item = new ListItem((previousSize + Constants.PageSizeIncrement).ToString(), (previousSize + Constants.PageSizeIncrement).ToString());
                //        ddlPageSize.Items.Add(item);
                //    }
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void BindSpotPagingDropDown(int TotalRecords)
        {
            try
            {
                ddlSpotPaze.Items.Clear();
                if (TotalRecords < Constants.PageSizeStarts)
                {
                    ListItem item = new ListItem(Constants.PageSizeStarts.ToString(), Constants.PageSizeStarts.ToString());
                    ddlSpotPaze.Items.Add(item);
                }
                else
                {
                    int previousSize = Constants.PageSizeStarts;
                    for (int i = Constants.PageSizeStarts; i <= TotalRecords; i = i + Constants.PageSizeIncrement)
                    {
                        ListItem item = new ListItem(i.ToString(), i.ToString());
                        ddlSpotPaze.Items.Add(item);
                        previousSize = i;
                    }
                    if (previousSize < TotalRecords)
                    {
                        ListItem item = new ListItem((previousSize + Constants.PageSizeIncrement).ToString(), (previousSize + Constants.PageSizeIncrement).ToString());
                        ddlSpotPaze.Items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));

                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                objiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void CreateSpotPagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSpotSize));

                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfspotPageNo.Value);
                lnkspotlast.CommandArgument = hfspotLastPage.Value = totalpages.ToString();
                objiDsignBAL.GridViewPaging(lnkspotnext, lnkspotprevious, lnkspotfirst, lnkspotlast, currentpage, TotalNoOfRecs);
                lblSpotCurrentPage.Text = hfspotPageNo.Value;
                if (totalpages == 1) { lnkspotfirst.ForeColor = lnkspotlast.ForeColor = lnkspotnext.ForeColor = lnkspotprevious.ForeColor = System.Drawing.Color.Gray; }
                if (string.Compare(hfspotPageNo.Value, hfspotLastPage.Value, true) == 0) { lnkspotnext.Enabled = lnkspotlast.Enabled = false; }
                else { lnkspotnext.Enabled = lnkspotlast.Enabled = true; }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void BindSpotMessages(string stateid)
        {
            try
            {

                //_objGlobalMessege.DistrictCode = ddlDistrict.SelectedItem.Value;
                _objGlobalMessege.BU_ID = ddlBusinessUnit.SelectedItem.Value;
                _objGlobalMessege.PageNo = Convert.ToInt32(hfspotPageNo.Value);
                _objGlobalMessege.PageSize = PageSpotSize;
                _objGlobalMessege.BillingTypeId = 1;
                GlobalMessagesListBE ObjGlobalMessageListBE = objiDsignBAL.DeserializeFromXml<GlobalMessagesListBE>(objGlobalMessagesBAL.GetDistrictlevelMessagesBAL(objiDsignBAL.DoSerialize<GlobalMessages>(_objGlobalMessege)));
                if (ObjGlobalMessageListBE.Items.Count > 0)
                {
                    divgrid1.Visible = divpaging1.Visible = true;

                    hfspotTotalRecords.Value = ObjGlobalMessageListBE.Items[0].TotalRecords.ToString();
                }
                else
                {
                    divgrid1.Visible = divpaging1.Visible = false;

                    hfspotTotalRecords.Value = ObjGlobalMessageListBE.Items.Count.ToString();
                    // ObjGlobalMessageListBE.Visible = divpaging.Visible = false;
                }
                GvSpotMSG.DataSource = objiDsignBAL.ConvertListToDataSet<GlobalMessages>(ObjGlobalMessageListBE.Items).Tables[0];
                GvSpotMSG.DataBind();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void BindManualMessages(string stateid)
        {
            try
            {

                //                _objGlobalMessege.DistrictCode = ddlDistrict.SelectedItem.Value;
                _objGlobalMessege.BU_ID = ddlBusinessUnit.SelectedItem.Value;
                _objGlobalMessege.PageNo = Convert.ToInt32(hfPageNo.Value);
                _objGlobalMessege.PageSize = PageSize;
                _objGlobalMessege.BillingTypeId = 2;
                GlobalMessagesListBE ObjGlobalMessageListBE = objiDsignBAL.DeserializeFromXml<GlobalMessagesListBE>(objGlobalMessagesBAL.GetDistrictlevelMessagesBAL(objiDsignBAL.DoSerialize<GlobalMessages>(_objGlobalMessege)));
                if (ObjGlobalMessageListBE.Items.Count > 0)
                {
                    divgrid.Visible = divpaging.Visible = true;
                    hfTotalRecords.Value = ObjGlobalMessageListBE.Items[0].TotalRecords.ToString();
                }
                else
                {
                    divgrid.Visible = divpaging.Visible = false;
                    hfTotalRecords.Value = ObjGlobalMessageListBE.Items.Count.ToString();
                    // ObjGlobalMessageListBE.Visible = divpaging.Visible = false;
                }
                GvBillingMsg.DataSource = objiDsignBAL.ConvertListToDataSet<GlobalMessages>(ObjGlobalMessageListBE.Items).Tables[0];
                GvBillingMsg.DataBind();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void BindGlobalMessages(string stateid)
        {
            try
            {

                //_objGlobalMessege.DistrictCode = ddlDistrict.SelectedItem.Value;
                _objGlobalMessege.BU_ID = ddlBusinessUnit.SelectedItem.Value;

                GlobalMessagesListBE ObjGlobalMessageListBE = objiDsignBAL.DeserializeFromXml<GlobalMessagesListBE>(objGlobalMessagesBAL.GetBillingMessagesBasedOnDistrictsBAL(objiDsignBAL.DoSerialize<GlobalMessages>(_objGlobalMessege)));
                if (ObjGlobalMessageListBE.Items.Count > 0)
                {
                    //divgrid.Visible = divpaging.Visible = divgrid1.Visible = divpaging1.Visible = true;
                    btnViewGMsg.Visible = true;
                }
                else
                {
                    //hfTotalRecords.Value = ObjGlobalMessageListBE.Items.Count.ToString();
                    // divgrid.Visible = divpaging.Visible = divgrid1.Visible = divpaging1.Visible = false;
                    btnViewGMsg.Visible = false;
                }
                GvViewMSG.DataSource = objiDsignBAL.ConvertListToDataSet<GlobalMessages>(ObjGlobalMessageListBE.Items).Tables[0];
                GvViewMSG.DataBind();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        protected override void InitializeCulture()
        {
            if (Session[UMS_Resource.CULTURE] != null)
            {
                string culture = Session[UMS_Resource.CULTURE].ToString();

                Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
            }
            base.InitializeCulture();
        }
        #endregion
    }
}