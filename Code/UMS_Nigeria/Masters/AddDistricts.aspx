﻿<%@ Page Title="::Add Districts::" Language="C#" MasterPageFile="~/MasterPages/EDMUMS.Master" Theme="Green"
    AutoEventWireup="true" CodeBehind="AddDistricts.aspx.cs" Inherits="UMS_Nigeria.Masters.AddDistricts" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="edmcontainer">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upAddDistricts" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <h3>
                    <asp:Literal ID="litAddDistrict" runat="server" Text="<%$ Resources:Resource, ADD_DISTRICT%>"></asp:Literal>
                </h3>
                <div class="clear">
                    &nbsp;</div>
                <div class="consumer_feild">
                    <div class="consume_name">
                        <asp:Literal ID="litCountryCode" runat="server" Text="<%$ Resources:Resource, COUNTRY_NAME%>"></asp:Literal><span>*</span>
                    </div>
                    <div class="consume_input">
                        <%--<asp:UpdatePanel ID="upnlCountryName" runat="server">
                        <ContentTemplate>--%>
                        <asp:DropDownList ID="ddlCountryName" AutoPostBack="true" onchange="DropDownlistOnChangelbl(this,'spanCountryCode','Country')"
                            runat="server" OnSelectedIndexChanged="ddlCountryCode_SelectedIndexChanged">
                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                        </asp:DropDownList>
                        <span id="spanCountryCode" class="spancolor"></span>
                        <%--</ContentTemplate>
                    </asp:UpdatePanel>--%>
                    </div>
                    <%--<div class="consume_input" style="width: 3px">
                        <asp:HyperLink ID="lkbtnAddNewCountry" NavigateUrl="~/Masters/AddCountries.aspx" runat="server"
                            Text="<%$ Resources:Resource, ADD%>"></asp:HyperLink>
                    </div>--%>
                    <div class="consume_name">
                        <asp:Literal ID="litStateCode" runat="server" Text="<%$ Resources:Resource, STATE_NAME%>"></asp:Literal><span>*</span>
                    </div>
                    <div class="consume_input" style="width: 174px">
                        <asp:DropDownList ID="ddlStateName" onchange="DropDownlistOnChangelbl(this,'spanStateCode',' State')"
                            runat="server">
                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                        </asp:DropDownList>
                        <span id="spanStateCode" class="spancolor"></span>
                    </div>
                    <%--<div class="consume_input" style="width: 7px">
                        <asp:HyperLink ID="lkbtnAddNewState" NavigateUrl="~/Masters/AddStates.aspx" runat="server"
                            Text="<%$ Resources:Resource, ADD%>"></asp:HyperLink>
                    </div>--%>
                    <div class="consume_name">
                        <asp:Literal ID="litDistrictName" runat="server" Text="<%$ Resources:Resource, DISTRICT_NAME%>"></asp:Literal><span>*</span>
                    </div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtDistrictName" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanDistrictName','District Name')"
                            MaxLength="50" placeholder="Enter District Name"></asp:TextBox>
                        <span id="spanDistrictName" class="spancolor"></span>
                    </div>
                    <div class="clear pad_10">
                    </div>
                    <div class="consume_name">
                        <asp:Literal ID="litDistrictCode" runat="server" Text="<%$ Resources:Resource, DISTRICT_CODE%>"></asp:Literal><span>*</span>
                    </div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtDistrictCode" runat="server" MaxLength="20" onblur="return TextBoxBlurValidationlbl(this,'spanDistrictCode','District Code')"
                            placeholder="Enter District Code"></asp:TextBox>
                        <span id="spanDistrictCode" class="spancolor"></span>
                    </div>
                    <div class="consume_name" style="margin-left: 13px;">
                        <asp:Literal ID="litNotes" runat="server" Text="<%$ Resources:Resource, NOTES%>"></asp:Literal>
                    </div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" placeholder="Enter Comments Here"></asp:TextBox>
                        <span id="span4" class="spancolor"></span>
                    </div>
                    <div class="clear pad_10">
                        <div style="margin-left: 409px;">
                            <asp:Button ID="btnSaveNext" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                CssClass="calculate_but" OnClick="btnSaveNext_Click" runat="server" />
                            <%--<asp:Button ID="btnUpdate" Visible="false" OnClientClick="return Validate();" Text="UPDATE"
                        CssClass="calculate_but" runat="server" OnClick="btnUpdate_Click" />--%>
                            <asp:Button ID="btnReset" Visible="false" Text="<%$ Resources:Resource, RESET%>"
                                CssClass="calculate_but" runat="server" OnClick="btnReset_Click" />
                        </div>
                    </div>
                </div>
                <%--Grid Starts Here--%>
                <div class="consumer_total">
                    <div class="pad_10">
                    </div>
                </div>
                <div class="consumer_total">
                    <div class="pad_10">
                    </div>
                </div>
                <div id="divgrid" class="grid" runat="server" style="width: 100% !important; float: left;">
                    <div id="divpaging" runat="server">
                        <div align="right" class="marg_20t" runat="server" id="divLinks">
                            <h3 class="gridhead" align="left">
                                <asp:Literal ID="litDistrictsList" runat="server" Text="<%$ Resources:Resource, GRID_DISTRICTS_LIST%>"></asp:Literal>
                            </h3>
                            <asp:LinkButton ID="lkbtnFirst" runat="server" CssClass="pagingfirst" OnClick="lkbtnFirst_Click">
                                <asp:Literal ID="litFirst" runat="server" Text="<%$ Resources:Resource, FIRST%>"></asp:Literal></asp:LinkButton>
                            &nbsp;|
                            <asp:LinkButton ID="lkbtnPrevious" runat="server" CssClass="pagingfirst" OnClick="lkbtnPrevious_Click">
                                <asp:Literal ID="litPrevious" runat="server" Text="<%$ Resources:Resource, PREVIOUS%>"></asp:Literal></asp:LinkButton>
                            &nbsp;|
                            <asp:Label ID="lblCurrentPage" runat="server" Font-Bold="true"></asp:Label>
                            &nbsp;|
                            <asp:LinkButton ID="lkbtnNext" runat="server" CssClass="pagingfirst" OnClick="lkbtnNext_Click">
                                <asp:Literal ID="litNext" runat="server" Text="<%$ Resources:Resource, NEXT%>"></asp:Literal></asp:LinkButton>
                            &nbsp;|
                            <asp:LinkButton ID="lkbtnLast" runat="server" CssClass="pagingfirst" OnClick="lkbtnLast_Click">
                                <asp:Literal ID="litLast" runat="server" Text="<%$ Resources:Resource, LAST%>"></asp:Literal></asp:LinkButton>
                            &nbsp;|<asp:Literal ID="litPageSize" runat="server" Text="<%$ Resources:Resource, PAGE_SIZE%>"></asp:Literal>
                            <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" CssClass="paging-size"
                                OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="consumer_total">
                    <div class="clear">
                    </div>
                </div>
                <div class="grid" id="divPrint" runat="server" align="center" style="overflow-x: auto;">
                    <asp:GridView ID="gvDistrictList" runat="server" AutoGenerateColumns="False" OnRowCommand="gvDistrictList_RowCommand"
                        AlternatingRowStyle-CssClass="color" OnRowDataBound="gvDistrictList_RowDataBound">
                        <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                        <EmptyDataTemplate>
                            There is no data.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <%--<%#Container.DataItemIndex+1%>--%>
                                    <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, DISTRICT_CODE%>" ItemStyle-Width="8%"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblDistrictCode" runat="server" Text='<%#Eval("DistrictCode") %>'></asp:Label>
                                    <asp:Label ID="lblIsActive" Visible="false" runat="server" Text='<%#Eval("IsActive") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, DISTRICT_NAME%> " ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblDistrictName" runat="server" Text='<%#Eval("DistrictName") %>'></asp:Label>
                                    <asp:TextBox ID="txtGridDistrictName" MaxLength="50" placeholder="Enter District Name"
                                        runat="server" Visible="false"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, NOTES%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblNotes" runat="server" Text='<%#Eval("Notes") %>'></asp:Label>
                                    <asp:TextBox ID="txtGridNotes" placeholder="Enter Comments" runat="server" Visible="false"
                                        TextMode="MultiLine"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, COUNTRY_NAME%>" ItemStyle-Width="8%"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblCountryCode" Visible="false" runat="server" Text='<%#Eval("CountryCode") %>'></asp:Label>
                                    <asp:DropDownList ID="ddlGridCountryName" OnSelectedIndexChanged="ddlGridCountryName_SelectedIndexChanged"
                                        runat="server" AutoPostBack="true" Visible="false">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblCountryName" runat="server" Text='<%#Eval("CountryName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, STATE_NAME%>" ItemStyle-Width="8%"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblStateCode" Visible="false" runat="server" Text='<%#Eval("StateCode") %>'></asp:Label>
                                    <asp:DropDownList ID="ddlGridStateName" runat="server" Visible="false">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblStateName" runat="server" Text='<%#Eval("StateName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%--<asp:LinkButton ID="lbtnEdit" runat="server" Text="Edit" CommandArgument='<%# Eval("DistrictCode") %>'
                                            OnClick="lkbtnEdit_Click">
                                        <img src="../images/Edit.png"  style="padding:3px; width:20px; height:20px" alt="Edit"/></asp:LinkButton>
                                        <asp:LinkButton ID="lbtnDelete" runat="server" Text="Delete" OnClick="lkbtnDelete_Click"
                                            CommandArgument='<%# Eval("DistrictCode") %>' OnClientClick="return confirm('Are you sure you want to DeActivate this District?')"> 
                                            <img src="../images/Dustbin.png" style="padding:3px; width:20px; height:20px" alt="Delete" /></asp:LinkButton>--%>
                                    <asp:LinkButton ID="lkbtnDistrictEdit" runat="server" ToolTip="Edit" Text="Edit"
                                        CommandName="EditDistrict">
                                        <img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                                    <asp:LinkButton ID="lkbtnDistrictUpdate" Visible="false" runat="server" ToolTip="Update"
                                        CommandName="UpdateDistrict"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                    <asp:LinkButton ID="lkbtnDistrictCancel" Visible="false" runat="server" ToolTip="Cancel"
                                        CommandName="CancelDistrict"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                    <asp:LinkButton ID="lkbtnDistrictActive" Visible="false" runat="server" ToolTip="Activate District"
                                        CommandName="ActiveDistrict"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                    <asp:LinkButton ID="lkbtnDistrictDeActive" runat="server" ToolTip="DeActivate District"
                                        CommandName="DeActiveDistrict"><img src="../images/Deactivate.gif" alt="DeActive" /></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <%--Grid Endss Here--%>
                <asp:HiddenField ID="hfCountryCode" runat="server" />
                <asp:HiddenField ID="hfDistrictName" runat="server" />
                <asp:HiddenField ID="hfStateCode" runat="server" />
                <asp:HiddenField ID="hfDistrictCode" runat="server" />
                <asp:HiddenField ID="hfPageNo" runat="server" />
                <asp:HiddenField ID="hfTotalRecords" runat="server" />
                <asp:HiddenField ID="hfLastPage" runat="server" />
                <asp:HiddenField ID="hfPageSize" runat="server" />
                <div id="hidepopup">
                    <%-- Active Btn popup Start--%>
                    <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                        TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelActivate" runat="server" DefaultButton="btnActiveOk" CssClass="modalPopup"
                        Style="display: none; border-color: #639B00;">
                        <div class="popheader" style="background-color: #639B00;">
                            <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                        </div>
                        <br />
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnActiveOk" runat="server" OnClick="btnActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Active  Btn popup Closed--%>
                    <%-- DeActive  Btn popup Start--%>
                    <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                        TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnDeActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelDeActivate" runat="server" DefaultButton="btnDeActiveOk" CssClass="modalPopup"
                        Style="display: none; border-color: #639B00;">
                        <div class="popheader" style="background-color: red;">
                            <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnDeActiveOk" runat="server" OnClick="btnDeActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- DeActive  popup Closed--%>
                    <%-- Grid Alert popup Start--%>
                    <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                        TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                        <div class="popheader popheaderlblred">
                            <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Grid Alert popup Closed--%>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/PageValidations/AddDistricts.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
