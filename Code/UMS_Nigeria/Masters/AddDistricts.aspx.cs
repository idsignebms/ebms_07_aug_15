﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddBusinessUnits
                     
 Developer        : id065-Bhimaraju V
 Creation Date    : 15-Feb-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBE;
using System.Xml;
using System.Drawing;
using UMS_NigeriaBAL;
using Resources;
using System.Data;
using System.Threading;
using System.Globalization;
using UMS_NigriaDAL;

namespace UMS_Nigeria.Masters
{
    public partial class AddDistricts : System.Web.UI.Page
    {

        #region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        MastersBAL _ObjMastersBAL = new MastersBAL();
        UMS_Service _ObjUMS_Service = new UMS_Service();
        public int PageNum;
        XmlDocument xml = null;
        string Key = "AddDistricts";
        #endregion

        #region Properties

        public int PageSize
        {
            get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }

        private string DistrictName
        {
            get { return txtDistrictName.Text.Trim(); }
            set { txtDistrictName.Text = value; }
        }

        private string DistrictCode
        {
            get { return txtDistrictCode.Text.Trim(); }
            set { txtDistrictCode.Text = value; }
        }

        private string Notes
        {
            get { return txtNotes.Text.Trim(); }
            set { txtNotes.Text = value; }
        }

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                string path = string.Empty;
                path = _ObjCommonMethods.GetPagePath(this.Request.Url);
                if (_ObjCommonMethods.IsAccess(path))
                {

                    //PageAccessPermissions();
                    //Session[SeaWaysMIS_Resources.SESSION_PAGENAME] = ConstantsBE.NewCountryPageName;
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    BindGridDistrictsList();
                    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    _ObjCommonMethods.BindCountries(ddlCountryName, true, true);
                    _ObjCommonMethods.BindStates(ddlStateName, string.Empty, true);
                    if ((!string.IsNullOrEmpty(Request.QueryString[UMS_Resource.QSTR_COUNTRY_CODE])) && (!string.IsNullOrEmpty(Request.QueryString[UMS_Resource.QSTR_STATE_CODE])))
                    {

                        ddlCountryName.SelectedValue = _ObjiDsignBAL.Decrypt(Request.QueryString[UMS_Resource.QSTR_COUNTRY_CODE]).ToString();
                        //ddlCountryName.SelectedIndex = ddlCountryName.Items.IndexOf(ddlCountryName.Items.FindByValue(_ObjiDsignBAL.Decrypt(Request.QueryString[UMS_Resource.QSTR_COUNTRY_CODE]).ToString()));

                        //_ObjCommonMethods.BindCountries(ddlCountryName);
                        _ObjCommonMethods.BindStates(ddlStateName, ddlCountryName.SelectedValue, true);
                        ddlStateName.SelectedValue = _ObjiDsignBAL.Decrypt(Request.QueryString[UMS_Resource.QSTR_STATE_CODE]).ToString();
                        //ddlStateName.SelectedIndex = ddlStateName.Items.IndexOf(ddlStateName.Items.FindByValue(_ObjiDsignBAL.Decrypt(Request.QueryString[UMS_Resource.QSTR_STATE_CODE]).ToString()));

                    }
                }
                else
                {
                    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE, false);
                }
            }
        }

        //private void PageAccessPermissions()
        //{
        //    try
        //    {
        //        if (_ObjCommonMethods.IsAccess(UMS_Resource.ADD_BUSINESS_UNITS_PAGE)) { lkbtnAddNewState.Visible = true; } else { lkbtnAddNewState.Visible = false; }
        //        if (_ObjCommonMethods.IsAccess(UMS_Resource.ADD_BUSINESS_UNITS_PAGE)) { lkbtnAddNewCountry.Visible = true; } else { lkbtnAddNewCountry.Visible = false; }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridDistrictsList();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridDistrictsList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridDistrictsList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridDistrictsList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                BindGridDistrictsList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);

        }
        protected void btnSaveNext_Click(object sender, EventArgs e)
        {
            try
            {
                InsertDistrictsDetails();

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnAddNewCountry_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_COUNTRIES_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnAddNewState_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_STATES_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                btnSaveNext.Visible = txtDistrictCode.Enabled = true;
                //btnUpdate.Visible = 
                btnReset.Visible = false;
                txtDistrictCode.Text = txtDistrictName.Text = txtNotes.Text = string.Empty;
                ddlCountryName.SelectedIndex = ddlStateName.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //protected void btnUpdate_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        UpdateStateDetails();
        //        btnSaveNext.Visible = txtDistrictCode.Enabled = true;
        //        btnUpdate.Visible = btnReset.Visible = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message,UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnDelete_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        LinkButton lbtnDelete = (LinkButton)sender;
        //        MastersBE _ObjMastersBE = new MastersBE();
        //        _ObjMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
        //        _ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(_ObjUMS_Service.DeleteDistrict(lbtnDelete.CommandArgument, _ObjMastersBE.ModifiedBy));

        //        if (_ObjMastersBE.IsSuccess > 0)
        //        {
        //            BindGridDistrictsList();
        //            BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
        //            CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //            _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_SUCCESS, UMS_Resource.DISTRICT_DETAILS_DEACTIVATE_SUCCESS, pnlMessage, lblMessage);
        //        }
        //        else
        //            _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.DISTRICT_DETAILS_DEACTIVATE_FAIL, pnlMessage, lblMessage);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message,UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnEdit_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        btnSaveNext.Visible = txtDistrictCode.Enabled = false;
        //        btnUpdate.Visible = btnReset.Visible = true;

        //        LinkButton lkbtnEdit = (LinkButton)sender;
        //        Label lblDistrictCode = (Label)lkbtnEdit.Parent.NamingContainer.FindControl("lblDistrictCode");
        //        Label lblDistrictName = (Label)lkbtnEdit.Parent.NamingContainer.FindControl("lblDistrictName");
        //        Label lblNotes = (Label)lkbtnEdit.Parent.NamingContainer.FindControl("lblNotes");
        //        Label lblCountryCode = (Label)lkbtnEdit.Parent.NamingContainer.FindControl("lblCountryCode");
        //        Label lblStateCode = (Label)lkbtnEdit.Parent.NamingContainer.FindControl("lblStateCode");
        //        txtDistrictCode.Text = lblDistrictCode.Text;
        //        txtDistrictName.Text = lblDistrictName.Text;
        //        ddlStateName.SelectedValue = lblStateCode.Text;
        //        ddlCountryName.SelectedValue = lblCountryCode.Text;
        //        txtNotes.Text = lblNotes.Text.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message,UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //} 

        protected void gvDistrictList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnDistrictUpdate = (LinkButton)e.Row.FindControl("lkbtnDistrictUpdate");
                    TextBox txtGridDistrictName = (TextBox)e.Row.FindControl("txtGridDistrictName");
                    DropDownList ddlGridCountryName = (DropDownList)e.Row.FindControl("ddlGridCountryName");
                    DropDownList ddlGridStateName = (DropDownList)e.Row.FindControl("ddlGridStateName");

                    lkbtnDistrictUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridDistrictName.ClientID + "','" + ddlGridCountryName.ClientID + "','" + ddlGridStateName.ClientID + "')");

                    Label lblIsActive = (Label)e.Row.FindControl("lblIsActive");


                    if ((!Convert.ToBoolean(lblIsActive.Text)))
                    {
                        e.Row.FindControl("lkbtnDistrictDeActive").Visible = e.Row.FindControl("lkbtnDistrictEdit").Visible = false;
                        e.Row.FindControl("lkbtnDistrictActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDistrictDeActive").Visible = e.Row.FindControl("lkbtnDistrictEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvDistrictList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                MastersListBE _ObjMastersListBE = new MastersListBE();
                MastersBE _ObjMastersBE = new MastersBE();
                DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();


                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridDistrictName = (TextBox)row.FindControl("txtGridDistrictName");
                TextBox txtGridNotes = (TextBox)row.FindControl("txtGridNotes");

                DropDownList ddlGridCountryName = (DropDownList)row.FindControl("ddlGridCountryName");
                DropDownList ddlGridStateName = (DropDownList)row.FindControl("ddlGridStateName");

                Label lblDistrictCode = (Label)row.FindControl("lblDistrictCode");
                Label lblDistrictName = (Label)row.FindControl("lblDistrictName");
                Label lblCountryCode = (Label)row.FindControl("lblCountryCode");
                Label lblStateCode = (Label)row.FindControl("lblStateCode");
                Label lblCountryName = (Label)row.FindControl("lblCountryName");
                Label lblStateName = (Label)row.FindControl("lblStateName");
                Label lblNotes = (Label)row.FindControl("lblNotes");
                switch (e.CommandName.ToUpper())
                {
                    case "EDITDISTRICT":
                        lblCountryName.Visible = lblStateName.Visible = lblDistrictName.Visible = lblNotes.Visible = lblCountryCode.Visible = lblStateCode.Visible = false;
                        txtGridDistrictName.Visible = ddlGridStateName.Visible = ddlGridCountryName.Visible = txtGridNotes.Visible = true;
                        txtGridDistrictName.Text = lblDistrictName.Text;
                        _ObjCommonMethods.BindCountries(ddlGridCountryName, true, true);
                        ddlGridCountryName.SelectedIndex = ddlGridCountryName.Items.IndexOf(ddlGridCountryName.Items.FindByValue(lblCountryCode.Text));
                        _ObjCommonMethods.BindStates(ddlGridStateName, lblCountryCode.Text, true);
                        ddlGridStateName.SelectedIndex = ddlGridStateName.Items.IndexOf(ddlGridStateName.Items.FindByValue(lblStateCode.Text));
                        txtGridNotes.Text = _ObjiDsignBAL.ReplaceNewLines(lblNotes.Text, false);
                        row.FindControl("lkbtnDistrictCancel").Visible = row.FindControl("lkbtnDistrictUpdate").Visible = true;
                        row.FindControl("lkbtnDistrictDeActive").Visible = row.FindControl("lkbtnDistrictEdit").Visible = false;
                        break;
                    case "CANCELDISTRICT":
                        row.FindControl("lkbtnDistrictDeActive").Visible = row.FindControl("lkbtnDistrictEdit").Visible = true;
                        row.FindControl("lkbtnDistrictCancel").Visible = row.FindControl("lkbtnDistrictUpdate").Visible = false;
                        txtGridDistrictName.Visible = ddlGridStateName.Visible = ddlGridCountryName.Visible = txtGridNotes.Visible = false;
                        lblCountryName.Visible = lblStateName.Visible = lblDistrictName.Visible = lblNotes.Visible = true;
                        break;
                    case "UPDATEDISTRICT":
                        //xml = _ObjUMS_Service.UpdateDistrict(txtGridDistrictName.Text, lblDistrictCode.Text, ddlGridCountryName.SelectedValue, ddlGridStateName.SelectedValue, _ObjiDsignBAL.ReplaceNewLines(txtGridNotes.Text, true));
                        _ObjMastersBE.DistrictName = txtGridDistrictName.Text;
                        _ObjMastersBE.DistrictCode = lblDistrictCode.Text;
                        _ObjMastersBE.CountryCode = ddlGridCountryName.SelectedValue;
                        _ObjMastersBE.StateCode = ddlGridStateName.SelectedValue;
                        _ObjMastersBE.Notes = _ObjiDsignBAL.ReplaceNewLines(txtGridNotes.Text, true);
                        _ObjMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //xml = _ObjMastersBAL.Districts(_ObjMastersBE, Statement.Update);
                        _ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                        if (Convert.ToBoolean(_ObjMastersBE.IsSuccess))
                        {
                            BindGridDistrictsList();
                            BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            Message(UMS_Resource.DISTRICT_DETAILS_UPDATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                            btnReset_Click(null, null);
                        }
                        else
                            Message(UMS_Resource.DISTRICT_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "ACTIVEDISTRICT":
                        mpeActivate.Show();
                        hfDistrictCode.Value = lblDistrictCode.Text;
                        lblActiveMsg.Text = UMS_Resource.ACTIVE_DISTRICT_POPUP_TEXT;
                        btnActiveOk.Focus();
                        //_ObjMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //_ObjUMS_Service.DeleteActiveDistrict(1, lblDistrictCode.Text)
                        //_ObjMastersBE.DistrictCode = lblDistrictCode.Text;
                        //_ObjMastersBE.IsActive = Convert.ToBoolean(1);
                        //_ObjMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();

                        //_ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(_ObjMastersBAL.Districts(_ObjMastersBE, Statement.Delete));

                        //if (Convert.ToBoolean(_ObjMastersBE.IsSuccess))
                        //{
                        //    BindGridDistrictsList();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message( UMS_Resource.DISTRICT_DETAILS_ACTIVATE_SUCCESS,UMS_Resource.MESSAGETYPE_SUCCESS);
                        //}
                        //else
                        //    Message(UMS_Resource.DISTRICT_DETAILS_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                        break;
                    case "DEACTIVEDISTRICT":
                        mpeDeActive.Show();
                        hfDistrictCode.Value = lblDistrictCode.Text;
                        lblDeActiveMsg.Text = UMS_Resource.DEACTIVE_DISTRICT_POPUP_TEXT;
                        btnDeActiveOk.Focus();
                        //_ObjMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //_ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(_ObjUMS_Service.DeleteActiveDistrict(0, lblDistrictCode.Text));

                        //_ObjMastersBE.DistrictCode = lblDistrictCode.Text;
                        //_ObjMastersBE.IsActive = Convert.ToBoolean(0);
                        //_ObjMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();

                        //_ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(_ObjMastersBAL.Districts(_ObjMastersBE, Statement.Delete));


                        //if (Convert.ToBoolean(_ObjMastersBE.IsSuccess))
                        //{
                        //    BindGridDistrictsList();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message( UMS_Resource.DISTRICT_DETAILS_DEACTIVATE_SUCCESS,UMS_Resource.MESSAGETYPE_SUCCESS);
                        //}
                        //else
                        //    Message(UMS_Resource.DISTRICT_DETAILS_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE _ObjMastersBE = new MastersBE();
                _ObjMastersBE.DistrictCode = hfDistrictCode.Value;
                _ObjMastersBE.IsActive = Convert.ToBoolean(1);
                _ObjMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();

                //_ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(_ObjMastersBAL.Districts(_ObjMastersBE, Statement.Delete));

                if (Convert.ToBoolean(_ObjMastersBE.IsSuccess))
                {
                    BindGridDistrictsList();
                    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.DISTRICT_DETAILS_ACTIVATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                    Message(UMS_Resource.DISTRICT_DETAILS_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE _ObjMastersBE = new MastersBE();
                _ObjMastersBE.DistrictCode = hfDistrictCode.Value;
                _ObjMastersBE.IsActive = Convert.ToBoolean(0);
                _ObjMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();

                //_ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(_ObjMastersBAL.Districts(_ObjMastersBE, Statement.Delete));


                if (Convert.ToBoolean(_ObjMastersBE.IsSuccess))
                {
                    BindGridDistrictsList();
                    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.DISTRICT_DETAILS_DEACTIVATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                    Message(UMS_Resource.DISTRICT_DETAILS_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlCountryCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlStateName.Items.Clear();
                MastersListBE _ObjMastersListBE = new MastersListBE();
                MastersBE _ObjMastersBE = new MastersBE();
                _ObjMastersBE.CountryCode = ddlCountryName.SelectedValue;

                //_ObjMastersListBE = _ObjiDsignBAL.DeserializeFromXml<MastersListBE>(_ObjUMS_Service.GetStatesByCountryCode(_ObjMastersBE.CountryCode));
                _ObjMastersListBE = _ObjiDsignBAL.DeserializeFromXml<MastersListBE>(_ObjMastersBAL.Get(_ObjMastersBE, ReturnType.Single));
                DataSet ds = new DataSet();
                ds = _ObjiDsignBAL.ConvertListToDataSet<MastersBE>(_ObjMastersListBE.Items);
                _ObjiDsignBAL.FillDropDownList(ds.Tables[0], ddlStateName, "StateName", "StateCode", UMS_Resource.DROPDOWN_SELECT, true);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlGridCountryName_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlGridCountryName = (DropDownList)sender;
            DropDownList ddlGridState = (DropDownList)ddlGridCountryName.NamingContainer.FindControl("ddlGridStateName");
            ddlGridState.Items.Clear();
            MastersListBE _ObjMastersListBE = new MastersListBE();
            MastersBE _ObjMastersBE = new MastersBE();
            _ObjMastersBE.CountryCode = ddlGridCountryName.SelectedValue;
            //_ObjMastersListBE = _ObjiDsignBAL.DeserializeFromXml<MastersListBE>(_ObjUMS_Service.GetStatesByCountryCode(_ObjMastersBE.CountryCode));
            _ObjMastersListBE = _ObjiDsignBAL.DeserializeFromXml<MastersListBE>(_ObjMastersBAL.Get(_ObjMastersBE, ReturnType.Single));
            DataSet ds = new DataSet();
            ds = _ObjiDsignBAL.ConvertListToDataSet<MastersBE>(_ObjMastersListBE.Items);
            _ObjiDsignBAL.FillDropDownList(ds.Tables[0], ddlGridState, "StateName", "StateCode", UMS_Resource.DROPDOWN_SELECT, true);
        }

        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                _ObjCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);

                //ddlPageSize.Items.Clear();
                //if (TotalRecords < Constants.PageSizeStarts)
                //{
                //    ListItem item = new ListItem(Constants.PageSizeStarts.ToString(), Constants.PageSizeStarts.ToString());
                //    ddlPageSize.Items.Add(item);
                //}
                //else
                //{
                //    int previousSize = Constants.PageSizeStarts;
                //    for (int i = Constants.PageSizeStarts; i <= TotalRecords; i = i + Constants.PageSizeIncrement)
                //    {
                //        ListItem item = new ListItem(i.ToString(), i.ToString());
                //        ddlPageSize.Items.Add(item);
                //        previousSize = i;
                //    }
                //    if (previousSize < TotalRecords)
                //    {
                //        ListItem item = new ListItem((previousSize + Constants.PageSizeIncrement).ToString(), (previousSize + Constants.PageSizeIncrement).ToString());
                //        ddlPageSize.Items.Add(item);
                //    }
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                _ObjiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindGridDistrictsList()
        {
            try
            {
                MastersListBE _objMastersListBE = new MastersListBE();
                MastersBE _ObjMastersBE = new MastersBE();
                XmlDocument resultedXml = new XmlDocument();
                //resultedXml = _ObjUMS_Service.GetActiveDistrictsList(Convert.ToInt32(hfPageNo.Value), PageSize);

                _ObjMastersBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                _ObjMastersBE.PageSize = PageSize;
                resultedXml = _ObjMastersBAL.Get(_ObjMastersBE, ReturnType.Double);
                _objMastersListBE = _ObjiDsignBAL.DeserializeFromXml<MastersListBE>(resultedXml);
                if (_objMastersListBE.Items.Count > 0)
                {
                    divgrid.Visible = divpaging.Visible = true;
                    hfTotalRecords.Value = _objMastersListBE.Items[0].TotalRecords.ToString();
                    gvDistrictList.DataSource = _objMastersListBE.Items;
                    gvDistrictList.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = _objMastersListBE.Items.Count.ToString();
                    divgrid.Visible = divpaging.Visible = false;
                    gvDistrictList.DataSource = new DataTable();
                    gvDistrictList.DataBind();
                }

                
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void InsertDistrictsDetails()
        {
            try
            {
                MastersBE _ObjMastersBE = new MastersBE();
                //xml = _ObjUMS_Service.Districts(DistrictName, DistrictCode, ddlCountryName.SelectedValue, ddlStateName.SelectedValue, _ObjiDsignBAL.ReplaceNewLines(Notes, true));
                _ObjMastersBE.DistrictName = DistrictName;
                _ObjMastersBE.DistrictCode = DistrictCode;
                _ObjMastersBE.CountryCode = ddlCountryName.SelectedValue;
                _ObjMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _ObjMastersBE.StateCode = ddlStateName.SelectedValue;
                _ObjMastersBE.Notes = _ObjiDsignBAL.ReplaceNewLines(Notes, true);
                //xml = _ObjMastersBAL.Districts(_ObjMastersBE, Statement.Insert);

                _ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (Convert.ToBoolean(_ObjMastersBE.IsSuccess))
                {
                    BindGridDistrictsList();
                    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.DISTRICT_DETAILS_INSERT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    btnReset_Click(null, null);
                }
                else if (_ObjMastersBE.IsDistrictCodeExists)
                {
                    Message(UMS_Resource.DISTRICT_CODE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (_ObjMastersBE.IsDistrictNameExists)
                {
                    Message(UMS_Resource.DISTRICT_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void BindddlStates()
        {
            try
            {
                MastersListBE _ObjMastersListBE = new MastersListBE();
                MastersBE _ObjMastersBE = new MastersBE();
                _ObjMastersListBE = _ObjiDsignBAL.DeserializeFromXml<MastersListBE>(_ObjMastersBAL.GetStates(ReturnType.Get));
                if (_ObjMastersListBE.Items.Count > 0)
                {
                    DataSet ds = new DataSet();
                    ds = _ObjiDsignBAL.ConvertListToDataSet<MastersBE>(_ObjMastersListBE.Items);
                    _ObjiDsignBAL.FillDropDownList(ds.Tables[0], ddlStateName, "StateName", "StateCode", "--Select--", true);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void BindddlCountries()
        {
            try
            {
                MastersListBE _ObjMastersListBE = new MastersListBE();
                MastersBE _ObjMastersBE = new MastersBE();
                _ObjMastersListBE = _ObjiDsignBAL.DeserializeFromXml<MastersListBE>(_ObjMastersBAL.GetCountries(ReturnType.Get));
                if (_ObjMastersListBE.Items.Count > 0)
                {
                    DataSet ds = new DataSet();
                    ds = _ObjiDsignBAL.ConvertListToDataSet<MastersBE>(_ObjMastersListBE.Items);
                    _ObjiDsignBAL.FillDropDownList(ds.Tables[0], ddlCountryName, "CountryName", "CountryCode", "--Select--", true);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //protected void UpdateStateDetails()
        //{
        //    try
        //    {
        //        MastersBE _ObjMastersBE = new MastersBE();
        //        xml = _ObjUMS_Service.UpdateDistrict(this.DistrictName, this.DistrictCode, ddlCountryName.SelectedValue, ddlStateName.SelectedValue, _ObjiDsignBAL.ReplaceNewLines(this.Notes, true));
        //        _ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(xml);

        //        if (_ObjMastersBE.IsSuccess == 1)
        //        {
        //            BindGridDistrictsList();
        //            BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
        //            CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //            _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_SUCCESS, UMS_Resource.DISTRICT_DETAILS_UPDATE_SUCCESS, pnlMessage, lblMessage);
        //            btnReset_Click(null, null);
        //        }
        //        else
        //            _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.DISTRICT_EXISTS, pnlMessage, lblMessage);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message,UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}