﻿<%@ Page Title=":: Add Global Messages ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master" Theme="Green"
    AutoEventWireup="true" CodeBehind="AddGlobalMessage.aspx.cs" Inherits="UMS_Nigeria.Messages.AddGlobalMessage" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upAgencies" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <asp:UpdatePanel ID="upAddAgency" runat="server">
                    <ContentTemplate>
                        <div class="inner-sec">
                            <asp:Panel ID="pnlMessage" runat="server">
                                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                            </asp:Panel>
                            <div class="text_total">
                                <div class="text-heading">
                                    <asp:Literal ID="litBillmsgs" runat="server" Text="<%$ Resources:Resource, BILLING_MESSAGES%>"></asp:Literal>
                                </div>
                                <div class="star_text">
                                    <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="dot-line">
                            </div>
                            <div class="inner-box">
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litAgencyName" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                                            <span class="span_star">*</span></label>
                                        <div class="space">
                                        </div>
                                        <asp:DropDownList ID="ddlBusinessUnit" TabIndex="1" runat="server" CssClass="text-box select_box">
                                            <asp:ListItem Value="" Text="ALL"></asp:ListItem>
                                        </asp:DropDownList>
                                        <span id="spanddlBusinessUnit" class="spancolor"></span>
                                    </div>
                                    <div class="text-inner">
                                        <asp:DataList runat="server" ID="dtlBillingTypes" RepeatDirection="Vertical" RepeatColumns="1"
                                            OnItemDataBound="dtlBillingTypes_ItemDataBound">
                                            <ItemTemplate>
                                                <label for="name">
                                                    <asp:LinkButton ID="lnkMessageId" CommandArgument='<%# Eval("MessageId") %>' runat="server"
                                                        Text='<%# Eval("MessageType") %>' Enabled="false" Style="text-decoration: none;
                                                        color: #426700;"></asp:LinkButton>
                                                </label>
                                                <div class="space">
                                                </div>
                                                <asp:TextBox ID="txtBillingType" TabIndex="2" runat="server" TextMode="MultiLine" style="min-height: 60px;"
                                                    CssClass="text-box" placeholder='<%# "Enter "+ Eval("MessageType")+" Message." %>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </div>
                                    <div class="box_totalThree_b">
                                        <asp:Button ID="btnSave" TabIndex="3" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                            CssClass="box_s" runat="server" OnClick="btnSave_Click" />
                                    </div>
                                </div>
                            </div>
                            <div style="clear: both;">
                            </div>
                            <div class="grid_middle" style="margin: 17px 0 0 33px;">
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal></label>
                                        <asp:DropDownList ID="ddlBUSearch" AutoPostBack="true" OnSelectedIndexChanged="ddlBUSearch_SelectedIndexChanged"
                                            runat="server">
                                            <asp:ListItem Value="" Text="ALL"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="grid_boxes">
                                    <div class="grid_pagingTwo_top">
                                        <div class="paging_top_title">
                                            <asp:Literal ID="litCountriesList" runat="server" Text="<%$ Resources:Resource, BILLING_MESSAGES%>"></asp:Literal>
                                        </div>
                                        <div class="paging_top_rightTwo_content" id="divpaging" runat="server">
                                            <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="grid_tb" id="divgrid" runat="server">
                                    <asp:GridView ID="gvMessages" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="grid_tb_hd "
                                        OnRowDataBound="gvMessages_RowDataBound" OnRowCommand="gvMessages_RowCommand">
                                        <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                        <EmptyDataTemplate>
                                            There is no messages.
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                                HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <%--<%#Container.DataItemIndex+1%>--%>
                                                    <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                                    <asp:Label ID="lblActiveStatusId" Visible="false" runat="server" Text='<%#Eval("ActiveStatusId") %>'></asp:Label>
                                                    <asp:Label ID="lblGlobalMessageId" Visible="false" runat="server" Text='<%#Eval("GlobalMessageId")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, BUSINESS_UNIT_NAME%>" ItemStyle-Width="8%"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBU_ID" Visible="false" runat="server" Text='<%#Eval("BU_ID") %>'></asp:Label>
                                                    <asp:Label ID="lblBUName" runat="server" Text='<%#Eval("BusinessUnitName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, BILLING_MESSAGES%>" ItemStyle-Width="10%"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBillingTypeId" Visible="false" runat="server" Text='<%#Eval("BillingTypeId") %>'></asp:Label>
                                                    <asp:Label ID="lblBillingType" runat="server" Text='<%#Eval("BillingType") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, MESSAGES%>" ItemStyle-Width="10%"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMessage" runat="server" Text='<%#Eval("Message") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="10%"
                                                ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lkbtnActive" Visible="false" runat="server" ToolTip="Activate Msg"
                                                        CommandName="ActiveMsg"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                                    <asp:LinkButton ID="lkbtnDeActive" runat="server" ToolTip="DeActivate Msg" CommandName="DeActiveMsg"><img src="../images/Deactivate.gif" alt="DeActive" /></asp:LinkButton>
                                                    <asp:LinkButton ID="lkbtnDelete" runat="server" ToolTip="Delete" CommandName="DeleteMsg"><img src="../images/delete.png" alt="Delete" /></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <%--<div class="page_na">
                                    <uc2:UCPagingV2 ID="UCPagingV2" runat="server" />
                                </div>--%>
                                    <asp:HiddenField ID="hfPageSize" runat="server" />
                                    <asp:HiddenField ID="hfPageNo" runat="server" Value="1" />
                                    <asp:HiddenField ID="hfLastPage" runat="server" />
                                    <asp:HiddenField ID="hfcount" runat="server" />
                                    <asp:HiddenField ID="hfGlobalMessageId" runat="server" />
                                    <asp:HiddenField ID="hfRecognize" runat="server" />
                                    <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                                </div>
                                <div class="grid_boxes">
                                    <div id="divdownpaging" runat="server">
                                        <div class="grid_paging_bottom">
                                            <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="rnkland">
                <%-- Grid Alert popup Start--%>
                <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                    TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                </asp:ModalPopupExtender>
                <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                    <div class="popheader popheaderlblred">
                        <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- Grid Alert popup Closed--%>
                <%-- Active Btn popup Start--%>
                <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                    TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnActivate" runat="server" Text="Button" CssClass="popbtn" />
                <asp:Panel ID="PanelActivate" runat="server" CssClass="modalPopup"
                    class="poppnl" Style="display: none;">
                    <div id="divlblColor" runat="server" class="popheader">
                        <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                    </div>
                    <br />
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btnActiveOk" runat="server" OnClick="btnActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                            <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- Active  Btn popup Closed--%>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".rnkland").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/PageValidations/AddGlobalMessage.js" type="text/javascript"></script>

      <script language="javascript" type="text/javascript">
          $(document).ready(function () { assignDiv('rptrMainMasterMenu_90_3', '90') });
    </script>

    <%--==============Validation script ref ends==============--%>
</asp:Content>
