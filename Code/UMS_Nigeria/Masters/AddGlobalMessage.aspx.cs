﻿
#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddGlobalMessage
                     
 Developer        : Id075-RamaDevi M
 Creation Date    : 14-Mar-2014
  Developer        : Id065-Bhimaraju V
 Creation Date    : 25-Jul-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using System.Xml;
using UMS_NigeriaBE;
using System.Data;
using Resources;
using System.Globalization;
using System.Threading;
using System.Drawing;
using UMS_NigriaDAL;
using System.Collections;
using System.Xml.Linq;
using System.Configuration;

namespace UMS_Nigeria.Messages
{
    public partial class AddGlobalMessage : System.Web.UI.Page
    {

        # region Members
        iDsignBAL objiDsignBal = new iDsignBAL();
        GlobalMessagesBAL objGlobalMessageBal = new GlobalMessagesBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        public int PageNum;
        public int RowNum;
        DataTable dtBillingTypes = new DataTable();
        string Key = Resources.UMS_Resource.ADD_GLOBALMESSAGE;
        Hashtable htableMaxLength = new Hashtable();
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
        }
        public string BUSearch
        {
            get { return string.IsNullOrEmpty(ddlBUSearch.SelectedValue) ? objCommonMethods.CollectAllValues(ddlBUSearch, ",") : ddlBUSearch.SelectedValue; }
            set { ddlBUSearch.SelectedValue = value.ToString(); }
        }
        public string BusinessUnitName
        {
            get { return string.IsNullOrEmpty(ddlBusinessUnit.SelectedValue) ? objCommonMethods.CollectAllValues(ddlBusinessUnit, ",") : ddlBusinessUnit.SelectedValue; }
            set { ddlBusinessUnit.SelectedValue = value.ToString(); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                lblMessage.Text = pnlMessage.CssClass = string.Empty;
                if (!IsPostBack)
                {
                    //string path = string.Empty;
                    //path = objCommonMethods.GetPagePath(this.Request.Url);
                    //if (objCommonMethods.IsAccess(path))
                    //{
                        hfPageNo.Value = Constants.pageNoValue.ToString();
                        objCommonMethods.BindUserBusinessUnits(ddlBusinessUnit, string.Empty, false, Resource.DDL_ALL);
                        objCommonMethods.BindUserBusinessUnits(ddlBUSearch, string.Empty, false, Resource.DDL_ALL);
                        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        {
                            ddlBUSearch.SelectedIndex = ddlBusinessUnit.SelectedIndex = ddlBusinessUnit.Items.IndexOf(ddlBusinessUnit.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                            ddlBusinessUnit.Enabled = ddlBUSearch.Enabled = false;
                            ddlBUSearch_SelectedIndexChanged(ddlBUSearch, new EventArgs());
                        }
                        else
                        {
                            BindGrid();
                            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        }
                        htableMaxLength = objCommonMethods.getApplicationSettings(Resource.SETTING_G_MSG_LENGTH);
                        BindBillingTypes();

                    //}
                    //else { Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }
                }
            }
            else { Response.Redirect(UMS_Resource.DEFAULT_PAGE); }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int count = 0;
                foreach (DataListItem li in dtlBillingTypes.Items)
                {
                    TextBox txtBillingType = (TextBox)li.FindControl("txtBillingType");
                    if (isValidate(txtBillingType)) { count++; };
                }
                if (count == Convert.ToInt32(hfcount.Value))
                {
                    lblgridalertmsg.Text = "Enter atleast one message ";
                    mpegridalert.Show();
                    btngridalertok.Focus();
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("BillingTypeId", typeof(int));
                    dt.Columns.Add("Message", typeof(string));
                    dt.Columns.Add("CreatedBy", typeof(string));
                    dt.Columns.Add("BU_ID", typeof(string));
                    string[] BU_ID = this.BusinessUnitName.Split(',');
                    foreach (string item in BU_ID)
                    {
                        foreach (DataListItem li in dtlBillingTypes.Items)
                        {
                            LinkButton lnkMessageId = (LinkButton)li.FindControl("lnkMessageId");
                            TextBox txtBillingType = (TextBox)li.FindControl("txtBillingType");

                            if (txtBillingType.Text != string.Empty && item != string.Empty)
                                dt = GetEffectedBillDetails(dt, Convert.ToInt32(lnkMessageId.CommandArgument), txtBillingType.Text, Session[UMS_Resource.SESSION_LOGINID].ToString(), item);
                        }
                    }

                    GlobalMessages objGlobalMessege = new GlobalMessages();
                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    XmlDocument xml = objiDsignBal.DoSerialize(dt);

                    objGlobalMessege = objiDsignBal.DeserializeFromXml<GlobalMessages>(objGlobalMessageBal.InsertGlobalMessages(xml, Statement.Insert));

                    if (objGlobalMessege.IsSuccess)
                    {
                        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Reset", "ClearControls();", true);
                        if (Session[UMS_Resource.SESSION_USER_BUID] == null)
                        {
                            ddlBUSearch.SelectedIndex = 0;
                            ddlBusinessUnit.SelectedIndex = 0;
                        }
                       
                        BindGrid();
                        UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        Message(UMS_Resource.GLOBALMESSAGE_INSERTSUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        
                        BindBillingTypes();
                    }
                    else
                    {
                        Message(UMS_Resource.GLOBALMESSAGE_INSERTFAILED, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvMessages_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");
                    if (Convert.ToInt32(lblActiveStatusId.Text) == Constants.DeActive)
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void dtlBillingTypes_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (htableMaxLength.Count > 0)
                {
                    LinkButton lnkMessageId = (LinkButton)e.Item.FindControl("lnkMessageId");
                    TextBox txtBillingType = (TextBox)e.Item.FindControl("txtBillingType");

                    if (lnkMessageId.Text == Constants.Mobile)
                    {
                        txtBillingType.Attributes.Add("MaxLength", htableMaxLength[Constants.Mobile].ToString());
                    }
                    else if (lnkMessageId.Text == Constants.Web)
                    {
                        txtBillingType.Attributes.Add("MaxLength", htableMaxLength[Constants.Web].ToString());
                    }
                    else if (lnkMessageId.Text == Constants.Spot)
                    {
                        txtBillingType.Attributes.Add("MaxLength", htableMaxLength[Constants.Spot].ToString());
                    }
                }
            }
        }

        protected void gvMessages_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                hfRecognize.Value = string.Empty;
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                Label lblGlobalMessageId = (Label)row.FindControl("lblGlobalMessageId");
                switch (e.CommandName.ToUpper())
                {
                    case "ACTIVEMSG":
                        mpeActivate.Show();
                        hfRecognize.Value = Constants.ACTIVE;
                        hfGlobalMessageId.Value = lblGlobalMessageId.Text;
                        divlblColor.Style.Add("background-color", "Green");
                        lblActiveMsg.Text = Resource.ACTIVE_MSG_POPUP_TEXT;
                        btnActiveOk.Focus();
                        break;
                    case "DEACTIVEMSG":
                        mpeActivate.Show();
                        hfRecognize.Value = Constants.DEACTIVE;
                        divlblColor.Style.Add("background-color", "Red");
                        hfGlobalMessageId.Value = lblGlobalMessageId.Text;
                        lblActiveMsg.Text = Resource.DEACTIVE_MSG_POPUP_TEXT;
                        btnActiveOk.Focus();
                        break;
                    case "DELETEMSG":
                        mpeActivate.Show();
                        hfRecognize.Value = Constants.DELETE;
                        divlblColor.Style.Add("background-color", "Red");
                        hfGlobalMessageId.Value = lblGlobalMessageId.Text;
                        lblActiveMsg.Text = Resource.DELETE_MSG_POPUP_TEXT;
                        btnActiveOk.Focus();
                        break;
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                GlobalMessages objGlobalMessages = new GlobalMessages();
                if (hfRecognize.Value == Constants.ACTIVE)
                {
                    objGlobalMessages.ActiveStatusId = Constants.Active;
                    objGlobalMessages.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objGlobalMessages.GlobalMessageId = Convert.ToInt32(hfGlobalMessageId.Value);
                    objGlobalMessages = objiDsignBal.DeserializeFromXml<GlobalMessages>(objGlobalMessageBal.GetGlobalMessages(objGlobalMessages, ReturnType.Single));

                    if (objGlobalMessages.IsSuccess)
                    {
                        BindGrid();
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        Message(Resource.MSG_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                    }
                    else
                        Message(Resource.MSG_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (hfRecognize.Value == Constants.DEACTIVE)
                {
                    objGlobalMessages.ActiveStatusId = Constants.DeActive;
                    objGlobalMessages.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objGlobalMessages.GlobalMessageId = Convert.ToInt32(hfGlobalMessageId.Value);
                    objGlobalMessages = objiDsignBal.DeserializeFromXml<GlobalMessages>(objGlobalMessageBal.GetGlobalMessages(objGlobalMessages, ReturnType.Single));

                    if (objGlobalMessages.IsSuccess)
                    {
                        BindGrid();
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        Message(Resource.MSG_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                    }
                    else
                        Message(Resource.MSG_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);

                }
                else if (hfRecognize.Value == Constants.DELETE)
                {
                    objGlobalMessages.ActiveStatusId = Constants.Delete;
                    objGlobalMessages.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objGlobalMessages.GlobalMessageId = Convert.ToInt32(hfGlobalMessageId.Value);
                    objGlobalMessages = objiDsignBal.DeserializeFromXml<GlobalMessages>(objGlobalMessageBal.GetGlobalMessages(objGlobalMessages, ReturnType.Single));

                    if (objGlobalMessages.IsSuccess)
                    {
                        int PageNo = Convert.ToInt32(hfPageNo.Value);
                        if (gvMessages.Rows.Count == 1)
                        {
                            hfPageNo.Value = (PageNo == 1 ? PageNo : (PageNo - 1)).ToString();
                            UCPaging1.CreatePagingDT(Convert.ToInt32(hfPageNo.Value));
                        }
                        BindGrid();
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        Message(Resource.MSG_DELETE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                    }
                    else
                        Message(Resource.MSG_DELETE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlBUSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfPageNo.Value = Constants.pageNoValue.ToString();
            BindGrid();
            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        }

        #endregion

        #region Methods
        public void BindGrid()
        {
            GlobalMessages objGlobalMessages = new GlobalMessages();
            GlobalMessagesListBE objGlobalMessagesListBE = new GlobalMessagesListBE();
            objGlobalMessages.PageNo = Convert.ToInt32(hfPageNo.Value);
            objGlobalMessages.PageSize = PageSize;
            objGlobalMessages.BU_ID = this.BUSearch;
            objGlobalMessagesListBE = objiDsignBal.DeserializeFromXml<GlobalMessagesListBE>(objGlobalMessageBal.GetGlobalMessages(objGlobalMessages, ReturnType.Get));
            if (objGlobalMessagesListBE.Items.Count > 0)
            {
                divdownpaging.Visible = divpaging.Visible = true;
                hfTotalRecords.Value = objGlobalMessagesListBE.Items[0].TotalRecords.ToString();
                gvMessages.DataSource = objGlobalMessagesListBE.Items;
                gvMessages.DataBind();
            }
            else
            {
                hfTotalRecords.Value = objGlobalMessagesListBE.Items.Count.ToString();
                divdownpaging.Visible = divpaging.Visible = false;
                gvMessages.DataSource = new DataTable(); ;
                gvMessages.DataBind();
            }

        }
        private DataTable GetEffectedBillDetails(DataTable dt, int BillingTypeId, string Message, string CreatedBy, string BU_ID)
        {
            DataRow dr = dt.NewRow();
            dr["BillingTypeId"] = BillingTypeId;
            dr["Message"] = Message;
            dr["CreatedBy"] = CreatedBy;
            dr["BU_ID"] = BU_ID;
            dt.Rows.Add(dr);
            return dt;
        }
        private bool isValidate(TextBox txt)
        {
            if (txt.Text == string.Empty)
                return true;
            else
                return false;
        }

        private void BindBillingTypes()
        {
            MastersBE objMasterBe = new MastersBE();
            MastersListBE objMasterListBe = new MastersListBE();
            objMasterListBe = objiDsignBal.DeserializeFromXml<MastersListBE>(objGlobalMessageBal.GetBillingTypesBAL());
            dtBillingTypes = objiDsignBal.ConvertListToDataSet<MastersBE>(objMasterListBe.Items).Tables[0];
            if (objMasterListBe.Items.Count > 0)
            {
                hfcount.Value = objMasterListBe.Items.Count.ToString();
                dtlBillingTypes.DataSource = dtBillingTypes;
                dtlBillingTypes.DataBind();

            }
        }
        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        //#region Paging
        //protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        BindGridMessages();
        //        hfPageSize.Value = ddlPageSize.SelectedItem.Text;
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnFirst_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        BindGridMessages();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnPrevious_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum -= Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        BindGridMessages();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnNext_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum += Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        BindGridMessages();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnLast_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = hfLastPage.Value;
        //        BindGridMessages();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //private void CreatePagingDT(int TotalNoOfRecs)
        //{
        //    try
        //    {
        //        double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
        //        DataTable dtPages = new DataTable();
        //        dtPages.Columns.Add("PageNo", typeof(int));
        //        int currentpage = Convert.ToInt32(hfPageNo.Value);
        //        lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
        //        objiDsignBal.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
        //        lblCurrentPage.Text = hfPageNo.Value;
        //        if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
        //        if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //private void BindPagingDropDown(int TotalRecords)
        //{
        //    try
        //    {
        //        objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //#endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        //# region Members
        //iDsignBAL objiDsignBAL = new iDsignBAL();
        //GlobalMessagesBAL objGlobalMessagesBAL = new GlobalMessagesBAL();
        ////XmlDocument xml = null;
        //public int PageNum;
        //public int PagespotNum;
        //string Key = Resources.UMS_Resource.ADD_GLOBALMESSAGE;
        //CommonMethods _ObjCommonMethods = new CommonMethods();
        //GlobalMessages _objGlobalMessege = new GlobalMessages();
        //#endregion

        //#region Properties
        //private string FirstMessage
        //{
        //    get
        //    { return txtMessage1.Text; }
        //    set { txtMessage1.Text = value; }
        //}
        //private string SecondMessage
        //{
        //    get
        //    { return txtMessage2.Text; }
        //    set { txtMessage2.Text = value; }
        //}
        //private string ThirdMessage
        //{
        //    get
        //    { return txtMessage3.Text; }
        //    set { txtMessage3.Text = value; }
        //}
        //public int PageSize
        //{
        //    get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
        //}
        //public int PageSpotSize
        //{
        //    get { return string.IsNullOrEmpty(ddlSpotPaze.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlSpotPaze.SelectedValue); }
        //}
        //#endregion

        //#region Events

        //protected void Page_PreInit(object sender, EventArgs e)
        //{
        //    if (Session[UMS_Resource.SESSION_LOGINID] == null)
        //        Response.Redirect(UMS_Resource.DEFAULT_PAGE);

        //}
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    pnlMessage.CssClass = lblMessage.Text = string.Empty;
        //    if (!IsPostBack)
        //    {
        //        string path = string.Empty;
        //        path = _ObjCommonMethods.GetPagePath(this.Request.Url);
        //        if (_ObjCommonMethods.IsAccess(path))
        //        {                    
        //            hfspotPageNo.Value = hfPageNo.Value = Constants.pageNoValue.ToString();
        //            _ObjCommonMethods.BindStates(ddlState, string.Empty, true);
        //            _ObjCommonMethods.BindBillingTypes(rblMessageType, false);
        //            BindManualMessages(string.Empty);
        //            BindSpotMessages(string.Empty);
        //            BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
        //            CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //            BindSpotPagingDropDown(Convert.ToInt32(hfspotTotalRecords.Value));
        //            CreateSpotPagingDT(Convert.ToInt32(hfspotTotalRecords.Value));

        //        }
        //        else { Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }
        //    }
        //}
        //protected void btnSave_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        _objGlobalMessege.StateId = ddlState.SelectedItem.Value;
        //        _objGlobalMessege.BillingTypeId = Convert.ToInt32(rblMessageType.SelectedItem.Value);
        //        _objGlobalMessege.Message = FirstMessage + "|" + SecondMessage + "|" + ThirdMessage;
        //        _objGlobalMessege.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
        //        _objGlobalMessege = objiDsignBAL.DeserializeFromXml<GlobalMessages>(objGlobalMessagesBAL.AddGllobalMessageBAL(objiDsignBAL.DoSerialize<GlobalMessages>(_objGlobalMessege)));
        //        if (_objGlobalMessege.ActiveStatus == 1)
        //        {
        //            BindManualMessages(string.Empty);
        //            BindSpotMessages(string.Empty);
        //            BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
        //            CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //            BindSpotPagingDropDown(Convert.ToInt32(hfspotTotalRecords.Value));
        //            CreateSpotPagingDT(Convert.ToInt32(hfspotTotalRecords.Value));
        //            Message(UMS_Resource.GLOBALMESSAGE_INSERTSUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
        //            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Script", "javascript:Clearall();", true);
        //        }
        //        else
        //        {
        //            Message(UMS_Resource.GLOBALMESSAGE_INSERTFAILED, UMS_Resource.MESSAGETYPE_ERROR);
        //        }
        //        upGV.Update(); upDDL.Update();
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //    }
        //}
        //protected void GvBillingMsg_RowDeleting(object sender, GridViewDeleteEventArgs e)
        //{
        //    try
        //    {
        //        hfTemp.Value = Convert.ToInt32(GvBillingMsg.DataKeys[e.RowIndex].Value).ToString();
        //        hfRecognize.Value = UMS_Resource.SPOT_MSG_DELETE;
        //        lblDeActiveMsg.Text = UMS_Resource.SPOT_MSG_DELETE_POPUP_TEXT;
        //        btnDeActiveOk.Text = UMS_Resource.YES;
        //        btnDeActiveCancel.Text = UMS_Resource.NO;
        //        mpeDeActive.Show();
        //        btnDeActiveOk.Focus();
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void GvManulaMsg_RowDeleting(object sender, GridViewDeleteEventArgs e)
        //{
        //    try
        //    {
        //        hfTemp.Value = Convert.ToInt32(GvManulaMsg.DataKeys[e.RowIndex].Value).ToString();
        //        hfRecognize.Value = UMS_Resource.BILLING_MSG_DELETE;
        //        lblDeActiveMsg.Text = UMS_Resource.BILLING_MSG_DELETE_POPUP_TEXT;
        //        btnDeActiveOk.Text = UMS_Resource.YES;
        //        btnDeActiveCancel.Text = UMS_Resource.NO;
        //        mpeDeActive.Show();
        //        btnDeActiveOk.Focus();
        //    }

        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //private void PageAccessPermissions()
        //{
        //    try
        //    {
        //        if (_ObjCommonMethods.IsAccess(UMS_Resource.ADD_STATES_PAGE)) { lkbtnAddNewState.Visible = true; } else { lkbtnAddNewState.Visible = false; }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void lkbtnAddNewState_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Response.Redirect(UMS_Resource.ADD_STATES_PAGE);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void btnDeActiveOk_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (hfRecognize.Value == UMS_Resource.SPOT_MSG_DELETE)
        //        {
        //            _objGlobalMessege.GlobalMessageId = Convert.ToInt32(hfTemp.Value);
        //            _objGlobalMessege.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
        //            _objGlobalMessege = objiDsignBAL.DeserializeFromXml<GlobalMessages>(objGlobalMessagesBAL.DeleteGlobalMessageBAL(objiDsignBAL.DoSerialize<GlobalMessages>(_objGlobalMessege)));
        //            if (_objGlobalMessege.ActiveStatus == 1)
        //            {
        //                BindSpotMessages(string.Empty);
        //                Message(UMS_Resource.MESSAGE_DELETED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

        //            }
        //        }
        //        else if (hfRecognize.Value == UMS_Resource.BILLING_MSG_DELETE)
        //        {
        //            _objGlobalMessege.GlobalMessageId = Convert.ToInt32(hfTemp.Value);
        //            _objGlobalMessege.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
        //            _objGlobalMessege = objiDsignBAL.DeserializeFromXml<GlobalMessages>(objGlobalMessagesBAL.DeleteGlobalMessageBAL(objiDsignBAL.DoSerialize<GlobalMessages>(_objGlobalMessege)));
        //            if (_objGlobalMessege.ActiveStatus == 1)
        //            {
        //                BindManualMessages(string.Empty);
        //                Message(UMS_Resource.MESSAGE_DELETED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    BindSpotMessages(ddlState.SelectedItem.Value);
        //    BindManualMessages(ddlState.SelectedItem.Value);

        //}
        //protected void lkbtnFirst_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        //BindManualMessages(ddlDistrict.SelectedItem.Value);
        //        BindSpotMessages(ddlState.SelectedItem.Value);
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void lkbtnPrevious_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum -= Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        //BindManualMessages(ddlDistrict.SelectedItem.Value);
        //        BindSpotMessages(ddlState.SelectedItem.Value);
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void lkbtnNext_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum += Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        //BindManualMessages(ddlDistrict.SelectedItem.Value);
        //        BindSpotMessages(ddlState.SelectedItem.Value);
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void lkbtnLast_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = hfLastPage.Value;
        //        //BindManualMessages(ddlDistrict.SelectedItem.Value);
        //        BindSpotMessages(ddlState.SelectedItem.Value);
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        //BindManualMessages(ddlDistrict.SelectedItem.Value);
        //        BindSpotMessages(ddlState.SelectedItem.Value);
        //        hfPageSize.Value = ddlPageSize.SelectedItem.Text;
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void lnkspotfirst_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfspotPageNo.Value = Constants.pageNoValue.ToString();
        //        //BindSpotMessages(ddlDistrict.SelectedItem.Value);
        //        BindManualMessages(ddlState.SelectedItem.Value);
        //        CreateSpotPagingDT(Convert.ToInt32(hfspotTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void lnkspotprevious_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PagespotNum = Convert.ToInt32(lblSpotCurrentPage.Text.Trim());
        //        PagespotNum -= Constants.pageNoValue;
        //        hfspotPageNo.Value = PagespotNum.ToString();
        //        //BindSpotMessages(ddlDistrict.SelectedItem.Value);
        //        BindManualMessages(ddlState.SelectedItem.Value);
        //        CreateSpotPagingDT(Convert.ToInt32(hfspotTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void lnkspotnext_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PagespotNum = Convert.ToInt32(lblSpotCurrentPage.Text.Trim());
        //        PagespotNum += Constants.pageNoValue;
        //        hfspotPageNo.Value = PagespotNum.ToString();
        //        //BindSpotMessages(ddlDistrict.SelectedItem.Value);
        //        BindManualMessages(ddlState.SelectedItem.Value);
        //        CreateSpotPagingDT(Convert.ToInt32(hfspotTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void lnkspotlast_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfspotPageNo.Value = hfspotLastPage.Value;
        //        //BindSpotMessages(ddlDistrict.SelectedItem.Value);
        //        BindManualMessages(ddlState.SelectedItem.Value);
        //        CreateSpotPagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void ddlSpotPaze_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfspotPageNo.Value = Constants.pageNoValue.ToString();
        //        //BindSpotMessages(ddlDistrict.SelectedItem.Value);
        //        BindManualMessages(ddlState.SelectedItem.Value);
        //        hfspotPageSize.Value = ddlSpotPaze.SelectedItem.Text;
        //        CreateSpotPagingDT(Convert.ToInt32(hfspotTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //#endregion

        //#region Methods
        //private void BindSpotMessages(string stateid)
        //{
        //    try
        //    {

        //        _objGlobalMessege.BillingTypeId = 1;
        //        _objGlobalMessege.StateId = stateid;
        //        _objGlobalMessege.PageNo = Convert.ToInt32(hfPageNo.Value);
        //        _objGlobalMessege.PageSize = PageSize;
        //        GlobalMessagesListBE ObjGlobalMessageListBE = objiDsignBAL.DeserializeFromXml<GlobalMessagesListBE>(objGlobalMessagesBAL.GetBillingMessagesBAL(objiDsignBAL.DoSerialize<GlobalMessages>(_objGlobalMessege)));
        //        //if (ObjGlobalMessageListBE.Items.Count > 0)
        //        //{
        //        //    gridHeadManualBill.Visible =  true;
        //        //}
        //        //else
        //        //{
        //        //    gridHeadManualBill.Visible = false;
        //        //}
        //        if (ObjGlobalMessageListBE.Items.Count > 0)
        //        {
        //            gridHeadSpotBill.Visible = divgrid.Visible = true;
        //            hfTotalRecords.Value = ObjGlobalMessageListBE.Items[0].TotalRecords.ToString();
        //        }
        //        else
        //        {
        //            divgrid.Visible = gridHeadSpotBill.Visible = false;
        //            hfTotalRecords.Value = ObjGlobalMessageListBE.Items.Count.ToString();

        //        }
        //        GvBillingMsg.DataSource = objiDsignBAL.ConvertListToDataSet<GlobalMessages>(ObjGlobalMessageListBE.Items).Tables[0];
        //        GvBillingMsg.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //private void BindManualMessages(string stateid)
        //{
        //    try
        //    {
        //        _objGlobalMessege.BillingTypeId = 2;
        //        _objGlobalMessege.StateId = stateid;
        //        _objGlobalMessege.PageNo = Convert.ToInt32(hfspotPageNo.Value);
        //        _objGlobalMessege.PageSize = PageSpotSize;
        //        GlobalMessagesListBE ObjGlobalMessageListBE = objiDsignBAL.DeserializeFromXml<GlobalMessagesListBE>(objGlobalMessagesBAL.GetBillingMessagesBAL(objiDsignBAL.DoSerialize<GlobalMessages>(_objGlobalMessege)));
        //        //if (ObjGlobalMessageListBE.Items.Count > 0)
        //        //{
        //        //    gridHeadSpotBill.Visible = true;
        //        //}
        //        //else
        //        //{
        //        //    gridHeadSpotBill.Visible = false;
        //        //}
        //        if (ObjGlobalMessageListBE.Items.Count > 0)
        //        {
        //            gridHeadManualBill.Visible = divgrid1.Visible = true;
        //            hfspotTotalRecords.Value = ObjGlobalMessageListBE.Items[0].TotalRecords.ToString();
        //        }
        //        else
        //        {
        //            gridHeadManualBill.Visible = divgrid1.Visible = false;
        //            hfspotTotalRecords.Value = ObjGlobalMessageListBE.Items.Count.ToString();
        //        }
        //        GvManulaMsg.DataSource = objiDsignBAL.ConvertListToDataSet<GlobalMessages>(ObjGlobalMessageListBE.Items).Tables[0];
        //        GvManulaMsg.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //private void Message(string Message, string MessageType)
        //{
        //    try
        //    {
        //        _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
        //    }
        //    catch (Exception Ex)
        //    {
        //        throw Ex;
        //    }
        //}

        //private void BindPagingDropDown(int TotalRecords)
        //{
        //    try
        //    {
        //        _ObjCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);

        //        //ddlPageSize.Items.Clear();
        //        //if (TotalRecords < Constants.PageSizeStarts)
        //        //{
        //        //    ListItem item = new ListItem(Constants.PageSizeStarts.ToString(), Constants.PageSizeStarts.ToString());
        //        //    ddlPageSize.Items.Add(item);
        //        //}
        //        //else
        //        //{
        //        //    int previousSize = Constants.PageSizeStarts;
        //        //    for (int i = Constants.PageSizeStarts; i <= TotalRecords; i = i + Constants.PageSizeIncrement)
        //        //    {
        //        //        ListItem item = new ListItem(i.ToString(), i.ToString());
        //        //        ddlPageSize.Items.Add(item);
        //        //        previousSize = i;
        //        //    }
        //        //    if (previousSize < TotalRecords)
        //        //    {
        //        //        ListItem item = new ListItem((previousSize + Constants.PageSizeIncrement).ToString(), (previousSize + Constants.PageSizeIncrement).ToString());
        //        //        ddlPageSize.Items.Add(item);
        //        //    }
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //private void CreatePagingDT(int TotalNoOfRecs)
        //{
        //    try
        //    {
        //        double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));

        //        DataTable dtPages = new DataTable();
        //        dtPages.Columns.Add("PageNo", typeof(int));
        //        int currentpage = Convert.ToInt32(hfPageNo.Value);
        //        lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
        //        objiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
        //        lblCurrentPage.Text = hfPageNo.Value;
        //        if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.Color.Gray; }
        //        if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //private void BindSpotPagingDropDown(int TotalRecords)
        //{
        //    try
        //    {
        //        ddlSpotPaze.Items.Clear();
        //        if (TotalRecords < Constants.PageSizeStarts)
        //        {
        //            ListItem item = new ListItem(Constants.PageSizeStarts.ToString(), Constants.PageSizeStarts.ToString());
        //            ddlSpotPaze.Items.Add(item);
        //        }
        //        else
        //        {
        //            int previousSize = Constants.PageSizeStarts;
        //            for (int i = Constants.PageSizeStarts; i <= TotalRecords; i = i + Constants.PageSizeIncrement)
        //            {
        //                ListItem item = new ListItem(i.ToString(), i.ToString());
        //                ddlSpotPaze.Items.Add(item);
        //                previousSize = i;
        //            }
        //            if (previousSize < TotalRecords)
        //            {
        //                ListItem item = new ListItem((previousSize + Constants.PageSizeIncrement).ToString(), (previousSize + Constants.PageSizeIncrement).ToString());
        //                ddlSpotPaze.Items.Add(item);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //private void CreateSpotPagingDT(int TotalNoOfRecs)
        //{
        //    try
        //    {
        //        double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSpotSize));

        //        DataTable dtPages = new DataTable();
        //        dtPages.Columns.Add("PageNo", typeof(int));
        //        int currentpage = Convert.ToInt32(hfspotPageNo.Value);
        //        lnkspotlast.CommandArgument = hfspotLastPage.Value = totalpages.ToString();
        //        objiDsignBAL.GridViewPaging(lnkspotnext, lnkspotprevious, lnkspotfirst, lnkspotlast, currentpage, TotalNoOfRecs);
        //        lblSpotCurrentPage.Text = hfspotPageNo.Value;
        //        if (totalpages == 1) { lnkspotfirst.ForeColor = lnkspotlast.ForeColor = lnkspotnext.ForeColor = lnkspotprevious.ForeColor = System.Drawing.Color.Gray; }
        //        if (string.Compare(hfspotPageNo.Value, hfspotLastPage.Value, true) == 0) { lnkspotnext.Enabled = lnkspotlast.Enabled = false; }
        //        else { lnkspotnext.Enabled = lnkspotlast.Enabled = true; }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected override void InitializeCulture()
        //{
        //    if (Session[UMS_Resource.CULTURE] != null)
        //    {
        //        string culture = Session[UMS_Resource.CULTURE].ToString();

        //        Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
        //        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
        //    }
        //    base.InitializeCulture();
        //}
        //#endregion

    }
}

