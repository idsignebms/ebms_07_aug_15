﻿<%@ Page Title=":: Add Govt Account Type ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="AddGovtType.aspx.cs" Inherits="UMS_Nigeria.Masters.AddGovtType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server" AssociatedUpdatePanelID="upAddGovtType">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <div class="inner-sec">
            <asp:UpdatePanel ID="upAddGovtType" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddGovtType" runat="server" Text="<%$ Resources:Resource, ADD_GOVT_TYPE%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <%--<asp:Literal ID="litGovtTypes" runat="server" Text="<%$ Resources:Resource, ADD_GOVT_TYPE %>"></asp:Literal>--%>
                                </label>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div style="width: 700px; margin-top: 62px; padding: 20px;">
                            <div class="inner-box3" style="float: left; height: auto; margin-top: -103px; top: -140px;">
                                <div id="divActType" runat="server" class="text-inner">
                                    <label for="name" style="color: #0078c5">
                                        <asp:Literal ID="litFullPath" runat="server"></asp:Literal>
                                    </label>
                                    <div class="space" style="margin-bottom: 10px;">
                                    </div>
                                    <label for="name">
                                        <asp:Literal ID="litAccountType" runat="server" Text="<%$ Resources:Resource, ACCOUNT_TYPE %>"></asp:Literal><span
                                            class="span_star">*</span>
                                    </label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox CssClass="text-box" ID="txtAccountType" TabIndex="3" runat="server"
                                        onblur="return TextBoxBlurValidationlbl(this,'spanAccountType','Account Type')"
                                        MaxLength="50" placeholder="Enter Account Type"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="ftbTxtAccountType" runat="server" TargetControlID="txtAccountType"
                                        FilterType="LowercaseLetters, UppercaseLetters, Custom" ValidChars=" ,-/&">
                                    </asp:FilteredTextBoxExtender>
                                    <span id="spanAccountType" class="span_color"></span>
                                </div>
                                <asp:Button ID="btnSave" TabIndex="3" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                    CssClass="box_s" runat="server" OnClick="btnSave_OnClick" />
                                <asp:Button ID="btnUpdate" Visible="False" TabIndex="5" OnClientClick="return Validate();"
                                    Text="<%$ Resources:Resource, UPDATE%>" CssClass="box_s" runat="server" />
                                <asp:Button ID="btnCancel" TabIndex="7" Text="<%$ Resources:Resource, CANCEL%>" CssClass="box_s"
                                    runat="server" OnClick="btnCancel_Click" />
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div style="width: 150px; float: left; margin-bottom: 20px;">
                            <div class="inner-box1" style="width: 0px;">
                                <asp:TreeView ID="tvGovtType" ExpandDepth="0" runat="server" OnSelectedNodeChanged="tvGovtType_SelectedNodeChanged"
                                    OnTreeNodePopulate="tvGovtType_TreeNodePopulate">
                                    <RootNodeStyle Font-Bold="True" />
                                    <SelectedNodeStyle BackColor="#7FBF4D" Font-Bold="True" Font-Italic="True" ForeColor="White" />
                                </asp:TreeView>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                    </div>
                    <!--Dynamic start -->
                    <asp:HiddenField ID="hfRefActTypeIDString" runat="server" Value="" />
                    <div id="divGrandParent" runat="server" class="inner-box">
                        <%--<div id="divParent" class="inner-box1">
                            <div ID="divChield" class="text-inner">
                            </div>
                        </div>--%>
                    </div>
                    <!--Dynamic end -->
                    <br />
                    <br />
                    <div class="box_total">
                        <div class="box_totalTwi_c">
                            <div class="space">
                            </div>
                            <div class="space">
                            </div>
                            <asp:Button ID="btnAdd" TabIndex="2" Text="<%$ Resources:Resource, ADD%>" CssClass="box_s"
                                runat="server" Visible="False" OnClick="btnAdd_Click" />
                            <asp:Button ID="btnEdit" TabIndex="4" Text="<%$ Resources:Resource, EDIT%>" CssClass="box_s"
                                runat="server" Visible="False" OnClick="btnEdit_Click" />
                            <asp:Button ID="btnDelete" Visible="False" TabIndex="6" Text="<%$ Resources:Resource, DELETE%>"
                                CssClass="box_s" runat="server" />
                        </div>
                    </div>
                    <div id="hidepopup">
                        <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                            TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                            <div class="popheader popheaderlblred">
                                <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                            </div>
                            <div class="footer popfooterbtnrt">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Shift GovtCustomer Confirmation popup Start--%>
                        <asp:ModalPopupExtender ID="mpeShiftGovtCustomer" runat="server" PopupControlID="PanelShiftGovtCustomer"
                            TargetControlID="btnShiftGovtCustomer" BackgroundCssClass="modalBackground" CancelControlID="btnShiftGovtCustomerNo">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnShiftGovtCustomer" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="PanelShiftGovtCustomer" runat="server" CssClass="modalPopup" Style="display: none;">
                            <div class="popheader popheaderlblred">
                                <asp:Label ID="lblShiftGovtCustomerMsg" runat="server"></asp:Label>
                            </div>
                            <div class="space">
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btnShiftGovtCustomerYes" runat="server" OnClick="btnShiftGovtCustomerYes_Click"
                                        Text="<%$ Resources:Resource, YES%>" CssClass="ok_btn" />
                                    <asp:Button ID="btnShiftGovtCustomerNo" runat="server" Text="<%$ Resources:Resource, NO%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Shift GovtCustomer Confirmation popup Closed--%>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/PageValidations/AddGovtType.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { assignDiv('rptrMainMasterMenu_156_0', '160') });
    </script>
</asp:Content>
