﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using iDSignHelper;
using Resources;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;

namespace UMS_Nigeria.Masters
{
    public partial class AddGovtType : System.Web.UI.Page
    {
        #region Members
        iDsignBAL objIdsignBal = new iDsignBAL();
        GovtAccountTypeBAL objGovtAccountTypeBAL = new GovtAccountTypeBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        XmlDocument xml = null;
        GovtAccountTypeListBE objGovtAccountTypeListBe = new GovtAccountTypeListBE();
        GovtAccountTypeBE _objGovtAccountTypeBe = new GovtAccountTypeBE();
        public string Key = "AddGovtType";

        public delegate void SelectedIndexChange();
        #endregion

        #region Events

        //protected override void OnInit(EventArgs e)
        //{

        //}
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE, false);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.Visible = false;
            if (!IsPostBack)
            {
                //objCommonMethods.BindGovtAccountTypeList(new GovtAccountTypeBE(), ddlAcountType, true);
                PopulateRootLevel();
                TreeNode tn = tvGovtType.Nodes[0];
                tn.Selected = litFullPath.Visible = true;
                tn.Expand();
                litFullPath.Text = getFullNodepath();
            }
        }

        protected void tvGovtType_TreeNodePopulate(object sender, TreeNodeEventArgs e)
        {

            objGovtAccountTypeListBe =
                objIdsignBal.DeserializeFromXml<GovtAccountTypeListBE>(
                    objGovtAccountTypeBAL.GetGovtAccountTypeList());
            PopulateSubLevel(int.Parse(e.Node.Value), e.Node);
        }

        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            SaveGovtAccountType(false);
        }

        protected void btnShiftGovtCustomerYes_Click(object sender, EventArgs e)
        {
            SaveGovtAccountType(true);
        }

        protected void tvGovtType_SelectedNodeChanged(object sender, EventArgs e)
        {
            litFullPath.Text = getFullNodepath();
        }


        //protected override object SaveViewState()
        //{
        //    var viewState = new object[2];
        //    //Saving the dropdownlist value to the View State
        //    viewState[0] = base.SaveViewState();
        //    return viewState;
        //}

        //protected override void LoadViewState(object savedState)
        //{
        //    //Getting the dropdown list value from view state.
        //    if (savedState is object[] )
        //    {
        //        var viewState = (object[])savedState;
        //        var count = int.Parse(viewState[0].ToString());
        //        BindDdlGovtAccountType();
        //        base.LoadViewState(viewState[0]);
        //    }
        //    else
        //    {
        //        base.LoadViewState(savedState);
        //    }
        //}

        #endregion

        #region Methods

        private void PopulateRootLevel()
        {
            objGovtAccountTypeListBe =
                    objIdsignBal.DeserializeFromXml<GovtAccountTypeListBE>(
                        objGovtAccountTypeBAL.GetGovtAccountTypeList());
            var _objGovtAccountTypeListBe =
                objGovtAccountTypeListBe.Items.Where(x => x.RefActTypeID == 0).ToList();
            PopulateNodes(_objGovtAccountTypeListBe, tvGovtType.Nodes);
        }

        private void PopulateNodes(List<GovtAccountTypeBE> objGovtAccountTypeBe, TreeNodeCollection nodes)
        {

            foreach (var Item in objGovtAccountTypeBe)
            {
                TreeNode tn = new TreeNode();
                tn.Text = Item.AccountType;
                tn.Value = Item.GActTypeID.ToString();
                tn.SelectAction = TreeNodeSelectAction.SelectExpand;
                nodes.Add(tn);
                // If node has child nodes, then enable on-demand populating
                tn.PopulateOnDemand = objGovtAccountTypeListBe.Items.Exists(x => x.RefActTypeID == Item.GActTypeID);
            }
        }

        private void PopulateSubLevel(int parentid, TreeNode parentNode)
        {
            var _objgoGovtAccountTypeBe =
                objGovtAccountTypeListBe.Items.Where(x => x.RefActTypeID == parentid).ToList();
            PopulateNodes(_objgoGovtAccountTypeBe, parentNode.ChildNodes);
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public string getFullNodepath()
        {
            string FullPath = string.Empty;
            int depth = tvGovtType.SelectedNode.Depth;
            List<string> s = new List<string>();
            TreeNode node = tvGovtType.SelectedNode;
            for (int i = depth; i > -1; i--)
            {
                s.Add(node.Text);
                //if (!string.IsNullOrEmpty(FullPath))
                //    FullPath +=  " --> " + node.Text;
                //else
                //    FullPath = node.Text;

                node = node.Parent;
            }
            for (int i = s.Count; i > 0; i--)
            {
                if (!string.IsNullOrEmpty(FullPath))
                    FullPath += " --> " + s[i - 1];
                else
                    FullPath = s[i - 1];
            }
            return FullPath;
        }

        public void SaveGovtAccountType(bool IsShiftCustomer)
        {
            _objGovtAccountTypeBe.AccountType = txtAccountType.Text.Trim();
            _objGovtAccountTypeBe.RefActTypeID = Convert.ToInt32(tvGovtType.SelectedValue);
            _objGovtAccountTypeBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            _objGovtAccountTypeBe.IsShiftCustomer = IsShiftCustomer;
            xml = objGovtAccountTypeBAL.GovtAccoutnType(_objGovtAccountTypeBe, Statement.Insert);
            _objGovtAccountTypeBe = objIdsignBal.DeserializeFromXml<GovtAccountTypeBE>(xml);

            _objGovtAccountTypeBe.AccountType = txtAccountType.Text.Trim();

            if (_objGovtAccountTypeBe.RowsEffected > 0)
            {
                TreeNode tn = new TreeNode(_objGovtAccountTypeBe.AccountType,
                    _objGovtAccountTypeBe.GActTypeID.ToString());
                tvGovtType.SelectedNode.ChildNodes.Add(tn);
                Message("Govt Account Type Added Successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                txtAccountType.Text = string.Empty;
            }
            else if (_objGovtAccountTypeBe.RowsEffected < 0)
            {

                lblShiftGovtCustomerMsg.Text = "The Node " + tvGovtType.SelectedNode.Text +
                                       " has customer associated with it do you want it to Link it with this new child.";
                mpeShiftGovtCustomer.Show();
            }
            else
            {
                Message("Govt Type already Exists.", UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        //public void BindDdlGovtAccountType()
        //{
        //    GovtAccountTypeBE _objGovtAccountTypeBe = new GovtAccountTypeBE();
        //    _objGovtAccountTypeBe.RefActTypeIDString = hfRefActTypeIDString.Value;
        //    objGovtAccountTypeListBe = objIdsignBal.DeserializeFromXml<GovtAccountTypeListBE>(objGovtAccountTypeBAL.GetGovtAccountTypeList(_objGovtAccountTypeBe));

        //    var drAccountType = from x in objGovtAccountTypeListBe.Items.AsEnumerable()
        //                        group x by new
        //                        {
        //                            x.Levels
        //                        }
        //                            into g
        //                            select g.Key.Levels;


        //    string cssClass = string.Empty;
        //    int counter = 1;
        //    string[] cssClasslist = new string[6];
        //    cssClasslist[0] = "inner-box";
        //    cssClasslist[1] = "text-inner";
        //    cssClasslist[2] = "clr";
        //    cssClasslist[4] = "text-box";
        //    cssClasslist[5] = "text-box text-select";

        //    HtmlGenericControl divParent = new HtmlGenericControl("div");

        //    foreach (var Level in drAccountType)
        //    {
        //        HtmlGenericControl divChild = new HtmlGenericControl("div");
        //        HtmlGenericControl divClr = new HtmlGenericControl("div");


        //        divClr.Attributes.Add("class", cssClasslist[2]);
        //        cssClass = cssClasslist[0] + counter;

        //        DropDownList ddlActType = new DropDownList();

        //        ddlActType.ID = "ddl" + Level;
        //        ddlActType.CssClass = "text-box";
        //        ddlActType.AutoPostBack = true;
        //        ddlActType.Items.Insert(0, new ListItem("--Select--", "0"));
        //        ddlActType.SelectedIndexChanged += new EventHandler(dynamicDDL_SelectedIndexChanged);
        //        var ddlDataSource = from x in objGovtAccountTypeListBe.Items
        //                            where x.Levels == Level
        //                            select x;

        //        ddlActType.DataValueField = "GActTypeID";
        //        ddlActType.DataTextField = "AccountType";
        //        ddlActType.DataSource = ddlDataSource;
        //        ddlActType.DataBind();
        //        divChild.Controls.Add(ddlActType);
        //        divParent.Controls.Add(divChild);

        //        counter++;
        //        if (counter == 4)
        //        {
        //            counter = 1;
        //            divGrandParent.Controls.Add(divClr);
        //        }
        //    }

        //    divGrandParent.Controls.Add(divParent);

        //}

        //protected void dynamicDDL_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DropDownList ddl = (DropDownList)sender;
        //    if (!string.IsNullOrEmpty(hfRefActTypeIDString.Value))
        //        hfRefActTypeIDString.Value = hfRefActTypeIDString.Value + "|" + ddl.SelectedValue;
        //    else
        //        hfRefActTypeIDString.Value = ddl.SelectedValue;
        //    //Response.Redirect(Request.RawUrl);
        //    //BindDdlGovtAccountType();
        //    OnInit(e);
        //}

        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tvGovtType.SelectedValue))
            {
                litFullPath.Visible = true;
                litFullPath.Text = getFullNodepath();
                divActType.Visible = btnSave.Visible = btnCancel.Visible = true;
                btnAdd.Visible = btnEdit.Visible = btnDelete.Visible = false;//tvGovtType.Enabled= false;
            }
            else
            {
                lblgridalertmsg.Text = "Please select the Govt type to add the new node in it.";
                mpegridalert.Show();
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(tvGovtType.SelectedValue))
            //{
            //    litFullPath.Visible = true;
            //    litFullPath.Text = getFullNodepath();
            //    divActType.Visible = btnUpdate.Visible = btnCancel.Visible = true;
            //    btnAdd.Visible = btnEdit.Visible = btnDelete.Visible = false;
            //}
            //else
            //{
            //    lblgridalertmsg.Text = "Please select the Govt type to add the new node in it.";
            //    mpegridalert.Show();
            //}
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtAccountType.Text = string.Empty;
            tvGovtType.Nodes[0].Selected = true;
            litFullPath.Text = getFullNodepath();
            //divActType.Visible = btnSave.Visible = btnUpdate.Visible = btnCancel.Visible = false;
            //btnAdd.Visible = tvGovtType.Enabled= true;
            //btnEdit.Visible = btnDelete.Visible = true;
        }


    }
}