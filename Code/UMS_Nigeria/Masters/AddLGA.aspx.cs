﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using iDSignHelper;
using Resources;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using UMS_NigeriaBE;
using System.Threading;
using iDSignHelper;
using System.Data;

namespace UMS_Nigeria.Masters
{
    public partial class AddLGA : System.Web.UI.Page
    {
        #region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        CommonDAL _ObjCommonDAL = new CommonDAL();
        UMS_Service _ObjUMS_Service = new UMS_Service();
        public int PageNum;
        XmlDocument xml = null;
        LGABAL _ObjLGABAL = new LGABAL();
        string Key = "AddLGA";
        #endregion

        #region Properties

        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
        }
        private string LGAName
        {
            get { return txtLGAName.Text.Trim(); }
            set { txtLGAName.Text = value; }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE, false);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;

            if (!IsPostBack)
            {
                if (Session[UMS_Resource.SESSION_LOGINID] != null)
                {
                    _ObjCommonMethods.BindBusinessUnits(ddlBusinessUnitName, string.Empty, true);
                    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    {
                        ddlBusinessUnitName.SelectedIndex = ddlBusinessUnitName.Items.IndexOf(ddlBusinessUnitName.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                        ddlBusinessUnitName.Enabled = false;
                    }
                    
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    BindGrid();
                }
                else
                    Response.Redirect(UMS_Resource.DEFAULT_PAGE, false);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                LGABE objLGABE = new LGABE();

                objLGABE.LGAName = txtLGAName.Text;
                objLGABE.BU_ID = ddlBusinessUnitName.SelectedValue;
                objLGABE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                xml = _ObjLGABAL.LGA(objLGABE, Statement.Insert);
                objLGABE = _ObjiDsignBAL.DeserializeFromXml<LGABE>(xml);
                if (Convert.ToBoolean(objLGABE.RowsEffected > 0))
                {
                    BindGrid();
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.LGA_DETAILS_INSERT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    txtLGAName.Text = string.Empty;
                    //ddlBusinessUnitName.SelectedIndex = 0;
                }
                else
                {
                    Message("LGA Existed", UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {

                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvLGAList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                LGAListBE _ObjLGAListBE = new LGAListBE();
                LGABE _ObjLGABE = new LGABE();
                DataSet ds = new DataSet();


                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridLGAName = (TextBox)row.FindControl("txtGridLGAName");
                DropDownList ddlGridBusinessUnitName = (DropDownList)row.FindControl("ddlGridBusinessUnitName");

                Label lblLGAName = (Label)row.FindControl("lblLGAName");
                Label lblLGAId = (Label)row.FindControl("lblLGAId");
                Label lblBusinessUnitName = (Label)row.FindControl("lblBusinessUnitName");
                Label lblBusinessUnitId = (Label)row.FindControl("lblBusinessUnitId");

                hfLGAId.Value = lblLGAId.Text;

                hfRecognize.Value = string.Empty;
                switch (e.CommandName.ToUpper())
                {
                    case "EDITLGA":
                        lblLGAName.Visible = lblBusinessUnitName.Visible = false;
                        txtGridLGAName.Visible = ddlGridBusinessUnitName.Visible = true;
                        txtGridLGAName.Text = lblLGAName.Text;
                        _ObjCommonMethods.BindBusinessUnits(ddlGridBusinessUnitName, string.Empty, true);
                        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        {
                            ddlGridBusinessUnitName.SelectedIndex = ddlGridBusinessUnitName.Items.IndexOf(ddlGridBusinessUnitName.Items.FindByValue(lblBusinessUnitId.Text));
                            ddlGridBusinessUnitName.Enabled = false;
                        }
                        ddlGridBusinessUnitName.SelectedIndex = ddlGridBusinessUnitName.Items.IndexOf(ddlGridBusinessUnitName.Items.FindByValue(lblBusinessUnitId.Text));

                        row.FindControl("lkbtnLGACancel").Visible = row.FindControl("lkbtnLGAUpdate").Visible = true;
                        row.FindControl("lkbtnDelete").Visible = row.FindControl("lkbtnLGAEdit").Visible = false;
                        break;
                    case "CANCELLGA":
                        row.FindControl("lkbtnDelete").Visible = row.FindControl("lkbtnLGAEdit").Visible = true;
                        row.FindControl("lkbtnLGACancel").Visible = row.FindControl("lkbtnLGAUpdate").Visible = false;
                        txtGridLGAName.Visible = ddlGridBusinessUnitName.Visible = false;
                        lblLGAName.Visible = lblBusinessUnitName.Visible = true;
                        break;
                    case "UPDATELGA":
                        //xml = _ObjUMS_Service.UpdateStates(txtGridStateName.Text, lblStateCode.Text, txtGridDisplayCode.Text, ddlGridCountryName.SelectedValue, _ObjiDsignBAL.ReplaceNewLines(txtGridNotes.Text, true));
                        _ObjLGABE.LGAName = txtGridLGAName.Text;
                        _ObjLGABE.LGAId = Convert.ToInt32(lblLGAId.Text);
                        _ObjLGABE.BU_ID = ddlGridBusinessUnitName.SelectedValue;
                        _ObjLGABE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        xml = _ObjLGABAL.LGA(_ObjLGABE, Statement.Update);
                        _ObjLGABE = _ObjiDsignBAL.DeserializeFromXml<LGABE>(xml);

                        if (_ObjLGABE.RowsEffected > 0)
                        {
                            BindGrid();
                            Message("LGA Updated Successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                            txtLGAName.Text = string.Empty;
                        }
                        else
                            Message("LGA already exists.", UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "ACTIVELGA":
                        //xml = _ObjUMS_Service.DeleteActiveState(1, lblStateCode.Text);
                        mpeActivate.Show();
                        lblActiveMsg.Text = "Are you sure you want to Activate this LGA?";
                        btnActiveOk.Focus();

                        break;
                    case "DEACTIVELGA":
                        //xml = _ObjUMS_Service.DeleteActiveState(0, lblStateCode.Text);
                        mpeDeActive.Show();
                        //hfStateCode.Value = lblStateCode.Text;
                        lblDeActiveLGA.Text = "Are you sure you want to DeActivate this LGA?";
                        btnDeActiveOk.Focus();
                        break;
                }
            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void gvLGAList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnLGAUpdate = (LinkButton)e.Row.FindControl("lkbtnLGAUpdate");
                    TextBox txtGridLGAName = (TextBox)e.Row.FindControl("txtGridLGAName");
                    DropDownList ddlGridBusinessUnitName = (DropDownList)e.Row.FindControl("ddlGridBusinessUnitName");

                    lkbtnLGAUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridLGAName.ClientID + "','" +
                                                                                        ddlGridBusinessUnitName.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");


                    if (Convert.ToInt32(lblActiveStatusId.Text) == 2)
                    {
                        e.Row.FindControl("lkbtnDelete").Visible = e.Row.FindControl("lkbtnLGAEdit").Visible = false;
                        e.Row.FindControl("lkbtnLGAActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDelete").Visible = e.Row.FindControl("lkbtnLGAEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {

            try
            {
                LGABE _ObjLGABE = new LGABE();
                _ObjLGABE.LGAId = Convert.ToInt32(hfLGAId.Value);
                _ObjLGABE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _ObjLGABE.ActiveStatusId = 1;

                xml = _ObjLGABAL.LGA(_ObjLGABE, Statement.Delete);
                _ObjLGABE = _ObjiDsignBAL.DeserializeFromXml<LGABE>(xml);

                if (_ObjLGABE.RowsEffected > 0)
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message("LGA activated successfully", UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                    Message("LGA activation failed", UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                LGABE _ObjLGABE = new LGABE();
                _ObjLGABE.LGAId = Convert.ToInt32(hfLGAId.Value);
                _ObjLGABE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _ObjLGABE.ActiveStatusId = 2;
                xml = _ObjLGABAL.LGA(_ObjLGABE, Statement.Delete);
                _ObjLGABE = _ObjiDsignBAL.DeserializeFromXml<LGABE>(xml);
                if (_ObjLGABE.RowsEffected > 0)
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message("LGA DeActivated Successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                {
                    if (_ObjLGABE.AssciatedStateCount > 0)
                    {
                        lblgridalertmsg.Text = "LGA can not be deactivated as it contains " +
                                               _ObjLGABE.AssciatedStateCount + " States.";
                        mpegridalert.Show();
                    }
                    else if (_ObjLGABE.AssciatedStateCount == 0 && _ObjLGABE.Count > 0)
                        Message(string.Format(UMS_Resource.LGA_HAVE_CUSTOMERS, _ObjLGABE.Count), UMS_Resource.MESSAGETYPE_ERROR);
                    else
                        Message("LGA DeActivation Failed.", UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        public void BindGrid()
        {
            try
            {
                LGAListBE _objLGAListBE = new LGAListBE();
                LGABE _ObjLGABE = new LGABE();
                XmlDocument resultedXml = new XmlDocument();
                //resultedXml = _ObjUMS_Service.GetActiveCountriesList(Convert.ToInt32(hfPageNo.Value), PageSize);
                _ObjLGABE.PageNo = Convert.ToInt32(hfPageNo.Value);
                _ObjLGABE.PageSize = PageSize;
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    _ObjLGABE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    _ObjLGABE.BU_ID = string.Empty;
                resultedXml = _ObjLGABAL.LGA(_ObjLGABE, ReturnType.Get);
                _objLGAListBE = _ObjiDsignBAL.DeserializeFromXml<LGAListBE>(resultedXml);

                if (_objLGAListBE.items.Count > 0)
                {
                    divdownpaging.Visible = divpaging.Visible = true;
                    UCPaging1.Visible = UCPaging2.Visible = true;
                    hfTotalRecords.Value = _objLGAListBE.items[0].TotalRecords.ToString();
                    gvLGAList.DataSource = _objLGAListBE.items;
                    gvLGAList.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = _objLGAListBE.items.Count.ToString();
                    divdownpaging.Visible = divpaging.Visible = false;
                    UCPaging1.Visible = UCPaging2.Visible = false;
                    gvLGAList.DataSource = new DataTable(); ;
                    gvLGAList.DataBind();
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}