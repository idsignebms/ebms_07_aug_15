﻿<%@ Page Title=":: Add Marketer ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    AutoEventWireup="true" Theme="Green" CodeBehind="AddMarketers.aspx.cs" Inherits="UMS_Nigeria.Masters.AddMarketers" 
    MaintainScrollPositionOnPostback="true" %>
    
<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upAgencies" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <asp:UpdatePanel ID="upAddAgency" runat="server">
                    <ContentTemplate>
                        <div class="inner-sec">
                            <asp:Panel ID="pnlMessage" runat="server">
                                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                            </asp:Panel>
                            <div class="text_total">
                                <div class="text-heading">
                                    <asp:Literal ID="litAddAgency" runat="server" Text="Add Marketer"></asp:Literal>
                                </div>
                                <div class="star_text">
                                    <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="dot-line">
                            </div>
                            <div class="inner-box">
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                                            <span class="span_star">*</span></label><div class="space">
                                            </div>
                                        <asp:DropDownList ID="ddlBusinessUnitName" TabIndex="1" onchange="DropDownlistOnChangelbl(this,'spanddlBusinessUnitName',' Business Unit')"
                                            runat="server" CssClass="text-box text-select">
                                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                        <%--<span class="text-heading_2">
                                            <asp:HyperLink ID="lkbtnAddNewBusinessUnit" NavigateUrl="~/Masters/AddBusinessUnits.aspx"
                                                runat="server" Text="<%$ Resources:Resource, ADD%>"></asp:HyperLink></span>--%>
                                        <div class="space">
                                        </div>
                                        <span id="spanddlBusinessUnitName" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box2">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litName" runat="server" Text="<%$ Resources:Resource, F_NAME%>"></asp:Literal>
                                            <span class="span_star">*</span></label>
                                        <div class="space">
                                        </div>
                                        <asp:TextBox CssClass="text-box" ID="txtFName" TabIndex="2" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanFName','First Name')"
                                            MaxLength="99" placeholder="Enter First Name"></asp:TextBox>
                                        <span id="spanFName" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box3">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, M_NAME%>"></asp:Literal>
                                        </label>
                                        <div class="space">
                                        </div>
                                        <asp:TextBox CssClass="text-box" ID="txtMName" TabIndex="3" runat="server" MaxLength="99"
                                            placeholder="Enter Middle Name"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, L_NAME%>"></asp:Literal>
                                            <span class="span_star">*</span></label>
                                        <div class="space">
                                        </div>
                                        <asp:TextBox CssClass="text-box" ID="txtLName" TabIndex="4" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanLName','Last Name')"
                                            MaxLength="99" placeholder="Enter Last Name"></asp:TextBox>
                                        <span id="spanLName" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box2">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, CONTACT_NO%>"></asp:Literal>
                                            <span class="span_star">*</span></label>
                                        <div class="space">
                                        </div>
                                        <asp:TextBox CssClass="text-box" ID="txtCode1" TabIndex="5" runat="server" MaxLength="3"
                                            Width="11%" placeholder="Code" onblur="return TextBoxBlurValidationlbl(this,'spanContactNo','Code')"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtCode1"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:TextBox CssClass="text-box" ID="txtContactNo" Width="40%" TabIndex="6" runat="server"
                                            onblur="return TextBoxBlurValidationlbl(this,'spanContactNo','Contact No')" MaxLength="20"
                                            placeholder="Enter Contact No"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtContactNo"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanContactNo" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box3">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="litAnotherContactNo" runat="server" Text="<%$ Resources:Resource, ANOTHER_CONTACT_NO%>"></asp:Literal>
                                        </label>
                                        <div class="space">
                                        </div>
                                        <asp:TextBox CssClass="text-box" ID="txtCode2" TabIndex="7" runat="server" MaxLength="3"
                                            Width="11%" placeholder="Code"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtCode2"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:TextBox CssClass="text-box" ID="txtAnotherContactNo" Width="40%" TabIndex="8"
                                            runat="server" MaxLength="20" placeholder="Enter Alternate Contact No"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="ftbTxtAnotherContactNo" runat="server" TargetControlID="txtAnotherContactNo"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanAnotherNo" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, EMAIL_ID%>"></asp:Literal>
                                            <span class="span_star">*</span></label>
                                        <div class="space">
                                        </div>
                                        <asp:TextBox CssClass="text-box" ID="txtEmail" runat="server" TabIndex="9" MaxLength="500"
                                            onblur="return TextBoxBlurValidationlbl(this,'spanEmail','Email Id')" placeholder="Enter Email Id"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="ftbPrimaryEmail" runat="server" TargetControlID="txtEmail"
                                            ValidChars="@._" FilterType="LowercaseLetters,UppercaseLetters,Numbers,Custom">
                                        </asp:FilteredTextBoxExtender>
                                        <span id="spanEmail" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box2">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, CITY%>"></asp:Literal>
                                            <span class="span_star">*</span></label>
                                        <div class="space">
                                        </div>
                                        <asp:TextBox CssClass="text-box" ID="txtCity" TabIndex="10" onblur="return TextBoxBlurValidationlbl(this,'spanCity','City')"
                                            runat="server" MaxLength="50" placeholder="Enter City"></asp:TextBox>
                                        <span class="span_color" id="spanCity"></span>
                                    </div>
                                </div>
                                <div class="inner-box3">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, ZIP%>"></asp:Literal>
                                            <%--<span class="span_star">*</span>--%></label>
                                        <div class="space">
                                        </div>
                                        <asp:TextBox CssClass="text-box" ID="txtZip" TabIndex="11" 
                                            runat="server" MaxLength="10" placeholder="Enter City"></asp:TextBox><%--onblur="return TextBoxBlurValidationlbl(this,'spanZip','Zip Code')"--%>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtZip"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                        <span class="span_color" id="spanZip"></span>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, ADDRESS_1%>"></asp:Literal>
                                            <span class="span_star">*</span></label>
                                        <div class="space">
                                        </div>
                                        <asp:TextBox CssClass="text-box" ID="txtAddress1" TextMode="MultiLine" TabIndex="12"
                                            runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanAddress1','Address1')"
                                            placeholder="Enter Address1"></asp:TextBox>
                                        <span id="spanAddress1" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="inner-box2">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, ADDRESS_2%>"></asp:Literal>
                                        </label>
                                        <div class="space">
                                        </div>
                                        <asp:TextBox CssClass="text-box" ID="txtAddress2" TabIndex="13" runat="server" TextMode="MultiLine"
                                            placeholder="Enter Address2"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="inner-box3">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:Resource, PHOTO%>"></asp:Literal>
                                        </label>
                                        <div class="space">
                                        </div>
                                        <asp:FileUpload ID="fupPhoto" TabIndex="14" runat="server" onchange="return filevalidate(this.value,'Photo')" />
                                        <div class="space">
                                        </div>
                                        <span id="spanPhoto" class="span_color"></span>
                                    </div>
                                </div>
                                <div style="clear: both;">
                                </div>
                            </div>
                            <br />
                            <div class="box_total">
                                <div class="box_total_a">
                                    <asp:Button ID="btnSave" TabIndex="15" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                        CssClass="box_s" runat="server" OnClick="btnSave_Click" />
                                </div>
                                <div class="grid_boxes">
                                    <div class="grid_pagingTwo_top">
                                        <div class="paging_top_title">
                                            <asp:Literal ID="litMarketersList" runat="server" Text="Marketers List"></asp:Literal>
                                        </div>
                                        <div class="paging_top_rightTwo_content" id="divpaging" runat="server">
                                            <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="grid_tb" id="divgrid" runat="server">
                                    <asp:GridView ID="gvMarketersList" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="grid_tb_hd"
                                        OnRowCommand="gvMarketersList_OnRowCommand" OnRowDataBound="gvMarketersList_OnRowDataBound">
                                        <EmptyDataRowStyle HorizontalAlign="Center" />
                                        <EmptyDataTemplate>
                                            There is no data.
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                                HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                                    <asp:Label ID="lblActiveStatusId" Visible="false" runat="server" Text='<%#Eval("ActiveStatusId")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField HeaderText="<%$ Resources:Resource, BU_ID%>" ItemStyle-Width="10%"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBU_ID" runat="server" Text='<%#Eval("BU_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, F_NAME%>" ItemStyle-Width="10%"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFirstName" runat="server" Text='<%#Eval("FirstName") %>'></asp:Label>
                                                    <asp:TextBox CssClass="text-box" ID="txtGridFirstName" MaxLength="50" placeholder="Enter First Name"
                                                        Visible="false" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, M_NAME%>" ItemStyle-Width="10%"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMiddleName" runat="server" Text='<%#Eval("MiddleName") %>'></asp:Label>
                                                    <asp:TextBox CssClass="text-box" ID="txtGridMiddleName" MaxLength="50" placeholder="Enter First Name"
                                                        Visible="false" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, L_NAME%>" ItemStyle-Width="10%"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLastName" runat="server" Text='<%#Eval("LastName") %>'></asp:Label>
                                                    <asp:TextBox CssClass="text-box" ID="txtGridLastName" MaxLength="50" placeholder="Enter First Name"
                                                        Visible="false" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, CONTACT_NO%>" ItemStyle-Width="10%"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblContactNo1" runat="server" Text='<%#Eval("ContactNo1") %>'></asp:Label>
                                                    <asp:TextBox CssClass="text-box" ID="txtGridCode1" runat="server" MaxLength="3" Width="11%"
                                                        placeholder="Code" Visible="false"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtGridCode1"
                                                        FilterType="Numbers" ValidChars=" ">
                                                    </asp:FilteredTextBoxExtender>
                                                    <asp:TextBox CssClass="text-box" ID="txtGridContactNo1" runat="server" MaxLength="15"
                                                        placeholder="Enter Contact No" Visible="false"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="ftbGridTxtContactNo7" runat="server" TargetControlID="txtGridContactNo1"
                                                        FilterType="Numbers" ValidChars=" ">
                                                    </asp:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, ANOTHER_CONTACT_NO%>" ItemStyle-Width="10%"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblContactNo2" runat="server" Text='<%#Eval("ContactNo2") %>'></asp:Label>
                                                    <asp:TextBox CssClass="text-box" ID="txtGridCode2" Visible="false" runat="server"
                                                        MaxLength="3" Width="11%" placeholder="Code"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtGridCode2"
                                                        FilterType="Numbers" ValidChars=" ">
                                                    </asp:FilteredTextBoxExtender>
                                                    <asp:TextBox CssClass="text-box" ID="txtGridContactNo2" MaxLength="15" placeholder="Enter Alternate Contact No"
                                                        runat="server" Visible="false">
                                                    </asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="ftbTxtGridContactNo5" runat="server" TargetControlID="txtGridContactNo2"
                                                        FilterType="Numbers" ValidChars=" ">
                                                    </asp:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, EMAIL_ID%>" ItemStyle-Width="10%"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmailId" runat="server" Text='<%#Eval("EmailId") %>'></asp:Label>
                                                    <asp:TextBox CssClass="text-box" ID="txtGridEmailId" MaxLength="50" placeholder="Enter Email ID"
                                                        Visible="false" runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                            
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, CITY%>" ItemStyle-Width="8%"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCity" runat="server" Text='<%#Eval("City") %>'></asp:Label>
                                                    <asp:TextBox CssClass="text-box" ID="txtGridCity" MaxLength="50" placeholder="Enter City"
                                                        runat="server" Visible="false"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, ZIP%>" ItemStyle-Width="8%"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblZip" runat="server" Text='<%#Eval("ZipCode") %>'></asp:Label>
                                                    <asp:TextBox CssClass="text-box" ID="txtGridZip" MaxLength="10" placeholder="Enter Zip"
                                                        runat="server" Visible="false"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtGridZip"
                                                        FilterType="Numbers" ValidChars=" ">
                                                    </asp:FilteredTextBoxExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, ADDRESS_1%>" ItemStyle-Width="8%"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAddress1" runat="server" Text='<%#Eval("Address1") %>'></asp:Label>
                                                    <asp:TextBox CssClass="text-box" ID="txtGridAddress1" MaxLength="99" placeholder="Enter Address1"
                                                        runat="server" Visible="false"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, ADDRESS_2%>" ItemStyle-Width="8%"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAddress2" runat="server" Text='<%#Eval("Address2") %>'></asp:Label>
                                                    <asp:TextBox CssClass="text-box" ID="txtGridAddress2" MaxLength="99" placeholder="Enter Address2"
                                                        runat="server" Visible="false"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, PHOTO%>" ItemStyle-Width="8%"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                                <ItemTemplate>
                                                    <asp:Label Visible="False" ID="lblGridPhoto" runat="server" Text='<%#Eval("Photo") %>'></asp:Label>
                                                    <asp:Image ID="imgMarketer" runat="server" Style="border: 1px solid #000; width: 150px;
                                                        height: 150px;" ImageUrl="../images/user_default_icon.png" />
                                                    <asp:FileUpload ID="fupGridPhoto" runat="server" Visible="False" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="8%"
                                                ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:UpdatePanel runat="server"><ContentTemplate>
                                                    <asp:Label ID="lblMarketerId" Visible="False" runat="server" Text='<%#Eval("MarketerId")%>'></asp:Label>
                                                    <asp:LinkButton ID="lkbtnMarketerEdit" runat="server" ToolTip="Edit" Text="Edit"
                                                        CommandName="EditMarketer">
                                                        <img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                                                    <asp:LinkButton ID="lkbtnMarketerUpdate" Visible="false" runat="server" ToolTip="Update"
                                                        CommandName="UpdateMarketer"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                                    <asp:LinkButton ID="lkbtnMarketerCancel" Visible="false" runat="server" ToolTip="Cancel"
                                                        CommandName="CancelMarketer"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                                    <asp:LinkButton ID="lkbtnMarketerActive" Visible="false" runat="server" ToolTip="Activate Marketer"
                                                        CommandName="ActiveMarketer"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                                    <asp:LinkButton ID="lkbtnDelete" runat="server" ToolTip="DeActivate Marketer" CommandName="DeActiveMarketer">
                                                        <img src="../images/Deactivate.gif"   alt="DeActive" /></asp:LinkButton>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                        <asp:PostBackTrigger ControlID="lkbtnMarketerUpdate"/></Triggers>
                                                        </asp:UpdatePanel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:HiddenField ID="hfMarketerId" runat="server" />
                                    <asp:HiddenField ID="hfMarketerName" runat="server" />
                                    <asp:HiddenField ID="hfPageNo" runat="server" />
                                    <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                                    <asp:HiddenField ID="hfLastPage" runat="server" />
                                    <asp:HiddenField ID="hfRecognize" runat="server" />
                                    <asp:HiddenField ID="hfPageSize" runat="server" />
                                </div>
                                <div class="grid_boxes">
                                    <div id="divdownpaging" runat="server">
                                        <div class="grid_paging_bottom">
                                            <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnSave" />
                        <%--<asp:PostBackTrigger ControlID="lkbtnMarketerUpdate" />--%>
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <%-- Active Country Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                            TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="PanelActivate" runat="server" CssClass="modalPopup"
                            Style="display: none;">
                            <div class="popheader popheaderlblgreen">
                                <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                            </div>
                            <div class="space">
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btnActiveOk" OnClick="btnActiveOk_OnClick" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                    <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Active Country Btn popup Closed--%>
                        <%-- DeActive Country Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                            TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnDeActivate" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="PanelDeActivate" runat="server" CssClass="modalPopup"
                            Style="display: none; border-color: #639B00;">
                            <div class="popheader" style="background-color: red;">
                                <asp:Label ID="lblDeActive" runat="server"></asp:Label>
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <div class="space">
                                    </div>
                                    <asp:Button ID="btnDeActiveOk" OnClick="btnDeActiveOk_OnClick" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                    <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- DeActive Country popup Closed--%>
            <%-- Grid Alert popup Start--%>
            <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
            </asp:ModalPopupExtender>
            <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
            <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                <div class="popheader popheaderlblred">
                    <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                </div>
                <div class="footer popfooterbtnrt">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                            CssClass="ok_btn" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
            <%-- Grid Alert popup Closed--%>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <script src="../JavaScript/PageValidations/AddMarketers.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
     <script language="javascript" type="text/javascript">
         $(document).ready(function () { assignDiv('rptrMainMasterMenu_156_0', '117') });
    </script>

</asp:Content>
