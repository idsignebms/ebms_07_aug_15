﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using Resources;
using iDSignHelper;
using System.Threading;
using System.Globalization;
using System.Xml;
using System.Configuration;
using System.IO;
using System.Data;

namespace UMS_Nigeria.Masters
{
    public partial class AddMarketers : System.Web.UI.Page
    {
        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
        }
        #endregion

        #region Members
        MarketersBal _objMarketersBal = new MarketersBal();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        CommonDAL _ObjCommonDAL = new CommonDAL();
        XmlDocument xml = new XmlDocument();
        public int PageNum;
        string Key = "AddMarketers";

        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE, false);

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                InsertMarketers();

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = _ObjCommonMethods.GetPagePath(this.Request.Url);
                //if (_ObjCommonMethods.IsAccess(path))
                //{
                BindDropDowns();
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
                //}
                //else
                //{
                //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                //}
            }
        }

        private void BindDropDowns()
        {
            try
            {
                _ObjCommonMethods.BindBusinessUnits(ddlBusinessUnitName, string.Empty, true);
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                {
                    ddlBusinessUnitName.SelectedIndex = ddlBusinessUnitName.Items.IndexOf(ddlBusinessUnitName.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                    ddlBusinessUnitName.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods
        protected void InsertMarketers()
        {
            try
            {
                MarketersBe _objMarketersBe = new MarketersBe();
                _objMarketersBe.FirstName = txtFName.Text;
                _objMarketersBe.MiddleName = txtMName.Text;
                _objMarketersBe.LastName = txtLName.Text;
                _objMarketersBe.ContactNo1 = txtCode1.Text + "-" + txtContactNo.Text;
                if (txtAnotherContactNo.Text != string.Empty)
                    _objMarketersBe.ContactNo2 = txtCode2.Text + "-" + txtAnotherContactNo.Text;
                _objMarketersBe.EmailId = txtEmail.Text;
                _objMarketersBe.Address1 = txtAddress1.Text;
                _objMarketersBe.Address2 = txtAddress2.Text;
                _objMarketersBe.City = txtCity.Text;
                _objMarketersBe.ZipCode = txtZip.Text;
                _objMarketersBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _objMarketersBe.BU_ID = ddlBusinessUnitName.SelectedValue;
                string PhotoFileName = string.Empty;
                if (fupPhoto.FileContent.Length > 0)
                {
                    PhotoFileName = System.IO.Path.GetFileName(fupPhoto.FileName).ToString();//Assinging FileName
                    string PhotoExtension = Path.GetExtension(PhotoFileName);//Storing Extension of file into variable Extension
                    TimeZoneInfo PhotoIND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                    DateTime PhotocurrentTime = DateTime.Now;
                    DateTime Photoourtime = TimeZoneInfo.ConvertTime(PhotocurrentTime, PhotoIND_ZONE);
                    string PhotoCurrentDate = Photoourtime.ToString("dd-MM-yyyy_hh-mm-ss");
                    string PhotoModifiedFileName = Path.GetFileNameWithoutExtension(PhotoFileName) + "_" + PhotoCurrentDate + PhotoExtension;//Setting File Name to store it in database
                    fupPhoto.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["UserPhotos"].ToString()) + PhotoModifiedFileName);//Saving File in to the server.
                    _objMarketersBe.Photo = PhotoModifiedFileName;
                }

                XmlDocument xml = new XmlDocument();
                xml = _objMarketersBal.InsertMarketer(_objMarketersBe, Statement.Insert);

                _objMarketersBe = _ObjiDsignBAL.DeserializeFromXml<MarketersBe>(xml);
                if (_objMarketersBe.IsSuccess)
                {
                    txtCode1.Text = txtCode2.Text = txtFName.Text = txtMName.Text = txtLName.Text = txtContactNo.Text = txtAnotherContactNo.Text = txtEmail.Text = txtAddress1.Text = txtAddress2.Text = txtCity.Text = txtZip.Text = string.Empty;
                    BindGrid();
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(Resource.MARKETER_INSERT, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else if (_objMarketersBe.IsEmailIdExists)
                {
                    Message(Resource.EMAIL_ID_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                {
                    Message(Resource.MARKETER_FAILD, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        public void BindGrid()
        {
            try
            {
                MarketersBe _objMarketersBe = new MarketersBe();
                MarketersListBe _objMarketersListBe = new MarketersListBe();

                XmlDocument resultedXml = new XmlDocument();

                _objMarketersBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                _objMarketersBe.PageSize = PageSize;
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    _objMarketersBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    _objMarketersBe.BU_ID = string.Empty;
                resultedXml = _objMarketersBal.GetMarketer(_objMarketersBe, ReturnType.List);
                _objMarketersListBe = _ObjiDsignBAL.DeserializeFromXml<MarketersListBe>(resultedXml);

                if (_objMarketersListBe.Items.Count > 0)
                {
                    divdownpaging.Visible = divpaging.Visible = true;
                    UCPaging1.Visible = UCPaging2.Visible = true;
                    hfTotalRecords.Value = _objMarketersListBe.Items[0].TotalRecords.ToString();
                    gvMarketersList.DataSource = _objMarketersListBe.Items;
                    gvMarketersList.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = _objMarketersListBe.Items.Count.ToString();
                    divdownpaging.Visible = divpaging.Visible = false;
                    UCPaging1.Visible = UCPaging2.Visible = false;
                    gvMarketersList.DataSource = new DataTable(); ;
                    gvMarketersList.DataBind();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        protected void gvMarketersList_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                MarketersListBe _ObjMarketersListBe = new MarketersListBe();
                MarketersBe _ObjMarketersBe = new MarketersBe();
                DataSet ds = new DataSet();


                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridFirstName = (TextBox)row.FindControl("txtGridFirstName");
                TextBox txtGridMiddleName = (TextBox)row.FindControl("txtGridMiddleName");
                TextBox txtGridLastName = (TextBox)row.FindControl("txtGridLastName");
                TextBox txtGridEmailId = (TextBox)row.FindControl("txtGridEmailId");
                TextBox txtGridContactNo1 = (TextBox)row.FindControl("txtGridContactNo1");
                TextBox txtGridContactNo2 = (TextBox)row.FindControl("txtGridContactNo2");
                TextBox txtGridCode1 = (TextBox)row.FindControl("txtGridCode1");
                TextBox txtGridCode2 = (TextBox)row.FindControl("txtGridCode2");
                TextBox txtGridAddress1 = (TextBox)row.FindControl("txtGridAddress1");
                TextBox txtGridAddress2 = (TextBox)row.FindControl("txtGridAddress2");
                TextBox txtGridCity = (TextBox)row.FindControl("txtGridCity");
                TextBox txtGridZip = (TextBox)row.FindControl("txtGridZip");

                Label lblMarketerId = (Label)row.FindControl("lblMarketerId");
                Label lblFirstName = (Label)row.FindControl("lblFirstName");
                Label lblMiddleName = (Label)row.FindControl("lblMiddleName");
                Label lblLastName = (Label)row.FindControl("lblLastName");
                Label lblEmailId = (Label)row.FindControl("lblEmailId");
                Label lblContactNo1 = (Label)row.FindControl("lblContactNo1");
                Label lblContactNo2 = (Label)row.FindControl("lblContactNo2");
                Label lblAddress1 = (Label)row.FindControl("lblAddress1");
                Label lblAddress2 = (Label)row.FindControl("lblAddress2");
                Label lblCity = (Label)row.FindControl("lblCity");
                Label lblZip = (Label)row.FindControl("lblZip");

                Label lblGridPhoto = (Label)row.FindControl("lblGridPhoto");
                Image imgMarketer = (Image)row.FindControl("imgMarketer");
                FileUpload fupGridPhoto = (FileUpload)row.FindControl("fupGridPhoto");


                //ScriptManager.GetCurrent(this).RegisterPostBackControl(fupGridPhoto);


                hfMarketerId.Value = lblMarketerId.Text;

                hfRecognize.Value = string.Empty;
                switch (e.CommandName.ToUpper())
                {
                    case "EDITMARKETER":
                        lblFirstName.Visible = lblLastName.Visible = lblMiddleName.Visible = lblEmailId.Visible =
                            lblAddress1.Visible = lblAddress2.Visible = lblCity.Visible = lblZip.Visible =
                            lblContactNo1.Visible = lblContactNo2.Visible = imgMarketer.Visible = false;

                        txtGridFirstName.Visible = txtGridMiddleName.Visible = txtGridLastName.Visible = txtGridEmailId.Visible =
                            txtGridAddress1.Visible = txtGridAddress2.Visible = txtGridCity.Visible = txtGridZip.Visible =
                            txtGridCode1.Visible = txtGridCode2.Visible = txtGridContactNo1.Visible = txtGridContactNo2.Visible =
                            fupGridPhoto.Visible = true;

                        txtGridFirstName.Text = lblFirstName.Text;
                        txtGridMiddleName.Text = lblMiddleName.Text;
                        txtGridLastName.Text = lblLastName.Text;
                        txtGridEmailId.Text = lblEmailId.Text;

                        if (lblContactNo1.Text.Length > 0 && lblContactNo1.Text != "--")
                        {
                            string[] contactNo = lblContactNo1.Text.Split('-');
                            if (contactNo.Length < 2)
                            {
                                txtGridCode1.Text = lblContactNo1.Text.Substring(0, 3);
                                txtGridContactNo1.Text = lblContactNo1.Text.Remove(0, 3);
                            }
                            else
                            {
                                txtGridCode1.Text = contactNo[0].ToString();
                                txtGridContactNo1.Text = contactNo[1].ToString();
                            }
                        }
                        else
                            txtGridCode1.Text = txtGridContactNo1.Text = string.Empty;

                        if (lblContactNo2.Text.Length > 0 && lblContactNo2.Text != "--")
                        {
                            string[] contactNo = lblContactNo2.Text.Split('-');
                            if (contactNo.Length < 2)
                            {
                                txtGridCode2.Text = lblContactNo2.Text.Substring(0, 3);
                                txtGridContactNo2.Text = lblContactNo2.Text.Remove(0, 3);
                            }
                            else
                            {
                                txtGridCode2.Text = contactNo[0].ToString();
                                txtGridContactNo2.Text = contactNo[1].ToString();
                            }
                        }
                        else
                            txtGridCode2.Text = txtGridContactNo2.Text = string.Empty;

                        txtGridAddress1.Text = lblAddress1.Text;
                        txtGridAddress2.Text = lblAddress2.Text;
                        txtGridCity.Text = lblCity.Text;
                        txtGridZip.Text = lblZip.Text;

                        row.FindControl("lkbtnMarketerCancel").Visible = row.FindControl("lkbtnMarketerUpdate").Visible = true;
                        row.FindControl("lkbtnDelete").Visible = row.FindControl("lkbtnMarketerEdit").Visible = false;
                        break;
                    case "CANCELMARKETER":
                        row.FindControl("lkbtnDelete").Visible = row.FindControl("lkbtnMarketerEdit").Visible = true;
                        row.FindControl("lkbtnMarketerCancel").Visible = row.FindControl("lkbtnMarketerUpdate").Visible = false;
                        txtGridFirstName.Visible = txtGridMiddleName.Visible = txtGridLastName.Visible = txtGridEmailId.Visible =
                            txtGridAddress1.Visible = txtGridAddress2.Visible = txtGridCity.Visible = txtGridZip.Visible =
                            txtGridCode1.Visible = txtGridCode2.Visible = txtGridContactNo1.Visible = txtGridContactNo2.Visible =
                            fupGridPhoto.Visible = false;
                        lblFirstName.Visible = lblLastName.Visible = lblMiddleName.Visible = lblEmailId.Visible =
                            lblAddress1.Visible = lblAddress2.Visible = lblCity.Visible = lblZip.Visible =
                                lblContactNo1.Visible = lblContactNo2.Visible = imgMarketer.Visible = true;
                        break;
                    case "UPDATEMARKETER":
                        //xml = _ObjUMS_Service.UpdateStates(txtGridStateName.Text, lblStateCode.Text, txtGridDisplayCode.Text, ddlGridCountryName.SelectedValue, _ObjiDsignBAL.ReplaceNewLines(txtGridNotes.Text, true));
                        _ObjMarketersBe.FirstName = txtGridFirstName.Text.Trim();
                        _ObjMarketersBe.MiddleName = txtGridMiddleName.Text.Trim();
                        _ObjMarketersBe.LastName = txtGridLastName.Text.Trim();
                        _ObjMarketersBe.EmailId = txtGridEmailId.Text.Trim();
                        _ObjMarketersBe.ContactNo1 = txtGridCode1.Text.Trim() + "-" + txtGridContactNo1.Text.Trim();
                        _ObjMarketersBe.ContactNo2 = txtGridCode2.Text.Trim() + "-" + txtGridContactNo2.Text.Trim();
                        _ObjMarketersBe.Address1 = txtGridAddress1.Text.Trim();
                        _ObjMarketersBe.Address2 = txtGridAddress2.Text.Trim();
                        _ObjMarketersBe.City = txtGridCity.Text.Trim();
                        _ObjMarketersBe.ZipCode = txtGridZip.Text.Trim();

                        if (fupGridPhoto.HasFile)
                        {
                            string PhotoFileName = string.Empty;
                            if (fupGridPhoto.FileContent.Length > 0)
                            {
                                PhotoFileName = System.IO.Path.GetFileName(fupGridPhoto.FileName).ToString();//Assinging FileName
                                string PhotoExtension = Path.GetExtension(PhotoFileName);//Storing Extension of file into variable Extension
                                TimeZoneInfo PhotoIND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                                DateTime PhotocurrentTime = DateTime.Now;
                                DateTime Photoourtime = TimeZoneInfo.ConvertTime(PhotocurrentTime, PhotoIND_ZONE);
                                string PhotoCurrentDate = Photoourtime.ToString("dd-MM-yyyy_hh-mm-ss");
                                string PhotoModifiedFileName = Path.GetFileNameWithoutExtension(PhotoFileName) + "_" + PhotoCurrentDate + PhotoExtension;//Setting File Name to store it in database
                                fupGridPhoto.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["UserPhotos"].ToString()) + PhotoModifiedFileName);//Saving File in to the server.
                                _ObjMarketersBe.Photo = PhotoModifiedFileName;
                            }
                        }
                        else
                        {
                            _ObjMarketersBe.Photo = lblGridPhoto.Text;
                        }

                        _ObjMarketersBe.MarketerId = Convert.ToInt32(lblMarketerId.Text);
                        _ObjMarketersBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();

                        xml = _objMarketersBal.InsertMarketer(_ObjMarketersBe, Statement.Update);
                        _ObjMarketersBe = _ObjiDsignBAL.DeserializeFromXml<MarketersBe>(xml);

                        if (_ObjMarketersBe.RowsEffected > 0)
                        {
                            BindGrid();
                            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            Message("Marketer Updated Successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);

                            txtGridFirstName.Text = txtGridMiddleName.Text = txtGridLastName.Text =
                                txtGridEmailId.Text = txtGridAddress1.Text = txtGridAddress2.Text =
                                    txtGridCity.Text = txtGridZip.Text = string.Empty;
                        }
                        else if (_ObjMarketersBe.IsEmailIdExists)
                        {
                            Message("Marketer Email Id already exists.", UMS_Resource.MESSAGETYPE_ERROR);
                            txtGridEmailId.Focus();
                        }
                        else
                            Message("Marketer updation failed.", UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "ACTIVEMARKETER":
                        //xml = _ObjUMS_Service.DeleteActiveState(1, lblStateCode.Text);
                        mpeActivate.Show();
                        lblActiveMsg.Text = "Are you sure you want to Activate this Marketer?";
                        btnActiveOk.Focus();

                        break;
                    case "DEACTIVEMARKETER":
                        //xml = _ObjUMS_Service.DeleteActiveState(0, lblStateCode.Text);
                        mpeDeActive.Show();
                        //hfStateCode.Value = lblStateCode.Text;
                        lblDeActive.Text = "Are you sure you want to DeActivate this Marketer?";
                        btnDeActiveOk.Focus();

                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvMarketersList_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            Page.Form.Attributes.Add("enctype", "multipart/form-data");
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnMarketerUpdate = (LinkButton)e.Row.FindControl("lkbtnMarketerUpdate");
                    TextBox txtGridFirstName = (TextBox)e.Row.FindControl("txtGridFirstName");
                    TextBox txtGridLastName = (TextBox)e.Row.FindControl("txtGridLastName");
                    TextBox txtGridEmailId = (TextBox)e.Row.FindControl("txtGridEmailId");
                    TextBox txtGridContactNo1 = (TextBox)e.Row.FindControl("txtGridContactNo1");
                    TextBox txtGridContactNo2 = (TextBox)e.Row.FindControl("txtGridContactNo2");
                    TextBox txtGridCode1 = (TextBox)e.Row.FindControl("txtGridCode1");
                    TextBox txtGridCode2 = (TextBox)e.Row.FindControl("txtGridCode2");
                    TextBox txtGridAddress1 = (TextBox)e.Row.FindControl("txtGridAddress1");
                    TextBox txtGridAddress2 = (TextBox)e.Row.FindControl("txtGridAddress2");
                    TextBox txtGridCity = (TextBox)e.Row.FindControl("txtGridCity");
                    TextBox txtGridZip = (TextBox)e.Row.FindControl("txtGridZip");

                    FileUpload fupGridPhoto = (FileUpload)e.Row.FindControl("fupGridPhoto");


                    //PostBackTrigger pst = new PostBackTrigger();
                    //pst.ControlID = fupGridPhoto.UniqueID;
                    //this.upAgencies.Triggers.Add(pst);


                    //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(fupGridPhoto);
                    //upAddAgency.Update();
                    //TextBox txtGridDisplayCode = (TextBox)e.Row.FindControl("txtGridDisplayCode");

                    lkbtnMarketerUpdate.Attributes.Add("onclick",
                        "return UpdateValidation('" + txtGridFirstName.ClientID + "','"
                                                    + txtGridLastName.ClientID + "','"
                                                    + txtGridEmailId.ClientID + "','"
                                                    + txtGridContactNo1.ClientID + "','"
                                                    + txtGridContactNo2.ClientID + "','"
                                                    + txtGridCode1.ClientID + "','"
                                                    + txtGridCode2.ClientID + "','"
                                                    + txtGridAddress1.ClientID + "','"
                                                    + txtGridCity.ClientID + "','"
                                                    + txtGridZip.ClientID + "','"
                                                    + fupGridPhoto.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");
                    Label lblGridPhoto = (Label)e.Row.FindControl("lblGridPhoto");
                    Image imgMarketer = (Image)e.Row.FindControl("imgMarketer");
                    if (!string.IsNullOrEmpty(lblGridPhoto.Text))
                        imgMarketer.ImageUrl = "~/Uploads/UserPhotos/" + lblGridPhoto.Text;

                    if (Convert.ToInt32(lblActiveStatusId.Text) == 2)
                    {
                        e.Row.FindControl("lkbtnDelete").Visible = e.Row.FindControl("lkbtnMarketerEdit").Visible = false;
                        e.Row.FindControl("lkbtnMarketerActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDelete").Visible = e.Row.FindControl("lkbtnMarketerEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnActiveOk_OnClick(object sender, EventArgs e)
        {
            try
            {
                MarketersBe _ObjMarketersBe = new MarketersBe();
                _ObjMarketersBe.MarketerId = Convert.ToInt32(hfMarketerId.Value);
                _ObjMarketersBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _ObjMarketersBe.ActiveStatusId = 1;

                xml = _objMarketersBal.InsertMarketer(_ObjMarketersBe, Statement.Delete);
                _ObjMarketersBe = _ObjiDsignBAL.DeserializeFromXml<MarketersBe>(xml);

                if (_ObjMarketersBe.RowsEffected > 0)
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message("Marketer activated successfully", UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                    Message("Marketer activation failed", UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_OnClick(object sender, EventArgs e)
        {
            try
            {
                MarketersBe _ObjMarketersBe = new MarketersBe();
                _ObjMarketersBe.MarketerId = Convert.ToInt32(hfMarketerId.Value);
                _ObjMarketersBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _ObjMarketersBe.ActiveStatusId = 2;
                xml = _objMarketersBal.InsertMarketer(_ObjMarketersBe, Statement.Delete);
                _ObjMarketersBe = _ObjiDsignBAL.DeserializeFromXml<MarketersBe>(xml);
                if (_ObjMarketersBe.RowsEffected > 0)
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message("Marketer Deactivated Successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                {
                    //if (_ObjMarketersBe.AssciatedStateCount > 0)
                    //{
                    //    lblgridalertmsg.Text = "LGA can not be deactivated as it contains " +
                    //                           _ObjMarketersBe.AssciatedStateCount + " States.";
                    //    mpegridalert.Show();
                    //}
                    //else
                    Message("Marketer Deactivation Failed.", UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

    }
}