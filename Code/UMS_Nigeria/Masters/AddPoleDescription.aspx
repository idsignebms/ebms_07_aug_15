﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master" Theme="Green"
    Title=":: Add Pole ::" AutoEventWireup="true" CodeBehind="AddPoleDescription.aspx.cs"
    Inherits="UMS_Nigeria.Masters.AddPoleDescription" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdatePanel ID="upAddPoleDescription" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddPole" runat="server" Text="Add Pole Information"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litPoleMaster" runat="server" Text="<%$ Resources:Resource, POLE_NAME%>"></asp:Literal>
                                    <span class="span_star">*</span></label><br />
                                <asp:DropDownList ID="ddlPoleName" TabIndex="1" AutoPostBack="True" onchange="DropDownlistOnChangelbl(this,'spanddlPoleName',' Pole Name')"
                                    runat="server" OnSelectedIndexChanged="ddlPoleName_SelectedIndexChanged" CssClass="text-box">
                                </asp:DropDownList>
                                <span class="text-heading_2">
                                    <%--<asp:HyperLink ID="lkbtnAddNewState" runat="server" NavigateUrl="~/Masters/AddPoleManagement.aspx" 
                                        Text="<%$ Resources:Resource, ADD%>"></asp:HyperLink>--%><br />
                                </span><span id="spanddlPoleName" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner" id="divParent" runat="server">
                                <label for="name">
                                    <asp:Literal ID="ltlParent" runat="server" Text="<%$ Resources:Resource, PARENT%>"></asp:Literal>
                                    <span class="span_star">*</span></label><br />
                                <asp:DropDownList ID="ddlParent" TabIndex="2" onchange="DropDownlistOnChangelbl(this,'spanddlParent',' Parent')"
                                    runat="server" CssClass="text-box">
                                </asp:DropDownList>
                                <div class="space">
                                </div>
                                <span id="spanddlParent" class="span_color"></span>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="Name">
                                    <asp:Literal ID="litName" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                                    <span class="span_star">*</span></label><div class="space">
                                    </div>
                                <asp:TextBox CssClass="text-box" ID="txtName" TabIndex="6" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanName','Name')"
                                    MaxLength="50" placeholder="Enter Name"></asp:TextBox>
                                <span id="spanName" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="Description">
                                    <asp:Literal ID="litDescription" runat="server" Text="<%$ Resources:Resource, DESCRIPTION%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:TextBox CssClass="text-box" ID="txtDescription" TabIndex="5" runat="server"
                                    MaxLength="150" TextMode="MultiLine" placeholder="Enter Description"></asp:TextBox>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <label for="Comments">
                                    <asp:Literal ID="litComments" runat="server" Text="<%$ Resources:Resource, COMMENTS%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:TextBox CssClass="text-box" ID="txtComments" TabIndex="5" runat="server" MaxLength="150"
                                    TextMode="MultiLine" placeholder="Enter Comments"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="box_total">
                        <div class="box_totalTwi_c">
                            <br />
                            <asp:Button ID="btnSave" TabIndex="4" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                CssClass="box_s" runat="server" OnClick="btnSave_Click" />
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="grid_boxes">
                        <div class="grid_pagingTwo_top">
                            <div class="paging_top_title">
                                <asp:Literal ID="litLGA" runat="server" Text="<%$ Resources:Resource, POLES_LIST %>"></asp:Literal>
                            </div>
                            <div class="paging_top_rightTwo_content" id="divpaging" runat="server">
                                <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvPoleDescriptionList" runat="server" AutoGenerateColumns="False"
                            OnRowDataBound="gvPoleDescriptionList_RowDataBound" OnRowCommand="gvPoleDescriptionList_RowCommand"
                            HeaderStyle-CssClass="grid_tb_hd">
                            <EmptyDataRowStyle HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                    HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        <asp:Label ID="lblActiveStatusId" Visible="false" runat="server" Text='<%#Eval("ActiveStatusId")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, POLE_NAME%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPoleMasterId" Visible="false" runat="server" Text='<%#Eval("PoleMasterId") %>'></asp:Label>
                                        <%--<asp:DropDownList ID="ddlGridPoelName" Visible="false" runat="server" AutoPostBack="True" >
                                        </asp:DropDownList>--%>
                                        <asp:Label ID="lblPoleName" runat="server" Text='<%#Eval("Master") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, PARENT%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPoleOrder" Visible="false" runat="server" Text='<%#Eval("PoleOrder") %>'></asp:Label>
                                        <%--<asp:DropDownList ID="ddlGridParent" Visible="false" runat="server">
                                        </asp:DropDownList>--%>
                                        <asp:Label ID="lblParent" runat="server" Text='<%#Eval("Parent") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridName" MaxLength="50" placeholder="Enter Name"
                                            Visible="false" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, CODE%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCode" runat="server" Text='<%#Eval("Code")%>'></asp:Label>
                                        <%--<asp:TextBox CssClass="text-box" ID="txtGridCode" MaxLength="50" placeholder="Enter Code"
                                            Visible="false" runat="server"></asp:TextBox>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, DESCRIPTION%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDescription" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridDescription" MaxLength="150" TextMode="MultiLine"
                                            placeholder="Enter Description" Visible="false" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, COMMENTS%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblComments" runat="server" Text='<%#Eval("Comments") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridComments" MaxLength="150" TextMode="MultiLine"
                                            placeholder="Enter Comments" Visible="false" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPoleId" Visible="False" Text='<%#Eval("PoleId")%>' runat="server"></asp:Label>
                                        <asp:LinkButton ID="lkbtnPoleDescriptionEdit" runat="server" ToolTip="Edit" Text="Edit"
                                            CommandName="EditPoleDescription">
                                        <img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnPoleDescriptionUpdate" Visible="false" runat="server" ToolTip="Update"
                                            CommandName="UpdatePoleDescription"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnPoleDescriptionCancel" Visible="false" runat="server" ToolTip="Cancel"
                                            CommandName="CancelPoleDescription"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnPoleDescriptionActive" Visible="false" runat="server" ToolTip="Activate Pole Description"
                                            CommandName="ActivePoleDescription"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnDelete" runat="server" ToolTip="DeActivate Pole Description"
                                            CommandName="DeActivePoleDescription">
                                    <img src="../images/Deactivate.gif"   alt="DeActive" /></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:HiddenField ID="hfPoleId" runat="server" />
                        <asp:HiddenField ID="hfName" runat="server" />
                        <asp:HiddenField ID="hfPageNo" runat="server" />
                        <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                        <asp:HiddenField ID="hfLastPage" runat="server" />
                        <asp:HiddenField ID="hfRecognize" runat="server" />
                        <asp:HiddenField ID="hfPageSize" runat="server" />
                    </div>
                    <div class="grid_boxes">
                        <div id="divdownpaging" runat="server">
                            <div class="grid_paging_bottomtwo">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div id="hidepopup">
                        <%-- SAVE & NEXT Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeLGApopup" runat="server" TargetControlID="btnpopup"
                            PopupControlID="pnlConfirmation" BackgroundCssClass="popbg">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnpopup" runat="server" Text="" Style="display: none;" />
                        <asp:Panel runat="server" ID="pnlConfirmation" DefaultButton="btnOk" CssClass="editpopup_two"
                            Style="display: none;">
                            <div class="panelMessage">
                                <div>
                                    <asp:Panel ID="pnlMsgPopup" runat="server" align="center">
                                        <asp:Label ID="lblMsgPopup" runat="server" Text=""></asp:Label>
                                    </asp:Panel>
                                </div>
                                <div class="clear pad_10">
                                </div>
                                <div class="buttondiv">
                                    <asp:Button ID="btnOk" runat="server" Text="OK" CssClass="btn_ok" Style="margin-left: 105px;" />
                                    <div class="space">
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- SAVE & NEXT Btn popup Closed--%>
                        <%-- Active Country Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                            TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="PanelActivate" runat="server" CssClass="modalPopup"
                            Style="display: none;">
                            <div class="popheader popheaderlblred">
                                <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                            </div>
                            <div class="space">
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btnActiveOk" OnClick="btnActiveOk_Click" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                    <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Active Country Btn popup Closed--%>
                        <%-- alert panel for parent Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeAlertParentPanel" runat="server" PopupControlID="PanelParentPoel"
                            TargetControlID="btnParentPoleTarget" BackgroundCssClass="modalBackground" CancelControlID="btnParentOk">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnParentPoleTarget" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="PanelParentPoel" runat="server" DefaultButton="btnActiveOk" CssClass="modalPopup"
                            Style="display: none;">
                            <div class="popheader popheaderlblred">
                                <asp:Label ID="lblParentPoleMsg" runat="server"></asp:Label>
                            </div>
                            <div class="space">
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btnParentOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="ok_btn" />
                                    <%--<asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />--%>
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- alert panel for parent Btn popup Closed--%>
                        <%-- DeActive Country Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                            TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnDeActivate" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="PanelDeActivate" runat="server" CssClass="modalPopup"
                            Style="display: none; border-color: #639B00;">
                            <div class="popheader" style="background-color: red;">
                                <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <div class="space">
                                    </div>
                                    <asp:Button ID="btnDeActiveOk" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        OnClick="btnDeActiveOk_Click" CssClass="ok_btn" />
                                    <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- DeActive Country popup Closed--%>
                        <%-- Grid Alert popup Start--%>
                        <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                            TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                            <div class="popheader popheaderlblred">
                                <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                            </div>
                            <div class="footer popfooterbtnrt">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Grid Alert popup Closed--%>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/PageValidations/AddPoleDescription.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        $(document).ready(function () { assignDiv('rptrMainMasterMenu_164_5', '121') });
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
