﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using iDSignHelper;
using Resources;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Data;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;

namespace UMS_Nigeria.Masters
{
    public partial class AddPoleDescription : System.Web.UI.Page
    {

        # region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        // MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        string Key = "AddPoleDescription";
        PoleBAL _objPoleBAL = new PoleBAL();
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
        }

        public string PoleMasterId
        {
            get { return string.IsNullOrEmpty(ddlPoleName.SelectedValue) ? Constants.SelectedValue : ddlPoleName.SelectedValue; }
            set { ddlPoleName.SelectedValue = value.ToString(); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                _ObjCommonMethods.BindPolesMaster(ddlPoleName, true, true);
                divParent.Visible = false;
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                PoleBE objPoleBE = new PoleBE();

                objPoleBE.Name = txtName.Text;
                objPoleBE.PoleMasterId = Convert.ToInt32(ddlPoleName.SelectedValue);
                if (ddlParent.Visible)
                    objPoleBE.PoleOrder = ddlPoleName.SelectedIndex == 1 ? 0 : Convert.ToInt32(ddlParent.SelectedValue);
                objPoleBE.Description = txtDescription.Text;
                objPoleBE.Comments = txtComments.Text;
                objPoleBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                xml = _objPoleBAL.AddPoleDescription(objPoleBE);
                objPoleBE = _ObjiDsignBAL.DeserializeFromXml<PoleBE>(xml);
                if (objPoleBE.RowsEffected > 0)
                {
                    Message(UMS_Resource.POLE_INSERT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    BindGrid();
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    txtName.Text = txtDescription.Text = txtComments.Text = string.Empty;
                    ddlPoleName.SelectedIndex = 0;
                    if (ddlParent.Visible)
                        ddlParent.SelectedIndex = 0;
                    divParent.Visible = false;
                }
                else if (objPoleBE.RowsEffected == -1)
                {
                    Message("You have exceeded the limit to add this Pole.", UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (objPoleBE.IsExists)
                {
                    Message("Pole in the same parent already exists.", UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {

                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlPoleName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlPoleName.SelectedIndex > 1)
                {
                    _ObjCommonMethods.BindParentPole(ddlParent, Convert.ToInt32(PoleMasterId), true, true);
                    if (ddlParent.Items.Count < 2)
                    {
                        string ParentPole = ddlPoleName.Items[ddlPoleName.SelectedIndex - 1].Text;
                        //Message("Please Add the Parent Pole " + ParentPole + " in order to add this Pole.", UMS_Resource.MESSAGETYPE_ERROR);
                        lblParentPoleMsg.Text = "Please Add the Parent Pole " + ParentPole +
                                                " in order to add this Pole.";
                        mpeAlertParentPanel.Show();
                    }
                    divParent.Visible = true;
                    ddlParent.SelectedIndex = 0;
                }
                else
                {
                    //ddlParent.SelectedIndex = 0;
                    divParent.Visible = false;
                }
                ddlPoleName.Focus();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }


        //ddl in grid change event
        //protected void ddlGridPoleName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DropDownList ddlGridPoleName = (DropDownList)sender;
        //        GridViewRow row = (GridViewRow)ddlGridPoleName.NamingContainer;
        //        if (row != null)
        //        {
        //            DropDownList ddlGridParent = (DropDownList)row.FindControl("ddlGridParent");
        //            if (ddlGridPoleName.SelectedIndex > 1)
        //            {
        //                Label lblPoleMasterId = (Label)row.FindControl("lblPoleMasterId");
        //                _ObjCommonMethods.BindParentPole(ddlGridParent, Convert.ToInt32(ddlGridPoleName.SelectedValue), true, true);
        //                if (ddlGridParent.Items.Count < 2)
        //                {

        //                    string ParentPole = ddlGridPoleName.Items[ddlGridPoleName.SelectedIndex - 1].Text;
        //                    //Message("Please Add the Parent Pole " + ParentPole + " in order to add this Pole.", UMS_Resource.MESSAGETYPE_ERROR);
        //                    lblParentPoleMsg.Text = "Please Add the Parent Pole " + ParentPole +
        //                                        " in order to add this Pole.";
        //                    mpeAlertParentPanel.Show();
        //                }
        //                ddlGridParent.Visible = true;
        //                ddlGridParent.SelectedIndex = 0;
        //                //divParent.Visible = true;
        //                //ddlParent.SelectedIndex = 0;
        //            }
        //            else
        //            {
        //                //ddlParent.SelectedIndex = 0;
        //                ddlGridParent.Visible = false;
        //                //divParent.Visible = false;
        //            }
        //            ddlGridPoleName.Focus();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void gvPoleDescriptionList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                PoleListBE _ObjPoleListBE = new PoleListBE();
                PoleBE _ObjPoleBE = new PoleBE();
                DataSet ds = new DataSet();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                //DropDownList ddlGridPoelName = (DropDownList)row.FindControl("ddlGridPoelName");
                //DropDownList ddlGridParent = (DropDownList)row.FindControl("ddlGridParent");

                TextBox txtGridName = (TextBox)row.FindControl("txtGridName");
                TextBox txtGridComments = (TextBox)row.FindControl("txtGridComments");
                TextBox txtGridDescription = (TextBox)row.FindControl("txtGridDescription");

                Label lblPoleName = (Label)row.FindControl("lblPoleName");
                Label lblParent = (Label)row.FindControl("lblParent");
                Label lblName = (Label)row.FindControl("lblName");
                Label lblDescription = (Label)row.FindControl("lblDescription");
                Label lblComments = (Label)row.FindControl("lblComments");
                Label lblPoleId = (Label)row.FindControl("lblPoleId");
                hfPoleId.Value = lblPoleId.Text;

                Label lblPoleMasterId = (Label)row.FindControl("lblPoleMasterId");
                Label lblPoleOrder = (Label)row.FindControl("lblPoleOrder");

                hfRecognize.Value = string.Empty;
                switch (e.CommandName.ToUpper())
                {
                    case "EDITPOLEDESCRIPTION":

                        lblName.Visible = lblDescription.Visible = lblComments.Visible = false;
                        txtGridName.Visible = txtGridDescription.Visible = txtGridComments.Visible = true;

                        //ddlGridPoelName.Items
                        

                        txtGridName.Text = lblName.Text;
                        txtGridDescription.Text = lblDescription.Text;
                        txtGridComments.Text = lblComments.Text;
                        row.FindControl("lkbtnPoleDescriptionCancel").Visible = row.FindControl("lkbtnPoleDescriptionUpdate").Visible = true;
                        row.FindControl("lkbtnDelete").Visible = row.FindControl("lkbtnPoleDescriptionEdit").Visible = false;
                        break;
                    case "CANCELPOLEDESCRIPTION":
                        row.FindControl("lkbtnDelete").Visible = row.FindControl("lkbtnPoleDescriptionEdit").Visible = true;
                        row.FindControl("lkbtnPoleDescriptionCancel").Visible = row.FindControl("lkbtnPoleDescriptionUpdate").Visible = false;
                        txtGridName.Visible = txtGridDescription.Visible = txtGridComments.Visible = false;
                        lblName.Visible = lblDescription.Visible = lblComments.Visible = true;
                        break;
                    case "UPDATEPOLEDESCRIPTION":
                        //xml = _ObjUMS_Service.UpdateStates(txtGridStateName.Text, lblStateCode.Text, txtGridDisplayCode.Text, ddlGridCountryName.SelectedValue, _ObjiDsignBAL.ReplaceNewLines(txtGridNotes.Text, true));
                        _ObjPoleBE.Name = txtGridName.Text;
                        _ObjPoleBE.PoleId = Convert.ToInt32(lblPoleId.Text);
                        _ObjPoleBE.PoleMasterId = string.IsNullOrEmpty(lblPoleMasterId.Text) ? 0 : Convert.ToInt32(lblPoleMasterId.Text.Trim());
                        //_ObjPoleBE.PoleOrder = Convert.ToInt32(ddlParent.SelectedValue);
                        _ObjPoleBE.Description = txtGridDescription.Text;
                        _ObjPoleBE.Comments = txtGridComments.Text;
                        _ObjPoleBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        xml = _objPoleBAL.UpdatePole(_ObjPoleBE);
                        _ObjPoleBE = _ObjiDsignBAL.DeserializeFromXml<PoleBE>(xml);

                        if (_ObjPoleBE.RowsEffected > 0)
                        {
                            BindGrid();
                            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            Message("Pole Updated Successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                            txtName.Text = string.Empty;
                        }
                        //else if (_ObjLGAsBE.IsDisplayCodeExists)
                        //{
                        //    Message(UMS_Resource.DISPLAY_CODE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                        //}
                        else if(_ObjPoleBE.IsExists)
                            Message("Pole in the same parent already exists.", UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "ACTIVEPOLEDESCRIPTION":
                        //xml = _ObjUMS_Service.DeleteActiveState(1, lblStateCode.Text);
                        mpeActivate.Show();
                        lblActiveMsg.Text = "Are you sure you want to Activate this Pole?";
                        btnActiveOk.Focus();

                        break;
                    case "DEACTIVEPOLEDESCRIPTION":
                        //xml = _ObjUMS_Service.DeleteActiveState(0, lblStateCode.Text);
                        mpeDeActive.Show();
                        //hfStateCode.Value = lblStateCode.Text;
                        lblDeActiveMsg.Text = "Are you sure you want to DeActivate this Pole?";
                        btnDeActiveOk.Focus();

                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void gvPoleDescriptionList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnPoleDescriptionUpdate = (LinkButton)e.Row.FindControl("lkbtnPoleDescriptionUpdate");
                    TextBox txtGridName = (TextBox)e.Row.FindControl("txtGridName");
                    //TextBox txtGridDisplayCode = (TextBox)e.Row.FindControl("txtGridDisplayCode");

                    lkbtnPoleDescriptionUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridName.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");


                    if (Convert.ToInt32(lblActiveStatusId.Text) == 2)
                    {
                        e.Row.FindControl("lkbtnDelete").Visible = e.Row.FindControl("lkbtnPoleDescriptionEdit").Visible = false;
                        e.Row.FindControl("lkbtnPoleDescriptionActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDelete").Visible = e.Row.FindControl("lkbtnPoleDescriptionEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {

            try
            {
                PoleBE _ObjPoleBE = new PoleBE();
                _ObjPoleBE.PoleId = Convert.ToInt32(hfPoleId.Value);
                _ObjPoleBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _ObjPoleBE.ActiveStatusId = 1;

                xml = _objPoleBAL.DeletePole(_ObjPoleBE);
                _ObjPoleBE = _ObjiDsignBAL.DeserializeFromXml<PoleBE>(xml);

                if (_ObjPoleBE.RowsEffected > 0)
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message("Pole activated successfully", UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                    Message("Pole activation failed", UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                PoleBE _ObjPoleBE = new PoleBE();
                _ObjPoleBE.PoleId = Convert.ToInt32(hfPoleId.Value);
                _ObjPoleBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _ObjPoleBE.ActiveStatusId = 2;
                xml = _objPoleBAL.DeletePole(_ObjPoleBE);
                _ObjPoleBE = _ObjiDsignBAL.DeserializeFromXml<PoleBE>(xml);
                if (_ObjPoleBE.RowsEffected > 0)
                {
                    BindGrid();
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message("Pole DeActivated Successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else if(_ObjPoleBE.IsHavingChilds)
                    Message("Pole is having childs, so you can not DeActivate.", UMS_Resource.MESSAGETYPE_ERROR);
                else if (_ObjPoleBE.IsHavingCustomers)
                    Message("Pole is having customers, so you can not DeActivate.", UMS_Resource.MESSAGETYPE_ERROR);
                else
                    Message("Pole DeActivation Failed.", UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        public void BindGrid()
        {
            try
            {
                PoleListBE _objPoleListBE = new PoleListBE();
                PoleBE _ObjPoleBE = new PoleBE();
                XmlDocument resultedXml = new XmlDocument();
                //resultedXml = _ObjUMS_Service.GetActiveCountriesList(Convert.ToInt32(hfPageNo.Value), PageSize);
                _ObjPoleBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                _ObjPoleBE.PageSize = PageSize;
                resultedXml = _objPoleBAL.GetPoles(_ObjPoleBE, ReturnType.List);
                _objPoleListBE = _ObjiDsignBAL.DeserializeFromXml<PoleListBE>(resultedXml);

                if (_objPoleListBE.items.Count > 0)
                {
                    divdownpaging.Visible = divpaging.Visible = true;
                    UCPaging1.Visible = UCPaging2.Visible = true;
                    hfTotalRecords.Value = _objPoleListBE.items[0].TotalRecords.ToString();
                    gvPoleDescriptionList.DataSource = _objPoleListBE.items;
                    gvPoleDescriptionList.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = _objPoleListBE.items.Count.ToString();
                    divdownpaging.Visible = divpaging.Visible = false;
                    UCPaging1.Visible = UCPaging2.Visible = false;
                    gvPoleDescriptionList.DataSource = new DataTable(); ;
                    gvPoleDescriptionList.DataBind();
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion
        
        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}