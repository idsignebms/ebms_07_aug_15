﻿<%@ Page Title=":: Add Pole Management ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="AddPoleManagement.aspx.cs" Inherits="UMS_Nigeria.Masters.AddPoleManagement" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdatePanel ID="upAddPole" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddPole" runat="server" Text="Add Pole Master"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div id="divInput" runat="server">
                        <div class="inner-box">
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litPoleName" runat="server" Text="<%$ Resources:Resource, POLE_NAME%>"></asp:Literal>
                                        <span class="span_star">*</span></label><div class="space">
                                        </div>
                                    <asp:TextBox CssClass="text-box" ID="txtPoleName" TabIndex="1" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanPoleName','Pole Name')"
                                        MaxLength="50" placeholder="Enter Pole Name"></asp:TextBox>
                                    <span id="spanPoleName" class="span_color"></span>
                                </div>
                                <div class="text-inner">
                                    <label for="Description">
                                        <asp:Literal ID="litDescription" runat="server" Text="<%$ Resources:Resource, DESCRIPTION%>"></asp:Literal>
                                    </label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox CssClass="text-box" ID="txtDescription" TabIndex="4" runat="server"
                                        MaxLength="150" TextMode="MultiLine" placeholder="Enter Pole Description"></asp:TextBox>
                                </div>
                            </div>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <label for="PoleOrderId">
                                        <asp:Literal ID="litPoleOrderId" runat="server" Text="<%$ Resources:Resource, POLE_ORDER_ID%>"></asp:Literal>
                                        <span class="span_star">*</span></label><div class="space">
                                        </div>
                                    <asp:TextBox CssClass="text-box" ID="txtPoleOrderId" TabIndex="2" runat="server"
                                        onblur="return TextBoxBlurValidationlbl(this,'spanPoleOrderId','Pole Order ID')"
                                        MaxLength="50" placeholder="Enter Pole Order Id"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="ftbTxtPoleOrderId" runat="server" TargetControlID="txtPoleOrderId"
                                        FilterType="Numbers" ValidChars=" ">
                                    </asp:FilteredTextBoxExtender>
                                    <span id="spanPoleOrderId" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box3">
                                <div class="text-inner">
                                    <label for="CodeLength">
                                        <asp:Literal ID="litCodeLength" runat="server" Text="<%$ Resources:Resource, CODE_LENGTH%>"></asp:Literal>
                                        <span class="span_star">*</span></label><div class="space">
                                        </div>
                                    <asp:TextBox CssClass="text-box" ID="txtCodeLength" TabIndex="3" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanCodeLength','Code Length')"
                                        MaxLength="50" placeholder="Enter Code Length"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="fteTxtCodeLength" runat="server" TargetControlID="txtCodeLength"
                                        FilterType="Numbers" ValidChars=" ">
                                    </asp:FilteredTextBoxExtender>
                                    <span id="spanCodeLength" class="span_color"></span>
                                </div>
                            </div>
                        </div>
                        <div class="box_total">
                            <div class="box_total_a">
                                <asp:Button ID="btnSave" TabIndex="4" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                    CssClass="box_s" runat="server" OnClick="btnSave_Click" />
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="grid_boxes">
                        <div class="grid_paging_top">
                            <div class="paging_top_title">
                                <asp:Literal ID="litLGA" runat="server" Text="Pole Master List"></asp:Literal>
                            </div>
                            <div class="paging_top_right_content" id="divpaging" runat="server">
                                <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvPoleMasterList" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvPoleMasterList_OnRowDataBound"
                            OnRowCommand="gvPoleMasterList_OnRowCommand" HeaderStyle-CssClass="grid_tb_hd ">
                            <EmptyDataRowStyle HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                    HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        <asp:Label ID="lblPoleMasterId" Visible="false" runat="server" Text='<%#Eval("PoleMasterId")%>'></asp:Label>
                                        <asp:Label ID="lblActiveStatusId" Visible="false" runat="server" Text='<%#Eval("ActiveStatusId")%>'></asp:Label>
                                        <asp:Label ID="lblIsParentActive" Visible="false" runat="server" Text='<%#Eval("IsParentActive")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, POLE_NAME%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridName" MaxLength="50" placeholder="Enter Pole Name"
                                            Visible="false" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, POLE_ORDER_ID%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPoleMasterOrderID" runat="server" Text='<%#Eval("PoleMasterOrderID") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridPoleMasterOrderID" MaxLength="50" placeholder="Enter Pole Order"
                                            Visible="false" runat="server"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="ftbTxtGridPoleMasterOrderID" runat="server" TargetControlID="txtGridPoleMasterOrderID"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, CODE_LENGTH%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPoleMasterCodeLength" runat="server" Text='<%#Eval("PoleMasterCodeLength") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridPoleMasterCodeLength" MaxLength="50"
                                            placeholder="Enter Code Length" Visible="false" runat="server"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="ftbTxtGridPoleMasterCodeLength" runat="server" TargetControlID="txtGridPoleMasterCodeLength"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, DESCRIPTION%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDescription" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridDescription" MaxLength="150" placeholder="Enter Pole Description"
                                            Visible="false" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lkbtnPoleEdit" runat="server" ToolTip="Edit" Text="Edit" CommandName="EditPole"><img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnPoleUpdate" Visible="false" runat="server" ToolTip="Update"
                                            CommandName="UpdatePole"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnPoleCancel" Visible="false" runat="server" ToolTip="Cancel"
                                            CommandName="CancelPole"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnPoleActive" Visible="false" runat="server" ToolTip="Activate Pole"
                                            CommandName="ActivePole"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnDelete" runat="server" ToolTip="DeActivate Pole" CommandName="DeActivePole">
                                    <img src="../images/Deactivate.gif"   alt="DeActive" /></asp:LinkButton>
                                        <asp:Label ID="lblGridParentDeactive" Text="Parent Deactivated" runat="server" Visible="false" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:HiddenField ID="hfPoleMasterId" runat="server" />
                        <asp:HiddenField ID="hfName" runat="server" />
                        <asp:HiddenField ID="hfPageNo" runat="server" />
                        <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                        <asp:HiddenField ID="hfLastPage" runat="server" />
                        <asp:HiddenField ID="hfRecognize" runat="server" />
                        <asp:HiddenField ID="hfPageSize" runat="server" />
                    </div>
                    <div class="grid_boxes">
                        <div id="divdownpaging" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div id="hidepopup">
                        <%-- SAVE & NEXT Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeLGApopup" runat="server" TargetControlID="btnpopup"
                            PopupControlID="pnlConfirmation" BackgroundCssClass="popbg">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnpopup" runat="server" Text="" Style="display: none;" />
                        <asp:Panel runat="server" ID="pnlConfirmation" DefaultButton="btnOk" CssClass="editpopup_two"
                            Style="display: none;">
                            <div class="panelMessage">
                                <div>
                                    <asp:Panel ID="pnlMsgPopup" runat="server" align="center">
                                        <asp:Label ID="lblMsgPopup" runat="server" Text=""></asp:Label>
                                    </asp:Panel>
                                </div>
                                <div class="clear pad_10">
                                </div>
                                <div class="buttondiv">
                                    <asp:Button ID="btnOk" runat="server" Text="OK" CssClass="btn_ok" Style="margin-left: 105px;" />
                                    <div class="space">
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- SAVE & NEXT Btn popup Closed--%>
                        <%-- Active Country Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                            TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="PanelActivate" runat="server" CssClass="modalPopup" Style="display: none;">
                            <div class="popheader popheaderlblred">
                                <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                            </div>
                            <div class="space">
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btnActiveOk" OnClick="btnActiveOk_OnClick" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                    <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Active Country Btn popup Closed--%>
                        <%-- DeActive Country Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                            TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnDeActivate" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="PanelDeActivate" runat="server" DefaultButton="btnDeActiveOk" CssClass="modalPopup"
                            Style="display: none; border-color: #639B00;">
                            <div class="popheader" style="background-color: red;">
                                <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <div class="space">
                                    </div>
                                    <asp:Button ID="btnDeActiveOk" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        OnClick="btnDeActiveOk_OnClick" CssClass="ok_btn" />
                                    <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- DeActive Country popup Closed--%>
                        <%-- Grid Alert popup Start--%>
                        <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                            TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                            <div class="popheader popheaderlblred">
                                <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                            </div>
                            <div class="footer popfooterbtnrt">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Grid Alert popup Closed--%>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/PageValidations/AddPoleManagement.js" type="text/javascript"></script>

     <script language="javascript" type="text/javascript">
         $(document).ready(function () { assignDiv('rptrMainMasterMenu_164_5', '164') });
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
