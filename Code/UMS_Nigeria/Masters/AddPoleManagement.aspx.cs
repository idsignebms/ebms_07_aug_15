﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using iDSignHelper;
using Resources;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Data;

namespace UMS_Nigeria.Masters
{
    public partial class AddPoleManagement : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        // MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        string Key = "AddPoleManagement";
        PoleBAL _objPoleBAL = new PoleBAL();
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                GetPoleOrderId();
                BindGrid();
            }
            IsCustomerExists();
        }

        public void GetPoleOrderId()
        {
            PoleBE objPoleBE = new PoleBE();
            XmlDocument XmlPole = new XmlDocument();
            XmlPole = _objPoleBAL.GetPoleOrderId();
            objPoleBE = _ObjiDsignBAL.DeserializeFromXml<PoleBE>(XmlPole);
            txtPoleOrderId.Text = objPoleBE.MaxPoleOrderId.ToString();
            txtPoleOrderId.Enabled = false;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                PoleBE _ObjPoleBE = new PoleBE();

                _ObjPoleBE.Name = txtPoleName.Text;
                _ObjPoleBE.PoleMasterOrderID = Convert.ToInt32(txtPoleOrderId.Text);
                _ObjPoleBE.PoleMasterCodeLength = Convert.ToInt32(txtCodeLength.Text);
                _ObjPoleBE.Description = txtDescription.Text;
                _ObjPoleBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                xml = _objPoleBAL.AddPole(_ObjPoleBE);
                _ObjPoleBE = _ObjiDsignBAL.DeserializeFromXml<PoleBE>(xml);
                if (Convert.ToBoolean(_ObjPoleBE.RowsEffected > 0))
                {
                    Message(Resource.POLE_DETAILS_SAVED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    txtPoleName.Text = txtPoleOrderId.Text = txtCodeLength.Text = txtDescription.Text = string.Empty;
                    GetPoleOrderId();
                    BindGrid();
                }
                else if (_ObjPoleBE.IsOrderIdExists)
                {
                    Message(Resource.ORDER_ID_ALREADY_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (_ObjPoleBE.IsExists)
                {
                    Message(UMS_Resource.POLE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                {
                    Message(Resource.POLE_DETAILS_SAVE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {

                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvPoleMasterList_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnPoleUpdate = (LinkButton)e.Row.FindControl("lkbtnPoleUpdate");
                    TextBox txtGridName = (TextBox)e.Row.FindControl("txtGridName");
                    //TextBox txtGridPoleMasterOrderID = (TextBox)e.Row.FindControl("txtGridPoleMasterOrderID");
                    //TextBox txtGridPoleMasterCodeLength = (TextBox)e.Row.FindControl("txtGridPoleMasterCodeLength");
                    TextBox txtGridDescription = (TextBox)e.Row.FindControl("txtGridDescription");

                    //TextBox txtGridDisplayCode = (TextBox)e.Row.FindControl("txtGridDisplayCode");

                    lkbtnPoleUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridName.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");
                    Label lblIsParentActive = (Label)e.Row.FindControl("lblIsParentActive");
                    Label lblGridParentDeactive = (Label)e.Row.FindControl("lblGridParentDeactive");


                    if (Convert.ToInt32(lblActiveStatusId.Text) == 2)
                    {
                        if (Convert.ToInt32(lblIsParentActive.Text) == 2)
                        {
                            e.Row.FindControl("lkbtnDelete").Visible = e.Row.FindControl("lkbtnPoleEdit").Visible = false;
                            lblGridParentDeactive.Visible = true;
                        }
                        else
                        {

                            e.Row.FindControl("lkbtnDelete").Visible = e.Row.FindControl("lkbtnPoleEdit").Visible = false;
                            e.Row.FindControl("lkbtnPoleActive").Visible = true;
                        }
                    }
                    else
                    {
                        if (Convert.ToInt32(lblIsParentActive.Text) == 2)
                        {
                            e.Row.FindControl("lkbtnDelete").Visible = e.Row.FindControl("lkbtnPoleEdit").Visible = false;
                            lblGridParentDeactive.Visible = true;
                        }
                        else
                        {
                            e.Row.FindControl("lkbtnDelete").Visible = e.Row.FindControl("lkbtnPoleEdit").Visible = true;
                            e.Row.FindControl("lkbtnPoleActive").Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvPoleMasterList_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                PoleListBE _ObjPoleListBE = new PoleListBE();
                PoleBE _ObjPoleBE = new PoleBE();
                DataSet ds = new DataSet();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                //DropDownList ddlGridPoelName = (DropDownList)row.FindControl("ddlGridPoelName");
                //DropDownList ddlGridParent = (DropDownList)row.FindControl("ddlGridParent");

                TextBox txtGridName = (TextBox)row.FindControl("txtGridName");
                //TextBox txtGridPoleMasterOrderID = (TextBox)row.FindControl("txtGridPoleMasterOrderID");
                //TextBox txtGridPoleMasterCodeLength = (TextBox)row.FindControl("txtGridPoleMasterCodeLength");
                TextBox txtGridDescription = (TextBox)row.FindControl("txtGridDescription");


                Label lblName = (Label)row.FindControl("lblName");
                Label lblDescription = (Label)row.FindControl("lblDescription");
                //Label lblPoleMasterOrderID = (Label)row.FindControl("lblPoleMasterOrderID");
                //Label lblPoleMasterCodeLength = (Label)row.FindControl("lblPoleMasterCodeLength");
                Label lblPoleMasterId = (Label)row.FindControl("lblPoleMasterId");
                hfPoleMasterId.Value = lblPoleMasterId.Text;

                //Label lblPoleMasterId = (Label)row.FindControl("lblPoleMasterId");
                Label lblPoleOrder = (Label)row.FindControl("lblPoleOrder");

                hfRecognize.Value = string.Empty;
                switch (e.CommandName.ToUpper())
                {
                    case "EDITPOLE":

                        lblName.Visible = lblDescription.Visible = false;
                        txtGridName.Visible = txtGridDescription.Visible = true;

                        //ddlGridPoelName.Items


                        txtGridName.Text = lblName.Text;
                        txtGridDescription.Text = lblDescription.Text;
                        //txtGridPoleMasterOrderID.Text = lblPoleMasterOrderID.Text;
                        //txtGridPoleMasterCodeLength.Text = lblPoleMasterCodeLength.Text;
                        row.FindControl("lkbtnPoleCancel").Visible = row.FindControl("lkbtnPoleUpdate").Visible = true;
                        row.FindControl("lkbtnDelete").Visible = row.FindControl("lkbtnPoleEdit").Visible = false;
                        break;
                    case "CANCELPOLE":
                        row.FindControl("lkbtnDelete").Visible = row.FindControl("lkbtnPoleEdit").Visible = true;
                        row.FindControl("lkbtnPoleCancel").Visible = row.FindControl("lkbtnPoleUpdate").Visible = false;
                        txtGridName.Visible = txtGridDescription.Visible = false;
                        lblName.Visible = lblDescription.Visible = true;
                        break;
                    case "UPDATEPOLE":
                        //xml = _ObjUMS_Service.UpdateStates(txtGridStateName.Text, lblStateCode.Text, txtGridDisplayCode.Text, ddlGridCountryName.SelectedValue, _ObjiDsignBAL.ReplaceNewLines(txtGridNotes.Text, true));
                        _ObjPoleBE.Name = txtGridName.Text;
                        _ObjPoleBE.PoleMasterId = Convert.ToInt32(lblPoleMasterId.Text.Trim());
                        //_ObjPoleBE.PoleMasterId = Convert.ToInt32(ddlGridPoelName.SelectedValue);
                        //_ObjPoleBE.PoleOrder = Convert.ToInt32(ddlParent.SelectedValue);
                        _ObjPoleBE.Description = txtGridDescription.Text.Trim();
                        //_ObjPoleBE.PoleMasterOrderID = Convert.ToInt32(txtGridPoleMasterOrderID.Text.Trim());
                        //_ObjPoleBE.PoleMasterCodeLength = Convert.ToInt32(txtGridPoleMasterCodeLength.Text.Trim());
                        _ObjPoleBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        xml = _objPoleBAL.UpdatePoleMaster(_ObjPoleBE);
                        _ObjPoleBE = _ObjiDsignBAL.DeserializeFromXml<PoleBE>(xml);

                        if (_ObjPoleBE.RowsEffected > 0)
                        {
                            BindGrid();
                            Message(Resource.POLE_MASTER_UPDATED, UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        else if (_ObjPoleBE.IsPoleInformationExisis)
                        {
                            lblgridalertmsg.Text = "Pole information records are associated with this pole master. Pole can not be updated.";
                            mpegridalert.Show();
                            btngridalertok.Focus();
                        }
                        else if (_ObjPoleBE.IsExists)
                        {
                            Message(Resource.POLE_MASTER_NAME_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else
                        {
                            Message(Resource.POLE_MASTER_UPDATION_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                        }

                        break;
                    case "ACTIVEPOLE":
                        //xml = _ObjUMS_Service.DeleteActiveState(1, lblStateCode.Text);
                        mpeActivate.Show();
                        lblActiveMsg.Text = "Are you sure you want to Activate this Pole?";
                        btnActiveOk.Focus();

                        break;
                    case "DEACTIVEPOLE":
                        //xml = _ObjUMS_Service.DeleteActiveState(0, lblStateCode.Text);
                        mpeDeActive.Show();
                        //hfStateCode.Value = lblStateCode.Text;
                        lblDeActiveMsg.Text = "Are you sure you want to DeActivate this Pole?";
                        btnDeActiveOk.Focus();

                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_OnClick(object sender, EventArgs e)
        {
            try
            {
                PoleBE _ObjPoleBE = new PoleBE();
                _ObjPoleBE.PoleMasterId = Convert.ToInt32(hfPoleMasterId.Value);
                _ObjPoleBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _ObjPoleBE.ActiveStatusId = 1;

                xml = _objPoleBAL.DeletePoleMaster(_ObjPoleBE);
                _ObjPoleBE = _ObjiDsignBAL.DeserializeFromXml<PoleBE>(xml);

                if (_ObjPoleBE.RowsEffected > 0)
                {
                    BindGrid();
                    Message(Resource.POLE_MASTER_ACTIVATED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                {
                    Message(Resource.POLE_MASTER_ACTIVATATION_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_OnClick(object sender, EventArgs e)
        {
            try
            {
                PoleBE _ObjPoleBE = new PoleBE();
                _ObjPoleBE.PoleMasterId = Convert.ToInt32(hfPoleMasterId.Value);
                _ObjPoleBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _ObjPoleBE.ActiveStatusId = 2;
                xml = _objPoleBAL.DeletePoleMaster(_ObjPoleBE);
                _ObjPoleBE = _ObjiDsignBAL.DeserializeFromXml<PoleBE>(xml);
                if (_ObjPoleBE.RowsEffected > 0)
                {
                    BindGrid();
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message("Pole Master DeActivated Successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else if (_ObjPoleBE.IsPoleInformationExisis)
                {
                    lblgridalertmsg.Text = "Pole information records are associated with this pole master. Pole can not be deactivated.";
                    mpegridalert.Show();
                    btngridalertok.Focus();
                }
                else
                {
                    Message("Pole Master DeActivation Failed.", UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        public void BindGrid()
        {
            try
            {
                PoleListBE _objPoleListBE = new PoleListBE();
                PoleBE _ObjPoleBE = new PoleBE();
                XmlDocument resultedXml = new XmlDocument();
                _ObjPoleBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                _ObjPoleBE.PageSize = PageSize;
                resultedXml = _objPoleBAL.GetMasterPoles(_ObjPoleBE, ReturnType.Get);
                _objPoleListBE = _ObjiDsignBAL.DeserializeFromXml<PoleListBE>(resultedXml);

                if (_objPoleListBE.items.Count > 0)
                {
                    divdownpaging.Visible = divpaging.Visible = true;
                    UCPaging1.Visible = UCPaging2.Visible = true;
                    hfTotalRecords.Value = _objPoleListBE.items[0].TotalRecords.ToString();
                    gvPoleMasterList.DataSource = _objPoleListBE.items;
                    gvPoleMasterList.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = _objPoleListBE.items.Count.ToString();
                    divdownpaging.Visible = divpaging.Visible = false;
                    UCPaging1.Visible = UCPaging2.Visible = false;
                    gvPoleMasterList.DataSource = new DataTable(); ;
                    gvPoleMasterList.DataBind();
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void IsCustomerExists()
        {
            XmlDocument resultedXmlObj = new XmlDocument();           
            PoleBE _objPoleBE = new PoleBE(); 
            resultedXmlObj = _objPoleBAL.IsCustomerExistsExists();
            _objPoleBE = _ObjiDsignBAL.DeserializeFromXml<PoleBE>(resultedXmlObj);

            if (_objPoleBE.IsExists)
            {
                divInput.Visible = false;
                lblgridalertmsg.Text = Resource.NEW_POLE_NOT_PERMITTED;
                mpegridalert.Show();
            }
            else divInput.Visible = true;
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}