﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddServiceUnits
                     
 Developer        : id065-Bhimaraju V
 Creation Date    : 20-Feb-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using Resources;
using iDSignHelper;
using System.Xml;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Globalization;

namespace UMS_Nigeria.Masters
{
    public partial class AddPoles : System.Web.UI.Page
    {
        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        string Key = "AddPoles";
        DataTable dtData = new DataTable();
        static int customersCount = 0;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                string path = string.Empty;
                path = objCommonMethods.GetPagePath(this.Request.Url);
                if (objCommonMethods.IsAccess(path))
                {
                    //PageAccessPermissions();
                    objCommonMethods.BindTransformers(ddlTransformerName, string.Empty, true);
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    BindGridPolesList();
                    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    if (!string.IsNullOrEmpty(Request.QueryString[UMS_Resource.QSTR_TRANSFORMER_CODE]))
                    {
                        ddlTransformerName.SelectedValue = objiDsignBAL.Decrypt(Request.QueryString[UMS_Resource.QSTR_TRANSFORMER_CODE]).ToString();
                    }
                }
                else { Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }
            }

        }

        //private void PageAccessPermissions()
        //{
        //    try
        //    {
        //        if (objCommonMethods.IsAccess(UMS_Resource.ADD_TRANSFORMER_PAGE)) { lkbtnAddNewTransFormer.Visible = true; } else { lkbtnAddNewTransFormer.Visible = false; }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();

                objMastersBE.PoleName = txtPoleName.Text;
                objMastersBE.TransFormerId = ddlTransformerName.SelectedValue;
                objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.Notes = objiDsignBAL.ReplaceNewLines(txtNotes.Text, true);
                //xml = objMastersBAL.Pole(objMastersBE, Statement.Insert);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);
                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGridPolesList();
                    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.POLE_INSERT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    txtPoleName.Text = txtNotes.Text = string.Empty;

                }
                else
                    Message(UMS_Resource.POLE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }


        protected void lkbtnAddNewTransFormer_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_TRANSFORMER_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridPolesList();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridPolesList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridPolesList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridPolesList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                BindGridPolesList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                objiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
                //ddlPageSize.Items.Clear();
                //if (TotalRecords < Constants.PageSizeStarts)
                //{
                //    ListItem item = new ListItem(Constants.PageSizeStarts.ToString(), Constants.PageSizeStarts.ToString());
                //    ddlPageSize.Items.Add(item);
                //}
                //else
                //{
                //    int previousSize = Constants.PageSizeStarts;
                //    for (int i = Constants.PageSizeStarts; i <= TotalRecords; i = i + Constants.PageSizeIncrement)
                //    {
                //        ListItem item = new ListItem(i.ToString(), i.ToString());
                //        ddlPageSize.Items.Add(item);
                //        previousSize = i;
                //    }
                //    if (previousSize < TotalRecords)
                //    {
                //        ListItem item = new ListItem((previousSize + Constants.PageSizeIncrement).ToString(), (previousSize + Constants.PageSizeIncrement).ToString());
                //        ddlPageSize.Items.Add(item);
                //    }
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvPolesList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridPoleName = (TextBox)row.FindControl("txtGridPoleName");
                TextBox txtGridNotes = (TextBox)row.FindControl("txtGridNotes");
                DropDownList ddlGridTransFormerName = (DropDownList)row.FindControl("ddlGridTransFormerName");

                Label lblPoleId = (Label)row.FindControl("lblPoleId");
                Label lblPoleName = (Label)row.FindControl("lblPoleName");
                Label lblTransFormerName = (Label)row.FindControl("lblTransFormerName");
                Label lblGridTransFormerName = (Label)row.FindControl("lblGridTransFormerName");
                Label lblNotes = (Label)row.FindControl("lblNotes");
                switch (e.CommandName.ToUpper())
                {
                    case "EDITPOLE":
                        lblGridTransFormerName.Visible = lblNotes.Visible = lblTransFormerName.Visible = lblPoleName.Visible = false;
                        txtGridPoleName.Visible = ddlGridTransFormerName.Visible = txtGridNotes.Visible = true;
                        txtGridPoleName.Text = lblPoleName.Text;
                        objCommonMethods.BindTransformers(ddlGridTransFormerName, string.Empty, true);
                        ddlGridTransFormerName.SelectedIndex = ddlGridTransFormerName.Items.IndexOf(ddlGridTransFormerName.Items.FindByValue(lblTransFormerName.Text));
                        txtGridNotes.Text = objiDsignBAL.ReplaceNewLines(lblNotes.Text, false);
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;
                        break;
                    case "CANCELPOLE":
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                        txtGridPoleName.Visible = ddlGridTransFormerName.Visible = txtGridNotes.Visible = false;
                        lblGridTransFormerName.Visible = lblNotes.Visible = lblPoleName.Visible = true;
                        break;
                    case "UPDATEPOLE":
                        objMastersBE.PoleId = lblPoleId.Text;
                        objMastersBE.TransFormerId = ddlGridTransFormerName.SelectedValue;
                        objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objMastersBE.PoleName = txtGridPoleName.Text;
                        objMastersBE.Notes = objiDsignBAL.ReplaceNewLines(txtGridNotes.Text, true);
                        //xml = objMastersBAL.Pole(objMastersBE, Statement.Update);
                        objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                        if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        {
                            BindGridPolesList();
                            BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            Message(UMS_Resource.POLE_UPDATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                        }
                        else
                            Message(UMS_Resource.POLE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "ACTIVEPOLE":
                        mpeActivate.Show();
                        hfPoleId.Value = lblPoleId.Text;
                        lblActiveMsg.Text = UMS_Resource.ACTIVE_POLE_POPUP_TEXT;
                        btnActiveOk.Focus();
                        //objMastersBE.ActiveStatusId = Constants.Active;
                        //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objMastersBE.PoleId = lblPoleId.Text;
                        //xml = objMastersBAL.Pole(objMastersBE, Statement.Delete);
                        //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                        //if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        //{
                        //    BindGridPolesList();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message( UMS_Resource.POLE_ACTIVATE_SUCESS,UMS_Resource.MESSAGETYPE_SUCCESS);

                        //}
                        //else
                        //    Message( UMS_Resource.POLE_ACTIVATE_FAIL,UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "DEACTIVEPOLE":
                        if (CustomerCount(e.CommandArgument.ToString()))
                        {
                            mpeShiftCustomer.Show();
                            //hfBU_ID.Value = lblBusinessUnitId.Text;
                            lblShiftCustomer.Text = Resource.SHIFT_CUSTOMERS_POLE.Insert(13, customersCount.ToString() + " ");
                            btnShipCustomer.Focus();
                        }
                        else
                        {
                            mpeDeActive.Show();
                            hfPoleId.Value = lblPoleId.Text;
                            lblDeActiveMsg.Text = UMS_Resource.DEACTIVE_POLE_POPUP_TEXT;
                            btnDeActiveOk.Focus();
                        }
                        
                        //objMastersBE.ActiveStatusId = Constants.DeActive;
                        //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objMastersBE.PoleId = lblPoleId.Text;
                        //xml = objMastersBAL.Pole(objMastersBE, Statement.Delete);
                        //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                        //if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        //{
                        //    BindGridPolesList();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message(UMS_Resource.POLE_DEACTIVATE_SUCESS,UMS_Resource.MESSAGETYPE_SUCCESS);

                        //}
                        //else
                        //    Message( UMS_Resource.POLE_DEACTIVATE_FAIL,UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.Active;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.PoleId = hfPoleId.Value;
                //xml = objMastersBAL.Pole(objMastersBE, Statement.Delete);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGridPolesList();
                    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.POLE_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else
                    Message(UMS_Resource.POLE_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.DeActive;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.PoleId = hfPoleId.Value;
                //xml = objMastersBAL.Pole(objMastersBE, Statement.Delete);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGridPolesList();
                    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.POLE_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else
                    Message(UMS_Resource.POLE_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvPolesList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    TextBox txtGridPoleName = (TextBox)e.Row.FindControl("txtGridPoleName");
                    DropDownList ddlGridTransFormerName = (DropDownList)e.Row.FindControl("ddlGridTransFormerName");

                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridPoleName.ClientID + "','" + ddlGridTransFormerName.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");


                    if (Convert.ToInt32(lblActiveStatusId.Text) == Constants.DeActive)
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public bool CustomerCount(string PoleId)
        {
            XmlDocument _xml = new XmlDocument();
            CustomerDetailsBe _ObjCustomerDetailsBe = new CustomerDetailsBe();
            CustomerDetailsBAL _ObjCustomerDetailsBAL = new CustomerDetailsBAL();
            _ObjCustomerDetailsBe.PoleId = PoleId;
            _xml = _ObjCustomerDetailsBAL.CountCustomersBAL(_ObjCustomerDetailsBe, Statement.Check);
            _ObjCustomerDetailsBe = objiDsignBAL.DeserializeFromXml<CustomerDetailsBe>(_xml);
            customersCount = _ObjCustomerDetailsBe.Count;
            if (_ObjCustomerDetailsBe.Count > 0)
                return true;
            else
                return false;
        }

        private void BindGridPolesList()
        {
            try
            {
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();
                XmlDocument resultedXml = new XmlDocument();

                objMastersBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                objMastersBE.PageSize = PageSize;
                //resultedXml = objMastersBAL.Pole(objMastersBE, ReturnType.Get);
                objMastersListBE = objiDsignBAL.DeserializeFromXml<MastersListBE>(resultedXml);
                if (objMastersListBE.Items.Count > 0)
                {
                    divgrid.Visible = divpaging.Visible = true;
                    hfTotalRecords.Value = objMastersListBE.Items[0].TotalRecords.ToString();
                    gvPolesList.DataSource = objMastersListBE.Items;
                    gvPolesList.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = objMastersListBE.Items.Count.ToString();
                    divgrid.Visible = divpaging.Visible = false;
                    gvPolesList.DataSource = new DataTable(); ;
                    gvPolesList.DataBind();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}