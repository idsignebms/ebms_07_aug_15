﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master" AutoEventWireup="true" Theme="Green"
    CodeBehind="AddRegions.aspx.cs" Inherits="UMS_Nigeria.Masters.AddRegions" Title=":: Add Regions ::" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdatePanel ID="upAddRegion" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddState" runat="server" Text="<%$ Resources:Resource, ADD_REGION%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litStateCode" runat="server" Text="<%$ Resources:Resource, COUNTRY_NAME%>"></asp:Literal>
                                    <span class="span_star">*</span></label><br />
                                <asp:DropDownList ID="ddlCountry" TabIndex="1" onchange="DropDownlistOnChangelbl(this,'spanddlCountry',' Country')"
                                    runat="server" CssClass="text-box text-select">
                                </asp:DropDownList><br />
                                <%--<span class="text-heading_2">
                                    <asp:HyperLink ID="lkbtnAddNewState" runat="server" NavigateUrl="~/Masters/AddStates.aspx"
                                        Text="<%$ Resources:Resource, ADD%>"></asp:HyperLink><br />
                                </span>--%><span id="spanddlCountry" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="LitRegionName" runat="server" Text="<%$ Resources:Resource, REGION_NAME%>"></asp:Literal>
                                    <span class="span_star">*</span></label><div class="space">
                                    </div>
                                <asp:TextBox ID="txtRegionName" TabIndex="2" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanRegionName','Region Name')"
                                    MaxLength="50" placeholder="Enter Region Name" CssClass="text-box"></asp:TextBox>
                                <span id="spanRegionName" class="span_color"></span>
                            </div>
                        </div>
                        <div class="box_total">
                            <div class="box_total_a">
                                <asp:Button ID="btnSave" TabIndex="4" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                    CssClass="box_s" runat="server" OnClick="btnSave_Click" />
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                        <br />
                        <br />
                    </div>
                    <div class="grid_boxes">
                        <div class="grid_pagingTwo_top">
                            <div class="paging_top_title">
                                <asp:Literal ID="litRegion" runat="server" Text="Region List"></asp:Literal>
                            </div>
                            <div class="paging_top_rightTwo_content" id="divpaging" runat="server">
                                <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvRegionList" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="grid_tb_hd"
                            OnRowCommand="gvRegionList_RowCommand" OnRowDataBound="gvRegionList_RowDataBound">
                            <EmptyDataRowStyle HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                    HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        <asp:Label ID="lblActiveStatusId" Visible="false" runat="server" Text='<%#Eval("ActiveStatusId")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>                                
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, REGION_NAME%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRegionName" runat="server" Text='<%#Eval("RegionName") %>'></asp:Label>
                                        <asp:TextBox ID="txtGridRegionName" CssClass="text-box" MaxLength="50" placeholder="Enter Region Name"
                                            Visible="false" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, COUNTRY_NAME%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCountryCode" Visible="false" runat="server" Text='<%#Eval("CountryCode") %>'></asp:Label>
                                        <asp:DropDownList ID="ddlGridCountry" CssClass="text-box" Visible="false" runat="server">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblCountryName" runat="server" Text='<%#Eval("CountryName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRegionId" runat="server" Visible="False" Text='<%#Eval("RegionId") %>'></asp:Label>
                                        <asp:LinkButton ID="lkbtnRegionEdit" runat="server" ToolTip="Edit" Text="Edit" CommandName="EditRegion">
                                        <img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnRegionUpdate" Visible="false" runat="server" ToolTip="Update"
                                            CommandName="UpdateRegion"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnRegionCancel" Visible="false" runat="server" ToolTip="Cancel"
                                            CommandName="CancelRegion"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnRegionActive" Visible="false" runat="server" ToolTip="Activate Region"
                                            CommandName="ActiveRegion"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnDelete" runat="server" ToolTip="DeActivate Region" CommandName="DeActiveRegion">
                                    <img src="../images/Deactivate.gif"   alt="DeActive" /></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:HiddenField ID="hfRegionId" runat="server" />
                        <asp:HiddenField ID="hfRegionname" runat="server" />
                        <asp:HiddenField ID="hfPageNo" runat="server" />
                        <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                        <asp:HiddenField ID="hfLastPage" runat="server" />
                        <asp:HiddenField ID="hfRecognize" runat="server" />
                        <asp:HiddenField ID="hfPageSize" runat="server" />
                    </div>
                    <div class="grid_boxes">
                        <div id="divdownpaging" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div id="hidepopup">
                        <%-- SAVE & NEXT Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeRegionpopup" runat="server" TargetControlID="btnpopup"
                            PopupControlID="pnlConfirmation" BackgroundCssClass="popbg">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnpopup" runat="server" Text="" Style="display: none;" />
                        <asp:Panel runat="server" ID="pnlConfirmation" DefaultButton="btnOk" CssClass="editpopup_two"
                            Style="display: none;">
                            <div class="panelMessage">
                                <div>
                                    <asp:Panel ID="pnlMsgPopup" runat="server" align="center">
                                        <asp:Label ID="lblMsgPopup" runat="server" Text=""></asp:Label>
                                    </asp:Panel>
                                </div>
                                <div class="clear pad_10">
                                </div>
                                <div class="buttondiv">
                                    <asp:Button ID="btnOk" runat="server" Text="OK" CssClass="btn_ok" Style="margin-left: 105px;" />
                                    <div class="space">
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- SAVE & NEXT Btn popup Closed--%>
                        <%-- Active Country Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                            TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="PanelActivate" runat="server" CssClass="modalPopup"
                            Style="display: none;">
                            <div class="popheader popheaderlblgreen">
                                <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                            </div>
                            <div class="space">
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btnActiveOk" OnClick="btnActiveOk_Click" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                    <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Active Country Btn popup Closed--%>
                        <%-- DeActive Country Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                            TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnDeActivate" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="PanelDeActivate" runat="server" CssClass="modalPopup"
                            Style="display: none; border-color: #639B00;">
                            <div class="popheader" style="background-color: red;">
                                <asp:Label ID="lblDeActiveRegion" runat="server"></asp:Label>
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <div class="space">
                                    </div>
                                    <asp:Button ID="btnDeActiveOk" OnClick="btnDeActiveOk_Click" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                    <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- DeActive Country popup Closed--%>
                        <%-- Grid Alert popup Start--%>
                        <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                            TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                            <div class="popheader popheaderlblred">
                                <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                            </div>
                            <div class="footer popfooterbtnrt">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Grid Alert popup Closed--%>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/PageValidations/AddRegions.js" type="text/javascript"></script>
    <%--<script src="../JavaScript/PageValidations/AddCountries.js" type="text/javascript"></script>--%>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
