﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using System.Xml;
using System.Data;
using System.Collections;
using Resources;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using System.Threading;

namespace UMS_Nigeria.Masters
{
    public partial class AddRegions : System.Web.UI.Page
    {

        # region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        // MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        string Key = "AddRegions";
        RegionsBAL _objRegionsBAL = new RegionsBAL();
        #endregion

        #region Properties

        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                //_ObjCommonMethods.BindStatesByBusinessUnits(ddlStateName,Session[UMS_Resource.SESSION_LOGINID].ToString(), string.Empty, true, UMS_Resource.DROPDOWN_SELECT,false);
                _ObjCommonMethods.BindCountries(ddlCountry, true, false);
                if (!string.IsNullOrEmpty(Request.QueryString[UMS_Resource.QSTR_COUNTRY_CODE]))
                {
                    ddlCountry.SelectedValue = _ObjiDsignBAL.Decrypt(Request.QueryString[UMS_Resource.QSTR_COUNTRY_CODE].ToString());
                }
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                RegionsBE objRegionsBe = new RegionsBE();

                objRegionsBe.RegionName = txtRegionName.Text;
                objRegionsBe.StateCode = ddlCountry.SelectedValue;
                objRegionsBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                xml = _objRegionsBAL.Region(objRegionsBe, Statement.Insert);
                objRegionsBe = _ObjiDsignBAL.DeserializeFromXml<RegionsBE>(xml);
                if (Convert.ToBoolean(objRegionsBe.RowsEffected > 0))
                {
                    BindGrid();
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    UCPaging2.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.REGION_DETAILS_INSERT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    txtRegionName.Text = string.Empty;
                    ddlCountry.SelectedIndex = 0;
                }
                else
                {
                    Message(Resource.REGION_EXISTED, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {

                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvRegionList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                RegionsListBE _ObjRegionsListBE = new RegionsListBE();
                RegionsBE _ObjRegionsBE = new RegionsBE();
                DataSet ds = new DataSet();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridRegionName = (TextBox)row.FindControl("txtGridRegionName");
                DropDownList ddlGridCountry = (DropDownList)row.FindControl("ddlGridCountry");
                Label lblRegionName = (Label)row.FindControl("lblRegionName");
                Label lblRegionId = (Label)row.FindControl("lblRegionId");
                hfRegionId.Value = lblRegionId.Text;
                Label lblCountryCode = (Label)row.FindControl("lblCountryCode");
                Label lblCountryName = (Label)row.FindControl("lblCountryName");

                hfRecognize.Value = string.Empty;
                switch (e.CommandName.ToUpper())
                {
                    case "EDITREGION":
                        lblRegionName.Visible = lblCountryName.Visible = false;
                        txtGridRegionName.Visible = ddlGridCountry.Visible = true;
                        txtGridRegionName.Text = lblRegionName.Text;
                        _ObjCommonMethods.BindCountries(ddlGridCountry, true, false);
                        ddlGridCountry.SelectedIndex = ddlGridCountry.Items.IndexOf(ddlGridCountry.Items.FindByValue(lblCountryCode.Text));

                        row.FindControl("lkbtnRegionCancel").Visible = row.FindControl("lkbtnRegionUpdate").Visible = true;
                        row.FindControl("lkbtnDelete").Visible = row.FindControl("lkbtnRegionEdit").Visible = false;
                        break;
                    case "CANCELREGION":
                        row.FindControl("lkbtnDelete").Visible = row.FindControl("lkbtnRegionEdit").Visible = true;
                        row.FindControl("lkbtnRegionCancel").Visible = row.FindControl("lkbtnRegionUpdate").Visible = false;
                        txtGridRegionName.Visible = ddlGridCountry.Visible = false;
                        lblCountryName.Visible = lblRegionName.Visible = true;
                        break;
                    case "UPDATEREGION":
                        _ObjRegionsBE.RegionName = txtGridRegionName.Text;
                        _ObjRegionsBE.RegionId = Convert.ToInt32(lblRegionId.Text);
                        _ObjRegionsBE.StateCode = ddlGridCountry.SelectedValue;
                        _ObjRegionsBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        xml = _objRegionsBAL.Region(_ObjRegionsBE, Statement.Update);
                        _ObjRegionsBE = _ObjiDsignBAL.DeserializeFromXml<RegionsBE>(xml);

                        if (_ObjRegionsBE.RowsEffected > 0)
                        {
                            BindGrid();
                            Message("Region Updated Successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                            ddlCountry.SelectedIndex = 0;
                            txtRegionName.Text = string.Empty;
                        }
                        else if (_ObjRegionsBE.RowsEffected == 0 && _ObjRegionsBE.Count > 0)
                            Message(string.Format(UMS_Resource.REGION_HAVE_CUSTOMERS, _ObjRegionsBE.Count), UMS_Resource.MESSAGETYPE_ERROR);
                        else
                            Message(Resource.REGION_EXISTED, UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "ACTIVEREGION":
                        //xml = _ObjUMS_Service.DeleteActiveState(1, lblStateCode.Text);
                        mpeActivate.Show();
                        lblActiveMsg.Text = "Are you sure you want to activate this Region?";
                        btnActiveOk.Focus();

                        break;
                    case "DEACTIVEREGION":
                        //xml = _ObjUMS_Service.DeleteActiveState(0, lblStateCode.Text);
                        mpeDeActive.Show();
                        //hfStateCode.Value = lblStateCode.Text;
                        lblDeActiveRegion.Text = "Are you sure you want to deactivate this Region?";
                        btnDeActiveOk.Focus();

                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void gvRegionList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnRegionUpdate = (LinkButton)e.Row.FindControl("lkbtnRegionUpdate");
                    TextBox txtGridRegionName = (TextBox)e.Row.FindControl("txtGridRegionName");
                    DropDownList ddlGridCountry = (DropDownList)e.Row.FindControl("ddlGridCountry");

                    lkbtnRegionUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridRegionName.ClientID + "','" + ddlGridCountry.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");


                    if (Convert.ToInt32(lblActiveStatusId.Text) == 2)
                    {
                        e.Row.FindControl("lkbtnDelete").Visible = e.Row.FindControl("lkbtnRegionEdit").Visible = false;
                        e.Row.FindControl("lkbtnRegionActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDelete").Visible = e.Row.FindControl("lkbtnRegionEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {

            try
            {
                RegionsBE _ObjRegionsBE = new RegionsBE();
                _ObjRegionsBE.RegionId = Convert.ToInt32(hfRegionId.Value);
                _ObjRegionsBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _ObjRegionsBE.ActiveStatusId = 1;

                xml = _objRegionsBAL.Region(_ObjRegionsBE, Statement.Delete);
                _ObjRegionsBE = _ObjiDsignBAL.DeserializeFromXml<RegionsBE>(xml);

                if (_ObjRegionsBE.RowsEffected > 0)
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message("Region activated successfully", UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                    Message("Region activation failed", UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                RegionsBE _ObjRegionsBE = new RegionsBE();
                _ObjRegionsBE.RegionId = Convert.ToInt32(hfRegionId.Value);
                _ObjRegionsBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _ObjRegionsBE.ActiveStatusId = 2;
                xml = _objRegionsBAL.Region(_ObjRegionsBE, Statement.Delete);
                _ObjRegionsBE = _ObjiDsignBAL.DeserializeFromXml<RegionsBE>(xml);
                if (_ObjRegionsBE.RowsEffected > 0)
                {
                    BindGrid();
                    Message("Regions deactivated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else if (_ObjRegionsBE.RowsEffected == 0 && _ObjRegionsBE.Count > 0)
                    Message(string.Format(UMS_Resource.REGION_HAVE_CUSTOMERS, _ObjRegionsBE.Count), UMS_Resource.MESSAGETYPE_ERROR);
                else
                    Message("Region deactivation failed.", UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        public void BindGrid()
        {
            try
            {
                RegionsListBE _objRegionsListBE = new RegionsListBE();
                RegionsBE _ObjRegionsBE = new RegionsBE();
                XmlDocument resultedXml = new XmlDocument();
                //resultedXml = _ObjUMS_Service.GetActiveCountriesList(Convert.ToInt32(hfPageNo.Value), PageSize);
                _ObjRegionsBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                _ObjRegionsBE.PageSize = PageSize;
                resultedXml = _objRegionsBAL.Get(_ObjRegionsBE, ReturnType.Get);
                _objRegionsListBE = _ObjiDsignBAL.DeserializeFromXml<RegionsListBE>(resultedXml);

                if (_objRegionsListBE.Items.Count > 0)
                {
                    divdownpaging.Visible = divpaging.Visible = true;
                    UCPaging1.Visible = UCPaging2.Visible = true;
                    hfTotalRecords.Value = _objRegionsListBE.Items[0].TotalRecords.ToString();
                    gvRegionList.DataSource = _objRegionsListBE.Items;
                    gvRegionList.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = _objRegionsListBE.Items.Count.ToString();
                    divdownpaging.Visible = divpaging.Visible = false;
                    UCPaging1.Visible = UCPaging2.Visible = false;
                    gvRegionList.DataSource = new DataTable(); ;
                    gvRegionList.DataBind();
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}