﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddRouteManagement
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 01-Apr-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using UMS_NigeriaBAL;
using iDSignHelper;
using Resources;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Threading;
using System.Globalization;

namespace UMS_Nigeria.Masters
{
    public partial class AddRouteManagement : System.Web.UI.Page
    {
        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        string Key = UMS_Resource.KEY_ADDROUTEMANAGEMENT;
        Hashtable htableMaxLength = new Hashtable();

        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
        }
        public string BusinessUnitName
        {
            get { return string.IsNullOrEmpty(ddlBusinessUnit.SelectedValue) ? Constants.SelectedValue : ddlBusinessUnit.SelectedValue; }
            set { ddlBusinessUnit.SelectedValue = value.ToString(); }
        }
        private string RouteName
        {
            get { return txtRouteName.Text.Trim(); }
            set { txtRouteName.Text = value; }
        }
        private string RouteCode
        {
            get { return txtRouteCode.Text.Trim(); }
            set { txtRouteCode.Text = value; }
        }
        //private string AvgMaxLimit
        //{
        //    get { return txtAvgMaxLimit.Text.Trim(); }
        //    set { txtAvgMaxLimit.Text = value; }
        //}
        private string Details
        {
            get { return txtDetails.Text.Trim(); }
            set { txtDetails.Text = value; }
        }
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void lkbtnAddNewBusinessUnit_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_BUSINESS_UNITS_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = objCommonMethods.GetPagePath(this.Request.Url);
                //if (objCommonMethods.IsAccess(path))
                //{
                    //PageAccessPermissions();
                    AssignMaxLength();
                    objCommonMethods.BindBusinessUnits(ddlBusinessUnit, string.Empty, true);
                    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    {
                        ddlBusinessUnit.SelectedIndex = ddlBusinessUnit.Items.IndexOf(ddlBusinessUnit.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                        ddlBusinessUnit.Enabled = false;
                    }
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                //}
                //else { Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }
            }
        }

        //private void PageAccessPermissions()
        //{
        //    try
        //    {
        //        if (objCommonMethods.IsAccess(UMS_Resource.ADD_BUSINESS_UNITS_PAGE)) { lkbtnAddNewBusinessUnit.Visible = true; } else { lkbtnAddNewBusinessUnit.Visible = false; }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                hfRecognize.Value = UMS_Resource.SAVE;
                lblActiveMsg.Text = Resource.CHK_BEFORE_U_SAVE;
                mpeActivate.Show();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlBusinessUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                txtRouteName.Focus();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvRouteList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                hfRecognize.Value = string.Empty;
                MastersBE objMastersBE = new MastersBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridRouteName = (TextBox)row.FindControl("txtGridRouteName");
                DropDownList ddlGridBusinessUnitName = (DropDownList)row.FindControl("ddlGridBusinessUnitName");
                //TextBox txtGridAvgMaxLimit = (TextBox)row.FindControl("txtGridAvgMaxLimit");
                TextBox txtGridDetails = (TextBox)row.FindControl("txtGridDetails");
                TextBox txtGridRouteCode = (TextBox)row.FindControl("txtGridRouteCode");

                Label lblBUName = (Label)row.FindControl("lblBUName");
                Label lblRouteName = (Label)row.FindControl("lblRouteName");
                Label lblRouteCode = (Label)row.FindControl("lblRouteCode");
                //Label lblAvgMaxLimit = (Label)row.FindControl("lblAvgMaxLimit");
                Label lblDetails = (Label)row.FindControl("lblDetails");
                Label lblRouteId = (Label)row.FindControl("lblRouteId");
                Label lblBU_ID = (Label)row.FindControl("lblBU_ID");
                switch (e.CommandName.ToUpper())
                {
                    case "EDITROUTE":
                        lblRouteCode.Visible = lblBUName.Visible = lblRouteName.Visible = lblDetails.Visible = false;
                        txtGridRouteCode.Visible = ddlGridBusinessUnitName.Visible = txtGridRouteName.Visible = txtGridDetails.Visible = true;
                        objCommonMethods.BindBusinessUnits(ddlGridBusinessUnitName, string.Empty, true);
                        ddlGridBusinessUnitName.SelectedIndex = ddlGridBusinessUnitName.Items.IndexOf(ddlGridBusinessUnitName.Items.FindByValue(lblBU_ID.Text));
                        txtGridRouteName.Text = lblRouteName.Text;
                        txtGridRouteCode.Text = lblRouteCode.Text;
                        //txtGridAvgMaxLimit.Text = lblAvgMaxLimit.Text;
                        txtGridDetails.Text = objiDsignBAL.ReplaceNewLines(lblDetails.Text, false);
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;
                        break;
                    case "CANCELROUTE":
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                        lblRouteCode.Visible = lblBUName.Visible = lblRouteName.Visible = lblDetails.Visible = true;
                        txtGridRouteCode.Visible = ddlGridBusinessUnitName.Visible = txtGridRouteName.Visible = txtGridDetails.Visible = false;
                        break;
                    case "UPDATEROUTE":
                        objMastersBE.BU_ID = ddlGridBusinessUnitName.SelectedValue;
                        objMastersBE.RouteName = txtGridRouteName.Text;
                        objMastersBE.RouteCode = txtGridRouteCode.Text;
                        //objMastersBE.AvgMaxLimit = Convert.ToDecimal(txtGridAvgMaxLimit.Text);
                        objMastersBE.Details = objiDsignBAL.ReplaceNewLines(txtGridDetails.Text, true);
                        objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objMastersBE.RouteId = Convert.ToInt32(lblRouteId.Text);
                        objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.RouteMangement(objMastersBE, Statement.Update));

                        if (objMastersBE.IsSuccess)
                        {
                            BindGrid();
                            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            Message(UMS_Resource.ROUTE_UPDATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        else if (objMastersBE.IsRouteNameExists)
                        {
                            Message(UMS_Resource.ROUTE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else if (objMastersBE.IsRouteCodeExists)
                        {
                            Message(Resource.ROUTE_CODE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else
                            Message(UMS_Resource.ROUTE_UPDATE_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                        break;
                    case "DEACTIVEROUTE":
                        mpeDeActive.Show();
                        hfId.Value = lblRouteId.Text;
                        lblDeActiveMsg.Text = UMS_Resource.DEACTIVE_ROUTE_POPUP_TEXT;
                        btnDeActiveOk.Focus();
                        //objMastersBE.RouteId = Convert.ToInt32(lblRouteId.Text);
                        //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objMastersBE.ActiveStatusId = Constants.DeActive;
                        //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.RouteMangement(objMastersBE, Statement.Delete));

                        //if (objMastersBE.IsSuccess)
                        //{
                        //    BindRouteList();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message(UMS_Resource.ROUTE_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        //}
                        //else
                        //    Message(UMS_Resource.ROUTE_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                        break;
                    case "ACTIVEROUTE":

                        mpeActivate.Show();
                        hfId.Value = lblRouteId.Text;
                        lblActiveMsg.Text = UMS_Resource.ACTIVE_ROUTE_POPUP_TEXT;
                        btnActiveOk.Focus();
                        //objMastersBE.RouteId = Convert.ToInt32(lblRouteId.Text);
                        //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objMastersBE.ActiveStatusId = Constants.Active;
                        //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.RouteMangement(objMastersBE, Statement.Delete));

                        //if (objMastersBE.IsSuccess)
                        //{
                        //    BindRouteList();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message(UMS_Resource.ROUTE_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        //}
                        //else
                        //    Message(UMS_Resource.ROUTE_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                        break;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (hfRecognize.Value == Constants.SAVE)
                {
                    hfRecognize.Value = string.Empty;
                    MastersBE objMastersBE = InsertRouteDetails();
                    if (objMastersBE.IsSuccess)
                    {
                        Message(UMS_Resource.ROUTE_INSERT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        RouteCode = Details = RouteName = string.Empty;
                        if (Session[UMS_Resource.SESSION_USER_BUID] == null)
                        {
                            ddlBusinessUnit.SelectedIndex = 0;
                        }
                        BindGrid();
                        UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    }
                    else if (objMastersBE.IsRouteNameExists)
                    {
                        Message(UMS_Resource.ROUTE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else if (objMastersBE.IsRouteCodeExists)
                    {
                        Message(Resource.ROUTE_CODE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else
                    {
                        Message(UMS_Resource.ROUTE_INSERT_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
                else
                {
                    MastersBE objMastersBE = new MastersBE();
                    objMastersBE.RouteId = Convert.ToInt32(hfId.Value);
                    objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objMastersBE.ActiveStatusId = Constants.Active;
                    objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.RouteMangement(objMastersBE, Statement.Delete));

                    if (objMastersBE.IsSuccess)
                    {
                        BindGrid();
                        UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        Message(UMS_Resource.ROUTE_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    }
                    else
                        Message(UMS_Resource.ROUTE_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.RouteId = Convert.ToInt32(hfId.Value);
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.ActiveStatusId = Constants.DeActive;
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.RouteMangement(objMastersBE, Statement.Delete));

                if (objMastersBE.IsSuccess)
                {
                    BindGrid();
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.ROUTE_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else if (objMastersBE.Count > 0)
                {
                    lblgridalertmsg.Text = "Custoemrs are associated with this Route. Shift the customers to other Route before deactivating.";
                    mpegridalert.Show();
                    btngridalertok.Focus();
                }
                else
                    Message(UMS_Resource.ROUTE_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvRouteList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    TextBox txtGridRouteName = (TextBox)e.Row.FindControl("txtGridRouteName");
                    TextBox txtGridRouteCode = (TextBox)e.Row.FindControl("txtGridRouteCode");
                    DropDownList ddlGridBusinessUnitName = (DropDownList)e.Row.FindControl("ddlGridBusinessUnitName");

                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + ddlGridBusinessUnitName.ClientID + "','"
                                                                                      + txtGridRouteName.ClientID + "','"
                                                                                      + txtGridRouteCode.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");

                    htableMaxLength = objCommonMethods.getApplicationSettings(UMS_Resource.SETTING_ROUTE_CODE_LENGTH);
                    txtGridRouteCode.Attributes.Add("MaxLength", htableMaxLength[Constants.Route].ToString());
                    if (Convert.ToInt32(lblActiveStatusId.Text) == Constants.DeActive)
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods
        private MastersBE InsertRouteDetails()
        {
            MastersBE objMastersBE = new MastersBE();
            try
            {
                objMastersBE.BU_ID = BusinessUnitName;
                objMastersBE.RouteName = RouteName;
                objMastersBE.RouteCode = RouteCode;
                //objMastersBE.AvgMaxLimit = Convert.ToDecimal(AvgMaxLimit);
                objMastersBE.Details = objiDsignBAL.ReplaceNewLines(Details, true);
                objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.RouteMangement(objMastersBE, Statement.Insert));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            return objMastersBE;
        }

        public void BindGrid()
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.BU_ID = ddlBusinessUnit.SelectedValue;
                objMastersBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                objMastersBE.PageSize = PageSize;
                MastersListBE objMastersListBE = objiDsignBAL.DeserializeFromXml<MastersListBE>(objMastersBAL.RouteMangement(objMastersBE, ReturnType.Get));
                if (objMastersListBE.Items.Count > 0)
                {
                    gvRouteList.DataSource = objMastersListBE.Items;
                    gvRouteList.DataBind();
                    hfTotalRecords.Value = objMastersListBE.Items[0].TotalRecords.ToString();
                    divdownpaging.Visible = divpaging.Visible = true;
                    //CreatePagingDT(objMastersListBE.Items[0].TotalRecords);
                }
                else
                {
                    gvRouteList.DataSource = new DataTable();
                    gvRouteList.DataBind();
                    divdownpaging.Visible = divpaging.Visible = false;
                    hfTotalRecords.Value = Constants.Zero.ToString();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void AssignMaxLength()
        {
            try
            {
                htableMaxLength = objCommonMethods.getApplicationSettings(UMS_Resource.SETTING_ROUTE_CODE_LENGTH);
                txtRouteCode.Attributes.Add("MaxLength", htableMaxLength[Constants.Route].ToString());
                hfRouteCodeLength.Value = htableMaxLength[Constants.Route].ToString();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        //#region Pageing
        //protected void lkbtnFirst_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        BindRouteList();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnPrevious_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum -= Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        BindRouteList();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnNext_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum += Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        BindRouteList();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnLast_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = hfLastPage.Value;
        //        BindRouteList();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        BindRouteList();
        //        hfPageSize.Value = ddlPageSize.SelectedItem.Text;
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //private void BindPagingDropDown(int TotalRecords)
        //{
        //    try
        //    {
        //        objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
        //        //ddlPageSize.Items.Clear();
        //        //if (TotalRecords < Constants.PageSizeStarts)
        //        //{
        //        //    ListItem item = new ListItem(Constants.PageSizeStarts.ToString(), Constants.PageSizeStarts.ToString());
        //        //    ddlPageSize.Items.Add(item);
        //        //}
        //        //else
        //        //{
        //        //    int previousSize = Constants.PageSizeStarts;
        //        //    for (int i = Constants.PageSizeStarts; i <= TotalRecords; i = i + Constants.PageSizeIncrement)
        //        //    {
        //        //        ListItem item = new ListItem(i.ToString(), i.ToString());
        //        //        ddlPageSize.Items.Add(item);
        //        //        previousSize = i;
        //        //    }
        //        //    if (previousSize < TotalRecords)
        //        //    {
        //        //        ListItem item = new ListItem((previousSize + Constants.PageSizeIncrement).ToString(), (previousSize + Constants.PageSizeIncrement).ToString());
        //        //        ddlPageSize.Items.Add(item);
        //        //    }
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //private void CreatePagingDT(int TotalNoOfRecs)
        //{
        //    try
        //    {
        //        double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
        //        DataTable dtPages = new DataTable();
        //        dtPages.Columns.Add("PageNo", typeof(int));
        //        int currentpage = Convert.ToInt32(hfPageNo.Value);
        //        lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
        //        objiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
        //        lblCurrentPage.Text = hfPageNo.Value;
        //        if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
        //        else { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Blue; }
        //        if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //#endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}