﻿<%@ Page Title="::Add Service Center::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master" Theme="Green"
    AutoEventWireup="true" CodeBehind="AddServiceCenters.aspx.cs" Inherits="UMS_Nigeria.Masters.AddServiceCenters" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <div class="out-bor">
        <asp:UpdatePanel ID="upSC" runat="server">
            <ContentTemplate>
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddServiceCenter" runat="server" Text="<%$ Resources:Resource, ADD_SERVICE_CENTER%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litServiceUnitName" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT_NAME%>"></asp:Literal>
                                    <span class="span_star">*</span></label><br />
                                <asp:DropDownList ID="ddlServiceUnitName" TabIndex="1" onchange="DropDownlistOnChangelbl(this,'spanddlServiceUnitName','Service Unit')"
                                    runat="server" CssClass="text-box text-select">
                                </asp:DropDownList>
                                <%--<span class="text-heading_2">
                                    <asp:HyperLink ID="lkbtnAddNewServiceUnit" NavigateUrl="~/Masters/AddServiceUnits.aspx"
                                        runat="server" Text="<%$ Resources:Resource, ADD%>"></asp:HyperLink></span>--%>
                                <div class="space">
                                </div>
                                <span id="spanddlServiceUnitName" class="span_color"></span>
                            </div>
                            <div class="text-inner">
                                <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, ADDRESS_2%>"></asp:Literal>
                                <span class="span_star">*</span><br />
                                <asp:TextBox CssClass="text-box" ID="txtAddress2" placeholder="Enter Address 2" runat="server"
                                    onblur="return TextBoxBlurValidationlbl(this,'spanAddress2','Address 2')"
                                    TabIndex="4"></asp:TextBox>
                                <span id="spanAddress2" class="span_color"></span>
                            </div>
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litNotes" runat="server" Text="<%$ Resources:Resource, NOTES%>"></asp:Literal></label>
                                <br />
                                <asp:TextBox CssClass="text-box" ID="txtNotes" runat="server" TabIndex="7" TextMode="MultiLine"
                                    placeholder="Enter Notes Here"></asp:TextBox>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label>
                                    <asp:Literal ID="litServiceCenterName" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER_NAME%>"></asp:Literal><span
                                        class="span_star">*</span></label><br />
                                <asp:TextBox CssClass="text-box" ID="txtServiceCenterName" runat="server" TabIndex="2"
                                    onblur="return TextBoxBlurValidationlbl(this,'spanServiceCenterName','Service Center Name')"
                                    MaxLength="300" placeholder="Enter Service Center Name"></asp:TextBox>
                                <span id="spanServiceCenterName" class="span_color"></span>
                            </div>
                            <div class="text-inner">
                                <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, CITY%>"></asp:Literal><span
                                    class="span_star">*</span><br />
                                <asp:TextBox CssClass="text-box" ID="txtCity" placeholder="Enter City" runat="server"
                                    onblur="return TextBoxBlurValidationlbl(this,'spanCity','City Name')" MaxLength="50"
                                    TabIndex="5"></asp:TextBox>
                                <span id="spanCity" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, ADDRESS_1%>"></asp:Literal>
                                <span class="span_star">*</span></label><br />
                                <asp:TextBox CssClass="text-box" ID="txtAddress1" placeholder="Enter Address 1" runat="server"
                                    onblur="return TextBoxBlurValidationlbl(this,'spanAddress1','Address 1')"
                                    TabIndex="3" MaxLength="99"></asp:TextBox>
                                <span id="spanAddress1" class="span_color"></span>
                            </div>
                            <div class="text-inner">
                                <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, ZIP%>"></asp:Literal><span
                                    class="span_star"></span><br />
                                <asp:TextBox CssClass="text-box" ID="txtZipCode" MaxLength="10" placeholder="Enter Zip Code" runat="server"
                                    
                                    TabIndex="10"></asp:TextBox><%--onblur="return TextBoxBlurValidationlbl(this,'spanZip','Zip Code')"--%>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtZipCode"
                                    FilterType="Numbers" ValidChars=" ">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanZip" class="span_color"></span>
                            </div>
                        </div>
                    </div>
                    <div class="box_total">
                        <div class="box_totalTwi_c">
                            <asp:Button ID="btnSave" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                CssClass="box_s" runat="server" OnClick="btnSave_Click" TabIndex="8" />
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div class="box_totalTwo_b">
                            <div class="text-heading_2">
                                <a href="SearchServiceCenterMaster.aspx"><span class="search_img"></span>
                                    <asp:Literal ID="Literal3" runat="server" Text="Service Center Search"></asp:Literal></a></div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="grid_boxes">
                        <div class="grid_pagingTwo_top">
                            <div class="paging_top_title">
                                <asp:Literal ID="litCountriesList" runat="server" Text="<%$ Resources:Resource, GRID_SERVICE_CENTERS_LIST%>"></asp:Literal>
                            </div>
                            <div class="paging_top_rightTwo_content" id="divpaging" runat="server">
                                <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvServiceCentersList" runat="server" AutoGenerateColumns="False"
                            AlternatingRowStyle-CssClass="color" HeaderStyle-CssClass="grid_tb_hd" OnRowCommand="gvServiceCentersList_RowCommand"
                            OnRowDataBound="gvServiceCentersList_RowDataBound">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                    HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, SERVICE_CENTER_ID%>" ItemStyle-Width="8%"
                                    Visible="false" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblServiceCenterId" runat="server" Text='<%#Eval("ServiceCenterId") %>'></asp:Label>
                                        <asp:Label ID="lblActiveStatusId" Visible="false" runat="server" Text='<%#Eval("ActiveStatusId") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SC" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblServiceCenterName" runat="server" Text='<%#Eval("ServiceCenterName") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridServiceCenterName" MaxLength="300" placeholder="Enter Service Center Name"
                                            runat="server" Visible="false"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, CODE%>" ItemStyle-Width="4%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSCCode" runat="server" Text='<%#Eval("SCCode") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, STATE_NAME%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblState" runat="server" Text='<%#Eval("StateName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, SERVICE_UNIT%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblServiceUnitId" runat="server" Visible="false" Text='<%#Eval("SU_ID") %>'></asp:Label>
                                        <asp:DropDownList ID="ddlGridServiceUnitName" runat="server" class="text-box select-box" style="width:100%;"
                                            Visible="false">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblServiceUnitName" runat="server" Text='<%#Eval("ServiceUnitName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, ADDRESS_1%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress1" runat="server" Text='<%#Eval("Address1") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridAddress1" MaxLength="99" placeholder="Enter Address1"
                                            runat="server" Visible="false"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, ADDRESS_2%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress2" runat="server" Text='<%#Eval("Address2") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridAddress2" MaxLength="99" placeholder="Enter Address1"
                                            runat="server" Visible="false"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, CITY%>" ItemStyle-Width="5%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCity" runat="server" Text='<%#Eval("City") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridCity" MaxLength="50" placeholder="Enter City"
                                            runat="server" Visible="false"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, ZIP%>" ItemStyle-Width="7%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblZip" runat="server" Text='<%#Eval("ZIP") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridZip" MaxLength="10" placeholder="Enter Zip"
                                            runat="server" Visible="false"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, NOTES%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNotes" runat="server" Text='<%#Eval("Notes") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridNotes" placeholder="Enter Notes Here"
                                            runat="server" Visible="false" TextMode="MultiLine"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="5%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lkbtnEdit" runat="server" ToolTip="Edit" Text="Edit" CommandName="EditServiceCenter">
                                        <img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnUpdate" Visible="false" runat="server" ToolTip="Update"
                                            CommandName="UpdateServiceCenter"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnCancel" Visible="false" runat="server" ToolTip="Cancel"
                                            CommandName="CancelServiceCenter"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnActive" Visible="false" runat="server" ToolTip="Activate Service Center"
                                            CommandName="ActiveServiceCenter"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnDeActive" runat="server" ToolTip="DeActivate Service Center"
                                            CommandName="DeActiveServiceCenter" CommandArgument='<%#Eval("ServiceCenterId") %>'><img src="../images/Deactivate.gif" alt="DeActive" /></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:HiddenField ID="hfPageSize" runat="server" />
                        <asp:HiddenField ID="hfPageNo" runat="server" />
                        <asp:HiddenField ID="hfLastPage" runat="server" />
                        <asp:HiddenField ID="hfTotalRecords" runat="server" />
                        <asp:HiddenField ID="hfSCCodeLength" runat="server" />
                        <asp:HiddenField ID="hfRecognize" runat="server" />
                        <asp:HiddenField ID="hfServiceCenterId" runat="server" />
                    </div>
                    <div class="grid_boxes">
                        <div id="divdownpaging" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="hidepopup">
                    <%-- <asp:ModalPopupExtender ID="mpeCountrypopup" runat="server" TargetControlID="btnpopup"
                        PopupControlID="pnlConfirmation" BackgroundCssClass="popbg">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnpopup" runat="server" Text="" Style="display: none;" />
                    <asp:Panel runat="server" ID="pnlConfirmation" DefaultButton="btnOk" CssClass="editpopup_two">
                        <div class="panelMessage">
                            <div>
                                <asp:Panel ID="pnlMsgPopup" runat="server" align="center">
                                    <asp:Label ID="lblMsgPopup" runat="server" Text=""></asp:Label>
                                </asp:Panel>
                            </div>
                            <div class="clear pad_10">
                            </div>
                            <div class="buttondiv">
                                <asp:Button ID="btnOk" runat="server" Text="OK" OnClick="btnOk_Click" CssClass="btn_ok"
                                    Style="margin-left: 106px;" />
                                <br />
                            </div>
                        </div>
                    </asp:Panel>--%>
                    <%-- Active Btn popup Start--%>
                    <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                        TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelActivate" runat="server" CssClass="modalPopup"
                        Style="display: none; border-color: #639B00;">
                        <div class="popheader" style="background-color: #639B00;">
                            <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                        </div>
                        <br />
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnActiveOk" runat="server" OnClick="btnActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Active  Btn popup Closed--%>
                    <%-- DeActive  Btn popup Start--%>
                    <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                        TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnDeActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelDeActivate" runat="server" CssClass="modalPopup"
                        Style="display: none; border-color: #639B00;">
                        <div class="popheader" style="background-color: red;">
                            <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnDeActiveOk" runat="server" OnClick="btnDeActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- DeActive  popup Closed--%>
                    <%-- Shift customers  Btn popup Start--%>
                    <asp:ModalPopupExtender ID="mpeShiftCustomer" runat="server" PopupControlID="pnlShiftCustomer"
                        TargetControlID="btnShipCustomerDemo" BackgroundCssClass="modalBackground" CancelControlID="btnShipCustomerCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnShipCustomerDemo" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="pnlShiftCustomer" runat="server" CssClass="modalPopup"
                        Style="display: none; border-color: #639B00;">
                        <div class="popheader" style="background-color: red;">
                            <asp:Label ID="lblShiftCustomer" runat="server"></asp:Label>
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnShipCustomer" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnShipCustomerCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" style="display:none;" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Shift customers  popup Closed--%>
                    <%-- Grid Alert popup Start--%>
                    <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                        TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                        <div class="popheader popheaderlblred">
                            <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Grid Alert popup Closed--%>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/PageValidations/AddServiceCenters.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { assignDiv('rptrMainMasterMenu_156_0', '95') });
    </script>
</asp:Content>
