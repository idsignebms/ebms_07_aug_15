﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddServiceUnits
                     
 Developer        : id065-Bhimaraju V
 Creation Date    : 20-Feb-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using Resources;
using iDSignHelper;
using System.Xml;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Globalization;
using System.Collections;

namespace UMS_Nigeria.Masters
{
    public partial class AddServiceCenters : System.Web.UI.Page
    {
        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        string Key = "AddServiceCenters";
        DataTable dtData = new DataTable();

        CustomerDetailsBe _ObjCustomerDetailsBe = new CustomerDetailsBe();
        CustomerDetailsBAL _ObjCustomerDetailsBAL = new CustomerDetailsBAL();
        static int customersCount = 0;
        Hashtable htableMaxLength = new Hashtable();

        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = objCommonMethods.GetPagePath(this.Request.Url);
                //if (objCommonMethods.IsAccess(path))
                //{

                //PageAccessPermissions();
                //AssignMaxLength();
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    objCommonMethods.BindServiceUnits(ddlServiceUnitName, Session[UMS_Resource.SESSION_USER_BUID].ToString(), true);
                else
                    objCommonMethods.BindServiceUnits(ddlServiceUnitName, string.Empty, true);
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                if (!string.IsNullOrEmpty(Request.QueryString[UMS_Resource.QSTR_SERVICE_UNIT_CODE]))
                {
                    ddlServiceUnitName.SelectedValue = objiDsignBAL.Decrypt(Request.QueryString[UMS_Resource.QSTR_SERVICE_UNIT_CODE]).ToString();
                }
                //}
                //else { Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }
            }

        }
        //private void PageAccessPermissions()
        //{
        //    try
        //    {
        //        if (objCommonMethods.IsAccess(UMS_Resource.ADD_SERVICE_UNITS_PAGE)) { lkbtnAddNewServiceUnit.Visible = true; } else { lkbtnAddNewServiceUnit.Visible = false; }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                hfRecognize.Value = UMS_Resource.SAVE;
                lblActiveMsg.Text = Resource.CHK_BEFORE_U_SAVE;
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //protected void btnSaveNext_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        InsertServiceCenterDetails();
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void btnOk_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Response.Redirect(UMS_Resource.ADD_SUB_STATION_PAGE + "?" + UMS_Resource.QSTR_SERVICE_CENTER_CODE + "=" + objiDsignBAL.Encrypt(hfServiceCenterId.Value), false);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void lkbtnAddNewServiceUnit_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_SERVICE_UNITS_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvServiceCentersList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                hfRecognize.Value = string.Empty;
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridServiceCenterName = (TextBox)row.FindControl("txtGridServiceCenterName");
                TextBox txtGridNotes = (TextBox)row.FindControl("txtGridNotes");
                DropDownList ddlGridServiceUnitName = (DropDownList)row.FindControl("ddlGridServiceUnitName");

                Label lblServiceCenterId = (Label)row.FindControl("lblServiceCenterId");
                Label lblServiceCenterName = (Label)row.FindControl("lblServiceCenterName");
                Label lblServiceUnitId = (Label)row.FindControl("lblServiceUnitId");
                Label lblServiceUnitName = (Label)row.FindControl("lblServiceUnitName");

                Label lblNotes = (Label)row.FindControl("lblNotes");

                Label lblAddress1 = (Label)row.FindControl("lblAddress1");
                Label lblAddress2 = (Label)row.FindControl("lblAddress2");
                Label lblCity = (Label)row.FindControl("lblCity");
                Label lblZip = (Label)row.FindControl("lblZip");
                TextBox txtGridAddress1 = (TextBox)row.FindControl("txtGridAddress1");
                TextBox txtGridAddress2 = (TextBox)row.FindControl("txtGridAddress2");
                TextBox txtGridCity = (TextBox)row.FindControl("txtGridCity");
                TextBox txtGridZip = (TextBox)row.FindControl("txtGridZip");

                switch (e.CommandName.ToUpper())
                {
                    case "EDITSERVICECENTER":
                        lblServiceUnitName.Visible = lblNotes.Visible = lblServiceCenterName.Visible = lblServiceUnitId.Visible =
                            lblAddress1.Visible = lblAddress2.Visible = lblCity.Visible = lblZip.Visible = false;
                        txtGridServiceCenterName.Visible = ddlGridServiceUnitName.Visible = txtGridNotes.Visible =
                            txtGridAddress1.Visible = txtGridAddress2.Visible = txtGridCity.Visible = txtGridZip.Visible = true;
                        txtGridServiceCenterName.Text = lblServiceCenterName.Text;
                        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                            objCommonMethods.BindServiceUnits(ddlGridServiceUnitName, Session[UMS_Resource.SESSION_USER_BUID].ToString(), true);
                        else
                            objCommonMethods.BindServiceUnits(ddlGridServiceUnitName, string.Empty, true);
                        ddlGridServiceUnitName.SelectedIndex = ddlGridServiceUnitName.Items.IndexOf(ddlGridServiceUnitName.Items.FindByValue(lblServiceUnitId.Text));
                        txtGridNotes.Text = objiDsignBAL.ReplaceNewLines(lblNotes.Text, false);

                        txtGridAddress1.Text = lblAddress1.Text;
                        txtGridAddress2.Text = lblAddress2.Text;
                        txtGridCity.Text = lblCity.Text;
                        txtGridZip.Text = lblZip.Text;

                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;
                        break;
                    case "CANCELSERVICECENTER":
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                        txtGridServiceCenterName.Visible = ddlGridServiceUnitName.Visible = txtGridNotes.Visible = txtGridAddress1.Visible = txtGridAddress2.Visible = txtGridCity.Visible = txtGridZip.Visible = false;
                        lblServiceUnitName.Visible = lblNotes.Visible = lblServiceCenterName.Visible = lblAddress1.Visible = lblAddress2.Visible = lblCity.Visible = lblZip.Visible = true;
                        break;
                    case "UPDATESERVICECENTER":
                        objMastersBE.ServiceCenterId = lblServiceCenterId.Text;
                        objMastersBE.SU_ID = ddlGridServiceUnitName.SelectedValue;
                        objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objMastersBE.ServiceCenterName = txtGridServiceCenterName.Text;

                        objMastersBE.Address1 = txtGridAddress1.Text.Trim();
                        objMastersBE.Address2 = txtGridAddress2.Text.Trim();
                        objMastersBE.City = txtGridCity.Text.Trim();
                        objMastersBE.ZIP = txtGridZip.Text.Trim();

                        //objMastersBE.SCCode = txtGridSCCode.Text;
                        objMastersBE.Notes = objiDsignBAL.ReplaceNewLines(txtGridNotes.Text, true);
                        xml = objMastersBAL.ServiceCenter(objMastersBE, Statement.Update);
                        objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                        if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        {
                            BindGrid();
                            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            Message(UMS_Resource.SERVICE_CENTER_UPDATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                        }
                        else if (objMastersBE.IsSCCodeExists)
                        {
                            Message(Resource.SERVICE_CENTER_CODE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else
                            Message(UMS_Resource.SERVICE_CENTER_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "ACTIVESERVICECENTER":
                        mpeActivate.Show();
                        hfServiceCenterId.Value = lblServiceCenterId.Text;
                        lblActiveMsg.Text = UMS_Resource.ACTIVE_SC_POPUP_TEXT;
                        btnActiveOk.Focus();
                        //objMastersBE.ActiveStatusId = Constants.Active;
                        //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objMastersBE.ServiceCenterId = lblServiceCenterId.Text;
                        //xml = objMastersBAL.ServiceCenter(objMastersBE, Statement.Delete);
                        //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                        //if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        //{
                        //    BindGridServiceCentersList();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message( UMS_Resource.SERVICE_CENTER_ACTIVATE_SUCESS,UMS_Resource.MESSAGETYPE_SUCCESS);

                        //}
                        //else
                        //    Message( UMS_Resource.SERVICE_CENTER_ACTIVATE_FAIL,UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "DEACTIVESERVICECENTER":
                        if (CustomerCount(e.CommandArgument.ToString()))
                        {
                            mpeShiftCustomer.Show();
                            //hfServiceCenterId.Value = lblServiceCenterId.Text;
                            //lblShiftCustomer.Text = UMS_Resource.SHIFT_CUSTOMERS_SC.Insert(24, customersCount.ToString() + " ");
                            lblShiftCustomer.Text = UMS_Resource.SHIFT_CUSTOMERS_SC;
                            btnShipCustomer.Focus();
                        }
                        else
                        {
                            mpeDeActive.Show();
                            hfServiceCenterId.Value = lblServiceCenterId.Text;
                            lblDeActiveMsg.Text = UMS_Resource.DEACTIVE_SC_POPUP_TEXT;
                            btnDeActiveOk.Focus();
                        }
                        //objMastersBE.ActiveStatusId = Constants.DeActive;
                        //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objMastersBE.ServiceCenterId = lblServiceCenterId.Text;
                        //xml = objMastersBAL.ServiceCenter(objMastersBE, Statement.Delete);
                        //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                        //if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        //{
                        //    BindGridServiceCentersList();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message( UMS_Resource.SERVICE_CENTER_DEACTIVATE_SUCESS,UMS_Resource.MESSAGETYPE_SUCCESS);

                        //}
                        //else
                        //    Message(UMS_Resource.SERVICE_CENTER_DEACTIVATE_FAIL,UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (hfRecognize.Value == Constants.SAVE)
                {
                    hfRecognize.Value = string.Empty;
                    MastersBE objMastersBE = new MastersBE();
                    objMastersBE.ServiceCenterName = txtServiceCenterName.Text;
                    //objMastersBE.StateCode = ddlStateName.SelectedValue;
                    objMastersBE.SU_ID = ddlServiceUnitName.SelectedValue;
                    //objMastersBE.SCCode = txtSCCode.Text;
                    objMastersBE.Address1 = txtAddress1.Text;
                    objMastersBE.Address2 = txtAddress2.Text;
                    objMastersBE.City = txtCity.Text;
                    objMastersBE.ZIP = txtZipCode.Text;
                    objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objMastersBE.Notes = objiDsignBAL.ReplaceNewLines(txtNotes.Text, true);
                    xml = objMastersBAL.ServiceCenter(objMastersBE, Statement.Insert);
                    objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);
                    if (Convert.ToBoolean(objMastersBE.IsSuccess))
                    {
                        BindGrid();
                        UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        Message(UMS_Resource.SERVICE_CENTER_INSERT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        txtServiceCenterName.Text = txtNotes.Text = txtAddress1.Text = txtAddress2.Text = txtCity.Text = txtZipCode.Text = string.Empty;
                    }
                    else if (objMastersBE.IsSCCodeExists)
                    {
                        Message(Resource.SERVICE_CENTER_CODE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else if (string.IsNullOrEmpty(objMastersBE.SCCode))
                    {
                        Message("Can not add the Service Center. Problem in SC Code Generation.", UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else
                        Message(UMS_Resource.SERVICE_CENTER_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                {
                    MastersBE objMastersBE = new MastersBE();
                    objMastersBE.ActiveStatusId = Constants.Active;
                    objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objMastersBE.ServiceCenterId = hfServiceCenterId.Value;
                    objMastersBE.Address1 = txtAddress1.Text;
                    objMastersBE.Address2 = txtAddress2.Text;
                    objMastersBE.City = txtCity.Text;
                    objMastersBE.ZIP = txtZipCode.Text;
                    xml = objMastersBAL.ServiceCenter(objMastersBE, Statement.Delete);
                    objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                    if (Convert.ToBoolean(objMastersBE.IsSuccess))
                    {
                        BindGrid();
                        UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        Message(UMS_Resource.SERVICE_CENTER_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                    }
                    else
                        Message(UMS_Resource.SERVICE_CENTER_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.DeActive;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.ServiceCenterId = hfServiceCenterId.Value;
                xml = objMastersBAL.ServiceCenter(objMastersBE, Statement.Delete);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (objMastersBE.IsSuccess)
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.SERVICE_CENTER_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else if (!objMastersBE.IsSuccess && objMastersBE.Count > 0)
                    Message(string.Format(UMS_Resource.SERVICE_CENTER_HAVE_CUSTOMERS, objMastersBE.Count), UMS_Resource.MESSAGETYPE_ERROR);
                else
                    Message(UMS_Resource.SERVICE_CENTER_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvServiceCentersList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    TextBox txtGridServiceCenterName = (TextBox)e.Row.FindControl("txtGridServiceCenterName");
                    DropDownList ddlGridServiceUnitName = (DropDownList)e.Row.FindControl("ddlGridServiceUnitName");

                    TextBox txtGridAddress1 = (TextBox)e.Row.FindControl("txtGridAddress1");
                    TextBox txtGridAddress2 = (TextBox)e.Row.FindControl("txtGridAddress2");
                    TextBox txtGridCity = (TextBox)e.Row.FindControl("txtGridCity");
                    TextBox txtGridZip = (TextBox)e.Row.FindControl("txtGridZip");


                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridServiceCenterName.ClientID + "','" +
                                                                                        ddlGridServiceUnitName.ClientID + "','" +
                                                                                        txtGridAddress1.ClientID + "','" +
                                                                                        txtGridAddress2.ClientID + "','" +
                                                                                        txtGridCity.ClientID + "','" +
                                                                                        txtGridZip.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");

                    //htableMaxLength = objCommonMethods.getApplicationSettings(UMS_Resource.SETTING_SC_LENGTH);
                    //txtGridSCCode.Attributes.Add("MaxLength", htableMaxLength[Constants.ServiceCenter].ToString());

                    if (Convert.ToInt32(lblActiveStatusId.Text) == Constants.DeActive)
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void BindGrid()
        {
            try
            {
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();
                XmlDocument resultedXml = new XmlDocument();
                objMastersBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                objMastersBE.PageSize = PageSize;
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    objMastersBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    objMastersBE.BU_ID = string.Empty;

                resultedXml = objMastersBAL.ServiceCenter(objMastersBE, ReturnType.Get);
                objMastersListBE = objiDsignBAL.DeserializeFromXml<MastersListBE>(resultedXml);
                if (objMastersListBE.Items.Count > 0)
                {
                    divdownpaging.Visible = divpaging.Visible = true;
                    hfTotalRecords.Value = objMastersListBE.Items[0].TotalRecords.ToString();
                    gvServiceCentersList.DataSource = objMastersListBE.Items;
                    gvServiceCentersList.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = objMastersListBE.Items.Count.ToString();
                    //hfTotalRecords.Value = Constants.Zero.ToString();
                    divdownpaging.Visible = divpaging.Visible = false;
                    gvServiceCentersList.DataSource = new DataTable(); ;
                    gvServiceCentersList.DataBind();
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //protected void InsertServiceCenterDetails()
        //{
        //    try
        //    {
        //        MastersBE objMastersBE = new MastersBE();
        //        objMastersBE.ServiceCenterName = txtServiceCenterName.Text;
        //        objMastersBE.SU_ID = ddlServiceUnitName.SelectedValue;
        //        objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
        //        objMastersBE.Notes = objiDsignBAL.ReplaceNewLines(txtNotes.Text, true);
        //        xml = objMastersBAL.ServiceCenter(objMastersBE, Statement.Insert);
        //        objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);
        //        hfServiceCenterId.Value = objMastersBE.ServiceCenterId;
        //        if (Convert.ToBoolean(objMastersBE.IsSuccess))
        //        {
        //            BindGridServiceCentersList();
        //            BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
        //            CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //            lblMsgPopup.Text = UMS_Resource.SERVICE_CENTER_INSERT_SUCCESS;
        //            mpeCountrypopup.Show();
        //            btnOk.Focus();
        //        }
        //        else
        //            Message(UMS_Resource.SERVICE_CENTER_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        public bool CustomerCount(string SC_ID)
        {
            XmlDocument _xml = new XmlDocument();
            _ObjCustomerDetailsBe.ServiceCenterName = SC_ID;
            _xml = _ObjCustomerDetailsBAL.CountCustomersBAL(_ObjCustomerDetailsBe, Statement.Check);
            _ObjCustomerDetailsBe = objiDsignBAL.DeserializeFromXml<CustomerDetailsBe>(_xml);
            customersCount = _ObjCustomerDetailsBe.Count;
            if (_ObjCustomerDetailsBe.Count > 0)
                return true;
            else
                return false;
        }

        private void AssignMaxLength()
        {
            try
            {
                htableMaxLength = objCommonMethods.getApplicationSettings(UMS_Resource.SETTING_SC_LENGTH);
                //txtSCCode.Attributes.Add("MaxLength", htableMaxLength[Constants.ServiceCenter].ToString());
                hfSCCodeLength.Value = htableMaxLength[Constants.ServiceCenter].ToString();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion        

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}