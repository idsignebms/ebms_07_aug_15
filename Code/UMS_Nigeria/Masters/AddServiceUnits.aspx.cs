﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddServiceUnits
                     
 Developer        : id065-Bhimaraju V
 Creation Date    : 20-Feb-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using Resources;
using iDSignHelper;
using System.Xml;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Globalization;
using System.Collections;

namespace UMS_Nigeria.Masters
{
    public partial class AddServiceUnits : System.Web.UI.Page
    {
        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        string Key = "AddServiceUnits";
        DataTable dtData = new DataTable();

        CustomerDetailsBe _ObjCustomerDetailsBe = new CustomerDetailsBe();
        CustomerDetailsBAL _ObjCustomerDetailsBAL = new CustomerDetailsBAL();
        static int customersCount = 0;
        Hashtable htableMaxLength = new Hashtable();

        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = objCommonMethods.GetPagePath(this.Request.Url);
                //if (objCommonMethods.IsAccess(path))
                //{
                //PageAccessPermissions();
                //AssignMaxLength();
                objCommonMethods.BindBusinessUnits(ddlBusinessUnitName, string.Empty, true);
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                {
                    ddlBusinessUnitName.SelectedIndex = ddlBusinessUnitName.Items.IndexOf(ddlBusinessUnitName.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                    ddlBusinessUnitName.Enabled = false;
                }
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                if (!string.IsNullOrEmpty(Request.QueryString[UMS_Resource.QSTR_BUSINESS_UNIT_CODE]))
                {
                    ddlBusinessUnitName.SelectedValue = objiDsignBAL.Decrypt(Request.QueryString[UMS_Resource.QSTR_BUSINESS_UNIT_CODE]).ToString();
                }
                //}
                //else { Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }
            }
        }

        //private void PageAccessPermissions()
        //{
        //    try
        //    {
        //        if (objCommonMethods.IsAccess(UMS_Resource.ADD_BUSINESS_UNITS_PAGE)) { lkbtnAddNewBusinessUnit.Visible = true; } else { lkbtnAddNewBusinessUnit.Visible = false; }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                hfRecognize.Value = UMS_Resource.SAVE;
                lblActiveMsg.Text = Resource.CHK_BEFORE_U_SAVE;
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSaveNext_Click(object sender, EventArgs e)
        {
            try
            {
                hfRecognize.Value = UMS_Resource.SAVENEXT;
                lblActiveMsg.Text = Resource.CHK_BEFORE_U_SAVE;
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnAddNewBusinessUnit_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_BUSINESS_UNITS_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_SERVICE_CENTER_PAGE + "?" + UMS_Resource.QSTR_SERVICE_UNIT_CODE + "=" + objiDsignBAL.Encrypt(hfSU_ID.Value), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvServiceUnitsList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                hfRecognize.Value = string.Empty;
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridServiceUnitName = (TextBox)row.FindControl("txtGridServiceUnitName");
                TextBox txtGridNotes = (TextBox)row.FindControl("txtGridNotes");
                DropDownList ddlGridBusinessUnitName = (DropDownList)row.FindControl("ddlGridBusinessUnitName");

                Label lblBusinessUnitId = (Label)row.FindControl("lblBusinessUnitId");
                Label lblBusinessUnitName = (Label)row.FindControl("lblBusinessUnitName");
                Label lblServiceUnitName = (Label)row.FindControl("lblServiceUnitName");
                Label lblServiceUnitId = (Label)row.FindControl("lblServiceUnitId");
                Label lblNotes = (Label)row.FindControl("lblNotes");

                Label lblAddress1 = (Label)row.FindControl("lblAddress1");
                Label lblAddress2 = (Label)row.FindControl("lblAddress2");
                Label lblCity = (Label)row.FindControl("lblCity");
                Label lblZip = (Label)row.FindControl("lblZip");
                TextBox txtGridAddress1 = (TextBox)row.FindControl("txtGridAddress1");
                TextBox txtGridAddress2 = (TextBox)row.FindControl("txtGridAddress2");
                TextBox txtGridCity = (TextBox)row.FindControl("txtGridCity");
                TextBox txtGridZip = (TextBox)row.FindControl("txtGridZip");

                switch (e.CommandName.ToUpper())
                {
                    case "EDITSERVICEUNIT":
                        lblBusinessUnitName.Visible = lblNotes.Visible = lblServiceUnitName.Visible = lblBusinessUnitId.Visible =
                            lblAddress1.Visible = lblAddress2.Visible = lblCity.Visible = lblZip.Visible = false;
                        txtGridServiceUnitName.Visible = ddlGridBusinessUnitName.Visible = txtGridNotes.Visible =
                            txtGridAddress1.Visible = txtGridAddress2.Visible = txtGridCity.Visible = txtGridZip.Visible = true;
                        txtGridServiceUnitName.Text = lblServiceUnitName.Text;

                        txtGridAddress1.Text = lblAddress1.Text;
                        txtGridAddress2.Text = lblAddress2.Text;
                        txtGridCity.Text = lblCity.Text;
                        txtGridZip.Text = lblZip.Text;

                        objCommonMethods.BindBusinessUnits(ddlGridBusinessUnitName, string.Empty, true);
                        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        {
                            //ListItem liSelect = new ListItem(UMS_Resource.SELECT, string.Empty);
                            //ListItem liBU = new ListItem(Session[UMS_Resource.SESSION_USER_BUNAME].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());
                            //liBU.Selected = true;
                            //ddlGridBusinessUnitName.Items.Add(liSelect);
                            //ddlGridBusinessUnitName.Items.Add(liBU);
                            ddlGridBusinessUnitName.SelectedIndex = ddlGridBusinessUnitName.Items.IndexOf(ddlGridBusinessUnitName.Items.FindByValue(lblBusinessUnitId.Text));
                            ddlGridBusinessUnitName.Enabled = false;
                        }
                        //else
                        //    objCommonMethods.BindBusinessUnits(ddlGridBusinessUnitName, string.Empty, true);
                        ddlGridBusinessUnitName.SelectedIndex = ddlGridBusinessUnitName.Items.IndexOf(ddlGridBusinessUnitName.Items.FindByValue(lblBusinessUnitId.Text));
                        txtGridNotes.Text = objiDsignBAL.ReplaceNewLines(lblNotes.Text, false);
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;
                        break;
                    case "CANCELSERVICEUNIT":
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                        txtGridServiceUnitName.Visible = ddlGridBusinessUnitName.Visible = txtGridNotes.Visible = txtGridAddress1.Visible = txtGridAddress2.Visible = txtGridCity.Visible = txtGridZip.Visible = false;
                        lblBusinessUnitName.Visible = lblNotes.Visible = lblServiceUnitName.Visible = lblAddress1.Visible = lblAddress2.Visible = lblCity.Visible = lblZip.Visible = true;
                        break;
                    case "UPDATESERVICEUNIT":
                        objMastersBE.SU_ID = lblServiceUnitId.Text;
                        objMastersBE.BU_ID = ddlGridBusinessUnitName.SelectedValue;
                        //objMastersBE.SUCode = txtGridSUCode.Text;
                        objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objMastersBE.ServiceUnitName = txtGridServiceUnitName.Text;
                        objMastersBE.Notes = objiDsignBAL.ReplaceNewLines(txtGridNotes.Text, true);

                        objMastersBE.Address1 = txtGridAddress1.Text.Trim();
                        objMastersBE.Address2 = txtGridAddress2.Text.Trim();
                        objMastersBE.City = txtGridCity.Text.Trim();
                        objMastersBE.ZIP = txtGridZip.Text.Trim();

                        xml = objMastersBAL.ServiceUnit(objMastersBE, Statement.Update);
                        objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                        if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        {
                            BindGrid();
                            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            Message(UMS_Resource.SERVICE_UNIT_UPDATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                        }
                        else if (objMastersBE.IsSUCodeExists)
                        {
                            Message(Resource.SERVICE_UNIT_CODE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else
                            Message(UMS_Resource.SERVICE_UNIT_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "ACTIVESERVICEUNIT":
                        mpeActivate.Show();
                        hfSU_ID.Value = lblServiceUnitId.Text;
                        lblActiveMsg.Text = UMS_Resource.ACTIVE_SU_POPUP_TEXT;
                        btnActiveOk.Focus();
                        break;
                    case "DEACTIVESERVICEUNIT":
                        if (CustomerCount(e.CommandArgument.ToString()))
                        {
                            mpeShiftCustomer.Show();
                            //hfSU_ID.Value = lblServiceUnitId.Text;
                            //lblShiftCustomer.Text = UMS_Resource.SHIFT_CUSTOMERS_SU.Insert(22, customersCount.ToString() + " ");
                            lblShiftCustomer.Text = UMS_Resource.SHIFT_CUSTOMERS_SU;
                            btnShipCustomer.Focus();
                        }
                        else
                        {
                            mpeDeActive.Show();
                            hfSU_ID.Value = lblServiceUnitId.Text;
                            lblDeActiveMsg.Text = UMS_Resource.DEACTIVE_SU_POPUP_TEXT;
                            btnDeActiveOk.Focus();
                        }
                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (hfRecognize.Value == Constants.SAVE)
                {
                    hfRecognize.Value = string.Empty;
                    MastersBE objMastersBE = new MastersBE();
                    objMastersBE.ServiceUnitName = txtServiceUnitName.Text;
                    objMastersBE.Address1 = txtAddress1.Text;
                    objMastersBE.Address2 = txtAddress2.Text;
                    objMastersBE.City = txtCity.Text;
                    objMastersBE.ZIP = txtZipCode.Text;
                    //objMastersBE.StateCode = ddlStateName.SelectedValue;
                    objMastersBE.BU_ID = ddlBusinessUnitName.SelectedValue;
                    //objMastersBE.SUCode = txtSUCode.Text;
                    objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objMastersBE.Notes = objiDsignBAL.ReplaceNewLines(txtNotes.Text, true);
                    xml = objMastersBAL.ServiceUnit(objMastersBE, Statement.Insert);
                    objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);
                    if (Convert.ToBoolean(objMastersBE.IsSuccess))
                    {
                        BindGrid();
                        UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        Message(UMS_Resource.SERVICE_UNIT_INSERT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        txtNotes.Text =
                            txtServiceUnitName.Text =
                                txtAddress1.Text = txtAddress2.Text = txtCity.Text = txtZipCode.Text = string.Empty;
                    }
                    else if (objMastersBE.IsSUCodeExists)
                    {
                        Message(Resource.SERVICE_UNIT_CODE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else if (string.IsNullOrEmpty(objMastersBE.SUCode))
                    {
                        Message("Can not add the Service Unit. Problem in SU Code Generation.", UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else
                        Message(UMS_Resource.SERVICE_UNIT_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (hfRecognize.Value == Constants.SAVENEXT)
                {
                    hfRecognize.Value = string.Empty;
                    InsertServiceUnitsDetails();
                }
                else
                {
                    MastersBE objMastersBE = new MastersBE();
                    objMastersBE.ActiveStatusId = Constants.Active;
                    objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objMastersBE.SU_ID = hfSU_ID.Value;
                    xml = objMastersBAL.ServiceUnit(objMastersBE, Statement.Delete);
                    objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                    if (Convert.ToBoolean(objMastersBE.IsSuccess))
                    {
                        BindGrid();
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        Message(UMS_Resource.SERVICE_UNIT_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                    }
                    else
                        Message(UMS_Resource.SERVICE_UNIT_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.DeActive;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.SU_ID = hfSU_ID.Value;
                xml = objMastersBAL.ServiceUnit(objMastersBE, Statement.Delete);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (objMastersBE.IsSuccess)
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.SERVICE_UNITS_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else if (!objMastersBE.IsSuccess && objMastersBE.Count > 0)
                    Message(string.Format(UMS_Resource.SERVICE_UNITS_HAVE_CUSTOMERS, objMastersBE.Count), UMS_Resource.MESSAGETYPE_ERROR);
                else
                    Message(UMS_Resource.SERVICE_UNITS_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvServiceUnitsList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    TextBox txtGridServiceUnitName = (TextBox)e.Row.FindControl("txtGridServiceUnitName");
                    DropDownList ddlGridBusinessUnitName = (DropDownList)e.Row.FindControl("ddlGridBusinessUnitName");

                    TextBox txtGridAddress1 = (TextBox)e.Row.FindControl("txtGridAddress1");
                    TextBox txtGridAddress2 = (TextBox)e.Row.FindControl("txtGridAddress2");
                    TextBox txtGridCity = (TextBox)e.Row.FindControl("txtGridCity");
                    TextBox txtGridZip = (TextBox)e.Row.FindControl("txtGridZip");

                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridServiceUnitName.ClientID + "','" +
                                                                                        ddlGridBusinessUnitName.ClientID + "','" +
                                                                                        txtGridAddress1.ClientID + "','" +
                                                                                        txtGridAddress2.ClientID + "','" +
                                                                                        txtGridCity.ClientID + "','" +
                                                                                        txtGridZip.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");

                    //htableMaxLength = objCommonMethods.getApplicationSettings(UMS_Resource.SETTING_SU_LENGTH);
                    //txtGridSUCode.Attributes.Add("MaxLength", htableMaxLength[Constants.ServiceUnit].ToString());

                    if (Convert.ToInt32(lblActiveStatusId.Text) == Constants.DeActive)
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void BindGrid()
        {
            try
            {
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();
                XmlDocument resultedXml = new XmlDocument();

                objMastersBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                objMastersBE.PageSize = PageSize;
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    objMastersBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    objMastersBE.BU_ID = string.Empty;
                resultedXml = objMastersBAL.ServiceUnit(objMastersBE, ReturnType.Get);

                objMastersListBE = objiDsignBAL.DeserializeFromXml<MastersListBE>(resultedXml);
                if (objMastersListBE.Items.Count > 0)
                {
                    divdownpaging.Visible = divpaging.Visible = true;
                    hfTotalRecords.Value = objMastersListBE.Items[0].TotalRecords.ToString();
                    gvServiceUnitsList.DataSource = objMastersListBE.Items;
                    gvServiceUnitsList.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = objMastersListBE.Items.Count.ToString();
                    divdownpaging.Visible = divpaging.Visible = false;
                    gvServiceUnitsList.DataSource = new DataTable(); ;
                    gvServiceUnitsList.DataBind();
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void InsertServiceUnitsDetails()
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();

                objMastersBE.ServiceUnitName = txtServiceUnitName.Text;
                //objMastersBE.SUCode = txtSUCode.Text;
                objMastersBE.BU_ID = ddlBusinessUnitName.SelectedValue;
                objMastersBE.Address1 = txtAddress1.Text;
                objMastersBE.Address2 = txtAddress2.Text;
                objMastersBE.City = txtCity.Text;
                objMastersBE.ZIP = txtZipCode.Text;
                objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.Notes = objiDsignBAL.ReplaceNewLines(txtNotes.Text, true);
                xml = objMastersBAL.ServiceUnit(objMastersBE, Statement.Insert);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);
                hfSU_ID.Value = objMastersBE.SU_ID;
                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    lblMsgPopup.Text = UMS_Resource.SERVICE_UNIT_INSERT_SUCCESS;
                    pop.Attributes.Add("class", "popheader popheaderlblgreen");
                    mpeCountrypopup.Show();
                    btnOk.Focus();
                }
                else if (objMastersBE.IsSUCodeExists)
                {
                    Message(Resource.SERVICE_UNIT_CODE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (string.IsNullOrEmpty(objMastersBE.SUCode))
                {
                    Message("Can not add the Service Unit. Problem in SU Code Generation.", UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                    Message(UMS_Resource.SERVICE_UNIT_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        public bool CustomerCount(string SU_ID)
        {
            XmlDocument _xml = new XmlDocument();
            _ObjCustomerDetailsBe.ServiceUnitName = SU_ID;
            _xml = _ObjCustomerDetailsBAL.CountCustomersBAL(_ObjCustomerDetailsBe, Statement.Check);
            _ObjCustomerDetailsBe = objiDsignBAL.DeserializeFromXml<CustomerDetailsBe>(_xml);
            customersCount = _ObjCustomerDetailsBe.Count;
            if (_ObjCustomerDetailsBe.Count > 0)
                return true;
            else
                return false;
        }

        private void AssignMaxLength()
        {
            try
            {
                htableMaxLength = objCommonMethods.getApplicationSettings(UMS_Resource.SETTING_SU_LENGTH);
                //txtSUCode.Attributes.Add("MaxLength", htableMaxLength[Constants.ServiceUnit].ToString());
                hfSUCodeLength.Value = htableMaxLength[Constants.ServiceUnit].ToString();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion        

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}