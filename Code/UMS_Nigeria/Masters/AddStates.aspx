﻿<%@ Page Title="::Add States::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="AddStates.aspx.cs" Inherits="UMS_Nigeria.Masters.AddStates" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upStates" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddState" runat="server" Text="<%$ Resources:Resource, ADD_STATE%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litCountryCode" runat="server" Text="<%$ Resources:Resource, COUNTRY_NAME%>"></asp:Literal>
                                    <span class="span_star">*</span></label><br />
                                <asp:DropDownList ID="ddlCountryName"  AutoPostBack="true" TabIndex="1" OnSelectedIndexChanged="ddlCountryName_SelectedIndexChanged"
                                onchange="DropDownlistOnChangelbl(this,'spanCountryCode','Country')"
                                    runat="server" CssClass="text-box text-select">
                                    <asp:ListItem Text="--Select--"></asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                <span id="spanCountryCode" class="span_color"></span>
                            </div>
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litDisplayCode" runat="server" Text="<%$ Resources:Resource, DISPLAY_CODE%>"></asp:Literal>
                                    <span class="span_star">*</span></label><br />
                                <asp:TextBox CssClass="text-box" ID="txtDisplayCode" onblur="return TextBoxBlurValidationlbl(this,'spanDisplayCode','Display Code')"
                                    runat="server" MaxLength="20" TabIndex="4" placeholder="Enter Display Code"></asp:TextBox>
                                <span id="spanDisplayCode" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litLGA" runat="server" Text="Region"></asp:Literal>
                                    <span class="span_star">*</span></label><br />
                                <asp:DropDownList ID="ddlRegion" Enabled="false" TabIndex="2" onchange="DropDownlistOnChangelbl(this,'spanRegion','Region')"
                                    runat="server" CssClass="text-box text-select">
                                    <asp:ListItem Text="--Select--"></asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                <span id="spanRegion" class="span_color"></span>
                            </div>
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litNotes" runat="server" Text="<%$ Resources:Resource, NOTES%>"></asp:Literal>
                                </label>
                                <div class="space_2">
                                </div>
                                <asp:TextBox ID="txtNotes" runat="server" TabIndex="5" CssClass="text-box" TextMode="MultiLine"
                                    placeholder="Enter Notes Here"></asp:TextBox>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litStateName" runat="server" Text="<%$ Resources:Resource, STATE_NAME%>"></asp:Literal>
                                    <span class="span_star">*</span></label><br />
                                <asp:TextBox CssClass="text-box" ID="txtStateName" runat="server" TabIndex="3" onblur="return TextBoxBlurValidationlbl(this,'spanStateName','State Name')"
                                    MaxLength="50" placeholder="Enter State Name"></asp:TextBox>
                                <span id="spanStateName" class="span_color"></span>
                            </div>
                        </div>
                    </div>
                    <div class="space">
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <asp:Button ID="btnSave" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                CssClass="box_s" runat="server" OnClick="btnSave_Click" TabIndex="7" />
                        </div>
                    </div>
                </div>
                <div style="clear: both;">
                </div>
                <div class="grid_boxes">
                    <div class="grid_pagingTwo_top">
                        <div class="paging_top_title">
                            <asp:Literal ID="litGridStatesList" runat="server" Text="<%$ Resources:Resource, GRID_STATES_LIST%>"></asp:Literal>
                        </div>
                        <div class="paging_top_rightTwo_content" id="divpaging" runat="server">
                            <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="grid_tb" id="divgrid" runat="server">
                    <asp:GridView ID="gvStatesList" runat="server" AutoGenerateColumns="False" OnRowCommand="gvStatesList_RowCommand"
                        HeaderStyle-CssClass="grid_tb_hd" OnRowDataBound="gvStatesList_RowDataBound">
                        <EmptyDataRowStyle HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            There is no data.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <%--<%#Container.DataItemIndex+1%>--%>
                                    <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, STATE_NAME%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblStateName" runat="server" Text='<%#Eval("StateName") %>'></asp:Label>
                                    <asp:TextBox CssClass="text-box" ID="txtGridStateName" MaxLength="50" placeholder="Enter State Name"
                                        runat="server" Visible="false"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, STATE_CODE%>" ItemStyle-Width="8%"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblStateCode" runat="server" Text='<%#Eval("StateCode") %>'></asp:Label>
                                    <asp:Label ID="lblIsActive" Visible="false" runat="server" Text='<%#Eval("IsActive")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, DISPLAY_CODE%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblDisplayCode" runat="server" Text='<%#Eval("DisplayCode") %>'></asp:Label>
                                    <asp:TextBox CssClass="text-box" ID="txtGridDisplayCode" MaxLength="20" placeholder="Enter Display Code"
                                        runat="server" Visible="false"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, COUNTRY_NAME%>" ItemStyle-Width="8%"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblCountryCode" Visible="false" runat="server" Text='<%#Eval("CountryCode") %>'></asp:Label>
                                    <asp:DropDownList ID="ddlGridCountryName" Visible="false" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlGridCountryName_SelectedIndexChanged" runat="server">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblCountryName" runat="server" Text='<%#Eval("CountryName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Region" ItemStyle-Width="8%"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblRegionId" Visible="false" runat="server" Text='<%#Eval("RegionId") %>'></asp:Label>
                                    <asp:DropDownList ID="ddlGridRegion" Visible="false" runat="server">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblRegion" runat="server" Text='<%#Eval("RegionName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, NOTES%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblNotes" runat="server" Text='<%#Eval("Notes") %>'></asp:Label>
                                    <asp:TextBox CssClass="text-box" ID="txtGridNotes" placeholder="Enter Notes" runat="server"
                                        TextMode="MultiLine" Visible="false"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lkbtnStateEdit" runat="server" ToolTip="Edit" Text="Edit" CommandName="EditState">
                                        <img src="../images/Edit.png"  style="padding:3px; width:20px; height:20px" alt="Edit"/></asp:LinkButton>
                                    <asp:LinkButton ID="lkbtnStateUpdate" Visible="false" runat="server" ToolTip="Update"
                                        CommandName="UpdateState"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                    <asp:LinkButton ID="lkbtnStateCancel" Visible="false" runat="server" ToolTip="Cancel"
                                        CommandName="CancelState"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                    <asp:LinkButton ID="lkbtnStateActive" Visible="false" runat="server" ToolTip="Activate State"
                                        CommandName="ActiveState"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                    <asp:LinkButton ID="lkbtnStateDelete" runat="server" ToolTip="DeActivate State" CommandName="DeActiveState"><img style="position: relative;top: -3px;" src="../images/Deactivate.gif" alt="DeActive" /></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:HiddenField ID="hfCountryCode" runat="server" />
                    <asp:HiddenField ID="hfStatename" runat="server" />
                    <asp:HiddenField ID="hfStateCode" runat="server" />
                    <asp:HiddenField ID="hfPageNo" runat="server" />
                    <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                    <asp:HiddenField ID="hfRecognize" runat="server" />
                    <asp:HiddenField ID="hfLastPage" runat="server" />
                    <asp:HiddenField ID="hfPageSize" runat="server" />
                </div>
                <div class="grid_boxes">
                    <div id="divdownpaging" runat="server">
                        <div class="grid_paging_bottom">
                            <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <div id="hidepopup">
                <%-- Save Btn Confirm popup Start--%>
                <%--<asp:ModalPopupExtender ID="mpeSaveConfirm" runat="server" PopupControlID="PanelSaveConfirm"
                    TargetControlID="btnSaveConfirm" BackgroundCssClass="modalBackground" CancelControlID="btnSaveConfirmCancel">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnSaveConfirm" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="PanelSaveConfirm" runat="server" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: #639B00;height:88px;">
                        <asp:Label ID="lblSaveConfirm" runat="server" style="line-height: 28px;"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr" style="margin-top: 22px; padding-right:10px;">
                            <asp:Button ID="btnSaveConfirmOk" runat="server" OnClick="btnSave_Click"  Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                            <asp:Button ID="btnSaveConfirmCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>--%>
                <%-- Save Btn Confirm popup Closed--%>
                <%-- Alert popup Start--%>
                <%-- <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="PanelAlert"
                    TargetControlID="btnAlert" BackgroundCssClass="modalPopup" CancelControlID="btnAlertOk">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnAlert" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="PanelAlert" runat="server" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: #639B00;">
                        <asp:Label ID="lblAlertMsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltl" style="margin-top: 22px; padding-left: 89px;">
                            <asp:Button ID="btnAlertOk" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="continue" />                            
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>--%>
                <%-- Alert popup Closed--%>
                <%-- Active Btn popup Start--%>
                <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                    TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="PanelActivate" runat="server" CssClass="modalPopup"
                    Style="display: none; border-color: #639B00;">
                    <div class="popheader" style="background-color: #639B00;">
                        <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                    </div>
                    <br />
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btnActiveOk" runat="server" OnClick="btnActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                            <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- Active  Btn popup Closed--%>
                <%-- DeActive  Btn popup Start--%>
                <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                    TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnDeActivate" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="PanelDeActivate" runat="server" CssClass="modalPopup"
                    Style="display: none; border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnDeActiveOk" runat="server" OnClick="btnDeActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                            <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- DeActive  popup Closed--%>
                <%-- Grid Alert popup Start--%>
                <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                    TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                </asp:ModalPopupExtender>
                <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                    <div class="popheader popheaderlblred">
                        <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- Grid Alert popup Closed--%>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/PageValidations/AddStates.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
