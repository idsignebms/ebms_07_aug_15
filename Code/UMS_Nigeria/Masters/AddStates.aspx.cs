﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddBusinessUnits
                     
 Developer        : id065-Bhimaraju V
 Creation Date    : 15-Feb-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Drawing;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBE;
using System.Xml;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using Resources;
using System.Data;
using System.Threading;
using System.Globalization;

namespace UMS_Nigeria.Masters
{
    public partial class AddStates : System.Web.UI.Page
    {

        #region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        MastersBAL _ObjMastersBAL = new MastersBAL();
        UMS_Service _ObjUMS_Service = new UMS_Service();
        public int PageNum;
        XmlDocument xml = null;
        string Key = "AddStates";
        #endregion

        #region Properties

        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }

        private string StateName
        {
            get { return txtStateName.Text.Trim(); }
            set { txtStateName.Text = value; }
        }

        //private string StateCode
        //{
        //    get { return txtStateCode.Text.Trim(); }
        //    set { txtStateCode.Text = value; }
        //}

        private string DisplayCode
        {
            get { return txtDisplayCode.Text.Trim(); }
            set { txtDisplayCode.Text = value; }
        }

        private string Notes
        {
            get { return txtNotes.Text.Trim(); }
            set { txtNotes.Text = value; }
        }

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = _ObjCommonMethods.GetPagePath(this.Request.Url);
                //if (_ObjCommonMethods.IsAccess(path))
                //{
                //Session[SeaWaysMIS_Resources.SESSION_PAGENAME] = ConstantsBE.NewCountryPageName;
                //PageAccessPermissions();
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
                //BindddlCountriesList();
                _ObjCommonMethods.BindCountries(ddlCountryName, true, false);
                //_ObjCommonMethods.BindLGA(ddlLGA, true, true);
                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                if (!string.IsNullOrEmpty(Request.QueryString[UMS_Resource.QSTR_COUNTRY_CODE]))
                {
                    ddlCountryName.SelectedValue = _ObjiDsignBAL.Decrypt(Request.QueryString[UMS_Resource.QSTR_COUNTRY_CODE].ToString());
                }
                //}
                //else
                //{
                //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                //}
            }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void lkbtnAddNewCountry_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_COUNTRIES_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                hfRecognize.Value = UMS_Resource.SAVE;
                lblActiveMsg.Text = Resource.CHK_BEFORE_U_SAVE;
                mpeActivate.Show();
                btnActiveOk.Focus();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlCountryName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlCountryName.SelectedIndex > 0)
                {
                    ddlRegion.Enabled = true;
                    _ObjCommonMethods.BindRegions(ddlRegion, ddlCountryName.SelectedValue, true, UMS_Resource.DROPDOWN_SELECT);
                }
                else
                {
                    ddlRegion.SelectedIndex = 0;
                    ddlRegion.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlGridCountryName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlGridCountryName = (DropDownList)sender;
                DropDownList ddlGridRegion = (DropDownList)ddlGridCountryName.NamingContainer.FindControl("ddlGridRegion");
                if (ddlGridCountryName.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindRegions(ddlGridRegion, ddlGridCountryName.SelectedValue, false,UMS_Resource.DROPDOWN_SELECT);                    
                    ddlGridRegion.Enabled = true;
                }
                else
                {
                    ddlGridRegion.SelectedIndex = 0;
                    ddlGridRegion.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_DISTRICTS_PAGE + "?" + UMS_Resource.QSTR_COUNTRY_CODE + "=" + _ObjiDsignBAL.Encrypt(hfCountryCode.Value) + "&" + UMS_Resource.QSTR_STATE_CODE + "=" + _ObjiDsignBAL.Encrypt(hfStateCode.Value), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void BindGrid()
        {
            try
            {
                MastersListBE _objMastersListBE = new MastersListBE();
                MastersBE _ObjMastersBE = new MastersBE();
                XmlDocument resultedXml = new XmlDocument();
                //resultedXml = _ObjUMS_Service.GetActiveStatesList(Convert.ToInt32(hfPageNo.Value), PageSize);
                _ObjMastersBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                _ObjMastersBE.PageSize = PageSize;
                resultedXml = _ObjMastersBAL.Get(_ObjMastersBE, ReturnType.Multiple);
                _objMastersListBE = _ObjiDsignBAL.DeserializeFromXml<MastersListBE>(resultedXml);
                if (_objMastersListBE.Items.Count > 0)
                {
                    divdownpaging.Visible = divpaging.Visible = true;
                    hfTotalRecords.Value = _objMastersListBE.Items[0].TotalRecords.ToString();
                    gvStatesList.DataSource = _objMastersListBE.Items;
                    gvStatesList.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = _objMastersListBE.Items.Count.ToString();
                    divdownpaging.Visible = divpaging.Visible = false;
                    gvStatesList.DataSource = new DataTable(); ;
                    gvStatesList.DataBind();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvStatesList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnStateUpdate = (LinkButton)e.Row.FindControl("lkbtnStateUpdate");
                    TextBox txtGridStateName = (TextBox)e.Row.FindControl("txtGridStateName");
                    TextBox txtGridDisplayCode = (TextBox)e.Row.FindControl("txtGridDisplayCode");
                    DropDownList ddlGridCountryName = (DropDownList)e.Row.FindControl("ddlGridCountryName");
                    DropDownList ddlGridRegion = (DropDownList)e.Row.FindControl("ddlGridRegion");

                    lkbtnStateUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridStateName.ClientID + "','" + txtGridDisplayCode.ClientID + "','" + ddlGridCountryName.ClientID + "','" + ddlGridRegion.ClientID + "')");

                    Label lblIsActive = (Label)e.Row.FindControl("lblIsActive");


                    if ((!Convert.ToBoolean(lblIsActive.Text)))
                    {
                        e.Row.FindControl("lkbtnStateDelete").Visible = e.Row.FindControl("lkbtnStateEdit").Visible = false;
                        e.Row.FindControl("lkbtnStateActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnStateDelete").Visible = e.Row.FindControl("lkbtnStateEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvStatesList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                MastersListBE _ObjMastersListBE = new MastersListBE();
                MastersBE _ObjMastersBE = new MastersBE();
                DataSet ds = new DataSet();


                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridStateName = (TextBox)row.FindControl("txtGridStateName");
                TextBox txtGridNotes = (TextBox)row.FindControl("txtGridNotes");
                TextBox txtGridDisplayCode = (TextBox)row.FindControl("txtGridDisplayCode");
                DropDownList ddlGridCountryName = (DropDownList)row.FindControl("ddlGridCountryName");
                DropDownList ddlGridRegion = (DropDownList)row.FindControl("ddlGridRegion");
                Label lblStateCode = (Label)row.FindControl("lblStateCode");
                Label lblStateName = (Label)row.FindControl("lblStateName");
                Label lblCountryCode = (Label)row.FindControl("lblCountryCode");
                Label lblCountryName = (Label)row.FindControl("lblCountryName");
                Label lblRegionId = (Label)row.FindControl("lblRegionId");
                Label lblRegion = (Label)row.FindControl("lblRegion");
                Label lblDisplayCode = (Label)row.FindControl("lblDisplayCode");
                Label lblNotes = (Label)row.FindControl("lblNotes");
                hfRecognize.Value = string.Empty;
                switch (e.CommandName.ToUpper())
                {
                    case "EDITSTATE":
                        lblCountryName.Visible = lblStateName.Visible = lblNotes.Visible = lblDisplayCode.Visible = lblCountryCode.Visible = lblRegion.Visible = false;
                        txtGridStateName.Visible = txtGridDisplayCode.Visible = ddlGridCountryName.Visible = ddlGridRegion.Visible = txtGridNotes.Visible = true;
                        txtGridStateName.Text = lblStateName.Text;
                        txtGridDisplayCode.Text = lblDisplayCode.Text;
                        _ObjCommonMethods.BindCountries(ddlGridCountryName, true, true);
                        //_ObjCommonMethods.BindLGA(ddlGridLGAName, true, true);
                        ddlGridCountryName.SelectedIndex = ddlGridCountryName.Items.IndexOf(ddlGridCountryName.Items.FindByValue(lblCountryCode.Text));
                        _ObjCommonMethods.BindRegions(ddlGridRegion, ddlGridCountryName.SelectedValue, false, UMS_Resource.DROPDOWN_SELECT);
                        ddlGridRegion.SelectedIndex = ddlGridRegion.Items.IndexOf(ddlGridRegion.Items.FindByValue(lblRegionId.Text));
                        txtGridNotes.Text = _ObjiDsignBAL.ReplaceNewLines(lblNotes.Text, false);
                        row.FindControl("lkbtnStateCancel").Visible = row.FindControl("lkbtnStateUpdate").Visible = true;
                        row.FindControl("lkbtnStateDelete").Visible = row.FindControl("lkbtnStateEdit").Visible = false;
                        break;
                    case "CANCELSTATE":
                        row.FindControl("lkbtnStateDelete").Visible = row.FindControl("lkbtnStateEdit").Visible = true;
                        row.FindControl("lkbtnStateCancel").Visible = row.FindControl("lkbtnStateUpdate").Visible = false;
                        txtGridStateName.Visible = txtGridDisplayCode.Visible = ddlGridCountryName.Visible = ddlGridRegion.Visible = txtGridNotes.Visible = false;
                        lblCountryName.Visible = lblRegion.Visible = lblStateName.Visible = lblNotes.Visible = lblDisplayCode.Visible = true;
                        break;
                    case "UPDATESTATE":
                        //xml = _ObjUMS_Service.UpdateStates(txtGridStateName.Text, lblStateCode.Text, txtGridDisplayCode.Text, ddlGridCountryName.SelectedValue, _ObjiDsignBAL.ReplaceNewLines(txtGridNotes.Text, true));
                        _ObjMastersBE.StateName = txtGridStateName.Text;
                        _ObjMastersBE.StateCode = lblStateCode.Text;
                        _ObjMastersBE.DisplayCode = txtGridDisplayCode.Text;
                        _ObjMastersBE.CountryCode = ddlGridCountryName.SelectedValue;
                        _ObjMastersBE.RegionId = Convert.ToInt32(ddlGridRegion.SelectedValue);
                        _ObjMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        _ObjMastersBE.Notes = _ObjiDsignBAL.ReplaceNewLines(txtGridNotes.Text, true);
                        xml = _ObjMastersBAL.States(_ObjMastersBE, Statement.Update);
                        _ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                        if (Convert.ToBoolean(_ObjMastersBE.IsSuccess))
                        {
                            BindGrid();
                            Message(UMS_Resource.STATE_DETAILS_UPDATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        else if (_ObjMastersBE.IsDisplayCodeExists)
                        {
                            Message(UMS_Resource.DISPLAY_CODE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else
                            Message(UMS_Resource.STATE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "ACTIVESTATE":
                        //xml = _ObjUMS_Service.DeleteActiveState(1, lblStateCode.Text);
                        mpeActivate.Show();
                        hfStateCode.Value = lblStateCode.Text;
                        lblActiveMsg.Text = UMS_Resource.ACTIVE_STATE_POPUP_TEXT;
                        btnActiveOk.Focus();
                        break;
                    case "DEACTIVESTATE":
                        //xml = _ObjUMS_Service.DeleteActiveState(0, lblStateCode.Text);
                        mpeDeActive.Show();
                        hfStateCode.Value = lblStateCode.Text;
                        lblDeActiveMsg.Text = UMS_Resource.DEACTIVE_STATE_POPUP_TEXT;
                        btnDeActiveOk.Focus();
                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (hfRecognize.Value == Constants.SAVE)
                {
                    hfRecognize.Value = string.Empty;
                    MastersBE _ObjMastersBE = new MastersBE();
                    //xml = _ObjUMS_Service.States(this.StateName, this.StateCode, this.DisplayCode, ddlCountryName.SelectedValue, _ObjiDsignBAL.ReplaceNewLines(this.Notes, true));
                    _ObjMastersBE.StateName = StateName;
                    //_ObjMastersBE.StateCode = StateCode;
                    _ObjMastersBE.DisplayCode = DisplayCode;
                    _ObjMastersBE.CountryCode = ddlCountryName.SelectedValue;
                    //_ObjMastersBE.LGAId = Convert.ToInt32(ddlLGA.SelectedValue);
                    _ObjMastersBE.RegionId = Convert.ToInt32(ddlRegion.SelectedValue);
                    _ObjMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    _ObjMastersBE.Notes = _ObjiDsignBAL.ReplaceNewLines(Notes, true);
                    xml = _ObjMastersBAL.States(_ObjMastersBE, Statement.Insert);
                    _ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                    if (Convert.ToBoolean(_ObjMastersBE.IsSuccess))
                    {
                        BindGrid();
                        UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        UCPaging2.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        txtDisplayCode.Text = txtStateName.Text = txtNotes.Text = string.Empty;
                        ddlCountryName.SelectedIndex=ddlRegion.SelectedIndex = 0;
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        Message(UMS_Resource.STATE_DETAILS_INSERT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                    }
                    else if (_ObjMastersBE.IsStateNameExists)
                    {
                        Message(UMS_Resource.STATE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else if (_ObjMastersBE.IsStateCodeExists)
                    {
                        Message(UMS_Resource.STATE_CODE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else if (_ObjMastersBE.IsDisplayCodeExists)
                    {
                        Message(UMS_Resource.DISPLAY_CODE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
                else
                {
                    MastersBE _ObjMastersBE = new MastersBE();
                    _ObjMastersBE.StateCode = hfStateCode.Value;
                    _ObjMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    _ObjMastersBE.IsActive = Convert.ToBoolean(1);

                    xml = _ObjMastersBAL.States(_ObjMastersBE, Statement.Delete);
                    _ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                    if (Convert.ToBoolean(_ObjMastersBE.IsSuccess))
                    {
                        BindGrid();
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        Message(UMS_Resource.STATE_DETAILS_ACTIVATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    }
                    else
                        Message(UMS_Resource.STATE_DETAILS_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE _ObjMastersBE = new MastersBE();
                _ObjMastersBE.StateCode = hfStateCode.Value;
                _ObjMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                _ObjMastersBE.IsActive = Convert.ToBoolean(0);
                xml = _ObjMastersBAL.States(_ObjMastersBE, Statement.Delete);
                _ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(xml);
                if (_ObjMastersBE.IsSuccess)
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.STATE_DETAILS_DEACTIVATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else if (!_ObjMastersBE.IsSuccess && _ObjMastersBE.Count > 0)
                    Message(string.Format(UMS_Resource.STATE_DETAILS_HAVE_CUSOTMERS, _ObjMastersBE.Count), UMS_Resource.MESSAGETYPE_ERROR);
                else
                    Message(UMS_Resource.STATE_DETAILS_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}