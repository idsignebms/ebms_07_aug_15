﻿<%@ Page Title="::Add SubStation::" Language="C#" MasterPageFile="~/MasterPages/EDMUMS.Master" Theme="Green"
    AutoEventWireup="true" CodeBehind="AddSubStations.aspx.cs" Inherits="UMS_Nigeria.Masters.AddSubStations" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="edmcontainer">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upAddSubStation" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <h3>
                    <asp:Literal ID="litAddSubStation" runat="server" Text="<%$ Resources:Resource, ADD_SUB_STATION%>"></asp:Literal>
                </h3>
                <div class="clear">
                    &nbsp;</div>
                <%-- <div class="consumer_feild">
                <div class="consume_name">
                    <asp:Literal ID="litServiceCenterName" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER_NAME%>"></asp:Literal><span>*</span>
                </div>
                <div class="consume_input" style="width: 163px">
                    <asp:DropDownList ID="ddlServiceCenterName" onchange="DropDownlistOnChangelbl(this,'spanddlServiceCenterName','Service Center')"
                        runat="server">
                    </asp:DropDownList>
                    <span id="spanddlServiceCenterName" class="spancolor"></span>
                </div>
                <div class="consume_input" style="width: 163px">
                    <asp:LinkButton ID="lkbtnAddNewServiceCenter" OnClick="lkbtnAddNewServiceCenter_Click" runat="server"
                        Text="<%$ Resources:Resource, ADD%>"></asp:LinkButton>
                </div>
            </div>--%>
                <div class="consumer_feild">
                    <div class="consume_name" style="width: 139px;">
                        <asp:Literal ID="litSubStationName" runat="server" Text="<%$ Resources:Resource, SUB_STATION_NAME%>"></asp:Literal><span>*</span>
                    </div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtSubStationName" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanSubStationName','Injection SubStation Name')"
                            MaxLength="300" placeholder="Injection SubStation Name"></asp:TextBox>
                        <span id="spanSubStationName" class="spancolor"></span>
                    </div>
                    <div class="consume_name c_left">
                        <asp:Literal ID="litNotes" runat="server" Text="<%$ Resources:Resource, NOTES%>"></asp:Literal>
                    </div>
                    <div class="consume_input">
                        <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" placeholder="Enter Comments Here"></asp:TextBox>
                    </div>
                    <div class="clear pad_10">
                        <div style="margin-left: 476px;">
                            <%--<asp:UpdatePanel ID="upButtons" runat="server">
                        <ContentTemplate>--%>
                            <asp:Button ID="btnSave" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                CssClass="calculate_but" runat="server" OnClick="btnSave_Click" />
                            <asp:Button ID="btnSaveNext" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVENEXT%>"
                                CssClass="calculate_but" runat="server" OnClick="btnSaveNext_Click" />
                            <%--</ContentTemplate>
                    </asp:UpdatePanel>--%>
                        </div>
                    </div>
                </div>
                <div class="consumer_total">
                    <div class="pad_10">
                    </div>
                </div>
                <div class="consumer_total">
                    <div class="pad_10">
                        <br />
                    </div>
                </div>
                <div id="divgrid" class="grid" runat="server" style="width: 100% !important; float: left;">
                    <div id="divpaging" runat="server">
                        <div align="right" class="marg_20t" runat="server" id="divLinks">
                            <h3 class="gridhead" align="left">
                                <asp:Literal ID="litSTList" runat="server" Text="<%$ Resources:Resource, GRID_SUB_STATIONS_LIST%>"></asp:Literal>
                            </h3>
                            <asp:LinkButton ID="lkbtnFirst" runat="server" CssClass="pagingfirst" OnClick="lkbtnFirst_Click">
                                <asp:Literal ID="litFirst" runat="server" Text="<%$ Resources:Resource, FIRST%>"></asp:Literal></asp:LinkButton>
                            &nbsp;|
                            <asp:LinkButton ID="lkbtnPrevious" runat="server" CssClass="pagingfirst" OnClick="lkbtnPrevious_Click">
                                <asp:Literal ID="litPrevious" runat="server" Text="<%$ Resources:Resource, PREVIOUS%>"></asp:Literal></asp:LinkButton>
                            &nbsp;|
                            <asp:Label ID="lblCurrentPage" runat="server" Font-Bold="true"></asp:Label>
                            &nbsp;|
                            <asp:LinkButton ID="lkbtnNext" runat="server" CssClass="pagingfirst" OnClick="lkbtnNext_Click">
                                <asp:Literal ID="litNext" runat="server" Text="<%$ Resources:Resource, NEXT%>"></asp:Literal></asp:LinkButton>
                            &nbsp;|
                            <asp:LinkButton ID="lkbtnLast" runat="server" CssClass="pagingfirst" OnClick="lkbtnLast_Click">
                                <asp:Literal ID="litLast" runat="server" Text="<%$ Resources:Resource, LAST%>"></asp:Literal></asp:LinkButton>
                            &nbsp;|<asp:Literal ID="litPageSize" runat="server" Text="<%$ Resources:Resource, PAGE_SIZE%>"></asp:Literal>
                            <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" CssClass="paging-size"
                                OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="consumer_total">
                    <div class="clear">
                    </div>
                </div>
                <div class="grid" id="divPrint" runat="server" align="center" style="overflow-x: auto;">
                    <asp:GridView ID="gvSubStationList" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                        OnRowCommand="gvSubStationList_RowCommand" OnRowDataBound="gvSubStationList_RowDataBound">
                        <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                        <EmptyDataTemplate>
                            There is no data.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, SUB_STATION_ID%>" ItemStyle-Width="20%"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblSubStationId" runat="server" Text='<%#Eval("SubStationId") %>'></asp:Label>
                                    <asp:Label ID="lblActiveStatusId" Visible="false" runat="server" Text='<%#Eval("ActiveStatusId") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, SUB_STATION_NAME%>" ItemStyle-Width="20%"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblSubStationName" runat="server" Text='<%#Eval("SubStationName") %>'></asp:Label>
                                    <asp:TextBox ID="txtGridSubStationName" MaxLength="300" placeholder="Injection SubStation Name"
                                        runat="server" Visible="false"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%-- <asp:TemplateField HeaderText="<%$ Resources:Resource, SERVICE_CENTER_NAME%>" ItemStyle-Width="8%"
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblServiceCenterName" Visible="false" runat="server" Text='<%#Eval("ServiceCenterId") %>'></asp:Label>
                                <asp:DropDownList ID="ddlGridServiceCenterName" runat="server" Visible="false">
                                </asp:DropDownList>
                                <asp:Label ID="lblGridServiceCenterName" runat="server" Text='<%#Eval("ServiceCenterName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, NOTES%>" ItemStyle-Width="20%"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblNotes" runat="server" Text='<%#Eval("Notes") %>'></asp:Label>
                                    <asp:TextBox ID="txtGridNotes" placeholder="Enter Sub Notes" runat="server" Visible="false"
                                        TextMode="MultiLine"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lkbtnEdit" runat="server" ToolTip="Edit" Text="Edit" CommandName="EditSubStation">
                                        <img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                                    <asp:LinkButton ID="lkbtnUpdate" Visible="false" runat="server" ToolTip="Update"
                                        CommandName="UpdateSubStation"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                    <asp:LinkButton ID="lkbtnCancel" Visible="false" runat="server" ToolTip="Cancel"
                                        CommandName="CancelSubStation"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                    <asp:LinkButton ID="lkbtnActive" Visible="false" runat="server" ToolTip="Activate Injection SubStation"
                                        CommandName="ActiveSubStation"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                    <asp:LinkButton ID="lkbtnDeActive" CommandArgument='<%#Eval("SubStationId") %>' runat="server"
                                        ToolTip="DeActivate Injection SubStation" CommandName="DeActiveSubStation"><img src="../images/Deactivate.gif" alt="DeActive" /></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <asp:HiddenField ID="hfPageSize" runat="server" />
                <asp:HiddenField ID="hfPageNo" runat="server" />
                <asp:HiddenField ID="hfLastPage" runat="server" />
                <asp:HiddenField ID="hfTotalRecords" runat="server" />
                <asp:HiddenField ID="hfSubStationId" runat="server" />
                <div id="hidepopup">
                    <asp:ModalPopupExtender ID="mpeCountrypopup" runat="server" TargetControlID="btnpopup"
                        PopupControlID="pnlConfirmation" BackgroundCssClass="popbg">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnpopup" runat="server" Text="" Style="display: none;" />
                    <asp:Panel runat="server" ID="pnlConfirmation" DefaultButton="btnOk" CssClass="editpopup_two">
                        <div class="panelMessage">
                            <div>
                                <asp:Panel ID="pnlMsgPopup" runat="server" align="center">
                                    <asp:Label ID="lblMsgPopup" runat="server" Text=""></asp:Label>
                                </asp:Panel>
                            </div>
                            <div class="clear pad_10">
                            </div>
                            <div class="buttondiv">
                                <asp:Button ID="btnOk" runat="server" Text="OK" OnClick="btnOk_Click" CssClass="btn_ok"
                                    Style="margin-left: 108px;" />
                                <br />
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Active Btn popup Start--%>
                    <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                        TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelActivate" runat="server" DefaultButton="btnActiveOk" CssClass="modalPopup"
                        Style="display: none; border-color: #639B00;">
                        <div class="popheader" style="background-color: #639B00;">
                            <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                        </div>
                        <br />
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnActiveOk" runat="server" OnClick="btnActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Active  Btn popup Closed--%>
                    <%-- Shift customers  Btn popup Start--%>
                    <asp:ModalPopupExtender ID="mpeShiftCustomer" runat="server" PopupControlID="pnlShiftCustomer"
                        TargetControlID="btnShipCustomerDemo" BackgroundCssClass="modalBackground" CancelControlID="btnShipCustomerCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnShipCustomerDemo" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="pnlShiftCustomer" runat="server" DefaultButton="btnShipCustomer" CssClass="modalPopup"
                        Style="display: none; border-color: #639B00;">
                        <div class="popheader" style="background-color: red;">
                            <asp:Label ID="lblShiftCustomer" runat="server"></asp:Label>
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnShipCustomer" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnShipCustomerCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Shift customers  popup Closed--%>
                    <%-- DeActive  Btn popup Start--%>
                    <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                        TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnDeActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelDeActivate" runat="server" DefaultButton="btnDeActiveOk" CssClass="modalPopup"
                        Style="display: none; border-color: #639B00;">
                        <div class="popheader" style="background-color: red;">
                            <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnDeActiveOk" runat="server" OnClick="btnDeActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- DeActive  popup Closed--%>
                    <%-- Grid Alert popup Start--%>
                    <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                        TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                        <div class="popheader popheaderlblred">
                            <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Grid Alert popup Closed--%>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/PageValidations/AddSubStations.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
