﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddServiceUnits
                     
 Developer        : id065-Bhimaraju V
 Creation Date    : 20-Feb-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using Resources;
using iDSignHelper;
using System.Xml;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Globalization;

namespace UMS_Nigeria.Masters
{
    public partial class AddSubStations : System.Web.UI.Page
    {
        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        string Key = "AddSubStations";
        DataTable dtData = new DataTable();
        static int customersCount = 0;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                string path = string.Empty;
                path = objCommonMethods.GetPagePath(this.Request.Url);
                if (objCommonMethods.IsAccess(path))
                {
                    //objCommonMethods.BindServiceCenters(ddlServiceCenterName, string.Empty, true);
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    BindGridSubStationsList();
                    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    //if ((Request.QueryString[UMS_Resource.QSTR_SERVICE_CENTER_CODE] != null))
                    //{
                    //    ddlServiceCenterName.SelectedValue = objiDsignBAL.Decrypt(Request.QueryString[UMS_Resource.QSTR_SERVICE_CENTER_CODE]).ToString();                        
                    //}
                }
                else { Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }
            }

        }

        protected void lkbtnAddNewServiceCenter_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_SERVICE_CENTER_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();

                objMastersBE.SubStationName = txtSubStationName.Text;
                //objMastersBE.ServiceCenterId = ddlServiceCenterName.SelectedValue;
                objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.Notes = objiDsignBAL.ReplaceNewLines(txtNotes.Text, true);
                xml = objMastersBAL.SubStation(objMastersBE, Statement.Insert);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);
                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGridSubStationsList();
                    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.SUB_STATION_INSERT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    txtSubStationName.Text = txtNotes.Text = string.Empty;
                }
                else
                    Message(UMS_Resource.SUB_STATION_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSaveNext_Click(object sender, EventArgs e)
        {
            try
            {
                InsertSubStationDetails();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_FEEDER_PAGE + "?" + UMS_Resource.QSTR_SUB_STATION_CODE + "=" + objiDsignBAL.Encrypt(hfSubStationId.Value), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridSubStationsList();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridSubStationsList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridSubStationsList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridSubStationsList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                BindGridSubStationsList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                objiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);

                //    ddlPageSize.Items.Clear();
                //    if (TotalRecords < Constants.PageSizeStarts)
                //    {
                //        ListItem item = new ListItem(Constants.PageSizeStarts.ToString(), Constants.PageSizeStarts.ToString());
                //        ddlPageSize.Items.Add(item);
                //    }
                //    else
                //    {
                //        int previousSize = Constants.PageSizeStarts;
                //        for (int i = Constants.PageSizeStarts; i <= TotalRecords; i = i + Constants.PageSizeIncrement)
                //        {
                //            ListItem item = new ListItem(i.ToString(), i.ToString());
                //            ddlPageSize.Items.Add(item);
                //            previousSize = i;
                //        }
                //        if (previousSize < TotalRecords)
                //        {
                //            ListItem item = new ListItem((previousSize + Constants.PageSizeIncrement).ToString(), (previousSize + Constants.PageSizeIncrement).ToString());
                //            ddlPageSize.Items.Add(item);
                //        }
                //    }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvSubStationList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridSubStationName = (TextBox)row.FindControl("txtGridSubStationName");
                TextBox txtGridNotes = (TextBox)row.FindControl("txtGridNotes");
                // DropDownList ddlGridServiceCenterName = (DropDownList)row.FindControl("ddlGridServiceCenterName");

                Label lblSubStationId = (Label)row.FindControl("lblSubStationId");
                Label lblSubStationName = (Label)row.FindControl("lblSubStationName");
                // Label lblServiceCenterName = (Label)row.FindControl("lblServiceCenterName");
                // Label lblGridServiceCenterName = (Label)row.FindControl("lblGridServiceCenterName");
                Label lblNotes = (Label)row.FindControl("lblNotes");
                switch (e.CommandName.ToUpper())
                {
                    case "EDITSUBSTATION":
                        lblNotes.Visible = lblSubStationName.Visible = false;
                        txtGridSubStationName.Visible = txtGridNotes.Visible = true;
                        txtGridSubStationName.Text = lblSubStationName.Text;
                        //objCommonMethods.BindServiceCenters(ddlGridServiceCenterName, string.Empty, true);
                        //ddlGridServiceCenterName.SelectedIndex = ddlGridServiceCenterName.Items.IndexOf(ddlGridServiceCenterName.Items.FindByValue(lblServiceCenterName.Text));
                        txtGridNotes.Text = objiDsignBAL.ReplaceNewLines(lblNotes.Text, false);
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;
                        break;
                    case "CANCELSUBSTATION":
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                        txtGridSubStationName.Visible = txtGridNotes.Visible = false;
                        lblNotes.Visible = lblSubStationName.Visible = true;
                        break;
                    case "UPDATESUBSTATION":
                        objMastersBE.SubStationId = lblSubStationId.Text;
                        //objMastersBE.ServiceCenterId = ddlGridServiceCenterName.SelectedValue;
                        objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objMastersBE.SubStationName = txtGridSubStationName.Text;
                        objMastersBE.Notes = objiDsignBAL.ReplaceNewLines(txtGridNotes.Text, true);
                        xml = objMastersBAL.SubStation(objMastersBE, Statement.Update);
                        objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                        if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        {
                            BindGridSubStationsList();
                            BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            Message(UMS_Resource.SUB_STATION_UPDATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                        }
                        else
                            Message(UMS_Resource.SUB_STATION_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "ACTIVESUBSTATION":

                        mpeActivate.Show();
                        hfSubStationId.Value = lblSubStationId.Text;
                        lblActiveMsg.Text = UMS_Resource.ACTIVE_STATION_POPUP_TEXT;
                        btnActiveOk.Focus();

                        //objMastersBE.ActiveStatusId = Constants.Active;
                        //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objMastersBE.SubStationId = lblSubStationId.Text;
                        //xml = objMastersBAL.SubStation(objMastersBE, Statement.Delete);
                        //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                        //if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        //{
                        //    BindGridSubStationsList();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message( UMS_Resource.SUB_STATION_ACTIVATE_SUCESS,UMS_Resource.MESSAGETYPE_SUCCESS);

                        //}
                        //else
                        //    Message( UMS_Resource.SUB_STATION_ACTIVATE_FAIL,UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "DEACTIVESUBSTATION":
                        if (CustomerCount(e.CommandArgument.ToString()))
                        {
                            mpeShiftCustomer.Show();
                            //hfBU_ID.Value = lblBusinessUnitId.Text;
                            lblShiftCustomer.Text = Resource.SHIFT_CUSTOMERS_SUBSTATION.Insert(19, customersCount.ToString() + " ");
                            btnShipCustomer.Focus();
                        }
                        else
                        {
                            mpeDeActive.Show();
                            hfSubStationId.Value = lblSubStationId.Text;
                            lblDeActiveMsg.Text = UMS_Resource.DEACTIVE_STATION_POPUP_TEXT;
                            btnDeActiveOk.Focus();
                        }

                        //objMastersBE.ActiveStatusId = Constants.DeActive;
                        //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objMastersBE.SubStationId = lblSubStationId.Text;
                        //xml = objMastersBAL.SubStation(objMastersBE, Statement.Delete);
                        //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                        //if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        //{
                        //    BindGridSubStationsList();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //   Message( UMS_Resource.SUB_STATION_DEACTIVATE_SUCESS,UMS_Resource.MESSAGETYPE_SUCCESS);
                        //}
                        //else
                        //    Message( UMS_Resource.SUB_STATION_DEACTIVATE_FAIL,UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.Active;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.SubStationId = hfSubStationId.Value;
                xml = objMastersBAL.SubStation(objMastersBE, Statement.Delete);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGridSubStationsList();
                    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.SUB_STATION_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else
                    Message(UMS_Resource.SUB_STATION_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.DeActive;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.SubStationId = hfSubStationId.Value;
                xml = objMastersBAL.SubStation(objMastersBE, Statement.Delete);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGridSubStationsList();
                    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.SUB_STATION_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                    Message(UMS_Resource.SUB_STATION_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvSubStationList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    TextBox txtGridSubStationName = (TextBox)e.Row.FindControl("txtGridSubStationName");
                    //DropDownList ddlGridServiceCenterName = (DropDownList)e.Row.FindControl("ddlGridServiceCenterName");

                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridSubStationName.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");


                    if (Convert.ToInt32(lblActiveStatusId.Text) == Constants.DeActive)
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void InsertSubStationDetails()
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();

                objMastersBE.SubStationName = txtSubStationName.Text;
                //objMastersBE.ServiceCenterId = ddlServiceCenterName.SelectedValue;
                objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.Notes = objiDsignBAL.ReplaceNewLines(txtNotes.Text, true);
                xml = objMastersBAL.SubStation(objMastersBE, Statement.Insert);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);
                hfSubStationId.Value = objMastersBE.SubStationId;
                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGridSubStationsList();
                    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    lblMsgPopup.Text = UMS_Resource.SUB_STATION_INSERT_SUCCESS;
                    mpeCountrypopup.Show();
                    btnOk.Focus();
                }
                else
                    Message(UMS_Resource.SUB_STATION_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public bool CustomerCount(string InjectionSubStationId)
        {
            XmlDocument _xml = new XmlDocument();
            CustomerDetailsBe _ObjCustomerDetailsBe = new CustomerDetailsBe();
            CustomerDetailsBAL _ObjCustomerDetailsBAL = new CustomerDetailsBAL();
            _ObjCustomerDetailsBe.InjectionSubStationId = InjectionSubStationId;
            _xml = _ObjCustomerDetailsBAL.CountCustomersBAL(_ObjCustomerDetailsBe, Statement.Check);
            _ObjCustomerDetailsBe = objiDsignBAL.DeserializeFromXml<CustomerDetailsBe>(_xml);
            customersCount = _ObjCustomerDetailsBe.Count;
            if (_ObjCustomerDetailsBe.Count > 0)
                return true;
            else
                return false;
        }

        private void BindGridSubStationsList()
        {
            try
            {
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();
                XmlDocument resultedXml = new XmlDocument();

                objMastersBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                objMastersBE.PageSize = PageSize;
                resultedXml = objMastersBAL.SubStation(objMastersBE, ReturnType.Get);
                objMastersListBE = objiDsignBAL.DeserializeFromXml<MastersListBE>(resultedXml);
                if (objMastersListBE.Items.Count > 0)
                {
                    divgrid.Visible = divpaging.Visible = true;
                    hfTotalRecords.Value = objMastersListBE.Items[0].TotalRecords.ToString();
                    gvSubStationList.DataSource = objMastersListBE.Items;
                    gvSubStationList.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = objMastersListBE.Items.Count.ToString();
                    divgrid.Visible = divpaging.Visible = false;
                    gvSubStationList.DataSource = new DataTable(); ;
                    gvSubStationList.DataBind();
                }
                
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}