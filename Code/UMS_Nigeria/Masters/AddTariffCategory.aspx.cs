﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddTariffCategory.aspx
                     
 Developer        : id065-Bhimaraju V
 Creation Date    : 26-June-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using Resources;
using iDSignHelper;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Globalization;

namespace UMS_Nigeria.Masters
{
    public partial class AddTariffCategory : System.Web.UI.Page
    {
        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        TariffManagementBal objTariffBal = new TariffManagementBal();
        public int PageNum;
        string Key = UMS_Resource.KEY_TARIFF_CATEGORY;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = objCommonMethods.GetPagePath(this.Request.Url);
                //if (objCommonMethods.IsAccess(path))
                //{
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                //}
                //else { Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                TariffManagementBE objTariffBE = new TariffManagementBE();
                objTariffBE.ClassName = txtCategoryName.Text.Trim();
                objTariffBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objTariffBE.Details = objiDsignBAL.ReplaceNewLines(txtDetails.Text, true);
                objTariffBE = objiDsignBAL.DeserializeFromXml<TariffManagementBE>(objTariffBal.InsertTariff(objTariffBE, Statement.Insert));
                if (objTariffBE.IsSuccess)
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(Resource.TARIFF_CATEGORY_INSERT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    txtCategoryName.Text = txtDetails.Text = string.Empty;
                }
                else if (objTariffBE.IsClassNameExists)
                    Message(Resource.TARIFF_CATEGORY_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                else
                    Message(Resource.TARIFF_CATEGORY_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvTariff_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridCategoryName = (TextBox)row.FindControl("txtGridCategoryName");
                TextBox txtGridDetails = (TextBox)row.FindControl("txtGridDetails");
                Label lblClassId = (Label)row.FindControl("lblClassId");
                Label lblCategoryName = (Label)row.FindControl("lblCategoryName");
                Label lblActiveStatusId = (Label)row.FindControl("lblActiveStatusId");
                Label lblDetails = (Label)row.FindControl("lblDetails");

                TariffManagementListBE objTariffManagementListBE = new TariffManagementListBE();
                TariffManagementBE objTariffManagementBE = new TariffManagementBE();

                switch (e.CommandName.ToUpper())
                {
                    case "EDITTARIFF":
                        lblDetails.Visible = lblCategoryName.Visible = false;
                        txtGridCategoryName.Visible = txtGridDetails.Visible = true;
                        txtGridCategoryName.Text = lblCategoryName.Text;
                        txtGridDetails.Text = objiDsignBAL.ReplaceNewLines(lblDetails.Text, false);
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;
                        break;
                    case "CANCELTARIFF":
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                        txtGridCategoryName.Visible = txtGridDetails.Visible = false;
                        lblDetails.Visible = lblCategoryName.Visible = true;
                        break;
                    case "UPDATETARIFF":
                        objTariffManagementBE.ClassID = Convert.ToInt32(lblClassId.Text);
                        objTariffManagementBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objTariffManagementBE.ClassName = txtGridCategoryName.Text;
                        objTariffManagementBE.Details = objiDsignBAL.ReplaceNewLines(txtGridDetails.Text, true);
                        objTariffManagementBE = objiDsignBAL.DeserializeFromXml<TariffManagementBE>(objTariffBal.InsertTariff(objTariffManagementBE, Statement.Update));

                        if (objTariffManagementBE.IsSuccess)
                        {
                            BindGrid();
                            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            Message(Resource.TARIFF_CATEGORY_UPDATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        else if (objTariffManagementBE.IsClassNameExists)
                            Message(Resource.TARIFF_CATEGORY_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                        else
                            Message(Resource.TARIFF_CATEGORY_UPDATED_FAILED, UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "ACTIVETARIFF":
                        mpeActivate.Show();
                        hfClassID.Value = lblClassId.Text;
                        lblActiveMsg.Text = Resource.ACTIVE_TARIFF_CATEGORY_POPUP_TEXT;
                        btnActiveOk.Focus();
                        break;
                    case "DEACTIVETARIFF":
                        mpeDeActive.Show();
                        hfClassID.Value = lblClassId.Text;
                        lblDeActiveMsg.Text = Resource.DEACTIVE_TARIFF_CATEGORY_POPUP_TEXT;
                        btnDeActiveOk.Focus();
                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                TariffManagementBE objTariffBE = new TariffManagementBE();
                objTariffBE.ActiveStatusId = true;
                objTariffBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objTariffBE.ClassID = Convert.ToInt32(hfClassID.Value);
                objTariffBE = objiDsignBAL.DeserializeFromXml<TariffManagementBE>(objTariffBal.InsertTariff(objTariffBE, Statement.Delete));

                if (objTariffBE.IsSuccess)
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(Resource.TARIFF_CATEGORY_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else
                    Message(Resource.TARIFF_CATEGORY_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                TariffManagementBE objTariffBE = new TariffManagementBE();
                objTariffBE.ActiveStatusId = false;
                objTariffBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objTariffBE.ClassID = Convert.ToInt32(hfClassID.Value);
                objTariffBE = objiDsignBAL.DeserializeFromXml<TariffManagementBE>(objTariffBal.InsertTariff(objTariffBE, Statement.Delete));

                if (objTariffBE.IsSuccess)
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(Resource.TARIFF_CATEGORY_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                {
                    if (objTariffBE.IsLastActiveRecord)
                    {
                        lblgridalertmsg.Text = "Failed to Deactivate. Tariff Category must contain atleast on active Record.";
                        mpegridalert.Show();
                        btngridalertok.Focus();
                    }
                    else if (objTariffBE.Count > 0)
                    {
                        //Message(string.Format(Resource.TARIFF_CATEGORY_HAVE_CUSTOMERS, objTariffBE.Count), UMS_Resource.MESSAGETYPE_ERROR);
                        Message(Resource.TARIFF_CATEGORY_HAVE_CUSTOMERS, UMS_Resource.MESSAGETYPE_ERROR);//Faiz-ID103
                    }
                    else
                    {
                        Message(Resource.TARIFF_CATEGORY_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvTariff_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    TextBox txtGridCategoryName = (TextBox)e.Row.FindControl("txtGridCategoryName");
                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridCategoryName.ClientID + "')");

                    Label lblIsActive = (Label)e.Row.FindControl("lblActiveStatusId");


                    if ((!Convert.ToBoolean(lblIsActive.Text)))
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        //#region Paging
        //protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        BindTariffCategoryList();
        //        hfPageSize.Value = ddlPageSize.SelectedItem.Text;
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnFirst_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        BindTariffCategoryList();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnPrevious_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum -= Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        BindTariffCategoryList();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnNext_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum += Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        BindTariffCategoryList();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnLast_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = hfLastPage.Value;
        //        BindTariffCategoryList();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //private void CreatePagingDT(int TotalNoOfRecs)
        //{
        //    try
        //    {
        //        double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
        //        DataTable dtPages = new DataTable();
        //        dtPages.Columns.Add("PageNo", typeof(int));
        //        int currentpage = Convert.ToInt32(hfPageNo.Value);
        //        lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
        //        objiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
        //        lblCurrentPage.Text = hfPageNo.Value;
        //        if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
        //        if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //private void BindPagingDropDown(int TotalRecords)
        //{
        //    try
        //    {
        //        objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //#endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void BindGrid()
        {
            try
            {
                TariffManagementListBE objTariffListBE = new TariffManagementListBE();
                TariffManagementBE objTariffBE = new TariffManagementBE();
                objTariffBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                objTariffBE.PageSize = PageSize;
                objTariffListBE = objiDsignBAL.DeserializeFromXml<TariffManagementListBE>(objTariffBal.GetTariff(objTariffBE, ReturnType.Get));
                if (objTariffListBE.items.Count > 0)
                {
                    divdownpaging.Visible = divpaging.Visible = true;
                    hfTotalRecords.Value = objTariffListBE.items[0].TotalRecords.ToString();
                    gvTariff.DataSource = objTariffListBE.items;
                    gvTariff.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = objTariffListBE.items.Count.ToString();
                    divdownpaging.Visible = divpaging.Visible = false;
                    gvTariff.DataSource = new DataTable(); ;
                    gvTariff.DataBind();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}