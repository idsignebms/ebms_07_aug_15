﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using UMS_NigeriaBAL;
using iDSignHelper;
using Resources;

namespace UMS_Nigeria.Masters
{
    public partial class AllMasters : System.Web.UI.Page
    {
        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        #endregion

        # region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                
                if (!IsPostBack)
                {
                    string path = string.Empty;
                    path = objCommonMethods.GetPagePath(this.Request.Url);
                    if (objCommonMethods.IsAccess(path))
                    {
                        
                    }
                    else { Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }
                }
            }
            else { Response.Redirect(UMS_Resource.DEFAULT_PAGE); }
        }
        #endregion
    }
}