﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master" AutoEventWireup="true" Theme="Green"
    CodeBehind="AppSettings.aspx.cs" Inherits="UMS_Nigeria.AppSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upStates" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddState" runat="server" Text="Application Settings"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litCountryCode" runat="server" Text=" Select Theme"></asp:Literal>
                                    <span class="span_star">*</span></label>
                                <asp:DropDownList ID="ddlThemeList" runat="server" CssClass="text-box">
                                    <%--    <asp:ListItem Text="Green" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Blue" Value="2"></asp:ListItem>--%>
                                </asp:DropDownList>
                                 <asp:Literal ID="lblCurrentTheme" runat="server" Text=" Select Theme"></asp:Literal>
                                <span id="spanCountryCode" class="span_color"></span>
                            </div>
                            <span>
                                <asp:Button ID="btnSetTheme" runat="server" Text="Set" OnClick="btnSetTheme_Click"
                                    CssClass="box_s" /></span>
                        </div>
                        <div style="clear: both;">
                        </div>
                    </div>
                </div>
                <%-- Grid Alert popup Start--%>
                <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                    TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                </asp:ModalPopupExtender>
                <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                    <div class="popheader popheaderlblred">
                        <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="pnlAlert" TargetControlID="hfAlert"
                    BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hfAlert" runat="server" />
                <asp:Panel ID="pnlAlert" runat="server" CssClass="modalPopup" Style="display: none;">
                    <div class="popheader popheaderlblred">
                        <asp:Label ID="lblAlert" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btnOkay" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="ok_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeConfirmation" runat="server" PopupControlID="pnlConfirmation"
                    TargetControlID="hfConfirmation" BackgroundCssClass="modalBackground" CancelControlID="btnCancel">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hfConfirmation" runat="server" />
                <asp:Panel ID="pnlConfirmation" runat="server" CssClass="modalPopup" Style="display: none;">
                    <div class="popheader popheaderlblred">
                        <asp:Label ID="lblConfirmation" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btnOk" runat="server" OnClick="btnOk_Click" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                            <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="ok_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
            </div>
            <asp:HiddenField ID="hfCurrentTheme" runat="server" Value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
