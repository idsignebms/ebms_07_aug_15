﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.Configuration;
using Resources;
using System.IO;
namespace UMS_Nigeria
{
    public partial class AppSettings : System.Web.UI.Page
    {
        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetThemes();
                getCurrentTheme();
            }
        }

        protected void btnSetTheme_Click(object sender, EventArgs e)
        {
            if (hfCurrentTheme.Value == ddlThemeList.SelectedItem.Text)
            {
               lblAlert.Text = "Please select new theme. Selected theme is your current theme.";
               mpeAlert.Show();
            }
            else
            {
                lblConfirmation.Text = "All login session will get expired. Are you sure to change theme?";
                mpeConfirmation.Show();
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            changeTheme();
        }

        protected void btnOkay_Click(object sender, EventArgs e)
        {
            Response.Redirect(UMS_Resource.DEFAULT_PAGE, false);
        }
        #endregion

        #region Get All Theme
        public void GetThemes()
        {
            DirectoryInfo dInfo = new DirectoryInfo(
              System.Web.HttpContext.Current.Server.MapPath("~/App_Themes"));
            DirectoryInfo[] dArrInfo = dInfo.GetDirectories();
            List<string> list = new List<string>();
            foreach (DirectoryInfo sDirectory in dArrInfo)
            {
                ListItem lst = new ListItem();
                lst.Text = sDirectory.Name;
                ddlThemeList.Items.Add(lst);
            }
        }

        public void changeTheme()
        {
            try
            {
                Configuration myConfig = WebConfigurationManager.OpenWebConfiguration("~/Masters/");
                PagesSection pageSection = (PagesSection)myConfig.GetSection("system.web/pages");
                pageSection.Theme = ddlThemeList.SelectedItem.Text;
                myConfig.Save();
                lblAlert.Text = "Theme has been change successfully. Please re-login.";
                mpeAlert.Show();
            }
            catch
            {
                lblAlert.Text = "Theme is not change!";
                mpeAlert.Show();
            }
        }

        public void getCurrentTheme()
        {
            string currentTheme = string.Empty;
            try
            {

                Configuration myConfig = WebConfigurationManager.OpenWebConfiguration("~/Masters/");
                PagesSection pageSection = (PagesSection)myConfig.GetSection("system.web/pages");
                currentTheme = pageSection.Theme;
                if (currentTheme != string.Empty)
                {
                    hfCurrentTheme.Value = currentTheme;
                    lblCurrentTheme.Text = "Pesent theme is: " + currentTheme;
                }
            }
            catch
            {

            }
        }
        #endregion
    }
}