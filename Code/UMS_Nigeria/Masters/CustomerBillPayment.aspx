﻿<%@ Page Title=":: Customer Bill Payments ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="CustomerBillPayment.aspx.cs"
    Inherits="UMS_Nigeria.HTML.CustomerBillPayment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControls/Authentication.ascx" TagName="Authentication" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out_bor_Payment">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upPaymentEntry" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div class="text_total">
                    <div class="text-heading">
                        <asp:Literal ID="litAddState" runat="server" Text="<%$ Resources:Resource, CUSTOMER_BILL_PAYMENT%>"></asp:Literal>
                    </div>
                    <div class="star_text">
                        <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div class="dot-line">
                </div>
                <div class="inner-box">
                    <div class="inner-box1">
                        <div class="text-inner">
                            <asp:Literal ID="litCountryName" runat="server" Text="<%$ Resources:Resource, ACCNO_OLD_ACCNO%>"></asp:Literal>
                            <span class="span_star">*</span><div class="space">
                            </div>
                            <asp:TextBox CssClass="text-box" runat="server" ID="txtAccountNo" MaxLength="20"
                                onblur="return TextBoxBlurValidationlbl(this,'spanAccountNo','Global Account No / Old Account No')"
                                onkeypress="GetAccountDetails(event)" placeholder="Enter Global Account No / Old Account No"></asp:TextBox>
                            &nbsp
                            <asp:FilteredTextBoxExtender ID="ftbAccNo" runat="server" TargetControlID="txtAccountNo"
                                FilterType="Numbers" ValidChars=" ">
                            </asp:FilteredTextBoxExtender>
                            <%--<asp:LinkButton runat="server" ID="lblSearch" OnClick="lblSearch_Click" CssClass="search_img"></asp:LinkButton>--%>
                            <br />
                            <span id="spanAccountNo" class="span_color"></span>
                        </div>
                    </div>
                </div>
                <div class="box_total">
                    <div class="box_total_a" style="margin-left: 15px;">
                        <asp:Button ID="btnGo" Text="<%$ Resources:Resource, GET_CUSTOMER_DETAILS%>" CssClass="box_s"
                            runat="server" OnClientClick="return Validate();" OnClick="btnGo_Click" />
                    </div>
                </div>
                <div class="clr">
                </div>
                <br />
                <div class="billsearch_Payments">
                    <div class="billcustomert_Payments">
                        <div class="custmrdtails">
                            <div class="text_total" style="margin-bottom: 15px;">
                                <div class="text-heading">
                                    <asp:Literal ID="Literal21" runat="server" Text="Customer Details"></asp:Literal>
                                </div>
                            </div>
                            <div class="out-bor_cTwo" style="width:87%;">
                                <div class="inner-box">
                                    <div class="inner-box1_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal32" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_b">
                                        <div class="customerDetailstext">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box3_b">
                                        <div class="text_cusa_a">
                                            <asp:Label runat="server" ID="lblAccNoAndGlobalAccNo"></asp:Label>
                                            <asp:Label runat="server" Visible="false" ID="lblAccountno"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box4_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal33" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box5_b">
                                        <div class="customerDetailstext">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box6_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblOldAccountNo" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="litOutstandingAmount" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_b">
                                        <div class="customerDetailstext">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box3_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblName" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box4_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box5_b">
                                        <div class="customerDetailstext">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box6_b">
                                        <div class="text_cusa_a">
                                            <asp:Label runat="server" ID="lblServiceAddress"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal35" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_b">
                                        <div class="customerDetailstext">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box3_b">
                                        <div class="text_cusa_a">
                                            <asp:Label runat="server" ID="lblBookGroup"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box4_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal36" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box5_b">
                                        <div class="customerDetailstext">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box6_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblBookName" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal37" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_b">
                                        <div class="customerDetailstext">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box3_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblTariff" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box4_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal38" runat="server" Text="<%$ Resources:Resource, OUT_STANDING_AMT%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box5_b">
                                        <div class="customerDetailstext">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box6_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblTotalDueAmount" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal39" runat="server" Text="<%$ Resources:Resource, LAST_AMOUNTPAID%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_b">
                                        <div class="customerDetailstext">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box3_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblLastPaidAmount" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box4_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal40" runat="server" Text="<%$ Resources:Resource, LAST_PAID_DATE%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box5_b">
                                        <div class="customerDetailstext">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box6_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblbLastPaidDate" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                            <%--<ul>
                                <li>
                                    <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal></li>
                                <li class="coloncus">:</li><li class="listright">
                                    <asp:Label runat="server" ID="lblAccNoAndGlobalAccNo"></asp:Label>
                                    <asp:Label runat="server" Visible="false" ID="lblAccountno"></asp:Label>
                                </li>
                                <li>
                                    <asp:Literal ID="litOldAccountNo" runat="server" Text="Old Account No"></asp:Literal></li>
                                <li class="coloncus">:</li>
                                <li class="listright">
                                    <asp:Label ID="lblOldAccountNo" runat="server"></asp:Label></li>
                                <li>
                                    <asp:Literal ID="Literal3" runat="server" Text="Name"></asp:Literal></li>
                                <li class="coloncus">:</li>
                                <li class="listright" style="width: 130px;">
                                    <asp:Label runat="server" ID="lblName"></asp:Label></li>
                                <div class="clear pad_10">
                                </div>
                                <li style="margin-left: 16px; min-width: 98px;">
                                    <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Literal>
                                </li>
                                <li class="coloncus">:</li><li class="listright" style="line-height: 23px; margin-right: 0;
                                    width: 180px;">                                    
                                    <asp:Label runat="server" ID="lblServiceAddress"></asp:Label></li>
                                <li>
                                    <asp:Literal ID="Literal1" runat="server" Text="Book Group"></asp:Literal></li>
                                <li class="coloncus">:</li><li class="listright">
                                    <asp:Label runat="server" ID="lblBookGroup"></asp:Label></li>
                                <li>
                                    <asp:Literal ID="Literal23" runat="server" Text="Book Name"></asp:Literal></li>
                                <li class="coloncus">:</li>
                                <li class="listright" style="width: 130px;">
                                    <asp:Label runat="server" ID="lblBookName"></asp:Label></li>
                                <div class="clear pad_10">
                                </div>
                                <li style="margin-left: 20px; min-width: 115px;">
                                    <asp:Literal ID="Literal24" runat="server" Text="Tariff"></asp:Literal>
                                </li>
                                <li class="coloncus">:</li><li class="listright" style="line-height: 23px; margin-right: 0;
                                    width: 180px;">
                                    <asp:Label runat="server" ID="lblTariff"></asp:Label></li>
                                <li style="min-width: 125px;">
                                    <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, OUT_STANDING_AMT%>"></asp:Literal></li>
                                <li class="coloncus">:</li>
                                <li class="listright">
                                    <asp:Label ID="lblTotalDueAmount" runat="server"></asp:Label></li>
                                <li style="min-width: 125px;">
                                    <asp:Literal ID="Literal19" runat="server" Text="Last Paid Amount"></asp:Literal></li>
                                <li class="coloncus">:</li>
                                <li class="listright">
                                    <asp:Label ID="lblLastPaidAmount" runat="server"></asp:Label></li>
                                <div class="clear pad_10">
                                </div>
                                <li style="margin-left: 32px; min-width: 114px;">
                                    <asp:Literal ID="Literal20" runat="server" Text="Last Paid Date"></asp:Literal></li>
                                <li class="coloncus">:</li>
                                <li class="listright">
                                    <asp:Label ID="lblbLastPaidDate" runat="server"></asp:Label></li>
                                <div class="clear pad_10">
                                </div>
                            </ul>--%>
                        </div>
                    </div>
                    <div class="clear pad_10">
                    </div>
                    <div id="divPendingBills" runat="server" visible="false" class="billcustomert" style="width: 67%;">
                        <div class="text_total">
                            <div class="text-heading" style="float: left;">
                                <asp:Literal ID="ltlLastBill" runat="server" Text="Last Bill Details"></asp:Literal>
                            </div>
                            <div style="float: right;">
                                <asp:LinkButton ID="lbtnTotalBillsCount" runat="server" OnClick="lbtnTotalBillsCount_Click">
                                    Total Pending Bills(<asp:Label ID="lblTotalPendingBills" runat="server"></asp:Label>)</asp:LinkButton>
                            </div>
                        </div>
                        <div id="divPrint" runat="server">
                            <div class="prev_total routwise" style="width: 800px;">
                                <ul>
                                    <div style="width: 777px;">
                                        <li class="prevleft fltl">
                                            <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:Resource, BILLNUMBER%>"></asp:Literal><br />
                                            <asp:Label runat="server" ID="lblBillNo" Text=""></asp:Label>
                                        </li>
                                        <li class=" fltl"></li>
                                        <li class=" fltl">
                                            <asp:Literal runat="server" ID="litCustomerBillID" Visible="false" Text='<%# Eval("CustomerBillID") %>'></asp:Literal>
                                        </li>
                                    </div>
                                    <div style="clear: none; float: left; width: 360px;">
                                        <li class="prevleft fltl">
                                            <asp:Literal ID="Literal25" runat="server" Text="Bill Type"></asp:Literal></li><li
                                                class=" fltl">:</li>
                                        <li class=" fltl" style="width:150px;">
                                            <asp:Label runat="server" ID="lblReadType" Text=""></asp:Label>
                                        </li>
                                    </div>
                                    <div style="clear: none; float: left; width: 395px; background: none repeat scroll 0 0 #fff;">
                                        <li class="prevleft fltl" style="width: 200px;">
                                            <asp:Literal ID="Literal31" runat="server" Text="<%$ Resources:Resource, ENERGYCHARGES%>"></asp:Literal></li><li
                                                class=" fltl">:</li>
                                        <li class=" fltl" style="width:150px;">
                                            <asp:Label runat="server" ID="lblNetEnergyCharges" Text=""></asp:Label>
                                        </li>
                                    </div>
                                    <div style="clear: none; float: left; width: 360px;">
                                        <li class="prevleft fltl">
                                            <asp:Literal ID="Literal26" runat="server" Text="Billing Month"></asp:Literal></li><li
                                                class=" fltl">:</li>
                                        <li class=" fltl" style="width:150px;">
                                            <asp:Label runat="server" ID="lblBillMonth" Text=""></asp:Label>
                                        </li>
                                    </div>
                                    <div style="clear: none; float: left; width: 395px; background: none repeat scroll 0 0 #fff;">
                                        <li class="prevleft fltl" style="width: 200px;">
                                            <asp:Literal ID="Literal28" runat="server" Text="Fixed Charges"></asp:Literal></li><li
                                                class=" fltl">:</li>
                                        <li class=" fltl" style="width:150px;">
                                            <asp:Label runat="server" ID="lblFixedCharges" Text=""></asp:Label>
                                        </li>
                                    </div>
                                    <div style="clear: none; float: left; width: 360px;">
                                        <li class="prevleft fltl">
                                            <asp:Literal ID="Literal27" runat="server" Text="Bill Generation Date"></asp:Literal></li><li
                                                class=" fltl">:</li>
                                        <li class=" fltl" style="width:150px;">
                                            <asp:Label runat="server" ID="lblBillDate" Text=""></asp:Label>
                                        </li>
                                    </div>
                                    <div style="clear: none; float: left; width: 395px; background: none repeat scroll 0 0 #fff;">
                                        <li class="prevleft fltl" style="width: 200px;">
                                            <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:Resource, BILL_AMT%>"></asp:Literal></li>
                                        <li class=" fltl">:</li>
                                        <li class=" fltl" style="width:150px;">
                                            <asp:Label runat="server" ID="lblTotalAmt" Text=""></asp:Label>
                                        </li>
                                    </div>
                                    <div style="clear: none; float: left; width: 360px;">
                                        <li class="prevleft fltl">
                                            <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, PREVIOUS_READING%>"></asp:Literal></li><li
                                                class=" fltl">:</li>
                                        <li class=" fltl" style="width:150px;">
                                            <asp:Label runat="server" ID="lblPreviousReading" Text=""></asp:Label>
                                        </li>
                                    </div>
                                    <div style="clear: none; float: left; width: 395px; background: none repeat scroll 0 0 #fff;">
                                        <li class="prevleft fltl" style="width: 200px;">
                                            <asp:Literal ID="Literal22" runat="server" Text="<%$ Resources:Resource, TAX%>"></asp:Literal></li><li
                                                class=" fltl">:</li>
                                        <li class=" fltl" style="width:150px;">
                                            <asp:Label runat="server" ID="lblTax" Text=""></asp:Label>
                                        </li>
                                    </div>
                                    <div style="clear: none; float: left; width: 360px;">
                                        <li class="prevleft fltl">
                                            <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, CURRENT_READING%>"></asp:Literal></li><li
                                                class=" fltl">:</li>
                                        <li class=" fltl" style="width:150px;">
                                            <asp:Label runat="server" ID="lblPresentReading" Text=""></asp:Label></li>
                                    </div>
                                    <div style="clear: none; float: left; width: 395px; background: none repeat scroll 0 0 #fff;">
                                        <li class="prevleft fltl" style="width: 200px;">
                                            <asp:Literal ID="Literal5" runat="server" Text="Total Bill Amount With VAT"></asp:Literal></li><li
                                                class=" fltl">:</li>
                                        <li class=" fltl" style="width:150px;">
                                            <asp:Label runat="server" ID="lblTotalBillwithVAT" Text=""></asp:Label>
                                        </li>
                                    </div>
                                    <div style="clear: none; float: left; width: 360px;">
                                        <li class="prevleft fltl">
                                            <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:Resource, CONSUMPTION%>"></asp:Literal>
                                        </li>
                                        <li class=" fltl">:</li><li class=" fltl" style="width:150px;">
                                            <asp:Label runat="server" ID="lblConsumption" Text=""></asp:Label></li>
                                    </div>
                                    <div style="clear: none; float: left; width: 395px; background: none repeat scroll 0 0 #fff;">
                                        <li class="prevleft fltl" style="width: 200px;">
                                            <asp:Literal ID="Literal6" runat="server" Text="Net Arrears"></asp:Literal>
                                        </li>
                                        <li class=" fltl">:</li>
                                        <li class=" fltl" style="width:150px;">
                                            <asp:Label runat="server" ID="lblNetArrears" Text=""></asp:Label>
                                        </li>
                                    </div>
                                    <div style="clear: none; float: left; width: 360px;">
                                        <li class="prevleft fltl">
                                            <asp:Literal ID="Literal29" runat="server" Text="<%$ Resources:Resource, CUSTOMER_TYPE%>"></asp:Literal>
                                        </li>
                                        <li class=" fltl">:</li>
                                        <li class=" fltl" style="width:150px;">
                                            <asp:Label runat="server" ID="lblCustomerType" Text=""></asp:Label></li>
                                    </div>
                                    <div style="clear: none; float: left; width: 395px; background: none repeat scroll 0 0 #fff;">
                                        <li class="prevleft fltl" style="color: green; font-size: 14px; font-weight: bold;
                                            width: 200px;">
                                            <asp:Literal ID="Literal7" runat="server" Text="Total Bill Amount with Arrears"></asp:Literal></li><li
                                                class=" fltl">:</li>
                                        <li class=" fltl" style="color: green; font-size: 14px; font-weight: bold; width:150px;">
                                            <asp:Label runat="server" ID="lblDueAmount" Text=""></asp:Label>
                                        </li>
                                    </div>
                                    <div style="clear: none; float: left; width: 360px;">
                                        <li class="prevleft fltl">
                                            <asp:Literal ID="Literal30" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                                        </li>
                                        <li class=" fltl">:</li>
                                        <li class=" fltl" style="width:150px;">
                                            <asp:Label runat="server" ID="lblBillTariff" Text=""></asp:Label>
                                        </li>
                                    </div>
                                    <div style="background: none repeat scroll 0 0 #fff; clear: none; float: left; width: 395px;">
                                        <li class="prevleft fltl" style="color: green; font-size: 14px; font-weight: bold;
                                            width: 200px;">
                                            <asp:Literal ID="Literal9" runat="server" Text="Outstanding Amount"></asp:Literal></li><li
                                                class=" fltl">:</li>
                                        <li class=" fltl" style="color: green; font-size: 14px; font-weight: bold; width:150px;">
                                            <asp:Label runat="server" ID="lblOutstandingBottom" Text=""></asp:Label>
                                        </li>
                                    </div>
                                </ul>
                            </div>
                            <br />
                            <%--<div id="divAdj" runat="server" visible="false">
                                <asp:Literal runat="server" ID="lblApprovalStatusId" Visible="false" Text='<%# Eval("ApprovalStatusId") %>'></asp:Literal>
                                <asp:Label ID="lblAdjMsg" runat="server" Text="<%$Resources:Resource, BILL_ADJUSTMENT_REQ_SENT %>"></asp:Label><br />
                                <asp:Label ID="Lable222" runat="server" Text="<%$ Resources:Resource, CLICK%>"></asp:Label>
                                <asp:LinkButton ID="lnkBtnDetails" runat="server" Visible="false" CommandName="getBADetails"
                                    CommandArgument='<%# Eval("BAID") %>' Text="here" ToolTip="<%$ Resources:Resource, GET_ADJUSTMENT_INFO_TOOL_TIP%>"></asp:LinkButton>
                                <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, FOR_DETAILS%>"></asp:Label>
                            </div>--%>
                        </div>
                        <%--  </ItemTemplate>
                            </asp:DataList>--%>
                        <%-- <asp:DataList runat="server" ID="dtDirectCustomer" RepeatDirection="Horizontal" RepeatColumns="4">
                                <ItemTemplate>
                                    <div class="prev_total routwise">
                                        <ul>
                                            <li class="prevleft fltl">
                                                <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:Resource, BILLNUMBER%>"></asp:Literal></li><li
                                                    class=" fltl">:</li>
                                            <li class=" fltl">
                                                <asp:Label runat="server" ID="lblBillno" Text='<%# Eval("BillNo") %>'></asp:Label>
                                            </li>
                                            <li class="clear pad_5"></li>
                                            <li class="prevleft fltl">
                                                <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, ENERGYCHARGES%>"></asp:Literal></li><li
                                                    class=" fltl">:</li>
                                            <li class=" fltl">
                                                <asp:Label runat="server" ID="lblDirectNetEnergyCharges" Text='<%# Eval("NetEnergyCharges") %>'></asp:Label>
                                            </li>
                                            <li class="clear pad_5"></li>
                                            <asp:Repeater runat="server" ID="rptrDirectCustomer">
                                                <ItemTemplate>
                                                    <li class="prevleft fltl">
                                                        <asp:Label ID="lblDirectChargeName" runat="server" Text='<%# Eval("ChargeName") %>'></asp:Label>
                                                    </li>
                                                    <li class=" fltl">:</li><li class=" fltl">
                                                        <asp:Label runat="server" ID="lblDirectAmount" Text='<%# Eval("Amount") %>'></asp:Label></li>
                                                    <li class="clear pad_5"></li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <li class="clear pad_5"></li>
                                            <li class="prevleft fltl">
                                                <asp:Literal ID="Literal29" runat="server" Text="<%$ Resources:Resource, REMARKS%>"></asp:Literal>
                                            </li>
                                            <li class=" fltl">:</li><li class=" fltl">
                                                <asp:Label runat="server" ID="Label2" Text='<%# Eval("Remarks") %>'></asp:Label></li>
                                            <li class="clear pad_5"></li>
                                        </ul>
                                    </div>
                                    <div class="billclose">
                                        <asp:LinkButton ID="lkbtnEdit" runat="server" CommandArgument='<%# Eval("BillNo") %>'
                                            CommandName='<%# Eval("CustomerReadingId") %>'>
                                                 <img src="../images/Edit.png" alt="Edit" /></asp:LinkButton>
                                        <div class="clear pad_5">
                                        </div>
                                </ItemTemplate>
                            </asp:DataList>--%>
                        <div class=" clear pad_10">
                        </div>
                        <div class="consumer_feild fltl">
                            <%--<div class="consumer_feild">
                                <div class="consume_name" style="width: 89px;">
                                    <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:Resource, PAYMENT_DATE%>"></asp:Literal><span>*</span>
                                </div>
                                <div style="width: 217px;" class="consume_input c_left">
                                    <asp:TextBox CssClass="text-box"  ID="txtBatchDate" AutoComplete="off" MaxLength="10" onblur="return TextBoxBlurValidationlbl(this,'spanBatchDate','Batch Date')"
                                        Width="133px" runat="server"></asp:TextBox>
                                    <asp:Image ID="imgDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                        vertical-align: middle" AlternateText="Calender" runat="server" />
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtBatchDate"
                                        FilterType="Custom,Numbers" ValidChars="/">
                                    </asp:FilteredTextBoxExtender>
                                    <span id="spanBatchDate" class="spancolor"></span>
                                </div>
                            </div>--%>
                            <%--   <div class="consumer_feild">
                                <div class="consume_name" style="width: 89px;">
                                    <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:Resource, RECEIPT_NO%>"></asp:Literal><span>*</span>
                                </div>
                                <div style="width: 217px;" class="consume_input c_left">
                                    <asp:TextBox CssClass="text-box"  ID="txtRecieptNo" runat="server" MaxLength="10"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtRecieptNo"
                                        FilterType="Numbers, Custom">
                                    </asp:FilteredTextBoxExtender>
                                    <span id="spanReciept" style="color: Red;"></span>
                                </div>
                            </div>--%>
                            <%--<div class="consumer_feild">
                                <div class="consume_name" style="width: 89px;">
                                    <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, PAID_AMOUNT%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </div>
                                <div class="consume_input c_left">
                                    <asp:TextBox CssClass="text-box" ID="txtAmount" runat="server" MaxLength="10" onkeypress="return isNumberKey(event,this)"
                                        onblur="return ValidatePaidAmount()" placeholder="Enter Paid Amount"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="ftbTxtAnotherContactNo" runat="server" TargetControlID="txtAmount"
                                        FilterType="Numbers, Custom" ValidChars=".">
                                    </asp:FilteredTextBoxExtender>
                                    <span id="spanPaidAmount" class="span_color"></span>
                                </div>
                            </div>--%>
                        </div>
                        <div class="inner-box">
                            <div class="inner-box1" style="margin-left: 1px;">
                                <div class="text-inner">
                                    <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:Resource, PAID_AMOUNT%>"></asp:Literal>
                                    <span class="span_star">*</span><div class="space">
                                    </div>
                                    <asp:TextBox CssClass="text-box" ID="txtAmount" runat="server" MaxLength="18" onkeypress="return isNumberKey(event,this)"
                                        onblur="return ValidatePaidAmount()" placeholder="Enter Paid Amount" onkeyup="GetCurrencyFormate(this);"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="ftbTxtAnotherContactNo" runat="server" TargetControlID="txtAmount"
                                        FilterType="Numbers, Custom" ValidChars=",.">
                                    </asp:FilteredTextBoxExtender>
                                    <span id="spanPaidAmount" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <asp:Literal ID="Literal8" runat="server" Text="Payment Received Date"></asp:Literal><span
                                        class="span_star">*</span><br />
                                    <asp:TextBox CssClass="text-box" ID="txtPaidDate" autocomplete="off" onblur="return TextBoxBlurValidationlbl(this,'spanPaidDate','Payment Received Date')"
                                        onchange="return TextBoxBlurValidationlbl(this,'spanPaidDate','Payment Received Date')"
                                        MaxLength="10" Width="186px" placeholder="Payment Received Date" runat="server"></asp:TextBox>
                                    <asp:Image ID="imgMeterChangedDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                        vertical-align: middle" AlternateText="Calender" runat="server" />
                                    <asp:FilteredTextBoxExtender ID="ftbNextDate" runat="server" TargetControlID="txtPaidDate"
                                        FilterType="Custom,Numbers" ValidChars="/">
                                    </asp:FilteredTextBoxExtender>
                                    <span id="spanPaidDate" class="span_color"></span>
                                </div>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="box_total">
                            <div class="box_total_a" style="margin-left: -9px; margin-bottom: 10px;">
                                <asp:Button ID="btnSave" runat="server" CssClass="box_s" Text="Save" OnClick="btnSave_Click"
                                    OnClientClick="return ValidatePayment();" />
                            </div>
                        </div>
                        <div>
                            <%-- <div class="fltl pad_5">
                                <img src="../images/printer.png" /><a href="#">
                                    <asp:LinkButton ID="btnPrint" runat="server" OnClientClick="return printGrid()" Text="<%$ Resources:Resource, PRINT_RPT%>"></asp:LinkButton></a></div>--%>
                            <%--<div class="fltl" style="margin-left: 121px;">
                            <asp:Button ID="btnSave" runat="server" CssClass="box_s" Text="Save" OnClick="btnSave_Click"
                                OnClientClick="return ValidatePayment();" /></div>
                        <div class=" clear pad_5">
                        </div>--%>
                        </div>
                    </div>
                </div>
                <div id="divNoPendingBills" runat="server" visible="false" class="billcustomert">
                    <h3>
                        <asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:Resource, NO_BILLS_PENDING%>"></asp:Literal>
                    </h3>
                </div>
                <asp:ModalPopupExtender ID="mpeCustomerNA" runat="server" TargetControlID="hdnCancelCNA"
                    PopupControlID="pnlCustomerNA" BackgroundCssClass="popbg" CancelControlID="btnBPOK">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hdnCancelCNA" runat="server"></asp:HiddenField>
                <asp:Panel runat="server" ID="pnlCustomerNA" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, CUSTOMER_NA%>"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnBPOK" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="btn_ok"
                                Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="PanelAlert"
                    TargetControlID="btnAlertDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnAlertOk">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnAlertDeActivate" runat="server" Text="Button" CssClass="popbtn" />
                <asp:Panel ID="PanelAlert" runat="server" CssClass="modalPopup" class="poppnl" Style="display: none;">
                    <div class="popheader popheaderlblred" id="pop" runat="server">
                        <asp:Label ID="lblAlertMsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnAlertOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeNotInScope" runat="server" TargetControlID="hdnNotInScope"
                    PopupControlID="pnlNotInScope" BackgroundCssClass="popbg" CancelControlID="btnNotInScope">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hdnNotInScope" runat="server"></asp:HiddenField>
                <asp:Panel runat="server" ID="pnlNotInScope" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblNotInScopeMsg" runat="server" Text="<%$ Resources: Resource,BILL_ADJST_NA_DIR_CST %>"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnNotInScope" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="btn_ok" Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <uc1:Authentication ID="ucAuthentication" runat="server" />
    </div>
    <asp:HiddenField ID="hfDecimals" runat="server" Value="0" />
    <asp:HiddenField ID="hfBillNo" runat="server" Value="0" />
    <asp:Literal ID="litMeterDial" runat="server" Visible="false"></asp:Literal>
    <asp:Literal ID="litBillType" runat="server" Text="0" Visible="false"></asp:Literal>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        //        $(document).keydown(function (e) {
        //            // ESCAPE key pressed 
        //            if (e.keyCode == 27) {
        //                $(".rnkland").fadeOut("slow");
        //                $(".mpeConfirmDelete").fadeOut("slow");
        //                document.getElementById("overlay").style.display = "none";
        //            }
        //        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        //        function pageLoad() {
        //            $get("<%=txtAccountNo.ClientID%>").focus();
        //        };


        $(document).ready(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
            //AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);
            DatePicker($('#<%=txtPaidDate.ClientID %>'), $('#<%=imgMeterChangedDate.ClientID %>'));

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
            //AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);
            DatePicker($('#<%=txtPaidDate.ClientID %>'), $('#<%=imgMeterChangedDate.ClientID %>'));
        });
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../Javascript/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../JavaScript/CommonValidations.js"></script>
    <script src="../JavaScript/PageValidations/CustomerBillPayment.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
</asp:Content>
