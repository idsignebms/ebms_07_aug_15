﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Xml;
using System.Data;
using Resources;
using System.Configuration;

namespace UMS_Nigeria.HTML
{
    public partial class CustomerBillPayment : System.Web.UI.Page
    {
        #region Members
        CustomerDetailsBAL _ObjCustomerDetailsBAL = new CustomerDetailsBAL();
        MastersBAL _ObjMastersBAL = new MastersBAL();
        ReportsBal objReportsBal = new ReportsBal();
        iDsignBAL _ObjIdsignBal = new iDsignBAL();
        ConsumerBal objConsumerBal = new ConsumerBal();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        public string Key = "Customer Bill Payments";

        #endregion

        #region Methods

        private void BindUserDetails()
        {
            CustomerDetailsBe objCustomerDetailsBe = new CustomerDetailsBe();
            XmlDocument xml = new XmlDocument();

            objCustomerDetailsBe.AccountNo = txtAccountNo.Text;
            xml = _ObjCustomerDetailsBAL.GetBillDetails(objCustomerDetailsBe, ReturnType.Bulk);
            objCustomerDetailsBe = _ObjIdsignBal.DeserializeFromXml<CustomerDetailsBe>(xml);

            if (objCustomerDetailsBe != null)
            {
                lblAccountno.Text = objCustomerDetailsBe.AccountNo;
                lblAccNoAndGlobalAccNo.Text = objCustomerDetailsBe.AccNoAndGlobalAccNo;
                lblServiceAddress.Text = objCustomerDetailsBe.ServiceAddress;
                lblName.Text = objCustomerDetailsBe.Name;
                lblBookGroup.Text = objCustomerDetailsBe.BookGroup;
                lblBookName.Text = objCustomerDetailsBe.BookName;
                lblTariff.Text = objCustomerDetailsBe.Tariff;
                lblTotalDueAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(objCustomerDetailsBe.OutStandingAmount.ToString()), 2, Constants.MILLION_Format).ToString();
                lblOutstandingBottom.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(objCustomerDetailsBe.OutStandingAmount.ToString()), 2, Constants.MILLION_Format).ToString();
                lblLastPaidAmount.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBe.LastPaidAmount, 2, Constants.MILLION_Format).ToString();
                lblbLastPaidDate.Text = objCustomerDetailsBe.LastPaymentDate;
                lblOldAccountNo.Text = objCustomerDetailsBe.OldAccountNo;

                litMeterDial.Text = Convert.ToString(objCustomerDetailsBe.MeterDials);
                lblBillNo.Text = hfBillNo.Value = objCustomerDetailsBe.BillNo;
                lblTotalPendingBills.Text = objCustomerDetailsBe.TotalRecords.ToString();
                lblBillMonth.Text = objCustomerDetailsBe.BillingMonthName + " " + objCustomerDetailsBe.BillYear;
                lblBillDate.Text = objCustomerDetailsBe.LastBillGeneratedDate;

                if (objCustomerDetailsBe.ReadType == "R")
                {
                    lblPresentReading.Text = _ObjCommonMethods.AppendZeros(Convert.ToInt32(objCustomerDetailsBe.PresentReading).ToString(), objCustomerDetailsBe.MeterDials);
                    lblPreviousReading.Text = _ObjCommonMethods.AppendZeros(Convert.ToInt32(objCustomerDetailsBe.PreviousReading).ToString(), objCustomerDetailsBe.MeterDials);
                }
                else
                {
                    lblPresentReading.Text = "NA";
                    lblPreviousReading.Text = "NA";
                }

                if (objCustomerDetailsBe.ActiveStatusId == 1)
                {
                    lblReadType.Text = objCustomerDetailsBe.ReadType;
                    lblConsumption.Text = Math.Round(objCustomerDetailsBe.Consumption, 0).ToString();
                }
                else
                {
                    lblReadType.Text = "NA";
                    lblConsumption.Text = "NA";
                }

                lblNetEnergyCharges.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBe.NetEnergyCharges, 2, Constants.MILLION_Format).ToString();
                lblFixedCharges.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBe.NetFixedCharges, 2, Constants.MILLION_Format).ToString();
                lblTotalAmt.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBe.TotalBillAmount, 2, Constants.MILLION_Format).ToString();
                lblTax.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBe.Vat, 2, Constants.MILLION_Format).ToString();
                lblTotalBillwithVAT.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBe.TotalBillAmountWithTax, 2, Constants.MILLION_Format).ToString();
                lblNetArrears.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBe.NetArrears, 2, Constants.MILLION_Format).ToString();
                lblDueAmount.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBe.TotalBillAmountWithArrears, 2, Constants.MILLION_Format).ToString();
                lblBillTariff.Text = objCustomerDetailsBe.ClassName;
                lblCustomerType.Text = objCustomerDetailsBe.CustomerType;
                divPendingBills.Visible = true;
                divNoPendingBills.Visible = false;
            }
            else
            {
                //mpeCustomerNA.Show();
                divPendingBills.Visible = false;
                divNoPendingBills.Visible = true;
                ClearFields();
            }
            txtAmount.Text = string.Empty;
            txtPaidDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }

        private void InsertPaymentDetails()
        {
            MastersBE objMastersBE = new MastersBE();
            try
            {
                objMastersBE.AccountNo = lblAccountno.Text;
                //objMastersBE.ReceiptNo = 100.ToString();
                objMastersBE.PaymentModeId = 1;//Cash-- need to make iput from interface
                objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.BillNo = lblBillNo.Text;
                objMastersBE.PaidAmount = decimal.Parse(txtAmount.Text.Replace(",", string.Empty));
                objMastersBE.PaymentRecievedDate = _ObjCommonMethods.Convert_MMDDYY(txtPaidDate.Text);
                objMastersBE.PaymentType = (int)PaymentType.CustomerPayment;
                objMastersBE.FunctionId = (int)EnumApprovalFunctions.PaymentEntry;
                objMastersBE.PaymentFromId = (int)PaymentFrom.CustomerWiseBillPayment;

                if (!string.IsNullOrEmpty(Session[UMS_Resource.SESSION_USER_BUID].ToString()))
                    objMastersBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    objMastersBE.BU_ID = string.Empty;

                if (_ObjCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.PaymentEntry, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString()))
                {
                    objMastersBE.ApprovalStatusId = (int)EnumApprovalStatus.Approved;
                    objMastersBE.IsFinalApproval = true;
                    //lblAlertMsg.Text = Resource.MODIFICATIONS_UPDATED;
                    lblAlertMsg.Text = Resource.PAYMENT_RECEIVED_SUCCESS;
                    objMastersBE = _ObjIdsignBal.DeserializeFromXml<MastersBE>(_ObjMastersBAL.PaymentEntry(objMastersBE, Statement.Insert));
                    SendMailAndMsg();
                }
                else
                {
                    objMastersBE.ApprovalStatusId = (int)EnumApprovalStatus.Process;
                    lblAlertMsg.Text = Resource.APPROVAL_TARIFF_CHANGE;
                    objMastersBE = _ObjIdsignBal.DeserializeFromXml<MastersBE>(_ObjMastersBAL.PaymentEntry(objMastersBE, Statement.Insert));
                }

                if (objMastersBE.IsSuccess)
                {
                    pop.Attributes.Add("class", "popheader popheaderlblgreen");
                    mpeAlert.Show();

                }
                else
                    Message(UMS_Resource.PAYMENT_ENTRY_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //private MastersBE InsertPaymentDetails()
        //{
        //    MastersBE objMastersBE = new MastersBE();
        //    try
        //    {
        //        objMastersBE.AccountNo = lblAccountno.Text;
        //        //objMastersBE.ReceiptNo = 100.ToString();
        //        objMastersBE.PaymentModeId = 1;//Cash-- need to make iput from interface
        //        objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
        //        objMastersBE.BillNo = lblBillNo.Text;
        //        objMastersBE.PaidAmount = decimal.Parse(txtAmount.Text.Replace(",", string.Empty));
        //        objMastersBE.PaymentRecievedDate = _ObjCommonMethods.Convert_MMDDYY(txtPaidDate.Text);
        //        objMastersBE.PaymentType = (int)PaymentType.CustomerPayment;

        //        if (_ObjCommonMethods.IsApproval(Resources.Resource.FEATURE_PAYMENT_ENTRY))
        //        {
        //            objMastersBE.Flag = Convert.ToInt32(Resources.Resource.FLAG_TEMP);
        //            objMastersBE = _ObjIdsignBal.DeserializeFromXml<MastersBE>(_ObjMastersBAL.PaymentEntryTemporaryBAL(objMastersBE, Statement.Insert));
        //        }
        //        else
        //            objMastersBE = _ObjIdsignBal.DeserializeFromXml<MastersBE>(_ObjMastersBAL.PaymentEntry(objMastersBE, Statement.Insert));

        //        txtAccountNo.Text = lblAccountno.Text;
        //        lblAccountno.Text = lblBillNo.Text = txtAmount.Text = lblName.Text = lblTotalDueAmount.Text = lblServiceAddress.Text = string.Empty;
        //        txtPaidDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        //        divPendingBills.Visible = false;
        //        return objMastersBE;
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //    return objMastersBE;
        //}

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void ClearFields()
        {
            lblAccNoAndGlobalAccNo.Text = lblAccountno.Text = lblBillNo.Text = txtAmount.Text = lblName.Text = lblTotalDueAmount.Text = lblServiceAddress.Text = string.Empty;
            txtAccountNo.Text = lblBookGroup.Text = lblBookName.Text = lblTariff.Text = lblLastPaidAmount.Text = lblbLastPaidDate.Text = lblOldAccountNo.Text = string.Empty;
            txtPaidDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            divPendingBills.Visible = false;
        }

        private void SendMailAndMsg()
        {

            string GlobalAccountNo = lblAccountno.Text;
            string SuccessMessage = string.Empty;
            SuccessMessage = "Bill generated successfully.";
            CommunicationFeaturesBE objCommunicationFeaturesBE = new CommunicationFeaturesBE();
            objCommunicationFeaturesBE.CommunicationFeaturesID = (int)CommunicationFeatures.Payments;

            try
            {
                if (_ObjCommonMethods.IsEmailFeatureEnabled(objCommunicationFeaturesBE))
                {
                    CommunicationFeaturesBE _objMailBE = new CommunicationFeaturesBE();
                    _objMailBE.GlobalAccountNumber = Convert.ToString(GlobalAccountNo);
                    XmlDocument xml = _ObjCommonMethods.GetDetails(_objMailBE, ReturnType.Bulk);
                    _objMailBE = _ObjIdsignBal.DeserializeFromXml<CommunicationFeaturesBE>(xml);

                    if (_objMailBE.EmailId == "0")
                    {
                        SuccessMessage += "Customer not having Mail Id.";
                    }
                    else if (!string.IsNullOrEmpty(_objMailBE.EmailId))
                    {
                        if (_objMailBE.IsSuccess)
                        {
                            string amount = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(_objMailBE.PaidAmount), 2, Constants.MILLION_Format);
                            string outstandingamount = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(_objMailBE.OutStandingAmount), 2, Constants.MILLION_Format);
                            string htmlBody = string.Empty;
                            htmlBody = _ObjCommonMethods.GetMailCommTemp(CommunicationFeatures.Payments, Convert.ToString(_objMailBE.Name), string.Empty, Convert.ToString(GlobalAccountNo), Convert.ToString(_objMailBE.MeterNumber), string.Empty, Convert.ToString(outstandingamount), string.Empty, Convert.ToString(amount), Convert.ToString(_objMailBE.PaidDate), string.Empty, "Dear Customer, bill payment of =N= " + Convert.ToString(amount) + " for your BEDC acc " + Convert.ToString(GlobalAccountNo) + " has been processed successfully. <br/>Txn Id: " + Convert.ToString(_objMailBE.TransactionId) + ".");

                            _ObjCommonMethods.sendmail(htmlBody, ConfigurationManager.AppSettings["SMTPUsername"].ToString(), "Payment Received from acc " + Convert.ToString(GlobalAccountNo), _objMailBE.EmailId, string.Empty);
                            SuccessMessage += "Mail sent successfully.";
                        }
                    }
                    else
                    {
                        SuccessMessage += "Mail not sent to this customer.";
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

            try
            {
                if (_ObjCommonMethods.IsSMSFeatureEnabled(objCommunicationFeaturesBE))
                {
                    CommunicationFeaturesBE _objSmsBE = new CommunicationFeaturesBE();
                    _objSmsBE.GlobalAccountNumber = Convert.ToString(GlobalAccountNo);
                    XmlDocument xml = _ObjCommonMethods.GetDetails(_objSmsBE, ReturnType.Bulk);
                    _objSmsBE = _ObjIdsignBal.DeserializeFromXml<CommunicationFeaturesBE>(xml);

                    if (_objSmsBE.MobileNo == "0")
                    {
                        SuccessMessage += "Customer not having Mobile No.";
                    }
                    else if (!string.IsNullOrEmpty(_objSmsBE.MobileNo) && _objSmsBE.MobileNo.Length == 10)
                    {
                        if (_objSmsBE.IsSuccess)
                        {
                            string amount = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(_objSmsBE.PaidAmount), 2, Constants.MILLION_Format);
                            string Messsage = string.Empty;
                            Messsage = _ObjCommonMethods.GetSMSCommTemp(CommunicationFeatures.Payments, Convert.ToString(GlobalAccountNo), string.Empty, Convert.ToString(amount), string.Empty, Convert.ToString(_objSmsBE.TransactionId), string.Empty);

                            _ObjCommonMethods.sendSMS(_objSmsBE.MobileNo, Messsage, ConfigurationManager.AppSettings["SMTPUsername"].ToString());
                            SuccessMessage += "Message sent successfully.";
                        }
                    }
                    else
                    {
                        SuccessMessage += "Message not sent to this customer.";
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString[UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO]))
                {
                    string AccountNo = _ObjIdsignBal.Decrypt(Request.QueryString[UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO]).ToString();
                    txtAccountNo.Text = AccountNo;
                    BindUserDetails();
                }
                txtPaidDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            InsertPaymentDetails();
            ClearFields();
        }

        protected void lblSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADVANCE_SEARCH_PAGE + "?" + UMS_Resource.FROM_PAGE + "=" + _ObjIdsignBal.Encrypt(Constants.CustomerBillPayment), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            RptCustomerLedgerBe objLedgerBe = new RptCustomerLedgerBe();
            objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
            objLedgerBe.Flag = 1;

            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                objLedgerBe.BUID = string.Empty;

            objLedgerBe = _ObjIdsignBal.DeserializeFromXml<RptCustomerLedgerBe>(objReportsBal.GetLedger(objLedgerBe, ReturnType.Single));

            if (objLedgerBe.IsSuccess)
            {
                if (objLedgerBe.IsCustExistsInOtherBU)
                {
                    lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                    mpeAlert.Show();
                    ClearFields();
                }
                else if (objLedgerBe.CustomerTypeId == 3)//check the customer is prepaid or not
                {
                    lblAlertMsg.Text = "Customer is Prepaid customer.";
                    mpeAlert.Show();
                    ClearFields();
                }
                else
                {
                    BindUserDetails();
                }
            }
            else
            {
                _ObjCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.CUSTOMER_NOT_FOUND_MSG, pnlMessage, lblMessage);
                ClearFields();
            }
        }

        protected void lbtnTotalBillsCount_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("CustomerPendingBills.aspx?" + UMS_Resource.QSTR_ACC_NO + "=" + _ObjIdsignBal.Encrypt(lblAccountno.Text), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //protected void dtlBills_ItemDataBound(object sender, DataListItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        Label lblPreviousReading = (Label)e.Item.FindControl("lblPreviousReading");
        //        Literal litCustomerBillID = (Literal)e.Item.FindControl("litCustomerBillID");
        //        Label lblPresentReading = (Label)e.Item.FindControl("lblPresentReading");
        //        Label lblNetEnergyCharges = (Label)e.Item.FindControl("lblNetEnergyCharges");
        //        Label lblTotalAmt = (Label)e.Item.FindControl("lblTotalAmt");
        //        Label lblTax = (Label)e.Item.FindControl("lblTax");
        //        Label lblGT = (Label)e.Item.FindControl("lblGT");
        //        Label lblPaidAmount = (Label)e.Item.FindControl("lblPaidAmount");
        //        Label lblDueAmount = (Label)e.Item.FindControl("lblDueAmount");
        //        Label lblConsumption = (Label)e.Item.FindControl("lblConsumption");
        //        Literal litBillTypes = (Literal)e.Item.FindControl("litBillTypes");
        //        int BillType = Convert.ToInt32(litBillTypes.Text);

        //        if (lblPresentReading.Text == "0" && lblPreviousReading.Text == "0")//if (BillType == (int)ReadCode.Estimate || BillType == (int)ReadCode.Direct)
        //        {
        //            lblPresentReading.Text = lblPreviousReading.Text = ((ReadCode)BillType).ToString();//Resource.ESTIMATED_BILL_MESSAGE;
        //        }
        //        else
        //        {
        //            lblPresentReading.Text = _ObjCommonMethods.AppendZeros(Convert.ToInt32(lblPresentReading.Text).ToString(), Convert.ToInt32(litMeterDial.Text));
        //            lblPreviousReading.Text = _ObjCommonMethods.AppendZeros(Convert.ToInt32(lblPreviousReading.Text).ToString(), Convert.ToInt32(litMeterDial.Text));
        //        }

        //        lblNetEnergyCharges.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblNetEnergyCharges.Text), 2, Constants.MILLION_Format).ToString();
        //        lblTotalAmt.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblTotalAmt.Text), 2, Constants.MILLION_Format).ToString();
        //        lblTax.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblTax.Text), 2, Constants.MILLION_Format).ToString();
        //        lblGT.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblGT.Text), 2, Constants.MILLION_Format).ToString();
        //        lblPaidAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblPaidAmount.Text), 2, Constants.MILLION_Format).ToString();
        //        lblDueAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblDueAmount.Text), 2, Constants.MILLION_Format).ToString();

        //        Repeater rptrAdditionalCharges = (Repeater)e.Item.FindControl("rptrAdditionalCharges");
        //        BindAdditionalCharges(rptrAdditionalCharges, txtAccountNo.Text, Convert.ToInt32(litCustomerBillID.Text));
        //    }
        //}

        //protected void rptrAdditionalCharges_ItemDataBound(object sender, RepeaterItemEventArgs e) //Raja-ID065
        //{
        //    try
        //    {
        //        Label lblAmount = (Label)e.Item.FindControl("lblAmount");
        //        Label lblChargeName = (Label)e.Item.FindControl("lblChargeName");
        //        lblAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblAmount.Text), 2, Constants.MILLION_Format).ToString();
        //        if (lblChargeName.Text == Constants.MMC)
        //        {
        //            lblChargeName.Text = "MMC";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //private void BindAdditionalCharges(Repeater rptr, string AccountNo, int CustomerBillID) //Raja-ID065
        //{
        //    CustomerDetailsBe objCustomerDetailsBe = new CustomerDetailsBe();
        //    CustomerDetailsBillsListBe objCustomerDetailsBillsListBe = new CustomerDetailsBillsListBe();
        //    objCustomerDetailsBe.AccountNo = AccountNo;
        //    objCustomerDetailsBe.CustomerBillID = CustomerBillID;
        //    XmlDocument xml = new XmlDocument();
        //    xml = _ObjCustomerDetailsBAL.GetBillDetails(objCustomerDetailsBe, ReturnType.Get);
        //    objCustomerDetailsBillsListBe = _ObjIdsignBal.DeserializeFromXml<CustomerDetailsBillsListBe>(xml);
        //    rptr.DataSource = objCustomerDetailsBillsListBe.items;
        //    rptr.DataBind();
        //}

        #endregion
    }
}