﻿<%@ Page Title=":: Customer Pending Bills ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="CustomerPendingBills.aspx.cs"
    Inherits="UMS_Nigeria.HTML.CustomerPendingBills" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControls/Authentication.ascx" TagName="Authentication" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out_bor_Payment">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upPaymentEntry" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div class="text_total">
                    <div class="text-heading" style="float: left;">
                        <asp:Literal ID="litAddState" runat="server" Text="Customer Pending Bills"></asp:Literal>
                    </div>
                    <%--<div class="previous">
                        <asp:LinkButton ID="lbtnBack" runat="server" OnClick="lbtnBack_Click" Style="font-size: 13px;"
                            class="back-btn">
                                    << Back To Customer Bill Payments</asp:LinkButton>
                    </div>--%>
                </div>
                <div class="clear">
                </div>
                <div class="dot-line">
                </div>
                <div class="previous">
                    <asp:LinkButton ID="LinkButton1" runat="server" Text='<%$ Resources:Resource, BACK %>'
                        OnClick="lbtnBack_Click"></asp:LinkButton>
                </div>
                <div class="clr">
                </div>
                <br />
                <div class="billsearch_Payments">
                    <div class="billcustomert_Payments">
                        <div class="custmrdtails">
                            <div class="text_total" style="margin-bottom: 15px;">
                                <div class="text-heading">
                                    <asp:Literal ID="Literal21" runat="server" Text="Customer Details"></asp:Literal>
                                </div>
                            </div>
                            <div class="out-bor_cTwo" style="width: 87%;">
                                <div class="inner-box">
                                    <div class="inner-box1_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal32" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_b">
                                        <div class="customerDetailstext">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box3_b">
                                        <div class="text_cusa_a">
                                            <asp:Label runat="server" ID="lblAccNoAndGlobalAccNo"></asp:Label>
                                            <asp:Label runat="server" Visible="false" ID="lblAccountno"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box4_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal33" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box5_b">
                                        <div class="customerDetailstext">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box6_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblOldAccountNo" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="litOutstandingAmount" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_b">
                                        <div class="customerDetailstext">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box3_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblName" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box4_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box5_b">
                                        <div class="customerDetailstext">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box6_b">
                                        <div class="text_cusa_a">
                                            <asp:Label runat="server" ID="lblServiceAddress"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal35" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_b">
                                        <div class="customerDetailstext">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box3_b">
                                        <div class="text_cusa_a">
                                            <asp:Label runat="server" ID="lblBookGroup"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box4_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal36" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box5_b">
                                        <div class="customerDetailstext">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box6_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblBookName" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal37" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_b">
                                        <div class="customerDetailstext">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box3_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblTariff" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box4_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal38" runat="server" Text="<%$ Resources:Resource, OUT_STANDING_AMT%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box5_b">
                                        <div class="customerDetailstext">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box6_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblTotalDueAmount" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal39" runat="server" Text="<%$ Resources:Resource, LAST_AMOUNTPAID%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_b">
                                        <div class="customerDetailstext">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box3_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblLastPaidAmount" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box4_b">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal40" runat="server" Text="<%$ Resources:Resource, LAST_PAID_DATE%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box5_b">
                                        <div class="customerDetailstext">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box6_b">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblbLastPaidDate" runat="server" Text="--"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clear pad_10">
                        </div>
                        <div id="divPendingBills" runat="server" visible="false" class="billcustomert" style="width: 67%;">
                            <div class="text_total">
                                <div class="text-heading" style="float: left;">
                                    <asp:Literal ID="ltlLastBill" runat="server" Text="Pending Bills"></asp:Literal>
                                </div>
                            </div>
                            <div id="divPrint" runat="server">
                                <asp:Repeater ID="rptrPendingBills" runat="server" OnItemDataBound="rptrPendingBills_ItemDataBound">
                                    <ItemTemplate>
                                        <div class="prev_total routwise" style="width: 800px;">
                                            <ul>
                                                <div style="width: 777px;">
                                                    <li class="prevleft fltl">
                                                        <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:Resource, BILLNUMBER%>"></asp:Literal><br />
                                                        <asp:Label runat="server" ID="lblBillNo" Text='<%# Eval("BillNo") %>'></asp:Label>
                                                    </li>
                                                    <li class=" fltl"></li>
                                                    <li class=" fltl" style=" width: 150px;">
                                                        <asp:Literal runat="server" ID="litCustomerBillID" Visible="false" Text='<%# Eval("CustomerBillID") %>'></asp:Literal>
                                                    </li>
                                                </div>
                                                <div style="clear: none; float: left; width: 360px;">
                                                    <li class="prevleft fltl">
                                                        <asp:Literal ID="Literal25" runat="server" Text="Bill Type"></asp:Literal></li><li
                                                            class=" fltl">:</li>
                                                    <li class=" fltl" style=" width: 150px;">
                                                        <asp:Label runat="server" ID="lblReadType" Text='<%# Eval("ReadType") %>'></asp:Label>
                                                    </li>
                                                </div>
                                                <div style="clear: none; float: left; width: 395px; background: none repeat scroll 0 0 #fff;">
                                                    <li class="prevleft fltl" style="width: 200px;">
                                                        <asp:Literal ID="Literal31" runat="server" Text="<%$ Resources:Resource, ENERGYCHARGES%>"></asp:Literal></li><li
                                                            class=" fltl">:</li>
                                                    <li class=" fltl" style=" width: 150px;">
                                                        <asp:Label runat="server" ID="lblNetEnergyCharges" Text='<%# Eval("NetEnergyCharges") %>'></asp:Label>
                                                    </li>
                                                </div>
                                                <div style="clear: none; float: left; width: 360px;">
                                                    <li class="prevleft fltl">
                                                        <asp:Literal ID="Literal26" runat="server" Text="Billing Month"></asp:Literal></li><li
                                                            class=" fltl">:</li>
                                                    <li class=" fltl" style=" width: 150px;">
                                                        <asp:Label runat="server" ID="lblBillMonth" Text='<%# Eval("BillingMonthName") + " " + Eval("BillYear") %>'></asp:Label>
                                                    </li>
                                                </div>
                                                <div style="clear: none; float: left; width: 395px; background: none repeat scroll 0 0 #fff;">
                                                    <li class="prevleft fltl" style="width: 200px;">
                                                        <asp:Literal ID="Literal28" runat="server" Text="Fixed Charges"></asp:Literal></li><li
                                                            class=" fltl">:</li>
                                                    <li class=" fltl" style=" width: 150px;">
                                                        <asp:Label runat="server" ID="lblFixedCharges" Text='<%# Eval("NetFixedCharges") %>'></asp:Label>
                                                    </li>
                                                </div>
                                                <div style="clear: none; float: left; width: 360px;">
                                                    <li class="prevleft fltl">
                                                        <asp:Literal ID="Literal27" runat="server" Text="Bill Generation Date"></asp:Literal></li><li
                                                            class=" fltl">:</li>
                                                    <li class=" fltl" style=" width: 150px;">
                                                        <asp:Label runat="server" ID="lblBillDate" Text='<%# Eval("LastBillGeneratedDate") %>'></asp:Label>
                                                    </li>
                                                </div>
                                                <div style="clear: none; float: left; width: 395px; background: none repeat scroll 0 0 #fff;">
                                                    <li class="prevleft fltl" style="width: 200px;">
                                                        <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:Resource, BILL_AMT%>"></asp:Literal></li>
                                                    <li class=" fltl">:</li>
                                                    <li class=" fltl" style=" width: 150px;">
                                                        <asp:Label runat="server" ID="lblTotalAmt" Text='<%# Eval("TotalBillAmount") %>'></asp:Label>
                                                    </li>
                                                </div>
                                                <div style="clear: none; float: left; width: 360px;">
                                                    <li class="prevleft fltl">
                                                        <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, PREVIOUS_READING%>"></asp:Literal></li><li
                                                            class=" fltl">:</li>
                                                    <li class=" fltl" style=" width: 150px;">
                                                        <asp:Label runat="server" ID="lblPreviousReading" Text='<%# Eval("PreviousReading") %>'></asp:Label>
                                                    </li>
                                                </div>
                                                <div style="clear: none; float: left; width: 395px; background: none repeat scroll 0 0 #fff;">
                                                    <li class="prevleft fltl" style="width: 200px;">
                                                        <asp:Literal ID="Literal22" runat="server" Text="<%$ Resources:Resource, TAX%>"></asp:Literal></li><li
                                                            class=" fltl">:</li>
                                                    <li class=" fltl" style=" width: 150px;">
                                                        <asp:Label runat="server" ID="lblTax" Text='<%# Eval("Vat") %>'></asp:Label>
                                                    </li>
                                                </div>
                                                <div style="clear: none; float: left; width: 360px;">
                                                    <li class="prevleft fltl">
                                                        <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, CURRENT_READING%>"></asp:Literal></li><li
                                                            class=" fltl">:</li>
                                                    <li class=" fltl" style=" width: 150px;">
                                                        <asp:Label runat="server" ID="lblPresentReading" Text='<%# Eval("PresentReading") %>'></asp:Label>
                                                        <asp:Label ID="litMeterDial" runat="server" Visible="false" Text='<%# Eval("MeterDials") %>'></asp:Label></li>
                                                </div>
                                                <div style="clear: none; float: left; width: 395px; background: none repeat scroll 0 0 #fff;">
                                                    <li class="prevleft fltl" style="width: 200px;">
                                                        <asp:Literal ID="Literal5" runat="server" Text="Total Bill Amount With VAT"></asp:Literal></li><li
                                                            class=" fltl">:</li>
                                                    <li class=" fltl" style=" width: 150px;">
                                                        <asp:Label runat="server" ID="lblTotalBillwithVAT" Text='<%# Eval("TotalBillAmountWithTax") %>'></asp:Label>
                                                    </li>
                                                </div>
                                                <div style="clear: none; float: left; width: 360px;">
                                                    <li class="prevleft fltl">
                                                        <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:Resource, CONSUMPTION%>"></asp:Literal>
                                                    </li>
                                                    <li class=" fltl">:</li><li class=" fltl" style=" width: 150px;">
                                                        <asp:Label runat="server" ID="lblConsumption" Text='<%# Eval("Consumption") %>'></asp:Label></li>
                                                </div>
                                                <div style="clear: none; float: left; width: 395px; background: none repeat scroll 0 0 #fff;">
                                                    <li class="prevleft fltl" style="width: 200px;">
                                                        <asp:Literal ID="Literal6" runat="server" Text="Net Arrears"></asp:Literal></li><li
                                                            class=" fltl">:</li>
                                                    <li class=" fltl" style=" width: 150px;">
                                                        <asp:Label runat="server" ID="lblNetArrears" Text='<%# Eval("NetArrears") %>'></asp:Label>
                                                    </li>
                                                </div>
                                                <div style="background: none repeat scroll 0 0 #fff; clear: none; float: left; width: 360px;">
                                                    <li class="prevleft fltl" style="color: green; font-size: 14px; font-weight: bold;">
                                                        <asp:Literal ID="Literal8" runat="server" Text="Paid Amount"></asp:Literal></li><li
                                                            class=" fltl">:</li>
                                                    <li class=" fltl" style="color: green; font-size: 14px; font-weight: bold; width: 150px;">
                                                        <asp:Label runat="server" ID="lblBillAmountPaid" Text='<%# Eval("LastPaidAmount") %>'></asp:Label>
                                                    </li>
                                                </div>
                                                <div style="background: none repeat scroll 0 0 #fff; clear: none; float: left; width: 395px;">
                                                    <li class="prevleft fltl" style="color: green; font-size: 14px; font-weight: bold;
                                                        width: 200px;">
                                                        <asp:Literal ID="Literal7" runat="server" Text="Total Bill Amount with Arrears"></asp:Literal></li><li
                                                            class=" fltl">:</li>
                                                    <li class=" fltl" style="color: green; font-size: 14px; font-weight: bold; width: 150px;">
                                                        <asp:Label runat="server" ID="lblDueAmount" Text='<%# Eval("TotalBillAmountWithArrears") %>'></asp:Label>
                                                    </li>
                                                </div>
                                            </ul>
                                        </div>
                                        <br />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                            <div class=" clear pad_10">
                            </div>
                            <div class="consumer_feild fltl">
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                    <div id="divNoPendingBills" runat="server" visible="false" class="billcustomert">
                        <h3>
                            <asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:Resource, NO_BILLS_PENDING%>"></asp:Literal>
                        </h3>
                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
      
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">

        $(document).ready(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../Javascript/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../JavaScript/CommonValidations.js"></script>
    <script src="../JavaScript/PageValidations/CustomerBillPayment.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
</asp:Content>
