﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Xml;
using System.Data;
using Resources;

namespace UMS_Nigeria.HTML
{
    public partial class CustomerPendingBills : System.Web.UI.Page
    {
        #region Members
        CustomerDetailsBAL _ObjCustomerDetailsBAL = new CustomerDetailsBAL();
        MastersBAL _ObjMastersBAL = new MastersBAL();
        ReportsBal objReportsBal = new ReportsBal();
        iDsignBAL _ObjIdsignBal = new iDsignBAL();
        ConsumerBal objConsumerBal = new ConsumerBal();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        public string Key = "Customer Bill Payments";

        #endregion

        #region Methods

        private void BindUserDetails(string AccountNo)
        {
            try
            {
                CustomerDetailsBe objCustomerDetailsBe = new CustomerDetailsBe();
                CustomerDetailsListBe objCustomerDetailsListBe = new CustomerDetailsListBe();
                CustomerDetailsBillsListBe objCustomerDetailsBillsListBe = new CustomerDetailsBillsListBe();
                XmlDocument xml = new XmlDocument();

                objCustomerDetailsBe.AccountNo = AccountNo;
                xml = _ObjCustomerDetailsBAL.GetBillDetails(objCustomerDetailsBe, ReturnType.Retrieve);
                objCustomerDetailsListBe = _ObjIdsignBal.DeserializeFromXml<CustomerDetailsListBe>(xml);
                objCustomerDetailsBillsListBe = _ObjIdsignBal.DeserializeFromXml<CustomerDetailsBillsListBe>(xml);

                if (objCustomerDetailsListBe.items.Count > 0)
                {
                    objCustomerDetailsBe = objCustomerDetailsListBe.items[0];
                    lblAccountno.Text = objCustomerDetailsBe.AccountNo;
                    lblAccNoAndGlobalAccNo.Text = objCustomerDetailsBe.AccNoAndGlobalAccNo;
                    lblServiceAddress.Text = objCustomerDetailsBe.ServiceAddress;
                    lblName.Text = objCustomerDetailsBe.Name;
                    lblBookGroup.Text = objCustomerDetailsBe.BookGroup;
                    lblBookName.Text = objCustomerDetailsBe.BookName;
                    lblTariff.Text = objCustomerDetailsBe.Tariff;
                    lblTotalDueAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(objCustomerDetailsBe.OutStandingAmount.ToString()), 2, Constants.MILLION_Format).ToString();
                    lblLastPaidAmount.Text = _ObjCommonMethods.GetCurrencyFormat(objCustomerDetailsBe.LastPaidAmount, 2, Constants.MILLION_Format).ToString();
                    lblbLastPaidDate.Text = objCustomerDetailsBe.LastPaymentDate;
                    lblOldAccountNo.Text = objCustomerDetailsBe.OldAccountNo;

                    if (objCustomerDetailsBillsListBe.items.Count > 0)
                    {
                        rptrPendingBills.DataSource = objCustomerDetailsBillsListBe.items;
                        rptrPendingBills.DataBind();
                        divPendingBills.Visible = true;
                        divNoPendingBills.Visible = false;
                    }
                    else
                    {
                        divPendingBills.Visible = false;
                        divNoPendingBills.Visible = true;
                    }
                }
                else
                {
                    Message("Customer Data not found.", UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString[UMS_Resource.QSTR_ACC_NO]))
                {
                    string AccountNo = _ObjIdsignBal.Decrypt(Request.QueryString[UMS_Resource.QSTR_ACC_NO]).ToString();
                    BindUserDetails(AccountNo);
                }
            }
        }

        protected void rptrPendingBills_ItemDataBound(object sender, RepeaterItemEventArgs e) //Raja-ID065
        {
            try
            {
                Label lblNetEnergyCharges = (Label)e.Item.FindControl("lblNetEnergyCharges");
                Label lblFixedCharges = (Label)e.Item.FindControl("lblFixedCharges");
                Label lblTotalAmt = (Label)e.Item.FindControl("lblTotalAmt");
                Label lblTax = (Label)e.Item.FindControl("lblTax");
                Label lblTotalBillwithVAT = (Label)e.Item.FindControl("lblTotalBillwithVAT");
                Label lblNetArrears = (Label)e.Item.FindControl("lblNetArrears");
                Label lblBillAmountPaid = (Label)e.Item.FindControl("lblBillAmountPaid");
                Label lblDueAmount = (Label)e.Item.FindControl("lblDueAmount");
                Label lblPresentReading = (Label)e.Item.FindControl("lblPresentReading");
                Label lblPreviousReading = (Label)e.Item.FindControl("lblPreviousReading");
                Label litMeterDial = (Label)e.Item.FindControl("litMeterDial");
                Label lblConsumption = (Label)e.Item.FindControl("lblConsumption");

                if (string.IsNullOrEmpty(litMeterDial.Text))
                    litMeterDial.Text = "0";

                if (!string.IsNullOrEmpty(lblNetEnergyCharges.Text))
                    lblNetEnergyCharges.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblNetEnergyCharges.Text), 2, Constants.MILLION_Format).ToString();
                else
                    lblNetEnergyCharges.Text = "0.00";

                if (!string.IsNullOrEmpty(lblFixedCharges.Text))
                    lblFixedCharges.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblFixedCharges.Text), 2, Constants.MILLION_Format).ToString();
                else
                    lblFixedCharges.Text = "0.00";

                if (!string.IsNullOrEmpty(lblTotalAmt.Text))
                    lblTotalAmt.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblTotalAmt.Text), 2, Constants.MILLION_Format).ToString();
                else
                    lblTotalAmt.Text = "0.00";

                if (!string.IsNullOrEmpty(lblTax.Text))
                    lblTax.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblTax.Text), 2, Constants.MILLION_Format).ToString();
                else
                    lblTax.Text = "0.00";

                if (!string.IsNullOrEmpty(lblTotalBillwithVAT.Text))
                    lblTotalBillwithVAT.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblTotalBillwithVAT.Text), 2, Constants.MILLION_Format).ToString();
                else
                    lblTotalBillwithVAT.Text = "0.00";

                if (!string.IsNullOrEmpty(lblNetArrears.Text))
                    lblNetArrears.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblNetArrears.Text), 2, Constants.MILLION_Format).ToString();
                else
                    lblNetArrears.Text = "0.00";

                if (!string.IsNullOrEmpty(lblBillAmountPaid.Text))
                    lblBillAmountPaid.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblBillAmountPaid.Text), 2, Constants.MILLION_Format).ToString();
                else
                    lblBillAmountPaid.Text = "0.00";

                if (!string.IsNullOrEmpty(lblDueAmount.Text))
                    lblDueAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblDueAmount.Text), 2, Constants.MILLION_Format).ToString();
                else
                    lblDueAmount.Text = "0.00";

                if (!string.IsNullOrEmpty(lblPresentReading.Text))
                    lblPresentReading.Text = _ObjCommonMethods.AppendZeros(lblPresentReading.Text, Convert.ToInt32(litMeterDial.Text));
                else
                    lblPresentReading.Text = "0";

                if (!string.IsNullOrEmpty(lblPreviousReading.Text))
                    lblPreviousReading.Text = _ObjCommonMethods.AppendZeros(lblPreviousReading.Text, Convert.ToInt32(litMeterDial.Text));
                else
                    lblPreviousReading.Text = "0";

                if (!string.IsNullOrEmpty(lblConsumption.Text))
                    lblConsumption.Text = Math.Round(Convert.ToDecimal(lblConsumption.Text), 0).ToString();
                else
                    lblConsumption.Text = "0";
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lbtnBack_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("CustomerBillPayment.aspx?" + UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO + "=" + _ObjIdsignBal.Encrypt(lblAccountno.Text), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion
    }
}