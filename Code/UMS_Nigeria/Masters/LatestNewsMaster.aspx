﻿<%@ Page Title="::Latest News::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="LatestNewsMaster.aspx.cs" Inherits="UMS_Nigeria.Masters.LatestNewsMaster" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdatePanel ID="upAddCountries" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            Latest News
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner" style="margin-top: 8px;">
                                <label for="name">
                                    <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$Resources:Resource ,BUSINESS_UNIT %>"></asp:Literal>
                                    <span class="span_star">*</span></label><div class="space">
                                    </div>
                                <asp:CheckBox Style="margin-left: 2px;" TabIndex="1" ID="allCheck" onclick="return BUCheckAll();"
                                    runat="server" Text="All" />
                                <asp:CheckBoxList ID="chkBusinessUnit" Enabled="false" onclick="return BUUnCheckAll();"
                                    RepeatDirection="Horizontal" RepeatColumns="3" runat="server" Width="400">
                                </asp:CheckBoxList>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="inner-box1" style="margin-top: -12px;">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litSubject" runat="server" Text="<%$Resources:Resource ,SUBJECT %>"></asp:Literal>
                                    <span class="span_star">*</span></label><div class="space">
                                    </div>
                                <asp:TextBox ID="txtSubject" CssClass="text-box" TabIndex="2" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanSubject','Subject')"
                                    MaxLength="500" placeholder=" Enter Subject" Style="width: 200px;"></asp:TextBox>
                                <span id="spanSubject" class="span_color"></span>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner" style="margin-top: 8px;">
                                <label for="name">
                                    <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:Resource ,DETAILS %>"></asp:Literal>
                                    <span class="span_star">*</span></label>
                                <div class="space">
                                </div>
                                <asp:TextBox ID="txtDetails" placeholder=" Enter Details" TabIndex="3" CssClass="text-box" runat="server" TextMode="MultiLine" Rows="5" onblur="return TextBoxBlurValidationlbl(this,'spanDetails','Details')"
                                    Style="width: 200px; height: 65px;"></asp:TextBox>
                                <span id="spanDetails" class="span_color"></span>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner" style="margin-top: 8px;">
                                <label for="name">
                                    <asp:Literal ID="Literal2" runat="server" Text="<%$Resources:Resource ,DOCUMENTS %>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:FileUpload ID="upldaAtrchment" TabIndex="4" runat="server" />
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="box_totalThree_c">
                            <asp:Button ID="btnNext" TabIndex="5" Style="margin-left: 135px;" CssClass="box_s" Text="Add"
                                runat="server" OnClientClick="return Validate();" OnClick="btnNext_Click" />
                            <asp:Button ID="btnCancel" TabIndex="6" CssClass="box_s" OnClientClick="return ClearControls();"
                                Text="Reset" runat="server" />
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="grid_boxes">
                        <div class="grid_pagingTwo_top">
                            <div class="paging_top_title">
                                <asp:Literal ID="litBuList" runat="server" Text="<%$ Resources:Resource, GRID_NOTIFICATION_LIST%>"></asp:Literal>
                            </div>
                            <div class="paging_top_rightTwo_content" id="divpaging1" runat="server">
                                <uc1:UCPagingV1 ID="UCPagingV1" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvNotificationList" runat="server" AutoGenerateColumns="False"
                            AlternatingRowStyle-CssClass="color" HeaderStyle-CssClass="grid_tb_hd" OnRowCommand="gvNotificationList_RowCommand">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no News.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:BoundField HeaderText="S.No" DataField="Srno" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField HeaderText="Subject" DataField="Subject" />
                                <asp:BoundField HeaderText="Business Unit" DataField="BusinessUnitName" />
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Literal ID="litBUNoticeID" Visible="false" runat="server" Text='<%#Eval("BUNoticeID")%>'></asp:Literal>
                                        <asp:LinkButton ID="lkbtnDeActive" runat="server" ToolTip="Delete Latest News" CommandArgument='<%#Eval("BUNoticeID") %>'
                                            CommandName="DEACTIVATENEWS"><img src="../images/delete.png" alt="DeActive" /></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:HiddenField ID="hfBUNoticeID" runat="server" />
                        <asp:HiddenField ID="hfPageSize" runat="server" />
                        <asp:HiddenField ID="hfPageNo" runat="server" />
                        <asp:HiddenField ID="hfLastPage" runat="server" />
                        <asp:HiddenField ID="hfTotalRecords" runat="server" />
                    </div>
                    <div class="grid_boxes">
                        <div id="divpaging2" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div id="rnkland">
                        <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                            TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnDeActivate" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="PanelDeActivate" runat="server" CssClass="modalPopup" Style="display: none;
                            border-color: #639B00;">
                            <div class="popheader" style="background-color: red;">
                                <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <br />
                                    <asp:Button ID="btnDeActiveOk" runat="server" Text="<%$ Resources:Resource, OK%>
                            " CssClass="ok_btn" OnClick="btnDeActiveOk_Click" />
                                    <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnNext" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <%--==============Escape button script starts==============--%>
        <script type="text/javascript">
            $(document).keydown(function (e) {
                // ESCAPE key pressed 
                if (e.keyCode == 27) {
                    $(".rnkland").fadeOut("slow");
                    $(".modalPopup").fadeOut("slow");
                    $(".modalBackground").fadeOut("slow");
                    document.getElementById("overlay").style.display = "none";
                }
            });
        </script>
        <%--==============Escape button script ends==============--%>
        <%--==============Ajax loading script starts==============--%>
        <script type="text/javascript">
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
            prm.add_beginRequest(BeginRequestHandler);
            // Raised after an asynchronous postback is finished and control has been returned to the browser.
            prm.add_endRequest(EndRequestHandler);
            function BeginRequestHandler(sender, args) {
                //Shows the modal popup - the update progress
                var popup = $find('<%= modalPopupLoading.ClientID %>');
                if (popup != null) {
                    popup.show();
                }
            }

            function EndRequestHandler(sender, args) {
                //Hide the modal popup - the update progress
                var popup = $find('<%= modalPopupLoading.ClientID %>');
                if (popup != null) {
                    popup.hide();
                }
            }
   
        </script>
        <%--==============Ajax loading script ends==============--%>
        <%--==============Validation script ref starts==============--%>
        <script src="../JavaScript/PageValidations/LatestNewsMaster.js" type="text/javascript"></script>

         <script language="javascript" type="text/javascript">
             $(document).ready(function () { assignDiv('rptrMainMasterMenu_90_3', '103') });
    </script>
        <%--==============Validation script ref ends==============--%>
</asp:Content>
