﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Configuration;
using UMS_NigeriaBAL;
using System.Data;
using System.Drawing;

namespace UMS_Nigeria.Masters
{
    public partial class LatestNewsMaster : System.Web.UI.Page
    {
        #region Members
        CommonMethods _ObjCommonMethods = new CommonMethods();
        NotificationsBE _ObjNotificationsBE = new NotificationsBE();
        NotificationsBAL _objNotificationsBAL = new NotificationsBAL();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        XmlDocument _ObjXmlDocument = new XmlDocument();
        CommonMethods objCommonMethods = new CommonMethods();
        string Key = "AddNotice";
        public int PageNum;
        #endregion

        #region Properties
        public int PageSize
        {
            //get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStartsReport : Convert.ToInt32(hfPageSize.Value); }
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = objCommonMethods.GetPagePath(this.Request.Url);
                //if (objCommonMethods.IsAccess(path))
                //{
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    BindGrid();
                    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    {
                        ListItem liBU = new ListItem(Session[UMS_Resource.SESSION_USER_BUNAME].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());
                        liBU.Selected = true;
                        chkBusinessUnit.Items.Add(liBU);
                        allCheck.Visible = false;
                    }
                    else
                        _ObjCommonMethods.BindBusinessUnits(chkBusinessUnit, string.Empty, true);
                    //if (chkBusinessUnit.Items.Count > 0)
                    //    //if (chkBusinessUnit.Items.Count > 1)
                    //    //    chkBusinessUnit.Items[0].Text = "All";
                    //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value)); ;
                //}
                //else { Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            //if (upldaAtrchment.HasFile)
            //{
            string bu_id = string.Empty;
            string fileName = upldaAtrchment.FileName;
            _ObjNotificationsBE.CreatedBy = Convert.ToString(Session[UMS_Resource.SESSION_LOGINID]);
            _ObjNotificationsBE.CreatedDate = DateTime.Now;
            _ObjNotificationsBE.Details = txtDetails.Text;

            if (!allCheck.Checked)
            {
                foreach (ListItem c in chkBusinessUnit.Items)
                {
                    if (c.Selected)
                    {
                        bu_id += bu_id == "" ? c.Value : "," + c.Value;
                    }
                }
            }

            _ObjNotificationsBE.BU_ID = bu_id;//bu id
            _ObjNotificationsBE.FilePath = fileName;
            _ObjNotificationsBE.Subject = txtSubject.Text;
            _ObjXmlDocument = _objNotificationsBAL.InsertNotificationBAL(_ObjNotificationsBE, Statement.Insert);
            _ObjNotificationsBE = _ObjiDsignBAL.DeserializeFromXml<NotificationsBE>(_ObjXmlDocument);
            if (_ObjNotificationsBE.IsSuccess == 1)
            {
                if (upldaAtrchment.HasFile)
                {
                    upldaAtrchment.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["Notifications"].ToString()) + fileName);//Saving File in to the server.
                }
                ClearFields();
                BindGrid();
                UCPagingV1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                Message(Resource.LATESTNWEWS_INSERTIONSUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                allCheck.Checked = false;
                //_ObjCommonMethods.BindBusinessUnits(chkBusinessUnit, string.Empty, true);
            }
            else
                Message(UMS_Resource.LATESTNWEWS_INSERTIONFAILURE, UMS_Resource.MESSAGETYPE_ERROR);
        }

        protected void gvNotificationList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Literal litBUNoticeID = (Literal)row.FindControl("litBUNoticeID");
            switch (e.CommandName.ToUpper())
            {
                case "DEACTIVATENEWS":
                    mpeDeActive.Show();
                    hfBUNoticeID.Value = litBUNoticeID.Text;
                    lblDeActiveMsg.Text = Resource.DELETE_NEWS_CONFIRM;
                    break;
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            _ObjNotificationsBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            _ObjNotificationsBE.BUNoticeID = hfBUNoticeID.Value;
            _ObjNotificationsBE.Activestatus = Constants.Delete;
            _ObjXmlDocument = _objNotificationsBAL.DeleteNotificationBAL(_ObjNotificationsBE, Statement.Delete);
            _ObjNotificationsBE = _ObjiDsignBAL.DeserializeFromXml<NotificationsBE>(_ObjXmlDocument);
            if (_ObjNotificationsBE.IsSuccess == 1)
            {
                BindGrid();
                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                Message(UMS_Resource.NOTICE_DELETED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
            }
            else
                Message(UMS_Resource.NOTICE_DELETED_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
        }

        #endregion

        #region Methods
        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        //Clear all the inputs
        public void ClearFields()
        {
            txtDetails.Text = txtSubject.Text = string.Empty;
        }

        public void BindGrid()
        {
            NotificationsListBE objNotificationList = new NotificationsListBE();
            _ObjNotificationsBE.PageSize = PageSize;
            _ObjNotificationsBE.PageNo = Convert.ToInt32(hfPageNo.Value);
            _ObjNotificationsBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                _ObjNotificationsBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                _ObjNotificationsBE.BU_ID = string.Empty;
            XmlDocument ResultantXml = _objNotificationsBAL.GetNotificationListBAL(_ObjNotificationsBE);
            objNotificationList = _ObjiDsignBAL.DeserializeFromXml<NotificationsListBE>(ResultantXml);
            if (objNotificationList.Items.Count > 0)
            {
                hfTotalRecords.Value = objNotificationList.Items[0].TotalRecords.ToString();
                UCPaging2.Visible = UCPagingV1.Visible = true;
                divpaging1.Visible = divpaging2.Visible = true;
            }
            else
            {
                hfTotalRecords.Value = objNotificationList.Items.Count.ToString();
                divpaging1.Visible = divpaging2.Visible = false;
                UCPaging2.Visible = UCPagingV1.Visible = false;
            }
            gvNotificationList.DataSource = _ObjiDsignBAL.ConvertListToDataSet<NotificationsBE>(objNotificationList.Items).Tables[0];
            gvNotificationList.DataBind();
        }


        #endregion

        #region Paging
        /*private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                _ObjiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
                //ddlPageSize.Items.Clear();
                //if (TotalRecords < Constants.PageSizeStarts)
                //{
                //    ListItem item = new ListItem(Constants.PageSizeStarts.ToString(), Constants.PageSizeStarts.ToString());
                //    ddlPageSize.Items.Add(item);
                //}
                //else
                //{
                //    int previousSize = Constants.PageSizeStarts;
                //    for (int i = Constants.PageSizeStarts; i <= TotalRecords; i = i + Constants.PageSizeIncrement)
                //    {
                //        ListItem item = new ListItem(i.ToString(), i.ToString());
                //        ddlPageSize.Items.Add(item);
                //        previousSize = i;
                //    }
                //    if (previousSize < TotalRecords)
                //    {
                //        ListItem item = new ListItem((previousSize + Constants.PageSizeIncrement).ToString(), (previousSize + Constants.PageSizeIncrement).ToString());
                //        ddlPageSize.Items.Add(item);
                //    }
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGrid();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGrid();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                BindGrid();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }*/
        #endregion
    }
}