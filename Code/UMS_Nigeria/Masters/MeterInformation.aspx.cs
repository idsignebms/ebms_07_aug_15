﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for MeterInformation
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 07-Apr-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;


namespace UMS_Nigeria.Billing
{
    public partial class MeterInformation : System.Web.UI.Page
    {

        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        XmlDocument xml = null;
        string Key = UMS_Resource.KEY_METER_INFORMATION;
        string FunctionName = "BindGrid";
        #endregion

        #region Properties


        public string _pageNo
        {
            get { return hfPageNo.Value; }
            set { hfPageNo.Value = Convert.ToString(value); }
        }

        public string _lastPage
        {
            get { return hfLastPage.Value; }
            set { hfLastPage.Value = Convert.ToString(value); }
        }

        public string _totalRecords
        {
            get { return hfTotalRecords.Value; }
            set { hfTotalRecords.Value = Convert.ToString(value); }
        }

        public int _pageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStartsReport : Convert.ToInt32(hfPageSize.Value); }
            set { hfPageSize.Value = Convert.ToString(value); }
        }

        public string MeterType
        {
            get { return string.IsNullOrEmpty(ddlMeterType.SelectedValue) ? Constants.SelectedValue : ddlMeterType.SelectedValue; }
            set { ddlMeterType.SelectedValue = value.ToString(); }
        }
        private string MeterNo
        {
            get { return txtMeterNo.Text.Trim(); }
            set { txtMeterNo.Text = value; }
        }
        private string MeterSerialNo
        {
            get { return txtSerialNo.Text.Trim(); }
            set { txtSerialNo.Text = value; }
        }
        private string Size
        {
            get { return txtSize.Text.Trim(); }
            set { txtSize.Text = value; }
        }
        private string Details
        {
            get { return txtDetails.Text.Trim(); }
            set { txtDetails.Text = value; }
        }
        private string Multiplier
        {
            get { return txtMultiplier.Text.Trim(); }
            set { txtMultiplier.Text = value; }
        }
        private string Brand
        {
            get { return txtBrand.Text.Trim(); }
            set { txtBrand.Text = value; }
        }
        private string MeterRating
        {
            get { return txtMeterRating.Text.Trim(); }
            set { txtMeterRating.Text = value; }
        }
        private string NextCalibrationDate
        {
            get { return txtNextDate.Text.Trim(); }
            set { txtNextDate.Text = value; }
        }
        private string ServiceHoursBeforeCalibration
        {
            get { return txtServiceBeforeCalibration.Text.Trim(); }
            set { txtServiceBeforeCalibration.Text = value; }
        }
        private string MeterDials
        {
            get { return txtDials.Text.Trim(); }
            set { txtDials.Text = value; }
        }

        //private string Decimal
        //{
        //    get { return txtDecimal.Text.Trim(); }
        //    set { txtDecimal.Text = value; }
        //}


        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        //private void PageAccessPermissions()
        //{
        //    try
        //    {
        //        if (objCommonMethods.IsAccess(UMS_Resource.Add_METERTYPE_PAGE)) { lkbtnAddMeterType.Visible = true; } else { lkbtnAddMeterType.Visible = false; }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = objCommonMethods.GetPagePath(this.Request.Url);
                //if (objCommonMethods.IsAccess(path))
                //{
                BindDropDowns();
                //PageAccessPermissions();
                objCommonMethods.BindMeterTypes(ddlMeterType, true);
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                //}
                //else { Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }
            }
        }

        private void BindDropDowns()
        {
            try
            {
                objCommonMethods.BindBusinessUnits(ddlBusinessUnitName, string.Empty, true);
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                {
                    ddlBusinessUnitName.SelectedIndex = ddlBusinessUnitName.Items.IndexOf(ddlBusinessUnitName.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                    ddlBusinessUnitName.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnAddMeterType_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.Add_METERTYPE_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvMetersList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                Label txtGridMeterNo = (Label)row.FindControl("lblGridMeterNo");
                TextBox txtGridSerialNo = (TextBox)row.FindControl("txtGridSerialNo");
                DropDownList ddlGridMeterType = (DropDownList)row.FindControl("ddlGridMeterType");
                TextBox txtGridSize = (TextBox)row.FindControl("txtGridSize");
                TextBox txtGridMultiplier = (TextBox)row.FindControl("txtGridMultiplier");
                TextBox txtGridBrand = (TextBox)row.FindControl("txtGridBrand");
                TextBox txtGridMeterRating = (TextBox)row.FindControl("txtGridMeterRating");
                TextBox txtGridNextDate = (TextBox)row.FindControl("txtGridNextDate");
                //System.Web.UI.WebControls.Image imgDate1 = (System.Web.UI.WebControls.Image)row.FindControl("imgDate1");
                TextBox txtGridServiceBeforeCalibration = (TextBox)row.FindControl("txtGridServiceBeforeCalibration");
                TextBox txtGridDials = (TextBox)row.FindControl("txtGridDials");
                //TextBox txtGridDecimal = (TextBox)row.FindControl("txtGridDecimal");
                TextBox txtGridDetails = (TextBox)row.FindControl("txtGridDetails");

                Label lblMeterId = (Label)row.FindControl("lblMeterId");
                Label lblMeterNo = (Label)row.FindControl("lblMeterNo");
                Label lblMeterSize = (Label)row.FindControl("lblMeterSize");
                Label lblMeterType = (Label)row.FindControl("lblMeterType");
                Label lblMeterMultiplier = (Label)row.FindControl("lblMeterMultiplier");
                Label lblMeterTypeId = (Label)row.FindControl("lblMeterTypeId");
                Label lblMeterBrand = (Label)row.FindControl("lblMeterBrand");
                Label lblMeterSerialNo = (Label)row.FindControl("lblMeterSerialNo");
                Label lblMeterRating = (Label)row.FindControl("lblMeterRating");
                Label lblNextCalibrationDate = (Label)row.FindControl("lblNextCalibrationDate");
                Label lblServiceHoursBeforeCalibration = (Label)row.FindControl("lblServiceHoursBeforeCalibration");
                Label lblMeterDials = (Label)row.FindControl("lblMeterDials");
                //Label lblDecimals = (Label)row.FindControl("lblDecimals");
                Label lblDetails = (Label)row.FindControl("lblDetails");


                RadioButtonList rblGIsCAPMI = (RadioButtonList)row.FindControl("rblGIsCAPMI");
                TextBox txtGAmount = (TextBox)row.FindControl("txtGAmount");
                Label lblIsCAPMI = (Label)row.FindControl("lblIsCAPMI");
                Label lblCAPMIAmount = (Label)row.FindControl("lblCAPMIAmount");

                switch (e.CommandName.ToUpper())
                {
                    case "EDITMETER": //
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;
                        lblMeterNo.Visible = lblMeterSize.Visible = lblMeterType.Visible = lblMeterMultiplier.Visible = lblMeterBrand.Visible = lblMeterSerialNo.Visible = lblMeterSerialNo.Visible = lblMeterRating.Visible = lblNextCalibrationDate.Visible = lblServiceHoursBeforeCalibration.Visible = lblMeterDials.Visible = lblDetails.Visible = lblIsCAPMI.Visible = lblCAPMIAmount.Visible = false;
                        txtGridMeterNo.Visible = txtGridSize.Visible = txtGridSerialNo.Visible = ddlGridMeterType.Visible = txtGridMultiplier.Visible = txtGridBrand.Visible = txtGridMeterRating.Visible = txtGridNextDate.Visible = txtGridServiceBeforeCalibration.Visible = txtGridDials.Visible = txtGridDetails.Visible = rblGIsCAPMI.Visible = txtGAmount.Visible = true;
                        //imgDate1.Visible =
                        txtGridMeterNo.Text = lblMeterNo.Text;
                        txtGridSerialNo.Text = lblMeterSerialNo.Text;
                        objCommonMethods.BindMeterTypes(ddlGridMeterType, true);
                        ddlGridMeterType.SelectedIndex = ddlGridMeterType.Items.IndexOf(ddlGridMeterType.Items.FindByValue(lblMeterTypeId.Text));
                        txtGridSize.Text = lblMeterSize.Text;
                        txtGridMultiplier.Text = lblMeterMultiplier.Text;
                        txtGridBrand.Text = lblMeterBrand.Text;
                        txtGridMeterRating.Text = lblMeterRating.Text;
                        txtGridNextDate.Text = lblNextCalibrationDate.Text;
                        txtGridServiceBeforeCalibration.Text = lblServiceHoursBeforeCalibration.Text;
                        txtGridDials.Text = lblMeterDials.Text;
                        // txtGridDecimal.Text = lblDecimals.Text;
                        txtGridDetails.Text = objiDsignBAL.ReplaceNewLines(lblDetails.Text, false);


                        if (lblIsCAPMI.Text == "No")
                        {
                            rblGIsCAPMI.SelectedIndex = 1;
                            txtGAmount.Text = "0.00";
                            txtGAmount.Enabled = false;
                        }
                        else
                        {
                            rblGIsCAPMI.SelectedIndex = 0;
                            txtGAmount.Enabled = true;
                            txtGAmount.Text = lblCAPMIAmount.Text;
                        }
                        break;
                    case "CANCELMETER":
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                        lblMeterNo.Visible = lblMeterSize.Visible = lblMeterType.Visible = lblMeterMultiplier.Visible = lblMeterBrand.Visible = lblMeterSerialNo.Visible = lblMeterSerialNo.Visible = lblMeterRating.Visible = lblNextCalibrationDate.Visible = lblServiceHoursBeforeCalibration.Visible = lblMeterDials.Visible = lblDetails.Visible = lblIsCAPMI.Visible = lblCAPMIAmount.Visible = true;
                        txtGridMeterNo.Visible = txtGridSize.Visible = txtGridSerialNo.Visible = ddlGridMeterType.Visible = txtGridMultiplier.Visible = txtGridBrand.Visible = txtGridMeterRating.Visible = txtGridNextDate.Visible = txtGridServiceBeforeCalibration.Visible = txtGridDials.Visible = txtGridDetails.Visible = rblGIsCAPMI.Visible = txtGAmount.Visible = false;
                        break;
                    case "UPDATEMETER":

                        objMastersBE.MeterId = Convert.ToInt32(lblMeterId.Text);
                        objMastersBE.MeterNo = txtGridMeterNo.Text;
                        objMastersBE.MeterSerialNo = txtGridSerialNo.Text;
                        objMastersBE.MeterType = ddlGridMeterType.SelectedValue;
                        objMastersBE.MeterSize = txtGridSize.Text;
                        objMastersBE.Details = objiDsignBAL.ReplaceNewLines(txtGridDetails.Text, true);
                        objMastersBE.MeterMultiplier = txtGridMultiplier.Text;
                        objMastersBE.MeterBrand = txtGridBrand.Text;
                        objMastersBE.MeterRating = txtGridMeterRating.Text;
                        if (!string.IsNullOrEmpty(txtGridNextDate.Text))
                            objMastersBE.NextCalibrationDate = objCommonMethods.Convert_MMDDYY(txtGridNextDate.Text);
                        else
                            objMastersBE.NextCalibrationDate = string.Empty;
                        objMastersBE.ServiceHoursBeforeCalibration = txtGridServiceBeforeCalibration.Text;
                        objMastersBE.MeterDials = Convert.ToInt32(txtGridDials.Text);
                        //objMastersBE.Decimals = Convert.ToInt32(txtGridDecimal.Text);
                        objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();

                        if (rblGIsCAPMI.SelectedValue == "1")
                            objMastersBE.IsCAPMIMeter = true;
                        else
                            objMastersBE.IsCAPMIMeter = false;

                        objMastersBE.CAPMIAmount = Convert.ToDecimal(txtGAmount.Text);

                        objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.MeterType(objMastersBE, Statement.Check));

                        if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        {
                            BindGrid();
                            Message(UMS_Resource.METER_INFO_UPDATED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        else if (objMastersBE.IsMeterNoExists)
                        {
                            Message(UMS_Resource.METERNO_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else if (objMastersBE.IsMeterSerialNoExists)
                        {
                            Message(UMS_Resource.METER_SERIALNO_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else
                            Message("This Meter is already assigned to some customer, You can not change the Meter Information.", UMS_Resource.MESSAGETYPE_ERROR);//UMS_Resource.METER_INFO_DEACTIVATE_FAIL

                        break;
                    case "ACTIVEMETER":
                        mpeActivate.Show();
                        hfId.Value = lblMeterId.Text;
                        lblActiveMsg.Text = "Are you sure you want to Activate this Meter Information?";
                        btnActiveOk.Focus();
                        //objMastersBE.ActiveStatusId = Constants.Active;
                        //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objMastersBE.MeterId = Convert.ToInt32(lblMeterId.Text);
                        //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.MeterType(objMastersBE, Statement.Modify));

                        //if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        //{
                        //    BindGrid();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message(UMS_Resource.METER_INFO_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                        //}
                        //else
                        //    Message(UMS_Resource.METER_INFO_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "DEACTIVEMETER":
                        mpeDeActive.Show();
                        hfId.Value = lblMeterId.Text;
                        lblDeActiveMsg.Text = "Are you sure you want to DeActivate this Meter Information?";
                        btnDeActiveOk.Focus();
                        //objMastersBE.ActiveStatusId = Constants.DeActive;
                        //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objMastersBE.MeterId = Convert.ToInt32(lblMeterId.Text);
                        //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.MeterType(objMastersBE, Statement.Modify));

                        //if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        //{
                        //    BindGrid();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message(UMS_Resource.METER_INFO_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                        //}
                        //else
                        //    Message(UMS_Resource.METER_INFO_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void rblIsCAPMI_Changed(object Sender, EventArgs e)
        {
            try
            {
                RadioButtonList rblList = (RadioButtonList)Sender;
                var row = (GridViewRow)rblList.NamingContainer;
                TextBox txtCapmi = (TextBox)row.FindControl("txtGAmount");
                if (rblList.SelectedIndex == 0)
                {
                    txtCapmi.Enabled = true;
                }
                else
                {
                    txtCapmi.Text = "0.00";
                    txtCapmi.Enabled = false;
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.Active;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.MeterId = Convert.ToInt32(hfId.Value);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.MeterType(objMastersBE, Statement.Modify));

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.METER_INFO_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else
                    Message(UMS_Resource.METER_INFO_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.DeActive;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.MeterId = Convert.ToInt32(hfId.Value);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.MeterType(objMastersBE, Statement.Modify));

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    Message(UMS_Resource.METER_INFO_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else
                    Message("This Meter is already assigned to some customer, You can not change the Meter Information.", UMS_Resource.MESSAGETYPE_ERROR);//UMS_Resource.METER_INFO_DEACTIVATE_FAIL
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvMetersList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    Label lblGlobalAccNo = (Label)e.Row.FindControl("lblGlobalAccNo");
                    Label txtGridMeterNo = (Label)e.Row.FindControl("lblGridMeterNo");
                    TextBox txtGridSerialNo = (TextBox)e.Row.FindControl("txtGridSerialNo");
                    DropDownList ddlGridMeterType = (DropDownList)e.Row.FindControl("ddlGridMeterType");
                    TextBox txtGridSize = (TextBox)e.Row.FindControl("txtGridSize");
                    TextBox txtGridMultiplier = (TextBox)e.Row.FindControl("txtGridMultiplier");
                    TextBox txtGridBrand = (TextBox)e.Row.FindControl("txtGridBrand");
                    TextBox txtGridMeterRating = (TextBox)e.Row.FindControl("txtGridMeterRating");
                    TextBox txtGridNextDate = (TextBox)e.Row.FindControl("txtGridNextDate");
                    RadioButtonList rblGIsCAPMI = (RadioButtonList)e.Row.FindControl("rblGIsCAPMI");
                    TextBox txtGAmount = (TextBox)e.Row.FindControl("txtGAmount");
                    TextBox txtGridDials = (TextBox)e.Row.FindControl("txtGridDials");


                    Label lblIsCAPMI = (Label)e.Row.FindControl("lblIsCAPMI");
                    Label lblCAPMIAmount = (Label)e.Row.FindControl("lblCAPMIAmount");
                    Label lblActiveStatus = (Label)e.Row.FindControl("lblActiveStatus");

                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridMeterNo.ClientID + "','"
                                                                                      + txtGridSerialNo.ClientID + "','"
                                                                                      + ddlGridMeterType.ClientID + "','"
                                                                                      + txtGridSize.ClientID + "','"
                                                                                      + txtGridMultiplier.ClientID + "','"
                                                                                      + txtGridBrand.ClientID + "','"
                                                                                      + rblGIsCAPMI.ClientID + "','"
                                                                                      + txtGAmount.ClientID + "','"
                        //+ txtGridMeterRating.ClientID + "','"
                        //+ txtGridNextDate.ClientID + "','"
                                                                                      + txtGridDials.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");

                    if (Convert.ToInt32(lblActiveStatusId.Text) == Constants.DeActive)
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }

                    if (lblGlobalAccNo.Text != "--")
                    {
                        e.Row.FindControl("lkbtnActive").Visible = e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lblHyphen").Visible = true;
                    }
                    else if (lblActiveStatus.Text == "1")
                    {
                        e.Row.FindControl("lkbtnActive").Visible = e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lblHyphen").Visible = true;
                    }

                    if (!string.IsNullOrEmpty(lblIsCAPMI.Text))
                    {
                        if (Convert.ToBoolean(lblIsCAPMI.Text))
                        {
                            lblIsCAPMI.Text = "Yes";
                        }
                        else
                        {
                            lblIsCAPMI.Text = "No";
                        }
                    }
                    else
                    {
                        lblIsCAPMI.Text = "No";
                    }
                    if (!string.IsNullOrEmpty(lblCAPMIAmount.Text))
                    {
                        if (Convert.ToDecimal(lblCAPMIAmount.Text) != 0)
                        {
                            lblCAPMIAmount.Text = objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblCAPMIAmount.Text), 2, Constants.MILLION_Format);
                        }
                        else
                        {
                            lblCAPMIAmount.Text = "---";
                        }
                    }
                    else
                    {
                        lblCAPMIAmount.Text = "---";
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = InsertMeterInformation();

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    //UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.METER_INFORMATION_INSERT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    txtMeterNo.Text = txtSerialNo.Text = txtSize.Text = txtDetails.Text = txtMultiplier.Text = txtBrand.Text = txtMeterRating.Text = txtNextDate.Text = txtDials.Text = txtServiceBeforeCalibration.Text = txtAmount.Text = string.Empty;
                    ddlMeterType.SelectedIndex = 0;
                    rblIsCAPMI.SelectedIndex = 1;
                }
                else if (objMastersBE.IsMeterNoExists)
                {
                    Message(UMS_Resource.METERNO_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (objMastersBE.IsMeterSerialNoExists)
                {
                    Message(UMS_Resource.METER_SERIALNO_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                    Message(UMS_Resource.METER_INFORMATION_INSERT_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods
        public void BindGrid()
        {
            try
            {

                int startIndex, endIndex = 0;
                startIndex = ((Convert.ToInt32(hfPageNo.Value) - 1) * _pageSize) + 1;
                endIndex = (Convert.ToInt32(hfPageNo.Value) * _pageSize);

                MastersBE objMastersBE = new MastersBE();
                objMastersBE.startIndex = startIndex;
                objMastersBE.endIndex = endIndex;

                objMastersBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                objMastersBE.PageSize = _pageSize;

                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    objMastersBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    objMastersBE.BU_ID = string.Empty;
                MastersListBE objMastersListBE = objiDsignBAL.DeserializeFromXml<MastersListBE>(objMastersBAL.MeterType(objMastersBE, ReturnType.Fetch));

                if (objMastersListBE.Items.Count > 0)
                {
                    gvMetersList.DataSource = objMastersListBE.Items;
                    gvMetersList.DataBind();
                    lblCountRecords.Text = hfTotalRecords.Value = objMastersListBE.Items[0].TotalRecords.ToString();
                    divPaging1.Visible = divPaging2.Visible = true;
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));

                    // GeneratePaging();
                }
                else
                {
                    gvMetersList.DataSource = new DataTable();
                    gvMetersList.DataBind();
                    lblCountRecords.Text = hfTotalRecords.Value = Constants.Zero.ToString();
                    divPaging1.Visible = divPaging2.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private MastersBE InsertMeterInformation()
        {
            MastersBE objMastersBE = new MastersBE();
            try
            {
                objMastersBE.MeterNo = MeterNo;
                objMastersBE.MeterSerialNo = MeterSerialNo;
                objMastersBE.MeterType = MeterType;
                objMastersBE.MeterSize = Size;
                objMastersBE.Details = objiDsignBAL.ReplaceNewLines(Details, true);
                objMastersBE.MeterMultiplier = Multiplier;
                objMastersBE.MeterBrand = Brand;
                objMastersBE.MeterRating = MeterRating;

                objMastersBE.IsCAPMIMeter = (string.IsNullOrEmpty(rblIsCAPMI.SelectedValue)) ? false : (rblIsCAPMI.SelectedValue == "1") ? true : false;
                if (objMastersBE.IsCAPMIMeter)
                    objMastersBE.CAPMIAmount = (string.IsNullOrEmpty(txtAmount.Text)) ? 0 : Convert.ToDecimal(txtAmount.Text);
                else
                    objMastersBE.CAPMIAmount = 0;

                if (!string.IsNullOrEmpty(NextCalibrationDate))
                    objMastersBE.NextCalibrationDate = objCommonMethods.Convert_MMDDYY(NextCalibrationDate);
                else
                    objMastersBE.NextCalibrationDate = string.Empty;
                objMastersBE.ServiceHoursBeforeCalibration = ServiceHoursBeforeCalibration;
                objMastersBE.MeterDials = Convert.ToInt32(MeterDials);
                objMastersBE.BU_ID = ddlBusinessUnitName.SelectedValue;
                //objMastersBE.Decimals = Convert.ToInt32(Decimal);
                objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.MeterType(objMastersBE, Statement.Change));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            return objMastersBE;
        }
        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        //private void bindSearch(string SearchString)
        //{
        //    try
        //    {
        //        int startIndex, endIndex = 0;
        //        startIndex = ((Convert.ToInt32(hfPageNo.Value) - 1) * _pageSize) + 1;
        //        endIndex = (Convert.ToInt32(hfPageNo.Value) * _pageSize);

        //        DataSet ds = _objCoonsumerBAl.GetSearchCustomers(SearchString, startIndex, endIndex);
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            gvMetersList.DataSource = ds.Tables[0];
        //            gvMetersList.DataBind();
        //            divLinks.Visible = divpaging.Visible = true;
        //            hfTotalRecords.Value = ds.Tables[0].Rows[0]["TotalRecords"].ToString();
        //            CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //        }
        //        else
        //        {
        //            hfTotalRecords.Value = Constants.Zero.ToString();
        //            gvMetersList.DataSource = new DataTable();
        //            gvMetersList.DataBind();
        //            divLinks.Visible = divpaging.Visible = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLog.LoggingIntoText(ex.Message, "bindSearch", ErrorLog.LogType.Error, ex);
        //    }
        //}
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        //#region Paging

        //public void CreatePagingDT(int TotalNoOfRecs)
        //{
        //    try
        //    {
        //        //if (TotalNoOfRecs == 0)
        //        //    TotalNoOfRecs++;
        //        double totalpages = Math.Ceiling(((double)TotalNoOfRecs / _pageSize));

        //        DataTable dtPages = new DataTable();
        //        dtPages.Columns.Add("PageNo", typeof(int));
        //        int currentpage = Convert.ToInt32(_pageNo);
        //        lkbtnLast.CommandArgument = _lastPage = lblTotalPages.Text = totalpages.ToString();

        //        objiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);

        //        lblCurrentPage.Text = _pageNo;

        //        if (totalpages == 1)
        //        {
        //            lkbtnFirst.ForeColor
        //           = lkbtnLast.ForeColor
        //           = lkbtnNext.ForeColor
        //           = lkbtnPrevious.ForeColor = System.Drawing.Color.Gray;
        //            ddlGoPage.Enabled = false;
        //        }
        //        else
        //        {
        //            ddlGoPage.Enabled = true;
        //        }
        //        if (string.Compare(_pageNo, _lastPage, true) == 0)
        //        {
        //            lkbtnNext.Enabled = lkbtnLast.Enabled = false;
        //        }
        //        else
        //        {
        //            lkbtnNext.Enabled
        //            = lkbtnLast.Enabled = true;
        //            lkbtnFirst.ForeColor
        //           = lkbtnLast.ForeColor
        //           = lkbtnNext.ForeColor
        //           = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5");
        //        }

        //        BindPageCount(Convert.ToInt32(totalpages));

        //        BindPagingDropDown(TotalNoOfRecs);


        //        //UCPaging paging2 = (UCPaging)Parent.FindControl("UCPaging2");
        //        //paging2.ddlGoPage.SelectedIndex = ddlGoPage.SelectedIndex;

        //        UpdateInstance(Convert.ToInt32(totalpages), currentpage);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //private void BindPagingDropDown(int TotalRecords)
        //{
        //    try
        //    {
        //        objCommonMethods.BindPagingDDLReport(TotalRecords, Convert.ToInt32(_pageSize), ddlPageSize);

        //        //objCommonMethods.BindPagingDropDownListReport(TotalRecords, Convert.ToInt32(_pageSize), ddlPageSize);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //public void BindPageCount(int PageCount)
        //{
        //    ddlGoPage.Items.Clear();
        //    for (int i = 1; i <= PageCount; i++)
        //    {
        //        ListItem list = new ListItem(i.ToString(), i.ToString());
        //        ddlGoPage.Items.Add(list);
        //    }
        //    ddlGoPage.Items.FindByText(_pageNo).Selected = true;
        //}

        //private void InvokeBaseFunction()
        //{
        //    this.Page.GetType().InvokeMember(FunctionName, System.Reflection.BindingFlags.InvokeMethod, null, this.Page, new object[] { });
        //}

        //private void UpdateInstance(int PageCount, int Currentpage)
        //{

        //    ddlGoPage.Enabled = ddlGoPage.Enabled;
        //    ddlPageSize.Enabled = ddlPageSize.Enabled;

        //    lblCurrentPage.Text = lblCurrentPage.Text;
        //    lblTotalPages.Text = lblTotalPages.Text;
        //    litFirst.Text = litFirst.Text;
        //    litLast.Text = litLast.Text;
        //    litNext.Text = litNext.Text;
        //    litPageSize.Text = litPageSize.Text;
        //    litPrevious.Text = litPrevious.Text;

        //    lkbtnLast.ForeColor = lkbtnLast.ForeColor;
        //    lkbtnFirst.ForeColor = lkbtnFirst.ForeColor;

        //    lkbtnPrevious.ForeColor = lkbtnPrevious.ForeColor;
        //    lkbtnNext.ForeColor = lkbtnLast.ForeColor;

        //    lkbtnPrevious.Enabled = lkbtnPrevious.Enabled;
        //    lkbtnNext.Enabled = lkbtnLast.Enabled;

        //    lkbtnLast.Enabled = lkbtnLast.Enabled;
        //    lkbtnFirst.Enabled = lkbtnFirst.Enabled;

        //    ddlGoPage.Items.Clear();
        //    for (int i = 1; i <= PageCount; i++)
        //    {
        //        ListItem itm = new ListItem(i.ToString(), i.ToString());
        //        ddlGoPage.Items.Add(itm);
        //    }
        //    ddlGoPage.SelectedIndex = Currentpage - 1;
        //    ddlPageSize.SelectedIndex = ddlPageSize.SelectedIndex;

        //    //paging1.ddlGoPage.Enabled = ddlGoPage.Enabled;
        //    //paging1.ddlPageSize.Enabled = ddlPageSize.Enabled;

        //    //paging1.lblCurrentPage.Text = lblCurrentPage.Text;
        //    //paging1.lblTotalPages.Text = lblTotalPages.Text;
        //    //paging1.litFirst.Text = litFirst.Text;
        //    //paging1.litLast.Text = litLast.Text;
        //    //paging1.litNext.Text = litNext.Text;
        //    //paging1.litPageSize.Text = litPageSize.Text;
        //    //paging1.litPrevious.Text = litPrevious.Text;

        //    //paging1.lkbtnLast.ForeColor = lkbtnLast.ForeColor;
        //    //paging1.lkbtnFirst.ForeColor = lkbtnFirst.ForeColor;

        //    //paging1.lkbtnPrevious.ForeColor = lkbtnPrevious.ForeColor;
        //    //paging1.lkbtnNext.ForeColor = lkbtnLast.ForeColor;

        //    //paging1.lkbtnPrevious.Enabled = lkbtnPrevious.Enabled;
        //    //paging1.lkbtnNext.Enabled = lkbtnLast.Enabled;

        //    //paging1.lkbtnLast.Enabled = lkbtnLast.Enabled;
        //    //paging1.lkbtnFirst.Enabled = lkbtnFirst.Enabled;

        //    //paging1.ddlGoPage.Items.Clear();
        //    //for (int i = 1; i <= PageCount; i++)
        //    //{
        //    //    ListItem itm = new ListItem(i.ToString(), i.ToString());
        //    //    paging1.ddlGoPage.Items.Add(itm);
        //    //}
        //    //paging1.ddlGoPage.SelectedIndex = Currentpage - 1;
        //    //paging1.ddlPageSize.SelectedIndex = ddlPageSize.SelectedIndex;
        //}

        ////private void UpdateInstance(int PageCount, int Currentpage)
        ////{
        ////    UCPagingV2 paging1 = (UCPagingV2)Parent.FindControl("UCPaging1");
        ////    UCPagingV2 paging2 = (UCPagingV2)Parent.FindControl("UCPaging2");
        ////    if (paging2 != null)
        ////    {

        ////        paging2.ddlGoPage.Enabled = ddlGoPage.Enabled;
        ////        paging2.ddlPageSize.Enabled = ddlPageSize.Enabled;

        ////        paging2.lblCurrentPage.Text = lblCurrentPage.Text;
        ////        paging2.lblTotalPages.Text = lblTotalPages.Text;
        ////        paging2.litFirst.Text = litFirst.Text;
        ////        paging2.litLast.Text = litLast.Text;
        ////        paging2.litNext.Text = litNext.Text;
        ////        paging2.litPageSize.Text = litPageSize.Text;
        ////        paging2.litPrevious.Text = litPrevious.Text;

        ////        paging2.lkbtnLast.ForeColor = lkbtnLast.ForeColor;
        ////        paging2.lkbtnFirst.ForeColor = lkbtnFirst.ForeColor;

        ////        paging2.lkbtnPrevious.ForeColor = lkbtnPrevious.ForeColor;
        ////        paging2.lkbtnNext.ForeColor = lkbtnLast.ForeColor;

        ////        paging2.lkbtnPrevious.Enabled = lkbtnPrevious.Enabled;
        ////        paging2.lkbtnNext.Enabled = lkbtnLast.Enabled;

        ////        paging2.lkbtnLast.Enabled = lkbtnLast.Enabled;
        ////        paging2.lkbtnFirst.Enabled = lkbtnFirst.Enabled;

        ////        paging2.ddlGoPage.Items.Clear();
        ////        for (int i = 1; i <= PageCount; i++)
        ////        {
        ////            ListItem itm = new ListItem(i.ToString(), i.ToString());
        ////            paging2.ddlGoPage.Items.Add(itm);
        ////        }
        ////        paging2.ddlGoPage.SelectedIndex = Currentpage - 1;
        ////        paging2.ddlPageSize.SelectedIndex = ddlPageSize.SelectedIndex;
        ////    }
        ////    if (paging1 != null)
        ////    {
        ////        paging1.ddlGoPage.Enabled = ddlGoPage.Enabled;
        ////        paging1.ddlPageSize.Enabled = ddlPageSize.Enabled;

        ////        paging1.lblCurrentPage.Text = lblCurrentPage.Text;
        ////        paging1.lblTotalPages.Text = lblTotalPages.Text;
        ////        paging1.litFirst.Text = litFirst.Text;
        ////        paging1.litLast.Text = litLast.Text;
        ////        paging1.litNext.Text = litNext.Text;
        ////        paging1.litPageSize.Text = litPageSize.Text;
        ////        paging1.litPrevious.Text = litPrevious.Text;

        ////        paging1.lkbtnLast.ForeColor = lkbtnLast.ForeColor;
        ////        paging1.lkbtnFirst.ForeColor = lkbtnFirst.ForeColor;

        ////        paging1.lkbtnPrevious.ForeColor = lkbtnPrevious.ForeColor;
        ////        paging1.lkbtnNext.ForeColor = lkbtnLast.ForeColor;

        ////        paging1.lkbtnPrevious.Enabled = lkbtnPrevious.Enabled;
        ////        paging1.lkbtnNext.Enabled = lkbtnLast.Enabled;

        ////        paging1.lkbtnLast.Enabled = lkbtnLast.Enabled;
        ////        paging1.lkbtnFirst.Enabled = lkbtnFirst.Enabled;

        ////        paging1.ddlGoPage.Items.Clear();
        ////        for (int i = 1; i <= PageCount; i++)
        ////        {
        ////            ListItem itm = new ListItem(i.ToString(), i.ToString());
        ////            paging1.ddlGoPage.Items.Add(itm);
        ////        }
        ////        paging1.ddlGoPage.SelectedIndex = Currentpage - 1;
        ////        paging1.ddlPageSize.SelectedIndex = ddlPageSize.SelectedIndex;
        ////    }
        ////  }

        //private void GeneratePaging()
        //{
        //    CreatePagingDT(Convert.ToInt32(_totalRecords));
        //    // BindPagingDropDown(Convert.ToInt32(_totalRecords));
        //}

        //protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        _pageNo = Constants.pageNoValue.ToString();
        //        _pageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
        //        InvokeBaseFunction();
        //        CreatePagingDT(Convert.ToInt32(_totalRecords));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnFirst_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        _pageNo = Constants.pageNoValue.ToString();
        //        InvokeBaseFunction();
        //        CreatePagingDT(Convert.ToInt32(_totalRecords));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnPrevious_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum -= Constants.pageNoValue;
        //        _pageNo = PageNum.ToString();

        //        InvokeBaseFunction();
        //        CreatePagingDT(Convert.ToInt32(_totalRecords));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnNext_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum += Constants.pageNoValue;
        //        _pageNo = PageNum.ToString();
        //        InvokeBaseFunction();
        //        CreatePagingDT(Convert.ToInt32(_totalRecords));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnLast_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        _pageNo = _lastPage;
        //        InvokeBaseFunction();
        //        CreatePagingDT(Convert.ToInt32(_totalRecords));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void ddlGoPage_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        _pageNo = _goPageNo.ToString();
        //        InvokeBaseFunction();
        //        CreatePagingDT(Convert.ToInt32(_totalRecords));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //#endregion

        #region Paging
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                //bindSearch(hfSearchString.Value);
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                BindGrid();
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                //bindSearch(hfSearchString.Value);
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                BindGrid();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                //bindSearch(hfSearchString.Value);
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                BindGrid();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                //  bindSearch(hfSearchString.Value);
                // CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                BindGrid();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                // bindSearch(hfSearchString.Value);
                // CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                BindGrid();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / _pageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                objiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; }
                else
                {
                    lkbtnNext.Enabled = lkbtnLast.Enabled = true;
                    lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5");
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                objCommonMethods.BindPagingDropDownList(TotalRecords, this._pageSize, ddlPageSize, Constants.SearchCustPageSize, Constants.SearchCustPageSizeIncrement);
                //ddlPageSize.Items.Clear();
                //if (TotalRecords < Constants.PageSizeStarts)
                //{
                //    ListItem item = new ListItem(Constants.PageSizeStarts.ToString(), Constants.PageSizeStarts.ToString());
                //    ddlPageSize.Items.Add(item);
                //}
                //else
                //{
                //    int previousSize = Constants.PageSizeStarts;
                //    for (int i = Constants.PageSizeStarts; i <= TotalRecords; i = i + Constants.PageSizeIncrement)
                //    {
                //        ListItem item = new ListItem(i.ToString(), i.ToString());
                //        ddlPageSize.Items.Add(item);
                //        previousSize = i;
                //    }
                //    if (previousSize < TotalRecords)
                //    {
                //        ListItem item = new ListItem((previousSize + Constants.PageSizeIncrement).ToString(), (previousSize + Constants.PageSizeIncrement).ToString());
                //        ddlPageSize.Items.Add(item);
                //    }
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

    }
}