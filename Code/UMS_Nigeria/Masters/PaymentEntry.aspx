﻿<%@ Page Title="::Payment Entry::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="PaymentEntry.aspx.cs" Inherits="UMS_Nigeria.Masters.PaymentEntry" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="edmcontainer">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upPaymentEntry" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div class="consumer_total condumepay">
                    <h3>
                        <asp:Literal ID="litReadMethod" runat="server" Text="<%$ Resources:Resource, PAYMENT_ENTRY%>"> </asp:Literal>
                    </h3>
                    <div class="pad_10">
                    </div>
                    <div class="consumer_feild">
                        <div class="consume_name">
                            <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, ACCNO_OLD_ACCNO%>"> </asp:Literal>
                            <span>*</span>
                        </div>
                        <br />
                        <div class="consume_input c_left pEntry">
                            <%--<asp:Button ID="btndummy" runat="server" Text="this is for getting customer details based on given accountnumber"
                                Style="display: none;" OnClick="btndummy_Click" />--%>
                            <%--onblur="GetAccountDetails(this,'spanAccountNo','Account No')"--%>
                            <asp:TextBox CssClass="text-box" ID="txtAccountNo" runat="server" MaxLength="20"> </asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtAccountNo"
                                ValidChars="" FilterMode="ValidChars" FilterType="Numbers">
                            </asp:FilteredTextBoxExtender>
                            <span id="spanAccountNo" class="span_color"></span>
                        </div>
                        <div class="fltl pmtgetdetbtntwo">
                            <asp:Button ID="btnGetDetails" CssClass="box_s" Text="Get Details" runat="server"
                                OnClientClick="return ValidateAccountNo();" OnClick="btnGetDetails_Click" />
                            <asp:Button ID="btnClear" CssClass="box_s" Text="Clear" Visible="false" runat="server" 
                             OnClick="btnClear_Click" />
                        </div>
                        <div class="clear pad_10">
                        </div>
                        <div id="DivCustPaymentDetails" runat="server" visible="false">
                            <div class="consume_name">
                                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, PAID_AMOUNT%>"> </asp:Literal>
                                <span class="spancolor">*</span>
                            </div>
                            <%--onblur="return TextBoxBlurValidationlbl(this,'spanAccountNo','Account No')" OnTextChanged="txtAccountNo_TextChanged"--%>
                            <%--onkeypress="return isNumberKey(event,this)"--%>
                            <div class="consume_input c_left pEntry">
                                <asp:TextBox CssClass="text-box" ID="txtPaidAmount" runat="server" onblur="return ValidatePaidAmount()"
                                    onkeypress="return isNumberKey(event,this)" AutoComplete="off" MaxLength="16"
                                    onkeyup="GetCurrencyFormate(this);"> </asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtPaidAmount"
                                    ValidChars=".," FilterMode="ValidChars" FilterType="Numbers,Custom">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanPaidAmount" class="span_color"></span>
                            </div>
                            <div class="clear pad_10">
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, RECEIPT_NO%>"> </asp:Literal><span>*</span></div>
                            <div class="consume_input c_left pEntry">
                                <asp:TextBox CssClass="text-box" ID="txtReciptNo" runat="server" MaxLength="20" onblur="return TextBoxBlurValidationlbl(this,'spanReciptNo','Receipt No')"> </asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbName" runat="server" TargetControlID="txtReciptNo"
                                    ValidChars="_" FilterMode="ValidChars" FilterType="LowercaseLetters,UppercaseLetters,Numbers,Custom">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanReciptNo" class="span_color"></span>
                            </div>
                            <div class="clear pad_10">
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, PAYMENT_MODE%>"> </asp:Literal><span>*</span></div>
                            <div class="consume_input c_left pEntry">
                                <asp:DropDownList ID="ddlPaymentMode" Style="width: 175px;" onchange="DropDownlistOnChangelbl(this,'spanPaymentMode',' Payment Mode')"
                                    runat="server" CssClass="text-box select_box">
                                    <asp:ListItem Text="--Select--">
                                    </asp:ListItem>
                                </asp:DropDownList>
                                <span id="spanPaymentMode" class="span_color"></span>
                            </div>
                            <div class="clear pad_10">
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal16" runat="server" Text="Payment Received Date"> </asp:Literal><span>*</span></div>
                            <div class="consume_input c_left" style="margin-left: 2px !important;">
                                <asp:TextBox CssClass="text-box" ID="txtPaymentRecievedDate" AutoComplete="off" MaxLength="10"
                                    onchange="DoBlur(this);" onblur="DoBlur(this);" Width="130px" runat="server"></asp:TextBox>
                                <asp:Image ID="imgDate" ImageUrl="~/images/icon_calendar.png" Style="vertical-align: middle"
                                    AlternateText="Calender" runat="server" />
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtPaymentRecievedDate"
                                    FilterType="Custom,Numbers" ValidChars="/">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanPaymentRecievedDate" class="span_color"></span>
                            </div>
                            <div class="clear pad_10">
                            </div>
                            <div class="consume_name">
                                <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, DOCUMENT%>"> </asp:Literal>
                            </div>
                            <div class="consume_input c_left pEntry" id="divFile">
                                <asp:FileUpload Style="width: 163px;" ID="fupDocument" runat="server" CssClass="fltl" /><br />
                                <span id="spanDocuments" style="color: Red; width: 200px;"></span>
                            </div>
                            <div class="clear pad_10">
                            </div>
                            <div class="consume_name" style="display: none;">
                                <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, BILLNUMBER%>"> </asp:Literal><span>*</span>
                            </div>
                            <%-- <div class="consume_input c_left" id="div1" style="display: none;">
                            <asp:DropDownList ID="ddlBillNos" runat="server">
                            </asp:DropDownList>
                            <span id="spanBillNos" class="span_color"></span>
                        </div>--%>
                            <div style="width: 305px;">
                                <asp:Button runat="server" ID="btnPrevious" CssClass="payprev fltl" Text="PREVIOUS"
                                    OnClick="btnPrevious_Click"></asp:Button>
                                <asp:Button ID="btnUpdate" CssClass="box_s pmtgetdetbtnUpdate" Text="<%$ Resources:Resource, UPDATE%>"
                                    OnClientClick="return Validate();" runat="server" OnClick="btnUpdate_Click" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="paymen_total">
                    <div class="paymententry fltl">
                        <div class="consumer_total">
                            <div class="paymenthead">
                                <h3 style="text-decoration: none;">
                                    <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, BATCH_DETAILS%>"> </asp:Literal>
                                    <asp:Label ID="lblBatchDate" Style="float: right; margin-left: 12px;" runat="server"
                                        Text=""></asp:Label>
                                    <%--<asp:Label ID="Label3" Style="float: right;" runat="server" Text="<%$ Resources:Resource, BATCH_DATE%>"></asp:Label>--%>
                                </h3>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="d_brdPEnt">
                                <ul>
                                    <li class="dashleft Bat_dtl">
                                        <asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:Resource, BATCH_NO%>"> </asp:Literal></li>
                                    <li>:</li>
                                    <li class="dashright">
                                        <asp:Label ID="lblBatchNo" runat="server" Text="--"></asp:Label></li>
                                    <li class="clear"></li>
                                    <li class="dashleft Bat_dtl">
                                        <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:Resource, NO_OF_PAY_RECVD%>"> </asp:Literal></li>
                                    <li>:</li>
                                    <li class="dashright">
                                        <asp:Label ID="lblpayreceivedcount" runat="server" Text="--"></asp:Label></li>
                                    <li class="clear"></li>
                                    <asp:Label ID="lblBatchId" runat="server" Text="" Visible="false"></asp:Label>
                                    <%--<li class="dashleft">
                                        <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:Resource, BATCH_DATE%>"> </asp:Literal></li><li>
                                            :</li>
                                    <li class="dashright">
                                        <asp:Label ID="lblBatchDate" runat="server" Text=""></asp:Label></li>
                                    <li class="clear"></li>--%>
                                    <li class="dashleft Bat_dtl">
                                        <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, BATCH_TOTAL%>"> </asp:Literal></li>
                                    <li>:</li>
                                    <li class="dashright"><span class="duecolr">
                                        <asp:Label ID="lblBatchTotal" runat="server" Text="--"></asp:Label></span></li>
                                    <li class="clear"></li>
                                    <li class="dashleft Bat_dtl">
                                        <asp:Literal ID="Literal19" runat="server" Text="<%$ Resources:Resource, COLLECTION_TOT%>"> </asp:Literal></li>
                                    <li>:</li>
                                    <li class="dashright"><span class="duecolr">
                                        <asp:Label ID="lblCollectionTotal" runat="server" Text="--"></asp:Label></span></li>
                                    <li class="clear"></li>
                                    <li class="dashleft Bat_dtl">
                                        <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:Resource, PENDING%>"> </asp:Literal></li>
                                    <li>:</li>
                                    <li class="dashright"><span style="color: Green;">
                                        <asp:Label ID="lblPending" runat="server" Text="--"></asp:Label></span></li>
                                    <li class="clear"></li>
                                </ul>
                            </div>
                            <%--<div class="dashboard" style="width: 172px; float:right;">
                                <ul>
                                    <li class="dashleft" style="width: 116px; ">
                                        <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:Resource, NO_OF_PAY_RECVD%>"> </asp:Literal></li>
                                    <li>:</li>
                                    <li class="dashright">
                                        <asp:Label ID="lblpayreceivedcount" runat="server" Text="120"></asp:Label></li>
                                    <li class="clear"></li>
                                </ul>
                            </div>--%>
                            <%--<div class="pending" style="display:none;">
                                <h4 class="dashboard dashleft ul li" style="font-size: 16px; width: auto; padding-left: 10px;
                                    font-family: Arial;">
                                    <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:Resource, PENDING%>"> </asp:Literal>
                                </h4>
                                <h3 class="dashboard dashleft ul li duecolr" style="width: auto;">
                                    <asp:Label ID="lblPending" runat="server" Text="400.00"></asp:Label></h3>
                                <h4 class="dashboard dashleft ul li" style="font-size: 16px; width: auto; padding-left: 10px;
                                    font-family: Arial;">
                                    <asp:Literal ID="Literal14" runat="server" Text=" No of Payments Received"> </asp:Literal>
                                </h4>
                                <h3 class="dashboard dashleft ul li duecolr" style="width: auto; padding-left: 74px;">
                                    <asp:Label ID="lblpayreceivedcount" runat="server" Text="40"></asp:Label></h3>
                                <div class="payviewbill" style="display: none;">
                                    <a href="#">View Batch</a>
                                </div>
                            </div>--%>
                        </div>
                        <div class="clear pad_10">
                        </div>
                        <div class="consumer_total" runat="server" id="divBatchDetailsGrid" visible="false"
                            style="overflow: auto;">
                            <hr />
                            <h3 style="text-decoration: none;">
                                <asp:Label ID="Literal9" runat="server" Text="<%$ Resources:Resource, BATCH_PAYMENT%>"> </asp:Label>
                            </h3>
                            <div class="grid">
                                <asp:GridView ID="gvPaymentEntryList" runat="server" AutoGenerateColumns="False"
                                    AlternatingRowStyle-CssClass="color" OnRowCommand="gvPaymentEntryList_RowCommand"
                                    OnRowDataBound="gvPaymentEntryList_RowDataBound">
                                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" BorderStyle="Outset" />
                                    <EmptyDataTemplate>
                                        There is no bills to clear.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Account No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAccountNo" runat="server" Text='<%# Eval("AccountNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--     <asp:TemplateField HeaderText="<%$Resources:Resource, PAID_AMOUNT %>" ItemStyle-HorizontalAlign="Right">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBillTotal" runat="server" Text='<%# Eval("BillTotal") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$Resources:Resource, BILLNUMBER %>">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBillNo" runat="server" Text='<%# Eval("BillNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="<%$Resources:Resource, TOTAL_PAID %>" ItemStyle-HorizontalAlign="Right">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaidAmount" runat="server" Text='<%# Eval("PaidAmount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center" Visible="false">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnDeletePaymentEntry" runat="server" CommandName="CONFIRMDELETE"
                                                    CommandArgument='<%# Eval("AccountNo") %>' ToolTip="Delete This Payment From Batch"
                                                    Text="Delete"><img src="../images/delete.png" alt="Delete" width="16px" /></asp:LinkButton>
                                                <asp:Literal ID="lblCustomerPaymentID" runat="server" Text='<%#Eval("CustomerPaymentID") %>'
                                                    Visible="false"></asp:Literal>
                                                <asp:Literal ID="litCustomerBillPaymentId" runat="server" Text='<%#Eval("CustomerBPID") %>'
                                                    Visible="false"></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="clear pad_10">
                            </div>
                        </div>
                    </div>
                    <div class="paymententry fltr">
                        <div class="consumer_total">
                            <div class="paymenthead">
                                <h3 style="text-decoration: none;">
                                    <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, CUSTOMER_DETAILS%>"> </asp:Literal>
                                </h3>
                            </div>
                            <div class="dashboard" style="width: 410px;">
                                <ul>
                                    <li class="dashleft">
                                        <asp:Literal ID="Litera25" runat="server" Text="Acc No"></asp:Literal>
                                    </li>
                                    <li>:</li>
                                    <li class="dashright">
                                        <asp:Label ID="lblAccNoAndGlobalAccNo" runat="server" Text="--"></asp:Label>
                                        <asp:Label ID="lblAccountNo" Visible="false" runat="server" Text="--"></asp:Label>
                                        </li>
                                    <li class="clear"></li>
                                    <li class="dashleft">
                                        <asp:Literal ID="litOldAccoutnNo" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                    </li>
                                    <li>:</li>
                                    <li class="dashright">
                                        <asp:Label ID="lblOldAccountNo" runat="server" Text="--"></asp:Label></li>
                                    <li class="clear"></li>
                                    <li class="dashleft">
                                        <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                                    </li>
                                    <li>:</li>
                                    <li class="dashright">
                                        <asp:Label ID="lblName" runat="server" Text="--"></asp:Label></li>
                                    <li class="clear"></li>
                                    <li class="dashleft">
                                        <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal></li><li>
                                            :</li>
                                    <li class="dashright">
                                        <asp:Label ID="lblTariff" runat="server" Text="--"></asp:Label></li>
                                    <li class="clear"></li>
                                    <li class="dashleft">
                                        <asp:Literal ID="Literal22" runat="server" Text="<%$ Resources:Resource, CUSTOMER_TYPE%>"></asp:Literal></li>
                                    <li>:</li>
                                    <li class="dashright">
                                        <asp:Label ID="lblCustomerType" runat="server" Text="--"></asp:Label></li>
                                    <li class="clear"></li>
                                    <li class="dashleft">
                                        <asp:Literal ID="Literal23" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Literal></li>
                                    <li>:</li>
                                    <li class="dashright" style="font-size: 14px;position: relative;">
                                        <div style="width:250px;position:absolute;"><asp:Label ID="lblServiceAddress" runat="server" Text="--"></asp:Label></div></li>
                                    <li class="clear"></li>
                                    <li class="dashleft">
                                        <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:Resource, TOTAL_DUE%>"></asp:Literal></li>
                                    <li>:</li>
                                    <li class="dashright"><span class="duecolr">
                                        <asp:Label ID="lblDue" runat="server" Text="--"></asp:Label></span></li>
                                    <li class="clear"></li>
                                </ul>
                            </div>
                            <div class="payviewbill" style="display: none;">
                                <a href="#">View Bill</a>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <%--<div class="consumer_total" id="divCustomerDetailsGrid" visible="false" runat="server"
                            style="overflow: auto;">
                            <hr />
                            <h3 style="text-decoration: none;">
                                <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, CUSTOMER_BILL_DETAILS%>"> </asp:Label>
                            </h3>
                            <div class="grid">
                                <asp:GridView ID="gvCustomerBills" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                                    OnRowDataBound="gvCustomerBills_RowDataBound">
                                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                    <EmptyDataTemplate>
                                        There is no bills.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Bill No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBillNo" runat="server" Text='<%# Eval("BillNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Bill Date" ItemStyle-HorizontalAlign="Right">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBillDate" runat="server" Text='<%# Eval("BillDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Bill Amount" ItemStyle-HorizontalAlign="Right">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBillTotal" runat="server" Text='<%# Eval("BillTotal") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Bill Due" ItemStyle-HorizontalAlign="Right">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDueAmount" runat="server" Text='<%# Eval("DueAmount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:BoundField HeaderText="Bill Date" DataField="BillDate" />
                                        <asp:BoundField HeaderText="Bill Amount" DataField="BillTotal" />
                                        <asp:BoundField HeaderText="Bill Due" DataField="DueAmount" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="clear pad_10">
                            </div>
                        </div>--%>
                    </div>
                    <div class="fltr pad_10">
                        <asp:Button CssClass="orange_btn" ID="btnFinish" runat="server" Text="BATCH FINISH"
                            OnClick="btnFinish_Click" />
                        <br />
                        <br />
                        <div style="font-weight: bold; font-size: 12px;">
                            <b class="spancolor">
                                <asp:Label ID="Label5" runat="server" Text="<%$Resources:Resource, NOTE %>"></asp:Label></b>
                            <span>
                                <asp:Label ID="lblBatchMsg" runat="server" Text="<%$Resources:Resource, BATCH_FINISH_ALERT %>"></asp:Label></span></div>
                    </div>
                </div>
                <asp:ModalPopupExtender ID="mpeConfirmDelete" runat="server" TargetControlID="btnConfirmDelete"
                    PopupControlID="pnlConfirmDelete" BackgroundCssClass="popbg" CancelControlID="btnDelCancel">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnConfirmDelete" runat="server" Text="" Style="display: none;" />
                <asp:Panel runat="server" ID="pnlConfirmDelete" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblConfirmDelete" runat="server" Text="<%$Resources:Resource, SURE_TO_DELETE_PAY %>"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnConfirmPayDelete" runat="server" OnClick="btnConfirmPayDelete_Click"
                                Text="<%$ Resources:Resource, OK%>" CssClass="btn_ok" Style="margin-left: 10px;
                                width: 60px; height: 27px;" />
                            <asp:Button ID="btnDelCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="cancel_btn" Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpePendingAmount" runat="server" TargetControlID="btnPATarget"
                    PopupControlID="pnlPendingAmount" BackgroundCssClass="popbg" CancelControlID="btnPendingAmountCancel">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnPATarget" runat="server" Text="" Style="display: none;" />
                <asp:Panel runat="server" ID="pnlPendingAmount" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, BATCH_INCOMPLETE%>"></asp:Label>
                    </div>
                    <div id="divPopBatchDetails" style="margin-left: 10px; margin-top: 10px;" runat="server"
                        visible="false">
                        <table style="font-size: 12px;">
                            <tr>
                                <td style="width: 140px">
                                    <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:Resource, BATCH_TOTAL%>"> </asp:Literal>
                                </td>
                                <td style="width: 10px">
                                    :
                                </td>
                                <td>
                                    <asp:Label ID="lblpopTotBatchAmt" Text="--" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Literal ID="Literal20" runat="server" Text="<%$ Resources:Resource, COLLECTION_TOT%>"> </asp:Literal>
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    <asp:Label ID="lblpopTotBatchcollection" Text="--" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:Resource, PENDING%>"> </asp:Literal>
                                </td>
                                <td>
                                    :
                                </td>
                                <td>
                                    <asp:Label ID="lblpopTotPending" Text="--" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnPendingAmountOk" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="cancel_btn" Style="margin-left: 10px;" OnClick="btnPendingAmountOk_Click" />
                            <asp:Button ID="btnPendingAmountCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="cancel_btn" Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeBatchAmountExceed" runat="server" TargetControlID="btnBAETarger"
                    PopupControlID="pnlBatchAmountExceed" BackgroundCssClass="popbg" CancelControlID="btnBAECancel">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnBAETarger" runat="server" Text="" Style="display: none;" />
                <asp:Panel runat="server" ID="pnlBatchAmountExceed" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, BATCH_EXCEED_LIMIT%>"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnBAExceed" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="btn_ok"
                                Style="margin-left: 10px;" OnClick="btnBAExceed_Click" />
                            <asp:Button ID="btnBAECancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="cancel_btn" Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeBatchFinishComplete" runat="server" TargetControlID="btnBatchFinishComplete"
                    PopupControlID="pnlBatchFinishComplete" BackgroundCssClass="popbg">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnBatchFinishComplete" runat="server" Text="" Style="display: none;" />
                <asp:Panel runat="server" ID="pnlBatchFinishComplete" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblBatchFinishComplete" runat="server" Text="Batch Finished Successfully"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnBatchFinishCompleteSuccess" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="btn_ok" Style="margin-left: 10px;" PostBackUrl="~/Billing/Batchentry.aspx" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:ModalPopupExtender ID="mpeCommanMessage" runat="server" TargetControlID="hdnCancel1"
                    PopupControlID="pnlBillInProcess" BackgroundCssClass="popbg" CancelControlID="btnBPOK">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hdnCancel1" runat="server"></asp:HiddenField>
                <asp:Panel runat="server" ID="pnlBillInProcess" CssClass="modalPopup" Style="display: none;
                    border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblCommanMessage" runat="server" Text="<%$ Resources:Resource, BILL_IN_PROCESS_PAYMENT%>"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnBPOK" runat="server" OnClientClick="FocusAccountNo()" Text="<%$ Resources:Resource, OK%>"
                                CssClass="btn_ok" Style="margin-left: 10px;" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- Grid Alert popup Start--%>
                <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                    TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                </asp:ModalPopupExtender>
                <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                    <div class="popheader popheaderlblred">
                        <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- Grid Alert popup Closed--%>
                <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="PanelAlert"
                    TargetControlID="btnAlertDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnAlertOk">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnAlertDeActivate" runat="server" Text="Button" CssClass="popbtn" />
                <asp:Panel ID="PanelAlert" runat="server" CssClass="modalPopup" class="poppnl" Style="display: none;">
                    <div class="popheader popheaderlblred" id="pop" runat="server">
                        <asp:Label ID="lblAlertMsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnAlertOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="hfCustomerPaymentID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".rnkland").fadeOut("slow");
                $(".mpeConfirmDelete").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        $(document).ready(function () {
            //AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            //AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);
        });         
    </script>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }

        function ValidatePaidAmount() {
            var txt = document.getElementById('<%= txtPaidAmount.ClientID %>');
            if (txt.value.charAt(0) == "." || txt.value < 1) {
                var lbl = document.getElementById('spanPaidAmount');
                lbl.innerHTML = "Enter Valid Amount";
                lbl.style.display = "block";
                txt.style.border = "1px solid red";
                return false;
            }
            else {
                txt.style.border = "1px solid #8E8E8E";
                document.getElementById('spanPaidAmount').innerHTML = "";
                return TextBoxBlurValidationlbl(obj, 'spanPaidAmount', 'Paid Amount');
                return true;
            }
        }

    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../Javascript/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/PaymentEntry.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
