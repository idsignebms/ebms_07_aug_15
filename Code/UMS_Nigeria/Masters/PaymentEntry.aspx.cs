﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddRouteManagement
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 04-Apr-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using Resources;
using UMS_NigeriaBE;
using System.Globalization;
using System.Threading;
using UMS_NigriaDAL;
using System.Configuration;
using System.IO;
using System.Data;
using System.Xml;

namespace UMS_Nigeria.Masters
{
    public partial class PaymentEntry : System.Web.UI.Page
    {
        # region Members

        iDsignBAL _objiDsignBAL = new iDsignBAL();
        BillReadingsBAL objBillingBAL = new BillReadingsBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        string Key = UMS_Resource.KEY_BATCH_ENTRY;

        #endregion

        #region Properties

        public string PaymentMode
        {
            get { return string.IsNullOrEmpty(ddlPaymentMode.SelectedItem.Value) ? Constants.SelectedValue : ddlPaymentMode.SelectedItem.Value; }
            set { ddlPaymentMode.SelectedValue = value.ToString(); }
        }

        private string AccountNo
        {
            get { return txtAccountNo.Text.Trim(); }
            set { txtAccountNo.Text = value; }
        }

        private string PaidAmount
        {
            get { return txtPaidAmount.Text.Replace(",", string.Empty).Trim(); }
            set { txtPaidAmount.Text = value; }
        }

        private string ReciptNo
        {
            get { return txtReciptNo.Text.Trim(); }
            set { txtReciptNo.Text = value; }
        }

        #endregion

        # region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                lblMessage.Text = pnlMessage.CssClass = string.Empty;
                if (!IsPostBack)
                {
                    if (Request.QueryString["bno"] != null)
                    {
                        _objCommonMethods.BindPaymentModes(ddlPaymentMode, true, UMS_Resource.DROPDOWN_SELECT, false);

                        lblBatchNo.Text = _objiDsignBAL.Decrypt(Request.QueryString["bno"]);
                        lblBatchId.Text = _objiDsignBAL.Decrypt(Request.QueryString["bid"]);
                        if (Request.QueryString["pm"] != null)
                        {
                            ddlPaymentMode.SelectedIndex = ddlPaymentMode.Items.IndexOf(ddlPaymentMode.Items.FindByValue(_objiDsignBAL.Decrypt(Request.QueryString["pm"])));
                            ddlPaymentMode.Enabled = false;
                        }
                        BindBatchDetails();
                        BindPaymentEntryList();
                        txtPaidAmount.Attributes.Add("onkeypress", "return controlEnter('" + txtReciptNo.ClientID + "',event);");
                        txtReciptNo.Attributes.Add("onkeypress", "return controlEnter('" + txtPaymentRecievedDate.ClientID + "',event);");
                        txtPaymentRecievedDate.Attributes.Add("onkeypress", "return controlEnter('" + btnUpdate.ClientID + "',event);");
                    }
                    else
                    {
                        Response.Redirect(UMS_Resource.PREVIOUS_PAYMENTENTRY);
                    }
                }
            }
            else
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void btnGetDetails_Click(object sender, EventArgs e)
        {
            BindCustomerDetails();
            BindPaymentEntryList();
            txtPaidAmount.Focus();
            btnClear.Visible = true;
            btnGetDetails.Visible = false;
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtAccountNo.Enabled = btnGetDetails.Visible = true;
            btnClear.Visible = DivCustPaymentDetails.Visible = false;
            txtAccountNo.Text = string.Empty;
            lblName.Text = lblTariff.Text = lblDue.Text = lblAccountNo.Text = lblAccNoAndGlobalAccNo.Text = lblCustomerType.Text = lblServiceAddress.Text = lblOldAccountNo.Text = "--";
            BindPaymentEntryList();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                AddPayment();
                BindBatchDetails();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            txtPaymentRecievedDate.Text = "";
        }

        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.PREVIOUS_PAYMENTENTRY + "?bno=" + _objiDsignBAL.Encrypt(lblBatchNo.Text) + "&pm=" + _objiDsignBAL.Encrypt(ddlPaymentMode.SelectedValue) + "&dt=" + _objiDsignBAL.Encrypt(lblBatchDate.Text), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnFinish_Click(object sender, EventArgs e)
        {
            decimal BatchPendingAmount = Convert.ToDecimal(lblPending.Text);
            if (BatchPendingAmount < 0)
            {
                divPopBatchDetails.Visible = false;
                mpeBatchAmountExceed.Show();
            }
            else if (BatchPendingAmount > 0)
            {
                divPopBatchDetails.Visible = true;
                lblpopTotBatchAmt.Text = lblBatchTotal.Text;
                lblpopTotBatchcollection.Text = lblCollectionTotal.Text;
                lblpopTotPending.Text = lblPending.Text;
                mpePendingAmount.Show();
            }
            else
                BatchStatusUpdate(); //mpeBatchFinishComplete.Show();
        }

        //protected void btnContinue_Click(object sender, EventArgs e)
        //{
        //    BillingBE objBillingBe = new BillingBE();
        //    objBillingBe.BatchNo = Convert.ToInt32(lblBatchNo.Text);
        //    objBillingBe = _objiDsignBAL.DeserializeFromXml<BillingBE>(objBillingBAL.InsertBatchEntry(objBillingBe, Statement.Edit));
        //    if (Convert.ToBoolean(objBillingBe.IsSuccess))
        //    {
        //        Response.Redirect(UMS_Resource.PREVIOUS_PAYMENTENTRY, false);
        //    }
        //    else
        //    {
        //        Message("Batch finish update failed", UMS_Resource.MESSAGETYPE_ERROR);
        //    }
        //}

        protected void gvPaymentEntryList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            string litCustomerPaymentID = ((Literal)row.FindControl("lblCustomerPaymentID")).Text;
            string litCustomerBillPaymentId = ((Literal)row.FindControl("litCustomerBillPaymentId")).Text;

            switch (e.CommandName.ToUpper())
            {
                case "CONFIRMDELETE":
                    btnConfirmDelete.CommandArgument = litCustomerBillPaymentId;
                    btnConfirmDelete.CommandName = litCustomerPaymentID;
                    mpeConfirmDelete.Show();
                    break;
            }
        }

        public bool DeleteCustomerPayment(string CustomerBPID, string CustomerPaymentID)
        {
            bool IsApproval = _objCommonMethods.IsApproval(Resource.FEATURE_PAYMENT_ENTRY);
            MastersBE objMasterBe = new MastersBE();

            objMasterBe.CustomerBPID = Convert.ToInt32(CustomerBPID);
            objMasterBe.CustomerPaymentID = Convert.ToInt32(CustomerPaymentID);

            if (IsApproval)
                objMasterBe.Flag = Convert.ToInt32(Resource.FLAG_TEMP);
            else
                objMasterBe.Flag = Convert.ToInt32(Resource.INSERT_FINAL);

            objMasterBe = _objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.PaymentList(objMasterBe, Statement.Delete));

            if (objMasterBe.IsSuccess)
            {
                Message(Resource.PAY_DELETE, UMS_Resource.MESSAGETYPE_SUCCESS);
                return true;
            }
            else
            {
                Message(Resource.PAY_NOT_DELETED, UMS_Resource.MESSAGETYPE_ERROR);
                return false;
            }
        }

        //protected void btnOK_Click(object sender, EventArgs e)
        //{
        //    AddPayment();
        //}

        protected void btnConfirmPayDelete_Click(object sender, EventArgs e)
        {
            DeleteCustomerPayment(btnConfirmDelete.CommandArgument, btnConfirmDelete.CommandName);
            BindBatchDetails();
            BindCustomerDetails(); //Raja-ID065 ForAfter Deleting a record it shold effect on customer details also
            BindPaymentEntryList();
        }

        protected void btnPendingAmountOk_Click(object sender, EventArgs e)
        {
            //Response.Redirect(UMS_Resource.BATCH_ENTRY_PAGE);
            BatchStatusUpdate();
        }

        protected void btnBAExceed_Click(object sender, EventArgs e)
        {
            BatchStatusUpdate();
        }

        private void BatchStatusUpdate()
        {
            BillingBE _objBillingBe = new BillingBE();
            bool IsApproval = _objCommonMethods.IsApproval(Resource.FEATURE_PAYMENT_ENTRY);
            //decimal BatchPendingAmount = Convert.ToDecimal(lblPending.Text);
            _objBillingBe.BatchId = Convert.ToInt32(lblBatchId.Text);
            _objBillingBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            _objBillingBe.BatchTotal = Convert.ToDecimal(lblPending.Text);

            if (IsApproval)
                _objBillingBe.Flag = Convert.ToInt32(Resource.FLAG_TEMP);
            else
                _objBillingBe.Flag = Convert.ToInt32(Resource.INSERT_FINAL);

            _objBillingBe = _objiDsignBAL.DeserializeFromXml<BillingBE>(objBillingBAL.UpdateBatchTotalBAL(_objiDsignBAL.DoSerialize(_objBillingBe)));

            Response.Redirect(UMS_Resource.BATCH_ENTRY_PAGE);
        }

        protected void gvPaymentEntryList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblPaidAmount = (Label)e.Row.FindControl("lblPaidAmount");
                Label lblBillTotal = (Label)e.Row.FindControl("lblBillTotal");
                if (lblPaidAmount.Text != "")
                {
                    lblPaidAmount.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblPaidAmount.Text), 2, Constants.MILLION_Format).ToString();
                }
            }
        }

        protected void gvCustomerBills_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblBillTotal = (Label)e.Row.FindControl("lblBillTotal");
                Label lblDueAmount = (Label)e.Row.FindControl("lblDueAmount");
                Label lblBillDate = (Label)e.Row.FindControl("lblBillDate");

                if (lblDueAmount.Text != "")
                {
                    lblDueAmount.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblDueAmount.Text), 2, Constants.MILLION_Format).ToString();
                }
                if (lblBillTotal.Text != "")
                {
                    lblBillTotal.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblBillTotal.Text), 2, Constants.MILLION_Format).ToString();
                }
            }
        }

        #endregion

        # region Methods

        private MastersBE InsertPaymentDetails()
        {
            MastersBE objMastersBE = new MastersBE();
            try
            {
                objMastersBE.AccountNo = lblAccountNo.Text;
                objMastersBE.ReceiptNo = ReciptNo;
                objMastersBE.PaymentModeId = Convert.ToInt32(PaymentMode);
                objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.BatchId = Convert.ToInt32(lblBatchId.Text);
                objMastersBE.PaidAmount = decimal.Parse(txtPaidAmount.Text);
                objMastersBE.PaymentType = (int)PaymentType.BatchPayment;
                objMastersBE.PaymentRecievedDate = _objCommonMethods.Convert_MMDDYY(txtPaymentRecievedDate.Text);
                objMastersBE.FunctionId = (int)EnumApprovalFunctions.PaymentEntry;
                objMastersBE.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                objMastersBE.PaymentFromId = (int)PaymentFrom.BatchWisePayment;

                if (fupDocument.HasFile)
                {
                    string DocumentFileName = Path.GetFileName(fupDocument.FileName).ToString();//Assinging FileName
                    string DocumentExtension = Path.GetExtension(DocumentFileName);//Storing Extension of file into variable Extension
                    DateTime PhotocurrentTime = DateTime.Now;
                    string DocumentFileNameCurrentDate = PhotocurrentTime.ToString("dd-MM-yyyy_hh-mm-ss");
                    string DocumentFileNameModifiedFileName = Path.GetFileNameWithoutExtension(DocumentFileName) + "_" + DocumentFileNameCurrentDate + DocumentExtension;
                    fupDocument.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["Payments"].ToString()) + DocumentFileNameModifiedFileName);//Saving File in to the server.
                    objMastersBE.Document = DocumentFileNameModifiedFileName;
                    objMastersBE.DocumentPath = ConfigurationManager.AppSettings["Payments"].ToString().Replace("~", "..");
                }

                if (_objCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.PaymentEntry, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString()))
                {
                    objMastersBE.ApprovalStatusId = (int)EnumApprovalStatus.Approved;
                    objMastersBE.IsFinalApproval = true;
                    //lblAlertMsg.Text = Resource.MODIFICATIONS_UPDATED;
                    lblAlertMsg.Text = Resource.PAYMENT_RECEIVED_SUCCESS;
                    objMastersBE = _objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.PaymentEntry(objMastersBE, Statement.Insert));
                    SendMailAndMsg();
                }
                else
                {
                    objMastersBE.ApprovalStatusId = (int)EnumApprovalStatus.Process;
                    objMastersBE.IsFinalApproval = false;
                    lblAlertMsg.Text = Resource.APPROVAL_TARIFF_CHANGE;
                    objMastersBE = _objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.PaymentEntry(objMastersBE, Statement.Insert));
                }


                return objMastersBE;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            return objMastersBE;
        }

        private void SendMailAndMsg()
        {
            string GlobalAccountNo = lblAccountNo.Text;
            string SuccessMessage = string.Empty;
            SuccessMessage = "Bill generated successfully.";
            CommunicationFeaturesBE objCommunicationFeaturesBE = new CommunicationFeaturesBE();
            objCommunicationFeaturesBE.CommunicationFeaturesID = (int)CommunicationFeatures.Payments;

            try
            {
                if (_objCommonMethods.IsEmailFeatureEnabled(objCommunicationFeaturesBE))
                {
                    CommunicationFeaturesBE _objMailBE = new CommunicationFeaturesBE();
                    _objMailBE.GlobalAccountNumber = Convert.ToString(GlobalAccountNo);
                    XmlDocument xml = _objCommonMethods.GetDetails(_objMailBE, ReturnType.Bulk);
                    _objMailBE = _objiDsignBAL.DeserializeFromXml<CommunicationFeaturesBE>(xml);

                    if (_objMailBE.EmailId == "0")
                    {
                        SuccessMessage += "Customer not having Mail Id.";
                    }
                    else if (!string.IsNullOrEmpty(_objMailBE.EmailId))
                    {
                        if (_objMailBE.IsSuccess)
                        {
                            string amount = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(_objMailBE.PaidAmount), 2, Constants.MILLION_Format);
                            string outstandingamount = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(_objMailBE.OutStandingAmount), 2, Constants.MILLION_Format);
                            string htmlBody = string.Empty;
                            htmlBody = _objCommonMethods.GetMailCommTemp(CommunicationFeatures.Payments, Convert.ToString(_objMailBE.Name), string.Empty, Convert.ToString(GlobalAccountNo), Convert.ToString(_objMailBE.MeterNumber), string.Empty, Convert.ToString(outstandingamount), string.Empty, Convert.ToString(amount), Convert.ToString(_objMailBE.PaidDate), string.Empty, "Dear Customer, bill payment of =N= " + Convert.ToString(amount) + " for your BEDC acc " + Convert.ToString(GlobalAccountNo) + " has been processed successfully. <br/>Txn Id: " + Convert.ToString(_objMailBE.TransactionId) + ".");

                            _objCommonMethods.sendmail(htmlBody, ConfigurationManager.AppSettings["SMTPUsername"].ToString(), "Payment Received from acc " + Convert.ToString(GlobalAccountNo), _objMailBE.EmailId, string.Empty);
                            SuccessMessage += "Mail sent successfully.";
                        }
                    }
                    else
                    {
                        SuccessMessage += "Mail not sent to this customer.";
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

            try
            {
                if (_objCommonMethods.IsSMSFeatureEnabled(objCommunicationFeaturesBE))
                {
                    CommunicationFeaturesBE _objSmsBE = new CommunicationFeaturesBE();
                    _objSmsBE.GlobalAccountNumber = Convert.ToString(GlobalAccountNo);
                    XmlDocument xml = _objCommonMethods.GetDetails(_objSmsBE, ReturnType.Bulk);
                    _objSmsBE = _objiDsignBAL.DeserializeFromXml<CommunicationFeaturesBE>(xml);

                    if (_objSmsBE.MobileNo == "0")
                    {
                        SuccessMessage += "Customer not having Mobile No.";
                    }
                    else if (!string.IsNullOrEmpty(_objSmsBE.MobileNo) && _objSmsBE.MobileNo.Length == 10)
                    {
                        if (_objSmsBE.IsSuccess)
                        {
                            string amount = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(_objSmsBE.PaidAmount), 2, Constants.MILLION_Format);
                            string Messsage = string.Empty;
                            Messsage = _objCommonMethods.GetSMSCommTemp(CommunicationFeatures.Payments, Convert.ToString(GlobalAccountNo), string.Empty, Convert.ToString(amount), string.Empty, Convert.ToString(_objSmsBE.TransactionId), string.Empty);

                            _objCommonMethods.sendSMS(_objSmsBE.MobileNo, Messsage, ConfigurationManager.AppSettings["SMTPUsername"].ToString());
                            SuccessMessage += "Message sent successfully.";
                        }
                    }
                    else
                    {
                        SuccessMessage += "Message not sent to this customer.";
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //private MastersBE InsertPaymentDetails()
        //{
        //    MastersBE objMastersBE = new MastersBE();
        //    try
        //    {
        //        bool IsApproval = _objCommonMethods.IsApproval(Resource.FEATURE_PAYMENT_ENTRY);
        //        objMastersBE.AccountNo = lblAccountNo.Text;
        //        objMastersBE.ReceiptNo = ReciptNo;
        //        objMastersBE.PaymentModeId = Convert.ToInt32(PaymentMode);
        //        objMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
        //        objMastersBE.BatchNo = Convert.ToInt32(lblBatchId.Text);
        //        objMastersBE.PaidAmount = decimal.Parse(txtPaidAmount.Text);
        //        objMastersBE.PaymentType = (int)PaymentType.BatchPayment;
        //        objMastersBE.PaymentRecievedDate = _objCommonMethods.Convert_MMDDYY(txtPaymentRecievedDate.Text);

        //        if (fupDocument.HasFile)
        //        {
        //            string DocumentFileName = Path.GetFileName(fupDocument.FileName).ToString();//Assinging FileName
        //            string DocumentExtension = Path.GetExtension(DocumentFileName);//Storing Extension of file into variable Extension
        //            DateTime PhotocurrentTime = DateTime.Now;
        //            string DocumentFileNameCurrentDate = PhotocurrentTime.ToString("dd-MM-yyyy_hh-mm-ss");
        //            string DocumentFileNameModifiedFileName = Path.GetFileNameWithoutExtension(DocumentFileName) + "_" + DocumentFileNameCurrentDate + DocumentExtension;
        //            fupDocument.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["Payments"].ToString()) + DocumentFileNameModifiedFileName);//Saving File in to the server.
        //            objMastersBE.Document = DocumentFileNameModifiedFileName;
        //            objMastersBE.DocumentPath = ConfigurationManager.AppSettings["Payments"].ToString().Replace("~", "..");
        //        }
        //        if (IsApproval)
        //        {
        //            objMastersBE.Flag = Convert.ToInt32(Resources.Resource.FLAG_TEMP);
        //            objMastersBE = _objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.PaymentEntryTemporaryBAL(objMastersBE, Statement.Insert));
        //        }
        //        else
        //            objMastersBE = _objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.PaymentEntry(objMastersBE, Statement.Insert));

        //        return objMastersBE;
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //    return objMastersBE;
        //}

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void BindBatchDetails()
        {
            try
            {
                bool IsApproval = _objCommonMethods.IsApproval(Resource.FEATURE_PAYMENT_ENTRY);
                BillingBE objBillingBe = new BillingBE();
                objBillingBe.BatchNo = Convert.ToInt32(lblBatchNo.Text);
                objBillingBe.BatchDate = _objCommonMethods.Convert_MMDDYY(_objiDsignBAL.Decrypt(Request.QueryString["dt"]));
                objBillingBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();

                if (IsApproval)
                    objBillingBe.Flag = Convert.ToInt32(Resources.Resource.FLAG_TEMP);
                else
                    objBillingBe.Flag = Convert.ToInt32(Resources.Resource.INSERT_FINAL);

                objBillingBe = _objiDsignBAL.DeserializeFromXml<BillingBE>(objBillingBAL.Get(objBillingBe, ReturnType.Get));

                if (objBillingBe != null)
                {
                    lblBatchDate.Text = objBillingBe.BatchDate;
                    lblBatchTotal.Text = (objBillingBe.BatchTotal == 0) ? "NA" : _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(objBillingBe.BatchTotal.ToString()), 2, Constants.MILLION_Format).ToString();
                    lblPending.Text = (objBillingBe.BatchTotal == 0) ? "NA" : _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(objBillingBe.BatchPendingAmount.ToString()), 2, Constants.MILLION_Format).ToString();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //private void BindBills()
        //{
        //    if (txtAccountNo.Text != "")
        //    {
        //        MastersBE objMasterBe = new MastersBE();
        //        MastersListBE objMasterListBe = new MastersListBE();
        //        objMasterBe.AccountNo = txtAccountNo.Text;
        //        objMasterListBe = _objiDsignBAL.DeserializeFromXml<MastersListBE>(objMastersBAL.Get(objMasterBe, ReturnType.Retrieve));

        //        //_objiDsignBAL.FillDropDownList(_objiDsignBAL.ConvertListToDataSet<MastersBE>(objMasterListBe.Items), ddlBillNos, "BillNo", "BillNo", UMS_Resource.DROPDOWN_SELECT, true);

        //        if (objMasterListBe.Items.Count > 0)
        //        {
        //            divCustomerDetailsGrid.Visible = true;
        //            gvCustomerBills.DataSource = objMasterListBe.Items;
        //            gvCustomerBills.DataBind();
        //        }
        //        else
        //        {
        //            gvCustomerBills.DataSource = new DataTable();
        //            gvCustomerBills.DataBind();
        //        }
        //    }
        //}

        private void BindPaymentEntryList()
        {
            bool IsApproval = _objCommonMethods.IsApproval(Resource.FEATURE_PAYMENT_ENTRY);
            MastersBE objMasterBe = new MastersBE();
            MastersListBE objMasterListBe = new MastersListBE();
            objMasterBe.BatchId = Convert.ToInt32(lblBatchId.Text);
            objMasterBe.BatchNo = Convert.ToInt32(lblBatchNo.Text);

            if (IsApproval)
                objMasterBe.Flag = Convert.ToInt32(Resources.Resource.FLAG_TEMP);
            else
                objMasterBe.Flag = Convert.ToInt32(Resources.Resource.INSERT_FINAL);

            objMasterListBe = _objiDsignBAL.DeserializeFromXml<MastersListBE>(objMastersBAL.Get(objMasterBe, ReturnType.List));

            if (objMasterListBe.Items.Count > 0)
            {
                divBatchDetailsGrid.Visible = true;
                DataTable dt = _objiDsignBAL.ConvertListToDataSet<MastersBE>(objMasterListBe.Items).Tables[0];
                DataRow dr = dt.NewRow();
                dr["AccountNo"] = "Total";
                dt.Rows.Add(dr);

                gvPaymentEntryList.DataSource = dt;
                gvPaymentEntryList.DataBind();

                gvPaymentEntryList.Rows[gvPaymentEntryList.Rows.Count - 1].Cells[gvPaymentEntryList.Columns.Count - 3].Text = lblCollectionTotal.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(dt.Compute("Sum(PaidAmount)", "").ToString()), 2, Constants.MILLION_Format).ToString();
                gvPaymentEntryList.Rows[gvPaymentEntryList.Rows.Count - 1].Cells[gvPaymentEntryList.Columns.Count - 3].Attributes.Add("colSpan ", "4");
                gvPaymentEntryList.Rows[gvPaymentEntryList.Rows.Count - 1].Cells[gvPaymentEntryList.Columns.Count - 3].Attributes.Add("style ", "text-align:right;padding-right:95px;");
                gvPaymentEntryList.Rows[gvPaymentEntryList.Rows.Count - 1].Cells[gvPaymentEntryList.Columns.Count - 2].Visible = false;
                gvPaymentEntryList.Rows[gvPaymentEntryList.Rows.Count - 1].Cells[gvPaymentEntryList.Columns.Count - 1].Visible = false;

                lblpayreceivedcount.Text = (gvPaymentEntryList.Rows.Count - 1).ToString();
            }
            else
            {
                divBatchDetailsGrid.Visible = false;
                gvPaymentEntryList.DataSource = new DataTable();
                gvPaymentEntryList.DataBind();
                lblCollectionTotal.Text = "0.00";
            }
        }

        private void BindCustomerDetails()
        {
            MastersBE objMasterBe = new MastersBE();
            objMasterBe.AccountNo = txtAccountNo.Text;

            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                objMasterBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                objMasterBe.BU_ID = string.Empty;

            objMasterBe = _objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.Get(objMasterBe, ReturnType.Group));

            if (string.Compare(objMasterBe.StatusText, "success", 0) == 1)//If customer doesn't exists. 
            {
                //divCustomerDetailsGrid.Visible = false;
                DivCustPaymentDetails.Visible = false;
                txtAccountNo.Text = string.Empty;
                lblName.Text = lblTariff.Text = lblDue.Text = lblAccountNo.Text = lblAccNoAndGlobalAccNo.Text = lblCustomerType.Text = lblServiceAddress.Text = lblOldAccountNo.Text = "--";
                Message(objMasterBe.StatusText, UMS_Resource.MESSAGETYPE_ERROR);
            }
            else//If customer does exists.
            {
                if (string.IsNullOrEmpty(objMasterBe.AccountNo))
                {
                    Message(objMasterBe.StatusText, UMS_Resource.MESSAGETYPE_ERROR);
                    DivCustPaymentDetails.Visible = false;
                    txtAccountNo.Enabled = true;
                }
                else if (objMasterBe.CustomerTypeId == 3)//To check whether the customer is prepaid or not
                {
                    Message("Customer is Prepaid customer.", UMS_Resource.MESSAGETYPE_ERROR);
                    DivCustPaymentDetails.Visible = false;
                    txtAccountNo.Enabled = true;
                }
                else
                {
                    //divCustomerDetailsGrid.Visible = true;
                    DivCustPaymentDetails.Visible = true;
                    txtAccountNo.Enabled = false;
                    lblAccountNo.Text = objMasterBe.AccountNo; //txtAccountNo.Text;
                    lblAccNoAndGlobalAccNo.Text = objMasterBe.AccNoAndGlobalAccNo;
                    lblOldAccountNo.Text = objMasterBe.OldAccountNo;
                    lblName.Text = objMasterBe.Name;
                    lblTariff.Text = objMasterBe.ClassName;
                    lblDue.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(objMasterBe.DueAmount.ToString()), 2, Constants.MILLION_Format).ToString();
                    lblCustomerType.Text = objMasterBe.CustomerType;
                    lblServiceAddress.Text = objMasterBe.Address1;
                    //BindBills();//Binding bill details.
                }
            }
        }

        private void AddPayment()
        {
            MastersBE objMastersBE = InsertPaymentDetails();
            if (objMastersBE != null)
            {
                if (objMastersBE.IsSuccess && objMastersBE.IsCustomerExists)
                {
                    lblPending.Text = objMastersBE.BatchPendingAmount.ToString();
                    // Message(UMS_Resource.PAYMENTENRY_ISERTIONSUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    BindCustomerDetails(); //Raja-ID065 for the purpose of binding customerDetails
                    BindPaymentEntryList();
                    PaidAmount = ReciptNo = string.Empty;
                    mpeCommanMessage.Hide();
                    pop.Attributes.Add("class", "popheader popheaderlblgreen");
                    mpeAlert.Show();
                }
                else
                {
                    Message(objMastersBE.StatusText, UMS_Resource.MESSAGETYPE_ERROR);
                    //Message(UMS_Resource.PAYMENTINSERTION_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            else
                Message(UMS_Resource.PAYMENTINSERTION_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
        }

        //public static void MergeRows(GridView gridView)
        //{
        //    for (int rowIndex = gridView.Rows.Count - 2; rowIndex >= 0; rowIndex--)
        //    {
        //        GridViewRow row = gridView.Rows[rowIndex];
        //        GridViewRow previousRow = gridView.Rows[rowIndex + 1];

        //        Label lblBillNo = (Label)row.FindControl("lblBillNo");
        //        Label lblPreviousBillNo = (Label)previousRow.FindControl("lblBillNo");

        //        if (!string.IsNullOrEmpty(lblPreviousBillNo.Text))
        //        {
        //            for (int i = 0; i < row.Cells.Count; i++)
        //            {
        //                if (i == 0 || i == 2)
        //                {
        //                    if (lblBillNo.Text == lblPreviousBillNo.Text)
        //                    {
        //                        row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
        //                        previousRow.Cells[i].Visible = false;

        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        #endregion

        #region Culture

        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region WebMethod

        [System.Web.Services.WebMethod]
        public static int UpdateTimeElapsed()
        {
            return Constants.SessionPageMethodsSeconds;
        }

        #endregion

    }
}