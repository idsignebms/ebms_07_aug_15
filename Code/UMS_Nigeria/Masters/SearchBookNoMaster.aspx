﻿<%@ Page Title=":: Search BookNo Master ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="SearchBookNoMaster.aspx.cs"
    Inherits="UMS_Nigeria.Masters.SearchBookNoMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <%--  <div class="out-bor">
        <asp:UpdatePanel ID="upStates" runat="server">
            <ContentTemplate>
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddState" runat="server" Text="<%$ Resources:Resource, SEARCH_BOOK%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            &nbsp;
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="litBookNo" runat="server" Text="<%$ Resources:Resource, BOOK_NAME%>"></asp:Literal><br />
                                <asp:TextBox CssClass="text-box" ID="txtBookNo" TabIndex="1" runat="server" MaxLength="20"
                                    placeholder="Enter Book Name"></asp:TextBox>
                                <span id="spanBookNo" class="spancolor"></span>
                            </div>
                            <div class="box_totalThree_a">
                                <asp:Button ID="btnSearch" OnClientClick="return Validate();" TabIndex="3" Text="<%$ Resources:Resource, SEARCH%>"
                                    CssClass="box_s" OnClick="btnSearch_Click" runat="server" />
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, BOOK_CODE%>"></asp:Literal><br />
                                <asp:TextBox CssClass="text-box" ID="txtBookCode" TabIndex="2" runat="server" onkeyup="this.value=this.value.toUpperCase()"
                                    placeholder="Enter Book Code"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbtxtBookCode" runat="server" TargetControlID="txtBookCode"
                                    FilterType="LowercaseLetters,UppercaseLetters,Custom,Numbers" ValidChars="">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanBookCode" class="spancolor"></span>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="grid_boxes">
                        <div>
                            <div class="grid_pagingTwo_top">
                                <div class="paging_top_title">
                                    <asp:Literal ID="litGridStatesList" runat="server" Text="<%$ Resources:Resource, GRID_BOOK_NUMBERS_LIST%>"></asp:Literal>
                                </div>
                                <div class="paging_top_rightTwo_content" id="divPagin1" runat="server">
                                    <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvBookNumbersList" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                            HeaderStyle-CssClass="grid_tb_hd">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no book information.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:BoundField HeaderText="<%$ Resources:Resource, GRID_SNO%>" DataField="RowNumber"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, BOOK_NAME%>"
                                    DataField="BookNo" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, BOOK_CODE%>"
                                    DataField="BookCode" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Old Book" DataField="BookCode"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <%--<asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, DETAILS%>"
                                    DataField="Details" />--%>
    <%--  <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, NO_OF_ACCOUNT%>"
                                    DataField="NoOfAccounts" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, BUSINESS_UNIT_NAME%>"
                                    DataField="BusinessUnitName" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, SERVICE_UNIT_NAME%>"
                                    DataField="ServiceUnitName" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, SERVICE_CENTER_NAME%>"
                                    DataField="ServiceCenterName" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, CYCLE%>"
                                    DataField="CycleName" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, STATE_NAME%>"
                                    DataField="StateName" ItemStyle-CssClass="grid_tb_td" />--%>
    <%--<asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, DETAILS%>"
                                    DataField="Details" />--%>
    <%--   </Columns>
                        </asp:GridView>
                    </div>
                    <div class="grid_boxes">
                        <div id="divPagin2" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                <%--Variables decleration starts--%>
    <%--  <asp:HiddenField ID="hfPageSize" runat="server" />
                <asp:HiddenField ID="hfPageNo" runat="server" />
                <asp:HiddenField ID="hfLastPage" runat="server" />
                <asp:HiddenField ID="hfBookCodeLength" runat="server" />
                <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                <asp:HiddenField ID="hfBookNo" runat="server" Value="" />
                <asp:HiddenField ID="hfBookCode" runat="server" Value="" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>--%>
    <asp:UpdatePanel ID="upStates" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddState" runat="server" Text="<%$ Resources:Resource, SEARCH_BOOK%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            &nbsp;
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="litBookNo" runat="server" Text="<%$ Resources:Resource, BOOK_NAME%>"></asp:Literal><br />
                                <asp:TextBox CssClass="text-box" ID="txtBookNo" TabIndex="1" runat="server" MaxLength="20"
                                    placeholder="Enter Book Name"></asp:TextBox>
                                <span id="spanBookNo" class="spancolor"></span>
                            </div>
                            <div class="box_totalThree_a">
                                <asp:Button ID="btnSearch" OnClientClick="return Validate();" TabIndex="3" Text="<%$ Resources:Resource, SEARCH%>"
                                    CssClass="box_s" OnClick="btnSearch_Click" runat="server" />
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, BOOK_CODE%>"></asp:Literal><br />
                                <asp:TextBox CssClass="text-box" ID="txtBookCode" TabIndex="2" runat="server" onkeyup="this.value=this.value.toUpperCase()"
                                    placeholder="Enter Book Code"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbtxtBookCode" runat="server" TargetControlID="txtBookCode"
                                    FilterType="LowercaseLetters,UppercaseLetters,Custom,Numbers" ValidChars="">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanBookCode" class="spancolor"></span>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="grid_boxes">
                        <div class="grid_pagingTwo_top">
                            <div class="paging_top_titleTwo">
                                <asp:Literal ID="litCountriesList" runat="server" Text="<%$ Resources:Resource, GRID_BOOK_NUMBERS_LIST%>"></asp:Literal>
                            </div>
                            <div class="paging_top_rightTwo_content" id="divPagin1" runat="server">
                                <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvBookNumbersList" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                            HeaderStyle-CssClass="grid_tb_hd" OnRowCommand="gvBookNumbersList_RowCommand"
                            OnRowDataBound="gvBookNumbersList_RowDataBound">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                    HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, BOOK_NAME%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" Visible="True" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridBookNo" runat="server" Text='<%#Eval("BookNo") %>'></asp:Label>
                                        <asp:Label ID="lblActiveStatusId" Visible="false" runat="server" Text='<%#Eval("ActiveStatusId") %>'></asp:Label>
                                        <asp:Label ID="lblActualBookNo" runat="server" Visible="false" Text='<%#Eval("BNO") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridBookName" placeholder="Enter Book Name"
                                            runat="server" Visible="false" MaxLength="20"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, BOOK_CODE%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBookCode" runat="server" Text='<%#Eval("BookCode") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Old Book" Visible="false" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridDetails" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridDetails" placeholder="Enter Details"
                                            runat="server" Visible="false" TextMode="MultiLine"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, NO_OF_ACCOUNT%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridNoOfAccounts" runat="server" Text='<%#Eval("NoOfAccounts") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridNoOfAccounts" MaxLength="10" placeholder="Enter No Of Accounts"
                                            runat="server" Visible="false"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="ftbGridNoOfAcc" runat="server" TargetControlID="txtGridNoOfAccounts"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, STATE_NAME%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblState" runat="server" Text='<%#Eval("StateName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, BU%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridBusinessUnitId" Visible="false" runat="server" Text='<%#Eval("BU_ID") %>'></asp:Label>
                                        <asp:DropDownList ID="ddlGridBusinessUnitName" runat="server" AutoPostBack="true"
                                            Visible="false" OnSelectedIndexChanged="ddlGidBusinessUnitName_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblGridBusinessUnitName" runat="server" Text='<%#Eval("BusinessUnitName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, SU%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridServiceUnitId" Visible="false" runat="server" Text='<%#Eval("SU_ID") %>'></asp:Label>
                                        <asp:DropDownList ID="ddlGridServiceUnitName" runat="server" AutoPostBack="true"
                                            Visible="false" OnSelectedIndexChanged="ddlGridServiceUnitName_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblGridServiceUnitName" runat="server" Text='<%#Eval("ServiceUnitName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, SC%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridServiceCenterId" Visible="false" runat="server" Text='<%#Eval("ServiceCenterId") %>'></asp:Label>
                                        <asp:DropDownList ID="ddlGridServiceCenterName" runat="server" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlGridServiceCenterName_SelectedIndexChanged" Visible="false">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblGridServiceCenterName" runat="server" Text='<%#Eval("ServiceCenterName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, CYCLE%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCycleId" Visible="false" runat="server" Text='<%#Eval("CycleId") %>'></asp:Label>
                                        <asp:Label ID="lblCycleName" runat="server" Text='<%#Eval("CycleName") %>'></asp:Label>
                                        <%--<asp:TextBox CssClass="text-box"  ID="txtGridCycleName" runat="server" Visible="false"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlGridCycle" runat="server" Visible="false">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, MARKETER%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMarketerId" Visible="false" runat="server" Text='<%#Eval("MarketerId") %>'></asp:Label>
                                        <asp:Label ID="lblMarketer" runat="server" Text='<%#Eval("Marketer") %>'></asp:Label>
                                        <%--<asp:TextBox CssClass="text-box"  ID="txtGridCycleName" runat="server" Visible="false"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlGridMarketer" runat="server" Visible="false">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lkbtnEdit" runat="server" ToolTip="Edit" Text="Edit" CommandName="EditBook">
                                        <img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnUpdate" Visible="false" runat="server" ToolTip="Update"
                                            CommandName="UpdateBook"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnCancel" Visible="false" runat="server" ToolTip="Cancel"
                                            CommandName="CancelBook"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnActive" Visible="false" runat="server" ToolTip="Activate Book Number"
                                            CommandName="ActiveBook"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnDeActive" runat="server" ToolTip="DeActivate Book Number"
                                            CommandName="DeActiveBook"><img src="../images/Deactivate.gif" alt="DeActive" /></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <%--<div class="page_na">
                            <uc2:UCPagingV2 ID="UCPagingV2" runat="server" />
                        </div>--%>
                        <asp:HiddenField ID="hfPageSize" runat="server" />
                        <asp:HiddenField ID="hfPageNo" runat="server" />
                        <asp:HiddenField ID="hfLastPage" runat="server" />
                        <asp:HiddenField ID="hfTotalRecords" runat="server" />
                        <asp:HiddenField ID="hfBookCodeLength" runat="server" />
                        <asp:HiddenField ID="hfRecognize" runat="server" />
                        <asp:HiddenField ID="hfBookNumberId" runat="server" />
                        <asp:HiddenField ID="hfBookNo" runat="server" Value="" />
                        <asp:HiddenField ID="hfBookCode" runat="server" Value="" />
                    </div>
                    <div class="grid_boxes">
                        <div id="divPagin2" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="rnkland">
                <asp:ModalPopupExtender ID="mpeCountrypopup" runat="server" TargetControlID="btnpopup"
                    PopupControlID="pnlConfirmation" BackgroundCssClass="popbg">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnpopup" runat="server" Text="" CssClass="popbtn dis_none" />
                <asp:Panel runat="server" ID="pnlConfirmation" DefaultButton="btnOk" CssClass="editpopup_two"
                    Style="display: none;">
                    <div class="panelMessage">
                        <div>
                            <asp:Panel ID="pnlMsgPopup" runat="server" CssClass="aligncenter">
                                <asp:Label ID="lblMsgPopup" runat="server" Text=""></asp:Label>
                            </asp:Panel>
                        </div>
                        <div class="clear pad_10">
                        </div>
                        <div class="buttondiv">
                            <asp:Button ID="btnOk" runat="server" Text="OK" OnClick="btnOk_Click" CssClass="btn_ok"
                                class="m_left108" />
                            <div class="space">
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <%-- Active Btn popup Start--%>
                <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                    TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnActivate" runat="server" Text="Button" CssClass="popbtn dis_none" />
                <asp:Panel ID="PanelActivate" runat="server" CssClass="modalPopup" class="poppnl"
                    Style="display: none;">
                    <div class="popheader popheaderlblgreen">
                        <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                    </div>
                    <div class="space">
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btnActiveOk" runat="server" OnClick="btnActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                            <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- Active  Btn popup Closed--%>
                <%-- DeActive  Btn popup Start--%>
                <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                    TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnDeActivate" runat="server" Text="Button" CssClass="popbtn dis_none" />
                <asp:Panel ID="PanelDeActivate" runat="server" CssClass="modalPopup" Style="display: none;">
                    <div class="popheader popheaderlblred">
                        <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <div class="space">
                            </div>
                            <asp:Button ID="btnDeActiveOk" runat="server" OnClick="btnDeActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                            <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- DeActive  popup Closed--%>
                <%-- Grid Alert popup Start--%>
                <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                    TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                </asp:ModalPopupExtender>
                <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                    <div class="popheader popheaderlblred">
                        <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- Grid Alert popup Closed--%>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--Variables decleration ends--%>

    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".rnkland").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                $(".popbg").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>

    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/PageValidations/AddBookNumbers.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
