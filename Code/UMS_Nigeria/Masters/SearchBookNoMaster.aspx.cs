﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Collections;

namespace UMS_Nigeria.Masters
{
    public partial class SearchBookNoMaster : System.Web.UI.Page
    {
        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        ReportsBal _objRerpotsBal = new ReportsBal();
        MastersBAL objMastersBAL = new MastersBAL();
        XmlDocument xml = null;
        public int PageNum;
        string Key = "SearchBookNoMaster";
        Hashtable htableMaxLength = new Hashtable();

        private bool IsSearchClick = false;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                AssignMaxLength();
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
            }
        }

        private void AssignMaxLength()
        {
            try
            {
                htableMaxLength = _ObjCommonMethods.getApplicationSettings(UMS_Resource.SETTING_BOOK_NUM_LENGTH);
                txtBookCode.Attributes.Add("MaxLength", htableMaxLength[Constants.BookNumbers].ToString());
                hfBookCodeLength.Value = htableMaxLength[Constants.BookNumbers].ToString();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                hfBookNo.Value = txtBookNo.Text.Trim();
                hfBookCode.Value = txtBookCode.Text.Trim();
                IsSearchClick = true;
                BindGrid();
                //UCPaging1.GeneratePaging();
                UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                UCPaging2.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                // Response.Redirect(UMS_Resource.ADD_BOOK_NUMBERS_PAGE);
                //Response.Redirect(UMS_Resource.ADD_SERVICE_UNITS_PAGE + "?" + UMS_Resource.QSTR_BUSINESS_UNIT_CODE + "=" + objiDsignBAL.Encrypt(hfBU_ID.Value), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnAddNewCycle_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_CYCLE_PAGE);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvBookNumbersList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                hfRecognize.Value = string.Empty;
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridNoOfAccounts = (TextBox)row.FindControl("txtGridNoOfAccounts");
                DropDownList ddlGridBusinessUnitName = (DropDownList)row.FindControl("ddlGridBusinessUnitName");
                DropDownList ddlGridServiceUnitName = (DropDownList)row.FindControl("ddlGridServiceUnitName");
                DropDownList ddlGridServiceCenterName = (DropDownList)row.FindControl("ddlGridServiceCenterName");
                TextBox txtGridBookName = (TextBox)row.FindControl("txtGridBookName");
                TextBox txtGridDetails = (TextBox)row.FindControl("txtGridDetails");
                DropDownList ddlGridCycle = (DropDownList)row.FindControl("ddlGridCycle");
                DropDownList ddlGridMarketer = (DropDownList)row.FindControl("ddlGridMarketer");

                Label lblMarketerId = (Label)row.FindControl("lblMarketerId");
                Label lblMarketer = (Label)row.FindControl("lblMarketer");
                Label lblGridBookNo = (Label)row.FindControl("lblGridBookNo");
                Label lblActualBookNo = (Label)row.FindControl("lblActualBookNo");
                Label lblGridNoOfAccounts = (Label)row.FindControl("lblGridNoOfAccounts");
                Label lblGridBusinessUnitName = (Label)row.FindControl("lblGridBusinessUnitName");
                Label lblGridServiceUnitName = (Label)row.FindControl("lblGridServiceUnitName");
                Label lblGridServiceCenterName = (Label)row.FindControl("lblGridServiceCenterName");
                Label lblGridDetails = (Label)row.FindControl("lblGridDetails");
                Label lblGridBusinessUnitId = (Label)row.FindControl("lblGridBusinessUnitId");
                Label lblGridServiceUnitId = (Label)row.FindControl("lblGridServiceUnitId");
                Label lblGridServiceCenterId = (Label)row.FindControl("lblGridServiceCenterId");
                Label lblCycleName = (Label)row.FindControl("lblCycleName");
                Label lblCycleId = (Label)row.FindControl("lblCycleId");

                objMastersBE.Flag = Convert.ToInt32(Resource.FLAG_ACCOUNT_BOOK_NO);
                objMastersBE.BookNo = lblGridBookNo.Text;

                objMastersBE = _ObjCommonMethods.CustomerBillProcessDetails(objMastersBE);

                if (!objMastersBE.Status)
                {
                    switch (e.CommandName.ToUpper())
                    {
                        case "EDITBOOK":
                            lblMarketer.Visible = lblCycleName.Visible = lblGridDetails.Visible = lblGridNoOfAccounts.Visible = lblGridBusinessUnitName.Visible = lblGridServiceUnitName.Visible = lblGridServiceCenterName.Visible = lblGridBookNo.Visible = false;
                            ddlGridMarketer.Visible = ddlGridCycle.Visible = txtGridNoOfAccounts.Visible = ddlGridBusinessUnitName.Visible = ddlGridServiceUnitName.Visible = ddlGridServiceCenterName.Visible = txtGridDetails.Visible = txtGridBookName.Visible = true;
                            txtGridNoOfAccounts.Text = lblGridNoOfAccounts.Text;

                            _ObjCommonMethods.BindBusinessUnits(ddlGridBusinessUnitName, string.Empty, true);
                            _ObjCommonMethods.BindMarketers(ddlGridMarketer, string.Empty, UMS_Resource.DROPDOWN_SELECT, false);
                            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                            {
                                ddlGridBusinessUnitName.SelectedIndex = ddlGridBusinessUnitName.Items.IndexOf(ddlGridBusinessUnitName.Items.FindByValue(lblGridBusinessUnitId.Text));
                                ddlGridBusinessUnitName.Enabled = false;
                                _ObjCommonMethods.BindMarketers(ddlGridMarketer, ddlGridBusinessUnitName.SelectedValue, UMS_Resource.DROPDOWN_SELECT, false);
                                ddlGridMarketer.SelectedIndex = ddlGridMarketer.Items.IndexOf(ddlGridMarketer.Items.FindByValue(lblMarketerId.Text));
                            }

                            ddlGridBusinessUnitName.SelectedIndex = ddlGridBusinessUnitName.Items.IndexOf(ddlGridBusinessUnitName.Items.FindByValue(lblGridBusinessUnitId.Text));
                            ddlGridMarketer.SelectedIndex = ddlGridMarketer.Items.IndexOf(ddlGridMarketer.Items.FindByValue(lblMarketerId.Text));
                            _ObjCommonMethods.BindServiceUnits(ddlGridServiceUnitName, ddlGridBusinessUnitName.SelectedValue, true);
                            ddlGridServiceUnitName.SelectedIndex = ddlGridServiceUnitName.Items.IndexOf(ddlGridServiceUnitName.Items.FindByValue(lblGridServiceUnitId.Text));

                            _ObjCommonMethods.BindServiceCenters(ddlGridServiceCenterName, ddlGridServiceUnitName.SelectedValue, true);
                            ddlGridServiceCenterName.SelectedIndex = ddlGridServiceCenterName.Items.IndexOf(ddlGridServiceCenterName.Items.FindByValue(lblGridServiceCenterId.Text));

                            _ObjCommonMethods.BindCyclesByBUSUSC(ddlGridCycle, ddlGridBusinessUnitName.SelectedValue, ddlGridServiceUnitName.SelectedValue, ddlGridServiceCenterName.SelectedValue, true, UMS_Resource.DROPDOWN_SELECT, false);
                            ddlGridCycle.SelectedIndex = ddlGridCycle.Items.IndexOf(ddlGridCycle.Items.FindByValue(lblCycleId.Text));
                            txtGridDetails.Text = objiDsignBAL.ReplaceNewLines(lblGridDetails.Text, false);
                            txtGridBookName.Text = lblGridBookNo.Text;


                            row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                            row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;
                            break;
                        case "CANCELBOOK":
                            row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                            row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                            ddlGridMarketer.Visible = ddlGridCycle.Visible = txtGridNoOfAccounts.Visible = ddlGridBusinessUnitName.Visible = ddlGridServiceUnitName.Visible = ddlGridServiceCenterName.Visible = txtGridDetails.Visible = txtGridBookName.Visible = false;
                            lblMarketer.Visible = lblCycleName.Visible = lblGridDetails.Visible = lblGridNoOfAccounts.Visible = lblGridBusinessUnitName.Visible = lblGridServiceUnitName.Visible = lblGridServiceCenterName.Visible = lblGridBookNo.Visible = true;
                            break;
                        case "UPDATEBOOK":
                            objMastersBE.BookNo = txtGridBookName.Text.Trim();
                            objMastersBE.BU_ID = ddlGridBusinessUnitName.SelectedValue;
                            objMastersBE.SU_ID = ddlGridServiceUnitName.SelectedValue;
                            //objMastersBE.BookCode = txtGridBookCode.Text;
                            objMastersBE.BNO = lblActualBookNo.Text;
                            objMastersBE.OldSeviceCenterID = lblGridServiceCenterId.Text;
                            objMastersBE.ServiceCenterId = ddlGridServiceCenterName.SelectedValue;
                            objMastersBE.NoOfAccounts = string.IsNullOrEmpty(txtGridNoOfAccounts.Text) ? Constants.Zero : Convert.ToInt32(txtGridNoOfAccounts.Text);
                            objMastersBE.CycleId = ddlGridCycle.SelectedValue;
                            objMastersBE.Details = objiDsignBAL.ReplaceNewLines(txtGridDetails.Text, true);
                            objMastersBE.MarketerId = Convert.ToInt32(ddlGridMarketer.SelectedValue);
                            objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                            objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.BookNumber(objMastersBE, Statement.Update));

                            if (Convert.ToBoolean(objMastersBE.IsSuccess))
                            {
                                BindGrid();
                                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                                Message(UMS_Resource.BOOK_UPDATED, UMS_Resource.MESSAGETYPE_SUCCESS);
                            }
                            else if (objMastersBE.IsBookCodeExists)
                            {
                                Message(Resource.BOOK_NAME_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                            }

                            break;
                        case "ACTIVEBOOK":
                            mpeActivate.Show();
                            //hfBookNumberId.Value = lblGridBookNo.Text;
                            hfBookNumberId.Value = lblActualBookNo.Text;//Neeraj
                            lblActiveMsg.Text = UMS_Resource.ACTIVE_BOOK_POPUP_TEXT;
                            btnActiveOk.Focus();
                            //objMastersBE.ActiveStatusId = Constants.Active;
                            //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                            //objMastersBE.BookNo = lblGridBookNo.Text;
                            //xml = objMastersBAL.BookNumber(objMastersBE, Statement.Delete);
                            //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                            //if (Convert.ToBoolean(objMastersBE.IsSuccess))
                            //{
                            //    BindGridBookNumbersList();
                            //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            //    Message(UMS_Resource.BOOK_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                            //}
                            //else
                            //    Message(UMS_Resource.BOOK_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);

                            break;
                        case "DEACTIVEBOOK":
                            mpeDeActive.Show();
                            hfBookNumberId.Value = lblActualBookNo.Text;
                            lblDeActiveMsg.Text = UMS_Resource.DEACTIVE_BOOK_POPUP_TEXT;
                            btnDeActiveOk.Focus();
                            //objMastersBE.ActiveStatusId = Constants.DeActive;
                            //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                            //objMastersBE.BookNo = lblGridBookNo.Text;
                            //xml = objMastersBAL.BookNumber(objMastersBE, Statement.Delete);
                            //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                            //if (Convert.ToBoolean(objMastersBE.IsSuccess))
                            //{
                            //    BindGridBookNumbersList();
                            //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            //    Message(UMS_Resource.BOOK_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                            //}
                            //else
                            //    Message(UMS_Resource.BOOK_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);

                            break;
                    }

                }
                else
                {
                    btnDeActiveOk.Visible = false;
                    lblDeActiveMsg.Text = Resource.BILL_IN_PROCESS_BOOK_NO;
                    btnDeActiveCancel.Text = Resource.OK;
                    mpeDeActive.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.Active;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.BookNo = hfBookNumberId.Value;
                xml = objMastersBAL.BookNumber(objMastersBE, Statement.Delete);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.BOOK_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else
                    Message(UMS_Resource.BOOK_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.DeActive;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.BookNo = hfBookNumberId.Value;
                xml = objMastersBAL.BookNumber(objMastersBE, Statement.Delete);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (objMastersBE.IsSuccess)
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.BOOK_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else if (!objMastersBE.IsSuccess && objMastersBE.Count > 0)
                {
                    //Message(string.Format(UMS_Resource.BOOK_HAVE_CUSTOMERS, objMastersBE.Count), UMS_Resource.MESSAGETYPE_ERROR);
                    lblgridalertmsg.Text = UMS_Resource.BOOK_HAVE_CUSTOMERS;
                    mpegridalert.Show();
                }
                else
                    Message(UMS_Resource.BOOK_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvBookNumbersList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    DropDownList ddlGridBusinessUnitName = (DropDownList)e.Row.FindControl("ddlGridBusinessUnitName");
                    DropDownList ddlGridServiceUnitName = (DropDownList)e.Row.FindControl("ddlGridServiceUnitName");
                    DropDownList ddlGridServiceCenterName = (DropDownList)e.Row.FindControl("ddlGridServiceCenterName");
                    DropDownList ddlGridMarketer = (DropDownList)e.Row.FindControl("ddlGridMarketer");
                    DropDownList ddlGridCycle = (DropDownList)e.Row.FindControl("ddlGridCycle");
                    //htableMaxLength = _ObjCommonMethods.getApplicationSettings(UMS_Resource.SETTING_BOOK_NUM_LENGTH);
                    //txtGridBookCode.Attributes.Add("MaxLength", htableMaxLength[Constants.BookNumbers].ToString());
                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + ddlGridBusinessUnitName.ClientID + "','"
                                                                                      + ddlGridServiceUnitName.ClientID + "','"
                                                                                      + ddlGridServiceCenterName.ClientID + "','"
                                                                                      + ddlGridCycle.ClientID + "','"
                                                                                      + ddlGridMarketer.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");


                    if (Convert.ToInt32(lblActiveStatusId.Text) == Constants.DeActive)
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlGidBusinessUnitName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlGridBusinessUnitName = (DropDownList)sender;
                DropDownList ddlGridServiceUnitName = (DropDownList)ddlGridBusinessUnitName.NamingContainer.FindControl("ddlGridServiceUnitName");
                DropDownList ddlGridServiceCenterName = (DropDownList)ddlGridBusinessUnitName.NamingContainer.FindControl("ddlGridServiceCenterName");
                DropDownList ddlGridCycle = (DropDownList)ddlGridBusinessUnitName.NamingContainer.FindControl("ddlGridCycle");
                if (ddlGridBusinessUnitName.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindServiceUnits(ddlGridServiceUnitName, ddlGridBusinessUnitName.SelectedValue, true);
                    ddlGridServiceUnitName.Enabled = true;
                    ddlGridCycle.SelectedIndex = ddlGridServiceCenterName.SelectedIndex = ddlGridServiceUnitName.SelectedIndex = 0;
                    ddlGridCycle.Enabled = ddlGridServiceCenterName.Enabled = false;
                }
                else
                {
                    ddlGridCycle.SelectedIndex = ddlGridServiceUnitName.SelectedIndex = ddlGridServiceCenterName.SelectedIndex = 0;
                    ddlGridCycle.Enabled = ddlGridServiceCenterName.Enabled = ddlGridServiceUnitName.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlGridServiceUnitName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlGridServiceUnitName = (DropDownList)sender;
                DropDownList ddlGridServiceCenterName = (DropDownList)ddlGridServiceUnitName.NamingContainer.FindControl("ddlGridServiceCenterName");
                DropDownList ddlGridCycle = (DropDownList)ddlGridServiceUnitName.NamingContainer.FindControl("ddlGridCycle");
                if (ddlGridServiceUnitName.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindServiceCenters(ddlGridServiceCenterName, ddlGridServiceUnitName.SelectedValue, true);
                    ddlGridServiceCenterName.Enabled = true;
                    ddlGridCycle.Enabled = false;
                    ddlGridCycle.SelectedIndex = 0;
                }
                else
                {
                    ddlGridCycle.SelectedIndex = ddlGridServiceCenterName.SelectedIndex = 0;
                    ddlGridCycle.Enabled = ddlGridServiceCenterName.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlGridServiceCenterName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlGridServiceCenterName = (DropDownList)sender;
                DropDownList ddlGridBusinessUnitName = (DropDownList)ddlGridServiceCenterName.NamingContainer.FindControl("ddlGridBusinessUnitName");
                DropDownList ddlGridServiceUnitName = (DropDownList)ddlGridServiceCenterName.NamingContainer.FindControl("ddlGridServiceUnitName");
                DropDownList ddlGridServiceCenter = (DropDownList)ddlGridServiceCenterName.NamingContainer.FindControl("ddlGridServiceCenterName");
                DropDownList ddlGridCycle = (DropDownList)ddlGridServiceCenterName.NamingContainer.FindControl("ddlGridCycle");
                if (ddlGridServiceCenterName.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindCyclesByBUSUSC(ddlGridCycle, ddlGridBusinessUnitName.SelectedValue, ddlGridServiceUnitName.SelectedValue, ddlGridServiceCenter.SelectedValue, true, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlGridCycle.Enabled = true;
                }
                else
                {
                    ddlGridCycle.SelectedIndex = 0;
                    ddlGridCycle.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }


        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void BindGrid()
        {
            try
            {
                MasterSearchBE _objSearchBe = new MasterSearchBE();
                _objSearchBe.PageNo = IsSearchClick ? Convert.ToInt32(Constants.pageNoValue) : Convert.ToInt32(hfPageNo.Value);
                //_objSearchBe.PageNo = Convert.ToInt32(Constants.pageNoValue);
                _objSearchBe.PageSize = PageSize;
                _objSearchBe.Flag = Convert.ToInt32(SearchFlags.BookNoSearch);
                //_objSearchBe.BookNo = txtBookNo.Text.Trim();
                //_objSearchBe.BookCode = txtBookCode.Text.Trim();
                _objSearchBe.BookNo = hfBookNo.Value;
                _objSearchBe.BookCode = hfBookCode.Value;

                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    _objSearchBe.BU_ID = Convert.ToString(Session[UMS_Resource.SESSION_USER_BUID]);
                else
                    _objSearchBe.BU_ID = string.Empty;
                DataSet ds = new DataSet();
                ds = _objRerpotsBal.SearchMasterBAL(_objSearchBe);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    hfTotalRecords.Value = ds.Tables[0].Rows[0]["TotalRecords"].ToString();
                    gvBookNumbersList.DataSource = ds.Tables[0];
                    gvBookNumbersList.DataBind();
                    divPagin1.Visible = divPagin2.Visible = true;
                }
                else
                {
                    hfTotalRecords.Value = 0.ToString();
                    gvBookNumbersList.DataSource = new DataTable();
                    gvBookNumbersList.DataBind();
                    divPagin1.Visible = divPagin2.Visible = false;
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }


        #endregion

        #region Pageing
        /* protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridBookNumbersList();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridBookNumbersList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridBookNumbersList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridBookNumbersList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                BindGridBookNumbersList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                objiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                _ObjCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
                //ddlPageSize.Items.Clear();
                //if (TotalRecords < Constants.PageSizeStarts)
                //{
                //    ListItem item = new ListItem(Constants.PageSizeStarts.ToString(), Constants.PageSizeStarts.ToString());
                //    ddlPageSize.Items.Add(item);
                //}
                //else
                //{
                //    int previousSize = Constants.PageSizeStarts;
                //    for (int i = Constants.PageSizeStarts; i <= TotalRecords; i = i + Constants.PageSizeIncrement)
                //    {
                //        ListItem item = new ListItem(i.ToString(), i.ToString());
                //        ddlPageSize.Items.Add(item);
                //        previousSize = i;
                //    }
                //    if (previousSize < TotalRecords)
                //    {
                //        ListItem item = new ListItem((previousSize + Constants.PageSizeIncrement).ToString(), (previousSize + Constants.PageSizeIncrement).ToString());
                //        ddlPageSize.Items.Add(item);
                //    }
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }*/
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}