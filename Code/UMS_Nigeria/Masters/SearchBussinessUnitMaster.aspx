﻿<%@ Page Title=":: Search BU Master ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master" Theme="Green"
    AutoEventWireup="true" CodeBehind="SearchBussinessUnitMaster.aspx.cs" Inherits="UMS_Nigeria.Masters.SearchBussinessUnitMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <div class="out-bor">
        <asp:UpdatePanel ID="upStates" runat="server">
            <ContentTemplate>
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddBusinessUnit" runat="server" Text="Search Business Unit"></asp:Literal>
                        </div>
                        <div class="star_text">
                            &nbsp;
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="litBusinessUnitName" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT_NAME%>"></asp:Literal><br />
                                <asp:TextBox CssClass="text-box" ID="txtBusinessUnitName" TabIndex="1" runat="server"
                                    MaxLength="300" autocomplete="off" placeholder="Enter Business Unit Name" ></asp:TextBox>
                                <span id="spanBusinessUnitName" class="spancolor"></span>
                            </div>
                            <div class="box_totalThree_a">
                                    <asp:Button ID="btnSearch" TabIndex="3" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s"
                                        runat="server" OnClick="btnSearch_Click" />
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT_CODE%>"></asp:Literal>
                            <asp:TextBox CssClass="text-box" ID="txtBUCode" runat="server"
                            TabIndex="2" autocomplete="off" placeholder="Enter Business Unit Code"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbtxtBUCode" runat="server" TargetControlID="txtBUCode"
                                FilterType="LowercaseLetters,UppercaseLetters,Custom,Numbers" ValidChars="">
                            </asp:FilteredTextBoxExtender>
                            <span id="spanBUCode" class="spancolor"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="clear: both;">
                </div>
                <div class="grid_boxes">
                    <div>
                        <div class="grid_pagingTwo_top">
                            <div class="paging_top_title">
                                <asp:Literal ID="litGridStatesList" runat="server" Text="<%$ Resources:Resource, GRID_BUSINESS_UNITS_LIST%>"></asp:Literal>
                            </div>
                            <div class="paging_top_rightTwo_content" id="divPaging1" runat="server">
                                <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid_tb" id="divgrid" runat="server">
                    <asp:GridView ID="gvBusinessUnitsList" runat="server" AutoGenerateColumns="False"
                        AlternatingRowStyle-CssClass="color" HeaderStyle-CssClass="grid_tb_hd">
                        <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                        <EmptyDataTemplate>
                            There is no business unit information.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:BoundField HeaderText="<%$ Resources:Resource, GRID_SNO%>" DataField="RowNumber"
                                ItemStyle-CssClass="grid_tb_td" />
                            <%--<asp:BoundField HeaderText="<%$ Resources:Resource, BUSINESS_UNIT_ID%>" DataField="BU_ID" />--%>
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, BUSINESS_UNIT_NAME%>"
                                DataField="BusinessUnitName" ItemStyle-CssClass="grid_tb_td" />
                            <asp:BoundField HeaderText="<%$ Resources:Resource, BUSINESS_UNIT_CODE%>" DataField="BusinessUnitCode"
                                ItemStyle-CssClass="grid_tb_td" />
                            <%--<asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, NOTES%>" DataField="Notes" />--%>
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, STATE_NAME%>"
                                DataField="StateName" ItemStyle-CssClass="grid_tb_td" />
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="grid_boxes">
                    <div id="divPaging2" runat="server">
                        <div class="grid_paging_bottom">
                            <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                        </div>
                    </div>
                </div>
                <%--Variables decleration starts--%>
                <asp:HiddenField ID="hfPageSize" runat="server" />
                <asp:HiddenField ID="hfPageNo" runat="server" />
                <asp:HiddenField ID="hfLastPage" runat="server" />
                <asp:HiddenField ID="hfBUCodeLength" runat="server" />
                <asp:HiddenField ID="hfTotalRecords" Value="0" runat="server" />
                <asp:HiddenField ID="hfBuName" runat="server" Value="" />
                <asp:HiddenField ID="hfBuCode" runat="server" Value="" />
                <%--Variables decleration ends--%>
                <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/PageValidations/SearchBussinessUnitMaster.js" type="text/javascript"></script>
    
    <%--==============Validation script ref ends==============--%>
                <%--==============Ajax loading script starts==============--%>
                <script type="text/javascript">
                    var prm = Sys.WebForms.PageRequestManager.getInstance();
                    //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                    prm.add_beginRequest(BeginRequestHandler);
                    // Raised after an asynchronous postback is finished and control has been returned to the browser.
                    prm.add_endRequest(EndRequestHandler);
                    function BeginRequestHandler(sender, args) {
                        //Shows the modal popup - the update progress
                        var popup = $find('<%= modalPopupLoading.ClientID %>');
                        if (popup != null) {
                            popup.show();
                        }
                    }

                    function EndRequestHandler(sender, args) {
                        //Hide the modal popup - the update progress
                        var popup = $find('<%= modalPopupLoading.ClientID %>');
                        if (popup != null) {
                            popup.hide();
                        }
                    }
   
                </script>
                <%--==============Ajax loading script ends==============--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>
