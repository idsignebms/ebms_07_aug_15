﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using System.Xml;
using System.Data;
using UMS_NigeriaBE;
using Resources;
using System.Threading;
using System.Globalization;
using UMS_NigriaDAL;
using System.Drawing;
using UMS_Nigeria.UserControls;
using System.Collections;

namespace UMS_Nigeria.Masters
{
    public partial class SearchBussinessUnitMaster : System.Web.UI.Page
    {
        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        ReportsBal _objRerpotsBal = new ReportsBal();
        public int PageNum;
        string Key = "SearchBUMaster";
        Hashtable htableMaxLength = new Hashtable();

        private bool IsSearchClick = false;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                AssignMaxLength();
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
            }

        }

        private void AssignMaxLength()
        {
            try
            {
                htableMaxLength = objCommonMethods.getApplicationSettings(UMS_Resource.SETTING_BU_CODE_LENGTH);
                txtBUCode.Attributes.Add("MaxLength", htableMaxLength[Constants.BusinessUnits].ToString());
                hfBUCodeLength.Value = htableMaxLength[Constants.BusinessUnits].ToString();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                hfBuName.Value = txtBusinessUnitName.Text.Trim();
                hfBuCode.Value = txtBUCode.Text.Trim();
                IsSearchClick = true;
                BindGrid();
                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                //UCPaging1.GeneratePaging();
                UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                UCPaging2.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void BindGrid()
        {
            try
            {
                MasterSearchBE _objSearchBe = new MasterSearchBE();
                _objSearchBe.PageNo = IsSearchClick ? Convert.ToInt32(Constants.pageNoValue) : Convert.ToInt32(hfPageNo.Value);
                _objSearchBe.PageSize = PageSize;
                _objSearchBe.Flag = Convert.ToInt32(SearchFlags.BussinessUnitSearch);
                //_objSearchBe.BusinessUnit = txtBusinessUnitName.Text.Trim();
                //_objSearchBe.BusinessUnitCode = txtBUCode.Text.Trim();
                _objSearchBe.BusinessUnit = hfBuName.Value;
                _objSearchBe.BusinessUnitCode = hfBuCode.Value;
                //if ((Session[UMS_Resource.SESSION_USER_BUID] != null) && (Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID]) != Convert.ToInt32(Roles.SuperAdmin)))
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    _objSearchBe.BU_ID = Convert.ToString(Session[UMS_Resource.SESSION_USER_BUID]);
                else
                    _objSearchBe.BU_ID = string.Empty;
                //resultedXml = objMastersBAL.GetBusinessUnits(_objSearchBe, ReturnType.Multiple);
                //objMastersListBE = objiDsignBAL.DeserializeFromXml<MastersListBE>(resultedXml);

                DataSet ds = new DataSet();
                ds = _objRerpotsBal.SearchMasterBAL(_objSearchBe);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    int totalRecoreds = Convert.ToInt32(ds.Tables[0].Rows[0]["TotalRecords"]);
                    hfTotalRecords.Value = totalRecoreds.ToString();
                    gvBusinessUnitsList.DataSource = ds.Tables[0];
                    gvBusinessUnitsList.DataBind();
                    divPaging1.Visible = divPaging2.Visible = true;

                }
                else
                {
                    hfTotalRecords.Value = 0.ToString();
                    gvBusinessUnitsList.DataSource = new DataTable();
                    gvBusinessUnitsList.DataBind();
                    divPaging1.Visible = divPaging2.Visible = false;
                }


            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Paging
        /*protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridBusinessUnitsList();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridBusinessUnitsList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridBusinessUnitsList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridBusinessUnitsList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                BindGridBusinessUnitsList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                objiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; }
                else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                objCommonMethods.BindPagingDropDownListReport(TotalRecords, this.PageSize, ddlPageSize);                
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }*/
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}