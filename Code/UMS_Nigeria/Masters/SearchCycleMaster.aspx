﻿<%@ Page Title=":: Search Book Group Master ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="SearchCycleMaster.aspx.cs" Inherits="UMS_Nigeria.Masters.SearchCycleMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script type="text/javascript">
        function pageLoad() {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        };       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <%-- <div class="out-bor">
        <asp:UpdatePanel ID="upStates" runat="server">
            <ContentTemplate>
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text-heading">
                        <asp:Literal ID="litAddCycle" runat="server" Text="<%$ Resources:Resource, SEARCH_CYCLE%>"></asp:Literal>
                    </div>
                    <div class="star_text">
                        &nbsp;
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="litCycleName" runat="server" Text="<%$ Resources:Resource, CYCLE_NAME%>"></asp:Literal>
                                <asp:TextBox CssClass="text-box" ID="txtCycleName" TabIndex="2" runat="server" MaxLength="300"
                                    placeholder="Enter Book Group Name"></asp:TextBox>
                                <span id="spanCycleName" class="spancolor"></span>
                            </div>
                            <div class="box_totalThree_a">
                                <asp:Button ID="btnSearch" TabIndex="3" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s"
                                    runat="server" OnClick="btnSearch_Click" />
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, CYCLE_CODE%>"></asp:Literal>
                                <asp:TextBox CssClass="text-box" ID="txtCycleCode" TabIndex="2" runat="server" onkeyup="this.value=this.value.toUpperCase()"
                                    placeholder="Enter Book Group Code"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbtxtCycleCode" runat="server" TargetControlID="txtCycleCode"
                                    FilterType="LowercaseLetters,UppercaseLetters,Custom,Numbers" ValidChars="">
                                </asp:FilteredTextBoxExtender>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="grid_boxes">
                        <div>
                            <div class="grid_pagingTwo_top">
                                <div class="paging_top_title">
                                    <asp:Literal ID="litGridStatesList" runat="server" Text="<%$ Resources:Resource, GRID_CYCLE_LIST%>"></asp:Literal>
                                </div>
                                <div class="paging_top_rightTwo_content" id="divPagin1" runat="server">
                                    <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvCycleList" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                            HeaderStyle-CssClass="grid_tb_hd">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no Book Group information.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:BoundField HeaderText="<%$ Resources:Resource, GRID_SNO%>" DataField="RowNumber"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField HeaderText="<%$ Resources:Resource, CYCLE_NAME%>" DataField="CycleName"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField HeaderText="<%$ Resources:Resource, CYCLE_CODE%>" DataField="CycleCode"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField HeaderText="<%$ Resources:Resource, BUSINESS_UNIT_NAME%>" DataField="BusinessUnit"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField HeaderText="<%$ Resources:Resource, SERVICE_UNIT_NAME%>" DataField="ServiceUnit"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField HeaderText="<%$ Resources:Resource, SERVICE_CENTER_NAME%>" DataField="ServiceCenter"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, STATE_NAME%>"
                                    DataField="StateName" ItemStyle-CssClass="grid_tb_td" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="grid_boxes">
                        <div id="divPagin2" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                <%--Variables decleration starts--%>
    <%--  <asp:HiddenField ID="hfPageSize" runat="server" />
                <asp:HiddenField ID="hfPageNo" runat="server" />
                <asp:HiddenField ID="hfLastPage" runat="server" />
                <asp:HiddenField ID="hfCycleCodeLength" runat="server" />
                <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                <asp:HiddenField ID="hfCycleName" runat="server" Value="" />
                <asp:HiddenField ID="hfCycleCode" runat="server" Value="" />--%>
    <%--Variables decleration ends--%>
    <%--==============Ajax loading script starts==============--%>
    <%--  <script type="text/javascript">
                    var prm = Sys.WebForms.PageRequestManager.getInstance();
                    //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                    prm.add_beginRequest(BeginRequestHandler);
                    // Raised after an asynchronous postback is finished and control has been returned to the browser.
                    prm.add_endRequest(EndRequestHandler);
                    function BeginRequestHandler(sender, args) {
                        //Shows the modal popup - the update progress
                        var popup = $find('<%= modalPopupLoading.ClientID %>');
                        if (popup != null) {
                            popup.show();
                        }
                    }

                    function EndRequestHandler(sender, args) {
                        //Hide the modal popup - the update progress
                        var popup = $find('<%= modalPopupLoading.ClientID %>');
                        if (popup != null) {
                            popup.hide();
                        }
                    }
   
                </script>
                <%--==============Ajax loading script ends==============--%>
    <%-- </ContentTemplate>
        </asp:UpdatePanel>
    </div>--%>
    <div class="out-bor">
        <asp:UpdatePanel ID="upAddCycle" runat="server">
            <ContentTemplate>
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text-heading">
                        <asp:Literal ID="litAddCycle" runat="server" Text="<%$ Resources:Resource, SEARCH_CYCLE%>"></asp:Literal>
                    </div>
                    <div class="star_text">
                        &nbsp;
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="litCycleName" runat="server" Text="<%$ Resources:Resource, CYCLE_NAME%>"></asp:Literal>
                                <asp:TextBox CssClass="text-box" ID="txtCycleName" TabIndex="2" runat="server" MaxLength="300"
                                    placeholder="Enter Book Group Name"></asp:TextBox>
                                <span id="spanCycleName" class="spancolor"></span>
                            </div>
                            <div class="box_totalThree_a">
                                <asp:Button ID="btnSearch" TabIndex="3" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s"
                                    runat="server" OnClick="btnSearch_Click" />
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, CYCLE_CODE%>"></asp:Literal>
                                <asp:TextBox CssClass="text-box" ID="txtCycleCode" TabIndex="2" runat="server" onkeyup="this.value=this.value.toUpperCase()"
                                    placeholder="Enter Book Group Code"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbtxtCycleCode" runat="server" TargetControlID="txtCycleCode"
                                    FilterType="LowercaseLetters,UppercaseLetters,Custom,Numbers" ValidChars="">
                                </asp:FilteredTextBoxExtender>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="grid_boxes">
                        <div class="grid_pagingTwo_top">
                            <div class="paging_top_title">
                                <asp:Literal ID="litCountriesList" runat="server" Text="<%$ Resources:Resource, GRID_CYCLE_LIST%>"></asp:Literal>
                            </div>
                            <div class="paging_top_rightTwo_content" id="divPagin1" runat="server">
                                <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvCycleList" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                            OnRowCommand="gvCycleList_RowCommand" HeaderStyle-CssClass="grid_tb_hd" OnRowDataBound="gvCycleList_RowDataBound">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                    HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="BG Name" ItemStyle-Width="8%" Visible="false" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridCycleId" runat="server" Text='<%#Eval("CycleId") %>'></asp:Label>
                                        <asp:Label ID="lblActiveStatusId" Visible="false" runat="server" Text='<%#Eval("ActiveStatusId") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="BG Name" ItemStyle-Width="8%" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridCycleName" runat="server" Text='<%#Eval("CycleName") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridCycleName" MaxLength="300" placeholder="Enter BookGroup Name"
                                            runat="server" Visible="false"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, CYCLE_CODE%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCycleCode" runat="server" Text='<%#Eval("CycleCode") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, STATE_NAME%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblState" runat="server" Text='<%#Eval("StateName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, BUSINESS_UNIT_NAME%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridBusinessUnitId" Visible="false" runat="server" Text='<%#Eval("BU_ID") %>'></asp:Label>
                                        <asp:DropDownList ID="ddlGridBusinessUnitName" runat="server" AutoPostBack="true"
                                            Visible="false" OnSelectedIndexChanged="ddlGidBusinessUnitName_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblGridBusinessUnitName" runat="server" Text='<%#Eval("BusinessUnitName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, SERVICE_UNIT_NAME%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridServiceUnitId" Visible="false" runat="server" Text='<%#Eval("SU_ID") %>'></asp:Label>
                                        <asp:DropDownList ID="ddlGridServiceUnitName" runat="server" AutoPostBack="true"
                                            Visible="false" OnSelectedIndexChanged="ddlGridServiceUnitName_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblGridServiceUnitName" runat="server" Text='<%#Eval("ServiceUnitName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SC" ItemStyle-Width="8%" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridServiceCenterId" Visible="false" runat="server" Text='<%#Eval("ServiceCenterId") %>'></asp:Label>
                                        <asp:DropDownList ID="ddlGridServiceCenterName" runat="server" AutoPostBack="true"
                                            Visible="false">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblGridServiceCenterName" runat="server" Text='<%#Eval("ServiceCenterName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, DETAILS%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridDetails" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridDetails" placeholder="Enter Details"
                                            runat="server" Visible="false" TextMode="MultiLine"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lkbtnEdit" runat="server" ToolTip="Edit" Text="Edit" CommandName="EditCycle">
                                        <img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnUpdate" Visible="false" runat="server" ToolTip="Update"
                                            CommandName="UpdateCycle"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnCancel" Visible="false" runat="server" ToolTip="Cancel"
                                            CommandName="CancelCycle"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnActive" Visible="false" runat="server" ToolTip="Activate BookGroup"
                                            CommandName="ActiveCycle"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnDeActive" runat="server" ToolTip="DeActivate BookGroup"
                                            CommandName="DeActiveCycle"><img src="../images/Deactivate.gif" alt="DeActive" /></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <%--<div class="page_na">
                    <uc2:UCPagingV2 ID="UCPagingV2" runat="server" />
                </div>--%>
                        <asp:HiddenField ID="hfPageSize" runat="server" />
                        <asp:HiddenField ID="hfPageNo" runat="server" />
                        <asp:HiddenField ID="hfLastPage" runat="server" />
                        <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                        <asp:HiddenField ID="hfCycleCodeLength" runat="server" />
                        <asp:HiddenField ID="hfRecognize" runat="server" />
                        <asp:HiddenField ID="hfCycleName" runat="server" Value="" />
                        <asp:HiddenField ID="hfCycleCode" runat="server" Value="" />
                        <asp:HiddenField ID="hfCycleId" runat="server" />
                    </div>
                    <div class="grid_boxes">
                        <div id="divPagin2" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="hidepopup">
                    <asp:ModalPopupExtender ID="mpeCountrypopup" runat="server" TargetControlID="btnpopup"
                        PopupControlID="pnlConfirmation" BackgroundCssClass="popbg">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnpopup" runat="server" Text="" Style="display: none;" />
                    <asp:Panel runat="server" ID="pnlConfirmation" DefaultButton="btnOk" CssClass="editpopup_two"
                        Style="display: none;">
                        <div class="panelMessage">
                            <div>
                                <asp:Panel ID="pnlMsgPopup" runat="server" align="center">
                                    <asp:Label ID="lblMsgPopup" runat="server" Text=""></asp:Label>
                                </asp:Panel>
                            </div>
                            <div class="clear pad_10">
                            </div>
                            <div class="buttondiv">
                                <asp:Button ID="btnOk" runat="server" Text="OK" OnClick="btnOk_Click" CssClass="btn_ok"
                                    Style="margin-left: 108px;" />
                                <div class="space">
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Active Btn popup Start--%>
                    <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                        TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelActivate" runat="server" CssClass="modalPopup" Style="display: none;
                        border-color: #639B00;">
                        <div class="popheader" style="background-color: #639B00;">
                            <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                        </div>
                        <div class="space">
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnActiveOk" runat="server" OnClick="btnActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Active  Btn popup Closed--%>
                    <%-- DeActive  Btn popup Start--%>
                    <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                        TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnDeActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelDeActivate" runat="server" CssClass="modalPopup" Style="display: none;
                        border-color: #639B00;">
                        <div class="popheader" style="background-color: red;">
                            <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <div class="space">
                                </div>
                                <asp:Button ID="btnDeActiveOk" runat="server" OnClick="btnDeActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Grid Alert popup Start--%>
                    <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                        TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                        <div class="popheader popheaderlblred">
                            <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Grid Alert popup Closed--%>
                    <%-- DeActive  popup Closed--%>
                    <asp:ModalPopupExtender ID="mpeBillInProcess" runat="server" TargetControlID="hdnCancel1"
                        PopupControlID="pnlBillInProcess" BackgroundCssClass="popbg" CancelControlID="btnBPOK">
                    </asp:ModalPopupExtender>
                    <asp:HiddenField ID="hdnCancel1" runat="server"></asp:HiddenField>
                    <asp:Panel runat="server" ID="pnlBillInProcess" CssClass="modalPopup" Style="display: none;
                        border-color: #639B00;">
                        <div class="popheader" style="background-color: red;">
                            <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, BILL_IN_PROCESS_EDIT_CYCLE%>"></asp:Label>
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <div class="space">
                                </div>
                                <asp:Button ID="btnBPOK" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="btn_ok"
                                    Style="margin-left: 10px;" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <script src="../JavaScript/PageValidations/AddCycle.js" type="text/javascript"></script>
</asp:Content>
