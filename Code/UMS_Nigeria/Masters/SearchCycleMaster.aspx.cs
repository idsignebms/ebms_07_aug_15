﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Collections;

namespace UMS_Nigeria.Masters
{
    public partial class SearchCycleMaster : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        ReportsBal _objRerpotsBal = new ReportsBal();
        public int PageNum;
        string Key = "SearchCycleMaster";
        Hashtable htableMaxLength = new Hashtable();
        private bool IsSearchClick = false;

        MastersBAL objMastersBAL = new MastersBAL();
        XmlDocument xml = null;


        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                AssignMaxLength();
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
            }

        }

        private void AssignMaxLength()
        {
            try
            {
                htableMaxLength = _ObjCommonMethods.getApplicationSettings(UMS_Resource.SETTING_CYCLE_CODE_LENGTH);
                txtCycleCode.Attributes.Add("MaxLength", htableMaxLength[Constants.Cycle].ToString());
                hfCycleCodeLength.Value = htableMaxLength[Constants.Cycle].ToString();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                hfCycleName.Value = txtCycleName.Text.Trim();
                hfCycleCode.Value = txtCycleCode.Text.Trim();
                IsSearchClick = true;
                BindGrid();
                //UCPaging1.GeneratePaging();
                UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                UCPaging2.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvCycleList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridCycleName = (TextBox)row.FindControl("txtGridCycleName");
                DropDownList ddlGridBusinessUnitName = (DropDownList)row.FindControl("ddlGridBusinessUnitName");
                DropDownList ddlGridServiceUnitName = (DropDownList)row.FindControl("ddlGridServiceUnitName");
                DropDownList ddlGridServiceCenterName = (DropDownList)row.FindControl("ddlGridServiceCenterName");
                TextBox txtGridDetails = (TextBox)row.FindControl("txtGridDetails");

                Label lblGridCycleId = (Label)row.FindControl("lblGridCycleId");
                Label lblGridCycleName = (Label)row.FindControl("lblGridCycleName");
                Label lblGridBusinessUnitName = (Label)row.FindControl("lblGridBusinessUnitName");
                Label lblGridServiceUnitName = (Label)row.FindControl("lblGridServiceUnitName");
                Label lblGridServiceCenterName = (Label)row.FindControl("lblGridServiceCenterName");
                Label lblGridDetails = (Label)row.FindControl("lblGridDetails");
                Label lblGridBusinessUnitId = (Label)row.FindControl("lblGridBusinessUnitId");
                Label lblGridServiceUnitId = (Label)row.FindControl("lblGridServiceUnitId");
                Label lblGridServiceCenterId = (Label)row.FindControl("lblGridServiceCenterId");

                hfRecognize.Value = string.Empty;
                objMastersBE.CycleId = lblGridCycleId.Text;
                objMastersBE.Flag = Convert.ToInt16(Resource.FLAG_CYCLE_ID);
                objMastersBE = _ObjCommonMethods.CustomerBillProcessDetails(objMastersBE);
                if (!objMastersBE.Status)
                {
                    switch (e.CommandName.ToUpper())
                    {
                        case "EDITCYCLE":
                            lblGridDetails.Visible = lblGridCycleName.Visible = lblGridBusinessUnitName.Visible = lblGridServiceUnitName.Visible = lblGridServiceCenterName.Visible = false;
                            txtGridCycleName.Visible = ddlGridBusinessUnitName.Visible = ddlGridServiceUnitName.Visible = ddlGridServiceCenterName.Visible = txtGridDetails.Visible = true;
                            txtGridCycleName.Text = lblGridCycleName.Text;
                            txtGridDetails.Text = _ObjiDsignBAL.ReplaceNewLines(lblGridDetails.Text, false);
                            _ObjCommonMethods.BindBusinessUnits(ddlGridBusinessUnitName, string.Empty, true);
                            ddlGridBusinessUnitName.SelectedIndex = ddlGridBusinessUnitName.Items.IndexOf(ddlGridBusinessUnitName.Items.FindByValue(lblGridBusinessUnitId.Text));
                            ddlGridBusinessUnitName.Enabled = false;
                            _ObjCommonMethods.BindServiceUnits(ddlGridServiceUnitName, ddlGridBusinessUnitName.SelectedValue, true);
                            ddlGridServiceUnitName.SelectedIndex = ddlGridServiceUnitName.Items.IndexOf(ddlGridServiceUnitName.Items.FindByValue(lblGridServiceUnitId.Text));
                            _ObjCommonMethods.BindServiceCenters(ddlGridServiceCenterName, ddlGridServiceUnitName.SelectedValue, true);
                            ddlGridServiceCenterName.SelectedIndex = ddlGridServiceCenterName.Items.IndexOf(ddlGridServiceCenterName.Items.FindByValue(lblGridServiceCenterId.Text));
                            row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                            row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;
                            break;
                        case "CANCELCYCLE":
                            row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                            row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                            txtGridDetails.Visible = txtGridCycleName.Visible = ddlGridBusinessUnitName.Visible = ddlGridServiceUnitName.Visible = ddlGridServiceCenterName.Visible = false;
                            lblGridDetails.Visible = lblGridCycleName.Visible = lblGridBusinessUnitName.Visible = lblGridServiceUnitName.Visible = lblGridServiceCenterName.Visible = true;
                            break;
                        case "UPDATECYCLE":
                            objMastersBE.CycleId = lblGridCycleId.Text;
                            objMastersBE.BU_ID = ddlGridBusinessUnitName.SelectedValue;
                            objMastersBE.SU_ID = ddlGridServiceUnitName.SelectedValue;
                            objMastersBE.ServiceCenterId = ddlGridServiceCenterName.SelectedValue;
                            objMastersBE.CycleName = txtGridCycleName.Text;
                            objMastersBE.Details = _ObjiDsignBAL.ReplaceNewLines(txtGridDetails.Text, true);
                            objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                            objMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.Cycle(objMastersBE, Statement.Update));

                            if (Convert.ToBoolean(objMastersBE.IsSuccess))
                            {
                                BindGrid();
                                Message(UMS_Resource.CYCLE_UPDATED, UMS_Resource.MESSAGETYPE_SUCCESS);
                            }
                            else if (objMastersBE.IsCycleExists)
                            {
                                Message(UMS_Resource.CYCLE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                            }
                            else if (objMastersBE.IsCycleNoExists)
                            {
                                Message(UMS_Resource.CYCLE_NUMBER_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                            }
                            else if (objMastersBE.IsCycleCodeExists)
                            {
                                Message(Resource.CYCLE_CODE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                            }
                            else
                            {
                                Message(UMS_Resource.CYCLE_INSERT_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                            }

                            break;
                        case "ACTIVECYCLE":
                            mpeActivate.Show();
                            hfCycleId.Value = lblGridCycleId.Text;
                            lblActiveMsg.Text = UMS_Resource.ACTIVE_CYCLE_POPUP_TEXT;
                            btnActiveOk.Focus();
                            break;
                        case "DEACTIVECYCLE":
                            mpeDeActive.Show();
                            hfCycleId.Value = lblGridCycleId.Text;
                            lblDeActiveMsg.Text = UMS_Resource.DEACTIVE_CYCLE_POPUP_TEXT;
                            btnDeActiveOk.Focus();
                            break;
                    }

                }
                else
                {
                    mpeBillInProcess.Show();
                }
            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.Active;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.CycleId = hfCycleId.Value;
                xml = objMastersBAL.Cycle(objMastersBE, Statement.Delete);
                objMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    Message(UMS_Resource.CYCLE_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else
                    Message(UMS_Resource.CYCLE_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.DeActive;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.CycleId = hfCycleId.Value;
                xml = objMastersBAL.Cycle(objMastersBE, Statement.Delete);
                objMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (objMastersBE.IsSuccess)
                {
                    BindGrid();
                    Message(UMS_Resource.CYCLE_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else if (!objMastersBE.IsSuccess && objMastersBE.Count > 0)
                {
                    lblgridalertmsg.Text = UMS_Resource.CYCLE_HAVE_CUSTOMERS_MSG;
                    mpegridalert.Show();
                }
                else
                    Message(UMS_Resource.CYCLE_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvCycleList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    TextBox txtGridCycleName = (TextBox)e.Row.FindControl("txtGridCycleName");
                    DropDownList ddlGridBusinessUnitName = (DropDownList)e.Row.FindControl("ddlGridBusinessUnitName");
                    DropDownList ddlGridServiceUnitName = (DropDownList)e.Row.FindControl("ddlGridServiceUnitName");
                    DropDownList ddlGridServiceCenterName = (DropDownList)e.Row.FindControl("ddlGridServiceCenterName");

                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridCycleName.ClientID + "','"
                                                                                      + ddlGridBusinessUnitName.ClientID + "','"
                                                                                      + ddlGridServiceUnitName.ClientID + "','"
                                                                                      + ddlGridServiceCenterName.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");
                    //htableMaxLength = _ObjCommonMethods.getApplicationSettings(UMS_Resource.SETTING_CYCLE_CODE_LENGTH);
                    //txtGridCycleCode.Attributes.Add("MaxLength", htableMaxLength[Constants.Cycle].ToString());
                    if (Convert.ToInt32(lblActiveStatusId.Text) == Constants.DeActive)
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlGidBusinessUnitName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlGridBusinessUnitName = (DropDownList)sender;
                DropDownList ddlGridServiceUnitName = (DropDownList)ddlGridBusinessUnitName.NamingContainer.FindControl("ddlGridServiceUnitName");
                DropDownList ddlGridServiceCenterName = (DropDownList)ddlGridBusinessUnitName.NamingContainer.FindControl("ddlGridServiceCenterName");
                if (ddlGridBusinessUnitName.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindServiceUnits(ddlGridServiceUnitName, ddlGridBusinessUnitName.SelectedValue, true);
                    ddlGridServiceUnitName.Enabled = true;
                    ddlGridServiceCenterName.SelectedIndex = ddlGridServiceUnitName.SelectedIndex = 0;
                    ddlGridServiceCenterName.Enabled = false;
                }
                else
                {
                    ddlGridServiceUnitName.SelectedIndex = ddlGridServiceCenterName.SelectedIndex = 0;
                    ddlGridServiceCenterName.Enabled = ddlGridServiceUnitName.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlGridServiceUnitName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlGridServiceUnitName = (DropDownList)sender;
                DropDownList ddlGridServiceCenterName = (DropDownList)ddlGridServiceUnitName.NamingContainer.FindControl("ddlGridServiceCenterName");
                if (ddlGridServiceUnitName.SelectedIndex > 0)
                {
                    _ObjCommonMethods.BindServiceCenters(ddlGridServiceCenterName, ddlGridServiceUnitName.SelectedValue, true);
                    ddlGridServiceCenterName.Enabled = true;
                }
                else
                {
                    ddlGridServiceCenterName.SelectedIndex = 0;
                    ddlGridServiceCenterName.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADD_BOOK_NUMBERS_PAGE + "?" + UMS_Resource.QSTR_CYCLE_ID + "=" + _ObjiDsignBAL.Encrypt(hfCycleId.Value), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void BindGrid()
        {
            try
            {
                MasterSearchBE _objSearchBe = new MasterSearchBE();
                _objSearchBe.PageNo = IsSearchClick ? Convert.ToInt32(Constants.pageNoValue) : Convert.ToInt32(hfPageNo.Value);
                _objSearchBe.PageSize = PageSize;
                _objSearchBe.Flag = Convert.ToInt32(SearchFlags.CycleSearch); ;
                //_objSearchBe.CycleName = txtCycleName.Text.Trim();
                //_objSearchBe.CycleCode = txtCycleCode.Text.Trim();
                _objSearchBe.CycleName = hfCycleName.Value;
                _objSearchBe.CycleCode = hfCycleCode.Value;

                _objSearchBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                DataSet ds = new DataSet();
                ds = _objRerpotsBal.SearchMasterBAL(_objSearchBe);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    hfTotalRecords.Value = ds.Tables[0].Rows[0]["TotalRecords"].ToString();
                    gvCycleList.DataSource = ds.Tables[0];
                    gvCycleList.DataBind();
                    divPagin1.Visible = divPagin2.Visible = true;
                }
                else
                {
                    hfTotalRecords.Value = 0.ToString();
                    gvCycleList.DataSource = new DataTable();
                    gvCycleList.DataBind();
                    divPagin1.Visible = divPagin2.Visible = false;
                }

                //MastersListBE objMastersListBE = new MastersListBE();
                //MastersBE objMastersBE = new MastersBE();
                //XmlDocument resultedXml = new XmlDocument();

                //objMastersBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                //objMastersBE.PageSize = PageSize;
                //if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                //    objMastersBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                //else
                //    objMastersBE.BU_ID = string.Empty;

                //resultedXml = objMastersBAL.Cycle(objMastersBE, ReturnType.Get);
                //objMastersListBE = _ObjiDsignBAL.DeserializeFromXml<MastersListBE>(resultedXml);
                //if (objMastersListBE.Items.Count > 0)
                //{
                //    divgrid.Visible = divpaging.Visible = true;
                //    hfTotalRecords.Value = objMastersListBE.Items[0].TotalRecords.ToString();
                //    gvCycleList.DataSource = objMastersListBE.Items;
                //    gvCycleList.DataBind();
                //}
                //else
                //{
                //    hfTotalRecords.Value = objMastersListBE.Items.Count.ToString();
                //    divgrid.Visible = divpaging.Visible = false;
                //    gvCycleList.DataSource = new DataTable();
                //    gvCycleList.DataBind();
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }


        #endregion

        #region Pageing
        /*protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridCycleList();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridCycleList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridCycleList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridCycleList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                BindGridCycleList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                _ObjiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                _ObjCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
                //ddlPageSize.Items.Clear();
                //if (TotalRecords < Constants.PageSizeStarts)
                //{
                //    ListItem item = new ListItem(Constants.PageSizeStarts.ToString(), Constants.PageSizeStarts.ToString());
                //    ddlPageSize.Items.Add(item);
                //}
                //else
                //{
                //    int previousSize = Constants.PageSizeStarts;
                //    for (int i = Constants.PageSizeStarts; i <= TotalRecords; i = i + Constants.PageSizeIncrement)
                //    {
                //        ListItem item = new ListItem(i.ToString(), i.ToString());
                //        ddlPageSize.Items.Add(item);
                //        previousSize = i;
                //    }
                //    if (previousSize < TotalRecords)
                //    {
                //        ListItem item = new ListItem((previousSize + Constants.PageSizeIncrement).ToString(), (previousSize + Constants.PageSizeIncrement).ToString());
                //        ddlPageSize.Items.Add(item);
                //    }
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }*/
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}