﻿<%@ Page Title=":: Search MeterInfo Master ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="SearchMeterInformationMaster.aspx.cs"
    Inherits="UMS_Nigeria.Masters.SearchMeterInformationMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <div class="out-bor">
        <asp:UpdatePanel ID="upStates" runat="server">
            <ContentTemplate>
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text-heading">
                        <asp:Literal ID="litReadMethod" runat="server" Text="Search Meter Information"></asp:Literal>
                    </div>
                    <div class="star_text">
                        &nbsp;
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="litMeterNo" runat="server" Text="<%$ Resources:Resource, METERNO%>"></asp:Literal>
                                <div class="space">
                                </div>
                                <asp:TextBox CssClass="text-box" ID="txtMeterNo" TabIndex="1" MaxLength="50" placeholder="Enter Meter No"
                                    runat="server"></asp:TextBox>
                                <span id="spanMeterNo" class="spancolor"></span>
                            </div>
                            <div class="text-inner">
                                <asp:Literal ID="litMeterType" runat="server" Text="<%$ Resources:Resource, METER_NAME%>"></asp:Literal>
                                <div class="space">
                                </div>
                                <asp:DropDownList ID="ddlMeterType" runat="server" TabIndex="3" CssClass="text-box text-select">
                                    <asp:ListItem Text="--Select--"></asp:ListItem>
                                </asp:DropDownList>
                                <span id="spanMeterType" class="spancolor"></span>
                            </div>
                            <div class="box_totalTwi_c" style="margin-left: -9px;">
                                <asp:Button ID="Button1" TabIndex="4" OnClientClick="return Validate();" OnClick="btnSearch_Click"
                                    CssClass="box_s" Text="<%$ Resources:Resource, SEARCH%>" runat="server" />
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <asp:Literal ID="litMeterSerialNo" runat="server" Text="<%$ Resources:Resource, METER_SERIAL_NO%>"></asp:Literal>
                                <div class="space">
                                </div>
                                <asp:TextBox CssClass="text-box" ID="txtSerialNo" TabIndex="2" MaxLength="50" placeholder="Enter Meter Serial Number"
                                    runat="server"></asp:TextBox>
                                <span id="spanSerialNo" class="spancolor"></span>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <asp:Literal ID="litBrand" runat="server" Text="<%$ Resources:Resource, BRAND%>"></asp:Literal>
                                <div class="space">
                                </div>
                                <asp:TextBox CssClass="text-box" ID="txtBrand" TabIndex="3" MaxLength="50" runat="server"
                                    placeholder="Enter Meter Brand"></asp:TextBox>
                                <span id="spanBrand" class="spancolor"></span>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="grid_boxes">
                        <div>
                            <div class="grid_pagingTwo_top">
                                <div class="paging_top_title">
                                    <asp:Literal ID="litGridStatesList" runat="server" Text="Meter Information List"></asp:Literal>
                                </div>
                                <div class="paging_top_rightTwo_content" id="divPagin1" runat="server">
                                    <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <%--   <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvMetersList" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                            HeaderStyle-CssClass="grid_tb_hd" OnRowDataBound="gvMetersList_RowDataBound">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no meter information.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:BoundField HeaderText="<%$ Resources:Resource, GRID_SNO%>" DataField="RowNumber"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, METERNO%>"
                                    DataField="MeterNo" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField HeaderText="<%$ Resources:Resource, METER_SERIAL_NO%>" DataField="MeterSerialNo"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, METER_TYPE%>"
                                    DataField="MeterType" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, SIZE%>"
                                    DataField="MeterSize" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField HeaderText="<%$ Resources:Resource, MULTIPLIER%>" DataField="MeterMultiplier"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, BRAND%>"
                                    DataField="MeterBrand" />
                                <asp:BoundField HeaderText="<%$ Resources:Resource, METER_RATING%>" DataField="MeterRating"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField HeaderText="<%$ Resources:Resource, NEXT_CALIBRATION_DATE%>" DataField="NextCalibrationDate"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField HeaderText="<%$ Resources:Resource, METER_DIALS%>" DataField="MeterDials"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:TemplateField HeaderText="Is CAPMI Meter?" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIsCAPMI" runat="server" Text='<%#Eval("IsCAPMIMeter") %>'></asp:Label>
                                        <asp:RadioButtonList ID="rblGIsCAPMI" runat="server" RepeatDirection="Horizontal"
                                            Style="width: 150px;"  Visible="false"  OnSelectedIndexChanged="rblIsCAPMI_Changed" AutoPostBack="true">
                                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="2"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CAPMI Amount" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCAPMIAmount" runat="server" Text='<%#Eval("CAPMIAmount") %>'></asp:Label>
                                          <asp:TextBox CssClass="text-box" ID="txtGAmount" TabIndex="12" MaxLength="20" runat="server"
                                            onkeypress="return isNumberKey(event,this)" onkeyup="GetCurrencyFormate(this);"
                                            Visible="false" onblur="GetCurrencyFormate(this);" placeholder="Enter CAPMI Amount"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="GFilteredTextBoxExtender1" runat="server" TargetControlID="txtGAmount"
                                            FilterType="Numbers,Custom" ValidChars=",.">
                                        </asp:FilteredTextBoxExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblActiveStatus" runat="server" Text='<%#Eval("ActiveStatus") %>'
                                            Visible="false"></asp:Label>
                                        <asp:Label ID="lblHyphen" Text="--" Visible="false" runat="server"></asp:Label>
                                        <asp:LinkButton ID="lkbtnEdit" runat="server" ToolTip="Edit" Text="Edit" CommandName="EditMeter">
                                        <img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnUpdate" Visible="false" runat="server" ToolTip="Update"
                                            CommandName="UpdateMeter"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnCancel" Visible="false" runat="server" ToolTip="Cancel"
                                            CommandName="CancelMeter"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnActive" Visible="false" runat="server" ToolTip="Activate Meter Information"
                                            CommandName="ActiveMeter"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnDeActive" runat="server" ToolTip="DeActivate Meter Information"
                                            CommandName="DeActiveMeter"><img src="../images/Deactivate.gif" alt="DeActive" /></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, DETAILS%>"
                                    DataField="Details" />
                  
                    </Columns> </asp:GridView>
                </div>--%>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvMetersList" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                            HeaderStyle-CssClass="grid_tb_hd" OnRowCommand="gvMetersList_RowCommand" OnRowDataBound="gvMetersList_RowDataBound">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no meter information.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                    HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="grid_tb_td">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        <asp:Label ID="lblMeterId" Visible="false" runat="server" Text='<%#Eval("MeterId")%>'></asp:Label>
                                        <asp:Label ID="lblActiveStatusId" Visible="false" runat="server" Text='<%#Eval("ActiveStatusId")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, METERNO%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMeterNo" runat="server" Text='<%#Eval("MeterNo") %>'></asp:Label>
                                        <asp:Label ID="lblGridMeterNo" Visible="false" runat="server" Enabled="false"></asp:Label>
                                        <%-- <asp:TextBox CssClass="text-box" ID="txtGridMeterNo" Visible="false" MaxLength="50"
                                            placeholder="Enter Meter No" runat="server" Enabled="false"></asp:TextBox>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ resources:resource, account_no%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblglobalaccno" runat="server" Text='<%#Eval("accountno") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, METER_SERIAL_NO%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMeterSerialNo" runat="server" Text='<%#Eval("MeterSerialNo") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridSerialNo" Visible="false" MaxLength="50"
                                            placeholder="Enter Meter Serial No" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, METER_TYPE%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlGridMeterType" runat="server" Visible="false">
                                            <asp:ListItem Text="--Select--"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:Label ID="lblMeterType" runat="server" Text='<%#Eval("MeterType") %>'></asp:Label>
                                        <asp:Label ID="lblMeterTypeId" runat="server" Text='<%#Eval("MeterTypeId") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, SIZE%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMeterSize" runat="server" Text='<%#Eval("MeterSize") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridSize" Visible="false" MaxLength="50"
                                            placeholder="Enter Meter Size" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, MULTIPLIER%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMeterMultiplier" runat="server" Text='<%#Eval("MeterMultiplier") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridMultiplier" Visible="false" MaxLength="50"
                                            runat="server" placeholder="Enter Multiplier"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="ftbMultiplier" runat="server" TargetControlID="txtGridMultiplier"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, BRAND%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMeterBrand" runat="server" Text='<%#Eval("MeterBrand") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridBrand" Visible="false" MaxLength="50"
                                            runat="server" placeholder="Enter Meter Brand"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, METER_RATING%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" Visible="false" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMeterRating" runat="server" Text='<%#Eval("MeterRating") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridMeterRating" Visible="false" runat="server"
                                            MaxLength="50" placeholder="Enter Meter Rating"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="ftbGridMeterRating" runat="server" TargetControlID="txtGridMeterRating"
                                            FilterType="Numbers" ValidChars="">
                                        </asp:FilteredTextBoxExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, NEXT_CALIBRATION_DATE%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Center" Visible="false" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNextCalibrationDate" runat="server" Text='<%#Eval("NextCalibrationDate") %>'></asp:Label>
                                        <asp:TextBox ID="txtGridNextDate" palceholder="Enter dd/mm/yyyy Format" MaxLength="15"
                                            Visible="false" Width="133px" runat="server"></asp:TextBox>
                                        <%--Text='<%#Eval("BillDate") %>'<asp:Image ID="imgDate1" Visible="false" CssClass="cssimgNextDate" ImageUrl="~/images/icon_calendar.png"
                                        Style="width: 20px; vertical-align: middle" AlternateText="Calender" runat="server" />--%>
                                        <asp:FilteredTextBoxExtender ID="ftbNextDate" runat="server" TargetControlID="txtGridNextDate"
                                            FilterType="Custom,Numbers" ValidChars="/">
                                        </asp:FilteredTextBoxExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, SERVICE_HOURS_BEFORE_CALIBRATION%>"
                                    ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblServiceHoursBeforeCalibration" runat="server" Text='<%#Eval("ServiceHoursBeforeCalibration") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridServiceBeforeCalibration" Visible="false"
                                            MaxLength="50" runat="server" placeholder="Enter Service Hours"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="ftbServiceBeforeCalibration" runat="server" TargetControlID="txtGridServiceBeforeCalibration"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, METER_DIALS%>" ItemStyle-Width="5%"
                                    ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMeterDials" runat="server" Text='<%#Eval("MeterDials") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridDials" Visible="false" MaxLength="1"
                                            runat="server" placeholder="Enter Meter Dials"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3a" runat="server" TargetControlID="txtGridDials"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Is CAPMI Meter?" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIsCAPMI" runat="server" Text='<%#Eval("IsCAPMIMeter") %>'></asp:Label>
                                        <asp:RadioButtonList ID="rblGIsCAPMI" runat="server" RepeatDirection="Horizontal"
                                            Style="width: 150px;" Visible="false" OnSelectedIndexChanged="rblIsCAPMI_Changed"
                                            AutoPostBack="true">
                                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="2"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CAPMI Amount" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCAPMIAmount" runat="server" Text='<%#Eval("CAPMIAmount") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGAmount" TabIndex="12" MaxLength="12" runat="server"
                                            onkeypress="return isNumberKey(event,this)" onkeyup="GetCurrencyFormate(this);"
                                            Visible="false" onblur="GetCurrencyFormate(this);" placeholder="Enter CAPMI Amount"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="GFilteredTextBoxExtender1" runat="server" TargetControlID="txtGAmount"
                                            FilterType="Numbers,Custom" ValidChars=",.">
                                        </asp:FilteredTextBoxExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, DETAILS%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" Visible="false" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDetails" runat="server" Text='<%#Eval("MeterDetails") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridDetails" placeholder="Enter Details"
                                            runat="server" Visible="false" TextMode="MultiLine"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblActiveStatus" runat="server" Text='<%#Eval("ActiveStatus") %>'
                                            Visible="false"></asp:Label>
                                        <asp:Label ID="lblHyphen" Text="--" Visible="false" runat="server"></asp:Label>
                                        <asp:LinkButton ID="lkbtnEdit" runat="server" ToolTip="Edit" Text="Edit" CommandName="EditMeter">
                                        <img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnUpdate" Visible="false" runat="server" ToolTip="Update"
                                            CommandName="UpdateMeter"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnCancel" Visible="false" runat="server" ToolTip="Cancel"
                                            CommandName="CancelMeter"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnActive" Visible="false" runat="server" ToolTip="Activate Meter Information"
                                            CommandName="ActiveMeter"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnDeActive" runat="server" ToolTip="DeActivate Meter Information"
                                            CommandName="DeActiveMeter"><img src="../images/Deactivate.gif" alt="DeActive" /></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="grid_boxes">
                        <div id="divPagin2" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                    <%-- Active Btn popup Start--%>
                    <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                        TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelActivate" runat="server" CssClass="modalPopup" Style="display: none;
                        border-color: #639B00;">
                        <div class="popheader" style="background-color: #639B00;">
                            <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                        </div>
                        <br />
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnActiveOk" runat="server" OnClick="btnActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Active  Btn popup Closed--%>
                    <%-- DeActive  Btn popup Start--%>
                    <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                        TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnDeActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelDeActivate" runat="server" CssClass="modalPopup" Style="display: none;
                        border-color: #639B00;">
                        <div class="popheader" style="background-color: red;">
                            <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnDeActiveOk" runat="server" OnClick="btnDeActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- DeActive  popup Closed--%>
                    <%-- Grid Alert popup Start--%>
                    <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                        TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                        <div class="popheader popheaderlblred">
                            <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Grid Alert popup Closed--%>
                </div>
                <%--Variables decleration starts--%>
                <asp:HiddenField ID="hfPageSize" runat="server" />
                <asp:HiddenField ID="hfPageNo" runat="server" />
                <asp:HiddenField ID="hfLastPage" runat="server" />
                <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                <asp:HiddenField ID="hfMeterNo" runat="server" Value="" />
                <asp:HiddenField ID="hfMeterSerialNo" runat="server" Value="" />
                <asp:HiddenField ID="hfBrand" runat="server" Value="" />
                <asp:HiddenField ID="hfMeterType" runat="server" Value="" />
                <asp:HiddenField ID="hfId" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--Variables decleration ends--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <script src="../JavaScript/PageValidations/MeterInformation.js" type="text/javascript"></script>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
</asp:Content>
