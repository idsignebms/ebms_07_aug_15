﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;

namespace UMS_Nigeria.Masters
{
    public partial class SearchMeterInformationMaster : System.Web.UI.Page
    {
        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        ReportsBal _objRerpotsBal = new ReportsBal();
        MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        string Key = "SearchMeterInfoMaster";

        private bool IsSearchClick = false;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }

        public string MeterType
        {
            get { return string.IsNullOrEmpty(ddlMeterType.SelectedValue) ? string.Empty : ddlMeterType.SelectedItem.Text; }
        }

        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                objCommonMethods.BindMeterTypes(ddlMeterType, true);
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                txtMeterNo.Text = txtMeterNo.Text.TrimStart();
                txtMeterNo.Text = txtMeterNo.Text.Trim();

                txtSerialNo.Text = txtSerialNo.Text.TrimStart();
                txtSerialNo.Text = txtSerialNo.Text.Trim();

                txtBrand.Text = txtBrand.Text.TrimStart();
                txtBrand.Text = txtBrand.Text.Trim();

                hfMeterNo.Value = txtMeterNo.Text.Trim();
                hfMeterSerialNo.Value = txtSerialNo.Text.Trim();
                hfBrand.Value = txtBrand.Text.Trim();
                hfMeterType.Value = MeterType;
                IsSearchClick = true;
                BindGrid();
                //UCPaging1.GeneratePaging();
                UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                UCPaging2.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.Active;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.MeterId = Convert.ToInt32(hfId.Value);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.MeterType(objMastersBE, Statement.Modify));

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.METER_INFO_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else
                    Message(UMS_Resource.METER_INFO_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.DeActive;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.MeterId = Convert.ToInt32(hfId.Value);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.MeterType(objMastersBE, Statement.Modify));

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    Message(UMS_Resource.METER_INFO_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else
                    Message("This Meter is already assigned to some customer, You can not change the Meter Information.", UMS_Resource.MESSAGETYPE_ERROR);//UMS_Resource.METER_INFO_DEACTIVATE_FAIL
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvMetersList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                Label txtGridMeterNo = (Label)row.FindControl("lblGridMeterNo");
                TextBox txtGridSerialNo = (TextBox)row.FindControl("txtGridSerialNo");
                DropDownList ddlGridMeterType = (DropDownList)row.FindControl("ddlGridMeterType");
                TextBox txtGridSize = (TextBox)row.FindControl("txtGridSize");
                TextBox txtGridMultiplier = (TextBox)row.FindControl("txtGridMultiplier");
                TextBox txtGridBrand = (TextBox)row.FindControl("txtGridBrand");
                TextBox txtGridMeterRating = (TextBox)row.FindControl("txtGridMeterRating");
                TextBox txtGridNextDate = (TextBox)row.FindControl("txtGridNextDate");
                //System.Web.UI.WebControls.Image imgDate1 = (System.Web.UI.WebControls.Image)row.FindControl("imgDate1");
                TextBox txtGridServiceBeforeCalibration = (TextBox)row.FindControl("txtGridServiceBeforeCalibration");
                TextBox txtGridDials = (TextBox)row.FindControl("txtGridDials");
                //TextBox txtGridDecimal = (TextBox)row.FindControl("txtGridDecimal");
                TextBox txtGridDetails = (TextBox)row.FindControl("txtGridDetails");

                Label lblMeterId = (Label)row.FindControl("lblMeterId");
                Label lblMeterNo = (Label)row.FindControl("lblMeterNo");
                Label lblMeterSize = (Label)row.FindControl("lblMeterSize");
                Label lblMeterType = (Label)row.FindControl("lblMeterType");
                Label lblMeterMultiplier = (Label)row.FindControl("lblMeterMultiplier");
                Label lblMeterTypeId = (Label)row.FindControl("lblMeterTypeId");
                Label lblMeterBrand = (Label)row.FindControl("lblMeterBrand");
                Label lblMeterSerialNo = (Label)row.FindControl("lblMeterSerialNo");
                Label lblMeterRating = (Label)row.FindControl("lblMeterRating");
                Label lblNextCalibrationDate = (Label)row.FindControl("lblNextCalibrationDate");
                Label lblServiceHoursBeforeCalibration = (Label)row.FindControl("lblServiceHoursBeforeCalibration");
                Label lblMeterDials = (Label)row.FindControl("lblMeterDials");
                //Label lblDecimals = (Label)row.FindControl("lblDecimals");
                Label lblDetails = (Label)row.FindControl("lblDetails");


                RadioButtonList rblGIsCAPMI = (RadioButtonList)row.FindControl("rblGIsCAPMI");
                TextBox txtGAmount = (TextBox)row.FindControl("txtGAmount");
                Label lblIsCAPMI = (Label)row.FindControl("lblIsCAPMI");
                Label lblCAPMIAmount = (Label)row.FindControl("lblCAPMIAmount");

                switch (e.CommandName.ToUpper())
                {
                    case "EDITMETER": //
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;
                        lblMeterNo.Visible = lblMeterSize.Visible = lblMeterType.Visible = lblMeterMultiplier.Visible = lblMeterBrand.Visible = lblMeterSerialNo.Visible = lblMeterSerialNo.Visible = lblMeterRating.Visible = lblNextCalibrationDate.Visible = lblMeterDials.Visible = lblDetails.Visible = lblIsCAPMI.Visible = lblCAPMIAmount.Visible = false;
                        txtGridMeterNo.Visible = txtGridSize.Visible = txtGridSerialNo.Visible = ddlGridMeterType.Visible = txtGridMultiplier.Visible = txtGridBrand.Visible = txtGridMeterRating.Visible = txtGridNextDate.Visible = rblGIsCAPMI.Visible = txtGAmount.Visible = true;
                        //imgDate1.Visible =
                        txtGridMeterNo.Text = lblMeterNo.Text;
                        txtGridSerialNo.Text = lblMeterSerialNo.Text;
                        objCommonMethods.BindMeterTypes(ddlGridMeterType, true);
                        ddlGridMeterType.SelectedIndex = ddlGridMeterType.Items.IndexOf(ddlGridMeterType.Items.FindByValue(lblMeterTypeId.Text));
                        txtGridSize.Text = lblMeterSize.Text;
                        txtGridMultiplier.Text = lblMeterMultiplier.Text;
                        txtGridBrand.Text = lblMeterBrand.Text;
                        txtGridMeterRating.Text = lblMeterRating.Text;
                        txtGridNextDate.Text = lblNextCalibrationDate.Text;
                        txtGridServiceBeforeCalibration.Text = lblServiceHoursBeforeCalibration.Text;
                        txtGridDials.Text = lblMeterDials.Text;
                        // txtGridDecimal.Text = lblDecimals.Text;
                        txtGridDetails.Text = objiDsignBAL.ReplaceNewLines(lblDetails.Text, false);


                        if (lblIsCAPMI.Text == "No")
                        {
                            rblGIsCAPMI.SelectedIndex = 1;
                            txtGAmount.Text = "0.00";
                            txtGAmount.Enabled = false;
                        }
                        else
                        {
                            rblGIsCAPMI.SelectedIndex = 0;
                            txtGAmount.Enabled = true;
                            txtGAmount.Text = lblCAPMIAmount.Text;
                        }
                        break;
                    case "CANCELMETER":
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                        lblMeterNo.Visible = lblMeterSize.Visible = lblMeterType.Visible = lblMeterMultiplier.Visible = lblMeterBrand.Visible = lblMeterSerialNo.Visible = lblMeterSerialNo.Visible = lblMeterRating.Visible = lblNextCalibrationDate.Visible = lblMeterDials.Visible = lblDetails.Visible = lblIsCAPMI.Visible = lblCAPMIAmount.Visible = true;
                        txtGridMeterNo.Visible = txtGridSize.Visible = txtGridSerialNo.Visible = ddlGridMeterType.Visible = txtGridMultiplier.Visible = txtGridBrand.Visible = txtGridMeterRating.Visible = txtGridNextDate.Visible = txtGridDials.Visible = txtGridDetails.Visible = rblGIsCAPMI.Visible = txtGAmount.Visible = false;
                        break;
                    case "UPDATEMETER":
                        objMastersBE.MeterId = Convert.ToInt32(lblMeterId.Text);
                        objMastersBE.MeterNo = txtGridMeterNo.Text;
                        objMastersBE.MeterSerialNo = txtGridSerialNo.Text;
                        objMastersBE.MeterType = ddlGridMeterType.SelectedValue;
                        objMastersBE.MeterSize = txtGridSize.Text;
                        objMastersBE.Details = objiDsignBAL.ReplaceNewLines(txtGridDetails.Text, true);
                        objMastersBE.MeterMultiplier = txtGridMultiplier.Text;
                        objMastersBE.MeterBrand = txtGridBrand.Text;
                        objMastersBE.MeterRating = txtGridMeterRating.Text;
                        if (!string.IsNullOrEmpty(txtGridNextDate.Text))
                            objMastersBE.NextCalibrationDate = objCommonMethods.Convert_MMDDYY(txtGridNextDate.Text);
                        else
                            objMastersBE.NextCalibrationDate = string.Empty;
                        objMastersBE.ServiceHoursBeforeCalibration = txtGridServiceBeforeCalibration.Text;
                        objMastersBE.MeterDials = Convert.ToInt32(txtGridDials.Text);
                        //objMastersBE.Decimals = Convert.ToInt32(txtGridDecimal.Text);
                        objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();

                        if (rblGIsCAPMI.SelectedValue == "1")
                            objMastersBE.IsCAPMIMeter = true;
                        else
                            objMastersBE.IsCAPMIMeter = false;

                        objMastersBE.CAPMIAmount = Convert.ToDecimal(txtGAmount.Text);


                        objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.MeterType(objMastersBE, Statement.Check));

                        if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        {
                            BindGrid();
                            Message(UMS_Resource.METER_INFO_UPDATED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        else if (objMastersBE.IsMeterNoExists)
                        {
                            Message(UMS_Resource.METERNO_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else if (objMastersBE.IsMeterSerialNoExists)
                        {
                            Message(UMS_Resource.METER_SERIALNO_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else
                            Message("This Meter is already assigned to some customer, You can not change the Meter Information.", UMS_Resource.MESSAGETYPE_ERROR);//UMS_Resource.METER_INFO_DEACTIVATE_FAIL

                        break;
                    case "ACTIVEMETER":
                        mpeActivate.Show();
                        hfId.Value = lblMeterId.Text;
                        lblActiveMsg.Text = "Are you sure you want to Activate this Meter Information?";
                        btnActiveOk.Focus();
                        //objMastersBE.ActiveStatusId = Constants.Active;
                        //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objMastersBE.MeterId = Convert.ToInt32(lblMeterId.Text);
                        //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.MeterType(objMastersBE, Statement.Modify));

                        //if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        //{
                        //    BindGrid();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message(UMS_Resource.METER_INFO_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                        //}
                        //else
                        //    Message(UMS_Resource.METER_INFO_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "DEACTIVEMETER":
                        mpeDeActive.Show();
                        hfId.Value = lblMeterId.Text;
                        lblDeActiveMsg.Text = "Are you sure you want to DeActivate this Meter Information?";
                        btnDeActiveOk.Focus();
                        //objMastersBE.ActiveStatusId = Constants.DeActive;
                        //objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objMastersBE.MeterId = Convert.ToInt32(lblMeterId.Text);
                        //objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.MeterType(objMastersBE, Statement.Modify));

                        //if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        //{
                        //    BindGrid();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message(UMS_Resource.METER_INFO_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                        //}
                        //else
                        //    Message(UMS_Resource.METER_INFO_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                }
            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void rblIsCAPMI_Changed(object Sender, EventArgs e)
        {
            try
            {
                RadioButtonList rblList = (RadioButtonList)Sender;
                var row = (GridViewRow)rblList.NamingContainer;
                TextBox txtCapmi = (TextBox)row.FindControl("txtGAmount");
                if (rblList.SelectedIndex == 0)
                {
                    txtCapmi.Enabled = true;
                }
                else
                {
                    txtCapmi.Text = "0.00";
                    txtCapmi.Enabled = false;
                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        #region Paging
        /*protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindMetersList();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindMetersList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindMetersList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindMetersList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                BindMetersList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                objiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }*/
        #endregion

        #region Methods
        public void BindGrid()
        {
            try
            {
                MasterSearchBE _objSearchBe = new MasterSearchBE();
                _objSearchBe.PageNo = IsSearchClick ? Convert.ToInt32(Constants.pageNoValue) : Convert.ToInt32(hfPageNo.Value);
                _objSearchBe.PageSize = PageSize;
                _objSearchBe.Flag = Convert.ToInt32(SearchFlags.MeterSearch);
                //_objSearchBe.Brand = txtBrand.Text.Trim();
                //_objSearchBe.MeterNo = txtMeterNo.Text.Trim();
                //_objSearchBe.MeterSerialNo = txtSerialNo.Text.Trim();
                _objSearchBe.Brand = hfBrand.Value;
                _objSearchBe.MeterNo = hfMeterNo.Value;
                _objSearchBe.MeterSerialNo = hfMeterSerialNo.Value;
                _objSearchBe.MeterType = hfMeterType.Value;

                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    _objSearchBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    _objSearchBe.BU_ID = string.Empty;
                DataSet ds = new DataSet();
                ds = _objRerpotsBal.SearchMasterBAL(_objSearchBe);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    hfTotalRecords.Value = ds.Tables[0].Rows[0]["TotalRecords"].ToString();
                    gvMetersList.DataSource = ds.Tables[0];
                    gvMetersList.DataBind();
                    divPagin1.Visible = divPagin2.Visible = true;
                }
                else
                {
                    hfTotalRecords.Value = 0.ToString();
                    gvMetersList.DataSource = new DataTable();
                    gvMetersList.DataBind();
                    divPagin1.Visible = divPagin2.Visible = false;
                }
                //MastersBE objMastersBE = new MastersBE();
                //objMastersBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                //objMastersBE.PageSize = PageSize;
                //MastersListBE objMastersListBE = objiDsignBAL.DeserializeFromXml<MastersListBE>(objMastersBAL.MeterType(objMastersBE, ReturnType.Fetch));

                //if (objMastersListBE.Items.Count > 0)
                //{
                //    gvMetersList.DataSource = objMastersListBE.Items;
                //    gvMetersList.DataBind();
                //    hfTotalRecords.Value = objMastersListBE.Items[0].TotalRecords.ToString();
                //    divPaging.Visible = true;
                //}
                //else
                //{
                //    gvMetersList.DataSource = new DataTable();
                //    gvMetersList.DataBind();
                //    hfTotalRecords.Value = Constants.Zero.ToString();
                //    divPaging.Visible = false;
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        //protected void gvMetersList_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    try
        //    {
        //        if (e.Row.RowType == DataControlRowType.DataRow)
        //        {
        //            Label lblIsCAPMI = (Label)e.Row.FindControl("lblIsCAPMI");
        //            Label lblCAPMIAmount = (Label)e.Row.FindControl("lblCAPMIAmount");

        //            if (!string.IsNullOrEmpty(lblIsCAPMI.Text))
        //            {
        //                if (Convert.ToBoolean(lblIsCAPMI.Text))
        //                {
        //                    lblIsCAPMI.Text = "Yes";
        //                }
        //                else
        //                {
        //                    lblIsCAPMI.Text = "No";
        //                }
        //            }
        //            else
        //            {
        //                lblIsCAPMI.Text = "No";
        //            }
        //            if (!string.IsNullOrEmpty(lblCAPMIAmount.Text))
        //            {
        //                if (Convert.ToDecimal(lblCAPMIAmount.Text) != 0)
        //                {
        //                    lblCAPMIAmount.Text = objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblCAPMIAmount.Text), 2, Constants.MILLION_Format);
        //                }
        //                else
        //                {
        //                    lblCAPMIAmount.Text = "---";
        //                }
        //            }
        //            else
        //            {
        //                lblCAPMIAmount.Text = "---";
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void gvMetersList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    Label lblGlobalAccNo = (Label)e.Row.FindControl("lblGlobalAccNo");
                    Label txtGridMeterNo = (Label)e.Row.FindControl("lblGridMeterNo");
                    TextBox txtGridSerialNo = (TextBox)e.Row.FindControl("txtGridSerialNo");
                    DropDownList ddlGridMeterType = (DropDownList)e.Row.FindControl("ddlGridMeterType");
                    TextBox txtGridSize = (TextBox)e.Row.FindControl("txtGridSize");
                    TextBox txtGridMultiplier = (TextBox)e.Row.FindControl("txtGridMultiplier");
                    TextBox txtGridBrand = (TextBox)e.Row.FindControl("txtGridBrand");
                    TextBox txtGridMeterRating = (TextBox)e.Row.FindControl("txtGridMeterRating");
                    TextBox txtGridNextDate = (TextBox)e.Row.FindControl("txtGridNextDate");
                    TextBox txtGridDials = (TextBox)e.Row.FindControl("txtGridDials");
                    RadioButtonList rblGIsCAPMI = (RadioButtonList)e.Row.FindControl("rblGIsCAPMI");
                    TextBox txtGAmount = (TextBox)e.Row.FindControl("txtGAmount");

                    Label lblIsCAPMI = (Label)e.Row.FindControl("lblIsCAPMI");
                    Label lblCAPMIAmount = (Label)e.Row.FindControl("lblCAPMIAmount");
                    Label lblActiveStatus = (Label)e.Row.FindControl("lblActiveStatus");


                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridMeterNo.ClientID + "','"
                                                                                      + txtGridSerialNo.ClientID + "','"
                                                                                      + ddlGridMeterType.ClientID + "','"
                                                                                      + txtGridSize.ClientID + "','"
                                                                                      + txtGridMultiplier.ClientID + "','"
                                                                                      + txtGridBrand.ClientID + "','"
                                                                                      + rblGIsCAPMI.ClientID + "','"
                                                                                      + txtGAmount.ClientID + "','"
                        //+ txtGridMeterRating.ClientID + "','"
                        //+ txtGridNextDate.ClientID + "','"
                                                                                      + txtGridDials.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");

                    if (Convert.ToInt32(lblActiveStatusId.Text) == Constants.DeActive)
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }

                    if (lblGlobalAccNo.Text != "--")
                    {
                        e.Row.FindControl("lkbtnActive").Visible = e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lblHyphen").Visible = true;
                    }
                    else if (lblActiveStatus.Text == "1")
                    {
                        e.Row.FindControl("lkbtnActive").Visible = e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lblHyphen").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}