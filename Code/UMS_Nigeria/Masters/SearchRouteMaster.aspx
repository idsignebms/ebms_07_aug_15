﻿<%@ Page Title=":: Search Route Master ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master" Theme="Green"
    AutoEventWireup="true" CodeBehind="SearchRouteMaster.aspx.cs" Inherits="UMS_Nigeria.Masters.SearchRouteMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script type="text/javascript">
        function pageLoad() {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        };       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <div class="out-bor">
        <asp:UpdatePanel ID="upStates" runat="server">
            <ContentTemplate>
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text-heading">
                        <asp:Literal ID="litAddState" runat="server" Text="Search Route Management"></asp:Literal>
                    </div>
                    <div class="star_text">
                        &nbsp;
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="litStateName" runat="server" Text="<%$ Resources:Resource, ROUTE_NAME%>"></asp:Literal><br />
                                <asp:TextBox CssClass="text-box" ID="txtRouteName" TabIndex="1" runat="server" MaxLength="500"
                                    placeholder="Enter Route Name"></asp:TextBox>
                                <span id="spanServiceCenterName" class="spancolor"></span>
                            </div>
                            <div class="box_totalThree_a">
                                <asp:Button ID="Button1" TabIndex="3" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s"
                                    runat="server" OnClick="btnSearch_Click" />
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, ROUTE_CODE%>"></asp:Literal><br />
                                <asp:TextBox CssClass="text-box" ID="txtRouteCode" TabIndex="2" runat="server" onkeyup="this.value=this.value.toUpperCase()"
                                    placeholder="Enter Route Code"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbtxtRouteCode" runat="server" TargetControlID="txtRouteCode"
                                    FilterType="LowercaseLetters,UppercaseLetters,Custom,Numbers" ValidChars="">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanRouteCode" class="spancolor"></span>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="grid_boxes">
                        <div id="divpaging" runat="server">
                            <div class="grid_pagingTwo_top">
                                <div class="paging_top_title">
                                    <asp:Literal ID="litSUList" runat="server" Text="Routes List"></asp:Literal>
                                </div>
                                <div id="divPagin1" runat="server" class="paging_top_rightTwo_content">
                                    <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvRouteList" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                            HeaderStyle-CssClass="grid_tb_hd">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no routes information.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:BoundField HeaderText="<%$ Resources:Resource, GRID_SNO%>" DataField="RowNumber"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, ROUTE_NAME%>"
                                    DataField="RouteName" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, ROUTE_CODE%>"
                                    DataField="RouteCode" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, BUSINESS_UNIT%>"
                                    DataField="BusinessUnit" ItemStyle-CssClass="grid_tb_td" />
                                <%--<asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, DETAILS%>"
                                    DataField="Details" />--%>
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, STATE_CODE%>"
                                    DataField="StateCode" ItemStyle-CssClass="grid_tb_td" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="grid_boxes">
                        <div id="divPagin2" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                    <%--Variables decleration starts--%>
                    <asp:HiddenField ID="hfPageNo" runat="server" />
                    <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                    <asp:HiddenField ID="hfLastPage" runat="server" />
                    <asp:HiddenField ID="hfRouteCodeLength" runat="server" />
                    <asp:HiddenField ID="hfPageSize" runat="server" />
                    <asp:HiddenField ID="hfRouteName" runat="server" Value="" />
                    <asp:HiddenField ID="hfRouteCode" runat="server" Value="" />
                    <%--Variables decleration ends--%>
                    <%--==============Ajax loading script starts==============--%>
                    <script type="text/javascript">
                        var prm = Sys.WebForms.PageRequestManager.getInstance();
                        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                        prm.add_beginRequest(BeginRequestHandler);
                        // Raised after an asynchronous postback is finished and control has been returned to the browser.
                        prm.add_endRequest(EndRequestHandler);
                        function BeginRequestHandler(sender, args) {
                            //Shows the modal popup - the update progress
                            var popup = $find('<%= modalPopupLoading.ClientID %>');
                            if (popup != null) {
                                popup.show();
                            }
                        }

                        function EndRequestHandler(sender, args) {
                            //Hide the modal popup - the update progress
                            var popup = $find('<%= modalPopupLoading.ClientID %>');
                            if (popup != null) {
                                popup.hide();
                            }
                        }
   
                    </script>
                    <%--==============Ajax loading script ends==============--%>
                </span>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
