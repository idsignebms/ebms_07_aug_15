﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using UMS_NigeriaBAL;
using iDSignHelper;
using Resources;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Threading;
using System.Globalization;
using UMS_Nigeria.UserControls;

namespace UMS_Nigeria.Masters
{
    public partial class SearchRouteMaster : System.Web.UI.Page
    {
        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        ReportsBal _objRerpotsBal = new ReportsBal();
        public int PageNum;
        string Key = "SearchRouteMaster";
        Hashtable htableMaxLength = new Hashtable();

        private bool IsSearchClick = false;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                AssignMaxLength();
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
            }
        }
        private void AssignMaxLength()
        {
            try
            {
                htableMaxLength = objCommonMethods.getApplicationSettings(UMS_Resource.SETTING_ROUTE_CODE_LENGTH);
                txtRouteCode.Attributes.Add("MaxLength", htableMaxLength[Constants.Route].ToString());
                hfRouteCodeLength.Value = htableMaxLength[Constants.Route].ToString();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                hfRouteName.Value = txtRouteName.Text.Trim();
                hfRouteCode.Value = txtRouteCode.Text.Trim();
                IsSearchClick = true;
                BindGrid();
                //UCPaging1.GeneratePaging();
                UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                UCPaging2.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        public void BindGrid()
        {
            try
            {
                MasterSearchBE _objSearchBe = new MasterSearchBE();
                _objSearchBe.PageNo = IsSearchClick ? Convert.ToInt32(Constants.pageNoValue) : Convert.ToInt32(hfPageNo.Value);
                _objSearchBe.PageSize = PageSize;
                _objSearchBe.Flag = Convert.ToInt32(SearchFlags.RouteSearch); ;
                //_objSearchBe.RouteCode = txtRouteCode.Text.Trim();
                //_objSearchBe.RouteName = txtRouteName.Text.Trim();
                _objSearchBe.RouteCode = hfRouteCode.Value;
                _objSearchBe.RouteName = hfRouteName.Value;
                _objSearchBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                DataSet ds = new DataSet();
                ds = _objRerpotsBal.SearchMasterBAL(_objSearchBe);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    int totalRecoreds = Convert.ToInt32(ds.Tables[0].Rows[0]["TotalRecords"]);
                    hfTotalRecords.Value = totalRecoreds.ToString();
                    gvRouteList.DataSource = ds.Tables[0];
                    gvRouteList.DataBind();

                    divPagin1.Visible = divPagin1.Visible = true;
                }
                else
                {
                    hfTotalRecords.Value = 0.ToString();
                    gvRouteList.DataSource = new DataTable();
                    gvRouteList.DataBind();
                    divPagin1.Visible = divPagin2.Visible = false;
                }

                //MastersBE objMastersBE = new MastersBE();
                //objMastersBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                //objMastersBE.PageSize = PageSize;
                //MastersListBE objMastersListBE = objiDsignBAL.DeserializeFromXml<MastersListBE>(objMastersBAL.RouteMangement(objMastersBE, ReturnType.Get));
                //if (objMastersListBE.Items.Count > 0)
                //{
                //    gvRouteList.DataSource = objMastersListBE.Items;
                //    gvRouteList.DataBind();
                //    hfTotalRecords.Value = objMastersListBE.Items[0].TotalRecords.ToString();
                //    divPaging.Visible = true;
                //    CreatePagingDT(objMastersListBE.Items[0].TotalRecords);
                //}
                //else
                //{
                //    gvRouteList.DataSource = new DataTable();
                //    gvRouteList.DataBind();
                //    divPaging.Visible = false;
                //    hfTotalRecords.Value = Constants.Zero.ToString();
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Pageing
        /* protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindRouteList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindRouteList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindRouteList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                BindRouteList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindRouteList();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
                //ddlPageSize.Items.Clear();
                //if (TotalRecords < Constants.PageSizeStarts)
                //{
                //    ListItem item = new ListItem(Constants.PageSizeStarts.ToString(), Constants.PageSizeStarts.ToString());
                //    ddlPageSize.Items.Add(item);
                //}
                //else
                //{
                //    int previousSize = Constants.PageSizeStarts;
                //    for (int i = Constants.PageSizeStarts; i <= TotalRecords; i = i + Constants.PageSizeIncrement)
                //    {
                //        ListItem item = new ListItem(i.ToString(), i.ToString());
                //        ddlPageSize.Items.Add(item);
                //        previousSize = i;
                //    }
                //    if (previousSize < TotalRecords)
                //    {
                //        ListItem item = new ListItem((previousSize + Constants.PageSizeIncrement).ToString(), (previousSize + Constants.PageSizeIncrement).ToString());
                //        ddlPageSize.Items.Add(item);
                //    }
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                objiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                else { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Blue; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }*/
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}