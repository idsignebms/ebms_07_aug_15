﻿<%@ Page Title=":: Search SU Master ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="SearchServiceUnitMaster.aspx.cs"
    Inherits="UMS_Nigeria.Masters.SearchServiceUnitMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <div class="out-bor">
        <asp:UpdatePanel ID="upStates" runat="server">
            <ContentTemplate>
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text-heading">
                        <asp:Literal ID="litAddState" runat="server" Text="Search Service Unit"></asp:Literal>
                    </div>
                    <div class="star_text">
                        &nbsp;
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="litServiceUnitName" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT_NAME%>"></asp:Literal><br />
                                <asp:TextBox CssClass="text-box" ID="txtServiceUnitName" TabIndex="1" runat="server"
                                    MaxLength="300" placeholder="Enter Service Unit Name"></asp:TextBox>
                                <span id="spanServiceUnitName" class="spancolor"></span>
                            </div>
                            <div class="box_totalThree_a">
                                <asp:Button ID="btnSearch" TabIndex="3" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s"
                                    OnClick="btnSearch_Click" runat="server" /></div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT_CODE%>"></asp:Literal>
                                <asp:TextBox CssClass="text-box" ID="txtSUCode" TabIndex="2" runat="server" onkeyup="this.value=this.value.toUpperCase()"
                                    placeholder="Enter Service Unit Code"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbtxtSUCode" runat="server" TargetControlID="txtSUCode"
                                    FilterType="LowercaseLetters,UppercaseLetters,Custom,Numbers" ValidChars="">
                                </asp:FilteredTextBoxExtender>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <%--<div class="grid_boxes">
                        <div id="divpaging" runat="server">
                            <div class="grid_pagingTwo_top">
                                <div class="paging_top_title">
                                    <asp:Literal ID="litSUList" runat="server" Text="<%$ Resources:Resource, GRID_SERVICE_UNITS_LIST%>"></asp:Literal>
                                </div>
                                <div id="divPaging1" runat="server" class="paging_top_rightTwo_content">
                                    <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvServiceUnitsList" runat="server" AutoGenerateColumns="False"
                            AlternatingRowStyle-CssClass="color" HeaderStyle-CssClass="grid_tb_hd">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no service unit information.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:BoundField HeaderText="<%$ Resources:Resource, GRID_SNO%>" DataField="RowNumber"
                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, SERVICE_UNIT_NAME%>"
                                    DataField="ServiceUnitName" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField HeaderText="<%$ Resources:Resource, SERVICE_UNIT_CODE%>" DataField="ServiceUnitCode" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, BUSINESS_UNIT_NAME%>"
                                    DataField="BusinessUnitName" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, STATE_NAME%>"
                                    DataField="StateName" ItemStyle-CssClass="grid_tb_td" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="grid_boxes">
                        <div id="divPaging2" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>--%>
                    <div class="grid_boxes">
                        <div class="grid_pagingTwo_top">
                            <div class="paging_top_title">
                                <asp:Literal ID="litCountriesList" runat="server" Text="<%$ Resources:Resource, GRID_SERVICE_UNITS_LIST%>"></asp:Literal>
                            </div>
                            <div class="paging_top_rightTwo_content" id="divpaging" runat="server">
                                <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvServiceUnitsList" runat="server" AutoGenerateColumns="False"
                            OnRowCommand="gvServiceUnitsList_RowCommand" HeaderStyle-CssClass="grid_tb_hd"
                            OnRowDataBound="gvServiceUnitsList_RowDataBound">
                            <EmptyDataRowStyle HorizontalAlign="Center" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                    HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%-- <%#Container.DataItemIndex+1%>--%>
                                        <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, SERVICE_UNIT_ID%>" ItemStyle-Width="8%"
                                    Visible="false" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblServiceUnitId" runat="server" Text='<%#Eval("SU_ID") %>'></asp:Label>
                                        <asp:Label ID="lblActiveStatusId" Visible="false" runat="server" Text='<%#Eval("ActiveStatusId") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, SERVICE_UNIT_NAME%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblServiceUnitName" runat="server" Text='<%#Eval("ServiceUnitName") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridServiceUnitName" MaxLength="300" placeholder="Enter Service Unit Name"
                                            runat="server" Visible="false"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SU Code" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSUCode" runat="server" Text='<%#Eval("SUCode") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, STATE_NAME%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblState" runat="server" Text='<%#Eval("StateName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, BU%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBusinessUnitId" Visible="false" runat="server" Text='<%#Eval("BU_ID") %>'></asp:Label>
                                        <asp:DropDownList ID="ddlGridBusinessUnitName" runat="server" Visible="false">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblBusinessUnitName" runat="server" Text='<%#Eval("BusinessUnitName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, ADDRESS_1%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress1" runat="server" Text='<%#Eval("Address1") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridAddress1" MaxLength="99" placeholder="Enter Address1"
                                            runat="server" Visible="false"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, ADDRESS_2%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress2" runat="server" Text='<%#Eval("Address2") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridAddress2" MaxLength="99" placeholder="Enter Address1"
                                            runat="server" Visible="false"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, CITY%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCity" runat="server" Text='<%#Eval("City") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridCity" MaxLength="50" placeholder="Enter City"
                                            runat="server" Visible="false"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, ZIP%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblZip" runat="server" Text='<%#Eval("ZIP") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridZip" MaxLength="10" placeholder="Enter Zip"
                                            runat="server" Visible="false"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtGridZip"
                                            FilterType="Numbers" ValidChars=" ">
                                        </asp:FilteredTextBoxExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, NOTES%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNotes" runat="server" Text='<%#Eval("Notes") %>'></asp:Label>
                                        <asp:TextBox CssClass="text-box" ID="txtGridNotes" placeholder="Enter Notes" runat="server"
                                            Visible="false" TextMode="MultiLine"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lkbtnEdit" runat="server" ToolTip="Edit" Text="Edit" CommandName="EditServiceUnit">
                                        <img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnUpdate" Visible="false" runat="server" ToolTip="Update"
                                            CommandName="UpdateServiceUnit"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnCancel" Visible="false" runat="server" ToolTip="Cancel"
                                            CommandName="CancelServiceUnit"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnActive" Visible="false" runat="server" ToolTip="Activate Service Unit"
                                            CommandName="ActiveServiceUnit"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnDeActive" runat="server" ToolTip="DeActivate Service Unit"
                                            CommandName="DeActiveServiceUnit" CommandArgument='<%#Eval("SU_ID") %>'><img src="../images/Deactivate.gif" alt="DeActive" /></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="grid_boxes">
                        <div id="divdownpaging" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                <%--Variables decleration starts--%>
                <asp:HiddenField ID="hfPageSize" runat="server" />
                <asp:HiddenField ID="hfPageNo" runat="server" />
                <asp:HiddenField ID="hfLastPage" runat="server" />
                <asp:HiddenField ID="hfSUCodeLength" runat="server" />
                <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                <asp:HiddenField ID="hfSuName" runat="server" Value="" />
                <asp:HiddenField ID="hfSuCode" runat="server" Value="" />
                <asp:HiddenField ID="hfRecognize" runat="server" />
                <asp:HiddenField ID="hfSU_ID" runat="server" />
                <div id="hidepopup">
                    <%-- Active Btn popup Start--%>
                    <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                        TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelActivate" runat="server" CssClass="modalPopup" Style="display: none;
                        border-color: #639B00;">
                        <div class="popheader" style="background-color: #639B00;">
                            <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                        </div>
                        <div class="space">
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnActiveOk" runat="server" OnClick="btnActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Active  Btn popup Closed--%>
                    <%-- DeActive  Btn popup Start--%>
                    <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                        TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnDeActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelDeActivate" runat="server" CssClass="modalPopup" Style="display: none;
                        border-color: #639B00;">
                        <div class="popheader" style="background-color: red;">
                            <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <div class="space">
                                </div>
                                <asp:Button ID="btnDeActiveOk" runat="server" OnClick="btnDeActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- DeActive  popup Closed--%>
                    <%-- Shift customers  Btn popup Start--%>
                    <asp:ModalPopupExtender ID="mpeShiftCustomer" runat="server" PopupControlID="pnlShiftCustomer"
                        TargetControlID="btnShipCustomerDemo" BackgroundCssClass="modalBackground" CancelControlID="btnShipCustomerCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnShipCustomerDemo" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="pnlShiftCustomer" runat="server" CssClass="modalPopup" Style="display: none;
                        border-color: #639B00;">
                        <div class="popheader" style="background-color: red;">
                            <asp:Label ID="lblShiftCustomer" runat="server"></asp:Label>
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <div class="space">
                                </div>
                                <asp:Button ID="btnShipCustomer" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnShipCustomerCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" Style="display: none;" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Shift customers  popup Closed--%>
                    <%-- Grid Alert popup Start--%>
                    <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                        TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                        <div class="popheader popheaderlblred">
                            <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Grid Alert popup Closed--%>
                </div>
                <%--Variables decleration ends--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/PageValidations/AddServiceUnits.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
