﻿
#region Namespaces
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using iDSignHelper;
using UMS_NigeriaBAL;
using Resources;
using System.Threading;
using System.Globalization;
using System.Xml;
using UMS_NigriaDAL;
using System.Data;
using System.Drawing;
using System.Collections; 
#endregion

namespace UMS_Nigeria.Masters
{
    public partial class SearchServiceUnitMaster : System.Web.UI.Page
    {
        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        ReportsBal _objRerpotsBal = new ReportsBal();
        MastersBAL objMastersBAL = new MastersBAL();
        public int PageNum;
        string Key = "SearchSUMaster";
        static int customersCount = 0;
        Hashtable htableMaxLength = new Hashtable();
        XmlDocument xml = null;
        private bool IsSearchClick = false;
        CustomerDetailsBe _ObjCustomerDetailsBe = new CustomerDetailsBe();
        CustomerDetailsBAL _ObjCustomerDetailsBAL = new CustomerDetailsBAL();
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                AssignMaxLength();
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                hfSuName.Value = txtServiceUnitName.Text.Trim();
                hfSuCode.Value = txtSUCode.Text.Trim();
                IsSearchClick = true;
                BindGrid();
                //UCPaging1.GeneratePaging();
                UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                UCPaging2.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvServiceUnitsList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                hfRecognize.Value = string.Empty;
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBE objMastersBE = new MastersBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridServiceUnitName = (TextBox)row.FindControl("txtGridServiceUnitName");
                TextBox txtGridNotes = (TextBox)row.FindControl("txtGridNotes");
                DropDownList ddlGridBusinessUnitName = (DropDownList)row.FindControl("ddlGridBusinessUnitName");

                Label lblBusinessUnitId = (Label)row.FindControl("lblBusinessUnitId");
                Label lblBusinessUnitName = (Label)row.FindControl("lblBusinessUnitName");
                Label lblServiceUnitName = (Label)row.FindControl("lblServiceUnitName");
                Label lblServiceUnitId = (Label)row.FindControl("lblServiceUnitId");
                Label lblNotes = (Label)row.FindControl("lblNotes");

                Label lblAddress1 = (Label)row.FindControl("lblAddress1");
                Label lblAddress2 = (Label)row.FindControl("lblAddress2");
                Label lblCity = (Label)row.FindControl("lblCity");
                Label lblZip = (Label)row.FindControl("lblZip");
                TextBox txtGridAddress1 = (TextBox)row.FindControl("txtGridAddress1");
                TextBox txtGridAddress2 = (TextBox)row.FindControl("txtGridAddress2");
                TextBox txtGridCity = (TextBox)row.FindControl("txtGridCity");
                TextBox txtGridZip = (TextBox)row.FindControl("txtGridZip");

                switch (e.CommandName.ToUpper())
                {
                    case "EDITSERVICEUNIT":
                        lblBusinessUnitName.Visible = lblNotes.Visible = lblServiceUnitName.Visible = lblBusinessUnitId.Visible =
                            lblAddress1.Visible = lblAddress2.Visible = lblCity.Visible = lblZip.Visible = false;
                        txtGridServiceUnitName.Visible = ddlGridBusinessUnitName.Visible = txtGridNotes.Visible =
                            txtGridAddress1.Visible = txtGridAddress2.Visible = txtGridCity.Visible = txtGridZip.Visible = true;
                        txtGridServiceUnitName.Text = lblServiceUnitName.Text;

                        txtGridAddress1.Text = lblAddress1.Text;
                        txtGridAddress2.Text = lblAddress2.Text;
                        txtGridCity.Text = lblCity.Text;
                        txtGridZip.Text = lblZip.Text;

                        objCommonMethods.BindBusinessUnits(ddlGridBusinessUnitName, string.Empty, true);
                        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        {
                            //ListItem liSelect = new ListItem(UMS_Resource.SELECT, string.Empty);
                            //ListItem liBU = new ListItem(Session[UMS_Resource.SESSION_USER_BUNAME].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());
                            //liBU.Selected = true;
                            //ddlGridBusinessUnitName.Items.Add(liSelect);
                            //ddlGridBusinessUnitName.Items.Add(liBU);
                            ddlGridBusinessUnitName.SelectedIndex = ddlGridBusinessUnitName.Items.IndexOf(ddlGridBusinessUnitName.Items.FindByValue(lblBusinessUnitId.Text));
                            ddlGridBusinessUnitName.Enabled = false;
                        }
                        //else
                        //    objCommonMethods.BindBusinessUnits(ddlGridBusinessUnitName, string.Empty, true);
                        ddlGridBusinessUnitName.SelectedIndex = ddlGridBusinessUnitName.Items.IndexOf(ddlGridBusinessUnitName.Items.FindByValue(lblBusinessUnitId.Text));
                        txtGridNotes.Text = objiDsignBAL.ReplaceNewLines(lblNotes.Text, false);
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;
                        break;
                    case "CANCELSERVICEUNIT":
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                        txtGridServiceUnitName.Visible = ddlGridBusinessUnitName.Visible = txtGridNotes.Visible = txtGridAddress1.Visible = txtGridAddress2.Visible = txtGridCity.Visible = txtGridZip.Visible = false;
                        lblBusinessUnitName.Visible = lblNotes.Visible = lblServiceUnitName.Visible = lblAddress1.Visible = lblAddress2.Visible = lblCity.Visible = lblZip.Visible = true;
                        break;
                    case "UPDATESERVICEUNIT":
                        objMastersBE.SU_ID = lblServiceUnitId.Text;
                        objMastersBE.BU_ID = ddlGridBusinessUnitName.SelectedValue;
                        //objMastersBE.SUCode = txtGridSUCode.Text;
                        objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objMastersBE.ServiceUnitName = txtGridServiceUnitName.Text;
                        objMastersBE.Notes = objiDsignBAL.ReplaceNewLines(txtGridNotes.Text, true);

                        objMastersBE.Address1 = txtGridAddress1.Text.Trim();
                        objMastersBE.Address2 = txtGridAddress2.Text.Trim();
                        objMastersBE.City = txtGridCity.Text.Trim();
                        objMastersBE.ZIP = txtGridZip.Text.Trim();

                        xml = objMastersBAL.ServiceUnit(objMastersBE, Statement.Update);
                        objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                        if (Convert.ToBoolean(objMastersBE.IsSuccess))
                        {
                            BindGrid();
                            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            Message(UMS_Resource.SERVICE_UNIT_UPDATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                        }
                        else if (objMastersBE.IsSUCodeExists)
                        {
                            Message(Resource.SERVICE_UNIT_CODE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        else
                            Message(UMS_Resource.SERVICE_UNIT_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "ACTIVESERVICEUNIT":
                        mpeActivate.Show();
                        hfSU_ID.Value = lblServiceUnitId.Text;
                        lblActiveMsg.Text = UMS_Resource.ACTIVE_SU_POPUP_TEXT;
                        btnActiveOk.Focus();
                        break;
                    case "DEACTIVESERVICEUNIT":
                        if (CustomerCount(e.CommandArgument.ToString()))
                        {
                            mpeShiftCustomer.Show();
                            //hfSU_ID.Value = lblServiceUnitId.Text;
                            //lblShiftCustomer.Text = UMS_Resource.SHIFT_CUSTOMERS_SU.Insert(22, customersCount.ToString() + " ");
                            lblShiftCustomer.Text = UMS_Resource.SHIFT_CUSTOMERS_SU;
                            btnShipCustomer.Focus();
                        }
                        else
                        {
                            mpeDeActive.Show();
                            hfSU_ID.Value = lblServiceUnitId.Text;
                            lblDeActiveMsg.Text = UMS_Resource.DEACTIVE_SU_POPUP_TEXT;
                            btnDeActiveOk.Focus();
                        }
                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.Active;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.SU_ID = hfSU_ID.Value;
                xml = objMastersBAL.ServiceUnit(objMastersBE, Statement.Delete);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (Convert.ToBoolean(objMastersBE.IsSuccess))
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.SERVICE_UNIT_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else
                    Message(UMS_Resource.SERVICE_UNIT_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.ActiveStatusId = Constants.DeActive;
                objMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objMastersBE.SU_ID = hfSU_ID.Value;
                xml = objMastersBAL.ServiceUnit(objMastersBE, Statement.Delete);
                objMastersBE = objiDsignBAL.DeserializeFromXml<MastersBE>(xml);

                if (objMastersBE.IsSuccess)
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.SERVICE_UNITS_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else if (!objMastersBE.IsSuccess && objMastersBE.Count > 0)
                    Message(string.Format(UMS_Resource.SERVICE_UNITS_HAVE_CUSTOMERS, objMastersBE.Count), UMS_Resource.MESSAGETYPE_ERROR);
                else
                    Message(UMS_Resource.SERVICE_UNITS_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvServiceUnitsList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    TextBox txtGridServiceUnitName = (TextBox)e.Row.FindControl("txtGridServiceUnitName");
                    DropDownList ddlGridBusinessUnitName = (DropDownList)e.Row.FindControl("ddlGridBusinessUnitName");

                    TextBox txtGridAddress1 = (TextBox)e.Row.FindControl("txtGridAddress1");
                    TextBox txtGridAddress2 = (TextBox)e.Row.FindControl("txtGridAddress2");
                    TextBox txtGridCity = (TextBox)e.Row.FindControl("txtGridCity");
                    TextBox txtGridZip = (TextBox)e.Row.FindControl("txtGridZip");

                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridServiceUnitName.ClientID + "','" +
                                                                                        ddlGridBusinessUnitName.ClientID + "','" +
                                                                                        txtGridAddress1.ClientID + "','" +
                                                                                        txtGridAddress2.ClientID + "','" +
                                                                                        txtGridCity.ClientID + "','" +
                                                                                        txtGridZip.ClientID + "')");

                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");

                    //htableMaxLength = objCommonMethods.getApplicationSettings(UMS_Resource.SETTING_SU_LENGTH);
                    //txtGridSUCode.Attributes.Add("MaxLength", htableMaxLength[Constants.ServiceUnit].ToString());

                    if (Convert.ToInt32(lblActiveStatusId.Text) == Constants.DeActive)
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods
        private void AssignMaxLength()
        {
            try
            {
                htableMaxLength = objCommonMethods.getApplicationSettings(UMS_Resource.SETTING_SU_LENGTH);
                txtSUCode.Attributes.Add("MaxLength", htableMaxLength[Constants.ServiceUnit].ToString());
                hfSUCodeLength.Value = htableMaxLength[Constants.ServiceUnit].ToString();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public bool CustomerCount(string SU_ID)
        {
            XmlDocument _xml = new XmlDocument();
            _ObjCustomerDetailsBe.ServiceUnitName = SU_ID;
            _xml = _ObjCustomerDetailsBAL.CountCustomersBAL(_ObjCustomerDetailsBe, Statement.Check);
            _ObjCustomerDetailsBe = objiDsignBAL.DeserializeFromXml<CustomerDetailsBe>(_xml);
            customersCount = _ObjCustomerDetailsBe.Count;
            if (_ObjCustomerDetailsBe.Count > 0)
                return true;
            else
                return false;
        }
        public void BindGrid()
        {
            try
            {
                MasterSearchBE _objSearchBe = new MasterSearchBE();
                _objSearchBe.PageNo = IsSearchClick ? Convert.ToInt32(Constants.pageNoValue) : Convert.ToInt32(hfPageNo.Value);
                _objSearchBe.PageSize = PageSize;
                _objSearchBe.Flag = Convert.ToInt32(SearchFlags.ServiceUnitSearch);
                //_objSearchBe.ServiceUnit = txtServiceUnitName.Text.Trim();
                //_objSearchBe.ServiceUnitCode = txtSUCode.Text.Trim();
                _objSearchBe.ServiceUnit = hfSuName.Value;
                _objSearchBe.ServiceUnitCode = hfSuCode.Value;
                _objSearchBe.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                DataSet ds = new DataSet();
                ds = _objRerpotsBal.SearchMasterBAL(_objSearchBe);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    hfTotalRecords.Value = ds.Tables[0].Rows[0]["TotalRecords"].ToString();
                    gvServiceUnitsList.DataSource = ds.Tables[0];
                    gvServiceUnitsList.DataBind();
                    //divLinks.Visible = divDownLinks.Visible = divPrint.Visible = true;
                    divpaging.Visible = divdownpaging.Visible = true;
                }
                else
                {
                    hfTotalRecords.Value = "0";
                    gvServiceUnitsList.DataSource = new DataTable();
                    gvServiceUnitsList.DataBind();
                    //divLinks.Visible = divDownLinks.Visible = false;
                    //divPrint.Visible = true;
                    divpaging.Visible = divdownpaging.Visible = false;
                }
                //MastersListBE objMastersListBE = new MastersListBE();
                //MastersBE objMastersBE = new MastersBE();
                //XmlDocument resultedXml = new XmlDocument();

                //objMastersBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                //objMastersBE.PageSize = PageSize;
                //if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                //    objMastersBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                //else
                //    objMastersBE.BU_ID = string.Empty;
                //resultedXml = objMastersBAL.ServiceUnit(objMastersBE, ReturnType.Get);

                //objMastersListBE = objiDsignBAL.DeserializeFromXml<MastersListBE>(resultedXml);
                //if (objMastersListBE.Items.Count > 0)
                //{
                //    divgrid.Visible = divpaging.Visible = true;
                //    hfTotalRecords.Value = objMastersListBE.Items[0].TotalRecords.ToString();
                //    gvServiceUnitsList.DataSource = objMastersListBE.Items;
                //    gvServiceUnitsList.DataBind();
                //}
                //else
                //{
                //    hfTotalRecords.Value = objMastersListBE.Items.Count.ToString();
                //    divgrid.Visible = divpaging.Visible = false;
                //    gvServiceUnitsList.DataSource = new DataTable(); ;
                //    gvServiceUnitsList.DataBind();
                //}

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Paging
        /* protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridServiceUnitsList();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridServiceUnitsList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridServiceUnitsList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridServiceUnitsList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                BindGridServiceUnitsList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                objiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                objCommonMethods.BindPagingDropDownListReport(TotalRecords, this.PageSize, ddlPageSize);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }*/
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}