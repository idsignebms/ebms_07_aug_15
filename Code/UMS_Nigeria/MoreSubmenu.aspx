﻿<%@ Page Title="::Sub Menu::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="MoreSubmenu.aspx.cs" Inherits="UMS_Nigeria.MoreSubmenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .bildblock_sm
        {
            padding: 1%;
            background: #fff;
            float: left;
            margin-right: 3%;
            max-height: none;
            width: 98%;
            border: 0.1% solid #f6f5f5;
            background-image: -webkit-gradient(
	linear,
	left top,
	left bottom,
	color-stop(0, rgb(247, 247, 247)),
	color-stop(1, rgb(247, 246, 246))
);
            background-image: -o-linear-gradient(bottom, rgb(247, 247, 247) 0%, rgb(247, 246, 246) 100%);
            background-image: -moz-linear-gradient(bottom, rgb(247, 247, 247) 0%, rgb(247, 246, 246) 100%);
            background-image: -webkit-linear-gradient(bottom, rgb(247, 247, 247) 0%, rgb(247, 246, 246) 100%);
            background-image: -ms-linear-gradient(bottom, rgb(247, 247, 247) 0%, rgb(247, 246, 246) 100%);
            background-image: linear-gradient(to bottom, rgb(247, 247, 247) 0%, rgb(247, 246, 246) 100%);
        }
        .bildblock_sm ul li
        {
            list-style-image: url(../images/arrow.png);
            padding-top: 15px;
            margin-bottom: 5px;
            line-height: 10px;
            float: left;
            width: 25%;
        }
        
        .bildblock_sm ul li a
        {
            color: #2d2d2d;
            font-size: 14px;
        }
        
        
        .bildblock_sm h2
        {
            border-bottom: 1px solid #fff;
            box-shadow: 0 1px 0 #d4d4d4;
            color: #426700;
            font-size: 17px;
            font-weight: bold;
            height: 40px;
            line-height: 32px;
            padding-left: 48px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <div class="inner-sec">
            <%--<div class="clear pad_10 fltl">
                <asp:Button ID="btnBack" Visible="false" Style="padding-right: 82px;" Text="<%$ Resources:Resource, GO_TO_HOME%>"
                    CssClass="back-btn" runat="server" OnClick="btnBack_Click" />
            </div>--%>
            <div class="text_total">
                <div class="text-heading">
                    &nbsp
                </div>
                <div class="star_text">
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="dot-line">
            </div>
            <div class="previous">
                <a href="Home.aspx">Go To Home</a>
            </div>
            <div class="edmcontainer">
                <%=UserNavigation()%>
            </div>
        </div>
    </div>
</asp:Content>
