﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using iDSignHelper;
using Resources;
using UMS_NigeriaBE;
using System.Xml;

namespace UMS_Nigeria
{
    public partial class MoreSubmenu : System.Web.UI.Page
    {
        #region Members
        UMS_Service _ObjUMS_Service = new UMS_Service();
        SideMenusBAL _objMenuBAL = new SideMenusBAL();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        string Key = UMS_Resource.HOMEPAGE_AFTERLOGIN;
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(UMS_Resource.ADD_HOME_PAGE,false);
        }
        #region Methods

        public string UserNavigation()
        {
            try
            {
                SideMenusBE _ObjSideMenusBE = new SideMenusBE();

                //  _ObjSideMenusBe.Role_Id = 8;
                XmlDocument resultXml = new XmlDocument();
                _ObjSideMenusBE.Role_Id = Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID]);
                _ObjSideMenusBE.PageName = "HomePage";
                //_ObjSideMenusBE.Main_Menu_Id = Convert.ToInt32(_ObjiDsignBAL.AESDecryptString(Request.QueryString[UMS_Resource.MENUID_HOME]));
                _ObjSideMenusBE.Main_Menu_Id = Convert.ToInt32(_ObjiDsignBAL.Decrypt(Request.QueryString[UMS_Resource.MENUID_HOME]));
                resultXml = _objMenuBAL.GetNewMenusBAL(_ObjSideMenusBE);
              

                SideMenusListBE _ObjSideMenusListBE = new SideMenusListBE();
                _ObjSideMenusListBE = _ObjiDsignBAL.DeserializeFromXml<SideMenusListBE>(resultXml);

                MainMenuListBE _ObjMainMenuListBE = new MainMenuListBE();
                _ObjMainMenuListBE = _ObjiDsignBAL.DeserializeFromXml<MainMenuListBE>(resultXml);

              
                string strNavigation = string.Empty;
                for (var i = 0; i < _ObjMainMenuListBE.items.Count; i++)
                {
                    _ObjSideMenusBE = (SideMenusBE)_ObjMainMenuListBE.items[i];
                    string path = _ObjSideMenusBE.Path;
                    string name = _ObjSideMenusBE.Main_Menu_Text;
                    int MainMenuId = _ObjSideMenusBE.Main_Menu_Id;
                    string cssclassAdvance = "margin:0px;";
                    string cssclass = _ObjSideMenusBE.CssClassName;
                    if (cssclass == "")
                    {
                        cssclass = "nav-home";
                    }
                   
                    if (_ObjSideMenusBE.HasSubMenu == false)
                    {
                        //strNavigation += "<li><a>" + name + "</a></li>";
                        //strNavigation += "<div class='bildblock' style='" + cssclassAdvance + "'><h2 class='" + cssclass + "'>" + name + "</h2><ul><li><a href='" + path + "?" + UMS_Resource.PageId + "=" + _ObjiDsignBAL.AESEncryptString(_ObjSideMenusBE.Page_Id.ToString()) + "'>" + name + "</a></li></ul></div> ";//ID-065 Bhimaraju
                        strNavigation += "<div class='bildblock_sm' style='" + cssclassAdvance + "'><h2 class='" + cssclass + "'>" + name + "</h2><ul><li><a href='" + path + "?" + UMS_Resource.PageId + "=" + _ObjSideMenusBE.Page_Id.ToString() + "'>" + name + "</a></li></ul></div> ";
                    }
                    else if (_ObjSideMenusListBE.items.Count > 0)
                    {
                      
                       
                        string subname = string.Empty;
                        strNavigation += "<div class='bildblock_sm' style='" + cssclassAdvance + "'><h2 class='" + cssclass + "'>" + name + "</h2><ul> ";

                        for (int item = 0; item < _ObjSideMenusListBE.items.Count; item++)
                        {
                            subname = _ObjSideMenusListBE.items[item].SubMenu_Text;
                            string path1 = _ObjSideMenusListBE.items[item].Path;
                            int submenuid = _ObjSideMenusListBE.items[item].Main_Menu_Id;
                            string page_Id = _ObjSideMenusListBE.items[item].Page_Id.ToString();
                            if (MainMenuId == submenuid)
                            {
                              
                                    if (name.Contains("Forums"))
                                        //strNavigation += "<li><a>" + subname + "</a></li>";
                                        strNavigation += "<li><a href='" + path1 + "' target='_blank'>" + subname + "</a></li>";
                                    else
                                    {
                                        if (page_Id != "")
                                            //strNavigation += "<li><a>" + subname + "</a></li>";                                    
                                            strNavigation += "<li ><a href='" + path1 + "'>" + subname + "</a></li>";
                                        //strNavigation += "<li class='radius'><a href='" + path1 + "?" + UMS_Resource.PageId + "=" + _ObjiDsignBAL.Encrypt(page_Id) + "'>" + subname + "</a></li>";
                                        else
                                            //strNavigation += "<li><a>" + subname + "</a></li>";
                                            strNavigation += "<li ><a href='" + path1 + "'>" + subname + "</a></li>";
                                    }
                                   
                              
                            }

                        }
                        strNavigation += "</ul></div>";
                    }
                  



                }
                return strNavigation;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            return string.Empty;
        }
        private void Message(string Message, string MessageType)
        {
            try
            {
                //_ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion
    }
}