﻿<%@ Page Title="::Account Status Report::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="AccountStatusReport.aspx.cs"
    Inherits="UMS_Nigeria.Reports.AccountStatusReport" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddCycle" runat="server" Text="<%$ Resources:Resource, ACCOUNT_STATUS%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <asp:Literal ID="Literal31" runat="server" Text="<%$ Resources:Resource, MONTH%>"></asp:Literal><br />
                            <asp:DropDownList ID="ddlMonth" runat="server" CssClass="text-box select_box">
                                <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <span id="spanddlMonth" class="spancolor"></span>
                        </div>
                        <%-- <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal></label><br />
                                <asp:DropDownList ID="ddlBusinessUnitName" AutoPostBack="true" runat="server" CssClass="text-box select_box"
                                    OnSelectedIndexChanged="ddlBusinessUnitName_SelectedIndexChanged">
                                    <asp:ListItem Value="" Text="ALL"></asp:ListItem>
                                </asp:DropDownList>
                            </div>--%>
                        <%--<div class="text-inner">
                                <asp:Literal ID="litCycles" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal><br />
                                <asp:DropDownList ID="ddlCycle" runat="server" AutoPostBack="true" CssClass="text-box select_box"
                                    OnSelectedIndexChanged="ddlCycle_SelectedIndexChanged">
                                    <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </div>--%>
                    </div>
                    <div class="inner-box2">
                        <div class="text-inner">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, YEAR%>"></asp:Literal><br />
                            <asp:DropDownList ID="ddlYear" CssClass="text-box select_box" runat="server">
                                <asp:ListItem Value="" Text="ALL"></asp:ListItem>
                            </asp:DropDownList>
                            <span id="spanddlYear" class="spancolor"></span>
                        </div>
                        <%--<div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litServiceUnit" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal></label><br />
                                <asp:DropDownList ID="ddlServiceUnitName" AutoPostBack="true" runat="server" CssClass="text-box select_box"
                                 OnSelectedIndexChanged="ddlServiceUnitName_SelectedIndexChanged">
                                    <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </div>--%>
                        <%--<div class="text-inner">
                                <asp:Literal ID="litBookNo" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal><br />
                                <asp:DropDownList ID="ddlBookNo" runat="server" AutoPostBack="true"  CssClass="text-box select_box">
                                    <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </div>--%>
                    </div>
                    <div class="inner-box">
                        <div id="divBusinessUnit" runat="server">
                            <div class="check_baa">
                                <div class="text-inner_a">
                                    <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" ID="ChkBUAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                        <asp:CheckBoxList ID="cblBusinessUnits" class="text-inner-b" AutoPostBack="true"
                                            RepeatDirection="Horizontal" RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblBusinessUnits_SelectedIndexChanged">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div id="divServiceUnits" runat="server" visible="false">
                        <div class="check_baa">
                            <div class="text-inner_a">
                                <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" ID="ChkSUAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                    <asp:CheckBoxList ID="cblServiceUnits" class="text-inner-b" AutoPostBack="true" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceUnits_SelectedIndexChanged">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div id="divServiceCenters" runat="server" visible="false">
                        <div class="check_baa">
                            <div class="text-inner_a">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" ID="chkSCAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                    <asp:CheckBoxList ID="cblServiceCenters" class="text-inner-b" RepeatDirection="Horizontal"
                                        AutoPostBack="true" RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceCenters_SelectedIndexChanged">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div id="divCycles" runat="server" visible="false">
                        <div class="check_baa">
                            <div class="text-inner_a">
                                <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" class="text-inner-b" ID="chkCycleAll" Text="All" AutoPostBack="true" />
                                    <asp:CheckBoxList ID="cblCycles" class="text-inner-b" AutoPostBack="true" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblCycles_SelectedIndexChanged">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div id="divBookNos" runat="server" visible="false">
                        <div class="check_baa">
                            <div class="text-inner_a">
                                <asp:Literal ID="litBookNo" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" class="text-inner-b" ID="chkBooksAll" Text="All" />
                                    <asp:CheckBoxList ID="cblBooks" class="text-inner-b" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box3">
                        <%--<div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litServiceCenter" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal></label><br />
                                <asp:DropDownList ID="ddlServiceCenterName" runat="server" AutoPostBack="true" CssClass="text-box select_box"
                                  OnSelectedIndexChanged="ddlServiceCenterName_SelectedIndexChanged">
                                    <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </div>--%>
                    </div>
                </div>
                <div style="clear: both;">
                </div>
                <div class="box_total">
                    <div class="box_total_a">
                        <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s"
                            OnClick="btnSearch_Click" runat="server" />
                    </div>
                </div>
                <div class="grid_boxes">
                    <div class="grid_paging_top">
                        <div class="paging_top_title">
                        </div>
                        <div id="divGridContent1" align="center" runat="server" visible="false" style="margin: 5px 0;"
                            class="fltl">
                            <div class="consume_name fltl">
                                <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, TOTAL_CUSTOMERS%>"></asp:Label>
                            </div>
                            <div class="consume_input fltl" style="margin-right: 10px; border-right: 1px solid #000;
                                padding-right: 10px;">
                                <asp:Label ID="lblTotalCustomers1" runat="server" Text="--"></asp:Label>
                            </div>
                            <div class="consume_name fltl">
                                <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, TOTAL_DUE_BALENCE%>"></asp:Label>
                            </div>
                            <div class="consume_input fltl" style="margin-right: 10px; border-right: 1px solid #000;
                                padding-right: 10px;">
                                <asp:Label ID="lblTotalBalence1" runat="server" Text="--"></asp:Label>
                            </div>
                            <div class="consume_name fltl">
                                <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, MONTH_YEAR%>"></asp:Label>
                            </div>
                            <div class="consume_input fltl">
                                <asp:Label ID="lblYearMonth1" runat="server" Text="--"></asp:Label>
                            </div>
                        </div>
                        <div class="paging_top_right_content" id="divPaging1" runat="server" visible="false">
                            <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                        </div>
                        <div align="right" class="fltr" runat="server" id="divExport" visible="false">
                            <%--<asp:Button ID="btnPrint" runat="server" OnClientClick="return printGrid()" CssClass="print"
                                    Text="Print" />--%>
                            <asp:Button ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" CssClass="excel"
                                Text="Export to excel" />
                        </div>
                    </div>
                </div>
                <div class="grid_tb" id="divgrid" runat="server">
                    <asp:GridView ID="gvAccountStatus" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                        OnRowDataBound="gvAccountStatus_RowDataBound" HeaderStyle-CssClass="grid_tb_hd"
                        HeaderStyle-BackColor="#558D00">
                        <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                        <EmptyDataTemplate>
                            There is no data.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:BoundField HeaderText="<%$ Resources:Resource, GRID_SNO%>" DataField="RowNumber"
                                ItemStyle-CssClass="grid_tb_td" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>"
                                DataField="GlobalAccountNumber" ItemStyle-CssClass="grid_tb_td" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, ACC_NO%>"
                                DataField="AccountNo" ItemStyle-CssClass="grid_tb_td" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, NAME%>"
                                DataField="Name" ItemStyle-CssClass="grid_tb_td" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, SERVICE_ADDRESS%>"
                                DataField="ServiceAddress" ItemStyle-CssClass="grid_tb_td" />
                            <asp:BoundField HeaderText="<%$ Resources:Resource, BILL_GENERATED_DATE%>" DataField="BillDate" />
                            <%--<asp:BoundField HeaderText="<%$ Resources:Resource, BILL_AMOUNT%>" DataField="BillAmount" />
                            <asp:BoundField HeaderText="<%$ Resources:Resource, TOTAL_PAID_AMOUNT%>" DataField="TotalPaidAmount" />
                            <asp:BoundField HeaderText="<%$ Resources:Resource, TOTAL_DUE%>" DataField="DueAmount" />--%>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, BILL_AMOUNT%>" runat="server"
                                ItemStyle-CssClass="grid_tb_td">
                                <ItemStyle HorizontalAlign="Right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblBillAmount" runat="server" Text='<%#Eval("BillAmount")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, TOTAL_PAID_AMOUNT%>" runat="server"
                                ItemStyle-CssClass="grid_tb_td">
                                <ItemStyle HorizontalAlign="Right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblTotalPaidAmount" runat="server" Text='<%#Eval("TotalPaidAmount")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, TOTAL_DUE%>" runat="server"
                                ItemStyle-CssClass="grid_tb_td">
                                <ItemStyle HorizontalAlign="Right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblDueAmount" runat="server" Text='<%#Eval("DueAmount")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle HorizontalAlign="center"></RowStyle>
                    </asp:GridView>
                    <div id="divGridContent" style="margin-left: 350px;" runat="server" visible="false">
                        <div class="consume_name fltl">
                            <asp:Label ID="Label2" runat="server" Visible="false" Text="<%$ Resources:Resource, TOTAL_CUSTOMERS%>"></asp:Label>
                        </div>
                        <div class="consume_input fltl" style="margin-right: 10px; border-right: 1px solid #000;
                            padding-right: 10px;">
                            <asp:Label ID="lblTotalCustomers" Visible="false" runat="server" Text="--"></asp:Label>
                        </div>
                        <div class="consume_name fltl">
                            <asp:Label ID="Label1" runat="server" Visible="false" Text="<%$ Resources:Resource, TOTAL_DUE_BALENCE%>"></asp:Label>
                        </div>
                        <div class="consume_input fltl" style="margin-right: 10px; border-right: 1px solid #000;
                            padding-right: 10px;">
                            <asp:Label ID="lblTotalBalence" Visible="false" runat="server" Text="--"></asp:Label>
                        </div>
                        <div class="consume_name fltl">
                            <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, MONTH_YEAR%>"></asp:Label>
                        </div>
                        <div class="consume_input fltl">
                            <asp:Label ID="lblYearMonth" runat="server" Text="--"></asp:Label>
                        </div>
                    </div>
                    <asp:HiddenField ID="hfPageSize" runat="server" />
                    <asp:HiddenField ID="hfPageNo" runat="server" />
                    <asp:HiddenField ID="hfLastPage" runat="server" />
                    <asp:HiddenField ID="hfTotalRecords" runat="server" />
                </div>
                <div class="grid_boxes">
                    <div id="divPaging2" runat="server" visible="false">
                        <div class="grid_paging_bottom">
                            <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script starts==============--%>
    <%--<script type="text/javascript">
        function pageLoad() {
            DisplayMessage('<%= pnlMessage.ClientID %>');            
            var table = '<%= gvAccountStatus.ClientID %>';
            $("#<%= btnExportExcel.ClientID%>").click(function () {

                $("#<%= gvAccountStatus.ClientID %>").btechco_excelexport({
                    containerid: table
               , datatype: $datatype.Table
                });
            });
        }
    </script>--%>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/UnBilledCustomersReport.js" type="text/javascript"></script>
    <%--<script src="../scripts/jquery.btechco.excelexport.js" type="text/javascript"></script>
    <script src="../scripts/jquery.base64.js" type="text/javascript"></script>--%>
    <script type="text/javascript">

        //        function printGrid() {


        //            var printContent = document.getElementById('<= divPrint.ClientID %>');
        //            var uniqueName = new Date();

        //            var gridrows = document.getElementById('<%= gvAccountStatus.ClientID %>');

        //            var windowName = 'Print' + uniqueName.getTime();

        //            var printWindow = window.open();
        //            printWindow.document.write('<html><head>');
        //            printWindow.document.write('</head><body><h3>');
        //            printWindow.document.write('Payments Received Report');
        //            printWindow.document.write('</h3>');
        //            printWindow.document.write("<link href='../css/style.css' rel='stylesheet' type='text/css' />");
        //            printWindow.document.write('<link href="../css/reset.css" rel="stylesheet" type="text/css" />');
        //            printWindow.document.write(printContent.innerHTML);
        //            printWindow.document.write('</body></html>');

        //            printWindow.document.close();
        //            printWindow.focus();
        //            printWindow.print();
        //            printWindow.close();

        //            return false;

        //        }
    </script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
