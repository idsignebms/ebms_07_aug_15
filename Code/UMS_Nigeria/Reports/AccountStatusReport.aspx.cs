﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for Account Status Report
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 06-June-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Configuration;
using org.in2bits.MyXls;

namespace UMS_Nigeria.Reports
{
    public partial class AccountStatusReport : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        ReportsBal _objReportsBal = new ReportsBal();
        BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
        public int PageNum;
        XmlDocument xml = null;
        string Key = UMS_Resource.ACCOUNTS_STATUS_REPORT_PAGE;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        public string BusinessUnitName
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblBusinessUnits, ","); }
            //get { return string.IsNullOrEmpty(ddlBusinessUnitName.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlBusinessUnitName, ",") : ddlBusinessUnitName.SelectedValue; }
            //set { ddlBusinessUnitName.SelectedValue = value.ToString(); }
        }
        public string ServiceUnitName
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceUnits, ","); }
            //get { return string.IsNullOrEmpty(ddlServiceUnitName.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlServiceUnitName, ",") : ddlServiceUnitName.SelectedValue; }
            //set { ddlServiceUnitName.SelectedValue = value.ToString(); }
        }
        public string ServiceCenterName
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceCenters, ","); }

            //get { return string.IsNullOrEmpty(ddlServiceCenterName.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlServiceCenterName, ",") : ddlServiceCenterName.SelectedValue; }
            //set { ddlServiceCenterName.SelectedValue = value.ToString(); }
        }
        //public string SubStation
        //{
        //    get { return string.IsNullOrEmpty(ddlSubStation.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlSubStation, ",") : ddlSubStation.SelectedValue; }
        //    set { ddlSubStation.SelectedValue = value.ToString(); }
        //}
        //public string Feeder
        //{
        //    get { return string.IsNullOrEmpty(ddlFeeder.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlFeeder, ",") : ddlFeeder.SelectedValue; }
        //    set { ddlFeeder.SelectedValue = value.ToString(); }
        //}
        //public string Transformer
        //{
        //    get { return string.IsNullOrEmpty(ddlTransformer.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlTransformer, ",") : ddlTransformer.SelectedValue; }
        //    set { ddlTransformer.SelectedValue = value.ToString(); }
        //}
        //public string Pole
        //{
        //    get { return string.IsNullOrEmpty(ddlPole.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlPole, ",") : ddlPole.SelectedValue; }
        //    set { ddlPole.SelectedValue = value.ToString(); }
        //}
        public string Cycle
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblCycles, ","); }
            //get { return string.IsNullOrEmpty(ddlCycle.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlCycle, ",") : ddlCycle.SelectedValue; }
            //set { ddlCycle.SelectedValue = value.ToString(); }
        }
        public string BookNo
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblBooks, ","); }
            //get { return string.IsNullOrEmpty(ddlBookNo.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlBookNo, ",") : ddlBookNo.SelectedValue; }
            //set { ddlBookNo.SelectedValue = value.ToString(); }
        }
        //public string ReadCode
        //{
        //    get { return string.IsNullOrEmpty(ddlReadCode.SelectedValue) ? Constants.SelectedValue : ddlReadCode.SelectedValue; }
        //    set { ddlReadCode.SelectedValue = value.ToString(); }
        //}
        public string Year
        {
            get { return string.IsNullOrEmpty(ddlYear.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlYear, ",") : ddlYear.SelectedValue; }
            set { ddlYear.SelectedValue = value.ToString(); }
        }
        public string Month
        {
            get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlMonth, ",") : ddlMonth.SelectedValue; }
            set { ddlMonth.SelectedValue = value.ToString(); }
        }
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }
        //protected void Page_Load(object sender, EventArgs e)
        //{

        //    if (Session[UMS_Resource.SESSION_LOGINID] != null)
        //    {
        //        if (!IsPostBack)
        //        {
        //            //string path = string.Empty;
        //            ////path = __objCommonMethods.GetPagePath(this.Request.Url);
        //            ////if (__objCommonMethods.IsAccess(path))
        //            ////{
        //            //BindBusinessUnits();---------------------------------------------
        //            //hfPageNo.Value = Constants.pageNoValue.ToString();---------------
        //            //BindGrid();------------------------------------------------------
        //            ////CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //            ////BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
        //            ////}
        //            ////else
        //            ////{
        //            ////    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
        //            ////}
        //            string path = string.Empty;
        //            _objCommonMethods.BindUserBusinessUnits(ddlBusinessUnitName, string.Empty, false, UMS_Resource.DROPDOWN_SELECT);
        //            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
        //            {
        //                ddlBusinessUnitName.SelectedIndex = ddlBusinessUnitName.Items.IndexOf(ddlBusinessUnitName.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
        //                ddlBusinessUnitName.Enabled = false;
        //                ddlBusinessUnitName_SelectedIndexChanged(ddlBusinessUnitName, new EventArgs());
        //            }
        //            CheckBoxesJS();
        //            BindMonths();
        //            BindYears();

        //            hfPageNo.Value = Constants.pageNoValue.ToString();
        //            BindGrid();
        //        }
        //    }
        //    else
        //    {
        //        Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        //    }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                if (!IsPostBack)
                {
                    string path = string.Empty;
                    path = _objCommonMethods.GetPagePath(this.Request.Url);
                    if (_objCommonMethods.IsAccess(path))
                    {
                        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        {
                            ListItem LiBU = new ListItem(Session[UMS_Resource.SESSION_USER_BUNAME].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());
                            cblBusinessUnits.Items.Add(LiBU);
                            cblBusinessUnits.Items[0].Selected = true;
                            cblBusinessUnits.Enabled = false;
                            ChkBUAll.Visible = false;
                            divServiceUnits.Visible = true;
                            _objCommonMethods.BindUserServiceUnits_BuIds(cblServiceUnits, BusinessUnitName, Resource.DDL_ALL, false);
                        }
                        else
                            _objCommonMethods.BindBusinessUnits(cblBusinessUnits, string.Empty, false);
                        //BindDropDowns();
                        CheckBoxesJS();
                        BindYears();
                        BindMonths();

                        hfPageNo.Value = Constants.pageNoValue.ToString();
                        hfTotalRecords.Value = Constants.Zero.ToString();
                        //BindGrid();

                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    }
                    else
                    {
                        Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    }
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }


        //protected void ddlBusinessUnitName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        _objCommonMethods.BindUserServiceUnits_BuIds(ddlServiceUnitName, BusinessUnitName, Resource.DDL_ALL, false);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void ddlServiceUnitName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        _objCommonMethods.BindUserServiceCenters_SuIds(ddlServiceCenterName, ServiceUnitName, Resource.DDL_ALL, false);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void ddlServiceCenterName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    _objCommonMethods.BindBookNumbersByServiceDetails(ddlBookNo, BusinessUnitName, ServiceUnitName, ServiceCenterName, false);
        //    ddlBookNo.Items.Insert(0, new ListItem("ALL", ""));

        //}
        //protected void ddlSubStation_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    _objCommonMethods.BindFeeders(ddlFeeder, this.SubStation, Resource.DDL_ALL, true);
        //    ddlFeeder.Items.Insert(0, new ListItem("ALL", ""));
        //}
        //protected void ddlFeeder_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    _objCommonMethods.BindTransformers(ddlTransformer, this.Feeder, false);
        //    ddlTransformer.Items.Insert(0, new ListItem("ALL", ""));
        //}
        //protected void ddlTransformer_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    _objCommonMethods.BindPoles(ddlPole, this.Transformer, false);
        //    ddlPole.Items.Insert(0, new ListItem("ALL", ""));
        //}

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindGrid();
            UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
        }
        protected void gvAccountStatus_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblBillAmount = (Label)e.Row.FindControl("lblBillAmount");
                    lblBillAmount.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblBillAmount.Text), 2, Constants.MILLION_Format);
                    Label lblTotalPaidAmount = (Label)e.Row.FindControl("lblTotalPaidAmount");
                    lblTotalPaidAmount.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblTotalPaidAmount.Text), 2, Constants.MILLION_Format);
                    Label lblDueAmount = (Label)e.Row.FindControl("lblDueAmount");
                    lblDueAmount.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblDueAmount.Text), 2, Constants.MILLION_Format);

                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        //protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        BindMonths();
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //    }
        //}

        //protected void btnExportExcel_Click(object sender, EventArgs e)
        //{
        //    DataSet dataSet = new DataSet();
        //    ReportsBe _objReportsBe = new ReportsBe();
        //    _objReportsBe.BU_ID = this.BusinessUnitName;
        //    _objReportsBe.SU_ID = this.ServiceUnitName;
        //    _objReportsBe.ServiceCenterId = this.ServiceCenterName;
        //    _objReportsBe.CycleId = this.Cycle;
        //    _objReportsBe.SubStationId = this.SubStation;
        //    _objReportsBe.FeederId = this.Feeder;
        //    _objReportsBe.TransFormerId = this.Transformer;
        //    _objReportsBe.PoleId = this.Pole;
        //    _objReportsBe.YearName = this.Year;
        //    _objReportsBe.MonthName = this.Month;
        //    _objReportsBe.PageNo = Convert.ToInt32(hfPageNo.Value);
        //    _objReportsBe.PageSize = PageSize;

        //    XmlNodeReader xmlReader = new XmlNodeReader(_objReportsBal.Get(_objReportsBe, ReturnType.Fetch));
        //    dataSet.ReadXml(xmlReader);
        //    if (dataSet.Tables[0].Rows.Count > 0)
        //    {
        //        DataTable dt1 = dataSet.Tables[0];
        //        _objCommonMethods.ExportToExcel(dt1, "AccountStatus_Report");
        //    }
        //}

        protected void cblBusinessUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                cblBooks.Items.Clear();
                cblServiceCenters.Items.Clear();
                cblServiceUnits.Items.Clear();
                chkCycleAll.Checked = chkBooksAll.Checked = chkSCAll.Checked = ChkSUAll.Checked = ChkBUAll.Checked = false;

                divServiceUnits.Visible = divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = chkSCAll.Checked = divServiceCenters.Visible = false;
                if (!string.IsNullOrEmpty(BusinessUnitName))
                {
                    _objCommonMethods.BindUserServiceUnits_BuIds(cblServiceUnits, BusinessUnitName, Resource.DDL_ALL, false);
                    if (cblServiceUnits.Items.Count > 0)
                        divServiceUnits.Visible = true;
                    else
                        divServiceUnits.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //cblCycles.Items.Clear();
                //cblBooks.Items.Clear();
                cblServiceCenters.Items.Clear();
                //divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = chkSCAll.Checked = divServiceCenters.Visible = false;
                if (!string.IsNullOrEmpty(ServiceUnitName))
                {
                    _objCommonMethods.BindUserServiceCenters_SuIds(cblServiceCenters, ServiceUnitName, Resource.DDL_ALL, false);
                    if (cblServiceCenters.Items.Count > 0)
                        divServiceCenters.Visible = true;
                    else
                        divServiceCenters.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceCenters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                cblBooks.Items.Clear();
                divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = false;
                if (!string.IsNullOrEmpty(ServiceCenterName))
                {
                    _objCommonMethods.BindCyclesByBUSUSC(cblCycles, BusinessUnitName, ServiceUnitName, ServiceCenterName, true, string.Empty, false);
                    if (cblCycles.Items.Count > 0)
                        divCycles.Visible = true;
                    else
                        divCycles.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblCycles_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblBooks.Items.Clear();
                chkBooksAll.Checked = divBookNos.Visible = false;
                if (!string.IsNullOrEmpty(Cycle))
                {
                    _objCommonMethods.BindAllBookNumbers(cblBooks, Cycle, true, false, string.Empty);
                    if (cblBooks.Items.Count > 0)
                        divBookNos.Visible = true;
                    else
                        divBookNos.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }


        #endregion

        #region ddlEvents
        protected void ddlBusinessUnitName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //_objCommonMethods.BindUserServiceUnits_BuIds(ddlServiceUnitName, BusinessUnitName, Resource.DDL_ALL, false);

                try
                {
                    _objCommonMethods.BindUserServiceUnits_BuIds(cblServiceUnits, BusinessUnitName, Resource.DDL_ALL, false);
                    if (cblServiceUnits.Items.Count > 0)
                        divServiceUnits.Visible = true;
                    else
                        divServiceUnits.Visible = false;
                }
                catch (Exception ex)
                {
                    Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                    try
                    {
                        _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                    }
                    catch (Exception logex)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlServiceUnitName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //_objCommonMethods.BindUserServiceCenters_SuIds(ddlServiceCenterName, ServiceUnitName, Resource.DDL_ALL, false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlServiceCenterName_SelectedIndexChanged(object sender, EventArgs e)
        {
            //_objCommonMethods.BindCyclesByBUSUSC(ddlCycle, BusinessUnitName, ServiceUnitName, ServiceCenterName, true, Resource.DDL_ALL, false);
        }
        protected void ddlCycle_SelectedIndexChanged(object sender, EventArgs e)
        {
            //_objCommonMethods.BindAllBookNumbers(ddlBookNo, Cycle, true, false, Resource.DDL_ALL);
            //_objCommonMethods.BindBookNumbersByServiceDetails(ddlBookNo, BusinessUnitName, ServiceUnitName, ServiceCenterName, false);
            //ddlBookNo.Items.Insert(0, new ListItem("ALL", ""));

        }
        //protected void ddlSubStation_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    _objCommonMethods.BindFeeders(ddlFeeder, this.SubStation, Resource.DDL_ALL, true);
        //}
        //protected void ddlFeeder_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    _objCommonMethods.BindTransformers(ddlTransformer, this.Feeder, false);
        //    ddlTransformer.Items.Insert(0, new ListItem("ALL", ""));
        //}
        //protected void ddlTransformer_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    _objCommonMethods.BindPoles(ddlPole, this.Transformer, false);
        //    ddlPole.Items.Insert(0, new ListItem("ALL", ""));
        //}
        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindMonths();
        }
        #endregion

        //#region Paging
        //protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        BindAccountStatusReport();
        //        hfPageSize.Value = ddlPageSize.SelectedItem.Text;
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnFirst_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        BindAccountStatusReport();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnPrevious_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum -= Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        BindAccountStatusReport();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnNext_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum += Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        BindAccountStatusReport();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnLast_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = hfLastPage.Value;
        //        BindAccountStatusReport();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //private void CreatePagingDT(int TotalNoOfRecs)
        //{
        //    try
        //    {
        //        double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
        //        DataTable dtPages = new DataTable();
        //        dtPages.Columns.Add("PageNo", typeof(int));
        //        int currentpage = Convert.ToInt32(hfPageNo.Value);
        //        lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
        //        _objiDsignBal.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
        //        lblCurrentPage.Text = hfPageNo.Value;
        //        if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor =System.Drawing.Color.Gray; }
        //        if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //private void BindPagingDropDown(int TotalRecords)
        //{
        //    try
        //    {
        //        _objCommonMethods.BindPagingDropDownListReport(TotalRecords, this.PageSize, ddlPageSize);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //#endregion

        #region Methods
        //private void BindBusinessUnits()
        //{
        //    _objCommonMethods.BindUserBusinessUnits(ddlBusinessUnitName, string.Empty, false, Resource.DDL_ALL);
        //    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
        //    {
        //        string BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
        //        ddlBusinessUnitName.SelectedIndex = ddlBusinessUnitName.Items.IndexOf(ddlBusinessUnitName.Items.FindByValue(BUID));
        //        ddlBusinessUnitName.Enabled = false;
        //        ddlBusinessUnitName_SelectedIndexChanged(ddlBusinessUnitName, new EventArgs());
        //        //ddlServiceUnitName_SelectedIndexChanged(ddlServiceUnitName, new EventArgs());
        //        //_objCommonMethods.BindCyclesByBUSUSC(ddlCycle, BUID, string.Empty, string.Empty, true, Resource.DDL_ALL, false);
        //       // _objCommonMethods.BindAllBookNumbers(ddlBookNo, Cycle, true, false, Resource.DDL_ALL);
        //    }
        //    BindYears();
        //    BindMonths();
        //}
        private void CheckBoxesJS()
        {
            ChkBUAll.Attributes.Add("onclick", "CheckAll('" + ChkBUAll.ClientID + "','" + cblBusinessUnits.ClientID + "')");
            ChkSUAll.Attributes.Add("onclick", "CheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            chkSCAll.Attributes.Add("onclick", "CheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            chkCycleAll.Attributes.Add("onclick", "CheckAll('" + chkCycleAll.ClientID + "','" + cblCycles.ClientID + "')");
            chkBooksAll.Attributes.Add("onclick", "CheckAll('" + chkBooksAll.ClientID + "','" + cblBooks.ClientID + "')");

            cblBusinessUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkBUAll.ClientID + "','" + cblBusinessUnits.ClientID + "')");
            cblServiceUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            cblServiceCenters.Attributes.Add("onclick", "UnCheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            cblCycles.Attributes.Add("onclick", "UnCheckAll('" + chkCycleAll.ClientID + "','" + cblCycles.ClientID + "')");
            cblBooks.Attributes.Add("onclick", "UnCheckAll('" + chkBooksAll.ClientID + "','" + cblBooks.ClientID + "')");
        }
        public void BindGrid()
        {
            divGridContent1.Visible = divGridContent.Visible = Label1.Visible = Label2.Visible = lblTotalBalence.Visible = lblTotalCustomers.Visible = false;

            ReportsBe _objReportsBe = new ReportsBe();
            _objReportsBe.BU_ID = this.BusinessUnitName;
            _objReportsBe.SU_ID = this.ServiceUnitName;
            _objReportsBe.ServiceCenterId = this.ServiceCenterName;
            _objReportsBe.CycleId = this.Cycle;
            _objReportsBe.BookNo = this.BookNo;
            //_objReportsBe.SubStationId = this.SubStation;
            //_objReportsBe.FeederId = this.Feeder;
            //_objReportsBe.TransFormerId = this.Transformer;
            //_objReportsBe.PoleId = this.Pole;
            _objReportsBe.YearName = this.Year;
            _objReportsBe.MonthName = this.Month;
            _objReportsBe.PageNo = Convert.ToInt32(hfPageNo.Value);
            _objReportsBe.PageSize = PageSize;
            //ReportsListBe _objReportsListBe = _objiDsignBal.DeserializeFromXml<ReportsListBe>(_objReportsBal.Get(_objReportsBe, ReturnType.Fetch));
            //DataTable dt = new DataTable();
            //dt = _objiDsignBal.ConvertListToDataSet<ReportsBe>(_objReportsListBe.items).Tables[0];

            DataSet ds = new DataSet();
            ds = _objReportsBal.GetReport(_objReportsBe, ReturnType.Get);

            //lblTotalBalence.Text = lblTotalBalence1.Text = dt.Compute("Sum(DueAmount)", "").ToString();
            //lblTotalCustomers.Text = lblTotalCustomers1.Text = dt.Compute("Count(RowNumber)", "").ToString();
            lblYearMonth.Text = lblYearMonth1.Text = " : " + ddlMonth.SelectedItem.Text + "-" + ddlYear.SelectedItem.Text;
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvAccountStatus.DataSource = ds.Tables[0];
                gvAccountStatus.DataBind();
                divgrid.Visible = UCPaging1.Visible = UCPaging2.Visible = true;
                lblTotalCustomers.Text = lblTotalCustomers1.Text = hfTotalRecords.Value = ds.Tables[0].Rows[0]["TotalRecords"].ToString();
                divExport.Visible = divGridContent1.Visible = divGridContent.Visible = Label1.Visible = Label2.Visible = lblTotalBalence.Visible = lblTotalCustomers.Visible = true;
                //lblTotalBalence.Text = lblTotalBalence1.Text = _objReportsListBe.items[0].TotalDueAmount.ToString();
                lblTotalBalence.Text = lblTotalBalence1.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(ds.Tables[0].Rows[0]["TotalDueAmount"]), 2, Constants.MILLION_Format);
            }
            else
            {
                UCPaging1.Visible = UCPaging2.Visible = false;
                divExport.Visible = divGridContent1.Visible = divGridContent.Visible = Label1.Visible = Label2.Visible = lblTotalBalence.Visible = lblTotalCustomers.Visible = false;
                hfTotalRecords.Value = Constants.Zero.ToString();
                gvAccountStatus.DataSource = new DataTable();
                gvAccountStatus.DataBind();
            }


        }
        private void BindYears()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                xmlResult = _objBillGenerationBal.GetDetails(ReturnType.Bulk);
                BillGenerationListBe objBillsListBE = _objiDsignBal.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                _objiDsignBal.FillDropDownList(_objiDsignBal.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlYear, "Year", "Year", "ALL", true);

                //oldCode
                //int CurrentYear = DateTime.Now.Year;
                //int year = DateTime.Now.Year - Constants.BILLING_YEAR_LENGTH;
                //for (int i = year; i <= CurrentYear; i++)
                //{
                //    ListItem item = new ListItem();
                //    item.Text = item.Value = i.ToString();
                //    ddlYear.Items.Add(item);
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void BindMonths()
        {
            try
            {
                //XmlDocument xmlResult = new XmlDocument();
                //BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                //_objBillGenerationBe.YearName = this.Year;
                //xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.Bulk);
                //BillGenerationListBe objBillsListBE = _objiDsignBal.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                //_objiDsignBal.FillDropDownList(_objiDsignBal.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlMonth, "MonthName", "Month", "ALL", true);

                //oldCode
                DataSet dsMonths = new DataSet();
                dsMonths.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["Months"]));
                _objiDsignBal.FillDropDownList(dsMonths.Tables[0], ddlMonth, "name", "value", false);
                ddlMonth.Items.Insert(0, new ListItem("ALL", ""));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region ExportExcel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {

                ReportsBe _objReportsBe = new ReportsBe();
                _objReportsBe.BU_ID = this.BusinessUnitName;
                _objReportsBe.SU_ID = this.ServiceUnitName;
                _objReportsBe.ServiceCenterId = this.ServiceCenterName;
                _objReportsBe.CycleId = this.Cycle;
                _objReportsBe.BookNo = this.BookNo;
                //_objReportsBe.SubStationId = this.SubStation;
                //_objReportsBe.FeederId = this.Feeder;
                //_objReportsBe.TransFormerId = this.Transformer;
                //_objReportsBe.PoleId = this.Pole;
                _objReportsBe.YearName = this.Year;
                _objReportsBe.MonthName = this.Month;
                _objReportsBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                _objReportsBe.PageSize = Convert.ToInt32(hfTotalRecords.Value);
                //ReportsListBe _objReportsListBe = _objiDsignBal.DeserializeFromXml<ReportsListBe>(_objReportsBal.Get(_objReportsBe, ReturnType.Fetch));

                DataSet ds = new DataSet();
                ds = _objReportsBal.GetReport(_objReportsBe, ReturnType.Get);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    var BUlist = (from list in ds.Tables[0].AsEnumerable()
                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                  orderby list["CycleName"] ascending
                                  group list by list.Field<string>("BusinessUnitName") into g
                                  select new { BusinessUnit = g.Key }).ToList();

                    //var SUlist = (from list in ds.Tables[0].AsEnumerable()
                    //              orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                    //              orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                    //              orderby list["CycleName"] ascending
                    //              group list by list.Field<string>("ServiceUnitName") into g
                    //              select new { ServiceUnit = g.Key }).ToList();

                    //var Cyclelist = (from list in ds.Tables[0].AsEnumerable()
                    //                 orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                    //                 orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                    //                 orderby list["CycleName"] ascending
                    //                 group list by list.Field<string>("CycleName") into g
                    //                 select new { Cycle = g.Key }).ToList();

                    //var Booklist = (from list in ds.Tables[0].AsEnumerable()
                    //                orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                    //                orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                    //                orderby list["CycleName"] ascending
                    //                group list by list.Field<string>("BookNumber") into g
                    //                select new { Book = g.Key }).ToList();

                    if (BUlist.Count() > 0)
                    {
                        XlsDocument xls = new XlsDocument();
                        Worksheet mainSheet = xls.Workbook.Worksheets.Add("Summary");
                        Cells mainCells = mainSheet.Cells;
                        Cell _objMainCells = null;

                        _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
                        mainCells.Add(4, 2, "Report Code ");
                        mainCells.Add(4, 3, Constants.AccountStatusReport_Code).Font.Bold = true;
                        mainCells.Add(4, 4, "Report Name");
                        mainCells.Add(4, 5, Constants.AccountStatusReport_Name).Font.Bold = true;
                        mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
                        _objMainCells.Font.Weight = FontWeight.ExtraBold;
                        _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                        _objMainCells.Font.FontFamily = FontFamilies.Roman;

                        int MainSheetDetailsRowNo = 6;
                        _objMainCells = mainCells.Add(MainSheetDetailsRowNo, 1, "S.No.");
                        _objMainCells.Font.Bold = true;
                        _objMainCells.UseBorder = true;
                        _objMainCells = mainCells.Add(MainSheetDetailsRowNo, 2, "Service Center");
                        _objMainCells.Font.Bold = true;
                        _objMainCells.UseBorder = true;
                        _objMainCells = mainCells.Add(MainSheetDetailsRowNo, 3, "Customer Count");
                        _objMainCells.Font.Bold = true;
                        _objMainCells.UseBorder = true;
                        _objMainCells = mainCells.Add(MainSheetDetailsRowNo, 4, "Total Due Amount");
                        _objMainCells.Font.Bold = true;
                        _objMainCells.UseBorder = true;

                        decimal TotalDueAmount = 0;

                        var SClist = (from list in ds.Tables[0].AsEnumerable()
                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                      orderby list["CycleName"] ascending
                                      group list by list.Field<string>("ServiceCenterName") into g
                                      select new { ServiceCenter = g.Key }).ToList();

                        for (int k = 0; k < SClist.Count; k++)//SC
                        {
                            var Customers = (from list in ds.Tables[0].AsEnumerable()
                                             where list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                             select list).ToList();

                            TotalDueAmount = Customers.Sum(x => Convert.ToDecimal(x["DueAmount"]));
                            string DueAmount = _objCommonMethods.GetCurrencyFormat(TotalDueAmount, 2, Constants.MILLION_Format);

                            MainSheetDetailsRowNo++;
                            mainCells.Add(MainSheetDetailsRowNo, 1, k + 1).UseBorder = true;
                            mainCells.Add(MainSheetDetailsRowNo, 2, SClist[k].ServiceCenter).UseBorder = true;
                            mainCells.Add(MainSheetDetailsRowNo, 3, Customers.Count).UseBorder = true;
                            mainCells.Add(MainSheetDetailsRowNo, 4, DueAmount).UseBorder = true;
                        }

                        decimal finalTotal = 0;

                        for (int i = 0; i < BUlist.Count; i++)//BU
                        {
                            var SUlistByBU = (from list in ds.Tables[0].AsEnumerable()
                                              where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                              orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                              orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                              group list by list.Field<string>("ServiceUnitName") into g
                                              select new { ServiceUnitName = g.Key }).ToList();

                            for (int j = 0; j < SUlistByBU.Count; j++)//SU
                            {
                                var SClistBySU = (from list in ds.Tables[0].AsEnumerable()
                                                  where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                  && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                  group list by list.Field<string>("ServiceCenterName") into g
                                                  select new { ServiceCenter = g.Key }).ToList();

                                for (int k = 0; k < SClistBySU.Count; k++)//SC
                                {
                                    var CyclelistBySC = (from list in ds.Tables[0].AsEnumerable()
                                                         where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                         && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                         && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                         orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                         orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                         group list by list.Field<string>("CycleName") into g
                                                         select new { CycleName = g.Key }).ToList();

                                    if (CyclelistBySC.Count > 0)
                                    {
                                        int DetailsRowNo = 2;
                                        int CustomersRowNo = 5;

                                        Worksheet sheet = xls.Workbook.Worksheets.Add(SClistBySU[k].ServiceCenter);
                                        Cells cells = sheet.Cells;
                                        Cell _objCell = null;

                                        for (int l = 0; l < CyclelistBySC.Count; l++)//Cycle
                                        {
                                            var BooklistByCycle = (from list in ds.Tables[0].AsEnumerable()
                                                                   where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                   && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                   && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                   && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                   orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                   orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                   group list by list.Field<string>("BookNumber") into g
                                                                   select new { BookNumber = g.Key }).ToList();

                                            for (int m = 0; m < BooklistByCycle.Count; m++)//Book
                                            {
                                                var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                                 where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                 && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                 && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                 && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                 && list.Field<string>("BookNumber") == BooklistByCycle[m].BookNumber
                                                                 orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                 orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                 orderby Convert.ToString(list["CycleName"]) ascending
                                                                 select list).ToList();

                                                if (Customers.Count() > 0)
                                                {
                                                    cells.Add(DetailsRowNo, 1, "Business Unit");
                                                    cells.Add(DetailsRowNo, 3, "Service Unit");
                                                    cells.Add(DetailsRowNo, 5, "Service Center");
                                                    cells.Add(DetailsRowNo + 1, 1, "BookGroup");
                                                    cells.Add(DetailsRowNo + 1, 3, "Book Code");

                                                    cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 4, SUlistByBU[j].ServiceUnitName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 6, SClist[k].ServiceCenter).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 2, CyclelistBySC[l].CycleName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 4, BooklistByCycle[m].BookNumber).Font.Bold = true;

                                                    _objCell = cells.Add(DetailsRowNo + 3, 1, "Sno");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 2, "Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 3, "Old Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 4, "Name");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 5, "Service Address");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 6, "Bill Generated Date");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 7, "Bill Amount");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 8, "Total Paid Amount");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 9, "Total Due Amount");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;

                                                    CustomersRowNo++;
                                                    decimal subtotal = 0;
                                                    for (int n = 0; n < Customers.Count; n++)
                                                    {
                                                        DataRow dr = (DataRow)Customers[n];
                                                        cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                                                        cells.Add(CustomersRowNo, 2, dr["AccountNo"].ToString());
                                                        cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
                                                        cells.Add(CustomersRowNo, 4, dr["Name"].ToString());
                                                        cells.Add(CustomersRowNo, 5, dr["ServiceAddress"]);
                                                        cells.Add(CustomersRowNo, 6, dr["BillDate"]);
                                                        cells.Add(CustomersRowNo, 7, _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(dr["BillAmount"].ToString()), 2, Constants.MILLION_Format));
                                                        cells.Add(CustomersRowNo, 8, _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(dr["TotalPaidAmount"].ToString()), 2, Constants.MILLION_Format));
                                                        cells.Add(CustomersRowNo, 9, _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(dr["DueAmount"].ToString()), 2, Constants.MILLION_Format));

                                                        CustomersRowNo++;
                                                        subtotal += Convert.ToDecimal(dr["DueAmount"]);
                                                    }
                                                    _objCell = cells.Add(CustomersRowNo + 1, 8, "Sub Total");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(CustomersRowNo + 1, 9, _objCommonMethods.GetCurrencyFormat(subtotal, 2, Constants.MILLION_Format));
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;

                                                    finalTotal += subtotal;
                                                    DetailsRowNo = CustomersRowNo + 3;
                                                    CustomersRowNo = CustomersRowNo + 6;
                                                }
                                            }
                                        }

                                        _objCell = cells.Add(CustomersRowNo - 3, 8, "Grand Total");
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                        _objCell = cells.Add(CustomersRowNo - 3, 9, _objCommonMethods.GetCurrencyFormat(finalTotal, 2, Constants.MILLION_Format));
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                    }
                                }
                            }
                        }
                        string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                        fileName = "AccountStatusRerpot_" + fileName;
                        string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                        xls.FileName = filePath;
                        xls.Save();
                        _objiDsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                    }
                    else
                        Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                    Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}