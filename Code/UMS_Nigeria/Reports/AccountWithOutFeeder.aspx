﻿<%@ Page Title="::Customers With Out Feeder::" Language="C#"  MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="AccountWithOutFeeder.aspx.cs" Inherits="UMS_Nigeria.Reports.AccountWithOutFeeder" %>
    <%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upRptAccWithCreditBalance" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAccountWoCycle" runat="server" Text="<%$ Resources:Resource, ACCOUNT_WITHOUT_FEEDER%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT_NAME%>"></asp:Literal><br />
                            <asp:DropDownList ID="ddlBU" runat="server" CssClass="text_box select_box" AutoPostBack="true" OnSelectedIndexChanged="ddlBU_SelectedIndexChanged">
                                <asp:ListItem Value="">ALL</asp:ListItem>
                            </asp:DropDownList>
                            </div>
                            <div id="divServiceUnits" runat="server" Visible="False" class="text-inner">
                                <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal><br />
                                    <asp:CheckBox runat="server" ID="ChkSUAll" Text="All" AutoPostBack="true" /><br />
                                    <asp:CheckBoxList ID="cblServiceUnits" Width="45%" AutoPostBack="true" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceUnits_SelectedIndexChanged">
                                    </asp:CheckBoxList>
                            </div>
                            <div id="divServiceCenters" runat="server" Visible="False" class="text-inner">
                                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal><br />
                                    <asp:CheckBox runat="server" ID="chkSCAll" Text="All" AutoPostBack="true" /><br/>
                                    <asp:CheckBoxList ID="cblServiceCenters" Width="45%" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server">
                                    </asp:CheckBoxList>
                            </div>
                            
                        </div>
                        
                        
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <asp:Button ID="btnSearch" CssClass="box_s" Text="<%$ Resources:Resource, SEARCH%>"
                                    runat="server" OnClick="btnSearch_Click" />
                        </div>
                    </div>
                </div>
                <div class="grid_boxes">
                    <div class="grid_paging_top">
                        <div class="paging_top_title">
                            <asp:Literal ID="litBuList" runat="server" Text="<%$ Resources:Resource, ACCOUNT_WITHOUT_FEEDER_LIST%>"></asp:Literal>
                        </div>
                        <div id="divGridContent" align="center" runat="server" visible="false">
                            <div class="fltl" style="margin-top: 8px;">
                                <div class="consume_name" style="width: 121px;">
                                    <asp:Literal ID="Label3" runat="server" Text="<%$ Resources:Resource, TOTAL_CUSTOMERS%>"></asp:Literal>
                                </div>
                                <div class="consume_input" style="width: 50px; margin-top: 4px;">
                                    <asp:Label ID="lblTotalCustomers" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="paging_top_right_content" id="divPaging1" runat="server">
                            <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                        </div>
                        <div align="right" style="padding-right: 3px;" class="marg_10t" runat="server" id="divExport">
                                <asp:Button ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" CssClass="excel"
                                    Text="Export to excel" /></div>
                        </div>
                    </div>
                <div class="grid_tb" id="divgrid" runat="server">
                    <asp:GridView ID="gvAccountWithoutFeeder" runat="server" AutoGenerateColumns="False"
                                AlternatingRowStyle-CssClass="color" OnRowCommand="gvAccountWithoutFeeder_RowCommand" HeaderStyle-CssClass="grid_tb_hd">
                                <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                <EmptyDataTemplate>
                                    No Details Found Without Feeder.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, GRID_SNO%>" DataField="RowNumber" ItemStyle-CssClass="grid_tb_td"/>
                                    <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>"
                                        DataField="AccountNo" ItemStyle-CssClass="grid_tb_td"/>
                                    <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"
                                        DataField="OldAccountNo" ItemStyle-CssClass="grid_tb_td"/>
                                    <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, NAME%>"
                                        DataField="Name" ItemStyle-CssClass="grid_tb_td"/>
                                    <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, SERVICE_ADDRESS%>"
                                        DataField="Serviceaddress" ItemStyle-CssClass="grid_tb_td"/>
                                    <asp:TemplateField Visible="false" HeaderText="<%$ Resources:Resource, ASSIGNFEEDER%>" ItemStyle-CssClass="grid_tb_td">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lkAssign" CommandArgument='<%#Eval("CustomerUniqueNo") %>' ToolTip="Assign Feeder"
                                                runat="server" Text="<%$ Resources:Resource, ASSIGNFEEDER%>"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle HorizontalAlign="Center" />
                            </asp:GridView>
                    <asp:HiddenField ID="hfPageSize" runat="server" />
                    <asp:HiddenField ID="hfPageNo" runat="server" />
                    <asp:HiddenField ID="hfLastPage" runat="server" />
                    <asp:HiddenField ID="hfTotalRecords" runat="server" />
                </div>
                <div class="grid_boxes">
                    <div id="divPaging2" runat="server">
                        <div class="grid_paging_bottom">
                            <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
     <%--==============Escape button script starts==============--%>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/AccountWithOutFeeder.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
