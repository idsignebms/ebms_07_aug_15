﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AccountWithoutFeeder
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 07-May-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using UMS_NigeriaBE;
using System.Data;
using Resources;
using System.Drawing;
using System.Xml;
using org.in2bits.MyXls;
using System.Configuration;

namespace UMS_Nigeria.Reports
{
    public partial class AccountWithOutFeeder : System.Web.UI.Page
    {
        #region Members
        CommonMethods _objCommonMethods = new CommonMethods();
        ConsumerBal _objConsumerBal = new ConsumerBal();
        ConsumerBe _objConsumerBE = new ConsumerBe();
        iDsignBAL _objIdsignBAL = new iDsignBAL();
        string Key = UMS_Resource.ACCOUNT_WITHOUT_FEEDER;
        public int PageNum;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        public string BU
        {
            get { return string.IsNullOrEmpty(ddlBU.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlBU, ",") : ddlBU.SelectedValue; }
            set { ddlBU.SelectedValue = value.ToString(); }
        }
        public string SU
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceUnits, ","); }
        }
        public string SC
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceCenters, ","); }
        }
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                if (!IsPostBack)
                {
                    string path = string.Empty;
                    path = _objCommonMethods.GetPagePath(this.Request.Url);
                    if (_objCommonMethods.IsAccess(path))
                    {
                        _objCommonMethods.BindBusinessUnits(ddlBU, string.Empty, false);
                        ddlBU.Items.Insert(0, new ListItem("ALL", ""));
                        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        {
                            ddlBU.SelectedIndex = ddlBU.Items.IndexOf(ddlBU.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                            ddlBU.Enabled = false;
                            ddlBU_SelectedIndexChanged(ddlBU, new EventArgs());
                        }
                        CheckBoxesJS();
                        hfPageNo.Value = Constants.pageNoValue.ToString();
                        BindGrid();
                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    }
                    else
                    {
                        Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    }
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }

        }

        //protected void ddlBU_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ddlBU.SelectedIndex > 0)
        //        {
        //            _objCommonMethods.BindServiceUnits(ddlSU, ddlBU.SelectedItem.Value, false);
        //            ddlSU.Items.Insert(0, new ListItem(UMS_Resource.DROPDOWN_SELECT, ""));
        //            ddlSU.Enabled = true;
        //            ddlSC.SelectedIndex = ddlSU.SelectedIndex = 0;
        //            ddlSC.Enabled = false;
        //        }
        //        else
        //        {
        //            ddlSU.SelectedIndex = ddlSC.SelectedIndex = 0;
        //            ddlSC.Enabled = ddlSU.Enabled = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void ddlSU_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ddlSU.SelectedIndex > 0)
        //        {
        //            _objCommonMethods.BindServiceCenters(ddlSC, ddlSU.SelectedItem.Value, false);
        //            ddlSC.Items.Insert(0, new ListItem(UMS_Resource.DROPDOWN_SELECT, ""));
        //            ddlSC.Enabled = true;
        //        }
        //        else
        //        {
        //            ddlSC.SelectedIndex = 0;
        //            ddlSC.Enabled = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void ddlBU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _objCommonMethods.BindUserServiceUnits_BuIds(cblServiceUnits, BU, Resource.DDL_ALL, false);
                if (cblServiceUnits.Items.Count > 0)
                    divServiceUnits.Visible = true;
                else
                    divServiceUnits.Visible = false;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblServiceCenters.Items.Clear();
                chkSCAll.Checked = divServiceCenters.Visible = false;
                if (!string.IsNullOrEmpty(SU))
                {
                    _objCommonMethods.BindUserServiceCenters_SuIds(cblServiceCenters, SU, Resource.DDL_ALL, false);
                    if (cblServiceCenters.Items.Count > 0)
                        divServiceCenters.Visible = true;
                    else
                        divServiceCenters.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvAccountWithoutFeeder_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                LinkButton lkAssign = (LinkButton)row.FindControl("lkAssign");
                Response.Redirect(UMS_Resource.EDIT_CONSUMER_PAGE + "?" + UMS_Resource.QUERY_CUSTOMER_UNIQUE_NO + "=" + _objIdsignBAL.Encrypt(lkAssign.CommandArgument));
            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hfPageNo.Value = Constants.pageNoValue.ToString();
            BindGrid();
            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
        }
        #endregion

        //#region Paging
        //protected void lkbtnFirst_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        BindAccountGv();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnPrevious_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum -= Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        BindAccountGv();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));

        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnNext_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum += Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        BindAccountGv();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));

        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnLast_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = hfLastPage.Value;
        //        BindAccountGv();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        hfPageSize.Value = ddlPageSize.SelectedItem.Text;
        //        BindAccountGv();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //private void CreatePagingDT(int TotalNoOfRecs)
        //{
        //    try
        //    {
        //        double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
        //        DataTable dtPages = new DataTable();
        //        dtPages.Columns.Add("PageNo", typeof(int));
        //        int currentpage = Convert.ToInt32(hfPageNo.Value);
        //        lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
        //        _objIdsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
        //        lblCurrentPage.Text = hfPageNo.Value;
        //        if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.Color.Gray; }
        //        else { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
        //        if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //private void BindPagingDropDown(int TotalRecords)
        //{
        //    try
        //    {
        //        _objCommonMethods.BindPagingDropDownListReport(TotalRecords, this.PageSize, ddlPageSize);
        //        //ddlPageSize.Items.Clear();
        //        //if (TotalRecords < Constants.PageSizeStarts)
        //        //{
        //        //    ListItem item = new ListItem(Constants.PageSizeStarts.ToString(), Constants.PageSizeStarts.ToString());
        //        //    ddlPageSize.Items.Add(item);
        //        //}
        //        //else
        //        //{
        //        //    int previousSize = Constants.PageSizeStarts;
        //        //    for (int i = Constants.PageSizeStarts; i <= TotalRecords; i = i + Constants.PageSizeIncrement)
        //        //    {
        //        //        ListItem item = new ListItem(i.ToString(), i.ToString());
        //        //        ddlPageSize.Items.Add(item);
        //        //        previousSize = i;
        //        //    }
        //        //    if (previousSize < TotalRecords)
        //        //    {
        //        //        ListItem item = new ListItem((previousSize + Constants.PageSizeIncrement).ToString(), (previousSize + Constants.PageSizeIncrement).ToString());
        //        //        ddlPageSize.Items.Add(item);
        //        //    }
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //#endregion

        #region Methods
        private void CheckBoxesJS()
        {
            ChkSUAll.Attributes.Add("onclick", "CheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            chkSCAll.Attributes.Add("onclick", "CheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");

            cblServiceUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            cblServiceCenters.Attributes.Add("onclick", "UnCheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");

        }
        public void BindGrid()
        {
            _objConsumerBE.BU_ID = BU;
            _objConsumerBE.SU_ID = SU;
            _objConsumerBE.ServiceCenterId = SC;
            _objConsumerBE.PageNo = Convert.ToInt32(hfPageNo.Value);
            _objConsumerBE.PageSize = PageSize;
            ConsumerListBe objConsumerList = _objIdsignBAL.DeserializeFromXml<ConsumerListBe>(_objConsumerBal.GetServiceDetails(_objConsumerBE, ReturnType.Bulk));
            if (objConsumerList.items.Count > 0)
            {
                UCPaging1.Visible = UCPaging2.Visible = divExport.Visible = true;
                lblTotalCustomers.Text = hfTotalRecords.Value = objConsumerList.items[0].TotalRecords.ToString();
            }
            else
            {
                UCPaging1.Visible = UCPaging2.Visible = divExport.Visible = false;
                hfTotalRecords.Value = objConsumerList.items.Count.ToString();
            }
            gvAccountWithoutFeeder.DataSource = _objIdsignBAL.ConvertListToDataSet<ConsumerBe>(objConsumerList.items).Tables[0];
            gvAccountWithoutFeeder.DataBind();

        }
        private void Message(string Message, string MessageType)
        {
            try
            {

                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region ExportExcel
        private void CustomersNotFound()
        {
            Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {

            _objConsumerBE.BU_ID = BU;
            _objConsumerBE.SU_ID = SU;
            _objConsumerBE.ServiceCenterId = SC;
            _objConsumerBE.PageNo = Convert.ToInt32(hfPageNo.Value);
            _objConsumerBE.PageSize = Convert.ToInt32(hfTotalRecords.Value);
            ConsumerListBe objConsumerList = _objIdsignBAL.DeserializeFromXml<ConsumerListBe>(_objConsumerBal.GetServiceDetails(_objConsumerBE, ReturnType.Bulk));
            DataSet ds = new DataSet();
            ds = _objIdsignBAL.ConvertListToDataSet<ConsumerBe>(objConsumerList.items);

            if (ds.Tables[0].Rows.Count > 0)
            {
                var BUlist = (from list in ds.Tables[0].AsEnumerable()
                              orderby list["CustomerSortOrder"] ascending
                              group list by list.Field<string>("BusinessUnitName") into g
                              select new { BusinessUnit = g.Key }).ToList();

                var SUlist = (from list in ds.Tables[0].AsEnumerable()
                              orderby list["CustomerSortOrder"] ascending
                              group list by list.Field<string>("ServiceUnitName") into g
                              select new { ServiceUnit = g.Key }).ToList();

                var SClist = (from list in ds.Tables[0].AsEnumerable()
                              orderby list["CustomerSortOrder"] ascending
                              group list by list.Field<string>("ServiceCenterName") into g
                              select new { ServiceCenter = g.Key }).ToList();

                if (BUlist.Count() > 0)
                {
                    XlsDocument xls = new XlsDocument();
                    Worksheet sheet = xls.Workbook.Worksheets.Add(UMS_Resource.SHEET);
                    Cells cells = sheet.Cells;
                    Cell _objCell = null;

                    _objCell = cells.Add(1, 2, Resource.BEDC + "\n" + UMS_Resource.RPT_CUST_WITH_OUT_FEEDER + DateTime.Today.ToShortDateString());
                    sheet.AddMergeArea(new MergeArea(1, 2, 2, 5));
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;

                    int DetailsRowNo = 4;
                    int CustomersRowNo = 7;
                    for (int i = 0; i < BUlist.Count; i++)//BU
                    {
                        for (int j = 0; j < SUlist.Count; j++)//SU
                        {
                            for (int k = 0; k < SClist.Count; k++)//SC
                            {
                                var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                 where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                 && list.Field<string>("ServiceUnitName") == SUlist[j].ServiceUnit
                                                 && list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                                 orderby list["CustomerSortOrder"] ascending
                                                 select list).ToList();

                                if (Customers.Count() > 0)
                                {
                                    cells.Add(DetailsRowNo, 1, "Business Unit");
                                    cells.Add(DetailsRowNo, 3, "Service Unit");
                                    cells.Add(DetailsRowNo, 5, "Service Center");

                                    cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                    cells.Add(DetailsRowNo, 4, SUlist[j].ServiceUnit).Font.Bold = true;
                                    cells.Add(DetailsRowNo, 6, SClist[k].ServiceCenter).Font.Bold = true;

                                    _objCell = cells.Add(DetailsRowNo + 2, 1, "Sno");
                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                    _objCell = cells.Add(DetailsRowNo + 2, 2, "Account No");
                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                    _objCell = cells.Add(DetailsRowNo + 2, 3, "Old Account No");
                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                    _objCell = cells.Add(DetailsRowNo + 2, 4, "Name");
                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                    _objCell = cells.Add(DetailsRowNo + 2, 5, "Service Address");
                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                    _objCell = cells.Add(DetailsRowNo + 2, 6, "Tariff");
                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                    _objCell.Font.FontFamily = FontFamilies.Roman;

                                    for (int n = 0; n < Customers.Count; n++)
                                    {
                                        DataRow dr = (DataRow)Customers[n];
                                        cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                                        cells.Add(CustomersRowNo, 2, dr["AccountNo"].ToString());
                                        cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
                                        cells.Add(CustomersRowNo, 4, dr["Name"].ToString());
                                        cells.Add(CustomersRowNo, 5, dr["ServiceAddress"].ToString());
                                        cells.Add(CustomersRowNo, 6, dr["ClassName"].ToString());

                                        CustomersRowNo++;
                                    }
                                    DetailsRowNo = CustomersRowNo + 2;
                                    CustomersRowNo = CustomersRowNo + 6;
                                }
                            }
                        }
                    }

                    string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                    fileName = "CustomersWithOutFeeder_" + fileName;
                    string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                    xls.FileName = filePath;
                    xls.Save();
                    _objIdsignBAL.DownLoadFile(filePath, "application//vnd.ms-excel");
                }
                CustomersNotFound();
            }
            CustomersNotFound();
        }        
        #endregion
    }
}