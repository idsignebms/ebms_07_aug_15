﻿<%@ Page Title="::Customer With Out BookGroup::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="AccountWithoutCycle.aspx.cs"
    Inherits="UMS_Nigeria.Reports.AccountWithoutCycle" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upRptAccWithCreditBalance" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAccountWoCycle" runat="server" Text="<%$ Resources:Resource, ACCOUNT_WITHOUT_CYCLE%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div id="divNotRequired" runat="server" visible="false">
                        <%--AutoPostBack="true" OnSelectedIndexChanged="ddlBU_SelectedIndexChanged"--%>
                        <div class="inner-box">
                            <%--<div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT_NAME%>"></asp:Literal><br />
                               
                                <asp:DropDownList ID="ddlBU" runat="server" CssClass="text-box select_box"  AutoPostBack="true" OnSelectedIndexChanged="ddlBU_SelectedIndexChanged">
                                    <asp:ListItem Value="">ALL</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="text-inner">
                                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER_NAME%>"></asp:Literal><br />
                                <asp:DropDownList ID="ddlSC" runat="server" CssClass="text-box select_box"  Enabled="false">
                                    <asp:ListItem Value="">--Select--</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            
                        </div>--%>
                            <%--AutoPostBack="true" OnSelectedIndexChanged="ddlSU_SelectedIndexChanged"--%>
                            <%--<div class="inner-box2">
                            <div class="text-inner">
                                <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT_NAME%>"></asp:Literal><br />
                                
                                <asp:DropDownList ID="ddlSU" runat="server" CssClass="text-box select_box"  Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="ddlSU_SelectedIndexChanged">
                                    <asp:ListItem Value="">ALL</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="text-inner">
                                <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource,  TARIFF%>"></asp:Literal><br />
                                <asp:DropDownList ID="ddlTariff" runat="server" CssClass="text-box select_box" >
                                    <asp:ListItem Value="">--Select--</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            
                        </div>--%>
                            <%--<div class="inner-box3">
                            <div class="text-inner">
                            </div>
                        </div>--%>
                            <div id="divBusinessUnit" runat="server">
                                <div class="check_baa">
                                    <div class="text-inner_a">
                                        <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                                    </div>
                                    <div class="check_ba">
                                        <div class="text-inner mster-input">
                                            <asp:CheckBox runat="server" ID="ChkBUAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                            <asp:CheckBoxList ID="cblBusinessUnits" class="text-inner-b" AutoPostBack="true"
                                                RepeatDirection="Horizontal" RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblBusinessUnits_SelectedIndexChanged">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div id="divServiceUnits" runat="server" visible="false">
                            <div class="check_baa">
                                <div class="text-inner_a">
                                    <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" ID="ChkSUAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                        <asp:CheckBoxList ID="cblServiceUnits" class="text-inner-b" AutoPostBack="true" RepeatDirection="Horizontal"
                                            RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceUnits_SelectedIndexChanged">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="divServiceCenters" runat="server" visible="false">
                            <div class="check_baa">
                                <div class="text-inner_a">
                                    <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" ID="chkSCAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                        <asp:CheckBoxList ID="cblServiceCenters" class="text-inner-b" RepeatDirection="Horizontal"
                                            AutoPostBack="true" RepeatColumns="3" runat="server">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                    </div>
                    <div id="divTariff" runat="server">
                        <div class="check_baa">
                            <div class="text-inner_a">
                                <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" ID="chkTariffAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                    <asp:CheckBoxList ID="cblTarif" class="text-inner-b" RepeatDirection="Horizontal"
                                        AutoPostBack="true" RepeatColumns="3" runat="server">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <asp:Button ID="btnNext" CssClass="box_s" Text="<%$ Resources:Resource, SEARCH%>"
                                runat="server" OnClick="btnNext_Click" />
                        </div>
                    </div>
                </div>
                <div class="grid_boxes">
                    <div class="grid_paging_top">
                        <div class="paging_top_title">
                            <asp:Literal ID="litBuList" runat="server" Text="<%$ Resources:Resource, GVLIST_WOTHOUTCYCLE%>"></asp:Literal>
                        </div>
                        <div id="divGridContent" align="center" runat="server" visible="false">
                            <div class="consume_name" style="width: 130px; float: left;">
                                <asp:Literal ID="Label3" runat="server" Text="<%$ Resources:Resource, TOTAL_CUSTOMERS%>"></asp:Literal>
                            </div>
                            <div class="consume_input" style="width: 50px; float: left; text-align: left;">
                                <asp:Label ID="lblTotalCustomers" runat="server" Text="--"></asp:Label>
                            </div>
                        </div>
                        <div class="paging_top_right_content" id="divPaging1" runat="server">
                            <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                        </div>
                        <div align="right" class="fltr" runat="server" id="divExport" visible="false">
                            <asp:Button ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" CssClass="exporttoexcelButton" />
                        </div>
                    </div>
                </div>
                <div class="grid_tb" id="divgrid" runat="server">
                    <asp:GridView ID="gvAccountWithoutCycle" runat="server" AutoGenerateColumns="False"
                        AlternatingRowStyle-CssClass="color" HeaderStyle-CssClass="grid_tb_hd">
                        <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                        <EmptyDataTemplate>
                            No Details Found Without Book Group.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:BoundField HeaderText="<%$ Resources:Resource, GRID_SNO%>" DataField="RowNumber"
                                ItemStyle-CssClass="grid_tb_td" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>"
                                DataField="AccountNo" ItemStyle-CssClass="grid_tb_td" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"
                                DataField="OldAccountNo" ItemStyle-CssClass="grid_tb_td" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, NAME%>"
                                DataField="Name" ItemStyle-CssClass="grid_tb_td" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, SERVICE_ADDRESS%>"
                                DataField="ServiceAddress" ItemStyle-CssClass="grid_tb_td" />
                            <asp:BoundField HeaderText="<%$ Resources:Resource, TARIFF%>" DataField="ClassName"
                                ItemStyle-CssClass="grid_tb_td" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, BOOK_NO%>"
                                DataField="BookNo" ItemStyle-CssClass="grid_tb_td" />
                            <asp:BoundField HeaderText="<%$ Resources:Resource, CONNECTION_DATE%>" DataField="ConnectionDate"
                                DataFormatString="{0:M}" ItemStyle-CssClass="grid_tb_td" />
                            <asp:TemplateField Visible="false" HeaderText="<%$ Resources:Resource, ASSIGN_CYCLE%>"
                                ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lkAssign" runat="server" PostBackUrl="~/Mappings/BookCycleMapping.aspx"
                                        Text="<%$ Resources:Resource, ASSIGNCYCLE%>"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle HorizontalAlign="Center" />
                    </asp:GridView>
                    <asp:HiddenField ID="hfPageSize" runat="server" />
                    <asp:HiddenField ID="hfPageNo" runat="server" Value="1" />
                    <asp:HiddenField ID="hfLastPage" runat="server" />
                    <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                </div>
                <div class="grid_boxes">
                    <div id="divPaging2" runat="server">
                        <div class="grid_paging_bottom">
                            <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
    <%--==============Escape button script starts==============--%>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/UnBilledCustomersReport.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
