﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AccountWithoutCycle
                     
 Developer        : Id075-RamaDevi M
 Creation Date    : 12-Apr-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using UMS_NigeriaBE;
using System.Data;
using Resources;
using System.Drawing;
using System.Xml;
using System.Threading;
using System.Globalization;
using System.Configuration;
using org.in2bits.MyXls;

namespace UMS_Nigeria.Reports
{
    public partial class AccountWithoutCycle : System.Web.UI.Page
    {
        #region Members
        CommonMethods _objCommonMethods = new CommonMethods();
        ConsumerBal _objConsumerBal = new ConsumerBal();
        ConsumerBe _objConsumerBE = new ConsumerBe();
        iDsignBAL _objIdsignBAL = new iDsignBAL();
        string Key = UMS_Resource.WEB_ACCOUNTWITHOUTCYCLE;
        public int PageNum;
        string ReportCode = "BEDC_R1";
        string ReportName = "Customers Without Book Group Rerpot";
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        public string BusinessUnitName
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblBusinessUnits, ","); }
            //get { return string.IsNullOrEmpty(ddlBU.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlBU, ",") : ddlBU.SelectedValue; }
            //set { ddlBU.SelectedValue = value.ToString(); }
        }
        public string SU
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceUnits, ","); }
        }
        public string SC
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceCenters, ","); }
        }
        public string Tariff
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblTarif, ","); }
            //get { return string.IsNullOrEmpty(ddlTariff.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlTariff, ",") : ddlTariff.SelectedValue; }
            //set { ddlTariff.SelectedValue = value.ToString(); }
        }
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    if (Session[UMS_Resource.SESSION_LOGINID] != null)
        //    {
        //        if (!IsPostBack)
        //        {
        //            string path = string.Empty;
        //            path = _objCommonMethods.GetPagePath(this.Request.Url);
        //            if (_objCommonMethods.IsAccess(path))
        //            {
        //                //BindDdls();
        //                //_objCommonMethods.BindBusinessUnits(ddlBU, string.Empty, false);
        //                //ddlBU.Items.Insert(0, new ListItem("ALL", ""));
        //                hfPageNo.Value = Constants.pageNoValue.ToString();
        //                BindGrid();
        //                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
        //                //_objCommonMethods.BindTariffSubTypes(ddlTariff, 0, false);
        //                //ddlTariff.Items.Insert(0, new ListItem("ALL", ""));
        //            }
        //            else
        //            {
        //                Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        //    }

        //}
        //protected void ddlBU_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    _objCommonMethods.BindServiceUnits(ddlSU, ddlBU.SelectedItem.Value, false);
        //    ddlSU.Items.Insert(0, new ListItem("ALL", ""));
        //}

        //protected void ddlSU_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    _objCommonMethods.BindServiceCenters(ddlSC, ddlSU.SelectedItem.Value, false);
        //    ddlSC.Items.Insert(0, new ListItem("ALL", ""));
        //}
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                if (!IsPostBack)
                {
                    string path = string.Empty;
                    path = _objCommonMethods.GetPagePath(this.Request.Url);
                    if (_objCommonMethods.IsAccess(path))
                    {
                        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        {
                            ListItem LiBU = new ListItem(Session[UMS_Resource.SESSION_USER_BUNAME].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());
                            cblBusinessUnits.Items.Add(LiBU);
                            cblBusinessUnits.Items[0].Selected = true;
                            cblBusinessUnits.Enabled = false;
                            ChkBUAll.Visible = false;
                            divServiceUnits.Visible = true;
                            _objCommonMethods.BindUserServiceUnits_BuIds(cblServiceUnits, BusinessUnitName, Resource.DDL_ALL, false);
                        }
                        else
                            _objCommonMethods.BindBusinessUnits(cblBusinessUnits, string.Empty, false);


                        _objCommonMethods.BindTariffSubTypes(cblTarif, 0, false);
                        //BindDropDowns();
                        CheckBoxesJS();
                        hfPageNo.Value = Constants.pageNoValue.ToString();
                        BindGrid();
                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    }
                    else
                    {
                        Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    }
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            hfPageNo.Value = Constants.pageNoValue.ToString();
            BindGrid();
            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
        }

        protected void cblBusinessUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblServiceCenters.Items.Clear();
                cblServiceUnits.Items.Clear();
                chkSCAll.Checked = ChkSUAll.Checked = ChkBUAll.Checked = false;

                divServiceUnits.Visible = chkTariffAll.Checked = chkSCAll.Checked = divServiceCenters.Visible = false;
                if (!string.IsNullOrEmpty(BusinessUnitName))
                {
                    _objCommonMethods.BindUserServiceUnits_BuIds(cblServiceUnits, BusinessUnitName, Resource.DDL_ALL, false);
                    if (cblServiceUnits.Items.Count > 0)
                        divServiceUnits.Visible = true;
                    else
                        divServiceUnits.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblServiceCenters.Items.Clear();
                chkSCAll.Checked = divServiceCenters.Visible = false;
                if (!string.IsNullOrEmpty(SU))
                {
                    _objCommonMethods.BindUserServiceCenters_SuIds(cblServiceCenters, SU, Resource.DDL_ALL, false);
                    if (cblServiceCenters.Items.Count > 0)
                        divServiceCenters.Visible = true;
                    else
                        divServiceCenters.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceCenters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblTarif.Items.Clear();
                divTariff.Visible = chkTariffAll.Checked = false;
                if (!string.IsNullOrEmpty(SC))
                {
                    _objCommonMethods.BindCyclesByBUSUSC(cblTarif, BusinessUnitName, SU, SC, true, string.Empty, false);
                    if (cblTarif.Items.Count > 0)
                        divTariff.Visible = true;
                    else
                        divTariff.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods
        public void BindGrid()
        {
            try
            {
                //_objConsumerBE.BU_ID = ddlBU.SelectedItem.Value;
                //_objConsumerBE.SU_ID = ddlSU.SelectedItem.Value;
                //_objConsumerBE.ServiceCenterId = ddlSC.SelectedValue;
                //_objConsumerBE.Tariff = ddlTariff.SelectedItem.Value;
                //_objConsumerBE.BU_ID = BusinessUnitName;
                //_objConsumerBE.SU_ID = SU;
                //_objConsumerBE.ServiceCenterId = SC;
                _objConsumerBE.Tariff = Tariff;
                _objConsumerBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                _objConsumerBE.PageSize = PageSize;
                DataSet ds = new DataSet();
                //XmlDocument resultant = _objConsumerBal.GetAccountswithoutCycleBAL(_objConsumerBE);
                ds = _objConsumerBal.GetAccountswithoutCycleBAL(_objConsumerBE);

                //ConsumerListBe objConsumerList = _objIdsignBAL.DeserializeFromXml<ConsumerListBe>(resultant);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    divExport.Visible = divGridContent.Visible = UCPaging1.Visible = UCPaging2.Visible = true;
                    lblTotalCustomers.Text = hfTotalRecords.Value = ds.Tables[0].Rows[0]["TotalRecords"].ToString();
                }
                else
                {
                    divExport.Visible = divGridContent.Visible = UCPaging1.Visible = UCPaging2.Visible = false;
                    hfTotalRecords.Value = ds.Tables[0].Rows.Count.ToString();
                }
                gvAccountWithoutCycle.DataSource = ds.Tables[0];
                gvAccountWithoutCycle.DataBind();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        //private void BindDdls()
        //{
        //    try
        //    {
        //        _objCommonMethods.BindUserBusinessUnits(ddlBU, string.Empty, false, Resource.DDL_ALL);
        //        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
        //        {
        //            ddlBU.SelectedIndex = ddlBU.Items.IndexOf(ddlBU.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
        //            ddlBU.Enabled = false;
        //            ddlBU_SelectedIndexChanged(ddlBU, new EventArgs());
        //        }
        //        //_objCommonMethods.BindUserServiceUnits_BuIds(ddlSU, BusinessUnitName, Resource.DDL_ALL, false);
        //        //_objCommonMethods.BindUserServiceCenters_SuIds(ddlSC, ServiceUnitName, Resource.DDL_ALL, false);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        private void CheckBoxesJS()
        {
            ChkBUAll.Attributes.Add("onclick", "CheckAll('" + ChkBUAll.ClientID + "','" + cblBusinessUnits.ClientID + "')");
            ChkSUAll.Attributes.Add("onclick", "CheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            chkSCAll.Attributes.Add("onclick", "CheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            chkTariffAll.Attributes.Add("onclick", "CheckAll('" + chkTariffAll.ClientID + "','" + cblTarif.ClientID + "')");

            cblBusinessUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkBUAll.ClientID + "','" + cblBusinessUnits.ClientID + "')");
            cblServiceUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            cblServiceCenters.Attributes.Add("onclick", "UnCheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            cblTarif.Attributes.Add("onclick", "UnCheckAll('" + chkTariffAll.ClientID + "','" + cblTarif.ClientID + "')");
        }
        private void Message(string Message, string MessageType)
        {
            try
            {

                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        //#region Paging
        //private void CreatePagingDT(int TotalNoOfRecs)
        //{
        //    try
        //    {
        //        double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
        //        DataTable dtPages = new DataTable();
        //        dtPages.Columns.Add("PageNo", typeof(int));
        //        int currentpage = Convert.ToInt32(hfPageNo.Value);
        //        lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
        //        _objIdsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
        //        lblCurrentPage.Text = hfPageNo.Value;
        //        if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
        //        else { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Blue; }
        //        if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //private void BindPagingDropDown(int TotalRecords)
        //{
        //    try
        //    {
        //        _objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
        //        //ddlPageSize.Items.Clear();
        //        //if (TotalRecords < Constants.PageSizeStarts)
        //        //{
        //        //    ListItem item = new ListItem(Constants.PageSizeStarts.ToString(), Constants.PageSizeStarts.ToString());
        //        //    ddlPageSize.Items.Add(item);
        //        //}
        //        //else
        //        //{
        //        //    int previousSize = Constants.PageSizeStarts;
        //        //    for (int i = Constants.PageSizeStarts; i <= TotalRecords; i = i + Constants.PageSizeIncrement)
        //        //    {
        //        //        ListItem item = new ListItem(i.ToString(), i.ToString());
        //        //        ddlPageSize.Items.Add(item);
        //        //        previousSize = i;
        //        //    }
        //        //    if (previousSize < TotalRecords)
        //        //    {
        //        //        ListItem item = new ListItem((previousSize + Constants.PageSizeIncrement).ToString(), (previousSize + Constants.PageSizeIncrement).ToString());
        //        //        ddlPageSize.Items.Add(item);
        //        //    }
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void lkbtnFirst_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        BindAccountGv();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void lkbtnPrevious_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum -= Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        BindAccountGv();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));

        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void lkbtnNext_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum += Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        BindAccountGv();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));

        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void lkbtnLast_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = hfLastPage.Value;
        //        BindAccountGv();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        hfPageSize.Value = ddlPageSize.SelectedItem.Text;
        //        BindAccountGv();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //#endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region ExportExcel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            _objConsumerBE.Tariff = Tariff;
            _objConsumerBE.PageNo = Convert.ToInt32(hfPageNo.Value);
            _objConsumerBE.PageSize = Convert.ToInt32(hfTotalRecords.Value);
            DataSet ds = new DataSet();
            ds = _objConsumerBal.GetAccountswithoutCycleBAL(_objConsumerBE);

            if (ds.Tables[0].Rows.Count > 0)
            {
                var customers = ds.Tables[0].Rows;
                int DetailsRowNo = 2;
                int CustomersRowNo = 5;

                XlsDocument xls = new XlsDocument();
                Worksheet sheet = xls.Workbook.Worksheets.Add("Sheet 1");
                Cells cells = sheet.Cells;
                Cell _objCell = null;

                _objCell = cells.Add(1, 4, Resource.BEDC + "\n" + "Accounts With Out Cycle Report As On " + DateTime.Today.ToShortDateString());
                sheet.AddMergeArea(new MergeArea(1, 2, 4, 6));
                _objCell.Font.Weight = FontWeight.ExtraBold;
                _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                _objCell.Font.FontFamily = FontFamilies.Roman;

                _objCell = cells.Add(DetailsRowNo + 2, 1, "S.No");
                _objCell.Font.Weight = FontWeight.ExtraBold;
                _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                _objCell.Font.FontFamily = FontFamilies.Roman;
                _objCell = cells.Add(DetailsRowNo + 2, 2, "Global Account No");
                _objCell.Font.Weight = FontWeight.ExtraBold;
                _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                _objCell.Font.FontFamily = FontFamilies.Roman;
                _objCell = cells.Add(DetailsRowNo + 2, 3, "Old Account No");
                _objCell.Font.Weight = FontWeight.ExtraBold;
                _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                _objCell.Font.FontFamily = FontFamilies.Roman;
                _objCell = cells.Add(DetailsRowNo + 2, 4, "Name");
                _objCell.Font.Weight = FontWeight.ExtraBold;
                _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                _objCell.Font.FontFamily = FontFamilies.Roman;
                _objCell = cells.Add(DetailsRowNo + 2, 5, "Service Address");
                _objCell.Font.Weight = FontWeight.ExtraBold;
                _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                _objCell.Font.FontFamily = FontFamilies.Roman;
                _objCell = cells.Add(DetailsRowNo + 2, 6, "Tariff");
                _objCell.Font.Weight = FontWeight.ExtraBold;
                _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                _objCell.Font.FontFamily = FontFamilies.Roman;
                _objCell = cells.Add(DetailsRowNo + 2, 7, "Book Number");
                _objCell.Font.Weight = FontWeight.ExtraBold;
                _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                _objCell.Font.FontFamily = FontFamilies.Roman;
                _objCell = cells.Add(DetailsRowNo + 2, 8, "Connection Date");
                _objCell.Font.Weight = FontWeight.ExtraBold;
                _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                _objCell.Font.FontFamily = FontFamilies.Roman;

                for (int n = 0; n < customers.Count; n++)
                {
                    DataRow dr = (DataRow)customers[n];
                    cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                    cells.Add(CustomersRowNo, 2, dr["AccountNo"].ToString());
                    cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
                    cells.Add(CustomersRowNo, 4, dr["Name"].ToString());
                    cells.Add(CustomersRowNo, 5, dr["ServiceAddress"]);
                    cells.Add(CustomersRowNo, 6, dr["ClassName"]);
                    cells.Add(CustomersRowNo, 7, dr["BookNo"]);
                    cells.Add(CustomersRowNo, 8, dr["ConnectionDate"]);

                    CustomersRowNo++;
                }
                DetailsRowNo = CustomersRowNo + 2;
                CustomersRowNo = CustomersRowNo + 6;

                string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                fileName = "CustomersWithoutBookGroupRerpot_" + fileName;
                string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                xls.FileName = filePath;
                xls.Save();
                _objIdsignBAL.DownLoadFile(filePath, "application//vnd.ms-excel");
            }
            else
                Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
        }
        //protected void btnExportExcel_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        _objConsumerBE.BU_ID = BusinessUnitName;
        //        _objConsumerBE.SU_ID = SU;
        //        _objConsumerBE.ServiceCenterId = SC;
        //        _objConsumerBE.Tariff = Tariff;
        //        _objConsumerBE.PageNo = Convert.ToInt32(hfPageNo.Value);
        //        _objConsumerBE.PageSize = Convert.ToInt32(hfTotalRecords.Value);
        //        DataSet ds = new DataSet();
        //        ds = _objConsumerBal.GetAccountswithoutCycleBAL(_objConsumerBE);

        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            var BUlist = (from list in ds.Tables[0].AsEnumerable()
        //                          orderby list["CustomerSortOrder"] ascending
        //                          orderby list["BookSortOrder"] ascending
        //                          orderby list["CycleName"] ascending
        //                          group list by list.Field<string>("BusinessUnitName") into g
        //                          select new { BusinessUnit = g.Key }).ToList();

        //            var SUlist = (from list in ds.Tables[0].AsEnumerable()
        //                          orderby list["CustomerSortOrder"] ascending
        //                          orderby list["BookSortOrder"] ascending
        //                          orderby list["CycleName"] ascending
        //                          group list by list.Field<string>("ServiceUnitName") into g
        //                          select new { ServiceUnit = g.Key }).ToList();

        //            var SClist = (from list in ds.Tables[0].AsEnumerable()
        //                          orderby list["CustomerSortOrder"] ascending
        //                          orderby list["BookSortOrder"] ascending
        //                          orderby list["CycleName"] ascending
        //                          group list by list.Field<string>("ServiceCenterName") into g
        //                          select new { ServiceCenter = g.Key }).ToList();

        //            var Cyclelist = (from list in ds.Tables[0].AsEnumerable()
        //                             orderby list["CustomerSortOrder"] ascending
        //                             orderby list["BookSortOrder"] ascending
        //                             orderby list["CycleName"] ascending
        //                             group list by list.Field<string>("CycleName") into g
        //                             select new { Cycle = g.Key }).ToList();

        //            var Booklist = (from list in ds.Tables[0].AsEnumerable()
        //                            orderby list["CustomerSortOrder"] ascending
        //                            orderby list["BookSortOrder"] ascending
        //                            orderby list["CycleName"] ascending
        //                            group list by list.Field<string>("BookCode") into g
        //                            select new { Book = g.Key }).ToList();

        //            if (BUlist.Count() > 0)
        //            {
        //                XlsDocument xls = new XlsDocument();
        //                Worksheet mainSheet = xls.Workbook.Worksheets.Add("Summary");
        //                Cells mainCells = mainSheet.Cells;
        //                Cell _objMainCells = null;

        //                _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
        //                mainCells.Add(4, 2, "Report Code ");
        //                mainCells.Add(4, 3, ReportCode).Font.Bold = true;
        //                mainCells.Add(4, 4, "Report Name");
        //                mainCells.Add(4, 5, ReportName).Font.Bold = true;
        //                mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
        //                _objMainCells.Font.Weight = FontWeight.ExtraBold;
        //                _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
        //                _objMainCells.Font.FontFamily = FontFamilies.Roman;

        //                int MainSheetDetailsRowNo = 6;
        //                mainCells.Add(MainSheetDetailsRowNo, 1, "S.No.").Font.Bold = true;
        //                mainCells.Add(MainSheetDetailsRowNo, 2, "Service Center").Font.Bold = true;
        //                mainCells.Add(MainSheetDetailsRowNo, 3, "Customer Count").Font.Bold = true;

        //                for (int k = 0; k < SClist.Count; k++)//SC
        //                {
        //                    var Customers = (from list in ds.Tables[0].AsEnumerable()
        //                                     where list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
        //                                     select list).ToList();

        //                    MainSheetDetailsRowNo++;
        //                    mainCells.Add(MainSheetDetailsRowNo, 1, k + 1);
        //                    mainCells.Add(MainSheetDetailsRowNo, 2, SClist[k].ServiceCenter);
        //                    mainCells.Add(MainSheetDetailsRowNo, 3, Customers.Count);
        //                }


        //                for (int i = 0; i < BUlist.Count; i++)//BU
        //                {
        //                    for (int j = 0; j < SUlist.Count; j++)//SU
        //                    {
        //                        for (int k = 0; k < SClist.Count; k++)//SC
        //                        {
        //                            for (int l = 0; l < Cyclelist.Count; l++)//Cycle
        //                            {
        //                                for (int m = 0; m < Booklist.Count; m++)//Book
        //                                {
        //                                    int DetailsRowNo = 2;
        //                                    int CustomersRowNo = 5;

        //                                    var Customers = (from list in ds.Tables[0].AsEnumerable()
        //                                                     where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
        //                                                     && list.Field<string>("ServiceUnitName") == SUlist[j].ServiceUnit
        //                                                     && list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
        //                                                     && list.Field<string>("CycleName") == Cyclelist[l].Cycle
        //                                                     && list.Field<string>("BookCode") == Booklist[m].Book
        //                                                     orderby list["CustomerSortOrder"] ascending
        //                                                     orderby list["BookSortOrder"] ascending
        //                                                     orderby list["CycleName"] ascending
        //                                                     select list).ToList();

        //                                    if (Customers.Count() > 0)
        //                                    {
        //                                        Worksheet sheet = xls.Workbook.Worksheets.Add(SClist[k].ServiceCenter);
        //                                        Cells cells = sheet.Cells;
        //                                        Cell _objCell = null;

        //                                        _objCell = cells.Add(DetailsRowNo, 1, "Business Unit");
        //                                        cells.Add(DetailsRowNo, 3, "Service Unit");
        //                                        cells.Add(DetailsRowNo, 5, "Service Center");
        //                                        cells.Add(DetailsRowNo + 1, 1, "Book Group");
        //                                        cells.Add(DetailsRowNo + 1, 3, "Book Code");

        //                                        cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
        //                                        cells.Add(DetailsRowNo, 4, SUlist[j].ServiceUnit).Font.Bold = true;
        //                                        cells.Add(DetailsRowNo, 6, SClist[k].ServiceCenter).Font.Bold = true;
        //                                        cells.Add(DetailsRowNo + 1, 2, Cyclelist[l].Cycle).Font.Bold = true;
        //                                        cells.Add(DetailsRowNo + 1, 4, Booklist[m].Book).Font.Bold = true;


        //                                        _objCell = cells.Add(DetailsRowNo + 2, 1, "S.No");
        //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
        //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
        //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
        //                                        _objCell = cells.Add(DetailsRowNo + 2, 2, "Global Account No");
        //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
        //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
        //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
        //                                        _objCell = cells.Add(DetailsRowNo + 2, 3, "Old Account No");
        //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
        //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
        //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
        //                                        _objCell = cells.Add(DetailsRowNo + 2, 4, "Name");
        //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
        //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
        //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
        //                                        _objCell = cells.Add(DetailsRowNo + 2, 5, "Service Address");
        //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
        //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
        //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
        //                                        _objCell = cells.Add(DetailsRowNo + 2, 6, "Tariff");
        //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
        //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
        //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
        //                                        _objCell = cells.Add(DetailsRowNo + 2, 7, "Book Number");
        //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
        //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
        //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
        //                                        _objCell = cells.Add(DetailsRowNo + 2, 8, "Connection Date");
        //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
        //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
        //                                        _objCell.Font.FontFamily = FontFamilies.Roman;

        //                                        for (int n = 0; n < Customers.Count; n++)
        //                                        {
        //                                            DataRow dr = (DataRow)Customers[n];
        //                                            cells.Add(CustomersRowNo, 1, (n + 1).ToString());
        //                                            cells.Add(CustomersRowNo, 2, dr["GlobalAccountNumber"].ToString());
        //                                            cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
        //                                            cells.Add(CustomersRowNo, 4, dr["Name"].ToString());
        //                                            cells.Add(CustomersRowNo, 5, dr["ServiceAddress"]);
        //                                            cells.Add(CustomersRowNo, 6, dr["ClassName"]);
        //                                            cells.Add(CustomersRowNo, 7, dr["BookNo"]);
        //                                            cells.Add(CustomersRowNo, 8, dr["ConnectionDate"]);

        //                                            CustomersRowNo++;
        //                                        }
        //                                        DetailsRowNo = CustomersRowNo + 2;
        //                                        CustomersRowNo = CustomersRowNo + 6;
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                }

        //                string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
        //                fileName = "CustomersWithoutBookGroupRerpot_" + fileName;
        //                string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
        //                xls.FileName = filePath;
        //                xls.Save();
        //                _objIdsignBAL.DownLoadFile(filePath, "application//vnd.ms-excel");
        //            }
        //            else
        //                Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
        //        }
        //        else
        //            Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        #endregion

        //protected void ddlBU_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ddlBU.SelectedIndex > 0)
        //        {
        //            _objCommonMethods.BindUserServiceUnits_BuIds(ddlSU, BusinessUnitName, UMS_Resource.DROPDOWN_SELECT, false);
        //            ddlSU.Enabled = true;
        //        }
        //        else
        //        {
        //            ddlSU.SelectedIndex = ddlSC.SelectedIndex = Constants.Zero;
        //            ddlSU.Enabled = ddlSC.Enabled = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void ddlSU_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (ddlSU.SelectedIndex > 0)
        //        {
        //            _objCommonMethods.BindUserServiceCenters_SuIds(ddlSC, ServiceUnitName, UMS_Resource.DROPDOWN_SELECT, false);
        //            ddlSC.Enabled = true;
        //        }
        //        else
        //        {
        //            ddlSC.SelectedIndex = Constants.Zero;
        //            ddlSC.Enabled = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
    }
}