﻿<%@ Page Title="::Accounts On Continuous Estimation::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="AccountsOnContinuousEstimations.aspx.cs"
    Inherits="UMS_Nigeria.Reports.AccountsOnContinuousEstimations" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upAddCycle" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddCycle" runat="server" Text="<%$ Resources:Resource, ACCOUNTS_ON_CONTINUOUS_ESTIMATION%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, MONTH%>"></asp:Literal>
                                    <span class="span_star">*</span></label><br />
                                <asp:DropDownList ID="ddlMonth" runat="server" CssClass="text-box select_box">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <span id="spanddlMonth" class="spancolor"></span>
                            </div>
                            <div class="text-inner">
                                <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal><br />
                                <asp:DropDownList ID="ddlBusinessUnitName" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlBusinessUnitName_SelectedIndexChanged"
                                    CssClass="text-box select_box">
                                    <asp:ListItem Value="" Text="ALL"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, YEAR%>"></asp:Literal>
                                    <span class="span_star">*</span></label><br />
                                <asp:DropDownList ID="ddlYear" runat="server" CssClass="text-box select_box">
                                    <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                                </asp:DropDownList>
                                <span id="spanddlYear" class="spancolor"></span>
                            </div>
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                    <div style="display: none;">
                        <div class="consume_name">
                            <asp:Literal ID="litServiceUnit" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal>
                        </div>
                        <div class="consume_input">
                            <asp:DropDownList ID="ddlServiceUnitName" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlServiceUnitName_SelectedIndexChanged">
                                <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="litServiceCenter" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal>
                        </div>
                        <div class="consume_input">
                            <asp:DropDownList ID="ddlServiceCenterName" runat="server">
                                <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="litCycles" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal>
                        </div>
                        <div class="consume_input">
                            <asp:DropDownList ID="ddlCycle" runat="server">
                                <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="litSubStationName" runat="server" Text="<%$ Resources:Resource, SUB_STATION_NAME%>"></asp:Literal>
                        </div>
                        <div class="consume_input">
                            <asp:DropDownList ID="ddlSubStation" AutoPostBack="true" OnSelectedIndexChanged="ddlSubStation_SelectedIndexChanged"
                                runat="server">
                                <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="litFeederName" runat="server" Text="<%$ Resources:Resource, FEEDER_NAME%>"></asp:Literal>
                        </div>
                        <div class="consume_input">
                            <asp:DropDownList ID="ddlFeeder" AutoPostBack="true" OnSelectedIndexChanged="ddlFeeder_SelectedIndexChanged"
                                runat="server">
                                <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="litTransformerName" runat="server" Text="<%$ Resources:Resource, TRANSFORMER_NAME%>"></asp:Literal>
                        </div>
                        <div class="consume_input">
                            <asp:DropDownList ID="ddlTransformer" AutoPostBack="true" OnSelectedIndexChanged="ddlTransformer_SelectedIndexChanged"
                                runat="server">
                                <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, POLE_NAME%>"></asp:Literal>
                        </div>
                        <div class="consume_input">
                            <asp:DropDownList ID="ddlPole" runat="server">
                                <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="consume_name">
                            <asp:Literal ID="litReadCode" runat="server" Text="<%$ Resources:Resource, READCODE%>"></asp:Literal></div>
                        <div class="consume_input">
                            <asp:DropDownList ID="ddlReadCode" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlReadCode_SelectedIndexChanged">
                                <asp:ListItem Text="Select" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="clear pad_5">
                    </div>
                    <div id="divServiceUnits" runat="server" visible="false">
                        <div class="inner-box">
                            <div class="check_baa">
                                <div class="text-inner_a">
                                    <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" ID="ChkSUAll" Text="All" class="text-inner-b" AutoPostBack="true" />
                                        <asp:CheckBoxList ID="cblServiceUnits" class="text-inner-b" AutoPostBack="true" RepeatDirection="Horizontal"
                                            RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceUnits_SelectedIndexChanged">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div id="divServiceCenters" runat="server" visible="false">
                            <div class="inner-box">
                                <div class="check_baa">
                                    <div class="text-inner_a">
                                        <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal>
                                    </div>
                                    <div class="check_ba">
                                        <div class="text-inner mster-input">
                                            <asp:CheckBox runat="server" ID="chkSCAll" Text="All" class="text-inner-b" AutoPostBack="true" />
                                            <asp:CheckBoxList ID="cblServiceCenters" class="text-inner-b" RepeatDirection="Horizontal"
                                                AutoPostBack="true" RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceCenters_SelectedIndexChanged">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear pad_5">
                            </div>
                            <div id="divCycles" runat="server" visible="false">
                                <div class="inner-box">
                                    <div class="check_baa">
                                        <div class="text-inner_a">
                                            <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal>
                                        </div>
                                        <div class="check_ba">
                                            <div class="text-inner mster-input">
                                                <asp:CheckBox runat="server" ID="chkCycleAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                                <asp:CheckBoxList ID="cblCycles" class="text-inner-b" RepeatDirection="Horizontal"
                                                    RepeatColumns="3" runat="server">
                                                </asp:CheckBoxList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear pad_5">
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <asp:Button ID="btnSearch" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SEARCH%>"
                                CssClass="box_s" OnClick="btnSearch_Click" runat="server" />
                        </div>
                    </div>
                    <div class="grid_boxes">
                        <div class="grid_paging_top">
                            <div class="paging_top_title">
                            </div>
                            <div class="paging_top_right_content" id="divPaging1" runat="server" visible="false">
                                <asp:Button ID="btnExportExcel" runat="server" CssClass="excel" OnClick="btnExportExcel_Click"
                                    Text="Export to excel" />
                                <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvEstimations" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                            OnRowDataBound="gvEstimations_RowDataBound">
                            <EmptyDataRowStyle HorizontalAlign="Left" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <%--<asp:BoundField HeaderText="<%$ Resources:Resource, GRID_SNO%>" DataField="RowNumber" />--%>
                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, GRID_SNO%>"
                                    runat="server">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" DataField="AccountNo" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, NAME%>" DataField="Name" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, ContinusEstimationsCount%>" DataField="ContinusEstimations" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, AVERAGE_READING%>" DataField="AverageReading" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, BILL_STATUS%>" DataField="BillStatus" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, SERVICE_ADDRESS%>" DataField="ServiceAddress" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, CYCLE%>" DataField="CycleName" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, READCODE%>" DataField="ReadCode" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, MONTH_YEAR%>"
                                    DataField="MonthYear" />
                                <%--<asp:BoundField HeaderText="<%$ Resources:Resource, AMOUNT%>" DataField="Amount" />--%>
                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, BILL_AMOUNT%>"
                                    runat="server">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("Amount")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle HorizontalAlign="center"></RowStyle>
                        </asp:GridView>
                        <asp:HiddenField ID="hfPageSize" runat="server" />
                        <asp:HiddenField ID="hfPageNo" runat="server" />
                        <asp:HiddenField ID="hfLastPage" runat="server" />
                        <asp:HiddenField ID="hfTotalRecords" runat="server" />
                    </div>
                    <div class="grid_boxes">
                        <div id="divPaging2" runat="server" visible="false">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                <asp:ModalPopupExtender ID="mpeGenMsg" runat="server" PopupControlID="pnlGenMsg"
                    TargetControlID="hfGenMsg" BackgroundCssClass="modalBackground" CancelControlID="btnOKGenMsg">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="hfGenMsg" runat="server" />
                <asp:Panel ID="pnlGenMsg" runat="server" CssClass="modalPopup" Style="display: none;">
                    <div class="popheader popheaderlblred">
                        <asp:Label ID="lblgridalertmsg" runat="server" Text="No data found for the selection."></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btnOKGenMsg" runat="server" Text="<%$
            Resources:Resource, OK%>" CssClass="ok_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/AccountsOnContinuousEstimations.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
