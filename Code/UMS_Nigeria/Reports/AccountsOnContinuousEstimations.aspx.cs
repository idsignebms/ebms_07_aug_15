﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AccountsOnContinuousEstimations
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 30-May-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Configuration;
using org.in2bits.MyXls;

namespace UMS_Nigeria.Reports
{
    public partial class AccountsOnContinuousEstimations : System.Web.UI.Page
    {

        # region Members

        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        MastersBAL objMastersBAL = new MastersBAL();
        ConsumerBal _objConsumerBal = new ConsumerBal();
        public int PageNum;
        XmlDocument xml = null;
        string Key = UMS_Resource.ACCOUNTS_ON_CONTINUOUS_ESTIMATIONS_PAGE;
        DataTable dtForReports = new DataTable();

        #endregion

        #region Properties

        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        public string BusinessUnitName
        {
            get { return string.IsNullOrEmpty(ddlBusinessUnitName.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlBusinessUnitName, ",") : ddlBusinessUnitName.SelectedValue; }
            set { ddlBusinessUnitName.SelectedValue = value.ToString(); }
        }
        public string ServiceUnitName
        {
            get { return string.IsNullOrEmpty(ddlServiceUnitName.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlServiceUnitName, ",") : ddlServiceUnitName.SelectedValue; }
            set { ddlServiceUnitName.SelectedValue = value.ToString(); }
        }
        public string ServiceCenterName
        {
            get { return string.IsNullOrEmpty(ddlServiceCenterName.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlServiceCenterName, ",") : ddlServiceCenterName.SelectedValue; }
            set { ddlServiceCenterName.SelectedValue = value.ToString(); }
        }
        public string SubStation
        {
            get { return string.IsNullOrEmpty(ddlSubStation.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlSubStation, ",") : ddlSubStation.SelectedValue; }
            set { ddlSubStation.SelectedValue = value.ToString(); }
        }
        public string Feeder
        {
            get { return string.IsNullOrEmpty(ddlFeeder.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlFeeder, ",") : ddlFeeder.SelectedValue; }
            set { ddlFeeder.SelectedValue = value.ToString(); }
        }
        public string Transformer
        {
            get { return string.IsNullOrEmpty(ddlTransformer.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlTransformer, ",") : ddlTransformer.SelectedValue; }
            set { ddlTransformer.SelectedValue = value.ToString(); }
        }
        public string Pole
        {
            get { return string.IsNullOrEmpty(ddlPole.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlPole, ",") : ddlPole.SelectedValue; }
            set { ddlPole.SelectedValue = value.ToString(); }
        }
        public string Cycle
        {
            get { return string.IsNullOrEmpty(ddlCycle.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlCycle, ",") : ddlCycle.SelectedValue; }
            set { ddlCycle.SelectedValue = value.ToString(); }
        }
        public string ReadCodeId
        {
            get { return string.IsNullOrEmpty(ddlReadCode.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlReadCode, ",") : ddlReadCode.SelectedValue; }
            set { ddlReadCode.SelectedValue = value.ToString(); }
        }
        public string Year
        {
            get { return string.IsNullOrEmpty(ddlYear.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlYear, ",") : ddlYear.SelectedValue; }
            set { ddlYear.SelectedValue = value.ToString(); }
        }
        public string Month
        {
            get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlMonth, ",") : ddlMonth.SelectedValue; }
            set { ddlMonth.SelectedValue = value.ToString(); }
        }

        public string BU_CB
        {
            get { return string.IsNullOrEmpty(ddlBusinessUnitName.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlBusinessUnitName, ",") : ddlBusinessUnitName.SelectedValue; }
            set { ddlBusinessUnitName.SelectedValue = value.ToString(); }
        }
        public string SU_CB
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceUnits, ","); }
        }
        public string SC_CB
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceCenters, ","); }
        }
        public string Cy_CB
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblCycles, ","); }
        }

        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                pnlMessage.CssClass = lblMessage.Text = string.Empty;
                if (!IsPostBack)
                {
                    //string path = string.Empty;
                    //path = objCommonMethods.GetPagePath(this.Request.Url);
                    //if (objCommonMethods.IsAccess(path))
                    //{

                    BindBusinessUnits();
                    ddlMonth.Items[1].Selected = true;
                    ddlYear.Items[1].Selected = true;
                    // BindDropDowns();
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    hfTotalRecords.Value = Constants.Zero.ToString();
                    //BindGrid();

                    //}
                    //else
                    //{
                    //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    //}
                    CheckBoxesJS();
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }

        protected void gvEstimations_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblAmount = (Label)e.Row.FindControl("lblAmount");
                    lblAmount.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblAmount.Text), 2, Constants.MILLION_Format);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlBusinessUnitName_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Checkbox selection functionality starts.
            try
            {
                _objCommonMethods.BindUserServiceUnits_BuIds(cblServiceUnits, BU_CB, Resource.DDL_ALL, false);
                if (cblServiceUnits.Items.Count > 0)
                    divServiceUnits.Visible = true;
                else
                    divServiceUnits.Visible = false;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            //Checkbox selection functionality ends.

            //Dropdown selection functionality starts.
            //try
            //{
            //    //if (ddlBusinessUnitName.SelectedIndex > 0)
            //    //{
            //    _objCommonMethods.BindUserServiceUnits_BuIds(ddlServiceUnitName, BusinessUnitName, Resource.DDL_ALL, false);
            //    //    ddlServiceCenterName.SelectedIndex = Constants.Zero;
            //    //    ddlServiceUnitName.Enabled = true;
            //    //    ddlServiceCenterName.Enabled = false;
            //    //}
            //    //else
            //    //{
            //    //    ddlServiceUnitName.SelectedIndex = ddlServiceCenterName.SelectedIndex = Constants.Zero;
            //    //    ddlServiceUnitName.Enabled = ddlServiceCenterName.Enabled = false;
            //    //}
            //}
            //catch (Exception ex)
            //{
            //    Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
            //    try
            //    {
            //        _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            //    }
            //    catch (Exception logex)
            //    {

            //    }
            //}
            //Dropdown selection functionality end.
        }
        protected void ddlServiceUnitName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (ddlServiceUnitName.SelectedIndex > 0)
                //{
                _objCommonMethods.BindUserServiceCenters_SuIds(ddlServiceCenterName, ServiceUnitName, Resource.DDL_ALL, false);
                //    ddlServiceCenterName.Enabled = true;
                //}
                //else
                //{
                //    ddlServiceCenterName.SelectedIndex = Constants.Zero;
                //    ddlServiceCenterName.Enabled = false;
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlSubStation_SelectedIndexChanged(object sender, EventArgs e)
        {
            _objCommonMethods.BindFeeders(ddlFeeder, this.SubStation, Resource.DDL_ALL, true);
            ddlFeeder.Items.Insert(0, new ListItem("ALL", ""));
        }
        protected void ddlFeeder_SelectedIndexChanged(object sender, EventArgs e)
        {
            _objCommonMethods.BindTransformers(ddlTransformer, this.Feeder, false);
            ddlTransformer.Items.Insert(0, new ListItem("ALL", ""));
        }
        protected void ddlTransformer_SelectedIndexChanged(object sender, EventArgs e)
        {
            _objCommonMethods.BindPoles(ddlPole, this.Transformer, false);
            ddlPole.Items.Insert(0, new ListItem("ALL", ""));
        }
        protected void ddlReadCode_SelectedIndexChanged(object sender, EventArgs e) { }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindGrid();
            #region Temp downloading file in search button later on grid will bind and download link will uncommented.
            #endregion
        }


        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                ConsumerBe objConsumerBe = new ConsumerBe();
                objConsumerBe.BU_ID = this.BusinessUnitName;
                objConsumerBe.SU_ID = this.SU_CB;
                objConsumerBe.ServiceCenterId = this.SC_CB;
                objConsumerBe.CycleId = this.Cy_CB;
                objConsumerBe.SubStationId = this.SubStation;
                objConsumerBe.FeederId = this.Feeder;
                objConsumerBe.TransFormerId = this.Transformer;
                objConsumerBe.PoleId = this.Pole;
                objConsumerBe.ReadCode = this.ReadCodeId;
                objConsumerBe.Year = Convert.ToInt32(ddlYear.SelectedItem.Value == "" ? "0" : ddlYear.SelectedItem.Value);
                objConsumerBe.Month = Convert.ToInt32(ddlMonth.SelectedItem.Value == "" ? "0" : ddlMonth.SelectedItem.Value);
                objConsumerBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                objConsumerBe.PageSize = Convert.ToInt32(hfTotalRecords.Value);
                ConsumerListBe objConsumerList = _ObjiDsignBAL.DeserializeFromXml<ConsumerListBe>(_objConsumerBal.Retrieve(objConsumerBe, ReturnType.Group));

                if (objConsumerList.items.Count > 0)
                {
                    DataSet ds = _ObjiDsignBAL.ConvertListToDataSet<ConsumerBe>(objConsumerList.items);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        var BUlist = (from list in ds.Tables[0].AsEnumerable()
                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                      group list by list.Field<string>("BusinessUnitName") into g
                                      select new { BusinessUnit = g.Key }).ToList();

                        var SUlist = (from list in ds.Tables[0].AsEnumerable()
                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                      group list by list.Field<string>("ServiceUnitName") into g
                                      select new { ServiceUnit = g.Key }).ToList();

                        var SClist = (from list in ds.Tables[0].AsEnumerable()
                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                      group list by list.Field<string>("ServiceCenterName") into g
                                      select new { ServiceCenter = g.Key }).ToList();

                        var Cyclelist = (from list in ds.Tables[0].AsEnumerable()
                                         orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                         orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                         group list by list.Field<string>("CycleName") into g
                                         select new { Cycle = g.Key }).ToList();

                        var Booklist = (from list in ds.Tables[0].AsEnumerable()
                                        orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                        orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                        group list by list.Field<string>("BookNumber") into g
                                        select new { Book = g.Key }).ToList();

                        if (BUlist.Count() > 0)
                        {
                            XlsDocument xls = new XlsDocument();
                            Worksheet mainSheet = xls.Workbook.Worksheets.Add("Summary");
                            Cells mainCells = mainSheet.Cells;
                            Cell _objMainCells = null;

                            _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
                            mainCells.Add(4, 2, "Report Code ");
                            mainCells.Add(4, 3, Constants.AccountsonContinuousEstimationReport_Code).Font.Bold = true;
                            mainCells.Add(4, 4, "Report Name");
                            mainCells.Add(4, 5, Constants.AccountsonContinuousEstimationReport_Name).Font.Bold = true;
                            mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
                            _objMainCells.Font.Weight = FontWeight.ExtraBold;
                            _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objMainCells.Font.FontFamily = FontFamilies.Roman;

                            int MainSheetDetailsRowNo = 6;
                            _objMainCells = mainCells.Add(MainSheetDetailsRowNo, 1, "S.No.");
                            _objMainCells.Font.Bold = true;
                            _objMainCells.UseBorder = true;
                            _objMainCells = mainCells.Add(MainSheetDetailsRowNo, 2, "Service Center");
                            _objMainCells.Font.Bold = true;
                            _objMainCells.UseBorder = true;
                            _objMainCells = mainCells.Add(MainSheetDetailsRowNo, 3, "Customer Count");
                            _objMainCells.Font.Bold = true;
                            _objMainCells.UseBorder = true;

                            for (int k = 0; k < SClist.Count; k++)//SC
                            {
                                var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                 where list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                                 select list).ToList();

                                MainSheetDetailsRowNo++;
                                mainCells.Add(MainSheetDetailsRowNo, 1, k + 1).UseBorder = true;
                                mainCells.Add(MainSheetDetailsRowNo, 2, SClist[k].ServiceCenter).UseBorder = true;
                                mainCells.Add(MainSheetDetailsRowNo, 3, Customers.Count).UseBorder = true;
                            }

                            for (int i = 0; i < BUlist.Count; i++)//BU
                            {
                                for (int j = 0; j < SUlist.Count; j++)//SU
                                {
                                    var SClistBySU = (from list in ds.Tables[0].AsEnumerable()
                                                      where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                      && list.Field<string>("ServiceUnitName") == SUlist[j].ServiceUnit
                                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                      group list by list.Field<string>("ServiceCenterName") into g
                                                      select new { ServiceCenter = g.Key }).ToList();

                                    for (int k = 0; k < SClistBySU.Count; k++)//SC
                                    {
                                        int DetailsRowNo = 2;
                                        int CustomersRowNo = 6;

                                        Worksheet sheet = xls.Workbook.Worksheets.Add(SClistBySU[k].ServiceCenter);
                                        Cells cells = sheet.Cells;
                                        Cell _objCell = null;

                                        for (int l = 0; l < Cyclelist.Count; l++)//Cycle
                                        {
                                            for (int m = 0; m < Booklist.Count; m++)//Book
                                            {
                                                var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                                 where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                 && list.Field<string>("ServiceUnitName") == SUlist[j].ServiceUnit
                                                                 && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                 && list.Field<string>("CycleName") == Cyclelist[l].Cycle
                                                                 && list.Field<string>("BookNumber") == Booklist[m].Book
                                                                 orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                 orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                 orderby Convert.ToString(list["CycleName"]) ascending
                                                                 select list).ToList();

                                                if (Customers.Count() > 0)
                                                {

                                                    _objCell = cells.Add(DetailsRowNo, 1, "Business Unit");
                                                    cells.Add(DetailsRowNo, 3, "Service Unit");
                                                    cells.Add(DetailsRowNo, 5, "Service Center");
                                                    cells.Add(DetailsRowNo + 1, 1, "Book Group");
                                                    cells.Add(DetailsRowNo + 1, 3, "Book Code");

                                                    cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 4, SUlist[j].ServiceUnit).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 6, SClistBySU[k].ServiceCenter).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 2, Cyclelist[l].Cycle).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 4, Booklist[m].Book).Font.Bold = true;

                                                    _objCell = cells.Add(DetailsRowNo + 3, 1, "Sno");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.UseBorder = true;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 2, "Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.UseBorder = true;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 3, "Name");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.UseBorder = true;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 4, "Continus Estimations Count");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.UseBorder = true;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 5, "Average Reading");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.UseBorder = true;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 6, "Bill Status");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.UseBorder = true;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 7, "Service Address");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.UseBorder = true;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 8, "Book Group");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.UseBorder = true;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 9, "Read Code");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.UseBorder = true;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 10, "Month & Year");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.UseBorder = true;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 11, "Bill Amount");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.UseBorder = true;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                   
                                                    for (int n = 0; n < Customers.Count; n++)
                                                    {
                                                        DataRow dr = (DataRow)Customers[n];
                                                        cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                                                        cells.Add(CustomersRowNo, 2, dr["AccountNo"].ToString()).UseBorder = true;
                                                        cells.Add(CustomersRowNo, 3, dr["Name"].ToString()).UseBorder = true;
                                                        cells.Add(CustomersRowNo, 4, dr["ContinusEstimations"].ToString()).UseBorder = true;
                                                        cells.Add(CustomersRowNo, 5, dr["AverageReading"].ToString()).UseBorder = true;
                                                        cells.Add(CustomersRowNo, 6, dr["BillStatus"].ToString()).UseBorder = true;
                                                        cells.Add(CustomersRowNo, 7, dr["ServiceAddress"].ToString()).UseBorder = true;
                                                        cells.Add(CustomersRowNo, 8, dr["CycleName"].ToString()).UseBorder = true;
                                                        cells.Add(CustomersRowNo, 9, dr["ReadCode"].ToString()).UseBorder = true;
                                                        cells.Add(CustomersRowNo, 10, dr["MonthYear"].ToString()).UseBorder = true;
                                                        cells.Add(CustomersRowNo, 11, _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(dr["Amount"].ToString()),2,Constants.MILLION_Format)).UseBorder = true;
                                                        CustomersRowNo++;
                                                    }
                                                    DetailsRowNo = CustomersRowNo + 2;
                                                    CustomersRowNo = CustomersRowNo + 6;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                            fileName = Constants.AccountsonContinuousEstimationReport_Name.Replace(" ", string.Empty) + "_" + fileName;
                            string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                            xls.FileName = filePath;
                            xls.Save();
                            _ObjiDsignBAL.DownLoadFile(filePath, "application//vnd.ms-excel");
                        }
                    }
                    else
                        Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                    Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                // cblBooks.Items.Clear();
                cblServiceCenters.Items.Clear();
                divCycles.Visible = chkCycleAll.Checked = chkSCAll.Checked = divServiceCenters.Visible = false;
                if (!string.IsNullOrEmpty(SU_CB))
                {
                    _objCommonMethods.BindUserServiceCenters_SuIds(cblServiceCenters, SU_CB, Resource.DDL_ALL, false);
                    if (cblServiceCenters.Items.Count > 0)
                        divServiceCenters.Visible = true;
                    else
                        divServiceCenters.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceCenters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                //  cblBooks.Items.Clear();
                divCycles.Visible = chkCycleAll.Checked = false;
                if (!string.IsNullOrEmpty(SC_CB))
                {
                    _objCommonMethods.BindCyclesByBUSUSC(cblCycles, BU_CB, SU_CB, SC_CB, true, string.Empty, false);
                    if (cblCycles.Items.Count > 0)
                        divCycles.Visible = true;
                    else
                        divCycles.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods
        //private void BindDropDowns()
        //{
        //    objCommonMethods.BindBusinessUnits(ddlBusinessUnitName, string.Empty, false);
        //    ddlBusinessUnitName.Items.Insert(0, new ListItem("ALL", ""));
        //    hfPageNo.Value = Constants.pageNoValue.ToString();
        //    objCommonMethods.BindCyclesList(ddlCycle, false);
        //    ddlCycle.Items.Insert(0, new ListItem("ALL", ""));
        //    objCommonMethods.BindInjectionSubStations(ddlSubStation, false);
        //    ddlSubStation.Items.Insert(0, new ListItem("ALL", ""));
        //    objCommonMethods.BindReadCodes(ddlReadCode, false);
        //    ddlReadCode.Items.Insert(0, new ListItem("ALL", ""));
        //    BindYears();
        //    BindMonths();
        //}

        private void BindBusinessUnits()
        {
            _objCommonMethods.BindUserBusinessUnits(ddlBusinessUnitName, string.Empty, false, Resource.DDL_ALL);
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
            {
                string BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                ddlBusinessUnitName.SelectedIndex = ddlBusinessUnitName.Items.IndexOf(ddlBusinessUnitName.Items.FindByValue(BUID));
                ddlBusinessUnitName.Enabled = false;
                ddlBusinessUnitName_SelectedIndexChanged(ddlBusinessUnitName, new EventArgs());
                ddlServiceUnitName_SelectedIndexChanged(ddlServiceUnitName, new EventArgs());
                _objCommonMethods.BindCyclesByBUSUSC(ddlCycle, BUID, string.Empty, string.Empty, false, "ALL", false);
            }
            else
            {
                _objCommonMethods.BindCyclesList(ddlCycle, false);
                ddlCycle.Items.Insert(0, new ListItem("ALL", ""));
            }
            //_objCommonMethods.BindUserServiceUnits_BuIds(ddlServiceUnitName, BusinessUnitName, Resource.DDL_ALL, false);
            //_objCommonMethods.BindUserServiceCenters_SuIds(ddlServiceCenterName, ServiceUnitName, Resource.DDL_ALL, false);
            //_objCommonMethods.BindInjectionSubStations(ddlSubStation, false);
            //ddlSubStation.Items.Insert(0, new ListItem("ALL", ""));
            //_objCommonMethods.BindFeeders(ddlFeeder, string.Empty, Resource.DDL_ALL, true);
            //_objCommonMethods.BindTransformers(ddlTransformer, string.Empty, false);
            //ddlTransformer.Items.Insert(0, new ListItem("ALL", ""));
            //_objCommonMethods.BindPoles(ddlPole, string.Empty, false);
            //ddlPole.Items.Insert(0, new ListItem("ALL", ""));
            _objCommonMethods.BindReadCodes(ddlReadCode, false);
            ddlReadCode.Items.Insert(0, new ListItem("ALL", ""));
            ddlReadCode.SelectedValue = 2.ToString();
            ddlReadCode.Enabled = false;
            BindYears();
            BindMonths();
        }

        public void BindGrid()
        {
            Session[UMS_Resource.SESSION_REPORTS_DATA] = null;// maintining session, in future gridview display may be required. -- Neeraj ID077
            ConsumerBe objConsumerBe = new ConsumerBe();
            objConsumerBe.BU_ID = this.BusinessUnitName;
            objConsumerBe.SU_ID = this.SU_CB;
            objConsumerBe.ServiceCenterId = this.SC_CB;
            objConsumerBe.CycleId = this.Cy_CB;
            objConsumerBe.SubStationId = this.SubStation;
            objConsumerBe.FeederId = this.Feeder;
            objConsumerBe.TransFormerId = this.Transformer;
            objConsumerBe.PoleId = this.Pole;
            objConsumerBe.ReadCode = this.ReadCodeId;
            objConsumerBe.Year = Convert.ToInt32(ddlYear.SelectedItem.Value == "" ? "0" : ddlYear.SelectedItem.Value);
            objConsumerBe.Month = Convert.ToInt32(ddlMonth.SelectedItem.Value == "" ? "0" : ddlMonth.SelectedItem.Value);
            objConsumerBe.PageNo = Convert.ToInt32(hfPageNo.Value);
            objConsumerBe.PageSize = PageSize;
            ConsumerListBe objConsumerList = _ObjiDsignBAL.DeserializeFromXml<ConsumerListBe>(_objConsumerBal.Retrieve(objConsumerBe, ReturnType.Group));

            if (objConsumerList.items.Count > 0)
            {
                divPaging1.Visible = divPaging1.Visible = true;
                int totalRecord = objConsumerList.items[0].TotalRecords;
                hfTotalRecords.Value = totalRecord.ToString();


                gvEstimations.DataSource = objConsumerList.items;
                gvEstimations.DataBind();
            }
            else
            {
                divgrid.Visible = UCPaging1.Visible = UCPaging2.Visible = false;
                hfTotalRecords.Value = Constants.Zero.ToString();
                Message("Customers Not Found", UMS_Resource.MESSAGETYPE_ERROR);
                divPaging1.Visible = divPaging1.Visible = false;
                gvEstimations.DataSource = new DataTable();
                gvEstimations.DataBind();
            }

        }

        private void BindYears()
        {
            try
            {
                BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
                XmlDocument xmlResult = new XmlDocument();
                xmlResult = _objBillGenerationBal.GetDetails(ReturnType.Bulk);
                BillGenerationListBe objBillsListBE = _ObjiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                //IEnumerable<BillGenerationBe> YearLisxt = objBillsListBE.Items.OrderBy(o => o.Year);
                _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlYear, "Year", "Year", "--Select--", true);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindMonths()
        {
            try
            {
                DataSet dsMonths = new DataSet();
                dsMonths.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["Months"]));
                _ObjiDsignBAL.FillDropDownList(dsMonths.Tables[0], ddlMonth, "name", "value", false);
                ddlMonth.Items.Insert(0, new ListItem("--Select--", ""));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void CreateDtForReport()
        {
            dtForReports.Columns.Add("S.No", typeof(string));
            dtForReports.Columns.Add("AccountNo", typeof(string));
            dtForReports.Columns.Add("Name", typeof(string));
            dtForReports.Columns.Add("Service Address", typeof(string));
            dtForReports.Columns.Add("Business Unit", typeof(string));
            dtForReports.Columns.Add("Book Code", typeof(string));
            dtForReports.Columns.Add("Outstanding", typeof(decimal));
            dtForReports.Columns.Add("Continus Estimations", typeof(string));

            dtForReports.Columns.Add("Month Year 1", typeof(string));
            dtForReports.Columns.Add("Energy Units 1", typeof(decimal));
            dtForReports.Columns.Add("Energy Charges 1", typeof(decimal));
            dtForReports.Columns.Add("Tariff Calss 1", typeof(string));

            dtForReports.Columns.Add("Month Year 2", typeof(string));
            dtForReports.Columns.Add("Energy Units 2", typeof(decimal));
            dtForReports.Columns.Add("Energy Charges 2", typeof(decimal));
            dtForReports.Columns.Add("Tariff Calss 2", typeof(string));

            dtForReports.Columns.Add("Month Year 3", typeof(string));
            dtForReports.Columns.Add("Energy Units 3", typeof(decimal));
            dtForReports.Columns.Add("Energy Charges 3", typeof(decimal));
            dtForReports.Columns.Add("Tariff Calss 3", typeof(string));


        }

        public void DownloadReport(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                ExportToExcelSavedReport(dt, "ContinuousEstimation");
            }
            else
                mpeGenMsg.Show();

        }

        private void CheckBoxesJS()
        {
            ChkSUAll.Attributes.Add("onclick", "CheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            chkSCAll.Attributes.Add("onclick", "CheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            chkCycleAll.Attributes.Add("onclick", "CheckAll('" + chkCycleAll.ClientID + "','" + cblCycles.ClientID + "')");

            cblServiceUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            cblServiceCenters.Attributes.Add("onclick", "UnCheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            cblCycles.Attributes.Add("onclick", "UnCheckAll('" + chkCycleAll.ClientID + "','" + cblCycles.ClientID + "')");
        }

        public void ExportToExcelSavedReport(DataTable Data, string FileName)
        {

            XlsDocument xls = new org.in2bits.MyXls.XlsDocument();
            Worksheet sheet = xls.Workbook.Worksheets.Add(UMS_Resource.SHEET);
            //state the name of the column headings 
            Cells cells = sheet.Cells;
            Cell _objCell = null;

            _objCell = cells.Add(1, 6, Resource.BEDC + "\n" + "Continuous Estimations Report On " + DateTime.Today.ToShortDateString());
            sheet.AddMergeArea(new MergeArea(1, 2, 6, 14));
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            _objCell.Font.FontFamily = FontFamilies.Roman;

            int columnPosition = 1, rowPosition = 4;

            string data = string.Empty;
            foreach (DataColumn column in Data.Columns)
            {
                sheet.Cells.Add(4, columnPosition, column.ColumnName);
                _objCell = cells.Add(4, columnPosition, column.ColumnName);
                _objCell.Font.Weight = FontWeight.ExtraBold;
                _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                _objCell.Font.FontFamily = FontFamilies.Roman;
                columnPosition++;
            }

            foreach (DataRow row in Data.Rows)
            {
                rowPosition++;
                int col = 1;
                foreach (DataColumn column in Data.Columns)
                {
                    data = row[column.ColumnName].ToString();
                    if (data.Trim() == "0")
                        data = "";
                    cells.Add(rowPosition, col, data);
                    col++;
                }
            }

            string filePath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + FileName + "_" + DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";

            if ((System.IO.File.Exists(filePath)))
            {
                System.IO.File.Delete(filePath);
            }

            //set up a filestream
            xls.FileName = filePath;

            //xls.FileName = Server.MapPath("Files/test.xls");
            xls.Save();

            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            _ObjiDsignBAL.DownLoadFile(filePath, "application//vnd.ms-excel");

        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}