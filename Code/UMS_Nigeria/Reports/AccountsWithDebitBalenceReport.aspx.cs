﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for Accounts With Debit Balence
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 05-June-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Configuration;
using org.in2bits.MyXls;

namespace UMS_Nigeria.Reports
{
    public partial class AccountsWithDebitBalenceReport : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        GeneralReportsBal _objGeneralReportsBal = new GeneralReportsBal();
        string Key = UMS_Resource.ACCOUNTS_WITH_DEBIT_BALENCE_REPORT_PAGE;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStartsReport : Convert.ToInt32(hfPageSize.Value); }
        }

        public string BusinessUnitName
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblBusinessUnits, ","); }
        }
        public string SU
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceUnits, ","); }
        }
        public string SC
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceCenters, ","); }
        }
        public string Cycle
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblCycles, ","); }
        }
        public string BookNo
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblBooks, ","); }
        }
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.CssClass = lblMessage.Text = string.Empty;
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                if (!IsPostBack)
                {
                    //string path = string.Empty;
                    //path = _objCommonMethods.GetPagePath(this.Request.Url);
                    //if (_objCommonMethods.IsAccess(path))
                    //{
                    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    {
                        ListItem LiBU = new ListItem(Session[UMS_Resource.SESSION_USER_BUNAME].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());
                        cblBusinessUnits.Items.Add(LiBU);
                        cblBusinessUnits.Items[0].Selected = true;
                        cblBusinessUnits.Enabled = false;
                        ChkBUAll.Visible = false;
                        divServiceUnits.Visible = true;
                        _objCommonMethods.BindUserServiceUnits_BuIds(cblServiceUnits, BusinessUnitName, Resource.DDL_ALL, false);
                    }
                    else
                        _objCommonMethods.BindBusinessUnits(cblBusinessUnits, string.Empty, false);

                    CheckBoxesJS();
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    hfTotalRecords.Value = Constants.Zero.ToString();
                    //}
                    //else
                    //{
                    //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    //}
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindGrid();
            UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            UCPaging2.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));            
        }

        protected void cblBusinessUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                cblBooks.Items.Clear();
                cblServiceCenters.Items.Clear();
                cblServiceUnits.Items.Clear();
                chkCycleAll.Checked = chkBooksAll.Checked = chkSCAll.Checked = ChkSUAll.Checked = ChkBUAll.Checked = false;

                divServiceUnits.Visible = divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = chkSCAll.Checked = divServiceCenters.Visible = false;
                if (!string.IsNullOrEmpty(BusinessUnitName))
                {
                    _objCommonMethods.BindUserServiceUnits_BuIds(cblServiceUnits, BusinessUnitName, Resource.DDL_ALL, false);
                    if (cblServiceUnits.Items.Count > 0)
                        divServiceUnits.Visible = true;
                    else
                        divServiceUnits.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                cblBooks.Items.Clear();
                cblServiceCenters.Items.Clear();
                divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = chkSCAll.Checked = divServiceCenters.Visible = false;
                if (!string.IsNullOrEmpty(SU))
                {
                    _objCommonMethods.BindUserServiceCenters_SuIds(cblServiceCenters, SU, Resource.DDL_ALL, false);
                    if (cblServiceCenters.Items.Count > 0)
                        divServiceCenters.Visible = true;
                    else
                        divServiceCenters.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceCenters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                cblBooks.Items.Clear();
                divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = false;
                if (!string.IsNullOrEmpty(SC))
                {
                    _objCommonMethods.BindCyclesByBUSUSC(cblCycles, BusinessUnitName, SU, SC, true, string.Empty, false);
                    if (cblCycles.Items.Count > 0)
                        divCycles.Visible = true;
                    else
                        divCycles.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblCycles_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblBooks.Items.Clear();
                chkBooksAll.Checked = divBookNos.Visible = false;
                if (!string.IsNullOrEmpty(Cycle))
                {
                    _objCommonMethods.BindAllBookNumbers(cblBooks, Cycle, true, false, string.Empty);
                    if (cblBooks.Items.Count > 0)
                        divBookNos.Visible = true;
                    else
                        divBookNos.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods
        public void BindGrid()
        {
            RptDebitBe _objRptDebitBe = new RptDebitBe();
            _objRptDebitBe.BU_ID = this.BusinessUnitName;
            _objRptDebitBe.SU_ID = this.SU;
            _objRptDebitBe.SC_ID = this.SC;
            _objRptDebitBe.CycleId = this.Cycle;
            _objRptDebitBe.BookNo = this.BookNo;
            _objRptDebitBe.MinAmt = string.IsNullOrEmpty(txtFrom.Text.Replace(",", "")) ? "" : txtFrom.Text.Replace(",", "");
            _objRptDebitBe.MaxAmt = string.IsNullOrEmpty(txtTo.Text.Replace(",", "")) ? "" : txtTo.Text.Replace(",", "");
            _objRptDebitBe.PageNo = Convert.ToInt32(hfPageNo.Value);
            _objRptDebitBe.PageSize = PageSize;

            DataSet dsDebit = new DataSet();
            dsDebit = _objGeneralReportsBal.GetDebitReport(_objRptDebitBe, ReturnType.Get);

            if (dsDebit != null && dsDebit.Tables.Count > 0 && dsDebit.Tables[0].Rows.Count > 0)
            {
                divToprptrContent.Visible = divExport.Visible = divDownrptrContent.Visible = rptrheaders.Visible = divrptrdetails.Visible = true;
                divNoDataFound.Visible = false;
                rptrDebitBalence.DataSource = dsDebit.Tables[0];
                rptrDebitBalence.DataBind();
                lblTopTotalCustomers.Text = lblDownTotalCustomers.Text = hfTotalRecords.Value = dsDebit.Tables[0].Rows[0]["TotalRecords"].ToString();
                lblTopTotalBalence.Text = lblDownTotalBalence.Text = dsDebit.Tables[0].Rows[0]["DueAmount"].ToString();
            }
            else
            {
                divToprptrContent.Visible = divExport.Visible = divDownrptrContent.Visible = rptrheaders.Visible = false;
                divNoDataFound.Visible = divrptrdetails.Visible = true;
                rptrDebitBalence.DataSource = new DataTable(); ;
                rptrDebitBalence.DataBind();
                lblTopTotalCustomers.Text = lblDownTotalCustomers.Text = hfTotalRecords.Value = Constants.Zero.ToString();
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void CheckBoxesJS()
        {
            ChkBUAll.Attributes.Add("onclick", "CheckAll('" + ChkBUAll.ClientID + "','" + cblBusinessUnits.ClientID + "')");
            ChkSUAll.Attributes.Add("onclick", "CheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            chkSCAll.Attributes.Add("onclick", "CheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            chkCycleAll.Attributes.Add("onclick", "CheckAll('" + chkCycleAll.ClientID + "','" + cblCycles.ClientID + "')");
            chkBooksAll.Attributes.Add("onclick", "CheckAll('" + chkBooksAll.ClientID + "','" + cblBooks.ClientID + "')");

            cblBusinessUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkBUAll.ClientID + "','" + cblBusinessUnits.ClientID + "')");
            cblServiceUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            cblServiceCenters.Attributes.Add("onclick", "UnCheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            cblCycles.Attributes.Add("onclick", "UnCheckAll('" + chkCycleAll.ClientID + "','" + cblCycles.ClientID + "')");
            cblBooks.Attributes.Add("onclick", "UnCheckAll('" + chkBooksAll.ClientID + "','" + cblBooks.ClientID + "')");
        }
        #endregion

        #region ExportExcel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                RptDebitBe _objRptDebitBe = new RptDebitBe();
                _objRptDebitBe.BU_ID = this.BusinessUnitName;
                _objRptDebitBe.SU_ID = this.SU;
                _objRptDebitBe.SC_ID = this.SC;
                _objRptDebitBe.CycleId = this.Cycle;
                _objRptDebitBe.BookNo = this.BookNo;
                _objRptDebitBe.MinAmt = string.IsNullOrEmpty(txtFrom.Text) ? "" : txtFrom.Text;
                _objRptDebitBe.MaxAmt = string.IsNullOrEmpty(txtTo.Text) ? "" : txtTo.Text;
                _objRptDebitBe.PageNo = Constants.pageNoValue;
                _objRptDebitBe.PageSize = Convert.ToInt32(hfTotalRecords.Value);

                DataSet dsDebit = new DataSet();
                dsDebit = _objGeneralReportsBal.GetDebitReport(_objRptDebitBe, ReturnType.Get);

                if (dsDebit.Tables[0].Rows.Count > 0)
                {
                    var BUlist = (from list in dsDebit.Tables[0].AsEnumerable()
                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                  orderby list["CycleName"] ascending
                                  group list by list.Field<string>("BusinessUnitName") into g
                                  select new { BusinessUnit = g.Key }).ToList();

                    if (BUlist.Count() > 0)
                    {
                        XlsDocument xls = new XlsDocument();
                        Worksheet mainSheet = xls.Workbook.Worksheets.Add("Summary");
                        Cells mainCells = mainSheet.Cells;
                        Cell _objMainCells = null;

                        _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
                        mainCells.Add(4, 2, "Report Code ");
                        mainCells.Add(4, 3, Constants.AccountsWithDebitBalanceReport_Code).Font.Bold = true;
                        mainCells.Add(4, 4, "Report Name");
                        mainCells.Add(4, 5, Constants.AccountsWithDebitBalanceReport_Name).Font.Bold = true;
                        mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
                        _objMainCells.Font.Weight = FontWeight.ExtraBold;
                        _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                        _objMainCells.Font.FontFamily = FontFamilies.Roman;

                        int MainSheetDetailsRowNo = 6;
                        _objMainCells = mainCells.Add(MainSheetDetailsRowNo, 1, "S.No.");
                        _objMainCells.Font.Bold = true;
                        _objMainCells.UseBorder = true;
                        _objMainCells = mainCells.Add(MainSheetDetailsRowNo, 2, "Service Center");
                        _objMainCells.Font.Bold = true;
                        _objMainCells.UseBorder = true;
                        _objMainCells = mainCells.Add(MainSheetDetailsRowNo, 3, "Customer Count");
                        _objMainCells.Font.Bold = true;
                        _objMainCells.UseBorder = true;
                        _objMainCells = mainCells.Add(MainSheetDetailsRowNo, 4, "Total Due Amount");
                        _objMainCells.Font.Bold = true;
                        _objMainCells.UseBorder = true;

                        decimal TotalDueAmount = 0;

                        var SClist = (from list in dsDebit.Tables[0].AsEnumerable()
                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                      orderby list["CycleName"] ascending
                                      group list by list.Field<string>("ServiceCenterName") into g
                                      select new { ServiceCenter = g.Key }).ToList();

                        for (int k = 0; k < SClist.Count; k++)//SC
                        {
                            var Customers = (from list in dsDebit.Tables[0].AsEnumerable()
                                             where list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                             select list).ToList();

                            TotalDueAmount = Customers.Sum(x => Convert.ToDecimal(x["TotalDueAmount"]));
                            string DueAmount = _objCommonMethods.GetCurrencyFormat(TotalDueAmount, 2, Constants.MILLION_Format);

                            MainSheetDetailsRowNo++;
                            mainCells.Add(MainSheetDetailsRowNo, 1, k + 1).UseBorder = true;
                            mainCells.Add(MainSheetDetailsRowNo, 2, SClist[k].ServiceCenter).UseBorder = true;
                            mainCells.Add(MainSheetDetailsRowNo, 3, Customers.Count).UseBorder = true;
                            mainCells.Add(MainSheetDetailsRowNo, 4, DueAmount).UseBorder = true;
                        }

                        for (int i = 0; i < BUlist.Count; i++)//BU
                        {
                            var SUlistByBU = (from list in dsDebit.Tables[0].AsEnumerable()
                                              where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                              orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                              orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                              group list by list.Field<string>("ServiceUnitName") into g
                                              select new { ServiceUnitName = g.Key }).ToList();

                            for (int j = 0; j < SUlistByBU.Count; j++)//SU
                            {
                                var SClistBySU = (from list in dsDebit.Tables[0].AsEnumerable()
                                                  where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                  && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                  group list by list.Field<string>("ServiceCenterName") into g
                                                  select new { ServiceCenter = g.Key }).ToList();

                                for (int k = 0; k < SClistBySU.Count; k++)//SC
                                {
                                    var CyclelistBySC = (from list in dsDebit.Tables[0].AsEnumerable()
                                                         where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                         && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                         && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                         orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                         orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                         group list by list.Field<string>("CycleName") into g
                                                         select new { CycleName = g.Key }).ToList();

                                    if (CyclelistBySC.Count > 0)
                                    {
                                        decimal finalTotal = 0;
                                        int DetailsRowNo = 2;
                                        int CustomersRowNo = 5;

                                        Worksheet sheet = xls.Workbook.Worksheets.Add(SClistBySU[k].ServiceCenter);
                                        Cells cells = sheet.Cells;
                                        Cell _objCell = null;

                                        for (int l = 0; l < CyclelistBySC.Count; l++)//Cycle
                                        {
                                            var BooklistByCycle = (from list in dsDebit.Tables[0].AsEnumerable()
                                                                   where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                   && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                   && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                   && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                   orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                   orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                   group list by list.Field<string>("BookNumber") into g
                                                                   select new { BookNumber = g.Key }).ToList();

                                            for (int m = 0; m < BooklistByCycle.Count; m++)//Book
                                            {
                                                var Customers = (from list in dsDebit.Tables[0].AsEnumerable()
                                                                 where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                 && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                 && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                 && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                 && list.Field<string>("BookNumber") == BooklistByCycle[m].BookNumber
                                                                 orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                 orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                 orderby Convert.ToString(list["CycleName"]) ascending
                                                                 select list).ToList();

                                                if (Customers.Count() > 0)
                                                {
                                                    cells.Add(DetailsRowNo, 1, "Business Unit");
                                                    cells.Add(DetailsRowNo, 3, "Service Unit");
                                                    cells.Add(DetailsRowNo, 5, "Service Center");
                                                    cells.Add(DetailsRowNo + 1, 1, "Book Group");
                                                    cells.Add(DetailsRowNo + 1, 3, "Book Code");

                                                    cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 4, SUlistByBU[j].ServiceUnitName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 6, SClist[k].ServiceCenter).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 2, CyclelistBySC[l].CycleName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 4, BooklistByCycle[m].BookNumber).Font.Bold = true;

                                                    _objCell = cells.Add(DetailsRowNo + 3, 1, "Sno");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 2, "Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 3, "Old Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 4, "Name");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 5, "Service Address");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 6, "Total Due Amount");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 7, "Last Paid Amount");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 8, "Last PaidDate");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;

                                                    CustomersRowNo++;
                                                    decimal subtotal = 0;
                                                    for (int n = 0; n < Customers.Count; n++)
                                                    {
                                                        DataRow dr = (DataRow)Customers[n];
                                                        cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                                                        cells.Add(CustomersRowNo, 2, dr["GlobalAccountNumber"].ToString());
                                                        cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
                                                        cells.Add(CustomersRowNo, 4, dr["Name"].ToString());
                                                        cells.Add(CustomersRowNo, 5, dr["ServiceAddress"]);
                                                        cells.Add(CustomersRowNo, 6, dr["TotalDueAmount"].ToString());
                                                        cells.Add(CustomersRowNo, 7, dr["LastPaidAmount"].ToString());
                                                        cells.Add(CustomersRowNo, 8, dr["LastPaidDate"].ToString());

                                                        CustomersRowNo++;
                                                        subtotal += Convert.ToDecimal(dr["TotalDueAmount"]);
                                                    }
                                                    _objCell = cells.Add(CustomersRowNo + 1, 5, "Sub Total");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(CustomersRowNo + 1, 6, _objCommonMethods.GetCurrencyFormat(subtotal, 2, Constants.MILLION_Format));
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;

                                                    finalTotal += subtotal;
                                                    DetailsRowNo = CustomersRowNo + 3;
                                                    CustomersRowNo = CustomersRowNo + 6;
                                                }
                                            }
                                        }

                                        _objCell = cells.Add(CustomersRowNo - 3, 5, "Grand Total");
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                        _objCell = cells.Add(CustomersRowNo - 3, 6, _objCommonMethods.GetCurrencyFormat(finalTotal, 2, Constants.MILLION_Format));
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                    }
                                }
                            }
                        }
                        string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                        fileName = "CustomersDebitBalance_" + fileName;
                        string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                        xls.FileName = filePath;
                        xls.Save();
                        _objiDsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                    }
                    else
                        Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                    Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}