﻿<%@ Page Title=":: In Complete Customer Details Report ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="AccountsWithNoNameOrAddress.aspx.cs"
    Inherits="UMS_Nigeria.Reports.AccountsWithNoNameOrAddress" %>

<%@ Register Src="../UserControls/UCPagingV2.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upRptAccWithCreditBalance" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddCycle" runat="server" Text="InComplete Customer Details Report"></asp:Literal>
                            <%--<asp:Literal ID="litAddCycle" runat="server" Text="<%$ Resources:Resource, ACCOUNTS_WITH_NO_NAME_ADD_CUSTOMERS%>"></asp:Literal>--%>
                        </div>
                        <div class="star_text">
                            <%--<asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>--%>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="previous">
                        <a href="ReportsSummary.aspx">< < Back To Summary</a>
                    </div>
                    <div class="inner-box">
                        <div id="divBusinessUnit" runat="server">
                            <div class="check_baa iabody_lblBook">
                                <div class="text-inner_a">
                                    <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" ID="ChkBUAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                        <asp:CheckBoxList ID="cblBusinessUnits" class="text-inner-b" AutoPostBack="true"
                                            RepeatDirection="Horizontal" RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblBusinessUnits_SelectedIndexChanged">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="divServiceUnits" runat="server" visible="false">
                        <div class="check_baa iabody_lblBook">
                            <div class="text-inner_a">
                                <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" ID="ChkSUAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                    <asp:CheckBoxList ID="cblServiceUnits" class="text-inner-b" AutoPostBack="true" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceUnits_SelectedIndexChanged">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div id="divServiceCenters" runat="server" visible="false">
                        <div class="check_baa iabody_lblBook">
                            <div class="text-inner_a">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" ID="chkSCAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                    <asp:CheckBoxList ID="cblServiceCenters" class="text-inner-b" RepeatDirection="Horizontal"
                                        AutoPostBack="true" RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceCenters_SelectedIndexChanged">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div id="divCycles" runat="server" visible="false">
                        <div class="check_baa iabody_lblBook">
                            <div class="text-inner_a">
                                <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" class="text-inner-b" ID="chkCycleAll" Text="All" AutoPostBack="true" />
                                    <asp:CheckBoxList ID="cblCycles" class="text-inner-b" AutoPostBack="true" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblCycles_SelectedIndexChanged">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div id="divBookNos" runat="server" visible="false">
                        <div class="check_baa iabody_lblBook">
                            <div class="text-inner_a">
                                <asp:Literal ID="litBookNo" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" class="text-inner-b" ID="chkBooksAll" Text="All" />
                                    <asp:CheckBoxList ID="cblBooks" class="text-inner-b" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                </div>
                <div style="clear: both;">
                </div>
                <div class="box_total">
                    <div class="box_total_a">
                        <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s"
                            OnClick="btnSearch_Click" runat="server" />
                    </div>
                </div>
                <div class="grid_boxes" id="rptrDetails" runat="server" visible="false">
                    <div class="grid_paging_top">
                        <div class="paging_top_title" style="position: relative; top: 24px;">
                            <asp:Literal ID="Literal3" runat="server" Text="InComplete Customer Details List"></asp:Literal>
                            <%--<asp:Literal ID="litBuList" runat="server" Text="<%$ Resources:Resource, ACCOUNTS_WITH_NO_NAME_ADD_LIST%>"></asp:Literal>--%>
                            
                        </div>
                        <div class="clear">
                        </div>
                        <div id="divToprptrContent" runat="server" visible="false">
                            <div class="consume_name fltl" style="position: relative; top: 30px;">
                                <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, TOTAL_CUSTOMERS%>"></asp:Label>
                            </div>
                            <div class="consume_input fltl" style="margin-right: 10px; padding-right: 10px; position: relative;
                                top: 30px;">
                                <asp:Label ID="lblTopTotalCustomers" runat="server" Text="--"></asp:Label>
                            </div>
                            <div class="paging_top_right_content" id="divTopPaging" runat="server">
                                <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                            </div>
                        </div>
                        <div align="right" runat="server" id="divExport" visible="false">
                            <asp:Button ID="btnExportExcel" OnClick="btnExportExcel_Click" runat="server" CssClass="exporttoexcelButton" /></div>
                    </div>
                    <div class="clr pad_10">
                    </div>
                    <div class="rptr_tb">
                        <table style="border-collapse: collapse; border: 1px #000 solid;">
                            <tr class="rptr_tb_hd" align="center" runat="server" visible="false" id="rptrheader">
                                <th>
                                    <asp:Label ID="lblHSno" runat="server" Text="<%$ Resources:Resource, GRID_SNO%>"></asp:Label>
                                </th>
                                <th>
                                    <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Label>
                                </th>
                                <th>
                                    <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Label>
                                </th>
                                <th>
                                    <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Label>
                                </th>
                                <th>
                                    <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Label>
                                </th>
                                <th>
                                    <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, BU%>"></asp:Label>
                                </th>
                            </tr>
                            <asp:Repeater ID="rptrDebitBalence" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td class="rptr_tb_td" align="center">
                                            <asp:Label runat="server" ID="lblsno" Text='<%#Eval("RowNumber") %>'></asp:Label>
                                        </td>
                                        <td class="rptr_tb_td" align="center">
                                            <asp:Label runat="server" ID="lblAccNo" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </td>
                                        <td class="rptr_tb_td" align="left">
                                            <asp:Label runat="server" ID="lblOldAccountNo" Text='<%#Eval("OldAccountNo") %>'></asp:Label>
                                        </td>
                                        <td class="rptr_tb_td" align="left">
                                            <asp:Label runat="server" ID="lblName" Text='<%#Eval("Name") %>'></asp:Label>
                                        </td>
                                        <td class="rptr_tb_td" align="left">
                                            <asp:Label runat="server" ID="lblAddress" Text='<%#Eval("ServiceAddress") %>'></asp:Label>
                                        </td>
                                        <td class="rptr_tb_td" align="right">
                                            <asp:Label runat="server" ID="lblBU" Text='<%#Eval("BusinessUnitName") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <tr align="center" runat="server" id="divNoDataFound">
                                <td colspan="8">
                                    No data found
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="divDownrptrContent" runat="server" class="reports_txt" visible="false">
                        <div class="consume_name fltl">
                            <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, TOTAL_CUSTOMERS%>"></asp:Label>
                        </div>
                        <div class="consume_input fltl" style="margin-right: 10px; padding-right: 10px; position: relative;">
                            <asp:Label ID="lblDownTotalCustomers" runat="server" Text="--"></asp:Label>
                        </div>
                        <div id="divDownPaging" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                    <asp:HiddenField ID="hfPageSize" runat="server" />
                    <asp:HiddenField ID="hfPageNo" runat="server" />
                    <asp:HiddenField ID="hfLastPage" runat="server" />
                    <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
    <%--==============Escape button script starts==============--%>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/UnBilledCustomersReport.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
