﻿<%@ Page Title=":: Aged Customer Account Receivable Report ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="AgedCustomersAccountReceivableReport.aspx.cs"
    Inherits="UMS_Nigeria.Reports.AgedCustomersAccountReceivableReport" %>

    <%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upRptAccWithCreditBalance" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddCycle" runat="server" Text="Aged Customers Account Receivable Reprot"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal><br />
                                <asp:DropDownList ID="ddlBU" AutoPostBack="true" runat="server" CssClass="text_box select_box"
                                    OnSelectedIndexChanged="ddlBU_SelectedIndexChanged">
                                    <asp:ListItem Value="" Text="ALL"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div id="divSU" runat="server" class="text-inner">
                                <asp:Literal ID="litBookNo" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal><br/>
                                <asp:CheckBox runat="server" ID="cbSUAll" Text="All" AutoPostBack="true" OnSelectedIndexChanged="cblSU_SelectedIndexChanged"
                                    onclick="CheckSUAll();" /><br/>
                                <asp:CheckBoxList ID="cblSU" Width="45%" AutoPostBack="true" RepeatDirection="Horizontal"
                                    onclick="UnCheckSUAll();" RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblSU_SelectedIndexChanged">
                                </asp:CheckBoxList>
                            </div>
                            <div id="divSC" runat="server" visible="False" class="text-inner">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal><br />
                                <asp:CheckBox runat="server" ID="cbSCAll" Text="All" onclick="CheckSCAll();" /><br />
                                <asp:CheckBoxList ID="cblSC" Width="45%" RepeatDirection="Horizontal" onclick="UnCheckSCAll();"
                                    RepeatColumns="3" runat="server">
                                </asp:CheckBoxList>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <asp:Button ID="btnExportExcel" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s"
                                runat="server" OnClick="btnExport_Click" />
                        </div>
                    </div>
                </div>
                <div class="grid_boxes">
                    <div class="grid_paging_top">
                        <div class="paging_top_title">
                        </div>
                        <div class="paging_top_right_content" id="divPaging1" runat="server">
                            <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                        </div>
                        <div align="right" style="padding-right: 45px;" class="marg_10t" runat="server" id="divExport"
                        visible="false">
                        <asp:Button ID="btnPrint" runat="server" OnClientClick="return printGrid()" CssClass="print"
                            Text="Print" />
                        <%--<asp:Button ID="btnExportExcel" runat="server" CssClass="excel" Text="Export to excel"
                        OnClick="btnExportExcel_Click" />--%></div>
                    </div>
                <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvAgedCustomers" runat="server" OnRowDataBound="gvAgedCustomers_RowDataBound"
                            AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color" HeaderStyle-CssClass="grid_tb_hd">
                            <EmptyDataRowStyle HorizontalAlign="Left" CssClass="color"/>
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <%--<asp:BoundField HeaderText="<%$ Resources:Resource, GRID_SNO%>" DataField="RowNumber" />--%>
                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, GRID_SNO%>"
                                    runat="server">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" DataField="AccountNo" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" DataField="OldAccountNo" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, NAME%>" DataField="Name" />
                                <%--<asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="Out Standing Amount" DataField="OutStandingAmount" />--%>
                                <asp:TemplateField HeaderText="Out Standing Amount" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOutStandingAmount" runat="server" Text='<%#Eval("OutStandingAmount") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, BUSINESS_UNIT%>" DataField="BusinessUnitName" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, SERVICE_UNIT%>" DataField="ServiceUnitName" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, SERVICE_CENTER%>" DataField="ServiceCenterName" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="BillGenerated Date In Last 30 Days" DataField="BillGeneratedDate30" />
                                <%-- <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="Total Amount With Tax In Last 30 Days" DataField="TotalBillAmountWithTax30" />--%>
                                <asp:TemplateField HeaderText="Total Amount With Tax In Last 30 Days" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalBillAmountWithTax30" runat="server" Text='<%#Eval("TotalBillAmountWithTax30") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="Total Amount In Last 30 Days" DataField="TotalBillAmountWithArrears30" />--%>
                                <asp:TemplateField HeaderText="Total Amount In Last 30 Days" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalBillAmountWithArrears30" runat="server" Text='<%#Eval("TotalBillAmountWithArrears30") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="BillGenerated Date In Last 60 Days" DataField="BillGeneratedDate60" />
                                <%--<asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="Total Amount With Tax In Last 60 Days" DataField="TotalBillAmountWithTax60" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="Total Amount In Last 60 Days" DataField="TotalBillAmountWithArrears60" />--%>
                                <asp:TemplateField HeaderText="Total Amount With Tax In Last 60 Days" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalBillAmountWithTax60" runat="server" Text='<%#Eval("TotalBillAmountWithTax60") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total Amount In Last 60 Days" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalBillAmountWithArrears60" runat="server" Text='<%#Eval("TotalBillAmountWithArrears60") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="BillGenerated Date In Last 90 Days" DataField="BillGeneratedDate90" />
                                <%--<asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="Total Amount With Tax In Last 90 Days" DataField="TotalBillAmountWithTax90" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="Total Amount In Last 90 Days" DataField="TotalBillAmountWithArrears90" />--%>
                                <asp:TemplateField HeaderText="Total Amount With Tax In Last 90 Days" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalBillAmountWithTax90" runat="server" Text='<%#Eval("TotalBillAmountWithTax90") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total Amount In Last 90 Days" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalBillAmountWithArrears90" runat="server" Text='<%#Eval("TotalBillAmountWithArrears90") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="BillGenerated Date In Last 120 Days" DataField="BillGeneratedDate120" />
                                <%-- <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="Total Amount With Tax In Last 120 Days" DataField="TotalBillAmountWithTax120" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="Total Amount In Last 120 Days" DataField="TotalBillAmountWithArrears120" />--%>
                                <asp:TemplateField HeaderText="Total Amount With Tax In Last 120 Days" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalBillAmountWithTax120" runat="server" Text='<%#Eval("TotalBillAmountWithTax120") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total Amount In Last 120 Days" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalBillAmountWithArrears120" runat="server" Text='<%#Eval("TotalBillAmountWithArrears120") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:BoundField HeaderText="<%$ Resources:Resource, AMOUNT%>" DataField="Amount" />--%>
                            </Columns>
                            <RowStyle HorizontalAlign="center"></RowStyle>
                        </asp:GridView>
                    <asp:HiddenField ID="hfPageSize" runat="server" />
                <asp:HiddenField ID="hfPageNo" runat="server" />
                <asp:HiddenField ID="hfLastPage" runat="server" />
                <asp:HiddenField ID="hfTotalRecords" runat="server" />
                </div>
                <div class="grid_boxes">
                    <div id="divPaging2" runat="server">
                        <div class="grid_paging_bottom">
                            <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
         <%--==============Escape button script starts==============--%>
    <%--<script type="text/javascript">
        $(document).ready(function () {
            Calendar('<%=txtDate.ClientID %>', '<%=imgDate.ClientID %>', "-2:+8");
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            Calendar('<%=txtDate.ClientID %>', '<%=imgDate.ClientID %>', "-2:+8");
        });
    </script>--%>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <%--<script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/AgedCustomersAccountReceivableReport.js"
        type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
