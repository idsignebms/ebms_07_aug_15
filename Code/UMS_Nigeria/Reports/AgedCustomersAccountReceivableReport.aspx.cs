﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AgedCustomersAccountReceivableReport
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 01-Nov-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using Resources;
using System.Xml;
using UMS_NigeriaBE;
using System.Data;
using System.Threading;
using System.Globalization;
using UMS_NigriaDAL;
using UMS_NigeriaBAL;
using System.Configuration;
using org.in2bits.MyXls;

namespace UMS_Nigeria.Reports
{
    public partial class AgedCustomersAccountReceivableReport : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        ReportsBal _objReportsBal = new ReportsBal();
        public int PageNum;
        XmlDocument xml = null;
        string Key = "RptAgedCustsAccReceivable";
        #endregion

        #region Properties
        //public int PageSize
        //{
        //    get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
        //}
        public string BU
        {
            get { return string.IsNullOrEmpty(ddlBU.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlBU, ",") : ddlBU.SelectedValue; }
            set { ddlBU.SelectedValue = value.ToString(); }
        }

        public int PageSize
        {

            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
        }
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.CssClass = lblMessage.Text = string.Empty;
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                if (!IsPostBack)
                {
                    //string path = string.Empty;
                    //path = _objCommonMethods.GetPagePath(this.Request.Url);
                    //if (_objCommonMethods.IsAccess(path))
                    //{
                    BindDropDowns();
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    //BindCheckMetersList();
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //}
                    //else
                    //{
                    //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    //} 

                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    BindGrid();

                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            BindGrid();
        }
        protected void gvAgedCustomers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblOutStandingAmount = (Label)e.Row.FindControl("lblOutStandingAmount");
                    lblOutStandingAmount.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblOutStandingAmount.Text), 2, Constants.MILLION_Format);
                    Label lblTotalBillAmountWithTax30 = (Label)e.Row.FindControl("lblTotalBillAmountWithTax30");
                    lblTotalBillAmountWithTax30.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblTotalBillAmountWithTax30.Text), 2, Constants.MILLION_Format);
                    Label lblTotalBillAmountWithArrears30 = (Label)e.Row.FindControl("lblTotalBillAmountWithArrears30");
                    lblTotalBillAmountWithArrears30.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblTotalBillAmountWithArrears30.Text), 2, Constants.MILLION_Format);
                    Label lblTotalBillAmountWithTax60 = (Label)e.Row.FindControl("lblTotalBillAmountWithTax60");
                    lblTotalBillAmountWithTax60.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblTotalBillAmountWithTax60.Text), 2, Constants.MILLION_Format);
                    Label lblTotalBillAmountWithArrears60 = (Label)e.Row.FindControl("lblTotalBillAmountWithArrears60");
                    lblTotalBillAmountWithArrears60.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblTotalBillAmountWithArrears60.Text), 2, Constants.MILLION_Format);
                    Label lblTotalBillAmountWithTax90 = (Label)e.Row.FindControl("lblTotalBillAmountWithTax90");
                    lblTotalBillAmountWithTax90.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblTotalBillAmountWithTax90.Text), 2, Constants.MILLION_Format);
                    Label lblTotalBillAmountWithArrears90 = (Label)e.Row.FindControl("lblTotalBillAmountWithArrears90");
                    lblTotalBillAmountWithArrears90.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblTotalBillAmountWithArrears90.Text), 2, Constants.MILLION_Format);
                    Label lblTotalBillAmountWithTax120 = (Label)e.Row.FindControl("lblTotalBillAmountWithTax120");
                    lblTotalBillAmountWithTax120.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblTotalBillAmountWithTax120.Text), 2, Constants.MILLION_Format);
                    Label lblTotalBillAmountWithArrears120 = (Label)e.Row.FindControl("lblTotalBillAmountWithArrears120");
                    lblTotalBillAmountWithArrears120.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblTotalBillAmountWithArrears120.Text), 2, Constants.MILLION_Format);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {

                RptAgedCustsBe _objRptAgedCustsBe = new RptAgedCustsBe();
                _objRptAgedCustsBe.BU_ID = this.BU;
                _objRptAgedCustsBe.SU_ID = _objCommonMethods.CollectSelectedItemsValues(cblSU, ",");
                _objRptAgedCustsBe.SC_ID = _objCommonMethods.CollectSelectedItemsValues(cblSC, ",");
                _objRptAgedCustsBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                _objRptAgedCustsBe.PageSize = PageSize;

                DataSet ds = new DataSet();

                ds = _objReportsBal.GetAgedCustsList(_objRptAgedCustsBe);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    var BUlist = (from list in ds.Tables[0].AsEnumerable()
                                  orderby list["CustomerSortOrder"] ascending
                                  orderby list["BookSortOrder"] ascending
                                  //orderby list["CycleName"] ascending
                                  group list by list.Field<string>("BusinessUnitName") into g
                                  select new { BusinessUnit = g.Key }).ToList();

                    var SUlist = (from list in ds.Tables[0].AsEnumerable()
                                  orderby list["CustomerSortOrder"] ascending
                                  orderby list["BookSortOrder"] ascending
                                  //orderby list["CycleName"] ascending
                                  group list by list.Field<string>("ServiceUnitName") into g
                                  select new { ServiceUnit = g.Key }).ToList();

                    var SClist = (from list in ds.Tables[0].AsEnumerable()
                                  orderby list["CustomerSortOrder"] ascending
                                  orderby list["BookSortOrder"] ascending
                                  //orderby list["CycleName"] ascending
                                  group list by list.Field<string>("ServiceCenterName") into g
                                  select new { ServiceCenter = g.Key }).ToList();

                    //var Cyclelist = (from list in ds.Tables[0].AsEnumerable()
                    //                 orderby list["CustomerSortOrder"] ascending
                    //                 orderby list["BookSortOrder"] ascending
                    //                 orderby list["CycleName"] ascending
                    //                 group list by list.Field<string>("CycleName") into g
                    //                 select new { Cycle = g.Key }).ToList();

                    //var Booklist = (from list in ds.Tables[0].AsEnumerable()
                    //                orderby list["CustomerSortOrder"] ascending
                    //                orderby list["BookSortOrder"] ascending
                    //                orderby list["CycleName"] ascending
                    //                group list by list.Field<string>("BookCode") into g
                    //                select new { Book = g.Key }).ToList();

                    if (BUlist.Count() > 0)
                    {
                        XlsDocument xls = new XlsDocument();
                        Worksheet sheet = xls.Workbook.Worksheets.Add(UMS_Resource.SHEET);
                        Cells cells = sheet.Cells;
                        Cell _objCell = null;

                        _objCell = cells.Add(1, 4, Resource.BEDC + "\n" +UMS_Resource.RPT_AGED + DateTime.Today.ToShortDateString());
                        sheet.AddMergeArea(new MergeArea(1, 2, 4, 6));
                        _objCell.Font.Weight = FontWeight.ExtraBold;
                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                        _objCell.Font.FontFamily = FontFamilies.Roman;

                        int DetailsRowNo = 4;
                        int CustomersRowNo = 8;

                        for (int i = 0; i < BUlist.Count; i++)//BU
                        {
                            for (int j = 0; j < SUlist.Count; j++)//SU
                            {
                                for (int k = 0; k < SClist.Count; k++)//SC
                                {
                                    //for (int l = 0; l < Cyclelist.Count; l++)//Cycle
                                    //{
                                    //    for (int m = 0; m < Booklist.Count; m++)//Book
                                    //    {

                                    var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                     where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                     && list.Field<string>("ServiceUnitName") == SUlist[j].ServiceUnit
                                                     && list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                                     //&& list.Field<string>("CycleName") == Cyclelist[l].Cycle
                                                     //&& list.Field<string>("BookCode") == Booklist[m].Book
                                                     orderby list["CustomerSortOrder"] ascending
                                                     orderby list["BookSortOrder"] ascending
                                                     //orderby list["CycleName"] ascending
                                                     select list).ToList();

                                    if (Customers.Count() > 0)
                                    {
                                        cells.Add(DetailsRowNo, 1, "Business Unit");
                                        cells.Add(DetailsRowNo, 3, "Service Unit");
                                        cells.Add(DetailsRowNo, 5, "Service Center");
                                        //cells.Add(DetailsRowNo + 2, 1, "BookGroup");
                                        //cells.Add(DetailsRowNo + 2, 4, "Book Code");

                                        cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                        cells.Add(DetailsRowNo, 4, SUlist[j].ServiceUnit).Font.Bold = true;
                                        cells.Add(DetailsRowNo, 6, SClist[k].ServiceCenter).Font.Bold = true;
                                        //cells.Add(DetailsRowNo + 2, 2, Cyclelist[l].Cycle).Font.Bold = true;
                                        //cells.Add(DetailsRowNo + 2, 5, Booklist[m].Book).Font.Bold = true;


                                        _objCell = cells.Add(DetailsRowNo + 3, 1, "Sno");
                                        sheet.AddMergeArea(new MergeArea(DetailsRowNo + 3, DetailsRowNo + 4, 1, 1));
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                        _objCell = cells.Add(DetailsRowNo + 3, 2, "Account No");
                                        sheet.AddMergeArea(new MergeArea(DetailsRowNo + 3, DetailsRowNo + 4, 2, 2));
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                        _objCell = cells.Add(DetailsRowNo + 3, 3, "Old Account No");
                                        sheet.AddMergeArea(new MergeArea(DetailsRowNo + 3, DetailsRowNo + 4, 3, 3));
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                        _objCell = cells.Add(DetailsRowNo + 3, 4, "Name");
                                        sheet.AddMergeArea(new MergeArea(DetailsRowNo + 3, DetailsRowNo + 4, 4, 4));
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                        _objCell = cells.Add(DetailsRowNo + 3, 5, "OutStandingAmount");
                                        sheet.AddMergeArea(new MergeArea(DetailsRowNo + 3, DetailsRowNo + 4, 5, 5));
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                        _objCell = cells.Add(DetailsRowNo + 3, 6, "30 Days");
                                        sheet.AddMergeArea(new MergeArea(DetailsRowNo + 3, DetailsRowNo + 3, 6, 8));
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                        _objCell = cells.Add(DetailsRowNo + 4, 6, "BillGeneratedDate");
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                        _objCell = cells.Add(DetailsRowNo + 4, 7, "TotalBillAmountWithTax");
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                        _objCell = cells.Add(DetailsRowNo + 4, 8, "TotalBillAmountWithArrears");
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                        _objCell = cells.Add(DetailsRowNo + 3, 9, "60 Days");
                                        sheet.AddMergeArea(new MergeArea(DetailsRowNo + 3, DetailsRowNo + 3, 9, 11));
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                        _objCell = cells.Add(DetailsRowNo + 4, 9, "BillGeneratedDate");
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                        _objCell = cells.Add(DetailsRowNo + 4, 10, "TotalBillAmountWithTax");
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                        _objCell = cells.Add(DetailsRowNo + 4, 11, "TotalBillAmountWithArrears");
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                        _objCell = cells.Add(DetailsRowNo + 3, 12, "90 Days");
                                        sheet.AddMergeArea(new MergeArea(DetailsRowNo + 3, DetailsRowNo + 3, 12, 14));
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                        _objCell = cells.Add(DetailsRowNo + 4, 12, "BillGeneratedDate");
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                        _objCell = cells.Add(DetailsRowNo + 4, 13, "TotalBillAmountWithTax");
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell = cells.Add(DetailsRowNo + 4, 14, "TotalBillAmountWithArrears");
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                        _objCell = cells.Add(DetailsRowNo + 3, 15, "120 Days");
                                        sheet.AddMergeArea(new MergeArea(DetailsRowNo + 3, DetailsRowNo + 3, 15, 17));
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                        _objCell = cells.Add(DetailsRowNo + 4, 15, "BillGeneratedDate");
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                        _objCell = cells.Add(DetailsRowNo + 4, 16, "TotalBillAmountWithTax");
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                        _objCell = cells.Add(DetailsRowNo + 4, 17, "TotalBillAmountWithArrears");
                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                        _objCell.Font.FontFamily = FontFamilies.Roman;

                                        CustomersRowNo++;
                                        //decimal subtotal = 0;
                                        for (int n = 0; n < Customers.Count; n++)
                                        {
                                            DataRow dr = (DataRow)Customers[n];
                                            cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                                            cells.Add(CustomersRowNo, 2, dr["AccountNo"].ToString());
                                            cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
                                            cells.Add(CustomersRowNo, 4, dr["Name"].ToString());
                                            cells.Add(CustomersRowNo, 5, Convert.ToDecimal(dr["OutStandingAmount"]));
                                            cells.Add(CustomersRowNo, 6, dr["BillGeneratedDate30"].ToString());
                                            cells.Add(CustomersRowNo, 7, Convert.ToDecimal(dr["TotalBillAmountWithTax30"]));
                                            cells.Add(CustomersRowNo, 8, Convert.ToDecimal(dr["TotalBillAmountWithArrears30"]));
                                            cells.Add(CustomersRowNo, 9, dr["BillGeneratedDate60"].ToString());
                                            cells.Add(CustomersRowNo, 10, Convert.ToDecimal(dr["TotalBillAmountWithTax60"]));
                                            cells.Add(CustomersRowNo, 11, Convert.ToDecimal(dr["TotalBillAmountWithArrears60"]));
                                            cells.Add(CustomersRowNo, 12, dr["BillGeneratedDate90"].ToString());
                                            cells.Add(CustomersRowNo, 13, Convert.ToDecimal(dr["TotalBillAmountWithTax90"]));
                                            cells.Add(CustomersRowNo, 14, Convert.ToDecimal(dr["TotalBillAmountWithArrears90"]));
                                            cells.Add(CustomersRowNo, 15, dr["BillGeneratedDate120"].ToString());
                                            cells.Add(CustomersRowNo, 16, Convert.ToDecimal(dr["TotalBillAmountWithTax120"]));
                                            cells.Add(CustomersRowNo, 17, Convert.ToDecimal(dr["TotalBillAmountWithArrears120"]));

                                            CustomersRowNo++;
                                            //subtotal += Convert.ToDecimal(dr["OutStandingAmount"]);                                            
                                        }

                                        //cells.Add(CustomersRowNo + 2, 4, "Total");
                                        //cells.Add(CustomersRowNo + 2, 5, Convert.ToDecimal(subtotal));

                                        DetailsRowNo = CustomersRowNo + 1;
                                        CustomersRowNo = CustomersRowNo + 5;
                                    }
                                    //    }
                                    //}
                                }
                            }
                        }

                        string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                        fileName = "CustomersLast120daysBillStatus_" + fileName;
                        string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                        xls.FileName = filePath;
                        xls.Save();
                        _objiDsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                    }
                    else
                        Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                    Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region DDLEvents
        protected void ddlBU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _objCommonMethods.BindUserServiceUnits_BuIds(cblSU, BU, Resource.DDL_ALL, false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void cblSU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _objCommonMethods.BindUserServiceCenters_SuIds(cblSC, _objCommonMethods.CollectSelectedItemsValues(cblSU, ","), Resource.DDL_ALL, false);
                if (cblSC.Items.Count > 0)
                    divSC.Visible = true;
                else
                    divSC.Visible = false;
                //int count = 0;
                //foreach (ListItem li in cblSU.Items)
                //{
                //    if (li.Selected == true)
                //    {
                //        count++;
                //    }
                //}
                //if (count > 0)
                //{
                //    divSC.Visible = true;
                //    _objCommonMethods.BindUserServiceCenters_SuIds(cblSC, _objCommonMethods.CollectSelectedItemsValues(cblSU, ","), Resource.DDL_ALL, false);
                //}
                //else
                //{
                //    divSC.Visible = false;
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods
        private void BindDropDowns()
        {
            _objCommonMethods.BindUserBusinessUnits(ddlBU, string.Empty, false, Resource.DDL_ALL);
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
            {
                string BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                ddlBU.SelectedIndex = ddlBU.Items.IndexOf(ddlBU.Items.FindByValue(BUID));
                ddlBU.Enabled = false;
                ddlBU_SelectedIndexChanged(ddlBU, new EventArgs());
            }
        }
        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void BindGrid()
        {
            try
            {

                RptAgedCustsBe _objRptAgedCustsBe = new RptAgedCustsBe();
                _objRptAgedCustsBe.BU_ID = this.BU;
                _objRptAgedCustsBe.SU_ID = _objCommonMethods.CollectSelectedItemsValues(cblSU, ",");
                _objRptAgedCustsBe.SC_ID = _objCommonMethods.CollectSelectedItemsValues(cblSC, ",");
                _objRptAgedCustsBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                _objRptAgedCustsBe.PageSize = PageSize;
                DataSet ds = new DataSet();

                ds = _objReportsBal.GetAgedCustsList(_objRptAgedCustsBe);

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    UCPaging1.Visible= UCPaging2.Visible=divExport.Visible = true;
                    hfTotalRecords.Value = ds.Tables[0].Rows.Count.ToString();

                    //BindPagingDropDown(ds.Tables[0].Rows.Count);
                    //CreatePagingDT(ds.Tables[0].Rows.Count);

                    gvAgedCustomers.DataSource = ds.Tables[0];
                    gvAgedCustomers.DataBind();
                }
                else
                {
                    UCPaging1.Visible= UCPaging2.Visible=divExport.Visible = false;
                    hfTotalRecords.Value = Constants.Zero.ToString();
                    Message("Customers Not Found", UMS_Resource.MESSAGETYPE_ERROR);
                    gvAgedCustomers.DataSource = new DataTable();
                    gvAgedCustomers.DataBind();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }
        #endregion

        //#region Paging
        //protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        BindCheckMetersList();
        //        hfPageSize.Value = ddlPageSize.SelectedItem.Text;
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void lkbtnFirst_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        BindCheckMetersList();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void lkbtnPrevious_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum -= Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        BindCheckMetersList();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void lkbtnNext_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum += Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        BindCheckMetersList();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void lkbtnLast_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = hfLastPage.Value;
        //        BindCheckMetersList();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //private void CreatePagingDT(int TotalNoOfRecs)
        //{
        //    try
        //    {
        //        double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
        //        DataTable dtPages = new DataTable();
        //        dtPages.Columns.Add("PageNo", typeof(int));
        //        int currentpage = Convert.ToInt32(hfPageNo.Value);
        //        lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
        //        _objiDsignBal.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
        //        lblCurrentPage.Text = hfPageNo.Value;
        //        if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
        //        if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //private void BindPagingDropDown(int TotalRecords)
        //{
        //    try
        //    {
        //        _objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //#endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        //#region Paging
        //protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        GetAgedCustomers();
        //        hfPageSize.Value = ddlPageSize.SelectedItem.Text;
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnFirst_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        GetAgedCustomers();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnPrevious_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum -= Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        GetAgedCustomers();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnNext_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum += Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        GetAgedCustomers();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnLast_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = hfLastPage.Value;
        //        GetAgedCustomers();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //private void CreatePagingDT(int TotalNoOfRecs)
        //{
        //    try
        //    {
        //        double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
        //        DataTable dtPages = new DataTable();
        //        dtPages.Columns.Add("PageNo", typeof(int));
        //        int currentpage = Convert.ToInt32(hfPageNo.Value);
        //        lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
        //        _objiDsignBal.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
        //        lblCurrentPage.Text = hfPageNo.Value;
        //        if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.Color.Gray; }
        //        if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //private void BindPagingDropDown(int TotalRecords)
        //{
        //    try
        //    {
        //        _objCommonMethods.BindPagingDropDownListReport(TotalRecords, this.PageSize, ddlPageSize);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //#endregion


    }

}