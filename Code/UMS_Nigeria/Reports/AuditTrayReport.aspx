﻿<%@ Page Title="::Audit Tray Report::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="AuditTrayReport.aspx.cs" Inherits="UMS_Nigeria.Reports.AuditTrayReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="contentBody" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upAddCycle" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div class="text_total">
                    <div class="text-heading">
                        <asp:Literal ID="litSearchCustomer" runat="server" Text="<%$ Resources:Resource, AUDIT_TRAY_REPORT_HEADING%>"></asp:Literal>
                    </div>
                    <div class="star_text">
                    </div>
                </div>
                <div class="clear">
                </div>
                <div class="dot-line">
                </div>
                <div class="inner-box">
                    <div class="inner-box1">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litFromDate" runat="server" Text="<%$ Resources:Resource, FROM_DATE%>"></asp:Literal>
                                <span class="span_star">*</span></label>
                            <div class="space">
                            </div>
                            <asp:TextBox ID="txtFromDate" MaxLength="10" runat="server" AutoComplete="off" CssClass="text-box"
                                onblur="TextBoxBlurValidationlbl(this, 'spanFromDate', 'From Date')" onchange="TextBoxBlurValidationlbl(this, 'spanFromDate', 'From Date')"
                                placeholder="Enter From Date"></asp:TextBox>
                            <asp:Image ID="imgFromDate" ImageUrl="~/images/icon_calendar.png" Style="width: 24px;
                                vertical-align: middle" AlternateText="Calender" runat="server" />
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtFromDate"
                                FilterType="Custom,Numbers" ValidChars="/">
                            </asp:FilteredTextBoxExtender>
                            <span id="spanFromDate" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box2">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litToDate" runat="server" Text="<%$ Resources:Resource, TO_DATE%>"></asp:Literal>
                                <span class="span_star">*</span></label>
                            <div class="space">
                            </div>
                            <asp:TextBox ID="txtToDate" MaxLength="10" runat="server" AutoComplete="off" CssClass="text-box"
                                onblur="TextBoxBlurValidationlbl(this, 'spanToDate', 'To Date')" onchange="TextBoxBlurValidationlbl(this, 'spanToDate', 'To Date')"
                                placeholder="Enter To Date"></asp:TextBox>
                            <asp:Image ID="imgToDate" ImageUrl="~/images/icon_calendar.png" Style="width: 24px;
                                vertical-align: middle" AlternateText="Calender" runat="server" />
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtToDate"
                                FilterType="Custom,Numbers" ValidChars="/">
                            </asp:FilteredTextBoxExtender>
                            <span id="spanToDate" class="span_color"></span>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div id="divBusinessUnit" runat="server">
                        <div class="check_baa iabody_lblBook">
                            <div class="text-inner_a">
                                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" ID="ChkBUAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                    <asp:CheckBoxList ID="cblBusinessUnits" class="text-inner-b" AutoPostBack="true"
                                        RepeatDirection="Horizontal" RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblBusinessUnits_SelectedIndexChanged">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div id="divServiceUnits" runat="server" visible="false">
                        <div class="check_baa iabody_lblBook">
                            <div class="text-inner_a">
                                <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" ID="ChkSUAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                    <asp:CheckBoxList ID="cblServiceUnits" class="text-inner-b" AutoPostBack="true" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceUnits_SelectedIndexChanged">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div id="divServiceCenters" runat="server" visible="false">
                        <div class="check_baa iabody_lblBook">
                            <div class="text-inner_a">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" ID="chkSCAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                    <asp:CheckBoxList ID="cblServiceCenters" class="text-inner-b" RepeatDirection="Horizontal"
                                        AutoPostBack="true" RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceCenters_SelectedIndexChanged">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div id="divCycles" runat="server" visible="false">
                        <div class="check_baa iabody_lblBook">
                            <div class="text-inner_a">
                                <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" class="text-inner-b" ID="chkCycleAll" Text="All" AutoPostBack="true" />
                                    <asp:CheckBoxList ID="cblCycles" class="text-inner-b" AutoPostBack="true" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblCycles_SelectedIndexChanged">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div id="divBookNos" runat="server" visible="false">
                        <div class="check_baa iabody_lblBook">
                            <div class="text-inner_a">
                                <asp:Literal ID="litBookNo" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" class="text-inner-b" ID="chkBooksAll" Text="All" AutoPostBack="true" />
                                    <asp:CheckBoxList ID="cblBooks" class="text-inner-b" AutoPostBack="true" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblBooks_SelectedIndexChanged">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div id="divtariff" runat="server" visible="false">
                        <div class="check_baa iabody_lblBook">
                            <div class="text-inner_a">
                                <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" class="text-inner-b" ID="chkTariffAll" Text="All" />
                                    <asp:CheckBoxList ID="cblTariff" class="text-inner-b" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server">
                                    </asp:CheckBoxList>
                                    <%--OnSelectedIndexChanged="cblTariff_SelectedIndexChanged"AutoPostBack="true"AutoPostBack="true"--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <%--<div id="divUser" runat="server" visible="false">
                        <div class="check_baa">
                            <div class="text-inner_a">
                                <asp:Literal ID="Literal6" runat="server" Text="Users List"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" class="text-inner-b" ID="chkUserAll" Text="All" />
                                    <asp:CheckBoxList ID="cblUser" class="text-inner-b" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>--%>
                </div>
                <div style="clear: both;">
                </div>
                <div class="box_total">
                    <div class="box_total_a">
                        <asp:Button ID="btnGo" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SEARCH%>"
                            CssClass="box_s" runat="server" OnClick="btnGo_Click" />
                    </div>
                </div>
                <div class="pad_10 clear">
                </div>
                <div class="audit_rsp_total" style="margin-left: 6px;">
                    <div class="audit_rsp2">
                        <div class="audti_design">
                            <h2>
                                Meter Assign Customers</h2>
                            <div class="audit_change">
                                <p>
                                    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, TOTAL_CHANGES%>"></asp:Label>
                                    <asp:Label ID="lblAssignedMeterCount" runat="server" Text="0"></asp:Label>
                                </p>
                            </div>
                            <div class=" audit_grid">
                                <asp:GridView ID="gvAssignedMeter" runat="server" AutoGenerateColumns="false">
                                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                    <EmptyDataTemplate>
                                        Meter Assigned Customers are not found.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Global Account No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAccount" runat="server" Text='<%# Eval("AccountNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="OldAccountNo" HeaderText="Old Account No" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="Name" HeaderText="Name" />
                                        <asp:BoundField ItemStyle-CssClass="width_40" ItemStyle-HorizontalAlign="Left" DataField="ClassName"
                                            HeaderText="Tariff" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="NewMeterNo" HeaderText="Meter No" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="AssignedMeterDate" HeaderText="Meter Assigned Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="InitialBillingkWh" HeaderText="Initial Reading" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="CreatedBy" HeaderText="Requested By" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="TransactionDate" HeaderText="Requested Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedBy" HeaderText="Last Approved By" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedDate" HeaderText="Last Approved Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ApprovalStatus" HeaderText="Status" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="LastTransactionDate"
                                            HeaderText="Last Transaction Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" HtmlEncode="False" DataField="Remarks"
                                            HeaderText="Remarks" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="audit_showall">
                                <div class="aushow">
                                    <p>
                                        <asp:LinkButton ID="lbtnAssignMeterShowAll" Visible="false" runat="server" OnClick="lbtnAssignMeterShowAll_Click">Show All</asp:LinkButton>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="audit_rsp2">
                        <div class="audti_design">
                            <h2>
                                Name Changes</h2>
                            <div class="audit_change">
                                <p>
                                    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, TOTAL_CHANGES%>"></asp:Label>
                                    <asp:Label ID="lblNameChangesCount" runat="server" Text="0"></asp:Label>
                                </p>
                            </div>
                            <div class=" audit_grid">
                                <asp:GridView ID="gvNameChanges" runat="server" AutoGenerateColumns="false">
                                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                    <EmptyDataTemplate>
                                        No Name Changes Found.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Global Account No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAccount" runat="server" Text='<%# Eval("AccountNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField ItemStyle-CssClass="width_92" ItemStyle-HorizontalAlign="Left" DataField="OldAccountNo"
                                            HeaderText="Old Account No" />
                                        <asp:BoundField ItemStyle-CssClass="width_40" ItemStyle-HorizontalAlign="Left" DataField="ClassName"
                                            HeaderText="Tariff" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="OldName" HeaderText="Old Name" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="NewName" HeaderText="New Name" />
                                        <asp:BoundField DataField="OldSurName" HeaderText="Old Known AS" />
                                        <asp:BoundField DataField="NewSurName" HeaderText="New Known AS" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="CreatedBy" HeaderText="Requested By" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="TransactionDate" HeaderText="Requested Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedBy" HeaderText="Last Approved By" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedDate" HeaderText="Last Approved Date" />
                                        <asp:BoundField ItemStyle-CssClass="width_70" ItemStyle-HorizontalAlign="Left" DataField="ApprovalStatus"
                                            HeaderText="Status" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="LastTransactionDate"
                                            HeaderText="Last Transaction Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" HtmlEncode="False" DataField="Remarks"
                                            HeaderText="Remarks" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="audit_showall">
                                <div class="aushow">
                                    <p>
                                        <asp:LinkButton ID="lbtnNameShowAll" Visible="false" runat="server" OnClick="lbtnNameShowAll_Click">Show All</asp:LinkButton>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="audit_rsp2">
                        <div class="audti_design">
                            <h2>
                                Address Changes</h2>
                            <div class="audit_change">
                                <p>
                                    <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, TOTAL_CHANGES%>"></asp:Label>
                                    <asp:Label ID="lblAddressChangesCount" runat="server" Text="0"></asp:Label>
                                </p>
                            </div>
                            <div class=" audit_grid">
                                <asp:GridView ID="gvAddressChanges" runat="server" AutoGenerateColumns="false">
                                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                    <EmptyDataTemplate>
                                        No Address Changes Found.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Global Account No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAccount" runat="server" Text='<%# Eval("GlobalAccountNumber") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField ItemStyle-CssClass="width_92" ItemStyle-HorizontalAlign="Left" DataField="OldAccountNo"
                                            HeaderText="Old Account No" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="Name" HeaderText="Name" />
                                        <asp:BoundField ItemStyle-CssClass="width_40" ItemStyle-HorizontalAlign="Left" DataField="ClassName"
                                            HeaderText="Tariff" />
                                        <asp:BoundField HeaderText="Old Postal Address" DataField="OldPostalAddress" />
                                        <asp:BoundField HeaderText="New Postal Address" DataField="NewPostalAddress" />
                                        <asp:BoundField HeaderText="Old Service Address" DataField="OldServiceAddress" />
                                        <asp:BoundField HeaderText="New Service Address" DataField="NewServiceAddress" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="CreatedBy" HeaderText="Requested By" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="TransactionDate" HeaderText="Requested Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedBy" HeaderText="Last Approved By" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedDate" HeaderText="Last Approved Date" />
                                        <asp:BoundField ItemStyle-CssClass="width_70" HeaderText="Status" DataField="ApprovalStatus" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="LastTransactionDate"
                                            HeaderText="Last Transaction Date" />
                                        <asp:BoundField ItemStyle-CssClass="width_62" HeaderText="Remarks" HtmlEncode="False"
                                            DataField="Remarks" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="audit_showall">
                                <div class="aushow">
                                    <p>
                                        <asp:LinkButton ID="lbtnAddressShowAll" Visible="false" runat="server" OnClick="lbtnAddressShowAll_Click">Show All</asp:LinkButton>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="audit_rsp2">
                        <div class="audti_design">
                            <h2>
                                Book Number Changes</h2>
                            <div class="audit_change">
                                <p>
                                    <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, TOTAL_CHANGES%>"></asp:Label>
                                    <asp:Label ID="lblBookNoChangesCount" runat="server" Text="0"></asp:Label>
                                </p>
                            </div>
                            <div class=" audit_grid">
                                <asp:GridView ID="gvBookNoChanges" runat="server" AutoGenerateColumns="false">
                                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                    <EmptyDataTemplate>
                                        No BookNo Changes Found.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Global Account No" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="7%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAccount" runat="server" Text='<%# Eval("AccountNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField ItemStyle-CssClass="width_92" DataField="OldAccountNo" HeaderText="Old Account No" />
                                        <asp:BoundField DataField="Name" HeaderText="Name" />
                                        <asp:BoundField ItemStyle-CssClass="width_40" ItemStyle-HorizontalAlign="Left" DataField="ClassName"
                                            HeaderText="Tariff" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="OldPostalAddress" HeaderText="Old Postal Address" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="NewPostalAddress" HeaderText="New Postal Address" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="OldServiceAddress" HeaderText="Old Service Address" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="NewServiceAddress" HeaderText="New Service Address" />
                                        <asp:TemplateField ItemStyle-Width="10%" HeaderText="Old Book Name" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div id="divOldBookNo" style="width: 150px;" runat="server">
                                                    <%# Eval("OldBookNo") %></div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-Width="10%" HeaderText="New Book Name" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <div id="divNewBookNo" style="width: 150px;" runat="server">
                                                    <%# Eval("NewBookNo") %></div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField ItemStyle-CssClass="width_70" ItemStyle-HorizontalAlign="Left" DataField="CreatedBy"
                                            HeaderText="Requested By" />
                                        <asp:BoundField ItemStyle-CssClass="width_70" ItemStyle-HorizontalAlign="Left" DataField="TransactionDate"
                                            HeaderText="Requested Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedBy" HeaderText="Last Approved By" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedDate" HeaderText="Last Approved Date" />
                                        <asp:BoundField ItemStyle-CssClass="width_70" ItemStyle-HorizontalAlign="Left" DataField="ApprovalStatus"
                                            HeaderText="Status" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="LastTransactionDate"
                                            HeaderText="Last Transaction Date" />
                                        <asp:BoundField ItemStyle-CssClass="width_62" ItemStyle-HorizontalAlign="Left" DataField="Remarks"
                                            HtmlEncode="False" HeaderText="Remarks" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="audit_showall">
                                <div class="aushow">
                                    <p>
                                        <asp:LinkButton ID="lbtnBookNoShowAll" Visible="false" runat="server" OnClick="lbtnBookNoShowAll_Click">Show All</asp:LinkButton>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="audit_rsp2">
                        <div class="audti_design">
                            <h2>
                                Tariff Changes</h2>
                            <div class="audit_change">
                                <p>
                                    <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, TOTAL_CHANGES%>"></asp:Label>
                                    <asp:Label ID="lblTariffChangesCount" runat="server" Text="0"></asp:Label>
                                </p>
                            </div>
                            <div class=" audit_grid">
                                <asp:GridView ID="gvTariffChanges" runat="server" AutoGenerateColumns="false">
                                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                    <EmptyDataTemplate>
                                        No Tariff Changes Found.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Global Account No" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAccount" runat="server" Text='<%# Eval("AccountNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="OldAccountNo" HeaderText="Old Account No" />
                                        <asp:BoundField DataField="Name" HeaderText="Name" />
                                        <asp:BoundField DataField="OldTariff" HeaderText="Old Tariff" />
                                        <asp:BoundField DataField="NewTariff" HeaderText="New Tariff" />
                                        <asp:BoundField DataField="OldCluster" HeaderText="Old Cluster Category" />
                                        <asp:BoundField DataField="NewCluster" HeaderText="New Cluster Category" />
                                        <asp:BoundField DataField="CreatedBy" HeaderText="Requested By" />
                                        <asp:BoundField DataField="TransactionDate" HeaderText="Requested Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedBy" HeaderText="Last Approved By" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedDate" HeaderText="Last Approved Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ApprovalStatus" HeaderText="Status" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="LastTransactionDate"
                                            HeaderText="Last Transaction Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" HtmlEncode="False" DataField="Remarks"
                                            HeaderText="Remarks" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="audit_showall">
                                <div class="aushow">
                                    <p>
                                        <asp:LinkButton ID="lbtnTariffShowAll" Visible="false" runat="server" OnClick="lbtnTariffShowAll_Click">Show All</asp:LinkButton>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="audit_rsp2">
                        <div class="audti_design">
                            <h2>
                                Meter Changes</h2>
                            <div class="audit_change">
                                <p>
                                    <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, TOTAL_CHANGES%>"></asp:Label>
                                    <asp:Label ID="lblMeterChangesCount" runat="server" Text="0"></asp:Label>
                                </p>
                            </div>
                            <div class=" audit_grid">
                                <asp:GridView ID="gvMeterChanges" runat="server" AutoGenerateColumns="false">
                                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                    <EmptyDataTemplate>
                                        No Meter Changes Found.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Global Account No" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAccount" runat="server" Text='<%# Eval("AccountNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField ItemStyle-CssClass="width_92" DataField="OldAccountNo" HeaderText="Old Account No" />
                                        <asp:BoundField DataField="Name" HeaderText="Name" />
                                        <asp:BoundField ItemStyle-CssClass="width_40" DataField="ClassName" HeaderText="Tariff" />
                                        <asp:BoundField DataField="OldMeterNo" HtmlEncode="False" HeaderText="Old Meter No" />
                                        <asp:BoundField DataField="NewMeterNo" HtmlEncode="False" HeaderText="New Meter No" />
                                        <asp:BoundField DataField="Readings" HtmlEncode="False" HeaderText="Old & New Meter Readings" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="OldMeterType" HeaderText="Old MeterType" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="NewMeterType" HeaderText="New MeterType" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="AssignedMeterDate" HeaderText="Meter Changed Date" />
                                        <asp:BoundField DataField="CreatedBy" HeaderText="Requested By" />
                                        <asp:BoundField DataField="TransactionDate" HeaderText="Requested Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedBy" HeaderText="Last Approved By" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedDate" HeaderText="Last Approved Date" />
                                        <asp:BoundField ItemStyle-CssClass="width_70" ItemStyle-HorizontalAlign="Left" DataField="ApprovalStatus"
                                            HeaderText="Status" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="LastTransactionDate"
                                            HeaderText="Last Transaction Date" />
                                        <asp:BoundField ItemStyle-CssClass="width_62" ItemStyle-HorizontalAlign="Left" HtmlEncode="False"
                                            DataField="Remarks" HeaderText="Remarks" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="audit_showall">
                                <div class="aushow">
                                    <p>
                                        <asp:LinkButton ID="lbtnMeterShowAll" Visible="false" runat="server" OnClick="lbtnMeterShowAll_Click">Show All</asp:LinkButton>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="audit_rsp2">
                        <div class="audti_design">
                            <h2>
                                Read To Direct Changed Customers</h2>
                            <div class="audit_change">
                                <p>
                                    <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, TOTAL_CHANGES%>"></asp:Label>
                                    <asp:Label ID="lblReadToDirectCount" runat="server" Text="0"></asp:Label>
                                </p>
                            </div>
                            <div class=" audit_grid">
                                <asp:GridView ID="gvReadToDirect" runat="server" AutoGenerateColumns="false">
                                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                    <EmptyDataTemplate>
                                        No Read To Direct Changed Customers Found.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Global Account No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAccount" runat="server" Text='<%# Eval("AccountNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="OldAccountNo" HeaderText="Old Account No" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="Name" HeaderText="Name" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ClassName" HeaderText="Tariff" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="NewMeterNo" HeaderText="Meter No" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="CreatedBy" HeaderText="Requested By" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="TransactionDate" HeaderText="Requested Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedBy" HeaderText="Last Approved By" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedDate" HeaderText="Last Approved Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ApprovalStatus" HeaderText="Status" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="LastTransactionDate"
                                            HeaderText="Last Transaction Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" HtmlEncode="False" DataField="Remarks"
                                            HeaderText="Remarks" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="audit_showall">
                                <div class="aushow">
                                    <p>
                                        <asp:LinkButton ID="lbtnReadToDirectShowAll" Visible="false" runat="server" OnClick="lbtnReadToDirectShowAll_Click">Show All</asp:LinkButton>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="audit_rsp2">
                        <div class="audti_design">
                            <h2>
                                Customer Status Changes</h2>
                            <div class="audit_change">
                                <p>
                                    <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, TOTAL_CHANGES%>"></asp:Label>
                                    <asp:Label ID="lblCustomerChanges" runat="server" Text="0"></asp:Label>
                                </p>
                            </div>
                            <div class=" audit_grid">
                                <asp:GridView ID="gvCustomerChanges" runat="server" AutoGenerateColumns="false">
                                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                    <EmptyDataTemplate>
                                        No Customer Changes Found.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Global Account No" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAccount" runat="server" Text='<%# Eval("AccountNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="OldAccountNo" HeaderText="Old Account No" />
                                        <asp:BoundField DataField="Name" HeaderText="Name" />
                                        <asp:BoundField ItemStyle-CssClass="width_40" ItemStyle-HorizontalAlign="Left" DataField="ClassName"
                                            HeaderText="Tariff" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="OldStatus" HeaderText="Old Status" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="NewStatus" HeaderText="New Status" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="StatusChangedDate" HeaderText="Status Changed Date" />
                                        <asp:BoundField DataField="CreatedBy" HeaderText="Requested By" />
                                        <asp:BoundField DataField="TransactionDate" HeaderText="Requested Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedBy" HeaderText="Last Approved By" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedDate" HeaderText="Last Approved Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ApprovalStatus" HeaderText="Status" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="LastTransactionDate"
                                            HeaderText="Last Transaction Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" HtmlEncode="False" DataField="Remarks"
                                            HeaderText="Remarks" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="audit_showall">
                                <div class="aushow">
                                    <p>
                                        <asp:LinkButton ID="lbtnCustomerShowAll" Visible="false" runat="server" OnClick="lbtnCustomerShowAll_Click">Show All</asp:LinkButton>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="audit_rsp2">
                        <div class="audti_design">
                            <h2>
                                Customer Type Changes</h2>
                            <div class="audit_change">
                                <p>
                                    <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, TOTAL_CHANGES%>"></asp:Label>
                                    <asp:Label ID="lblCustTypeCount" runat="server" Text="0"></asp:Label>
                                </p>
                            </div>
                            <div class=" audit_grid">
                                <asp:GridView ID="gvCustomerType" runat="server" AutoGenerateColumns="false">
                                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                    <EmptyDataTemplate>
                                        No CustomerType Changes Found.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Global Account No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAccount" runat="server" Text='<%# Eval("AccountNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="OldAccountNo" HeaderText="Old Account No" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="Name" HeaderText="Name" />
                                        <asp:BoundField ItemStyle-CssClass="width_40" ItemStyle-HorizontalAlign="Left" DataField="ClassName"
                                            HeaderText="Tariff" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="OldCustomerType" HeaderText="Old Customer Type" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="NewCustomerType" HeaderText="New Customer Type" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="CreatedBy" HeaderText="Requested By" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="TransactionDate" HeaderText="Requested Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedBy" HeaderText="Last Approved By" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedDate" HeaderText="Last Approved Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ApprovalStatus" HeaderText="Status" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="LastTransactionDate"
                                            HeaderText="Last Transaction Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" HtmlEncode="False" DataField="Remarks"
                                            HeaderText="Remarks" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="audit_showall">
                                <div class="aushow">
                                    <p>
                                        <asp:LinkButton ID="lbtnCustTypeShowAll" Visible="false" runat="server" OnClick="lbtnCustTypeShowAll_Click">Show All</asp:LinkButton>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="audit_rsp2">
                        <div class="audti_design">
                            <h2>
                                Customer Present Meter Reading Adjusted Changes</h2>
                            <div class="audit_change">
                                <p>
                                    <asp:Label ID="Label10" runat="server" Text="<%$ Resources:Resource, TOTAL_CHANGES%>"></asp:Label>
                                    <asp:Label ID="lblPreserntMeterReading" runat="server" Text="0"></asp:Label>
                                </p>
                            </div>
                            <div class=" audit_grid">
                                <asp:GridView ID="gvPresentMeterReading" runat="server" AutoGenerateColumns="false">
                                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                    <EmptyDataTemplate>
                                        No CustomerType Changes Found.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Global Account No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAccount" runat="server" Text='<%# Eval("AccountNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="OldAccountNo" HeaderText="Old Account No" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="Name" HeaderText="Name" />
                                        <asp:BoundField ItemStyle-CssClass="width_40" ItemStyle-HorizontalAlign="Left" DataField="ClassName"
                                            HeaderText="Tariff" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="PreviousReading" HeaderText="Last Previous Reading" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="PresentReading" HeaderText="Last Present Reading" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="NewPresentReading" HeaderText="Adjusted Reading" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="PreviousReadDate" HeaderText="Previous Read Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="PresentReadDate" HeaderText="Adjusted Reading Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="NewMeterNo" HeaderText="Meter Number" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="CreatedBy" HeaderText="Requested By" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="TransactionDate" HeaderText="Requested Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedBy" HeaderText="Last Approved By" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedDate" HeaderText="Last Approved Date" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ApprovalStatus" HeaderText="Status" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="LastTransactionDate"
                                            HeaderText="Last Transaction Date" />
                                        <asp:BoundField ItemStyle-CssClass="width_62" ItemStyle-HorizontalAlign="Left" HtmlEncode="False"
                                            DataField="Remarks" HeaderText="Remarks" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="audit_showall">
                                <div class="aushow">
                                    <p>
                                        <asp:LinkButton ID="lbtnPresentMeterReading" Visible="false" runat="server" OnClick="lbtnPresentMeterReadingShowAll_Click">Show All</asp:LinkButton>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="audit_rsp2">
                        <div class="audti_design">
                            <h2>
                                New Customer Registrations</h2>
                            <div class="audit_change">
                                <p>
                                    <asp:Label ID="Label11" runat="server" Text="<%$ Resources:Resource, TOTAL_CHANGES%>"></asp:Label>
                                    <asp:Label ID="lblTotalApproval" runat="server" Text="0"></asp:Label>
                                </p>
                            </div>
                            <div class="audit_grid">
                                <asp:GridView ID="gvCustomerRegistration" runat="server" AutoGenerateColumns="false">
                                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                                    <EmptyDataTemplate>
                                        No Registration Approval Found.
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                            HeaderStyle-HorizontalAlign="Center">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Global Account Number" runat="server" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-Width="8%" ItemStyle-Width="8%">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblGlobalAccountNumber" runat="server" Text='<%#Eval("GlobalAccountNumber")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Full Name" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFirstNameLandlord" runat="server" Text='<%#Eval("FullName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--   <asp:TemplateField HeaderText="Customer Type" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCustomerType" runat="server" Text='<%#Eval("CustomerType") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Tariff" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTariff" runat="server" Text='<%#Eval("Tariff") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Read Type" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblReadType" runat="server" Text='<%#Eval("ReadType") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--   <asp:TemplateField HeaderText="Phase" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPhaseId" runat="server" Text='<%#Eval("PhaseId") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Route Name" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRouteName" runat="server" Text='<%#Eval("RouteName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Business Unit" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBusinessUnitName" runat="server" Text='<%#Eval("BusinessUnit") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Service Unit" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblServiceUnitName" runat="server" Text='<%#Eval("ServiceUnitName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Service Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblServiceCenterName" runat="server" Text='<%#Eval("ServiceCenterName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Book Group" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBookGroup" runat="server" Text='<%#Eval("CycleName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Book Number" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBookCode" runat="server" Text='<%#Eval("BookCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Approval Status" ItemStyle-HorizontalAlign="Center"
                                            HeaderStyle-Width="12%" ItemStyle-Width="12%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Request Date" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="8%"
                                            ItemStyle-Width="8%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCreatedDate" runat="server" Text='<%#Eval("CreatedDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedBy" HeaderText="Last Approved By" />
                                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ModifiedDate" HeaderText="Last Approved Date" />
                                        <asp:TemplateField HeaderText="Details" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkgvCustomerRegistrationDetails" CommandArgument='<%#Eval("ARID") %>'
                                                    CommandName="details" ForeColor="Green" runat="server" Text="View" OnClick="lnkgvCustomerRegistrationDetails_Click"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="audit_showall">
                                <div class="aushow">
                                    <p>
                                        <asp:LinkButton ID="lnkCustomerRegistrationShowAll" Visible="false" runat="server"
                                            OnClick="lbtnCustomerRegistrationShowAll_Click">Show All</asp:LinkButton>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <asp:ModalPopupExtender ID="mpeCustomerDetails" runat="server" PopupControlID="pnlCustomerDetails"
                    TargetControlID="HiddenField2" BackgroundCssClass="modalBackground" CancelControlID="btnClose">
                </asp:ModalPopupExtender>
                <asp:HiddenField ID="HiddenField2" runat="server" />
                <asp:Panel ID="pnlCustomerDetails" runat="server" CssClass="modalPopup">
                    <div style="width: 1100px; position: relative; top: 0px; height: 35px; background-color: #ffffff">
                        <div class="text-heading">
                            <asp:Literal ID="litAddState" runat="server" Text="APPROVAL REGISTRATION STATUS"></asp:Literal>
                        </div>
                        <div style="width: 21px; float: right; font-size: 12px; position: relative; margin-right: 50PX;">
                            <div class="star_text">
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="ok_btn" />
                            </div>
                        </div>
                    </div>
                    <div style="width: 1100px; height: 600px; overflow-x: scroll;">
                        <div>
                            <div class="inner-sec">
                                <div class="text_total" style="margin-bottom: -9px;">
                                    <%-- <div class="text-heading">
                                    <asp:Literal ID="litAddState" runat="server" Text="CUSTOMER DETAILS"></asp:Literal>
                                </div>
                                <div style="width: 21px; float: right; font-size: 12px; position: relative; margin-right: 40PX;">
                                    <div class="star_text">
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="ok_btn" />
                                    </div>
                                </div>--%>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-sec">
                                        <h4 class="subHeading">
                                            <asp:Literal ID="Literal35" runat="server" Text="BOOK INFORMATION"></asp:Literal>
                                        </h4>
                                        <div class="out-bor_aDetails">
                                            <table class="customerdetailsTamle">
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal37" runat="server" Text="Business Unit"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblBuDsp" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal36" runat="server" Text="Service Unit"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblSUDsp" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal38" runat="server" Text="Service Center"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblSCDsp" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal39" runat="server" Text="Book Group"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblBookGroupDsp" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal40" runat="server" Text="Book Number"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblBookCodeDsp" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-sec">
                                        <h4 class="subHeading">
                                            <asp:Literal ID="Literal41" runat="server" Text="ACCOUNT STATUS DETAILS"></asp:Literal>
                                        </h4>
                                        <div class="out-bor_aDetails">
                                            <table class="customerdetailsTamle">
                                                <%--style="margin-left: -20px;"--%>
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <%--style="width: 50%"--%>
                                                        <asp:Literal ID="Literal14" runat="server" Text="Global Account No"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <%--class="customerdetailstamleTdsize"--%>
                                                        <asp:Label ID="lblGlobalAccountNo" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <%--style="width: 50%"--%>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <%--style="width: 50%"--%>
                                                        <%--<asp:Literal ID="litAccountNo" runat="server" Text="Account No"></asp:Literal>--%>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        <%--:--%>
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <%--<asp:Label ID="lblAccountNo" runat="server" Text="--"></asp:Label>--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <%--<div class="inner-box1">--%>
                                    <div class="out-bor_aDetails">
                                        <table class="customerdetailsTamle">
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litCustomerType" runat="server" Text="<%$ Resources:Resource, CUSTOMER_TYPE%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <%--<td class="customerdetailstamleTdsize">--%>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblCustomerType" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litCustomerStatus" runat="server" Text="<%$ Resources:Resource, STATUS%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two" colspan="4">
                                                    <b>
                                                        <asp:Label ID="lblCustomerStatus" runat="server" Text="--"></asp:Label></b>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <%--<div class="inner-box1">--%>
                                    <div class="out-bor_aDetails">
                                        <table class="customerdetailsTamle" width="25px;">
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Literal ID="litOutStanding" runat="server" Text="<%$ Resources:Resource, OUT_STANDING_AMT%>"></asp:Literal>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="customerdetailstamleTdsize-two">
                                                    <asp:Label ID="lblOutStanding" runat="server" Text="--"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-two">
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                </td>
                                                <td class="customerdetailstamleTdsize-two" colspan="4">
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <%-------------------------Land Lord Information Start----------------------%>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-sec">
                                        <h4 class="subHeading">
                                            <asp:Literal ID="Literal213" runat="server" Text="<%$ Resources:Resource, LAND_LORD_INFO%>"></asp:Literal>
                                        </h4>
                                        <div class="out-bor_aDetails">
                                            <table class="customerdetailsTamle">
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:Resource, TITLE%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblLInfoTitle" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, F_NAME%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblLInfoFirstname" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:Resource, M_NAME%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblLInfoMiddleName" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:Resource, L_NAME%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblLInfoLastName" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:Resource, KNOWN_AS%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblLInfoKnownAs" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal19" runat="server" Text="<%$ Resources:Resource, EMAIL_ID%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblLInfoEmail" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal20" runat="server" Text="<%$ Resources:Resource, HOME%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblLInfoHomePhone" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal21222" runat="server" Text="<%$ Resources:Resource, BUSINESS%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblLInfoBussPhone" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal22" runat="server" Text="<%$ Resources:Resource, OTHERS%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblLInfoOtherPhone" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <%-------------------------Land Lord Information End----------------------%>
                                    <%-------------------------Tenent Information Start----------------------%>
                                    <table class="customerdetailsTamle" style="width: 34%">
                                        <tr>
                                            <td class="customerdetailstamleTd-two">
                                                <asp:Label ID="Label12" runat="server" Text="Is Tenant"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd-dot">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize-two">
                                                <asp:Label ID="lblIsTenant" runat="server" Text="--"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="clr">
                                    </div>
                                    <div id="divTenent" runat="server" visible="false">
                                        <div class="inner-sec">
                                            <h4 class="subHeading">
                                                <asp:Literal ID="Literal23" runat="server" Text="<%$ Resources:Resource, TENENT_INFO%>"></asp:Literal>
                                            </h4>
                                            <div class="out-bor_aDetails">
                                                <table class="customerdetailsTamle">
                                                    <tr>
                                                        <td class="customerdetailstamleTd-two">
                                                            <asp:Literal ID="Literal24" runat="server" Text="<%$ Resources:Resource, TITLE%>"></asp:Literal>
                                                        </td>
                                                        <td class="customerdetailstamleTd-dot">
                                                            :
                                                        </td>
                                                        <td class="customerdetailstamleTdsize-two">
                                                            <asp:Label ID="lblTanentTitle" runat="server" Text="--"></asp:Label>
                                                        </td>
                                                        <td class="customerdetailstamleTd-two">
                                                            <asp:Literal ID="Literal25" runat="server" Text="<%$ Resources:Resource, F_NAME%>"></asp:Literal>
                                                        </td>
                                                        <td class="customerdetailstamleTd-dot">
                                                            :
                                                        </td>
                                                        <td class="customerdetailstamleTdsize-two">
                                                            <asp:Label ID="lblTanentFirstName" runat="server" Text="--"></asp:Label>
                                                        </td>
                                                        <td class="customerdetailstamleTd-two">
                                                            <asp:Literal ID="Literal26" runat="server" Text="<%$ Resources:Resource, M_NAME%>"></asp:Literal>
                                                        </td>
                                                        <td class="customerdetailstamleTd-dot">
                                                            :
                                                        </td>
                                                        <td class="customerdetailstamleTdsize-two">
                                                            <asp:Label ID="lblTanentMiddleName" runat="server" Text="--"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="customerdetailstamleTd-two">
                                                            <asp:Literal ID="Literal27" runat="server" Text="<%$ Resources:Resource, L_NAME%>"></asp:Literal>
                                                        </td>
                                                        <td class="customerdetailstamleTd-dot">
                                                            :
                                                        </td>
                                                        <td class="customerdetailstamleTdsize-two">
                                                            <asp:Label ID="lblTanentLastName" runat="server" Text="--"></asp:Label>
                                                        </td>
                                                        <td class="customerdetailstamleTd">
                                                            <asp:Literal ID="Literal28" runat="server" Text="<%$ Resources:Resource, EMAIL_ID%>"></asp:Literal>
                                                        </td>
                                                        <td class="customerdetailstamleTd-dot">
                                                            :
                                                        </td>
                                                        <td class="customerdetailstamleTdsize-two">
                                                            <asp:Label ID="lblTanentEmail" runat="server" Text="--"></asp:Label>
                                                        </td>
                                                        <td class="customerdetailstamleTd-two">
                                                            <asp:Literal ID="litTHPhone" runat="server" Text="<%$ Resources:Resource, HOME%>"></asp:Literal>
                                                        </td>
                                                        <td class="customerdetailstamleTd-dot">
                                                            :
                                                        </td>
                                                        <td class="customerdetailstamleTdsize-two">
                                                            <asp:Label ID="lblTanentHomeContactNo" runat="server" Text="--"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="customerdetailstamleTd-two">
                                                            <asp:Literal ID="Literal29" runat="server" Text="<%$ Resources:Resource, ANOTHER_CONTACT_NO%>"></asp:Literal>
                                                        </td>
                                                        <td class="customerdetailstamleTd-dot">
                                                            :
                                                        </td>
                                                        <td class="customerdetailstamleTdsize-two">
                                                            <asp:Label ID="lblTanentOtherContactNo" runat="server" Text="--"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                    </div>
                                    <%-------------------------Tenent Information End----------------------%>
                                    <%-------------------------Postal Information Start----------------------%>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-sec">
                                        <h4 class="subHeading">
                                            <asp:Literal ID="litPostalAddress" runat="server" Text="Postal Address"></asp:Literal>
                                        </h4>
                                        <div class="out-bor_aDetails">
                                            <table class="customerdetailsTamle">
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litPHNo" runat="server" Text="<%$ Resources:Resource, H_NO%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblPostalHouseNo" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litPStreet" runat="server" Text="<%$ Resources:Resource, STREET%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblPostalStreet" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litPLGAVillage" runat="server" Text="<%$ Resources:Resource, CITY%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblPostalVillage" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litPZip" runat="server" Text="<%$ Resources:Resource, ZIP%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblPostalZip" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal30" runat="server" Text="<%$ Resources:Resource, AREA_CODE%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblPostalAreaCode" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <%-------------------------Postal Information End----------------------%>
                                    <table class="customerdetailsTamle" style="width: 50%">
                                        <tr>
                                            <td class="customerdetailstamleTd-two">
                                                <asp:Label ID="Label13" runat="server" Text="Is Same As Postal Address "></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd-dot">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize-two">
                                                <asp:Label ID="lblsSameAsService" runat="server" Text="Yes"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="clr">
                                    </div>
                                    <%-------------------------Service Information Start----------------------%>
                                    <div class="inner-sec">
                                        <h4 class="subHeading">
                                            <asp:Literal ID="litServiceAddress" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Literal>
                                        </h4>
                                        <div class="out-bor_aDetails">
                                            <table class="customerdetailsTamle">
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litSHNo" runat="server" Text="<%$ Resources:Resource, H_NO%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblServiceHouseNo" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd">
                                                        <asp:Literal ID="litSStreet" runat="server" Text="<%$ Resources:Resource, STREET%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblServiceStreet" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litSLGAVillage" runat="server" Text="<%$ Resources:Resource, CITY%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblServiceVillage" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litSZip" runat="server" Text="<%$ Resources:Resource, ZIP%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblServiceZip" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litSArea" runat="server" Text="<%$ Resources:Resource, AREA_CODE%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblServiceAreaCode" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table class="customerdetailsTamle" style="width: 41%">
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal31" runat="server" Text="<%$ Resources:Resource, COMMUNICATION_ADD%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblCommunicationAddress" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <%-------------------------Service Information End----------------------%>
                                        <div class="clr">
                                        </div>
                                        <h4 class="subHeading">
                                            Other Details</h4>
                                        <table class="customerdetailsTamle">
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Label ID="lblIsBedc" runat="server" Text="Is BEDC Employee"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td style="width: 11%;" class="cDfontsize">
                                                    <asp:Label ID="lblIsBedcEmployee" runat="server" Text="No"></asp:Label>
                                                </td>
                                                <td>
                                                    <table>
                                                        <tr id="divEmployeeCode" runat="server" visible="false">
                                                            <td style="width: 3%; color: #1a6807; font-size: 14px; font-weight: bold;">
                                                                <asp:Literal ID="litEmployeCode" runat="server" Text="<%$ Resources:Resource, EMPLOYEE_CODE%>"></asp:Literal>
                                                            </td>
                                                            <td class="customerdetailstamleTd-dot">
                                                                :
                                                            </td>
                                                            <td class="customerdetailstamleTdsize-two">
                                                                <asp:Label ID="lblEmployeeCode" runat="server" Text="--"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="customerdetailstamleTd-two" style="width: 2%;">
                                                    <asp:Label ID="Label14" runat="server" Text="Is Embassy"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td class="cDfontsize">
                                                    <asp:Label ID="lblIsEmbassyCustomer" runat="server" Text="No"></asp:Label>
                                                </td>
                                                <td>
                                                    <table id="tblEmbassyCode" runat="server" visible="false">
                                                        <tr>
                                                            <td style="width: 3%; color: #1a6807; font-size: 14px; font-weight: bold;">
                                                                <asp:Literal ID="litEmbassy" runat="server" Text="<%$ Resources:Resource, EMBASSY_CODE%>"></asp:Literal>
                                                            </td>
                                                            <td class="customerdetailstamleTd-dot">
                                                                :
                                                            </td>
                                                            <td class="customerdetailstamleTdsize-two">
                                                                <asp:Label ID="lblEmbassyCode" runat="server" Text="--"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="customerdetailstamleTd-two">
                                                    <asp:Label ID="Label15" runat="server" Text="Is VIP"></asp:Label>
                                                </td>
                                                <td class="customerdetailstamleTd-dot">
                                                    :
                                                </td>
                                                <td colspan="2" class="cDfontsize">
                                                    <asp:Label ID="lblIsVipCustomer" runat="server" Text="No"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="clr">
                                        </div>
                                    </div>
                                </div>
                                <%----------------------------------------------------------Step 2----------------------------------------- --%>
                                <div class="">
                                    <div class="clr">
                                    </div>
                                    <div class="inner-sec">
                                        <h4 class="subHeading">
                                            Account Information</h4>
                                        <div class="out-bor_aDetails">
                                            <table class="customerdetailsTamle">
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litAccountType" runat="server" Text="<%$ Resources:Resource, ACCOUNT_TYPE%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblAccountType" runat="server" Text="--"></asp:Label>
                                                        <asp:Label ID="lblGovtAccountTypeName" runat="server" Visible="false" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litBook" runat="server" Text="Book Number"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblBookCode" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litPole" runat="server" Text="<%$ Resources:Resource, POLE%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblPoleNo" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litTariff" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblTariff" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litClusterType" runat="server" Text="<%$ Resources:Resource, CLUSTER_TYPE%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblClusterType" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litPhase" runat="server" Text="<%$ Resources:Resource, PHASE%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblPhase" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litRouteNo" runat="server" Text="<%$ Resources:Resource, ROUTE_NO%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblRouteNo" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litInitialBilling" runat="server" Text="<%$ Resources:Resource, INITIAL_BILLING_KWH%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblInitialBillingkWh" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal32" runat="server" Text="<%$ Resources:Resource, READCODE%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblReadCode" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="divReadCustomer" runat="server" visible="false">
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litMeterNoDisplay" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblMeterNo" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Label ID="lblcm" runat="server" Text="Is CAPMI : "></asp:Label>
                                                        <%--<asp:Literal ID="Literal20" runat="server" Text="Present Reading"></asp:Literal>--%>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblIsCapmy" runat="server" Text="No"></asp:Label>
                                                        <%--  <asp:Label ID="lblPresentReading" runat="server"Text="--"></asp:Label>--%>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litAmount" runat="server" Text="<%$ Resources:Resource, AMOUNT%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblMeterAmount" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="divDirectCustomer" runat="server" visible="false">
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal33" runat="server" Text="<%$ Resources:Resource, AVERAGE_READING%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblAverageReading" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="trPresentReading" runat="server" visible="false">
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litCapmiOutstandingAmount" runat="server" Text="CAPMI Outstanding"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblCapmiOutstandingAmount" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="Literal34" runat="server" Text="Initial Meter Reading"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize">
                                                        <asp:Label ID="lblPresentReading" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                    </td>
                                                    <td class="customerdetailstamleTd">
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <%--Popus Govt Account Type Ends--%>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-sec">
                                        <h4 class="subHeading">
                                            Setup Information</h4>
                                        <div class="out-bor_aDetails">
                                            <table class="customerdetailsTamle">
                                                <tr>
                                                    <td class="customerdetailstamleTd">
                                                        <asp:Literal ID="litApplicationDate" runat="server" Text="<%$ Resources:Resource, APPLICATION_DATE%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblApplicationDate" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litConnectionDate" runat="server" Text="<%$ Resources:Resource, CONNECTION_DATE%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblConnectionDate" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litSetupDate" runat="server" Text="<%$ Resources:Resource, SETUP_DATE%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblSetUpDate" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litCertifiedBy" runat="server" Text="<%$ Resources:Resource, CERTIFIED_BY%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblCertifiedBy" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litSeal1" runat="server" Text="<%$ Resources:Resource, SEAL_1%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblSeal1" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litSeal2" runat="server" Text="<%$ Resources:Resource, SEAL_2%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblSeal2" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="customerdetailstamleTd-two">
                                                        <asp:Literal ID="litOldAccountNo" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                                    </td>
                                                    <td class="customerdetailstamleTd-dot">
                                                        :
                                                    </td>
                                                    <td class="customerdetailstamleTdsize-two">
                                                        <asp:Label ID="lblOldAccount" runat="server" Text="--"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <table class="customerdetailsTamle" style="margin-left: 6px; width: 50%;">
                                        <tr>
                                            <td class="customerdetailstamleTd-change">
                                                <asp:Literal ID="litApplicationProcessedBy" runat="server" Text="<%$ Resources:Resource, APPLICATION_PROCCESED_BY%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd-dot">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize-twochange">
                                                <asp:Label ID="lblAppProcessedBy" runat="server" Text="--"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="customerdetailstamleTd-two">
                                                <asp:Literal ID="litInstalledBy" runat="server" Text="<%$ Resources:Resource, INSTALLED_BY%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd-dot">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize-two">
                                                <asp:Label ID="lblInstallByName" runat="server" Text="--"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div class="out-bor" id="divIdentityInfo" runat="server" visible="false">
                                <div class="clr">
                                </div>
                                <div class="inner-sec">
                                    <h4 class="subHeading">
                                        Identity Information</h4>
                                    <div class="out-bor_aDetails">
                                        <asp:Repeater ID="rptrIdentityDetails" runat="server">
                                            <ItemTemplate>
                                                <table class="customerdetailsTamle">
                                                    <tr>
                                                        <td class="customerdetailstamleTd-two">
                                                            <asp:Literal ID="litIdentityType" runat="server" Text="Type"></asp:Literal>
                                                        </td>
                                                        <td class="customerdetailstamleTd-dot">
                                                            :
                                                        </td>
                                                        <td class="customerdetailstamleTdsize-two">
                                                            <%#Eval("IdentityTypeIdList")%>
                                                        </td>
                                                        <td class="customerdetailstamleTd-two">
                                                            <asp:Literal ID="litIdentityNo" runat="server" Text="Number"></asp:Literal>
                                                        </td>
                                                        <td class="customerdetailstamleTd-dot">
                                                            :
                                                        </td>
                                                        <td class="customerdetailstamleTdsize-two">
                                                            <%#Eval("IdentityNumberList")%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div class="out-bor" id="divUploadsList" runat="server" visible="false">
                                <div class="clr">
                                </div>
                                <div class="inner-sec">
                                    <h4 class="subHeading">
                                        Uploads</h4>
                                    <div class="out-bor_aDetails">
                                        <asp:Repeater ID="rptrGetCustomerDocuments" runat="server">
                                            <ItemTemplate>
                                                <table class="customerdetailsTamle">
                                                    <tr>
                                                        <td class="customerdetailstamleTd-two">
                                                            <asp:Literal ID="Literal22" runat="server" Text="Document"></asp:Literal>
                                                        </td>
                                                        <td class="customerdetailstamleTd-dot">
                                                            :
                                                        </td>
                                                        <td class="customerdetailstamleTdsize-two">
                                                            <%#Eval("DocumentName")%>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <%-- <%#Eval("Path")%>--%>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div class="out-bor" id="divUDFList" runat="server" visible="false">
                                <div class="clr">
                                </div>
                                <div class="inner-sec">
                                    <h4 class="subHeading">
                                        User Defined Fields</h4>
                                    <div class="out-bor_aDetails">
                                        <asp:Repeater ID="rptrGetCustomerUDFValues" runat="server">
                                            <ItemTemplate>
                                                <table class="customerdetailsTamle">
                                                    <tr>
                                                        <td class="customerdetailstamleTd-two">
                                                            <asp:Literal ID="Literal24" runat="server" Text='<%#Eval("UDFTypeIdList")%>'></asp:Literal>
                                                        </td>
                                                        <td class="customerdetailstamleTd-dot">
                                                            :
                                                        </td>
                                                        <td class="customerdetailstamleTdsize-two">
                                                            <%#Eval("UDFValueList")%>
                                                        </td>
                                                        <%--   <td class="customerdetailstamleTd-two">
                                            <asp:Literal ID="Literal25" runat="server" Text="Ref Doc"></asp:Literal>
                                        </td>
                                        <td class="customerdetailstamleTd-dot">
                                            :
                                        </td>
                                        <td class="customerdetailstamleTdsize-two">
                                            <%#Eval("UDFTypeIdList")%>
                                        </td>--%>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="PanelAlert"
            TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btnAlertOk">
        </asp:ModalPopupExtender>
        <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
        <asp:Panel ID="PanelAlert" runat="server" CssClass="modalPopup" Style="display: none;">
            <div class="popheader popheaderlblred">
                <asp:Label ID="lblAlertMsg" runat="server"></asp:Label>
            </div>
            <div class="footer popfooterbtnrt">
                <div class="fltr popfooterbtn">
                    <asp:Button ID="btnAlertOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="ok_btn" />
                </div>
                <div class="clear pad_5">
                </div>
            </div>
        </asp:Panel>
    </div>
    <asp:HiddenField ID="hfARID" runat="server" />
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/AuditTrayReport.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
    </script>
    <script type="text/javascript">
        function pageLoad() {
            Calendar('<%=txtFromDate.ClientID %>', '<%=imgFromDate.ClientID %>', "-2:+8");
            Calendar('<%=txtToDate.ClientID %>', '<%=imgToDate.ClientID %>', "-2:+8");
            DisplayMessage('<%= pnlMessage.ClientID %>');
        };       
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
