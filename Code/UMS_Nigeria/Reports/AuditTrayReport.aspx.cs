﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for Audit Tray Report
                     
 Developer        : Id027-Karthik Thati
 Creation Date    : 04-10-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Threading;
using System.Globalization;
using System.Data;
using System.Xml;

namespace UMS_Nigeria.Reports
{
    public partial class AuditTrayReport : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        MastersBAL objMastersBAL = new MastersBAL();
        ConsumerBal _objConsumerBal = new ConsumerBal();
        ReportsBal objReportsBal = new ReportsBal();
        string Key = UMS_Resource.AUDIT_TRAY_REPORT_ERROR_KEY;
        #endregion

        #region Properties
        public string FromDate
        {
            get { return string.IsNullOrEmpty(txtFromDate.Text) ? string.Empty : _objCommonMethods.Convert_MMDDYY(txtFromDate.Text); }
        }
        public string ToDate
        {
            get { return string.IsNullOrEmpty(txtToDate.Text) ? string.Empty : _objCommonMethods.Convert_MMDDYY(txtToDate.Text); }
        }

        #region Properties
        public string BU
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblBusinessUnits, ","); }
        }
        public string SU
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceUnits, ","); }
        }
        public string SC
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceCenters, ","); }
        }
        public string Cycle
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblCycles, ","); }
        }
        public string BookNo
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblBooks, ","); }
        }
        public string Tariff
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblTariff, ","); }
        }
        public string BUName
        {
            get { return _objCommonMethods.CollectSelectedItemsText(cblBusinessUnits, ","); }
        }
        public string SUName
        {
            get { return _objCommonMethods.CollectSelectedItemsText(cblServiceUnits, ","); }
        }
        public string SCName
        {
            get { return _objCommonMethods.CollectSelectedItemsText(cblServiceCenters, ","); }
        }
        public string CycleName
        {
            get { return _objCommonMethods.CollectSelectedItemsText(cblCycles, ","); }
        }
        public string BookName
        {
            get { return _objCommonMethods.CollectSelectedItemsText(cblBooks, ","); }
        }
        public string TariffName
        {
            get { return _objCommonMethods.CollectSelectedItemsText(cblTariff, ","); }
        }
        #endregion
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                if (!IsPostBack)
                {
                    string path = string.Empty;
                    path = _objCommonMethods.GetPagePath(this.Request.Url);
                    if (_objCommonMethods.IsAccess(path))
                    {
                        CheckBoxesJS();
                        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        {
                            ListItem LiBU = new ListItem(Session[UMS_Resource.SESSION_USER_BUNAME].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());
                            cblBusinessUnits.Items.Add(LiBU);
                            ChkBUAll.Checked = cblBusinessUnits.Items[0].Selected = true;
                            ChkBUAll.Enabled = cblBusinessUnits.Enabled = false;
                            divServiceUnits.Visible = true;
                            _objCommonMethods.BindUserServiceUnits_BuIds(cblServiceUnits, BU, Resource.DDL_ALL, false);
                        }
                        else
                            _objCommonMethods.BindBusinessUnits(cblBusinessUnits, string.Empty, false);

                        BindGrids();
                    }
                    else
                    {
                        Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    }
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }

        private void CheckBoxesJS()
        {
            ChkBUAll.Attributes.Add("onclick", "CheckAll('" + ChkBUAll.ClientID + "','" + cblBusinessUnits.ClientID + "')");
            ChkSUAll.Attributes.Add("onclick", "CheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            chkSCAll.Attributes.Add("onclick", "CheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            chkCycleAll.Attributes.Add("onclick", "CheckAll('" + chkCycleAll.ClientID + "','" + cblCycles.ClientID + "')");
            chkBooksAll.Attributes.Add("onclick", "CheckAll('" + chkBooksAll.ClientID + "','" + cblBooks.ClientID + "')");
            chkTariffAll.Attributes.Add("onclick", "CheckAll('" + chkTariffAll.ClientID + "','" + cblTariff.ClientID + "')");
            //chkUserAll.Attributes.Add("onclick", "CheckAll('" + chkUserAll.ClientID + "','" + cblUser.ClientID + "')");

            cblBusinessUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkBUAll.ClientID + "','" + cblBusinessUnits.ClientID + "')");
            cblServiceUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            cblServiceCenters.Attributes.Add("onclick", "UnCheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            cblCycles.Attributes.Add("onclick", "UnCheckAll('" + chkCycleAll.ClientID + "','" + cblCycles.ClientID + "')");
            cblBooks.Attributes.Add("onclick", "UnCheckAll('" + chkBooksAll.ClientID + "','" + cblBooks.ClientID + "')");
            cblTariff.Attributes.Add("onclick", "UnCheckAll('" + chkTariffAll.ClientID + "','" + cblTariff.ClientID + "')");
            //cblUser.Attributes.Add("onclick", "UnCheckAll('" + chkUserAll.ClientID + "','" + cblUser.ClientID + "')");
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrids();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lbtnBookNoShowAll_Click(object sender, EventArgs e)
        {
            string Query = BuildQuery();
            Response.Redirect(Constants.BookNoChangeLogsPage + Query, false);
            //Response.Redirect(Constants.BookNoChangeLogsPage, false);
        }

        protected void lbtnTariffShowAll_Click(object sender, EventArgs e)
        {
            string Query = BuildQuery();
            Response.Redirect(Constants.TariffChangeLogsPage + Query, false);
            //Response.Redirect(Constants.TariffChangeLogsPage, false);
        }

        protected void lbtnCustomerShowAll_Click(object sender, EventArgs e)
        {
            string Query = BuildQuery();
            Response.Redirect(Constants.CustomerChangeLogsPage + Query, false);
            //Response.Redirect(Constants.CustomerChangeLogsPage, false);
        }

        protected void lbtnMeterShowAll_Click(object sender, EventArgs e)
        {
            string Query = BuildQuery();
            Response.Redirect(Constants.MeterChangeLogsPage + Query, false);
            //Response.Redirect(Constants.MeterChangeLogsPage, false);
        }

        protected void lbtnAddressShowAll_Click(object sender, EventArgs e)
        {
            string Query = BuildQuery();
            Response.Redirect(Constants.CustomerAddressChangeLogsPage + Query, false);
            // Response.Redirect(Constants.CustomerAddressChangeLogsPage, false);
        }

        protected void lbtnNameShowAll_Click(object sender, EventArgs e)
        {
            string Query = BuildQuery();
            Response.Redirect(Constants.CustomerNameChangeLogsPage + Query, false);
            //Response.Redirect(Constants.CustomerNameChangeLogsPage, false);
        }

        protected void lbtnCustTypeShowAll_Click(object sender, EventArgs e)
        {
            string Query = BuildQuery();
            Response.Redirect(Constants.CustomerTypeChangeLogsPage + Query, false);
            //Response.Redirect(Constants.CustomerNameChangeLogsPage, false);
        }

        protected void lbtnReadToDirectShowAll_Click(object sender, EventArgs e)
        {
            string Query = BuildQuery();
            Response.Redirect(Constants.ReadToDirectChangeLogsPage + Query, false);
            //Response.Redirect(Constants.CustomerNameChangeLogsPage, false);
        }

        protected void lbtnAssignMeterShowAll_Click(object sender, EventArgs e)
        {
            string Query = BuildQuery();
            Response.Redirect(Constants.AssignedMeterCustomersLogsPage + Query, false);
            //Response.Redirect(Constants.CustomerNameChangeLogsPage, false);
        }
        protected void lbtnPresentMeterReadingShowAll_Click(object sender, EventArgs e)
        {
            string Query = BuildQuery();
            Response.Redirect(Constants.PresentMeterReadingChangeLogsPage + Query, false);
            //Response.Redirect(Constants.CustomerNameChangeLogsPage, false);
        }

        protected void lnkgvCustomerRegistrationDetails_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfARID.Value = lnkApprove.CommandArgument;
                GetCustomerDetail();
                mpeCustomerDetails.Show();
            }
            catch (Exception ex)
            {
                _objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lbtnCustomerRegistrationShowAll_Click(object sender, EventArgs e)
        {
            string Query = BuildQuery();
            Response.Redirect(Constants.CustomerRegistrationApprovalLog + Query, false);
            //Response.Redirect(Constants.CustomerNameChangeLogsPage, false);
        }
        #endregion

        #region CheckBoxEvents
        protected void cblBusinessUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                cblBooks.Items.Clear();
                cblServiceCenters.Items.Clear();
                cblServiceUnits.Items.Clear();
                cblTariff.Items.Clear();
                //cblUser.Items.Clear();
                //chkUserAll.Checked = 
                chkTariffAll.Checked = chkCycleAll.Checked = chkBooksAll.Checked = chkSCAll.Checked = ChkSUAll.Checked = ChkBUAll.Checked = false;
                // divUser.Visible = 
                divtariff.Visible = divServiceUnits.Visible = divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = chkSCAll.Checked = divServiceCenters.Visible = false;
                if (!string.IsNullOrEmpty(BU))
                {
                    _objCommonMethods.BindUserServiceUnits_BuIds(cblServiceUnits, BU, Resource.DDL_ALL, false);
                    if (cblServiceUnits.Items.Count > 0)
                        divServiceUnits.Visible = true;
                    else
                        divServiceUnits.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                cblBooks.Items.Clear();
                cblServiceCenters.Items.Clear();
                cblTariff.Items.Clear();
                //cblUser.Items.Clear();
                // chkUserAll.Checked = divUser.Visible = 
                chkTariffAll.Checked = divtariff.Visible = divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = chkSCAll.Checked = divServiceCenters.Visible = false;
                if (!string.IsNullOrEmpty(SU))
                {
                    _objCommonMethods.BindUserServiceCenters_SuIds(cblServiceCenters, SU, Resource.DDL_ALL, false);
                    if (cblServiceCenters.Items.Count > 0)
                        divServiceCenters.Visible = true;
                    else
                        divServiceCenters.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceCenters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                cblBooks.Items.Clear();
                cblTariff.Items.Clear();
                //cblUser.Items.Clear();
                //chkUserAll.Checked = divUser.Visible = 
                chkTariffAll.Checked = divtariff.Visible = divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = false;
                if (!string.IsNullOrEmpty(SC))
                {
                    _objCommonMethods.BindCyclesByBUSUSC(cblCycles, BU, SU, SC, true, string.Empty, false);
                    if (cblCycles.Items.Count > 0)
                        divCycles.Visible = true;
                    else
                        divCycles.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblCycles_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblBooks.Items.Clear();
                cblTariff.Items.Clear();
                //cblUser.Items.Clear();
                //chkUserAll.Checked = divUser.Visible = 
                chkTariffAll.Checked = divtariff.Visible = chkBooksAll.Checked = divBookNos.Visible = false;
                if (!string.IsNullOrEmpty(Cycle))
                {
                    _objCommonMethods.BindAllBookNumbers(cblBooks, Cycle, true, false, string.Empty);
                    if (cblBooks.Items.Count > 0)
                        divBookNos.Visible = true;
                    else
                        divBookNos.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblBooks_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblTariff.Items.Clear();
                //cblUser.Items.Clear();
                //chkUserAll.Checked = divUser.Visible = 
                chkTariffAll.Checked = divtariff.Visible = false;
                if (!string.IsNullOrEmpty(BookNo))
                {
                    _objCommonMethods.BindTariffSubTypes(cblTariff, 0, false);
                    if (cblTariff.Items.Count > 0)
                        divtariff.Visible = true;
                    else
                        divtariff.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //protected void cblTariff_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        cblUser.Items.Clear();
        //        chkUserAll.Checked = divUser.Visible = false;
        //        if (!string.IsNullOrEmpty(BookNo))
        //        {
        //            _objCommonMethods.BindEmployeesList(cblUser, false);
        //            if (cblTariff.Items.Count > 0)
        //                divUser.Visible = true;
        //            else
        //                divUser.Visible = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        #endregion

        #region Methods

        private void BindGrids()
        {
            lblBookNoChangesCount.Text = lblTariffChangesCount.Text = lblCustomerChanges.Text = lblMeterChangesCount.Text
                = lblAddressChangesCount.Text = lblNameChangesCount.Text = lblCustTypeCount.Text = lblReadToDirectCount.Text
                = lblAssignedMeterCount.Text = lblPreserntMeterReading.Text = "0";
            BindBookNoChanges();
            BindTariffChanges();
            BindCustomerChanges();
            BindMeterChanges();
            BindCustomerTypeChanges();
            BindNameChanges();
            BindAddressChanges();
            BindReadToDirect();
            BindAssignedMeters();
            BindPresentMeterReadingAdjustment();
            BindCustomerRegistrationApprovals();
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void BindBookNoChanges()
        {
            try
            {
                RptAuditTrayListBe objRptAuditTrayList = new RptAuditTrayListBe();
                RptAuditTrayBe objRptAuditTrayBe = new RptAuditTrayBe();
                objRptAuditTrayBe.BU_ID = BU;
                objRptAuditTrayBe.SU_ID = SU;
                objRptAuditTrayBe.SC_ID = SC;
                objRptAuditTrayBe.CycleId = Cycle;
                objRptAuditTrayBe.ClassId = Tariff;
                objRptAuditTrayBe.NewBookNo = BookNo;
                objRptAuditTrayBe.FromDate = FromDate;
                objRptAuditTrayBe.ToDate = ToDate;
                objRptAuditTrayList = _ObjiDsignBAL.DeserializeFromXml<RptAuditTrayListBe>(objReportsBal.GetList(objRptAuditTrayBe, ReturnType.List));
                if (objRptAuditTrayList.items.Count > 0)
                {
                    lblBookNoChangesCount.Text = objRptAuditTrayList.items[0].TotalChanges.ToString();
                    if (objRptAuditTrayList.items[0].TotalChanges > 0)
                        lbtnBookNoShowAll.Visible = true;
                }
                else
                    lbtnBookNoShowAll.Visible = false;
                gvBookNoChanges.DataSource = objRptAuditTrayList.items;
                gvBookNoChanges.DataBind();
                MergeRows(gvBookNoChanges);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindTariffChanges()
        {
            try
            {
                RptAuditTrayListBe objRptAuditTrayList = new RptAuditTrayListBe();
                RptAuditTrayBe objRptAuditTrayBe = new RptAuditTrayBe();
                objRptAuditTrayBe.BU_ID = BU;
                objRptAuditTrayBe.SU_ID = SU;
                objRptAuditTrayBe.SC_ID = SC;
                objRptAuditTrayBe.CycleId = Cycle;
                objRptAuditTrayBe.ClassId = Tariff;
                objRptAuditTrayBe.NewBookNo = BookNo;
                objRptAuditTrayBe.FromDate = FromDate;
                objRptAuditTrayBe.ToDate = ToDate;
                objRptAuditTrayList = _ObjiDsignBAL.DeserializeFromXml<RptAuditTrayListBe>(objReportsBal.GetList(objRptAuditTrayBe, ReturnType.Fetch));
                if (objRptAuditTrayList.items.Count > 0)
                {
                    lblTariffChangesCount.Text = objRptAuditTrayList.items[0].TotalChanges.ToString();
                    if (objRptAuditTrayList.items[0].TotalChanges > 0)
                        lbtnTariffShowAll.Visible = true;
                }
                else
                    lbtnTariffShowAll.Visible = false;
                gvTariffChanges.DataSource = objRptAuditTrayList.items;
                gvTariffChanges.DataBind();
                MergeRows(gvTariffChanges);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindCustomerChanges()
        {
            try
            {
                RptAuditTrayListBe objRptAuditTrayList = new RptAuditTrayListBe();
                RptAuditTrayBe objRptAuditTrayBe = new RptAuditTrayBe();
                objRptAuditTrayBe.BU_ID = BU;
                objRptAuditTrayBe.SU_ID = SU;
                objRptAuditTrayBe.SC_ID = SC;
                objRptAuditTrayBe.CycleId = Cycle;
                objRptAuditTrayBe.ClassId = Tariff;
                objRptAuditTrayBe.NewBookNo = BookNo;
                objRptAuditTrayBe.FromDate = FromDate;
                objRptAuditTrayBe.ToDate = ToDate;
                objRptAuditTrayList = _ObjiDsignBAL.DeserializeFromXml<RptAuditTrayListBe>(objReportsBal.Get(objRptAuditTrayBe, ReturnType.Retrieve));
                if (objRptAuditTrayList.items.Count > 0)
                {
                    lblCustomerChanges.Text = objRptAuditTrayList.items[0].TotalChanges.ToString();
                    if (objRptAuditTrayList.items[0].TotalChanges > 0)
                        lbtnCustomerShowAll.Visible = true;
                }
                else
                    lbtnCustomerShowAll.Visible = false;
                gvCustomerChanges.DataSource = objRptAuditTrayList.items;
                gvCustomerChanges.DataBind();
                MergeRows(gvCustomerChanges);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindMeterChanges()
        {
            try
            {
                RptAuditTrayListBe objRptAuditTrayList = new RptAuditTrayListBe();
                RptAuditTrayBe objRptAuditTrayBe = new RptAuditTrayBe();
                objRptAuditTrayBe.BU_ID = BU;
                objRptAuditTrayBe.SU_ID = SU;
                objRptAuditTrayBe.SC_ID = SC;
                objRptAuditTrayBe.CycleId = Cycle;
                objRptAuditTrayBe.ClassId = Tariff;
                objRptAuditTrayBe.NewBookNo = BookNo;
                objRptAuditTrayBe.FromDate = FromDate;
                objRptAuditTrayBe.ToDate = ToDate;
                objRptAuditTrayList = _ObjiDsignBAL.DeserializeFromXml<RptAuditTrayListBe>(objReportsBal.Get(objRptAuditTrayBe, ReturnType.Double));
                if (objRptAuditTrayList.items.Count > 0)
                {
                    lblMeterChangesCount.Text = objRptAuditTrayList.items[0].TotalChanges.ToString();
                    if (objRptAuditTrayList.items[0].TotalChanges > 0)
                        lbtnMeterShowAll.Visible = true;
                }
                else
                    lbtnMeterShowAll.Visible = false;
                gvMeterChanges.DataSource = objRptAuditTrayList.items;
                gvMeterChanges.DataBind();
                MergeRows(gvMeterChanges);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindCustomerTypeChanges()
        {
            try
            {
                RptAuditTrayListBe objRptAuditTrayList = new RptAuditTrayListBe();
                RptAuditTrayBe objRptAuditTrayBe = new RptAuditTrayBe();
                objRptAuditTrayBe.BU_ID = BU;
                objRptAuditTrayBe.SU_ID = SU;
                objRptAuditTrayBe.SC_ID = SC;
                objRptAuditTrayBe.CycleId = Cycle;
                objRptAuditTrayBe.ClassId = Tariff;
                objRptAuditTrayBe.NewBookNo = BookNo;
                objRptAuditTrayBe.FromDate = FromDate;
                objRptAuditTrayBe.ToDate = ToDate;
                objRptAuditTrayList = _ObjiDsignBAL.DeserializeFromXml<RptAuditTrayListBe>(objReportsBal.GetList(objRptAuditTrayBe, ReturnType.User));
                if (objRptAuditTrayList.items.Count > 0)
                {
                    lblCustTypeCount.Text = objRptAuditTrayList.items[0].TotalChanges.ToString();
                    if (objRptAuditTrayList.items[0].TotalChanges > 0)
                        lbtnCustTypeShowAll.Visible = true;
                }
                else
                    lbtnCustTypeShowAll.Visible = false;
                gvCustomerType.DataSource = objRptAuditTrayList.items;
                gvCustomerType.DataBind();
                MergeRows(gvCustomerType);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindNameChanges()
        {
            try
            {
                RptAuditTrayListBe objRptAuditTrayList = new RptAuditTrayListBe();
                RptAuditTrayBe objRptAuditTrayBe = new RptAuditTrayBe();
                objRptAuditTrayBe.BU_ID = BU;
                objRptAuditTrayBe.SU_ID = SU;
                objRptAuditTrayBe.SC_ID = SC;
                objRptAuditTrayBe.CycleId = Cycle;
                objRptAuditTrayBe.ClassId = Tariff;
                objRptAuditTrayBe.NewBookNo = BookNo;
                objRptAuditTrayBe.FromDate = FromDate;
                objRptAuditTrayBe.ToDate = ToDate;
                objRptAuditTrayList = _ObjiDsignBAL.DeserializeFromXml<RptAuditTrayListBe>(objReportsBal.GetList(objRptAuditTrayBe, ReturnType.Check));
                if (objRptAuditTrayList.items.Count > 0)
                {
                    lblNameChangesCount.Text = objRptAuditTrayList.items[0].TotalChanges.ToString();
                    if (objRptAuditTrayList.items[0].TotalChanges > 0)
                        lbtnNameShowAll.Visible = true;
                }
                else
                    lbtnNameShowAll.Visible = false;
                gvNameChanges.DataSource = objRptAuditTrayList.items;
                gvNameChanges.DataBind();
                MergeRows(gvNameChanges);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindReadToDirect()
        {
            try
            {
                RptAuditTrayListBe objRptAuditTrayList = new RptAuditTrayListBe();
                RptAuditTrayBe objRptAuditTrayBe = new RptAuditTrayBe();
                objRptAuditTrayBe.BU_ID = BU;
                objRptAuditTrayBe.SU_ID = SU;
                objRptAuditTrayBe.SC_ID = SC;
                objRptAuditTrayBe.CycleId = Cycle;
                objRptAuditTrayBe.ClassId = Tariff;
                objRptAuditTrayBe.NewBookNo = BookNo;
                objRptAuditTrayBe.FromDate = FromDate;
                objRptAuditTrayBe.ToDate = ToDate;
                objRptAuditTrayList = _ObjiDsignBAL.DeserializeFromXml<RptAuditTrayListBe>(objReportsBal.GetList(objRptAuditTrayBe, ReturnType.Retrieve));
                if (objRptAuditTrayList.items.Count > 0)
                {
                    lblReadToDirectCount.Text = objRptAuditTrayList.items[0].TotalChanges.ToString();
                    if (objRptAuditTrayList.items[0].TotalChanges > 0)
                        lbtnReadToDirectShowAll.Visible = true;
                }
                else
                    lbtnReadToDirectShowAll.Visible = false;
                gvReadToDirect.DataSource = objRptAuditTrayList.items;
                gvReadToDirect.DataBind();
                MergeRows(gvReadToDirect);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindAssignedMeters()
        {
            try
            {
                RptAuditTrayListBe objRptAuditTrayList = new RptAuditTrayListBe();
                RptAuditTrayBe objRptAuditTrayBe = new RptAuditTrayBe();
                objRptAuditTrayBe.BU_ID = BU;
                objRptAuditTrayBe.SU_ID = SU;
                objRptAuditTrayBe.SC_ID = SC;
                objRptAuditTrayBe.CycleId = Cycle;
                objRptAuditTrayBe.ClassId = Tariff;
                objRptAuditTrayBe.NewBookNo = BookNo;
                objRptAuditTrayBe.FromDate = FromDate;
                objRptAuditTrayBe.ToDate = ToDate;
                objRptAuditTrayList = _ObjiDsignBAL.DeserializeFromXml<RptAuditTrayListBe>(objReportsBal.GetList(objRptAuditTrayBe, ReturnType.Single));
                if (objRptAuditTrayList.items.Count > 0)
                {
                    lblAssignedMeterCount.Text = objRptAuditTrayList.items[0].TotalChanges.ToString();
                    if (objRptAuditTrayList.items[0].TotalChanges > 0)
                        lbtnAssignMeterShowAll.Visible = true;

                    gvAssignedMeter.DataSource = objRptAuditTrayList.items;
                    gvAssignedMeter.DataBind();
                    //divNoDataFound.Visible = false;
                    //headers.Visible = true;
                }
                else
                {
                    //divNoDataFound.Visible = true;
                    //headers.Visible = lbtnAssignMeterShowAll.Visible = false;
                    lbtnAssignMeterShowAll.Visible = false;
                    gvAssignedMeter.DataSource = new DataTable();
                    gvAssignedMeter.DataBind();
                }

                //MergeRows(gvAssignedMeter);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindAddressChanges()
        {
            try
            {
                RptAuditAddressListBe objRptList = new RptAuditAddressListBe();
                RptAuditAddressBe objRptBe = new RptAuditAddressBe();
                objRptBe.BU_ID = BU;
                objRptBe.SU_ID = SU;
                objRptBe.SC_ID = SC;
                objRptBe.CycleId = Cycle;
                objRptBe.ClassId = Tariff;
                objRptBe.NewBookNo = BookNo;
                objRptBe.FromDate = FromDate;
                objRptBe.ToDate = ToDate;
                objRptList = _ObjiDsignBAL.DeserializeFromXml<RptAuditAddressListBe>(objReportsBal.GetAddressList(objRptBe, ReturnType.List));
                if (objRptList.items.Count > 0)
                {
                    lblAddressChangesCount.Text = objRptList.items[0].TotalChanges.ToString();
                    if (objRptList.items[0].TotalChanges > 0)
                        lbtnAddressShowAll.Visible = true;
                }
                else
                    lbtnAddressShowAll.Visible = false;
                gvAddressChanges.DataSource = objRptList.items;
                gvAddressChanges.DataBind();
                MergeRows(gvAddressChanges);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPresentMeterReadingAdjustment()
        {
            try
            {
                RptAuditTrayListBe objRptAuditTrayList = new RptAuditTrayListBe();
                RptAuditTrayBe objRptAuditTrayBe = new RptAuditTrayBe();
                objRptAuditTrayBe.BU_ID = BU;
                objRptAuditTrayBe.SU_ID = SU;
                objRptAuditTrayBe.SC_ID = SC;
                objRptAuditTrayBe.CycleId = Cycle;
                objRptAuditTrayBe.ClassId = Tariff;
                objRptAuditTrayBe.NewBookNo = BookNo;
                objRptAuditTrayBe.FromDate = FromDate;
                objRptAuditTrayBe.ToDate = ToDate;
                objRptAuditTrayList = _ObjiDsignBAL.DeserializeFromXml<RptAuditTrayListBe>(objReportsBal.GetList(objRptAuditTrayBe, ReturnType.Double));
                if (objRptAuditTrayList.items.Count > 0)
                {
                    lblPreserntMeterReading.Text = objRptAuditTrayList.items[0].TotalChanges.ToString();
                    if (objRptAuditTrayList.items[0].TotalChanges > 0)
                        lbtnPresentMeterReading.Visible = true;
                }
                else
                    lbtnPresentMeterReading.Visible = false;
                gvPresentMeterReading.DataSource = objRptAuditTrayList.items;
                gvPresentMeterReading.DataBind();
                MergeRows(gvPresentMeterReading);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindCustomerRegistrationApprovals()
        {
            try
            {
                RptAuditTrayListBe objRptAuditTrayList = new RptAuditTrayListBe();
                RptAuditTrayBe objRptAuditTrayBe = new RptAuditTrayBe();
                objRptAuditTrayBe.BU_ID = BU;
                objRptAuditTrayBe.SU_ID = SU;
                objRptAuditTrayBe.SC_ID = SC;
                objRptAuditTrayBe.CycleId = Cycle;
                objRptAuditTrayBe.ClassId = Tariff;
                objRptAuditTrayBe.NewBookNo = BookNo;
                objRptAuditTrayBe.FromDate = FromDate;
                objRptAuditTrayBe.ToDate = ToDate;
                objRptAuditTrayBe.PageNo = 1;
                objRptAuditTrayBe.PageSize = 10;
                DataSet ds = objReportsBal.GetApprovalRegistrationsAuditRay(objRptAuditTrayBe);

                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables.Count > 1) { lblTotalApproval.Text = ds.Tables[1].Rows[0]["Count"].ToString(); lnkCustomerRegistrationShowAll.Visible = true; }
                            else { lblTotalApproval.Text = "N/A"; lnkCustomerRegistrationShowAll.Visible = false; }
                            gvCustomerRegistration.DataSource = ds.Tables[0];
                            gvCustomerRegistration.DataBind();
                            //MergeRows(gvAdjustment);
                        }
                        else
                        {
                            gvCustomerRegistration.DataSource = new DataTable();
                            gvCustomerRegistration.DataBind();
                            lblTotalApproval.Text = "0"; lnkCustomerRegistrationShowAll.Visible = false;
                        }
                    }
                    else
                    {
                        gvCustomerRegistration.DataSource = new DataTable();
                        gvCustomerRegistration.DataBind();
                        lblTotalApproval.Text = "0"; lnkCustomerRegistrationShowAll.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        public static void MergeRows(GridView gridView)
        {
            for (int rowIndex = gridView.Rows.Count - 2; rowIndex >= 0; rowIndex--)
            {
                GridViewRow row = gridView.Rows[rowIndex];
                GridViewRow previousRow = gridView.Rows[rowIndex + 1];

                Label lblAccountNo = (Label)row.FindControl("lblAccount");
                Label lblPreviousAccountNo = (Label)previousRow.FindControl("lblAccount");
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    if (i == 0 || i == 1 || i == 2)
                    {
                        if (lblAccountNo.Text == lblPreviousAccountNo.Text)
                        {
                            row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                            previousRow.Cells[i].Visible = false;
                        }
                    }
                }
            }
        }

        private string BuildQuery()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("BU_ID", typeof(string));
            dt.Columns.Add("SU_ID", typeof(string));
            dt.Columns.Add("SC_ID", typeof(string));
            dt.Columns.Add("CYCLE_ID", typeof(string));
            dt.Columns.Add("BOOK_ID", typeof(string));
            dt.Columns.Add("TARIFF_ID", typeof(string));
            dt.Columns.Add("BUName", typeof(string));
            dt.Columns.Add("SUName", typeof(string));
            dt.Columns.Add("SCName", typeof(string));
            dt.Columns.Add("CycleName", typeof(string));
            dt.Columns.Add("BookName", typeof(string));
            dt.Columns.Add("TariffName", typeof(string));
            dt.Rows.Add(BU, SU, SC, Cycle, BookNo, Tariff, BUName, SUName, SCName, CycleName, BookName, TariffName);
            Session[UMS_Resource.AUDIT_SESSION_DT] = dt;

            string Query = "?" + UMS_Resource.FROM_PAGE + "=" + _ObjiDsignBAL.Encrypt(txtFromDate.Text) +
                           "&To=" + _ObjiDsignBAL.Encrypt(txtToDate.Text);
            return Query;
        }
        public void GetCustomerDetail()
        {
            try
            {
                CustomerRegistrationBE objCustomerRegistrationBE = new CustomerRegistrationBE();
                XmlDocument xml = new XmlDocument();
                ConsumerBal objConsumerBal = new ConsumerBal();


                ChangeCustomerTypeBE objChangeCustomerTypeBE = new ChangeCustomerTypeBE();
                objChangeCustomerTypeBE.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                objChangeCustomerTypeBE.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objChangeCustomerTypeBE.FunctionId = (int)EnumApprovalFunctions.CustomerRegistration;

                objChangeCustomerTypeBE.ARID = Convert.ToInt32(hfARID.Value);
                xml = objConsumerBal.GetCustomerDetailsByApprovalID(objChangeCustomerTypeBE);
                if (xml != null)
                {

                    objCustomerRegistrationBE = _ObjiDsignBAL.DeserializeFromXml<CustomerRegistrationBE>(xml);
                    lblGlobalAccountNo.Text = objCustomerRegistrationBE.GlobalAccountNumber;
                    lblBuDsp.Text = objCustomerRegistrationBE.BusinessUnit;
                    lblSUDsp.Text = objCustomerRegistrationBE.ServiceUnitName;
                    lblSCDsp.Text = objCustomerRegistrationBE.ServiceCenterName;
                    lblBookGroupDsp.Text = objCustomerRegistrationBE.CycleName;
                    lblBookCodeDsp.Text = objCustomerRegistrationBE.BookCode;
                    #region Postal AddressDetails

                    #region Postal
                    lblServiceHouseNo.Text = lblPostalHouseNo.Text = objCustomerRegistrationBE.HouseNoPostal;
                    lblServiceStreet.Text = lblPostalStreet.Text = objCustomerRegistrationBE.StreetPostal;
                    lblServiceVillage.Text = lblPostalVillage.Text = objCustomerRegistrationBE.CityPostaL;
                    lblServiceZip.Text = lblPostalZip.Text = objCustomerRegistrationBE.PZipCode;

                    if (objCustomerRegistrationBE.AreaPostal != "0" && !string.IsNullOrEmpty(objCustomerRegistrationBE.AreaPostal)) lblPostalAreaCode.Text = objCustomerRegistrationBE.AreaPostal;
                    if (objCustomerRegistrationBE.AreaService != "0" && !string.IsNullOrEmpty(objCustomerRegistrationBE.AreaService)) lblServiceAreaCode.Text = objCustomerRegistrationBE.AreaService;
                    if (objCustomerRegistrationBE.IsSameAsService) lblServiceAreaCode.Text = lblPostalAreaCode.Text = objCustomerRegistrationBE.AreaPostal;
                    if (objCustomerRegistrationBE.IsCommunicationPostal) lblCommunicationAddress.Text = "Postal";
                    #endregion

                    #region Service
                    if (objCustomerRegistrationBE.ServiceAddressID > 0)
                    {
                        if (objCustomerRegistrationBE.IsCommunicationService) lblCommunicationAddress.Text = "Service";
                        lblServiceHouseNo.Text = objCustomerRegistrationBE.HouseNoService;
                        lblServiceStreet.Text = objCustomerRegistrationBE.StreetService;
                        lblServiceVillage.Text = objCustomerRegistrationBE.CityService;
                        lblServiceZip.Text = objCustomerRegistrationBE.SZipCode;
                        lblServiceAreaCode.Text = objCustomerRegistrationBE.AreaService;
                        lblsSameAsService.Text = "No";
                    }
                    #endregion


                    #endregion

                    #region Tenent Details
                    if (!objCustomerRegistrationBE.IsSameAsTenent)
                    {
                        divTenent.Visible = true;
                        //objCustomerRegistrationBE.TitleTanent = ddlTTitle.SelectedItem.Text;
                        lblTanentTitle.Text = objCustomerRegistrationBE.TitleTanent;
                        lblTanentFirstName.Text = objCustomerRegistrationBE.FirstNameTanent;
                        lblTanentMiddleName.Text = objCustomerRegistrationBE.MiddleNameTanent;
                        lblTanentLastName.Text = objCustomerRegistrationBE.LastNameTanent;
                        lblTanentHomeContactNo.Text = objCustomerRegistrationBE.PhoneNumberTanent;
                        lblTanentOtherContactNo.Text = objCustomerRegistrationBE.AlternatePhoneNumberTanent;
                        lblTanentEmail.Text = objCustomerRegistrationBE.EmailIdTanent;
                    }
                    else divTenent.Visible = false;
                    #endregion

                    #region Customer Details
                    lblCustomerStatus.Text = objCustomerRegistrationBE.Status;
                    lblOutStanding.Text = _objCommonMethods.GetCurrencyFormat(objCustomerRegistrationBE.OutStandingAmount, 2, Constants.MILLION_Format);
                    lblCustomerType.Text = objCustomerRegistrationBE.CustomerType;
                    lblLInfoTitle.Text = objCustomerRegistrationBE.TitleLandlord;
                    //objCustomerRegistrationBE.TitleLandlord = ddlOTitle.SelectedItem.Text;
                    lblLInfoFirstname.Text = objCustomerRegistrationBE.FirstNameLandlord;
                    lblLInfoMiddleName.Text = objCustomerRegistrationBE.MiddleNameLandlord;
                    lblLInfoLastName.Text = objCustomerRegistrationBE.LastNameLandlord;
                    lblLInfoHomePhone.Text = objCustomerRegistrationBE.HomeContactNumberLandlord;
                    lblLInfoBussPhone.Text = objCustomerRegistrationBE.BusinessPhoneNumberLandlord;
                    lblLInfoOtherPhone.Text = objCustomerRegistrationBE.OtherPhoneNumberLandlord;
                    lblLInfoEmail.Text = objCustomerRegistrationBE.EmailIdLandlord;
                    lblLInfoKnownAs.Text = objCustomerRegistrationBE.KnownAs;
                    //objCustomerRegistrationBE.IsSameAsService;
                    //objCustomerRegistrationBE.IsSameAsTenent ;
                    //if (fupDocument.HasFile) objCustomerRegistrationBE.DocumentNo;
                    if (!string.IsNullOrEmpty(objCustomerRegistrationBE.ApplicationDate)) lblApplicationDate.Text = objCustomerRegistrationBE.ApplicationDate;
                    if (!string.IsNullOrEmpty(objCustomerRegistrationBE.ConnectionDate)) lblConnectionDate.Text = objCustomerRegistrationBE.ConnectionDate;
                    if (!string.IsNullOrEmpty(objCustomerRegistrationBE.SetupDate)) lblSetUpDate.Text = objCustomerRegistrationBE.SetupDate;
                    if (objCustomerRegistrationBE.IsBEDCEmployee) { lblIsBedcEmployee.Text = "Yes"; divEmployeeCode.Visible = true; lblEmployeeCode.Text = objCustomerRegistrationBE.EmployeeName; }
                    if (objCustomerRegistrationBE.IsVIPCustomer) lblIsVipCustomer.Text = "Yes";
                    if (objCustomerRegistrationBE.IsEmbassyCustomer) { lblIsEmbassyCustomer.Text = "Yes"; tblEmbassyCode.Visible = true; }
                    lblEmployeeCode.Text = objCustomerRegistrationBE.EmployeeName;
                    //objCustomerRegistrationBE.IsBEDCEmployee = cbIsBEDCEmployee.Checked;
                    //objCustomerRegistrationBE.IsVIPCustomer = cbIsVIP.Checked;
                    lblOldAccount.Text = objCustomerRegistrationBE.OldAccountNo;
                    //lblAccountNo.Text = objCustomerRegistrationBE.AccountNo;
                    //lblEmployeeCode.Text = objCustomerRegistrationBE.EmployeeCode;
                    #endregion

                    #region Procedural Details
                    //objCustomerRegistrationBE.CustomerTypeId;
                    //objCustomerRegistrationBE.BookNo = 
                    lblPoleNo.Text = objCustomerRegistrationBE.PoleID;
                    //if (Convert.ToInt32(ReadCodeType) != (int)ReadCode.Direct) objCustomerRegistrationBE.MeterNumber;
                    //objCustomerRegistrationBE.TariffClassID;
                    //objCustomerRegistrationBE.IsEmbassyCustomer = cbIsEmbassy.Checked;
                    lblEmbassyCode.Text = objCustomerRegistrationBE.EmbassyCode;
                    lblPhase.Text = objCustomerRegistrationBE.Phase;
                    //objCustomerRegistrationBE.ReadCodeID
                    //objCustomerRegistrationBE.AccountTypeId;
                    ////objCustomerRegistrationBE.ClusterCategoryId;
                    //objCustomerRegistrationBE.ClusterCategoryId;
                    //objCustomerRegistrationBE.RouteSequenceNumber;
                    //if (!string.IsNullOrEmpty(hfGovtAccountTypeID.Value)) objCustomerRegistrationBE.MGActTypeID = Convert.ToInt32(hfGovtAccountTypeID.Value);



                    //lblPresentReading.Text = Convert.ToString(objCustomerRegistrationBE.PresentReading);
                    lblPresentReading.Text = Convert.ToString(objCustomerRegistrationBE.InitialReading);
                    #endregion

                    #region Identity Details
                    //if (hfIdentityIdList.Value != "" && hfIdentityNumberList.Value != "")
                    //{
                    //    objCustomerRegistrationBE.IdentityTypeIdList = hfIdentityIdList.Value; //Convert.ToInt32(ddlIdentityType.SelectedItem.Value);
                    //    objCustomerRegistrationBE.IdentityNumberList = hfIdentityNumberList.Value;
                    //}
                    //objCustomerRegistrationBE.UploadDocumentID = fupDocument.FileName;
                    #endregion

                    #region Application Process Details
                    //objCustomerRegistrationBE.CertifiedBy
                    lblCertifiedBy.Text = objCustomerRegistrationBE.CertifiedBy;
                    lblSeal1.Text = objCustomerRegistrationBE.Seal1;
                    lblSeal2.Text = objCustomerRegistrationBE.Seal2;
                    lblAppProcessedBy.Text = objCustomerRegistrationBE.ApplicationProcessedBy;
                    if (objCustomerRegistrationBE.ApplicationProcessedBy != null)
                        if (objCustomerRegistrationBE.ApplicationProcessedBy.ToLower() == "bedc")
                        {
                            litInstalledBy.Text = Resource.INSTALLED_BY;
                            lblInstallByName.Text = objCustomerRegistrationBE.EmployeeNameProcessBy;
                        }
                        else if (objCustomerRegistrationBE.ApplicationProcessedBy.ToLower() == "others")
                        {
                            litInstalledBy.Text = "Agency Name";
                            lblInstallByName.Text = objCustomerRegistrationBE.AgencyName;
                        }
                    #endregion

                    #region Application Process Person Details
                    //if (rblApplicationProcessedBy.Items[0].Selected) objCustomerRegistrationBE.InstalledBy;
                    //if (rblApplicationProcessedBy.Items[1].Selected) objCustomerRegistrationBE.AgencyId;
                    #endregion

                    #region Customer Active Details
                    //objCustomerRegistrationBE.IsCAPMI;
                    //if (Convert.ToInt32(ReadCodeType) != (int)ReadCode.Direct)
                    //{
                    //    if (cbIsCAPMY.Checked) objCustomerRegistrationBE.MeterAmount = Convert.ToDecimal(txtAmount.Text.Replace(",", string.Empty));
                    //    objCustomerRegistrationBE.PresentReading = Convert.ToInt32(txtPresentReading.Text);
                    //}
                    lblInitialBillingkWh.Text = Convert.ToString(objCustomerRegistrationBE.InitialBillingKWh);
                    lblAccountType.Text = objCustomerRegistrationBE.AccountType;
                    lblBookCode.Text = objCustomerRegistrationBE.BookCode;
                    lblTariff.Text = objCustomerRegistrationBE.ClassName;
                    lblClusterType.Text = objCustomerRegistrationBE.ClusterCategoryName;
                    lblPhase.Text = objCustomerRegistrationBE.Phase;
                    lblRouteNo.Text = objCustomerRegistrationBE.RouteName;
                    lblReadCode.Text = objCustomerRegistrationBE.ReadType;
                    if (objCustomerRegistrationBE.ReadType != null)
                        if (objCustomerRegistrationBE.ReadType.ToLower() == "read") { divReadCustomer.Visible = true; lblMeterNo.Text = objCustomerRegistrationBE.MeterNumber; trPresentReading.Visible = true; }
                        else if (objCustomerRegistrationBE.ReadType.ToLower() == "direct") { divDirectCustomer.Visible = true; lblAverageReading.Text = objCustomerRegistrationBE.AverageReading.ToString(); }
                    if (!objCustomerRegistrationBE.IsSameAsTenent) lblIsTenant.Text = "Yes";
                    else lblIsTenant.Text = "No";
                    if (objCustomerRegistrationBE.IsCAPMI != null)
                        if (objCustomerRegistrationBE.IsCAPMI) { lblIsCapmy.Text = "Yes"; lblMeterAmount.Text = _objCommonMethods.GetCurrencyFormat(objCustomerRegistrationBE.MeterAmount, 2, Constants.MILLION_Format); lblCapmiOutstandingAmount.Text = _objCommonMethods.GetCurrencyFormat(objCustomerRegistrationBE.CapmiOutstandingAmount, 2, Constants.MILLION_Format); }
                    //divMeterAmoount.Visible = true; 
                    #endregion

                    #region UDF Values List

                    //if (GetUDFValues())
                    //{
                    //    objCustomerRegistrationBE.UDFTypeIdList = hfUserControlFieldsID.Value;
                    //    objCustomerRegistrationBE.UDFValueList = hfUserControlFieldsValue.Value;
                    //}

                    #endregion

                    //GetIdentityDetails(GlobalAccountNo);
                    //GetCustomerDocuments(GlobalAccountNo);
                    //GetCustomerUDFValues(GlobalAccountNo);
                }
                else
                {
                    lblMessage.Text = Resource.CST_NOT_FND;
                    //mpeMessage.Show();
                }
            }
            catch (Exception ex)
            {
                _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                Message(Resource.CST_SRCH_FAILS, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}