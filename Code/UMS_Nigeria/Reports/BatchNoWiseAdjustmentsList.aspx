﻿<%@ Page Title=":: BatchNo Wise BillAdjustments List ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master" Theme="Green"
    AutoEventWireup="true" CodeBehind="BatchNoWiseAdjustmentsList.aspx.cs" Inherits="UMS_Nigeria.Reports.BatchNoWiseAdjustmentsList" %>
    <%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
   <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upBatchEntry" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAccountWoCycle" runat="server" Text="<%$ Resources:Resource, BATCH_ADJUS_LIST%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource,  FROM_DATE%>"></asp:Literal><br />
                                    <asp:TextBox ID="txtFromDate" MaxLength="10" Width="133px" ToolTip="Enter From Date"
                                        runat="server"></asp:TextBox>
                                    <asp:Image ID="imgFromDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                        vertical-align: middle" AlternateText="Calender" runat="server" />
                                    <asp:FilteredTextBoxExtender ID="ftbFromDate" runat="server" TargetControlID="txtFromDate"
                                        FilterType="Custom,Numbers" ValidChars="/">
                                    </asp:FilteredTextBoxExtender>
                                    <span id="spanFromDate" class="span_color"></span>
                            </div>
                            
                            <div id="divFilterType" runat="server" class="text-inner">
                                <asp:Literal ID="litFilterType" runat="server" Text="<%$ Resources:Resource, BATCHNUM%>"></asp:Literal><span class="span_star">*</span><br />
                            <asp:CheckBoxList ID="cblBatchNos" RepeatDirection="Horizontal" RepeatColumns="5"
                                    runat="server">
                                </asp:CheckBoxList>
                                <asp:Label ID="lblNoBatchNo" runat="server" Text="<%$ Resources:Resource,  BATCH_OPEN_MTHS%>"
                                Visible="false"></asp:Label>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource,  TO_DATE%>"></asp:Literal><br />
                                    <asp:TextBox ID="txtToDate" MaxLength="10" Width="133px" ToolTip="Enter To Date"
                                        runat="server"></asp:TextBox>
                                    <asp:Image ID="imgToDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                        vertical-align: middle" AlternateText="Calender" runat="server" />
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtToDate"
                                        FilterType="Custom,Numbers" ValidChars="/">
                                    </asp:FilteredTextBoxExtender>
                                    <span id="span1" class="span_color"></span>
                            </div>
                        </div>
                        
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <asp:Button ID="btnGo" CssClass="box_s" OnClientClick="return Validate()"
                                    Text="<%$ Resources:Resource, GO%>" OnClick="btnGo_Click" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="grid_boxes">
                    <div class="grid_paging_top">
                        <div class="paging_top_title">
                            <asp:Literal ID="litBuList" runat="server" Text="<%$ Resources:Resource, BATCH_PAY_RPT%>"></asp:Literal>
                        </div>
                        
                        <div class="paging_top_right_content" id="divPaging1" runat="server">
                            <uc1:UCPagingV1 ID="UCPaging1" runat="server" Visible="False" />
                        </div>
                        
                        </div>
                    </div>
                <div class="grid_tb" id="divgrid" runat="server">
                    <asp:GridView ID="gvBatchNoPaymentsList" runat="server" AutoGenerateColumns="False"
                        AlternatingRowStyle-CssClass="color" OnRowDataBound="gvBatchNoPaymentsList_RowDataBound" HeaderStyle-CssClass="grid_tb_hd">
                        <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                        <EmptyDataTemplate>
                            No Search Details Found.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:BoundField HeaderText="<%$ Resources:Resource, GRID_SNO%>" DataField="RowNumber" ItemStyle-CssClass="grid_tb_td"/>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, BATCHNUM%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblBatchNo" runat="server" Text='<%#Eval("BatchNo") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" DataField="AccountNo" ItemStyle-CssClass="grid_tb_td" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, NAME%>"
                                DataField="Name" ItemStyle-CssClass="grid_tb_td"/>
                            <asp:BoundField HeaderText="<%$ Resources:Resource, BATCHDATE%>" DataField="BatchDate" ItemStyle-CssClass="grid_tb_td"/>
                            <asp:TemplateField HeaderText="Total Batch Amount / Units" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotalBatchAmount" runat="server" Text='<%#Eval("TotalBatchAmount") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Adjusted Date" DataField="AdjustedDate" ItemStyle-CssClass="grid_tb_td"/>
                            <asp:TemplateField HeaderText="Adjusted Amount" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblAdjustedAmount" runat="server" Text='<%#Eval("AdjustedAmt") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Adjustment Type" DataField="AdjustmentType" ItemStyle-CssClass="grid_tb_td"/>
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, REASON%>"
                                DataField="Reason" ItemStyle-CssClass="grid_tb_td"/>
                        </Columns>
                        <RowStyle HorizontalAlign="Center" />
                    </asp:GridView>
                    <asp:HiddenField ID="hfPageSize" runat="server" />
                    <asp:HiddenField ID="hfPageNo" runat="server" />
                    <asp:HiddenField ID="hfLastPage" runat="server" />
                    <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                </div>
                <div class="grid_boxes">
                    <div id="divPaging2" runat="server">
                        <div class="grid_paging_bottom">
                            <uc1:UCPagingV1 ID="UCPaging2" runat="server" Visible="False" />
                        </div>
                    </div>
                </div>
                </div>
            
            <%-- Grid Alert popup Start--%>
        <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
            TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
        </asp:ModalPopupExtender>
        <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
        <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
            <div class="popheader popheaderlblred">
                <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
            </div>
            <div class="footer popfooterbtnrt">
                <div class="fltr popfooterbtn">
                    <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                        CssClass="ok_btn" />
                </div>
                <div class="clear pad_5">
                </div>
            </div>
        </asp:Panel>
        <%-- Grid Alert popup Closed--%>

        </ContentTemplate>
        
    </asp:UpdatePanel>
    
     <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).ready(function () {
            Calendar('<%=txtFromDate.ClientID %>', '<%=imgFromDate.ClientID %>', "-2:+8");
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            Calendar('<%=txtFromDate.ClientID %>', '<%=imgFromDate.ClientID %>', "-2:+8");
        });

        $(document).ready(function () {
            Calendar('<%=txtToDate.ClientID %>', '<%=imgToDate.ClientID %>', "-2:+8");
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            Calendar('<%=txtToDate.ClientID %>', '<%=imgToDate.ClientID %>', "-2:+8");
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/BatchNoWiseAdjustmentsList.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>