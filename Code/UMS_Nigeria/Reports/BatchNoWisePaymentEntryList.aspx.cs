﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for BatchNoWisePaymentEntryList
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 01-Oct-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using Resources;
using System.Threading;
using System.Globalization;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Data;
using System.Xml;
using org.in2bits.MyXls;
using System.Configuration;


namespace UMS_Nigeria.Reports
{
    public partial class BatchNoWisePaymentEntryList : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        RptBatchNoWisePaymentsBal _objBatchNoBal = new RptBatchNoWisePaymentsBal();
        CommonMethods _objCommonMethods = new CommonMethods();
        public int PageNum;
        string Key = UMS_Resource.KEY_RPT_PAYMENTS;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        public string FromDate
        {
            get { return string.IsNullOrEmpty(txtFromDate.Text) ? string.Empty : _objCommonMethods.Convert_MMDDYY(txtFromDate.Text); }
        }
        public string ToDate
        {
            get { return string.IsNullOrEmpty(txtToDate.Text) ? string.Empty : _objCommonMethods.Convert_MMDDYY(txtToDate.Text); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                string path = string.Empty;
                path = _objCommonMethods.GetPagePath(this.Request.Url);
                if (_objCommonMethods.IsAccess(path))
                {
                    BindBatchNos();
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                }
                else { Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }
            }
        }
        protected void btnGo_Click(object sender, EventArgs e)
        {
            BindGrid();
            UCPaging1.GeneratePaging();
            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        }

        protected void gvBatchNoPaymentsList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblTotalBatchAmount = (Label)e.Row.FindControl("lblTotalBatchAmount");
                    lblTotalBatchAmount.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblTotalBatchAmount.Text), 2, Constants.MILLION_Format);
                    Label lblPaidAmount = (Label)e.Row.FindControl("lblPaidAmount");
                    lblPaidAmount.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblPaidAmount.Text), 2, Constants.MILLION_Format);
                    Label lblBatchTotal = (Label)e.Row.FindControl("lblBatchTotal");
                    lblBatchTotal.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblBatchTotal.Text), 2, Constants.MILLION_Format);

                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods
        public void BindGrid()
        {
            try
            {
                RptBatchNoWisePaymentsBe _objBatchNoBe = new RptBatchNoWisePaymentsBe();
                _objBatchNoBe.BatchNo = _objCommonMethods.CollectSelectedItemsValues(cblBatchNos, ",");
                _objBatchNoBe.FromDate = FromDate;
                _objBatchNoBe.ToDate = ToDate;
                _objBatchNoBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                _objBatchNoBe.PageSize = PageSize;
                RptBatchNoWisePaymentsListBe objBatchNoListBe = _objiDsignBal.DeserializeFromXml<RptBatchNoWisePaymentsListBe>(_objBatchNoBal.Get(_objBatchNoBe, ReturnType.List));
                if (objBatchNoListBe.items.Count > 0)
                {
                    gvBatchNoPaymentsList.DataSource = objBatchNoListBe.items;
                    gvBatchNoPaymentsList.DataBind();
                    MergeRows(gvBatchNoPaymentsList);
                    hfTotalRecords.Value = objBatchNoListBe.items[0].TotalRecords.ToString();
                    divExport.Visible = UCPaging1.Visible=UCPaging2.Visible = true;
                }
                else
                {
                    gvBatchNoPaymentsList.DataSource = new DataTable();
                    gvBatchNoPaymentsList.DataBind();
                    hfTotalRecords.Value = Constants.Zero.ToString();
                    divExport.Visible = UCPaging1.Visible = UCPaging2.Visible = false;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public static void MergeRows(GridView gridView)
        {
            for (int rowIndex = gridView.Rows.Count - 2; rowIndex >= 0; rowIndex--)
            {
                GridViewRow row = gridView.Rows[rowIndex];
                GridViewRow previousRow = gridView.Rows[rowIndex + 1];

                Label lblBatchNo = (Label)row.FindControl("lblBatchNo");
                Label lblPreviousBatchNo = (Label)previousRow.FindControl("lblBatchNo");
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    if (i == 1 || i == 2 || i == 3 || i == 4 || i == 10)
                    {
                        if (lblBatchNo.Text == lblPreviousBatchNo.Text)
                        {
                            row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                            previousRow.Cells[i].Visible = false;
                        }
                    }
                }
            }
        }
        private void BindBatchNos()
        {
            RptBatchNoWisePaymentsBe objBatchNoBe = new RptBatchNoWisePaymentsBe();
            RptBatchNoWisePaymentsListBe objBatchNoListBe = _objiDsignBal.DeserializeFromXml<RptBatchNoWisePaymentsListBe>(_objBatchNoBal.Get(objBatchNoBe, ReturnType.Get));
            if (objBatchNoListBe.items.Count > 0)
                _objiDsignBal.FillCheckBoxList(_objiDsignBal.ConvertListToDataSet<RptBatchNoWisePaymentsBe>(objBatchNoListBe.items), cblBatchNos, "BatchNo", "BatchNo");
            else
                lblNoBatchNo.Visible = true;

        }
        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region ExcelExport

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            GetPaymentsDetails();
        }

        private void GetPaymentsDetails()
        {

            RptBatchNoWisePaymentsBe _objBatchNoBe = new RptBatchNoWisePaymentsBe();
            RptBatchNoWisePaymentsListBe _objBatchNoListBe = new RptBatchNoWisePaymentsListBe();
            _objBatchNoBe.BatchNo = _objCommonMethods.CollectSelectedItemsValues(cblBatchNos, ",");
            _objBatchNoBe.FromDate = FromDate;
            _objBatchNoBe.ToDate = ToDate;
            _objBatchNoBe.PageNo = Convert.ToInt32(hfPageNo.Value);
            _objBatchNoBe.PageSize = PageSize;
            XmlDocument xmlResult = _objBatchNoBal.Get(_objBatchNoBe, ReturnType.List);
            _objBatchNoListBe = _objiDsignBal.DeserializeFromXml<RptBatchNoWisePaymentsListBe>(xmlResult);

            XlsDocument xls = new XlsDocument();
            Worksheet sheet = xls.Workbook.Worksheets.Add("sheet");
            //state the name of the column headings 
            Cells cells = sheet.Cells;
            AddCelltoFile(cells, _objBatchNoListBe.items);

            string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
            string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
            xls.FileName = filePath;
            xls.Save();
            _objiDsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");

            //string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
            //string filePath = Server.MapPath(ConfigurationManager.AppSettings["BillGeneration"].ToString()) + fileName;

            //if ((System.IO.File.Exists(filePath)))
            //{
            //    System.IO.File.Delete(filePath);
            //}

            ////set up a filestream
            //xls.FileName = filePath;

            ////xls.FileName = Server.MapPath("Files/test.xls");
            //xls.Save();

            //System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            //response.ClearContent();
            //response.Clear();
            //response.ContentType = "application//vnd.ms-excel";
            //response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + ";");
            //response.TransmitFile(filePath);
            //response.Flush();
            //response.End();

        }

        private void AddCelltoFile(Cells cells, List<RptBatchNoWisePaymentsBe> list)
        {
            int i = 1;
            							
            cells.Add(1, 1, "SNo");
            cells.Add(1, 2, "BatchNo");
            cells.Add(1, 3, "Batch Name");
            cells.Add(1, 4, "Batch Date");
            cells.Add(1, 5, "Total Batch Amount");
            cells.Add(1, 6, "Receipt No");
            cells.Add(1, 7, "Account No");
            cells.Add(1, 8, "BillNo");
            cells.Add(1, 9, "Payment Mode");
            cells.Add(1, 10, "Paid Amount");
            cells.Add(1, 11, "Batch Total");
            foreach (RptBatchNoWisePaymentsBe item in list)
            {
                i++;
                cells.Add(i, 1, item.RowNumber);
                cells.Add(i, 2, item.BatchNo);
                cells.Add(i, 3, item.BatchName);
                cells.Add(i, 4, item.BatchDate);
                cells.Add(i, 5, item.TotalBatchAmount);
                cells.Add(i, 6, item.ReceiptNo);
                cells.Add(i, 7, item.AccountNo);
                cells.Add(i, 8, item.BillNo);
                cells.Add(i, 9, item.PaymentMode);
                cells.Add(i, 10, item.PaidAmount);
                cells.Add(i, 11, item.BatchTotal);
            }


        }
        #endregion

        //#region Paging
        //protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        BindBatchNoWisePaymentsList();
        //        hfPageSize.Value = ddlPageSize.SelectedItem.Text;
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnFirst_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        BindBatchNoWisePaymentsList();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnPrevious_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum -= Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        BindBatchNoWisePaymentsList();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnNext_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum += Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        BindBatchNoWisePaymentsList();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnLast_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = hfLastPage.Value;
        //        BindBatchNoWisePaymentsList();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //private void CreatePagingDT(int TotalNoOfRecs)
        //{
        //    try
        //    {
        //        double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
        //        DataTable dtPages = new DataTable();
        //        dtPages.Columns.Add("PageNo", typeof(int));
        //        int currentpage = Convert.ToInt32(hfPageNo.Value);
        //        lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
        //        _objiDsignBal.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
        //        lblCurrentPage.Text = hfPageNo.Value;
        //        if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.Color.Gray; }
        //        if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //private void BindPagingDropDown(int TotalRecords)
        //{
        //    try
        //    {
        //        _objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //#endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}