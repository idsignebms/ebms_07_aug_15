﻿<%@ Page Title="::Payments Report::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="BatchwisePaymentsList.aspx.cs"
    Inherits="UMS_Nigeria.Reports.BatchwisePaymentsList" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="clear pad_10 fltl">
                    <a style="padding-right: 82px; font-size: 13px; text-decoration: underline;" class="back-btn"
                        id="btnBack" href="PaymentEditListReport.aspx"><< Back To Payments Report</a>
                </div>
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddCycle" runat="server" Text="Batch Wise Payments List"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="clr">
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="out-bor_a" id="divdetails" runat="server">
                    <div class="inner-sec">
                        <div class="text-heading">
                            <asp:Literal ID="Literal9" runat="server" Text="Batch Details"></asp:Literal>
                        </div>
                        <div class="out-bor_cTwo">
                            <div class="inner-box">
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal21" runat="server" Text="Batch Number"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_b">
                                    <div class="textcustomerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblBatchNo" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal1" runat="server" Text="Batch Date"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblBatchDate" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal13" runat="server" Text="Batch Amount"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblBatchAmount" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box4_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal16" runat="server" Text="Received Amount"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box5_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box6_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblReceivedAmount" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_b">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal15" runat="server" Text="Total Customers"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_b">
                                    <div class="customerDetailstext">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_b">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblTotalCustomers" runat="server" Text="--"></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div id="divBaseGrid" runat="server">
                    <div class="grid_boxes">
                        <div class="grid_paging_top">
                            <div class="paging_top_title">
                            </div>
                            <div id="divGridContent1" align="center" runat="server" visible="false" style="margin: 5px 0;"
                                class="fltl">
                                <div class="consume_name fltl">
                                    <asp:Label ID="lblTotalUsers" runat="server" Text="<%$ Resources:Resource, TOTAL_CUSTOMERS%>"></asp:Label>
                                </div>
                                <div class="consume_input fltl" style="margin-right: 10px; border-right: 1px solid #000;
                                    padding-right: 10px;">
                                    <asp:Label ID="lblTotalUsers1" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                            <div class="paging_top_right_content" id="divPaging1" runat="server">
                                <uc1:UCPagingV1 ID="UCPaging1" runat="server" Visible="false" />
                            </div>
                            <div align="right" class="fltr" runat="server" id="divExport" visible="false">
                                <asp:Button ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" CssClass="excel"
                                    Text="Export to excel" />
                            </div>
                        </div>
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvPayments" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                            HeaderStyle-CssClass="grid_tb_hd" HeaderStyle-BackColor="#558D00">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:BoundField HeaderText="<%$ Resources:Resource, GRID_SNO%>" DataField="RowNumber"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, USER%>"
                                    DataField="UserName" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, TRANSACTION_DATE%>"
                                    DataField="TransactionDate" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>"
                                    DataField="GlobalAccountNumber" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"
                                    DataField="OldAccountNo" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, CUSTOMER_NAME%>"
                                    DataField="CustomerName" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Meter Number" DataField="MeterNumber"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, SERVICE_ADDRESS%>"
                                    DataField="ServiceAdress" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, CYCLE%>"
                                    DataField="BookGroup" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Right" HeaderText="<%$ Resources:Resource, PAID_DATE%>"
                                    DataField="PaidDate" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Receipt Number" DataField="ReceiptNo"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Right" HeaderText="<%$ Resources:Resource, AMOUNT%>"
                                    DataField="Amount" ItemStyle-CssClass="grid_tb_td" />
                            </Columns>
                            <RowStyle HorizontalAlign="center"></RowStyle>
                        </asp:GridView>
                        <asp:HiddenField ID="hfPageSize" runat="server" />
                        <asp:HiddenField ID="hfPageNo" runat="server" />
                        <asp:HiddenField ID="hfLastPage" runat="server" />
                        <asp:HiddenField ID="hfTotalRecords" runat="server" />
                    </div>
                    <div class="grid_boxes">
                        <div id="divPaging2" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" Visible="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }
        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
