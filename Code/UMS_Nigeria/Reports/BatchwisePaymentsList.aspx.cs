﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for Meter Reading Report
                     
 Developer        : Id044-Karteek.P
 Creation Date    : 25-Mar-2015
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Configuration;
using org.in2bits.MyXls;

namespace UMS_Nigeria.Reports
{
    public partial class BatchwisePaymentsList : System.Web.UI.Page
    {
        #region Members

        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        ReportsBal _objReportsBal = new ReportsBal();

        public int PageNum;
        public int DefaultPageSize = 500;
        string Key = UMS_Resource.PAYMENT_REPORT;

        #endregion

        #region Properties

        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
        }

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                if (!IsPostBack)
                {
                    //string path = string.Empty;
                    //path = _objCommonMethods.GetPagePath(this.Request.Url);
                    //if (_objCommonMethods.IsAccess(path))
                    //{
                    if (Request.QueryString["bno"] != null)
                    {
                        hfPageNo.Value = Constants.pageNoValue.ToString();
                        hfTotalRecords.Value = Constants.Zero.ToString();
                        int BatchNo = Convert.ToInt32(Request.QueryString["bno"].ToString());
                        BindBatchSummary(BatchNo);
                        BindGrid(BatchNo);
                    }
                    //}
                    //else
                    //{
                    //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    //}
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }

        #endregion

        #region Methods

        private void BindBatchSummary(int BatchNo)
        {
             RptPaymentsEditListBE _objRptPaymentsEditListBE = new RptPaymentsEditListBE();
            _objRptPaymentsEditListBE.BatchNo = BatchNo;

            _objRptPaymentsEditListBE = _objiDsignBal.DeserializeFromXml<RptPaymentsEditListBE>(_objReportsBal.GetDetails(_objRptPaymentsEditListBE, ReturnType.Get));

            if (_objRptPaymentsEditListBE != null)
            {
                lblBatchNo.Text = _objRptPaymentsEditListBE.BatchNo.ToString();
                lblBatchDate.Text = _objRptPaymentsEditListBE.PaidDate;
                lblBatchAmount.Text = _objRptPaymentsEditListBE.PaidAmount;
                lblReceivedAmount.Text = _objRptPaymentsEditListBE.PaidAmount;
                lblTotalCustomers.Text = _objRptPaymentsEditListBE.TotalRecords.ToString();
            }
        }

        public void BindGrid(int BatchNo)
        {
            RptPaymentsEditListBE _objRptPaymentsEditListBE = new RptPaymentsEditListBE();
            _objRptPaymentsEditListBE.BatchNo = BatchNo;
            _objRptPaymentsEditListBE.PageNo = Convert.ToInt32(hfPageNo.Value);
            _objRptPaymentsEditListBE.PageSize = PageSize;

            DataSet ds = new DataSet();
            ds = _objReportsBal.GetBatchWisePaymentsList(_objRptPaymentsEditListBE);

            if (ds != null)
            {
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    gvPayments.DataSource = ds.Tables[0];
                    gvPayments.DataBind();
                    divgrid.Visible = true;
                    UCPaging1.Visible = UCPaging2.Visible = true;
                    lblTotalUsers1.Text = hfTotalRecords.Value = ds.Tables[0].Rows[0]["TotalRecords"].ToString();
                    divExport.Visible = divGridContent1.Visible = lblTotalUsers.Visible = lblTotalUsers1.Visible = true;
                }
                else
                {
                    UCPaging1.Visible = UCPaging2.Visible = false;
                    divExport.Visible = divGridContent1.Visible = lblTotalUsers1.Visible = false;
                    hfTotalRecords.Value = Constants.Zero.ToString();
                    gvPayments.DataSource = new DataTable();
                    gvPayments.DataBind();
                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

        #region ExportExcel

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["bno"] != null)
                {
                    int BatchNo = 0;

                    BatchNo = Convert.ToInt32(Request.QueryString["bno"].ToString());

                    RptPaymentsEditListBE _objRptPaymentsEditListBE = new RptPaymentsEditListBE();
                    _objRptPaymentsEditListBE.BatchNo = BatchNo;
                    _objRptPaymentsEditListBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                    _objRptPaymentsEditListBE.PageSize = PageSize;
                    DataSet ds = new DataSet();
                    ds = _objReportsBal.GetBatchWisePaymentsList(_objRptPaymentsEditListBE);

                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            XlsDocument xls = new XlsDocument();
                            Worksheet sheet = xls.Workbook.Worksheets.Add("Summary");
                            Cells cells = sheet.Cells;
                            Cell _objCell = null;

                            _objCell = cells.Add(2, 3, Resource.BEDC + " - Batch Wise Payments");
                            sheet.AddMergeArea(new MergeArea(2, 2, 3, 6));
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;

                            _objCell = cells.Add(4, 2, "Batch Number");
                            cells.Add(4, 5, "Batch Date");
                            cells.Add(5, 2, "Batch Amount");
                            cells.Add(5, 5, "Received Amount");
                            cells.Add(6, 2, "Total Customers");

                            cells.Add(4, 3, lblBatchNo.Text).Font.Bold = true;
                            cells.Add(4, 6, lblBatchDate.Text).Font.Bold = true;
                            cells.Add(5, 3, lblBatchAmount.Text).Font.Bold = true;
                            cells.Add(5, 6, lblReceivedAmount.Text).Font.Bold = true;
                            cells.Add(6, 3, lblTotalCustomers.Text).Font.Bold = true;

                            int DetailsRowNo = 8;
                            int CustomersRowNo = 9;

                            _objCell = cells.Add(DetailsRowNo, 1, "S.No");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;
                            _objCell = cells.Add(DetailsRowNo, 2, "User Name");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;
                            _objCell = cells.Add(DetailsRowNo, 3, "Transaction Date");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;
                            _objCell = cells.Add(DetailsRowNo, 4, "Global Account No");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;
                            _objCell = cells.Add(DetailsRowNo, 5, "Old Account No");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;
                            _objCell = cells.Add(DetailsRowNo, 6, "Customer Name");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;
                            _objCell = cells.Add(DetailsRowNo, 7, "Meter Number");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;
                            _objCell = cells.Add(DetailsRowNo, 8, "Service Address");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;
                            _objCell = cells.Add(DetailsRowNo, 9, "Book Group");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;
                            _objCell = cells.Add(DetailsRowNo, 10, "Paid Date");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;
                            _objCell = cells.Add(DetailsRowNo, 11, "Receipt Number");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;
                            _objCell = cells.Add(DetailsRowNo, 12, "Amount");
                            _objCell.Font.Weight = FontWeight.ExtraBold;
                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objCell.Font.FontFamily = FontFamilies.Roman;

                            for (int n = 0; n < ds.Tables[0].Rows.Count; n++)
                            {
                                DataRow dr = (DataRow)ds.Tables[0].Rows[n];
                                cells.Add(CustomersRowNo, 1, dr["RowNumber"].ToString());
                                cells.Add(CustomersRowNo, 2, dr["UserName"].ToString());
                                cells.Add(CustomersRowNo, 3, dr["TransactionDate"].ToString());
                                cells.Add(CustomersRowNo, 4, dr["GlobalAccountNumber"].ToString());
                                cells.Add(CustomersRowNo, 5, dr["OldAccountNo"].ToString());
                                cells.Add(CustomersRowNo, 6, dr["CustomerName"].ToString());
                                cells.Add(CustomersRowNo, 7, dr["MeterNumber"].ToString());
                                cells.Add(CustomersRowNo, 8, dr["ServiceAdress"].ToString());
                                cells.Add(CustomersRowNo, 9, dr["BookGroup"].ToString());
                                cells.Add(CustomersRowNo, 10, dr["PaidDate"].ToString());
                                cells.Add(CustomersRowNo, 11, dr["ReceiptNo"].ToString());
                                cells.Add(CustomersRowNo, 12, dr["Amount"].ToString());

                                CustomersRowNo++;
                            }

                            string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                            fileName = "BatchWisePaymentsRerpot_" + fileName;
                            string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                            xls.FileName = filePath;
                            xls.Save();
                            _objiDsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                        }
                        else
                            Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}