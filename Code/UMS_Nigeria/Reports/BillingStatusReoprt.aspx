﻿<%@ Page Title=":: Billing Status Report ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    AutoEventWireup="true" Theme="Green" CodeBehind="BillingStatusReoprt.aspx.cs"
    Inherits="UMS_Nigeria.Reports.BillingStatusReoprt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upnlReport" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litReportHeading" runat="server" Text="Billing Status Report"></asp:Literal>
                        </div>
                        <div class="star_text" style="margin-right: 155px;">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="previous">
                        <a href="ReportsSummary.aspx">< < Back To Summary</a>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litBusinessUnitName" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT_NAME%>"></asp:Literal>
                                <span class="span_star">*</span></label><div class="space">
                                </div>
                            <asp:DropDownList ID="ddlBusinessUnitName" TabIndex="1" onchange="DropDownlistOnChangelbl(this,'spanddlBusinessUnitName','Business Unit')"
                                runat="server" CssClass="text-box text-select">
                            </asp:DropDownList>
                            <div class="space">
                            </div>
                            <span id="spanddlBusinessUnitName" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <asp:Literal ID="litYear" runat="server" Text="<%$ Resources:Resource, YEAR%>"></asp:Literal><span
                                class="span_star">*</span><br />
                            <asp:DropDownList ID="ddlYear" runat="server" CssClass="text-box select_box" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <br />
                            <span id="spanddlYear" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box2">
                        <div class="text-inner">
                            <asp:Literal ID="litMonth" runat="server" Text="<%$ Resources:Resource, MONTH%>"></asp:Literal><span
                                class="span_star">*</span><br />
                            <asp:DropDownList ID="ddlMonth" runat="server" CssClass="text-box select_box">
                                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <br />
                            <span id="spanddlMonth" class="span_color"></span>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="space">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s"
                                OnClick="btnSearch_Click" runat="server" OnClientClick="return Validate();" />
                        </div>
                    </div>
                    <div class="clr">
                        <br />
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server" visible="false">
                        <div align="right" class="fltr" runat="server" id="divExport">
                            <asp:Button ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" CssClass="exporttoexcelButton"
                                Text="" />
                        </div>
                        <div class="clear">
                        </div>
                        <div class="dot-line">
                        </div>
                        <div class="text_total">
                            <div class="text-headingReports">
                                <asp:Literal ID="ltrlReportNamewithBU" runat="server" Text=""></asp:Literal>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="dot-line">
                        </div>
                        <%--<asp:GridView ID="grdReport" runat="server" AutoGenerateColumns="false" AlternatingRowStyle-CssClass="color"
                            HeaderStyle-CssClass="grid_tb_hd" OnRowDataBound="grdReport_RowDataBound">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                No Details Found.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Customer Type" DataField="CustomerType"
                                    ItemStyle-CssClass="reportsGrid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Active" DataField="Active"
                                    ItemStyle-CssClass="reportsGrid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Total Population" DataField="TotalPopulation"
                                    ItemStyle-CssClass="reportsGrid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Energy Delivered" DataField="EnergyDelivered"
                                    ItemStyle-CssClass="reportsGrid_tb_td" />
                                <asp:TemplateField HeaderText="Energy Billed" runat="server" ItemStyle-CssClass="grid_tb_td">
                                    <ItemStyle HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblEnergyBilled" runat="server" Text='<%#Eval("EnergyBilled")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Revenue Billed" runat="server" ItemStyle-CssClass="grid_tb_td">
                                    <ItemStyle HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblRevenueBilled" runat="server" Text='<%#Eval("RevenueBilled")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Amount Billed" runat="server" ItemStyle-CssClass="grid_tb_td">
                                    <ItemStyle HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblAmountBilled" runat="server" Text='<%#Eval("AmountBilled")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="WAT" DataField="WAT"
                                    ItemStyle-CssClass="reportsGrid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="PIM" DataField="PIM"
                                    ItemStyle-CssClass="reportsGrid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="MIN FC" DataField="MIN FC"
                                    ItemStyle-CssClass="reportsGrid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Read" DataField="Read"
                                    ItemStyle-CssClass="reportsGrid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="EST" DataField="EST"
                                    ItemStyle-CssClass="reportsGrid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Direct" DataField="Direct"
                                    ItemStyle-CssClass="reportsGrid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="No Of Billed" DataField="NoOfBilled"
                                    ItemStyle-CssClass="reportsGrid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Response" DataField="Response"
                                    ItemStyle-CssClass="reportsGrid_tb_td" />
                                <asp:TemplateField HeaderText="Total Collection" runat="server" ItemStyle-CssClass="grid_tb_td">
                                    <ItemStyle HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalCollection" runat="server" Text='<%#Eval("TotalCollection")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total Adjustment" runat="server" ItemStyle-CssClass="grid_tb_td">
                                    <ItemStyle HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalAdjustment" runat="server" Text='<%#Eval("TotalAdjustment")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Opening Balance" runat="server" ItemStyle-CssClass="grid_tb_td">
                                    <ItemStyle HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblOpeningBalance" runat="server" Text='<%#Eval("OpeningBalance")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Clossing Balance" runat="server" ItemStyle-CssClass="grid_tb_td">
                                    <ItemStyle HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblClossingBalance" runat="server" Text='<%#Eval("ClossingBalance")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>--%>
                        <table style="border-collapse: collapse;">
                            <asp:Repeater ID="rptrReport" runat="server">
                                <HeaderTemplate>
                                    <tr class="rptr_tb_hd">
                                        <th>
                                            S.No
                                        </th>
                                        <th>
                                            Customer Type
                                        </th>
                                        <th>
                                            Active
                                        </th>
                                        <th>
                                            Total Population
                                        </th>
                                        <th>
                                            Energy Delivered
                                        </th>
                                        <th>
                                            Energy Billed
                                        </th>
                                        <th>
                                            Amount Billed
                                        </th>
                                        <th>
                                            Revenue Billed
                                        </th>
                                        <%-- <th>
                                            WAT
                                        </th>
                                        <th>
                                            PIM
                                        </th>--%>
                                        <th>
                                            MIN FC
                                        </th>
                                        <th>
                                            Read
                                        </th>
                                        <th>
                                            EST
                                        </th>
                                        <th>
                                            Direct
                                        </th>
                                        <th>
                                            No Of Billed
                                        </th>
                                        <%-- <th>
                                            Response
                                        </th>--%>
                                        <th>
                                            Total Collection
                                        </th>
                                        <%-- <th>
                                            Total Adjustment
                                        </th>--%>
                                        <th>
                                            Opening Balance
                                        </th>
                                        <th>
                                            Closing Balance
                                        </th>
                                    </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="reportsGrid_tb_td">
                                        <td align="left" class="rptr_tb_td">
                                            <%--<asp:Label ID="lblSno" runat="server" Text='<%#Eval("ID")%>'></asp:Label>--%>
                                            <%#Container.ItemIndex+1 %>
                                        </td>
                                        <td align="left" class="rptr_tb_td">
                                            <asp:Label ID="lblCustomerType" runat="server" Text='<%#Eval("CustomerType")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblActive" runat="server" Text='<%#Eval("Active")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblTotalPopulation" runat="server" Text='<%#Eval("TotalPopulation")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblEnergyDelivered" runat="server" Text='<%#Eval("EnergyDelivered")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblEnergyBilled" runat="server" Text='<%#Eval("EnergyBilled")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblAmountBilled" runat="server" Text='<%#Eval("AmountBilled")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblRevenueBilled" runat="server" Text='<%#Eval("RevenueBilled")%>'></asp:Label>
                                        </td>
                                        <%-- <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblWAT" runat="server" Text='<%#Eval("WAT")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblPIM" runat="server" Text='<%#Eval("PIM")%>'></asp:Label>
                                        </td>--%>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblMINFC" runat="server" Text='<%#Eval("MINFC")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblRead" runat="server" Text='<%#Eval("Read")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblEST" runat="server" Text='<%#Eval("EST")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblDirect" runat="server" Text='<%#Eval("Direct")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblNoOfBilled" runat="server" Text='<%#Eval("NoOfBilled")%>'></asp:Label>
                                        </td>
                                        <%--  <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblResponse" runat="server" Text='<%#Eval("Response")%>'></asp:Label>
                                        </td>--%>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblTotalCollection" runat="server" Text='<%#Eval("TotalCollection")%>'></asp:Label>
                                        </td>
                                        <%--<td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblTotalAdjustment" runat="server" Text='<%#Eval("TotalAdjustment")%>'></asp:Label>
                                        </td>--%>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblOpeningBalance" runat="server" Text='<%#Eval("OpeningBalance")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblClossingBalance" runat="server" Text='<%#Eval("ClossingBalance")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <tr id="NoDataFound" runat="server" align="center">
                                <td colspan="16">
                                    No data found for selected month & Year
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script type="text/javascript">
        function pageLoad() {
            DisplayMessage('UMSNigeriaBody_pnlMessage');
        };
        function Validate() {

            var ddlYear = document.getElementById('UMSNigeriaBody_ddlYear');
            var ddlMonth = document.getElementById('UMSNigeriaBody_ddlMonth');

            var IsValid = true;

            if (DropDownlistValidationlbl(ddlYear, document.getElementById("spanddlYear"), "Year") == false) IsValid = false;
            if (DropDownlistValidationlbl(ddlMonth, document.getElementById("spanddlMonth"), "Month") == false) IsValid = false;

            if (!IsValid) {
                return false;
            }
        }
    </script>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
