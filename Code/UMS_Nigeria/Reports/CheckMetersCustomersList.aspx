﻿<%@ Page Title=":: Check Meters Customers List ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master" Theme="Green"
    AutoEventWireup="true" CodeBehind="CheckMetersCustomersList.aspx.cs" Inherits="UMS_Nigeria.Reports.CheckMetersCustomersList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upRptAccWithCreditBalance" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>                
                <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litSearchCustomer" runat="server" Text="<%$ Resources:Resource, CHK_MTR_CUST_RPT%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                <div class="consumer_feild">
                    <div class="consume_nameTwo" style="margin-left: 6px;">
                        <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                    </div>
                    <div class="consume_input">
                        <asp:DropDownList ID="ddlBU" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlBU_SelectedIndexChanged">
                            <asp:ListItem Value="" Text="ALL"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="consume_nameTwo">
                        <asp:Literal ID="litServiceUnit" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal>
                    </div>
                    <div class="consume_input">
                        <asp:DropDownList ID="ddlSU" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlSU_SelectedIndexChanged">
                            <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="consume_nameTwo" style="margin-left: 6px;">
                        <asp:Literal ID="litServiceCenter" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal>
                    </div>
                    <div class="consume_input">
                        <asp:DropDownList ID="ddlSC" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSC_SelectedIndexChanged">
                            <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="clear pad_5">
                    </div>
                    <div class="consume_nameTwo" style="margin-left: 6px;">
                        <asp:Literal ID="litCycles" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal>
                    </div>
                    <div class="consume_input">
                        <asp:DropDownList ID="ddlCycle" AutoPostBack="true" OnSelectedIndexChanged="ddlCycle_SelectedIndexChanged"
                            runat="server">
                            <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="consume_nameTwo">
                        <asp:Literal ID="litBookNo" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal>
                    </div>
                    <div class="consume_input">
                        <asp:DropDownList ID="ddlBookNo" Enabled="false" runat="server" AutoPostBack="true">
                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="clear pad_5">
                        <div style="margin-left: 480px;">
                            <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s"
                                OnClick="btnSearch_Click" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="consumer_total">
                    <div class="pad_10">
                    </div>
                </div>
                <div class="consumer_total">
                    <div class="pad_10">
                        <br />
                    </div>
                </div>
                <div id="divgrid" class="grid" runat="server" style="width: 100% !important; float: left;">
                    <div id="divpaging" runat="server">
                        <div align="right" class="marg_20t" runat="server" id="divLinks">
                            <h3 class="gridhead" align="left" style="width: 28%;">
                                <asp:Literal ID="litBuList" runat="server" Text="<%$ Resources:Resource, CHK_MTR_CUST_LIST%>"></asp:Literal>
                            </h3>
                            <div id="divGridContent1" align="center" runat="server" visible="false" style="margin: 10px 0;
                                margin-left: 115px;" class="fltl">
                                <div class="consume_nameTwo fltl">
                                    <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, TOTAL_CUSTOMERS%>"></asp:Label>
                                </div>
                                <div class="consume_input fltl" style="margin-right: 10px; padding-right: 10px;">
                                    <asp:Label ID="lblTotalCustomers1" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                            <asp:LinkButton ID="lkbtnFirst" runat="server" CssClass="pagingfirst" OnClick="lkbtnFirst_Click">
                                <asp:Literal ID="litFirst" runat="server" Text="<%$ Resources:Resource, FIRST%>"></asp:Literal></asp:LinkButton>
                            &nbsp;|
                            <asp:LinkButton ID="lkbtnPrevious" runat="server" CssClass="pagingfirst" OnClick="lkbtnPrevious_Click">
                                <asp:Literal ID="litPrevious" runat="server" Text="<%$ Resources:Resource, PREVIOUS%>"></asp:Literal></asp:LinkButton>
                            &nbsp;|
                            <asp:Label ID="lblCurrentPage" runat="server" Font-Bold="true"></asp:Label>
                            &nbsp;|
                            <asp:LinkButton ID="lkbtnNext" runat="server" CssClass="pagingfirst" OnClick="lkbtnNext_Click">
                                <asp:Literal ID="litNext" runat="server" Text="<%$ Resources:Resource, NEXT%>"></asp:Literal></asp:LinkButton>
                            &nbsp;|
                            <asp:LinkButton ID="lkbtnLast" runat="server" CssClass="pagingfirst" OnClick="lkbtnLast_Click">
                                <asp:Literal ID="litLast" runat="server" Text="<%$ Resources:Resource, LAST%>"></asp:Literal></asp:LinkButton>
                            &nbsp;|<asp:Literal ID="litPageSize" runat="server" Text="<%$ Resources:Resource, PAGE_SIZE%>"></asp:Literal>
                            <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" CssClass="paging-size"
                                OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="consumer_total">
                    <div class="clear">
                    </div>
                </div>
                <div class="grid" id="divPrint" runat="server" align="center" style="overflow-x: auto;">
                    <asp:GridView ID="gvCheckMetersList" runat="server" AutoGenerateColumns="False"

                    AlternatingRowStyle-CssClass="color">
                        <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                        <EmptyDataTemplate>
                            There is no data.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:BoundField HeaderText="<%$ Resources:Resource, GRID_SNO%>" DataField="RowNumber" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>"
                                DataField="AccountNo" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, NAME%>"
                                DataField="Name" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, CHK_MTR_INI_READING%>"
                                DataField="InitialReading" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, CHK_MTR_CUR_READING%>"
                                DataField="CheckMeterCurrentReading" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, MTR_CUR_READING%>"
                                DataField="MeterCurrentReading" />
                        </Columns>
                        <RowStyle HorizontalAlign="center"></RowStyle>
                    </asp:GridView>
                    <div class="clear pad_10">
                    </div>
                    <div id="divGridContent" style="margin-left: 483px;" runat="server" visible="false">
                        <div class="consume_nameTwo fltl">
                            <asp:Label ID="Label2" runat="server" Visible="false" Text="<%$ Resources:Resource, TOTAL_CUSTOMERS%>"></asp:Label>
                        </div>
                        <div class="consume_input fltl" style="margin-right: 10px; padding-right: 10px;">
                            <asp:Label ID="lblTotalCustomers" Visible="false" runat="server" Text="--"></asp:Label>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="hfPageSize" runat="server" />
                <asp:HiddenField ID="hfPageNo" runat="server" />
                <asp:HiddenField ID="hfLastPage" runat="server" />
                <asp:HiddenField ID="hfTotalRecords" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--==============Escape button script starts==============--%>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
