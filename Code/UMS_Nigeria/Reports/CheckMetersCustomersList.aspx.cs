﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for CheckMetersCustomersList
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 17-Sep-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Configuration;

namespace UMS_Nigeria.Reports
{
    public partial class CheckMetersCustomersList : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        ReportsBal _objReportsBal = new ReportsBal();
        public int PageNum;
        XmlDocument xml = null;
        string Key = UMS_Resource.RPT_CHECK_METERSE_LIST;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        public string BusinessUnitName
        {
            get { return string.IsNullOrEmpty(ddlBU.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlBU, ",") : ddlBU.SelectedValue; }
            set { ddlBU.SelectedValue = value.ToString(); }
        }
        public string ServiceUnitName
        {
            get { return string.IsNullOrEmpty(ddlSU.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlSU, ",") : ddlSU.SelectedValue; }
            set { ddlSU.SelectedValue = value.ToString(); }
        }
        public string ServiceCenterName
        {
            get { return string.IsNullOrEmpty(ddlSC.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlSC, ",") : ddlSC.SelectedValue; }
            set { ddlSC.SelectedValue = value.ToString(); }
        }
        public string BookNo
        {
            get { return string.IsNullOrEmpty(ddlBookNo.SelectedValue) ? string.Empty : ddlBookNo.SelectedValue; }
            set { ddlBookNo.SelectedValue = value.ToString(); }
        }
        public string Cycle
        {
            get { return string.IsNullOrEmpty(ddlCycle.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlCycle, ",") : ddlCycle.SelectedValue; }
            set { ddlCycle.SelectedValue = value.ToString(); }
        }
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                if (!IsPostBack)
                {
                    //string path = string.Empty;
                    //path = _objCommonMethods.GetPagePath(this.Request.Url);
                    //if (_objCommonMethods.IsAccess(path))
                    //{
                        BindDropDowns();
                        hfPageNo.Value = Constants.pageNoValue.ToString();
                        BindCheckMetersList();
                        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //}
                    //else
                    //{
                    //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    //}
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindCheckMetersList();
            CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
        }
        #endregion

        #region DDLEvents
        protected void ddlBU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _objCommonMethods.BindUserServiceUnits_BuIds(ddlSU, BusinessUnitName, Resource.DDL_ALL, false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlSU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _objCommonMethods.BindUserServiceCenters_SuIds(ddlSC, ServiceUnitName, Resource.DDL_ALL, false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _objCommonMethods.BindCyclesByBUSUSC(ddlCycle, BusinessUnitName, ServiceUnitName, ServiceCenterName, true, Resource.DDL_ALL, false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlCycle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCycle.SelectedIndex > 0)
            {
                _objCommonMethods.BindAllBookNumbers(ddlBookNo, ddlCycle.SelectedValue, true, false, Resource.DDL_ALL);
                ddlBookNo.Enabled = true;
            }
            else
            {
                ddlBookNo.SelectedIndex = Constants.Zero;
                ddlBookNo.Enabled = false;
            }
               // _objCommonMethods.BindAllBookNumbers(ddlBookNo, , true, false, Resource.DDL_ALL);
        }
        #endregion

        #region Methods
        private void BindDropDowns()
        {
            _objCommonMethods.BindUserBusinessUnits(ddlBU, string.Empty, false, Resource.DDL_ALL);
            //_objCommonMethods.BindUserServiceUnits_BuIds(ddlSU, _objCommonMethods.CollectAllValues(ddlBU, ","), Resource.DDL_ALL, false);
            //_objCommonMethods.BindUserServiceCenters_SuIds(ddlSC, _objCommonMethods.CollectAllValues(ddlSU, ","), Resource.DDL_ALL, false);
            //_objCommonMethods.BindAllCyclesList(ddlCycle, true, Resource.DDL_ALL, false);
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
            {
                string BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                ddlBU.SelectedIndex = ddlBU.Items.IndexOf(ddlBU.Items.FindByValue(BUID));
                ddlBU.Enabled = false;
                ddlBU_SelectedIndexChanged(ddlBU, new EventArgs());
                ddlSU_SelectedIndexChanged(ddlSU, new EventArgs());
                _objCommonMethods.BindCyclesByBUSUSC(ddlCycle, BUID, string.Empty, string.Empty, true, "ALL", true);
            }
            else
            {
                _objCommonMethods.BindAllCyclesList(ddlCycle, true, Resource.DDL_ALL, false);
            }
            //_objCommonMethods.BindAllBookNumbers(ddlBookNo, ddlCycle.SelectedValue, true, false, Resource.DDL_ALL);
        }
        private void BindCheckMetersList()
        {
            divGridContent1.Visible = divGridContent.Visible = Label2.Visible = lblTotalCustomers.Visible = false;
            RptCheckMetersBe _objRptCheckMetersBe = new RptCheckMetersBe();
            _objRptCheckMetersBe.BU_ID = this.BusinessUnitName;
            _objRptCheckMetersBe.SU_ID = this.ServiceUnitName;
            _objRptCheckMetersBe.ServiceCenterId = this.ServiceCenterName;
            _objRptCheckMetersBe.CycleId = this.Cycle;
            _objRptCheckMetersBe.BookNo = this.BookNo;
            _objRptCheckMetersBe.PageNo = Convert.ToInt32(hfPageNo.Value);
            _objRptCheckMetersBe.PageSize = PageSize;
            RptCheckMetersListBe _objRptCheckMetersListBe = _objiDsignBal.DeserializeFromXml<RptCheckMetersListBe>(_objReportsBal.GetList(_objRptCheckMetersBe, ReturnType.Get));

            if (_objRptCheckMetersListBe.items.Count > 0)
            {
                divgrid.Visible = true;
                lblTotalCustomers.Text = lblTotalCustomers1.Text = hfTotalRecords.Value = _objRptCheckMetersListBe.items[0].TotalRecords.ToString();
                divGridContent1.Visible = divGridContent.Visible = Label2.Visible = lblTotalCustomers.Visible = true;
                gvCheckMetersList.DataSource = _objRptCheckMetersListBe.items;
                gvCheckMetersList.DataBind();
            }
            else
            {
                divgrid.Visible = false;
                hfTotalRecords.Value = Constants.Zero.ToString();
                gvCheckMetersList.DataSource = new DataTable();
                gvCheckMetersList.DataBind();
            }
        }
        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Paging
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindCheckMetersList();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindCheckMetersList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindCheckMetersList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindCheckMetersList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                BindCheckMetersList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                _objiDsignBal.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                _objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}