﻿<%@ Page Title="::Customer Bills::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    AutoEventWireup="true" CodeBehind="CustomerBills.aspx.cs" Theme="Green" Inherits="UMS_Nigeria.Reports.CustomerBills" %>

<%@ Register Src="../UserControls/UCPagingV2.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdateProgress ID="UpdateProgress" runat="server">
                <ProgressTemplate>
                    <center>
                        <div id="loading-div" class="pbloading">
                            <div id="title-loading" class="pbtitle">
                                BEDC</div>
                            <center>
                                <img src="../images/loading.GIF" class="pbimg"></center>
                            <p class="pbtxt">
                                Loading Please wait.....</p>
                        </div>
                    </center>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
                PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
            <asp:UpdatePanel ID="upAddCycle" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddCycle" runat="server" Text="<%$ Resources:Resource, CUSTOMER_BILLS%>"></asp:Literal>
                        </div>
                        <div class="star_text" style="margin-right:136px;">
                            <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                            &nbsp
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="previous">
                        <a href="ReportsSummary.aspx">< < Back To Summary</a>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                                </label>
                                <br />
                                <asp:DropDownList ID="ddlBusinessUnitName" CssClass="text-box select_box" AutoPostBack="true"
                                    runat="server" OnSelectedIndexChanged="ddlBusinessUnitName_SelectedIndexChanged">
                                    <asp:ListItem Value="" Text="ALL"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litServiceUnit" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal>
                                </label>
                                <br />
                                <asp:DropDownList ID="ddlServiceUnitName" CssClass="text-box select_box" AutoPostBack="true"
                                    runat="server" OnSelectedIndexChanged="ddlServiceUnitName_SelectedIndexChanged">
                                    <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litCycles" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal>
                                </label>
                                <br />
                                <asp:DropDownList ID="ddlCycle" CssClass="text-box select_box" runat="server">
                                    <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="litYear" runat="server" Text="<%$ Resources:Resource, YEAR%>"></asp:Literal><span
                                    class="span_star">*</span><br />
                                <asp:DropDownList ID="ddlYear" runat="server" CssClass="text-box select_box" onchange="DropDownlistOnChangelbl(this,'spanddlYear',' Year')"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                <span id="spanddlYear" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <asp:Literal ID="litMonth" runat="server" Text="<%$ Resources:Resource, MONTH%>"></asp:Literal><span
                                    class="span_star">*</span><br />
                                <asp:DropDownList ID="ddlMonth" runat="server" CssClass="text-box select_box" onchange="DropDownlistOnChangelbl(this,'spanddlMonth',' Month')">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                <span id="spanddlMonth" class="span_color"></span>
                            </div>
                        </div>
                        <%--<div class="inner-box3">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, YEAR%>"></asp:Literal>
                                </label>
                                <br />
                                <asp:DropDownList ID="ddlYear" CssClass="text-box select_box" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" runat="server">
                                    <asp:ListItem Value="" Text="ALL"></asp:ListItem>
                                </asp:DropDownList>
                                <div class="clear space">
                                </div>
                                <span id="spanddlYear" class="spancolor"></span>
                            </div>
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, MONTH%>"></asp:Literal>
                                </label>
                                <br />
                                <asp:DropDownList ID="ddlMonth" CssClass="text-box select_box" AutoPostBack="true"
                                    runat="server">
                                    <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <div class="clear space">
                                </div>
                                <span id="spanddlMonth" class="spancolor"></span>
                            </div>
                        </div>--%>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource,  TARIFF%>"></asp:Literal>
                                </label>
                                <br />
                                <asp:DropDownList ID="ddlTariff" CssClass="text-box select_box" runat="server">
                                    <asp:ListItem Value="">ALL</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, REQUEST_FROM%>"></asp:Literal>
                                </label>
                                <br />
                                <asp:DropDownList ID="ddlRequestFrom" CssClass="text-box select_box" runat="server">
                                    <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <div class="clear space">
                                </div>
                                <span id="span1" class="spancolor"></span>
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <asp:Button ID="btnSearch" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SEARCH%>"
                                CssClass="box_s" OnClick="btnSearch_Click" runat="server" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="consumer_total">
                <div class="pad_10">
                </div>
            </div>
            <div class="consumer_total">
                <div class="pad_10">
                    <br />
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="grid_boxes" id="rptrDetails" runat="server" visible="false">
                        <div class="grid_paging_top">
                            <div class="paging_top_title">
                                <asp:Literal ID="litBuList" runat="server" Text="Customer Bills List"></asp:Literal>
                            </div>
                            <div class="clear">
                            </div>
                            <div id="divToprptrContent" runat="server" visible="false">
                                <div class="consume_name fltl">
                                    <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, TOTAL_CUSTOMERS%>"></asp:Label>
                                </div>
                                <div class="consume_input fltl" style="margin-right: 10px;">
                                    <asp:Label ID="lblTopTotalCustomers" runat="server" Text="--"></asp:Label>
                                </div>
                                <div class="paging_top_right_content" id="divTopPaging" runat="server">
                                    <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                                </div>
                            </div>
                            <div align="right" runat="server" id="divExport" visible="false">
                                <asp:Button ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" CssClass="exporttoexcelButton" /></div>
                        </div>
                        <div class="clr pad_10"></div>
                        <div class="rptr_tb">
                            <table style="border-collapse: collapse; border:1px #000 solid;">
                                <tr class="rptr_tb_hd" align="center" runat="server" visible="false" id="rptrheader">
                                    <th>
                                        <asp:Label ID="lblHSno" runat="server" Text="<%$ Resources:Resource, GRID_SNO%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label30" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label31" runat="server" Text="Customer Status"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label10" runat="server" Text="<%$ Resources:Resource, NetEnergyCharges%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label11" runat="server" Text="<%$ Resources:Resource, NET_FIXED_CHARGES%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, TOT_BILLS_AMT%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, VAT%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label12" runat="server" Text="<%$ Resources:Resource, VAT_PERCENTAGE%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label13" runat="server" Text="<%$ Resources:Resource, TOTAL_BILL_AMT_WITH_TAX%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label14" runat="server" Text="<%$ Resources:Resource, NET_ARREARS%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label15" runat="server" Text="<%$ Resources:Resource, TOTAL_BILL_AMOT_WITH_ARREARS%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label16" runat="server" Text="<%$ Resources:Resource, READCODE%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label17" runat="server" Text="<%$ Resources:Resource, USAGE%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label18" runat="server" Text="<%$ Resources:Resource, PAY_LAST_DATE%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label19" runat="server" Text="<%$ Resources:Resource, PAID_AMOUNT%>"></asp:Label>
                                    </th>
                                </tr>
                                <asp:Repeater ID="rptrDebitBalence" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td class="rptr_tb_td" align="center">
                                                <asp:Label runat="server" ID="lblsno" Text='<%#Eval("RowNumber") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="center">
                                                <asp:Label runat="server" ID="lblAccNo" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="left">
                                                <asp:Label runat="server" ID="lblOldAccountNo" Text='<%#Eval("OldAccountNo") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="left">
                                                <asp:Label runat="server" ID="lblName" Text='<%#Eval("Name") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="left">
                                                <asp:Label runat="server" ID="lblAddress" Text='<%#Eval("ServiceAddress") %>'></asp:Label>
                                            </td>
                                             <td class="rptr_tb_td" align="left">
                                                <asp:Label runat="server" ID="Label32" Text='<%#Eval("TariffName") %>'></asp:Label>
                                            </td>
                                             <td class="rptr_tb_td" align="left">
                                                <asp:Label runat="server" ID="Label33" Text='<%#Eval("ActiveStatus") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="right">
                                                <asp:Label runat="server" ID="lblOutStanding" Text='<%#Eval("MeterNo") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="right">
                                                <asp:Label ID="lblLastPaidAmount" runat="server" Text='<%#Eval("NetEnergyCharges")%>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="center">
                                                <asp:Label runat="server" ID="lblLastPaidDate" Text='<%#Eval("NetFixedCharges") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="center">
                                                <asp:Label runat="server" ID="Label20" Text='<%#Eval("TotalBillAmount") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="center">
                                                <asp:Label runat="server" ID="Label21" Text='<%#Eval("VAT") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="center">
                                                <asp:Label runat="server" ID="Label22" Text='<%#Eval("VATPercentage") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="center">
                                                <asp:Label runat="server" ID="Label23" Text='<%#Eval("TotalBillAmountWithTax") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="center">
                                                <asp:Label runat="server" ID="Label24" Text='<%#Eval("NetArrears") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="center">
                                                <asp:Label runat="server" ID="Label25" Text='<%#Eval("TotalBillAmountWithArrears") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="center">
                                                <asp:Label runat="server" ID="Label26" Text='<%#Eval("ReadCode") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="center">
                                                <asp:Label runat="server" ID="Label27" Text='<%#Eval("Usage") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="center">
                                                <asp:Label runat="server" ID="Label28" Text='<%#Eval("PaymentLastDate") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="center">
                                                <asp:Label runat="server" ID="Label29" Text='<%#Eval("PaidAmount") %>'></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <tr align="center" runat="server" id="divNoDataFound" visible="false">
                                    <td colspan="18">
                                        No data found
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="divDownrptrContent" class="reports_txt" runat="server" visible="false">
                            <div class="consume_name fltl">
                                <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, TOTAL_CUSTOMERS%>"></asp:Label>
                            </div>
                            <div class="consume_input fltl" style="margin-right: 10px;">
                                <asp:Label ID="lblDownTotalCustomers" runat="server" Text="--"></asp:Label>
                            </div>
                            <div id="divDownPaging" runat="server">
                                <div class="grid_paging_bottom">
                                    <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:HiddenField ID="hfPageSize" runat="server" />
                    <asp:HiddenField ID="hfPageNo" runat="server" />
                    <asp:HiddenField ID="hfLastPage" runat="server" />
                    <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                    <asp:PostBackTrigger ControlID="btnExportExcel" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script type="text/javascript">
        function Validate() {
            var ddlYear = document.getElementById('UMSNigeriaBody_ddlYear');
            var ddlMonth = document.getElementById('UMSNigeriaBody_ddlMonth');

            var IsValid = true;

            if (DropDownlistValidationlbl(ddlYear, document.getElementById("spanddlYear"), "Year") == false) IsValid = false;
            if (DropDownlistValidationlbl(ddlMonth, document.getElementById("spanddlMonth"), "Month") == false) IsValid = false;

            if (!IsValid) {
                return false;
            }
        }
    </script>
    <script type="text/javascript">
        //        function Validate() {
        //            var ddlYear = document.getElementById('<%=ddlYear.ClientID %>');
        //            var ddlMonth = document.getElementById('<%=ddlMonth.ClientID %>');
        //            var IsValid = true;
        //            if (DropDownlistValidationlbl(ddlYear, document.getElementById("spanddlYear"), " Year") == false) IsValid = false;
        //            if (DropDownlistValidationlbl(ddlMonth, document.getElementById("spanddlMonth"), " Month") == false) IsValid = false;
        //            if (!IsValid) {
        //                return false;
        //            }
        //        }
    </script>
    <script type="text/javascript">
        function pageLoad() {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        };       
    </script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
