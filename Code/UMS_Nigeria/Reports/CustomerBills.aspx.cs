﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for CustomerBills
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 03-June-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Data.SqlClient;
using org.in2bits.MyXls;

namespace UMS_Nigeria.Reports
{
    public partial class CustomerBills : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        BillGenerationBal objBillGenerationBal = new BillGenerationBal();
        GeneralReportsBal _objGeneralReportsBal = new GeneralReportsBal();
        string Key = "CustomerBills";
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStartsReport : Convert.ToInt32(hfPageSize.Value); }
        }

        public string BusinessUnitName
        {
            get { return string.IsNullOrEmpty(ddlBusinessUnitName.SelectedValue) ? objCommonMethods.CollectAllValues(ddlBusinessUnitName, ",") : ddlBusinessUnitName.SelectedValue; }
            set { ddlBusinessUnitName.SelectedValue = value.ToString(); }
        }

        public string ServiceUnit
        {
            get { return string.IsNullOrEmpty(ddlServiceUnitName.SelectedValue) ? objCommonMethods.CollectAllValues(ddlServiceUnitName, ",") : ddlServiceUnitName.SelectedValue; }
            set { ddlServiceUnitName.SelectedValue = value.ToString(); }
        }
        public string CycleIds
        {
            get { return string.IsNullOrEmpty(ddlCycle.SelectedValue) ? objCommonMethods.CollectAllValues(ddlCycle, ",") : ddlCycle.SelectedValue; }
            set { ddlCycle.SelectedValue = value.ToString(); }
        }

        public string TariffIds
        {
            get { return string.IsNullOrEmpty(ddlTariff.SelectedValue) ? objCommonMethods.CollectAllValues(ddlTariff, ",") : ddlTariff.SelectedValue; }
            set { ddlTariff.SelectedValue = value.ToString(); }
        }

        //public string Year
        //{
        //    get { return string.IsNullOrEmpty(ddlYear.SelectedValue) ? objCommonMethods.CollectAllValues(ddlYear, ",") : ddlYear.SelectedValue; }
        //    set { ddlYear.SelectedValue = value.ToString(); }
        //}

        //public string Month
        //{
        //    get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? objCommonMethods.CollectAllValues(ddlMonth, ",") : ddlMonth.SelectedValue; }
        //    set { ddlMonth.SelectedValue = value.ToString(); }
        //}
        private int YearId
        {
            get { return string.IsNullOrEmpty(ddlYear.SelectedValue) ? 0 : Convert.ToInt32(ddlYear.SelectedValue); }
            set { ddlYear.SelectedValue = value.ToString(); }
        }
        private int MonthId
        {
            get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? 0 : Convert.ToInt32(ddlMonth.SelectedValue); }
            set { ddlMonth.SelectedValue = value.ToString(); }
        }
        public int RequestFrom
        {
            get { return string.IsNullOrEmpty(ddlRequestFrom.SelectedValue) ? 0 : Convert.ToInt32(ddlRequestFrom.SelectedValue); }
            set { ddlRequestFrom.SelectedValue = value.ToString(); }
        }

        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE, false);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                if (!IsPostBack)
                {
                    //string path = string.Empty;
                    //path = objCommonMethods.GetPagePath(this.Request.Url);
                    //if (objCommonMethods.IsAccess(path))
                    //{
                    BindDropDowns();
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    //}
                    //else
                    //{
                    //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    //}
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE, false);
            }
        }
        protected void ddlBusinessUnitName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objCommonMethods.BindUserServiceUnits_BuIds(ddlServiceUnitName, BusinessUnitName, Resource.DDL_ALL, true);
                // objCommonMethods.BindFeeders_BySuId(ddlFeeder, this.ServiceUnit, Resource.DDL_ALL, true); //Feeders Table removed
                objCommonMethods.BindCycles_BySuId(ddlCycle, this.ServiceUnit, Resource.DDL_ALL, true);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hfPageNo.Value = Constants.pageNoValue.ToString();
            BindGrid();
            UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            UCPaging2.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        }
        //protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        BindMonths();
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //    }
        //}
        #endregion

        #region ExportExcel

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            RptCustomerBillsBe _objBillsBe = new RptCustomerBillsBe();
            _objBillsBe.BU_ID = this.BusinessUnitName;
            _objBillsBe.SU_ID = this.ServiceUnit;
            _objBillsBe.CycleId = this.CycleIds;
            _objBillsBe.TariffIds = this.TariffIds;
            _objBillsBe.Years = this.YearId.ToString();
            _objBillsBe.Months = this.MonthId.ToString();
            _objBillsBe.BillProcessTypeId = this.RequestFrom;
            _objBillsBe.PageNo = Constants.pageNoValue;
            _objBillsBe.PageSize = Convert.ToInt32(hfTotalRecords.Value);
            DataSet ds = new DataSet();
            ds = _objGeneralReportsBal.GetBillsReport(_objBillsBe, ReturnType.Get);

            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    var BUlist = (from list in ds.Tables[0].AsEnumerable()
                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                  group list by list.Field<string>("BusinessUnitName") into g
                                  select new { BusinessUnit = g.Key }).ToList();

                    if (BUlist.Count() > 0)
                    {
                        XlsDocument xls = new XlsDocument();
                        Worksheet mainSheet = xls.Workbook.Worksheets.Add("Summary");
                        Cells mainCells = mainSheet.Cells;
                        Cell _objMainCells = null;


                        _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
                        mainCells.Add(4, 2, "Report Code ");
                        mainCells.Add(4, 3, Constants.CustomerBillsReport_Code).Font.Bold = true;
                        mainCells.Add(4, 4, "Report Name");
                        mainCells.Add(4, 5, Constants.CustomerBillsReport_Name).Font.Bold = true;
                        mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
                        _objMainCells.Font.Weight = FontWeight.ExtraBold;
                        _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                        _objMainCells.Font.FontFamily = FontFamilies.Roman;

                        int MainSheetDetailsRowNo = 6;
                        mainCells.Add(MainSheetDetailsRowNo, 1, "S.No.").Font.Bold = true;
                        mainCells.Add(MainSheetDetailsRowNo, 2, "Service Center").Font.Bold = true;
                        mainCells.Add(MainSheetDetailsRowNo, 3, "Customer Count").Font.Bold = true;

                        var SClist = (from list in ds.Tables[0].AsEnumerable()
                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                      group list by list.Field<string>("ServiceCenterName") into g
                                      select new { ServiceCenter = g.Key }).ToList();

                        for (int k = 0; k < SClist.Count; k++)//SC
                        {
                            var Customers = (from list in ds.Tables[0].AsEnumerable()
                                             where list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                             select list).ToList();

                            MainSheetDetailsRowNo++;
                            mainCells.Add(MainSheetDetailsRowNo, 1, k + 1);
                            mainCells.Add(MainSheetDetailsRowNo, 2, SClist[k].ServiceCenter);
                            mainCells.Add(MainSheetDetailsRowNo, 3, Customers.Count);
                        }

                        for (int i = 0; i < BUlist.Count; i++)//BU
                        {
                            var SUlistByBU = (from list in ds.Tables[0].AsEnumerable()
                                              where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                              orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                              orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                              group list by list.Field<string>("ServiceUnitName") into g
                                              select new { ServiceUnitName = g.Key }).ToList();

                            for (int j = 0; j < SUlistByBU.Count; j++)//SU
                            {
                                var SClistBySU = (from list in ds.Tables[0].AsEnumerable()
                                                  where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                  && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                  group list by list.Field<string>("ServiceCenterName") into g
                                                  select new { ServiceCenter = g.Key }).ToList();

                                for (int k = 0; k < SClistBySU.Count; k++)//SC
                                {

                                    var CyclelistBySC = (from list in ds.Tables[0].AsEnumerable()
                                                         where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                         && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                         && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                         orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                         orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                         group list by list.Field<string>("CycleName") into g
                                                         select new { CycleName = g.Key }).ToList();

                                    if (CyclelistBySC.Count > 0)
                                    {
                                        int DetailsRowNo = 2;
                                        int CustomersRowNo = 6;

                                        Worksheet sheet = xls.Workbook.Worksheets.Add(SClistBySU[k].ServiceCenter);
                                        Cells cells = sheet.Cells;
                                        Cell _objCell = null;

                                        for (int l = 0; l < CyclelistBySC.Count; l++)//Cycle
                                        {
                                            var BooklistByCycle = (from list in ds.Tables[0].AsEnumerable()
                                                                   where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                   && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                   && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                   && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                   orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                   orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                   group list by list.Field<string>("BookNumber") into g
                                                                   select new { BookNumber = g.Key }).ToList();

                                            for (int m = 0; m < BooklistByCycle.Count; m++)//Book
                                            {
                                                var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                                 where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                 && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                 && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                 && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                 && list.Field<string>("BookNumber") == BooklistByCycle[m].BookNumber
                                                                 orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                 orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                 orderby Convert.ToString(list["CycleName"]) ascending
                                                                 select list).ToList();

                                                if (Customers.Count() > 0)
                                                {
                                                    _objCell = cells.Add(DetailsRowNo, 1, "Business Unit");
                                                    cells.Add(DetailsRowNo, 3, "Service Unit");
                                                    cells.Add(DetailsRowNo, 5, "Service Center");
                                                    cells.Add(DetailsRowNo + 1, 1, "Book Group");
                                                    cells.Add(DetailsRowNo + 1, 3, "Book Number");

                                                    cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 4, SUlistByBU[j].ServiceUnitName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 6, SClistBySU[k].ServiceCenter).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 2, CyclelistBySC[l].CycleName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 4, BooklistByCycle[m].BookNumber).Font.Bold = true;

                                                    _objCell = cells.Add(DetailsRowNo + 3, 1, "S.No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 2, "Global Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 3, "Old Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 4, "Name");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 5, "Service Address");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 6, "Tariff");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 7, "Customer Status");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 8, "Meter No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 9, "Net Energy Charges");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 10, "Net Fixed Charges");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 11, "Total Bill Amount");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 12, "VAT");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 13, "VAT Percentage");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 14, "Total Bill Amount With Tax");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 15, "Net Arrears");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 16, "Total Bill Amount With Arrears");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 17, "Read Code");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 18, "Usage");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 19, "PaymentLastDate");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 20, "Paid Amount");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;

                                                    for (int n = 0; n < Customers.Count; n++)
                                                    {
                                                        DataRow dr = (DataRow)Customers[n];
                                                        cells.Add(CustomersRowNo, 1, dr["RowNumber"].ToString());
                                                        cells.Add(CustomersRowNo, 2, dr["GlobalAccountNumber"].ToString());
                                                        cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
                                                        cells.Add(CustomersRowNo, 4, dr["Name"].ToString());
                                                        cells.Add(CustomersRowNo, 5, dr["ServiceAddress"].ToString());
                                                        cells.Add(CustomersRowNo, 6, dr["TariffName"].ToString());
                                                        cells.Add(CustomersRowNo, 7, dr["ActiveStatus"].ToString());
                                                        cells.Add(CustomersRowNo, 8, dr["MeterNo"].ToString());
                                                        cells.Add(CustomersRowNo, 9, dr["NetEnergyCharges"].ToString());
                                                        cells.Add(CustomersRowNo, 10, dr["NetFixedCharges"].ToString());
                                                        cells.Add(CustomersRowNo, 11, dr["TotalBillAmount"].ToString());
                                                        cells.Add(CustomersRowNo, 12, dr["VAT"].ToString());
                                                        cells.Add(CustomersRowNo, 13, dr["VATPercentage"].ToString());
                                                        cells.Add(CustomersRowNo, 14, dr["TotalBillAmountWithTax"].ToString());
                                                        cells.Add(CustomersRowNo, 15, dr["NetArrears"].ToString());
                                                        cells.Add(CustomersRowNo, 16, dr["TotalBillAmountWithArrears"].ToString());
                                                        cells.Add(CustomersRowNo, 17, dr["ReadCode"].ToString());
                                                        cells.Add(CustomersRowNo, 18, dr["Usage"].ToString());
                                                        cells.Add(CustomersRowNo, 19, dr["PaymentLastDate"].ToString());
                                                        cells.Add(CustomersRowNo, 20, dr["PaidAmount"].ToString());

                                                        CustomersRowNo++;
                                                    }
                                                    DetailsRowNo = CustomersRowNo + 3;
                                                    CustomersRowNo = CustomersRowNo + 6;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                        fileName = Constants.CustomerBillsReport_Name.Replace(" ", string.Empty) + "_" + fileName;
                        string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                        xls.FileName = filePath;
                        xls.Save();
                        _ObjiDsignBAL.DownLoadFile(filePath, "application//vnd.ms-excel");
                    }
                }
                else
                {
                    Message(Resource.NO_READ_CUST, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            else
            {
                Message(Resource.NO_READ_CUST, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }
        #endregion

        #region Methods
        private void BindDropDowns()
        {
            BindBusinessUnits();
            objCommonMethods.BindBillingTypes(ddlRequestFrom, true);
            BindYears();
            objCommonMethods.BindTotalTariffSubTypes(ddlTariff, 0, true, Resource.DDL_ALL);
        }

        private void BindBusinessUnits()
        {
            objCommonMethods.BindUserBusinessUnits(ddlBusinessUnitName, string.Empty, true, Resource.DDL_ALL);
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
            {
                string BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                ddlBusinessUnitName.SelectedIndex = ddlBusinessUnitName.Items.IndexOf(ddlBusinessUnitName.Items.FindByValue(BUID));
                ddlBusinessUnitName.Enabled = false;
                ddlBusinessUnitName_SelectedIndexChanged(ddlBusinessUnitName, new EventArgs());
            }
            objCommonMethods.BindUserServiceUnits_BuIds(ddlServiceUnitName, BusinessUnitName, Resource.DDL_ALL, true);
            objCommonMethods.BindCycles_BySuId(ddlCycle, this.ServiceUnit, Resource.DDL_ALL, true);
        }

        public void BindGrid()
        {
            RptCustomerBillsBe _objBillsBe = new RptCustomerBillsBe();
            _objBillsBe.BU_ID = this.BusinessUnitName;
            _objBillsBe.SU_ID = this.ServiceUnit;
            _objBillsBe.CycleId = this.CycleIds;
            _objBillsBe.TariffIds = this.TariffIds;
            _objBillsBe.Years = this.YearId.ToString();
            _objBillsBe.Months = this.MonthId.ToString();
            _objBillsBe.BillProcessTypeId = this.RequestFrom;
            _objBillsBe.PageNo = Convert.ToInt32(hfPageNo.Value);
            _objBillsBe.PageSize = PageSize;
            DataSet dsBills = new DataSet();
            dsBills = _objGeneralReportsBal.GetBillsReport(_objBillsBe, ReturnType.Get);

            if (dsBills != null && dsBills.Tables.Count > 0 && dsBills.Tables[0].Rows.Count > 0)
            {
                divToprptrContent.Visible = divExport.Visible = divDownrptrContent.Visible = rptrDetails.Visible = rptrheader.Visible = true;
                divNoDataFound.Visible = false;
                rptrDebitBalence.DataSource = dsBills.Tables[0];
                rptrDebitBalence.DataBind();
                lblTopTotalCustomers.Text = lblDownTotalCustomers.Text = hfTotalRecords.Value = dsBills.Tables[0].Rows[0]["TotalRecords"].ToString();
            }
            else
            {
                divToprptrContent.Visible = divExport.Visible = divDownrptrContent.Visible = rptrheader.Visible = false;
                divNoDataFound.Visible = rptrDetails.Visible = true;
                rptrDebitBalence.DataSource = new DataTable(); ;
                rptrDebitBalence.DataBind();
                lblTopTotalCustomers.Text = lblDownTotalCustomers.Text = hfTotalRecords.Value = Constants.Zero.ToString();
            }
        }
        //private void BindYears()
        //{
        //    try
        //    {
        //        XmlDocument xmlResult = new XmlDocument();
        //        BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
        //        xmlResult = _objBillGenerationBal.GetDetails(ReturnType.Fetch);
        //        BillGenerationListBe objBillsListBE = _ObjiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
        //        if (objBillsListBE.Items.Count > 0)
        //        {
        //            _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlYear, "Year", "Year", true);
        //            BindMonths();
        //        }
        //        //XmlDocument xmlResult = new XmlDocument();
        //        //xmlResult = objBillGenerationBal.GetDetails(ReturnType.Multiple);
        //        //BillGenerationListBe objBillsListBE = _ObjiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
        //        //_ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlYear, "Year", "Year", "ALL", true);

        //        //BindMonths();
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //private void BindMonths()
        //{
        //    try
        //    {
        //        XmlDocument xmlResult = new XmlDocument();
        //        BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
        //        BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
        //        _objBillGenerationBe.Year = Convert.ToInt32(ddlYear.SelectedValue);
        //        xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.Group);
        //        BillGenerationListBe objBillsListBE = _ObjiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
        //        if (objBillsListBE.Items.Count > 0)
        //        {
        //            ddlMonth.Enabled = true;
        //            _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlMonth, "MonthName", "Month", "ALL", true);
        //        }
        //        else
        //        {
        //            ddlMonth.SelectedIndex = 0;
        //            ddlMonth.Enabled = false;
        //        }
        //        //XmlDocument xmlResult = new XmlDocument();
        //        //BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
        //        //_objBillGenerationBe.YearName = this.Year;
        //        //xmlResult = objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.Bulk);
        //        //BillGenerationListBe objBillsListBE = _ObjiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
        //        //_ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlMonth, "MonthName", "Month", "ALL", true);

        //        //oldCode
        //        //DataSet dsMonths = new DataSet();
        //        //dsMonths.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["Months"]));
        //        //_ObjiDsignBAL.FillDropDownList(dsMonths.Tables[0], ddlMonth, "name", "value", false);
        //        //ddlMonth.Items.Insert(0, new ListItem("ALL", ""));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindMonths();
        }

        private void BindYears()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
                xmlResult = _objBillGenerationBal.GetDetails(ReturnType.Bulk);
                BillGenerationListBe objBillsListBE = _ObjiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                if (objBillsListBE.Items.Count > 0)
                {
                    _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlYear, "Year", "Year", "--Select--", true);
                    BindMonths();
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindMonths()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
                _objBillGenerationBe.Year = this.YearId;
                xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.User);
                BillGenerationListBe objBillsListBE = _ObjiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                if (objBillsListBE.Items.Count > 0)
                {
                    ddlMonth.Enabled = true;
                    _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlMonth, "MonthName", "Month", "--Select--", true);
                }
                else
                {
                    ddlMonth.SelectedIndex = 0;
                    ddlMonth.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        protected void ddlServiceUnitName_SelectedIndexChanged(object sender, EventArgs e)
        {
            objCommonMethods.BindCycles_BySuId(ddlCycle, this.ServiceUnit, Resource.DDL_ALL, true);

        }
    }
}