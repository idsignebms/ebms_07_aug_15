﻿<%@ Page Title=":: Customer Ledger ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="CustomerLedger.aspx.cs" Inherits="UMS_Nigeria.Reports.CustomerLedger" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="contentBody" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:Panel ID="pnlSearch" DefaultButton="btnGo" runat="server">
        <asp:UpdatePanel ID="upSearchCustomer" runat="server">
            <ContentTemplate>
                <div class="out-bor">
                    <div class="inner-sec">
                        <asp:Panel ID="pnlMessage" runat="server">
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </asp:Panel>
                        <div class="text_total">
                            <div class="text-heading">
                                <asp:Literal ID="litSearchCustomer" runat="server" Text="Customer Ledger Report"></asp:Literal>
                            </div>
                            <div class="star_text" style="margin-right: 155px;">
                                <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="dot-line">
                        </div>
                        <div class="previous">
                            <a href="ReportsSummary.aspx">< < Back To Summary</a>
                        </div>
                        <div class="inner-box">
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <asp:Literal ID="litAccountNo" runat="server" Text="<%$ Resources:Resource, ACCNO_OLD_ACCNO%>"></asp:Literal>
                                    <span class="span_star">*</span><br />
                                    <asp:TextBox ID="txtAccountNo" runat="server" CssClass="text-box" MaxLength="15"
                                        onblur="TextBoxBlurValidationlbl(this, 'spanAccountNo', 'Global Account No / Old Account No')"
                                        placeholder="Global Account No / Old Account No"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender37" runat="server" TargetControlID="txtAccountNo"
                                        FilterType="Numbers">
                                    </asp:FilteredTextBoxExtender>
                                    <br />
                                    <span id="spanAccountNo" class="span_color"></span>
                                </div>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="box_total">
                            <div class="box_total_a">
                                <asp:Button ID="btnGo" OnClientClick="return Validate();" Text="<%$ Resources:Resource, GET_CUSTOMER_DETAILS%>"
                                    OnClick="btnGo_Click" CssClass="box_s" runat="server" />
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div id="divCustomerDetails" runat="server" visible="false">
                            <div class="inner-sec">
                                <div class="heading" style="padding-left: 16px; padding-top: 17px;">
                                    <asp:Literal ID="Literal213" runat="server" Text="<%$ Resources:Resource, CUSTOMER_DETAILS%>"></asp:Literal>
                                </div>
                                <div class="out-bor_aTwo">
                                    <table class="customerdetailsTamle">
                                        <tr>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label runat="server" ID="lblGlobalAccNo"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="lit" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label runat="server" ID="lblOldAccNo"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="litName" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label runat="server" ID="lblName"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label ID="lblAddress" runat="server"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, EMAIL_ID%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label ID="lblEmailId" runat="server"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, MOBILE_NO%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label ID="lblMobileNo" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, READCODE%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label ID="lblReadCode" runat="server"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label ID="lblTariff" runat="server"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label ID="lblMeterNo" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:Resource, AVERAGE_READING%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label ID="lblAvgReading" runat="server"></asp:Label>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, OUT_STANDING_AMT%>"></asp:Literal>
                                            </td>
                                            <td class="customerdetailstamleTd">
                                                :
                                            </td>
                                            <td class="customerdetailstamleTdsize">
                                                <asp:Label runat="server" ID="lblOutStanding"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="clear">
                                <br />
                            </div>
                            <div class="rptr_tb" runat="server" id="divrptr" visible="false">
                                <div align="right" class="fltr">
                                    <asp:Button ID="btnExportExcel" OnClick="btnExportExcel_Click" runat="server" CssClass="exporttoexcelButton" />
                                </div>
                                <div class="clear">
                                </div>
                                <div class="out-bor_Reports">
                                    <table style="border-collapse: collapse;">
                                        <tr class="rptr_tb_hd" align="center">
                                            <th>
                                                S.No
                                            </th>
                                            <th>
                                                Transaction Date
                                            </th>
                                            <th>
                                                Reference No
                                            </th>
                                            <th>
                                                Description
                                            </th>
                                            <th>
                                                Remarks
                                            </th>
                                            <th>
                                                Debit
                                            </th>
                                            <th>
                                                Credit
                                            </th>
                                            <th>
                                                Balance
                                            </th>
                                        </tr>
                                        <asp:Repeater ID="rptrLedger" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="rptr_tb_td" align="center">
                                                        <asp:Label runat="server" ID="lblsno" Text='<%#Eval("RowNumber") %>'></asp:Label>
                                                    </td>
                                                    <td class="rptr_tb_td" align="center">
                                                        <asp:Label runat="server" ID="lblTransactionDate" Text='<%#Eval("TransactionDate") %>'></asp:Label>
                                                    </td>
                                                    <td class="rptr_tb_td" align="center">
                                                        <asp:LinkButton ID="lnkReferenceNo" runat="server" CommandArgument='<%#Eval("ReferenceNo") %>'
                                                            Text='<%#Eval("ReferenceNo") %>' Enabled="false" ForeColor="Black" OnClick="lnkReferenceNo_Click"></asp:LinkButton>
                                                    </td>
                                                    <td class="rptr_tb_td" align="left">
                                                        <asp:Label runat="server" ID="lblParticulars" Text='<%#Eval("Particulars") %>'></asp:Label>
                                                    </td>
                                                    <td class="rptr_tb_td" align="left">
                                                        <asp:Label runat="server" ID="lblRemarks" Text='<%#Eval("Remarks") %>'></asp:Label>
                                                    </td>
                                                    <td class="rptr_tb_td" align="right">
                                                        <asp:Label runat="server" ID="lblCredit" Text='<%#Eval("Credit") %>'></asp:Label>
                                                    </td>
                                                    <td class="rptr_tb_td" align="right">
                                                        <asp:Label runat="server" ID="lblDebit" Text='<%#Eval("Debit") %>'></asp:Label>
                                                    </td>
                                                    <td class="rptr_tb_td" align="right">
                                                        <asp:Label runat="server" ID="lblBalance" Text='<%#Eval("Balance") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <tr align="right">
                                            <td>
                                                &nbsp
                                            </td>
                                            <td>
                                                &nbsp
                                            </td>
                                            <td>
                                                &nbsp
                                            </td>
                                            <td>
                                                &nbsp
                                            </td>
                                            <td>
                                                &nbsp
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblTotCredit" Text=''></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblTotDebit" Text=''></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblTotBalance" Text=''></asp:Label>
                                            </td>
                                        </tr>
                                        <%--<tr align="center" > <td></td>No data found</tr>--%>
                                    </table>
                                </div>
                            </div>
                            <asp:HiddenField ID="hfTotalCredit" runat="server" />
                            <asp:HiddenField ID="hfTotalDebit" runat="server" />
                        </div>
                    </div>
                </div>
                <%-- Popup --%>
                <asp:ModalPopupExtender ID="mpeRefDetails" runat="server" TargetControlID="btnpopup"
                    PopupControlID="pnlChangePwd" BackgroundCssClass="popbg">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnpopup" runat="server" Text="" Style="display: none;" />
                <asp:Panel ID="pnlChangePwd" runat="server" CssClass="editpopup_two" Style="display: none;
                    width: 720px; left: 259.5px; position: fixed; z-index: 100001; top: 111.5px;">
                    <h3 class="edithead" align="left" style="background: none repeat scroll 0 0 #008000;
                        border-bottom: 1px solid #CCCCCC; color: #FFFFFF; margin-bottom: 20px; padding: 10px 10px 8px 14px;">
                        <asp:Literal ID="Literal26" runat="server" Text="Bill Details"></asp:Literal>
                        &nbsp;:&nbsp;
                        <asp:Label ID="lblBillId" runat="server"></asp:Label>
                    </h3>
                    <div class="adjust-meter">
                        <ul style="width: 357px; float: left;">
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal></li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label runat="server" ID="lblPopAccNo"></asp:Label>
                            </li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal19" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal></li><li
                                    class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label runat="server" ID="lblPopFullName"></asp:Label>
                            </li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal25" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT_NAME%>"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblPopBU" runat="server"></asp:Label></li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal37" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT_NAME%>"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblPopSU" runat="server"></asp:Label></li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER_NAME%>"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblPopSC" runat="server"></asp:Label></li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, CYCLE_NAME%>"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblPopCycle" runat="server"></asp:Label></li>
                            <li class="clear pad_5"></li>
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal>
                            </li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label ID="lblPopBookNo" runat="server"></asp:Label></li>
                            <li class="clear pad_5"></li>
                        </ul>
                        <ul style="width: 357px; float: left;">
                            <li class="prevleft fltl">
                                <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, ADDRESS%>"></asp:Literal></li>
                            <li class="middle fltl">:</li>
                            <li class="prevright fltl">
                                <asp:Label runat="server" ID="lblPopAddress"></asp:Label>
                                <li class=" clear pad_5"></li>
                                <li class="prevleft fltl">
                                    <asp:Literal ID="Literal57" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Literal>
                                </li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <asp:Label ID="lblPopMeterNo" runat="server"></asp:Label>
                                </li>
                                <li class="clear pad_5"></li>
                                <li class="prevleft fltl">
                                    <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                </li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <asp:Label ID="lblPopOldAccNo" runat="server"></asp:Label>
                                </li>
                                <li class=" clear pad_5"></li>
                                <li class="prevleft fltl">
                                    <asp:Literal ID="Literal56" runat="server" Text='<%$ Resources:Resource, MOBILE_NO %>'></asp:Literal>
                                </li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <asp:Label ID="lblPopMobileNo" runat="server"></asp:Label></li>
                                <li class="clear pad_5"></li>
                                <li class="prevleft fltl">
                                    <asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                                </li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <asp:Label ID="lblPopTariff" runat="server"></asp:Label></li>
                                <li class="clear pad_5"></li>
                                <li class="prevleft fltl">
                                    <asp:Literal ID="Literal39" runat="server" Text="Total Before Adjustment"></asp:Literal>
                                </li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <asp:Label ID="lblTotalBill" runat="server"></asp:Label></li>
                                <li class="clear pad_5"></li>
                                <li class="prevleft fltl" style="line-height: 13px;">
                                    <asp:Literal ID="Literal40" runat="server" Text="Total After Adjustment"></asp:Literal>
                                </li>
                                <li class="middle fltl">:</li>
                                <li class="prevright fltl">
                                    <asp:Label ID="lblTotalAfterAdjustment" runat="server"></asp:Label></li>
                                <li class="clear pad_5"></li>
                                <asp:HiddenField ID="hdnCustomerBillID" runat="server" Visible="false" />
                                <asp:HiddenField ID="hdnCustomerID" runat="server" Visible="false" />
                                <asp:HiddenField ID="hfGlobalAccNo" runat="server" />
                    </div>
                    <div class="clear pad_10">
                    </div>
                    <div class="consumer_feild">
                        <div style="margin-left: 556px; overflow: auto; float: left; height: 43px;">
                            <asp:Button ID="btnCancel" Text="<%$ Resources:Resource, CANCEL%>" CssClass="calculate_but"
                                runat="server" />
                        </div>
                    </div>
                </asp:Panel>
                <%--Popup--%>
                <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="PanelAlert"
                    TargetControlID="btnAlertDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnAlertOk">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnAlertDeActivate" runat="server" Text="Button" CssClass="popbtn" />
                <asp:Panel ID="PanelAlert" runat="server" CssClass="modalPopup" class="poppnl" Style="display: none;">
                    <div class="popheader popheaderlblred" id="pop" runat="server">
                        <asp:Label ID="lblAlertMsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnAlertOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnExportExcel" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/CustomerLedger.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
