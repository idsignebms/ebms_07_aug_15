﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using iDSignHelper;
using UMS_NigeriaBE;
using System.Globalization;
using System.Threading;
using UMS_NigriaDAL;
using UMS_NigeriaBAL;
using System.Data;
using System.Xml;
using System.Drawing;
using org.in2bits.MyXls;
using org.in2bits.MyXls.ByteUtil;
using System.Configuration;

namespace UMS_Nigeria.Reports
{
    public partial class CustomerLedger : System.Web.UI.Page
    {

        #region Members
        iDsignBAL _objIdsignBal = new iDsignBAL();
        ReportsBal _objReportsBal = new ReportsBal();
        GeneralReportsBal _objGeneralReportsBal = new GeneralReportsBal();
        CommonMethods _objCommonMethods = new CommonMethods();
        public string Key = "Customer Ledger";

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.CssClass = lblMessage.Text = string.Empty;
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString[UMS_Resource.QSTR_ACC_NO]))
                {
                    txtAccountNo.Text = _objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.QSTR_ACC_NO]);
                }
            }
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                getCustomerLedger();

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lnkReferenceNo_Click(object sender, EventArgs e)
        {

            try
            {
                RptCustomerLedgerBe _objLedgerBe = new RptCustomerLedgerBe();
                RptCustomerLedgerListBe _objRptCustomerLedgerListBe = new RptCustomerLedgerListBe();
                RptCustomerLedgerDetailsListBe _objRptCustomerLedgerDetailsListBe = new RptCustomerLedgerDetailsListBe();
                _objLedgerBe.AccountNo = hfGlobalAccNo.Value;
                _objLedgerBe.ReportMonths = Constants.ReportMonths;
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    _objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    _objLedgerBe.BUID = string.Empty;
                LinkButton lnkReferenceNo = (LinkButton)sender;
                _objLedgerBe.ReferenceNo = lnkReferenceNo.CommandArgument;

                XmlDocument xmlResult = _objReportsBal.GetLedger(_objLedgerBe, ReturnType.User);
                _objRptCustomerLedgerListBe = _objIdsignBal.DeserializeFromXml<RptCustomerLedgerListBe>(xmlResult);
                _objRptCustomerLedgerDetailsListBe = _objIdsignBal.DeserializeFromXml<RptCustomerLedgerDetailsListBe>(xmlResult);
                if (_objRptCustomerLedgerDetailsListBe.items.Count > 0)
                {
                    _objLedgerBe = _objRptCustomerLedgerDetailsListBe.items[0];
                }
                if (_objLedgerBe.IsSuccess)
                {
                    lblBillId.Text = lnkReferenceNo.CommandArgument;
                    lblPopAccNo.Text = _objLedgerBe.AccountNo;
                    lblPopFullName.Text = _objLedgerBe.Name;
                    lblPopAddress.Text = _objLedgerBe.ServiceAddress;
                    lblPopMeterNo.Text = _objLedgerBe.MeterNo;
                    lblPopTariff.Text = _objLedgerBe.ClassName;
                    lblPopOldAccNo.Text = _objLedgerBe.OldAccountNo;
                    lblPopMobileNo.Text = _objLedgerBe.MobileNo;
                    lblPopBU.Text = _objLedgerBe.BusinessUnitName;
                    lblPopSU.Text = _objLedgerBe.ServiceUnitName;
                    lblPopSC.Text = _objLedgerBe.ServiceCenterName;
                    lblPopCycle.Text = _objLedgerBe.CycleName;
                    lblPopBookNo.Text = _objLedgerBe.BookNo;
                    mpeRefDetails.Show();

                    hfTotalDebit.Value = (from x in _objRptCustomerLedgerListBe.items.AsEnumerable()
                                          select x.Debit).Sum().ToString();
                    hfTotalCredit.Value = (from x in _objRptCustomerLedgerListBe.items.AsEnumerable()
                                           select x.Credit).Sum().ToString();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        private void getCustomerLedger()
        {
            try
            {
                LedgerBe _objLedgerBe = new LedgerBe();
                _objLedgerBe.GlobalAccountNo = txtAccountNo.Text;
                _objLedgerBe.ReportMonths = Constants.ReportMonths;
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    _objLedgerBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    _objLedgerBe.BU_ID = string.Empty;

                DataSet dsLedger = new DataSet();
                dsLedger = _objGeneralReportsBal.GetLedgerReport(_objLedgerBe, ReturnType.Get);

                if (dsLedger != null && dsLedger.Tables.Count > 0)
                {
                    DataRow drLedger = dsLedger.Tables[0].NewRow();
                    drLedger = dsLedger.Tables[0].Rows[0];

                    if (drLedger["IsSuccess"].ToString() == "1")
                    {
                        if (drLedger["IsCustExistsInOtherBU"].ToString() == "0")
                        {
                            divCustomerDetails.Visible = true;
                            hfGlobalAccNo.Value = drLedger["AccountNo"].ToString();
                            lblGlobalAccNo.Text = drLedger["GlobalAccountNumber"].ToString();
                            lblOldAccNo.Text = drLedger["OldAccountNo"].ToString();
                            lblName.Text = drLedger["Name"].ToString();
                            lblOutStanding.Text = drLedger["OutStandingAmount"].ToString();
                            lblReadCode.Text = drLedger["ReadCode"].ToString();
                            lblMobileNo.Text = drLedger["MobileNo"].ToString();
                            lblAddress.Text = drLedger["ServiceAddress"].ToString();
                            lblTariff.Text = drLedger["ClassName"].ToString();
                            lblAvgReading.Text = drLedger["AvgReading"].ToString();
                            lblEmailId.Text = drLedger["EmailId"].ToString();
                            lblMeterNo.Text = drLedger["MeterNumber"].ToString();

                            if (dsLedger.Tables.Count > 1)
                            {
                                divrptr.Visible = true;
                                rptrLedger.DataSource = dsLedger.Tables[1];
                                rptrLedger.DataBind();

                                var result = (from x in dsLedger.Tables[1].AsEnumerable()
                                              select x).ToList();

                                lblTotBalance.Text = result.Select(g => Convert.ToDecimal(g.Field<string>("Balance").Replace(",", ""))).Sum().ToString();
                                lblTotCredit.Text = result.Select(g => Convert.ToDecimal(g.Field<string>("Credit").Replace(",", ""))).Sum().ToString();
                                lblTotDebit.Text = result.Select(g => Convert.ToDecimal(g.Field<string>("Debit").Replace(",", ""))).Sum().ToString();

                                //lblTotBalance.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblTotBalance.Text), 2, Constants.MILLION_Format);
                                lblTotCredit.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblTotCredit.Text), 2, Constants.MILLION_Format);
                                lblTotDebit.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblTotDebit.Text), 2, Constants.MILLION_Format);
                                lblTotBalance.Text = _objCommonMethods.GetCurrencyFormat((Convert.ToDecimal(lblTotCredit.Text) - Convert.ToDecimal(lblTotDebit.Text)), 2, Constants.MILLION_Format);
                            }
                        }
                        else
                        {
                            txtAccountNo.Text = string.Empty;
                            divCustomerDetails.Visible = divrptr.Visible = false;
                            pop.Attributes.Add("class", "popheader popheaderlblred");
                            lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                            mpeAlert.Show();
                        }
                    }
                    else
                    {
                        txtAccountNo.Text = string.Empty;
                        divCustomerDetails.Visible = divrptr.Visible = false;
                        pop.Attributes.Add("class", "popheader popheaderlblred");
                        lblAlertMsg.Text = "Customer Account number doesn't exist. Please check account number and try again.";
                        mpeAlert.Show();
                    }
                }

                #region OldCode
                //RptCustomerLedgerBe _objLedgerBe = new RptCustomerLedgerBe();
                //RptCustomerLedgerListBe _objRptCustomerLedgerListBe = new RptCustomerLedgerListBe();
                //RptCustomerLedgerDetailsListBe _objRptCustomerLedgerDetailsListBe = new RptCustomerLedgerDetailsListBe();
                //_objLedgerBe.AccountNo = txtAccountNo.Text.Trim();
                //// _objLedgerBe.OldAccountNo = txtOldAcountNo.Text.Trim();
                //_objLedgerBe.ReportMonths = Constants.ReportMonths;
                //if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                //    _objLedgerBe.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                //else
                //    _objLedgerBe.BUID = string.Empty;
                //XmlDocument xmlResult = _objReportsBal.GetLedger(_objLedgerBe, ReturnType.Bulk);
                //_objRptCustomerLedgerListBe = _objIdsignBal.DeserializeFromXml<RptCustomerLedgerListBe>(xmlResult);
                //_objRptCustomerLedgerDetailsListBe = _objIdsignBal.DeserializeFromXml<RptCustomerLedgerDetailsListBe>(xmlResult);
                //if (_objRptCustomerLedgerDetailsListBe.items.Count > 0)
                //{
                //    _objLedgerBe = _objRptCustomerLedgerDetailsListBe.items[0];
                //}
                //if (_objLedgerBe.IsSuccess)
                //{
                //    if (_objLedgerBe.IsCustExistsInOtherBU)
                //    {
                //        lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                //        mpeAlert.Show();
                //    }
                //    else
                //    {
                //        lblGlobalAccNo.Text = _objLedgerBe.AccountNo;
                //        lblAddress.Text = _objLedgerBe.ServiceAddress;
                //        lblName.Text = _objLedgerBe.Name;
                //        lblMeterNo.Text = _objLedgerBe.MeterNo;
                //        lblTariff.Text = _objLedgerBe.ClassName;

                //        hfTotalDebit.Value = (from x in _objRptCustomerLedgerListBe.items.AsEnumerable()
                //                              select x.Debit).Sum().ToString();
                //        hfTotalCredit.Value = (from x in _objRptCustomerLedgerListBe.items.AsEnumerable()
                //                               select x.Credit).Sum().ToString();

                //        if (_objRptCustomerLedgerListBe.items.Count > 1)
                //        {
                //            rptrLedger.DataSource = _objRptCustomerLedgerListBe.items;
                //            rptrLedger.DataBind();
                //        }
                //        else
                //        {
                //            CustomerNotFound();
                //            Message("No payment made by this customer so far.", UMS_Resource.MESSAGETYPE_ERROR);
                //        }
                //    }
                //}
                //else
                //{
                //    CustomerNotFound();
                //    Message("Customer Account number doesn't exist. Please check account number and try again.", UMS_Resource.MESSAGETYPE_ERROR);
                //}
                #endregion
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

        #region ExportToExcel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            LedgerBe _objLedgerBe = new LedgerBe();
            _objLedgerBe.GlobalAccountNo = txtAccountNo.Text;
            _objLedgerBe.ReportMonths = Constants.ReportMonths;
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                _objLedgerBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                _objLedgerBe.BU_ID = string.Empty;

            DataSet dsLedger = new DataSet();
            dsLedger = _objGeneralReportsBal.GetLedgerReport(_objLedgerBe, ReturnType.Get);

            if (dsLedger != null && dsLedger.Tables.Count > 0)
            {
                DataRow drLedger = dsLedger.Tables[0].NewRow();
                drLedger = dsLedger.Tables[0].Rows[0];

                XlsDocument xls = new XlsDocument();
                Worksheet mainSheet = xls.Workbook.Worksheets.Add(Constants.CustomerLedgerReport_Name);
                Cells mainCells = mainSheet.Cells;
                Cell _objMainCells = null;
                int i = 12, j = 1;

                _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
                mainCells.Add(4, 2, "Report Code ");
                mainCells.Add(4, 3, Constants.CustomerLedgerReport_Code).Font.Bold = true;
                mainCells.Add(4, 4, "Report Name");
                mainCells.Add(4, 5, Constants.CustomerLedgerReport_Name).Font.Bold = true;
                mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                _objMainCells.Font.FontFamily = FontFamilies.Roman;


                mainCells.Add(7, 1, Resource.ACCOUNT_NO);
                mainCells.Add(7, 4, Resource.OLD_ACCOUNT_NO);
                mainCells.Add(7, 7, Resource.NAME);
                mainCells.Add(8, 1, Resource.SERVICE_ADDRESS);
                mainCells.Add(8, 4, Resource.EMAIL_ID);
                mainCells.Add(8, 7, Resource.MOBILE_NO);
                mainCells.Add(9, 1, Resource.READCODE);
                mainCells.Add(9, 4, Resource.TARIFF);
                mainCells.Add(9, 7, Resource.METER_NO);
                mainCells.Add(10, 1, Resource.AVERAGE_READING);
                mainCells.Add(10, 4, Resource.OUT_STANDING_AMT);

                _objMainCells = mainCells.Add(7, 2, drLedger["GlobalAccountNumber"].ToString());
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells = mainCells.Add(7, 5, drLedger["OldAccountNo"].ToString());
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells = mainCells.Add(7, 8, drLedger["Name"].ToString());
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells = mainCells.Add(8, 2, drLedger["ServiceAddress"].ToString());
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells = mainCells.Add(8, 5, drLedger["EmailId"].ToString());
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells = mainCells.Add(8, 8, drLedger["MobileNo"].ToString());
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells = mainCells.Add(9, 2, drLedger["ReadCode"].ToString());
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells = mainCells.Add(9, 5, drLedger["ClassName"].ToString());
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells = mainCells.Add(9, 8, drLedger["MeterNumber"].ToString());
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells = mainCells.Add(10, 2, drLedger["AvgReading"].ToString());
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells = mainCells.Add(10, 5, drLedger["OutStandingAmount"].ToString());
                _objMainCells.Font.Weight = FontWeight.ExtraBold;

                _objMainCells = mainCells.Add(i, 1, "S.No");
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells = mainCells.Add(i, 2, "Transaction Date");
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells = mainCells.Add(i, 3, "Reference No");
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells = mainCells.Add(i, 4, "Description");
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells = mainCells.Add(i, 5, "Remarks");
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells = mainCells.Add(i, 6, "Debit");
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells = mainCells.Add(i, 7, "Credit");
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells = mainCells.Add(i, 8, "Balance");
                _objMainCells.Font.Weight = FontWeight.ExtraBold;

                foreach (DataRow dr in dsLedger.Tables[1].Rows)
                {
                    i++;
                    mainCells.Add(i, 1, dr["RowNumber"].ToString());
                    mainCells.Add(i, 2, dr["TransactionDate"].ToString());
                    mainCells.Add(i, 3, dr["ReferenceNo"].ToString());
                    mainCells.Add(i, 4, dr["Particulars"].ToString());
                    mainCells.Add(i, 5, dr["Remarks"].ToString());
                    mainCells.Add(i, 6, dr["Credit"].ToString());
                    mainCells.Add(i, 7, dr["Debit"].ToString());
                    mainCells.Add(i, 8, dr["Balance"].ToString());
                    j++;
                }

                var result = (from x in dsLedger.Tables[1].AsEnumerable()
                              select x).ToList();

                //decimal TotBalance = result.Select(g => Convert.ToDecimal(g.Field<string>("Balance").Replace(",", ""))).Sum();
                decimal TotCredit = result.Select(g => Convert.ToDecimal(g.Field<string>("Credit").Replace(",", ""))).Sum();
                decimal TotDebit = result.Select(g => Convert.ToDecimal(g.Field<string>("Debit").Replace(",", ""))).Sum();
                decimal TotBalance = TotCredit - TotDebit;

                mainCells.Add(i + 1, 6, _objCommonMethods.GetCurrencyFormat(TotCredit, 2, Constants.MILLION_Format));
                mainCells.Add(i + 1, 7, _objCommonMethods.GetCurrencyFormat(TotDebit, 2, Constants.MILLION_Format));
                mainCells.Add(i + 1, 8, _objCommonMethods.GetCurrencyFormat(TotBalance, 2, Constants.MILLION_Format));

                string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                fileName = Constants.CustomerLedgerReport_Name.Replace(" ", string.Empty) + "_" + fileName;
                string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                xls.FileName = filePath;
                xls.Save();
                _objIdsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");

            }
            else
            {
                Message("No Transactions are found for this Customer.", UMS_Resource.MESSAGETYPE_ERROR);
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}