﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for Customer Change Logs
                     
 Developer        : Id077-Neeraj Kanojiya
 Creation Date    : 01-Aug-15
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using Resources;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Data;
using org.in2bits.MyXls;
using System.Configuration;
using System.Xml;

namespace UMS_Nigeria.Reports
{
    public partial class CustomerRegistrationApprovalLog : System.Web.UI.Page
    {
        #region Members
        iDsignBAL objIdsignBal = new iDsignBAL();
        ReportsBal objReportsBal = new ReportsBal();
        CommonMethods objCommonMethods = new CommonMethods();
        string Key = UMS_Resource.KEY_CustomerChangeLogs;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStartsReport : Convert.ToInt32(hfPageSize.Value); }
        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                try
                {
                    trBU.Visible = trSU.Visible = trSC.Visible = trCycle.Visible = trBook.Visible = trTariff.Visible = trFD.Visible = trTD.Visible = false;
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    BindCustomerChangeLogs();
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    UCPaging2.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));

                }
                catch (Exception ex)
                {
                    Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                    try
                    {
                        objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                    }
                    catch (Exception logex)
                    {

                    }
                }
            }
        }
        protected void lbtnBack_Click(object sender, EventArgs e)
        {
            //Response.Redirect(Constants.AuditTrayReportPage + Request.Url.Query, false);
            Response.Redirect(Constants.AuditTrayReportPage, false);
        }
        protected void lnkgvCustomerRegistrationDetails_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkApprove = (LinkButton)sender;
                hfARID.Value = lnkApprove.CommandArgument;
                GetCustomerDetail();
                mpeCustomerDetails.Show();
            }
            catch (Exception ex)
            {
                objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, ex.Message, pnlMessage, lblMessage);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods
        private void BindCustomerChangeLogs()
        {
            try
            {
                if ((Session[UMS_Resource.AUDIT_SESSION_DT] != null) && (Request.QueryString[UMS_Resource.FROM_PAGE] != null))
                {
                    DataTable dt = (DataTable)Session[UMS_Resource.AUDIT_SESSION_DT];
                    //Display Start
                    if (!string.IsNullOrEmpty(dt.Rows[0]["BUName"].ToString())) { trBU.Visible = true; lblBU.Text = dt.Rows[0]["BUName"].ToString(); }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["SUName"].ToString())) { trSU.Visible = true; lblSU.Text = dt.Rows[0]["SUName"].ToString(); }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["SCName"].ToString())) { trSC.Visible = true; lblSC.Text = dt.Rows[0]["SCName"].ToString(); }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["CycleName"].ToString())) { trCycle.Visible = true; lblCycle.Text = dt.Rows[0]["CycleName"].ToString(); }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["BookName"].ToString())) { trBook.Visible = true; lblBook.Text = dt.Rows[0]["BookName"].ToString(); }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["TariffName"].ToString())) { trTariff.Visible = true; lblTariff.Text = dt.Rows[0]["TariffName"].ToString(); }
                    if (!string.IsNullOrEmpty(objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.FROM_PAGE])))
                    {
                        trFD.Visible = true;
                        lblFromDate.Text = objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.FROM_PAGE]);
                    }
                    if (!string.IsNullOrEmpty(objIdsignBal.Decrypt(Request.QueryString["To"])))
                    {
                        trTD.Visible = true;
                        lblToDate.Text = objIdsignBal.Decrypt(Request.QueryString["To"]);
                    }
                    //Display End

                    RptAuditTrayListBe objRptAuditTrayList = new RptAuditTrayListBe();
                    RptAuditTrayBe objRptAuditTrayBe = new RptAuditTrayBe();
                    objRptAuditTrayBe.BU_ID = dt.Rows[0]["BU_ID"].ToString();
                    objRptAuditTrayBe.SU_ID = dt.Rows[0]["SU_ID"].ToString();
                    objRptAuditTrayBe.SC_ID = dt.Rows[0]["SC_ID"].ToString();
                    objRptAuditTrayBe.CycleId = dt.Rows[0]["CYCLE_ID"].ToString();
                    objRptAuditTrayBe.NewBookNo = dt.Rows[0]["BOOK_ID"].ToString();
                    objRptAuditTrayBe.ClassId = dt.Rows[0]["TARIFF_ID"].ToString();
                    objRptAuditTrayBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                    objRptAuditTrayBe.PageSize = PageSize;
                    if (!string.IsNullOrEmpty(objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.FROM_PAGE])))
                        objRptAuditTrayBe.FromDate = objCommonMethods.Convert_MMDDYY(objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.FROM_PAGE]));
                    else
                        objRptAuditTrayBe.FromDate = string.Empty;
                    if (!string.IsNullOrEmpty(objIdsignBal.Decrypt(Request.QueryString["To"])))
                        objRptAuditTrayBe.ToDate = objCommonMethods.Convert_MMDDYY(objIdsignBal.Decrypt(Request.QueryString["To"]));
                    else
                        objRptAuditTrayBe.ToDate = string.Empty;

                    DataSet ds = new DataSet();
                    ds = objReportsBal.GetApprovalRegistrationsAuditRay(objRptAuditTrayBe);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        divrptrdetails.Visible = true;
                        gvCustomerRegistration.DataSource = ds.Tables[0];
                        //MergeRows(gvCustomerChanges);
                        gvCustomerRegistration.DataBind();

                        if (ds.Tables.Count > 1) lblTopTotalCustomers.Text = lblDownTotalCustomers.Text = hfTotalRecords.Value = ds.Tables[1].Rows[0]["Count"].ToString();
                        else lblTopTotalCustomers.Text = lblDownTotalCustomers.Text = "N/A";
                    }
                    else
                    {
                        divrptrdetails.Visible = false;
                        hfTotalRecords.Value = Constants.Zero.ToString();
                        gvCustomerRegistration.DataSource = new DataTable();
                        gvCustomerRegistration.DataBind();
                    }
                }
                else
                {
                    Response.Redirect(Constants.AuditTrayReportPage, false);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        public void GetCustomerDetail()
        {
            try
            {
                CustomerRegistrationBE objCustomerRegistrationBE = new CustomerRegistrationBE();
                XmlDocument xml = new XmlDocument();
                ConsumerBal objConsumerBal = new ConsumerBal();


                ChangeCustomerTypeBE objChangeCustomerTypeBE = new ChangeCustomerTypeBE();
                objChangeCustomerTypeBE.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                objChangeCustomerTypeBE.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objChangeCustomerTypeBE.FunctionId = (int)EnumApprovalFunctions.CustomerRegistration;

                objChangeCustomerTypeBE.ARID = Convert.ToInt32(hfARID.Value);
                xml = objConsumerBal.GetCustomerDetailsByApprovalID(objChangeCustomerTypeBE);
                if (xml != null)
                {

                    objCustomerRegistrationBE = objIdsignBal.DeserializeFromXml<CustomerRegistrationBE>(xml);
                    lblGlobalAccountNo.Text = objCustomerRegistrationBE.GlobalAccountNumber;
                    lblBuDsp.Text = objCustomerRegistrationBE.BusinessUnit;
                    lblSUDsp.Text = objCustomerRegistrationBE.ServiceUnitName;
                    lblSCDsp.Text = objCustomerRegistrationBE.ServiceCenterName;
                    lblBookGroupDsp.Text = objCustomerRegistrationBE.CycleName;
                    lblBookCodeDsp.Text = objCustomerRegistrationBE.BookCode;
                    #region Postal AddressDetails

                    #region Postal
                    lblServiceHouseNo.Text = lblPostalHouseNo.Text = objCustomerRegistrationBE.HouseNoPostal;
                    lblServiceStreet.Text = lblPostalStreet.Text = objCustomerRegistrationBE.StreetPostal;
                    lblServiceVillage.Text = lblPostalVillage.Text = objCustomerRegistrationBE.CityPostaL;
                    lblServiceZip.Text = lblPostalZip.Text = objCustomerRegistrationBE.PZipCode;

                    if (objCustomerRegistrationBE.AreaPostal != "0" && !string.IsNullOrEmpty(objCustomerRegistrationBE.AreaPostal)) lblPostalAreaCode.Text = objCustomerRegistrationBE.AreaPostal;
                    if (objCustomerRegistrationBE.AreaService != "0" && !string.IsNullOrEmpty(objCustomerRegistrationBE.AreaService)) lblServiceAreaCode.Text = objCustomerRegistrationBE.AreaService;
                    if (objCustomerRegistrationBE.IsSameAsService) lblServiceAreaCode.Text = lblPostalAreaCode.Text = objCustomerRegistrationBE.AreaPostal;
                    if (objCustomerRegistrationBE.IsCommunicationPostal) lblCommunicationAddress.Text = "Postal";
                    #endregion

                    #region Service
                    if (objCustomerRegistrationBE.ServiceAddressID > 0)
                    {
                        if (objCustomerRegistrationBE.IsCommunicationService) lblCommunicationAddress.Text = "Service";
                        lblServiceHouseNo.Text = objCustomerRegistrationBE.HouseNoService;
                        lblServiceStreet.Text = objCustomerRegistrationBE.StreetService;
                        lblServiceVillage.Text = objCustomerRegistrationBE.CityService;
                        lblServiceZip.Text = objCustomerRegistrationBE.SZipCode;
                        lblServiceAreaCode.Text = objCustomerRegistrationBE.AreaService;
                        lblsSameAsService.Text = "No";
                    }
                    #endregion


                    #endregion

                    #region Tenent Details
                    if (!objCustomerRegistrationBE.IsSameAsTenent)
                    {
                        divTenent.Visible = true;
                        //objCustomerRegistrationBE.TitleTanent = ddlTTitle.SelectedItem.Text;
                        lblTanentTitle.Text = objCustomerRegistrationBE.TitleTanent;
                        lblTanentFirstName.Text = objCustomerRegistrationBE.FirstNameTanent;
                        lblTanentMiddleName.Text = objCustomerRegistrationBE.MiddleNameTanent;
                        lblTanentLastName.Text = objCustomerRegistrationBE.LastNameTanent;
                        lblTanentHomeContactNo.Text = objCustomerRegistrationBE.PhoneNumberTanent;
                        lblTanentOtherContactNo.Text = objCustomerRegistrationBE.AlternatePhoneNumberTanent;
                        lblTanentEmail.Text = objCustomerRegistrationBE.EmailIdTanent;
                    }
                    else divTenent.Visible = false;
                    #endregion

                    #region Customer Details
                    lblCustomerStatus.Text = objCustomerRegistrationBE.Status;
                    lblOutStanding.Text = objCommonMethods.GetCurrencyFormat(objCustomerRegistrationBE.OutStandingAmount, 2, Constants.MILLION_Format);
                    lblCustomerType.Text = objCustomerRegistrationBE.CustomerType;
                    lblLInfoTitle.Text = objCustomerRegistrationBE.TitleLandlord;
                    //objCustomerRegistrationBE.TitleLandlord = ddlOTitle.SelectedItem.Text;
                    lblLInfoFirstname.Text = objCustomerRegistrationBE.FirstNameLandlord;
                    lblLInfoMiddleName.Text = objCustomerRegistrationBE.MiddleNameLandlord;
                    lblLInfoLastName.Text = objCustomerRegistrationBE.LastNameLandlord;
                    lblLInfoHomePhone.Text = objCustomerRegistrationBE.HomeContactNumberLandlord;
                    lblLInfoBussPhone.Text = objCustomerRegistrationBE.BusinessPhoneNumberLandlord;
                    lblLInfoOtherPhone.Text = objCustomerRegistrationBE.OtherPhoneNumberLandlord;
                    lblLInfoEmail.Text = objCustomerRegistrationBE.EmailIdLandlord;
                    lblLInfoKnownAs.Text = objCustomerRegistrationBE.KnownAs;
                    //objCustomerRegistrationBE.IsSameAsService;
                    //objCustomerRegistrationBE.IsSameAsTenent ;
                    //if (fupDocument.HasFile) objCustomerRegistrationBE.DocumentNo;
                    if (!string.IsNullOrEmpty(objCustomerRegistrationBE.ApplicationDate)) lblApplicationDate.Text = objCustomerRegistrationBE.ApplicationDate;
                    if (!string.IsNullOrEmpty(objCustomerRegistrationBE.ConnectionDate)) lblConnectionDate.Text = objCustomerRegistrationBE.ConnectionDate;
                    if (!string.IsNullOrEmpty(objCustomerRegistrationBE.SetupDate)) lblSetUpDate.Text = objCustomerRegistrationBE.SetupDate;
                    if (objCustomerRegistrationBE.IsBEDCEmployee) { lblIsBedcEmployee.Text = "Yes"; divEmployeeCode.Visible = true; lblEmployeeCode.Text = objCustomerRegistrationBE.EmployeeName; }
                    if (objCustomerRegistrationBE.IsVIPCustomer) lblIsVipCustomer.Text = "Yes";
                    if (objCustomerRegistrationBE.IsEmbassyCustomer) { lblIsEmbassyCustomer.Text = "Yes"; tblEmbassyCode.Visible = true; }
                    lblEmployeeCode.Text = objCustomerRegistrationBE.EmployeeName;
                    //objCustomerRegistrationBE.IsBEDCEmployee = cbIsBEDCEmployee.Checked;
                    //objCustomerRegistrationBE.IsVIPCustomer = cbIsVIP.Checked;
                    lblOldAccount.Text = objCustomerRegistrationBE.OldAccountNo;
                    //lblAccountNo.Text = objCustomerRegistrationBE.AccountNo;
                    //lblEmployeeCode.Text = objCustomerRegistrationBE.EmployeeCode;
                    #endregion

                    #region Procedural Details
                    //objCustomerRegistrationBE.CustomerTypeId;
                    //objCustomerRegistrationBE.BookNo = 
                    lblPoleNo.Text = objCustomerRegistrationBE.PoleID;
                    //if (Convert.ToInt32(ReadCodeType) != (int)ReadCode.Direct) objCustomerRegistrationBE.MeterNumber;
                    //objCustomerRegistrationBE.TariffClassID;
                    //objCustomerRegistrationBE.IsEmbassyCustomer = cbIsEmbassy.Checked;
                    lblEmbassyCode.Text = objCustomerRegistrationBE.EmbassyCode;
                    lblPhase.Text = objCustomerRegistrationBE.Phase;
                    //objCustomerRegistrationBE.ReadCodeID
                    //objCustomerRegistrationBE.AccountTypeId;
                    ////objCustomerRegistrationBE.ClusterCategoryId;
                    //objCustomerRegistrationBE.ClusterCategoryId;
                    //objCustomerRegistrationBE.RouteSequenceNumber;
                    //if (!string.IsNullOrEmpty(hfGovtAccountTypeID.Value)) objCustomerRegistrationBE.MGActTypeID = Convert.ToInt32(hfGovtAccountTypeID.Value);



                    //lblPresentReading.Text = Convert.ToString(objCustomerRegistrationBE.PresentReading);
                    lblPresentReading.Text = Convert.ToString(objCustomerRegistrationBE.InitialReading);
                    #endregion

                    #region Identity Details
                    //if (hfIdentityIdList.Value != "" && hfIdentityNumberList.Value != "")
                    //{
                    //    objCustomerRegistrationBE.IdentityTypeIdList = hfIdentityIdList.Value; //Convert.ToInt32(ddlIdentityType.SelectedItem.Value);
                    //    objCustomerRegistrationBE.IdentityNumberList = hfIdentityNumberList.Value;
                    //}
                    //objCustomerRegistrationBE.UploadDocumentID = fupDocument.FileName;
                    #endregion

                    #region Application Process Details
                    //objCustomerRegistrationBE.CertifiedBy
                    lblCertifiedBy.Text = objCustomerRegistrationBE.CertifiedBy;
                    lblSeal1.Text = objCustomerRegistrationBE.Seal1;
                    lblSeal2.Text = objCustomerRegistrationBE.Seal2;
                    lblAppProcessedBy.Text = objCustomerRegistrationBE.ApplicationProcessedBy;
                    if (objCustomerRegistrationBE.ApplicationProcessedBy != null)
                        if (objCustomerRegistrationBE.ApplicationProcessedBy.ToLower() == "bedc")
                        {
                            litInstalledBy.Text = Resource.INSTALLED_BY;
                            lblInstallByName.Text = objCustomerRegistrationBE.EmployeeNameProcessBy;
                        }
                        else if (objCustomerRegistrationBE.ApplicationProcessedBy.ToLower() == "others")
                        {
                            litInstalledBy.Text = "Agency Name";
                            lblInstallByName.Text = objCustomerRegistrationBE.AgencyName;
                        }
                    #endregion

                    #region Application Process Person Details
                    //if (rblApplicationProcessedBy.Items[0].Selected) objCustomerRegistrationBE.InstalledBy;
                    //if (rblApplicationProcessedBy.Items[1].Selected) objCustomerRegistrationBE.AgencyId;
                    #endregion

                    #region Customer Active Details
                    //objCustomerRegistrationBE.IsCAPMI;
                    //if (Convert.ToInt32(ReadCodeType) != (int)ReadCode.Direct)
                    //{
                    //    if (cbIsCAPMY.Checked) objCustomerRegistrationBE.MeterAmount = Convert.ToDecimal(txtAmount.Text.Replace(",", string.Empty));
                    //    objCustomerRegistrationBE.PresentReading = Convert.ToInt32(txtPresentReading.Text);
                    //}
                    lblInitialBillingkWh.Text = Convert.ToString(objCustomerRegistrationBE.InitialBillingKWh);
                    lblAccountType.Text = objCustomerRegistrationBE.AccountType;
                    lblBookCode.Text = objCustomerRegistrationBE.BookCode;
                    lblTariff.Text = objCustomerRegistrationBE.ClassName;
                    lblClusterType.Text = objCustomerRegistrationBE.ClusterCategoryName;
                    lblPhase.Text = objCustomerRegistrationBE.Phase;
                    lblRouteNo.Text = objCustomerRegistrationBE.RouteName;
                    lblReadCode.Text = objCustomerRegistrationBE.ReadType;
                    if (objCustomerRegistrationBE.ReadType != null)
                        if (objCustomerRegistrationBE.ReadType.ToLower() == "read") { divReadCustomer.Visible = true; lblMeterNo.Text = objCustomerRegistrationBE.MeterNumber; trPresentReading.Visible = true; }
                        else if (objCustomerRegistrationBE.ReadType.ToLower() == "direct") { divDirectCustomer.Visible = true; lblAverageReading.Text = objCustomerRegistrationBE.AverageReading.ToString(); }
                    if (!objCustomerRegistrationBE.IsSameAsTenent) lblIsTenant.Text = "Yes";
                    else lblIsTenant.Text = "No";
                    if (objCustomerRegistrationBE.IsCAPMI != null)
                        if (objCustomerRegistrationBE.IsCAPMI) { lblIsCapmy.Text = "Yes"; lblMeterAmount.Text = objCommonMethods.GetCurrencyFormat(objCustomerRegistrationBE.MeterAmount, 2, Constants.MILLION_Format); lblCapmiOutstandingAmount.Text = objCommonMethods.GetCurrencyFormat(objCustomerRegistrationBE.CapmiOutstandingAmount, 2, Constants.MILLION_Format); }
                    //divMeterAmoount.Visible = true; 
                    #endregion

                    #region UDF Values List

                    //if (GetUDFValues())
                    //{
                    //    objCustomerRegistrationBE.UDFTypeIdList = hfUserControlFieldsID.Value;
                    //    objCustomerRegistrationBE.UDFValueList = hfUserControlFieldsValue.Value;
                    //}

                    #endregion

                    //GetIdentityDetails(GlobalAccountNo);
                    //GetCustomerDocuments(GlobalAccountNo);
                    //GetCustomerUDFValues(GlobalAccountNo);
                }
                else
                {
                    lblMessage.Text = Resource.CST_NOT_FND;
                    //mpeMessage.Show();
                }
            }
            catch (Exception ex)
            {
                objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                Message(Resource.CST_SRCH_FAILS, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        public static void MergeRows(GridView gridView)
        {
            for (int rowIndex = gridView.Rows.Count - 2; rowIndex >= 0; rowIndex--)
            {
                GridViewRow row = gridView.Rows[rowIndex];
                GridViewRow previousRow = gridView.Rows[rowIndex + 1];

                Label lblAccountNo = (Label)row.FindControl("lblAccount");
                Label lblPreviousAccountNo = (Label)previousRow.FindControl("lblAccount");
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    if (i == 0 || i == 1 || i == 2)
                    {
                        if (lblAccountNo.Text == lblPreviousAccountNo.Text)
                        {
                            row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                            previousRow.Cells[i].Visible = false;
                        }
                    }
                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region ExportExcel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if ((Session[UMS_Resource.AUDIT_SESSION_DT] != null) && (Request.QueryString[UMS_Resource.FROM_PAGE] != null))
                {
                    DataTable dt = (DataTable)Session[UMS_Resource.AUDIT_SESSION_DT];
                    RptAuditTrayListBe objRptAuditTrayList = new RptAuditTrayListBe();
                    RptAuditTrayBe objRptAuditTrayBe = new RptAuditTrayBe();
                    objRptAuditTrayBe.BU_ID = dt.Rows[0]["BU_ID"].ToString();
                    objRptAuditTrayBe.SU_ID = dt.Rows[0]["SU_ID"].ToString();
                    objRptAuditTrayBe.SC_ID = dt.Rows[0]["SC_ID"].ToString();
                    objRptAuditTrayBe.CycleId = dt.Rows[0]["CYCLE_ID"].ToString();
                    objRptAuditTrayBe.ClassId = dt.Rows[0]["TARIFF_ID"].ToString();
                    objRptAuditTrayBe.NewBookNo = dt.Rows[0]["BOOK_ID"].ToString();
                    objRptAuditTrayBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                    objRptAuditTrayBe.PageSize = Convert.ToInt32(hfTotalRecords.Value);
                    if (!string.IsNullOrEmpty(objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.FROM_PAGE])))
                        objRptAuditTrayBe.FromDate = objCommonMethods.Convert_MMDDYY(objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.FROM_PAGE]));
                    else
                        objRptAuditTrayBe.FromDate = string.Empty;
                    if (!string.IsNullOrEmpty(objIdsignBal.Decrypt(Request.QueryString["To"])))
                        objRptAuditTrayBe.ToDate = objCommonMethods.Convert_MMDDYY(objIdsignBal.Decrypt(Request.QueryString["To"]));
                    else
                        objRptAuditTrayBe.ToDate = string.Empty;

                    DataSet ds = new DataSet();
                    //ds = objReportsBal.GetLogsList(objRptAuditTrayBe, ReturnType.Bulk);
                    ds = objReportsBal.GetApprovalRegistrationsAuditRay(objRptAuditTrayBe);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        var BUlist = (from list in ds.Tables[0].AsEnumerable()
                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                      //orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                      group list by list.Field<string>("BusinessUnit") into g
                                      select new { BusinessUnit = g.Key }).ToList();

                        if (BUlist.Count() > 0)
                        {
                            XlsDocument xls = new XlsDocument();
                            Worksheet mainSheet = xls.Workbook.Worksheets.Add("Summary");
                            Cells mainCells = mainSheet.Cells;
                            Cell _objMainCells = null;

                            _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
                            mainCells.Add(4, 2, "Report Code ");
                            mainCells.Add(4, 3, Constants.StatusChangeLogReport_Code).Font.Bold = true;
                            mainCells.Add(4, 4, "Report Name");
                            mainCells.Add(4, 5, Constants.StatusChangeLogReport_Name).Font.Bold = true;
                            mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
                            _objMainCells.Font.Weight = FontWeight.ExtraBold;
                            _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objMainCells.Font.FontFamily = FontFamilies.Roman;

                            int MainSheetDetailsRowNo = 6;
                            mainCells.Add(MainSheetDetailsRowNo, 1, "S.No.").Font.Bold = true;
                            mainCells.Add(MainSheetDetailsRowNo, 2, "Service Center").Font.Bold = true;
                            mainCells.Add(MainSheetDetailsRowNo, 3, "Customer Count").Font.Bold = true;

                            var SClist = (from list in ds.Tables[0].AsEnumerable()
                                          orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                          // orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                          group list by list.Field<string>("ServiceCenterName") into g
                                          select new { ServiceCenter = g.Key }).ToList();

                            for (int k = 0; k < SClist.Count; k++)//SC
                            {
                                var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                 where list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                                 select list).ToList();

                                MainSheetDetailsRowNo++;
                                mainCells.Add(MainSheetDetailsRowNo, 1, k + 1);
                                mainCells.Add(MainSheetDetailsRowNo, 2, SClist[k].ServiceCenter);
                                mainCells.Add(MainSheetDetailsRowNo, 3, Customers.Count);
                            }


                            for (int i = 0; i < BUlist.Count; i++)//BU
                            {
                                var SUlistByBU = (from list in ds.Tables[0].AsEnumerable()
                                                  where list.Field<string>("BusinessUnit") == BUlist[i].BusinessUnit
                                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                  //orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                  group list by list.Field<string>("ServiceUnitName") into g
                                                  select new { ServiceUnitName = g.Key }).ToList();

                                for (int j = 0; j < SUlistByBU.Count; j++)//SU
                                {
                                    var SClistBySU = (from list in ds.Tables[0].AsEnumerable()
                                                      where list.Field<string>("BusinessUnit") == BUlist[i].BusinessUnit
                                                      && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                      //orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                      group list by list.Field<string>("ServiceCenterName") into g
                                                      select new { ServiceCenter = g.Key }).ToList();

                                    for (int k = 0; k < SClistBySU.Count; k++)//SC
                                    {
                                        var CyclelistBySC = (from list in ds.Tables[0].AsEnumerable()
                                                             where list.Field<string>("BusinessUnit") == BUlist[i].BusinessUnit
                                                             && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                             && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                             orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                             //orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                             group list by list.Field<string>("CycleName") into g
                                                             select new { CycleName = g.Key }).ToList();

                                        if (CyclelistBySC.Count > 0)
                                        {
                                            int DetailsRowNo = 2;
                                            int CustomersRowNo = 6;

                                            Worksheet sheet = xls.Workbook.Worksheets.Add(SClistBySU[k].ServiceCenter);
                                            Cells cells = sheet.Cells;
                                            Cell _objCell = null;

                                            for (int l = 0; l < CyclelistBySC.Count; l++)//Cycle
                                            {
                                                var BooklistByCycle = (from list in ds.Tables[0].AsEnumerable()
                                                                       where list.Field<string>("BusinessUnit") == BUlist[i].BusinessUnit
                                                                       && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                       && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                       && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                       orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                       //orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                       group list by list.Field<string>("BookNumber") into g
                                                                       select new { BookNumber = g.Key }).ToList();

                                                for (int m = 0; m < BooklistByCycle.Count; m++)//Book
                                                {
                                                    var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                                     where list.Field<string>("BusinessUnit") == BUlist[i].BusinessUnit
                                                                     && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                     && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                     && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                     && list.Field<string>("BookNumber") == BooklistByCycle[m].BookNumber
                                                                     orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                     //orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                     orderby Convert.ToString(list["CycleName"]) ascending
                                                                     select list).ToList();

                                                    if (Customers.Count() > 0)
                                                    {
                                                        cells.Add(DetailsRowNo, 1, "Business Unit");
                                                        cells.Add(DetailsRowNo, 3, "Service Unit");
                                                        cells.Add(DetailsRowNo, 5, "Service Center");
                                                        cells.Add(DetailsRowNo + 1, 1, "Book Group");
                                                        cells.Add(DetailsRowNo + 1, 3, "Book Name");

                                                        cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                                        cells.Add(DetailsRowNo, 4, SUlistByBU[j].ServiceUnitName).Font.Bold = true;
                                                        cells.Add(DetailsRowNo, 6, SClistBySU[k].ServiceCenter).Font.Bold = true;
                                                        cells.Add(DetailsRowNo + 1, 2, CyclelistBySC[l].CycleName).Font.Bold = true;
                                                        cells.Add(DetailsRowNo + 1, 4, BooklistByCycle[m].BookNumber).Font.Bold = true;


                                                        _objCell = cells.Add(DetailsRowNo + 3, 1, "Sno"); _objCell.Font.Weight = FontWeight.ExtraBold; _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 2, "Global Account No"); _objCell.Font.Weight = FontWeight.ExtraBold; _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 3, "Old Account No"); _objCell.Font.Weight = FontWeight.ExtraBold; _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 4, "Customer Type"); _objCell.Font.Weight = FontWeight.ExtraBold; _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 5, "Title (Landlord)"); _objCell.Font.Weight = FontWeight.ExtraBold; _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 6, "First Name (Landlord)"); _objCell.Font.Weight = FontWeight.ExtraBold; _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 7, "Middle Name (Landlord)"); _objCell.Font.Weight = FontWeight.ExtraBold; _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 8, "Last Name (Landlord)"); _objCell.Font.Weight = FontWeight.ExtraBold; _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 9, "Known As (Landlord)"); _objCell.Font.Weight = FontWeight.ExtraBold; _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 10, "EmailId (Landlord)"); _objCell.Font.Weight = FontWeight.ExtraBold; _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 11, "Home Contact No.(Landlord)"); _objCell.Font.Weight = FontWeight.ExtraBold; _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 12, "Business Phone No.(Landlord)"); _objCell.Font.Weight = FontWeight.ExtraBold; _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 13, "Other Phone No.(Landlord)"); _objCell.Font.Weight = FontWeight.ExtraBold; _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 14, "Is Same As Tenent"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 15, "Title (Tanent"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 16, "First Name (Tanent)"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 17, "Middle Name (Tanent)"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 18, "Last Name (Tanent)"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 19, "Phone No. (Tanent)"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 20, "Alternate Phone No. (Tanent)"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 21, "EmailId (Tanent)"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 22, "PoleID"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 23, "Tariff"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 24, "Is Embassy Customer"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 25, "Embassy Code"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 26, "Phase"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 27, "Read Type"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 28, "Is VIP Customer"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 29, "Is BEDC Employee"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 30, "House No (Service)"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 31, "Street (Service)"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 32, "City (Service)"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 33, "Area (Service)"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 34, "Zipcode (Service)"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 35, "IsCommunicationService"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 36, "House No (Postal)"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 37, "Street (Postal)"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 38, "City (PostaL)"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 39, "Area (Postal)"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 40, "Zipcode (Postal)"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 41, "Is Communication Postal"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 42, "Is CAPMY"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 43, "Initial Billing KWh"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 44, "Initial Reading"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 45, "Present Reading"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 46, "Average Reading"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 47, "Highest Consumption"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 48, "Out Standing Amount"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 49, "Opening Balance"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 50, "Remarks"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 51, "Seal1"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 52, "Seal2"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 53, "Account Type"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 54, "Class Name"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 55, "Cluster Category Name"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 56, "Route Name"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 57, "Employee Name"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 58, "Application Processed By"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 59, "Employee Name Process By"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 60, "Agency Name"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 61, "Meter Number"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 62, "Meter Amount"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 63, "Certified By1"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 64, "Certified By"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 65, "Is Same As Service"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 66, "Next Role"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 67, "Current Role"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 68, "Present Approval Role"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 69, "Next Approval Role"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 70, "Book Code"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 71, "Business Unit"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 72, "ServiceUnitName"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 73, "Service Center Name"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 74, "Cycle Name"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 75, "Book Number"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 76, "Document No"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 77, "Connection Date"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 78, "Application Date"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 79, "Setup Date"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 80, "Status"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 81, "Last Approved By"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 82, "Last Approved Date"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 83, "Request Date"); _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;


                                                        for (int n = 0; n < Customers.Count; n++)
                                                        {
                                                            DataRow dr = (DataRow)Customers[n];
                                                            cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                                                            cells.Add(CustomersRowNo, 2, dr["GlobalAccountNumber"].ToString());
                                                            cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
                                                            cells.Add(CustomersRowNo, 4, dr["CustomerType"].ToString());
                                                            cells.Add(CustomersRowNo, 5, dr["TitleLandlord"].ToString());
                                                            cells.Add(CustomersRowNo, 6, dr["FirstNameLandlord"].ToString());
                                                            cells.Add(CustomersRowNo, 7, dr["MiddleNameLandlord"].ToString());
                                                            cells.Add(CustomersRowNo, 8, dr["LastNameLandlord"].ToString());
                                                            cells.Add(CustomersRowNo, 9, dr["KnownAs"].ToString());
                                                            cells.Add(CustomersRowNo, 10, dr["EmailIdLandlord"].ToString());
                                                            cells.Add(CustomersRowNo, 11, dr["HomeContactNumberLandlord"].ToString());
                                                            cells.Add(CustomersRowNo, 12, dr["BusinessPhoneNumberLandlord"].ToString());
                                                            cells.Add(CustomersRowNo, 13, dr["OtherPhoneNumberLandlord"].ToString());
                                                            if (Convert.ToString(dr["IsSameAsTenent"]).ToLower() == "true") cells.Add(CustomersRowNo, 14, "Yes");
                                                            else cells.Add(CustomersRowNo, 14, "No");
                                                            cells.Add(CustomersRowNo, 15, dr["TitleTanent"].ToString());
                                                            cells.Add(CustomersRowNo, 16, dr["FirstNameTanent"].ToString());
                                                            cells.Add(CustomersRowNo, 17, dr["MiddleNameTanent"].ToString());
                                                            cells.Add(CustomersRowNo, 18, dr["LastNameTanent"].ToString());
                                                            cells.Add(CustomersRowNo, 19, dr["PhoneNumberTanent"].ToString());
                                                            cells.Add(CustomersRowNo, 20, dr["AlternatePhoneNumberTanent"].ToString());
                                                            cells.Add(CustomersRowNo, 21, dr["EmailIdTanent"].ToString());
                                                            cells.Add(CustomersRowNo, 22, dr["PoleID"].ToString());
                                                            cells.Add(CustomersRowNo, 23, dr["Tariff"].ToString());
                                                            if (Convert.ToString(dr["IsEmbassyCustomer"]).ToLower() == "true") cells.Add(CustomersRowNo, 24, "Yes");
                                                            else cells.Add(CustomersRowNo, 24, "No");
                                                            cells.Add(CustomersRowNo, 25, dr["EmbassyCode"].ToString());
                                                            cells.Add(CustomersRowNo, 26, dr["Phase"].ToString());
                                                            cells.Add(CustomersRowNo, 27, dr["ReadType"].ToString());
                                                            if (Convert.ToString(dr["IsVIPCustomer"]).ToLower() == "true") cells.Add(CustomersRowNo, 28, "Yes");
                                                            else cells.Add(CustomersRowNo, 28, "No");
                                                            if (Convert.ToString(dr["IsBEDCEmployee"]).ToLower() == "true") cells.Add(CustomersRowNo, 29, "Yes");
                                                            else cells.Add(CustomersRowNo, 29, "No");
                                                            cells.Add(CustomersRowNo, 30, dr["HouseNoService"].ToString());
                                                            cells.Add(CustomersRowNo, 31, dr["StreetService"].ToString());
                                                            cells.Add(CustomersRowNo, 32, dr["CityService"].ToString());
                                                            cells.Add(CustomersRowNo, 33, dr["AreaService"].ToString());
                                                            cells.Add(CustomersRowNo, 34, dr["SZipCode"].ToString());
                                                            if (Convert.ToString(dr["IsCommunicationService"]).ToLower() == "true") cells.Add(CustomersRowNo, 35, "Yes");
                                                            else cells.Add(CustomersRowNo, 35, "No");
                                                            cells.Add(CustomersRowNo, 36, dr["HouseNoPostal"].ToString());
                                                            cells.Add(CustomersRowNo, 37, dr["StreetPostal"].ToString());
                                                            cells.Add(CustomersRowNo, 38, dr["CityPostaL"].ToString());
                                                            cells.Add(CustomersRowNo, 39, dr["AreaPostal"].ToString());
                                                            cells.Add(CustomersRowNo, 40, dr["PZipCode"].ToString());
                                                            if (Convert.ToString(dr["IsCommunicationPostal"]).ToLower() == "true") cells.Add(CustomersRowNo, 41, "Yes");
                                                            else cells.Add(CustomersRowNo, 41, "No");
                                                            if (Convert.ToString(dr["IsCAPMI"]).ToLower() == "true") cells.Add(CustomersRowNo, 42, "Yes");
                                                            else cells.Add(CustomersRowNo, 42, "No");
                                                            cells.Add(CustomersRowNo, 43, dr["InitialBillingKWh"].ToString());
                                                            cells.Add(CustomersRowNo, 44, dr["InitialReading"].ToString());
                                                            cells.Add(CustomersRowNo, 45, dr["PresentReading"].ToString());
                                                            cells.Add(CustomersRowNo, 46, dr["AverageReading"].ToString());
                                                            cells.Add(CustomersRowNo, 47, dr["Highestconsumption"].ToString());
                                                            cells.Add(CustomersRowNo, 48, dr["OutStandingAmount"].ToString());
                                                            cells.Add(CustomersRowNo, 49, dr["OpeningBalance"].ToString());
                                                            cells.Add(CustomersRowNo, 50, dr["Remarks"].ToString());
                                                            cells.Add(CustomersRowNo, 51, dr["Seal1"].ToString());
                                                            cells.Add(CustomersRowNo, 52, dr["Seal2"].ToString());
                                                            cells.Add(CustomersRowNo, 53, dr["AccountType"].ToString());
                                                            cells.Add(CustomersRowNo, 54, dr["ClassName"].ToString());
                                                            cells.Add(CustomersRowNo, 55, dr["ClusterCategoryName"].ToString());
                                                            cells.Add(CustomersRowNo, 56, dr["RouteName"].ToString());
                                                            cells.Add(CustomersRowNo, 57, dr["EmployeeName"].ToString());
                                                            cells.Add(CustomersRowNo, 58, dr["ApplicationProcessedBy"].ToString());
                                                            cells.Add(CustomersRowNo, 59, dr["EmployeeNameProcessBy"].ToString());
                                                            cells.Add(CustomersRowNo, 60, dr["AgencyName"].ToString());
                                                            cells.Add(CustomersRowNo, 61, dr["MeterNumber"].ToString());
                                                            cells.Add(CustomersRowNo, 62, dr["MeterAmount"].ToString());
                                                            cells.Add(CustomersRowNo, 63, dr["CertifiedBy1"].ToString());
                                                            cells.Add(CustomersRowNo, 64, dr["CertifiedBy"].ToString());
                                                            if (Convert.ToString(dr["IsSameAsService"]).ToLower() == "true") cells.Add(CustomersRowNo, 65, "Yes");
                                                            else cells.Add(CustomersRowNo, 65, "No");
                                                            cells.Add(CustomersRowNo, 66, dr["NextRole"].ToString());
                                                            cells.Add(CustomersRowNo, 67, dr["CurrentRole"].ToString());
                                                            cells.Add(CustomersRowNo, 68, dr["PresentApprovalRole"].ToString());
                                                            cells.Add(CustomersRowNo, 69, dr["NextApprovalRole"].ToString());
                                                            cells.Add(CustomersRowNo, 70, dr["BookCode"].ToString());
                                                            cells.Add(CustomersRowNo, 71, dr["BusinessUnit"].ToString());
                                                            cells.Add(CustomersRowNo, 72, dr["ServiceUnitName"].ToString());
                                                            cells.Add(CustomersRowNo, 73, dr["ServiceCenterName"].ToString());
                                                            cells.Add(CustomersRowNo, 74, dr["CycleName"].ToString());
                                                            cells.Add(CustomersRowNo, 75, dr["BookNumber"].ToString());
                                                            cells.Add(CustomersRowNo, 76, dr["DocumentNo"].ToString());
                                                            cells.Add(CustomersRowNo, 77, dr["ConnectionDate"].ToString());
                                                            cells.Add(CustomersRowNo, 78, dr["ApplicationDate"].ToString());
                                                            cells.Add(CustomersRowNo, 79, dr["SetupDate"].ToString());
                                                            cells.Add(CustomersRowNo, 80, dr["Status"].ToString());
                                                            cells.Add(CustomersRowNo, 81, dr["ModifiedBy"].ToString());
                                                            cells.Add(CustomersRowNo, 82, dr["ModifiedDate"].ToString());
                                                            cells.Add(CustomersRowNo, 83, dr["CreatedDate"].ToString());
                                                            //cells.Add(CustomersRowNo, 13, objIdsignBal.ReplaceNewLines(dr["Remarks"].ToString(), false));

                                                            CustomersRowNo++;
                                                        }
                                                        DetailsRowNo = CustomersRowNo + 2;
                                                        CustomersRowNo = CustomersRowNo + 6;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                            fileName = Constants.StatusChangeLogReport_Name.Replace(" ", string.Empty) + "_" + fileName;
                            string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                            xls.FileName = filePath;
                            xls.Save();
                            objIdsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                        }
                        else
                            Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else
                        Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}