﻿<%@ Page Title=":: Customer Statistics By Tariff ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="CustomerStatisticsByTariff.aspx.cs"
    Inherits="UMS_Nigeria.Reports.CustomerStatisticsByTariff" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upCustomerStatisticsByTariff" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddCycle" runat="server" Text="Customer Statistics By Tariff"></asp:Literal>
                        </div>
                        <div class="star_text" style="margin-right: 155px;">
                            <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="previous">
                        <a href="ReportsSummary.aspx">< < Back To Summary</a>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litBusinessUnitName" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT_NAME%>"></asp:Literal>
                                <span class="span_star">*</span></label><div class="space">
                                </div>
                            <asp:DropDownList ID="ddlBusinessUnitName" TabIndex="1" onchange="DropDownlistOnChangelbl(this,'spanddlBusinessUnitName','Business Unit')"
                                runat="server" CssClass="text-box text-select">
                            </asp:DropDownList>
                            <div class="space">
                            </div>
                            <span id="spanddlBusinessUnitName" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <asp:Literal ID="litYear" runat="server" Text="<%$ Resources:Resource, YEAR%>"></asp:Literal><span
                                class="span_star">*</span><br />
                            <asp:DropDownList ID="ddlYear" runat="server" CssClass="text-box select_box" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <br />
                            <span id="spanddlYear" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box2">
                        <div class="text-inner">
                            <asp:Literal ID="litMonth" runat="server" Text="<%$ Resources:Resource, MONTH%>"></asp:Literal><span
                                class="span_star">*</span><br />
                            <asp:DropDownList ID="ddlMonth" runat="server" CssClass="text-box select_box">
                                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <br />
                            <span id="spanddlMonth" class="span_color"></span>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="space">
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s"
                                runat="server" OnClick="btnSearch_Click" OnClientClick="return Validate();" />
                        </div>
                    </div>
                    <div class="clear">
                    <br />
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server" visible="false">
                        <div align="right" class="fltr" runat="server" id="divExport">
                            <asp:Button ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" CssClass="exporttoexcelButton"
                                Text="" />
                        </div>
                        <div class="clear">
                        </div>
                        <div class="dot-line">
                        </div>
                        <div class="text_total">
                            <div class="text-headingReports">
                                <asp:Literal ID="ltrlReportNamewithBU" runat="server" Text=""></asp:Literal>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="dot-line">
                        </div>
                        <%-- <asp:GridView ID="gvCustomerStatisticsByTariff" runat="server" AutoGenerateColumns="False"
                        AlternatingRowStyle-CssClass="color" HeaderStyle-CssClass="grid_tb_hd">
                        <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                        <EmptyDataTemplate>
                            No Details Found.
                        </EmptyDataTemplate>
                        <RowStyle HorizontalAlign="Center" />
                    </asp:GridView>--%>
                        <table style="border-collapse: collapse;">
                            <asp:Repeater ID="rptrReport" runat="server">
                                <HeaderTemplate>
                                    <tr class="rptr_tb_hd">
                                        <th>
                                            S.No
                                        </th>
                                        <th>
                                            Tariff
                                        </th>
                                        <th>
                                            Active
                                        </th>
                                        <%-- <th>
                                            InActive
                                        </th>--%>
                                        <th>
                                            Total Population
                                        </th>
                                        <th>
                                            Energy Billed
                                        </th>
                                        <th>
                                            Revenue Billed
                                        </th>
                                    </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="reportsGrid_tb_td">
                                        <td align="left" class="rptr_tb_td">
                                            <%--<asp:Label ID="lblSno" runat="server" Text='<%#Eval("ID")%>'></asp:Label>--%>
                                            <%#Container.ItemIndex+1 %>
                                        </td>
                                        <td align="left" class="rptr_tb_td">
                                            <asp:Label ID="lblTariff" runat="server" Text='<%#Eval("Tariff")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblActive" runat="server" Text='<%#Eval("Active")%>'></asp:Label>
                                        </td>
                                        <%-- <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblInActive" runat="server" Text='<%#Eval("InActive")%>'></asp:Label>
                                        </td>--%>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblTotalPopulation" runat="server" Text='<%#Eval("TotalPopulation")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblEnergyBilled" runat="server" Text='<%#Eval("EnergyBilled")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblRevenueBilled" runat="server" Text='<%#Eval("RevenueBilled")%>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <tr id="NoDataFound" runat="server" align="center"><td colspan="6"> No data found for selected month & Year</td></tr>
                        </table>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script type="text/javascript">
        function pageLoad() {
            DisplayMessage('UMSNigeriaBody_pnlMessage');
        };
        function Validate() {
            var ddlBusinessUnitName = document.getElementById('UMSNigeriaBody_ddlBusinessUnitName');
            var ddlYear = document.getElementById('UMSNigeriaBody_ddlYear');
            var ddlMonth = document.getElementById('UMSNigeriaBody_ddlMonth');

            var IsValid = true;

            if (DropDownlistValidationlbl(ddlBusinessUnitName, document.getElementById("spanddlBusinessUnitName"), "Business Unit") == false) IsValid = false;
            if (DropDownlistValidationlbl(ddlYear, document.getElementById("spanddlYear"), "Year") == false) IsValid = false;
            if (DropDownlistValidationlbl(ddlMonth, document.getElementById("spanddlMonth"), "Month") == false) IsValid = false;

            if (!IsValid) {
                return false;
            }
        }
    </script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        });
    </script>
</asp:Content>
