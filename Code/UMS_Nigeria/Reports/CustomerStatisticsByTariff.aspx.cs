﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using iDSignHelper;
using Resources;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using System.Data;
using UMS_NigriaDAL;
using System.Configuration;
using org.in2bits.MyXls;

namespace UMS_Nigeria.Reports
{
    public partial class CustomerStatisticsByTariff : System.Web.UI.Page
    {
        #region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        ReportsBal _objReportsBal = new ReportsBal();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        UMS_Service _ObjUMS_Service = new UMS_Service();
        XmlDocument xml = new XmlDocument();
        string Key = "CustomerStatisticsByTariff";
        #endregion

        #region Properties
        private int YearId
        {
            get { return string.IsNullOrEmpty(ddlYear.SelectedValue) ? 0 : Convert.ToInt32(ddlYear.SelectedValue); }
            set { ddlYear.SelectedValue = value.ToString(); }
        }
        private int MonthId
        {
            get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? 0 : Convert.ToInt32(ddlMonth.SelectedValue); }
            set { ddlMonth.SelectedValue = value.ToString(); }
        }
        private string BU_ID
        {
            get { return string.IsNullOrEmpty(ddlBusinessUnitName.SelectedValue) ? string.Empty : ddlBusinessUnitName.SelectedValue; }
            set { ddlBusinessUnitName.SelectedValue = value; }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                BindBusinessUnits();
                BindYears();
                BindMonths();
            }
        }

        private void BindBusinessUnits()
        {
            _ObjCommonMethods.BindBusinessUnits(ddlBusinessUnitName, string.Empty, true);
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
            {
                ddlBusinessUnitName.SelectedIndex = ddlBusinessUnitName.Items.IndexOf(ddlBusinessUnitName.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                ddlBusinessUnitName.Enabled = false;
            }
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindMonths();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindGrid();
        }

        #endregion

        #region Methods
        public void BindGrid()
        {
            PDFReportsBe _objPDFReportsBe = new PDFReportsBe();
            _objPDFReportsBe.Month = this.MonthId;
            _objPDFReportsBe.Year = this.YearId;
            _objPDFReportsBe.BU_ID = this.BU_ID;
            DataSet dsData = new DataSet();
            dsData = _objReportsBal.RptCustStatisticsByTariff(_objPDFReportsBe, ReturnType.Get);

            if (dsData.Tables[0].Rows.Count > 0)
            {
                divgrid.Visible = divExport.Visible = true;
                NoDataFound.Visible = false;
                rptrReport.DataSource = dsData.Tables[0];
                rptrReport.DataBind();
                string MonthYear = ddlYear.SelectedItem.Text + " " + ddlMonth.SelectedItem.Text;
                string BUName = string.IsNullOrEmpty(ddlBusinessUnitName.SelectedValue) ? "All" : ddlBusinessUnitName.SelectedItem.Text;
                ltrlReportNamewithBU.Text = string.Format(Resource.Rpt_Customer_Statistics_By_Tariff_Class, BUName, MonthYear);
            }
            else
            {
                divExport.Visible = false;
                divgrid.Visible = NoDataFound.Visible = true;
                rptrReport.DataSource = new DataTable();
                rptrReport.DataBind();
                string MonthYear = ddlYear.SelectedItem.Text + " " + ddlMonth.SelectedItem.Text;
                string BUName = string.IsNullOrEmpty(ddlBusinessUnitName.SelectedValue) ? "All" : ddlBusinessUnitName.SelectedItem.Text;
                ltrlReportNamewithBU.Text = string.Format(Resource.Rpt_Customer_Statistics_By_Tariff_Class, BUName, MonthYear);
            }
        }

        private void BindYears()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
                xmlResult = _objBillGenerationBal.GetDetails(ReturnType.Fetch);
                BillGenerationListBe objBillsListBE = _ObjiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                if (objBillsListBE.Items.Count > 0)
                {
                    _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlYear, "Year", "Year", "--Select--", true);
                    BindMonths();
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindMonths()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
                _objBillGenerationBe.Year = this.YearId;
                xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.Group);
                BillGenerationListBe objBillsListBE = _ObjiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                if (objBillsListBE.Items.Count > 0)
                {
                    ddlMonth.Enabled = true;
                    _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlMonth, "MonthName", "Month", "--Select--", true);
                }
                else
                {
                    ddlMonth.SelectedIndex = 0;
                    ddlMonth.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region ExportExcel

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                PDFReportsBe _objPDFReportsBe = new PDFReportsBe();
                _objPDFReportsBe.Month = this.MonthId;
                _objPDFReportsBe.Year = this.YearId;
                _objPDFReportsBe.BU_ID = this.BU_ID;
                DataSet dsData = new DataSet();
                dsData = _objReportsBal.RptCustStatisticsByTariff(_objPDFReportsBe, ReturnType.Get);

                if (dsData.Tables[0].Rows.Count > 0)
                {
                    XlsDocument xls = new XlsDocument();
                    Worksheet sheet = xls.Workbook.Worksheets.Add("Customer Statistics By Tariff");
                    Cells cells = sheet.Cells;
                    Cell _objCell = null;

                    _objCell = cells.Add(2, 3, ltrlReportNamewithBU.Text);
                    sheet.AddMergeArea(new MergeArea(2, 2, 3, 6));
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;

                    int DetailsRowNo = 5;
                    int CustomersRowNo = 6;

                    _objCell = cells.Add(DetailsRowNo, 1, "S.No");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 2, "Tariff");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 3, "Active");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    //_objCell = cells.Add(DetailsRowNo, 3, "InActive");
                    //_objCell.Font.Weight = FontWeight.ExtraBold;
                    //_objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    //_objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 4, "Total Population");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 5, "Energy Billed");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 6, "Revenue Billed");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;

                    for (int n = 0; n < dsData.Tables[0].Rows.Count; n++)
                    {
                        DataRow dr = (DataRow)dsData.Tables[0].Rows[n];
                        cells.Add(CustomersRowNo, 1, n + 1);
                        cells.Add(CustomersRowNo, 2, dr["Tariff"].ToString());
                        cells.Add(CustomersRowNo, 3, dr["Active"].ToString());
                        //cells.Add(CustomersRowNo, 3, dr["InActive"].ToString());
                        cells.Add(CustomersRowNo, 4, dr["TotalPopulation"].ToString());
                        cells.Add(CustomersRowNo, 5, dr["EnergyBilled"].ToString());
                        cells.Add(CustomersRowNo, 6, dr["RevenueBilled"].ToString());

                        CustomersRowNo++;
                    }

                    string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                    fileName = "CustomerStatisticsByTariff_" + fileName;
                    string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                    xls.FileName = filePath;
                    xls.Save();
                    _ObjiDsignBAL.DownLoadFile(filePath, "application//vnd.ms-excel");
                }
                else
                    Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}