﻿<%@ Page Title="::Direct Customers Report::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="CustomerWithNoMeter.aspx.cs"
    Inherits="UMS_Nigeria.Reports.CustomerWithNoMeter" %>

<%@ Register Src="../UserControls/UCPagingV2.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upDirectCustomers" runat="server" ChildrenAsTriggers="true">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAccountWoMeter" runat="server" Text="Direct Customers Report"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <%--<asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>--%>
                            &nbsp
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="previous">
                        <a href="ReportsSummary.aspx">< < Back To Summary</a>
                    </div>
                    <div class="inner-box">
                        <div id="divBusinessUnit" runat="server">
                            <div class="check_baa">
                                <div class="text-inner_a">
                                    <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" ID="ChkBUAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                        <asp:CheckBoxList ID="cblBusinessUnits" class="text-inner-b" AutoPostBack="true"
                                            RepeatDirection="Horizontal" RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblBusinessUnits_SelectedIndexChanged">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div id="divServiceUnits" runat="server" visible="false">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT_NAME%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:DropDownList ID="ddlSU" runat="server" CssClass="text-box text-select" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlSU_SelectedIndexChanged">
                                    <asp:ListItem Value="">--Select--</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div id="divServiceCenters" runat="server" visible="false">
                        <div class="check_baa">
                            <div class="text-inner_a">
                                <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" ID="chkSCAll" class="text-inner-b" Text="All" />
                                    <asp:CheckBoxList ID="cblServiceCenters" class="text-inner-b" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div id="divTariff" runat="server">
                        <div class="check_baa">
                            <div class="text-inner_a">
                                <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" ID="chkTariffAll" class="text-inner-b" Text="All" />
                                    <asp:CheckBoxList ID="cblTarif" class="text-inner-b" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="box_total">
                    <div class="box_total_a">
                        <asp:Button ID="btnNext" CssClass="box_s" Text="<%$ Resources:Resource, SEARCH%>"
                            runat="server" OnClick="btnNext_Click" />
                    </div>
                </div>
                <div class="grid_boxes" id="divrptrDetails" runat="server" visible="false">
                    <div class="grid_paging_top">
                        <div class="paging_top_title" style="position: relative; top: 24px;">
                            <asp:Literal ID="Literal2" runat="server" Text="Direct Customers Report List"></asp:Literal>
                            <%--<asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, GVLIST_WOTHOUTMETER%>"></asp:Literal>--%>
                        </div>
                        <div class="clear">
                        </div>
                        <div id="divToprptrContent" runat="server" visible="false">
                            <div class="consume_name fltl" style="position: relative; top: 30px;">
                                <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, TOTAL_CUSTOMERS%>"></asp:Label>
                            </div>
                            <div class="consume_input fltl" style="margin-right: 10px; border-right: 1px solid #000;
                                padding-right: 10px;position: relative; top: 30px;">
                                <asp:Label ID="lblTopTotalCustomers" runat="server" Text="--"></asp:Label>
                            </div>
                            <div class="consume_name fltl" style="position: relative; top: 30px;">
                                <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, TOTAL_DUE_BALENCE%>"></asp:Label>
                            </div>
                            <div class="consume_input fltl" style="margin-right: 10px; padding-right: 10px;position: relative;
                                        top: 30px;">
                                <asp:Label ID="lblTopTotalOutStanding" runat="server" Text="--"></asp:Label>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="paging_top_right_content">
                                <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                            </div>
                        </div>
                        <div id="divExport" visible="false" runat="server">
                            <asp:Button ID="btnExportExcel" OnClick="btnExportExcel_Click" runat="server" CssClass="exporttoexcelButton" />
                        </div>
                    </div>
                    <div class="clr pad_10"></div>
                    <div class="rptr_tb">
                        <table style="border-collapse:collapse; border:1px #000 solid;">
                            <tr class="rptr_tb_hd" align="center" runat="server" visible="false" id="rptrheader">
                                <th>
                                    <asp:Label ID="lblHSno" runat="server" Text="<%$ Resources:Resource, GRID_SNO%>"></asp:Label>
                                </th>
                                <th>
                                    <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Label>
                                </th>
                                <th>
                                    <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Label>
                                </th>
                                <th>
                                    <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Label>
                                </th>
                                <th>
                                    <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Label>
                                </th>
                                <th>
                                    <asp:Label ID="Label12" runat="server" Text="<%$ Resources:Resource, AVERAGE_READING%>"></asp:Label>
                                </th>
                                <th>
                                    <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, OUT_STANDING_AMT%>"></asp:Label>
                                </th>
                                <th>
                                    <asp:Label ID="Label10" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Label>
                                </th>
                                <th>
                                    <asp:Label ID="Label11" runat="server" Text="<%$ Resources:Resource, CONNECTION_DATE%>"></asp:Label>
                                </th>
                            </tr>
                            <asp:Repeater ID="rptrDirectCustomers" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td class="rptr_tb_td" align="center">
                                            <asp:Label runat="server" ID="lblsno" Text='<%#Eval("RowNumber") %>'></asp:Label>
                                        </td>
                                        <td class="rptr_tb_td" align="center">
                                            <asp:Label runat="server" ID="lblAccNo" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                        </td>
                                        <td class="rptr_tb_td" align="left">
                                            <asp:Label runat="server" ID="lblOldAccountNo" Text='<%#Eval("OldAccountNo") %>'></asp:Label>
                                        </td>
                                        <td class="rptr_tb_td" align="left">
                                            <asp:Label runat="server" ID="lblName" Text='<%#Eval("Name") %>'></asp:Label>
                                        </td>
                                        <td class="rptr_tb_td" align="left">
                                            <asp:Label runat="server" ID="lblAddress" Text='<%#Eval("ServiceAddress") %>'></asp:Label>
                                        </td>
                                        <td class="rptr_tb_td" align="left">
                                            <asp:Label runat="server" ID="Label13" Text='<%#Eval("AvgReading") %>'></asp:Label>
                                        </td>
                                        <td class="rptr_tb_td" align="right">
                                            <asp:Label runat="server" ID="lblOutStanding" Text='<%#Eval("OutstandingAmount") %>'></asp:Label>
                                        </td>
                                        <td class="rptr_tb_td" align="right">
                                            <asp:Label ID="lblLastPaidAmount" runat="server" Text='<%#Eval("ClassName")%>'></asp:Label>
                                        </td>
                                        <td class="rptr_tb_td" align="center">
                                            <asp:Label runat="server" ID="lblLastPaidDate" Text='<%#Eval("ConnectionDate") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <tr align="center" runat="server" id="divNoDataFound" visible="false">
                                <td colspan="8">
                                    No data found
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="divDownrptrContent"  class="reports_txt" runat="server" visible="false">
                        <div class="consume_name fltl">
                            <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, TOTAL_CUSTOMERS%>"></asp:Label>
                        </div>
                        <div class="consume_input fltl" style="margin-right: 10px; border-right: 1px solid #000;
                            padding-right: 10px;">
                            <asp:Label ID="lblDownTotalCustomers" runat="server" Text="--"></asp:Label>
                        </div>
                        <div class="consume_name fltl">
                            <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, TOTAL_DUE%>"></asp:Label>
                        </div>
                        <div class="consume_input fltl" style="margin-right: 10px; ">
                            <asp:Label ID="lblDownTotalOutStanding" runat="server" Text="--"></asp:Label>
                        </div>
                        <div class="paging_top_right_content">
                            <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                        </div>
                    </div>
                    <asp:HiddenField ID="hfPageSize" runat="server" />
                    <asp:HiddenField ID="hfPageNo" runat="server" />
                    <asp:HiddenField ID="hfLastPage" runat="server" />
                    <asp:HiddenField ID="hfTotalRecords" runat="server" />
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
    <%--==============Escape button script starts==============--%>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/UnBilledCustomersReport.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
