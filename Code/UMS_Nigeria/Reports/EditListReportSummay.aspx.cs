﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using iDSignHelper;
using Resources;
using System.Resources;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;

namespace UMS_Nigeria.Reports
{
    public partial class EditListReportSummay : System.Web.UI.Page
    {
        #region Members

        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        DataSet dsReports = new DataSet();
        MenuBAL objMenuBAL = new MenuBAL();
        string key = "EditListReports";

        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                BindReports();
            }
        }

        private void BindReports()
        {
            EditListReportsBE_MoreInfo _objEditListReportsBE_MoreInfo = new EditListReportsBE_MoreInfo();
            _objEditListReportsBE_MoreInfo = _ObjiDsignBAL.DeserializeFromXml<EditListReportsBE_MoreInfo>(objMenuBAL.Get(ReturnType.Get));

            gvReports.DataSource = _objEditListReportsBE_MoreInfo.Items;
            gvReports.DataBind();
        }
    }
}