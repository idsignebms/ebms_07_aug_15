﻿<%@ Page Title=":: General Reports ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master" Theme="Green"
AutoEventWireup="true" CodeBehind="GeneralReports.aspx.cs" Inherits="UMS_Nigeria.Reports.GeneralReports" %>
<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .scrollbar
        {
            width: 150px;
            height: 300px;
            background-color: lightgray;
            margin-top: 40px;
            margin-left: 40px;
            overflow-y: scroll;
            float: left;
        }
        #ex3::-webkit-scrollbar-thumb
        {
            background-color: #B03C3F;
            border-radius: 10px;
        }
        #ex3::-webkit-scrollbar-thumb:hover
        {
            background-color: #BF4649;
            border: 1px solid #333333;
        }
        #ex3::-webkit-scrollbar-thumb:active
        {
            background-color: #A6393D;
            border: 1px solid #333333;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="updRptSumry" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddState" runat="server" Text="General Reports"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                </div>
                <div style="clear: both;">
                </div>
                <div id="Div1" class="grid_tb" runat="server" style="overflow-y: scroll; height: 380px;">
                    <asp:GridView ID="gvReports" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="grid_tb_hd"
                        OnRowCommand="gvReports_RowCommand">
                        <EmptyDataRowStyle HorizontalAlign="Center" />
                        <%-- OnDataBound="gvReports_DataBound"--%>
                        <EmptyDataRowStyle HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            There is no data.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="S.No." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%"
                                ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <%--  <%# Container.DataItemIndex + 1 %>--%>
                                    <asp:Label ID="lblIndex" runat="server"></asp:Label>
                                    <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("SNo") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Report Title" ItemStyle-Width="31%" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblReportName" runat="server" Text='<%# Eval("ReportName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Report Description" ItemStyle-Width="35%" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblDetails" runat="server" Text='<%# Eval("Details") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="Last Close Month" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblLastCloseMonth" runat="server" Text='<%# Eval("LastCloseMonth") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Report" ItemStyle-Width="17%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label runat="server" Visible="false" ID="lblUrl" Text='<%# Eval("Url") %>'></asp:Label>
                                    <asp:LinkButton id="lkbtnNavigate" runat="server" tooltip="Navigate" text="Navigate" commandname="Navigate">
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <asp:HiddenField ID="hfMonth" runat="server" Value="0" />
            <asp:HiddenField ID="hfYear" runat="server" Value="0" />
            <asp:HiddenField ID="hdnLastCloseMonth" runat="server" Value="N/A" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        function pageLoad() {
            DisplayMessage('UMSNigeriaBody_pnlMessage');
        }; 
    </script>
     <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }
        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }   
    </script>
</asp:Content>
