﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using iDSignHelper;
using Resources;
using System.Resources;
using UMS_NigeriaBAL;

namespace UMS_Nigeria.Reports
{
    public partial class GeneralReports : System.Web.UI.Page
    {
        #region Members

        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        DataSet dsReports = new DataSet();
        ReportsBal objReportsBal = new ReportsBal();
        string key = "ReportsList";

        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                //GetLastCloseMonth();
                BindReports();
            }

        }
        protected void gvReports_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblUrl = (Label)row.FindControl("lblUrl");
            Label lblLastCloseMonth = (Label)row.FindControl("lblLastCloseMonth");

            switch (e.CommandName.ToUpper())
            {
                case "NAVIGATE":
                    Response.Redirect(lblUrl.Text, false);
                    break;
            }
        }

        private void BindReports()
        {
            string roleId = "0";
            dsReports.ReadXml(Server.MapPath(ConfigurationSettings.AppSettings["GeneralReports"]));
            //DataColumn newColumn = new DataColumn("LastCloseMonth", typeof(System.String));
            //newColumn.DefaultValue = hdnLastCloseMonth.Value;
            //dsReports.Tables[0].Columns.Add(newColumn);
            var result = (from x in dsReports.Tables[0].AsEnumerable()
                          //where (x.Field<string>("AccessTo").Split(',')).Contains(roleId) && x.Field<string>("Status").Equals("0")
                          select new
                          {
                              Id = x.Field<string>("Id"),
                              SNo = x.Field<string>("SNo"),
                              //ReportNo = x.Field<string>("ReportNo"),
                              Url = x.Field<string>("Url"),
                              ReportName = x.Field<string>("ReportName"),
                              Details = x.Field<string>("Details"),
                              //LastCloseMonth = x.Field<string>("LastCloseMonth"),
                              //ReportFormat = x.Field<string>("ReportFormat")

                          });


            gvReports.DataSource = result;
            gvReports.DataBind();

            //MergeRows(gvReports);
        }

        public static void MergeRows(GridView gridView)
        {
            for (int rowIndex = gridView.Rows.Count - 2; rowIndex >= 0; rowIndex--)
            {
                GridViewRow row = gridView.Rows[rowIndex];
                GridViewRow previousRow = gridView.Rows[rowIndex + 1];

                //Label lblIndex = (Label)row.FindControl("lblIndex");

                Label lblSNo = (Label)row.FindControl("lblSNo");
                Label lblPreviousSNo = (Label)previousRow.FindControl("lblSNo");
                //Label lblIndex = (Label)row.FindControl("lblIndex");

                Label lblDetails = (Label)row.FindControl("lblDetails");
                Label lblPreviousDetails = (Label)previousRow.FindControl("lblDetails");
                //Label lblPreIndex = (Label)previousRow.FindControl("lblIndex");

                for (int i = 0; i < row.Cells.Count; i++)
                {
                    if (i == 0)
                    {
                        if (Convert.ToInt32(lblSNo.Text) == Convert.ToInt32(lblPreviousSNo.Text))
                        {
                            row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                            previousRow.Cells[i].Visible = false;
                            //lblPreIndex.Text = (rowIndex + 1).ToString();

                        }
                        //lblIndex.Text = (rowIndex + 1).ToString();
                    }
                }
            }
        }

        public void GetLastCloseMonth()
        {
            DataSet ds = new DataSet();
            ds = objReportsBal.GetLastCloseMonth();
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    hdnLastCloseMonth.Value = Convert.ToString(ds.Tables[0].Rows[0]["LastOpenMonth"]);
                    hfMonth.Value = ds.Tables[0].Rows[0]["Month"].ToString();
                    hfYear.Value = ds.Tables[0].Rows[0]["Year"].ToString();
                }
                else
                    hdnLastCloseMonth.Value = "N/A";
            }
            else
                hdnLastCloseMonth.Value = "N/A";
        }
    }
}