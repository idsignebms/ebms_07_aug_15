﻿<%@ Page Title="::Meter Change Logs Report::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="MeterChangeLogs.aspx.cs" Inherits="UMS_Nigeria.Reports.MeterChangeLogs" %>

<%@ Register Src="../UserControls/UCPagingV2.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <div class="inner-sec">
            <asp:Panel ID="pnlMessage" runat="server">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
            <div class="text_total">
                <div class="text-heading">
                    <asp:Literal ID="litAddState" runat="server" Text="Meter Change Logs"></asp:Literal>
                </div>
                <div class="star_text">
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="dot-line">
            </div>
            <div class="previous">
                <asp:LinkButton ID="lbtnBack" runat="server" Text='<%$ Resources:Resource, BACK %>'
                    OnClick="lbtnBack_Click"></asp:LinkButton>
            </div>
            <div class="clr">
            </div>
            <div id="divCustomerDetails" runat="server">
                <div class="inner-sec">
                    <div class="heading" style="padding-left: 16px; padding-top: 17px;">
                        <asp:Literal ID="Literal213" runat="server" Text="Search Details"></asp:Literal>
                    </div>
                    <div class="out-bor_aTwo">
                        <table class="customerdetailsTamle">
                            <tr id="trBU" runat="server">
                                <td valign="top" class="customer_auditlog">
                                    <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                                </td>
                                <td valign="top" class="customerTd_auditlog">
                                    :
                                </td>
                                <td class="customerdetailstamleTdsize customerdetailstamlewidth">
                                    <div class="iabody_auditlog">
                                        <asp:Label ID="lblBU" runat="server" Text="--"></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr id="trSU" runat="server">
                                <td valign="top" class="customer_auditlog">
                                    <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal>
                                </td>
                                <td valign="top" class="customerTd_auditlog">
                                    :
                                </td>
                                <td class="customerdetailstamleTdsize">
                                    <div class="iabody_auditlog">
                                        <asp:Label ID="lblSU" runat="server" Text="--"></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr id="trSC" runat="server">
                                <td valign="top" class="customerdetailstamleTd">
                                    <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal>
                                </td>
                                <td valign="top" class="customerTd_auditlog">
                                    :
                                </td>
                                <td valign="top" class="customerdetailstamleTdsize">
                                    <div class="iabody_auditlog">
                                        <asp:Label ID="lblSC" runat="server" Text="--"></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr id="trCycle" runat="server">
                                <td valign="top" class="customerdetailstamleTd">
                                    <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal>
                                </td>
                                <td valign="top" class="customerTd_auditlog">
                                    :
                                </td>
                                <td class="customerdetailstamleTdsize">
                                    <div class="iabody_auditlog">
                                        <asp:Label ID="lblCycle" runat="server" Text="--"></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr id="trBook" runat="server">
                                <td valign="top" class="customerdetailstamleTd">
                                    <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal>
                                </td>
                                <td valign="top" class="customerTd_auditlog">
                                    :
                                </td>
                                <td class="customerdetailstamleTdsize">
                                    <div class="iabody_auditlog">
                                        <asp:Label ID="lblBook" runat="server" Text="--"></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr id="trTariff" runat="server">
                                <td valign="top" class="customerdetailstamleTd">
                                    <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                                </td>
                                <td valign="top" class="customerdetailstamleTd">
                                    :
                                </td>
                                <td valign="top" class="customerdetailstamleTdsize">
                                    <div class="iabody_auditlog">
                                        <asp:Label ID="lblTariff" runat="server" Text="--"></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr id="trFD" runat="server">
                                <td class="customerdetailstamleTd">
                                    <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, FROM_DATE%>"></asp:Literal>
                                </td>
                                <td class="customerdetailstamleTd">
                                    :
                                </td>
                                <td class="customerdetailstamleTdsize">
                                    <asp:Label ID="lblFromDate" runat="server" Text="--"></asp:Label>
                                </td>
                            </tr>
                            <tr id="trTD" runat="server">
                                <td class="customerdetailstamleTd">
                                    <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, TO_DATE%>"></asp:Literal>
                                </td>
                                <td class="customerdetailstamleTd">
                                    :
                                </td>
                                <td class="customerdetailstamleTdsize">
                                    <asp:Label ID="lblToDate" runat="server" Text="--"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="clr">
            </div>
            <%--<div class="grid_boxes">
                <div class="grid_paging_top">
                    <div class="paging_top_title">
                        &nbsp;
                    </div>
                    <div class="paging_top_right_content" id="divpaging" visible="false" runat="server">
                        <asp:Button ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" CssClass="exporttoexcelButton" />
                    </div>
                </div>
            </div>
            <div class="grid_tb" id="divgrid" runat="server">
                <asp:GridView ID="gvMeterChanges" runat="server" AutoGenerateColumns="false" HeaderStyle-CssClass="grid_tb_hd">
                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                    <EmptyDataTemplate>
                        No Meter Changes Found.
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Global Account No">
                            <ItemTemplate>
                                <asp:Label ID="lblAccount" runat="server" Text='<%# Eval("AccountNo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="OldAccountNo" HeaderText="Old Account No" />
                        <asp:BoundField DataField="Name" HeaderText="Name" />
                        <asp:BoundField DataField="ClassName" HeaderText="Tariff" />

                        <asp:BoundField DataField="OldMeterNo" HtmlEncode="false" HeaderText="Old Meter No" />
                        <asp:BoundField DataField="NewMeterNo" HtmlEncode="false" HeaderText="New Meter No" />
                        <asp:BoundField DataField="Readings" HtmlEncode="False" HeaderText="Old & New Meter Readings" />
                        <asp:BoundField DataField="OldMeterType" HeaderText="Old MeterType" />
                        <asp:BoundField DataField="NewMeterType" HeaderText="New MeterType" />
                        <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="AssignedMeterDate" HeaderText="Meter Changed Date" />
                        <asp:BoundField DataField="CreatedBy" HeaderText="Requested By" />
                        <asp:BoundField DataField="TransactionDate" HeaderText="Requested Date" />
                        <asp:BoundField DataField="ApprovalStatus" HeaderText="Approval Status" />
                        <asp:BoundField DataField="LastTransactionDate" HeaderText="Last Transaction Date" />
                        <asp:BoundField DataField="Remarks" HtmlEncode="false" HeaderText="Remarks" />
                    </Columns>
                </asp:GridView>
            </div>--%>
            <div class="grid_boxes" id="divrptrdetails" runat="server" visible="false">
                <div class="grid_paging_top">
                    <div class="paging_top_title" style="position: relative; top: 24px;">
                        <asp:Literal ID="litBuList" runat="server" Text="Name Change Logs List"></asp:Literal>
                    </div>
                    <div class="clear">
                    </div>
                    <div>
                        <div class="consume_name fltl" style="position: relative; top: 30px;">
                            <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, TOTAL_CHANGES%>"></asp:Label>
                        </div>
                        <div class="consume_input fltl" style="margin-right: 10px; padding-right: 10px; position: relative;
                            top: 30px;">
                            <asp:Label ID="lblTopTotalCustomers" runat="server" Text="--"></asp:Label>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="paging_top_right_content" id="divTopPaging" runat="server">
                            <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                        </div>
                    </div>
                    <div align="right">
                        <asp:Button ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" CssClass="exporttoexcelButton" />
                    </div>
                </div>
                <div class="clr pad_10">
                </div>
                <div class="rptr_tb">
                    <table style="border-collapse: collapse; border: 1px #000 solid;">
                        <tr class="rptr_tb_hd" align="center">
                            <th>
                                <asp:Label ID="lblHSno" runat="server" Text="<%$ Resources:Resource, GRID_SNO%>"></asp:Label>
                            </th>
                            <th>
                                <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Label>
                            </th>
                            <th>
                                <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Label>
                            </th>
                            <th>
                                <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Label>
                            </th>
                            <th>
                                <asp:Label ID="Label12" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Label>
                            </th>
                            <th>
                                <asp:Label ID="Label13" runat="server" Text="Old Meter No"></asp:Label>
                            </th>
                            <th>
                                <asp:Label ID="Label8" runat="server" Text="New Meter No"></asp:Label>
                            </th>
                            <th>
                                <asp:Label ID="Label17" runat="server" Text="Old & New Meter Readings"></asp:Label>
                            </th>
                            <th>
                                <asp:Label ID="Label10" runat="server" Text="Old MeterType"></asp:Label>
                            </th>
                            <th>
                                <asp:Label ID="Label7" runat="server" Text="New MeterType"></asp:Label>
                            </th>
                            <th>
                                <asp:Label ID="Label22" runat="server" Text="Meter Changed Date"></asp:Label>
                            </th>
                            <th>
                                <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, REQ_BY%>"></asp:Label>
                            </th>
                            <th>
                                <asp:Label ID="Label11" runat="server" Text="<%$ Resources:Resource, REQ_DATE%>"></asp:Label>
                            </th>
                            <th>
                                <asp:Label ID="Label23" runat="server" Text="Last Approved By"></asp:Label>
                            </th>
                            <th>
                                <asp:Label ID="Label24" runat="server" Text="Last Approved Date"></asp:Label>
                            </th>
                            <th>
                                <asp:Label ID="Label14" runat="server" Text="<%$ Resources:Resource, STATUS%>"></asp:Label>
                            </th>
                            <th>
                                <asp:Label ID="Label15" runat="server" Text="<%$ Resources:Resource, LAS_TRANSACTION_DATE%>"></asp:Label>
                            </th>
                            <th>
                                <asp:Label ID="Label16" runat="server" Text="<%$ Resources:Resource, REMARKS%>"></asp:Label>
                            </th>
                        </tr>
                        <asp:Repeater ID="rptrMeterChanges" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td class="rptr_tb_td" align="center">
                                        <asp:Label runat="server" ID="lblsno" Text='<%#Eval("RowNumber") %>'></asp:Label>
                                    </td>
                                    <td class="rptr_tb_td" align="center">
                                        <asp:Label ID="lblAccount" runat="server" Text='<%# Eval("AccountNo") %>'></asp:Label>
                                    </td>
                                    <td class="rptr_tb_td" align="left">
                                        <asp:Label runat="server" ID="lblOldAccountNo" Text='<%#Eval("OldAccountNo") %>'></asp:Label>
                                    </td>
                                    <td class="rptr_tb_td" align="left">
                                        <asp:Label runat="server" ID="lblName" Text='<%#Eval("Name") %>'></asp:Label>
                                    </td>
                                    <td class="rptr_tb_td" align="left">
                                        <asp:Label runat="server" ID="lblClassName" Text='<%#Eval("ClassName") %>'></asp:Label>
                                    </td>
                                    <td class="rptr_tb_td" align="left">
                                        <asp:Label runat="server" ID="lblOldMeterNo" Text='<%#Eval("OldMeterNo") %>'></asp:Label>
                                    </td>
                                    <td class="rptr_tb_td" align="left">
                                        <asp:Label ID="lblNewMeterNo" runat="server" Text='<%#Eval("NewMeterNo")%>'></asp:Label>
                                    </td>
                                    <td class="rptr_tb_td" align="center">
                                        <asp:Label runat="server" HtmlEncode="False" ID="lblOldSurName" Text='<%#Eval("Readings") %>'></asp:Label>
                                    </td>
                                    <td class="rptr_tb_td" align="left">
                                        <asp:Label runat="server" ID="lblOldMeterType" Text='<%#Eval("OldMeterType") %>'></asp:Label>
                                    </td>
                                    <td class="rptr_tb_td" align="left">
                                        <asp:Label runat="server" ID="lblNewMeterType" Text='<%#Eval("NewMeterType") %>'></asp:Label>
                                    </td>
                                    <td class="rptr_tb_td" align="center">
                                        <asp:Label runat="server" ID="lblAssignedMeterDate" Text='<%#Eval("AssignedMeterDate") %>'></asp:Label>
                                    </td>
                                    <td class="rptr_tb_td" align="center">
                                        <asp:Label runat="server" ID="Label9" Text='<%#Eval("CreatedBy") %>'></asp:Label>
                                    </td>
                                    <td class="rptr_tb_td" align="center">
                                        <asp:Label runat="server" ID="Label18" Text='<%#Eval("TransactionDate") %>'></asp:Label>
                                    </td>
                                    <td class="rptr_tb_td" align="center">
                                        <asp:Label runat="server" ID="Label25" Text='<%#Eval("ModifiedBy") %>'></asp:Label>
                                    </td>
                                    <td class="rptr_tb_td" align="center">
                                        <asp:Label runat="server" ID="Label22" Text='<%#Eval("ModifiedDate") %>'></asp:Label>
                                    </td>
                                    <td class="rptr_tb_td" align="center">
                                        <asp:Label runat="server" ID="Label19" Text='<%#Eval("ApprovalStatus") %>'></asp:Label>
                                    </td>
                                    <td class="rptr_tb_td" align="center">
                                        <asp:Label runat="server" ID="Label20" Text='<%#Eval("LastTransactionDate") %>'></asp:Label>
                                    </td>
                                    <td class="rptr_tb_td" align="center">
                                        <asp:Label runat="server" HtmlEncode="false" ID="Label21" Text='<%#Eval("Remarks") %>'></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <tr align="center" runat="server" id="divNoDataFound" visible="false">
                            <td colspan="8">
                                No data found
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="reports_txt">
                    <div class="consume_name fltl">
                        <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, TOTAL_CHANGES%>"></asp:Label>
                    </div>
                    <div class="consume_input fltl" style="margin-right: 10px; padding-right: 10px;">
                        <asp:Label ID="lblDownTotalCustomers" runat="server" Text="--"></asp:Label>
                    </div>
                    <div id="divDownPaging" runat="server">
                        <div class="grid_paging_bottom">
                            <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="hfPageSize" runat="server" />
            <asp:HiddenField ID="hfPageNo" runat="server" />
            <asp:HiddenField ID="hfLastPage" runat="server" />
            <asp:HiddenField ID="hfTotalRecords" runat="server" />
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        function pageLoad() {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        }
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
