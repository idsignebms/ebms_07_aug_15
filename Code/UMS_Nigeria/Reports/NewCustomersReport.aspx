﻿<%@ Page Title="::New Customers Report::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="NewCustomersReport.aspx.cs"
    Inherits="UMS_Nigeria.Reports.NewCustomersReport" %>

<%@ Register Src="../UserControls/UCPagingV2.ascx" TagName="UCPagingV2" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" AssociatedUpdatePanelID="upnlCustomerReorder">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdatePanel ID="upnlCustomerReorder" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddCycle" runat="server" Text="New Customers Report"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:DropDownList ID="ddlBU" CssClass="text-box select_box" AutoPostBack="true" runat="server"
                                    OnSelectedIndexChanged="ddlBU_SelectedIndexChanged">
                                    <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <span id="spanddlBU" class="spancolor"></span>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="divServiceUnits" runat="server" visible="false">
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal>
                                    </label>
                                    <div class="space">
                                    </div>
                                    <asp:CheckBox runat="server" ID="ChkSUAll" Text="All" AutoPostBack="true" />
                                    <asp:CheckBoxList ID="cblServiceUnits" Width="45%" AutoPostBack="true" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceUnits_SelectedIndexChanged">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="divServiceCenters" runat="server" visible="false">
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal>
                                    </label>
                                    <div class="space">
                                    </div>
                                    <asp:CheckBox runat="server" ID="chkSCAll" Text="All" AutoPostBack="true" />
                                    <asp:CheckBoxList ID="cblServiceCenters" Width="45%" RepeatDirection="Horizontal"
                                        AutoPostBack="true" RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceCenters_SelectedIndexChanged">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="divCycles" runat="server" visible="false">
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal>
                                    </label>
                                    <div class="space">
                                    </div>
                                    <asp:CheckBox runat="server" ID="chkCycleAll" Text="All" AutoPostBack="true" />
                                    <asp:CheckBoxList ID="cblCycles" AutoPostBack="true" Width="45%" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblCycles_SelectedIndexChanged">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="divBookNos" runat="server" visible="false">
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litBookNo" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal>
                                    </label>
                                    <div class="space">
                                    </div>
                                    <asp:CheckBox runat="server" ID="chkBooksAll" Text="All" />
                                    <asp:CheckBoxList ID="cblBooks" Width="45%" RepeatDirection="Horizontal" RepeatColumns="3"
                                        runat="server">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litFromDate" runat="server" Text="<%$ Resources:Resource, FROM_DATE%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:TextBox ID="txtFromDate" Style="width: 136px;" MaxLength="10" runat="server" placeholder="Enter From Date"
                                    AutoComplete="off" onblur="TextBoxBlurValidationlbl(this, 'spanFromDate', 'From Date')"></asp:TextBox>
                                <asp:Image ID="imgFromDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                    vertical-align: middle" AlternateText="Calender" runat="server" />
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtFromDate"
                                    FilterType="Custom,Numbers" ValidChars="/">
                                </asp:FilteredTextBoxExtender>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litToDate" runat="server" Text="<%$ Resources:Resource, TO_DATE%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:TextBox ID="txtToDate" Style="width: 136px;" MaxLength="10" placeholder="Enter To Date" runat="server" AutoComplete="off"
                                    onblur="TextBoxBlurValidationlbl(this, 'spanToDate', 'To Date')"></asp:TextBox>
                                <asp:Image ID="imgToDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                    vertical-align: middle" AlternateText="Calender" runat="server" />
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtToDate"
                                    FilterType="Custom,Numbers" ValidChars="/">
                                </asp:FilteredTextBoxExtender>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <asp:Button ID="btnExport" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s"
                                runat="server" OnClick="btnExport_Click" />
                        </div>
                    </div>
                    <div class="grid_boxes">
                        <div class="grid_paging_top">
                            <div class="paging_top_title">
                                &nbsp;
                            </div>
                            <div class="paging_top_right_content" id="divpaging" runat="server">
                                <asp:Button ID="btnExportExcel" runat="server" CssClass="excel" Text="Export to excel"
                                    OnClick="btnExportExcel_Click" />
                                <asp:Button ID="btnPrint" runat="server" Visible="false" OnClientClick="return printGrid()"
                                    CssClass="print" Text="Print" />
                                <uc2:UCPagingV2 ID="UCPaging1" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvNewCustomerList" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="grid_tb_hd">
                            <EmptyDataRowStyle HorizontalAlign="Left" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <%--<asp:BoundField HeaderText="<%$ Resources:Resource, GRID_SNO%>" DataField="RowNumber" />--%>
                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, GRID_SNO%>"
                                    runat="server">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" DataField="AccountNo" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, NAME%>" DataField="Name" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, HOME_CONTACT_NO%>" DataField="HomeContactNo" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, SERVICE_ADDRESS%>" DataField="ServiceAddress" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, METER_NO%>" DataField="MeterNo" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, AVERAGE_READING%>" DataField="AverageReading" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, TARIFF%>" DataField="Tariff" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, BILL_GENERATED_DATE%>" DataField="CreatedDate" />
                                <%--<asp:BoundField HeaderText="<%$ Resources:Resource, AMOUNT%>" DataField="Amount" />--%>
                            </Columns>
                            <RowStyle HorizontalAlign="center"></RowStyle>
                        </asp:GridView>
                        <asp:HiddenField ID="hfPageSize" runat="server" />
                        <asp:HiddenField ID="hfPageNo" runat="server" />
                        <asp:HiddenField ID="hfLastPage" runat="server" />
                        <asp:HiddenField ID="hfTotalRecords" runat="server" />
                    </div>
                    <div class="grid_boxes">
                        <div id="divdownpaging" runat="server">
                            <div class="grid_paging_bottom">
                                <uc2:UCPagingV2 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExportExcel" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
            </asp:ModalPopupExtender>
            <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
            <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                <div class="popheader popheaderlblred">
                    <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                </div>
                <div class="footer popfooterbtnrt">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                            CssClass="ok_btn" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        function pageLoad() {
            Calendar('<%=txtFromDate.ClientID %>', '<%=imgFromDate.ClientID %>', "-2:+8");
            Calendar('<%=txtToDate.ClientID %>', '<%=imgToDate.ClientID %>', "-2:+8");
            DisplayMessage('<%= pnlMessage.ClientID %>');
        };
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <script src="../scripts/jquery.ui.widget.js" type="text/javascript"></script>
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/NewCustomersReport.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <%--    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>--%>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
