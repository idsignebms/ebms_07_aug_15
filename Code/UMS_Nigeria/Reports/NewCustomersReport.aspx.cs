﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddEmployee
                     
 Developer        : id027-T.Karthik
 Creation Date    : 01-11-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using Resources;
using UMS_NigeriaBE;
using System.Data;
using org.in2bits.MyXls;
using System.Configuration;

namespace UMS_Nigeria.Reports
{
    public partial class NewCustomersReport : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        ReportsBal objReportsBal = new ReportsBal();
        public int PageNum;
        string Key = "NewCustomersReport";
        #endregion

        #region Properties

        public string BU
        {
            get { return string.IsNullOrEmpty(ddlBU.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlBU, ",") : ddlBU.SelectedValue; }
            set { ddlBU.SelectedValue = value.ToString(); }
        }
        public string SU
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceUnits, ","); }
        }
        public string SC
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceCenters, ","); }
        }
        public string Cycle
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblCycles, ","); }
        }
        public string BookNo
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblBooks, ","); }
        }
        public string FromDate
        {
            get { return string.IsNullOrEmpty(txtFromDate.Text) ? string.Empty : _objCommonMethods.Convert_MMDDYY(txtFromDate.Text); }
        }
        public string ToDate
        {
            get { return string.IsNullOrEmpty(txtToDate.Text) ? string.Empty : _objCommonMethods.Convert_MMDDYY(txtToDate.Text); }
        }

        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }

        #endregion

        #region Methods


        private void CheckBoxesJS()
        {
            ChkSUAll.Attributes.Add("onclick", "CheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            chkSCAll.Attributes.Add("onclick", "CheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            chkCycleAll.Attributes.Add("onclick", "CheckAll('" + chkCycleAll.ClientID + "','" + cblCycles.ClientID + "')");
            chkBooksAll.Attributes.Add("onclick", "CheckAll('" + chkBooksAll.ClientID + "','" + cblBooks.ClientID + "')");

            cblServiceUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            cblServiceCenters.Attributes.Add("onclick", "UnCheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            cblCycles.Attributes.Add("onclick", "UnCheckAll('" + chkCycleAll.ClientID + "','" + cblCycles.ClientID + "')");
            cblBooks.Attributes.Add("onclick", "UnCheckAll('" + chkBooksAll.ClientID + "','" + cblBooks.ClientID + "')");
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void GetNewCustomersList()
        {
            try
            {
                RptCheckMetersBe objReportBe = new RptCheckMetersBe();
                objReportBe.BU_ID = BU;
                objReportBe.SU_ID = SU;
                objReportBe.ServiceCenterId = SC;
                objReportBe.CycleId = Cycle;
                objReportBe.BookNo = BookNo;
                objReportBe.FromDate = FromDate;
                objReportBe.ToDate = ToDate;
                objReportBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                objReportBe.PageSize = PageSize;

                DataSet ds = new DataSet();
                ds = objReportsBal.GetNewCustomersReport(objReportBe);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    divpaging.Visible = divdownpaging.Visible = divgrid.Visible = true;
                    int totalRecord = Convert.ToInt32(ds.Tables[0].Rows[0]["TotalRecords"]);
                    hfTotalRecords.Value = totalRecord.ToString();

                    //BindPagingDropDown(ds.Tables[0].Rows.Count);
                    //CreatePagingDT(ds.Tables[0].Rows.Count);

                    gvNewCustomerList.DataSource = ds.Tables[0];
                    gvNewCustomerList.DataBind();
                }
                else
                {
                    divpaging.Visible = divdownpaging.Visible = divgrid.Visible = false;
                    hfTotalRecords.Value = Constants.Zero.ToString();
                    Message("Customers Not Found", UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region ddlEvents
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session[UMS_Resource.SESSION_LOGINID] != null)
                {
                    pnlMessage.CssClass = lblMessage.Text = string.Empty;
                    if (!IsPostBack)
                    {
                        string path = string.Empty;
                        _objCommonMethods.BindUserBusinessUnits(ddlBU, string.Empty, false, UMS_Resource.DROPDOWN_SELECT);
                        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        {
                            ddlBU.SelectedIndex = ddlBU.Items.IndexOf(ddlBU.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                            ddlBU.Enabled = false;
                            ddlBU_SelectedIndexChanged(ddlBU, new EventArgs());
                        }
                        CheckBoxesJS();

                        hfPageNo.Value = Constants.pageNoValue.ToString();
                        GetNewCustomersList();

                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    }
                }
                else
                {
                    Response.Redirect(UMS_Resource.DEFAULT_PAGE);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlBU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _objCommonMethods.BindUserServiceUnits_BuIds(cblServiceUnits, BU, Resource.DDL_ALL, false);
                if (cblServiceUnits.Items.Count > 0)
                    divServiceUnits.Visible = true;
                else
                    divServiceUnits.Visible = false;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                cblBooks.Items.Clear();
                cblServiceCenters.Items.Clear();
                divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = chkSCAll.Checked = divServiceCenters.Visible = false;
                if (!string.IsNullOrEmpty(SU))
                {
                    _objCommonMethods.BindUserServiceCenters_SuIds(cblServiceCenters, SU, Resource.DDL_ALL, false);
                    if (cblServiceCenters.Items.Count > 0)
                        divServiceCenters.Visible = true;
                    else
                        divServiceCenters.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceCenters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                cblBooks.Items.Clear();
                divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = false;
                if (!string.IsNullOrEmpty(SC))
                {
                    _objCommonMethods.BindCyclesByBUSUSC(cblCycles, BU, SU, SC, true, string.Empty, false);
                    if (cblCycles.Items.Count > 0)
                        divCycles.Visible = true;
                    else
                        divCycles.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblCycles_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblBooks.Items.Clear();
                chkBooksAll.Checked = divBookNos.Visible = false;
                if (!string.IsNullOrEmpty(Cycle))
                {
                    _objCommonMethods.BindAllBookNumbers(cblBooks, Cycle, true, false, string.Empty);
                    if (cblBooks.Items.Count > 0)
                        divBookNos.Visible = true;
                    else
                        divBookNos.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            GetNewCustomersList();
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                RptCheckMetersBe objReportBe = new RptCheckMetersBe();
                objReportBe.BU_ID = BU;
                objReportBe.SU_ID = SU;
                objReportBe.ServiceCenterId = SC;
                objReportBe.CycleId = Cycle;
                objReportBe.BookNo = BookNo;
                objReportBe.FromDate = FromDate;
                objReportBe.ToDate = ToDate;
                objReportBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                objReportBe.PageSize = Convert.ToInt32(hfTotalRecords.Value); ;
                DataSet ds = new DataSet();
                ds = objReportsBal.GetNewCustomersReport(objReportBe);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    var BUlist = (from list in ds.Tables[0].AsEnumerable()
                                  orderby list["CustomerSortOrder"] ascending
                                  orderby list["BookSortOrder"] ascending
                                  orderby list["CycleName"] ascending
                                  group list by list.Field<string>("BusinessUnitName") into g
                                  select new { BusinessUnit = g.Key }).ToList();

                    var SUlist = (from list in ds.Tables[0].AsEnumerable()
                                  orderby list["CustomerSortOrder"] ascending
                                  orderby list["BookSortOrder"] ascending
                                  orderby list["CycleName"] ascending
                                  group list by list.Field<string>("ServiceUnitName") into g
                                  select new { ServiceUnit = g.Key }).ToList();

                    var SClist = (from list in ds.Tables[0].AsEnumerable()
                                  orderby list["CustomerSortOrder"] ascending
                                  orderby list["BookSortOrder"] ascending
                                  orderby list["CycleName"] ascending
                                  group list by list.Field<string>("ServiceCenterName") into g
                                  select new { ServiceCenter = g.Key }).ToList();

                    var Cyclelist = (from list in ds.Tables[0].AsEnumerable()
                                     orderby list["CustomerSortOrder"] ascending
                                     orderby list["BookSortOrder"] ascending
                                     orderby list["CycleName"] ascending
                                     group list by list.Field<string>("CycleName") into g
                                     select new { Cycle = g.Key }).ToList();

                    var Booklist = (from list in ds.Tables[0].AsEnumerable()
                                    orderby list["CustomerSortOrder"] ascending
                                    orderby list["BookSortOrder"] ascending
                                    orderby list["CycleName"] ascending
                                    group list by list.Field<string>("BookCode") into g
                                    select new { Book = g.Key }).ToList();

                    if (BUlist.Count() > 0)
                    {
                        XlsDocument xls = new XlsDocument();
                        Worksheet sheet = xls.Workbook.Worksheets.Add("sheet");
                        Cells cells = sheet.Cells;
                        Cell _objCell = null;
                        _objCell = cells.Add(1, 4, Resource.BEDC + "\n" + Resource.NEW_CUSTOMERS_REPORT_EXCEL_HEAD + DateTime.Today.ToShortDateString());
                        sheet.AddMergeArea(new MergeArea(1, 2, 4, 6));

                        int DetailsRowNo = 6;
                        int CustomersRowNo = 10;

                        string From = string.IsNullOrEmpty(txtFromDate.Text) ? "--" : txtFromDate.Text;
                        string To = string.IsNullOrEmpty(txtToDate.Text) ? "--" : txtToDate.Text;

                        cells.Add(4, 1, "From Date");
                        cells.Add(4, 4, "To Date");
                        cells.Add(4, 2, From);
                        cells.Add(4, 5, To);

                        for (int i = 0; i < BUlist.Count; i++)//BU
                        {
                            for (int j = 0; j < SUlist.Count; j++)//SU
                            {
                                for (int k = 0; k < SClist.Count; k++)//SC
                                {
                                    for (int l = 0; l < Cyclelist.Count; l++)//Cycle
                                    {
                                        for (int m = 0; m < Booklist.Count; m++)//Book
                                        {

                                            var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                             where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                             && list.Field<string>("ServiceUnitName") == SUlist[j].ServiceUnit
                                                             && list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                                             && list.Field<string>("CycleName") == Cyclelist[l].Cycle
                                                             && list.Field<string>("BookCode") == Booklist[m].Book
                                                             orderby list["CustomerSortOrder"] ascending
                                                             select list).ToList();

                                            if (Customers.Count() > 0)
                                            {
                                                cells.Add(DetailsRowNo, 1, "Business Unit");
                                                cells.Add(DetailsRowNo, 4, "Service Unit");
                                                cells.Add(DetailsRowNo + 1, 1, "Service Center");
                                                cells.Add(DetailsRowNo + 1, 4, "BookGroup");
                                                cells.Add(DetailsRowNo + 2, 1, "Book");

                                                cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                                cells.Add(DetailsRowNo, 5, SUlist[j].ServiceUnit).Font.Bold = true;
                                                cells.Add(DetailsRowNo + 1, 2, SClist[k].ServiceCenter).Font.Bold = true;
                                                cells.Add(DetailsRowNo + 1, 5, Cyclelist[l].Cycle).Font.Bold = true;
                                                cells.Add(DetailsRowNo + 2, 2, Booklist[m].Book).Font.Bold = true;

                                                cells.Add(DetailsRowNo + 3, 1, "S.No").Font.Bold = true;
                                                cells.Add(DetailsRowNo + 3, 2, "Account No").Font.Bold = true;
                                                cells.Add(DetailsRowNo + 3, 3, "Name").Font.Bold = true;
                                                cells.Add(DetailsRowNo + 3, 4, "Service Address").Font.Bold = true;
                                                cells.Add(DetailsRowNo + 3, 5, "Home Phone").Font.Bold = true;
                                                cells.Add(DetailsRowNo + 3, 6, "Meter No").Font.Bold = true;
                                                cells.Add(DetailsRowNo + 3, 7, "Tariff").Font.Bold = true;
                                                cells.Add(DetailsRowNo + 3, 8, "Average Consumption").Font.Bold = true;
                                                cells.Add(DetailsRowNo + 3, 9, "Created Date").Font.Bold = true;


                                                CustomersRowNo++;
                                                for (int n = 0; n < Customers.Count; n++)
                                                {
                                                    DataRow dr = (DataRow)Customers[n];
                                                    cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                                                    cells.Add(CustomersRowNo, 2, dr["AccountNo"].ToString());
                                                    cells.Add(CustomersRowNo, 3, dr["Name"].ToString());
                                                    cells.Add(CustomersRowNo, 4, dr["ServiceAddress"].ToString());
                                                    cells.Add(CustomersRowNo, 5, dr["HomeContactNo"].ToString());
                                                    cells.Add(CustomersRowNo, 6, dr["MeterNo"].ToString());
                                                    cells.Add(CustomersRowNo, 7, dr["Tariff"].ToString());
                                                    cells.Add(CustomersRowNo, 8, dr["AverageReading"].ToString());
                                                    cells.Add(CustomersRowNo, 9, dr["CreatedDate"].ToString());
                                                    CustomersRowNo++;
                                                }

                                                DetailsRowNo = CustomersRowNo + 2;
                                                CustomersRowNo = CustomersRowNo + 5;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                        fileName = "NewCustomers" + fileName;
                        string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                        xls.FileName = filePath;
                        xls.Save();
                        _objiDsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                    }
                    else
                        Message("Customers Not Found", UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                    Message("Customers Not Found", UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Paging

        /*protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                GetNewCustomersList();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                GetNewCustomersList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                GetNewCustomersList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                GetNewCustomersList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                GetNewCustomersList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                _objiDsignBal.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                _objCommonMethods.BindPagingDropDownListReport(TotalRecords, this.PageSize, ddlPageSize);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }*/

        #endregion

    }
}