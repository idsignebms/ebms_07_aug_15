﻿<%@ Page Title="::No Usage Customers Report::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="NoUsageCustomersReport.aspx.cs"
    Inherits="UMS_Nigeria.Reports.NoUsageCustomersReport" %>

<%@ Register Src="../UserControls/UCPagingV2.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdateProgress ID="UpdateProgress" runat="server" AssociatedUpdatePanelID="upnlCustomerReorder">
                <ProgressTemplate>
                    <center>
                        <div id="loading-div" class="pbloading">
                            <div id="title-loading" class="pbtitle">
                                BEDC</div>
                            <center>
                                <img src="../images/loading.GIF" class="pbimg"></center>
                            <p class="pbtxt">
                                Loading Please wait.....</p>
                        </div>
                    </center>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
                PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
            <asp:UpdatePanel ID="upnlCustomerReorder" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddCycle" runat="server" Text="No Usage Customers Report"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <%--<asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>--%>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="previous">
                        <a href="ReportsSummary.aspx">< < Back To Summary</a>
                    </div>
                    <div class="inner-box">
                        <%--<div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litFromDate" runat="server" Text="<%$ Resources:Resource, FROM_DATE%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:TextBox ID="txtFromDate" placeholder="Enter From Date" Style="width: 136px;"
                                    MaxLength="10" runat="server" AutoComplete="off" onblur="TextBoxBlurValidationlbl(this, 'spanFromDate', 'From Date')"></asp:TextBox>
                                <asp:Image ID="imgFromDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                    vertical-align: middle" AlternateText="Calender" runat="server" />
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtFromDate"
                                    FilterType="Custom,Numbers" ValidChars="/">
                                </asp:FilteredTextBoxExtender>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litToDate" runat="server" Text="<%$ Resources:Resource, TO_DATE%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:TextBox ID="txtToDate" placeholder="Enter To Date" Style="width: 136px;" MaxLength="10"
                                    runat="server" AutoComplete="off" onblur="TextBoxBlurValidationlbl(this, 'spanToDate', 'To Date')"></asp:TextBox>
                                <asp:Image ID="imgToDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                    vertical-align: middle" AlternateText="Calender" runat="server" />
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtToDate"
                                    FilterType="Custom,Numbers" ValidChars="/">
                                </asp:FilteredTextBoxExtender>
                            </div>
                        </div>
                        <div class="clr">
                        </div>--%>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <asp:Literal ID="litYear" runat="server" Text="<%$ Resources:Resource, YEAR%>"></asp:Literal><span
                                    class="span_star">*</span><br />
                                <asp:DropDownList ID="ddlYear" runat="server" CssClass="text-box select_box" onchange="DropDownlistOnChangelbl(this,'spanddlYear',' Year')"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                <span id="spanddlYear" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <asp:Literal ID="litMonth" runat="server" Text="<%$ Resources:Resource, MONTH%>"></asp:Literal><span
                                    class="span_star">*</span><br />
                                <asp:DropDownList ID="ddlMonth" runat="server" CssClass="text-box select_box" onchange="DropDownlistOnChangelbl(this,'spanddlMonth',' Month')">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                <span id="spanddlMonth" class="span_color"></span>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:DropDownList ID="ddlBU" AutoPostBack="true" runat="server" CssClass="text-box select_box"
                                    OnSelectedIndexChanged="ddlBU_SelectedIndexChanged">
                                    <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <span id="spanddlBU" class="spancolor"></span>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="divServiceUnits" runat="server" visible="false">
                            <div class="check_baa iabody_lblBook">
                                <div class="text-inner_a">
                                    <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" ID="ChkSUAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                        <asp:CheckBoxList ID="cblServiceUnits" class="text-inner-b" AutoPostBack="true" RepeatDirection="Horizontal"
                                            RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceUnits_SelectedIndexChanged">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="divServiceCenters" runat="server" visible="false">
                            <div class="check_baa iabody_lblBook">
                                <div class="text-inner_a">
                                    <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" class="text-inner-b" ID="chkSCAll" Text="All" AutoPostBack="true" />
                                        <asp:CheckBoxList ID="cblServiceCenters" class="text-inner-b" RepeatDirection="Horizontal"
                                            AutoPostBack="true" RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceCenters_SelectedIndexChanged">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="divCycles" runat="server" visible="false">
                            <div class="check_baa iabody_lblBook">
                                <div class="text-inner_a">
                                    <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" class="text-inner-b" ID="chkCycleAll" Text="All" AutoPostBack="true" />
                                        <asp:CheckBoxList ID="cblCycles" class="text-inner-b" AutoPostBack="true" RepeatDirection="Horizontal"
                                            RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblCycles_SelectedIndexChanged">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="divBookNos" runat="server" visible="false">
                            <div class="check_baa iabody_lblBook">
                                <div class="text-inner_a">
                                    <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" ID="chkBooksAll" class="text-inner-b" Text="All" />
                                        <asp:CheckBoxList ID="cblBooks" class="text-inner-b" RepeatDirection="Horizontal"
                                            RepeatColumns="3" runat="server">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s"
                                runat="server" OnClick="btnSearch_Click" OnClientClick="return Validate();" />
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="grid_boxes" id="divrptrDetails" runat="server" visible="false">
                        <div class="grid_paging_top">
                            <div class="paging_top_title">
                                <asp:Literal ID="Literal6" runat="server" Text="No Usage Customers Report"></asp:Literal>
                            </div>
                            <div class="clear">
                            </div>
                            <div id="divToprptrContent" runat="server" visible="false">
                                <div class="consume_name fltl">
                                    <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, TOTAL_CUSTOMERS%>"></asp:Label>
                                </div>
                                <div class="consume_input fltl" style="margin-right: 10px; 
                                    padding-right: 10px;">
                                    <asp:Label ID="lblTopTotalCustomers" runat="server" Text="--"></asp:Label>
                                </div>
                                <div class="clear">
                                </div>
                                <div class="paging_top_right_content" id="divTopPaging" runat="server">
                                    <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                                </div>
                            </div>
                            <div align="right" runat="server" id="divExport" visible="false">
                                <asp:Button ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" CssClass="exporttoexcelButton" /></div>
                        </div>
                        <div class="clr pad_10"></div>
                        <div class="rptr_tb">
                            <table style="border-collapse: collapse; border:1px #000 solid;">
                                <tr class="rptr_tb_hd" align="center" runat="server" visible="false" id="rptrheader">
                                    <th>
                                        <asp:Label ID="lblHSno" runat="server" Text="<%$ Resources:Resource, GRID_SNO%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label10" runat="server" Text="<%$ Resources:Resource, ADDITIONAL_CHARGES%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label11" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, BILL_GENERATED_DATE%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label12" runat="server" Text="<%$ Resources:Resource, BILL_AMOUNT%>"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="Label13" runat="server" Text="<%$ Resources:Resource, READCODE%>"></asp:Label>
                                    </th>
                                </tr>
                                <asp:Repeater ID="rptrDebitBalence" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td class="rptr_tb_td" align="center">
                                                <asp:Label runat="server" ID="lblsno" Text='<%#Eval("RowNumber") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="center">
                                                <asp:Label runat="server" ID="lblAccNo" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="left">
                                                <asp:Label runat="server" ID="lblOldAccountNo" Text='<%#Eval("OldAccountNo") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="left">
                                                <asp:Label runat="server" ID="lblName" Text='<%#Eval("Name") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="left">
                                                <asp:Label runat="server" ID="lblAddress" Text='<%#Eval("ServiceAddress") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="right">
                                                <asp:Label runat="server" ID="lblMeterNo" Text='<%#Eval("MeterNo") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="right">
                                                <asp:Label ID="lblAdditionalCharges" runat="server" Text='<%#Eval("AdditionalCharges") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="center">
                                                <asp:Label runat="server" ID="lblTariff" Text='<%#Eval("Tariff") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="center">
                                                <asp:Label runat="server" ID="Label14" Text='<%#Eval("BillGeneratedDate") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="center">
                                                <asp:Label ID="lblTotalBilledAmount" runat="server" Text='<%#Eval("TotalBilledAmount") %>'></asp:Label>
                                            </td>
                                            <td class="rptr_tb_td" align="center">
                                                <asp:Label runat="server" ID="Label16" Text='<%#Eval("ReadCode") %>'></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <tr align="center" runat="server" id="divNoDataFound" visible="false">
                                    <td colspan="11">
                                        No data found
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="divDownrptrContent" class="reports_txt" runat="server" visible="false">
                            <div class="consume_name fltl">
                                <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, TOTAL_CUSTOMERS%>"></asp:Label>
                            </div>
                            <div class="consume_input fltl" style="margin-right: 10px; 
                                padding-right: 10px;">
                                <asp:Label ID="lblDownTotalCustomers" runat="server" Text="--"></asp:Label>
                            </div>
                            <div id="divDownPaging" runat="server">
                                <div class="grid_paging_bottom">
                                    <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:HiddenField ID="hfPageSize" runat="server" />
                    <asp:HiddenField ID="hfPageNo" runat="server" />
                    <asp:HiddenField ID="hfLastPage" runat="server" />
                    <asp:HiddenField ID="hfTotalRecords" runat="server" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExportExcel" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
            </asp:ModalPopupExtender>
            <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
            <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                <div class="popheader popheaderlblred">
                    <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                </div>
                <div class="footer popfooterbtnrt">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                            CssClass="ok_btn" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>

    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/NoUsageCustomersReport.js" type="text/javascript"></script>
    <script type="text/javascript">
        function Validate() {
            var ddlYear = document.getElementById('UMSNigeriaBody_ddlYear');
            var ddlMonth = document.getElementById('UMSNigeriaBody_ddlMonth');

            var IsValid = true;

            if (DropDownlistValidationlbl(ddlYear, document.getElementById("spanddlYear"), "Year") == false) IsValid = false;
            if (DropDownlistValidationlbl(ddlMonth, document.getElementById("spanddlMonth"), "Month") == false) IsValid = false;

            if (!IsValid) {
                return false;
            }
        }
    </script>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
