﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddEmployee
                     
 Developer        : id027-T.Karthik
 Creation Date    : 04-11-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using Resources;
using UMS_NigeriaBE;
using System.Data;
using org.in2bits.MyXls;
using System.Configuration;
using UMS_NigriaDAL;
using System.Threading;
using System.Globalization;
using System.Xml;

namespace UMS_Nigeria.Reports
{
    public partial class NoUsageCustomersReport : System.Web.UI.Page
    {

        # region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        GeneralReportsBal _objGeneralReportsBal = new GeneralReportsBal();
        public int PageNum;
        string Key = "NoUsageCustomersReport";
        #endregion

        #region Properties

        public string BU
        {
            get { return string.IsNullOrEmpty(ddlBU.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlBU, ",") : ddlBU.SelectedValue; }
            set { ddlBU.SelectedValue = value.ToString(); }
        }
        public string SU
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceUnits, ","); }
        }
        public string SC
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceCenters, ","); }
        }
        public string Cycle
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblCycles, ","); }
        }
        public string BookNo
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblBooks, ","); }
        }
        //public string FromDate
        //{
        //    get { return string.IsNullOrEmpty(txtFromDate.Text) ? string.Empty : _objCommonMethods.Convert_MMDDYY(txtFromDate.Text); }
        //}
        //public string ToDate
        //{
        //    get { return string.IsNullOrEmpty(txtToDate.Text) ? string.Empty : _objCommonMethods.Convert_MMDDYY(txtToDate.Text); }
        //}
        private int YearId
        {
            get { return string.IsNullOrEmpty(ddlYear.SelectedValue) ? 0 : Convert.ToInt32(ddlYear.SelectedValue); }
            set { ddlYear.SelectedValue = value.ToString(); }
        }
        private int MonthId
        {
            get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? 0 : Convert.ToInt32(ddlMonth.SelectedValue); }
            set { ddlMonth.SelectedValue = value.ToString(); }
        }
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStartsReport : Convert.ToInt32(hfPageSize.Value); }
        }
        #endregion

        #region Methods

        private void CheckBoxesJS()
        {
            ChkSUAll.Attributes.Add("onclick", "CheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            chkSCAll.Attributes.Add("onclick", "CheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            chkCycleAll.Attributes.Add("onclick", "CheckAll('" + chkCycleAll.ClientID + "','" + cblCycles.ClientID + "')");
            chkBooksAll.Attributes.Add("onclick", "CheckAll('" + chkBooksAll.ClientID + "','" + cblBooks.ClientID + "')");

            cblServiceUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            cblServiceCenters.Attributes.Add("onclick", "UnCheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            cblCycles.Attributes.Add("onclick", "UnCheckAll('" + chkCycleAll.ClientID + "','" + cblCycles.ClientID + "')");
            cblBooks.Attributes.Add("onclick", "UnCheckAll('" + chkBooksAll.ClientID + "','" + cblBooks.ClientID + "')");
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void BindGrid()
        {
            try
            {

                RptNoUsageConsumedBe _objRptNoUsageConsumedBe = new RptNoUsageConsumedBe();
                _objRptNoUsageConsumedBe.BU_ID = BU;
                _objRptNoUsageConsumedBe.SU_ID = SU;
                _objRptNoUsageConsumedBe.SC_ID = SC;
                _objRptNoUsageConsumedBe.CycleId = Cycle;
                _objRptNoUsageConsumedBe.BookNo = BookNo;
                _objRptNoUsageConsumedBe.YearId = this.YearId;
                _objRptNoUsageConsumedBe.MonthId = this.MonthId;
                _objRptNoUsageConsumedBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                _objRptNoUsageConsumedBe.PageSize = PageSize;

                DataSet dsDebit = new DataSet();
                dsDebit = _objGeneralReportsBal.GetNoUsageConsumedReport(_objRptNoUsageConsumedBe, ReturnType.Get);

                if (dsDebit != null && dsDebit.Tables.Count > 0 && dsDebit.Tables[0].Rows.Count > 0)
                {
                    divToprptrContent.Visible = divExport.Visible = divDownrptrContent.Visible = divrptrDetails.Visible = rptrheader.Visible = true;
                    divNoDataFound.Visible = false;
                    rptrDebitBalence.DataSource = dsDebit.Tables[0];
                    rptrDebitBalence.DataBind();
                    lblTopTotalCustomers.Text = lblDownTotalCustomers.Text = hfTotalRecords.Value = dsDebit.Tables[0].Rows[0]["TotalRecords"].ToString();
                }
                else
                {
                    divToprptrContent.Visible = divExport.Visible = divDownrptrContent.Visible = rptrheader.Visible = false;
                    divNoDataFound.Visible = divrptrDetails.Visible = true;
                    rptrDebitBalence.DataSource = new DataTable(); ;
                    rptrDebitBalence.DataBind();
                    lblTopTotalCustomers.Text = lblDownTotalCustomers.Text = hfTotalRecords.Value = Constants.Zero.ToString();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session[UMS_Resource.SESSION_LOGINID] != null)
                {
                    pnlMessage.CssClass = lblMessage.Text = string.Empty;
                    if (!IsPostBack)
                    {
                        string path = string.Empty;
                        _objCommonMethods.BindUserBusinessUnits(ddlBU, string.Empty, false, UMS_Resource.DROPDOWN_SELECT);
                        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        {
                            ddlBU.SelectedIndex = ddlBU.Items.IndexOf(ddlBU.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                            ddlBU.Enabled = false;
                            ddlBU_SelectedIndexChanged(ddlBU, new EventArgs());
                        }
                        CheckBoxesJS();

                        BindYears();
                        BindMonths();
                        hfPageNo.Value = Constants.pageNoValue.ToString();
                        hfTotalRecords.Value = Constants.Zero.ToString();
                    }
                }
                else
                {
                    Response.Redirect(UMS_Resource.DEFAULT_PAGE);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindGrid();
            UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            UCPaging2.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        }

        protected void ddlBU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _objCommonMethods.BindUserServiceUnits_BuIds(cblServiceUnits, BU, Resource.DDL_ALL, false);
                if (cblServiceUnits.Items.Count > 0)
                    divServiceUnits.Visible = true;
                else
                    divServiceUnits.Visible = false;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                cblBooks.Items.Clear();
                cblServiceCenters.Items.Clear();
                divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = chkSCAll.Checked = divServiceCenters.Visible = false;
                if (!string.IsNullOrEmpty(SU))
                {
                    _objCommonMethods.BindUserServiceCenters_SuIds(cblServiceCenters, SU, Resource.DDL_ALL, false);
                    if (cblServiceCenters.Items.Count > 0)
                        divServiceCenters.Visible = true;
                    else
                        divServiceCenters.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceCenters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                cblBooks.Items.Clear();
                divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = false;
                if (!string.IsNullOrEmpty(SC))
                {
                    _objCommonMethods.BindCyclesByBUSUSC(cblCycles, BU, SU, SC, true, string.Empty, false);
                    if (cblCycles.Items.Count > 0)
                        divCycles.Visible = true;
                    else
                        divCycles.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblCycles_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblBooks.Items.Clear();
                chkBooksAll.Checked = divBookNos.Visible = false;
                if (!string.IsNullOrEmpty(Cycle))
                {
                    _objCommonMethods.BindAllBookNumbers(cblBooks, Cycle, true, false, string.Empty);
                    if (cblBooks.Items.Count > 0)
                        divBookNos.Visible = true;
                    else
                        divBookNos.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindMonths();
        }

        private void BindYears()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
                xmlResult = _objBillGenerationBal.GetDetails(ReturnType.Bulk);
                BillGenerationListBe objBillsListBE = _objiDsignBal.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                if (objBillsListBE.Items.Count > 0)
                {
                    _objiDsignBal.FillDropDownList(_objiDsignBal.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlYear, "Year", "Year", "--Select--", true);
                    BindMonths();
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindMonths()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
                _objBillGenerationBe.Year = this.YearId;
                xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.User);
                BillGenerationListBe objBillsListBE = _objiDsignBal.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                if (objBillsListBE.Items.Count > 0)
                {
                    ddlMonth.Enabled = true;
                    _objiDsignBal.FillDropDownList(_objiDsignBal.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlMonth, "MonthName", "Month", "--Select--", true);
                }
                else
                {
                    ddlMonth.SelectedIndex = 0;
                    ddlMonth.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region ExportExcel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            RptNoUsageConsumedBe _objRptNoUsageConsumedBe = new RptNoUsageConsumedBe();
            _objRptNoUsageConsumedBe.BU_ID = BU;
            _objRptNoUsageConsumedBe.SU_ID = SU;
            _objRptNoUsageConsumedBe.SC_ID = SC;
            _objRptNoUsageConsumedBe.CycleId = Cycle;
            _objRptNoUsageConsumedBe.BookNo = BookNo;
            _objRptNoUsageConsumedBe.YearId = YearId;
            _objRptNoUsageConsumedBe.MonthId = MonthId;
            _objRptNoUsageConsumedBe.PageNo = Constants.pageNoValue;
            _objRptNoUsageConsumedBe.PageSize = Convert.ToInt32(hfTotalRecords.Value);

            DataSet ds = new DataSet();
            ds = _objGeneralReportsBal.GetNoUsageConsumedReport(_objRptNoUsageConsumedBe, ReturnType.Get);

            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    var BUlist = (from list in ds.Tables[0].AsEnumerable()
                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                  group list by list.Field<string>("BusinessUnitName") into g
                                  select new { BusinessUnit = g.Key }).ToList();

                    if (BUlist.Count() > 0)
                    {
                        XlsDocument xls = new XlsDocument();
                        Worksheet mainSheet = xls.Workbook.Worksheets.Add("Summary");
                        Cells mainCells = mainSheet.Cells;
                        Cell _objMainCells = null;


                        _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
                        mainCells.Add(4, 2, "Report Code ");
                        mainCells.Add(4, 3, Constants.NoUsageCustomersReport_Code).Font.Bold = true;
                        mainCells.Add(4, 4, "Report Name");
                        mainCells.Add(4, 5, Constants.NoUsageCustomersReport_Name).Font.Bold = true;
                        mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
                        _objMainCells.Font.Weight = FontWeight.ExtraBold;
                        _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                        _objMainCells.Font.FontFamily = FontFamilies.Roman;

                        int MainSheetDetailsRowNo = 6;
                        mainCells.Add(MainSheetDetailsRowNo, 1, "S.No.").Font.Bold = true;
                        mainCells.Add(MainSheetDetailsRowNo, 2, "Service Center").Font.Bold = true;
                        mainCells.Add(MainSheetDetailsRowNo, 3, "Customer Count").Font.Bold = true;

                        var SClist = (from list in ds.Tables[0].AsEnumerable()
                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                      group list by list.Field<string>("ServiceCenterName") into g
                                      select new { ServiceCenter = g.Key }).ToList();

                        for (int k = 0; k < SClist.Count; k++)//SC
                        {
                            var Customers = (from list in ds.Tables[0].AsEnumerable()
                                             where list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                             select list).ToList();

                            MainSheetDetailsRowNo++;
                            mainCells.Add(MainSheetDetailsRowNo, 1, k + 1);
                            mainCells.Add(MainSheetDetailsRowNo, 2, SClist[k].ServiceCenter);
                            mainCells.Add(MainSheetDetailsRowNo, 3, Customers.Count);
                        }

                        for (int i = 0; i < BUlist.Count; i++)//BU
                        {
                            var SUlistByBU = (from list in ds.Tables[0].AsEnumerable()
                                              where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                              orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                              orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                              group list by list.Field<string>("ServiceUnitName") into g
                                              select new { ServiceUnitName = g.Key }).ToList();

                            for (int j = 0; j < SUlistByBU.Count; j++)//SU
                            {
                                var SClistBySU = (from list in ds.Tables[0].AsEnumerable()
                                                  where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                  && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                  group list by list.Field<string>("ServiceCenterName") into g
                                                  select new { ServiceCenter = g.Key }).ToList();

                                for (int k = 0; k < SClistBySU.Count; k++)//SC
                                {

                                    var CyclelistBySC = (from list in ds.Tables[0].AsEnumerable()
                                                         where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                         && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                         && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                         orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                         orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                         group list by list.Field<string>("CycleName") into g
                                                         select new { CycleName = g.Key }).ToList();

                                    if (CyclelistBySC.Count > 0)
                                    {
                                        int DetailsRowNo = 2;
                                        int CustomersRowNo = 6;

                                        Worksheet sheet = xls.Workbook.Worksheets.Add(SClistBySU[k].ServiceCenter);
                                        Cells cells = sheet.Cells;
                                        Cell _objCell = null;

                                        for (int l = 0; l < CyclelistBySC.Count; l++)//Cycle
                                        {
                                            var BooklistByCycle = (from list in ds.Tables[0].AsEnumerable()
                                                                   where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                   && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                   && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                   && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                   orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                   orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                   group list by list.Field<string>("BookNumber") into g
                                                                   select new { BookNumber = g.Key }).ToList();

                                            for (int m = 0; m < BooklistByCycle.Count; m++)//Book
                                            {
                                                var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                                 where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                 && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                 && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                 && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                 && list.Field<string>("BookNumber") == BooklistByCycle[m].BookNumber
                                                                 orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                 orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                 orderby Convert.ToString(list["CycleName"]) ascending
                                                                 select list).ToList();

                                                if (Customers.Count() > 0)
                                                {
                                                    _objCell = cells.Add(DetailsRowNo, 1, "Business Unit");
                                                    cells.Add(DetailsRowNo, 3, "Service Unit");
                                                    cells.Add(DetailsRowNo, 5, "Service Center");
                                                    cells.Add(DetailsRowNo + 1, 1, "Book Group");
                                                    cells.Add(DetailsRowNo + 1, 3, "Book Code");

                                                    cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 4, SUlistByBU[j].ServiceUnitName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 6, SClistBySU[k].ServiceCenter).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 2, CyclelistBySC[l].CycleName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 4, BooklistByCycle[m].BookNumber).Font.Bold = true;


                                                    _objCell = cells.Add(DetailsRowNo + 3, 1, Resource.GRID_SNO);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 2, Resource.ACCOUNT_NO);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 3, Resource.OLD_ACCOUNT_NO);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 4, Resource.NAME);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 5, Resource.SERVICE_ADDRESS);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 6, Resource.METER_NO);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 7, Resource.ADDITIONAL_CHARGES);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 8, Resource.TARIFF);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 9, Resource.BILL_GENERATED_DATE);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 10, Resource.BILL_AMOUNT);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 11, Resource.READCODE);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;

                                                    for (int n = 0; n < Customers.Count; n++)
                                                    {
                                                        DataRow dr = (DataRow)Customers[n];
                                                        cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                                                        cells.Add(CustomersRowNo, 2, dr["GlobalAccountNumber"].ToString());
                                                        cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
                                                        cells.Add(CustomersRowNo, 4, dr["Name"].ToString());
                                                        cells.Add(CustomersRowNo, 5, dr["ServiceAddress"].ToString());
                                                        cells.Add(CustomersRowNo, 6, dr["MeterNo"].ToString());
                                                        cells.Add(CustomersRowNo, 7, dr["AdditionalCharges"].ToString());
                                                        cells.Add(CustomersRowNo, 8, dr["Tariff"].ToString());
                                                        cells.Add(CustomersRowNo, 9, dr["BillGeneratedDate"].ToString());
                                                        cells.Add(CustomersRowNo, 10, dr["TotalBilledAmount"].ToString());
                                                        cells.Add(CustomersRowNo, 11, dr["ReadCode"].ToString());

                                                        CustomersRowNo++;
                                                    }
                                                    DetailsRowNo = CustomersRowNo + 2;
                                                    CustomersRowNo = CustomersRowNo + 6;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                        fileName = Constants.NoUsageCustomersReport_Name.Replace(" ", string.Empty) + "_" + fileName;
                        string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                        xls.FileName = filePath;
                        xls.Save();
                        _objiDsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                    }
                }
                else
                {
                    Message(Resource.NO_READ_CUST, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            else
            {
                Message(Resource.NO_READ_CUST, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}