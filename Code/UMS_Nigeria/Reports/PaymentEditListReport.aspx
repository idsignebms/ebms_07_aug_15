﻿<%@ Page Title="::Payments Report::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="PaymentEditListReport.aspx.cs"
    Inherits="UMS_Nigeria.Reports.PaymentEditListReport" %>

<%@ Register Src="../UserControls/UCPagingV2.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <%--<div class="clear pad_10 fltl">
                    <a style="padding-right: 82px; font-size: 13px; text-decoration: underline;" class="back-btn"
                        id="btnBack" href="EditListReportSummay.aspx"><< Back To Summary</a>
                </div>--%>
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddCycle" runat="server" Text="<%$ Resources:Resource, PAYMENT_REPORT%>"></asp:Literal>
                        </div>
                        <div class="star_text" style="margin-right: 155px;">
                            <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="previous">
                        <a href="EditListReportSummay.aspx">< < Back To Summary</a>
                    </div>
                    <div class="inner-box">
                        <div class="check_baa">
                            <div class="text-inner_a">
                                <asp:Literal ID="ltlSelectUsers" runat="server" Text="User"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" ID="chkAllUsers" class="text-inner-b" Text="All" />
                                    <asp:CheckBoxList ID="cblUsers" class="text-inner-b" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server">
                                    </asp:CheckBoxList>
                                    <span id="spanUsers" class="span_color"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box">
                        <div id="divBusinessUnit" runat="server">
                            <div class="check_baa">
                                <div class="text-inner_a">
                                    <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" ID="ChkBUAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                        <asp:CheckBoxList ID="cblBusinessUnits" class="text-inner-b" AutoPostBack="true"
                                            RepeatDirection="Horizontal" RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblBusinessUnits_SelectedIndexChanged">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div id="divServiceUnits" runat="server" visible="false">
                        <div class="check_baa">
                            <div class="text-inner_a">
                                <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" ID="ChkSUAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                    <asp:CheckBoxList ID="cblServiceUnits" class="text-inner-b" AutoPostBack="true" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceUnits_SelectedIndexChanged">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div id="divServiceCenters" runat="server" visible="false">
                        <div class="check_baa">
                            <div class="text-inner_a">
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" ID="chkSCAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                    <asp:CheckBoxList ID="cblServiceCenters" class="text-inner-b" RepeatDirection="Horizontal"
                                        AutoPostBack="true" RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceCenters_SelectedIndexChanged">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div id="divCycles" runat="server" visible="false">
                        <div class="check_baa">
                            <div class="text-inner_a">
                                <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" class="text-inner-b" ID="chkCycleAll" Text="All" AutoPostBack="true" />
                                    <asp:CheckBoxList ID="cblCycles" class="text-inner-b" AutoPostBack="true" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblCycles_SelectedIndexChanged">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div id="divBookNos" runat="server" visible="false">
                        <div class="check_baa">
                            <div class="text-inner_a">
                                <asp:Literal ID="litBookNo" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal>
                            </div>
                            <div class="check_ba">
                                <div class="text-inner mster-input">
                                    <asp:CheckBox runat="server" class="text-inner-b" ID="chkBooksAll" Text="All" />
                                    <asp:CheckBoxList ID="cblBooks" class="text-inner-b" RepeatDirection="Horizontal"
                                        RepeatColumns="3" runat="server">
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box">
                        <div class="check_baa">
                            <div class="text-inner_a">
                                <asp:Literal ID="ltlReadingFrom" runat="server" Text="<%$ Resources:Resource, TRANSACTION_TYPE%>"></asp:Literal>
                            </div>
                            <div class="check_ba" style="width: 65%;">
                                <asp:CheckBox runat="server" ID="chkTrxTypeAll" class="text-inner-b" Text="All" />
                                <asp:CheckBoxList ID="cblReangsFrom" class="text-inner-b" RepeatDirection="Horizontal"
                                    RepeatColumns="3" runat="server">
                                    <%--<asp:ListItem Text="Entry" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Upload" Value="2"></asp:ListItem>--%>
                                </asp:CheckBoxList>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litFromDate" runat="server" Text="Transaction From Date"></asp:Literal>
                                <span class="span_star">*</span></label>
                            <div class="space">
                            </div>
                            <asp:TextBox ID="txtFromDate" MaxLength="10" runat="server" AutoComplete="off" CssClass="text-box"
                                placeholder="Enter From Date" onblur="TextBoxBlurValidationlbl(this, 'spanFromDate', 'From Date')"
                                onchange="TextBoxBlurValidationlbl(this, 'spanFromDate', 'From Date');"></asp:TextBox>
                            <asp:Image ID="imgFromDate" ImageUrl="~/images/icon_calendar.png" Style="width: 24px;
                                vertical-align: middle" AlternateText="Calender" runat="server" />
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtFromDate"
                                FilterType="Custom,Numbers" ValidChars="/">
                            </asp:FilteredTextBoxExtender>
                            <span id="spanFromDate" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box2">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litToDate" runat="server" Text="Transaction To Date"></asp:Literal>
                                <span class="span_star">*</span></label>
                            <div class="space">
                            </div>
                            <asp:TextBox ID="txtToDate" MaxLength="10" runat="server" AutoComplete="off" CssClass="text-box"
                                placeholder="Enter To Date" onblur="TextBoxBlurValidationlbl(this, 'spanToDate', 'To Date')"
                                onchange="TextBoxBlurValidationlbl(this, 'spanToDate', 'To Date');"></asp:TextBox>
                            <asp:Image ID="imgToDate" ImageUrl="~/images/icon_calendar.png" Style="width: 24px;
                                vertical-align: middle" AlternateText="Calender" runat="server" />
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtToDate"
                                FilterType="Custom,Numbers" ValidChars="/">
                            </asp:FilteredTextBoxExtender>
                            <span id="spanToDate" class="span_color"></span>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="box_total" style="margin-top: 20px;">
                    <div class="box_total_a">
                        <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s"
                            OnClick="btnSearch_Click" OnClientClick="return Validate();" runat="server" />
                    </div>
                </div>
                <div id="divBaseGrid" runat="server">
                    <div class="grid_boxes">
                        <div class="grid_paging_top">
                            <div class="paging_top_title">
                            </div>
                            <div id="divGridContent1" align="center" runat="server" visible="false" style="margin: 5px 0;"
                                class="fltl">
                                <div class="consume_name fltl">
                                    <asp:Label ID="lblTotalUsers" runat="server" Text="<%$ Resources:Resource, TOTAL_CUSTOMERS%>"></asp:Label>
                                </div>
                                <div class="consume_input fltl" style="margin-right: 10px; padding-right: 10px;">
                                    <asp:Label ID="lblTotalUsers1" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                            <div class="paging_top_right_content" id="divPaging1" runat="server">
                                <uc1:UCPagingV1 ID="UCPaging1" runat="server" Visible="false" />
                            </div>
                            <div align="right" class="fltr" runat="server" id="divExport" visible="false">
                                <asp:Button ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" CssClass="exporttoexcelButton"
                                    Text="" />
                            </div>
                        </div>
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvPayments" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                            HeaderStyle-CssClass="grid_tb_hd" HeaderStyle-BackColor="#558D00">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:BoundField HeaderText="<%$ Resources:Resource, GRID_SNO%>" DataField="RowNumber"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, USER%>"
                                    DataField="UserName" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, TRANSACTION_DATE%>"
                                    DataField="TransactionDate" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, TRANSACTION_TYPE%>"
                                    DataField="TransactionFrom" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>"
                                    DataField="GlobalAccountNumber" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"
                                    DataField="OldAccountNo" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, CUSTOMER_NAME%>"
                                    DataField="CustomerName" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Meter Number" DataField="MeterNumber"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, SERVICE_ADDRESS%>"
                                    DataField="ServiceAdress" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, CYCLE%>"
                                    DataField="BookGroup" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Right" HeaderText="<%$ Resources:Resource, PAID_DATE%>"
                                    DataField="PaidDate" ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Receipt Number" DataField="ReceiptNo"
                                    ItemStyle-CssClass="grid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Right" HeaderText="<%$ Resources:Resource, AMOUNT%>"
                                    DataField="Amount" ItemStyle-CssClass="grid_tb_td" />
                            </Columns>
                            <RowStyle HorizontalAlign="center"></RowStyle>
                        </asp:GridView>
                        <asp:HiddenField ID="hfPageSize" runat="server" />
                        <asp:HiddenField ID="hfPageNo" runat="server" />
                        <asp:HiddenField ID="hfLastPage" runat="server" />
                        <asp:HiddenField ID="hfTotalRecords" runat="server" />
                    </div>
                    <div class="grid_boxes">
                        <div id="divPaging2" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" Visible="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="divBatchPaymentsGrid" runat="server" style="margin-bottom: 20px;">
                <div class="grid_tb" id="div1" runat="server">
                    <asp:GridView ID="gvBatchPayments" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                        HeaderStyle-CssClass="grid_tb_hd" HeaderStyle-BackColor="#558D00">
                        <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                        <EmptyDataTemplate>
                            There is no data.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Batch Number" DataField="BatchNo"
                                ItemStyle-CssClass="grid_tb_td" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Batch Date" DataField="PaidDate"
                                ItemStyle-CssClass="grid_tb_td" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Right" HeaderText="Batch Amount" DataField="PaidAmount"
                                ItemStyle-CssClass="grid_tb_td" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Right" HeaderText="Total Customers" DataField="TotalRecords"
                                ItemStyle-CssClass="grid_tb_td" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Right" HeaderText="Total Amount Received"
                                DataField="PaidAmount" ItemStyle-CssClass="grid_tb_td" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="User Name" DataField="UserName"
                                ItemStyle-CssClass="grid_tb_td" />
                            <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Transaction Date" DataField="TransactionDate"
                                ItemStyle-CssClass="grid_tb_td" />
                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblBatchNo" runat="server" Style="display: none;" CssClass="BatchDummy"
                                        Text='<%#Eval("BatchID") %>'></asp:Label>
                                    <asp:LinkButton ID="lbtnView" runat="server" Text="View Report" OnClientClick="ViewReport(this);" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle HorizontalAlign="center"></RowStyle>
                    </asp:GridView>
                    <div class="clr">
                    </div>
                </div>
                <div class="clr">
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="PanelAlert"
        TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btnAlertOk">
    </asp:ModalPopupExtender>
    <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
    <asp:Panel ID="PanelAlert" runat="server" CssClass="modalPopup" Style="display: none;">
        <div class="popheader popheaderlblred">
            <asp:Label ID="lblAlertMsg" runat="server"></asp:Label>
        </div>
        <div class="footer popfooterbtnrt">
            <div class="fltr popfooterbtn">
                <asp:Button ID="btnAlertOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="ok_btn" />
            </div>
            <div class="clear pad_5">
            </div>
        </div>
    </asp:Panel>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/MeterReadingReport.js" type="text/javascript"></script>
    <script type="text/javascript">
        function pageLoad() {
            Calendar('<%=txtFromDate.ClientID %>', '<%=imgFromDate.ClientID %>', "-2:+8");
            Calendar('<%=txtToDate.ClientID %>', '<%=imgToDate.ClientID %>', "-2:+8");
        };       
    </script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }
        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }   
    </script>
    <%--==============Validation script ref ends==============--%>
    <script type="text/javascript">
        function ViewReport(obj) {
            var BatchNo = obj.parentElement.getElementsByClassName('BatchDummy')[0].innerHTML;
            window.location = "BatchwisePaymentsList.aspx?bno=" + BatchNo;
        }
    </script>
</asp:Content>
