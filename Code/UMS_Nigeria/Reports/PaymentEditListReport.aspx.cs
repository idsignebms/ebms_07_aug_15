﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for Meter Reading Report
                     
 Developer        : Id044-Karteek.P
 Creation Date    : 25-Mar-2015
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Configuration;
using org.in2bits.MyXls;

namespace UMS_Nigeria.Reports
{
    public partial class PaymentEditListReport : System.Web.UI.Page
    {
        #region Members

        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        ReportsBal _objReportsBal = new ReportsBal();

        public int PageNum;
        public int DefaultPageSize = 500;
        string Key = UMS_Resource.PAYMENT_REPORT;

        #endregion

        #region Properties

        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStartsReport : Convert.ToInt32(hfPageSize.Value); }
        }
        public string Users
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblUsers, ","); }
        }
        public string FromDate
        {
            get { return string.IsNullOrEmpty(txtFromDate.Text) ? string.Empty : _objCommonMethods.Convert_MMDDYY(txtFromDate.Text); }
        }
        public string ToDate
        {
            get { return string.IsNullOrEmpty(txtToDate.Text) ? string.Empty : _objCommonMethods.Convert_MMDDYY(txtToDate.Text); }
        }
        public string TransactionFrom
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblReangsFrom, ","); }
        }

        public string BusinessUnitName
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblBusinessUnits, ","); }
        }
        public string ServiceUnitName
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceUnits, ","); }
        }
        public string ServiceCenterName
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceCenters, ","); }
        }

        public string Cycle
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblCycles, ","); }
        }
        public string BookNo
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblBooks, ","); }
        }

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                if (!IsPostBack)
                {
                    //string path = string.Empty;
                    //path = _objCommonMethods.GetPagePath(this.Request.Url);
                    //if (_objCommonMethods.IsAccess(path))
                    //{
                    BindUsers();
                    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    {
                        ListItem LiBU = new ListItem(Session[UMS_Resource.SESSION_USER_BUNAME].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());
                        cblBusinessUnits.Items.Add(LiBU);
                        cblBusinessUnits.Items[0].Selected = true;
                        cblBusinessUnits.Enabled = false;
                        ChkBUAll.Visible = false;
                        divServiceUnits.Visible = true;
                        _objCommonMethods.BindUserServiceUnits_BuIds(cblServiceUnits, BusinessUnitName, Resource.DDL_ALL, false);
                    }
                    else
                        _objCommonMethods.BindBusinessUnits(cblBusinessUnits, string.Empty, false);
                    BindPaymentTypes();
                    CheckBoxesJS();
                    //hfPageSize.Value = Constants.PageSizeStarts.ToString();
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    hfTotalRecords.Value = Constants.Zero.ToString();
                    //BindGrid();
                    //}
                    //else
                    //{
                    //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    //}
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindGrid();
            UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            UCPaging2.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        }

        #endregion

        #region ddl AND cbl Events
        protected void ddlBusinessUnitName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    _objCommonMethods.BindUserServiceUnits_BuIds(cblServiceUnits, BusinessUnitName, Resource.DDL_ALL, false);
                    if (cblServiceUnits.Items.Count > 0)
                        divServiceUnits.Visible = true;
                    else
                        divServiceUnits.Visible = false;
                }
                catch (Exception ex)
                {
                    Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                    try
                    {
                        _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                    }
                    catch (Exception logex)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblBusinessUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                cblBooks.Items.Clear();
                cblServiceCenters.Items.Clear();
                cblServiceUnits.Items.Clear();
                chkCycleAll.Checked = chkBooksAll.Checked = chkSCAll.Checked = ChkSUAll.Checked = ChkBUAll.Checked = false;

                divServiceUnits.Visible = divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = chkSCAll.Checked = divServiceCenters.Visible = false;
                if (!string.IsNullOrEmpty(BusinessUnitName))
                {
                    _objCommonMethods.BindUserServiceUnits_BuIds(cblServiceUnits, BusinessUnitName, Resource.DDL_ALL, false);
                    if (cblServiceUnits.Items.Count > 0)
                        divServiceUnits.Visible = true;
                    else
                        divServiceUnits.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                cblBooks.Items.Clear();
                cblServiceCenters.Items.Clear();
                divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = chkSCAll.Checked = divServiceCenters.Visible = false;
                if (!string.IsNullOrEmpty(ServiceUnitName))
                {
                    _objCommonMethods.BindUserServiceCenters_SuIds(cblServiceCenters, ServiceUnitName, Resource.DDL_ALL, false);
                    if (cblServiceCenters.Items.Count > 0)
                        divServiceCenters.Visible = true;
                    else
                        divServiceCenters.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceCenters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                cblBooks.Items.Clear();
                divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = false;
                if (!string.IsNullOrEmpty(ServiceCenterName))
                {
                    _objCommonMethods.BindCyclesByBUSUSC(cblCycles, BusinessUnitName, ServiceUnitName, ServiceCenterName, true, string.Empty, false);
                    if (cblCycles.Items.Count > 0)
                        divCycles.Visible = true;
                    else
                        divCycles.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblCycles_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblBooks.Items.Clear();
                chkBooksAll.Checked = divBookNos.Visible = false;
                if (!string.IsNullOrEmpty(Cycle))
                {
                    _objCommonMethods.BindAllBookNumbers(cblBooks, Cycle, true, false, string.Empty);
                    if (cblBooks.Items.Count > 0)
                        divBookNos.Visible = true;
                    else
                        divBookNos.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        private void BindUsers()
        {
            _objCommonMethods.BindUsersForPayments(cblUsers, false);
        }

        private void BindPaymentTypes()
        {
            _objCommonMethods.BindPaymentTypes(cblReangsFrom, false);
        }

        private void CheckBoxesJS()
        {
            chkAllUsers.Attributes.Add("onclick", "CheckAll('" + chkAllUsers.ClientID + "','" + cblUsers.ClientID + "')");
            chkTrxTypeAll.Attributes.Add("onclick", "CheckAll('" + chkTrxTypeAll.ClientID + "','" + cblReangsFrom.ClientID + "')");

            cblUsers.Attributes.Add("onclick", "UnCheckAll('" + chkAllUsers.ClientID + "','" + cblUsers.ClientID + "')");
            cblReangsFrom.Attributes.Add("onclick", "UnCheckAll('" + chkTrxTypeAll.ClientID + "','" + cblReangsFrom.ClientID + "')");

            ChkBUAll.Attributes.Add("onclick", "CheckAll('" + ChkBUAll.ClientID + "','" + cblBusinessUnits.ClientID + "')");
            ChkSUAll.Attributes.Add("onclick", "CheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            chkSCAll.Attributes.Add("onclick", "CheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            chkCycleAll.Attributes.Add("onclick", "CheckAll('" + chkCycleAll.ClientID + "','" + cblCycles.ClientID + "')");
            chkBooksAll.Attributes.Add("onclick", "CheckAll('" + chkBooksAll.ClientID + "','" + cblBooks.ClientID + "')");

            cblBusinessUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkBUAll.ClientID + "','" + cblBusinessUnits.ClientID + "')");
            cblServiceUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            cblServiceCenters.Attributes.Add("onclick", "UnCheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            cblCycles.Attributes.Add("onclick", "UnCheckAll('" + chkCycleAll.ClientID + "','" + cblCycles.ClientID + "')");
            cblBooks.Attributes.Add("onclick", "UnCheckAll('" + chkBooksAll.ClientID + "','" + cblBooks.ClientID + "')");
        }

        public void BindGrid()
        {
            RptPaymentsEditListBE _objRptPaymentsEditListBE = new RptPaymentsEditListBE();
            _objRptPaymentsEditListBE.FromDate = this.FromDate;
            _objRptPaymentsEditListBE.ToDate = this.ToDate;
            _objRptPaymentsEditListBE.UserName = this.Users;
            _objRptPaymentsEditListBE.TransactionFrom = this.TransactionFrom;
            _objRptPaymentsEditListBE.PageNo = Convert.ToInt32(hfPageNo.Value);
            _objRptPaymentsEditListBE.PageSize = PageSize;
            _objRptPaymentsEditListBE.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
            _objRptPaymentsEditListBE.SU_ID = this.ServiceUnitName;
            _objRptPaymentsEditListBE.SC_ID = this.ServiceCenterName;
            _objRptPaymentsEditListBE.CycleId = this.Cycle;
            _objRptPaymentsEditListBE.BookNo = this.BookNo;

            bool isBatchPayment = false;

            if (!string.IsNullOrEmpty(this.TransactionFrom))
            {
                if (this.TransactionFrom.Split(',').Length == 1 && (Convert.ToInt32(this.TransactionFrom) == (int)PaymentType.BatchPayment || Convert.ToInt32(this.TransactionFrom) == (int)PaymentType.BulkUpload))
                {
                    isBatchPayment = true;
                }
            }

            if (isBatchPayment)
            {
                divBaseGrid.Visible = false;
                divBatchPaymentsGrid.Visible = true;

                RptPaymentsEditListListBe _objRptPaymentsEditListListBe = _objiDsignBal.DeserializeFromXml<RptPaymentsEditListListBe>(_objReportsBal.Get(_objRptPaymentsEditListBE, ReturnType.Multiple));

                if (_objRptPaymentsEditListListBe.items.Count > 0)
                {
                    gvBatchPayments.DataSource = _objRptPaymentsEditListListBe.items;
                    gvBatchPayments.DataBind();
                }
                else
                {
                    gvBatchPayments.DataSource = new DataTable();
                    gvBatchPayments.DataBind();
                }
            }
            else
            {
                divBaseGrid.Visible = true;
                divBatchPaymentsGrid.Visible = false;

                RptPaymentsEditListListBe _objRptPaymentsEditListListBe = _objiDsignBal.DeserializeFromXml<RptPaymentsEditListListBe>(_objReportsBal.Get(_objRptPaymentsEditListBE, ReturnType.Data));

                if (_objRptPaymentsEditListListBe.items.Count > 0)
                {
                    gvPayments.DataSource = _objRptPaymentsEditListListBe.items;
                    gvPayments.DataBind();
                    divgrid.Visible = true;
                    UCPaging1.Visible = UCPaging2.Visible = true;
                    lblTotalUsers1.Text = hfTotalRecords.Value = _objRptPaymentsEditListListBe.items[0].TotalRecords.ToString();
                    divExport.Visible = divGridContent1.Visible = lblTotalUsers.Visible = lblTotalUsers1.Visible = true;
                }
                else
                {
                    UCPaging1.Visible = UCPaging2.Visible = false;
                    divExport.Visible = divGridContent1.Visible = lblTotalUsers1.Visible = false;
                    hfTotalRecords.Value = Constants.Zero.ToString();
                    gvPayments.DataSource = new DataTable();
                    gvPayments.DataBind();
                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

        #region ExportExcel

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                RptPaymentsEditListBE _objRptPaymentsEditListBE = new RptPaymentsEditListBE();
                _objRptPaymentsEditListBE.FromDate = this.FromDate;
                _objRptPaymentsEditListBE.ToDate = this.ToDate;
                _objRptPaymentsEditListBE.UserName = this.Users;
                _objRptPaymentsEditListBE.TransactionFrom = this.TransactionFrom;
                _objRptPaymentsEditListBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                _objRptPaymentsEditListBE.PageSize = Convert.ToInt32(hfTotalRecords.Value);
                _objRptPaymentsEditListBE.BU_ID = (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString();
                _objRptPaymentsEditListBE.SU_ID = this.ServiceUnitName;
                _objRptPaymentsEditListBE.SC_ID = this.ServiceCenterName;
                _objRptPaymentsEditListBE.CycleId = this.Cycle;
                _objRptPaymentsEditListBE.BookNo = this.BookNo;

                RptPaymentsEditListListBe _objRptPaymentsEditListListBe = _objiDsignBal.DeserializeFromXml<RptPaymentsEditListListBe>(_objReportsBal.Get(_objRptPaymentsEditListBE, ReturnType.Data));
                DataSet ds = new DataSet();
                ds = _objiDsignBal.ConvertListToDataSet<RptPaymentsEditListBE>(_objRptPaymentsEditListListBe.items);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    XlsDocument xls = new XlsDocument();
                    Worksheet sheet = xls.Workbook.Worksheets.Add("Summary");
                    Cells cells = sheet.Cells;
                    Cell _objCell = null;

                    _objCell = cells.Add(2, 3, Resource.BEDC + " - Payments");
                    sheet.AddMergeArea(new MergeArea(2, 2, 3, 6));
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;

                    int DetailsRowNo = 5;
                    int CustomersRowNo = 6;

                    _objCell = cells.Add(DetailsRowNo, 1, "S.No");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 2, "User Name");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 3, "Transaction Date");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 4, "Transaction From");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 5, "Global Account No");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 6, "Old Account No");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 7, "Customer Name");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 8, "Meter Number");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 9, "Service Address");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 10, "Book Group");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 11, "Paid Date");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 12, "Receipt Number");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 13, "Amount");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;

                    for (int n = 0; n < ds.Tables[0].Rows.Count; n++)
                    {
                        DataRow dr = (DataRow)ds.Tables[0].Rows[n];
                        cells.Add(CustomersRowNo, 1, dr["RowNumber"].ToString());
                        cells.Add(CustomersRowNo, 2, dr["UserName"].ToString());
                        cells.Add(CustomersRowNo, 3, dr["TransactionDate"].ToString());
                        cells.Add(CustomersRowNo, 4, dr["TransactionFrom"].ToString());
                        cells.Add(CustomersRowNo, 5, dr["GlobalAccountNumber"].ToString());
                        cells.Add(CustomersRowNo, 6, dr["OldAccountNo"].ToString());
                        cells.Add(CustomersRowNo, 7, dr["CustomerName"].ToString());
                        cells.Add(CustomersRowNo, 8, dr["MeterNumber"].ToString());
                        cells.Add(CustomersRowNo, 9, dr["ServiceAdress"].ToString());
                        cells.Add(CustomersRowNo, 10, dr["BookGroup"].ToString());
                        cells.Add(CustomersRowNo, 11, dr["PaidDate"].ToString());
                        cells.Add(CustomersRowNo, 12, dr["ReceiptNo"].ToString());
                        cells.Add(CustomersRowNo, 13, dr["Amount"].ToString());

                        CustomersRowNo++;
                    }

                    string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                    fileName = "PaymentsRerpot_" + fileName;
                    string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                    xls.FileName = filePath;
                    xls.Save();
                    _objiDsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                }
                else
                    Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}