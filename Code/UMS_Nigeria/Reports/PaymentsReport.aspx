﻿<%@ Page Title=":: Payments Received Report ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="PaymentsReport.aspx.cs" Inherits="UMS_Nigeria.Reports.PaymentsReport" %>

<%@ Register Src="../UserControls/UCPagingV2.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdateProgress ID="UpdateProgress" runat="server">
                <ProgressTemplate>
                    <center>
                        <div id="loading-div" class="pbloading">
                            <div id="title-loading" class="pbtitle">
                                BEDC</div>
                            <center>
                                <img src="../images/loading.GIF" class="pbimg"></center>
                            <p class="pbtxt">
                                Loading Please wait.....</p>
                        </div>
                    </center>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
                PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
            <asp:UpdatePanel ID="upBatchEntry" runat="server" ChildrenAsTriggers="true">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddState" runat="server" Text="<%$ Resources:Resource, PAYMENTS_REPORT%>"></asp:Literal>
                        </div>
                        <div class="star_text" style="margin-right: 155px;">
                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="previous">
                        <a href="ReportsSummary.aspx">< < Back To Summary</a>
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, FILTER_TYPE%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </label>
                                <div class="space">
                                </div>
                                <asp:DropDownList ID="ddlFilterType" CssClass="text-box select_box" runat="server"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlFilterType_SelectedIndexChanged">
                                    <asp:ListItem>--Select--</asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                <span id="spanFilterType" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource,  RECEIVED_DEVICE%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:DropDownList ID="ddlDevice" CssClass="text-box select_box" runat="server">
                                    <asp:ListItem>ALL</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="divFilterType" visible="false" runat="server">
                            <div class="inner-box">
                                <div class="check_baa">
                                    <div class="text-inner_a">
                                        <asp:Literal ID="litFilterType" runat="server" Text=""></asp:Literal><span class="span_star">*</span>
                                    </div>
                                    <div class="check_ba">
                                        <div class="text-inner mster-input">
                                            <asp:CheckBox runat="server" ID="cbAll" class="text-inner-b" Text="All" onclick="CheckAll();" />
                                            <asp:CheckBoxList ID="cbl" class="text-inner-b" RepeatDirection="Horizontal" onclick="UnCheckAll();"
                                                RepeatColumns="3" runat="server">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource,  FROM_DATE%>"></asp:Literal>
                                    </label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox ID="txtFromDate" AutoComplete="off" placeholder="Enter From Date" CssClass="text-box"
                                        MaxLength="10" Width="169px" ToolTip="Enter From Date" runat="server" onchange="TextBoxBlurValidationlbl(this, 'spanFromDate', 'From Date')"></asp:TextBox>
                                    <asp:Image ID="imgFromDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                        vertical-align: middle" AlternateText="Calender" runat="server" />
                                    <asp:FilteredTextBoxExtender ID="ftbFromDate" runat="server" TargetControlID="txtFromDate"
                                        FilterType="Custom,Numbers" ValidChars="/">
                                    </asp:FilteredTextBoxExtender>
                                    <span id="spanFromDate" class="span_color"></span>
                                </div>
                            </div>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource,  TO_DATE%>"></asp:Literal>
                                    </label>
                                    <div class="space">
                                    </div>
                                    <asp:TextBox ID="txtToDate" MaxLength="10" AutoComplete="off" placeholder="Enter To Date"
                                        CssClass="text-box" Width="169px" ToolTip="Enter To Date" runat="server" onchange="TextBoxBlurValidationlbl(this, 'spanToDate', 'To Date')"></asp:TextBox>
                                    <asp:Image ID="imgToDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                                        vertical-align: middle" AlternateText="Calender" runat="server" />
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtToDate"
                                        FilterType="Custom,Numbers" ValidChars="/">
                                    </asp:FilteredTextBoxExtender>
                                    <span id="spanToDate" class="span_color"></span>
                                </div>
                            </div>
                        </div>
                        <div class="box_total">
                            <div class="box_total_a">
                                <asp:Button ID="btnGo" CssClass="box_s" OnClientClick="return Validate()" Text="<%$ Resources:Resource, GO%>"
                                    runat="server" OnClick="btnGo_Click" />
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="grid_boxes" id="divrptrdetails" runat="server" visible="false">
                            <div class="grid_paging_top">
                                <div class="paging_top_title" style="position: relative; top: 24px;">
                                    <asp:Literal ID="litGridStatesList" runat="server" Text="<%$ Resources:Resource, PAYMENTS_REPORT_LIST%>"></asp:Literal>
                                </div>
                                <div class="clear">
                                </div>
                                <div id="divToprptrContent" visible="false" runat="server">
                                    <div class="consume_name fltl" style="position: relative; top: 30px;">
                                        <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, OUT_STANDING_AMT%>"></asp:Label> &nbsp : &nbsp
                                    </div>
                                    <div class="consume_input fltl" style="margin-right: 10px; border-right: 1px solid #000;
                                        padding-right: 10px; position: relative; top: 30px;">
                                        <asp:Label ID="lblTopTotalDue" runat="server" Text="--"></asp:Label>
                                    </div>
                                    <div class="consume_input fltl" style="position: relative; top: 30px;">
                                        <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, AMT_RECEIVED%>"></asp:Label> &nbsp : &nbsp
                                    </div>
                                    <div class="consume_input fltl" style="margin-right: 10px; padding-right: 10px; position: relative;
                                        top: 30px;">
                                        <asp:Label ID="lblTopTotReceivedAmt" runat="server" Text="--"></asp:Label>
                                    </div>
                                    <div class="clear">
                                    </div>
                                    <div class="paging_top_right_content">
                                        <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                                    </div>
                                </div>
                                <div align="right" runat="server" visible="false" id="divExport">
                                    <asp:Button ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" CssClass="exporttoexcelButton" />
                                </div>
                            </div>
                            <div class="clr pad_10">
                            </div>
                            <div class="rptr_tb">
                                <table style="border-collapse: collapse; border: 1px #000 solid;">
                                    <tr class="rptr_tb_hd" id="rptrheaders" runat="server" visible="false" align="center">
                                        <th>
                                            <asp:Label ID="lblHSno" runat="server" Text="<%$ Resources:Resource, GRID_SNO%>"></asp:Label>
                                        </th>
                                        <th>
                                            <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Label>
                                        </th>
                                        <th>
                                            <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Label>
                                        </th>
                                        <th>
                                            <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Label>
                                        </th>
                                        <th>
                                            <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Label>
                                        </th>
                                        <th>
                                            <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, BU%>"></asp:Label>
                                        </th>
                                        <th>
                                            <asp:Label ID="Label10" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Label>
                                        </th>
                                        <th>
                                            <asp:Label ID="Label18" runat="server" Text="<%$ Resources:Resource, BATCH_NO%>"></asp:Label>
                                        </th>
                                        <th>
                                            <asp:Label ID="Label11" runat="server" Text="<%$ Resources:Resource, AMT_RECEIVED%>"></asp:Label>
                                        </th>
                                        <th>
                                            <asp:Label ID="Label12" runat="server" Text="<%$ Resources:Resource, OUT_STANDING_AMT%>"></asp:Label>
                                        </th>
                                        <th>
                                            <asp:Label ID="Label13" runat="server" Text="<%$ Resources:Resource, AMT_RECEIVED_DATE%>"></asp:Label>
                                        </th>
                                        <th>
                                            <asp:Label ID="Label16" runat="server" Text="<%$ Resources:Resource, RECEIVED_DEVICE%>"></asp:Label>
                                        </th>
                                    </tr>
                                    <asp:Repeater ID="rptrCreditBalance" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="rptr_tb_td" align="center">
                                                    <asp:Label runat="server" ID="lblsno" Text='<%#Eval("RowNumber") %>'></asp:Label>
                                                </td>
                                                <td class="rptr_tb_td" align="center">
                                                    <asp:Label runat="server" ID="lblAccNo" Text='<%#Eval("AccountNo") %>'></asp:Label>
                                                </td>
                                                <td class="rptr_tb_td" align="left">
                                                    <asp:Label runat="server" ID="lblOldAccountNo" Text='<%#Eval("OldAccountNo") %>'></asp:Label>
                                                </td>
                                                <td class="rptr_tb_td" align="left">
                                                    <asp:Label runat="server" ID="lblName" Text='<%#Eval("Name") %>'></asp:Label>
                                                </td>
                                                <td class="rptr_tb_td" align="left">
                                                    <asp:Label runat="server" ID="lblAddress" Text='<%#Eval("ServiceAddress") %>'></asp:Label>
                                                </td>
                                                <td class="rptr_tb_td" align="right">
                                                    <asp:Label runat="server" ID="lblOutStanding" Text='<%#Eval("BusinessUnitName") %>'></asp:Label>
                                                </td>
                                                <td class="rptr_tb_td" align="left">
                                                    <asp:Label ID="lblLastPaidAmount" runat="server" Text='<%#Eval("CycleName")%>'></asp:Label>
                                                </td>
                                                <td class="rptr_tb_td" align="left">
                                                    <asp:Label ID="Label19" runat="server" Text='<%#Eval("BatchNo")%>'></asp:Label>
                                                </td>
                                                <td class="rptr_tb_td" align="right">
                                                    <asp:Label runat="server" ID="lblPaidAmount" Text='<%#Eval("PaidAmount") %>'></asp:Label>
                                                </td>
                                                <td class="rptr_tb_td" align="right">
                                                    <asp:Label runat="server" ID="Label14" Text='<%#Eval("TotalDueAmount") %>'></asp:Label>
                                                </td>
                                                <td class="rptr_tb_td" align="center">
                                                    <asp:Label runat="server" ID="Label15" Text='<%#Eval("ReceivedDate") %>'></asp:Label>
                                                </td>
                                                <td class="rptr_tb_td" align="center">
                                                    <asp:Label runat="server" ID="Label17" Text='<%#Eval("ReceivedDevice") %>'></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <tr align="center" runat="server" id="divNoDataFound" visible="false">
                                        <td colspan="11">
                                            No data found
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <asp:HiddenField ID="hfPageSize" runat="server" />
                            <asp:HiddenField ID="hfPageNo" runat="server" />
                            <asp:HiddenField ID="hfLastPage" runat="server" />
                            <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                        </div>
                        <div id="divDownrptrContent" class="reports_txt" runat="server" visible="false">
                            <div class="consume_name fltl">
                                <div class="consume_name fltl">
                                    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, OUT_STANDING_AMT%>"></asp:Label> &nbsp : &nbsp
                                </div>
                                <div class="consume_input fltl" style="margin-right: 10px; border-right: 1px solid #000;
                                    padding-right: 10px;">
                                    <asp:Label ID="lblDownTotalDue" runat="server" Text="--"></asp:Label>
                                </div>
                                <div class="consume_input fltl">
                                    <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, AMT_RECEIVED%>"></asp:Label> &nbsp : &nbsp
                                </div>
                                <div class="consume_input fltl" style="margin-right: 10px; padding-right: 10px;">
                                    <asp:Label ID="lblDownTotReceivedAmt" runat="server" Text="--"></asp:Label>
                                </div>
                            </div>
                            <div id="divDownPaging" runat="server" class="fltr">
                                <div class="grid_paging_bottom">
                                    <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                                </div>
                            </div>
                        </div>
                        <%-- Grid Alert popup Start--%>
                        <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                            TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                            <div class="popheader popheaderlblred">
                                <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                            </div>
                            <div class="footer popfooterbtnrt">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Grid Alert popup Closed--%>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExportExcel" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            //Calendar('<%=txtFromDate.ClientID %>', '<%=imgFromDate.ClientID %>', "-2:+8");
            DatePicker($('#<%=txtFromDate.ClientID %>'), $('#<%=imgFromDate.ClientID %>'));
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            //Calendar('<%=txtFromDate.ClientID %>', '<%=imgFromDate.ClientID %>', "-2:+8");
            DatePicker($('#<%=txtFromDate.ClientID %>'), $('#<%=imgFromDate.ClientID %>'));
        });

        $(document).ready(function () {
            //Calendar('<%=txtToDate.ClientID %>', '<%=imgToDate.ClientID %>', "-2:+8");
            DatePicker($('#<%=txtToDate.ClientID %>'), $('#<%=imgToDate.ClientID %>'));
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            //Calendar('<%=txtToDate.ClientID %>', '<%=imgToDate.ClientID %>', "-2:+8");
            DatePicker($('#<%=txtToDate.ClientID %>'), $('#<%=imgToDate.ClientID %>'));
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.ui.core.js" type="text/javascript"></script>
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/PaymentsReport.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
