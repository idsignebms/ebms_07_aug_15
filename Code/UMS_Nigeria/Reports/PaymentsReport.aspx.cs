﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for PaymentsReport.aspx
                     
 Developer        : id065-Bhimaraju V
 Creation Date    : 31-July-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using Resources;
using iDSignHelper;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Globalization;
using System.Configuration;
using System.Xml;
using org.in2bits.MyXls;

namespace UMS_Nigeria.Reports
{
    public partial class PaymentsReport : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        GeneralReportsBal _objGeneralReportsBal = new GeneralReportsBal();
        CommonMethods _objCommonMethods = new CommonMethods();
        string Key = UMS_Resource.KEY_RPT_PAYMENTS;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStartsReport : Convert.ToInt32(hfPageSize.Value); }
        }
        public string Device
        {
            get { return string.IsNullOrEmpty(ddlDevice.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlDevice, ",") : ddlDevice.SelectedValue; }
            set { ddlDevice.SelectedValue = value.ToString(); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = _objCommonMethods.GetPagePath(this.Request.Url);
                //if (_objCommonMethods.IsAccess(path))
                //{
                BindFilter();
                _objCommonMethods.BindBillingTypes(ddlDevice, true);
                hfPageNo.Value = Constants.pageNoValue.ToString();
                //}
                //else { Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }

                hfPageNo.Value = Constants.pageNoValue.ToString();
                hfTotalRecords.Value = Constants.Zero.ToString();
            }

        }

        protected void ddlFilterType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlFilterType.SelectedIndex > 0)
                {
                    cbAll.Checked = false;
                    cbl.Items.Clear();
                    switch (Convert.ToInt32(ddlFilterType.SelectedValue))
                    {
                        case (int)EnumFilterTypes.BusinessUnits:
                            litFilterType.Text = Resource.BUSINESS_UNIT;
                            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                            {
                                ListItem LiBU = new ListItem(Session[UMS_Resource.SESSION_USER_BUNAME].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());
                                cbl.Items.Add(LiBU);
                                cbl.Items[0].Selected = true;
                                cbAll.Visible = cbl.Items[0].Enabled = false;
                            }
                            else
                                _objCommonMethods.BindBusinessUnits(cbl, string.Empty, false);
                            divFilterType.Visible = true;
                            break;
                        case (int)EnumFilterTypes.Cycles:
                            litFilterType.Text = Resource.CYCLE;
                            cbAll.Visible = true;
                            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                                _objCommonMethods.BindCyclesByBUSUSC(cbl, (Session[UMS_Resource.SESSION_USER_BUID] == null) ? string.Empty : Session[UMS_Resource.SESSION_USER_BUID].ToString(), string.Empty, string.Empty, false, string.Empty, false);
                            else
                                _objCommonMethods.BindCyclesList(cbl, false);
                            divFilterType.Visible = true;
                            break;
                        default:
                            divFilterType.Visible = false;
                            break;
                    }
                }
                else
                {
                    divFilterType.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            BindGrid();
            UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            UCPaging2.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        }

        #endregion

        #region Methods
        public void BindGrid()
        {
            RptPaymentsBe _objRptPaymentsBe = new RptPaymentsBe();

            if (ddlFilterType.SelectedIndex == 1)
            {
                _objRptPaymentsBe.BU_ID = _objCommonMethods.CollectSelectedItemsValues(cbl, ",");
                _objRptPaymentsBe.CycleId = "";

            }
            else if (ddlFilterType.SelectedIndex == 2)
            {
                _objRptPaymentsBe.CycleId = _objCommonMethods.CollectSelectedItemsValues(cbl, ",");
                _objRptPaymentsBe.BU_ID = "";
            }
            if (txtFromDate.Text != "")
                _objRptPaymentsBe.FromDate = _objCommonMethods.Convert_MMDDYY(txtFromDate.Text);
            else
                _objRptPaymentsBe.FromDate = "";
            if (txtToDate.Text != "")
                _objRptPaymentsBe.ToDate = _objCommonMethods.Convert_MMDDYY(txtToDate.Text);
            else
                _objRptPaymentsBe.ToDate = "";
            _objRptPaymentsBe.PageNo = Convert.ToInt32(hfPageNo.Value);
            _objRptPaymentsBe.PageSize = PageSize;
            _objRptPaymentsBe.ReceivedDeviceId = Device;

            DataSet dsPayments = new DataSet();
            dsPayments = _objGeneralReportsBal.GetPaymentsReport(_objRptPaymentsBe, ReturnType.Get);

            if (dsPayments != null && dsPayments.Tables.Count > 0 && dsPayments.Tables[0].Rows.Count > 0)
            {
                var totalPaidAmount = (from x in dsPayments.Tables[0].AsEnumerable() select x.Field<decimal>("PaidAmount")).Sum();
                var DueAmount = (from x in dsPayments.Tables[0].AsEnumerable()
                                 select new
                                     {
                                         AccountNo = x.Field<string>("AccountNo"),
                                         TotalDueAmount = x.Field<decimal>("TotalDueAmount")
                                     }).Distinct();
                var totalDueAmount = (from x in DueAmount.AsEnumerable() select x.TotalDueAmount).Sum();

                divToprptrContent.Visible = divExport.Visible = divDownrptrContent.Visible = rptrheaders.Visible = divrptrdetails.Visible = true;
                divNoDataFound.Visible = false;
                rptrCreditBalance.DataSource = dsPayments.Tables[0];
                rptrCreditBalance.DataBind();
                //MergeRows(rptrCreditBalance);
                hfTotalRecords.Value = dsPayments.Tables[0].Rows[0]["TotalRecords"].ToString();
                lblTopTotalDue.Text = lblDownTotalDue.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(totalDueAmount), 2, Constants.MILLION_Format);
                lblTopTotReceivedAmt.Text = lblDownTotReceivedAmt.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(totalPaidAmount), 2, Constants.MILLION_Format);
            }
            else
            {
                divToprptrContent.Visible = divExport.Visible = divDownrptrContent.Visible = rptrheaders.Visible = false;
                divNoDataFound.Visible = divrptrdetails.Visible = true;
                rptrCreditBalance.DataSource = new DataTable(); ;
                rptrCreditBalance.DataBind();
                lblTopTotalDue.Text = lblDownTotalDue.Text = lblTopTotReceivedAmt.Text = lblDownTotReceivedAmt.Text = hfTotalRecords.Value = Constants.Zero.ToString();
            }
        }

        //public static void MergeRows(Repeater gridView)
        //{
        //    for (int rowIndex = gridView.Rows.Count - 2; rowIndex >= 0; rowIndex--)
        //    {
        //        GridViewRow row = gridView.Rows[rowIndex];
        //        GridViewRow previousRow = gridView.Rows[rowIndex + 1];

        //        Label lblSNo = (Label)row.FindControl("lblAccountNo");
        //        Label lblPreviousSNo = (Label)previousRow.FindControl("lblAccountNo");
        //        for (int i = 0; i < row.Cells.Count; i++)
        //        {
        //            //if (i != 6 || i != 8 || i != 9)
        //            if (i == 1 || i == 2 || i == 3 || i == 4 || i == 5 || i == 7)
        //            {
        //                if (lblSNo.Text == lblPreviousSNo.Text)
        //                {
        //                    row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
        //                    previousRow.Cells[i].Visible = false;
        //                }
        //            }
        //        }
        //    }
        //}

        private void BindFilter()
        {
            DataSet dsFilter = new DataSet();
            dsFilter.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["FilterTypeSettings"]));
            _objiDsignBal.FillDropDownList(dsFilter.Tables[0], ddlFilterType, "name", "value", UMS_Resource.DROPDOWN_SELECT, false);
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region ExportExcel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {

                RptPaymentsBe _objRptPaymentsBe = new RptPaymentsBe();

                if (ddlFilterType.SelectedIndex == 1)
                {
                    _objRptPaymentsBe.BU_ID = _objCommonMethods.CollectSelectedItemsValues(cbl, ",");
                    _objRptPaymentsBe.CycleId = "";

                }
                else if (ddlFilterType.SelectedIndex == 2)
                {
                    _objRptPaymentsBe.CycleId = _objCommonMethods.CollectSelectedItemsValues(cbl, ",");
                    _objRptPaymentsBe.BU_ID = "";
                }
                if (txtFromDate.Text != "")
                    _objRptPaymentsBe.FromDate = _objCommonMethods.Convert_MMDDYY(txtFromDate.Text);
                else
                    _objRptPaymentsBe.FromDate = "";
                if (txtToDate.Text != "")
                    _objRptPaymentsBe.ToDate = _objCommonMethods.Convert_MMDDYY(txtToDate.Text);
                else
                    _objRptPaymentsBe.ToDate = "";
                _objRptPaymentsBe.PageNo = Constants.pageNoValue;
                _objRptPaymentsBe.PageSize = Convert.ToInt32(hfTotalRecords.Value);
                _objRptPaymentsBe.ReceivedDeviceId = Device;

                DataSet dsPayments = new DataSet();
                dsPayments = _objGeneralReportsBal.GetPaymentsReport(_objRptPaymentsBe, ReturnType.Get);

                if (dsPayments != null && dsPayments.Tables.Count > 0 && dsPayments.Tables[0].Rows.Count > 0)
                {
                    var BUlist = (from list in dsPayments.Tables[0].AsEnumerable()
                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                  group list by list.Field<string>("BusinessUnitName") into g
                                  select new { BusinessUnit = g.Key }).ToList();

                    if (BUlist.Count() > 0)
                    {
                        XlsDocument xls = new XlsDocument();
                        Worksheet mainSheet = xls.Workbook.Worksheets.Add("Summary");
                        Cells mainCells = mainSheet.Cells;
                        Cell _objMainCells = null;


                        _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
                        mainCells.Add(4, 2, "Report Code ");
                        mainCells.Add(4, 3, Constants.PaymentsReport_Code).Font.Bold = true;
                        mainCells.Add(4, 4, "Report Name");
                        mainCells.Add(4, 5, Constants.PaymentsReport_Name).Font.Bold = true;
                        mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
                        _objMainCells.Font.Weight = FontWeight.ExtraBold;
                        _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                        _objMainCells.Font.FontFamily = FontFamilies.Roman;

                        int MainSheetDetailsRowNo = 6;
                        mainCells.Add(MainSheetDetailsRowNo, 1, "S.No.").Font.Bold = true;
                        mainCells.Add(MainSheetDetailsRowNo, 2, "Service Center").Font.Bold = true;
                        mainCells.Add(MainSheetDetailsRowNo, 3, "Customer Count").Font.Bold = true;

                        var SClist = (from list in dsPayments.Tables[0].AsEnumerable()
                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                      group list by list.Field<string>("ServiceCenterName") into g
                                      select new { ServiceCenter = g.Key }).ToList();

                        for (int k = 0; k < SClist.Count; k++)//SC
                        {
                            var Customers = (from list in dsPayments.Tables[0].AsEnumerable()
                                             where list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                             select list).ToList();

                            MainSheetDetailsRowNo++;
                            mainCells.Add(MainSheetDetailsRowNo, 1, k + 1);
                            mainCells.Add(MainSheetDetailsRowNo, 2, SClist[k].ServiceCenter);
                            mainCells.Add(MainSheetDetailsRowNo, 3, Customers.Count);
                        }

                        for (int i = 0; i < BUlist.Count; i++)//BU
                        {
                            var SUlistByBU = (from list in dsPayments.Tables[0].AsEnumerable()
                                              where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                              orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                              orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                              group list by list.Field<string>("ServiceUnitName") into g
                                              select new { ServiceUnitName = g.Key }).ToList();

                            for (int j = 0; j < SUlistByBU.Count; j++)//SU
                            {
                                var SClistBySU = (from list in dsPayments.Tables[0].AsEnumerable()
                                                  where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                  && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                  group list by list.Field<string>("ServiceCenterName") into g
                                                  select new { ServiceCenter = g.Key }).ToList();

                                for (int k = 0; k < SClistBySU.Count; k++)//SC
                                {

                                    var CyclelistBySC = (from list in dsPayments.Tables[0].AsEnumerable()
                                                         where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                         && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                         && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                         orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                         orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                         group list by list.Field<string>("CycleName") into g
                                                         select new { CycleName = g.Key }).ToList();

                                    if (CyclelistBySC.Count > 0)
                                    {
                                        int DetailsRowNo = 2;
                                        int CustomersRowNo = 6;

                                        Worksheet sheet = xls.Workbook.Worksheets.Add(SClistBySU[k].ServiceCenter);
                                        Cells cells = sheet.Cells;
                                        Cell _objCell = null;

                                        for (int l = 0; l < CyclelistBySC.Count; l++)//Cycle
                                        {
                                            var BooklistByCycle = (from list in dsPayments.Tables[0].AsEnumerable()
                                                                   where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                   && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                   && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                   && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                   orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                   orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                   group list by list.Field<string>("BookNumber") into g
                                                                   select new { BookNumber = g.Key }).ToList();

                                            for (int m = 0; m < BooklistByCycle.Count; m++)//Book
                                            {
                                                var Customers = (from list in dsPayments.Tables[0].AsEnumerable()
                                                                 where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                 && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                 && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                 && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                 && list.Field<string>("BookNumber") == BooklistByCycle[m].BookNumber
                                                                 orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                 orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                 orderby Convert.ToString(list["CycleName"]) ascending
                                                                 select list).ToList();

                                                if (Customers.Count() > 0)
                                                {
                                                    _objCell = cells.Add(DetailsRowNo, 1, "Business Unit");
                                                    cells.Add(DetailsRowNo, 3, "Service Unit");
                                                    cells.Add(DetailsRowNo, 5, "Service Center");
                                                    cells.Add(DetailsRowNo + 1, 1, "Book Group");
                                                    cells.Add(DetailsRowNo + 1, 3, "Book Code");

                                                    cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 4, SUlistByBU[j].ServiceUnitName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 6, SClistBySU[k].ServiceCenter).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 2, CyclelistBySC[l].CycleName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 4, BooklistByCycle[m].BookNumber).Font.Bold = true;


                                                    _objCell = cells.Add(DetailsRowNo + 3, 1, Resource.GRID_SNO);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 2, Resource.ACCOUNT_NO);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 3, Resource.OLD_ACCOUNT_NO);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 4, Resource.NAME);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 5, Resource.SERVICE_ADDRESS);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 6, Resource.BU);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 7, Resource.CYCLE);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 8, Resource.BATCH_NO);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 9, Resource.AMT_RECEIVED);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 10, Resource.TOTAL_DUE);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 11, Resource.AMT_RECEIVED_DATE);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 12, Resource.RECEIVED_DEVICE);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;

                                                    for (int n = 0; n < Customers.Count; n++)
                                                    {
                                                        DataRow dr = (DataRow)Customers[n];
                                                        cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                                                        cells.Add(CustomersRowNo, 2, dr["AccountNo"].ToString());
                                                        cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
                                                        cells.Add(CustomersRowNo, 4, dr["Name"].ToString());
                                                        cells.Add(CustomersRowNo, 5, dr["ServiceAddress"].ToString());
                                                        cells.Add(CustomersRowNo, 6, dr["BusinessUnitName"].ToString());
                                                        cells.Add(CustomersRowNo, 7, dr["CycleName"].ToString());
                                                        cells.Add(CustomersRowNo, 8, dr["BatchNo"].ToString());
                                                        cells.Add(CustomersRowNo, 9, _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(dr["PaidAmount"]), 2, Constants.MILLION_Format).ToString());
                                                        cells.Add(CustomersRowNo, 10, _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(dr["TotalDueAmount"]), 2, Constants.MILLION_Format).ToString());
                                                        cells.Add(CustomersRowNo, 11, dr["ReceivedDate"].ToString());
                                                        cells.Add(CustomersRowNo, 12, dr["ReceivedDevice"].ToString());

                                                        CustomersRowNo++;
                                                    }
                                                    DetailsRowNo = CustomersRowNo + 2;
                                                    CustomersRowNo = CustomersRowNo + 6;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                        fileName = Constants.PaymentsReport_Name.Replace(" ", string.Empty) + "_" + fileName;
                        string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                        xls.FileName = filePath;
                        xls.Save();
                        _objiDsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                    }
                }



                //#region Old


                //if (ds.Tables.Count > 0)
                //{
                //    if (ds.Tables[0].Rows.Count > 0)
                //    {
                //        var BUlist = (from list in ds.Tables[0].AsEnumerable()
                //                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                //                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                //                      group list by list.Field<string>("BusinessUnitName") into g
                //                      select new { BusinessUnit = g.Key }).ToList();

                //        if (BUlist.Count() > 0)
                //        {
                //            XlsDocument xls = new XlsDocument();
                //            Worksheet mainSheet = xls.Workbook.Worksheets.Add("Summary");
                //            Cells mainCells = mainSheet.Cells;
                //            Cell _objMainCells = null;


                //            _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
                //            mainCells.Add(4, 2, "Report Code ");
                //            mainCells.Add(4, 3, Constants.PreBilling_ReadCustomersReport_Code).Font.Bold = true;
                //            mainCells.Add(4, 4, "Report Name");
                //            mainCells.Add(4, 5, Constants.PreBilling_ReadCustomersReport_Name).Font.Bold = true;
                //            mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
                //            _objMainCells.Font.Weight = FontWeight.ExtraBold;
                //            _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                //            _objMainCells.Font.FontFamily = FontFamilies.Roman;

                //            int MainSheetDetailsRowNo = 6;
                //            mainCells.Add(MainSheetDetailsRowNo, 1, "S.No.").Font.Bold = true;
                //            mainCells.Add(MainSheetDetailsRowNo, 2, "Service Center").Font.Bold = true;
                //            mainCells.Add(MainSheetDetailsRowNo, 3, "Customer Count").Font.Bold = true;

                //            var SClist = (from list in ds.Tables[0].AsEnumerable()
                //                          orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                //                          orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                //                          group list by list.Field<string>("ServiceCenterName") into g
                //                          select new { ServiceCenter = g.Key }).ToList();

                //            for (int k = 0; k < SClist.Count; k++)//SC
                //            {
                //                var Customers = (from list in ds.Tables[0].AsEnumerable()
                //                                 where list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                //                                 select list).ToList();

                //                MainSheetDetailsRowNo++;
                //                mainCells.Add(MainSheetDetailsRowNo, 1, k + 1);
                //                mainCells.Add(MainSheetDetailsRowNo, 2, SClist[k].ServiceCenter);
                //                mainCells.Add(MainSheetDetailsRowNo, 3, Customers.Count);
                //            }

                //            for (int i = 0; i < BUlist.Count; i++)//BU
                //            {
                //                var SUlistByBU = (from list in ds.Tables[0].AsEnumerable()
                //                                  where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                //                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                //                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                //                                  group list by list.Field<string>("ServiceUnitName") into g
                //                                  select new { ServiceUnitName = g.Key }).ToList();

                //                for (int j = 0; j < SUlistByBU.Count; j++)//SU
                //                {
                //                    var SClistBySU = (from list in ds.Tables[0].AsEnumerable()
                //                                      where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                //                                      && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                //                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                //                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                //                                      group list by list.Field<string>("ServiceCenterName") into g
                //                                      select new { ServiceCenter = g.Key }).ToList();

                //                    for (int k = 0; k < SClistBySU.Count; k++)//SC
                //                    {

                //                        var CyclelistBySC = (from list in ds.Tables[0].AsEnumerable()
                //                                             where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                //                                             && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                //                                             && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                //                                             orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                //                                             orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                //                                             group list by list.Field<string>("CycleName") into g
                //                                             select new { CycleName = g.Key }).ToList();

                //                        if (CyclelistBySC.Count > 0)
                //                        {
                //                            int DetailsRowNo = 2;
                //                            int CustomersRowNo = 6;

                //                            Worksheet sheet = xls.Workbook.Worksheets.Add(SClistBySU[k].ServiceCenter);
                //                            Cells cells = sheet.Cells;
                //                            Cell _objCell = null;

                //                            for (int l = 0; l < CyclelistBySC.Count; l++)//Cycle
                //                            {
                //                                var BooklistByCycle = (from list in ds.Tables[0].AsEnumerable()
                //                                                       where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                //                                                       && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                //                                                       && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                //                                                       && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                //                                                       orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                //                                                       orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                //                                                       group list by list.Field<string>("BookNumber") into g
                //                                                       select new { BookNumber = g.Key }).ToList();

                //                                for (int m = 0; m < BooklistByCycle.Count; m++)//Book
                //                                {
                //                                    var Customers = (from list in ds.Tables[0].AsEnumerable()
                //                                                     where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                //                                                     && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                //                                                     && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                //                                                     && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                //                                                     && list.Field<string>("BookNumber") == BooklistByCycle[m].BookNumber
                //                                                     orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                //                                                     orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                //                                                     orderby Convert.ToString(list["CycleName"]) ascending
                //                                                     select list).ToList();

                //                                    if (Customers.Count() > 0)
                //                                    {
                //                                        _objCell = cells.Add(DetailsRowNo, 1, "Business Unit");
                //                                        cells.Add(DetailsRowNo, 3, "Service Unit");
                //                                        cells.Add(DetailsRowNo, 5, "Service Center");
                //                                        cells.Add(DetailsRowNo + 1, 1, "Book Group");
                //                                        cells.Add(DetailsRowNo + 1, 3, "Book Code");

                //                                        cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                //                                        cells.Add(DetailsRowNo, 4, SUlistByBU[j].ServiceUnitName).Font.Bold = true;
                //                                        cells.Add(DetailsRowNo, 6, SClistBySU[k].ServiceCenter).Font.Bold = true;
                //                                        cells.Add(DetailsRowNo + 1, 2, CyclelistBySC[l].CycleName).Font.Bold = true;
                //                                        cells.Add(DetailsRowNo + 1, 4, BooklistByCycle[m].BookNumber).Font.Bold = true;


                //                                        _objCell = cells.Add(DetailsRowNo + 3, 1, "Sno");
                //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                //                                        _objCell = cells.Add(DetailsRowNo + 3, 2, "Global Account No");
                //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                //                                        _objCell = cells.Add(DetailsRowNo + 3, 3, "Old Account No");
                //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                //                                        _objCell = cells.Add(DetailsRowNo + 3, 4, "Meter Number");
                //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                //                                        _objCell = cells.Add(DetailsRowNo + 3, 5, "Tariff");
                //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                //                                        _objCell = cells.Add(DetailsRowNo + 3, 6, "Name");
                //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                //                                        _objCell = cells.Add(DetailsRowNo + 3, 7, "Service Address");
                //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                //                                        _objCell = cells.Add(DetailsRowNo + 3, 8, "Previous Reading");
                //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                //                                        _objCell = cells.Add(DetailsRowNo + 3, 9, "Previous Read Date");
                //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                //                                        _objCell = cells.Add(DetailsRowNo + 3, 10, "Present Reading");
                //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                //                                        _objCell = cells.Add(DetailsRowNo + 3, 11, "Usage");
                //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                //                                        _objCell = cells.Add(DetailsRowNo + 3, 12, "Total Readings");
                //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                //                                        _objCell = cells.Add(DetailsRowNo + 3, 13, "Last Transaction Date");
                //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                //                                        _objCell = cells.Add(DetailsRowNo + 3, 14, "Transaction Log");
                //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                //                                        _objCell.Font.FontFamily = FontFamilies.Roman;

                //                                        for (int n = 0; n < Customers.Count; n++)
                //                                        {
                //                                            DataRow dr = (DataRow)Customers[n];
                //                                            cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                //                                            cells.Add(CustomersRowNo, 2, dr["GlobalAccountNumber"].ToString());
                //                                            cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
                //                                            cells.Add(CustomersRowNo, 4, dr["MeterNo"].ToString());
                //                                            cells.Add(CustomersRowNo, 5, dr["ClassName"].ToString());
                //                                            cells.Add(CustomersRowNo, 6, dr["Name"].ToString());
                //                                            cells.Add(CustomersRowNo, 7, dr["ServiceAddress"].ToString());
                //                                            cells.Add(CustomersRowNo, 8, dr["PreviousReading"].ToString());
                //                                            cells.Add(CustomersRowNo, 9, dr["PreviousReadDate"].ToString());
                //                                            cells.Add(CustomersRowNo, 10, dr["PresentReading"].ToString());
                //                                            cells.Add(CustomersRowNo, 11, dr["Usage"].ToString());
                //                                            cells.Add(CustomersRowNo, 12, dr["TotalReadings"].ToString());
                //                                            cells.Add(CustomersRowNo, 13, dr["LatestTransactionDate"].ToString());
                //                                            cells.Add(CustomersRowNo, 14, dr["TransactionLog"].ToString());

                //                                            CustomersRowNo++;
                //                                        }
                //                                        DetailsRowNo = CustomersRowNo + 2;
                //                                        CustomersRowNo = CustomersRowNo + 6;
                //                                    }
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //            }
                //            string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                //            fileName = Constants.PreBilling_ReadCustomersReport_Name.Replace(" ", string.Empty) + "_" + fileName;
                //            string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                //            xls.FileName = filePath;
                //            xls.Save();
                //            _objIdsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                //        }
                //    }
                //    else
                //    {
                //        Message(Resource.NO_READ_CUST, UMS_Resource.MESSAGETYPE_ERROR);
                //    }

                //}
                //else
                //{
                //    Message(Resource.NO_READ_CUST, UMS_Resource.MESSAGETYPE_ERROR);
                //}
                //#endregion

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
            //try
            //{
            //    RptCreditBe _objRptCreditBe = new RptCreditBe();
            //    _objRptCreditBe.BU_ID = this.BusinessUnitName;
            //    _objRptCreditBe.SU_ID = this.SU;
            //    _objRptCreditBe.SC_ID = this.SC;
            //    _objRptCreditBe.CycleId = this.Cycle;
            //    _objRptCreditBe.BookNo = this.BookNo;
            //    _objRptCreditBe.PageNo = Convert.ToInt32(hfPageNo.Value);
            //    _objRptCreditBe.PageSize = PageSize;

            //    DataSet dsCredit = new DataSet();
            //    dsCredit = _objGeneralReportsBal.GetCreditReport(_objRptCreditBe, ReturnType.Get);


            //    if (dsCredit.Tables[0].Rows.Count > 0)
            //    {
            //        var BUlist = (from list in dsCredit.Tables[0].AsEnumerable()
            //                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
            //                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
            //                      orderby list["CycleName"] ascending
            //                      group list by list.Field<string>("BusinessUnitName") into g
            //                      select new { BusinessUnit = g.Key }).ToList();

            //        if (BUlist.Count() > 0)
            //        {
            //            XlsDocument xls = new XlsDocument();
            //            Worksheet mainSheet = xls.Workbook.Worksheets.Add("Summary");
            //            Cells mainCells = mainSheet.Cells;
            //            Cell _objMainCells = null;

            //            _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
            //            mainCells.Add(4, 2, "Report Code ");
            //            mainCells.Add(4, 3, Constants.AccountsWithCreditBalanceReport_Code).Font.Bold = true;
            //            mainCells.Add(4, 4, "Report Name");
            //            mainCells.Add(4, 5, Constants.AccountsWithCreditBalanceReport_Name).Font.Bold = true;
            //            mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
            //            _objMainCells.Font.Weight = FontWeight.ExtraBold;
            //            _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
            //            _objMainCells.Font.FontFamily = FontFamilies.Roman;

            //            int MainSheetDetailsRowNo = 6;
            //            _objMainCells = mainCells.Add(MainSheetDetailsRowNo, 1, "S.No.");
            //            _objMainCells.Font.Bold = true;
            //            _objMainCells.UseBorder = true;
            //            _objMainCells = mainCells.Add(MainSheetDetailsRowNo, 2, "Service Center");
            //            _objMainCells.Font.Bold = true;
            //            _objMainCells.UseBorder = true;
            //            _objMainCells = mainCells.Add(MainSheetDetailsRowNo, 3, "Customer Count");
            //            _objMainCells.Font.Bold = true;
            //            _objMainCells.UseBorder = true;
            //            _objMainCells = mainCells.Add(MainSheetDetailsRowNo, 4, "Total Credit Balance");
            //            _objMainCells.Font.Bold = true;
            //            _objMainCells.UseBorder = true;

            //            decimal TotalDueAmount = 0;

            //            var SClist = (from list in dsCredit.Tables[0].AsEnumerable()
            //                          orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
            //                          orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
            //                          orderby list["CycleName"] ascending
            //                          group list by list.Field<string>("ServiceCenterName") into g
            //                          select new { ServiceCenter = g.Key }).ToList();

            //            for (int k = 0; k < SClist.Count; k++)//SC
            //            {
            //                var Customers = (from list in dsCredit.Tables[0].AsEnumerable()
            //                                 where list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
            //                                 select list).ToList();

            //                TotalDueAmount = Customers.Sum(x => Convert.ToDecimal(x["OverPayAmount"]));
            //                string DueAmount = _objCommonMethods.GetCurrencyFormat(TotalDueAmount, 2, Constants.MILLION_Format);

            //                MainSheetDetailsRowNo++;
            //                mainCells.Add(MainSheetDetailsRowNo, 1, k + 1).UseBorder = true;
            //                mainCells.Add(MainSheetDetailsRowNo, 2, SClist[k].ServiceCenter).UseBorder = true;
            //                mainCells.Add(MainSheetDetailsRowNo, 3, Customers.Count).UseBorder = true;
            //                mainCells.Add(MainSheetDetailsRowNo, 4, DueAmount).UseBorder = true;
            //            }

            //            for (int i = 0; i < BUlist.Count; i++)//BU
            //            {
            //                var SUlistByBU = (from list in dsCredit.Tables[0].AsEnumerable()
            //                                  where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
            //                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
            //                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
            //                                  group list by list.Field<string>("ServiceUnitName") into g
            //                                  select new { ServiceUnitName = g.Key }).ToList();

            //                for (int j = 0; j < SUlistByBU.Count; j++)//SU
            //                {
            //                    var SClistBySU = (from list in dsCredit.Tables[0].AsEnumerable()
            //                                      where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
            //                                      && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
            //                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
            //                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
            //                                      group list by list.Field<string>("ServiceCenterName") into g
            //                                      select new { ServiceCenter = g.Key }).ToList();

            //                    for (int k = 0; k < SClistBySU.Count; k++)//SC
            //                    {
            //                        var CyclelistBySC = (from list in dsCredit.Tables[0].AsEnumerable()
            //                                             where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
            //                                             && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
            //                                             && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
            //                                             orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
            //                                             orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
            //                                             group list by list.Field<string>("CycleName") into g
            //                                             select new { CycleName = g.Key }).ToList();

            //                        if (CyclelistBySC.Count > 0)
            //                        {
            //                            decimal finalTotal = 0;
            //                            int DetailsRowNo = 2;
            //                            int CustomersRowNo = 5;

            //                            Worksheet sheet = xls.Workbook.Worksheets.Add(SClistBySU[k].ServiceCenter);
            //                            Cells cells = sheet.Cells;
            //                            Cell _objCell = null;

            //                            for (int l = 0; l < CyclelistBySC.Count; l++)//Cycle
            //                            {
            //                                var BooklistByCycle = (from list in dsCredit.Tables[0].AsEnumerable()
            //                                                       where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
            //                                                       && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
            //                                                       && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
            //                                                       && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
            //                                                       orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
            //                                                       orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
            //                                                       group list by list.Field<string>("BookNumber") into g
            //                                                       select new { BookNumber = g.Key }).ToList();

            //                                for (int m = 0; m < BooklistByCycle.Count; m++)//Book
            //                                {
            //                                    var Customers = (from list in dsCredit.Tables[0].AsEnumerable()
            //                                                     where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
            //                                                     && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
            //                                                     && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
            //                                                     && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
            //                                                     && list.Field<string>("BookNumber") == BooklistByCycle[m].BookNumber
            //                                                     orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
            //                                                     orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
            //                                                     orderby Convert.ToString(list["CycleName"]) ascending
            //                                                     select list).ToList();

            //                                    if (Customers.Count() > 0)
            //                                    {
            //                                        cells.Add(DetailsRowNo, 1, "Business Unit");
            //                                        cells.Add(DetailsRowNo, 3, "Service Unit");
            //                                        cells.Add(DetailsRowNo, 5, "Service Center");
            //                                        cells.Add(DetailsRowNo + 1, 1, "Book Group");
            //                                        cells.Add(DetailsRowNo + 1, 3, "Book Code");

            //                                        cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
            //                                        cells.Add(DetailsRowNo, 4, SUlistByBU[j].ServiceUnitName).Font.Bold = true;
            //                                        cells.Add(DetailsRowNo, 6, SClist[k].ServiceCenter).Font.Bold = true;
            //                                        cells.Add(DetailsRowNo + 1, 2, CyclelistBySC[l].CycleName).Font.Bold = true;
            //                                        cells.Add(DetailsRowNo + 1, 4, BooklistByCycle[m].BookNumber).Font.Bold = true;


            //                                        _objCell = cells.Add(DetailsRowNo + 3, 1, "Sno");
            //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
            //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
            //                                        _objCell = cells.Add(DetailsRowNo + 3, 2, "Account No");
            //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
            //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
            //                                        _objCell = cells.Add(DetailsRowNo + 3, 3, "Old Account No");
            //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
            //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
            //                                        _objCell = cells.Add(DetailsRowNo + 3, 4, "Name");
            //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
            //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
            //                                        _objCell = cells.Add(DetailsRowNo + 3, 5, "Service Address");
            //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
            //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
            //                                        _objCell = cells.Add(DetailsRowNo + 3, 6, "Book Code");
            //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
            //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
            //                                        _objCell = cells.Add(DetailsRowNo + 3, 7, "Last Paid Amount");
            //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
            //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
            //                                        _objCell = cells.Add(DetailsRowNo + 3, 8, "Last Paid Date");
            //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
            //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
            //                                        _objCell = cells.Add(DetailsRowNo + 3, 9, "Total Cedit Balance");
            //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
            //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
            //                                        _objCell = cells.Add(DetailsRowNo + 3, 10, "Total Bills Amount");
            //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
            //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
            //                                        _objCell = cells.Add(DetailsRowNo + 3, 11, "Total Paid Amount");
            //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
            //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            //                                        _objCell.Font.FontFamily = FontFamilies.Roman;

            //                                        CustomersRowNo++;
            //                                        decimal subtotal = 0;
            //                                        for (int n = 0; n < Customers.Count; n++)
            //                                        {
            //                                            DataRow dr = (DataRow)Customers[n];
            //                                            cells.Add(CustomersRowNo, 1, (n + 1).ToString());
            //                                            cells.Add(CustomersRowNo, 2, dr["GlobalAccountNumber"].ToString());
            //                                            cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
            //                                            cells.Add(CustomersRowNo, 4, dr["Name"].ToString());
            //                                            cells.Add(CustomersRowNo, 5, dr["ServiceAddress"].ToString());
            //                                            cells.Add(CustomersRowNo, 6, dr["BookCode"].ToString());
            //                                            cells.Add(CustomersRowNo, 7, dr["LastPaidAmount"].ToString());
            //                                            cells.Add(CustomersRowNo, 8, dr["LastPaidDate"].ToString());
            //                                            cells.Add(CustomersRowNo, 9, dr["OverPayAmount"].ToString());
            //                                            cells.Add(CustomersRowNo, 10, dr["TotalBillsAmount"].ToString());
            //                                            cells.Add(CustomersRowNo, 11, dr["TotalPaidAmount"].ToString());

            //                                            CustomersRowNo++;
            //                                            subtotal += Convert.ToDecimal(dr["OverPayAmount"]);
            //                                        }
            //                                        _objCell = cells.Add(CustomersRowNo + 1, 8, "Sub Total");
            //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
            //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            //                                        _objCell.Font.FontFamily = FontFamilies.Roman;
            //                                        _objCell = cells.Add(CustomersRowNo + 1, 9, _objCommonMethods.GetCurrencyFormat(subtotal, 2, Constants.MILLION_Format));
            //                                        _objCell.Font.Weight = FontWeight.ExtraBold;
            //                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            //                                        _objCell.Font.FontFamily = FontFamilies.Roman;

            //                                        finalTotal += subtotal;
            //                                        DetailsRowNo = CustomersRowNo + 3;
            //                                        CustomersRowNo = CustomersRowNo + 6;
            //                                    }
            //                                }
            //                            }
            //                            _objCell = cells.Add(CustomersRowNo - 3, 8, "Grand Total");
            //                            _objCell.Font.Weight = FontWeight.ExtraBold;
            //                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            //                            _objCell.Font.FontFamily = FontFamilies.Roman;
            //                            _objCell = cells.Add(CustomersRowNo - 3, 9, _objCommonMethods.GetCurrencyFormat(finalTotal, 2, Constants.MILLION_Format));
            //                            _objCell.Font.Weight = FontWeight.ExtraBold;
            //                            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            //                            _objCell.Font.FontFamily = FontFamilies.Roman;
            //                        }
            //                    }
            //                }
            //            }
            //            string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
            //            fileName = "CustomersCreditBalance_" + fileName;
            //            string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
            //            xls.FileName = filePath;
            //            xls.Save();
            //            _objiDsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
            //        }
            //        else
            //            Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
            //    }
            //    else
            //        Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
            //}
            //catch (Exception ex)
            //{
            //    Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
            //    try
            //    {
            //        _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
            //    }
            //    catch (Exception logex)
            //    {

            //    }
            //}
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}