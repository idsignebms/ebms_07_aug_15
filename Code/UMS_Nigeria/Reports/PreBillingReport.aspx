﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreBillingReport.aspx.cs"
    Title=":: Pre Billing Reports ::" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" Inherits="UMS_Nigeria.Reports.PreBillingReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="contentBody" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upAgencies" runat="server">
            <ContentTemplate>
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litSearchCustomer" runat="server" Text="Customer Pre Billing Report"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner" style="width: 430px;">
                                <label for="name">
                                    <asp:Literal ID="Literal3" runat="server" Text="Open Month Details :: "></asp:Literal></label>
                                <asp:Label ID="lblMonthDetails" runat="server" Text="---"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div id="divHide" runat="server" visible="true">
                        <%----BU SU SC filters Starts----%>
                        <div class="inner-box">
                            <div id="divBusinessUnit" runat="server">
                                <div class="check_baa">
                                    <div class="text-inner_a">
                                        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                                    </div>
                                    <div class="check_ba">
                                        <div class="text-inner mster-input">
                                            <asp:CheckBox runat="server" ID="ChkBUAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                            <asp:CheckBoxList ID="cblBusinessUnits" class="text-inner-b" AutoPostBack="true"
                                                RepeatDirection="Horizontal" RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblBusinessUnits_SelectedIndexChanged">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div id="divServiceUnits" runat="server" visible="false">
                            <div class="check_baa">
                                <div class="text-inner_a">
                                    <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" ID="ChkSUAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                        <asp:CheckBoxList ID="cblServiceUnits" class="text-inner-b" AutoPostBack="true" RepeatDirection="Horizontal"
                                            RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceUnits_SelectedIndexChanged">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="divServiceCenters" runat="server" visible="false">
                            <div class="check_baa">
                                <div class="text-inner_a">
                                    <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" ID="chkSCAll" class="text-inner-b" Text="All" />
                                        <asp:CheckBoxList ID="cblServiceCenters" class="text-inner-b" RepeatDirection="Horizontal"
                                            RepeatColumns="3" runat="server">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <%----BU SU SC filters Ends----%>
                        <div class="clr">
                            <br />
                        </div>
                        <div class="clr">
                            <br />
                        </div>
                        <div class="box_total">
                            <asp:Button ID="btnGetReadCustomers" CssClass="box_s" runat="server" Text="Get Read Customers"
                                OnClick="btnGetReadCustomers_Click" OnClientClick="ShowProgress();" />
                            <asp:Button ID="btnGetNonReadCustomers" CssClass="box_s" runat="server" Text="Get Non Read Customers"
                                OnClick="btnGetNonReadCustomers_Click" OnClientClick="ShowProgress();" />
                            <asp:Button ID="btnGetZeroUsageCustomers" CssClass="box_s" runat="server" Text="Get Zero Usage Customers"
                                OnClick="btnGetZeroUsageCustomers_Click" OnClientClick="ShowProgress();" />
                            <asp:Button ID="btnDirectCustomeresAvgDetails" CssClass="box_s" runat="server" Text="Get Direct Customers-Average"
                                OnClick="btnDirectCustomeresAvgDetails_Click" OnClientClick="ShowProgress();" />
                            <asp:Button ID="btnAvgNotUploadCustomers" CssClass="box_s" runat="server" Text="Get Average Not Upload Customers"
                                OnClick="btnAvgNotUploadCustomers_Click" OnClientClick="ShowProgress();" />
                            
                            <div class="clr pad_10">
                            </div>
                            <asp:Button ID="btnGetEstimatedCustomers" CssClass="box_s" runat="server" Text="Get High/Low Estimated Customers"
                                OnClick="btnGetEstimatedCustomers_Click" OnClientClick="ShowProgress();" />
                            <asp:Button ID="btnPartialBillCustomers" CssClass="box_s" runat="server" Text="Get Partial Bill Customers"
                                OnClick="btnPartialBillCustomers_Click" OnClientClick="ShowProgress();" />
                            <asp:Button ID="btnGetNoBillCustomers" CssClass="box_s" runat="server" Text="Get No Bill Customers"
                                OnClick="btnGetNoBillCustomers_Click" OnClientClick="ShowProgress();" />
                        </div>
                        <asp:HiddenField ID="hfMonthDetails" runat="server" />
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnGetReadCustomers" />
                <asp:PostBackTrigger ControlID="btnGetNonReadCustomers" />
                <asp:PostBackTrigger ControlID="btnGetZeroUsageCustomers" />
                <asp:PostBackTrigger ControlID="btnGetEstimatedCustomers" />
                <asp:PostBackTrigger ControlID="btnGetNoBillCustomers" />
                <asp:PostBackTrigger ControlID="btnPartialBillCustomers" />
                <asp:PostBackTrigger ControlID="btnAvgNotUploadCustomers" />
                <asp:PostBackTrigger ControlID="btnDirectCustomeresAvgDetails" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/UnBilledCustomersReport.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }  
    </script>
    <script language="javascript" type="text/javascript">
        function ShowProgress() {
            debugger;
            document.getElementById('UMSNigeriaBody_UpdateProgress').style = "position: fixed; z-index: 100001; left: 554.5px; top: 107.5px; display: block;";
            document.getElementById('UMSNigeriaBody_modalPopupLoading_backgroundElement').style = "position: fixed; left: 0px; top: 0px; z-index: 10000; width: 1343px; height: 699px;";
            setTimeout("HideProgress()", 1000);
        }
        function HideProgress() {
            document.getElementById('UMSNigeriaBody_UpdateProgress').style.display = "none";
            document.getElementById('UMSNigeriaBody_modalPopupLoading_backgroundElement').style.display = "none";
        }        
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
