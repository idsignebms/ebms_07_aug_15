﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using System.Xml;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using iDSignHelper;
using org.in2bits.MyXls;
using System.Configuration;
using System.IO;
using Resources;
using System.Data;

namespace UMS_Nigeria.Reports
{
    public partial class PreBillingReport : System.Web.UI.Page
    {
        #region Members
        ReportsBal _objReportsBal = new ReportsBal();
        iDsignBAL _objIdsignBal = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        string Key = "PreBillingReport";
        #endregion

        #region Properties
        public string BusinessUnitName
        {
            get { return objCommonMethods.CollectSelectedItemsValues(cblBusinessUnits, ","); }
        }
        public string ServiceUnitName
        {
            get { return objCommonMethods.CollectSelectedItemsValues(cblServiceUnits, ","); }
        }
        public string ServiceCenterName
        {
            get { return objCommonMethods.CollectSelectedItemsValues(cblServiceCenters, ","); }
        }
        #endregion Properties

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                if (!IsPostBack)
                {
                    string path = string.Empty;
                    path = objCommonMethods.GetPagePath(this.Request.Url);
                    if (objCommonMethods.IsAccess(path))
                    {
                        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        {
                            ListItem LiBU = new ListItem(Session[UMS_Resource.SESSION_USER_BUNAME].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());
                            cblBusinessUnits.Items.Add(LiBU);
                            cblBusinessUnits.Items[0].Selected = true;
                            cblBusinessUnits.Enabled = false;
                            ChkBUAll.Visible = false;
                            divServiceUnits.Visible = true;
                            objCommonMethods.BindUserServiceUnits_BuIds(cblServiceUnits, BusinessUnitName, Resource.DDL_ALL, false);
                        }
                        else
                            objCommonMethods.BindBusinessUnits(cblBusinessUnits, string.Empty, false);

                        CheckBoxesJS();
                        GetOpenMonthDetails();

                    }
                    else
                    {
                        Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                    }
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }

        protected void cblBusinessUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblServiceCenters.Items.Clear();
                cblServiceUnits.Items.Clear();
                chkSCAll.Checked = ChkSUAll.Checked = ChkBUAll.Checked = false;

                divServiceUnits.Visible = chkSCAll.Checked = divServiceCenters.Visible = false;
                if (!string.IsNullOrEmpty(BusinessUnitName))
                {
                    objCommonMethods.BindUserServiceUnits_BuIds(cblServiceUnits, BusinessUnitName, Resource.DDL_ALL, false);
                    if (cblServiceUnits.Items.Count > 0)
                        divServiceUnits.Visible = true;
                    else
                        divServiceUnits.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //cblCycles.Items.Clear();
                //cblBooks.Items.Clear();
                cblServiceCenters.Items.Clear();
                //divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = chkSCAll.Checked = 
                divServiceCenters.Visible = false;
                if (!string.IsNullOrEmpty(ServiceUnitName))
                {
                    objCommonMethods.BindUserServiceCenters_SuIds(cblServiceCenters, ServiceUnitName, Resource.DDL_ALL, false);
                    if (cblServiceCenters.Items.Count > 0)
                        divServiceCenters.Visible = true;
                    else
                        divServiceCenters.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnGetReadCustomers_Click(object sender, EventArgs e)
        {
            RptPreBillingBe _objPreBillingBe = new RptPreBillingBe();
            RptPreBillingListBe _objPreBillingListBe = new RptPreBillingListBe();
            _objPreBillingBe.BU_ID = BusinessUnitName;
            _objPreBillingBe.SU_ID = ServiceUnitName;
            _objPreBillingBe.ServiceCenterId = ServiceCenterName;
            DataSet ds = new DataSet();
            ds = _objReportsBal.GetPreBillingReports(_objPreBillingBe, ReturnType.Get);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    var BUlist = (from list in ds.Tables[0].AsEnumerable()
                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                  group list by list.Field<string>("BusinessUnitName") into g
                                  select new { BusinessUnit = g.Key }).ToList();

                    if (BUlist.Count() > 0)
                    {
                        XlsDocument xls = new XlsDocument();
                        Worksheet mainSheet = xls.Workbook.Worksheets.Add("Summary");
                        Cells mainCells = mainSheet.Cells;
                        Cell _objMainCells = null;


                        _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
                        mainCells.Add(4, 2, "Report Code ");
                        mainCells.Add(4, 3, Constants.PreBilling_ReadCustomersReport_Code).Font.Bold = true;
                        mainCells.Add(4, 4, "Report Name");
                        mainCells.Add(4, 5, Constants.PreBilling_ReadCustomersReport_Name).Font.Bold = true;
                        mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
                        _objMainCells.Font.Weight = FontWeight.ExtraBold;
                        _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                        _objMainCells.Font.FontFamily = FontFamilies.Roman;

                        int MainSheetDetailsRowNo = 6;
                        mainCells.Add(MainSheetDetailsRowNo, 1, "S.No.").Font.Bold = true;
                        mainCells.Add(MainSheetDetailsRowNo, 2, "Service Center").Font.Bold = true;
                        mainCells.Add(MainSheetDetailsRowNo, 3, "Customer Count").Font.Bold = true;

                        var SClist = (from list in ds.Tables[0].AsEnumerable()
                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                      group list by list.Field<string>("ServiceCenterName") into g
                                      select new { ServiceCenter = g.Key }).ToList();

                        for (int k = 0; k < SClist.Count; k++)//SC
                        {
                            var Customers = (from list in ds.Tables[0].AsEnumerable()
                                             where list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                             select list).ToList();

                            MainSheetDetailsRowNo++;
                            mainCells.Add(MainSheetDetailsRowNo, 1, k + 1);
                            mainCells.Add(MainSheetDetailsRowNo, 2, SClist[k].ServiceCenter);
                            mainCells.Add(MainSheetDetailsRowNo, 3, Customers.Count);
                        }

                        for (int i = 0; i < BUlist.Count; i++)//BU
                        {
                            var SUlistByBU = (from list in ds.Tables[0].AsEnumerable()
                                              where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                              orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                              orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                              group list by list.Field<string>("ServiceUnitName") into g
                                              select new { ServiceUnitName = g.Key }).ToList();

                            for (int j = 0; j < SUlistByBU.Count; j++)//SU
                            {
                                var SClistBySU = (from list in ds.Tables[0].AsEnumerable()
                                                  where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                  && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                  group list by list.Field<string>("ServiceCenterName") into g
                                                  select new { ServiceCenter = g.Key }).ToList();

                                for (int k = 0; k < SClistBySU.Count; k++)//SC
                                {

                                    var CyclelistBySC = (from list in ds.Tables[0].AsEnumerable()
                                                         where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                         && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                         && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                         orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                         orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                         group list by list.Field<string>("CycleName") into g
                                                         select new { CycleName = g.Key }).ToList();

                                    if (CyclelistBySC.Count > 0)
                                    {
                                        int DetailsRowNo = 2;
                                        int CustomersRowNo = 6;

                                        Worksheet sheet = xls.Workbook.Worksheets.Add(SClistBySU[k].ServiceCenter);
                                        Cells cells = sheet.Cells;
                                        Cell _objCell = null;

                                        for (int l = 0; l < CyclelistBySC.Count; l++)//Cycle
                                        {
                                            var BooklistByCycle = (from list in ds.Tables[0].AsEnumerable()
                                                                   where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                   && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                   && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                   && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                   orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                   orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                   group list by list.Field<string>("BookNumber") into g
                                                                   select new { BookNumber = g.Key }).ToList();

                                            for (int m = 0; m < BooklistByCycle.Count; m++)//Book
                                            {
                                                var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                                 where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                 && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                 && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                 && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                 && list.Field<string>("BookNumber") == BooklistByCycle[m].BookNumber
                                                                 orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                 orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                 orderby Convert.ToString(list["CycleName"]) ascending
                                                                 select list).ToList();

                                                if (Customers.Count() > 0)
                                                {
                                                    _objCell = cells.Add(DetailsRowNo, 1, "Business Unit");
                                                    cells.Add(DetailsRowNo, 3, "Service Unit");
                                                    cells.Add(DetailsRowNo, 5, "Service Center");
                                                    cells.Add(DetailsRowNo + 1, 1, "Book Group");
                                                    cells.Add(DetailsRowNo + 1, 3, "Book Code");

                                                    cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 4, SUlistByBU[j].ServiceUnitName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 6, SClistBySU[k].ServiceCenter).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 2, CyclelistBySC[l].CycleName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 4, BooklistByCycle[m].BookNumber).Font.Bold = true;


                                                    _objCell = cells.Add(DetailsRowNo + 3, 1, "Sno");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 2, "Global Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 3, "Old Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 4, "Meter Number");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 5, "Tariff");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 6, "Name");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 7, "Service Address");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 8, "Previous Reading");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 9, "Previous Read Date");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 10, "Present Reading");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 11, "Usage");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 12, "Total Readings");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 13, "Last Transaction Date");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 14, "Transaction Log");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;

                                                    for (int n = 0; n < Customers.Count; n++)
                                                    {
                                                        DataRow dr = (DataRow)Customers[n];
                                                        cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                                                        cells.Add(CustomersRowNo, 2, dr["GlobalAccountNumber"].ToString());
                                                        cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
                                                        cells.Add(CustomersRowNo, 4, dr["MeterNo"].ToString());
                                                        cells.Add(CustomersRowNo, 5, dr["ClassName"].ToString());
                                                        cells.Add(CustomersRowNo, 6, dr["Name"].ToString());
                                                        cells.Add(CustomersRowNo, 7, dr["ServiceAddress"].ToString());
                                                        cells.Add(CustomersRowNo, 8, dr["PreviousReading"].ToString());
                                                        cells.Add(CustomersRowNo, 9, dr["PreviousReadDate"].ToString());
                                                        cells.Add(CustomersRowNo, 10, dr["PresentReading"].ToString());
                                                        cells.Add(CustomersRowNo, 11, dr["Usage"].ToString());
                                                        cells.Add(CustomersRowNo, 12, dr["TotalReadings"].ToString());
                                                        cells.Add(CustomersRowNo, 13, dr["LatestTransactionDate"].ToString());
                                                        cells.Add(CustomersRowNo, 14, dr["TransactionLog"].ToString());

                                                        CustomersRowNo++;
                                                    }
                                                    DetailsRowNo = CustomersRowNo + 2;
                                                    CustomersRowNo = CustomersRowNo + 6;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                        fileName = Constants.PreBilling_ReadCustomersReport_Name.Replace(" ", string.Empty) + "_" + fileName;
                        string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                        xls.FileName = filePath;
                        xls.Save();
                        _objIdsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                    }
                }
                else
                {
                    Message(Resource.NO_READ_CUST, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            else
            {
                Message(Resource.NO_READ_CUST, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        protected void btnGetNonReadCustomers_Click(object sender, EventArgs e)
        {
            RptPreBillingBe _objPreBillingBe = new RptPreBillingBe();
            RptPreBillingListBe _objPreBillingListBe = new RptPreBillingListBe();
            _objPreBillingBe.BU_ID = BusinessUnitName;
            _objPreBillingBe.SU_ID = ServiceUnitName;
            _objPreBillingBe.ServiceCenterId = ServiceCenterName;
            DataSet ds = new DataSet();
            ds = _objReportsBal.GetPreBillingReports(_objPreBillingBe, ReturnType.Bulk);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    var BUlist = (from list in ds.Tables[0].AsEnumerable()
                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                  group list by list.Field<string>("BusinessUnitName") into g
                                  select new { BusinessUnit = g.Key }).ToList();

                    if (BUlist.Count() > 0)
                    {
                        XlsDocument xls = new XlsDocument();
                        Worksheet mainSheet = xls.Workbook.Worksheets.Add("Summary");
                        Cells mainCells = mainSheet.Cells;
                        Cell _objMainCells = null;


                        _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
                        mainCells.Add(4, 2, "Report Code ");
                        mainCells.Add(4, 3, Constants.PreBilling_NonReadCustomersReport_Code).Font.Bold = true;
                        mainCells.Add(4, 4, "Report Name");
                        mainCells.Add(4, 5, Constants.PreBilling_NonReadCustomersReport_Name).Font.Bold = true;
                        mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
                        _objMainCells.Font.Weight = FontWeight.ExtraBold;
                        _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                        _objMainCells.Font.FontFamily = FontFamilies.Roman;

                        int MainSheetDetailsRowNo = 6;
                        mainCells.Add(MainSheetDetailsRowNo, 1, "S.No.").Font.Bold = true;
                        mainCells.Add(MainSheetDetailsRowNo, 2, "Service Center").Font.Bold = true;
                        mainCells.Add(MainSheetDetailsRowNo, 3, "Customer Count").Font.Bold = true;

                        var SClist = (from list in ds.Tables[0].AsEnumerable()
                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                      group list by list.Field<string>("ServiceCenterName") into g
                                      select new { ServiceCenter = g.Key }).ToList();

                        for (int k = 0; k < SClist.Count; k++)//SC
                        {
                            var Customers = (from list in ds.Tables[0].AsEnumerable()
                                             where list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                             select list).ToList();

                            MainSheetDetailsRowNo++;
                            mainCells.Add(MainSheetDetailsRowNo, 1, k + 1);
                            mainCells.Add(MainSheetDetailsRowNo, 2, SClist[k].ServiceCenter);
                            mainCells.Add(MainSheetDetailsRowNo, 3, Customers.Count);
                        }

                        for (int i = 0; i < BUlist.Count; i++)//BU
                        {
                            var SUlistByBU = (from list in ds.Tables[0].AsEnumerable()
                                              where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                              orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                              orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                              group list by list.Field<string>("ServiceUnitName") into g
                                              select new { ServiceUnitName = g.Key }).ToList();

                            for (int j = 0; j < SUlistByBU.Count; j++)//SU
                            {
                                var SClistBySU = (from list in ds.Tables[0].AsEnumerable()
                                                  where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                  && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                  group list by list.Field<string>("ServiceCenterName") into g
                                                  select new { ServiceCenter = g.Key }).ToList();

                                for (int k = 0; k < SClistBySU.Count; k++)//SC
                                {

                                    var CyclelistBySC = (from list in ds.Tables[0].AsEnumerable()
                                                         where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                         && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                         && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                         orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                         orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                         group list by list.Field<string>("CycleName") into g
                                                         select new { CycleName = g.Key }).ToList();

                                    if (CyclelistBySC.Count > 0)
                                    {
                                        int DetailsRowNo = 2;
                                        int CustomersRowNo = 6;

                                        Worksheet sheet = xls.Workbook.Worksheets.Add(SClistBySU[k].ServiceCenter);
                                        Cells cells = sheet.Cells;
                                        Cell _objCell = null;

                                        for (int l = 0; l < CyclelistBySC.Count; l++)//Cycle
                                        {
                                            var BooklistByCycle = (from list in ds.Tables[0].AsEnumerable()
                                                                   where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                   && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                   && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                   && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                   orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                   orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                   group list by list.Field<string>("BookNumber") into g
                                                                   select new { BookNumber = g.Key }).ToList();

                                            for (int m = 0; m < BooklistByCycle.Count; m++)//Book
                                            {
                                                var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                                 where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                 && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                 && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                 && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                 && list.Field<string>("BookNumber") == BooklistByCycle[m].BookNumber
                                                                 orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                 orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                 orderby Convert.ToString(list["CycleName"]) ascending
                                                                 select list).ToList();

                                                if (Customers.Count() > 0)
                                                {
                                                    _objCell = cells.Add(DetailsRowNo, 1, "Business Unit");
                                                    cells.Add(DetailsRowNo, 3, "Service Unit");
                                                    cells.Add(DetailsRowNo, 5, "Service Center");
                                                    cells.Add(DetailsRowNo + 1, 1, "Book Group");
                                                    cells.Add(DetailsRowNo + 1, 3, "Book Code");

                                                    cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 4, SUlistByBU[j].ServiceUnitName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 6, SClistBySU[k].ServiceCenter).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 2, CyclelistBySC[l].CycleName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 4, BooklistByCycle[m].BookNumber).Font.Bold = true;


                                                    _objCell = cells.Add(DetailsRowNo + 3, 1, "Sno");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 2, "Global Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 3, "Old Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 4, "Meter Number");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 5, "Tariff");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 6, "Name");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 7, "Service Address");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 8, "Previous Reading");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 9, "Previous Read Date");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 10, "Present Reading");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 11, "Usage");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 12, "Created Date");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 13, "Created By");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 14, "Read Date");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;

                                                    for (int n = 0; n < Customers.Count; n++)
                                                    {
                                                        DataRow dr = (DataRow)Customers[n];
                                                        cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                                                        cells.Add(CustomersRowNo, 2, dr["GlobalAccountNumber"].ToString());
                                                        cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
                                                        cells.Add(CustomersRowNo, 4, dr["MeterNo"].ToString());
                                                        cells.Add(CustomersRowNo, 5, dr["ClassName"].ToString());
                                                        cells.Add(CustomersRowNo, 6, dr["Name"].ToString());
                                                        cells.Add(CustomersRowNo, 7, dr["ServiceAddress"].ToString());
                                                        cells.Add(CustomersRowNo, 8, dr["PreviousReading"].ToString());
                                                        cells.Add(CustomersRowNo, 9, dr["PreviousReadDate"].ToString());
                                                        cells.Add(CustomersRowNo, 10, dr["PresentReading"].ToString());
                                                        cells.Add(CustomersRowNo, 11, dr["Usage"].ToString());
                                                        cells.Add(CustomersRowNo, 12, dr["CreatedDate"].ToString());
                                                        cells.Add(CustomersRowNo, 13, dr["CreatedBy"].ToString());
                                                        cells.Add(CustomersRowNo, 14, dr["ReadDate"].ToString());

                                                        CustomersRowNo++;
                                                    }
                                                    DetailsRowNo = CustomersRowNo + 2;
                                                    CustomersRowNo = CustomersRowNo + 6;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                        fileName = Constants.PreBilling_NonReadCustomersReport_Name.Replace(" ", string.Empty) + "_" + fileName;
                        string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                        xls.FileName = filePath;
                        xls.Save();
                        _objIdsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                    }
                }
                else
                {
                    Message(Resource.NO_NONREAD_CUST, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            else
            {
                Message(Resource.NO_NONREAD_CUST, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        protected void btnGetNoBillCustomers_Click(object sender, EventArgs e)
        {
            RptPreBillingBe _objPreBillingBe = new RptPreBillingBe();
            RptPreBillingListBe _objPreBillingListBe = new RptPreBillingListBe();
            _objPreBillingBe.BU_ID = BusinessUnitName;
            _objPreBillingBe.SU_ID = ServiceUnitName;
            _objPreBillingBe.ServiceCenterId = ServiceCenterName;
            DataSet ds = new DataSet();
            ds = _objReportsBal.GetPreBillingReports(_objPreBillingBe, ReturnType.Data);

            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    var BUlist = (from list in ds.Tables[0].AsEnumerable()
                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                  group list by list.Field<string>("BusinessUnitName") into g
                                  select new { BusinessUnit = g.Key }).ToList();

                    if (BUlist.Count() > 0)
                    {
                        XlsDocument xls = new XlsDocument();
                        Worksheet mainSheet = xls.Workbook.Worksheets.Add("Summary");
                        Cells mainCells = mainSheet.Cells;
                        Cell _objMainCells = null;


                        _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
                        mainCells.Add(4, 2, "Report Code ");
                        mainCells.Add(4, 3, Constants.PreBilling_NoBilledCustomersReport_Code).Font.Bold = true;
                        mainCells.Add(4, 4, "Report Name");
                        mainCells.Add(4, 5, Constants.PreBilling_NoBilledCustomersReport_Name).Font.Bold = true;
                        mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
                        _objMainCells.Font.Weight = FontWeight.ExtraBold;
                        _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                        _objMainCells.Font.FontFamily = FontFamilies.Roman;

                        int MainSheetDetailsRowNo = 6;
                        mainCells.Add(MainSheetDetailsRowNo, 1, "S.No.").Font.Bold = true;
                        mainCells.Add(MainSheetDetailsRowNo, 2, "Service Center").Font.Bold = true;
                        mainCells.Add(MainSheetDetailsRowNo, 3, "Customer Count").Font.Bold = true;

                        var SClist = (from list in ds.Tables[0].AsEnumerable()
                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                      group list by list.Field<string>("ServiceCenterName") into g
                                      select new { ServiceCenter = g.Key }).ToList();

                        for (int k = 0; k < SClist.Count; k++)//SC
                        {
                            var Customers = (from list in ds.Tables[0].AsEnumerable()
                                             where list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                             select list).ToList();

                            MainSheetDetailsRowNo++;
                            mainCells.Add(MainSheetDetailsRowNo, 1, k + 1);
                            mainCells.Add(MainSheetDetailsRowNo, 2, SClist[k].ServiceCenter);
                            mainCells.Add(MainSheetDetailsRowNo, 3, Customers.Count);
                        }

                        for (int i = 0; i < BUlist.Count; i++)//BU
                        {
                            var SUlistByBU = (from list in ds.Tables[0].AsEnumerable()
                                              where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                              orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                              orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                              group list by list.Field<string>("ServiceUnitName") into g
                                              select new { ServiceUnitName = g.Key }).ToList();

                            for (int j = 0; j < SUlistByBU.Count; j++)//SU
                            {
                                var SClistBySU = (from list in ds.Tables[0].AsEnumerable()
                                                  where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                  && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                  group list by list.Field<string>("ServiceCenterName") into g
                                                  select new { ServiceCenter = g.Key }).ToList();

                                for (int k = 0; k < SClistBySU.Count; k++)//SC
                                {

                                    var CyclelistBySC = (from list in ds.Tables[0].AsEnumerable()
                                                         where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                         && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                         && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                         orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                         orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                         group list by list.Field<string>("CycleName") into g
                                                         select new { CycleName = g.Key }).ToList();

                                    if (CyclelistBySC.Count > 0)
                                    {
                                        int DetailsRowNo = 2;
                                        int CustomersRowNo = 6;

                                        Worksheet sheet = xls.Workbook.Worksheets.Add(SClistBySU[k].ServiceCenter);
                                        Cells cells = sheet.Cells;
                                        Cell _objCell = null;

                                        for (int l = 0; l < CyclelistBySC.Count; l++)//Cycle
                                        {
                                            var BooklistByCycle = (from list in ds.Tables[0].AsEnumerable()
                                                                   where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                   && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                   && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                   && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                   orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                   orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                   group list by list.Field<string>("BookNumber") into g
                                                                   select new { BookNumber = g.Key }).ToList();

                                            for (int m = 0; m < BooklistByCycle.Count; m++)//Book
                                            {
                                                var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                                 where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                 && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                 && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                 && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                 && list.Field<string>("BookNumber") == BooklistByCycle[m].BookNumber
                                                                 orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                 orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                 orderby Convert.ToString(list["CycleName"]) ascending
                                                                 select list).ToList();

                                                if (Customers.Count() > 0)
                                                {
                                                    _objCell = cells.Add(DetailsRowNo, 1, "Business Unit");
                                                    cells.Add(DetailsRowNo, 3, "Service Unit");
                                                    cells.Add(DetailsRowNo, 5, "Service Center");
                                                    cells.Add(DetailsRowNo + 1, 1, "Book Group");
                                                    cells.Add(DetailsRowNo + 1, 3, "Book Code");

                                                    cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 4, SUlistByBU[j].ServiceUnitName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 6, SClistBySU[k].ServiceCenter).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 2, CyclelistBySC[l].CycleName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 4, BooklistByCycle[m].BookNumber).Font.Bold = true;


                                                    _objCell = cells.Add(DetailsRowNo + 3, 1, "Sno");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 2, "Global Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 3, "Old Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 4, "Meter Number");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 5, "Tariff");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 6, "Name");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 7, "Service Address");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 8, "Comments");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;

                                                    for (int n = 0; n < Customers.Count; n++)
                                                    {
                                                        DataRow dr = (DataRow)Customers[n];
                                                        cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                                                        cells.Add(CustomersRowNo, 2, dr["GlobalAccountNumber"].ToString());
                                                        cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
                                                        cells.Add(CustomersRowNo, 4, dr["MeterNo"].ToString());
                                                        cells.Add(CustomersRowNo, 5, dr["ClassName"].ToString());
                                                        cells.Add(CustomersRowNo, 6, dr["Name"].ToString());
                                                        cells.Add(CustomersRowNo, 7, dr["ServiceAddress"].ToString());
                                                        cells.Add(CustomersRowNo, 8, dr["Comments"].ToString());
                                                        CustomersRowNo++;
                                                    }
                                                    DetailsRowNo = CustomersRowNo + 2;
                                                    CustomersRowNo = CustomersRowNo + 6;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                        fileName = Constants.PreBilling_NoBilledCustomersReport_Name.Replace(" ", string.Empty) + "_" + fileName;
                        string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                        xls.FileName = filePath;
                        xls.Save();
                        _objIdsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                    }
                }
                else
                {
                    Message(Resource.NO_BILLED_CUST, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            else
            {
                Message(Resource.NO_BILLED_CUST, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        protected void btnGetZeroUsageCustomers_Click(object sender, EventArgs e)
        {
            RptPreBillingBe _objPreBillingBe = new RptPreBillingBe();
            RptPreBillingListBe _objPreBillingListBe = new RptPreBillingListBe();
            _objPreBillingBe.BU_ID = BusinessUnitName;
            _objPreBillingBe.SU_ID = ServiceUnitName;
            _objPreBillingBe.ServiceCenterId = ServiceCenterName;
            DataSet ds = new DataSet();
            ds = _objReportsBal.GetPreBillingReports(_objPreBillingBe, ReturnType.Check);

            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    var BUlist = (from list in ds.Tables[0].AsEnumerable()
                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                  group list by list.Field<string>("BusinessUnitName") into g
                                  select new { BusinessUnit = g.Key }).ToList();

                    if (BUlist.Count() > 0)
                    {
                        XlsDocument xls = new XlsDocument();
                        Worksheet mainSheet = xls.Workbook.Worksheets.Add("Summary");
                        Cells mainCells = mainSheet.Cells;
                        Cell _objMainCells = null;


                        _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
                        mainCells.Add(4, 2, "Report Code ");
                        mainCells.Add(4, 3, Constants.PreBilling_ZeroUsageCustomersReport_Code).Font.Bold = true;
                        mainCells.Add(4, 4, "Report Name");
                        mainCells.Add(4, 5, Constants.PreBilling_ZeroUsageCustomersReport_Name).Font.Bold = true;
                        mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
                        _objMainCells.Font.Weight = FontWeight.ExtraBold;
                        _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                        _objMainCells.Font.FontFamily = FontFamilies.Roman;

                        int MainSheetDetailsRowNo = 6;
                        mainCells.Add(MainSheetDetailsRowNo, 1, "S.No.").Font.Bold = true;
                        mainCells.Add(MainSheetDetailsRowNo, 2, "Service Center").Font.Bold = true;
                        mainCells.Add(MainSheetDetailsRowNo, 3, "Customer Count").Font.Bold = true;

                        var SClist = (from list in ds.Tables[0].AsEnumerable()
                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                      group list by list.Field<string>("ServiceCenterName") into g
                                      select new { ServiceCenter = g.Key }).ToList();

                        for (int k = 0; k < SClist.Count; k++)//SC
                        {
                            var Customers = (from list in ds.Tables[0].AsEnumerable()
                                             where list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                             select list).ToList();

                            MainSheetDetailsRowNo++;
                            mainCells.Add(MainSheetDetailsRowNo, 1, k + 1);
                            mainCells.Add(MainSheetDetailsRowNo, 2, SClist[k].ServiceCenter);
                            mainCells.Add(MainSheetDetailsRowNo, 3, Customers.Count);
                        }

                        for (int i = 0; i < BUlist.Count; i++)//BU
                        {
                            var SUlistByBU = (from list in ds.Tables[0].AsEnumerable()
                                              where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                              orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                              orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                              group list by list.Field<string>("ServiceUnitName") into g
                                              select new { ServiceUnitName = g.Key }).ToList();

                            for (int j = 0; j < SUlistByBU.Count; j++)//SU
                            {
                                var SClistBySU = (from list in ds.Tables[0].AsEnumerable()
                                                  where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                  && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                  group list by list.Field<string>("ServiceCenterName") into g
                                                  select new { ServiceCenter = g.Key }).ToList();

                                for (int k = 0; k < SClistBySU.Count; k++)//SC
                                {

                                    var CyclelistBySC = (from list in ds.Tables[0].AsEnumerable()
                                                         where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                         && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                         && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                         orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                         orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                         group list by list.Field<string>("CycleName") into g
                                                         select new { CycleName = g.Key }).ToList();

                                    if (CyclelistBySC.Count > 0)
                                    {
                                        int DetailsRowNo = 2;
                                        int CustomersRowNo = 6;

                                        Worksheet sheet = xls.Workbook.Worksheets.Add(SClistBySU[k].ServiceCenter);
                                        Cells cells = sheet.Cells;
                                        Cell _objCell = null;

                                        for (int l = 0; l < CyclelistBySC.Count; l++)//Cycle
                                        {
                                            var BooklistByCycle = (from list in ds.Tables[0].AsEnumerable()
                                                                   where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                   && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                   && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                   && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                   orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                   orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                   group list by list.Field<string>("BookNumber") into g
                                                                   select new { BookNumber = g.Key }).ToList();

                                            for (int m = 0; m < BooklistByCycle.Count; m++)//Book
                                            {
                                                var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                                 where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                 && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                 && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                 && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                 && list.Field<string>("BookNumber") == BooklistByCycle[m].BookNumber
                                                                 orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                 orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                 orderby Convert.ToString(list["CycleName"]) ascending
                                                                 select list).ToList();

                                                if (Customers.Count() > 0)
                                                {
                                                    _objCell = cells.Add(DetailsRowNo, 1, "Business Unit");
                                                    cells.Add(DetailsRowNo, 3, "Service Unit");
                                                    cells.Add(DetailsRowNo, 5, "Service Center");
                                                    cells.Add(DetailsRowNo + 1, 1, "Book Group");
                                                    cells.Add(DetailsRowNo + 1, 3, "Book Code");

                                                    cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 4, SUlistByBU[j].ServiceUnitName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 6, SClistBySU[k].ServiceCenter).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 2, CyclelistBySC[l].CycleName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 4, BooklistByCycle[m].BookNumber).Font.Bold = true;


                                                    _objCell = cells.Add(DetailsRowNo + 3, 1, "Sno");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 2, "Global Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 3, "Old Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 4, "Tariff");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 5, "Name");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 6, "Service Address");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;

                                                    for (int n = 0; n < Customers.Count; n++)
                                                    {
                                                        DataRow dr = (DataRow)Customers[n];
                                                        cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                                                        cells.Add(CustomersRowNo, 2, dr["GlobalAccountNumber"].ToString());
                                                        cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
                                                        cells.Add(CustomersRowNo, 4, dr["ClassName"].ToString());
                                                        cells.Add(CustomersRowNo, 5, dr["Name"].ToString());
                                                        cells.Add(CustomersRowNo, 6, dr["ServiceAddress"].ToString());
                                                        CustomersRowNo++;
                                                    }
                                                    DetailsRowNo = CustomersRowNo + 2;
                                                    CustomersRowNo = CustomersRowNo + 6;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                        fileName = Constants.PreBilling_ZeroUsageCustomersReport_Name.Replace(" ", string.Empty) + "_" + fileName;
                        string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                        xls.FileName = filePath;
                        xls.Save();
                        _objIdsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                    }
                }
                else
                {
                    Message(Resource.NO_ZERO_USAGED_CUST, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            else
            {
                Message(Resource.NO_ZERO_USAGED_CUST, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }        

        protected void btnGetEstimatedCustomers_Click(object sender, EventArgs e)
        {
            try
            {
                RptPreBillingBe _objPreBillingBe = new RptPreBillingBe();
                RptPreBillingListBe _objPreBillingListBe = new RptPreBillingListBe();
                _objPreBillingBe.BU_ID = BusinessUnitName;
                _objPreBillingBe.SU_ID = ServiceUnitName;
                _objPreBillingBe.ServiceCenterId = ServiceCenterName;
                DataSet ds = new DataSet();
                ds = _objReportsBal.GetPreBillingReports(_objPreBillingBe, ReturnType.Double);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    var BUlist = (from list in ds.Tables[0].AsEnumerable()
                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                  group list by list.Field<string>("BusinessUnitName") into g
                                  select new { BusinessUnit = g.Key }).ToList();

                    if (BUlist.Count() > 0)
                    {
                        XlsDocument xls = new XlsDocument();
                        Worksheet mainSheet = xls.Workbook.Worksheets.Add("Summary");
                        Cells mainCells = mainSheet.Cells;
                        Cell _objMainCells = null;

                        _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
                        mainCells.Add(4, 2, "Report Code ");
                        mainCells.Add(4, 3, Constants.PreBilling_High_LowEstimatedCustomersReport_Code).Font.Bold = true;
                        mainCells.Add(4, 4, "Report Name");
                        mainCells.Add(4, 5, Constants.PreBilling_High_LowEstimatedCustomersReport_Name).Font.Bold = true;
                        mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
                        _objMainCells.Font.Weight = FontWeight.ExtraBold;
                        _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                        _objMainCells.Font.FontFamily = FontFamilies.Roman;

                        int MainSheetDetailsRowNo = 6;
                        _objMainCells = mainCells.Add(MainSheetDetailsRowNo, 1, "S.No.");
                        _objMainCells.Font.Bold = true;
                        _objMainCells.UseBorder = true;
                        _objMainCells = mainCells.Add(MainSheetDetailsRowNo, 2, "Service Center");
                        _objMainCells.Font.Bold = true;
                        _objMainCells.UseBorder = true;
                        _objMainCells = mainCells.Add(MainSheetDetailsRowNo, 3, "Customer Count");
                        _objMainCells.Font.Bold = true;
                        _objMainCells.UseBorder = true;

                        var SClist = (from list in ds.Tables[0].AsEnumerable()
                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                      group list by list.Field<string>("ServiceCenterName") into g
                                      select new { ServiceCenter = g.Key }).ToList();

                        for (int k = 0; k < SClist.Count; k++)//SC
                        {
                            var Customers = (from list in ds.Tables[0].AsEnumerable()
                                             where list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                             select list).ToList();

                            MainSheetDetailsRowNo++;
                            mainCells.Add(MainSheetDetailsRowNo, 1, k + 1).UseBorder = true;
                            mainCells.Add(MainSheetDetailsRowNo, 2, SClist[k].ServiceCenter).UseBorder = true;
                            mainCells.Add(MainSheetDetailsRowNo, 3, Customers.Count).UseBorder = true;
                        }


                        for (int i = 0; i < BUlist.Count; i++)//BU
                        {
                            var SUlistByBU = (from list in ds.Tables[0].AsEnumerable()
                                              where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                              orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                              orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                              group list by list.Field<string>("ServiceUnitName") into g
                                              select new { ServiceUnitName = g.Key }).ToList();

                            for (int j = 0; j < SUlistByBU.Count; j++)//SU
                            {
                                var SClistBySU = (from list in ds.Tables[0].AsEnumerable()
                                                  where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                  && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                  group list by list.Field<string>("ServiceCenterName") into g
                                                  select new { ServiceCenter = g.Key }).ToList();

                                for (int k = 0; k < SClistBySU.Count; k++)//SC
                                {
                                    var CyclelistBySC = (from list in ds.Tables[0].AsEnumerable()
                                                         where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                         && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                         && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                         orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                         orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                         group list by list.Field<string>("CycleName") into g
                                                         select new { CycleName = g.Key }).ToList();

                                    if (CyclelistBySC.Count > 0)
                                    {
                                        int DetailsRowNo = 2;
                                        int CustomersRowNo = 6;

                                        Worksheet sheet = xls.Workbook.Worksheets.Add(SClistBySU[k].ServiceCenter);
                                        Cells cells = sheet.Cells;
                                        Cell _objCell = null;

                                        for (int l = 0; l < CyclelistBySC.Count; l++)//Cycle
                                        {
                                            var BooklistByCycle = (from list in ds.Tables[0].AsEnumerable()
                                                                   where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                   && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                   && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                   && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                   orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                   orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                   group list by list.Field<string>("BookNumber") into g
                                                                   select new { BookNumber = g.Key }).ToList();

                                            for (int m = 0; m < BooklistByCycle.Count; m++)//Book
                                            {
                                                var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                                 where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                 && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                 && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                 && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                 && list.Field<string>("BookNumber") == BooklistByCycle[m].BookNumber
                                                                 orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                 orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                 orderby Convert.ToString(list["CycleName"]) ascending
                                                                 select list).ToList();

                                                if (Customers.Count() > 0)
                                                {

                                                    _objCell = cells.Add(DetailsRowNo, 1, "Business Unit");
                                                    cells.Add(DetailsRowNo, 3, "Service Unit");
                                                    cells.Add(DetailsRowNo, 5, "Service Center");
                                                    cells.Add(DetailsRowNo + 1, 1, "Book Group");
                                                    cells.Add(DetailsRowNo + 1, 3, "Book Number");

                                                    cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 4, SUlistByBU[j].ServiceUnitName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 6, SClistBySU[k].ServiceCenter).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 2, CyclelistBySC[l].CycleName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 4, BooklistByCycle[m].BookNumber).Font.Bold = true;

                                                    _objCell = cells.Add(DetailsRowNo + 3, 1, "Sno");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 2, "Global Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 3, "Old Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 4, "Meter Number");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 5, "Tariff");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 6, "Name");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 7, "Service Address");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 8, "Previous Reading");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 9, "Previous Read Date");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 10, "Present Reading");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 11, "Usage");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 12, "Total Readings");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 13, "Last Transaction Date");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 14, "Transaction By");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;

                                                    for (int n = 0; n < Customers.Count; n++)
                                                    {
                                                        DataRow dr = (DataRow)Customers[n];
                                                        cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                                                        cells.Add(CustomersRowNo, 2, dr["GlobalAccountNumber"].ToString());
                                                        cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
                                                        cells.Add(CustomersRowNo, 4, dr["MeterNo"].ToString());
                                                        cells.Add(CustomersRowNo, 5, dr["ClassName"].ToString());
                                                        cells.Add(CustomersRowNo, 6, dr["Name"].ToString());
                                                        cells.Add(CustomersRowNo, 7, dr["ServiceAddress"].ToString());
                                                        cells.Add(CustomersRowNo, 8, dr["PreviousReading"].ToString());
                                                        cells.Add(CustomersRowNo, 9, dr["PreviousReadDate"].ToString());
                                                        cells.Add(CustomersRowNo, 10, dr["PresentReading"].ToString());
                                                        cells.Add(CustomersRowNo, 11, dr["Usage"].ToString());
                                                        cells.Add(CustomersRowNo, 12, dr["TotalReadings"].ToString());
                                                        cells.Add(CustomersRowNo, 13, dr["LatestTransactionDate"].ToString());
                                                        cells.Add(CustomersRowNo, 14, dr["TransactionLog"].ToString());

                                                        CustomersRowNo++;
                                                    }
                                                    DetailsRowNo = CustomersRowNo + 2;
                                                    CustomersRowNo = CustomersRowNo + 6;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                        fileName = Constants.PreBilling_High_LowEstimatedCustomersReport_Name.Replace(" ", string.Empty) + "_" + fileName;
                        string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                        xls.FileName = filePath;
                        xls.Save();
                        _objIdsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                    }
                    else
                        Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                    Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnPartialBillCustomers_Click(object sender, EventArgs e)
        {
            RptPreBillingBe _objPreBillingBe = new RptPreBillingBe();
            RptPreBillingListBe _objPreBillingListBe = new RptPreBillingListBe();
            _objPreBillingBe.BU_ID = BusinessUnitName;
            _objPreBillingBe.SU_ID = ServiceUnitName;
            _objPreBillingBe.ServiceCenterId = ServiceCenterName;
            DataSet ds = new DataSet();
            ds = _objReportsBal.GetPreBillingReports(_objPreBillingBe, ReturnType.Fetch);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    var BUlist = (from list in ds.Tables[0].AsEnumerable()
                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                  group list by list.Field<string>("BusinessUnitName") into g
                                  select new { BusinessUnit = g.Key }).ToList();

                    if (BUlist.Count() > 0)
                    {
                        XlsDocument xls = new XlsDocument();
                        Worksheet mainSheet = xls.Workbook.Worksheets.Add("Summary");
                        Cells mainCells = mainSheet.Cells;
                        Cell _objMainCells = null;


                        _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
                        mainCells.Add(4, 2, "Report Code ");
                        mainCells.Add(4, 3, Constants.PreBilling_PartialBillCustomersReport_Code).Font.Bold = true;
                        mainCells.Add(4, 4, "Report Name");
                        mainCells.Add(4, 5, Constants.PreBilling_PartialBillCustomersReport_Name).Font.Bold = true;
                        mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
                        _objMainCells.Font.Weight = FontWeight.ExtraBold;
                        _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                        _objMainCells.Font.FontFamily = FontFamilies.Roman;

                        int MainSheetDetailsRowNo = 6;
                        mainCells.Add(MainSheetDetailsRowNo, 1, "S.No.").Font.Bold = true;
                        mainCells.Add(MainSheetDetailsRowNo, 2, "Service Center").Font.Bold = true;
                        mainCells.Add(MainSheetDetailsRowNo, 3, "Customer Count").Font.Bold = true;

                        var SClist = (from list in ds.Tables[0].AsEnumerable()
                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                      group list by list.Field<string>("ServiceCenterName") into g
                                      select new { ServiceCenter = g.Key }).ToList();

                        for (int k = 0; k < SClist.Count; k++)//SC
                        {
                            var Customers = (from list in ds.Tables[0].AsEnumerable()
                                             where list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                             select list).ToList();

                            MainSheetDetailsRowNo++;
                            mainCells.Add(MainSheetDetailsRowNo, 1, k + 1);
                            mainCells.Add(MainSheetDetailsRowNo, 2, SClist[k].ServiceCenter);
                            mainCells.Add(MainSheetDetailsRowNo, 3, Customers.Count);
                        }

                        for (int i = 0; i < BUlist.Count; i++)//BU
                        {
                            var SUlistByBU = (from list in ds.Tables[0].AsEnumerable()
                                              where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                              orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                              orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                              group list by list.Field<string>("ServiceUnitName") into g
                                              select new { ServiceUnitName = g.Key }).ToList();

                            for (int j = 0; j < SUlistByBU.Count; j++)//SU
                            {
                                var SClistBySU = (from list in ds.Tables[0].AsEnumerable()
                                                  where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                  && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                  group list by list.Field<string>("ServiceCenterName") into g
                                                  select new { ServiceCenter = g.Key }).ToList();

                                for (int k = 0; k < SClistBySU.Count; k++)//SC
                                {

                                    var CyclelistBySC = (from list in ds.Tables[0].AsEnumerable()
                                                         where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                         && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                         && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                         orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                         orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                         group list by list.Field<string>("CycleName") into g
                                                         select new { CycleName = g.Key }).ToList();

                                    if (CyclelistBySC.Count > 0)
                                    {
                                        int DetailsRowNo = 2;
                                        int CustomersRowNo = 6;

                                        Worksheet sheet = xls.Workbook.Worksheets.Add(SClistBySU[k].ServiceCenter);
                                        Cells cells = sheet.Cells;
                                        Cell _objCell = null;

                                        for (int l = 0; l < CyclelistBySC.Count; l++)//Cycle
                                        {
                                            var BooklistByCycle = (from list in ds.Tables[0].AsEnumerable()
                                                                   where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                   && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                   && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                   && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                   orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                   orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                   group list by list.Field<string>("BookNumber") into g
                                                                   select new { BookNumber = g.Key }).ToList();

                                            for (int m = 0; m < BooklistByCycle.Count; m++)//Book
                                            {
                                                var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                                 where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                 && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                 && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                 && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                 && list.Field<string>("BookNumber") == BooklistByCycle[m].BookNumber
                                                                 orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                 orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                 orderby Convert.ToString(list["CycleName"]) ascending
                                                                 select list).ToList();

                                                if (Customers.Count() > 0)
                                                {
                                                    _objCell = cells.Add(DetailsRowNo, 1, "Business Unit");
                                                    cells.Add(DetailsRowNo, 3, "Service Unit");
                                                    cells.Add(DetailsRowNo, 5, "Service Center");
                                                    cells.Add(DetailsRowNo + 1, 1, "Book Group");
                                                    cells.Add(DetailsRowNo + 1, 3, "Book Code");

                                                    cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 4, SUlistByBU[j].ServiceUnitName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 6, SClistBySU[k].ServiceCenter).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 2, CyclelistBySC[l].CycleName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 4, BooklistByCycle[m].BookNumber).Font.Bold = true;


                                                    _objCell = cells.Add(DetailsRowNo + 3, 1, "Sno");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 2, "Global Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 3, "Old Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 4, "Meter Number");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 5, "Tariff");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 6, "Name");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 7, "Service Address");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 8, "Comments");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;

                                                    for (int n = 0; n < Customers.Count; n++)
                                                    {
                                                        DataRow dr = (DataRow)Customers[n];
                                                        cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                                                        cells.Add(CustomersRowNo, 2, dr["GlobalAccountNumber"].ToString());
                                                        cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
                                                        cells.Add(CustomersRowNo, 4, dr["MeterNo"].ToString());
                                                        cells.Add(CustomersRowNo, 5, dr["ClassName"].ToString());
                                                        cells.Add(CustomersRowNo, 6, dr["Name"].ToString());
                                                        cells.Add(CustomersRowNo, 7, dr["ServiceAddress"].ToString());
                                                        cells.Add(CustomersRowNo, 8, dr["Comments"].ToString());

                                                        CustomersRowNo++;
                                                    }
                                                    DetailsRowNo = CustomersRowNo + 2;
                                                    CustomersRowNo = CustomersRowNo + 6;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                        fileName = Constants.PreBilling_PartialBillCustomersReport_Name.Replace(" ", string.Empty) + "_" + fileName;
                        string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                        xls.FileName = filePath;
                        xls.Save();
                        _objIdsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                    }
                }
                else
                {
                    Message(Resource.NO_PARTIAL_BILLED_CUST, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            else
            {
                Message(Resource.NO_PARTIAL_BILLED_CUST, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        protected void btnAvgNotUploadCustomers_Click(object sender, EventArgs e)
        {
            RptPreBillingBe _objPreBillingBe = new RptPreBillingBe();
            RptPreBillingListBe _objPreBillingListBe = new RptPreBillingListBe();
            _objPreBillingBe.BU_ID = BusinessUnitName;
            _objPreBillingBe.SU_ID = ServiceUnitName;
            _objPreBillingBe.ServiceCenterId = ServiceCenterName;
            DataSet ds = new DataSet();
            ds = _objReportsBal.GetPreBillingReports(_objPreBillingBe, ReturnType.Group);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    var BUlist = (from list in ds.Tables[0].AsEnumerable()
                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                  group list by list.Field<string>("BusinessUnitName") into g
                                  select new { BusinessUnit = g.Key }).ToList();

                    if (BUlist.Count() > 0)
                    {
                        XlsDocument xls = new XlsDocument();
                        Worksheet mainSheet = xls.Workbook.Worksheets.Add("Summary");
                        Cells mainCells = mainSheet.Cells;
                        Cell _objMainCells = null;


                        _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
                        mainCells.Add(4, 2, "Report Code ");
                        mainCells.Add(4, 3, Constants.PreBilling_AvgNotUploadedCustomersReport_Code).Font.Bold = true;
                        mainCells.Add(4, 4, "Report Name");
                        mainCells.Add(4, 5, Constants.PreBilling_AvgNotUploadedCustomersReport_Name).Font.Bold = true;
                        mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
                        _objMainCells.Font.Weight = FontWeight.ExtraBold;
                        _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                        _objMainCells.Font.FontFamily = FontFamilies.Roman;

                        int MainSheetDetailsRowNo = 6;
                        mainCells.Add(MainSheetDetailsRowNo, 1, "S.No.").Font.Bold = true;
                        mainCells.Add(MainSheetDetailsRowNo, 2, "Service Center").Font.Bold = true;
                        mainCells.Add(MainSheetDetailsRowNo, 3, "Customer Count").Font.Bold = true;

                        var SClist = (from list in ds.Tables[0].AsEnumerable()
                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                      group list by list.Field<string>("ServiceCenterName") into g
                                      select new { ServiceCenter = g.Key }).ToList();

                        for (int k = 0; k < SClist.Count; k++)//SC
                        {
                            var Customers = (from list in ds.Tables[0].AsEnumerable()
                                             where list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                             select list).ToList();

                            MainSheetDetailsRowNo++;
                            mainCells.Add(MainSheetDetailsRowNo, 1, k + 1);
                            mainCells.Add(MainSheetDetailsRowNo, 2, SClist[k].ServiceCenter);
                            mainCells.Add(MainSheetDetailsRowNo, 3, Customers.Count);
                        }

                        for (int i = 0; i < BUlist.Count; i++)//BU
                        {
                            var SUlistByBU = (from list in ds.Tables[0].AsEnumerable()
                                              where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                              orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                              orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                              group list by list.Field<string>("ServiceUnitName") into g
                                              select new { ServiceUnitName = g.Key }).ToList();

                            for (int j = 0; j < SUlistByBU.Count; j++)//SU
                            {
                                var SClistBySU = (from list in ds.Tables[0].AsEnumerable()
                                                  where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                  && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                  group list by list.Field<string>("ServiceCenterName") into g
                                                  select new { ServiceCenter = g.Key }).ToList();

                                for (int k = 0; k < SClistBySU.Count; k++)//SC
                                {

                                    var CyclelistBySC = (from list in ds.Tables[0].AsEnumerable()
                                                         where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                         && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                         && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                         orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                         orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                         group list by list.Field<string>("CycleName") into g
                                                         select new { CycleName = g.Key }).ToList();

                                    if (CyclelistBySC.Count > 0)
                                    {
                                        int DetailsRowNo = 2;
                                        int CustomersRowNo = 6;

                                        Worksheet sheet = xls.Workbook.Worksheets.Add(SClistBySU[k].ServiceCenter);
                                        Cells cells = sheet.Cells;
                                        Cell _objCell = null;

                                        for (int l = 0; l < CyclelistBySC.Count; l++)//Cycle
                                        {
                                            var BooklistByCycle = (from list in ds.Tables[0].AsEnumerable()
                                                                   where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                   && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                   && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                   && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                   orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                   orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                   group list by list.Field<string>("BookNumber") into g
                                                                   select new { BookNumber = g.Key }).ToList();

                                            for (int m = 0; m < BooklistByCycle.Count; m++)//Book
                                            {
                                                var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                                 where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                 && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                 && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                 && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                 && list.Field<string>("BookNumber") == BooklistByCycle[m].BookNumber
                                                                 orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                 orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                 orderby Convert.ToString(list["CycleName"]) ascending
                                                                 select list).ToList();

                                                if (Customers.Count() > 0)
                                                {
                                                    _objCell = cells.Add(DetailsRowNo, 1, "Business Unit");
                                                    cells.Add(DetailsRowNo, 3, "Service Unit");
                                                    cells.Add(DetailsRowNo, 5, "Service Center");
                                                    cells.Add(DetailsRowNo + 1, 1, "Book Group");
                                                    cells.Add(DetailsRowNo + 1, 3, "Book Code");

                                                    cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 4, SUlistByBU[j].ServiceUnitName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 6, SClistBySU[k].ServiceCenter).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 2, CyclelistBySC[l].CycleName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 4, BooklistByCycle[m].BookNumber).Font.Bold = true;


                                                    _objCell = cells.Add(DetailsRowNo + 3, 1, "Sno");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 2, "Global Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 3, "Old Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 4, "Meter Number");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 5, "Tariff");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 6, "Name");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 7, "Service Address");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 8, "Initial Billing kWh");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;

                                                    for (int n = 0; n < Customers.Count; n++)
                                                    {
                                                        DataRow dr = (DataRow)Customers[n];
                                                        cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                                                        cells.Add(CustomersRowNo, 2, dr["GlobalAccountNumber"].ToString());
                                                        cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
                                                        cells.Add(CustomersRowNo, 4, dr["MeterNo"].ToString());
                                                        cells.Add(CustomersRowNo, 5, dr["ClassName"].ToString());
                                                        cells.Add(CustomersRowNo, 6, dr["Name"].ToString());
                                                        cells.Add(CustomersRowNo, 7, dr["ServiceAddress"].ToString());
                                                        cells.Add(CustomersRowNo, 8, dr["InitialBillingKWh"].ToString());

                                                        CustomersRowNo++;
                                                    }
                                                    DetailsRowNo = CustomersRowNo + 2;
                                                    CustomersRowNo = CustomersRowNo + 6;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                        fileName = Constants.PreBilling_AvgNotUploadedCustomersReport_Name.Replace(" ", string.Empty) + "_" + fileName;
                        string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                        xls.FileName = filePath;
                        xls.Save();
                        _objIdsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                    }
                }
                else
                {
                    Message(Resource.NO_AVG_UPLOADED_CUST, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            else
            {
                Message(Resource.NO_AVG_UPLOADED_CUST, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        protected void btnDirectCustomeresAvgDetails_Click(object sender, EventArgs e)
        {
            RptPreBillingBe _objPreBillingBe = new RptPreBillingBe();
            RptPreBillingListBe _objPreBillingListBe = new RptPreBillingListBe();
            _objPreBillingBe.BU_ID = BusinessUnitName;
            _objPreBillingBe.SU_ID = ServiceUnitName;
            _objPreBillingBe.ServiceCenterId = ServiceCenterName;
            DataSet ds = new DataSet();
            ds = _objReportsBal.GetPreBillingReports(_objPreBillingBe, ReturnType.List);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    var BUlist = (from list in ds.Tables[0].AsEnumerable()
                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                  group list by list.Field<string>("BusinessUnitName") into g
                                  select new { BusinessUnit = g.Key }).ToList();

                    if (BUlist.Count() > 0)
                    {
                        XlsDocument xls = new XlsDocument();
                        Worksheet mainSheet = xls.Workbook.Worksheets.Add("Summary");
                        Cells mainCells = mainSheet.Cells;
                        Cell _objMainCells = null;


                        _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
                        mainCells.Add(4, 2, "Report Code ");
                        mainCells.Add(4, 3, Constants.PreBilling_AvgUploadedCustomersReport_Code).Font.Bold = true;
                        mainCells.Add(4, 4, "Report Name");
                        mainCells.Add(4, 5, Constants.PreBilling_AvgUploadedCustomersReport_Name).Font.Bold = true;
                        mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
                        _objMainCells.Font.Weight = FontWeight.ExtraBold;
                        _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                        _objMainCells.Font.FontFamily = FontFamilies.Roman;

                        int MainSheetDetailsRowNo = 6;
                        mainCells.Add(MainSheetDetailsRowNo, 1, "S.No.").Font.Bold = true;
                        mainCells.Add(MainSheetDetailsRowNo, 2, "Service Center").Font.Bold = true;
                        mainCells.Add(MainSheetDetailsRowNo, 3, "Customer Count").Font.Bold = true;

                        var SClist = (from list in ds.Tables[0].AsEnumerable()
                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                      group list by list.Field<string>("ServiceCenterName") into g
                                      select new { ServiceCenter = g.Key }).ToList();

                        for (int k = 0; k < SClist.Count; k++)//SC
                        {
                            var Customers = (from list in ds.Tables[0].AsEnumerable()
                                             where list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                             select list).ToList();

                            MainSheetDetailsRowNo++;
                            mainCells.Add(MainSheetDetailsRowNo, 1, k + 1);
                            mainCells.Add(MainSheetDetailsRowNo, 2, SClist[k].ServiceCenter);
                            mainCells.Add(MainSheetDetailsRowNo, 3, Customers.Count);
                        }

                        for (int i = 0; i < BUlist.Count; i++)//BU
                        {
                            var SUlistByBU = (from list in ds.Tables[0].AsEnumerable()
                                              where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                              orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                              orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                              group list by list.Field<string>("ServiceUnitName") into g
                                              select new { ServiceUnitName = g.Key }).ToList();

                            for (int j = 0; j < SUlistByBU.Count; j++)//SU
                            {
                                var SClistBySU = (from list in ds.Tables[0].AsEnumerable()
                                                  where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                  && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                  group list by list.Field<string>("ServiceCenterName") into g
                                                  select new { ServiceCenter = g.Key }).ToList();

                                for (int k = 0; k < SClistBySU.Count; k++)//SC
                                {

                                    var CyclelistBySC = (from list in ds.Tables[0].AsEnumerable()
                                                         where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                         && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                         && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                         orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                         orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                         group list by list.Field<string>("CycleName") into g
                                                         select new { CycleName = g.Key }).ToList();

                                    if (CyclelistBySC.Count > 0)
                                    {
                                        int DetailsRowNo = 2;
                                        int CustomersRowNo = 6;

                                        Worksheet sheet = xls.Workbook.Worksheets.Add(SClistBySU[k].ServiceCenter);
                                        Cells cells = sheet.Cells;
                                        Cell _objCell = null;

                                        for (int l = 0; l < CyclelistBySC.Count; l++)//Cycle
                                        {
                                            var BooklistByCycle = (from list in ds.Tables[0].AsEnumerable()
                                                                   where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                   && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                   && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                   && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                   orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                   orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                   group list by list.Field<string>("BookNumber") into g
                                                                   select new { BookNumber = g.Key }).ToList();

                                            for (int m = 0; m < BooklistByCycle.Count; m++)//Book
                                            {
                                                var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                                 where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                 && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                 && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                 && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                 && list.Field<string>("BookNumber") == BooklistByCycle[m].BookNumber
                                                                 orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                 orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                 orderby Convert.ToString(list["CycleName"]) ascending
                                                                 select list).ToList();

                                                if (Customers.Count() > 0)
                                                {
                                                    _objCell = cells.Add(DetailsRowNo, 1, "Business Unit");
                                                    cells.Add(DetailsRowNo, 3, "Service Unit");
                                                    cells.Add(DetailsRowNo, 5, "Service Center");
                                                    cells.Add(DetailsRowNo + 1, 1, "Book Group");
                                                    cells.Add(DetailsRowNo + 1, 3, "Book Code");

                                                    cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 4, SUlistByBU[j].ServiceUnitName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 6, SClistBySU[k].ServiceCenter).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 2, CyclelistBySC[l].CycleName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 4, BooklistByCycle[m].BookNumber).Font.Bold = true;


                                                    _objCell = cells.Add(DetailsRowNo + 3, 1, "Sno");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 2, "Global Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 3, "Old Account No");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 4, "Usage");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 5, "Tariff");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 6, "Name");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 7, "Service Address");
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;

                                                    for (int n = 0; n < Customers.Count; n++)
                                                    {
                                                        DataRow dr = (DataRow)Customers[n];
                                                        cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                                                        cells.Add(CustomersRowNo, 2, dr["GlobalAccountNumber"].ToString());
                                                        cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
                                                        cells.Add(CustomersRowNo, 4, dr["Usage"].ToString());
                                                        cells.Add(CustomersRowNo, 5, dr["ClassName"].ToString());
                                                        cells.Add(CustomersRowNo, 6, dr["Name"].ToString());
                                                        cells.Add(CustomersRowNo, 7, dr["ServiceAddress"].ToString());

                                                        CustomersRowNo++;
                                                    }
                                                    DetailsRowNo = CustomersRowNo + 2;
                                                    CustomersRowNo = CustomersRowNo + 6;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                        fileName = Constants.PreBilling_AvgUploadedCustomersReport_Name.Replace(" ", string.Empty) + "_" + fileName;
                        string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                        xls.FileName = filePath;
                        xls.Save();
                        _objIdsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                    }
                }
            }
            else
            {
                Message(Resource.NO_DIRECT_CUST_AVGS, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }
        #endregion Events

        #region Methods
        private void GetOpenMonthDetails()
        {
            RptPreBillingBe _objPreBillingBe = new RptPreBillingBe();
            XmlDocument xmlResult = _objReportsBal.GetPreBillingDetails(_objPreBillingBe, ReturnType.Fetch);
            _objPreBillingBe = _objIdsignBal.DeserializeFromXml<RptPreBillingBe>(xmlResult);
            if (_objPreBillingBe != null)
            {
                divHide.Visible = true;
                lblMonthDetails.Text = _objPreBillingBe.NameOfMonth + " - " + _objPreBillingBe.Year;
                hfMonthDetails.Value = _objPreBillingBe.NameOfMonth + "_" + _objPreBillingBe.Year;
            }
            else
            {
                lblMonthDetails.Text = Resource.OPEN_MNTHS_NOT_AVAIL;
                divHide.Visible = false;
            }
        }

        //private void GetPreBillingDetails(int readCodeId)
        //{
        //    RptPreBillingBe _objPreBillingBe = new RptPreBillingBe();
        //    RptPreBillingListBe _objPreBillingListBe = new RptPreBillingListBe();
        //    _objPreBillingBe.ReadCodeId = readCodeId;
        //    //_objPreBillingBe.BU_ID = Convert.ToString(Session[UMS_Resource.SESSION_USER_BUID]);
        //    _objPreBillingBe.BU_ID = this.BusinessUnitName;
        //    _objPreBillingBe.SU_ID = this.ServiceUnitName;
        //    _objPreBillingBe.ServiceCenterId = this.ServiceCenterName;
        //    //XmlDocument xmlResult = _objReportsBal.GetPreBillingDetails(_objPreBillingBe, ReturnType.Get);
        //    XmlDocument xmlResult = new XmlDocument();
        //    if (readCodeId == (int)ReadCode.Read)
        //        xmlResult = _objReportsBal.GetPreBillingDetails(_objPreBillingBe, ReturnType.Retrieve);
        //    else if (readCodeId == (int)ReadCode.Direct)
        //        xmlResult = _objReportsBal.GetPreBillingDetails(_objPreBillingBe, ReturnType.Multiple);
        //    else if (readCodeId == (int)ReadCode.Estimate)
        //        xmlResult = _objReportsBal.GetPreBillingDetails(_objPreBillingBe, ReturnType.Bulk);

        //    _objPreBillingListBe = _objIdsignBal.DeserializeFromXml<RptPreBillingListBe>(xmlResult);


        //    XlsDocument xls = new XlsDocument();
        //    Worksheet sheet = xls.Workbook.Worksheets.Add("sheet");
        //    //state the name of the column headings 
        //    Cells cells = sheet.Cells;
        //    AddCelltoFile(cells, readCodeId, _objPreBillingListBe.items);

        //    string fileName = hfMonthDetails.Value + ".xls";
        //    if (string.IsNullOrEmpty(hfMonthDetails.Value))
        //        fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
        //    switch (readCodeId)
        //    {
        //        case (int)ReadCode.Read:
        //            fileName = ReadCode.Read.ToString() + "_" + fileName;
        //            break;
        //        case (int)ReadCode.Direct:
        //            fileName = ReadCode.Direct.ToString() + "_" + fileName;
        //            break;
        //        case (int)ReadCode.Estimate:
        //            fileName = ReadCode.Estimate.ToString() + "_" + fileName;
        //            break;
        //    }

        //    string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
        //    xls.FileName = filePath;
        //    if (File.Exists(filePath))
        //    {
        //        File.Delete(filePath);
        //    }
        //    xls.Save();
        //    _objIdsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
        //}

        //private void AddCelltoFile(Cells cells, int readCodeId, List<RptPreBillingBe> list)
        //{
        //    int i = 1;
        //    switch (readCodeId)
        //    {
        //        #region IF ReadCustomer
        //        case (int)ReadCode.Read:
        //            //cells.Add(1, 1, "GlobalAccountNumber");
        //            //cells.Add(1, 2, "Name");
        //            //cells.Add(1, 3, "MeterNumber");
        //            //cells.Add(1, 4, "OldAccountNo");
        //            //cells.Add(1, 5, "PreviousReading");
        //            //cells.Add(1, 6, "PresentReading");
        //            //cells.Add(1, 7, "PreviousReadDate");
        //            //cells.Add(1, 8, "ClassName");
        //            //cells.Add(1, 9, "ServiceAddress");
        //            //cells.Add(1, 10, "ReadBy");
        //            //cells.Add(1, 11, "CreatedBy");
        //            //cells.Add(1, 12, "CreatedDate");
        //            //foreach (RptPreBillingBe item in list)
        //            //{
        //            //    i++;
        //            //    cells.Add(i, 1, item.GlobalAccountNumber);
        //            //    cells.Add(i, 2, item.Name);
        //            //    cells.Add(i, 3, item.MeterNo);
        //            //    cells.Add(i, 4, item.OldAccountNo);
        //            //    cells.Add(i, 5, item.PreviousReading);
        //            //    cells.Add(i, 6, item.PresentReading);
        //            //    cells.Add(i, 7, item.PreviousReadDate);
        //            //    cells.Add(i, 8, item.ClassName);
        //            //    cells.Add(i, 9, item.ServiceAddress);
        //            //    cells.Add(i, 10, item.ReadBy);
        //            //    cells.Add(i, 11, item.CreatedBy);
        //            //    cells.Add(i, 12, item.CreatedDate);
        //            //}


        //            cells.Add(1, 1, "Global Account Number");
        //            cells.Add(1, 2, "Old Account No");
        //            cells.Add(1, 3, "Meter Number");
        //            cells.Add(1, 4, "Tariff");
        //            cells.Add(1, 5, "Name");
        //            cells.Add(1, 6, "Service Address");
        //            cells.Add(1, 7, "Previous Reading");
        //            cells.Add(1, 8, "Present Reading");
        //            cells.Add(1, 9, "Usage");
        //            cells.Add(1, 10, "Average Reading");
        //            cells.Add(1, 11, "Previous Read Date");
        //            cells.Add(1, 12, "Present Read Date");
        //            cells.Add(1, 13, "User (CreatedBy)");
        //            cells.Add(1, 14, "Transaction (CreatedDate)");
        //            //cells.Add(1, 10, "ReadBy");
        //            foreach (RptPreBillingBe item in list)
        //            {
        //                i++;
        //                cells.Add(i, 1, item.GlobalAccountNumber);
        //                cells.Add(i, 2, item.OldAccountNo);
        //                cells.Add(i, 3, item.MeterNo);
        //                cells.Add(i, 4, item.ClassName);
        //                cells.Add(i, 5, item.Name);
        //                cells.Add(i, 6, item.ServiceAddress);
        //                cells.Add(i, 7, item.PreviousReading);
        //                cells.Add(i, 8, item.PresentReading);
        //                cells.Add(i, 9, item.Usage);
        //                cells.Add(i, 10, item.AverageReading);
        //                cells.Add(i, 11, item.PreviousReadDate);
        //                cells.Add(i, 11, item.PresentReadDate);
        //                cells.Add(i, 12, item.CreatedBy);
        //                cells.Add(i, 13, item.CreatedDate);
        //            }

        //            break;
        //        #endregion
        //        case (int)ReadCode.Direct:
        //            cells.Add(1, 1, "Global Account Number");
        //            cells.Add(1, 2, "Old Account No");
        //            cells.Add(1, 3, "Meter Number");
        //            cells.Add(1, 4, "Tariff");
        //            cells.Add(1, 5, "Name");
        //            cells.Add(1, 6, "Service Address");
        //            cells.Add(1, 7, "Consumption (Usage)");
        //            cells.Add(1, 8, "User");
        //            cells.Add(1, 9, "Transactional Date");

        //            foreach (RptPreBillingBe item in list)
        //            {
        //                i++;
        //                cells.Add(i, 1, item.GlobalAccountNumber);
        //                cells.Add(i, 2, item.OldAccountNo);
        //                cells.Add(i, 3, item.MeterNo);
        //                cells.Add(i, 4, item.ClassName);
        //                cells.Add(i, 5, item.Name);
        //                cells.Add(i, 6, item.ServiceAddress);
        //                cells.Add(i, 7, item.Usage);
        //                cells.Add(i, 8, item.CreatedBy);
        //                cells.Add(i, 9, item.CreatedDate);
        //            }
        //            break;
        //        case (int)ReadCode.Estimate:
        //            cells.Add(1, 1, "Global Account Number");
        //            cells.Add(1, 2, "Old Account No");
        //            cells.Add(1, 3, "Meter Number");
        //            cells.Add(1, 4, "Tariff");
        //            cells.Add(1, 5, "Name");
        //            cells.Add(1, 6, "Service Address");
        //            cells.Add(1, 7, "Previous Reading");
        //            cells.Add(1, 8, "Average Reading");
        //            cells.Add(1, 9, "Previous Read Date");

        //            foreach (RptPreBillingBe item in list)
        //            {
        //                i++;
        //                cells.Add(i, 1, item.GlobalAccountNumber);
        //                cells.Add(i, 2, item.OldAccountNo);
        //                cells.Add(i, 3, item.MeterNo);
        //                cells.Add(i, 4, item.ClassName);
        //                cells.Add(i, 5, item.Name);
        //                cells.Add(i, 6, item.ServiceAddress);
        //                cells.Add(i, 7, item.PreviousReading);
        //                cells.Add(i, 8, item.AverageReading);
        //                cells.Add(i, 9, item.PreviousReadDate);
        //            }
        //            break;
        //    }
        //}

        //private void GetHighLowReadingsDetails()
        //{
        //    RptPreBillingBe _objPreBillingBe = new RptPreBillingBe();
        //    RptPreBillingListBe _objPreBillingListBe = new RptPreBillingListBe();
        //    //_objPreBillingBe.BU_ID = Convert.ToString(Session[UMS_Resource.SESSION_USER_BUID]);
        //    _objPreBillingBe.BU_ID = this.BusinessUnitName;
        //    _objPreBillingBe.SU_ID = this.ServiceUnitName;
        //    _objPreBillingBe.ServiceCenterId = this.ServiceCenterName;
        //    XmlDocument xmlResult = _objReportsBal.GetPreBillingDetails(_objPreBillingBe, ReturnType.Double);
        //    _objPreBillingListBe = _objIdsignBal.DeserializeFromXml<RptPreBillingListBe>(xmlResult);


        //    XlsDocument xls = new XlsDocument();
        //    Worksheet sheet = xls.Workbook.Worksheets.Add("sheet");
        //    //state the name of the column headings 
        //    Cells cells = sheet.Cells;
        //    int i = 1;

        //    cells.Add(1, 1, "GlobalAccountNumber");
        //    cells.Add(1, 2, "MeterNo");
        //    cells.Add(1, 3, "ReadDate");
        //    cells.Add(1, 4, "Usage");
        //    cells.Add(1, 5, "Average Reading");
        //    cells.Add(1, 6, "HighLow ReadDetails");
        //    cells.Add(1, 7, "Present Reading");
        //    cells.Add(1, 9, "ClassName");
        //    cells.Add(1, 10, "Name");
        //    cells.Add(1, 11, "OldAccountNo");
        //    cells.Add(1, 12, "ServiceAddress");

        //    foreach (RptPreBillingBe item in _objPreBillingListBe.items)
        //    {
        //        i++;
        //        cells.Add(i, 1, item.GlobalAccountNumber);
        //        cells.Add(i, 2, item.MeterNo);
        //        cells.Add(i, 3, item.PreviousReadDate);
        //        cells.Add(i, 4, item.Usage);
        //        cells.Add(i, 5, item.AverageReading);
        //        cells.Add(i, 6, item.HighLowReadDetails);
        //        cells.Add(i, 7, item.PresentReading);
        //        cells.Add(i, 7, item.PreviousReading);
        //        cells.Add(i, 8, item.MeterNo);
        //        cells.Add(i, 9, item.ClassName);
        //        cells.Add(i, 10, item.Name);
        //        cells.Add(i, 11, item.OldAccountNo);
        //        cells.Add(i, 12, item.ServiceAddress);
        //    }


        //    string fileName = hfMonthDetails.Value + ".xls";
        //    if (string.IsNullOrEmpty(hfMonthDetails.Value))
        //        fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";

        //    fileName = "HighLowReadings" + fileName;

        //    string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
        //    xls.FileName = filePath;
        //    if (File.Exists(filePath))
        //    {
        //        File.Delete(filePath);
        //    }
        //    xls.Save();
        //    _objIdsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");

        //    //string filePath = Server.MapPath(ConfigurationManager.AppSettings["BillGeneration"].ToString()) + fileName;

        //    //if ((System.IO.File.Exists(filePath)))
        //    //{
        //    //    System.IO.File.Delete(filePath);
        //    //}

        //    ////set up a filestream
        //    //xls.FileName = filePath;

        //    ////xls.FileName = Server.MapPath("Files/test.xls");
        //    //xls.Save();

        //    //System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
        //    //response.ClearContent();
        //    //response.Clear();
        //    //response.ContentType = "application//vnd.ms-excel";
        //    //response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + ";");
        //    //response.TransmitFile(filePath);
        //    //response.Flush();
        //    //response.End();
        //}

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void CheckBoxesJS()
        {
            ChkBUAll.Attributes.Add("onclick", "CheckAll('" + ChkBUAll.ClientID + "','" + cblBusinessUnits.ClientID + "')");
            ChkSUAll.Attributes.Add("onclick", "CheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            chkSCAll.Attributes.Add("onclick", "CheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");


            cblBusinessUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkBUAll.ClientID + "','" + cblBusinessUnits.ClientID + "')");
            cblServiceUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            cblServiceCenters.Attributes.Add("onclick", "UnCheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
        }

        #endregion Methods
    }
}