﻿<%@ Page Title=":: Customers Billing Statistics Info ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    AutoEventWireup="true" Theme="Green" CodeBehind="RptCustomersBillingStatisticsInfo.aspx.cs"
    Inherits="UMS_Nigeria.Reports.RptCustomersBillingStatisticsInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upnlReport" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litReportHeading" runat="server" Text="Customers Bill Statistics Info"></asp:Literal>
                        </div>
                        <div class="star_text" style="margin-right: 155px;">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="previous">
                        <a href="ReportsSummary.aspx">< < Back To Summary</a>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <label for="name">
                                <asp:Literal ID="litBusinessUnitName" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT_NAME%>"></asp:Literal>
                                <span class="span_star">*</span></label><div class="space">
                                </div>
                            <asp:DropDownList ID="ddlBusinessUnitName" TabIndex="1" onchange="DropDownlistOnChangelbl(this,'spanddlBusinessUnitName','Business Unit')"
                                runat="server" CssClass="text-box text-select">
                            </asp:DropDownList>
                            <div class="space">
                            </div>
                            <span id="spanddlBusinessUnitName" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <asp:Literal ID="litYear" runat="server" Text="<%$ Resources:Resource, YEAR%>"></asp:Literal><span
                                class="span_star">*</span><br />
                            <asp:DropDownList ID="ddlYear" runat="server" CssClass="text-box select_box" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <br />
                            <span id="spanddlYear" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box2">
                        <div class="text-inner">
                            <asp:Literal ID="litMonth" runat="server" Text="<%$ Resources:Resource, MONTH%>"></asp:Literal><span
                                class="span_star">*</span><br />
                            <asp:DropDownList ID="ddlMonth" runat="server" CssClass="text-box select_box">
                                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <br />
                            <span id="spanddlMonth" class="span_color"></span>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="space">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s"
                                OnClick="btnSearch_Click" runat="server" OnClientClick="return Validate();" />
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="space">
                    </div>
                    <div class="clear">
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server" visible="false">
                        <div align="right" class="fltr" runat="server" id="divExport">
                            <asp:Button ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" CssClass="exporttoexcelButton"
                                Text="" />
                        </div>
                        <div class="clear">
                        </div>
                        <div class="dot-line">
                        </div>
                        <div class="text_total">
                            <div class="text-headingReports">
                                <asp:Literal ID="ltrlReportNamewithBU" runat="server" Text=""></asp:Literal>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="dot-line">
                        </div>
                        <%--<asp:GridView ID="grdReport" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="reportsGrid_tb_hd"
                            OnRowDataBound="grdReport_RowDataBound">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, GRID_SNO%>"
                                    DataField="Sno" ItemStyle-CssClass="reportsGrid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, TARIFF%>"
                                    DataField="Tariff" ItemStyle-CssClass="reportsGrid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="KWH Sold" DataField="KWHSold"
                                    ItemStyle-CssClass="reportsGrid_tb_td" />
                                <asp:TemplateField HeaderText="Fixed Charges" runat="server" ItemStyle-CssClass="grid_tb_td">
                                    <ItemStyle HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblFixedCharges" runat="server" Text='<%#Eval("FixedCharges")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="No of Billed" DataField="NoBilled"
                                    ItemStyle-CssClass="reportsGrid_tb_td" />
                                <asp:TemplateField HeaderText="Total Amount Billed" runat="server" ItemStyle-CssClass="grid_tb_td">
                                    <ItemStyle HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalAmountBilled" runat="server" Text='<%#Eval("TotalAmountBilled")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total Amount Collected" runat="server" ItemStyle-CssClass="grid_tb_td">
                                    <ItemStyle HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalAmountCollected" runat="server" Text='<%#Eval("TotalAmountCollected")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="No Of Stubs" DataField="NoOfStubs"
                                    ItemStyle-CssClass="reportsGrid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Total Population" DataField="TotalPopulation"
                                    ItemStyle-CssClass="reportsGrid_tb_td" />
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Weighted Average" DataField="WeightedAverage"
                                    ItemStyle-CssClass="reportsGrid_tb_td" />
                            </Columns>
                        </asp:GridView>--%>
                        <table style="border-collapse: collapse;">
                            <asp:Repeater ID="rptrReport" runat="server">
                                <HeaderTemplate>
                                    <tr class="rptr_tb_hd">
                                        <th>
                                            S.No.
                                        </th>
                                        <th>
                                            Tariff
                                        </th>
                                        <th>
                                            Energy Delivered
                                        </th>
                                        <th>
                                            Fixed Charges
                                        </th>
                                        <th>
                                            No of Billed
                                        </th>
                                        <th>
                                            Total Amount Billed
                                        </th>
                                        <th>
                                            Total Amount Collected
                                        </th>
                                        <th>
                                            No Of Stubs
                                        </th>
                                        <th>
                                            Total Population
                                        </th>
                                       <%-- <th>
                                            Weighted Average
                                        </th>--%>
                                    </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="reportsGrid_tb_td">
                                        <td align="left" class="rptr_tb_td">
                                            <%--<asp:Label ID="lblSno" runat="server" Text='<%#Eval("Sno")%>'></asp:Label>--%>
                                            <%#Container.ItemIndex+1 %>
                                        </td>
                                        <td align="left" class="rptr_tb_td">
                                            <asp:Label ID="lblTariff" runat="server" Text='<%#Eval("Tariff")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblKWHSold" runat="server" Text='<%#Eval("KWHSold")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblFixedCharges" runat="server" Text='<%#Eval("FixedCharges")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblNoBilled" runat="server" Text='<%#Eval("NoBilled")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblTotalAmountBilled" runat="server" Text='<%#Eval("TotalAmountBilled")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblTotalAmountCollected" runat="server" Text='<%#Eval("TotalAmountCollected")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblNoOfStubs" runat="server" Text='<%#Eval("NoOfStubs")%>'></asp:Label>
                                        </td>
                                        <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblTotalPopulation" runat="server" Text='<%#Eval("TotalPopulation")%>'></asp:Label>
                                        </td>
                                       <%-- <td align="right" class="rptr_tb_td">
                                            <asp:Label ID="lblWeightedAverage" runat="server" Text='<%#Eval("WeightedAverage")%>'></asp:Label>
                                        </td>--%>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <tr id="NoDataFound" runat="server" align="center"><td colspan="9"> No data found for selected month & Year</td></tr>
                        </table>
                    </div>
                </div>
            </div>
        </ContentTemplate>        
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script type="text/javascript">
        function pageLoad() {
            DisplayMessage('UMSNigeriaBody_pnlMessage');
        };
        function Validate() {
            var ddlBusinessUnitName = document.getElementById('UMSNigeriaBody_ddlBusinessUnitName');
            var ddlYear = document.getElementById('UMSNigeriaBody_ddlYear');
            var ddlMonth = document.getElementById('UMSNigeriaBody_ddlMonth');

            var IsValid = true;

            if (DropDownlistValidationlbl(ddlBusinessUnitName, document.getElementById("spanddlBusinessUnitName"), "Business Unit") == false) IsValid = false;
            if (DropDownlistValidationlbl(ddlYear, document.getElementById("spanddlYear"), "Year") == false) IsValid = false;
            if (DropDownlistValidationlbl(ddlMonth, document.getElementById("spanddlMonth"), "Month") == false) IsValid = false;

            if (!IsValid) {
                return false;
            }
        }
    </script>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
