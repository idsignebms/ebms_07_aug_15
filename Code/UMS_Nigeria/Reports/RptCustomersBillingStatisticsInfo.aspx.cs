﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Data;
using Resources;
using System.Threading;
using System.Globalization;
using System.Xml;
using System.Configuration;
using org.in2bits.MyXls;

namespace UMS_Nigeria.Reports
{
    public partial class RptCustomersBillingStatisticsInfo : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBAL = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        NewReportsBal _objNewReportsBal = new NewReportsBal();
        string Key = "RptCustomersBillingStatisticsInfo";
        int Month = 0, Year = 0;
        string MonthYear = string.Empty;
        #endregion

        #region Properties
        private int YearId
        {
            get { return string.IsNullOrEmpty(ddlYear.SelectedValue) ? 0 : Convert.ToInt32(ddlYear.SelectedValue); }
            set { ddlYear.SelectedValue = value.ToString(); }
        }
        private int MonthId
        {
            get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? 0 : Convert.ToInt32(ddlMonth.SelectedValue); }
            set { ddlMonth.SelectedValue = value.ToString(); }
        }
        private string BU_ID
        {
            get { return string.IsNullOrEmpty(ddlBusinessUnitName.SelectedValue) ? string.Empty : ddlBusinessUnitName.SelectedValue; }
            set { ddlBusinessUnitName.SelectedValue = value; }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            try
            {
                if (Session[UMS_Resource.SESSION_LOGINID] == null)
                    Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, ErrorConstants.Page_PreInit, ErrorLog.LogType.Error, ex);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session[UMS_Resource.SESSION_LOGINID] != null)
                {
                    lblMessage.Text = pnlMessage.CssClass = string.Empty;
                    if (!IsPostBack)
                    {
                        BindBusinessUnits();
                        BindYears();
                        BindMonths();
                    }
                }
                else { Response.Redirect(UMS_Resource.DEFAULT_PAGE); }
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, ErrorConstants.Page_Load, ErrorLog.LogType.Error, ex);
            }
        }

        private void BindBusinessUnits()
        {
            _objCommonMethods.BindBusinessUnits(ddlBusinessUnitName, string.Empty, true);
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
            {
                ddlBusinessUnitName.SelectedIndex = ddlBusinessUnitName.Items.IndexOf(ddlBusinessUnitName.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                ddlBusinessUnitName.Enabled = false;
            }
        }

        //protected void grdReport_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    try
        //    {

        //        if (e.Row.RowType == DataControlRowType.DataRow)
        //        {
        //            Label lblFixedCharges = (Label)e.Row.FindControl("lblFixedCharges");
        //            lblFixedCharges.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(string.IsNullOrEmpty(lblFixedCharges.Text) ? "0" : lblFixedCharges.Text), 2, Constants.MILLION_Format);
        //            Label lblTotalAmountBilled = (Label)e.Row.FindControl("lblTotalAmountBilled");
        //            lblTotalAmountBilled.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(string.IsNullOrEmpty(lblTotalAmountBilled.Text) ? "0" : lblTotalAmountBilled.Text), 2, Constants.MILLION_Format);
        //            Label lblTotalAmountCollected = (Label)e.Row.FindControl("lblTotalAmountCollected");
        //            lblTotalAmountCollected.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(string.IsNullOrEmpty(lblTotalAmountCollected.Text) ? "0" : lblTotalAmountCollected.Text), 2, Constants.MILLION_Format);

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindMonths();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindGrid();
        }
        #endregion

        #region Methods

        public void BindGrid()
        {
            try
            {
                PDFReportsBe _objPDFReportsBe = new PDFReportsBe();
                _objPDFReportsBe.Month = this.MonthId;
                _objPDFReportsBe.Year = this.YearId;
                _objPDFReportsBe.BU_ID = this.BU_ID;
                DataSet dsData = new DataSet();
                dsData = _objNewReportsBal.GetData(_objPDFReportsBe, ReportType.RptGetCustomersBillingStatisticsInfo);
                if (dsData.Tables[0].Rows.Count > 0)
                {
                    divgrid.Visible = divExport.Visible = true;
                    NoDataFound.Visible = false;
                    rptrReport.DataSource = dsData.Tables[0];
                    rptrReport.DataBind();
                    MonthYear = ddlYear.SelectedItem.Text + " " + ddlMonth.SelectedItem.Text;
                    string BUName = string.IsNullOrEmpty(ddlBusinessUnitName.SelectedValue) ? "All" : ddlBusinessUnitName.SelectedItem.Text;
                    ltrlReportNamewithBU.Text = BUName + Resource.RPT_CUST_SATISTICS + MonthYear;
                }
                else
                {
                    divgrid.Visible = NoDataFound.Visible = true;
                    divExport.Visible = false;
                    rptrReport.DataSource = new DataTable();
                    rptrReport.DataBind();
                    MonthYear = ddlYear.SelectedItem.Text + " " + ddlMonth.SelectedItem.Text;
                    string BUName = string.IsNullOrEmpty(ddlBusinessUnitName.SelectedValue) ? "All" : ddlBusinessUnitName.SelectedItem.Text;
                    ltrlReportNamewithBU.Text = BUName + Resource.RPT_CUST_SATISTICS + MonthYear;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, ErrorConstants.BindGrid, ErrorLog.LogType.Error, ex);
            }
        }

        private void BindYears()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
                xmlResult = _objBillGenerationBal.GetDetails(ReturnType.Fetch);
                BillGenerationListBe objBillsListBE = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                if (objBillsListBE.Items.Count > 0)
                {
                    _objiDsignBAL.FillDropDownList(_objiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlYear, "Year", "Year", "--Select--", true);
                    BindMonths();
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindMonths()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
                _objBillGenerationBe.Year = this.YearId;
                xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.Group);
                BillGenerationListBe objBillsListBE = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                if (objBillsListBE.Items.Count > 0)
                {
                    ddlMonth.Enabled = true;
                    _objiDsignBAL.FillDropDownList(_objiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlMonth, "MonthName", "Month", "--Select--", true);
                }
                else
                {
                    ddlMonth.SelectedIndex = 0;
                    ddlMonth.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception ex)
            {
                ErrorLog.LoggingIntoText(ex.Message, ErrorConstants.Message, ErrorLog.LogType.Error, ex);
            }
        }


        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region ExportExcel

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                PDFReportsBe _objPDFReportsBe = new PDFReportsBe();
                _objPDFReportsBe.Month = this.MonthId;
                _objPDFReportsBe.Year = this.YearId;
                _objPDFReportsBe.BU_ID = this.BU_ID;
                DataSet dsData = new DataSet();
                dsData = _objNewReportsBal.GetData(_objPDFReportsBe, ReportType.RptGetCustomersBillingStatisticsInfo);

                if (dsData.Tables[0].Rows.Count > 0)
                {
                    XlsDocument xls = new XlsDocument();
                    Worksheet sheet = xls.Workbook.Worksheets.Add("Customers Bill Statistics Info");
                    Cells cells = sheet.Cells;
                    Cell _objCell = null;

                    _objCell = cells.Add(2, 3, ltrlReportNamewithBU.Text);
                    sheet.AddMergeArea(new MergeArea(2, 2, 3, 6));
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;

                    int DetailsRowNo = 5;
                    int CustomersRowNo = 6;

                    _objCell = cells.Add(DetailsRowNo, 1, "S.No.");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 2, "Tariff");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 3, "Energy Delivered");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 4, "Fixed Charges");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 5, "No of Billed");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 6, "Total Amount Billed");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 7, "Total Amount Collected");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 8, "No Of Stubs");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    _objCell = cells.Add(DetailsRowNo, 9, "Total Population");
                    _objCell.Font.Weight = FontWeight.ExtraBold;
                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    _objCell.Font.FontFamily = FontFamilies.Roman;
                    //_objCell = cells.Add(DetailsRowNo, 10, "Weighted Average");
                    //_objCell.Font.Weight = FontWeight.ExtraBold;
                    //_objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                    //_objCell.Font.FontFamily = FontFamilies.Roman;

                    for (int n = 0; n < dsData.Tables[0].Rows.Count; n++)
                    {
                        DataRow dr = (DataRow)dsData.Tables[0].Rows[n];
                        cells.Add(CustomersRowNo, 1, n + 1);
                        cells.Add(CustomersRowNo, 2, dr["Tariff"].ToString());
                        cells.Add(CustomersRowNo, 3, dr["KWHSold"].ToString());
                        cells.Add(CustomersRowNo, 4, dr["FixedCharges"].ToString());
                        cells.Add(CustomersRowNo, 5, dr["NoBilled"].ToString());
                        cells.Add(CustomersRowNo, 6, dr["TotalAmountBilled"].ToString());
                        cells.Add(CustomersRowNo, 7, dr["TotalAmountCollected"].ToString());
                        cells.Add(CustomersRowNo, 8, dr["NoOfStubs"].ToString());
                        cells.Add(CustomersRowNo, 9, dr["TotalPopulation"].ToString());
                        //cells.Add(CustomersRowNo, 10, dr["WeightedAverage"].ToString());

                        CustomersRowNo++;
                    }

                    string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                    fileName = "CustomersBillStatisticsInfo_" + fileName;
                    string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                    xls.FileName = filePath;
                    xls.Save();
                    _objiDsignBAL.DownLoadFile(filePath, "application//vnd.ms-excel");
                }
                else
                    Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}