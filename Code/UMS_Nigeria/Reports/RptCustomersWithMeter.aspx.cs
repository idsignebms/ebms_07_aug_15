﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using UMS_NigeriaBE;
using System.Data;
using Resources;
using System.Drawing;
using System.Xml;
using System.Configuration;
using org.in2bits.MyXls;

namespace UMS_Nigeria.Reports
{
    public partial class RptCustomersWithMeter : System.Web.UI.Page
    {
        #region Members
        CommonMethods _objCommonMethods = new CommonMethods();
        iDsignBAL _objIdsignBAL = new iDsignBAL();
        GeneralReportsBal _objGeneralReportsBal = new GeneralReportsBal();
        string Key = "CustomersWithMeter";
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStartsReport : Convert.ToInt32(hfPageSize.Value); }
        }
        public string BU
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblBusinessUnits, ","); }
        }
        public string SU
        {
            get { return string.IsNullOrEmpty(ddlSU.SelectedValue) ? string.Empty : ddlSU.SelectedValue; }
            set { ddlSU.SelectedValue = value.ToString(); }
        }
        public string SC
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceCenters, ","); }
        }
        public string Tariff
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblTarif, ","); }
        }
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.CssClass = lblMessage.Text = string.Empty;
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                if (!IsPostBack)
                {
                    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    {
                        ListItem LiBU = new ListItem(Session[UMS_Resource.SESSION_USER_BUNAME].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());
                        cblBusinessUnits.Items.Add(LiBU);
                        cblBusinessUnits.Items[0].Selected = true;
                        cblBusinessUnits.Enabled = false;
                        ChkBUAll.Visible = false;
                        divServiceUnits.Visible = true;
                        _objCommonMethods.BindUserServiceUnits_BuIds(ddlSU, BU, UMS_Resource.DROPDOWN_SELECT, false);
                    }
                    else
                        _objCommonMethods.BindBusinessUnits(cblBusinessUnits, string.Empty, false);


                    _objCommonMethods.BindTariffSubTypes(cblTarif, 0, false);
                    CheckBoxesJS();
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    hfTotalRecords.Value = Constants.Zero.ToString();
                }
            }
            else
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            hfPageNo.Value = Constants.pageNoValue.ToString();
            BindGrid();
            UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            UCPaging2.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        }

        protected void cblBusinessUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblServiceCenters.Items.Clear();
                divServiceUnits.Visible = chkTariffAll.Checked = chkSCAll.Checked = divServiceCenters.Visible = false;

                if (!string.IsNullOrEmpty(BU))
                {
                    _objCommonMethods.BindUserServiceUnits_BuIds(ddlSU, BU, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlSU.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlSU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblServiceCenters.Items.Clear();
                chkSCAll.Checked = divServiceCenters.Visible = false;
                if (ddlSU.SelectedIndex > 0)
                {
                    _objCommonMethods.BindUserServiceCenters_SuIds(cblServiceCenters, SU, Resource.DDL_ALL, false);
                    if (cblServiceCenters.Items.Count > 0)
                        divServiceCenters.Visible = true;
                    else
                        divServiceCenters.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblServiceCenters.Items.Clear();
                chkSCAll.Checked = divServiceCenters.Visible = false;
                if (!string.IsNullOrEmpty(SU))
                {
                    _objCommonMethods.BindUserServiceCenters_SuIds(cblServiceCenters, SU, Resource.DDL_ALL, false);
                    if (cblServiceCenters.Items.Count > 0)
                        divServiceCenters.Visible = true;
                    else
                        divServiceCenters.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods

        public void BindGrid()
        {
            RptReadCustomersBe _objRptReadCustomersBe = new RptReadCustomersBe();
            _objRptReadCustomersBe.BU_ID = this.BU;
            _objRptReadCustomersBe.SU_ID = this.SU;
            _objRptReadCustomersBe.SC_ID = this.SC;
            _objRptReadCustomersBe.Tariff = this.Tariff;
            _objRptReadCustomersBe.PageNo = Convert.ToInt32(hfPageNo.Value);
            _objRptReadCustomersBe.PageSize = PageSize;

            DataSet dsRead = new DataSet();
            dsRead = _objGeneralReportsBal.GetReadCustReport(_objRptReadCustomersBe, ReturnType.Get);

            if (dsRead != null && dsRead.Tables.Count > 0 && dsRead.Tables[0].Rows.Count > 0)
            {
                divToprptrContent.Visible = divExport.Visible = divDownrptrContent.Visible = divrptrDetails.Visible = rptrheader.Visible = true;
                divNoDataFound.Visible = false;
                rptrReadCustomers.DataSource = dsRead.Tables[0];
                rptrReadCustomers.DataBind();
                //lblTopTotalCustomers.Text = lblDownTotalCustomers.Text = hfTotalRecords.Value = dsRead.Tables[0].Rows[0]["TotalRecords"].ToString();
                lblTopTotalCustomers.Text = lblDownTotalCustomers.Text = hfTotalRecords.Value = dsRead.Tables[0].Rows[0]["TotalRecords"].ToString();
                lblTopTotalOutStanding.Text = lblDownTotalOutStanding.Text = dsRead.Tables[0].Rows[0]["TotalDueAmount"].ToString();
                // Convert.ToInt32(_objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(hfTotalRecords.Value)));
                hfTotalRecords.Value = hfTotalRecords.Value.Replace(",", "");
            }
            else
            {
                divToprptrContent.Visible = divExport.Visible = divDownrptrContent.Visible = rptrheader.Visible = false;
                divNoDataFound.Visible = divrptrDetails.Visible = true;
                rptrReadCustomers.DataSource = new DataTable(); ;
                rptrReadCustomers.DataBind();
                lblTopTotalCustomers.Text = lblDownTotalCustomers.Text = hfTotalRecords.Value = Constants.Zero.ToString();
            }
        }

        private void CheckBoxesJS()
        {
            ChkBUAll.Attributes.Add("onclick", "CheckAll('" + ChkBUAll.ClientID + "','" + cblBusinessUnits.ClientID + "')");
            //ChkSUAll.Attributes.Add("onclick", "CheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            chkSCAll.Attributes.Add("onclick", "CheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            chkTariffAll.Attributes.Add("onclick", "CheckAll('" + chkTariffAll.ClientID + "','" + cblTarif.ClientID + "')");

            cblBusinessUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkBUAll.ClientID + "','" + cblBusinessUnits.ClientID + "')");
            //cblServiceUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            cblServiceCenters.Attributes.Add("onclick", "UnCheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            cblTarif.Attributes.Add("onclick", "UnCheckAll('" + chkTariffAll.ClientID + "','" + cblTarif.ClientID + "')");
        }

        private void Message(string Message, string MessageType)
        {
            try
            {

                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

        #region ExportToExcel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                RptReadCustomersBe _objRptReadCustomersBe = new RptReadCustomersBe();
                _objRptReadCustomersBe.BU_ID = this.BU;
                _objRptReadCustomersBe.SU_ID = this.SU;
                _objRptReadCustomersBe.SC_ID = this.SC;
                _objRptReadCustomersBe.Tariff = this.Tariff;
                _objRptReadCustomersBe.PageNo = Constants.pageNoValue;
                _objRptReadCustomersBe.PageSize = Convert.ToInt32(hfTotalRecords.Value);

                DataSet dsRead = new DataSet();
                dsRead = _objGeneralReportsBal.GetReadCustReport(_objRptReadCustomersBe, ReturnType.Get);

                if (dsRead.Tables[0].Rows.Count > 0)
                {
                    var BUlist = (from list in dsRead.Tables[0].AsEnumerable()
                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                  group list by list.Field<string>("BusinessUnitName") into g
                                  select new { BusinessUnit = g.Key }).ToList();

                    if (BUlist.Count() > 0)
                    {
                        XlsDocument xls = new XlsDocument();
                        Worksheet mainSheet = xls.Workbook.Worksheets.Add("Summary");
                        Cells mainCells = mainSheet.Cells;
                        Cell _objMainCells = null;

                        _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
                        mainCells.Add(4, 2, "Report Code ");
                        mainCells.Add(4, 3, Constants.CustomerWithMeterReport_Code).Font.Bold = true;
                        mainCells.Add(4, 4, "Report Name");
                        mainCells.Add(4, 5, Constants.CustomerWithMeterReport_Name).Font.Bold = true;
                        mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
                        _objMainCells.Font.Weight = FontWeight.ExtraBold;
                        _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                        _objMainCells.Font.FontFamily = FontFamilies.Roman;

                        int MainSheetDetailsRowNo = 6;
                        _objMainCells = mainCells.Add(MainSheetDetailsRowNo, 1, "S.No.");
                        _objMainCells.Font.Bold = true;
                        _objMainCells.UseBorder = true;
                        _objMainCells = mainCells.Add(MainSheetDetailsRowNo, 2, "Service Center");
                        _objMainCells.Font.Bold = true;
                        _objMainCells.UseBorder = true;
                        _objMainCells = mainCells.Add(MainSheetDetailsRowNo, 3, "Customer Count");
                        _objMainCells.Font.Bold = true;
                        _objMainCells.UseBorder = true;

                        var SClist = (from list in dsRead.Tables[0].AsEnumerable()
                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                      orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                      group list by list.Field<string>("ServiceCenterName") into g
                                      select new { ServiceCenter = g.Key }).ToList();

                        for (int k = 0; k < SClist.Count; k++)//SC
                        {
                            var Customers = (from list in dsRead.Tables[0].AsEnumerable()
                                             where list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                             select list).ToList();

                            MainSheetDetailsRowNo++;
                            mainCells.Add(MainSheetDetailsRowNo, 1, k + 1).UseBorder = true;
                            mainCells.Add(MainSheetDetailsRowNo, 2, SClist[k].ServiceCenter).UseBorder = true;
                            mainCells.Add(MainSheetDetailsRowNo, 3, Customers.Count).UseBorder = true;
                        }


                        for (int i = 0; i < BUlist.Count; i++)//BU
                        {
                            var SUlistByBU = (from list in dsRead.Tables[0].AsEnumerable()
                                              where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                              orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                              orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                              group list by list.Field<string>("ServiceUnitName") into g
                                              select new { ServiceUnitName = g.Key }).ToList();

                            for (int j = 0; j < SUlistByBU.Count; j++)//SU
                            {
                                var SClistBySU = (from list in dsRead.Tables[0].AsEnumerable()
                                                  where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                  && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                  orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                  group list by list.Field<string>("ServiceCenterName") into g
                                                  select new { ServiceCenter = g.Key }).ToList();

                                for (int k = 0; k < SClistBySU.Count; k++)//SC
                                {
                                    var CyclelistBySC = (from list in dsRead.Tables[0].AsEnumerable()
                                                         where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                         && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                         && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                         orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                         orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                         group list by list.Field<string>("CycleName") into g
                                                         select new { CycleName = g.Key }).ToList();

                                    if (CyclelistBySC.Count > 0)
                                    {
                                        int DetailsRowNo = 2;
                                        int CustomersRowNo = 6;

                                        Worksheet sheet = xls.Workbook.Worksheets.Add(SClistBySU[k].ServiceCenter);
                                        Cells cells = sheet.Cells;
                                        Cell _objCell = null;

                                        for (int l = 0; l < CyclelistBySC.Count; l++)//Cycle
                                        {
                                            var BooklistByCycle = (from list in dsRead.Tables[0].AsEnumerable()
                                                                   where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                   && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                   && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                   && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                   orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                   orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                   group list by list.Field<string>("BookNumber") into g
                                                                   select new { BookNumber = g.Key }).ToList();

                                            for (int m = 0; m < BooklistByCycle.Count; m++)//Book
                                            {
                                                var Customers = (from list in dsRead.Tables[0].AsEnumerable()
                                                                 where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                 && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                 && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                 && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                 && list.Field<string>("BookNumber") == BooklistByCycle[m].BookNumber
                                                                 orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                 orderby _objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                 orderby Convert.ToString(list["CycleName"]) ascending
                                                                 select list).ToList();

                                                if (Customers.Count() > 0)
                                                {

                                                    _objCell = cells.Add(DetailsRowNo, 1, "Business Unit");
                                                    cells.Add(DetailsRowNo, 3, "Service Unit");
                                                    cells.Add(DetailsRowNo, 5, "Service Center");
                                                    cells.Add(DetailsRowNo + 1, 1, "Book Group");
                                                    cells.Add(DetailsRowNo + 1, 3, "Book Code");

                                                    cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 4, SUlistByBU[j].ServiceUnitName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo, 6, SClistBySU[k].ServiceCenter).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 2, CyclelistBySC[l].CycleName).Font.Bold = true;
                                                    cells.Add(DetailsRowNo + 1, 4, BooklistByCycle[m].BookNumber).Font.Bold = true;

                                                    _objCell = cells.Add(DetailsRowNo + 3, 1, Resource.GRID_SNO);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.UseBorder = true;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 2, Resource.ACCOUNT_NO);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.UseBorder = true;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 3, Resource.OLD_ACCOUNT_NO);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.UseBorder = true;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 4, Resource.NAME);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.UseBorder = true;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 5, Resource.SERVICE_ADDRESS);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.UseBorder = true;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 6, Resource.METER_NO);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.UseBorder = true;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 7, Resource.METER_READING);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.UseBorder = true;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 8, Resource.TARIFF);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.UseBorder = true;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;
                                                    _objCell = cells.Add(DetailsRowNo + 3, 9, Resource.CONNECTION_DATE);
                                                    _objCell.Font.Weight = FontWeight.ExtraBold;
                                                    _objCell.UseBorder = true;
                                                    _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                    _objCell.Font.FontFamily = FontFamilies.Roman;

                                                    for (int n = 0; n < Customers.Count; n++)
                                                    {
                                                        DataRow dr = (DataRow)Customers[n];
                                                        cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                                                        cells.Add(CustomersRowNo, 2, dr["GlobalAccountNumber"].ToString()).UseBorder = true;
                                                        cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString()).UseBorder = true;
                                                        cells.Add(CustomersRowNo, 4, dr["Name"].ToString()).UseBorder = true;
                                                        cells.Add(CustomersRowNo, 5, dr["ServiceAddress"].ToString()).UseBorder = true;
                                                        cells.Add(CustomersRowNo, 6, dr["MeterNumber"].ToString()).UseBorder = true;
                                                        cells.Add(CustomersRowNo, 7, dr["PresentReading"].ToString()).UseBorder = true;
                                                        cells.Add(CustomersRowNo, 8, dr["ClassName"].ToString()).UseBorder = true;
                                                        cells.Add(CustomersRowNo, 9, dr["ConnectionDate"].ToString()).UseBorder = true;
                                                        CustomersRowNo++;
                                                    }
                                                    DetailsRowNo = CustomersRowNo + 2;
                                                    CustomersRowNo = CustomersRowNo + 6;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                        fileName = Constants.CustomerWithMeterReport_Name.Replace(" ", string.Empty) + "_" + fileName;
                        string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                        xls.FileName = filePath;
                        xls.Save();
                        _objIdsignBAL.DownLoadFile(filePath, "application//vnd.ms-excel");
                    }
                    else
                        Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                    Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}