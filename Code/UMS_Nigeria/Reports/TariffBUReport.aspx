﻿<%@ Page Title="::Tariff BU Report::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="TariffBUReport.aspx.cs" Inherits="UMS_Nigeria.Reports.TariffBUReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <div class="out-bor">
        <div class="inner-sec">
            <asp:UpdatePanel ID="upAddCountries" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="text_total">
                <div class="text-heading">
                    <asp:Literal ID="litAccountWoCycle" runat="server" Text="<%$ Resources:Resource, TARIFF_BU%>"></asp:Literal>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="dot-line">
            </div>
            <div class="previous">
                            <a href="ReportsSummary.aspx"> < < Back To Summary</a>
                        </div>
            <div class="grid_boxes">
                <div class="paging_top_title">
                    &nbsp;
                </div>
                <div class="grid_paging_top">
                    <div class="paging_top_right_content" runat="server" id="divExport">
                        <asp:Button ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" CssClass="exporttoexcelButton"
                            />
                    </div>
                </div>
            </div>
            <div class="grid_tb" id="divgrid" runat="server"> <%--OnRowDataBound="gvTariffBU_RowDataBound" --%>
                <asp:GridView ID="gvTariffBU" runat="server" AutoGenerateColumns="true" AlternatingRowStyle-CssClass="color"
                    HeaderStyle-CssClass="grid_tb_hd ">
                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                    <RowStyle HorizontalAlign="Center" />
                </asp:GridView>
            </div>
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <%--<script type="text/javascript">
        function pageLoad() {
            var table = '<%= gvTariffBU.ClientID %>';
            $("#<%= btnExportExcel.ClientID%>").click(function () {

                $("#<%= gvTariffBU.ClientID %>").btechco_excelexport({
                    containerid: table
               , datatype: $datatype.Table
                });
            });
        }
    </script>--%>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../Scripts/jquery.fixedheadertable.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <%--<script src="../scripts/jquery.btechco.excelexport.js" type="text/javascript"></script>
    <script src="../scripts/jquery.base64.js" type="text/javascript"></script>--%>
    <%--<script type="text/javascript">
        function printGrid() {

            var printContent = document.getElementById('<%=divPrint.ClientID %>');

            var windowUrl = 'about:blank';
            var uniqueName = new Date();
            var windowName = 'Print' + uniqueName.getTime();
            var printWindow = window.open();
            printWindow.document.write(printContent.innerHTML);
            printWindow.document.close();
            printWindow.focus();
            printWindow.print();
            printWindow.close();
        }     
    </script>--%>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
