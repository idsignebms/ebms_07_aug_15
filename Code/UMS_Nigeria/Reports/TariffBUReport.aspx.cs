﻿
#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for TariffBUReport
                     
 Developer        : Id075-RamaDevi M
 Creation Date    : 12-Apr-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using iDSignHelper;
using System.Data;
using System.Xml;
using System.IO;
using ClosedXML.Excel;
using Resources;
using org.in2bits.MyXls;
using System.Configuration;

namespace UMS_Nigeria.Reports
{
    public partial class TariffBUReport : System.Web.UI.Page
    {
        #region Members
        iDsignBAL _objIdesignBal = new iDsignBAL();
        ConsumerBe _objConsumerBE = new ConsumerBe();
        ConsumerBal _objConsumerBAl = new ConsumerBal();
        CommonMethods objCommonMethods = new CommonMethods();
        DataSet dataSet = new DataSet();
        int CountRows = 0;
        #endregion
        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
            {
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = objCommonMethods.GetPagePath(this.Request.Url);
                //if (objCommonMethods.IsAccess(path))
                //{
                BindTariffBU();
                //}
                //else
                //{
                //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                //}
            }

        }
        //protected void gvTariffBU_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        if (e.Row.RowIndex == CountRows - 1)
        //        {
        //            // e.Row.BackColor =System.Drawing.Color.Brown;
        //            e.Row.Style["background-color"] = "rgba(200, 152, 112, 1)";
        //        }
        //    }
        //}
        #endregion
        #region Methods
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
        protected void BindTariffBU()
        {
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                _objConsumerBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            else
                _objConsumerBE.BU_ID = string.Empty;

            DataSet dataSet = new DataSet();
            //XmlNodeReader xmlReader = new XmlNodeReader(_objConsumerBAl.GetTariffBUReportBAL());
            //dataSet.ReadXml(xmlReader);
            dataSet = _objConsumerBAl.GetTariffBUReportBAL(_objConsumerBE);

            if (dataSet.Tables[0].Rows.Count > 0)
            {

                divExport.Visible = true;
                if (Convert.ToInt32(UMS_NigriaDAL.Roles.SuperAdmin) == Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()))
                {
                    DataTable dt = dataSet.Tables[0];
                    dt.Rows.Add();
                    dt.Rows[dt.Rows.Count - 1][0] = "Total";
                    //dt.Columns[0].ColumnName = "Business Unit";

                    foreach (DataRow dr in dt.Rows)
                    {
                        foreach (DataColumn dc in dt.Columns)
                        {
                            if (dt.Columns.IndexOf(dc) != 0 && dt.Rows.IndexOf(dr) != (dt.Rows.Count - 1))
                            {
                                //dt.Rows[dt.Rows.Count - 1][dc] = dt.Rows[dt.Rows.Count - 1][dc].ToString() == "" ? (dr[dc].ToString() == "" ? 0 : Convert.ToInt32(dr[dc].ToString())) : Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][dc].ToString()) + (dr[dc].ToString() == "" ? 0 : Convert.ToInt32(dr[dc].ToString()));
                                dt.Rows[dt.Rows.Count - 1][dc] = dt.Rows[dt.Rows.Count - 1][dc].ToString() == "" ? (dr[dc].ToString() == "" ? 0 : Convert.ToDecimal(dr[dc].ToString())) : Convert.ToDecimal(dt.Rows[dt.Rows.Count - 1][dc].ToString()) + (dr[dc].ToString() == "" ? 0 : Convert.ToDecimal(dr[dc].ToString()));
                            }

                        }
                    }

                    foreach (DataColumn dc in dt.Columns)
                    {
                        if (dt.Columns.IndexOf(dc) > 1)
                        {
                            dt.Rows[dt.Rows.Count - 1][dc] = objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(dt.Rows[dt.Rows.Count - 1][dc].ToString()), 0, Constants.MILLION_Format);
                        }
                    }
                }
            }
            else
            {
                divExport.Visible = false;
            }
            CountRows = dataSet.Tables[0].Rows.Count;
            gvTariffBU.DataSource = dataSet;
            gvTariffBU.DataBind();
            Session[UMS_Resource.SESSION_REPORTS_DATA] = dataSet.Tables[0];

            //ConsumerListBe obConsumerList = _objIdesignBal.DeserializeFromXml<ConsumerListBe>(_objConsumerBAl.GetTariffBUReportBAL());
            //gvTariffBU.DataSource = _objIdesignBal.ConvertListToDataSet<ConsumerBe>(obConsumerList.items).Tables[0];
            //gvTariffBU.DataBind();
        }
        #endregion

        #region ExportToExcel

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            DataTable dt = null;
            if (Session[UMS_Resource.SESSION_REPORTS_DATA] != null)
            {
                dt = (DataTable)Session[UMS_Resource.SESSION_REPORTS_DATA];
                if (dt.Rows.Count > 0)
                    ExportToExcelSavedReport(dt, "TariffBU_");
                //objCommonMethods.ExportToExcelSavedReport(dt, "TariffBU");
            }
        }

        public void ExportToExcelSavedReport(DataTable Data, string FileName)
        {

            XlsDocument xls = new org.in2bits.MyXls.XlsDocument();
            Worksheet sheet = xls.Workbook.Worksheets.Add(UMS_Resource.SHEET);
            //state the name of the column headings 
            Cells cells = sheet.Cells;
            Cell _objCell = null;

            _objCell = cells.Add(1, 6, Resource.BEDC + "\n" + UMS_Resource.RPT_TARIFF_BU + DateTime.Today.ToShortDateString());
            sheet.AddMergeArea(new MergeArea(1, 2, 6, 14));
            _objCell.Font.Weight = FontWeight.ExtraBold;
            _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
            _objCell.Font.FontFamily = FontFamilies.Roman;

            int columnPosition = 1, rowPosition = 4;

            string data = string.Empty;
            foreach (DataColumn column in Data.Columns)
            {
                sheet.Cells.Add(4, columnPosition, column.ColumnName);
                _objCell = cells.Add(4, columnPosition, column.ColumnName);
                _objCell.Font.Weight = FontWeight.ExtraBold;
                _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                _objCell.Font.FontFamily = FontFamilies.Roman;
                columnPosition++;
            }

            foreach (DataRow row in Data.Rows)
            {
                rowPosition++;
                int col = 1;
                foreach (DataColumn column in Data.Columns)
                {
                    data = row[column.ColumnName].ToString();
                    if (data.Trim() == "0")
                        data = "";
                    cells.Add(rowPosition, col, data);
                    col++;
                }
            }

            string filePath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + FileName + "_" + DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";

            //set up a filestream
            xls.FileName = filePath;

            //xls.FileName = Server.MapPath("Files/test.xls");
            xls.Save();
            _objIdesignBal.DownLoadFile(filePath, "application//vnd.ms-excel");

        }

        public void getRows(string colName)
        {
            var rowColl = dataSet.Tables[0].AsEnumerable();
            var name = from r in rowColl
                       //where r.Field<int>("ID") == 0
                       select r.Field<string>(colName);
        }
        #endregion
    }
}