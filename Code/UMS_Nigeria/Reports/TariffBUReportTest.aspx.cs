﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace UMS_Nigeria.Reports
{
    public partial class TariffBUReportTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            getdata();
        }

        public void getdata()
        {
            DataSet ds = new DataSet();
            string cmd = "select ClassID,ClassName from Tbl_MTariffClasses where ClassID IN(2,3,4,5) AND IsActiveClass=1 ";
            cmd += "SELECT ReadCodeId,ReadCode,DisplayCode FROM Tbl_MReadCodes WHERE ActiveStatusId=1 ";
            cmd += "select C.ClassName,D.DisplayCode,A.ReadCodeId,A.TariffId,A.BU_ID,COUNT(0) AS TOAL from Tbl_CustomerDetails AS A JOIN Tbl_BussinessUnits AS B ON A.BU_ID=B.BU_ID JOIN Tbl_MTariffClasses AS C ON A.TariffId=C.ClassID JOIN Tbl_MReadCodes AS D ON A.ReadCodeId=D.ReadCodeId where A.TariffId IN(2,3,4,5) GROUP BY A.BU_ID,A.ReadCodeId,A.TariffId,D.DisplayCode,C.ClassName";

            SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["Sqlcon"].ToString());
            SqlDataAdapter da = new SqlDataAdapter(cmd, con);
            da.Fill(ds);
            //for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            //{
            //    //string s= ds.Tables[0].Rows[i]["ClassName"].ToString();
            //}
            createColumns(ds.Tables[0], ds.Tables[1]);
        }

        public void createColumns(DataTable dtTariff, DataTable dtReadCode)
        {
            DataTable dtTop = new DataTable();
            BoundField b = new BoundField();
            //b.HeaderText = "";
            //b.DataField = "";

            //DataColumnCollection dcf = new DataColumnCollection();
            //dcf.Add("");
            //gvTReport.Columns.Add(dcf.c);

            DataColumn dc = new DataColumn();

            foreach (DataRow drTariff in dtTariff.Rows)
            {
                dc = new DataColumn(drTariff["ClassName"].ToString(), typeof(int));
                dtTop.Columns.Add(dc);

                //foreach (DataRow drReadCode in dtReadCode.Rows)
                //{
                //    //dc = new DataColumn(drTariff["ClassName"].ToString(), typeof(int));
                //    //dtTop.Columns.Add(dc);

                //   // b = new BoundField();
                //    b.DataField = drTariff["ClassName"].ToString();
                //    b.HeaderText = drTariff["ClassName"].ToString();
                //    gvTReport.Columns.Add(b);
                //    //gvTReport.DataBind();
                //}
            }
            gvTReport.DataSource = dtTop;
            gvTReport.DataBind();
        }
    }
}