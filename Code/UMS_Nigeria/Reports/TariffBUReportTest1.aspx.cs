﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace UMS_Nigeria.Reports
{
    public partial class TariffBUReportTest1 : System.Web.UI.Page
    {
        DataTable dtMain = new DataTable();

        DataColumn dcReading = new DataColumn();
        DataColumn dcBusinessUnit = new DataColumn("Business Unil");
        DataColumn dcTotal = new DataColumn("Total", typeof(int));

        DataSet dsTop = new DataSet();
        DataTable dtTop = new DataTable();
        DataColumn dcTop = new DataColumn();

        protected void Page_Load(object sender, EventArgs e)
        {
            getdata();
        }

        public void getdata()
        {
            string cmd = "select ClassID as TariffID,ClassName AS Tariff from Tbl_MTariffClasses where ClassID IN(2,3,4,5) AND IsActiveClass=1 ";
            cmd += "SELECT ReadCodeId,ReadCode,DisplayCode FROM Tbl_MReadCodes WHERE ActiveStatusId=1 ";
            cmd += "select C.ClassName,D.DisplayCode,A.ReadCodeId,A.TariffId,A.BU_ID,COUNT(0) AS TOTAL from Tbl_CustomerDetails AS A JOIN Tbl_BussinessUnits AS B ON A.BU_ID=B.BU_ID JOIN Tbl_MTariffClasses AS C ON A.TariffId=C.ClassID JOIN Tbl_MReadCodes AS D ON A.ReadCodeId=D.ReadCodeId where A.TariffId IN(2,3,4,5) GROUP BY A.BU_ID,A.ReadCodeId,A.TariffId,D.DisplayCode,C.ClassName order by A.BU_ID";

            SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["Sqlcon"].ToString());
            SqlDataAdapter da = new SqlDataAdapter(cmd, con);
            da.Fill(dsTop);
            //createColumns(ds.Tables[0], ds.Tables[1]);
            createColumns(dsTop.Tables[0]);
        }

        public void createColumns(DataTable dtTariff, DataTable dtReadCode)
        {
            dtMain.Columns.Add(dcBusinessUnit);
            int count = 1;
            foreach (DataRow drTariff in dtTariff.Rows)
            {

                foreach (DataRow drReadCode in dtReadCode.Rows)
                {
                    dcReading = new DataColumn(drTariff["ClassName"].ToString() + "-" + count.ToString());
                    dtMain.Columns.Add(dcReading);
                    count++;
                }
            }
            dtMain.Columns.Add(dcTotal);
            AddReadCode(dtReadCode);


        }

        public void AddReadCode(DataTable dtReadCode)
        {
            int count = dtMain.Columns.Count;
            int countindex = 0;
            DataRow drMain = dtMain.NewRow();


            for (int i = 0; i < count; i += dtReadCode.Rows.Count)
            {
                for (int j = 0; j < dtReadCode.Rows.Count; j++)
                {
                    drMain[countindex] = dtReadCode.Rows[j]["DisplayCode"];
                    countindex++;
                }
            }
            dtMain.Rows.Add(drMain);
            MergeCol(dtReadCode);
        }

        public void MergeCol(DataTable dtReadCode)
        {
            int count = dtMain.Columns.Count;
            for (int i = 0; i < count; i += dtReadCode.Rows.Count)
            {
                for (int j = 0; j < dtReadCode.Rows.Count; j++)
                {
                    if (j > 0)
                    {
                        dtMain.Columns.RemoveAt(i);
                    }
                }
            }
        }


        //Creating Columns
        public void createColumns(DataTable dt)
        {
            dtTop = new DataTable();
            int count = 1;
            //dcTop = new DataColumn();
            //dcTop.ColumnName = "BussinessUnit";
            //dtTop.Columns.Add(dcTop);
            for (int i = 0; i < dt.Rows.Count * 2; i++)
            {
                dcTop = new DataColumn();
                dcTop.ColumnName = "Column" + count;
                dtTop.Columns.Add(dcTop);
                count++;

            }
            //dcTop = new DataColumn();
            //dcTop.ColumnName = "Total";
            //dtTop.Columns.Add(dcTop);
            AddTariff();
            AddReadCode();

        }

        public void AddTariff()
        {
            int count = dtTop.Columns.Count;
            int countindex = 0;
            DataRow drTop = dtTop.NewRow();
            DataTable dtTariff = dsTop.Tables[0];
            DataTable dtReadCode = dsTop.Tables[1];
            int countReadCode = 0;
            for (int i = 0; i < dtTop.Columns.Count; )
            {
                for (int j = 0; j < dtReadCode.Rows.Count; j++)
                {
                    drTop[countindex] = dtTariff.Rows[countReadCode]["Tariff"];
                    countindex++;
                    i++;
                }
                countReadCode++;
            }
            dtTop.Rows.Add(drTop);
        }

        public void MergeTariff()
        {
            foreach (DataColumn dc in dtTop.Columns)
            {

            }
        }

        public void AddReadCode()
        {
            int count = dtTop.Columns.Count;
            int countindex = 0;
            DataRow drTop = dtTop.NewRow();
            DataTable dtReadCode = dsTop.Tables[1];

            for (int i = 0; i < count; i += dtReadCode.Rows.Count)
            {
                for (int j = 0; j < dtReadCode.Rows.Count; j++)
                {
                    drTop[countindex] = dtReadCode.Rows[j]["DisplayCode"];
                    countindex++;
                }
            }
            dtTop.Rows.Add(drTop);
            dcTop = new DataColumn("BusinessUnit", typeof(string));
            dtTop.Columns.Add(dcTop);
            dcTop = new DataColumn("Total");
            dtTop.Columns.Add(dcTop);
            dtTop.Columns["BusinessUnit"].SetOrdinal(0);
            //MergeTariff();
            InsertData();
            gvTReport.DataSource = dtTop;
            gvTReport.DataBind();
        }

        public void InsertData()
        {
            int countIndex = 0;
            int countIndex1 = 0;
            int countColumn = dsTop.Tables[1].Rows.Count * 2;//Tariff Table
            DataRow drTariff = dtTop.Rows[0];//Get row where tariff has added
            DataRow drDisplayCode = dtTop.Rows[1];
            string bu = "";
            int countless = 0;
            foreach (DataRow dr in dsTop.Tables[2].Rows)//Loop for each row in result table
            {

                string buNext = dr["BU_ID"].ToString();
                if (!bu.Equals(buNext))//This loop will insert business unit once
                {
                    DataRow drTop = dtTop.NewRow();
                    //Insert business unit and total start


                    drTop[0] = dsTop.Tables[2].Rows[countIndex]["BU_ID"];
                    bu = dsTop.Tables[2].Rows[countIndex]["BU_ID"].ToString();
                    //drTop[dtTop.Columns.Count - 1] = dsTop.Tables[2].Rows[countIndex]["Total"];

                    //Insert business unit and total end


                    //If insertin then check

                    //Insert total values start

                    int contingfrom1 = 1;

                    //for (int i = 1; i < dtTop.Columns.Count - 2; )//Loop for repeated tariff in dtTop
                    for (int i = 1; i < drTariff.ItemArray.Count() - 1; i += dsTop.Tables[1].Rows.Count)//Loop for repeated tariff in dtTop
                    {
                        string s44 = drTariff[i].ToString();
                        string s55 = dsTop.Tables[2].Rows[countless]["ClassName"].ToString();
                        if (s44 == s55)
                        {
                            int conting = 0;
                            for (int j = 1; j <= dsTop.Tables[1].Rows.Count; j++)
                            {
                                string s99 = drDisplayCode[j].ToString();
                                string s88 = dsTop.Tables[2].Rows[countIndex]["DisplayCode"].ToString();
                                if (s99 == s88)
                                {
                                    //DataRow dr1Top = dtTop.NewRow();
                                    drTop[i] = dsTop.Tables[2].Rows[countless]["Total"];
                                    //break;
                                    //dtTop.Rows.Add(drTop);
                                }

                                contingfrom1++;
                            }
                        }
                       
                        countless++;
                    }
                    //Insert taotal values end
                    countIndex++;
                    dtTop.Rows.Add(drTop);
                }



            }


        }
    }
}