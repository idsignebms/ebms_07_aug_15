﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for Tariff Change Logs
                     
 Developer        : Id027-Karthik Thati
 Creation Date    : 08-10-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using Resources;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Data;
using org.in2bits.MyXls;
using System.Configuration;

namespace UMS_Nigeria.Reports
{
    public partial class TariffChangeLogs : System.Web.UI.Page
    {
        #region Members
        iDsignBAL objIdsignBal = new iDsignBAL();
        ReportsBal objReportsBal = new ReportsBal();
        CommonMethods objCommonMethods = new CommonMethods();
        string Key = UMS_Resource.KEY_TariffChangeLogs;
        DataTable dt = new DataTable();
        #endregion
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStartsReport : Convert.ToInt32(hfPageSize.Value); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                try
                {                    
                    trBU.Visible = trSU.Visible = trSC.Visible = trCycle.Visible = trBook.Visible = trTariff.Visible = trFD.Visible = trTD.Visible = false;
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    BindTariffChangeLogs();
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    UCPaging2.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                }
                catch (Exception ex)
                {
                    Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                    try
                    {
                        objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                    }
                    catch (Exception logex)
                    {

                    }
                }
            }
        }

        private void BindTariffChangeLogs()
        {
            try
            {
                if ((Session[UMS_Resource.AUDIT_SESSION_DT] != null) && (Request.QueryString[UMS_Resource.FROM_PAGE] != null))
                {
                    DataTable dt = (DataTable)Session[UMS_Resource.AUDIT_SESSION_DT];
                    //Display Start
                    if (!string.IsNullOrEmpty(dt.Rows[0]["BUName"].ToString())) { trBU.Visible = true; lblBU.Text = dt.Rows[0]["BUName"].ToString(); }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["SUName"].ToString())) { trSU.Visible = true; lblSU.Text = dt.Rows[0]["SUName"].ToString(); }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["SCName"].ToString())) { trSC.Visible = true; lblSC.Text = dt.Rows[0]["SCName"].ToString(); }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["CycleName"].ToString())) { trCycle.Visible = true; lblCycle.Text = dt.Rows[0]["CycleName"].ToString(); }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["BookName"].ToString())) { trBook.Visible = true; lblBook.Text = dt.Rows[0]["BookName"].ToString(); }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["TariffName"].ToString())) { trTariff.Visible = true; lblTariff.Text = dt.Rows[0]["TariffName"].ToString(); }
                    if (!string.IsNullOrEmpty(objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.FROM_PAGE])))
                    {
                        trFD.Visible = true;
                        lblFromDate.Text = objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.FROM_PAGE]);
                    }
                    if (!string.IsNullOrEmpty(objIdsignBal.Decrypt(Request.QueryString["To"])))
                    {
                        trTD.Visible = true;
                        lblToDate.Text = objIdsignBal.Decrypt(Request.QueryString["To"]);
                    }
                    //Display End

                    RptAuditTrayListBe objRptAuditTrayList = new RptAuditTrayListBe();
                    RptAuditTrayBe objRptAuditTrayBe = new RptAuditTrayBe();
                    objRptAuditTrayBe.BU_ID = dt.Rows[0]["BU_ID"].ToString();
                    objRptAuditTrayBe.SU_ID = dt.Rows[0]["SU_ID"].ToString();
                    objRptAuditTrayBe.SC_ID = dt.Rows[0]["SC_ID"].ToString();
                    objRptAuditTrayBe.CycleId = dt.Rows[0]["CYCLE_ID"].ToString();
                    objRptAuditTrayBe.NewBookNo = dt.Rows[0]["BOOK_ID"].ToString();
                    objRptAuditTrayBe.ClassId = dt.Rows[0]["TARIFF_ID"].ToString();
                    objRptAuditTrayBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                    objRptAuditTrayBe.PageSize = PageSize;
                    if (!string.IsNullOrEmpty(objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.FROM_PAGE])))
                        objRptAuditTrayBe.FromDate = objCommonMethods.Convert_MMDDYY(objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.FROM_PAGE]));
                    else
                        objRptAuditTrayBe.FromDate = string.Empty;
                    if (!string.IsNullOrEmpty(objIdsignBal.Decrypt(Request.QueryString["To"])))
                        objRptAuditTrayBe.ToDate = objCommonMethods.Convert_MMDDYY(objIdsignBal.Decrypt(Request.QueryString["To"]));
                    else
                        objRptAuditTrayBe.ToDate = string.Empty;

                    DataSet ds = new DataSet();
                    ds = objReportsBal.GetLogsList(objRptAuditTrayBe, ReturnType.Multiple);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        divrptrdetails.Visible = true;
                        rptrTariffChanges.DataSource = ds.Tables[0];
                        //MergeRows(gvTariffChanges);
                        rptrTariffChanges.DataBind();
                        lblTopTotalCustomers.Text = lblDownTotalCustomers.Text = hfTotalRecords.Value = ds.Tables[0].Rows[0]["TotalChanges"].ToString();
                    }
                    else
                    {
                        divrptrdetails.Visible = false;
                        hfTotalRecords.Value = Constants.Zero.ToString();
                        rptrTariffChanges.DataSource = new DataTable();
                        rptrTariffChanges.DataBind();
                    }
                }
                else
                {
                    Response.Redirect(Constants.AuditTrayReportPage, false);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        public static void MergeRows(GridView gridView)
        {
            for (int rowIndex = gridView.Rows.Count - 2; rowIndex >= 0; rowIndex--)
            {
                GridViewRow row = gridView.Rows[rowIndex];
                GridViewRow previousRow = gridView.Rows[rowIndex + 1];

                Label lblAccountNo = (Label)row.FindControl("lblAccount");
                Label lblPreviousAccountNo = (Label)previousRow.FindControl("lblAccount");
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    if (i == 0 || i == 1 || i == 2)
                    {
                        if (lblAccountNo.Text == lblPreviousAccountNo.Text)
                        {
                            row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                            previousRow.Cells[i].Visible = false;
                        }
                    }
                }
            }
        }
        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        protected void lbtnBack_Click(object sender, EventArgs e)
        {
            //Response.Redirect(Constants.AuditTrayReportPage + Request.Url.Query, false);
            Response.Redirect(Constants.AuditTrayReportPage, false);
        }

        #region ExportExcel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if ((Session[UMS_Resource.AUDIT_SESSION_DT] != null) && (Request.QueryString[UMS_Resource.FROM_PAGE] != null))
                {
                    DataTable dt = (DataTable)Session[UMS_Resource.AUDIT_SESSION_DT];
                    RptAuditTrayListBe objRptAuditTrayList = new RptAuditTrayListBe();
                    RptAuditTrayBe objRptAuditTrayBe = new RptAuditTrayBe();
                    objRptAuditTrayBe.BU_ID = dt.Rows[0]["BU_ID"].ToString();
                    objRptAuditTrayBe.SU_ID = dt.Rows[0]["SU_ID"].ToString();
                    objRptAuditTrayBe.SC_ID = dt.Rows[0]["SC_ID"].ToString();
                    objRptAuditTrayBe.CycleId = dt.Rows[0]["CYCLE_ID"].ToString();
                    objRptAuditTrayBe.ClassId = dt.Rows[0]["TARIFF_ID"].ToString();
                    objRptAuditTrayBe.NewBookNo = dt.Rows[0]["BOOK_ID"].ToString();
                    objRptAuditTrayBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                    objRptAuditTrayBe.PageSize = Convert.ToInt32(hfTotalRecords.Value);
                    if (!string.IsNullOrEmpty(objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.FROM_PAGE])))
                        objRptAuditTrayBe.FromDate = objCommonMethods.Convert_MMDDYY(objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.FROM_PAGE]));
                    else
                        objRptAuditTrayBe.FromDate = string.Empty;
                    if (!string.IsNullOrEmpty(objIdsignBal.Decrypt(Request.QueryString["To"])))
                        objRptAuditTrayBe.ToDate = objCommonMethods.Convert_MMDDYY(objIdsignBal.Decrypt(Request.QueryString["To"]));
                    else
                        objRptAuditTrayBe.ToDate = string.Empty;

                    DataSet ds = new DataSet();
                    ds = objReportsBal.GetLogsList(objRptAuditTrayBe, ReturnType.Multiple);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        var BUlist = (from list in ds.Tables[0].AsEnumerable()
                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                      group list by list.Field<string>("BusinessUnitName") into g
                                      select new { BusinessUnit = g.Key }).ToList();

                        if (BUlist.Count() > 0)
                        {
                            XlsDocument xls = new XlsDocument();
                            Worksheet mainSheet = xls.Workbook.Worksheets.Add("Summary");
                            Cells mainCells = mainSheet.Cells;
                            Cell _objMainCells = null;

                            _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
                            mainCells.Add(4, 2, "Report Code ");
                            mainCells.Add(4, 3, Constants.TariffChangeLogReport_Code).Font.Bold = true;
                            mainCells.Add(4, 4, "Report Name");
                            mainCells.Add(4, 5, Constants.TariffChangeLogReport_Name).Font.Bold = true;
                            mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
                            _objMainCells.Font.Weight = FontWeight.ExtraBold;
                            _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                            _objMainCells.Font.FontFamily = FontFamilies.Roman;

                            int MainSheetDetailsRowNo = 6;
                            mainCells.Add(MainSheetDetailsRowNo, 1, "S.No.").Font.Bold = true;
                            mainCells.Add(MainSheetDetailsRowNo, 2, "Service Center").Font.Bold = true;
                            mainCells.Add(MainSheetDetailsRowNo, 3, "Customer Count").Font.Bold = true;

                            var SClist = (from list in ds.Tables[0].AsEnumerable()
                                          orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                          orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                          group list by list.Field<string>("ServiceCenterName") into g
                                          select new { ServiceCenter = g.Key }).ToList();

                            for (int k = 0; k < SClist.Count; k++)//SC
                            {
                                var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                 where list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                                 select list).ToList();

                                MainSheetDetailsRowNo++;
                                mainCells.Add(MainSheetDetailsRowNo, 1, k + 1);
                                mainCells.Add(MainSheetDetailsRowNo, 2, SClist[k].ServiceCenter);
                                mainCells.Add(MainSheetDetailsRowNo, 3, Customers.Count);
                            }


                            for (int i = 0; i < BUlist.Count; i++)//BU
                            {
                                var SUlistByBU = (from list in ds.Tables[0].AsEnumerable()
                                                  where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                  orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                  group list by list.Field<string>("ServiceUnitName") into g
                                                  select new { ServiceUnitName = g.Key }).ToList();

                                for (int j = 0; j < SUlistByBU.Count; j++)//SU
                                {
                                    var SClistBySU = (from list in ds.Tables[0].AsEnumerable()
                                                      where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                      && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                      orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                      group list by list.Field<string>("ServiceCenterName") into g
                                                      select new { ServiceCenter = g.Key }).ToList();

                                    for (int k = 0; k < SClistBySU.Count; k++)//SC
                                    {
                                        var CyclelistBySC = (from list in ds.Tables[0].AsEnumerable()
                                                             where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                             && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                             && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                             orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                             orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                             group list by list.Field<string>("CycleName") into g
                                                             select new { CycleName = g.Key }).ToList();

                                        if (CyclelistBySC.Count > 0)
                                        {
                                            int DetailsRowNo = 2;
                                            int CustomersRowNo = 6;

                                            Worksheet sheet = xls.Workbook.Worksheets.Add(SClistBySU[k].ServiceCenter);
                                            Cells cells = sheet.Cells;
                                            Cell _objCell = null;

                                            for (int l = 0; l < CyclelistBySC.Count; l++)//Cycle
                                            {
                                                var BooklistByCycle = (from list in ds.Tables[0].AsEnumerable()
                                                                       where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                       && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                       && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                       && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                       orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                       orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                       group list by list.Field<string>("BookNumber") into g
                                                                       select new { BookNumber = g.Key }).ToList();

                                                for (int m = 0; m < BooklistByCycle.Count; m++)//Book
                                                {
                                                    var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                                     where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                     && list.Field<string>("ServiceUnitName") == SUlistByBU[j].ServiceUnitName
                                                                     && list.Field<string>("ServiceCenterName") == SClistBySU[k].ServiceCenter
                                                                     && list.Field<string>("CycleName") == CyclelistBySC[l].CycleName
                                                                     && list.Field<string>("BookNumber") == BooklistByCycle[m].BookNumber
                                                                     orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["BookSortOrder"])) ascending
                                                                     orderby objCommonMethods.IsNullValue_Int(Convert.ToString(list["CustomerSortOrder"])) ascending
                                                                     orderby Convert.ToString(list["CycleName"]) ascending
                                                                     select list).ToList();

                                                    if (Customers.Count() > 0)
                                                    {
                                                        cells.Add(DetailsRowNo, 1, "Business Unit");
                                                        cells.Add(DetailsRowNo, 3, "Service Unit");
                                                        cells.Add(DetailsRowNo, 5, "Service Center");
                                                        cells.Add(DetailsRowNo + 1, 1, "Book Group");
                                                        cells.Add(DetailsRowNo + 1, 3, "Book Name");

                                                        cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                                        cells.Add(DetailsRowNo, 4, SUlistByBU[j].ServiceUnitName).Font.Bold = true;
                                                        cells.Add(DetailsRowNo, 6, SClistBySU[k].ServiceCenter).Font.Bold = true;
                                                        cells.Add(DetailsRowNo + 1, 2, CyclelistBySC[l].CycleName).Font.Bold = true;
                                                        cells.Add(DetailsRowNo + 1, 4, BooklistByCycle[m].BookNumber).Font.Bold = true;

                                                        _objCell = cells.Add(DetailsRowNo + 3, 1, "Sno");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 2, "Global Account No");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 3, "Old Account No");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 4, "Name");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 5, "Old Tariff");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 6, "New Tariff");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 7, "Old Cluster Type");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 8, "New Cluster Type");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 9, "Requested By");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 10, "Requested Date");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 11, "Last Approved By");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 12, "Last Approved Date");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 13, "Approval Status");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 14, "Last Transaction Date");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;
                                                        _objCell = cells.Add(DetailsRowNo + 3, 15, "Remarks");
                                                        _objCell.Font.Weight = FontWeight.ExtraBold;
                                                        _objCell.HorizontalAlignment = HorizontalAlignments.Centered;
                                                        _objCell.Font.FontFamily = FontFamilies.Roman;

                                                        for (int n = 0; n < Customers.Count; n++)
                                                        {
                                                            DataRow dr = (DataRow)Customers[n];
                                                            cells.Add(CustomersRowNo, 1, (n + 1).ToString());
                                                            cells.Add(CustomersRowNo, 2, dr["AccountNo"].ToString());
                                                            cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
                                                            cells.Add(CustomersRowNo, 4, dr["Name"].ToString());
                                                            cells.Add(CustomersRowNo, 5, dr["OldTariff"].ToString());
                                                            cells.Add(CustomersRowNo, 6, dr["NewTariff"].ToString());
                                                            cells.Add(CustomersRowNo, 7, dr["OldCluster"].ToString());
                                                            cells.Add(CustomersRowNo, 8, dr["NewCluster"].ToString());
                                                            cells.Add(CustomersRowNo, 9, dr["CreatedBy"].ToString());
                                                            cells.Add(CustomersRowNo, 10, dr["TransactionDate"].ToString());
                                                            cells.Add(CustomersRowNo, 11, dr["ModifiedBy"].ToString());
                                                            cells.Add(CustomersRowNo, 12, dr["ModifiedDate"].ToString());
                                                            cells.Add(CustomersRowNo, 13, dr["ApprovalStatus"].ToString());
                                                            cells.Add(CustomersRowNo, 14, dr["LastTransactionDate"].ToString());
                                                            cells.Add(CustomersRowNo, 15, objIdsignBal.ReplaceNewLines(dr["Remarks"].ToString(),false));

                                                            CustomersRowNo++;
                                                        }
                                                        DetailsRowNo = CustomersRowNo + 2;
                                                        CustomersRowNo = CustomersRowNo + 6;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                            fileName = Constants.TariffChangeLogReport_Name.Replace(" ", string.Empty) + "_" + fileName;
                            string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                            xls.FileName = filePath;
                            xls.Save();
                            objIdsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                        }
                        else
                            Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
                    }
                    else
                        Message(Resource.CUSTOMERS_NOT_FOUND, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}