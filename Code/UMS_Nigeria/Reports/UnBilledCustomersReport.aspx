﻿<%@ Page Title="::UnBilled Customers Report::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="UnBilledCustomersReport.aspx.cs"
    Inherits="UMS_Nigeria.Reports.UnBilledCustomersReport" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPaging" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server" AssociatedUpdatePanelID="upnlCustomerReorder">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <div class="inner-sec">
            <asp:UpdatePanel ID="upnlCustomerReorder" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddCycle" runat="server" Text="UnBilled Customers Report"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="col_bg_l">
                            <div class="col_a">
                                <label for="name">
                                    <asp:Literal ID="litFromDate" runat="server" Text="<%$ Resources:Resource, FROM%>"></asp:Literal>
                                </label>
                            </div>
                            <div class="col_b">
                                <asp:DropDownList ID="ddlFromMonth" CssClass="text_selectbb" runat="server">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col_c">
                                <asp:DropDownList ID="ddlFromYear" CssClass="text_selectbb" runat="server">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="col_bg_l">
                            <div class="col_a">
                                <label for="name">
                                    <asp:Literal ID="litToDate" runat="server" Text="<%$ Resources:Resource, TO%>"></asp:Literal>
                                </label>
                            </div>
                            <div class="col_b">
                                <asp:DropDownList ID="ddlToMonth" CssClass="text_selectbb" runat="server">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col_c">
                                <asp:DropDownList ID="ddlToYear" CssClass="text_selectbb" runat="server">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:DropDownList ID="ddlBU" AutoPostBack="true" CssClass="text_selectab" runat="server"
                                    OnSelectedIndexChanged="ddlBU_SelectedIndexChanged">
                                    <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <span id="spanddlBU" class="spancolor"></span>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="divServiceUnits" runat="server" visible="false">
                            <div class="check_baa">
                                <div class="text-inner_a">
                                    <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" ID="ChkSUAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                        <asp:CheckBoxList ID="cblServiceUnits" class="text-inner-b" AutoPostBack="true" RepeatDirection="Horizontal"
                                            RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceUnits_SelectedIndexChanged">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="divServiceCenters" runat="server" visible="false">
                            <div class="check_baa">
                                <div class="text-inner_a">
                                    <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" ID="chkSCAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                        <asp:CheckBoxList ID="cblServiceCenters" class="text-inner-b" RepeatDirection="Horizontal"
                                            AutoPostBack="true" RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceCenters_SelectedIndexChanged">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="divCycles" runat="server" visible="false">
                            <div class="check_baa">
                                <div class="text-inner_a">
                                    <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" class="text-inner-b" ID="chkCycleAll" Text="All" AutoPostBack="true" />
                                        <asp:CheckBoxList ID="cblCycles" class="text-inner-b" AutoPostBack="true" RepeatDirection="Horizontal"
                                            RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblCycles_SelectedIndexChanged">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="divBookNos" runat="server" visible="false">
                            <div class="check_baa">
                                <div class="text-inner_a">
                                    <asp:Literal ID="litBookNo" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" class="text-inner-b" ID="chkBooksAll" Text="All" />
                                        <asp:CheckBoxList ID="cblBooks" class="text-inner-b" RepeatDirection="Horizontal"
                                            RepeatColumns="3" runat="server">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <asp:Button ID="btnExport" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s"
                                runat="server" OnClick="btnExport_Click" />
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div id="divPrint" runat="server" visible="false" style="float:right; margin-right:12px;">
                        <asp:Button ID="btnExportExcel" runat="server" CssClass="excel_btn" Text="Export to excel"
                            OnClick="btnExportExcel_Click" />
                        <asp:Button ID="btnPrint" runat="server" OnClientClick="return printGrid()" CssClass="print_btn"
                            Text="Print" />
                    </div>
                    <div class="grid_boxes">
                        <div class="grid_paging_top">
                            <div class="paging_top_title">
                                &nbsp;
                            </div>
                            <div class="paging_top_right_content" id="divpaging" runat="server">
                                <uc1:UCPaging ID="UCPaging1" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvUnBilledCustomers" runat="server" AutoGenerateColumns="False"
                            AlternatingRowStyle-CssClass="color" HeaderStyle-CssClass="grid_tb_hd " OnRowDataBound="gvUnBilledCustomers_RowDataBound">
                            <EmptyDataRowStyle HorizontalAlign="Left" CssClass="color" />
                            <EmptyDataTemplate>
                                There is no data.
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:Resource, GRID_SNO%>"
                                    runat="server">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderStyle-HorizontalAlign="Center" HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>"
                                    DataField="AccountNo" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, OLD_ACCOUNT_NO%>" DataField="OldAccountNo" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="grid_tb_td"
                                    HeaderText="<%$ Resources:Resource, NAME%>" DataField="Name" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="grid_tb_td"
                                    HeaderText="<%$ Resources:Resource, ADDRESS%>" DataField="Address" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Center" HeaderText="<%$ Resources:Resource, LAST_BILL_GENERATED_DATE%>"
                                    DataField="LastBilledDate" />
                                <%-- <asp:BoundField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, BILL_AMOUNT%>" DataField="BillAmountWithoutArrears" />--%>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, BILL_AMOUNT%>" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBillAmountWithoutArrears" runat="server" Text='<%#Eval("BillAmountWithoutArrears") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:BoundField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, LAST_AMOUNTPAID%>" DataField="LastPaymentAmount" />--%>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, LAST_AMOUNTPAID%>" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLastPaymentAmount" runat="server" Text='<%#Eval("LastPaymentAmount") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, LastAdjustmentDate%>" DataField="LastAdjustmentDate" />
                                <asp:BoundField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, BillAdjustmentType%>" DataField="AdjustmentType" />
                                <%--<asp:BoundField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, LastAdjustmentAmount%>" DataField="LastAdjustmentAmount" />--%>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, LastAdjustmentAmount%>" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLastAdjustmentAmount" runat="server" Text='<%#Eval("LastAdjustmentAmount") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                    HeaderText="<%$ Resources:Resource, TARIFF%>" DataField="Tariff" />
                            </Columns>
                            <RowStyle HorizontalAlign="center"></RowStyle>
                        </asp:GridView>
                        <asp:HiddenField ID="hfPageSize" runat="server" />
                        <asp:HiddenField ID="hfPageNo" runat="server" />
                        <asp:HiddenField ID="hfLastPage" runat="server" />
                        <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                    </div>
                    <div class="grid_boxes">
                        <div id="divdownpaging" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPaging ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExportExcel" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
            TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
        </asp:ModalPopupExtender>
        <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
        <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
            <div class="popheader popheaderlblred">
                <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
            </div>
            <div class="footer popfooterbtnrt">
                <div class="fltr popfooterbtn">
                    <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                        CssClass="ok_btn" />
                </div>
                <div class="clear pad_5">
                </div>
            </div>
        </asp:Panel>
    </div>
    <%--==============Escape button script starts==============--%>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/UnBilledCustomersReport.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
