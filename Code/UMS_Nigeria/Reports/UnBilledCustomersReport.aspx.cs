﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddEmployee
                     
 Developer        : id027-T.Karthik
 Creation Date    : 05-11-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBAL;
using Resources;
using UMS_NigeriaBE;
using System.Data;
using org.in2bits.MyXls;
using System.Configuration;
using UMS_Nigeria.UserControls;

namespace UMS_Nigeria.Reports
{


    public partial class UnBilledCustomersReport : System.Web.UI.Page
    {

        # region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        ReportsBal objReportsBal = new ReportsBal();
        public int PageNum;
        string Key = "UnBilledCustomersReport";
        #endregion

        #region Properties

        public string BU
        {
            get { return string.IsNullOrEmpty(ddlBU.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlBU, ",") : ddlBU.SelectedValue; }
            set { ddlBU.SelectedValue = value.ToString(); }
        }
        public string SU
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceUnits, ","); }
        }
        public string SC
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceCenters, ","); }
        }
        public string Cycle
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblCycles, ","); }
        }
        public string BookNo
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblBooks, ","); }
        }
        public int FromMonth
        {
            get { return string.IsNullOrEmpty(ddlFromMonth.SelectedValue) ? 0 : Convert.ToInt32(ddlFromMonth.SelectedValue); }
        }
        public int ToMonth
        {
            get { return string.IsNullOrEmpty(ddlToMonth.SelectedValue) ? 0 : Convert.ToInt32(ddlToMonth.SelectedValue); }
        }
        public int FromYear
        {
            get { return string.IsNullOrEmpty(ddlFromYear.SelectedValue) ? 0 : Convert.ToInt32(ddlFromYear.SelectedValue); }
        }
        public int ToYear
        {
            get { return string.IsNullOrEmpty(ddlToYear.SelectedValue) ? 0 : Convert.ToInt32(ddlToYear.SelectedValue); }
        }

        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        //public int GoPageNo
        //{
        //    get { return string.IsNullOrEmpty(ddlGoPage.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlGoPage.SelectedValue); }
        //}
        //public int GoPageNoBtm
        //{
        //    get { return string.IsNullOrEmpty(ddlGoPageBtm.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlGoPageBtm.SelectedValue); }
        //}

        #endregion

        #region Methods
        private void BindYears()
        {
            try
            {
                int CurrentYear = DateTime.Now.Year;
                int FromYear = Convert.ToInt32(GlobalConstants.TARIFF_ENTRY_NO_OF_LAST_YEARS);
                for (int i = FromYear; i < CurrentYear; i++)
                {
                    ListItem item = new ListItem();
                    item.Text = item.Value = i.ToString();
                    ddlFromYear.Items.Add(item);
                    ddlToYear.Items.Add(item);
                }
                for (int i = 0; i <= Convert.ToInt32(1); i++)
                {
                    ListItem item = new ListItem();
                    item.Text = item.Value = (CurrentYear + i).ToString();
                    ddlFromYear.Items.Add(item);
                    ddlToYear.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindMonths()
        {
            try
            {
                DataSet dsMonths = new DataSet();
                dsMonths.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["Months"]));
                _objiDsignBal.FillDropDownList(dsMonths.Tables[0], ddlFromMonth, "name", "value", UMS_Resource.DROPDOWN_SELECT, false);
                _objiDsignBal.FillDropDownList(dsMonths.Tables[0], ddlToMonth, "name", "value", UMS_Resource.DROPDOWN_SELECT, false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void CheckBoxesJS()
        {
            ChkSUAll.Attributes.Add("onclick", "CheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            chkSCAll.Attributes.Add("onclick", "CheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            chkCycleAll.Attributes.Add("onclick", "CheckAll('" + chkCycleAll.ClientID + "','" + cblCycles.ClientID + "')");
            chkBooksAll.Attributes.Add("onclick", "CheckAll('" + chkBooksAll.ClientID + "','" + cblBooks.ClientID + "')");

            cblServiceUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            cblServiceCenters.Attributes.Add("onclick", "UnCheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            cblCycles.Attributes.Add("onclick", "UnCheckAll('" + chkCycleAll.ClientID + "','" + cblCycles.ClientID + "')");
            cblBooks.Attributes.Add("onclick", "UnCheckAll('" + chkBooksAll.ClientID + "','" + cblBooks.ClientID + "')");
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void BindGrid()
        {
            try
            {
                RptCheckMetersBe objReportBe = new RptCheckMetersBe();
                objReportBe.BU_ID = BU;
                objReportBe.SU_ID = SU;
                objReportBe.ServiceCenterId = SC;
                objReportBe.CycleId = Cycle;
                objReportBe.BookNo = BookNo;
                objReportBe.FromMonth = FromMonth;
                objReportBe.ToMonth = ToMonth;
                objReportBe.FromYear = FromYear;
                objReportBe.ToYear = ToYear;
                objReportBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                objReportBe.PageSize = PageSize;

                DataSet ds = new DataSet();
                ds = objReportsBal.GetUnBilledCustomersReport(objReportBe);

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    divpaging.Visible = divdownpaging.Visible = divgrid.Visible = true;
                    int totalRecord = Convert.ToInt32(ds.Tables[0].Rows[0]["TotalRecords"]);
                    hfTotalRecords.Value = totalRecord.ToString();

                    gvUnBilledCustomers.DataSource = ds.Tables[0];
                    gvUnBilledCustomers.DataBind();
                }
                else
                {
                    divPrint.Visible = divpaging.Visible = divdownpaging.Visible = divgrid.Visible = false;
                    hfTotalRecords.Value = Constants.Zero.ToString();
                    Message("Customers Not Found", UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvUnBilledCustomers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblBillAmountWithoutArrears = (Label)e.Row.FindControl("lblBillAmountWithoutArrears");
                    lblBillAmountWithoutArrears.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblBillAmountWithoutArrears.Text), 2, Constants.MILLION_Format);
                    Label lblLastPaymentAmount = (Label)e.Row.FindControl("lblLastPaymentAmount");
                    lblLastPaymentAmount.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblLastPaymentAmount.Text), 2, Constants.MILLION_Format);
                    Label lblLastAdjustmentAmount = (Label)e.Row.FindControl("lblLastAdjustmentAmount");
                    lblLastAdjustmentAmount.Text = _objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblLastAdjustmentAmount.Text), 2, Constants.MILLION_Format);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region ddlEvents
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                if (Session[UMS_Resource.SESSION_LOGINID] != null)
                {
                    pnlMessage.CssClass = lblMessage.Text = string.Empty;
                    if (!IsPostBack)
                    {
                        string path = string.Empty;
                        _objCommonMethods.BindUserBusinessUnits(ddlBU, string.Empty, false, UMS_Resource.DROPDOWN_SELECT);
                        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        {
                            ddlBU.SelectedIndex = ddlBU.Items.IndexOf(ddlBU.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                            ddlBU.Enabled = false;
                            ddlBU_SelectedIndexChanged(ddlBU, new EventArgs());
                        }
                        CheckBoxesJS();
                        BindMonths();
                        BindYears();

                        hfPageNo.Value = Constants.pageNoValue.ToString();
                        BindGrid();

                        //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));

                    }

                }
                else
                {
                    Response.Redirect(UMS_Resource.DEFAULT_PAGE);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlBU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _objCommonMethods.BindUserServiceUnits_BuIds(cblServiceUnits, BU, Resource.DDL_ALL, false);
                if (cblServiceUnits.Items.Count > 0)
                    divServiceUnits.Visible = true;
                else
                    divServiceUnits.Visible = false;
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                cblBooks.Items.Clear();
                cblServiceCenters.Items.Clear();
                divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = chkSCAll.Checked = divServiceCenters.Visible = false;
                if (!string.IsNullOrEmpty(SU))
                {
                    _objCommonMethods.BindUserServiceCenters_SuIds(cblServiceCenters, SU, Resource.DDL_ALL, false);
                    if (cblServiceCenters.Items.Count > 0)
                        divServiceCenters.Visible = true;
                    else
                        divServiceCenters.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceCenters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                cblBooks.Items.Clear();
                divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = false;
                if (!string.IsNullOrEmpty(SC))
                {
                    _objCommonMethods.BindCyclesByBUSUSC(cblCycles, BU, SU, SC, true, string.Empty, false);
                    if (cblCycles.Items.Count > 0)
                        divCycles.Visible = true;
                    else
                        divCycles.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblCycles_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblBooks.Items.Clear();
                chkBooksAll.Checked = divBookNos.Visible = false;
                if (!string.IsNullOrEmpty(Cycle))
                {
                    _objCommonMethods.BindAllBookNumbers(cblBooks, Cycle, true, false, string.Empty);
                    if (cblBooks.Items.Count > 0)
                        divBookNos.Visible = true;
                    else
                        divBookNos.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            BindGrid();
        }

        private void CustomersNotFound()
        {
            Message("Customers Not Found", UMS_Resource.MESSAGETYPE_ERROR);
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                RptCheckMetersBe objReportBe = new RptCheckMetersBe();
                objReportBe.BU_ID = BU;
                objReportBe.SU_ID = SU;
                objReportBe.ServiceCenterId = SC;
                objReportBe.CycleId = Cycle;
                objReportBe.BookNo = BookNo;
                objReportBe.FromMonth = FromMonth;
                objReportBe.ToMonth = ToMonth;
                objReportBe.FromYear = FromYear;
                objReportBe.ToYear = ToYear;
                objReportBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                objReportBe.PageSize = Convert.ToInt32(hfTotalRecords.Value); ;

                DataSet ds = new DataSet();
                ds = objReportsBal.GetUnBilledCustomersReport(objReportBe);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    var BUlist = (from list in ds.Tables[0].AsEnumerable()
                                  orderby list["CustomerSortOrder"] ascending
                                  orderby list["BookSortOrder"] ascending
                                  orderby list["CycleName"] ascending
                                  group list by list.Field<string>("BusinessUnitName") into g
                                  select new { BusinessUnit = g.Key }).ToList();

                    var SUlist = (from list in ds.Tables[0].AsEnumerable()
                                  orderby list["CustomerSortOrder"] ascending
                                  orderby list["BookSortOrder"] ascending
                                  orderby list["CycleName"] ascending
                                  group list by list.Field<string>("ServiceUnitName") into g
                                  select new { ServiceUnit = g.Key }).ToList();

                    var SClist = (from list in ds.Tables[0].AsEnumerable()
                                  orderby list["CustomerSortOrder"] ascending
                                  orderby list["BookSortOrder"] ascending
                                  orderby list["CycleName"] ascending
                                  group list by list.Field<string>("ServiceCenterName") into g
                                  select new { ServiceCenter = g.Key }).ToList();

                    var Cyclelist = (from list in ds.Tables[0].AsEnumerable()
                                     orderby list["CustomerSortOrder"] ascending
                                     orderby list["BookSortOrder"] ascending
                                     orderby list["CycleName"] ascending
                                     group list by list.Field<string>("CycleName") into g
                                     select new { Cycle = g.Key }).ToList();

                    var Booklist = (from list in ds.Tables[0].AsEnumerable()
                                    orderby list["CustomerSortOrder"] ascending
                                    orderby list["BookSortOrder"] ascending
                                    orderby list["CycleName"] ascending
                                    group list by list.Field<string>("BookCode") into g
                                    select new { Book = g.Key }).ToList();

                    var Yearslist = (from list in ds.Tables[0].AsEnumerable()
                                     orderby list["Year"] ascending
                                     group list by list.Field<int>("Year") into g
                                     select new { Year = g.Key }).ToList();


                    if (BUlist.Count() > 0)
                    {
                        XlsDocument xls = new XlsDocument();
                        Worksheet sheet = xls.Workbook.Worksheets.Add("sheet");
                        Cells cells = sheet.Cells;
                        Cell _objCell = null;
                        _objCell = cells.Add(1, 4, Resource.BEDC + "\n" + Resource.UNBILLED_REPORT_EXCEL_HEAD + DateTime.Today.ToShortDateString());
                        sheet.AddMergeArea(new MergeArea(1, 2, 4, 6));

                        int DetailsRowNo = 4;
                        int CustomersRowNo = 8;

                        //string From = string.IsNullOrEmpty(txtFromDate.Text) ? "--" : txtFromDate.Text;
                        //string To = string.IsNullOrEmpty(txtToDate.Text) ? "--" : txtToDate.Text;

                        //cells.Add(1, 1, "From Date");
                        //cells.Add(1, 4, "To Date");
                        //cells.Add(1, 2, From);
                        //cells.Add(1, 5, To);

                        for (int i = 0; i < BUlist.Count; i++)//BU
                        {
                            for (int j = 0; j < SUlist.Count; j++)//SU
                            {
                                for (int k = 0; k < SClist.Count; k++)//SC
                                {
                                    for (int l = 0; l < Cyclelist.Count; l++)//Cycle
                                    {
                                        for (int m = 0; m < Booklist.Count; m++)//Book
                                        {
                                            for (int n = 0; n < Yearslist.Count; n++)//Years
                                            {
                                                var Monthslist = (from list in ds.Tables[0].AsEnumerable()
                                                                  where list.Field<int>("Year") == Yearslist[n].Year
                                                                  orderby list["MonthNo"] ascending
                                                                  group list by new
                                                                  {
                                                                      MonthNo = list.Field<int>("MonthNo")
                                                                      ,
                                                                      Month = list.Field<string>("Month")
                                                                  } into g
                                                                  select new { MonthNo = g.Key.MonthNo, Month = g.Key.Month }).ToList();

                                                if (Monthslist.Count() > 0)
                                                {
                                                    for (int p = 0; p < Monthslist.Count; p++)//Months
                                                    {
                                                        var Customers = (from list in ds.Tables[0].AsEnumerable()
                                                                         where list.Field<string>("BusinessUnitName") == BUlist[i].BusinessUnit
                                                                         && list.Field<string>("ServiceUnitName") == SUlist[j].ServiceUnit
                                                                         && list.Field<string>("ServiceCenterName") == SClist[k].ServiceCenter
                                                                         && list.Field<string>("CycleName") == Cyclelist[l].Cycle
                                                                         && list.Field<string>("BookCode") == Booklist[m].Book
                                                                         && list.Field<int>("Year") == Yearslist[n].Year
                                                                         && list.Field<int>("MonthNo") == Monthslist[p].MonthNo
                                                                         orderby list["CustomerSortOrder"] ascending
                                                                         select list).ToList();

                                                        if (Customers.Count() > 0)
                                                        {
                                                            cells.Add(DetailsRowNo, 1, "Business Unit");
                                                            cells.Add(DetailsRowNo, 4, "Service Unit");
                                                            cells.Add(DetailsRowNo, 7, "Service Center");
                                                            cells.Add(DetailsRowNo + 1, 1, "BookGroup");
                                                            cells.Add(DetailsRowNo + 1, 4, "Book");
                                                            cells.Add(DetailsRowNo + 2, 1, "Month");
                                                            cells.Add(DetailsRowNo + 2, 4, "Year");

                                                            cells.Add(DetailsRowNo, 2, BUlist[i].BusinessUnit).Font.Bold = true;
                                                            cells.Add(DetailsRowNo, 5, SUlist[j].ServiceUnit).Font.Bold = true;
                                                            cells.Add(DetailsRowNo, 8, SClist[k].ServiceCenter).Font.Bold = true;
                                                            cells.Add(DetailsRowNo + 1, 2, Cyclelist[l].Cycle).Font.Bold = true;
                                                            cells.Add(DetailsRowNo + 1, 5, Booklist[m].Book).Font.Bold = true;
                                                            cells.Add(DetailsRowNo + 2, 2, Monthslist[p].Month).Font.Bold = true;
                                                            cells.Add(DetailsRowNo + 2, 5, Yearslist[n].Year).Font.Bold = true;

                                                            cells.Add(DetailsRowNo + 3, 1, "S.No").Font.Bold = true;
                                                            cells.Add(DetailsRowNo + 3, 2, "Account No").Font.Bold = true;
                                                            cells.Add(DetailsRowNo + 3, 3, "Old Account No").Font.Bold = true;
                                                            cells.Add(DetailsRowNo + 3, 4, "Name").Font.Bold = true;
                                                            cells.Add(DetailsRowNo + 3, 5, "Address").Font.Bold = true;
                                                            cells.Add(DetailsRowNo + 3, 6, "Book Code").Font.Bold = true;
                                                            cells.Add(DetailsRowNo + 3, 7, "OutStanding").Font.Bold = true;
                                                            cells.Add(DetailsRowNo + 3, 8, "Tariff").Font.Bold = true;
                                                            cells.Add(DetailsRowNo + 3, 9, "Last Billed Date").Font.Bold = true;
                                                            cells.Add(DetailsRowNo + 3, 10, "Bill Amount Without Arrears").Font.Bold = true;
                                                            cells.Add(DetailsRowNo + 3, 11, "Last Payment Amount").Font.Bold = true;
                                                            cells.Add(DetailsRowNo + 3, 12, "Last Payment Date").Font.Bold = true;
                                                            cells.Add(DetailsRowNo + 3, 13, "Last Adjustment Date").Font.Bold = true;
                                                            cells.Add(DetailsRowNo + 3, 14, "Adjustment Type").Font.Bold = true;
                                                            cells.Add(DetailsRowNo + 3, 15, "Last Adjustment Amount").Font.Bold = true;

                                                            CustomersRowNo++;
                                                            for (int q = 0; q < Customers.Count; q++)
                                                            {
                                                                DataRow dr = (DataRow)Customers[q];
                                                                cells.Add(CustomersRowNo, 1, (q + 1).ToString());
                                                                cells.Add(CustomersRowNo, 2, dr["AccountNo"].ToString());
                                                                cells.Add(CustomersRowNo, 3, dr["OldAccountNo"].ToString());
                                                                cells.Add(CustomersRowNo, 4, dr["Name"].ToString());
                                                                cells.Add(CustomersRowNo, 5, dr["Address"].ToString());
                                                                cells.Add(CustomersRowNo, 6, dr["BookCode"].ToString());
                                                                cells.Add(CustomersRowNo, 7, dr["OutStanding"].ToString());
                                                                cells.Add(CustomersRowNo, 8, dr["Tariff"].ToString());
                                                                cells.Add(CustomersRowNo, 9, dr["LastBilledDate"].ToString());
                                                                cells.Add(CustomersRowNo, 10, dr["BillAmountWithoutArrears"].ToString());
                                                                cells.Add(CustomersRowNo, 11, dr["LastPaymentAmount"].ToString());
                                                                cells.Add(CustomersRowNo, 12, dr["LastPaymentDate"].ToString());
                                                                cells.Add(CustomersRowNo, 13, dr["LastAdjustmentDate"].ToString());
                                                                cells.Add(CustomersRowNo, 14, dr["AdjustmentType"].ToString());
                                                                cells.Add(CustomersRowNo, 15, dr["LastAdjustmentAmount"].ToString());
                                                                CustomersRowNo++;
                                                            }

                                                            DetailsRowNo = CustomersRowNo + 2;
                                                            CustomersRowNo = CustomersRowNo + 5;
                                                        }
                                                    }
                                                }
                                                else
                                                    CustomersNotFound();
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                        fileName = "UnBilledCustomers" + fileName;
                        string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                        xls.FileName = filePath;
                        xls.Save();
                        _objiDsignBal.DownLoadFile(filePath, "application//vnd.ms-excel");
                    }
                    else
                        CustomersNotFound();
                }
                else
                    CustomersNotFound();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void OnlkbtnNextClick(object sender, EventArgs e)
        {
            BindGrid();
        }

        #endregion

        #region Paging

        /*protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                BindGrid();
               // ddlPageSizeBtm.SelectedIndex = ddlPageSizeBtm.Items.IndexOf(ddlPageSizeBtm.Items.FindByValue(ddlPageSize.SelectedValue.ToString()));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlPageSizeBtm_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                hfPageSize.Value = ddlPageSizeBtm.SelectedItem.Text;
                BindGrid();
             //   ddlPageSize.SelectedIndex = ddlPageSize.Items.IndexOf(ddlPageSize.Items.FindByValue(ddlPageSizeBtm.SelectedValue.ToString()));


            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGrid();
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGrid();
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                BindGrid();
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnFirstBtm_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPreviousBtm_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPageBtm.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGrid();
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNextBtm_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPageBtm.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGrid();
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLastBtm_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                BindGrid();
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));

                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lblTotalPagesBtm.Text = lkbtnLastBtm.CommandArgument = lkbtnLast.CommandArgument = hfLastPage.Value = lblTotalPages.Text = totalpages.ToString();

                _objiDsignBal.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                _objiDsignBal.GridViewPaging(lkbtnNextBtm, lkbtnPreviousBtm, lkbtnFirstBtm, lkbtnLastBtm, currentpage, TotalNoOfRecs);

                lblCurrentPage.Text = hfPageNo.Value;
                lblCurrentPageBtm.Text = hfPageNo.Value;

                if (totalpages == 1)
                {
                    lkbtnFirstBtm.ForeColor
                  = lkbtnFirst.ForeColor
                  = lkbtnLastBtm.ForeColor
                  = lkbtnLast.ForeColor
                  = lkbtnNextBtm.ForeColor
                  = lkbtnNext.ForeColor
                  = lkbtnPreviousBtm.ForeColor
                  = lkbtnPrevious.ForeColor = System.Drawing.Color.Gray;
                    ddlGoPageBtm.Enabled = ddlGoPage.Enabled = false;

                }
                else
                {
                    ddlGoPageBtm.Enabled = ddlGoPage.Enabled = true;
                }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0)
                {
                    lkbtnNextBtm.Enabled
                        = lkbtnNext.Enabled
                        = lkbtnLastBtm.Enabled
                        = lkbtnLast.Enabled = false;
                }
                else
                {
                    lkbtnNextBtm.Enabled
                        = lkbtnNext.Enabled
                        = lkbtnLastBtm.Enabled
                        = lkbtnLast.Enabled = true;

                    lkbtnFirstBtm.ForeColor
                        = lkbtnFirst.ForeColor
                        = lkbtnLastBtm.ForeColor
                        = lkbtnLast.ForeColor
                        = lkbtnNextBtm.ForeColor
                        = lkbtnNext.ForeColor
                        = lkbtnPreviousBtm.ForeColor
                        = lkbtnPreviousBtm.ForeColor
                        = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5");
                }

                BindPageCount(Convert.ToInt32(totalpages));


            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                //_objCommonMethods.BindPagingDropDownListReport(TotalRecords, this.PageSize, ddlPageSize);
                _objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
                _objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSizeBtm);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlGoPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = GoPageNo.ToString();
                BindGrid();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlGoPageBtm_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = GoPageNoBtm.ToString();
                BindGrid();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        public void BindPageCount(int PageCount)
        {
            ddlGoPage.Items.Clear();
            ddlGoPageBtm.Items.Clear();
            for (int i = 1; i <= PageCount; i++)
            {
                ListItem list = new ListItem(i.ToString(), i.ToString());
                ddlGoPage.Items.Add(list);
                ddlGoPageBtm.Items.Add(list);
            }
            ddlGoPage.Items.FindByText(hfPageNo.Value).Selected = true;
        }*/

        #endregion

    }
}