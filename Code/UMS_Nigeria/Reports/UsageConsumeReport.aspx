﻿<%@ Page Title=":: Usage Consume Report ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    AutoEventWireup="true" Theme="Green" CodeBehind="UsageConsumeReport.aspx.cs"
    Inherits="UMS_Nigeria.Reports.UsageConsumeReport" %>

<%@ Register Src="../UserControls/UCPagingV2.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            <asp:Label ID="lblLoading" runat="server" Text="<%$ Resources:Resource, LOADING_TEXT%>"></asp:Label></p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <div class="inner-sec">
            <asp:UpdatePanel ID="upEstimationSetting" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="Literal4" runat="server" Text="Consumption Report"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <%--<asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>--%>
                            &nbsp
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="previous">
                        <a href="ReportsSummary.aspx">< < Back To Summary</a>
                    </div>
                    <%--<div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner" style="width: 430px;">
                                <label for="name">
                                    <asp:Literal ID="Literal3" runat="server" Text="Open Month Details :: "></asp:Literal></label>
                                <asp:Label ID="lblMonthDetails" runat="server" Text="---"></asp:Label>
                            </div>
                        </div>
                    </div>--%>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <asp:Literal ID="litYear" runat="server" Text="<%$ Resources:Resource, YEAR%>"></asp:Literal><span
                                class="span_star">*</span><br />
                            <asp:DropDownList ID="ddlYear" runat="server" CssClass="text-box select_box" 
                            onchange="DropDownlistOnChangelbl(this,'spanddlYear',' Year')" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <br />
                            <span id="spanddlYear" class="span_color"></span>
                        </div>
                    </div>
                    <div class="inner-box2">
                        <div class="text-inner">
                            <asp:Literal ID="litMonth" runat="server" Text="<%$ Resources:Resource, MONTH%>"></asp:Literal><span
                                class="span_star">*</span><br />
                            <asp:DropDownList ID="ddlMonth" runat="server" CssClass="text-box select_box"
                            onchange="DropDownlistOnChangelbl(this,'spanddlMonth',' Month')">
                                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <br />
                            <span id="spanddlMonth" class="span_color"></span>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div id="divHide" runat="server" >
                        <div class="inner-box">
                            <div id="divBusinessUnit" runat="server">
                                <div class="check_baa iabody_lblBook">
                                    <div class="text-inner_a">
                                        <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                                    </div>
                                    <div class="check_ba">
                                        <div class="text-inner mster-input">
                                            <asp:CheckBox runat="server" ID="ChkBUAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                            <asp:CheckBoxList ID="cblBusinessUnits" class="text-inner-b" AutoPostBack="true"
                                                RepeatDirection="Horizontal" RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblBusinessUnits_SelectedIndexChanged">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div id="divServiceUnits" runat="server" visible="false">
                            <div class="check_baa iabody_lblBook">
                                <div class="text-inner_a">
                                    <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, SERVICE_UNIT%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" ID="ChkSUAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                        <asp:CheckBoxList ID="cblServiceUnits" class="text-inner-b" AutoPostBack="true" RepeatDirection="Horizontal"
                                            RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceUnits_SelectedIndexChanged">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="divServiceCenters" runat="server" visible="false">
                            <div class="check_baa iabody_lblBook">
                                <div class="text-inner_a">
                                    <asp:Literal ID="litsc" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTER%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" ID="chkSCAll" class="text-inner-b" Text="All" AutoPostBack="true" />
                                        <asp:CheckBoxList ID="cblServiceCenters" class="text-inner-b" RepeatDirection="Horizontal"
                                            AutoPostBack="true" RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblServiceCenters_SelectedIndexChanged">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="divCycles" runat="server" visible="false">
                            <div class="check_baa iabody_lblBook">
                                <div class="text-inner_a">
                                    <asp:Literal ID="litcycle" runat="server" Text="<%$ Resources:Resource, CYCLE%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" class="text-inner-b" ID="chkCycleAll" Text="All" AutoPostBack="true" />
                                        <asp:CheckBoxList ID="cblCycles" class="text-inner-b" AutoPostBack="true" RepeatDirection="Horizontal"
                                            RepeatColumns="3" runat="server" OnSelectedIndexChanged="cblCycles_SelectedIndexChanged">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div id="divBookNos" runat="server" visible="false">
                            <div class="check_baa iabody_lblBook">
                                <div class="text-inner_a">
                                    <asp:Literal ID="litBookNo" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" class="text-inner-b" ID="chkBooksAll" Text="All" />
                                        <asp:CheckBoxList ID="cblBooks" class="text-inner-b" RepeatDirection="Horizontal"
                                            RepeatColumns="3" runat="server">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="box_total">
                            <div class="box_total_a">
                                <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, SEARCH%>" ToolTip="Search"
                                    CssClass="box_s" runat="server" OnClientClick="return Validate();" OnClick="btnSearch_Click" />
                            </div>
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div class="grid_boxes" id="rptrdetails" runat="server" visible="false">
                            <div class="grid_paging_top">
                                <div class="paging_top_title" style="position: relative; top: 20px;">
                                    <asp:Literal ID="litBuList" runat="server" Text="Consumption Report"></asp:Literal>
                                </div>
                                <div class="clear">
                                </div>
                                <div id="divToprptrContent" runat="server" visible="false">
                                    <div class="paging_top_right_content" id="divTopPaging" runat="server">
                                        <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                                    </div>
                                </div>
                                <div align="right" runat="server" id="divExport" visible="false">
                                    <asp:Button ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" CssClass="exporttoexcelButton" /></div>
                            </div>
                            <div class="clr pad_10"></div>
                            <div class="rptr_tb">
                                <table style="border-collapse: collapse; border:1px #000 solid;">
                                    <tr class="rptr_tb_hd" align="center" runat="server" visible="false" id="rptrheader">
                                        <th>
                                            <asp:Label ID="lblHSno" runat="server" Text="<%$ Resources:Resource, GRID_SNO%>"></asp:Label>
                                        </th>
                                        <%--<th>
                                            <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Label>
                                        </th>
                                        <th>
                                            <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Label>
                                        </th>
                                        <th>
                                            <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Label>
                                        </th>
                                        <th>
                                            <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, SERVICE_ADDRESS%>"></asp:Label>
                                        </th>--%>
                                        <th>
                                            <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Label>
                                        </th>
                                        <th>
                                            <asp:Label ID="Label10" runat="server" Text="Total Customers"></asp:Label>
                                        </th>
                                        <th>
                                            <asp:Label ID="Label11" runat="server" Text="Total Reading"></asp:Label>
                                        </th>
                                        <th>
                                            <asp:Label ID="Label1" runat="server" Text="Total Energy Read"></asp:Label>
                                        </th>
                                        <th>
                                            <asp:Label ID="Label5" runat="server" Text="Total"></asp:Label>
                                        </th>
                                    </tr>
                                    <asp:Repeater ID="rptrDebitBalence" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="rptr_tb_td" align="center">
                                                    <asp:Label runat="server" ID="lblsno" Text='<%#Eval("RowNumber") %>'></asp:Label>
                                                </td>
                                                <%--<td class="rptr_tb_td" align="center">
                                                    <asp:Label runat="server" ID="lblAccNo" Text='<%#Eval("GlobalAccountNumber") %>'></asp:Label>
                                                </td>
                                                <td class="rptr_tb_td" align="left">
                                                    <asp:Label runat="server" ID="lblOldAccountNo" Text='<%#Eval("OldAccountNo") %>'></asp:Label>
                                                </td>
                                                <td class="rptr_tb_td" align="left">
                                                    <asp:Label runat="server" ID="lblName" Text='<%#Eval("Name") %>'></asp:Label>
                                                </td>
                                                <td class="rptr_tb_td" align="left">
                                                    <asp:Label runat="server" ID="lblAddress" Text='<%#Eval("ServiceAddress") %>'></asp:Label>
                                                </td>--%>
                                                <td class="rptr_tb_td" align="left">
                                                    <asp:Label ID="lblNameClassName" runat="server" Text='<%#Eval("ClassName") %>'></asp:Label>
                                                    <asp:Label ID="tblTariffID" runat="server" Text='<%#Eval("ClassID") %>' Visible="false"></asp:Label>
                                                </td>
                                                <td class="rptr_tb_td" align="center">
                                                    <asp:Label ID="lblEstimatedCustomers" runat="server" Text='<%#Eval("EstimatedCustomers") %>'></asp:Label>
                                                </td>
                                                <td class="rptr_tb_td" align="center">
                                                    <asp:Label ID="Label13" runat="server" Text='<%#Eval("AvgReadingPerCust") %>'></asp:Label>
                                                </td>
                                                <td class="rptr_tb_td" align="center">
                                                    <asp:Label ID="Label14" runat="server" Text='<%#Eval("AvgUsage") %>'></asp:Label>
                                                </td>
                                                <td class="rptr_tb_td" align="center">
                                                    <asp:Label ID="lblAvgReadingPerCust" runat="server" Text='<%#Eval("Total") %>'></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <tr align="center" runat="server" id="divNoDataFound" visible="false">
                                        <td colspan="10">
                                            No data found
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="divDownrptrContent" runat="server" visible="false">
                                <div id="divDownPaging" runat="server">
                                    <div class="grid_paging_bottom">
                                        <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:HiddenField ID="hfPageSize" runat="server" />
                    <asp:HiddenField ID="hfPageNo" runat="server" Value="0" />
                    <asp:HiddenField ID="hfLastPage" runat="server" />
                    <asp:HiddenField ID="hfTotalRecords" runat="server" Value="10" />
                    <asp:HiddenField ID="hfRecognize" runat="server" />
                    <asp:HiddenField ID="hfOpenStatusId" runat="server" />
                    <asp:HiddenField ID="hfFocus" runat="server" />
                    <asp:HiddenField ID="hfIsAllExists" runat="server" Value="1" />
                    <asp:HiddenField ID="hfBillingRule" runat="server" Value="0" />
                    <asp:HiddenField ID="hfMonth" runat="server" Value="0" />
                    <asp:HiddenField ID="hfYear" runat="server" Value="0" />
                    <div id="rnkland">
                        <%-- Grid Alert popup Start--%>
                        <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                            TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                            <div class="popheader popheaderlblred">
                                <asp:Label ID="lblgridalertmsg" runat="server" Text='<%$Resources:Resource, ENTR_EST_ENRGY %>'></asp:Label>
                            </div>
                            <div class="footer popfooterbtnrt">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btnEditEstimation" runat="server" Text="Edit Estimation" CssClass="ok_btn"
                                        Visible="false" />
                                    <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" OnClientClick="CloseMP()" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Grid Alert popup Closed--%>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExportExcel" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".rnkland").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/UsageConsumeReport.js" type="text/javascript"></script>
    <script type="text/javascript">        
        function Validate() {
            var ddlYear = document.getElementById('UMSNigeriaBody_ddlYear');
            var ddlMonth = document.getElementById('UMSNigeriaBody_ddlMonth');

            var IsValid = true;

            if (DropDownlistValidationlbl(ddlYear, document.getElementById("spanddlYear"), "Year") == false) IsValid = false;
            if (DropDownlistValidationlbl(ddlMonth, document.getElementById("spanddlMonth"), "Month") == false) IsValid = false;

            if (!IsValid) {
                return false;
            }
        }
    </script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
