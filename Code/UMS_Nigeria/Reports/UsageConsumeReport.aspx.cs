﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for Estimation Setting
                     
 Developer        : ID077-NEERAJ KANOJIYA
 Creation Date    : 25-July-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Configuration;
using org.in2bits.MyXls;

namespace UMS_Nigeria.Reports
{
    public partial class UsageConsumeReport : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBAL = new iDsignBAL();
        GeneralReportsBal _objGeneralReportsBal = new GeneralReportsBal();
        CommonMethods _objCommonMethods = new CommonMethods();
        string Key = "UsageConsumedReport";
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStartsReport : Convert.ToInt32(hfPageSize.Value); }
        }
        public string BU
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblBusinessUnits, ","); }
        }
        public string SU
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceUnits, ","); }
        }
        public string SC
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblServiceCenters, ","); }
        }
        public string Cycle
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblCycles, ","); }
        }
        public string BookNo
        {
            get { return _objCommonMethods.CollectSelectedItemsValues(cblBooks, ","); }
        }
        private int YearId
        {
            get { return string.IsNullOrEmpty(ddlYear.SelectedValue) ? 0 : Convert.ToInt32(ddlYear.SelectedValue); }
            set { ddlYear.SelectedValue = value.ToString(); }
        }
        private int MonthId
        {
            get { return string.IsNullOrEmpty(ddlMonth.SelectedValue) ? 0 : Convert.ToInt32(ddlMonth.SelectedValue); }
            set { ddlMonth.SelectedValue = value.ToString(); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE, false);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            hfIsAllExists.Value = "1";
            if (Session[UMS_Resource.SESSION_LOGINID] != null)
            {
                if (!IsPostBack)
                {
                    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    {
                        ListItem LiBU = new ListItem(Session[UMS_Resource.SESSION_USER_BUNAME].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());
                        cblBusinessUnits.Items.Add(LiBU);
                        cblBusinessUnits.Items[0].Selected = true;
                        cblBusinessUnits.Enabled = false;
                        ChkBUAll.Visible = false;
                        divServiceUnits.Visible = true;
                        _objCommonMethods.BindUserServiceUnits_BuIds(cblServiceUnits, BU, Resource.DDL_ALL, false);
                    }
                    else
                        _objCommonMethods.BindBusinessUnits(cblBusinessUnits, string.Empty, false);


                    //GetOpenMonthDetails();
                    BindYears();
                    BindMonths();
                    CheckBoxesJS();
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    hfTotalRecords.Value = Constants.Zero.ToString();
                }
            }
            else
                Response.Redirect(UMS_Resource.DEFAULT_PAGE, false);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindGrid();
            UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            UCPaging2.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindMonths();
        }

        private void BindYears()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
                xmlResult = _objBillGenerationBal.GetDetails(ReturnType.Bulk);
                BillGenerationListBe objBillsListBE = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                if (objBillsListBE.Items.Count > 0)
                {
                    _objiDsignBAL.FillDropDownList(_objiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlYear, "Year", "Year", "--Select--", true);
                    BindMonths();
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindMonths()
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
                _objBillGenerationBe.Year = this.YearId;
                xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.User);
                BillGenerationListBe objBillsListBE = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                if (objBillsListBE.Items.Count > 0)
                {
                    ddlMonth.Enabled = true;
                    _objiDsignBAL.FillDropDownList(_objiDsignBAL.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlMonth, "MonthName", "Month", "--Select--", true);
                }
                else
                {
                    ddlMonth.SelectedIndex = 0;
                    ddlMonth.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region cblEvents
        protected void cblBusinessUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                cblBooks.Items.Clear();
                cblServiceCenters.Items.Clear();
                cblServiceUnits.Items.Clear();
                chkCycleAll.Checked = chkBooksAll.Checked = chkSCAll.Checked = ChkSUAll.Checked = ChkBUAll.Checked = false;

                divServiceUnits.Visible = divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = chkSCAll.Checked = divServiceCenters.Visible = false;
                if (!string.IsNullOrEmpty(BU))
                {
                    _objCommonMethods.BindUserServiceUnits_BuIds(cblServiceUnits, BU, Resource.DDL_ALL, false);
                    if (cblServiceUnits.Items.Count > 0)
                        divServiceUnits.Visible = true;
                    else
                        divServiceUnits.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                cblBooks.Items.Clear();
                cblServiceCenters.Items.Clear();
                divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = chkSCAll.Checked = divServiceCenters.Visible = false;
                if (!string.IsNullOrEmpty(SU))
                {
                    _objCommonMethods.BindUserServiceCenters_SuIds(cblServiceCenters, SU, Resource.DDL_ALL, false);
                    if (cblServiceCenters.Items.Count > 0)
                        divServiceCenters.Visible = true;
                    else
                        divServiceCenters.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblServiceCenters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblCycles.Items.Clear();
                cblBooks.Items.Clear();
                divCycles.Visible = divBookNos.Visible = chkCycleAll.Checked = chkBooksAll.Checked = false;
                if (!string.IsNullOrEmpty(SC))
                {
                    _objCommonMethods.BindCyclesByBUSUSC(cblCycles, BU, SU, SC, true, string.Empty, false);
                    if (cblCycles.Items.Count > 0)
                        divCycles.Visible = true;
                    else
                        divCycles.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblCycles_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblBooks.Items.Clear();
                chkBooksAll.Checked = divBookNos.Visible = false;
                if (!string.IsNullOrEmpty(Cycle))
                {
                    _objCommonMethods.BindAllBookNumbers(cblBooks, Cycle, true, false, string.Empty);
                    if (cblBooks.Items.Count > 0)
                        divBookNos.Visible = true;
                    else
                        divBookNos.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods
        //private void GetOpenMonthDetails()
        //{
        //    ReportsBal _objReportsBal = new ReportsBal();
        //    RptPreBillingBe _objPreBillingBe = new RptPreBillingBe();
        //    XmlDocument xmlResult = _objReportsBal.GetPreBillingDetails(_objPreBillingBe, ReturnType.Fetch);
        //    _objPreBillingBe = _objiDsignBAL.DeserializeFromXml<RptPreBillingBe>(xmlResult);
        //    if (_objPreBillingBe != null)
        //    {
        //        divHide.Visible = true;
        //        lblMonthDetails.Text = _objPreBillingBe.NameOfMonth + " - " + _objPreBillingBe.Year;
        //        hfMonth.Value = _objPreBillingBe.Month.ToString();
        //        hfYear.Value = _objPreBillingBe.Year.ToString();
        //    }
        //    else
        //    {
        //        lblMonthDetails.Text = Resource.OPEN_MNTHS_NOT_AVAIL;
        //        divHide.Visible = false;
        //    }
        //}
        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public void BindGrid()
        {
            RptUsageConsumedBe _objRptUsageConsumed = new RptUsageConsumedBe();
            _objRptUsageConsumed.BU_ID = this.BU;
            _objRptUsageConsumed.SU_ID = this.SU;
            _objRptUsageConsumed.SC_ID = this.SC;
            _objRptUsageConsumed.CycleId = this.Cycle;
            _objRptUsageConsumed.BookNo = this.BookNo;
            _objRptUsageConsumed.PageNo = Convert.ToInt32(hfPageNo.Value);
            _objRptUsageConsumed.PageSize = PageSize;
            _objRptUsageConsumed.MonthId = this.MonthId;
            _objRptUsageConsumed.YearId = this.YearId;

            DataSet dsDebit = new DataSet();
            dsDebit = _objGeneralReportsBal.GetUsageConsumedReport(_objRptUsageConsumed, ReturnType.Get);

            if (dsDebit != null && dsDebit.Tables.Count > 0 && dsDebit.Tables[0].Rows.Count > 0)
            {
                divToprptrContent.Visible = divExport.Visible = divDownrptrContent.Visible = rptrdetails.Visible = rptrheader.Visible = true;
                divNoDataFound.Visible = false;
                rptrDebitBalence.DataSource = dsDebit.Tables[0];
                rptrDebitBalence.DataBind();
                hfTotalRecords.Value = dsDebit.Tables[0].Rows[0]["TotalRecords"].ToString();
            }
            else
            {
                divToprptrContent.Visible = divExport.Visible = divDownrptrContent.Visible = rptrheader.Visible = false;
                divNoDataFound.Visible = rptrdetails.Visible = true;
                rptrDebitBalence.DataSource = new DataTable(); ;
                rptrDebitBalence.DataBind();
                hfTotalRecords.Value = Constants.Zero.ToString();
            }
        }
        private void CheckBoxesJS()
        {
            ChkBUAll.Attributes.Add("onclick", "CheckAll('" + ChkBUAll.ClientID + "','" + cblBusinessUnits.ClientID + "')");
            ChkSUAll.Attributes.Add("onclick", "CheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            chkSCAll.Attributes.Add("onclick", "CheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            chkCycleAll.Attributes.Add("onclick", "CheckAll('" + chkCycleAll.ClientID + "','" + cblCycles.ClientID + "')");
            chkBooksAll.Attributes.Add("onclick", "CheckAll('" + chkBooksAll.ClientID + "','" + cblBooks.ClientID + "')");

            cblBusinessUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkBUAll.ClientID + "','" + cblBusinessUnits.ClientID + "')");
            cblServiceUnits.Attributes.Add("onclick", "UnCheckAll('" + ChkSUAll.ClientID + "','" + cblServiceUnits.ClientID + "')");
            cblServiceCenters.Attributes.Add("onclick", "UnCheckAll('" + chkSCAll.ClientID + "','" + cblServiceCenters.ClientID + "')");
            cblCycles.Attributes.Add("onclick", "UnCheckAll('" + chkCycleAll.ClientID + "','" + cblCycles.ClientID + "')");
            cblBooks.Attributes.Add("onclick", "UnCheckAll('" + chkBooksAll.ClientID + "','" + cblBooks.ClientID + "')");
        }
        #endregion        

        #region ExportToExcel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            RptUsageConsumedBe _objRptUsageConsumed = new RptUsageConsumedBe();
            _objRptUsageConsumed.BU_ID = this.BU;
            _objRptUsageConsumed.SU_ID = this.SU;
            _objRptUsageConsumed.SC_ID = this.SC;
            _objRptUsageConsumed.CycleId = this.Cycle;
            _objRptUsageConsumed.BookNo = this.BookNo;
            _objRptUsageConsumed.PageNo = Convert.ToInt32(hfPageNo.Value);
            _objRptUsageConsumed.PageSize = Convert.ToInt32(hfTotalRecords.Value);
            _objRptUsageConsumed.MonthId = this.MonthId;
            _objRptUsageConsumed.YearId = this.YearId;

            DataSet dsLedger = new DataSet();
            dsLedger = _objGeneralReportsBal.GetUsageConsumedReport(_objRptUsageConsumed, ReturnType.Get);

            if (dsLedger != null && dsLedger.Tables.Count > 0 && dsLedger.Tables[0].Rows.Count > 0)
            {
                DataRow drLedger = dsLedger.Tables[0].NewRow();
                drLedger = dsLedger.Tables[0].Rows[0];

                XlsDocument xls = new XlsDocument();
                Worksheet mainSheet = xls.Workbook.Worksheets.Add(Constants.UsageConsumedReport_Name);
                Cells mainCells = mainSheet.Cells;
                Cell _objMainCells = null;
                int DetailsRowNo = 3, j = 1;

                _objMainCells = mainCells.Add(2, 2, Resource.BEDC);
                mainCells.Add(4, 2, "Report Code ");
                mainCells.Add(4, 3, Constants.UsageConsumedReport_Code).Font.Bold = true;
                mainCells.Add(4, 4, "Report Name");
                mainCells.Add(4, 5, Constants.UsageConsumedReport_Name).Font.Bold = true;
                mainSheet.AddMergeArea(new MergeArea(2, 2, 2, 5));
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                _objMainCells.Font.FontFamily = FontFamilies.Roman;


                _objMainCells = mainCells.Add(DetailsRowNo + 3, 1, "Sno");
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                _objMainCells.Font.FontFamily = FontFamilies.Roman;
                _objMainCells = mainCells.Add(DetailsRowNo + 3, 2, "Tariff");
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                _objMainCells.Font.FontFamily = FontFamilies.Roman;
                _objMainCells = mainCells.Add(DetailsRowNo + 3, 3, "Total Customers");
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                _objMainCells.Font.FontFamily = FontFamilies.Roman;
                _objMainCells = mainCells.Add(DetailsRowNo + 3, 4, "Total Reading");
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                _objMainCells.Font.FontFamily = FontFamilies.Roman;
                _objMainCells = mainCells.Add(DetailsRowNo + 3, 5, "Total Energy Read");
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                _objMainCells.Font.FontFamily = FontFamilies.Roman;
                _objMainCells = mainCells.Add(DetailsRowNo + 3, 6, "Total");
                _objMainCells.Font.Weight = FontWeight.ExtraBold;
                _objMainCells.HorizontalAlignment = HorizontalAlignments.Centered;
                _objMainCells.Font.FontFamily = FontFamilies.Roman;

                foreach (DataRow dr in dsLedger.Tables[0].Rows)
                {
                    DetailsRowNo++;
                    mainCells.Add(DetailsRowNo + 3, 1, dr["RowNumber"].ToString());
                    mainCells.Add(DetailsRowNo + 3, 2, dr["ClassName"].ToString());
                    mainCells.Add(DetailsRowNo + 3, 3, dr["EstimatedCustomers"].ToString());
                    mainCells.Add(DetailsRowNo + 3, 4, dr["AvgReadingPerCust"].ToString());
                    mainCells.Add(DetailsRowNo + 3, 5, dr["AvgUsage"].ToString());
                    mainCells.Add(DetailsRowNo + 3, 6, dr["Total"].ToString());

                    j++;
                }



                string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".xls";
                fileName = Constants.UsageConsumedReport_Name.Replace(" ", string.Empty) + "_" + fileName;
                string filePath = Server.MapPath(ConfigurationManager.AppSettings[Resource.TEMP_KEY].ToString()) + fileName;
                xls.FileName = filePath;
                xls.Save();
                _objiDsignBAL.DownLoadFile(filePath, "application//vnd.ms-excel");

            }
            else
            {
                Message("No data found.", UMS_Resource.MESSAGETYPE_ERROR);
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}