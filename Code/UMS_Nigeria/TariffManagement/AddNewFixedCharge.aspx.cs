﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddNewFixedCharge.aspx
                     
 Developer        : id065-Bhimaraju V
 Creation Date    : 15-Aug-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using Resources;
using iDSignHelper;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Globalization;

namespace UMS_Nigeria.TariffManagement
{
    public partial class AddNewFixedCharge : System.Web.UI.Page
    {
        # region Members
        iDsignBAL objiDsignBAL = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        TariffManagementBal objTariffBal = new TariffManagementBal();
        public int PageNum;
        string Key = UMS_Resource.KEY_FIXED_CHARGES;
        #endregion

        #region Properties

        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                //    string path = string.Empty;
                //    path = objCommonMethods.GetPagePath(this.Request.Url);
                //    if (objCommonMethods.IsAccess(path))
                //    {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                //}
                //else { Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }
                if (!string.IsNullOrEmpty(Request.QueryString["action"])) divBack.Visible = true;
                else divBack.Visible = false;
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                TariffManagementBE objTariffBE = new TariffManagementBE();
                objTariffBE.ChargeName = txtChargeName.Text;
                objTariffBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objTariffBE = objiDsignBAL.DeserializeFromXml<TariffManagementBE>(objTariffBal.InsertTariff(objTariffBE, Statement.Edit));
                if (objTariffBE.IsSuccess)
                {
                    BindGrid();
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(Resource.FIXED_CHARGE_INSERT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                    txtChargeName.Text = string.Empty;
                }
                else if (objTariffBE.IsChargeNameExists)
                    Message(Resource.FIXED_CHARGE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                else
                    Message(Resource.FIXED_CHARGE_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvCharge_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridChargeName = (TextBox)row.FindControl("txtGridChargeName");
                Label lblChargeId = (Label)row.FindControl("lblChargeId");
                Label lblChargeName = (Label)row.FindControl("lblChargeName");
                Label lblActiveStatusId = (Label)row.FindControl("lblActiveStatusId");
                TariffManagementBE objTariffBe = new TariffManagementBE();
                switch (e.CommandName.ToUpper())
                {
                    case "EDITCHARGE":
                        lblChargeName.Visible = false;
                        txtGridChargeName.Visible = true;
                        txtGridChargeName.Text = lblChargeName.Text;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;
                        break;
                    case "CANCELCHARGE":
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                        txtGridChargeName.Visible = false;
                        lblChargeName.Visible = true;
                        break;
                    case "UPDATECHARGE":
                        objTariffBe.ChargeId = Convert.ToInt32(lblChargeId.Text);
                        objTariffBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objTariffBe.ChargeName = txtGridChargeName.Text;
                        objTariffBe.Flag = 1;
                        objTariffBe = objiDsignBAL.DeserializeFromXml<TariffManagementBE>(objTariffBal.InsertTariff(objTariffBe, Statement.Change));
                        if (objTariffBe.IsSuccess)
                        {
                            BindGrid();
                            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            Message(Resource.FIXED_CHARGE_UPDATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        else if (objTariffBe.IsChargeNameExists)
                            Message(Resource.FIXED_CHARGE_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                        else
                            Message(Resource.FIXED_CHARGE_UPDATED_FAILED, UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "ACTIVECHARGE":
                        mpeActivate.Show();
                        hfRecognize.Value = UMS_Resource.ACTIVATE;
                        hfChargeId.Value = lblChargeId.Text;
                        pop.Attributes.Add("class", "popheader popheaderlblgreen");
                        lblActiveMsg.Text = Resource.ACTIVE_FIXED_CHARGE_POPUP_TEXT;
                        btnActiveOk.Focus();
                        break;
                    case "DEACTIVECHARGE":
                        mpeActivate.Show();
                        hfRecognize.Value = UMS_Resource.DEACTIVATE;
                        hfChargeId.Value = lblChargeId.Text;
                        pop.Attributes.Add("class", "popheader popheaderlblred");
                        lblActiveMsg.Text = Resource.DEACTIVE_FIXED_CHARGE_POPUP_TEXT;
                        btnActiveOk.Focus();
                        break;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (hfRecognize.Value == UMS_Resource.ACTIVATE)
                {
                    UpdateActiveStatus(true, Resource.FIXED_CHARGE_ACTIVATED_SUCCESS, Resource.FIXED_CHARGE_ACTIVATED_FAILED);
                }
                else if (hfRecognize.Value == UMS_Resource.DEACTIVATE)
                {
                    UpdateActiveStatus(false, Resource.FIXED_CHARGE_DEACTIVATED_SUCCESS, Resource.FIXED_CHARGE_DEACTIVATED_FAILED);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void UpdateActiveStatus(bool activeStatus, string successMessage, string failedMessage)
        {
            hfRecognize.Value = string.Empty;
            TariffManagementBE objTariffBe = new TariffManagementBE();
            objTariffBe.ChargeId = Convert.ToInt32(hfChargeId.Value);
            objTariffBe.ActiveStatusId = activeStatus;
            objTariffBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            objTariffBe = objiDsignBAL.DeserializeFromXml<TariffManagementBE>(objTariffBal.InsertTariff(objTariffBe, Statement.Change));
            if (objTariffBe.IsSuccess)
            {
                Message(successMessage, UMS_Resource.MESSAGETYPE_SUCCESS);
                BindGrid();
                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            else if(objTariffBe.IsLastActiveRecord)
            {
                lblgridalertmsg.Text = "Can not deactivate the fixed charge. There should be atleast on active record.";
                mpegridalert.Show();
                btngridalertok.Focus();
            }
            else
            {
                Message(failedMessage, UMS_Resource.MESSAGETYPE_ERROR);
            }
        }

        protected void gvCharge_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    TextBox txtGridChargeName = (TextBox)e.Row.FindControl("txtGridChargeName");
                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridChargeName.ClientID + "')");

                    Label lblIsActive = (Label)e.Row.FindControl("lblActiveStatusId");
                    if (Convert.ToInt32(lblIsActive.Text) == Constants.Active)
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion        

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void BindGrid()
        {
            try
            {
                TariffManagementListBE objTariffListBE = new TariffManagementListBE();
                TariffManagementBE objTariffBE = new TariffManagementBE();
                objTariffBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                objTariffBE.PageSize = PageSize;
                objTariffListBE = objiDsignBAL.DeserializeFromXml<TariffManagementListBE>(objTariffBal.Get(objTariffBE, ReturnType.Multiple));

                if (objTariffListBE.items.Count > 0)
                {
                    divdownpaging.Visible = divpaging.Visible = true;
                    hfTotalRecords.Value = objTariffListBE.items[0].TotalRecords.ToString();
                    gvCharge.DataSource = objTariffListBE.items;
                    gvCharge.DataBind();
                }
                else
                {
                    hfTotalRecords.Value = objTariffListBE.items.Count.ToString();
                    divdownpaging.Visible = divpaging.Visible = false;
                    gvCharge.DataSource = new DataTable();
                    gvCharge.DataBind();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(UMS_Resource.NAV_TARF_FXD_CHRG);
        }
    }
}