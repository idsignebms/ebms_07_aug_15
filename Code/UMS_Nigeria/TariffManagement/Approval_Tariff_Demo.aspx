﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master" AutoEventWireup="true"
    CodeBehind="Approval_Tariff_Demo.aspx.cs" Inherits="UMS_Nigeria.TariffManagement.Approval_Tariff_Demo"
    Theme="Green" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upStates" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddState" runat="server" Text="Approvals List"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                    </div>
                    <div class="space">
                    </div>
                    <div class="box_total">
                        <div class="box_total_a">
                            <%-- <asp:Button ID="btnGenerate" Text="Get Approval Details." CssClass="box_s" runat="server"
                                TabIndex="7" OnClick="btnGenerate_Click" />--%>
                            <%-- <asp:DropDownList ID="ddlFeatureList" runat="server" 
                                CssClass="text-box text-select" AutoPostBack="true" 
                                onselectedindexchanged="ddlFeatureList_SelectedIndexChanged">
                                <asp:ListItem Value="0" Text="Select"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Tariff Change"></asp:ListItem>
                                </asp:DropDownList>--%>
                        </div>
                    </div>
                </div>
                <div style="clear: both;">
                </div>
                <br />
                <br />
                <asp:Label ID="lblSelectedApprovals" runat="server" Text="" CssClass="text-heading"></asp:Label>
                <br />
                <br />
                <div class="grid_tb" id="divgrid" runat="server">
                   
                </div>
            </div>
            <div id="hidepopup">
                <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                    TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                </asp:ModalPopupExtender>
                <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                    <div class="popheader popheaderlblred">
                        <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- Grid Alert popup Closed--%>
            </div>
            <asp:ModalPopupExtender ID="mpeAter" runat="server" PopupControlID="pnlAlert" TargetControlID="hdnAlert"
                BackgroundCssClass="modalBackground" CancelControlID="btnOk">
            </asp:ModalPopupExtender>
            <asp:HiddenField ID="hdnAlert" runat="server" />
            <asp:Panel ID="pnlAlert" runat="server" CssClass="modalPopup" Style="display: none;">
                <div class="popheader popheaderlblred">
                    <asp:Label ID="lblAlert" runat="server"></asp:Label>
                </div>
                <div class="footer popfooterbtnrt">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="btnOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="ok_btn" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
            <asp:HiddenField ID="hdn" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
    
    </script>
    <%--==============Ajax loading script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
