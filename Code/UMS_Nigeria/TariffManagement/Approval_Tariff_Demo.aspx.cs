﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using System.Data;
using System.Configuration;
using System.Xml;
using System.IO;
using System.Web.UI.HtmlControls;

namespace UMS_Nigeria.TariffManagement
{
    public partial class Approval_Tariff_Demo : System.Web.UI.Page
    {
        #region Constructor
        public Approval_Tariff_Demo()
        {
            grdCustomerNameChange.HeaderStyle.CssClass = grdTariff.HeaderStyle.CssClass = "grid_tb_hd";
            divClr.Style.Add("clear", "both");

        }
        #endregion

        #region Members
        DataTable dtApprovalTariffInput = new DataTable();
        DataTable dtTariffApproval = new DataTable();

        DataTable dtApprovalNameChangeInput = new DataTable();
        DataTable dtNameChangeApproval = new DataTable();

        Label lblHeader = new Label();
        string Key = "";
        bool IsBoundField = false;
        GridView grdTariff = new GridView();
        GridView grdCustomerNameChange = new GridView();
        HtmlGenericControl divClr = new HtmlGenericControl("div");
        string[] ApprovalFeaturesList = new string[2];
        string[] dtInputList = new string[2];
        int TransactionID = 1;
        HtmlGenericControl divgrid_boxes = new HtmlGenericControl("div");
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            //if (!IsPostBack)
            //{              
            //}
            lblHeader.CssClass = "text-heading";
            divClr.Attributes.Add("class", "clear pad_5");
            ApprovalFeaturesList[0] = "Approval_Tariff";
            ApprovalFeaturesList[1] = "Approval_CustomerNameChange";
            GenerateDynamicApprovalsGrids();

        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }
        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            GenerateDynamicApprovalsGrids();
        }
        private void Gv_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Label lbl = (Label)grdTariff.Rows[Convert.ToInt32(e.CommandArgument)].FindControl("lbl_Global_AC_No");
            if (lbl != null)
            {
                foreach (DataRow DC in dtApprovalTariffInput.Rows)
                {
                    if (DC["Global_AC_No"] == lbl.Text)
                    {
                        XmlDocument xml = new XmlDocument();
                        xml.InnerXml = DC["XML"].ToString();
                        XmlNodeList xl = xml.GetElementsByTagName("Column");
                        for (int i = 0; i < xl.Count; i++)
                        {
                            if (xl[i].Attributes["RefID"].Value == "YES")
                            {
                                lblAlert.Text = "Tansaction Value: " + xl[i].Attributes["Value"].Value;
                                mpeAter.Show();
                                break;
                            }
                        }
                    }
                }

            }
        }
        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }
        protected void ddlFeatureList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        #endregion

        #region Method

        #region Dynamic Data Source

        public void CreateInputDataTable(DataTable dt)//Comman
        {
            dt.Columns.Add("Global_AC_No", typeof(string));
            dt.Columns.Add("NAME", typeof(string));
            dt.Columns.Add("XML", typeof(string));
            dt.Columns.Add("TransactionID", typeof(int));
        }

        public void CreateOuptputDataTable(string xmlPath, DataTable dt)//Comman
        {
            dt.Columns.Add("Global_AC_No", typeof(string));
            dt.Columns.Add("NAME", typeof(string));
            XmlDocument xml = new XmlDocument();
            xml.Load(Server.MapPath(ConfigurationManager.AppSettings[xmlPath]));
            XmlNodeList xl = xml.GetElementsByTagName("Column");
            for (int i = 0; i < xl.Count; i++)
            {
                dt.Columns.Add(xl[i].Attributes["Name"].Value, typeof(string));
            }
            dt.Columns.Add("Approve", typeof(string));
            dt.Columns.Add("Reject", typeof(string));
        }

        #endregion

        #region Dynamic Tariff Customer Name Change Request

        public void InsertRecordsToInputDataTableCustNameChange()
        {
            DataRow dr = dtApprovalTariffInput.NewRow();
            dr["TransactionID"] = TransactionID++;
            dr["Global_AC_No"] = "0000000011";
            dr["NAME"] = "Neeraj";
            dr["XML"] = GetXmlApprovalTariffCustNameChange("1001", "Mr", "Mr.", "Neer", "Neeraj", "K", "Kumar", "Kanojiya", "Kanojiya", "Neeraj", "Neeraj K Kanojiya", new int[] { 0, 3 }, new string[] { "SP_PROC_1", "", "", "SP_PROC_1" }).ToString();

            dtApprovalTariffInput.Rows.Add(dr);

            dr = dtApprovalTariffInput.NewRow();
            dr["TransactionID"] = TransactionID++;
            dr["Global_AC_No"] = "00000000012";
            dr["NAME"] = "Faiz";
            dr["XML"] = GetXmlApprovalTariffCustNameChange("1002", "Mr", "Mr.", "Faiyaz", "Faiz", "", "M", "S", "s", "Faiyaz", "Faiz M S", new int[] { 0, 2 }, new string[] { "", "", "SP_PROC_1", "" }).ToString();

            dtApprovalTariffInput.Rows.Add(dr);

            dr = dtApprovalTariffInput.NewRow();
            dr["TransactionID"] = TransactionID++;
            dr["Global_AC_No"] = "00000000013";
            dr["NAME"] = "Bhim";
            dr["XML"] = GetXmlApprovalTariffCustNameChange("1003", "Mr", "Mr.", "Raju", "Bhim", "B", "Bhim", "Bhim", "Bhim", "Bhi", "Bhimraj", new int[] { 0, 3 }, new string[] { "SP_PROC_1", "", "", "SP_PROC_1" }).ToString();

            dtApprovalTariffInput.Rows.Add(dr);

            dr = dtApprovalTariffInput.NewRow();
            dr["TransactionID"] = TransactionID++;
            dr["Global_AC_No"] = "00000000014";
            dr["NAME"] = "Padmini";
            dr["XML"] = GetXmlApprovalTariffCustNameChange("1004", "Ms", "Ms.", "Padmini", "Mini", "M", "M", "M", "M", "Padmini", "Munni", new int[] { 0 }, new string[] { "SP_PROC_1", "", "", "" }).ToString();

            dtApprovalTariffInput.Rows.Add(dr);

            dr = dtApprovalTariffInput.NewRow();
            dr["TransactionID"] = TransactionID++;
            dr["Global_AC_No"] = "00000000015";
            dr["NAME"] = "Narsimh";
            dr["XML"] = GetXmlApprovalTariffCustNameChange("1005", "Mr", "Mr.", "Simha", "Narsimha", "K", "Kumar", "Kanojiya", "Kanojiya", "Neeraj", "Neeraj K Kanojiya", new int[] { 0, 3 }, new string[] { "SP_PROC_1", "", "", "SP_PROC_1" }).ToString();
            dtApprovalTariffInput.Rows.Add(dr);
        }
        public string GetXmlApprovalTariffCustNameChange(string TransactionID, string Title_OLD, string Title_NEW, string FirstName_OLD, string FirstName_NEW,
            string MiddleName_OLD, string MiddleName_NEW, string LastName_OLD, string LastName_NEW, string KnownAS_OLD, string KnownAS_NEW, int[] boundFieldList, string[] spList)
        {
            IsBoundField = false;
            XmlDocument xml = new XmlDocument();
            xml.Load(Server.MapPath(ConfigurationManager.AppSettings["Approval_CustomerNameChange"].ToString()));
            XmlNodeList xl = xml.GetElementsByTagName("Column");
            for (int i = 0; i < xl.Count; i++)
            {
                if (xl[i].Attributes["Name"].Value == "Title_OLD")
                    xl[i].Attributes["Value"].Value = Title_OLD;
                else if (xl[i].Attributes["Name"].Value == "Title_NEW")
                    xl[i].Attributes["Value"].Value = Title_NEW;
                else if (xl[i].Attributes["Name"].Value == "FirstName_OLD")
                    xl[i].Attributes["Value"].Value = FirstName_OLD;
                else if (xl[i].Attributes["Name"].Value == "FirstName_NEW")
                    xl[i].Attributes["Value"].Value = FirstName_NEW;
                else if (xl[i].Attributes["Name"].Value == "MiddleName_OLD")
                    xl[i].Attributes["Value"].Value = MiddleName_OLD;
                else if (xl[i].Attributes["Name"].Value == "MiddleName_NEW")
                    xl[i].Attributes["Value"].Value = MiddleName_NEW;
                else if (xl[i].Attributes["Name"].Value == "LastName_OLD")
                    xl[i].Attributes["Value"].Value = LastName_OLD;
                else if (xl[i].Attributes["Name"].Value == "LastName_NEW")
                    xl[i].Attributes["Value"].Value = LastName_NEW;
                else if (xl[i].Attributes["Name"].Value == "KnownAS_OLD")
                    xl[i].Attributes["Value"].Value = KnownAS_OLD;
                else if (xl[i].Attributes["Name"].Value == "KnownAS_NEW")
                    xl[i].Attributes["Value"].Value = KnownAS_NEW;
                else if (xl[i].Attributes["Name"].Value == "TransactionID")
                    xl[i].Attributes["Value"].Value = TransactionID;
                //if (boundFieldList.Contains(i))
                //{
                //    xl[i].Attributes["BoundField"].Value = "Yes";
                //    xl[i].Attributes["BoundProcedure"].Value = spList[i];
                //    IsBoundField = true;
                //}
            }
            return xml.InnerXml;
        }

        public void InsertRecordsToOutputDataTableCustNameChange()
        {
            DataRow drOutput = null;
            foreach (DataRow InputTableCount in dtApprovalTariffInput.Rows)
            {
                XmlDocument xml = new XmlDocument();
                xml.InnerXml = InputTableCount["XML"].ToString();
                XmlNodeList xl = xml.GetElementsByTagName("Column");
                drOutput = dtTariffApproval.NewRow();
                for (int i = 0; i < xl.Count; i++)
                {
                    if (xl[i].Attributes["Name"].Value == "Title_OLD")
                        drOutput["Title_OLD"] = xl[i].Attributes["Value"].Value;
                    else if (xl[i].Attributes["Name"].Value == "Title_NEW")
                        drOutput["Title_NEW"] = xl[i].Attributes["Value"].Value;
                    else if (xl[i].Attributes["Name"].Value == "FirstName_OLD")
                        drOutput["FirstName_OLD"] = xl[i].Attributes["Value"].Value;
                    else if (xl[i].Attributes["Name"].Value == "FirstName_NEW")
                        drOutput["FirstName_NEW"] = xl[i].Attributes["Value"].Value;

                    else if (xl[i].Attributes["Name"].Value == "MiddleName_OLD")
                        drOutput["MiddleName_OLD"] = xl[i].Attributes["Value"].Value;
                    else if (xl[i].Attributes["Name"].Value == "MiddleName_NEW")
                        drOutput["MiddleName_NEW"] = xl[i].Attributes["Value"].Value;
                    else if (xl[i].Attributes["Name"].Value == "LastName_OLD")
                        drOutput["LastName_OLD"] = xl[i].Attributes["Value"].Value;
                    else if (xl[i].Attributes["Name"].Value == "LastName_NEW")
                        drOutput["LastName_NEW"] = xl[i].Attributes["Value"].Value;
                    else if (xl[i].Attributes["Name"].Value == "KnownAS_OLD")
                        drOutput["KnownAS_OLD"] = xl[i].Attributes["Value"].Value;
                    else if (xl[i].Attributes["Name"].Value == "KnownAS_NEW")
                        drOutput["KnownAS_NEW"] = xl[i].Attributes["Value"].Value;

                    drOutput["Global_AC_No"] = InputTableCount["Global_AC_No"];
                    drOutput["Name"] = InputTableCount["Name"];
                    drOutput["TransactionID"] = InputTableCount["TransactionID"];

                }
                dtTariffApproval.Rows.Add(drOutput);
            }
        }

        #endregion

        #region Dynamic Tariff Approval Request

        public void InsertRecordsToInputDataTableTariff()
        {
            DataRow dr = dtApprovalTariffInput.NewRow();
            dr["TransactionID"] = TransactionID++;
            dr["Global_AC_No"] = "0000000001";
            dr["NAME"] = "Neeraj";
            dr["XML"] = GetXmlApprovalTariff("0001", "R1", "R2", "A", "A", "1", "1", "1", "2", new int[] { 0, 3 }, new string[] { "SP_PROC_1", "", "", "SP_PROC_1" }).ToString();
            dtApprovalTariffInput.Rows.Add(dr);

            dr = dtApprovalTariffInput.NewRow();
            dr["TransactionID"] = TransactionID++;
            dr["Global_AC_No"] = "0000000002";
            dr["NAME"] = "Faiz";
            dr["XML"] = GetXmlApprovalTariff("0002", "C1", "C2", "B", "A", "2", "1", "1", "2", new int[] { 0, 1 }, new string[] { "SP_PROC_1", "SP_PROC_1" }).ToString();

            dtApprovalTariffInput.Rows.Add(dr);

            dr = dtApprovalTariffInput.NewRow();
            dr["TransactionID"] = TransactionID++;
            dr["Global_AC_No"] = "0000000003";
            dr["NAME"] = "Bhim";
            dr["XML"] = GetXmlApprovalTariff("0003", "C2", "C1", "A", "B", "1", "2", "1", "2", new int[] { }, new string[] { }).ToString();

            dtApprovalTariffInput.Rows.Add(dr);

            dr = dtApprovalTariffInput.NewRow();
            dr["TransactionID"] = TransactionID++;
            dr["Global_AC_No"] = "0000000004";
            dr["NAME"] = "Padmini";
            dr["XML"] = GetXmlApprovalTariff("0004", "R2", "C1", "C", "B", "1", "2", "1", "2", new int[] { 3 }, new string[] { "", "", "", "SP_PROC_1" }).ToString();

            dtApprovalTariffInput.Rows.Add(dr);

            dr = dtApprovalTariffInput.NewRow();
            dr["TransactionID"] = TransactionID++;
            dr["Global_AC_No"] = "0000000005";
            dr["NAME"] = "Narsimh";
            dr["XML"] = GetXmlApprovalTariff("0005", "R1", "C", "B", "C", "1", "2", "1", "2", new int[] { }, new string[] { }).ToString();
            dtApprovalTariffInput.Rows.Add(dr);
        }

        public void InsertRecordsToOutputDataTableTariff()
        {
            DataRow drOutput = null;
            foreach (DataRow InputTableCount in dtApprovalTariffInput.Rows)
            {
                XmlDocument xml = new XmlDocument();
                xml.InnerXml = InputTableCount["XML"].ToString();
                XmlNodeList xl = xml.GetElementsByTagName("Column");
                drOutput = dtTariffApproval.NewRow();
                for (int i = 0; i < xl.Count; i++)
                {
                    if (xl[i].Attributes["Name"].Value == "ClusterCategoryId_OLD")
                        drOutput["ClusterCategoryId_OLD"] = xl[i].Attributes["Value"].Value;
                    else if (xl[i].Attributes["Name"].Value == "ClusterCategoryId_NEW")
                        drOutput["ClusterCategoryId_NEW"] = xl[i].Attributes["Value"].Value;
                    else if (xl[i].Attributes["Name"].Value == "TariffClassID_OLD")
                        drOutput["TariffClassID_OLD"] = xl[i].Attributes["Value"].Value;
                    else if (xl[i].Attributes["Name"].Value == "TariffClassID_NEW")
                        drOutput["TariffClassID_NEW"] = xl[i].Attributes["Value"].Value;
                    else if (xl[i].Attributes["Name"].Value == "OldTariffName")
                        drOutput["OldTariffName"] = xl[i].Attributes["Value"].Value;
                    else if (xl[i].Attributes["Name"].Value == "NewTariffName")
                        drOutput["NewTariffName"] = xl[i].Attributes["Value"].Value;
                    else if (xl[i].Attributes["Name"].Value == "OldClusterName")
                        drOutput["OldClusterName"] = xl[i].Attributes["Value"].Value;
                    else if (xl[i].Attributes["Name"].Value == "NewClusterName")
                        drOutput["NewClusterName"] = xl[i].Attributes["Value"].Value;

                    drOutput["Global_AC_No"] = InputTableCount["Global_AC_No"];
                    drOutput["Name"] = InputTableCount["Name"];
                    drOutput["TransactionID"] = InputTableCount["TransactionID"];

                }
                dtTariffApproval.Rows.Add(drOutput);
            }
        }

        public string GetXmlApprovalTariff(string TransactionValue, string OldTariffName, string NewTariffName, string OldClusterName, string NewClusterName, string ClusterCategoryId_OLD, string ClusterCategoryId_NEW, string TariffClassID_OLD, string TariffClassID_NEW, int[] boundFieldList, string[] spList)
        {
            IsBoundField = false;
            XmlDocument xml = new XmlDocument();
            xml.Load(Server.MapPath(ConfigurationManager.AppSettings["Approval_Tariff"].ToString()));
            XmlNodeList xl = xml.GetElementsByTagName("Column");
            for (int i = 0; i < xl.Count; i++)
            {
                if (xl[i].Attributes["Name"].Value == "ClusterCategoryId_OLD")
                    xl[i].Attributes["Value"].Value = ClusterCategoryId_OLD;
                else if (xl[i].Attributes["Name"].Value == "ClusterCategoryId_NEW")
                    xl[i].Attributes["Value"].Value = ClusterCategoryId_NEW;
                else if (xl[i].Attributes["Name"].Value == "TariffClassID_OLD")
                    xl[i].Attributes["Value"].Value = TariffClassID_OLD;
                else if (xl[i].Attributes["Name"].Value == "TariffClassID_NEW")
                    xl[i].Attributes["Value"].Value = TariffClassID_NEW;
                else if (xl[i].Attributes["Name"].Value == "OldTariffName")
                    xl[i].Attributes["Value"].Value = OldTariffName;
                else if (xl[i].Attributes["Name"].Value == "NewTariffName")
                    xl[i].Attributes["Value"].Value = NewTariffName;
                else if (xl[i].Attributes["Name"].Value == "OldClusterName")
                    xl[i].Attributes["Value"].Value = OldClusterName;
                else if (xl[i].Attributes["Name"].Value == "NewClusterName")
                    xl[i].Attributes["Value"].Value = NewClusterName;
                else if (xl[i].Attributes["Name"].Value == "TransactionID")
                    xl[i].Attributes["Value"].Value = TransactionValue;
            }
            return xml.InnerXml;
        }

        #endregion

        public void BindGrid(GridView grd)
        {
            if (dtTariffApproval.Rows.Count > 0)
            {


                grd.AutoGenerateColumns = false;
                //Iterate through the columns of the datatable to set the data bound field dynamically.


                foreach (DataColumn col in dtTariffApproval.Columns)
                {

                    //Declare the bound field and allocate memory for the bound field.

                    TemplateField bfield = new TemplateField();

                    //if (col.ColumnName == "Global_AC_No")
                    //{
                    //    bfield.Visible = false;
                    //}
                    //else
                    //{
                    XmlDocument xml = new XmlDocument();
                    xml.Load(Server.MapPath(ConfigurationManager.AppSettings["Approval_Tariff"]));
                    XmlNodeList xl = xml.GetElementsByTagName("Column");
                    for (int i = 0; i < xl.Count; i++)
                    {
                        if (xl[i].Attributes["Name"].Value == col.ColumnName)
                        {
                            if (!Convert.ToBoolean(xl[i].Attributes["Enable"].Value))
                            {
                                bfield.Visible = false;
                            }
                        }
                    }
                    //}

                    //Initalize the DataField value.

                    bfield.HeaderTemplate = new GridViewTemplate(ListItemType.Header, col.ColumnName);




                    //Initialize the HeaderText field value.

                    bfield.ItemTemplate = new GridViewTemplate(ListItemType.Item, col.ColumnName);



                    //Add the newly created bound field to the GridView.

                    grd.Columns.Add(bfield);

                }



                //Initialize the DataSource
                grd.RowCommand += new GridViewCommandEventHandler(Gv_RowCommand);
                grd.RowDataBound += new GridViewRowEventHandler(gv_RowDataBound);
                grd.DataSource = dtTariffApproval;

                grd.DataBind();

                divgrid.Controls.Add(divgrid_boxes);
                divgrid.Controls.Add(grd);
                divgrid.Controls.Add(divClr);

            }
        }

        public void GenerateDynamicApprovalsGrids()
        {
            GetApprovalTariff();
            GetApprovalCustomerName();
        }

        public void GetApprovalTariff()
        {

            CreateInputDataTable(dtApprovalTariffInput);
            InsertRecordsToInputDataTableTariff();
            CreateOuptputDataTable(ApprovalFeaturesList[0], dtTariffApproval);
            InsertRecordsToOutputDataTableTariff();
            GetTitle("Teriff Approval");
            BindGrid(grdTariff);
            dtApprovalTariffInput = new DataTable();
            dtTariffApproval = new DataTable();

        }
        public void GetApprovalCustomerName()
        {
            CreateInputDataTable(dtApprovalTariffInput);
            InsertRecordsToInputDataTableCustNameChange();
            CreateOuptputDataTable(ApprovalFeaturesList[1], dtTariffApproval);
            InsertRecordsToOutputDataTableCustNameChange();
            GetTitle("Customer Name Change Approval");
            BindGrid(grdCustomerNameChange);
            dtApprovalNameChangeInput = new DataTable();
            dtNameChangeApproval = new DataTable();
        }
        public string GetTitle(string Title)
        {
            return divgrid_boxes.InnerHtml = "<div class='grid_boxes'><div class='grid_pagingTwo_top'><div class='paging_top_title'>" + Title + "</div></div></div>";
        }

        #endregion
    }
    #region Interface for itemtemplet
    public class GridViewTemplate : ITemplate
    {
        int GlobalCount = -1;
        //A variable to hold the type of ListItemType.

        ListItemType _templateType;
        //A variable to hold the column name.
        string _columnName;
        //Constructor where we define the template type and column name.
        public GridViewTemplate(ListItemType type, string colname)
        {

            //Stores the template type.

            _templateType = type;



            //Stores the column name.

            _columnName = colname;

        }
        void ITemplate.InstantiateIn(System.Web.UI.Control container)
        {

            switch (_templateType)
            {

                case ListItemType.Header:

                    //Creates a new label control and add it to the container.

                    Label lbl = new Label();            //Allocates the new label object.

                    lbl.Text = _columnName;             //Assigns the name of the column in the lable.

                    container.Controls.Add(lbl);        //Adds the newly created label control to the container.

                    break;



                case ListItemType.Item:

                    //Creates a new text box control and add it to the container.



                    //tb1.DataBinding += new EventHandler(tb1_DataBinding);   //Attaches the data binding event.

                    if (_columnName == "IsBoundField")
                    {
                        LinkButton tb1 = new LinkButton();
                        tb1.DataBinding += new EventHandler(lnk_DataBinding);   //Attaches the data binding event.
                        container.Controls.Add(tb1);
                    }
                    else if (_columnName == "Approve")
                    {
                        LinkButton tb1 = new LinkButton();
                        tb1.DataBinding += new EventHandler(lnk_DataBinding);   //Attaches the data binding event.
                        container.Controls.Add(tb1);
                    }
                    else if (_columnName == "Reject")
                    {
                        LinkButton tb1 = new LinkButton();
                        tb1.DataBinding += new EventHandler(lnk_DataBindingReject);   //Attaches the data binding event.
                        container.Controls.Add(tb1);
                    }
                    else if (_columnName == "TransactionID")
                    {
                        Label tb1 = new Label();                            //Allocates the new text box object.
                        tb1.DataBinding += new EventHandler(lbl_DataBindingTransactionID);   //Attaches the data binding event.
                        container.Controls.Add(tb1);
                    }
                    else
                    {
                        Label tb1 = new Label();                            //Allocates the new text box object.
                        tb1.DataBinding += new EventHandler(lbl_DataBinding);   //Attaches the data binding event.
                        container.Controls.Add(tb1);
                    }
                    //tb1.Columns = 4;                                        //Creates a column with size 4.

                    //Adds the newly created textbox to the container.

                    break;



                case ListItemType.EditItem:

                    //As, I am not using any EditItem, I didnot added any code here.

                    break;



                case ListItemType.Footer:

                    CheckBox chkColumn = new CheckBox();

                    chkColumn.ID = "Chk" + _columnName;

                    container.Controls.Add(chkColumn);

                    break;

            }

        }
        /// <summary>

        /// This is the event, which will be raised when the binding happens.

        /// </summary>

        /// <param name="sender"></param>

        /// <param name="e"></param>
        void tb1_DataBinding(object sender, EventArgs e)
        {
            TextBox txtdata = (TextBox)sender;

            GridViewRow container = (GridViewRow)txtdata.NamingContainer;

            object dataValue = DataBinder.Eval(container.DataItem, _columnName);

            if (dataValue != DBNull.Value)
            {
                txtdata.Text = dataValue.ToString();
            }

        }
        void lbl_DataBinding(object sender, EventArgs e)
        {
            Label txtdata = (Label)sender;
            txtdata.ID = "lbl_" + _columnName;
            GridViewRow container = (GridViewRow)txtdata.NamingContainer;

            object dataValue = DataBinder.Eval(container.DataItem, _columnName);

            if (dataValue != DBNull.Value)
            {
                txtdata.Text = dataValue.ToString();
            }
        }
        void lnk_DataBinding(object sender, EventArgs e)
        {
            GlobalCount++;
            LinkButton lnkIsBound = (LinkButton)sender;
            lnkIsBound.ID = "lnkIsBound";
            lnkIsBound.Text = "Click to " + _columnName;
            lnkIsBound.CommandArgument = GlobalCount.ToString();
            GridViewRow container = (GridViewRow)lnkIsBound.NamingContainer;

            object dataValue = DataBinder.Eval(container.DataItem, _columnName);

            if (dataValue != DBNull.Value)
            {
                lnkIsBound.Text = dataValue.ToString();
            }
        }
        void lnk_DataBindingReject(object sender, EventArgs e)
        {
            GlobalCount++;
            LinkButton lnkIsBound = (LinkButton)sender;
            lnkIsBound.ID = "lnkIsReject";
            lnkIsBound.Text = "Click to " + _columnName;
            lnkIsBound.CommandArgument = GlobalCount.ToString();
            GridViewRow container = (GridViewRow)lnkIsBound.NamingContainer;

            object dataValue = DataBinder.Eval(container.DataItem, _columnName);

            if (dataValue != DBNull.Value)
            {
                lnkIsBound.Text = dataValue.ToString();
            }
        }
        void lbl_DataBindingTransactionID(object sender, EventArgs e)
        {
            Label txtdata = (Label)sender;
            txtdata.ID = "lblTransactionID";

            GridViewRow container = (GridViewRow)txtdata.NamingContainer;

            object dataValue = DataBinder.Eval(container.DataItem, _columnName);

            if (dataValue != DBNull.Value)
            {
                txtdata.Text = dataValue.ToString();
            }
        }
    }
    #endregion
}