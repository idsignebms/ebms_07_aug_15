﻿<%@ Page Title="Customer Tariff Change" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="CustomerChangeTariff.aspx.cs"
    Inherits="UMS_Nigeria.TariffManagement.CustomerChangeTariff" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="contentHead" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script>
        function closePopup() {
            //            btnConfirmationOk
            var mpee = $find('UMSNigeriaBody_mpeDeActive');
            mpee.hide();
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="contentBody" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:Panel ID="pnlSearch" DefaultButton="btnSearch" runat="server">
            <asp:UpdatePanel ID="upSearchCustomer" DefaultButton="btnSearch" runat="server">
                <ContentTemplate>
                    <div class="inner-sec">
                        <asp:Panel ID="pnlMessage" runat="server">
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </asp:Panel>
                        <div class="text_total">
                            <div class="text-heading">
                                <asp:Literal ID="litSearchCustomer" runat="server" Text="<%$ Resources:Resource, CUST_CHANGE_TARIFF_SETTINGS%>"></asp:Literal>
                            </div>
                            <div class="star_text">
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                        <div class="dot-line">
                        </div>
                        <div class="inner-box">
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litAccountNo" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                                    </label>
                                    <br />
                                    <asp:TextBox CssClass="text-box" ID="txtAccountNo" runat="server" MaxLength="10"
                                        placeholder="Global Account No"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtAccountNo"
                                        FilterType="Numbers" ValidChars=" ">
                                    </asp:FilteredTextBoxExtender>
                                    <%--<span class="text-heading_2">
                                    <asp:LinkButton ID="lbtnSearch" OnClick="lbtnSearch_Click" Text="<%$ Resources:Resource, SEARCH%>"
                                        runat="server"></asp:LinkButton>
                                </span>--%>
                                    <span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp(OR)</span>
                                </div>
                            </div>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litDocumentNo" runat="server" Text="<%$ Resources:Resource, METER_NO%>"></asp:Literal>
                                    </label>
                                    <br />
                                    <asp:TextBox CssClass="text-box" ID="txtMeterNo" runat="server" MaxLength="50" placeholder="Meter No"></asp:TextBox>
                                    <span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp(OR)</span>
                                </div>
                            </div>
                            <div class="inner-box3">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, OLD_ACCOUNT_NO%>"></asp:Literal>
                                    </label>
                                    <br />
                                    <asp:TextBox CssClass="text-box" ID="txtOldAccNo" runat="server" MaxLength="15" placeholder="Old Account No"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtOldAccNo"
                                        FilterType="Numbers" ValidChars=" ">
                                    </asp:FilteredTextBoxExtender>
                                </div>
                            </div>
                        </div>
                        <div class="inner-box1">
                            <div class="search searchRight">
                                <br />
                                <asp:Button ID="btnSearch" OnClientClick="return Validate();" Text="<%$ Resources:Resource, GET_CUSTOMER_DETAILS%>"
                                    CssClass="box_s" runat="server" OnClick="btnSearch_Click" /></div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="out-bor_a" id="divCustomerDetails" runat="server" visible="false">
                        <div class="inner-sec">
                            <div class="heading customerDetailstwo">
                                <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, CUSTOMER_DETAILS%>"></asp:Literal>
                            </div>
                            <div class="out-bor_cTwo">
                                <div class="inner-box">
                                    <div class="inner-box1_a">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_a">
                                        <div class="tariffconfirm-inner">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box3_a">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblGlobalAccNoAndAccNo" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblAccountNo" Visible="false" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1_a">
                                        <div class="text_cus">
                                            <asp:Literal ID="litOldAccountNo" runat="server" Text="Old Account No"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_a">
                                        <div class="tariffconfirm-inner">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box3_a">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblOldAccountNo" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1_a">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_a">
                                        <div class="tariffconfirm-inner">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box3_a">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1_a">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, ADDRESS%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_a">
                                        <div class="tariffconfirm-inner">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box3_a">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1_a">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, LAST_BILLGENERATEDDATE%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_a">
                                        <div class="tariffconfirm-inner">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box3_a">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblLastBillDate" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1_a">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, LAST_PAIDDATE%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_a">
                                        <div class="tariffconfirm-inner">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box3_a">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblLastPaidDate" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1_a">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, OUT_STANDING_AMT%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_a">
                                        <div class="tariffconfirm-inner">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box3_a">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblDue" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1_a">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_a">
                                        <div class="tariffconfirm-inner">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box3_a">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblTariff" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1_a">
                                        <div class="text_cus">
                                            <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:Resource, CLUSTERTYPE%>"></asp:Literal>
                                        </div>
                                    </div>
                                    <div class="inner-box2_a">
                                        <div class="tariffconfirm-inner">
                                            :
                                        </div>
                                    </div>
                                    <div class="inner-box3_a">
                                        <div class="text_cusa_a">
                                            <asp:Label ID="lblClusterType" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="text_cusa_b">
                                        <asp:LinkButton ID="lbtnChangeTariff" OnClick="lbtnChangeTariff_Click" Text="<%$ Resources:Resource, CHANGE_TARIFF%>"
                                            runat="server"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="rnkland">
                        <%-- Confirmation Popup--%>
                        <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                            TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnDeActivate" runat="server" Text="Button" CssClass="popbtn" />
                        <asp:Panel ID="PanelDeActivate" runat="server" DefaultButton="btnYes" CssClass="modalPopup"
                            class="poppnl" Style="display: none;">
                            <div class="popheader popheaderlblred" id="pop" runat="server">
                                <asp:Label ID="lblConfirmMsg" runat="server"></asp:Label>
                            </div>
                            <div class="footer popfooterbtnrt">
                                <div class="fltr popfooterbtn">
                                    <br />
                                    <asp:Button ID="btnYes" runat="server" OnClientClick="return closePopup();" OnClick="btnYes_Click"
                                        Text="<%$ Resources:Resource, YES%>" CssClass="ok_btn" />
                                    <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, NO%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Confirmation Popup--%>
                        <%-- Confirm popup Start--%>
                    <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                        TargetControlID="btnActivate" BackgroundCssClass="modalBackground">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelActivate" runat="server" DefaultButton="btnActiveCancel" CssClass="modalPopup"
                        Style="display: none; border-color: #639B00;">
                        <div class="popheader popheaderlblred" id="popheader" runat="server">
                            <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                        </div>
                        <br />
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Confirm popup Closed--%>
                    </div>
                    <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="PanelAlert"
                        TargetControlID="btnAlertDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnAlertOk">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnAlertDeActivate" runat="server" Text="Button" CssClass="popbtn" />
                    <asp:Panel ID="PanelAlert" runat="server" CssClass="modalPopup" class="poppnl" Style="display: none;">
                        <div class="popheader popheaderlblgreen" id="Div1" runat="server">
                            <asp:Label ID="lblAlertMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnAlertOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".rnkland").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
//            AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);
//            AutoComplete($('#<%=txtMeterNo.ClientID %>'), 2);
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
//            AutoComplete($('#<%=txtAccountNo.ClientID %>'), 1);
//            AutoComplete($('#<%=txtMeterNo.ClientID %>'), 2);
        });         
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../Javascript/jquery.min.js" type="text/javascript"></script>
    <script src="../Javascript/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/CustomerChangeTariff.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
