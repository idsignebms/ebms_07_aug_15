﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for CustomerChangeTariff
                     
 Developer        : id065-Bhimaraju Vanka
 Creation Date    : 11-08-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using Resources;
using iDSignHelper;
using System.Threading;
using System.Globalization;

namespace UMS_Nigeria.TariffManagement
{
    public partial class CustomerChangeTariff : System.Web.UI.Page
    {
        #region Members

        iDsignBAL objIdsignBal = new iDsignBAL();
        TariffManagementBal objTariffBal = new TariffManagementBal();
        ConsumerBal objConsumerBal = new ConsumerBal();
        CommonMethods objCommonMethods = new CommonMethods();
        public string Key = UMS_Resource.CUSTOMER_CHANGE_TARIFF;

        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.CssClass = lblMessage.Text = string.Empty;
            if (!IsPostBack)
            {
                string path = string.Empty;
                path = objCommonMethods.GetPagePath(this.Request.Url);
                if (objCommonMethods.IsAccess(path))
                {
                    if (!string.IsNullOrEmpty(Request.QueryString[UMS_Resource.QSTR_ACC_NO]))
                    {
                        txtAccountNo.Text = objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.QSTR_ACC_NO]);
                    }
                }
                else
                {
                    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                }
            }

        }

        protected void lbtnChangeTariff_Click(object sender, EventArgs e)
        {
            try
            {
                TariffManagementBE objTariffBe = new TariffManagementBE();
                objTariffBe.AccountNo = lblAccountNo.Text;

                objTariffBe = objIdsignBal.DeserializeFromXml<TariffManagementBE>(objTariffBal.Get(objTariffBe, ReturnType.Retrieve));
                if (objTariffBe.ApprovalIsInProcess)
                {
                    lblConfirmMsg.Text = Resource.REQ_STILL_PENDING;
                    btnYes.Visible = false;
                    pop.Attributes.Add("class", "popheader popheaderlblred");
                    btnDeActiveCancel.Text = Resource.OK;
                    mpeDeActive.Show();
                }
                else
                {
                    lblConfirmMsg.Text = Resource.CONF_MSG_CHANGE_TARIFF;
                    btnYes.Visible = true;
                    pop.Attributes.Add("class", "popheader popheaderlblgreen");
                    btnYes.Text = Resource.YES;
                    btnDeActiveCancel.Text = Resource.NO;
                    mpeDeActive.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Constants.CustomerChangeTariffConfirm + "?" + UMS_Resource.QSTR_ACC_NO + "=" + objIdsignBal.Encrypt(lblAccountNo.Text), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtAccountNo.Text.Trim()) && string.IsNullOrEmpty(txtMeterNo.Text.Trim()) &&
                    string.IsNullOrEmpty(txtOldAccNo.Text.Trim()))
                    Message("Please enter Global Account No. / Meter No. / Old Acccount No. to search customer.",
                        UMS_Resource.MESSAGETYPE_ERROR);
                else
                    GetCustomerDetails();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lbtnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(UMS_Resource.ADVANCE_SEARCH_PAGE + "?" + UMS_Resource.FROM_PAGE + "=" + objIdsignBal.Encrypt(Constants.TariffChange), false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods

        private void GetCustomerDetails()
        {
            try
            {
                TariffManagementBE objTariffBe = new TariffManagementBE();
                objTariffBe.OldAccountNo = txtOldAccNo.Text.Trim();
                objTariffBe.AccountNo = txtAccountNo.Text.Trim();
                objTariffBe.MeterNo = txtMeterNo.Text.Trim();
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    objTariffBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    objTariffBe.BU_ID = string.Empty;
                objTariffBe = objIdsignBal.DeserializeFromXml<TariffManagementBE>(objTariffBal.Get(objTariffBe, ReturnType.Double));

                if (objTariffBe.IsSuccess)
                {
                    if (objTariffBe.IsCustExistsInOtherBU)
                    {
                        lblAlertMsg.Text = UMS_Resource.CUST_EXISTS_OTHER_BU_MSG;
                        mpeAlert.Show();
                    }
                    else
                    {
                        lblGlobalAccNoAndAccNo.Text=objTariffBe.GlobalAccNoAndAccNo;
                        lblAccountNo.Text = objTariffBe.AccountNo;
                        lblOldAccountNo.Text = objTariffBe.OldAccountNo;
                        lblName.Text = objTariffBe.Name;
                        lblAddress.Text = objTariffBe.ServiceAddress;
                        lblLastBillDate.Text = objTariffBe.LastBillDate;
                        lblLastPaidDate.Text = objTariffBe.LastPaidDate;
                        lblDue.Text = objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(objTariffBe.DueAmount), 2, Constants.MILLION_Format);
                        lblTariff.Text = objTariffBe.Tariff;
                        lblClusterType.Text = objTariffBe.ClusterType;
                        divCustomerDetails.Visible = true;
                    }
                }
                else
                {
                    divCustomerDetails.Visible = false;
                    txtAccountNo.Text = txtMeterNo.Text = txtOldAccNo.Text = string.Empty;
                    if (objTariffBe.CustomerActiveStatusId == (int)CustomerStatus.InActive)
                    {
                        lblActiveMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.InActive, "Tariff Change");
                        mpeActivate.Show();
                    }
                    else if (objTariffBe.CustomerActiveStatusId == (int)CustomerStatus.Hold)
                    {
                        lblActiveMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.Hold, "Tariff Change");
                        mpeActivate.Show();
                    }
                    else if (objTariffBe.CustomerActiveStatusId == (int)CustomerStatus.Closed)
                    {
                        lblActiveMsg.Text = string.Format(Resource.CHNGS_NOW_ALWD, CustomerStatus.Closed, "Tariff Change");
                        mpeActivate.Show();
                    }
                    else
                        Message(UMS_Resource.CUSTOMER_NOT_FOUND_MSG, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}