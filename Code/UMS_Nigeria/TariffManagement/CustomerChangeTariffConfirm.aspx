﻿<%@ Page Title="Customer Tariff Change Confirm" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="CustomerChangeTariffConfirm.aspx.cs"
    Inherits="UMS_Nigeria.TariffManagement.CustomerChangeTariffConfirm" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/UserControls/Authentication.ascx" TagName="Authentication" TagPrefix="UC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upSearchCustomer" runat="server">
            <ContentTemplate>
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text-heading">
                        <asp:Literal ID="litSearchCustomer" runat="server" Text="<%$ Resources:Resource, CUST_CHANGE_TARIFF_CNF%>"></asp:Literal>
                    </div>
                    <div class="dot-line">
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="out-bor_a" id="divCustomerDetails" runat="server" visible="false">
                    <div class="inner-sec">
                        <div class="heading customerDetailstwo">
                            <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, CUSTOMER_DETAILS%>"></asp:Literal>
                        </div>
                        <div class="out-bor_cTwo">
                            <div class="inner-box">
                                <div class="inner-box1_a">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, ACCOUNT_NO%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_a">
                                    <div class="tariffconfirm-inner">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_a">
                                    <div class="text_cusa_a">
                                        <%--<asp:Label ID="lblAccountNo" runat="server" Text=""></asp:Label>--%>
                                        <asp:Label ID="lblGlobalAccNoAndAccNo" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lblAccountNo" Visible="false" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_a">
                                    <div class="text_cus">
                                        <asp:Literal ID="litOldAccountNo" runat="server" Text="Old Account No"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_a">
                                    <div class="tariffconfirm-inner">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_a">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblOldAccountNo" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_a">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_a">
                                    <div class="tariffconfirm-inner">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_a">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_a">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, ADDRESS%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_a">
                                    <div class="tariffconfirm-inner">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_a">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_a">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, TARIFF%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_a">
                                    <div class="tariffconfirm-inner">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_a">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblTariffId" Visible="false" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lblTariff" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_a">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, CLUSTERTYPE%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="inner-box2_a">
                                    <div class="tariffconfirm-inner">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_a">
                                    <div class="text_cusa_a">
                                        <asp:Label ID="lblClusterTypeId" Visible="false" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lblClusterType" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_a">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, CLUSTERTYPE%>"></asp:Literal>
                                        <span class="span_star">*</span></div>
                                </div>
                                <div class="inner-box2_a">
                                    <div class="tariffconfirm-inner">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_a">
                                    <div class="tariffconfirm-inner">
                                        <asp:DropDownList ID="ddlClusterType" runat="server" CssClass="text-box">
                                        </asp:DropDownList>
                                        <div class="space">
                                        </div>
                                        <span id="span1" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_a">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, CHANGE_TARIFF%>"></asp:Literal>
                                        <span class="span_star">*</span></div>
                                </div>
                                <div class="inner-box2_a">
                                    <div class="tariffconfirm-inner">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_a">
                                    <div class="tariffconfirm-inner">
                                        <asp:DropDownList ID="ddlTariff" AutoPostBack="true" OnSelectedIndexChanged="ddlTariff_SelectedIndexChanged"
                                            runat="server" CssClass="text-box">
                                            <asp:ListItem Text="--Select--"></asp:ListItem>
                                        </asp:DropDownList>
                                        <div class="space">
                                        </div>
                                        <span id="spanTariff" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_a">
                                    <div class="text_cus">
                                    </div>
                                </div>
                                <div class="inner-box2_a">
                                    <div class="tariffconfirm-inner">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_a">
                                    <div class="tariffconfirm-inner">
                                        <asp:DropDownList ID="ddlTariffSubTypes" Enabled="false" runat="server" CssClass="text-box">
                                            <asp:ListItem Text="--Select--"></asp:ListItem>
                                        </asp:DropDownList>
                                        <div class="space">
                                        </div>
                                        <span id="spanTariffSubTypes" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1_a">
                                    <div class="text_cus">
                                        <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, REMARKS%>"></asp:Literal>
                                        <span class="span_star">*</span></div>
                                </div>
                                <div class="inner-box2_a">
                                    <div class="tariffconfirm-inner">
                                        :
                                    </div>
                                </div>
                                <div class="inner-box3_a">
                                    <div class="tariffconfirm-inner">
                                        <asp:TextBox CssClass="text-box" runat="server" ID="txtRemarks" placeholder="Enter Comments Here."
                                            TextMode="MultiLine" ToolTip="Enter Comments Here."></asp:TextBox>
                                        <span id="spanRemarks" class="span_color"></span>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="text_cusa_b">
                                    <asp:Button ID="btnUpdate" OnClientClick="return Validate()" CssClass="box_s" Text="<%$ Resources:Resource, UPDATE%>"
                                        OnClick="btnUpdate_Click" runat="server"></asp:Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <UC:Authentication ID="ucAuthentication" runat="server" OnChangeTariffAuthentication="ChangeTariffAuthentication" />
                <div id="rnkland">
                    <%-- Confirmation Popup--%>
                    <asp:ModalPopupExtender ID="mpeCofirm" runat="server" PopupControlID="PanelCofirm"
                        TargetControlID="btnCofirm" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnCofirm" runat="server" Text="Button" CssClass="popbtn" />
                    <asp:Panel ID="PanelCofirm" runat="server" DefaultButton="btnYes" CssClass="modalPopup"
                        class="poppnl" Style="display: none;">
                        <div class="popheader popheaderlblred" id="pop" runat="server">
                            <asp:Label ID="lblConfirmMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnYes" runat="server" OnClientClick="return closePopup();" OnClick="btnYes_Click"
                                    Text="<%$ Resources:Resource, YES%>" CssClass="ok_btn" />
                                <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, NO%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Confirmation Popup--%>
                    <%-- Confirm popup Start--%>
                    <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                        TargetControlID="btnActivate" BackgroundCssClass="modalBackground">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelActivate" runat="server" DefaultButton="btnActiveCancel" CssClass="modalPopup"
                        Style="display: none; border-color: #639B00;">
                        <div class="popheader" style="background-color: #639B00;">
                            <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                        </div>
                        <br />
                        <div class="footer" align="right">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="cancel_btn" OnClick="btnActiveCancel_Click" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Confirm popup Closed--%>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".rnkland").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/CustomerChangeTariffConfirm.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
