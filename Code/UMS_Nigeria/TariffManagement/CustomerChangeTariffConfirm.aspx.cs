﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for CustomerChangeTariffConfirm
                     
 Developer        : id065-Bhimaraju Vanka
 Creation Date    : 11-08-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using Resources;
using iDSignHelper;
using AjaxControlToolkit;
using System.Threading;
using System.Globalization;

namespace UMS_Nigeria.TariffManagement
{
    public partial class CustomerChangeTariffConfirm : System.Web.UI.Page
    {
        #region Members

        iDsignBAL objIdsignBal = new iDsignBAL();
        TariffManagementBal objTariffBal = new TariffManagementBal();
        ApprovalBal objApprovalBal = new ApprovalBal();
        CommonMethods objCommonMethods = new CommonMethods();
        public string Key = UMS_Resource.CUSTOMER_CHANGE_TARIFF_CONFIRM;
        public string AccountNo = string.Empty;
        public int flag = 0;
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.CssClass = lblMessage.Text = string.Empty;
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString[UMS_Resource.QSTR_ACC_NO]))
                {
                    GetCustomerDetails();
                    objCommonMethods.BindTariff(ddlTariff, true);
                    objCommonMethods.BindClusterCategories(ddlClusterType, false);
                }
            }

        }

        protected void ddlTariff_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlTariff.SelectedIndex > 0)
                {
                    objCommonMethods.BindTariffSubTypesWithOutCustomerHavingTariff(ddlTariffSubTypes, Convert.ToInt32(ddlTariff.SelectedValue), lblTariff.Text, true, UMS_Resource.DROPDOWN_SELECT, false);
                    ddlTariffSubTypes.Enabled = true;
                }
                else
                {
                    ddlTariffSubTypes.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnActiveCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Constants.CustomerChangeTariff, false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                //If Authentication required uncommented  this code
                //Session[UMS_Resource.FROM_PAGE] = Constants.TariffChangeConfirm;
                //ModalPopupExtender ModalPopupExtenderLogin = (ModalPopupExtender)ucAuthentication.FindControl("ModalPopupExtenderLogin");
                //((TextBox)ucAuthentication.FindControl("txtPasswordLogin")).Focus();
                //ModalPopupExtenderLogin.Show();
                //If Authentication required uncommented  this code

                //If  Authentication required  commented this under code start
                //bool IsApproval = objCommonMethods.IsApproval((int)EnumApprovalFunctions.ChangeCustomerTariff);

                if (objCommonMethods.IsFinalApproval((int)EnumApprovalFunctions.ChangeCustomerTariff, Convert.ToInt32(Session[UMS_Resource.SESSION_ROLEID].ToString()), Session[UMS_Resource.SESSION_LOGINID].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString()))
                {
                    lblConfirmMsg.Text = Resource.CONF_MSG_CHANGE_TARIFF;
                    mpeCofirm.Show();
                }
                else
                {
                    TariffManagementBE objTariffBe = new TariffManagementBE();
                    objTariffBe.AccountNo = lblAccountNo.Text;
                    objTariffBe.OldClassID = Convert.ToInt32(lblTariffId.Text);
                    objTariffBe.NewClassID = Convert.ToInt32(ddlTariffSubTypes.SelectedValue);
                    objTariffBe.OldClusterTypeId = Convert.ToInt32(lblClusterTypeId.Text);
                    objTariffBe.ClusterTypeId = Convert.ToInt32(ddlClusterType.SelectedValue);
                    objTariffBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objTariffBe.Details = objIdsignBal.ReplaceNewLines(txtRemarks.Text, true);
                    objTariffBe.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerTariff;
                    objTariffBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                    objTariffBe.IsFinalApproval = false;

                    objTariffBe.Flag = Convert.ToInt32(Resources.Resource.FLAG_TEMP);
                    objTariffBe.ApprovalStatusId = (int)EnumApprovalStatus.Process;
                    objTariffBe = objIdsignBal.DeserializeFromXml<TariffManagementBE>(objTariffBal.Insert(objTariffBe, Statement.Change));
                    if (objTariffBe.IsSuccess)
                    {
                        ddlTariff.SelectedIndex = ddlTariffSubTypes.SelectedIndex = 0;
                        ddlTariffSubTypes.Enabled = false;
                        txtRemarks.Text = string.Empty;
                        if (objTariffBe.IsFinalApproval)
                            lblActiveMsg.Text = Resource.MODIFICATIONS_UPDATED;
                        else
                            lblActiveMsg.Text = Resource.APPROVAL_TARIFF_CHANGE;
                        mpeActivate.Show();
                    }
                }
                //If  Authentication required  commented this under code start end
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            try
            {
                TariffManagementBE objTariffBe = new TariffManagementBE();
                objTariffBe.AccountNo = lblAccountNo.Text;
                objTariffBe.OldClassID = Convert.ToInt32(lblTariffId.Text);
                objTariffBe.NewClassID = Convert.ToInt32(ddlTariffSubTypes.SelectedValue);
                objTariffBe.OldClusterTypeId = Convert.ToInt32(lblClusterTypeId.Text);
                objTariffBe.ClusterTypeId = Convert.ToInt32(ddlClusterType.SelectedValue);
                objTariffBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objTariffBe.Details = objIdsignBal.ReplaceNewLines(txtRemarks.Text, true);
                objTariffBe.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerTariff;
                objTariffBe.ApprovalStatusId = (int)EnumApprovalStatus.Approved;
                objTariffBe.IsFinalApproval = true;

                objTariffBe = objIdsignBal.DeserializeFromXml<TariffManagementBE>(objTariffBal.Insert(objTariffBe, Statement.Change));
                if (objTariffBe.IsSuccess)
                {
                    GetCustomerDetails();
                    ddlTariff.SelectedIndex = ddlTariffSubTypes.SelectedIndex = 0;
                    ddlTariffSubTypes.Enabled = false;
                    txtRemarks.Text = string.Empty;
                    lblActiveMsg.Text = Resource.MODIFICATIONS_UPDATED;
                    mpeActivate.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ChangeTariffAuthentication(object sender, EventArgs e)
        {
            try
            {
                //Session.Remove(UMS_Resource.FROM_PAGE);
                //bool IsApproval = objCommonMethods.IsApproval(Resource.FEATURE_CUST_TARIFF_CHANGE_REQUEST);
                //TariffManagementBE objTariffBe = new TariffManagementBE();
                //objTariffBe.AccountNo = lblAccountNo.Text;
                //objTariffBe.OldClassID = Convert.ToInt32(lblTariffId.Text);
                //objTariffBe.NewClassID = Convert.ToInt32(ddlTariffSubTypes.SelectedValue);
                //objTariffBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                //objTariffBe.Details = objIdsignBal.ReplaceNewLines(txtRemarks.Text,true);
                //objTariffBe.ApprovalStatusId = Constants.Active;
                //if (IsApproval)
                //{
                //    objTariffBe.Flag = Convert.ToInt32(Resources.Resource.FLAG_TEMP);
                //    objTariffBe = objIdsignBal.DeserializeFromXml<TariffManagementBE>(objTariffBal.Insert(objTariffBe, Statement.Change));
                //    if (objTariffBe.IsSuccess)
                //    {
                //        //Message(Resource.CUSTOMER_TARIFF_UPDATED, UMS_Resource.MESSAGETYPE_SUCCESS);
                //        //GetCustomerDetails();
                //        ddlTariff.SelectedIndex = ddlTariffSubTypes.SelectedIndex = 0;
                //        ddlTariffSubTypes.Enabled = false;
                //        txtRemarks.Text = string.Empty;
                //        lblActiveMsg.Text =Resource.APPROVAL_TARIFF_CHANGE;
                //        mpeActivate.Show();
                //    }
                //}
                //else
                //{
                //    objTariffBe = objIdsignBal.DeserializeFromXml<TariffManagementBE>(objTariffBal.Insert(objTariffBe, Statement.Change));
                //    if (objTariffBe.IsSuccess)
                //    {
                //        //Message(Resource.CUSTOMER_TARIFF_UPDATED, UMS_Resource.MESSAGETYPE_SUCCESS);
                //        GetCustomerDetails();
                //        ddlTariff.SelectedIndex = ddlTariffSubTypes.SelectedIndex = 0;
                //        ddlTariffSubTypes.Enabled = false;
                //        txtRemarks.Text = string.Empty;
                //        lblActiveMsg.Text = Resource.MODIFICATIONS_UPDATED;
                //        mpeActivate.Show();
                //    }
                //}

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods

        private void GetCustomerDetails()
        {
            try
            {
                TariffManagementBE objTariffBe = new TariffManagementBE();
                objTariffBe.OldAccountNo = "";
                objTariffBe.AccountNo = objIdsignBal.Decrypt(Request.QueryString[UMS_Resource.QSTR_ACC_NO]);
                objTariffBe.MeterNo = "";
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    objTariffBe.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    objTariffBe.BU_ID = string.Empty;
                objTariffBe = objIdsignBal.DeserializeFromXml<TariffManagementBE>(objTariffBal.Get(objTariffBe, ReturnType.Double));

                if (objTariffBe != null)
                {
                    lblGlobalAccNoAndAccNo.Text = objTariffBe.GlobalAccNoAndAccNo;
                    lblAccountNo.Text = objTariffBe.AccountNo;
                    lblOldAccountNo.Text = objTariffBe.OldAccountNo;
                    lblName.Text = objTariffBe.Name;
                    lblAddress.Text = objTariffBe.ServiceAddress;
                    lblTariff.Text = objTariffBe.Tariff;
                    lblTariffId.Text = objTariffBe.ClassID.ToString();
                    lblClusterType.Text = objTariffBe.ClusterType;
                    ddlClusterType.SelectedValue = lblClusterTypeId.Text = objTariffBe.ClusterTypeId.ToString();

                    divCustomerDetails.Visible = true;
                }
                else
                {
                    divCustomerDetails.Visible = false;
                    Message(UMS_Resource.CUSTOMER_NOT_FOUND_MSG, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}