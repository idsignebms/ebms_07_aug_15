﻿<%@ Page Title="::Customer Tariff Approval::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="CustomerTariffApproval.aspx.cs"
    Inherits="UMS_Nigeria.TariffManagement.CustomerTariffApproval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../scripts/jquery-ui.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <div class="inner-sec">
            <asp:UpdatePanel ID="upAddCycle" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litAddBookNumber" runat="server" Text="<%$ Resources:Resource, CUSTOMER_TARIFF_APPROVAL%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litBusinessUnit" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNIT%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:DropDownList ID="ddlBU" AutoPostBack="true" CssClass="text-box text-select"
                                    OnSelectedIndexChanged="ddlBU_SelectedIndexChanged" runat="server">
                                    <asp:ListItem Value="" Text="ALL"></asp:ListItem>
                                </asp:DropDownList>
                                <span id="spanBU" class="spancolor"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litServiceUnit" runat="server" Text="<%$ Resources:Resource, BOOK_NO%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:DropDownList ID="ddlBook" CssClass="text-box text-select" runat="server">
                                    <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <span id="spanBook" class="spancolor"></span>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="box_total">
                        <div class="box_total_a" style="margin-left: 15px;">
                            <asp:Button ID="btnSearch" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s"
                                OnClick="btnSearch_Click" runat="server" />
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div id="divgrid" class="grid" runat="server" style="width: 98.5% !important; float: left;
                        top: 20px; position: relative;">
                        <div id="divpaging" runat="server">
                            <div align="right" class="marg_20t" runat="server" id="divLinks">
                                <h3 class="gridheadGri" align="left">
                                    <asp:Literal ID="litBuList" runat="server" Text="<%$ Resources:Resource, CUSTOMER_TARIFF_APPROVAL_LIST%>"></asp:Literal>
                                </h3>
                                <asp:LinkButton ID="lkbtnFirst" runat="server" CssClass="pagingfirst" OnClick="lkbtnFirst_Click">
                                    <asp:Literal ID="litFirst" runat="server" Text="<%$ Resources:Resource, FIRST%>"></asp:Literal></asp:LinkButton>
                                &nbsp;|
                                <asp:LinkButton ID="lkbtnPrevious" runat="server" CssClass="pagingfirst" OnClick="lkbtnPrevious_Click">
                                    <asp:Literal ID="litPrevious" runat="server" Text="<%$ Resources:Resource, PREVIOUS%>"></asp:Literal></asp:LinkButton>
                                &nbsp;|
                                <asp:Label ID="lblCurrentPage" runat="server" Font-Bold="true"></asp:Label>
                                &nbsp;|
                                <asp:LinkButton ID="lkbtnNext" runat="server" CssClass="pagingfirst" OnClick="lkbtnNext_Click">
                                    <asp:Literal ID="litNext" runat="server" Text="<%$ Resources:Resource, NEXT%>"></asp:Literal></asp:LinkButton>
                                &nbsp;|
                                <asp:LinkButton ID="lkbtnLast" runat="server" CssClass="pagingfirst" OnClick="lkbtnLast_Click">
                                    <asp:Literal ID="litLast" runat="server" Text="<%$ Resources:Resource, LAST%>"></asp:Literal></asp:LinkButton>
                                &nbsp;|<asp:Literal ID="litPageSize" runat="server" Text="<%$ Resources:Resource, PAGE_SIZE%>"></asp:Literal>
                                <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" CssClass="paging-size"
                                    OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="consumer_total">
                        <div class="clear">
                        </div>
                    </div>
                    <%--  <div class="grid" id="divPrint" runat="server" align="center" style="overflow-x: auto;">
                    <asp:GridView ID="gvTariffApproval" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                        OnRowCommand="gvTariffApproval_RowCommand" OnRowDataBound="gvTariffApproval_RowDataBound">
                        <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                        <EmptyDataTemplate>
                            There is no Pending Approvals.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblTariffChangeRequestId" Visible="false" runat="server" Text='<%#Eval("TariffChangeRequestId")%>'></asp:Label>
                                    <asp:Label ID="lblRowNumber" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, ACCOUNT_NO%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblAccountNo" runat="server" Text='<%#Eval("AccountNo") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" HeaderText="<%$ Resources:Resource, NAME%>"
                                DataField="Name" />
                            <asp:BoundField ItemStyle-Width="18%" ItemStyle-HorizontalAlign="Center" HeaderText="<%$ Resources:Resource, SERVICE_ADDRESS%>"
                                DataField="ServiceAddress" />
                            <asp:BoundField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" HeaderText="<%$ Resources:Resource, CURRENT_TARIFF%>"
                                DataField="PreviousTariffName" />
                            <asp:TemplateField HeaderText="Requested Tariff" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblNewClassId" Visible="false" runat="server" Text='<%#Eval("NewClassID") %>'></asp:Label>
                                    <asp:Label ID="lblChangeRequestedTariffName" runat="server" Text='<%#Eval("ChangeRequestedTariffName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" HeaderText="Request Status"
                                DataField="Status" />
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, REMARKS%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblIsPerformAction" runat="server" Visible="false" Text='<%#Eval("IsPerformAction") %>'></asp:Label>
                                    <asp:Label ID="lblStatus" runat="server" Visible="false" Text='<%#Eval("Status") %>'></asp:Label>
                                    <div id="divAction" runat="server">
                                        <asp:LinkButton ID="lkbtnApproved" runat="server" ToolTip="Want To Approve this Tariff"
                                            Text="<%$ Resources:Resource, APPROVE%>" CommandName="Approved">
                                        </asp:LinkButton>/
                                        <asp:LinkButton ID="lkbtnOther" runat="server" Text="Other" ToolTip="Update Tariff Request"
                                            CommandName="OTHER"></asp:LinkButton>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>--%>
                    <div id="accordion">
                        <asp:Repeater ID="repTariffRequests" runat="server" OnItemCommand="repTariffRequests_ItemCommand"
                            OnItemDataBound="repTariffRequests_ItemDataBound">
                            <ItemTemplate>
                                <div class="approval_ac">
                                    <ul>
                                        <li>Account No :
                                            <asp:Label ID="lblTariffChangeRequestId" Visible="false" runat="server" Text='<%#Eval("TariffChangeRequestId")%>'></asp:Label>
                                            <asp:Label ID="lblIsPerformAction" runat="server" Visible="false" Text='<%#Eval("IsPerformAction") %>'></asp:Label>
                                            <asp:Label ID="lblPresentApprovalRoleId" Visible="false" runat="server" Text='<%#Eval("PresentApprovalRole") %>'></asp:Label>
                                            <asp:Label ID="lblNewCLusterTypeId" Visible="false" runat="server" Text='<%#Eval("NewClusterTypeId") %>'></asp:Label>
                                            <asp:Label ID="lblNewClassId" Visible="false" runat="server" Text='<%#Eval("NewClassID") %>'></asp:Label>
                                            <asp:Label ID="lblAccountNo" runat="server" Text='<%#Eval("AccountNo") %>'></asp:Label></li>
                                        <li>
                                            <asp:Label ID="lblCurrentTariff" runat="server" Text='<%#Eval("PreviousTariffName") %>'></asp:Label>
                                            -->
                                            <asp:Label ID="lblRequestedTariff" runat="server" Text='<%#Eval("ChangeRequestedTariffName") %>'></asp:Label>
                                            <%--<span style="margin: 0 10px 0 10px;">&</span>--%>
                                            <br />
                                            <asp:Label ID="lblOldCluster" runat="server" Text='<%#Eval("PreviousClusterName") %>'></asp:Label>
                                            -->
                                            <asp:Label ID="lblNewCluster" runat="server" Text='<%#Eval("ChangeRequestedClusterName") %>'></asp:Label></li>
                                        <li>
                                            <asp:Label ID="lblIsActionRequired" runat="server" Text=""></asp:Label></li>
                                    </ul>
                                </div>
                                <div class="borddder">
                                    <div class="approv_content">
                                        <div class="approv_text">
                                            <ul>
                                                <li>Name </li>
                                                <li>:</li>
                                                <li>
                                                    <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label></li>
                                            </ul>
                                        </div>
                                        <div class="approv_text">
                                            <ul>
                                                <li>Service Address </li>
                                                <li>:</li>
                                                <li>
                                                    <asp:Label ID="lblServiceAddress" runat="server" Text='<%#Eval("ServiceAddress") %>'></asp:Label></li>
                                            </ul>
                                        </div>
                                        <div class="approv_text">
                                            <ul>
                                                <li>Status</li>
                                                <li>:</li>
                                                <li>
                                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label></li>
                                            </ul>
                                        </div>
                                        <div class="approv_text">
                                            <ul>
                                                <li>Remarks</li>
                                                <li>:</li>
                                                <li>
                                                    <asp:Label ID="lblRemarks" runat="server" Text='<%#Eval("Details") %>'></asp:Label></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="clear pad_10">
                                    </div>
                                    <div class="approv_content">
                                        <div class="approv_text">
                                            <ul>
                                                <li>Current Role</li>
                                                <li>:</li>
                                                <li>
                                                    <asp:Label ID="lblCurrentRole" runat="server" Text='<%#Eval("CurrentRole") %>'></asp:Label></li>
                                            </ul>
                                        </div>
                                        <div class="approv_text">
                                            <ul>
                                                <li>Next Role</li>
                                                <li>:</li>
                                                <li>
                                                    <asp:Label ID="lblNextRole" runat="server" Text='<%#Eval("NextRole") %>'></asp:Label></li>
                                            </ul>
                                        </div>
                                        <div class="approv_text">
                                            <ul>
                                                <li>
                                                    <div id="divAction" runat="server">
                                                        <asp:LinkButton ID="lkbtnApproved" runat="server" ToolTip="Want To Approve this Tariff"
                                                            Text="<%$ Resources:Resource, APPROVE%>" CommandName="Approved">
                                                        </asp:LinkButton>/
                                                        <asp:LinkButton ID="lkbtnOther" runat="server" Text="Other" ToolTip="Update Tariff Request"
                                                            CommandName="OTHER"></asp:LinkButton>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="border_bottom clear">
                                    </div>
                                    <div class="clear pad_10">
                                    </div>
                                    <div class="app_msg_first" id="divConverstation" runat="server">
                                        <h2>
                                            Conversation</h2>
                                        <asp:Repeater ID="repConverstation" runat="server">
                                            <ItemTemplate>
                                                <div class="appr_msg_total">
                                                    <p>
                                                        <asp:Label ID="lblMessage" runat="server" Text='<%#Eval("Message") %>'></asp:Label></p>
                                                    <div class="appr_detail">
                                                        <ul>
                                                            <li>
                                                                <asp:Label ID="lblMessageBy" runat="server" Text='<%#Eval("CreatedBy") %>'></asp:Label></li>
                                                            <li>
                                                                <asp:Label ID="lblMessageTime" runat="server" Text='<%#Eval("CreatedDate") %>'></asp:Label></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                    <div class="appr_msg_total">
                                        <div class="consume_name">
                                            <asp:Literal ID="litNotes" runat="server" Text="Message"></asp:Literal>
                                        </div>
                                        <div class="consume_input">
                                            <asp:TextBox CssClass="text-box" ID="txtMessage" runat="server" TextMode="MultiLine"
                                                placeholder="Enter Message Here"></asp:TextBox>
                                        </div>
                                        <div class="clear pad_5">
                                        </div>
                                    </div>
                                    <asp:Button ID="btnSave" CommandName="MESSAGE" Text="Send" CssClass="box_s" runat="server" />
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <%-- Active Btn popup Start--%>
                    <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                        TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnActivate" runat="server" Text="Button" CssClass="popbtn" />
                    <asp:Panel ID="PanelActivate" runat="server" DefaultButton="btnActiveOk" CssClass="modalPopup"
                        class="poppnl" Style="display: none;">
                        <div class="popheader popheaderlblgreen" id="pop" runat="server">
                            <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                        </div>
                        <br />
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btnActiveOk" runat="server" OnClick="btnActiveOk_Click" Text="<%$ Resources:Resource, PROCEED%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Active  Btn popup Closed--%>
                    <asp:HiddenField ID="hfPageSize" runat="server" />
                    <asp:HiddenField ID="hfPageNo" runat="server" />
                    <asp:HiddenField ID="hfLastPage" runat="server" />
                    <asp:HiddenField ID="hfTotalRecords" runat="server" />
                    <asp:HiddenField ID="hfRecognize" runat="server" />
                    <asp:HiddenField ID="hfAccountNo" runat="server" />
                    <asp:HiddenField ID="hfTariffChangeRequestId" runat="server" />
                    <asp:HiddenField ID="hfClusterTypeId" runat="server" />
                    <asp:HiddenField ID="hfclassId" runat="server" />
                    <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="PanelAlert"
                        TargetControlID="btngridalert" BackgroundCssClass="modalBackground">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelAlert" runat="server" CssClass="modalPopup" Style="display: none;">
                        <div class="popheader popheaderlblred">
                            <asp:Label ID="lblAlertMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="popfooterbtn">
                                <center>
                                    <asp:HyperLink ID="hlkOk" NavigateUrl="~/Home.aspx" CssClass="ok_btn" runat="server"
                                        Text='<%$ Resources:Resource, OK%>'></asp:HyperLink>
                                </center>
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:ModalPopupExtender ID="mpeRequestAction" runat="server" PopupControlID="pnlRequestAction"
                        TargetControlID="HiddenField1" BackgroundCssClass="modalBackground" CancelControlID="btnCancel">
                    </asp:ModalPopupExtender>
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                    <asp:Panel ID="pnlRequestAction" runat="server" CssClass="modalPopup" Style="display: none;
                        min-width: 362px;" DefaultButton="btnOK">
                        <div class="consumer_feild">
                            <div class="Dflt_pop">
                                Request Action</div>
                            <div class="consume_name" style="margin-left: 10px;">
                                <asp:Literal ID="LiteralUnits" runat="server" Text="Select Status"></asp:Literal>
                                <span>*</span></div>
                            <div class="consume_input">
                                <asp:DropDownList ID="ddlApprovalStatus" TabIndex="1" runat="server">
                                    <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                                </asp:DropDownList>
                                <span id="spanApprovalStatus" class="spancolor"></span>
                            </div>
                            <div class="clear pad_5">
                            </div>
                            <div class="consume_name" style="margin-left: 10px;">
                                Remarks <span>*</span>
                            </div>
                            <div class="consume_input">
                                <asp:TextBox CssClass="text-box" Style="width: 91%;" ID="txtRemarks" runat="server"
                                    TextMode="MultiLine" placeholder="Enter Remarks Here"></asp:TextBox>
                                <span id="spanRemarks" class="spancolor"></span>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="clear pad_10">
                            </div>
                            <div class="consume_name c_left">
                            </div>
                            <div class="fltl">
                                <asp:Button ID="btnOK" runat="server" TabIndex="2" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" OnClientClick="return ValidateRequest()" OnClick="btnActiveOk_Click" />
                                <asp:Button ID="btnCancel" runat="server" TabIndex="3" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="ok_btn" />
                            </div>
                            <div class="clear pad_10">
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                        TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                        <div class="popheader popheaderlblred">
                            <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:HiddenField ID="hidAccordionIndex" runat="server" Value="0" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/CustomerTariffApproval.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
