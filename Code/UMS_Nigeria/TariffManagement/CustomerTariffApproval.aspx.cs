﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for Billing Month Open
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 14-Aug-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Globalization;
using System.Threading;
using System.Data;
using System.Drawing;
using System.Configuration;

namespace UMS_Nigeria.TariffManagement
{
    public partial class CustomerTariffApproval : System.Web.UI.Page
    {
        # region Members
        iDsignBAL _objiDsignBAL = new iDsignBAL();
        CommonMethods _objCommonMethods = new CommonMethods();
        TariffManagementBal objTariffBal = new TariffManagementBal();
        ApprovalBal objApprovalBal = new ApprovalBal();
        public int PageNum;
        XmlDocument xml = null;
        string Key = UMS_Resource.TARIFF_APPROVAL;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        public string BookNo
        {
            get { return string.IsNullOrEmpty(ddlBook.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlBook, ",") : ddlBook.SelectedValue; }
            set { ddlBook.SelectedValue = value.ToString(); }
        }
        public string BusinessUnit
        {
            get { return string.IsNullOrEmpty(ddlBU.SelectedValue) ? _objCommonMethods.CollectAllValues(ddlBU, ",") : ddlBU.SelectedValue; }
            set { ddlBU.SelectedValue = value.ToString(); }
        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = _objCommonMethods.GetPagePath(this.Request.Url);
                //if (_objCommonMethods.IsAccess(path))
                //{
                try
                {
                    if (!_objCommonMethods.CheckApprovalAuthorization())
                    {
                        lblAlertMsg.Text = Resource.ACCESS_DENIED_MSG;
                        mpeAlert.Show();
                    }
                    else
                    {
                        _objCommonMethods.BindUserBusinessUnits(ddlBU, string.Empty, false, Resource.DDL_ALL);
                        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        {
                            ddlBU.SelectedIndex = ddlBU.Items.IndexOf(ddlBU.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                            ddlBU.Enabled = false;
                            ddlBU_SelectedIndexChanged(ddlBU, new EventArgs());
                        }
                        else
                            _objCommonMethods.BindBookNo(ddlBook, string.Empty, Resource.DDL_ALL, false);
                        hfPageNo.Value = Constants.pageNoValue.ToString();
                        BindTariffApprovalList();
                        BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        _objCommonMethods.BindMoidfyApprovalStatusList(ddlApprovalStatus, UMS_Resource.SELECT);
                    }
                }
                catch (Exception ex)
                {
                    Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                    try
                    {
                        _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                    }
                    catch (Exception logex)
                    {

                    }
                }
                //}
                //else { Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }
            }
        }



        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void ddlBU_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //_objCommonMethods.BindBookNo(ddlBook, String.Empty, Resource.DDL_ALL, false); //If Data is not relavant plz uncomment and down one is comment
                //above code fills the data of all book no
                _objCommonMethods.BindBookNo(ddlBook, BusinessUnit, Resource.DDL_ALL, false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //protected void gvTariffApproval_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    try
        //    {

        //        if (e.Row.RowType == DataControlRowType.DataRow)
        //        {
        //            Label lblIsPerformAction = (Label)e.Row.FindControl("lblIsPerformAction");
        //            if (!Convert.ToBoolean(lblIsPerformAction.Text))
        //            {
        //                Label lblStatus = (Label)e.Row.FindControl("lblStatus");
        //                lblStatus.Visible = true;
        //                e.Row.FindControl("divAction").Visible = false;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}
        //protected void gvTariffApproval_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
        //        Label lblClassId = (Label)row.FindControl("lblNewClassId");
        //        Label lblNewCLusterTypeId = (Label)row.FindControl("lblNewCLusterTypeId");
        //        Label TariffChangeRequestId = (Label)row.FindControl("lblTariffChangeRequestId");
        //        Label lblAccountNo = (Label)row.FindControl("lblAccountNo");
        //        switch (e.CommandName.ToUpper())
        //        {
        //            case "APPROVED":
        //                mpeActivate.Show();
        //                hfclassId.Value = lblClassId.Text;
        //                hfClusterTypeId.Value = lblNewCLusterTypeId.Text;
        //                hfTariffChangeRequestId.Value = TariffChangeRequestId.Text;
        //                hfRecognize.Value = UMS_Resource.APPROVED;
        //                hfAccountNo.Value = lblAccountNo.Text;
        //                pop.Attributes.Add("class", "popheader popheaderlblgreen");
        //                lblActiveMsg.Text = Resource.CNF_APPROVED_POPUP_TEXT;
        //                btnActiveOk.Focus();
        //                break;
        //            case "OTHER":
        //                //mpeActivate.Show();
        //                hfclassId.Value = lblClassId.Text;
        //                hfClusterTypeId.Value = lblNewCLusterTypeId.Text;
        //                hfTariffChangeRequestId.Value = TariffChangeRequestId.Text;
        //                hfAccountNo.Value = lblAccountNo.Text;
        //                hfRecognize.Value = UMS_Resource.REJECTED;
        //                //pop.Attributes.Add("class", "popheader popheaderlblred");
        //                //lblActiveMsg.Text = Resource.CNF_REJECT_POPUP_TEXT;
        //                //btnActiveOk.Focus();
        //                ddlApprovalStatus.SelectedIndex = Constants.Zero;
        //                txtRemarks.Text = string.Empty;
        //                mpeRequestAction.Show();
        //                break;
        //        }

        //    }

        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }

        //}

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (hfRecognize.Value == UMS_Resource.APPROVED)
                {
                    hfRecognize.Value = string.Empty;
                    TariffManagementBE objTariffBe = new TariffManagementBE();
                    objTariffBe.NewClassID = Convert.ToInt32(hfclassId.Value);
                    objTariffBe.ClusterTypeId = Convert.ToInt32(hfClusterTypeId.Value);
                    objTariffBe.AccountNo = hfAccountNo.Value;
                    objTariffBe.TariffChangeRequestId = Convert.ToInt32(hfTariffChangeRequestId.Value);
                    objTariffBe.ApprovalStatusId = Constants.APPROVED;
                    objTariffBe.Flag = Constants.DeActive;
                    objTariffBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objTariffBe.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerTariff;
                    objTariffBe = _objiDsignBAL.DeserializeFromXml<TariffManagementBE>(objTariffBal.Insert(objTariffBe, Statement.Change));
                    if (objTariffBe.IsSuccess)
                    {
                        Message(Resource.TARIFF_APPROVED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        BindTariffApprovalList();
                        BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    }
                }
                else if (hfRecognize.Value == UMS_Resource.REJECTED) //means other than Approve
                {
                    hfRecognize.Value = string.Empty;
                    TariffManagementBE objTariffBe = new TariffManagementBE();
                    objTariffBe.NewClassID = Convert.ToInt32(hfclassId.Value);
                    objTariffBe.ClusterTypeId = Convert.ToInt32(hfClusterTypeId.Value);
                    objTariffBe.AccountNo = hfAccountNo.Value;
                    objTariffBe.TariffChangeRequestId = Convert.ToInt32(hfTariffChangeRequestId.Value);
                    objTariffBe.ApprovalStatusId = Convert.ToInt32(ddlApprovalStatus.SelectedValue);
                    objTariffBe.Flag = Constants.DeActive; //means From Approval Page
                    objTariffBe.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objTariffBe.Details = _objiDsignBAL.ReplaceNewLines(txtRemarks.Text, true);
                    objTariffBe.FunctionId = (int)EnumApprovalFunctions.ChangeCustomerTariff;
                    objTariffBe = _objiDsignBAL.DeserializeFromXml<TariffManagementBE>(objTariffBal.Insert(objTariffBe, Statement.Change));
                    if (objTariffBe.IsSuccess)
                    {
                        Message("Tariff Change Request updated successfully.", UMS_Resource.MESSAGETYPE_SUCCESS);
                        BindTariffApprovalList();
                        BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        ddlApprovalStatus.SelectedIndex = Constants.Zero;
                        txtRemarks.Text = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                BindTariffApprovalList();
                BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods
        private void BindTariffApprovalList()
        {
            try
            {
                TariffManagementBE objTariffBe = new TariffManagementBE();
                objTariffBe.BU_ID = this.BusinessUnit;
                objTariffBe.BookNo = this.BookNo;
                objTariffBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                objTariffBe.PageSize = this.PageSize;
                objTariffBe.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                //TariffManagementListBE objTariffListBe = _objiDsignBAL.DeserializeFromXml<TariffManagementListBE>(objTariffBal.Get(objTariffBe, ReturnType.List));
                DataSet ds = objTariffBal.GetTariffApprovalList(objTariffBe);
                hidAccordionIndex.Value = Constants.Zero.ToString();
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        divgrid.Visible = divpaging.Visible = true;
                        hfTotalRecords.Value = ds.Tables[0].Rows[0]["TotalRecords"].ToString();
                        repTariffRequests.DataSource = ds.Tables[0];
                        repTariffRequests.DataBind();

                        foreach (RepeaterItem RequestItem in repTariffRequests.Items)
                        {
                            Label lblRequestId = (Label)RequestItem.FindControl("lblTariffChangeRequestId");
                            var messages = (from list in ds.Tables[1].AsEnumerable()
                                            where list.Field<int>("TariffChangeRequestId") == Convert.ToInt32(lblRequestId.Text)
                                            orderby list["MessageId"] ascending
                                            select list);
                            if (messages.Count() > 0)
                            {
                                DataTable dt = new DataTable();
                                dt = messages.CopyToDataTable<DataRow>();
                                Repeater repConverstation = (Repeater)RequestItem.FindControl("repConverstation");
                                repConverstation.DataSource = dt;
                                repConverstation.DataBind();
                            }
                            else
                            {
                                RequestItem.FindControl("divConverstation").Visible = false;
                            }
                        }
                    }
                    else
                    {
                        hfTotalRecords.Value = ds.Tables[0].Rows.Count.ToString();
                        divgrid.Visible = divpaging.Visible = false;
                        repTariffRequests.DataSource = new DataTable();
                        repTariffRequests.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Pageing
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindTariffApprovalList();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindTariffApprovalList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindTariffApprovalList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindTariffApprovalList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                BindTariffApprovalList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                _objiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                _objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        protected void repTariffRequests_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                Label lblClassId = (Label)e.Item.FindControl("lblNewClassId");
                Label lblNewCLusterTypeId = (Label)e.Item.FindControl("lblNewCLusterTypeId");
                Label lblPresentApprovalRoleId = (Label)e.Item.FindControl("lblPresentApprovalRoleId");
                Label lblTariffChangeRequestId = (Label)e.Item.FindControl("lblTariffChangeRequestId");
                Label lblAccountNo = (Label)e.Item.FindControl("lblAccountNo");


                switch (e.CommandName.ToUpper())
                {
                    case "APPROVED":
                        if (Session[UMS_Resource.SESSION_ROLEID].ToString() == lblPresentApprovalRoleId.Text)
                        {
                            mpeActivate.Show();
                            hfclassId.Value = lblClassId.Text;
                            hfClusterTypeId.Value = lblNewCLusterTypeId.Text;
                            hfTariffChangeRequestId.Value = lblTariffChangeRequestId.Text;
                            hfRecognize.Value = UMS_Resource.APPROVED;
                            hfAccountNo.Value = lblAccountNo.Text;
                            pop.Attributes.Add("class", "popheader popheaderlblgreen");
                            lblActiveMsg.Text = Resource.CNF_APPROVED_POPUP_TEXT;
                            btnActiveOk.Focus();
                        }
                        else
                        {
                            Message("You don't have permissions to Approve Now.", UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        break;
                    case "OTHER":
                        if (Session[UMS_Resource.SESSION_ROLEID].ToString() == lblPresentApprovalRoleId.Text)
                        {
                            //mpeActivate.Show();
                            hfclassId.Value = lblClassId.Text;
                            hfClusterTypeId.Value = lblNewCLusterTypeId.Text;
                            hfTariffChangeRequestId.Value = lblTariffChangeRequestId.Text;
                            hfAccountNo.Value = lblAccountNo.Text;
                            hfRecognize.Value = UMS_Resource.REJECTED;
                            //pop.Attributes.Add("class", "popheader popheaderlblred");
                            //lblActiveMsg.Text = Resource.CNF_REJECT_POPUP_TEXT;
                            //btnActiveOk.Focus();
                            ddlApprovalStatus.SelectedIndex = Constants.Zero;
                            txtRemarks.Text = string.Empty;
                            mpeRequestAction.Show();
                        }
                        else
                        {
                            Message("You don't have permissions to Reject Now.", UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        break;
                    case "MESSAGE":
                        TextBox txtMessage = (TextBox)e.Item.FindControl("txtMessage");
                        ApprovalMessagesBe objMessageBe = new ApprovalMessagesBe();
                        objMessageBe.TariffChangeRequestId = Convert.ToInt32(lblTariffChangeRequestId.Text);
                        objMessageBe.Message = _objiDsignBAL.ReplaceNewLines(txtMessage.Text, true);
                        objMessageBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objMessageBe = _objiDsignBAL.DeserializeFromXml<ApprovalMessagesBe>(objApprovalBal.Insert(objMessageBe, Statement.Service));
                        if (objMessageBe.IsSuccess)
                        {
                            Repeater repConverstation = (Repeater)e.Item.FindControl("repConverstation");
                            objMessageBe.TariffChangeRequestId = Convert.ToInt32(lblTariffChangeRequestId.Text);
                            DataSet ds = objApprovalBal.GetDataSet(objMessageBe, ReturnType.Get);
                            repConverstation.DataSource = ds.Tables[0];
                            repConverstation.DataBind();
                            txtMessage.Text = string.Empty;
                            hidAccordionIndex.Value = e.Item.ItemIndex.ToString();
                            e.Item.FindControl("divConverstation").Visible = true;
                            Message(Resource.APPROVAL_MESSAGE_SAVED_MSG, UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void repTariffRequests_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Label lblIsPerformAction = (Label)e.Item.FindControl("lblIsPerformAction");
                    Label lblIsActionRequired = (Label)e.Item.FindControl("lblIsActionRequired");
                    TextBox txtMessage = (TextBox)e.Item.FindControl("txtMessage");
                    Button btnSave = (Button)e.Item.FindControl("btnSave");
                    if (!Convert.ToBoolean(lblIsPerformAction.Text))
                    {
                        RepeaterItem item = e.Item;
                        Label lblStatus = (Label)item.FindControl("lblStatus");
                        lblStatus.Visible = true;
                        item.FindControl("divAction").Visible = false;
                        lblIsActionRequired.Text = Resource.APPROVAL_FROM_OTHER_MSG;
                        lblIsActionRequired.CssClass = Resource.APPROVAL_DISABLED_CSS;
                    }
                    else
                        lblIsActionRequired.Text = Resource.APPROVAL_REQUIRED_MSG;
                    btnSave.Attributes.Add("onclick", "return ValidateMessage('" + txtMessage.ClientID + "')");
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
    }
}