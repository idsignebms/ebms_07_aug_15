<%@ Page Title="::Bill Calculator::" Language="C#" MasterPageFile="~/MasterPages/EDMUMS.Master"
    AutoEventWireup="true" CodeBehind="TariffDetails.aspx.cs" Inherits="UMS_Nigeria.TariffCharge" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/Calendar.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="edmcontainer">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <asp:UpdatePanel ID="upBillCalculator" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlErrorMsg" runat="server" Style="width: 100%;">
                    <asp:Label ID="lblErrorMsg" runat="server"></asp:Label>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <h3>
            <asp:Literal ID="litAddAgency" runat="server" Text="<%$ Resources:Resource, BILL_CALCULATOR%>"></asp:Literal>
        </h3>
        <div class="clear">
            &nbsp;</div>
        <div class="consumer_total">
            <div class="pad_10">
            </div>
        </div>
        <div class="consumer_feild">
            <div class="consume_name">
                Select Category
            </div>
            <div class="consume_input consume_radio">
                <asp:RadioButtonList ID="rblCalCulatorType" Style="width: 250px;" runat="server"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Text="Customer Wise" Value="1" onclick="CalculatorType()"></asp:ListItem>
                    <asp:ListItem Text="Tariff Wise" Value="2" onclick="CalculatorType()"></asp:ListItem>
                </asp:RadioButtonList>
                <span id="spanCalculatorType" style="color: Red"></span>
            </div>
        </div>
        <div class="clear pad_10">
        </div>
        <div class="consumer_feild" id="divTariffClass" style="display: none;">
            <%-- <div class="consume_name">
                    Tariff Type<span>*</span></div>
                <div class="consume_input">
                    <asp:DropDownList ID="ddlTariffType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTariffType_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>--%>
            <div class="consume_name">
                Tariff <span>*</span></div>
            <div class="consume_input" style="width: 150px">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="ddlTariffSubType" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlTariffSubType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="consumer_feild" id="divAccountNo" style="display: none;">
            <div class="consume_name">
                Account No <span>*</span></div>
            <div class="consume_input" style="width: 150px">
                <asp:TextBox CssClass="text-box"  ID="txtAccountNo" onblur="return TextBoxBlurValidationlbl(this,'spanAccountNo','AccountNo')"
                    runat="server"></asp:TextBox>
                <span id="spanAccountNo" style="color: Red"></span>
            </div>
        </div>
        <div class="consumer_feild">
            <div class="consume_name">
                Kwh <span>*</span></div>
            <div class="consume_input">
                <asp:TextBox CssClass="text-box"  ID="txtKw" onblur="return TextBoxBlurValidationlbl(this,'spanKw','Kwh')"
                    runat="server" Text="1"></asp:TextBox>
                <asp:FilteredTextBoxExtender ID="ftbTxtkW" runat="server" TargetControlID="txtKw"
                    FilterType="Custom,Numbers" ValidChars=".">
                </asp:FilteredTextBoxExtender>
                <span id="spanKw" style="color: Red"></span>
            </div>
            <div class="consume_name">
                BillDate<span>*</span></div>
            <div class="consume_input">
                <asp:TextBox CssClass="text-box"  ID="txtBillDate" onchange="return TextBoxBlurValidationlbl(this,'spanBillDate','BillDate')"
                    onblur="return TextBoxBlurValidationlbl(this,'spanBillDate','BillDate')" Style="width: 125px;"
                    runat="server"></asp:TextBox>
                <asp:Image ID="imgDate" ImageUrl="~/images/icon_calendar.png" Style="width: 20px;
                    vertical-align: middle" AlternateText="Calender" runat="server" />
                <asp:Label ID="lblBillDate" runat="server" ForeColor="Red" Style="margin-left: 13px"></asp:Label>
                <asp:FilteredTextBoxExtender ID="ftbDob" runat="server" TargetControlID="txtBillDate"
                    FilterType="Custom,Numbers" ValidChars="/">
                </asp:FilteredTextBoxExtender>
                <span id="spanBillDate" style="color: Red"></span>
            </div>
        </div>
        <div class="consumer_feild">
            <div class="consume_name">
                Customer Type
            </div>
            <div class="consume_input">
                <asp:DropDownList ID="ddlCustomerType" runat="server">
                    <asp:ListItem Text="Consumption" Value="2" Selected="True">
                    </asp:ListItem>
                    <asp:ListItem Text="Demand" Value="1"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="clear">
            &nbsp;
        </div>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <div class="consumer_feild">
                    <div class="btncalc">
                        <asp:Button ID="btnCalculate" Text="Calculate" CssClass="calculate_but" runat="server"
                            OnClick="btnCalculate_Click" OnClientClick="return Validation()" />
                    </div>
                </div>
                <div class="clear pad_10">
                </div>
                <div class="consumer_feild lblaccno">
                    <div>
                        <div class="consume_name ac_tariff">
                            <asp:Label ID="lblAccountNoText" Style="color: Black;" runat="server" Text="Account No:"></asp:Label></div>
                        <div class="consume_name ac_tariff_value">
                            <asp:Label ID="lblAccountNo" Text="--" runat="server" Visible="true"></asp:Label>
                        </div>
                    </div>
                    <div class="clear pad_2">
                    </div>
                    <div>
                        <div class="consume_name ac_tariff">
                            Energy Charge:</div>
                        <div class="consume_input ac_tariff_value">
                            <asp:Label ID="lblEneryChargeAmount" Text="0.0" runat="server" Visible="true"></asp:Label>
                        </div>
                    </div>
                    <div class="clear pad_2">
                    </div>
                    <div>
                        <div class="consume_name ac_tariff">
                            Additional Charge:</div>
                        <div class="consume_input ac_tariff_value">
                            <asp:Label ID="lblAdditionalChargeAmount" Text="0.0" runat="server" Visible="true"></asp:Label>
                        </div>
                    </div>
                    <div class="clear pad_2">
                    </div>
                    <div>
                        <div class="consume_name ac_tariff">
                            Tax:</div>
                        <div class="consume_input ac_tariff_value">
                            <asp:Label ID="lblTax" Text="0.0" runat="server" Visible="true"></asp:Label>
                        </div>
                    </div>
                    <div class="clear pad_2">
                    </div>
                    <div>
                        <div class="consume_name ac_tariff">
                            Total Amount:</div>
                        <div class="consume_input ac_tariff_value">
                            <asp:Label ID="lblAmount" Text="0.0" runat="server" Visible="true"></asp:Label></div>
                    </div>
                </div>
                <div class="tariff_type" id="divTariffDetails">
                    <div id="divTariff" runat="server" visible="false">
                        <storng>  <b>  Tariff Type : </b></strong>
                       
                        <asp:Label ID="lblTariffType" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="grid">
                        <asp:GridView ID="gvTariffDetails" Width="250px" AutoGenerateColumns="false" runat="server"
                            Visible="true" CssClass="charges_bind" OnRowDataBound="gvTariffDetails_RowDataBound">
                            <Columns>
                                <asp:BoundField HeaderText="Charge" DataField="ChargeName" NullDisplayText="--" />
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, AMOUNT%>" ItemStyle-Width="8%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("Amount") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:BoundField HeaderText="Amount" DataField="Amount" NullDisplayText="--" /> --%>
                                <asp:BoundField HeaderText="Tax" DataField="Tax" NullDisplayText="--" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div class="clear pad_10">
                    <br />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <%--==============Escape button script starts==============--%>
        <%--==============Escape button script ends==============--%>
        <%--==============Validation script ref starts==============--%>
        <script src="../scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
        <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
        <script src="../scripts/jquery.ui.core.js" type="text/javascript"></script>
        <script src="../scripts/jquery.ui.datepicker.js" type="text/javascript"></script>
        <script src="../JavaScript/PageValidations/TariffDetails.js" type="text/javascript"></script>
        <%--==============Validation script ref ends==============--%>
        <%--==============Ajax loading script starts==============--%>
        <script type="text/javascript">
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
            prm.add_beginRequest(BeginRequestHandler);
            // Raised after an asynchronous postback is finished and control has been returned to the browser.
            prm.add_endRequest(EndRequestHandler);
            function BeginRequestHandler(sender, args) {
                //Shows the modal popup - the update progress
                var popup = $find('<%= modalPopupLoading.ClientID %>');
                if (popup != null) {
                    popup.show();
                }
            }

            function EndRequestHandler(sender, args) {
                //Hide the modal popup - the update progress
                var popup = $find('<%= modalPopupLoading.ClientID %>');
                if (popup != null) {
                    popup.hide();
                }
            }
   
        </script>
        <%--==============Validation script ref ends==============--%>
    </div>
</asp:Content>
<%--</asp:Content--%>
