﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using iDSignHelper;
using System.Xml;
using Resources;
using UMS_NigriaDAL;

namespace UMS_Nigeria
{
    public partial class TariffCharge : System.Web.UI.Page
    {

        #region Members
        UMS_Service _ObjUMS_Service = new UMS_Service();
        BillCalculatorBE _ObjBillCalculatorBE = new BillCalculatorBE();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        BillCalculatorListBE _ObjBillCalculatorListBE = new BillCalculatorListBE();
        BillCalculatorListCharges _ObjBillCalculatorListCharges = new BillCalculatorListCharges();
        BillCalculatorBAL _ObjBillCalculatorBAL = new BillCalculatorBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        string Key = "Default";

        #endregion

        #region Properties
        private string AccountNo
        {
            get { return txtAccountNo.Text; }
        }

        private int TariffClassId
        {
            get { return Convert.ToInt32(ddlTariffSubType.SelectedValue); }
        }
        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlErrorMsg.CssClass = lblErrorMsg.Text = string.Empty;
            if (!IsPostBack)
            {
                string path = string.Empty;
                path = _ObjCommonMethods.GetPagePath(this.Request.Url);
                if (_ObjCommonMethods.IsAccess(path))
                {
                    BindTariffSubTypes();
                    TimeZoneInfo IND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                    DateTime currentTime = DateTime.Now;
                    DateTime ourtime = TimeZoneInfo.ConvertTime(currentTime, IND_ZONE);
                    txtBillDate.Text = ourtime.ToString("dd/MM/yyyy").Replace("-", "/");
                    //ddlTariffSubType_SelectedIndexChanged(ddlTariffSubType, new EventArgs());
                }
                else
                {
                    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                }
            }
        }

        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            BillCalculatorBE _ObjBillCalculatorBE = new BillCalculatorBE();
            if (rblCalCulatorType.Items[0].Selected)
            {
                _ObjBillCalculatorBE.AccountNo = AccountNo;
                _ObjBillCalculatorBE = _ObjiDsignBAL.DeserializeFromXml<BillCalculatorBE>(_ObjBillCalculatorBAL.GetFixedAndEneryCharges(_ObjBillCalculatorBE, ReturnType.Single));
                if (!_ObjBillCalculatorBE.IsAccountNoExists)
                {
                    Message(UMS_Resource.ACCOUNT_NO_NOT_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                    return;
                }
                else
                    CalCulateBill();
            }
            else
                CalCulateBill();
        }

        private void CalCulateBill()
        {
            XmlDocument xml = null;
            try
            {
                BillCalculatorBE _ObjBillCalculatorBE = new BillCalculatorBE();
                _ObjBillCalculatorBE.Date = _ObjCommonMethods.Convert_MMDDYY(txtBillDate.Text);
                _ObjBillCalculatorBE.Consumption = float.Parse(txtKw.Text);
                if (rblCalCulatorType.Items[0].Selected)
                {
                    _ObjBillCalculatorBE.AccountNo = AccountNo;
                    _ObjBillCalculatorBE.SubType = Constants.Zero;
                    lblAccountNo.Text = AccountNo;
                    lblAccountNoText.Text = "Account No";
                }
                else
                {
                    _ObjBillCalculatorBE.AccountNo = string.Empty;
                    _ObjBillCalculatorBE.SubType = TariffClassId;
                    lblAccountNoText.Text = "Tariff";
                    lblAccountNo.Text = ddlTariffSubType.SelectedItem.Text;
                }
                // _ObjBillCalculatorBE.Vat = Vat;
                _ObjBillCalculatorBE.CustomerTypeId = Convert.ToInt32(ddlCustomerType.SelectedValue);
                xml = _ObjBillCalculatorBAL.CalculateBillWithConsumption(_ObjBillCalculatorBE, ReturnType.Get);
                //xml = _ObjUMS_Service.CalculateBillWithConsumption(_ObjCommonMethods.Convert_MMDDYY(txtBillDate.Text), Convert.ToInt32(ddlTariffSubType.SelectedValue), float.Parse(txtKw.Text), Convert.ToInt32(ddlCustomerType.SelectedValue));
                _ObjBillCalculatorListBE = _ObjiDsignBAL.DeserializeFromXml<BillCalculatorListBE>(xml);
                _ObjBillCalculatorListCharges = _ObjiDsignBAL.DeserializeFromXml<BillCalculatorListCharges>(xml);

                if (_ObjBillCalculatorListBE.items.Count > 0)
                {
                    lblEneryChargeAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(_ObjBillCalculatorListBE.items[0].EnergyChargeAmount), 2, Constants.MILLION_Format);
                    lblAdditionalChargeAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(_ObjBillCalculatorListBE.items[0].AdditionalChargeAmount), 2, Constants.MILLION_Format);
                    lblTax.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(_ObjBillCalculatorListBE.items[0].TaxAmount), 2, Constants.MILLION_Format);
                    lblAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(_ObjBillCalculatorListBE.items[0].TotalEnergyCharges), 2, Constants.MILLION_Format);
                    lblTariffType.Text = _ObjBillCalculatorListBE.items[0].TariffType;
                    divTariff.Visible = true;
                }
                gvTariffDetails.DataSource = _ObjBillCalculatorListCharges.items;
                gvTariffDetails.DataBind();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvTariffDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lblAmount = (Label)e.Row.FindControl("lblAmount");
                    lblAmount.Text = _ObjCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblAmount.Text), 2, Constants.MILLION_Format) + "/kWh";
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlTariffType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindTariffSubTypes();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindTariffSubTypes()
        {
            try
            {
                BillCalculatorBE _ObjBillCalculatorBE = new BillCalculatorBE();
                BillCalculatorListBE _ObjBillCalculatorListBE = new BillCalculatorListBE();
                //_ObjUMS_Service.GetCustomerSubTypes(0);
                _ObjBillCalculatorBE.ClassId = 0;
                XmlDocument xml = _ObjBillCalculatorBAL.GetCustomerSubTypes(_ObjBillCalculatorBE, ReturnType.Multiple);
                if (xml != null)
                {
                    _ObjBillCalculatorListBE = _ObjiDsignBAL.DeserializeFromXml<BillCalculatorListBE>(xml);
                    _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<BillCalculatorBE>(_ObjBillCalculatorListBE.items), ddlTariffSubType, "CustomerSubTypeNames", "ClassId", true);

                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlTariffSubType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                XmlDocument xml = null;
                xml = _ObjUMS_Service.CalculateBillWithConsumption(_ObjCommonMethods.Convert_MMDDYY(txtBillDate.Text), Convert.ToInt32(ddlTariffSubType.SelectedValue), float.Parse(txtKw.Text), Convert.ToInt32(ddlCustomerType.SelectedValue));
                _ObjBillCalculatorListBE = _ObjiDsignBAL.DeserializeFromXml<BillCalculatorListBE>(xml);
                _ObjBillCalculatorListCharges = _ObjiDsignBAL.DeserializeFromXml<BillCalculatorListCharges>(xml);

                if (_ObjBillCalculatorListBE.items.Count > 0)
                {
                    lblTariffType.Text = _ObjBillCalculatorListBE.items[0].TariffType;
                    divTariff.Visible = true;
                }
                gvTariffDetails.DataSource = _ObjBillCalculatorListCharges.items;
                gvTariffDetails.DataBind();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #region Methods

        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlErrorMsg, lblErrorMsg);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        //private void BindCustomerType()    //BindCutomerTypes
        //{
        //    _ObjBillCalculatorListBE = _ObjiDsignBAL.DeserializeFromXml<BillCalculatorListBE>(_ObjUMS_Service.GetCutomerType());
        //    _ObjiDsignBAL.FillDropDownList(_ObjiDsignBAL.ConvertListToDataSet<BillCalculatorBE>(_ObjBillCalculatorListBE.items), ddlTariffType, "CustomerTypeName", "ClassId", true);
        //}
        #endregion
    }
}