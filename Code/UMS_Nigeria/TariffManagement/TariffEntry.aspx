﻿<%@ Page Title="::Tariff Entry::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="TariffEntry.aspx.cs" Inherits="UMS_Nigeria.TariffManagement.TariffEntry" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="upnlTariffEntry" runat="server">
        <ContentTemplate>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litTariffEntry" runat="server" Text="<%$ Resources:Resource, TARIFF_ENTRY%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="inner-box">
                        <div id="divTariffClasses">
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litTariffClass" runat="server" Text="<%$ Resources:Resource, TARIFF_CLASS%>"></asp:Literal><span
                                            class="span_star">*</span></label>
                                    <div class="space">
                                    </div>
                                    <asp:DropDownList ID="ddlTariff" CssClass="text-box text-select" AutoPostBack="true"
                                        runat="server" OnSelectedIndexChanged="ddlTariff_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <div class="clr">
                                        <br />
                                        <br />
                                    </div>
                                    <div style="margin-left:-20px;width:100%;">
                                        <asp:PlaceHolder ID="phTariffSubTypes" runat="server" Visible="false">
                                            <asp:DropDownList ID="ddlTariffSubType" CssClass="text-box text-select" runat="server"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlTariffSubType_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <br />
                                        </asp:PlaceHolder>
                                    </div>
                                </div>
                                <span id="spanTariff" class="span_color_btwo">
                                </span><span id="spanddlBusinessUnitName" class="span_color"></span>
                            </div>
                            <div class="clr">
                                <br />
                            </div>
                            <div class="inner-box2">
                                <div class="text-inner">
                                    <label for="name">
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="tariffentry_baa">
                        <div class="tariffentry_b1 text-inner">
                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, FROM%>"></asp:Literal><span
                                class="span_star">*</span>
                        </div>
                        <div class="tariffentry_b2">
                            <asp:DropDownList ID="ddlFromMonth" CssClass="text_selectbb" runat="server" Enabled="false"
                                onchange="DropDownlistOnChangelbl(this,'spanFromMonth','From Month')">
                                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <br />
                            <span id="spanFromMonth" class="span_color_b"></span>
                        </div>
                        <div class="tariffentry_b3">
                            <asp:DropDownList ID="ddlFromYear" CssClass="text_selectbb" runat="server" Enabled="false"
                                onchange="DropDownlistOnChangelbl(this,'spanFromYear','From Year')">
                                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <br />
                            <span id="spanFromYear" class="span_color_b"></span>
                        </div>
                        <div class="tariffentry_b1" style="margin:0 0 0 74px;">
                            <div class="text-inner">
                                <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, TO%>"></asp:Literal><span
                                    class="span_star">*</span>
                            </div>
                        </div>
                        <div class="tariffentry_b2" style="margin-left: -60px;">
                            <asp:DropDownList ID="ddlToMonth" CssClass="text_selectbb" runat="server" onchange="DropDownlistOnChangelbl(this,'spanToMonth','To Month')">
                                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <br />
                            <span id="spanToMonth" class="span_color_b"></span>
                        </div>
                        <div class="tariffentry_b3">
                            <asp:DropDownList ID="ddlToYear" CssClass="text_selectbb" runat="server" onchange="DropDownlistOnChangelbl(this,'spanToYear','To Year')">
                                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <br />
                            <span id="spanToYear" class="span_color_b"></span>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div style="margin-left: -20px;" class="consumer_feildTwo">
                        <div class="tariffpanel">
                            <div class="consumer_feild" id="divCharges">
                                <div class="consume_name" style="width: 108px;">
                                    <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, FROM_KWH%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </div>
                                <div class="consume_input">
                                    <asp:TextBox ID="txtFromKw" CssClass="FromKW bgwhite" runat="server" MaxLength="7"
                                        onfocus="DoFocus(this);" placeholder="From kWh" Enabled="false"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtFromKw"
                                        FilterType="Numbers">
                                    </asp:FilteredTextBoxExtender>
                                </div>
                                <div class="consume_name" style="width: 84px;margin-left: 15px;">
                                    <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, TO_KWH%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </div>
                                <div class="consume_input">
                                    <asp:TextBox ID="txtToKw" CssClass="ToKW bgwhite" runat="server" MaxLength="7" placeholder="To kWh"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtToKw"
                                        FilterType="Numbers">
                                    </asp:FilteredTextBoxExtender>
                                </div>
                                <div class="consume_name" style="width: 81px;margin-left: 15px;">
                                    <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, CHARGE%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </div>
                                <div class="consume_input">
                                    <%--onkeypress="return isNumberKey(event,this)"--%>
                                    <asp:TextBox ID="txtChargeAmount" onkeyup="GetCurrencyFormate(this);" CssClass="Charges bgwhite"
                                        onblur="return ValidatePaidAmount(this)" MaxLength="16" runat="server" placeholder="Enter Charge"
                                        onkeypress="return isNumberKey(event,this)"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="ftbTxtGridContactNo2" runat="server" TargetControlID="txtChargeAmount"
                                        FilterType="Numbers,Custom" ValidChars=".," FilterMode="ValidChars">
                                    </asp:FilteredTextBoxExtender>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                        </div>
                        <div class="tariff_entry_box_cc">
                            <div class="text-innerBox">
                                <span class="heading_bottam">
                                    <asp:LinkButton ID="lbtnAdd" runat="server" OnClientClick="return AddCharge()">Add</asp:LinkButton>
                                </span>&nbsp; <span class="heading_bottam">
                                    <asp:LinkButton ID="lnkRemove" runat="server" OnClientClick="return removeChargs();">Remove</asp:LinkButton>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="consumer_total">
                    <div class="pad_10 clear">
                    </div>
                </div>
                <div class="clr">
                </div>
                <div style="clear: both;">
                </div>
                <div class="box_total">
                    <div class="box_totalTwi_d" style="margin-top: -29px;">
                        <asp:Button ID="btnSave" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                            CssClass="box_s" runat="server" OnClick="btnSave_Click" TabIndex="6" />
                    </div>
                </div>
                <div class="grid_boxes">
                    <div class="grid_pagingTwo_top">
                        <div class="paging_top_titleTwo">
                            <asp:Literal ID="litGridStatesList" runat="server" Text="<%$ Resources:Resource, TARIFF_ENTRY_LIST%>"></asp:Literal>
                        </div>
                        <div class="paging_top_rightTwo_content" id="divPaging1" runat="server">
                            <div class="text-inner" style="position: absolute; margin-left: -144px; margin-top: -6px;
                                width: 140px;">
                                <asp:DropDownList CssClass="text_selectbc" ID="ddlEnergyYears" runat="server" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlEnergyYears_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="grid_tb" id="divgrid" runat="server">
                    <asp:GridView ID="gvTariffEntries" runat="server" AutoGenerateColumns="False" AlternatingRowStyle-CssClass="color"
                        OnRowCommand="gvTariffEntries_RowCommand" OnRowDataBound="gvTariffEntries_RowDataBound"
                        HeaderStyle-CssClass="grid_tb_hd">
                        <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                        <EmptyDataTemplate>
                            There is no data.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="grid_tb_td">
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <%--<%#Container.DataItemIndex+1%>--%>
                                    <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                    <asp:Label ID="lblEnergyChargeClassID" Visible="false" runat="server" Text='<%#Eval("EnergyClassChargeID")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, TARIFF%>" ItemStyle-Width="8%"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblChargeName" runat="server" Text='<%#Eval("ClassName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, FROM_DATE_TO_DATE%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblFromDate" runat="server" Text='<%#Eval("FromDate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, FROM_KWH%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblFromKw" runat="server" Text='<%#Eval("FromKW") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, TO_KWH%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblToKw" runat="server" Text='<%#Eval("ToKWs") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, AMOUNT%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("Amount") %>'></asp:Label>
                                    <%--onkeyup="GetCurrencyFormate(this);"--%>
                                    <asp:TextBox CssClass="text-box" ID="txtGridAmount" Style="text-align: right" MaxLength="16"
                                        onkeypress="return isNumberKey(event,this)" placeholder="Enter Charge" runat="server"
                                        Visible="false" onblur="return ValidatePaidAmount(this)" onkeyup="GetCurrencyFormate(this);" ></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="ftbTxtGridContactNo2" runat="server" TargetControlID="txtGridAmount"
                                        FilterType="Numbers,Custom" ValidChars=".," FilterMode="ValidChars">
                                    </asp:FilteredTextBoxExtender>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, STATUS_TYPE%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblClassStatus" runat="server" Text='<%#Eval("ClassStatus") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblIsActive" runat="server" Visible="False" Text='<%#Eval("IsActive")%>'></asp:Label>
                                    <asp:Label ID="lblAction" Visible="False" runat="server" Text="Parent Deactivated"></asp:Label>
                                    <asp:LinkButton ID="lkbtnEdit" runat="server" ToolTip="Edit" Text="Edit" CommandName="EditTariffEntry"> <img src="../images/Edit.png" alt="Edit" /></asp:LinkButton>
                                    <asp:LinkButton ID="lkbtnUpdate" Visible="false" runat="server" ToolTip="Update"
                                        CommandName="UpdateTariffEntry"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                    <asp:LinkButton ID="lkbtnCancel" Visible="false" runat="server" ToolTip="Cancel"
                                        CommandName="CancelTariffEntry"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                    <asp:LinkButton ID="lkbtnActive" Visible="false" runat="server" ToolTip="Active"
                                        CommandName="ActiveCharge"><img src="../images/Activate.gif" alt="Cancel" /></asp:LinkButton>
                                    <asp:LinkButton ID="lkbtnDeActive" runat="server" ToolTip="Delete" CommandName="DeActiveTariffEntry"><img src="../images/delete.png" alt="DeActive" /></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="grid_boxes">
                    <div id="divPaging2" runat="server">
                        <div class="grid_paging_bottom">
                            <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="hfPageSize" runat="server" />
            <asp:HiddenField ID="hfPageNo" runat="server" />
            <asp:HiddenField ID="hfLastPage" runat="server" />
            <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
            <asp:HiddenField ID="hfCharges" runat="server" />
            <asp:HiddenField ID="hfId" runat="server" />
            <asp:HiddenField ID="hfTariffClass" runat="server" />
            <div id="hidepopup">
                <%-- DeActive Btn popup
            Start--%>
                <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                    TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnDeActivate" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="PanelDeActivate" runat="server" CssClass="modalPopup"
                    Style="display: none; border-color: #639B00;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr" style="margin-top: 22px; padding-right: 10px;">
                            <br />
                            <asp:Button ID="btnDeActiveOk" runat="server" OnClick="btnDeActiveOk_Click" Text="<%$ Resources:Resource, YES%>"
                                CssClass="ok_btn" />
                            <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, NO%>"
                                CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- DeActive popup Closed--%>
                <%-- Grid Alert popup Start--%>
                <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                    TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                </asp:ModalPopupExtender>
                <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                    <div class="popheader popheaderlblred">
                        <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- Grid Alert popup
            Closed--%>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript"> $(document).keydown(function (e) { // ESCAPE key
    pressed if (e.keyCode == 27) { $(".hidepopup").fadeOut("slow"); $(".modalPopup").fadeOut("slow");
    $(".modalBackground").fadeOut("slow"); $(".rnkland").fadeOut("slow"); $(".popbg").fadeOut("slow");
    document.getElementById("overlay").style.display = "none"; } }); </script>
    <%--==============Escape button script starts==============--%>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/TariffEntry.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
        initialSetup();
    </script>
    <%--==============Validation script ref ends==============--%>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { assignDiv('rptrMainMasterMenu_104_1', '98') });
    </script>
</asp:Content>
