﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for AddServiceUnits
                     
 Developer        : id027-Karthik T
 Creation Date    : 15-Mar-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No
 
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using iDSignHelper;
using Resources;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using System.Drawing;
using System.Xml;

namespace UMS_Nigeria.TariffManagement
{
    public partial class TariffEntry : System.Web.UI.Page
    {
        #region Members

        iDsignBAL objIdsignBal = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        TariffManagementBal objTariffBal = new TariffManagementBal();
        String Key = "TariffEntry";
        public int PageNum;

        #endregion

        #region Properties
        private int TariffYear
        {
            get { return string.IsNullOrEmpty(ddlEnergyYears.SelectedValue) ? 0 : Convert.ToInt32(ddlEnergyYears.SelectedValue); }
        }

        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.CssClass = lblMessage.Text = string.Empty;
            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = objCommonMethods.GetPagePath(this.Request.Url);
                //if (objCommonMethods.IsAccess(path))
                //{
                BindYears();
                BindMonths();
                objCommonMethods.BindTariff(ddlTariff, true);
                BindAddedEnergyYears();
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                //}
                //else
                //{
                //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                //}
            }
        }

        protected void ddlTariff_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlTariff.SelectedIndex > 0)
                {
                    objCommonMethods.BindTariffSubTypes(ddlTariffSubType, Convert.ToInt32(ddlTariff.SelectedValue), true);
                    if (ddlTariffSubType.Items.Count > 1)
                        phTariffSubTypes.Visible = true;
                }
                else
                {
                    ddlTariffSubType.SelectedIndex = Constants.Zero;
                    phTariffSubTypes.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlTariffSubType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlPHTariffSubType = (DropDownList)sender;
            try
            {
                if (ddlPHTariffSubType.SelectedIndex > 0)
                {
                    DropDownList ddlNewTariffSubType = new DropDownList();
                    ddlNewTariffSubType.AutoPostBack = true;
                    ddlNewTariffSubType.SelectedIndexChanged += ddlTariffSubType_SelectedIndexChanged;
                    int TariffSubType = Convert.ToInt32(ddlPHTariffSubType.SelectedValue);
                    objCommonMethods.BindTariffSubTypes(ddlNewTariffSubType, TariffSubType, true);
                    TariffManagementBE objTariffBe = new TariffManagementBE();
                    objTariffBe.ClassID = TariffSubType;
                    objTariffBe = objIdsignBal.DeserializeFromXml<TariffManagementBE>(objTariffBal.Get(objTariffBe, ReturnType.Single));
                    ddlFromMonth.SelectedIndex = ddlFromMonth.Items.IndexOf(ddlFromMonth.Items.FindByValue(objTariffBe.TariffMonth));
                    ddlFromYear.SelectedIndex = ddlFromYear.Items.IndexOf(ddlFromYear.Items.FindByValue(objTariffBe.TariffYear));
                    if (string.IsNullOrEmpty(objTariffBe.TariffMonth) && string.IsNullOrEmpty(objTariffBe.TariffYear))
                        ddlFromMonth.Enabled = ddlFromYear.Enabled = true;
                    else
                        ddlFromMonth.Enabled = ddlFromYear.Enabled = false;

                    if (ddlNewTariffSubType.Items.Count > 1)
                    {
                        phTariffSubTypes.Controls.Add(ddlNewTariffSubType);
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string FromKWs = string.Empty;
                string ToKWs = string.Empty;
                string Charges = string.Empty;
                string[] Classes = hfCharges.Value.TrimEnd('|').Split('|');

                for (int i = 0; i < Classes.Length; i++)
                {
                    string[] SingleItem = Classes[i].Split(',');
                    FromKWs = string.IsNullOrEmpty(FromKWs) ? SingleItem[0] : FromKWs + ',' + SingleItem[0];

                    if (i == Classes.Length - 1)
                        ToKWs = string.IsNullOrEmpty(ToKWs) ? string.Empty : ToKWs + ',' + ',';
                    else
                        ToKWs = string.IsNullOrEmpty(ToKWs) ? SingleItem[1] : ToKWs + "," + SingleItem[1];

                    Charges = string.IsNullOrEmpty(Charges) ? SingleItem[2] : Charges + "," + SingleItem[2];
                }

                TariffManagementBE objTariffBe = new TariffManagementBE();
                objTariffBe.FromKWs = FromKWs;
                objTariffBe.ToKWs = ToKWs;
                objTariffBe.Charges = Charges;
                objTariffBe.FromDate = ddlFromMonth.SelectedValue + "/01/" + ddlFromYear.SelectedValue;
                //objTariffBe.ToDate = ddlToMonth.SelectedValue + "/01/" + ddlToYear.SelectedValue;
                objTariffBe.ToDate = ddlToMonth.SelectedValue + "/" + (DateTime.DaysInMonth(Convert.ToInt32(ddlToYear.SelectedValue), Convert.ToInt32(ddlToMonth.SelectedValue))).ToString() + "/" + ddlToYear.SelectedValue;
                objTariffBe.ClassID = Convert.ToInt32(hfTariffClass.Value);
                objTariffBe.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objTariffBe = objIdsignBal.DeserializeFromXml<TariffManagementBE>(objTariffBal.Insert(objTariffBe, Statement.Check));
                if (objTariffBe.IsSuccess)
                {
                    //ScriptManager.RegisterStartupScript(upnlTariffEntry, upnlTariffEntry.GetType(), "reset", "ClearControls()", true);
                    BindAddedEnergyYears();
                    BindGrid();
                    ddlTariff.SelectedIndex = ddlFromYear.SelectedIndex = ddlFromMonth.SelectedIndex = ddlToYear.SelectedIndex = ddlToMonth.SelectedIndex = 0;
                    txtToKw.Text = txtChargeAmount.Text = string.Empty;
                    ddlTariff_SelectedIndexChanged(ddlTariff, null);
                    UCPaging1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.TARIFF_ENTRY_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                    Message(UMS_Resource.TARIFF_ENTRY_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //protected void lkbtnFirst_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        BindGrid();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnPrevious_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum -= Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        BindGrid();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnNext_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
        //        PageNum += Constants.pageNoValue;
        //        hfPageNo.Value = PageNum.ToString();
        //        BindGrid();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void lkbtnLast_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = hfLastPage.Value;
        //        BindGrid();
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        hfPageNo.Value = Constants.pageNoValue.ToString();
        //        BindGrid();
        //        hfPageSize.Value = ddlPageSize.SelectedItem.Text;
        //        CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        protected void ddlEnergyYears_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.Active.ToString();
                BindGrid();
                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvTariffEntries_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                TariffManagementListBE objTariffManagementListBE = new TariffManagementListBE();
                TariffManagementBE objTariffManagementBE = new TariffManagementBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridAmount = (TextBox)row.FindControl("txtGridAmount");

                Label lblEnergyChargeClassID = (Label)row.FindControl("lblEnergyChargeClassID");
                Label lblAmount = (Label)row.FindControl("lblAmount");

                switch (e.CommandName.ToUpper())
                {
                    case "EDITTARIFFENTRY":
                        lblAmount.Visible = false;
                        txtGridAmount.Visible = true;
                        //txtGridAmount.Text = lblAmount.Text
                        txtGridAmount.Text = lblAmount.Text.Replace(",", string.Empty);
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;
                        break;
                    case "CANCELTARIFFENTRY":
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                        lblAmount.Visible = true;
                        txtGridAmount.Visible = false;
                        break;
                    case "UPDATETARIFFENTRY":
                        objTariffManagementBE.ClassID = Convert.ToInt32(lblEnergyChargeClassID.Text);
                        objTariffManagementBE.Amount = Convert.ToDecimal(txtGridAmount.Text);
                        objTariffManagementBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objTariffManagementBE = objIdsignBal.DeserializeFromXml<TariffManagementBE>(objTariffBal.Insert(objTariffManagementBE, Statement.Modify));

                        if (objTariffManagementBE.IsSuccess && objTariffManagementBE.IsAmtIncr)
                        {
                            BindGrid();
                            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            Message(UMS_Resource.TARIFF_ENTRY_UPDATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        else if (!objTariffManagementBE.IsAmtIncr)
                        {
                            mpegridalert.Show();
                            lblgridalertmsg.Text = UMS_Resource.AMOUNT_IN_INCREASING_ORDER;
                            btngridalertok.Focus();
                        }
                        else
                            Message(UMS_Resource.TARIFF_ENTRY_UPDATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "DEACTIVETARIFFENTRY":
                        mpeDeActive.Show();
                        hfId.Value = lblEnergyChargeClassID.Text;
                        lblDeActiveMsg.Text = UMS_Resource.DELETE_TARIFF_ENTRY_POPUP_TEXT;
                        btnDeActiveOk.Focus();
                        //objTariffManagementBE.ActiveStatusId = Convert.ToBoolean(0);
                        //objTariffManagementBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objTariffManagementBE.ClassID = Convert.ToInt32(lblEnergyChargeClassID.Text);
                        //objTariffManagementBE = objIdsignBal.DeserializeFromXml<TariffManagementBE>(objTariffBal.Insert(objTariffManagementBE, Statement.Edit));

                        //if (Convert.ToBoolean(objTariffManagementBE.IsSuccess))
                        //{
                        //    BindTariffEnergyList();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message(UMS_Resource.TARIFF_ENTRY_DELETE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        //}
                        //else
                        //    Message(UMS_Resource.TARIFF_ENTRY_DELETE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                        break;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        //protected void btnExportExcel_Click(object sender, EventArgs e)
        //{
        //    DataSet dataSet = new DataSet();
        //    TariffManagementBE objTariffBe = new TariffManagementBE();
        //    objTariffBe.Year = TariffYear;
        //    objTariffBe.PageNo = Convert.ToInt32(hfPageNo.Value);
        //    objTariffBe.PageSize = PageSize;
        //    XmlNodeReader xmlReader = new XmlNodeReader(objTariffBal.Get(objTariffBe, ReturnType.Group));
        //    dataSet.ReadXml(xmlReader);
        //    if (dataSet.Tables[0].Rows.Count > 0)
        //    {
        //        DataTable dt = dataSet.Tables[0];
        //        objCommonMethods.ExportToExcel(dt, "Tariff_Energy_Charges");
        //    }
        //}

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                TariffManagementBE objTariffManagementBE = new TariffManagementBE();
                objTariffManagementBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objTariffManagementBE.ClassID = Convert.ToInt32(hfId.Value);
                objTariffManagementBE = objIdsignBal.DeserializeFromXml<TariffManagementBE>(objTariffBal.Insert(objTariffManagementBE, Statement.Edit));

                if (Convert.ToBoolean(objTariffManagementBE.IsSuccess))
                {
                    int PageNo = Convert.ToInt32(hfPageNo.Value);
                    if (gvTariffEntries.Rows.Count == 1)
                    {
                        hfPageNo.Value = (PageNo == 1 ? PageNo : (PageNo - 1)).ToString();
                        UCPaging1.BindPagingDropDown(Convert.ToInt32(hfPageNo.Value));
                        UCPaging1.CreatePagingDT(Convert.ToInt32(hfPageNo.Value));
                    }
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.TARIFF_ENTRY_DELETE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                    Message(UMS_Resource.TARIFF_ENTRY_DELETE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlFromMonth_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gvTariffEntries_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblAmount = (Label)e.Row.FindControl("lblAmount");
                    lblAmount.Text = objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblAmount.Text), 2, Constants.MILLION_Format);

                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    TextBox txtGridAmount = (TextBox)e.Row.FindControl("txtGridAmount");
                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridAmount.ClientID + "')");

                    Label lblIsActive = (Label)e.Row.FindControl("lblIsActive");
                    LinkButton lkbtnDeActive = (LinkButton)e.Row.FindControl("lkbtnDeActive");
                    LinkButton lkbtnActive = (LinkButton)e.Row.FindControl("lkbtnActive");
                    LinkButton lkbtnEdit = (LinkButton)e.Row.FindControl("lkbtnEdit");
                    Label lblClassStatus = (Label)e.Row.FindControl("lblClassStatus");
                    Label lblAction = (Label)e.Row.FindControl("lblAction");
                    if (lblIsActive.Text == "1")
                    {
                        lkbtnActive.Visible = false;
                        lkbtnEdit.Visible = lkbtnDeActive.Visible = true;
                        if (lblClassStatus.Text == "Inactive")
                        {
                            lkbtnDeActive.Visible = lkbtnEdit.Visible = false;
                            lblAction.Visible = true;
                        }
                        else
                        {
                            lblAction.Visible = false;
                        }
                    }
                    else
                    {
                        lkbtnActive.Visible = true;
                        lkbtnEdit.Visible = lkbtnDeActive.Visible = lblAction.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        private void BindMonths()
        {
            try
            {
                DataSet dsMonths = new DataSet();
                dsMonths.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["Months"]));
                objIdsignBal.FillDropDownList(dsMonths.Tables[0], ddlFromMonth, "name", "value", UMS_Resource.DROPDOWN_SELECT, false);
                objIdsignBal.FillDropDownList(dsMonths.Tables[0], ddlToMonth, "name", "value", UMS_Resource.DROPDOWN_SELECT, false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        public void BindGrid()
        {
            try
            {
                TariffManagementBE objTariffBe = new TariffManagementBE();
                objTariffBe.Year = TariffYear;
                objTariffBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                objTariffBe.PageSize = PageSize;
                TariffManagementListBE objTariffListBe = objIdsignBal.DeserializeFromXml<TariffManagementListBE>(objTariffBal.Get(objTariffBe, ReturnType.Group));
                if (objTariffListBe.items.Count > 0)
                {
                    gvTariffEntries.DataSource = objTariffListBe.items;
                    gvTariffEntries.DataBind();
                    MergeRows(gvTariffEntries);
                    hfTotalRecords.Value = objTariffListBe.items[0].TotalRecords.ToString();
                    divPaging1.Visible = divPaging2.Visible = true;
                    //CreatePagingDT(objTariffListBe.items[0].TotalRecords);
                }
                else
                {
                    gvTariffEntries.DataSource = new DataTable();
                    gvTariffEntries.DataBind();
                    divPaging1.Visible = divPaging2.Visible = false;
                    hfTotalRecords.Value = Constants.Zero.ToString();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        public static void MergeRows(GridView gridView)
        {
            for (int rowIndex = gridView.Rows.Count - 2; rowIndex >= 0; rowIndex--)
            {
                GridViewRow row = gridView.Rows[rowIndex];
                GridViewRow previousRow = gridView.Rows[rowIndex + 1];

                Label lblChargeName = (Label)row.FindControl("lblChargeName");
                Label lblPreviousChargeName = (Label)previousRow.FindControl("lblChargeName");

                Label lblFromDate = (Label)row.FindControl("lblFromDate");
                Label lblPreviousFromDate = (Label)previousRow.FindControl("lblFromDate");
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    if (i == 1)
                    {
                        if (lblChargeName.Text == lblPreviousChargeName.Text)
                        {
                            row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                            previousRow.Cells[i].Visible = false;
                        }
                    }
                    if (i == 2)
                    {
                        if (lblFromDate.Text == lblPreviousFromDate.Text)
                        {
                            row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                            previousRow.Cells[i].Visible = false;
                        }
                    }
                }
            }
        }

        //private void BindPagingDropDown(int TotalRecords)
        //{
        //    try
        //    {
        //        objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
        //        //ddlPageSize.Items.Clear();
        //        //if (TotalRecords < Constants.PageSizeStarts)
        //        //{
        //        //    ListItem item = new ListItem(Constants.PageSizeStarts.ToString(), Constants.PageSizeStarts.ToString());
        //        //    ddlPageSize.Items.Add(item);
        //        //}
        //        //else
        //        //{
        //        //    int previousSize = Constants.PageSizeStarts;
        //        //    for (int i = Constants.PageSizeStarts; i <= TotalRecords; i = i + Constants.PageSizeIncrement)
        //        //    {
        //        //        ListItem item = new ListItem(i.ToString(), i.ToString());
        //        //        ddlPageSize.Items.Add(item);
        //        //        previousSize = i;
        //        //    }
        //        //    if (previousSize < TotalRecords)
        //        //    {
        //        //        ListItem item = new ListItem((previousSize + Constants.PageSizeIncrement).ToString(), (previousSize + Constants.PageSizeIncrement).ToString());
        //        //        ddlPageSize.Items.Add(item);
        //        //    }
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        //private void CreatePagingDT(int TotalNoOfRecs)
        //{
        //    try
        //    {
        //        double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
        //        DataTable dtPages = new DataTable();
        //        dtPages.Columns.Add("PageNo", typeof(int));
        //        int currentpage = Convert.ToInt32(hfPageNo.Value);
        //        lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
        //        objIdsignBal.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
        //        lblCurrentPage.Text = hfPageNo.Value;
        //        if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
        //        else { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Blue; }
        //        if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
        //    }
        //    catch (Exception ex)
        //    {
        //        Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
        //        try
        //        {
        //            objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
        //        }
        //        catch (Exception logex)
        //        {

        //        }
        //    }
        //}

        private void BindYears()
        {
            try
            {
                int CurrentYear = DateTime.Now.Year;
                int FromYear = Convert.ToInt32(GlobalConstants.TARIFF_ENTRY_NO_OF_LAST_YEARS);
                for (int i = FromYear; i < CurrentYear; i++)
                {
                    ListItem item = new ListItem();
                    item.Text = item.Value = i.ToString();
                    ddlFromYear.Items.Add(item);
                    ddlToYear.Items.Add(item);
                }
                for (int i = 0; i <= Convert.ToInt32(GlobalConstants.TARIFF_ENTRY_NO_OF_PRESENT_AND_FUTURE_YEARS); i++)
                {
                    ListItem item = new ListItem();
                    item.Text = item.Value = (CurrentYear + i).ToString();
                    ddlFromYear.Items.Add(item);
                    ddlToYear.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindAddedEnergyYears()
        {
            try
            {
                TariffManagementListBE objTariffListBe = objIdsignBal.DeserializeFromXml<TariffManagementListBE>(objTariffBal.Get(ReturnType.Get));
                objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<TariffManagementBE>(objTariffListBe.items), ddlEnergyYears, "Year", "Year", UMS_Resource.SELECT, false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

    }
}