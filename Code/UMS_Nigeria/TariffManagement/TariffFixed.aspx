﻿<%@ Page Title="::Tariff Fixed::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="TariffFixed.aspx.cs" Inherits="UMS_Nigeria.TariffManagement.TariffFixed" %>

<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function pageLoad() {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        };

        function Validate() {
            var mpeAlert = $find('<%= mpegridalert.ClientID %>');
            var lblAlertMsg = document.getElementById('<%= lblgridalertmsg.ClientID %>');
            var divTariffClasses = document.getElementById('divTariffClasses');
            var Tariffs = divTariffClasses.getElementsByTagName('select');
            var ddlFromMonth = document.getElementById('<%= ddlFromMonth.ClientID %>');
            var ddlFromYear = document.getElementById('<%= ddlFromYear.ClientID %>');
            var ddlToMonth = document.getElementById('<%= ddlToMonth.ClientID %>');
            var ddlToYear = document.getElementById('<%= ddlToYear.ClientID %>');
            var hfTariffClass = document.getElementById('<%= hfTariffClass.ClientID %>');
            //var ddlTypeOfCharge = document.getElementById('ddlTypeOfCharge.ClientID');
            // var txtChargeAmount = document.getElementById('txtChargeAmount.ClientID');


            var IsValid = true;
            //            if (Tariffs.length > 0) {
            for (var i = 0; i < Tariffs.length; i++) {
                if (DropDownlistValidationlbl(Tariffs[i], document.getElementById("spanTariff"), "Tariff Class") == false) IsValid = false;
            }
            //            }
            //            divTariffClasses.visibility = false;
            if (DropDownlistValidationlbl(ddlFromMonth, document.getElementById("spanFromMonth"), "From Month") == false) IsValid = false;
            if (DropDownlistValidationlbl(ddlFromYear, document.getElementById("spanFromYear"), "From Year") == false) IsValid = false;
            if (DropDownlistValidationlbl(ddlToMonth, document.getElementById("spanToMonth"), "To Month") == false) IsValid = false;
            if (DropDownlistValidationlbl(ddlToYear, document.getElementById("spanToYear"), "To Year") == false) IsValid = false;

            var FromDate = '01/' + ddlFromMonth.options[ddlFromMonth.selectedIndex].value + '/' + ddlFromYear.options[ddlFromYear.selectedIndex].value;
            var ToDateTotalDays = daysInMonth(parseInt(ddlToMonth.options[ddlToMonth.selectedIndex].value), parseInt(ddlToYear.options[ddlToYear.selectedIndex].value));
            var ToDate = ToDateTotalDays.toString() + '/' + ddlToMonth.options[ddlToMonth.selectedIndex].value + '/' + ddlToYear.options[ddlToYear.selectedIndex].value;

            if (FromToDateInPopup(mpeAlert, lblAlertMsg, FromDate, ToDate, 'To date should be greater than the From date') == false) return false;

            var FromDateFormat = new Date(parseInt(ddlFromYear.options[ddlFromYear.selectedIndex].value), parseInt(ddlFromMonth.options[ddlFromMonth.selectedIndex].value) - 1, 1);
            var ToDateFormat = new Date(parseInt(ddlToYear.options[ddlToYear.selectedIndex].value), parseInt(ddlToMonth.options[ddlToMonth.selectedIndex].value) - 1, ToDateTotalDays)

            var TotalMonths = monthDiff(FromDateFormat, ToDateFormat);

            var MinNoMonthsLength = "<%= Resources.GlobalConstants.TARIFF_ENTRY_MIN_MONTHS_LENGTH %>";
            MinNoMonthsLength = parseInt(MinNoMonthsLength);

            if (TotalMonths < MinNoMonthsLength) {
                //alert("Tariff Fixed period must be atleast of " + MinNoMonthsLength + " months.");
                lblAlertMsg.innerHTML = "Tariff Fixed period must be atleast of " + MinNoMonthsLength + " months.";
                mpeAlert.show();
                return false;
            }

            //if (DropDownlistValidationlbl(ddlTypeOfCharge, document.getElementById("spanTypeOfCharge"), "Type Of Charge") == false) IsValid = false;
            // if (TextBoxValidationlblZeroAccepts(txtChargeAmount, document.getElementById("spanAmount"), "Charge Amount") == false) IsValid = false;

            //if (FromToDate('01/' + ddlFromMonth.options[ddlFromMonth.selectedIndex].value + '/' + ddlFromYear.options[ddlFromYear.selectedIndex].value, '01/' + ddlToMonth.options[ddlToMonth.selectedIndex].value + '/' + ddlToYear.options[ddlToYear.selectedIndex].value) == false) return false;
            if (!IsValid)
                return false;

            var Tariff = Tariffs[Tariffs.length - 1];
            hfTariffClass.value = Tariff.options[Tariff.selectedIndex].value;
        }

        function UpdateValidation(Amount) {
            var mpeAlert = $find('<%= mpegridalert.ClientID %>');
            var lblAlertMsg = document.getElementById('<%= lblgridalertmsg.ClientID %>');
            var Amount = document.getElementById(Amount);
            if (TextBoxValidationInPopup(mpeAlert, lblAlertMsg, Amount, "Amount") == false) { return false; }
        }

        function isNumberKey(evt, txt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode == 46 && txt.value.split('.').length > 1) {
                return false;
            }
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
        }

        function monthDiff(d1, d2) {
            var months;
            months = (d2.getFullYear() - d1.getFullYear()) * 12;
            months -= d1.getMonth() + 1;
            months += d2.getMonth() + 1;
            // edit: increment months if d2 comes later in its month than d1 in its month
            if (d2.getDate() >= d1.getDate())
                months++
            // end edit
            return months <= 0 ? 0 : months;
        }

        function daysInMonth(month, year) {
            return new Date(year, month, 0).getDate();
        }

        function GetCurrencyFormate(obj) {
            obj.value = NewText(obj);
        }        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <div class="out-bor">
        <asp:UpdatePanel ID="upnlTariffFixed" runat="server">
            <ContentTemplate>
                <div class="inner-sec">
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litTariffEntry" runat="server" Text="<%$ Resources:Resource, TARIFF_FIXED%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div id="divTariffClasses">
                        <div class="inner-box">
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="litTariffClass" runat="server" Text="<%$ Resources:Resource, TARIFF_CLASS%>"></asp:Literal>
                                        <span class="span_star">*</span></label>
                                </div>
                                <asp:DropDownList ID="ddlTariff" CssClass="text-box text-select" AutoPostBack="true"
                                    runat="server" OnSelectedIndexChanged="ddlTariff_SelectedIndexChanged">
                                </asp:DropDownList>
                                <span id="spanddlBusinessUnitName" class="span_color"></span>
                            </div>
                            <div class="clr">
                            </div>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        &nbsp</label><br />
                                    <asp:PlaceHolder ID="phTariffSubTypes" runat="server" Visible="false">
                                        <asp:DropDownList ID="ddlTariffSubType" CssClass="text-box text-select" Style="margin-left: -21px !important;"
                                            runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTariffSubType_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <br />
                                        <br />
                                    </asp:PlaceHolder>
                                    <span id="spanTariff" class="span_color_b"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear pad_10">
                    </div>
                    <div class="consumer_feildTwo">
                        <div class="tariffentry_baa">
                            <div class="tariffentry_b1 text-inner">
                                <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, FROM%>"></asp:Literal><span
                                    class="span_star">*</span>
                            </div>
                            <div class="tariffentry_b2">
                                <asp:DropDownList ID="ddlFromMonth" CssClass="text_selectbb" runat="server" Enabled="false"
                                    onchange="DropDownlistOnChangelbl(this,'spanFromMonth','From Month')">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                <span id="spanFromMonth" class="span_colorB"></span>
                            </div>
                            <div class="tariffentry_b3">
                                <asp:DropDownList ID="ddlFromYear" CssClass="text_selectbb" runat="server" Enabled="false"
                                    onchange="DropDownlistOnChangelbl(this,'spanFromYear','From Year')">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                <span id="spanFromYear" class="span_colorB"></span>
                            </div>
                            <div class="tariffentry_b1">
                                <div class="text-Input" style="margin-left: 108px;">
                                    <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, TO%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </div>
                            </div>
                            <div class="tariffentry_b2" style="margin-left: 35px;">
                                <asp:DropDownList ID="ddlToMonth" CssClass="text_selectbb" runat="server" onchange="DropDownlistOnChangelbl(this,'spanToMonth','To Month')">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                <span id="spanToMonth" class="span_colorB"></span>
                            </div>
                            <div class="tariffentry_b3">
                                <asp:DropDownList ID="ddlToYear" CssClass="text_selectbb" runat="server" onchange="DropDownlistOnChangelbl(this,'spanToYear','To Year')">
                                    <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                <span id="spanToYear" class="span_colorB"></span>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div style="margin-left: -22px;" class="consumer_feild">
                            <div class="tariffpanel">
                                <asp:DataList runat="server" ID="dtlChargeTypes" RepeatDirection="Vertical" RepeatColumns="1">
                                    <ItemTemplate>
                                        <div class="consume_name_aB" style="width: auto">
                                            <asp:LinkButton ID="lnkCharge" CommandArgument='<%# Eval("ChargeId") %>' CssClass="consume_name_change"
                                                runat="server" Text='<%# Eval("ChargeName") %>' Enabled="false"></asp:LinkButton>
                                            <span class="span_star_a" style="margin-left: -1px;">*</span>
                                        </div>
                                        <div class="consume_input fltl" style="width: 253px; margin-left: 112px;">
                                            <asp:TextBox CssClass="text-box" ID="txtCharge" MaxLength="16" runat="server" onkeyup="GetCurrencyFormate(this);"
                                                onblur="return TextBoxValidationlblZeroAccepts(this,'spanAmount','Amount')" onkeypress="return isNumberKey(event,this)"
                                                placeholder="Charge Amount"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="ftxtChargeAmount" runat="server" TargetControlID="txtCharge"
                                                FilterType="Numbers,Custom" FilterMode="ValidChars" ValidChars=".,">
                                            </asp:FilteredTextBoxExtender>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                                <div class="tariff_entry_box_dd" style="margin-left: 203px;">
                                    <asp:HyperLink ID="lbtnAddNewFixedChange" NavigateUrl="~/TariffManagement/AddNewFixedCharge.aspx?action=back"
                                        Text="<%$ Resources:Resource, FIXED_CHARGE%>" runat="server" CssClass="heading_bottam"></asp:HyperLink>
                                </div>
                            </div>
                        </div>
                        <div class="clear pad_10">
                            <div>
                                <asp:Button ID="Button2" MaxLength="8" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                                    CssClass="box_s" runat="server" OnClick="btnSave_Click" />
                                <%--<asp:Button ID="btnReset" OnClientClick="return ClearControls();" Text="<%$ Resources:Resource, RESET %>"
                            runat="server" CssClass="calculate_but" />--%>
                            </div>
                        </div>
                        <div class="clear pad_10">
                        </div>
                    </div>
                </div>
                <div class="clear pad_10">
                </div>
                <div class="consumer_total">
                    <div class="pad_10 clear">
                    </div>
                </div>
                <div class="grid_boxes">
                    <div class="grid_pagingTwo_top" style="margin-bottom: -9px; margin-top: -26px;">
                        <div class="paging_top_right_content" id="divPaging1" runat="server">
                            <div class="pad_5 fltr" style="padding-right: 10px;">
                                <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, SELECT_YEAR%>"></asp:Literal>
                                &nbsp&nbsp&nbsp
                                <asp:DropDownList ID="ddlFixedChargeYear" Style="margin-right: 5px;" runat="server"
                                    OnSelectedIndexChanged="ddlFixedChargeYear_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, SELECT_MONTH%>"></asp:Literal>
                                &nbsp&nbsp&nbsp
                                <asp:DropDownList ID="ddlFixedChargeMonth" Style="margin-right: -10px;" runat="server"
                                    OnSelectedIndexChanged="ddlFixedChargeMonth_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="grid_paging_top">
                        <div class="paging_top_title">
                            <asp:Literal ID="litGridStatesList" runat="server" Text="<%$ Resources:Resource, TARIFF_FIXED_LIST%>"></asp:Literal>
                        </div>
                        <div class="paging_top_rightTwo_content" id="divpaging" runat="server">
                            <uc1:UCPagingV1 ID="UCPagingV1" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="grid_tb" id="divgrid" runat="server">
                    <asp:GridView ID="gvTariffFixedChargesList" runat="server" AutoGenerateColumns="False"
                        AlternatingRowStyle-CssClass="color" OnRowCommand="gvTariffFixedChargesList_RowCommand"
                        OnRowDataBound="gvTariffFixedChargesList_RowDataBound" HeaderStyle-CssClass="grid_tb_hd">
                        <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                        <EmptyDataTemplate>
                            There is no data.
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <%--<%#Container.DataItemIndex+1%>--%>
                                    <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                    <asp:Label ID="lblActiveStatusId" Visible="false" runat="server" Text='<%#Eval("IsActive") %>'></asp:Label>
                                    <asp:Label ID="lblAdditionalChargeId" Visible="false" runat="server" Text='<%#Eval("AdditionalChargeID")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, TARIFF%>" ItemStyle-Width="8%"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblClassName" runat="server" Text='<%#Eval("ClassName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, TYPE_OF_CHARGE%>" ItemStyle-Width="12%"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                <ItemTemplate>
                                    <asp:Label ID="lblChargeName" runat="server" Text='<%#Eval("ChargeName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, FROM_DATE_TO_DATE%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblFromDate" runat="server" Text='<%#Eval("FromDate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="<%$ Resources:Resource, TO_DATE%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblToDate" runat="server" Text='<%#Eval("ToDate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, AMOUNT%>" ItemStyle-Width="6%"
                                ItemStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("Amount") %>'></asp:Label>
                                    <%--onkeyup="GetCurrencyFormate(this);"--%>
                                    <asp:TextBox CssClass="text-box" ID="txtGridAmount" MaxLength="16" Style="text-align: right"
                                        onkeypress="return isNumberKey(event,this)" placeholder="Enter Amount" runat="server"
                                        Visible="false" onkeyup="GetCurrencyFormate(this);"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="ftbTxtGridContactNo2" runat="server" TargetControlID="txtGridAmount"
                                        FilterType="Numbers,Custom" ValidChars=".," FilterMode="ValidChars">
                                    </asp:FilteredTextBoxExtender>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, STATUS_TYPE%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblClassStatus" runat="server" Text='<%#Eval("ClassStatus") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblAction" Visible="False" runat="server" Text="Parent Deactivated"></asp:Label>
                                    <asp:Label ID="lblPastAction" Visible="false" runat="server" Text='--'></asp:Label>
                                    <asp:Label ID="lblIsPastDate" Visible="false" runat="server" Text='<%#Eval("IsPastDate") %>'></asp:Label>
                                    <asp:LinkButton ID="lkbtnEdit" runat="server" ToolTip="Edit" Text="Edit" CommandName="EditCharge">
                                        <img src="../images/Edit.png" alt="Edit"/></asp:LinkButton>
                                    <asp:LinkButton ID="lkbtnUpdate" Visible="false" runat="server" ToolTip="Update"
                                        CommandName="UpdateCharge"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                                    <asp:LinkButton ID="lkbtnCancel" Visible="false" runat="server" ToolTip="Cancel"
                                        CommandName="CancelCharge"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>
                                    <asp:LinkButton ID="lkbtnActive" Visible="false" runat="server" ToolTip="Activate"
                                        CommandName="ActiveCharge"><img src="../images/Activate.gif" alt="Active" /></asp:LinkButton>
                                    <%--<asp:LinkButton ID="lkbtnActive1" Visible="false" runat="server" ToolTip="Active" CommandName="ActiveCharge">Active</asp:LinkButton>--%>
                                    <asp:LinkButton ID="lkbtnDeActive" runat="server" ToolTip="Delete" CommandName="DeleteCharge"><img src="../images/Deactivate.gif" alt="DeActive" /></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="grid_boxes">
                    <div id="divPaging2" runat="server">
                        <div class="grid_paging_bottom">
                            <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div style="clear: both;">
                </div>
                <asp:HiddenField ID="hfPageSize" runat="server" />
                <asp:HiddenField ID="hfPageNo" runat="server" />
                <asp:HiddenField ID="hfLastPage" runat="server" />
                <asp:HiddenField ID="hfTotalRecords" runat="server" Value="0" />
                <asp:HiddenField ID="hfId" runat="server" />
                <asp:HiddenField ID="hfTariffClass" runat="server" />
                <div id="hidepopup">
                    <%-- DeActive  Btn popup Start--%>
                    <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                        TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnDeActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelDeActivate" runat="server" CssClass="modalPopup" Style="display: none;
                        border-color: #639B00;">
                        <div class="popheader" style="background-color: red;">
                            <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer" align="right">
                            <div class="fltr" style="margin-top: 22px; padding-right: 10px;">
                                <br />
                                <asp:Button ID="btnDeActiveOk" runat="server" OnClick="btnDeActiveOk_Click" Text="<%$ Resources:Resource, YES%>"
                                    CssClass="ok_btn" />
                                <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, NO%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- DeActive  popup Closed--%>
                    <%-- Active Country Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                            TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="PanelActivate" runat="server" CssClass="modalPopup" Style="display: none;">
                            <div class="popheader popheaderlblgreen">
                                <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                            </div>
                            <div class="space">
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btnActiveOk" OnClick="btnActiveOk_Click" runat="server" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                    <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Active Country Btn popup Closed--%>
                    <%-- Grid Alert popup Start--%>
                    <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                        TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="Panelgridalert" runat="server" CssClass="modalPopup" Style="display: none;">
                        <div class="popheader popheaderlblred">
                            <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Grid Alert popup Closed--%>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { assignDiv('rptrMainMasterMenu_104_1', '99') });
    </script>
</asp:Content>
