﻿#region File Header
/* 
 Copyright (c) iDsign Technologies. All rights reserved.
 
 Description      : Code Behind for TariffFixed
                     
 Developer        : Id065-Bhimaraju V
 Creation Date    : 16-MAR-2014
 
 Revision History
 
 Modified By | Reviewer | Modification Date (dd-mm-yyyy) | Defect Id | Version No 
 
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using iDSignHelper;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using Resources;
using System.Data;
using System.Configuration;
using UMS_NigriaDAL;
using System.Threading;
using System.Globalization;
using System.Drawing;

namespace UMS_Nigeria.TariffManagement
{
    public partial class TariffFixed : System.Web.UI.Page
    {
        #region Members
        iDsignBAL objIdsignBal = new iDsignBAL();
        CommonMethods objCommonMethods = new CommonMethods();
        TariffManagementBal objTariffManagementBal = new TariffManagementBal();
        CommonDAL objCommonDal = new CommonDAL();
        XmlDocument xml = null;
        public int PageNum;
        string Key = "TariffFixed";
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }

        private int FixedChargeYear
        {
            get { return string.IsNullOrEmpty(ddlFixedChargeYear.SelectedValue) ? 0 : Convert.ToInt32(ddlFixedChargeYear.SelectedValue); }
        }

        private int FixedChargeMonth
        {
            get { return string.IsNullOrEmpty(ddlFixedChargeMonth.SelectedValue) ? 0 : Convert.ToInt32(ddlFixedChargeMonth.SelectedValue); }
        }

        //private int Charge
        //{
        //    get { return string.IsNullOrEmpty(ddlTypeOfCharge.SelectedValue) ? 0 : Convert.ToInt32(ddlTypeOfCharge.SelectedValue); }
        //}
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = objCommonMethods.GetPagePath(this.Request.Url);
                //if (objCommonMethods.IsAccess(path))
                //{
                    BindYears();
                    BindMonths();
                    objCommonMethods.BindTariff(ddlTariff, true);
                    //objCommonMethods.BindTariffChargeTypes(ddlTypeOfCharge, 0, true);
                    // objCommonMethods.BindTariffChargeTypes(dtlChargeList, 0, true);
                    BindTariffCharges();
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    BindFixedChargeYear();
                    BindFixedChargeMonth();
                //}
                //else
                //{
                //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                //}
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                TariffManagementBE objTariffManagementBE = new TariffManagementBE();


                //objTariffManagementBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                ////objTariffManagementBE.ChargeId = Convert.ToInt32(ddlTypeOfCharge.SelectedValue);
                //objTariffManagementBE.ClassID = Convert.ToInt32(hfTariffClass.Value);
                //objTariffManagementBE.FromDate = ddlFromMonth.SelectedValue + "/01/" + ddlFromYear.SelectedValue;
                ////objTariffManagementBE.ToDate = ddlToMonth.SelectedValue + "/01/" + ddlToYear.SelectedValue;
                //objTariffManagementBE.ToDate = ddlToMonth.SelectedValue + "/" + (DateTime.DaysInMonth(Convert.ToInt32(ddlToYear.SelectedValue), Convert.ToInt32(ddlToMonth.SelectedValue))).ToString() + "/" + ddlToYear.SelectedValue;
                ////objTariffManagementBE = objIdsignBal.DeserializeFromXml<TariffManagementBE>(objTariffManagementBal.Insert(objTariffManagementBE, Statement.Insert));
                ////objTariffManagementBE.Amount = Convert.ToDecimal((txtChargeAmount.Text).Replace(",", ""));

                int count = 0;
                foreach (DataListItem li in dtlChargeTypes.Items)
                {
                    LinkButton lnkChrgeType = (LinkButton)li.FindControl("lnkCharge");
                    TextBox txtChargeAmt = (TextBox)li.FindControl("txtCharge");
                    bool IsEmpty = isValidate(txtChargeAmt, lnkChrgeType);
                    if (IsEmpty) { count++; break; };
                }
                if (count == 0)
                {
                    foreach (DataListItem li in dtlChargeTypes.Items)
                    {
                        LinkButton lnkChrgeType = (LinkButton)li.FindControl("lnkCharge");
                        TextBox txtChargeAmt = (TextBox)li.FindControl("txtCharge");
                        if (txtChargeAmt.Text != string.Empty)
                        {
                            objTariffManagementBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                            objTariffManagementBE.ClassID = Convert.ToInt32(hfTariffClass.Value);
                            objTariffManagementBE.FromDate = ddlFromMonth.SelectedValue + "/01/" + ddlFromYear.SelectedValue;
                            objTariffManagementBE.ToDate = ddlToMonth.SelectedValue + "/" + (DateTime.DaysInMonth(Convert.ToInt32(ddlToYear.SelectedValue), Convert.ToInt32(ddlToMonth.SelectedValue))).ToString() + "/" + ddlToYear.SelectedValue;
                            objTariffManagementBE.ChargeId = Convert.ToInt32(lnkChrgeType.CommandArgument);
                            objTariffManagementBE.Amount = Convert.ToDecimal((txtChargeAmt.Text).Replace(",", ""));
                            objTariffManagementBE = objIdsignBal.DeserializeFromXml<TariffManagementBE>(objTariffManagementBal.Insert(objTariffManagementBE, Statement.Insert));
                        }
                    }
                }
                if (objTariffManagementBE.IsSuccess)
                {
                    BindGrid();
                    UCPagingV1.CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    ddlTariff.SelectedIndex =
                        ddlToMonth.SelectedIndex =
                            ddlToYear.SelectedIndex =
                                ddlTariffSubType.SelectedIndex =
                                    ddlFromMonth.SelectedIndex = ddlFromYear.SelectedIndex = 0;
                    for (int i = 0; i < dtlChargeTypes.Items.Count; i++)
                    {
                        TextBox txtCharge = ((TextBox)dtlChargeTypes.Controls[i].FindControl("txtCharge"));
                        if (txtCharge != null)
                            txtCharge.Text = string.Empty;
                    }
                    
                    //ScriptManager.RegisterStartupScript(upnlTariffFixed, upnlTariffFixed.GetType(), "reset", "ClearControls()", true);
                    Message(UMS_Resource.CHARGE_INSERT_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                //else
                //{
                //    Message(UMS_Resource.CHARGE_INSERT_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlTariff_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                EnableYearMonths(true);
                if (ddlTariff.SelectedIndex > 0)
                {
                    objCommonMethods.BindTariffSubTypes(ddlTariffSubType, Convert.ToInt32(ddlTariff.SelectedValue), true);
                    if (ddlTariffSubType.Items.Count > 1)
                        phTariffSubTypes.Visible = true;
                    ddlTariffSubType.Focus();
                }
                else
                {
                    ddlTariffSubType.SelectedIndex = Constants.Zero;
                    phTariffSubTypes.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlTariffSubType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddlPHTariffSubType = (DropDownList)sender;
            try
            {
                if (ddlPHTariffSubType.SelectedIndex > 0)
                {
                    DropDownList ddlNewTariffSubType = new DropDownList();
                    ddlNewTariffSubType.AutoPostBack = true;
                    ddlNewTariffSubType.SelectedIndexChanged += ddlTariffSubType_SelectedIndexChanged;
                    int TariffSubType = Convert.ToInt32(ddlPHTariffSubType.SelectedValue);
                    objCommonMethods.BindTariffSubTypes(ddlNewTariffSubType, TariffSubType, true);
                    TariffManagementBE objTariffBe = new TariffManagementBE();
                    objTariffBe.ClassID = TariffSubType;
                    //objTariffBe.ChargeId = Charge;
                    objTariffBe = objIdsignBal.DeserializeFromXml<TariffManagementBE>(objTariffManagementBal.Get(objTariffBe, ReturnType.Fetch));
                    ddlFromMonth.SelectedIndex = ddlFromMonth.Items.IndexOf(ddlFromMonth.Items.FindByValue(objTariffBe.TariffMonth));
                    ddlFromYear.SelectedIndex = ddlFromYear.Items.IndexOf(ddlFromYear.Items.FindByValue(objTariffBe.TariffYear));
                    if (string.IsNullOrEmpty(objTariffBe.TariffMonth) && string.IsNullOrEmpty(objTariffBe.TariffYear))
                        EnableYearMonths(true);
                    else
                        EnableYearMonths(false);
                    //ddlFromMonth.Enabled = ddlFromYear.Enabled = false;

                    if (ddlNewTariffSubType.Items.Count > 1)
                    {
                        phTariffSubTypes.Controls.Add(ddlNewTariffSubType);
                        ddlFromYear.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void EnableYearMonths(bool p)
        {
            ddlFromMonth.Enabled = ddlFromYear.Enabled = p;
            if (p)
            {
                ddlFromMonth.SelectedIndex = ddlFromYear.SelectedIndex = 0;
            }
        }

        protected void ddlFixedChargeYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlFixedChargeYear.SelectedIndex > 0)
                {
                    //ddlFixedChargeMonth.Enabled = true;
                    ddlFixedChargeMonth.SelectedIndex = 0;
                    //BindFixedChargeMonth();
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                }
                else
                {
                    //ddlFixedChargeMonth.Enabled = false;
                    ddlFixedChargeMonth.SelectedIndex = 0;
                    hfPageNo.Value = Constants.pageNoValue.ToString();
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlFixedChargeMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfPageNo.Value = Constants.pageNoValue.ToString();
            BindGrid();
            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
        }

        protected void lbtnAddNewFixedChange_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Constants.AddNewFixedChargePage, false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvTariffFixedChargesList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                TariffManagementListBE objTariffManagementListBE = new TariffManagementListBE();
                TariffManagementBE objTariffManagementBE = new TariffManagementBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                TextBox txtGridAmount = (TextBox)row.FindControl("txtGridAmount");

                Label lblAdditionalChargeId = (Label)row.FindControl("lblAdditionalChargeId");
                Label lblAmount = (Label)row.FindControl("lblAmount");
                Label lblChargeName = (Label)row.FindControl("lblChargeName");
                
                switch (e.CommandName.ToUpper())
                {
                    case "EDITCHARGE":
                        lblAmount.Visible = false;
                        txtGridAmount.Visible = true;
                        //txtGridAmount.Text = lblAmount.Text;
                        txtGridAmount.Text = lblAmount.Text.Replace(",",string.Empty);
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = true;
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = false;
                        break;
                    case "CANCELCHARGE":
                        row.FindControl("lkbtnDeActive").Visible = row.FindControl("lkbtnEdit").Visible = true;
                        row.FindControl("lkbtnCancel").Visible = row.FindControl("lkbtnUpdate").Visible = false;
                        lblAmount.Visible = true;
                        txtGridAmount.Visible = false;
                        break;
                    case "UPDATECHARGE":
                        objTariffManagementBE.AdditionalChargeID = Convert.ToInt32(lblAdditionalChargeId.Text);
                        objTariffManagementBE.Amount = Convert.ToDecimal((txtGridAmount.Text).Replace(",", ""));
                        objTariffManagementBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        objTariffManagementBE = objIdsignBal.DeserializeFromXml<TariffManagementBE>(objTariffManagementBal.Insert(objTariffManagementBE, Statement.Update));

                        if (Convert.ToBoolean(objTariffManagementBE.IsSuccess))
                        {
                            BindGrid();
                            //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                            //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                            Message(string.Format(UMS_Resource.CHARGE_UPDATED_SUCCESS, lblChargeName.Text), UMS_Resource.MESSAGETYPE_SUCCESS);
                        }
                        else
                        {
                            Message(UMS_Resource.CHARGE_UPDATED_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                        }
                        break;
                    case "DELETECHARGE":
                        mpeDeActive.Show();
                        hfId.Value = lblAdditionalChargeId.Text;
                        //lblDeActiveMsg.Text = UMS_Resource.DELETE_TARIFF_FIXED_POPUP_TEXT;
                        lblDeActiveMsg.Text = Resource.DEACTIVATE_TARIFF_FIXED_POPUP_TEXT;
                        btnDeActiveOk.Focus();
                        //objTariffManagementBE.ActiveStatusId = Convert.ToBoolean(0);
                        //objTariffManagementBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objTariffManagementBE.AdditionalChargeID = Convert.ToInt32(lblAdditionalChargeId.Text);
                        //xml = objTariffManagementBal.Insert(objTariffManagementBE, Statement.Delete);
                        //objTariffManagementBE = objIdsignBal.DeserializeFromXml<TariffManagementBE>(xml);

                        //if (Convert.ToBoolean(objTariffManagementBE.IsSuccess))
                        //{
                        //    BindGridTariffFixedChargesList();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message(UMS_Resource.CHARGE_DELETE_SUCESS,UMS_Resource.MESSAGETYPE_SUCCESS);
                        //}
                        //else
                        //    Message( UMS_Resource.CHARGE_DELETE_FAIL,UMS_Resource.MESSAGETYPE_ERROR);
                        break;
                    case "ACTIVECHARGE":
                        mpeActivate.Show();
                        hfId.Value = lblAdditionalChargeId.Text;
                        lblActiveMsg.Text = "Are you sure you want to Activate this Additional charge?";
                        btnActiveOk.Focus();
                        break;
                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                TariffManagementBE objTariffManagementBE = new TariffManagementBE();
                objTariffManagementBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objTariffManagementBE.AdditionalChargeID = Convert.ToInt32(hfId.Value);
                xml = objTariffManagementBal.Insert(objTariffManagementBE, Statement.Delete);
                objTariffManagementBE = objIdsignBal.DeserializeFromXml<TariffManagementBE>(xml);

                if (Convert.ToBoolean(objTariffManagementBE.IsSuccess))
                {
                    if (gvTariffFixedChargesList.Rows.Count == 1)
                    {
                        if (Convert.ToInt32(hfPageNo.Value) > 1)
                        {
                            hfPageNo.Value = (Convert.ToInt32(hfPageNo.Value) - 1).ToString();
                        }
                    }
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message(UMS_Resource.CHARGE_DELETE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else if(Convert.ToBoolean(objTariffManagementBE.IsCustomersAssociated))
                {
                    //Message("Custoemrs are associated with this additional charge, Shift custmers to another tariff before deactivating.", UMS_Resource.MESSAGETYPE_ERROR);
                    lblgridalertmsg.Text = "Customers are associated with this additional charge, Shift customers to another tariff before deactivating.";
                    mpegridalert.Show();
                    btngridalert.Focus();
                }
                else
                    Message(UMS_Resource.CHARGE_DELETE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {

            try
            {
                TariffManagementBE objTariffManagementBE = new TariffManagementBE();
                objTariffManagementBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objTariffManagementBE.ActiveStatusId = true;
                objTariffManagementBE.AdditionalChargeID = Convert.ToInt32(hfId.Value);
                xml = objTariffManagementBal.Insert(objTariffManagementBE, Statement.Delete);
                objTariffManagementBE = objIdsignBal.DeserializeFromXml<TariffManagementBE>(xml);

                if (objTariffManagementBE.IsSuccess)
                {
                    BindGrid();
                    //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    Message("Additional charge activated successfully", UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                    Message("Additional charge activation failed", UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvTariffFixedChargesList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lblAmount = (Label)e.Row.FindControl("lblAmount");
                    lblAmount.Text = objCommonMethods.GetCurrencyFormat(Convert.ToDecimal(lblAmount.Text), 2, Constants.MILLION_Format);

                    LinkButton lkbtnUpdate = (LinkButton)e.Row.FindControl("lkbtnUpdate");
                    LinkButton lkbtnActive = (LinkButton)e.Row.FindControl("lkbtnActive");
                    TextBox txtGridAmount = (TextBox)e.Row.FindControl("txtGridAmount");
                    lkbtnUpdate.Attributes.Add("onclick", "return UpdateValidation('" + txtGridAmount.ClientID + "')");


                    LinkButton lkbtnDeActive = (LinkButton)e.Row.FindControl("lkbtnDeActive");
                    LinkButton lkbtnEdit = (LinkButton)e.Row.FindControl("lkbtnEdit");
                    Label lblClassStatus = (Label)e.Row.FindControl("lblClassStatus");
                    Label lblAction = (Label)e.Row.FindControl("lblAction");
                    Label lblIsPastDate = (Label)e.Row.FindControl("lblIsPastDate");
                    Label lblPastAction = (Label)e.Row.FindControl("lblPastAction");
                    if (Convert.ToBoolean(lblIsPastDate.Text))
                    {
                        lblPastAction.Visible = true;
                        lkbtnDeActive.Visible = lkbtnEdit.Visible = lkbtnActive.Visible = false;
                    }
                    else
                    {
                        if (lblClassStatus.Text == "Inactive")
                        {
                            lkbtnDeActive.Visible = lkbtnEdit.Visible = false;
                            //lblAction.Visible = true;
                            lkbtnActive.Visible = true;
                        }
                        else if (lblClassStatus.Text == "ParentInactive")
                        {
                            lkbtnDeActive.Visible = lkbtnEdit.Visible =lkbtnActive.Visible =  false;
                            lblAction.Visible = true;
                            lblClassStatus.Text = "Inactive";
                        }
                        else
                        {
                            //lblAction.Visible = false;
                            lkbtnActive.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Methods
        private void BindMonths()
        {
            try
            {
                DataSet dsMonths = new DataSet();
                dsMonths.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["Months"]));
                objIdsignBal.FillDropDownList(dsMonths.Tables[0], ddlFromMonth, "name", "value", UMS_Resource.DROPDOWN_SELECT, false);
                objIdsignBal.FillDropDownList(dsMonths.Tables[0], ddlToMonth, "name", "value", UMS_Resource.DROPDOWN_SELECT, false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void BindYears()
        {
            try
            {
                int CurrentYear = DateTime.Now.Year;
                int FromYear = Convert.ToInt32(GlobalConstants.TARIFF_ENTRY_NO_OF_LAST_YEARS);
                for (int i = FromYear; i < CurrentYear; i++)
                {
                    ListItem item = new ListItem();
                    item.Text = item.Value = i.ToString();
                    ddlFromYear.Items.Add(item);
                    ddlToYear.Items.Add(item);
                }
                for (int i = 0; i <= Convert.ToInt32(GlobalConstants.TARIFF_ENTRY_NO_OF_PRESENT_AND_FUTURE_YEARS); i++)
                {
                    ListItem item = new ListItem();
                    item.Text = item.Value = (CurrentYear + i).ToString();
                    ddlFromYear.Items.Add(item);
                    ddlToYear.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void BindFixedChargeYear()
        {
            try
            {
                TariffManagementListBE objTariffManagementListBE = objIdsignBal.DeserializeFromXml<TariffManagementListBE>(objTariffManagementBal.Get(ReturnType.List));
                if (objTariffManagementListBE.items.Count > 0)
                    objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<TariffManagementBE>(objTariffManagementListBE.items), ddlFixedChargeYear, "Year", "Year", Resource.DDL_ALL, false);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        private void BindFixedChargeMonth()
        {
            DataSet dsMonths = new DataSet();
            dsMonths.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["Months"]));
            //objIdsignBal.FillDropDownList(dsMonths.Tables[0], ddlFixedChargeMonth, "name", "value", Resource.DDL_ALL,false);
            objIdsignBal.FillDropDownList(dsMonths.Tables[0], ddlFixedChargeMonth, "name", "value", Resource.DDL_ALL, false);
        }
        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        private void BindTariffCharges()
        {
            TariffManagementBE objTariffManagementBE = new TariffManagementBE();
            //objTariffManagementBE.ChargeId = ChargeId;
            TariffManagementListBE objTariffManagementListBE = new TariffManagementListBE();
            objTariffManagementListBE = objIdsignBal.DeserializeFromXml<TariffManagementListBE>(objTariffManagementBal.Get(objTariffManagementBE, ReturnType.Get));

            if (objTariffManagementListBE.items.Count > 0)
            {
                dtlChargeTypes.DataSource = objTariffManagementListBE.items;
                dtlChargeTypes.DataBind();
            }
        }
        public void BindGrid()
        {
            try
            {
                TariffManagementListBE objTariffManagementListBE = new TariffManagementListBE();
                TariffManagementBE objTariffManagementBE = new TariffManagementBE();
                XmlDocument resultedXml = new XmlDocument();

                objTariffManagementBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                objTariffManagementBE.PageSize = PageSize;
                objTariffManagementBE.Year = FixedChargeYear;
                objTariffManagementBE.Month = FixedChargeMonth;
                resultedXml = objTariffManagementBal.Get(objTariffManagementBE, ReturnType.Bulk);

                objTariffManagementListBE = objIdsignBal.DeserializeFromXml<TariffManagementListBE>(resultedXml);

                if (objTariffManagementListBE.items.Count > 0)
                {
                    divPaging1.Visible = divPaging2.Visible = true;
                    hfTotalRecords.Value = objTariffManagementListBE.items[0].TotalRecords.ToString();
                    gvTariffFixedChargesList.DataSource = objTariffManagementListBE.items;
                    gvTariffFixedChargesList.DataBind();
                    MergeRows(gvTariffFixedChargesList);
                }
                else
                {
                    hfTotalRecords.Value = objTariffManagementListBE.items.Count.ToString();
                    divPaging1.Visible = divPaging2.Visible = false;
                    gvTariffFixedChargesList.DataSource = new DataTable(); ;
                    gvTariffFixedChargesList.DataBind();
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        public static void MergeRows(GridView gridView)
        {
            for (int rowIndex = gridView.Rows.Count - 2; rowIndex >= 0; rowIndex--)
            {
                GridViewRow row = gridView.Rows[rowIndex];
                GridViewRow previousRow = gridView.Rows[rowIndex + 1];

                Label lblClassName = (Label)row.FindControl("lblClassName");
                Label lblPreviousClassName = (Label)previousRow.FindControl("lblClassName");

                Label lblFromDate = (Label)row.FindControl("lblFromDate");
                Label lblPreviousFromDate = (Label)previousRow.FindControl("lblFromDate");
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    if (i == 1)
                    {
                        if (lblClassName.Text == lblPreviousClassName.Text)
                        {
                            row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                            previousRow.Cells[i].Visible = false;
                        }
                    }
                    if (i == 3)
                    {
                        if (lblFromDate.Text == lblPreviousFromDate.Text)
                        {
                            row.Cells[i].RowSpan = previousRow.Cells[i].RowSpan < 2 ? 2 : previousRow.Cells[i].RowSpan + 1;
                            previousRow.Cells[i].Visible = false;
                        }
                    }
                }
            }
        }
        private bool isValidate(TextBox txt, LinkButton lnkChargeType)
        {
            if (txt.Text == string.Empty)
            {
                //lblgridalertmsg.Text = "Enter " + lnkChargeType.Text + " Amount";
                lblgridalertmsg.Text = Resource.CHARGE_TYPE.Insert(6, lnkChargeType.Text + " ");
                txt.Focus();
                mpegridalert.Show();
                return true;
            }
            else
                return false;
        }
        #endregion

        #region Pageing
        /*protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridTariffFixedChargesList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridTariffFixedChargesList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindGridTariffFixedChargesList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                BindGridTariffFixedChargesList();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                objIdsignBal.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);

                //ddlPageSize.Items.Clear();
                //if (TotalRecords < Constants.PageSizeStarts)
                //{
                //    ListItem item = new ListItem(Constants.PageSizeStarts.ToString(), Constants.PageSizeStarts.ToString());
                //    ddlPageSize.Items.Add(item);
                //}
                //else
                //{
                //    int previousSize = Constants.PageSizeStarts;
                //    for (int i = Constants.PageSizeStarts; i <= TotalRecords; i = i + Constants.PageSizeIncrement)
                //    {
                //        ListItem item = new ListItem(i.ToString(), i.ToString());
                //        ddlPageSize.Items.Add(item);
                //        previousSize = i;
                //    }
                //    if (previousSize < TotalRecords)
                //    {
                //        ListItem item = new ListItem((previousSize + Constants.PageSizeIncrement).ToString(), (previousSize + Constants.PageSizeIncrement).ToString());
                //        ddlPageSize.Items.Add(item);
                //    }
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGridTariffFixedChargesList();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }*/
        #endregion

        //protected void ddlTypeOfCharge_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    EnableYearMonths(true);
        //    if (ddlTariffSubType.Items.Count > 0)
        //        ddlTariffSubType.SelectedIndex = 0;
        //    if (ddlTariff.Items.Count > 0)
        //        ddlTariff.SelectedIndex = 0;

        //    if (ddlTypeOfCharge.SelectedIndex > 0)
        //        ddlTariff.Enabled = true;
        //    else
        //        ddlTariff.Enabled = false;
        //}
    }
}