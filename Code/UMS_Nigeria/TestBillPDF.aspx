﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestBillPDF.aspx.cs" Inherits="UMS_Nigeria.TestBillPDF" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Styles/reset.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="out-bor">
        <div class="inner-sec">
            <asp:Panel ID="pnlMessage" runat="server">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
            <div class="inner-box">
                <div class="inner-box1">
                    <div class="text-inner">
                        Account No.<br />
                        <asp:TextBox ID="txtAccNo" CssClass="text-box" runat="server"></asp:TextBox>
                        <br />
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner">
                        Month<br />
                        <asp:TextBox ID="txtMonth" CssClass="text-box" runat="server"></asp:TextBox>
                        <br />
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner">
                        Year<br />
                        <asp:TextBox ID="txtYear" CssClass="text-box" runat="server"></asp:TextBox>
                        <br />
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="box_total">
                    <div class="box_total_a">
                        <asp:Button ID="btnGetPDF" CssClass="box_s" Text="Get PDF" runat="server" OnClick="btnGetPDF_Click" />
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
