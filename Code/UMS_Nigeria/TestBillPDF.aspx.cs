﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Xsl;
using System.Text;
using System.IO;
using Communication;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using System.Configuration;
using iDSignHelper;

namespace UMS_Nigeria
{
    public partial class TestBillPDF : System.Web.UI.Page
    {
        BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
        iDsignBAL _objiDsignBAL = new iDsignBAL();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGetPDF_Click(object sender, EventArgs e)
        {
            CustomerBillPDFBE objBE = new CustomerBillPDFBE();
            CustomerBillDetailsListBE objListBE = new CustomerBillDetailsListBE();

            objBE.BillMonth = string.IsNullOrEmpty(txtMonth.Text) ? 0 : Convert.ToInt32(txtMonth.Text);
            objBE.BillYear = string.IsNullOrEmpty(txtYear.Text) ? 0 : Convert.ToInt32(txtYear.Text);
            objBE.GlobalAccountNo = string.IsNullOrEmpty(txtAccNo.Text) ? string.Empty : txtAccNo.Text;

            XmlDocument xml = _objBillGenerationBal.GetCustomerBillDetailsForPDF(objBE);

            if (xml.InnerXml != null)
            {
                string strXSLTFile = Server.MapPath(ConfigurationManager.AppSettings["CustomerBillPDF"].ToString());

                //string strXMLFile = Server.MapPath("XSLT/PDFOutput_Test.xml");
                //XmlReader reader = XmlReader.Create(strXMLFile);
                //XmlReader reader = XmlReader.Create(xml);

                XslCompiledTransform objXSLTransform = new XslCompiledTransform();
                objXSLTransform.Load(strXSLTFile);
                StringBuilder htmlOutput = new StringBuilder();
                TextWriter htmlWriter = new StringWriter(htmlOutput);
                objXSLTransform.Transform(xml, null, htmlWriter);
                string htmltext = htmlOutput.ToString();

                //reader.Close();

                objListBE = _objiDsignBAL.DeserializeFromXml<CustomerBillDetailsListBE>(xml);

                if (objListBE.Items.Count > 0)
                {
                    string BusinessUnit = objListBE.Items[0].BusinessUnit;
                    string ServiceUnit = objListBE.Items[0].ServiceUnit;
                    string ServiceCenter = objListBE.Items[0].ServiceCenter;
                    string BookGroup = objListBE.Items[0].BookGroup;
                    string BookNo = objListBE.Items[0].BookNo;
                    string GlobalAccountNo = objListBE.Items[0].GlobalAccountNo;
                    string MonthName = objListBE.Items[0].MonthName;
                    int Year = objListBE.Items[0].Year;

                    string path = Server.MapPath(ConfigurationManager.AppSettings["BillGenerationPDF"].ToString());

                    // Determine whether the directory exists. 
                    if (!Directory.Exists(path))
                    {
                        // Try to create the directory.
                        DirectoryInfo di = Directory.CreateDirectory(path);
                    }
                    if (!Directory.Exists(path + "\\" + BusinessUnit))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path + "\\" + BusinessUnit);
                    }
                    if (!Directory.Exists(path + "\\" + BusinessUnit + "\\" + ServiceUnit))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path + "\\" + BusinessUnit + "\\" + ServiceUnit);
                    }
                    if (!Directory.Exists(path + "\\" + BusinessUnit + "\\" + ServiceUnit + "\\" + ServiceCenter))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path + "\\" + BusinessUnit + "\\" + ServiceUnit + "\\" + ServiceCenter);
                    }
                    if (!Directory.Exists(path + "\\" + BusinessUnit + "\\" + ServiceUnit + "\\" + ServiceCenter + "\\" + BookGroup))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path + "\\" + BusinessUnit + "\\" + ServiceUnit + "\\" + ServiceCenter + "\\" + BookGroup);
                    }
                    if (!Directory.Exists(path + "\\" + BusinessUnit + "\\" + ServiceUnit + "\\" + ServiceCenter + "\\" + BookGroup + "\\" + BookNo))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path + "\\" + BusinessUnit + "\\" + ServiceUnit + "\\" + ServiceCenter + "\\" + BookGroup + "\\" + BookNo);
                    }

                    string FileName = GlobalAccountNo + "-" + MonthName + "-" + Year + ".pdf";

                    if (File.Exists(path + "\\" + BusinessUnit + "\\" + ServiceUnit + "\\" + ServiceCenter + "\\" + BookGroup + "\\" + BookNo + "\\" + FileName))
                    {
                        File.Delete(path + "\\" + BusinessUnit + "\\" + ServiceUnit + "\\" + ServiceCenter + "\\" + BookGroup + "\\" + BookNo + "\\" + FileName);
                    }

                    string PDFFilePath = "\\GeneratedBills\\CustomerPDFBills\\" + BusinessUnit + "\\" + ServiceUnit + "\\" + ServiceCenter + "\\" + BookGroup + "\\" + BookNo + "\\" + FileName;
                    GeneratePDF _objGeneratePDF = new GeneratePDF();
                    string pdfFileName = Request.PhysicalApplicationPath + PDFFilePath;// "\\XSLT\\" + "PDFSample4.pdf";
                    _objGeneratePDF.ConvertHTMLToPDF(htmltext, pdfFileName);
                }
            }
        }
    }
}