﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using iDSignHelper;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using System.Xml;
using System.Security.Cryptography;
using System.Web.Services.Protocols;
using UMS_NigriaDAL;
using Resources;
using System.Data;
using System.IO;
using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace UMS_Nigeria
{
    /// <summary>
    /// Summary description for UMS_Service
    /// </summary>

    [WebService(Namespace = "http://tempuri.org/")]
    //[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]

    public class UMS_Service : System.Web.Services.WebService
    {
        #region Members

        BillCalculatorBE _ObjBillCalculatorBE = new BillCalculatorBE();
        BillCalculatorBAL _ObjBillCalculatorBAL = new BillCalculatorBAL();
        SideMenusBAL _ObjSideMenusBAL = new SideMenusBAL();
        LoginPageBAL _ObjLoginPageBAL = new LoginPageBAL();
        MastersBAL _ObjMastersBAL = new MastersBAL();
        BillGenerationBal _objBillGenerationBal = new BillGenerationBal();
        BillReadingsBAL _objBillReadingsBAL = new BillReadingsBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        CommonDAL _ObjCommonDAL = new CommonDAL();
        iDsignBAL objIDsignBAL = new iDsignBAL();
        #endregion

        #region Other
        public XmlDocument RaiseException(string uri, string webServiceNamespace, string errorMessage, string errorNumber, string errorSource)
        {
            XmlDocument xmlDoc = new XmlDocument();
            //Create the Detail node

            XmlNode rootNode = xmlDoc.CreateNode(XmlNodeType.Element, SoapException.DetailElementName.Name, SoapException.DetailElementName.Namespace);

            //Build specific details for the SoapException
            //Add first child of detail XML element.
            XmlNode errorNode = xmlDoc.CreateNode(XmlNodeType.Element, "Error", webServiceNamespace);

            //Create and set the value for the ErrorNumber node
            XmlNode errorNumberNode = xmlDoc.CreateNode(XmlNodeType.Element, "ErrorNumber", webServiceNamespace);

            errorNumberNode.InnerText = errorNumber;
            //Create and set the value for the ErrorMessage node
            XmlNode errorMessageNode = xmlDoc.CreateNode(XmlNodeType.Element, "ErrorMessage", webServiceNamespace);

            errorMessageNode.InnerText = errorMessage;

            //Create and set the value for the ErrorSource node
            XmlNode errorSourceNode = xmlDoc.CreateNode(XmlNodeType.Element, "ErrorSource", webServiceNamespace);

            errorSourceNode.InnerText = errorSource;
            //Append the Error child element nodes to the root detail node.

            errorNode.AppendChild(errorNumberNode);
            errorNode.AppendChild(errorMessageNode);
            errorNode.AppendChild(errorSourceNode);
            //Append the Detail node to the root node
            rootNode.AppendChild(errorNode);
            xmlDoc.AppendChild(rootNode);
            //Raise the exception  back to the caller
            return xmlDoc;
        }
        [WebMethod(Description = "Get Fixed and Energy Charges")]
        public XmlDocument GetFixedAndEneryCharges(string date, int SubType, float Comsuption, float Vat)
        {

            XmlDocument xml = null;
            if (_ObjCommonMethods.AuthenticateKey(""))
            {
                try
                {
                    _ObjBillCalculatorBE.Date = date;
                    _ObjBillCalculatorBE.Consumption = Comsuption;
                    _ObjBillCalculatorBE.SubType = SubType;
                    _ObjBillCalculatorBE.Vat = Vat;

                    return _ObjBillCalculatorBAL.GetFixedAndEneryCharges(_ObjBillCalculatorBE, ReturnType.Get);
                }
                catch (Exception ex)
                {
                    xml = RaiseException("GetException", "WSSoapException", ex.Message, "1000", ex.Source);
                }
            }
            return xml;

        }

        [WebMethod(Description = "Get Fixed and Energy charges by Consumption")]
        public XmlDocument CalculateBillWithConsumption(string date, int SubType, float Comsuption, int CusomertType)
        {
            XmlDocument xml = null;
            if (_ObjCommonMethods.AuthenticateKey(""))
            {
                try
                {

                    _ObjBillCalculatorBE.Date = date;
                    _ObjBillCalculatorBE.Consumption = Comsuption;
                    _ObjBillCalculatorBE.SubType = SubType;
                    // _ObjBillCalculatorBE.Vat = Vat;
                    _ObjBillCalculatorBE.CustomerTypeId = CusomertType;
                    return _ObjBillCalculatorBAL.CalculateBillWithConsumption(_ObjBillCalculatorBE, ReturnType.Get);
                }
                catch (Exception ex)
                {
                    xml = RaiseException("GetException", "WSSoapException", ex.Message, "1000", ex.Source);
                }
            }
            return xml;
        }

        [WebMethod(Description = "Get Customer Type")]
        public XmlDocument GetCutomerType(string key)
        {
            XmlDocument xml = null;
            if (_ObjCommonMethods.AuthenticateKey(""))
            {
                try
                {
                    return _ObjBillCalculatorBAL.GetCustomerType(ReturnType.Multiple);
                }
                catch (Exception ex)
                {
                    xml = RaiseException("GetException", "WSSoapException", ex.Message, "1000", ex.Source);
                }
            }
            return xml;

        }

        [WebMethod(Description = "Get Custmer Sub Types")]
        public XmlDocument GetCustomerSubTypes(int ClassId)
        {
            XmlDocument xml = null;
            if (_ObjCommonMethods.AuthenticateKey(""))
            {
                try
                {
                    _ObjBillCalculatorBE.ClassId = ClassId;
                    return _ObjBillCalculatorBAL.GetCustomerSubTypes(_ObjBillCalculatorBE, ReturnType.Multiple);
                }
                catch (Exception ex)
                {
                    xml = RaiseException("GetException", "WSSoapException", ex.Message, "1000", ex.Source);
                }
            }
            return xml;

        }

        [WebMethod(Description = "To Get Menus From DataBase")]// Bhimaraju-ID065(10-Feb-2014)
        public XmlDocument GetMenus(int RoleId)
        {
            return _ObjSideMenusBAL.GetMenus(RoleId);
        }

        [WebMethod(Description = "Get Login Details")]// Bhimaraju-ID065(12-Feb-2014)
        public XmlDocument Login(string UserName, string Password)
        {
            XmlDocument xml = null;
            //if (_ObjCommonMethods.AuthenticateKey(""))
            //{
            try
            {
                LoginPageBE _ObjLoginPageBE = new LoginPageBE();
                _ObjLoginPageBE.UserName = UserName;
                _ObjLoginPageBE.Password = objIDsignBAL.AESEncryptString(Password);
                xml = _ObjLoginPageBAL.Login(_ObjLoginPageBE, ReturnType.Get);
            }
            catch (Exception ex)
            {
                xml = RaiseException("GetException", "WSSoapException", ex.Message, "1000", ex.Source);
            }
            //}
            return xml;
        }

        [WebMethod(Description = "Insert Countries")]// Bhimaraju-ID065(12-Feb-2014)
        public XmlDocument Countries(string CountryName, string CountryCode, string Notes)
        {
            XmlDocument xml = null;

            if (_ObjCommonMethods.AuthenticateKey(""))
            {
                try
                {
                    MastersBE _ObjMastersBE = new MastersBE();

                    _ObjMastersBE.CountryName = CountryName;
                    _ObjMastersBE.CountryCode = CountryCode;
                    _ObjMastersBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    _ObjMastersBE.Notes = Notes;
                    //
                    xml = _ObjMastersBAL.Countries(_ObjMastersBE, Statement.Insert);
                }
                catch (Exception ex)
                {
                    xml = RaiseException("GetException", "WSSoapException", ex.Message, "1000", ex.Source);
                }
            }
            return xml;
        }

        [WebMethod(Description = "Get Countries List")]// Bhimaraju-ID065(12-Feb-2014)
        public XmlDocument GetCountries(string key)
        {
            XmlDocument xml = null;
            if (_ObjCommonMethods.AuthenticateKey(key))
            {
                try
                {
                    return _ObjMastersBAL.GetCountries(ReturnType.Get);
                }
                catch (Exception ex)
                {
                    xml = RaiseException("GetException", "WSSoapException", ex.Message, "1000", ex.Source);
                }
            }
            return xml;

        }

        [WebMethod(Description = "Insert States")]// Bhimaraju-ID065(13-Feb-2014)
        public XmlDocument States(string StateName, string StateCode, string DisplayCode, string CountryCode, string Notes)
        {
            XmlDocument xml = null;
            if (_ObjCommonMethods.AuthenticateKey(""))
            {
                try
                {
                    MastersBE _ObjMastersBE = new MastersBE();

                    _ObjMastersBE.StateName = StateName;
                    _ObjMastersBE.StateCode = StateCode;
                    _ObjMastersBE.DisplayCode = DisplayCode;
                    _ObjMastersBE.CountryCode = CountryCode;
                    _ObjMastersBE.Notes = Notes;
                    xml = _ObjMastersBAL.States(_ObjMastersBE, Statement.Insert);
                }
                catch (Exception ex)
                {
                    xml = RaiseException("GetException", "WSSoapException", ex.Message, "1000", ex.Source);
                }
            }
            return xml;
        }

        [WebMethod(Description = "Get States List")]// Bhimaraju-ID065(13-Feb-2014)
        public XmlDocument GetStates(string key)
        {
            XmlDocument xml = null;
            if (_ObjCommonMethods.AuthenticateKey(""))
            {
                try
                {
                    return _ObjMastersBAL.GetStates(ReturnType.Get);
                }
                catch (Exception ex)
                {
                    xml = RaiseException("GetException", "WSSoapException", ex.Message, "1000", ex.Source);
                }
            }
            return xml;
        }

        //[WebMethod(Description = "Insert Districts")]// Bhimaraju-ID065(13-Feb-2014)
        //public XmlDocument Districts(string DistrictName, string DistrictCode, string CountryCode, string StateCode, string Notes)
        //{
        //    XmlDocument xml = null;
        //    if (_ObjCommonMethods.AuthenticateKey(""))
        //    {
        //        try
        //        {
        //            MastersBE _ObjMastersBE = new MastersBE();

        //            _ObjMastersBE.DistrictName = DistrictName;
        //            _ObjMastersBE.DistrictCode = DistrictCode;
        //            _ObjMastersBE.CountryCode = CountryCode;
        //            _ObjMastersBE.StateCode = StateCode;
        //            _ObjMastersBE.Notes = Notes;
        //            xml = _ObjMastersBAL.Districts(_ObjMastersBE, Statement.Insert);
        //        }
        //        catch (Exception ex)
        //        {
        //            xml = RaiseException("GetException", "WSSoapException", ex.Message, "1000", ex.Source);
        //        }
        //    }
        //    return xml;
        //}

        [WebMethod(Description = "To get the list of Active Countries")]
        public XmlDocument GetActiveCountriesList(int pageNo, int pageSize)//Bhimaraju-ID065(13-Feb-2014)
        {
            XmlDocument xml = new XmlDocument();
            if (_ObjCommonMethods.AuthenticateKey(""))
            {
                try
                {

                    MastersBE _ObjMastersBE = new MastersBE();
                    _ObjMastersBE.PageNo = pageNo;
                    _ObjMastersBE.PageSize = pageSize;
                    xml = _ObjMastersBAL.Get(_ObjMastersBE, ReturnType.Get);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return xml;
        }

        [WebMethod(Description = "To get the list of Active States")]
        public XmlDocument GetActiveStatesList(int pageNo, int pageSize)//Bhimaraju-ID065(13-Feb-2014)
        {
            XmlDocument xml = new XmlDocument();
            if (_ObjCommonMethods.AuthenticateKey(""))
            {
                try
                {

                    MastersBE _ObjMastersBE = new MastersBE();
                    _ObjMastersBE.PageNo = pageNo;
                    _ObjMastersBE.PageSize = pageSize;
                    xml = _ObjMastersBAL.Get(_ObjMastersBE, ReturnType.Multiple);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return xml;
        }

        [WebMethod(Description = "To get the list of Active Districts")]
        public XmlDocument GetActiveDistrictsList(int pageNo, int pageSize)//Bhimaraju-ID065(13-Feb-2014)
        {
            XmlDocument xml = new XmlDocument();
            if (_ObjCommonMethods.AuthenticateKey(""))
            {

                try
                {

                    MastersBE _ObjMastersBE = new MastersBE();
                    _ObjMastersBE.PageNo = pageNo;
                    _ObjMastersBE.PageSize = pageSize;
                    xml = _ObjMastersBAL.Get(_ObjMastersBE, ReturnType.Double);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            } return xml;
        }

        [WebMethod(Description = "To delete or Deactive/Active Country details")]
        public XmlDocument DeleteActiveCountry(int IsActive, string CountryCode)//Bhimaraju-ID065(13-Feb-2014)
        {
            XmlDocument xml = new XmlDocument();
            if (_ObjCommonMethods.AuthenticateKey(""))
            {
                try
                {

                    MastersBE _ObjMastersBE = new MastersBE();
                    _ObjMastersBE.CountryCode = CountryCode;
                    _ObjMastersBE.IsActive = Convert.ToBoolean(IsActive);
                    _ObjMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();

                    xml = _ObjMastersBAL.Countries(_ObjMastersBE, Statement.Delete);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            } return xml;
        }

        [WebMethod(Description = "To delete or Deactive/Active State details")]
        public XmlDocument DeleteActiveState(int IsActive, string StateCode)//Bhimaraju-ID065(13-Feb-2014)
        {
            XmlDocument xml = new XmlDocument();
            if (_ObjCommonMethods.AuthenticateKey(""))
            {

                try
                {

                    MastersBE _ObjMastersBE = new MastersBE();
                    _ObjMastersBE.StateCode = StateCode;
                    _ObjMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    _ObjMastersBE.IsActive = Convert.ToBoolean(IsActive);

                    xml = _ObjMastersBAL.States(_ObjMastersBE, Statement.Delete);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            } return xml;
        }

        //[WebMethod(Description = "To delete or Deactive/Active District details")]
        //public XmlDocument DeleteActiveDistrict(int IsActive, string DistrictCode)//Bhimaraju-ID065(13-Feb-2014)
        //{
        //    XmlDocument xml = new XmlDocument();
        //    if (_ObjCommonMethods.AuthenticateKey(""))
        //    {
        //        try
        //        {

        //            MastersBE _ObjMastersBE = new MastersBE();
        //            _ObjMastersBE.DistrictCode = DistrictCode;
        //            _ObjMastersBE.IsActive = Convert.ToBoolean(IsActive);
        //            _ObjMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();

        //            xml = _ObjMastersBAL.Districts(_ObjMastersBE, Statement.Delete);

        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }
        //    } return xml;
        //}

        [WebMethod(Description = "Update Countries")]// Bhimaraju-ID065(13-Feb-2014)
        public XmlDocument UpdateCountries(string CountryName, string CountryCode, string Notes)
        {
            XmlDocument xml = null;
            if (_ObjCommonMethods.AuthenticateKey(""))
            {

                try
                {
                    MastersBE _ObjMastersBE = new MastersBE();

                    _ObjMastersBE.CountryName = CountryName;
                    _ObjMastersBE.CountryCode = CountryCode;
                    _ObjMastersBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    _ObjMastersBE.Notes = Notes;
                    xml = _ObjMastersBAL.Countries(_ObjMastersBE, Statement.Update);
                }
                catch (Exception ex)
                {
                    xml = RaiseException("GetException", "WSSoapException", ex.Message, "1000", ex.Source);
                }

            } return xml;
        }

        [WebMethod(Description = "Update States")]// Bhimaraju-ID065(13-Feb-2014)
        public XmlDocument UpdateStates(string StateName, string StateCode, string DisplayCode, string CountryCode, string Notes)
        {

            XmlDocument xml = null;
            if (_ObjCommonMethods.AuthenticateKey(""))
            {
                try
                {
                    MastersBE _ObjMastersBE = new MastersBE();

                    _ObjMastersBE.StateName = StateName;
                    _ObjMastersBE.StateCode = StateCode;
                    _ObjMastersBE.DisplayCode = DisplayCode;
                    _ObjMastersBE.CountryCode = CountryCode;
                    _ObjMastersBE.Notes = Notes;
                    xml = _ObjMastersBAL.States(_ObjMastersBE, Statement.Update);
                }
                catch (Exception ex)
                {
                    xml = RaiseException("GetException", "WSSoapException", ex.Message, "1000", ex.Source);
                }

            } return xml;
        }
        //[WebMethod(Description = "Update District")]// Bhimaraju-ID065(13-Feb-2014)
        //public XmlDocument UpdateDistrict(string DistrictName, string DistrictCode, string CountryCode, string StateCode, string Notes)
        //{
        //    XmlDocument xml = null;
        //    try
        //    {
        //        MastersBE _ObjMastersBE = new MastersBE();

        //        _ObjMastersBE.DistrictName = DistrictName;
        //        _ObjMastersBE.DistrictCode = DistrictCode;
        //        _ObjMastersBE.CountryCode = CountryCode;
        //        _ObjMastersBE.StateCode = StateCode;
        //        _ObjMastersBE.Notes = Notes;
        //        xml = _ObjMastersBAL.Districts(_ObjMastersBE, Statement.Update);
        //    }
        //    catch (Exception ex)
        //    {
        //        xml = RaiseException("GetException", "WSSoapException", ex.Message, "1000", ex.Source);
        //    }
        //    return xml;
        //}

        [WebMethod(Description = "To get the list of Active States By ddlCountryCode")]
        public XmlDocument GetStatesByCountryCode(string CountryCode)//Bhimaraju-ID065(13-Feb-2014)
        {
            try
            {
                XmlDocument xml = new XmlDocument();
                MastersBE _ObjMastersBE = new MastersBE();
                _ObjMastersBE.CountryCode = CountryCode;
                xml = _ObjMastersBAL.Get(_ObjMastersBE, ReturnType.Single);
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod(Description = "To get the Bill Generation Service Start Details if exists ")]
        public XmlDocument GetBillGenerationDeatils_ByServiceStartDate()//Suresh Kumar D-ID031(26-May-2014)
        {
            try
            {
                XmlDocument xml = new XmlDocument();
                xml = _objBillGenerationBal.GetDetails(ReturnType.Check);
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod(Description = "To get the Bill Generation Service Start Details if exists ")]
        public DataSet GenerateBill_ByBillQueueDetails(string feederId, string cycleId, int billingQueueScheduleId)//Suresh Kumar D-ID031(26-May-2014)
        {
            try
            {
                XmlDocument xml = new XmlDocument();
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                BillGenerationListBe _objBillGenerationListBe = new BillGenerationListBe();
                _objBillGenerationBe.FeederId = feederId;
                _objBillGenerationBe.CycleId = cycleId;
                _objBillGenerationBe.BillingQueueScheduleId = billingQueueScheduleId;
                xml = _objBillGenerationBal.InsertDetails(_objBillGenerationBe, Statement.Update);

                iDsignBAL _objiDsignBAL = new iDsignBAL();
                _objBillGenerationListBe = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xml);
                return _objiDsignBAL.ConvertListToDataSet<BillGenerationBe>(_objBillGenerationListBe.Items);
                //return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod(Description = "To insert the Meter Reading Details and Generate the Bill")]
        public XmlDocument GenerateBillDetails_ByAccountNo(string accountNumber, string presentReading, int month, int year, string loginId)//Suresh Kumar D-ID031(26-May-2014)
        {
            try
            {
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                _objBillGenerationBe.PresentReading = presentReading;
                _objBillGenerationBe.AccountNo = accountNumber;
                _objBillGenerationBe.BillGeneratedBy = loginId;
                _objBillGenerationBe.BillingYear = year;
                _objBillGenerationBe.BillingMonth = month;
                //return _objBillGenerationBal.InsertDetails(_objBillGenerationBe, Statement.Modify);

                _objBillGenerationBe = objIDsignBAL.DeserializeFromXml<BillGenerationBe>(_objBillGenerationBal.InsertDetails(_objBillGenerationBe, Statement.Modify));

                if (_objBillGenerationBe.CustomerBillId > 0)
                {
                    _objBillGenerationBe.IsSuccess = true;
                    _objBillGenerationBe.Messages = Resource.BILL_GENERATION_SUCCESS;
                }
                else
                {
                    _objBillGenerationBe.IsSuccess = false;
                    _objBillGenerationBe.Messages = Resource.BILL_GENERATION_FAIL;
                }
                return objIDsignBAL.DoSerialize<BillGenerationBe>(_objBillGenerationBe);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod(Description = "To Get Customer Previous Reading")]
        public XmlDocument GetCustomerPreviousReading(string accountNumber)//Suresh Kumar D-ID031(26-May-2014)
        {
            try
            {
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                _objBillGenerationBe.AccountNo = accountNumber;
                return _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.Single);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod(Description = "Biiling Years")]
        public XmlDocument GetYear()//Bhimaraju V-ID065(02-June-2014)
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                xmlResult = _objBillGenerationBal.GetDetails(ReturnType.Multiple);
                return xmlResult;
                //BillGenerationListBe objBillsListBE = objIDsignBAL.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                //objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlYear, "Year", "Year", "Year", true);
                //objMastersListBE = objIdsignBal.DeserializeFromXml<MastersListBE>(objMastersBAL.PaymentEntry(ReturnType.Get));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod(Description = "Biiling Months")]
        public XmlDocument GetMonths(int YearId)//Bhimaraju V-ID065(02-June-2014)
        {
            try
            {
                XmlDocument xmlResult = new XmlDocument();
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                _objBillGenerationBe.Year = YearId;
                return xmlResult = _objBillGenerationBal.GetDetails(_objBillGenerationBe, ReturnType.Get);
                //BillGenerationListBe objBillsListBE = objIdsignBal.DeserializeFromXml<BillGenerationListBe>(xmlResult);
                //objIdsignBal.FillDropDownList(objIdsignBal.ConvertListToDataSet<BillGenerationBe>(objBillsListBE.Items), ddlMonth, "MonthName", "Month", "Month", true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod(Description = "AuthenticatedPersonOrNot")]
        public bool GetAuthentication(string UserId, string PageName)//Bhimaraju V-ID065(02-June-2014)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                objMastersBE.UserId = UserId;
                objMastersBE.PageName = PageName.Trim('~');
                objMastersBE = objIDsignBAL.DeserializeFromXml<MastersBE>(_ObjMastersBAL.GetBusinessUnits(objMastersBE, ReturnType.Single));
                bool IsPageAccess = objMastersBE.IsPageAccess;
                return IsPageAccess;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod(EnableSession = true, Description = "To Get Customer Previous Reading")]
        public XmlDocument CheckReading(string accountNumber, string currentReading)//Suresh Kumar D-ID031(26-May-2014)
        {
            try
            {
                decimal usage = 0;
                decimal AverageUsage = 0;
                XmlDocument resultXml = new XmlDocument();
                ServiceMessagesBe _objServiceMessagesBe = new ServiceMessagesBe();
                BillGenerationBe _objBilling = new BillGenerationBe();
                _objBilling.AccountNo = accountNumber;
                _objBilling = objIDsignBAL.DeserializeFromXml<BillGenerationBe>(_objBillGenerationBal.GetCustomerReadingDetails(_objBilling, ReturnType.Get));
                if (currentReading.Length == _objBilling.Dials)
                {
                    _objServiceMessagesBe.IsSuccess = true;


                    if (_objBilling.PresentReading.Trim().Substring(0, 1) == "9" && currentReading.Trim().Substring(0, 1) == "0")
                    {
                        _objServiceMessagesBe.IsSuccess = false;
                        _objServiceMessagesBe.ConfirmMessageStatus = true;
                        _objServiceMessagesBe.Message = Resource.RESETMETER_BODY;

                        string StrValue = _ObjCommonMethods.GetAllNines(_objBilling.Dials);
                        usage = ((Convert.ToDecimal(StrValue) - Convert.ToDecimal(_objBilling.PresentReading)) + Convert.ToDecimal(currentReading) + 1);

                        AverageUsage = ((Convert.ToDecimal(_objBilling.AverageReading) * _objBilling.TotalRecords) + usage) / (_objBilling.TotalRecords + 1);
                        if (AverageUsage > _ObjCommonMethods.AboveHugeAmount(Convert.ToDecimal(_objBilling.AverageReading)) || AverageUsage < _ObjCommonMethods.BelowHugeAmount(Convert.ToDecimal(_objBilling.AverageReading)))
                        {
                            _objServiceMessagesBe.ConfirmMessageStatus = true;
                            _objServiceMessagesBe.Message = Resource.METER_HUGE_RESET_MESSAGE;
                        }
                    }
                    else
                    {
                        if (Convert.ToDecimal(currentReading.Trim()) > Convert.ToDecimal(_objBilling.PresentReading.Trim()))
                        {
                            _objServiceMessagesBe.ConfirmMessageStatus = false;
                            usage = Convert.ToDecimal(currentReading) - Convert.ToDecimal(_objBilling.PresentReading);

                            AverageUsage = ((Convert.ToDecimal(_objBilling.AverageReading) * _objBilling.TotalRecords) + usage) / (_objBilling.TotalRecords + 1);
                            if (AverageUsage > _ObjCommonMethods.AboveHugeAmount(Convert.ToDecimal(_objBilling.AverageReading)) || AverageUsage < _ObjCommonMethods.BelowHugeAmount(Convert.ToDecimal(_objBilling.AverageReading)))
                            {
                                _objServiceMessagesBe.IsSuccess = false;
                                _objServiceMessagesBe.ConfirmMessageStatus = true;
                                _objServiceMessagesBe.Message = UMS_Resource.INVALIDUSAGEBODY_METERREADING;
                            }
                        }
                        else
                        {
                            _objServiceMessagesBe.IsSuccess = false;
                            _objServiceMessagesBe.ConfirmMessageStatus = false;
                            _objServiceMessagesBe.Message = string.Format(Resource.INVALID_READING, _objBilling.Dials);
                        }
                    }
                }
                else
                {
                    _objServiceMessagesBe.IsSuccess = false;
                    _objServiceMessagesBe.ConfirmMessageStatus = false;
                    _objServiceMessagesBe.Message = string.Format(Resource.INVALID_DIALS_MESSAGE, _objBilling.Dials);
                }
                return objIDsignBAL.DoSerialize<ServiceMessagesBe>(_objServiceMessagesBe);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod(Description = "Generate Bill and file for Bill Details")]
        public void GenerateBill_SendMail()
        {
            BillGenerationListBe _objBillGenerationListBe = new BillGenerationListBe();

            XmlDocument xml = GetBillGenerationDeatils_ByServiceStartDate();
            _objBillGenerationListBe = objIDsignBAL.DeserializeFromXml<BillGenerationListBe>(xml);

            string feederId = string.Empty;
            string cycleId = string.Empty;
            int billingQueueScheduleId = 0;
            foreach (BillGenerationBe item in _objBillGenerationListBe.Items)
            {
                if (item.CycleId != null)
                    cycleId = item.CycleId;
                if (item.FeederId != null)
                    feederId = item.FeederId;

                billingQueueScheduleId = item.BillingQueueScheduleId;

                string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".txt";
                //set up a filestream
                string filePath = Server.MapPath(ConfigurationManager.AppSettings["BillGeneration"].ToString()) + Path.GetFileName(fileName);
                FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);

                DataSet ds = GenerateBill_ByBillQueueDetails(feederId, cycleId, billingQueueScheduleId);

                //string fileName = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss");
                ////set up a filestream
                //FileStream fs = new FileStream(@"D:\EDMS_BillGenerations\" + fileName + ".txt", FileMode.OpenOrCreate, FileAccess.Write);

                //set up a streamwriter for adding text
                StreamWriter sw = new StreamWriter(fs);

                //find the end of the underlying filestream
                sw.BaseStream.Seek(0, SeekOrigin.End);

                int rowCount = 0;
                string billDetails = string.Empty;
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    rowCount++;
                    billDetails += string.Format("{0,-6},{1,-107},{2,-11}", string.Empty, rowCount, rowCount).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-15},{1,-70},{2,-15},{3,-6}", "Route: 0", "Seq.", "Route: 0", "Seq.").Replace(',', ' ') +
                                Environment.NewLine +
                                Environment.NewLine + string.Format("{0,-34},{1,-63},{2,-25}", string.Empty, dr["BusinessUnitName"].ToString(), dr["BusinessUnitName"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-23},{1,-74},{2,-9}", string.Empty, "PLS PAY AT ANY BEDC OFFICE OR DESIGNATED BANKS", dr["MonthName"].ToString() + dr["BillYear"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-31},{1,-27},{2,-36},{3,-22}", string.Empty, "Private Account", dr["MonthName"].ToString() + dr["BillYear"].ToString(), "Private Account", dr["MonthName"].ToString() + dr["BillYear"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-96},{1,-16}", string.Empty, dr["AccountNo"].ToString()).Replace(',', ' ') +
                                Environment.NewLine +
                                Environment.NewLine + string.Format("{0,8},{1,-38},{2,-28},{3,-22},{4,-30}", string.Empty, dr["AccountNo"].ToString(), string.Empty, string.Empty, dr["Name"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,8},{1,-38},{2,-28},{3,-22},{4,-30}", string.Empty, dr["Name"].ToString(), DateTime.Now.AddDays(5).ToString("dd/MM/yyyy"), dr["PreviousBalance"].ToString(), dr["PostalHouseNo"].ToString() + " " + dr["PostalStreet"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,8},{1,-38},{2,-28},{3,-22},{4,-30}", string.Empty, dr["PostalHouseNo"].ToString() + " " + dr["PostalStreet"].ToString(), dr["MeterNo"].ToString(), dr["NetEnergyCharges"].ToString(), dr["PostalZipCode"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,8},{1,-38},{2,-28},{3,-22},{4,-30}", string.Empty, dr["PostalZipCode"].ToString(), dr["NetFixedCharges"].ToString(), string.Empty, dr["ServiceHouseNo"].ToString() + " " + dr["ServiceStreet"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,8},{1,-38},{2,-28},{3,-22},{4,-30}", string.Empty, string.Empty, dr["Dials"].ToString(), dr["NetArrears"].ToString(), dr["ServiceZipCode"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,8},{1,-90},{2,-15}", string.Empty, "THIS BILLING IS FOR APRIL 2014 CONSUMPTION", dr["MeterNo"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + Environment.NewLine +
                                Environment.NewLine + string.Format("{0,-13},{1,-7},{2,-16},{3,-12},{4,-7},{5,-7},{6,7},{7,11}", "ENERGY", dr["TariffId"].ToString(), dr["ReadDate"].ToString(), dr["PreviousReading"].ToString(), dr["PresentReading"].ToString(), dr["Multiplier"].ToString(), dr["Usage"].ToString(), dr["NetEnergyCharges"].ToString()).Replace(',', ' ') +
                                Environment.NewLine +
                                Environment.NewLine + string.Format("{0,-13},{1,-7},{2,-16},{3,-12},{4,-7},{5,-7},{6,7},{7,11}", "FCR2S", dr["TariffId"].ToString(), "----", "----", "----", "----", "----", dr["NetFixedCharges"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-22},{1,-19}", string.Empty, "Billing Periods: 01").Replace(',', ' ') +
                                Environment.NewLine + Environment.NewLine +
                                Environment.NewLine + string.Format("{0,-33},{1,-22}", "LAST PAYMENT DATE  " + dr["LastPaymentDate"].ToString(), "AMOUNT      " + dr["LastPaidAmount"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-100},{1,-11}", "RECONNECTION FEE IS NOW =N=5000", DateTime.Now.AddDays(5).ToString("dd/MM/yyyy")).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-70},{1,17},{2,27}", "PAY WITHIN 7 DAYS TO AVOID DISCONNECTION", dr["NetArrears"].ToString(), dr["NetArrears"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-70},{1,17},{2,27}", string.Empty, dr["TotalBillAmount"].ToString(), dr["TotalBillAmount"].ToString()).Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-70},{1,17},{2,27}", string.Empty, dr["VAT"].ToString(), dr["VAT"].ToString()).Replace(',', ' ') +
                                Environment.NewLine +
                                Environment.NewLine + string.Format("{0,-70},{1,17},{2,27}", string.Empty, dr["TotalBillAmountWithTax"].ToString(), dr["TotalBillAmountWithTax"].ToString()).Replace(',', ' ') +
                                Environment.NewLine +
                                Environment.NewLine + string.Format("{0,-27},{1,-77},{2,-16}", "OLD ACCT --XYZ--", "ENR2S- Rate:=N= 11.37", "--XYZ--").Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-2},{1,-60}", string.Empty, "THANKS FOR YOUR PATRONAGE").Replace(',', ' ') +
                                Environment.NewLine + string.Format("{0,-5},{1,-26},{2,-61}", string.Empty, "- ENR2S- Rate:=N= 11.37", " Marketer:  ENOGHAYANGBON P.(07039738955)").Replace(',', ' ') +
                                Environment.NewLine +
                        //+ Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine +
                    "-------------------------------------------------------------------------------------------------------------------" + Environment.NewLine;

                }

                //add the text
                sw.WriteLine(billDetails);
                //add the text to the underlying filestream

                sw.Flush();
                //close the writer
                sw.Close();
                //fs.Flush();
                //fs.Close();
                UpdateBillFileDetails(billingQueueScheduleId, (int)EnumBillGenarationStatus.FileCreated, fileName);

                _ObjCommonMethods.SendMail_BillGeneration(filePath, cycleId, feederId, item.MonthName, item.BillingYear, Session[UMS_Resource.SESSION_USER_EMAILID].ToString());

                UpdateBillGenerationStatus(billingQueueScheduleId, (int)EnumBillGenarationStatus.EmailSent);
                UpdateBillGenerationStatus(billingQueueScheduleId, (int)EnumBillGenarationStatus.BillingQueeClosed);
            }
        }

        [WebMethod(Description = "To Update the Bill Generation status")]
        public XmlDocument UpdateBillGenerationStatus(int billingQueueScheduleId, int billGenarationStatusId)//Suresh Kumar D-ID031(26-May-2014)
        {
            try
            {
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                _objBillGenerationBe.BillingQueueScheduleId = billingQueueScheduleId;
                _objBillGenerationBe.BillGenarationStatusId = billGenarationStatusId;
                //_objBillGenerationBe.BillingYear = year;
                //_objBillGenerationBe.BillingMonth = month;

                return _objBillGenerationBal.UpdateBillDetails(_objBillGenerationBe, Statement.Update);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod(Description = "To Update the Bill Generation File Details")]
        public XmlDocument UpdateBillFileDetails(int billingQueueScheduleId, int billGenarationStatusId, string fileName)//Suresh Kumar D-ID031(14-Jul-2014)
        {
            try
            {
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                _objBillGenerationBe.BillingQueueScheduleId = billingQueueScheduleId;
                _objBillGenerationBe.BillGenarationStatusId = billGenarationStatusId;
                _objBillGenerationBe.FilePath = fileName;
                //_objBillGenerationBe.BillingMonth = month;

                return _objBillGenerationBal.UpdateBillDetails(_objBillGenerationBe, Statement.Insert);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod(Description = "To Update the Customer Bill Generation File Details")]
        public XmlDocument UpdateCustomerBillFile(string accountNo, int billYear, int billMonth, string fileName)//Suresh Kumar D-ID031(15-Jul-2014)
        {
            try
            {
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                _objBillGenerationBe.AccountNo = accountNo;
                _objBillGenerationBe.BillingYear = billYear;
                _objBillGenerationBe.BillingMonth = billMonth;
                _objBillGenerationBe.FilePath = fileName;
                return _objBillGenerationBal.UpdateBillDetails(_objBillGenerationBe, Statement.Modify);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod(MessageName = "UpdateBillGenFile1", Description = "To Update the Bill Generation All File Details")]
        public XmlDocument UpdateBillGenFile(string billingQueueScheduleIdList, int billGenarationStatusId, string fileName)//Suresh Kumar D-ID031(14-Jul-2014)
        {
            try
            {
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                _objBillGenerationBe.BillingQueueScheduleIdList = billingQueueScheduleIdList;
                _objBillGenerationBe.BillGenarationStatusId = billGenarationStatusId;
                _objBillGenerationBe.FilePath = fileName;
                //_objBillGenerationBe.BillingMonth = month;

                return _objBillGenerationBal.UpdateBillGenFile(_objBillGenerationBe);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [WebMethod(MessageName = "UpdateBillGenFile2", Description = "To Update the Bill Generation Status")]
        public XmlDocument UpdateBillGenFile(string billingQueueScheduleIdList, int billGenarationStatusId)//Neeraj Kanojiya(19-Feb-2014)
        {
            try
            {
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                _objBillGenerationBe.BillingQueueScheduleIdList = billingQueueScheduleIdList;
                _objBillGenerationBe.BillGenarationStatusId = billGenarationStatusId;
                return _objBillGenerationBal.UpdateBillDetails(_objBillGenerationBe, Statement.UpdateBillSattus);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Customers
        [WebMethod(Description = "Get customer details by account #.")]
        public XmlDocument GetCustomerDetialsByAccountMUser(string AccountNo)//NEERAJ D-ID077(31-JYLY-2014)
        {
            try
            {
                CustomerDetailsBe objCustomerDetailsBe = new CustomerDetailsBe();
                CustomerDetailsBAL objCustomerDetailsBAL = new CustomerDetailsBAL();
                objCustomerDetailsBe.AccountNo = AccountNo;
                XmlDocument xml = new XmlDocument();
                xml = objCustomerDetailsBAL.GetCustomersByAccountNo_ServBAL(objCustomerDetailsBe);
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Customer Payments

        [WebMethod(Description = "Return Due payment for an account number.")]
        public string GetDuePaymentByAccountNo(string AccountNo)//NEERAJ D-ID077(28-May-2014)
        {
            try
            {
                XmlDocument xml = new XmlDocument();
                iDsignBAL _objiDsignBAL = new iDsignBAL();
                MastersBE objMasterBe = new MastersBE();
                MastersListBE objMasterListBe = new MastersListBE();
                objMasterBe.AccountNo = AccountNo;
                objMasterListBe = _objiDsignBAL.DeserializeFromXml<MastersListBE>(_ObjMastersBAL.Get(objMasterBe, ReturnType.Retrieve));
                return objMasterListBe.Items[0].DueAmount.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod(Description = "Add customer payments.")]
        public bool AddCustomrPayment(string AccountNo, string Amount, string BatchNo, string PaymentMode, string ReceiveDate, string CreatedBy)//NEERAJ D-ID077(28-May-2014)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                MastersBAL objMastersBAL = new MastersBAL();
                iDsignBAL _objiDsignBAL = new iDsignBAL();
                objMastersBE.AccountNo = AccountNo;
                objMastersBE.ReceiptNo = 001.ToString();
                objMastersBE.PaymentModeId = Convert.ToInt32(PaymentMode);
                objMastersBE.CreatedBy = "";
                objMastersBE.BatchNo = Convert.ToInt32(BatchNo);
                objMastersBE.BillNo = "";
                objMastersBE.PaidAmount = decimal.Parse(Amount);
                objMastersBE.PaymentRecievedDate = _ObjCommonMethods.Convert_MMDDYY(ReceiveDate);

                objMastersBE = _objiDsignBAL.DeserializeFromXml<MastersBE>(objMastersBAL.PaymentEntry(objMastersBE, Statement.Insert));
                return objMastersBE.IsSuccess;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod(Description = "Payment Modes.")]
        public XmlDocument GetPaymentModes()//NEERAJ D-ID077(28-May-2014)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                MastersListBE objMastersListBE = new MastersListBE();
                MastersBAL objMastersBAL = new MastersBAL();
                iDsignBAL objIdsignBal = new iDsignBAL();
                XmlDocument xml = new XmlDocument();
                xml = objMastersBAL.PaymentEntry(ReturnType.Get);
                return xml;
                //objMastersListBE = objIdsignBal.DeserializeFromXml<MastersListBE>(objMastersBAL.PaymentEntry(ReturnType.Get));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod(Description = "Payment Modes.")]
        public XmlDocument GetCustomerPaymentHistory(string AccountNo, string NumberOfRecords)//NEERAJ D-ID077(29-May-2014)
        {
            try
            {
                ConsumerBe objConsumerBe = new ConsumerBe();
                ConsumerBal objConsumerBal = new ConsumerBal();
                iDsignBAL objIdsignBal = new iDsignBAL();
                XmlDocument xml = new XmlDocument();
                xml = objConsumerBal.GetCustomerPaymentHistoryBAL(objConsumerBe);
                return xml;
                //objMastersListBE = objIdsignBal.DeserializeFromXml<MastersListBE>(objMastersBAL.PaymentEntry(ReturnType.Get));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [WebMethod(Description = "Enter payments by account # and payment amount.")]
        public XmlDocument InsertPaymentByAccountMUser(string AccountNo, decimal PaidAmount, string Remarks, string CreatedBy)//NEERAJ D-ID077(31-JYLY-2014)
        {
            try
            {
                MastersBE objMastersBE = new MastersBE();
                MastersBAL objMastersDAL = new MastersBAL();
                objMastersBE.AccountNo = AccountNo;
                objMastersBE.PaidAmount = PaidAmount;
                objMastersBE.Remarks = Remarks;
                objMastersBE.CreatedBy = CreatedBy;
                XmlDocument xml = new XmlDocument();
                xml = objMastersDAL.PaymentEntry(objMastersBE, Statement.Service);
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Updated Customer lat and Lng
        [WebMethod(Description = "Get Cycle List.")]
        public XmlDocument GetCycleList(string key)//NEERAJ D-ID077(21-Aug-2014)
        {
            XmlDocument xml = new XmlDocument();
            if (_ObjCommonMethods.AuthenticateKey(key))
            {
                try
                {
                    ConsumerBe objConsumerBe = new ConsumerBe();
                    ConsumerBal objConsumerBal = new ConsumerBal();
                    iDsignBAL objIdsignBal = new iDsignBAL();
                    xml = objConsumerBal.Retrieve(ReturnType.Group);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return xml;
        }
        [WebMethod(Description = "Get BookNO List.")]
        public XmlDocument GetBookNoList(string key)//NEERAJ D-ID077(21-Aug-2014)
        {
            XmlDocument xml = new XmlDocument();
            if (_ObjCommonMethods.AuthenticateKey(key))
            {
                try
                {
                    ConsumerBe objConsumerBe = new ConsumerBe();
                    ConsumerBal objConsumerBal = new ConsumerBal();
                    iDsignBAL objIdsignBal = new iDsignBAL();
                    xml = objConsumerBal.GetBookNoListBAL();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return xml;
        }
        [WebMethod(Description = "Get Customer by Cycle/BookNo/Both List.")]
        public XmlDocument GetCustomerBy_CB(string key, string CycleID, string BookNoId)//NEERAJ D-ID077(21-Aug-2014)
        {
            XmlDocument xml = new XmlDocument();
            if (_ObjCommonMethods.AuthenticateKey(key))
            {
                try
                {
                    ConsumerBe objConsumerBe = new ConsumerBe();
                    ConsumerBal objConsumerBal = new ConsumerBal();
                    objConsumerBe.CycleId = CycleID;
                    objConsumerBe.BU_ID = BookNoId;
                    iDsignBAL objIdsignBal = new iDsignBAL();
                    xml = objConsumerBal.GetCustomersBy_CB_BAL(objIDsignBAL.DoSerialize<ConsumerBe>(objConsumerBe));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return xml;
        }
        [WebMethod(Description = "Update Customer longitude and latitude.")]
        public XmlDocument UpdateCustomerLongitudeAndLatitude(string key, string AccountNo, string Longitude, string Latitude)//NEERAJ D-ID077(21-Aug-2014)
        {
            XmlDocument xml = new XmlDocument();
            if (_ObjCommonMethods.AuthenticateKey(key))
            {
                try
                {
                    ConsumerBe objConsumerBe = new ConsumerBe();
                    ConsumerBal objConsumerBal = new ConsumerBal();
                    objConsumerBe.AccountNo = AccountNo;
                    objConsumerBe.Longitude = Longitude;
                    objConsumerBe.Latitude = Latitude;
                    iDsignBAL objIdsignBal = new iDsignBAL();
                    xml = objConsumerBal.UpdateCustomerLongitudeAndLatitudeBAL(objIDsignBAL.DoSerialize<ConsumerBe>(objConsumerBe));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return xml;
        }
        #endregion

        #region New Customer Locator
        [WebMethod(Description = "Insert New Customer Locator")]
        public XmlDocument InsertNewCustomerLocator(string key, string Name, string Primary_ContactNo, string Secondary_ContactNo, string CycleID, string MarketerID, string Note, string Longitude, string Latitude)//NEERAJ D-ID077(04-Sep-2014)
        {
            XmlDocument xml = new XmlDocument();
            if (_ObjCommonMethods.AuthenticateKey(key))
            {
                try
                {
                    NewCustomerLocatorBE objNewCustomerLocatorBE = new NewCustomerLocatorBE();
                    CustomerDetailsBAL objCustomerDetailsBAL = new CustomerDetailsBAL();
                    iDsignBAL objIdsignBal = new iDsignBAL();

                    objNewCustomerLocatorBE.Name = Name;
                    objNewCustomerLocatorBE.Primary_ContactNo = Primary_ContactNo;
                    objNewCustomerLocatorBE.Secondary_ContactNo = Secondary_ContactNo;
                    objNewCustomerLocatorBE.CycleID = CycleID;
                    objNewCustomerLocatorBE.MarketerName = MarketerID;
                    objNewCustomerLocatorBE.Note = Note;
                    objNewCustomerLocatorBE.Longitude = Longitude;
                    objNewCustomerLocatorBE.Latitude = Latitude;


                    xml = objCustomerDetailsBAL.InsertNewCustomerLocatorBAL(objNewCustomerLocatorBE);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return xml;
        }

        [WebMethod(Description = "Get Marketer List")]
        public XmlDocument GetAgenciesList(string key)//NEERAJ D-ID077(04-Sep-2014)
        {
            XmlDocument xml = new XmlDocument();
            if (_ObjCommonMethods.AuthenticateKey(key))
            {
                try
                {
                    ConsumerBal objConsumerBal = new ConsumerBal();



                    xml = objConsumerBal.Retrieve(ReturnType.Retrieve);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return xml;
        }
        #endregion

        #region EBill

        [WebMethod(Description = "Check EBill Time")]//Neeraj Kanojiya ID077-- This service with return ture or false based on service time for generating e-bill files.
        public bool CheckEBillExecutionTime()
        {
            MastersBE _ObjMastersBE = new MastersBE();
            MastersBAL _ObjMastersBAL = new MastersBAL();
            iDsignBAL _ObjiDsignBAL = new iDsignBAL();
            XmlDocument _ObjXmlDocument = new XmlDocument();
            _ObjXmlDocument = _ObjMastersBAL.CheckEBillExecutionTimeBAL(_ObjMastersBE);
            _ObjMastersBE = _ObjiDsignBAL.DeserializeFromXml<MastersBE>(_ObjXmlDocument);
            return _ObjMastersBE.IsExists;
        }

        [WebMethod(EnableSession = true, Description = "Generate file for E-Bill Details")]
        public string GetEBillDetails(string key)//Generate Ebill Payment Reports.
        {
            string returnMsg = Resource.SUCCESSFUL;
            try
            {
                if (_ObjCommonMethods.AuthenticateKey(key))
                {
                    #region DataTable to add business unit and files details
                    DataSet dsEBillMailLogs = new DataSet("EbillPaymentsList");
                    DataTable dtEBillMailLogs = new DataTable();
                    dsEBillMailLogs.Tables.Add(dtEBillMailLogs);

                    DataRow drEBillMailLogs;

                    dtEBillMailLogs.Columns.Add("BuName", typeof(string));
                    dtEBillMailLogs.Columns.Add("BU_TxtFile", typeof(string));
                    dtEBillMailLogs.Columns.Add("BU_XlsFile", typeof(string));
                    dtEBillMailLogs.Columns.Add("ConsolidatedDuplicateFile", typeof(string));
                    dtEBillMailLogs.Columns.Add("ConsolidatedFile", typeof(string));



                    #endregion

                    #region Members
                    MastersBE _ObjMastersBE = new MastersBE();
                    MastersListBE _ObjMastersListBE = new MastersListBE();
                    MastersListBE_MoreInfo _ObjMastersListBE_MoreInfo = new MastersListBE_MoreInfo();
                    MastersBAL _ObjMastersBAL = new MastersBAL();
                    XmlDocument _ObjXmlDocument = new XmlDocument();
                    iDsignBAL _ObjiDsignBAL = new iDsignBAL();
                    List<MastersBE> _ObjMastersBEFileGen = new List<MastersBE>();
                    DataSet WorkBook = new DataSet();
                    //Session[UMS_Resource.SESSION_DATASETEBILLPAY] = null;
                    string space = "";
                    string eBillFormate = string.Empty;
                    string TempBU = string.Empty;
                    int Frmtcounter = 0;
                    int itemCounter = 0;
                    string fileNameTxt = string.Empty;
                    string fileNameXls = string.Empty;
                    string BUS_Unit = string.Empty;
                    string FileName = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["EBillPayFiles"]) + Resource.EBILL_EXCELL_NAME + "_" + DateTime.Now.ToString(Resource.EBILL_DATE_FRMT1) + Resource.Formate_Excel;//Custom
                    string DupConsolidatedFileName = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["EBillPayFiles"]) + "Duplicate_Consolidate" + "_" + DateTime.Now.ToString(Resource.EBILL_DATE_FRMT1) + Resource.Formate_Excel;//Custom
                    string CompleteXlsFileName = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["EBillPayFiles"]) + fileNameXls + Resource.Formate_Excel;
                    #endregion

                    _ObjXmlDocument = _ObjMastersBAL.GetEBillDetailsBAL(_ObjMastersBE);

                    _ObjMastersListBE = _ObjiDsignBAL.DeserializeFromXml<MastersListBE>(_ObjXmlDocument);//Get Responce for Ebill Payment Rpt

                    _ObjMastersListBE_MoreInfo = _ObjiDsignBAL.DeserializeFromXml<MastersListBE_MoreInfo>(_ObjXmlDocument);//Get Responce for Ebill Payment Rpt for more than one payment by a customer in a day.

                    #region Generats EBill Payment Rpt
                    if (_ObjMastersListBE != null && _ObjMastersListBE.Items.Count > 0)
                    {
                        TempBU = _ObjMastersListBE.Items[0].DistName;

                        foreach (MastersBE Mas in _ObjMastersListBE.Items)
                        {
                            if (TempBU == Mas.DistName)
                            {
                                if (Frmtcounter == 0)
                                {
                                    BUS_Unit = Mas.DistName;
                                    eBillFormate += Mas.TrnDate.ToString(Resource.EBILL_DATE_FRMT2) + Mas.BatchNo + Mas.BatchTotal.ToString(Resource.EBILL_CURRENCY_FRMT1) + Resource.PAYDIRECT_PAYMENT + space.PadRight(33) + Mas.BatchNo;
                                }
                                eBillFormate += Environment.NewLine + Mas.AccountNo + Mas.TrnAmount.ToString(Resource.EBILL_CURRENCY_FRMT2) + Resource.EBILL_CASH_ID + space.PadRight(5) + Mas.TrnDate.ToString(Resource.EBILL_DATE_FRMT1);
                                _ObjMastersBEFileGen.Add(Mas);
                                Frmtcounter++;
                                if (itemCounter == _ObjMastersListBE.Items.Count - 1)
                                {
                                    //fileNameExcelCompletePath = _ObjCommonMethods.SaveExcel(_ObjMastersBEFileGen, fileNameTxt);
                                    fileNameXls = BUS_Unit + "_" + DateTime.Now.ToString(Resource.EBILL_DATE_FRMT1);
                                    WorkBook.Tables.Add(_ObjCommonMethods.SaveExcel(_ObjMastersBEFileGen, fileNameXls));

                                    fileNameTxt = _ObjCommonMethods.GenerateTxtEBillFile(eBillFormate, BUS_Unit);
                                    _ObjCommonMethods.SendMailToBUEBillFiles(fileNameTxt, fileNameXls);

                                    #region Adding values to dt column for mail log insertion
                                    drEBillMailLogs = dtEBillMailLogs.NewRow();
                                    drEBillMailLogs["BuName"] = BUS_Unit;
                                    drEBillMailLogs["BU_TxtFile"] = fileNameTxt;
                                    drEBillMailLogs["BU_XlsFile"] = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["EBillPayFiles"]) + fileNameXls + Resource.Formate_Excel;
                                    drEBillMailLogs["ConsolidatedFile"] = FileName;
                                    drEBillMailLogs["ConsolidatedDuplicateFile"] = DupConsolidatedFileName;
                                    dtEBillMailLogs.Rows.Add(drEBillMailLogs);
                                    #endregion

                                    _ObjMastersBEFileGen = new List<MastersBE>();
                                }
                            }
                            else
                            {
                                fileNameXls = BUS_Unit + "_" + DateTime.Now.ToString(Resource.EBILL_DATE_FRMT1);
                                WorkBook.Tables.Add(_ObjCommonMethods.SaveExcel(_ObjMastersBEFileGen, fileNameXls));
                                //fileNameExcelCompletePath = _ObjCommonMethods.SaveExcel(_ObjMastersBEFileGen, fileNameTxt);
                                _ObjMastersBEFileGen = new List<MastersBE>();
                                fileNameTxt = _ObjCommonMethods.GenerateTxtEBillFile(eBillFormate, BUS_Unit);
                                _ObjCommonMethods.SendMailToBUEBillFiles(fileNameTxt, fileNameXls);
                                #region Adding values to dt column for mail log insertion
                                drEBillMailLogs = dtEBillMailLogs.NewRow();
                                drEBillMailLogs["BuName"] = BUS_Unit;
                                drEBillMailLogs["BU_TxtFile"] = fileNameTxt;
                                drEBillMailLogs["BU_XlsFile"] = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["EBillPayFiles"]) + fileNameXls + Resource.Formate_Excel;
                                drEBillMailLogs["ConsolidatedFile"] = FileName;
                                drEBillMailLogs["ConsolidatedDuplicateFile"] = DupConsolidatedFileName;
                                dtEBillMailLogs.Rows.Add(drEBillMailLogs);
                                #endregion
                                TempBU = Mas.DistName;
                                eBillFormate = string.Empty;
                                BUS_Unit = Mas.DistName;
                                eBillFormate += Mas.TrnDate.ToString(Resource.EBILL_DATE_FRMT2) + Mas.BatchNo + Mas.BatchTotal.ToString(Resource.EBILL_CURRENCY_FRMT1) + Resource.PAYDIRECT_PAYMENT + space.PadRight(33) + Mas.BatchNo;
                                eBillFormate += Environment.NewLine + Mas.AccountNo + Mas.TrnAmount.ToString(Resource.EBILL_CURRENCY_FRMT2) + Resource.EBILL_CASH_ID + space.PadRight(5) + Mas.TrnDate.ToString(Resource.EBILL_DATE_FRMT1);
                                _ObjMastersBEFileGen.Add(Mas);
                                Frmtcounter++;
                                if (itemCounter == _ObjMastersListBE.Items.Count - 1)
                                {
                                    fileNameXls = BUS_Unit + "_" + DateTime.Now.ToString(Resource.EBILL_DATE_FRMT1);
                                    WorkBook.Tables.Add(_ObjCommonMethods.SaveExcel(_ObjMastersBEFileGen, fileNameXls));
                                    //fileNameExcelCompletePath = _ObjCommonMethods.SaveExcel(_ObjMastersBEFileGen, fileNameTxt);
                                    _ObjMastersBEFileGen = new List<MastersBE>();
                                    _ObjCommonMethods.GenerateTxtEBillFile(eBillFormate, BUS_Unit);
                                    _ObjCommonMethods.SendMailToBUEBillFiles(fileNameTxt, fileNameXls);
                                    #region Adding values to dt column for mail log insertion
                                    drEBillMailLogs = dtEBillMailLogs.NewRow();
                                    drEBillMailLogs["BuName"] = BUS_Unit;
                                    drEBillMailLogs["BU_TxtFile"] = fileNameTxt;
                                    drEBillMailLogs["BU_XlsFile"] = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["EBillPayFiles"]) + fileNameXls + Resource.Formate_Excel;
                                    drEBillMailLogs["ConsolidatedFile"] = FileName;
                                    drEBillMailLogs["ConsolidatedDuplicateFile"] = DupConsolidatedFileName;
                                    dtEBillMailLogs.Rows.Add(drEBillMailLogs);
                                    #endregion
                                }
                            }
                            itemCounter++;

                        }
                        //DataSet ds = (DataSet)Session[UMS_Resource.SESSION_DATASETEBILLPAY];
                        //_ObjCommonMethods.CreateExeclWithMultiWorksheets(ds, ConfigurationManager.AppSettings["EBillPayFiles"]);
                        #region Inserting MailLogs to DB
                        _ObjMastersBAL.InsertEBillMailLogsBAL(dtEBillMailLogs);
                        #endregion
                        _ObjCommonMethods.CreateExeclWithMultiWorksheets(WorkBook, FileName);
                    }
                    else
                        returnMsg = "No records found.";

                    #endregion

                    #region Generate EBill Payment Reports For More Than One Paymets By A Customer In A Day
                    if (_ObjMastersListBE_MoreInfo != null && _ObjMastersListBE_MoreInfo.Items.Count > 0)
                    {
                        WorkBook = new DataSet();
                        TempBU = "";
                        TempBU = _ObjMastersListBE_MoreInfo.Items[0].DistName;

                        foreach (MastersBE MasterMIno in _ObjMastersListBE_MoreInfo.Items)
                        {
                            if (TempBU == MasterMIno.DistName)
                            {
                                _ObjMastersBEFileGen.Add(MasterMIno); //Generate payment for each business unit
                                TempBU = MasterMIno.DistName;
                            }
                            else
                            {
                                WorkBook.Tables.Add(_ObjCommonMethods.GenDTForConsolidateRpt(_ObjMastersBEFileGen));//Add data table for each business unit
                                _ObjMastersBEFileGen = new List<MastersBE>();
                                _ObjMastersBEFileGen.Add(MasterMIno);
                                TempBU = MasterMIno.DistName;
                            }

                        }

                        WorkBook.Tables.Add(_ObjCommonMethods.GenDTForConsolidateRpt(_ObjMastersBEFileGen));

                        _ObjCommonMethods.CreateExeclWithMultiWorksheets(WorkBook, DupConsolidatedFileName);
                    }
                    #endregion
                }
                else
                    return Resource.AUTHENTICATION_UNSUCESS;
            }
            catch
            {
                returnMsg = "Exception";
            }
            return returnMsg;
        }


        [WebMethod(Description = "Read EBill Payment and insert Text File")]
        public string ReadAndInsertEBillPayments(string key)//NEERAJ D-ID077(04-Sep-2014)
        {
            string returnValue = string.Empty;
            XmlDocument xml = null;
            try
            {
                if (_ObjCommonMethods.AuthenticateKey(key))
                {
                    #region Creating dt with columns to push all data to database in one connection for insertion.

                    DataTable DtEBillPayment = new DataTable("");
                    DataRow drEBillPayment;

                    DtEBillPayment.Columns.Add("AVR_DISTRICT_SEQ", typeof(int));
                    DtEBillPayment.Columns.Add("Trnid", typeof(string));
                    DtEBillPayment.Columns.Add("Product", typeof(string));
                    DtEBillPayment.Columns.Add("AcNo", typeof(string));
                    DtEBillPayment.Columns.Add("TrnDate", typeof(string));
                    DtEBillPayment.Columns.Add("TrnTime", typeof(string));
                    DtEBillPayment.Columns.Add("TrnAmount", typeof(decimal));
                    DtEBillPayment.Columns.Add("SourceBank", typeof(string));
                    DtEBillPayment.Columns.Add("Status", typeof(string));
                    DtEBillPayment.Columns.Add("Ind", typeof(string));
                    DtEBillPayment.Columns.Add("TransactionFee", typeof(decimal));

                    #endregion

                    EBillPayment objEBillPayment = new EBillPayment();
                    string line;
                    int count = 0;
                    string[] PaymentData = null;
                    // Read the file and display it line by line.
                    string filePath = Server.MapPath(ConfigurationManager.AppSettings["EBillPaymentDetails"].ToString());
                    System.IO.StreamReader file =
                       new System.IO.StreamReader(filePath);
                    if (Path.GetExtension(filePath) == ".txt".ToLower())
                    {
                        while ((line = file.ReadLine()) != null)
                        {
                            if (count > 0)
                            {
                                PaymentData = line.Split(',');

                                #region adding value to dt for Ebill payment insertion.

                                drEBillPayment = DtEBillPayment.NewRow();
                                drEBillPayment["AVR_DISTRICT_SEQ"] = PaymentData[0].Split('/')[0];
                                drEBillPayment["AcNo"] = PaymentData[0].Split('/')[1];
                                drEBillPayment["Trnid"] = PaymentData[1];
                                drEBillPayment["SourceBank"] = PaymentData[2];
                                drEBillPayment["Status"] = PaymentData[6];
                                drEBillPayment["Product"] = PaymentData[8];
                                drEBillPayment["TrnAmount"] = PaymentData[9];
                                drEBillPayment["TransactionFee"] = PaymentData[10];
                                DateTime dtime = Convert.ToDateTime(PaymentData[11]);
                                drEBillPayment["TrnDate"] = dtime.ToString("MM/dd/yyyy"); ;
                                drEBillPayment["TrnTime"] = dtime.ToString("hh:MM:ss");
                                //drEBillPayment["Ind"] = PaymentData[9];


                                DtEBillPayment.Rows.Add(drEBillPayment);
                                #endregion
                            }
                            count++;
                        }

                        #region Inserting Payment Info in DB
                        if (count > 1)
                        {
                            DataSet ds = new DataSet("MastersBEInfoByXml");
                            ds.Tables.Add(DtEBillPayment);
                            objEBillPayment = objIDsignBAL.DeserializeFromXml<EBillPayment>(_ObjMastersBAL.InsertEBillPaymentBAL(DtEBillPayment));
                            if (objEBillPayment.RowsEffected > 0)
                                returnValue = "File Readed And Payment Inserted.";
                            else
                                returnValue = "Payment Not Inserted.";
                        }
                        #endregion

                    }
                    else
                    {
                        returnValue = "File formate is incorrect.";
                    }
                }
                else
                {
                    returnValue = Resource.AUTHENTICATION_UNSUCESS;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return returnValue;
        }

        [WebMethod(Description = "Get Email logs to send mails to BU Managers.")]
        public string GetEBillMailLogsToSendMails(string key)//NEERAJ D-ID077(04-Sep-2014)
        {
            string returnValue = string.Empty;
            XmlDocument xml = null;
            try
            {
                if (_ObjCommonMethods.AuthenticateKey(key))
                {
                    #region Declaring DT To store value and update mail sent status
                    DataSet DsEBillMails = new DataSet("EbillPaymentsList");
                    DataTable DtEBillMails = new DataTable();
                    DtEBillMails.Columns.Add("BUEBMID", typeof(int));
                    DataRow drEBillMails = null;
                    DsEBillMails.Tables.Add(DtEBillMails);

                    #endregion

                    #region Members
                    EBillPaymentList objEBillPaymentList = new EBillPaymentList();
                    EBillPayment objEBillPayment = new EBillPayment();
                    EBillPayment objEBillPaymentBuHeadOfficeMails = new EBillPayment();

                    #endregion

                    #region Get details of Mails log and BU managers emails to send mail
                    objEBillPaymentList = objIDsignBAL.DeserializeFromXml<EBillPaymentList>(_ObjMastersBAL.GetEBillMailToSendBAL());
                    #endregion

                    #region Get mail details of BU Head Office to send mails.
                    objEBillPaymentBuHeadOfficeMails = objIDsignBAL.DeserializeFromXml<EBillPayment>(_ObjMastersBAL.GetBuHeadOfficeMailsBAL());
                    #endregion

                    if (objEBillPaymentList != null && objEBillPaymentList.items.Count > 0)
                    {
                        NetworkCredential cred = new NetworkCredential("bedctesting@gmail.com", "bedc@Testing");
                        MailMessage msg = new MailMessage();

                        #region Send mails to BU Managers
                        foreach (EBillPayment ebillPay in objEBillPaymentList.items)
                        {
                            msg.To.Clear();
                            msg.CC.Clear();
                            msg.Bcc.Clear();

                            msg.To.Add(ebillPay.TO);
                            msg.CC.Add(ebillPay.CC);
                            msg.Bcc.Add(ebillPay.BCC);
                            msg.Subject = "EBMS E-Bill Reports.";

                            msg.Attachments.Add(new Attachment(ebillPay.BU_TxtFile));
                            msg.Attachments.Add(new Attachment(ebillPay.BU_XlsFile));
                            msg.Attachments.Add(new Attachment(ebillPay.ConsolidatedFile));
                            msg.Attachments.Add(new Attachment(ebillPay.ConsolidatedDuplicateFile));

                            msg.Body = "Ebill reports attached.";
                            msg.From = new MailAddress("bedctesting@gmail.com", "BEDC E-Bill Reports"); // Your Email Id
                            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                            //SmtpClient client1 = new SmtpClient("smtp.mail.yahoo.com", 465);
                            client.Credentials = cred;
                            client.EnableSsl = true;
                            //client.Send(msg);
                            drEBillMails = DtEBillMails.NewRow();
                            drEBillMails["BUEBMID"] = ebillPay.BUEBMID.ToString();
                            DtEBillMails.Rows.Add(drEBillMails);
                        }
                        #endregion

                        #region Send mails to BU Head Office
                        if (objEBillPaymentBuHeadOfficeMails != null)
                        {
                            msg.To.Clear();
                            msg.CC.Clear();
                            msg.Bcc.Clear();

                            msg.To.Add(objEBillPaymentBuHeadOfficeMails.TO);
                            msg.CC.Add(objEBillPaymentBuHeadOfficeMails.CC);
                            msg.Bcc.Add(objEBillPaymentBuHeadOfficeMails.BCC);
                            msg.Subject = "EBMS E-Bill Consolidated Reports.";

                            msg.Attachments.Add(new Attachment(objEBillPaymentBuHeadOfficeMails.ConsolidatedFile));
                            msg.Attachments.Add(new Attachment(objEBillPaymentBuHeadOfficeMails.ConsolidatedDuplicateFile));

                            msg.Body = "Ebill consolidated reports attached.";
                            msg.From = new MailAddress("bedctesting@gmail.com", "BEDC E-Bill Consolidated Reports"); // Your Email Id
                            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                            //SmtpClient client1 = new SmtpClient("smtp.mail.yahoo.com", 465);
                            client.Credentials = cred;
                            client.EnableSsl = true;

                        }
                        #endregion

                        #region Updated status to true in mail logs table
                        objEBillPayment = objIDsignBAL.DeserializeFromXml<EBillPayment>(_ObjMastersBAL.UpdateEBillMailsToSendBAL(DtEBillMails));
                        #endregion

                        if (objEBillPayment.RowsEffected > 0)
                        {
                            returnValue = "Mail Sent To Business Units.";
                        }
                        else
                        {
                            returnValue = "Mail Not Sent.";
                        }
                    }

                }
                else
                {
                    returnValue = Resource.AUTHENTICATION_UNSUCESS;
                }
            }
            catch
            {
                returnValue = "Exception";
            }
            return returnValue;
        }

        [WebMethod(Description = "To get the Bill Generation Service Start Details if exists ")]
        public DataSet GenerateBill_ByBillQueueDetailsV1(string cycleId, string billingQueueScheduleId)//Neeraj Kanojiya (13-Feb-2014)
        {
            try
            {
                //XmlDocument xml = new XmlDocument();
                BillGenerationBe _objBillGenerationBe = new BillGenerationBe();
                BillGenerationListBe _objBillGenerationListBe = new BillGenerationListBe();
                _objBillGenerationBe.CycleId = cycleId;
                _objBillGenerationBe.BillingQueueScheduleIdList = billingQueueScheduleId;
                //return _objBillGenerationBal.GenerateCustomersBills_GetDetails(_objBillGenerationBe);//Neeraj Code
                return _objBillGenerationBal.GenerateCustomersBills_GetDetails_Satya(_objBillGenerationBe);//Satya
                //xml = _objBillGenerationBal.GenerateCustomersBills(_objBillGenerationBe);

                //iDsignBAL _objiDsignBAL = new iDsignBAL();
                //_objBillGenerationListBe = _objiDsignBAL.DeserializeFromXml<BillGenerationListBe>(xml);
                ////return _objiDsignBAL.ConvertListToDataSet<BillGenerationBe>(_objBillGenerationListBe.Items);
                //return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Autocomplete
        [WebMethod(Description = "Get autocomplete list.", EnableSession = true)]//Neeraj/ID077
        public List<string> GetAutocompleteList(string SearchValue, int FilterType)
        {
            List<string> list = null;
            if (SearchValue.Length > 2)
            {
                XmlDocument xmldoc = new XmlDocument();
                iDsignBAL _objiDsignBAL = new iDsignBAL();

                AutoCompleteBE objAutoCompleteBE = new AutoCompleteBE();
                AutoCompleteListBE objAutoCompleteListBE = new AutoCompleteListBE();
                MastersBAL objMastersBAL = new MastersBAL();

                objAutoCompleteBE.FilterType = FilterType;
                objAutoCompleteBE.SearchValue = SearchValue;
                if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                    objAutoCompleteBE.BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                else
                    objAutoCompleteBE.BUID = string.Empty;
                xmldoc = objMastersBAL.AutoCompleteListBAL(_objiDsignBAL.DoSerialize<AutoCompleteBE>(objAutoCompleteBE));
                objAutoCompleteListBE = _objiDsignBAL.DeserializeFromXml<AutoCompleteListBE>(xmldoc);
                if (objAutoCompleteListBE != null)
                {
                    DataSet ds = new DataSet();
                    if (objAutoCompleteListBE.items.Count > 0)
                    {
                        ds = _objiDsignBAL.ConvertListToDataSet<AutoCompleteBE>(objAutoCompleteListBE.items);
                    }
                    list = (from x in ds.Tables[0].AsEnumerable() select x.Field<string>("ListValue")).ToList();
                }

            } return list;
        }

        [WebMethod(EnableSession = true)]
        public int SessionExtending(string UserName, string Password, string BU_ID, string LoginId)
        {
            if (LoginId == UserName)
            {
                XmlDocument xml = new XmlDocument();
                LoginPageBE objLoginPageBE = new LoginPageBE();
                objLoginPageBE.UserName = UserName;
                objLoginPageBE.Password = objIDsignBAL.AESEncryptString(Password);
                xml = _ObjLoginPageBAL.Login(objLoginPageBE, ReturnType.Get);
                //xml = _ObjUMS_Service.Login(this.UserName, this.Password);
                objLoginPageBE = objIDsignBAL.DeserializeFromXml<LoginPageBE>(xml);

                if (objLoginPageBE.IsSuccess)
                {
                    Session[UMS_Resource.SESSION_UserName] = objLoginPageBE.UserName;
                    Session[UMS_Resource.SESSION_LOGINID] = UserName;
                    Session[UMS_Resource.SESSION_ROLEID] = objLoginPageBE.RoleId;
                    Session[UMS_Resource.SESSION_DESIGNATION] = objLoginPageBE.Designation;
                    Session[UMS_Resource.SESSION_USER_EMAILID] = objLoginPageBE.PrimaryEmailId;
                    Session[UMS_Resource.SESSION_USER_BUID] = BU_ID.ToString();
                    //return true;
                    return 1;
                }
                else
                {
                    //mpeReLoginpopup.Show();                        
                    //mpeReLoginWarning.Show();
                    //return false;
                    return 2;
                }
            }
            else
            {
                //mpeReLoginpopup.Show();                    
                //mpeReLoginWarning.Show();
                //return false;
                return 3;
            }

        }
        #endregion
    }
}
