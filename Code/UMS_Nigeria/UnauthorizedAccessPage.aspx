﻿<%@ Page Language="C#" AutoEventWireup="true" Title="::Unauthorized Access::" CodeBehind="UnauthorizedAccessPage.aspx.cs" Inherits="UMS_Nigeria.UnauthorizedAccessPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Styles/InlineStyles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
    
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="restricted">
    <div class="restricted-head"><p style="float:left;"> RESTRICTED AREA </p> <img src="images/Restricted.png" /></div>

   <p class="restricted-text">Sorry!!! &nbsp You are not able to access this page.</p>
   <asp:HyperLink ID="hlnkHome" CssClass="chooslink" runat="server" Text="<%$ Resources:Resource, GO_TO_HOME%>" NavigateUrl="~/Home.aspx"></asp:HyperLink>
    </div>
    </form>
</body>
</html>
