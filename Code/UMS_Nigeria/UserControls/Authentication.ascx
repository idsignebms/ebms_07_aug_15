﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Authentication.ascx.cs"
    Inherits="UMS_Nigeria.UserControls.Authentication" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<script type="text/javascript">
    function pageLoad() {
        $get("<%=txtPasswordLogin.ClientID%>").focus();
    };
    function passwordLength() {
        var returnValue = false;
        var txtPasswordLogin = $get("<%=txtPasswordLogin.ClientID%>").value
        if (txtPasswordLogin.length < 6) {
            document.getElementById("<%=lblValidation.ClientID%>").innerHTML = "Password cannot be less than 6 characters.";
            var popup = $find('<%= mpeValidation.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }
        else
            returnValue = true;
        return returnValue;
    };
    function focusPassword() {
        $get("<%=txtPasswordLogin.ClientID%>").focus();
    }
</script>
<asp:Button ID="btnLogin" runat="server" Text="Button" Style="display: none;" />
<asp:ModalPopupExtender ID="ModalPopupExtenderLogin" runat="server" PopupControlID="PanelLogin"
    TargetControlID="btnLogin" BackgroundCssClass="modalPopup" CancelControlID="ButtonL">
</asp:ModalPopupExtender>
<asp:Panel ID="PanelLogin" DefaultButton="btnAuthentication" runat="server" CssClass="modalPopup"
    Style="display: none; border-color: #639B00;">
    <asp:Panel ID="pnlMessage" runat="server">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </asp:Panel>
    <div class="popheader" style="background-color: #639B00;">
        <asp:Label ID="Label1" runat="server" Text="Authentication Required"></asp:Label>
        <%-- <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, INVALIDACCOUNTNUMBER_HEADER%>"></asp:Literal>--%>
    </div>
    <div class="body" style="padding: 30px;">
        <asp:Label ID="Label2" runat="server" Text="Retype Your Password"></asp:Label>
        <asp:TextBox ID="txtPasswordLogin" TextMode="Password" AutoComplete="off" runat="server"
            placeholder="Enter Password" MaxLength="8" onblur="return TextBoxBlurValidationlbl(this,'spanAuthentication','Password')"></asp:TextBox>
        <span id="spanAuthentication" runat="server" class="spancolor"></span>
        <br />
        <asp:Label ID="LabelStatusLogin" runat="server" class="spancolor" Visible="false"></asp:Label>
        <%--  <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, INVALIDACCOUNTNUMBER_BODY%>"></asp:Literal>--%>
    </div>
    <div class="footer" align="right">
        <div class="fltl Nothanks">
            <a href="../Home.aspx">No Thanks, Go To Home</a></div>
        <div class="fltl" style="margin-top: 5px;">
            <asp:Button ID="btnAuthentication" runat="server" Text="<%$ Resources:UMS_Resource, CONTINUE%>"
                CssClass="continue" OnClick="btnAuthenticarion_Click" OnClientClick="return passwordLength();" />
            <asp:Button ID="ButtonL" runat="server" Text="<%$ Resources:Resource, CANCEL%>" CssClass="nobt" />
            <%--<asp:Button ID="ButtonCancelLogin" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                            CssClass="no" OnClientClick="window.location.assign('../Home.aspx')" />--%>
        </div>
        <div class="clear pad_5">
            <asp:HiddenField ID="hfFromPage" runat="server" />
        </div>
    </div>
</asp:Panel>
<%-- Comman Popup--%>
<%--Neeraj-ID077--%>
<asp:ModalPopupExtender ID="mpeCommanMsg" runat="server" PopupControlID="pnlCommanMsg"
    TargetControlID="hfCloseMpeFA" BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>
<asp:HiddenField ID="hfCloseMpeFA" runat="server" />
<asp:Panel ID="pnlCommanMsg" runat="server" CssClass="modalPopup" Style="display: none;">
    <div class="popheader popheaderlblred">
        <asp:Label ID="lblCommanMsg" runat="server"></asp:Label>
    </div>
    <div class="footer popfooterbtnrt">
        <div class="fltr popfooterbtn">
            <asp:Button ID="btnCloseMpeCM" runat="server" Text="<%$ Resources:Resource, OK%>"
                CssClass="ok_btn" OnClick="btnCloseMpeCM_Click" />
        </div>
        <div class="clear pad_5">
        </div>
    </div>
</asp:Panel>
<%-- Comman Popup--%>
<%--Neeraj-ID077--%>
<asp:ModalPopupExtender ID="mpeValidation" runat="server" PopupControlID="pnlValidation"
    TargetControlID="hfCloseMpeVal" BackgroundCssClass="modalBackground" CancelControlID="btnCancleId">
</asp:ModalPopupExtender>
<asp:HiddenField ID="hfCloseMpeVal" runat="server" />
<asp:Panel ID="pnlValidation" runat="server" CssClass="modalPopup" Style="display: none;">
    <div class="popheader popheaderlblred">
        <asp:Label ID="lblValidation" runat="server"></asp:Label>
    </div>
    <div class="footer popfooterbtnrt">
        <div class="fltr popfooterbtn">
            <asp:Button ID="btnCancleId" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="ok_btn" OnClientClick="focusPassword();" />
        </div>
        <div class="clear pad_5">
        </div>
    </div>
</asp:Panel>
<%-- Comman Popup--%>
