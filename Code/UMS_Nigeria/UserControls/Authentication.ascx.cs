﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using iDSignHelper;
using Resources;
using UMS_NigriaDAL;
using System.Collections;

namespace UMS_Nigeria.UserControls
{
    public partial class Authentication : System.Web.UI.UserControl
    {

        #region Members
        BillingBE _objBilling = new BillingBE();
        BillReadingsBAL _objBillingBAL = new BillReadingsBAL();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommonMethods _ObjCommonMethods = new CommonMethods();
        CommonDAL _ObjCommonDAL = new CommonDAL();
        public delegate void Authntication_Click(object sender, EventArgs e);
        public event Authntication_Click ChangeTariffAuthentication;
        #endregion

        #region Events


        protected void Page_Load(object sender, EventArgs e)
        {
            Hashtable htableMaxLength = new Hashtable();
            htableMaxLength = _ObjCommonMethods.getApplicationSettings(Resource.AUTHENTICATION);
            if (htableMaxLength.Count > 0 && htableMaxLength["IsAuthenticate"].ToString().ToLower().Contains("true"))
            {
                if (!IsPostBack && Session[UMS_Resource.SESSION_LOGINID] != null)
                {
                    txtPasswordLogin.Focus();
                    ModalPopupExtenderLogin.Show();
                }
                if (Session[UMS_Resource.FROM_PAGE] != null)
                {
                    hfFromPage.Value = Session[UMS_Resource.FROM_PAGE].ToString();
                }
            }
        }

        //Check the user password correct or not.
        protected void btnAuthenticarion_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPasswordLogin.Text != "")
                {
                    XmlDocument xml = new XmlDocument();
                    LoginPageBE objLoginPageBE = new LoginPageBE();
                    LoginPageBAL objLoginPageBAL = new LoginPageBAL();
                    objLoginPageBE.UserName = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objLoginPageBE.Password = _ObjiDsignBAL.AESEncryptString(txtPasswordLogin.Text);
                    xml = objLoginPageBAL.Login(objLoginPageBE, ReturnType.Get);
                    objLoginPageBE = _ObjiDsignBAL.DeserializeFromXml<LoginPageBE>(xml);
                    CommonMethods _objCommonMethods = new CommonMethods();

                    if (objLoginPageBE.IsSuccess)
                    {
                        Response.Cookies["objHttpCookieLogAtmpt"].Expires = DateTime.Now.AddDays(-1);//Neeraj-ID077
                        LabelStatusLogin.Visible = false;

                        //if (objLoginPageBE.RoleId != 1)// Commented by Suresh, y Because here only chekcing if conditon bt statements are commentes and at the same time they are not taking any else part also, so no use with this condition
                        //{
                        //    //ddlMeterReader.ClearSelection();
                        //    //ddlMeterReader.Items.FindByText(objLoginPageBE.UserId).Selected = true;
                        //    //ddlMeterReader.Enabled = false;
                        //}
                        ModalPopupExtenderLogin.Hide();

                        switch (hfFromPage.Value)
                        {
                            case Constants.TariffChangeConfirm:
                                ChangeTariffAuthentication(sender, e);
                                break;
                        }


                    }
                    else//Neeraj-ID077
                    {
                        int LoginAttempt = GetLoginAttempt(Session[UMS_Resource.SESSION_LOGINID].ToString());
                        if (LoginAttempt < Constants.MaxLoginAttempts)
                        {
                            LabelStatusLogin.Visible = true;
                            LabelStatusLogin.Text = string.Format(Resource.LOGIN_ATMPT_WRN_MSG, LoginAttempt);
                            txtPasswordLogin.Focus();
                            ModalPopupExtenderLogin.Show();
                        }
                        else
                        {
                            Session.Abandon();
                            _objCommonMethods.DisableUser(Session[UMS_Resource.SESSION_LOGINID].ToString());
                            Response.Cookies["objHttpCookieLogAtmpt"].Expires = DateTime.Now.AddDays(-1);
                            //txtPassword.Enabled = false;
                            lblCommanMsg.Text = Resource.NO_ATMPT;
                            mpeCommanMsg.Show();
                        }
                        //txtPassword.Focus();
                        //LabelStatusLogin.Visible = true;
                        //LabelStatusLogin.Text = Resource.AUTHENTICATIONFAILED;
                        //ModalPopupExtenderLogin.Show();
                    }
                }
                else
                {
                    LabelStatusLogin.Visible = true;
                    LabelStatusLogin.Text = "Enter Password";
                    ModalPopupExtenderLogin.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    // _ObjCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnCloseMpeCM_Click(object sender, EventArgs e)
        {
            Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }
        #endregion

        #region Method
        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void AddLoginAttempt(string UserId)//Neeraj-ID077
        {
            HttpCookie objHttpCookie = Request.Cookies[UMS_Resource.LOGIN_ATMPT_COOKIE];

            if (objHttpCookie != null)
            {
                string[] LoginDetails = new string[2];
                LoginDetails[0] = objHttpCookie[Resources.UMS_Resource.USERID];
                LoginDetails[1] = objHttpCookie[UMS_Resource.LOGIN_ATMPT];
                if (LoginDetails[0] == UserId)
                {
                    objHttpCookie[UMS_Resource.LOGIN_ATMPT] = (Convert.ToInt16(LoginDetails[1]) + 1).ToString();
                    Response.Cookies.Add(objHttpCookie);
                }
            }

            else
            {
                objHttpCookie = new HttpCookie(UMS_Resource.LOGIN_ATMPT_COOKIE);
                objHttpCookie[UMS_Resource.USERID] = UserId;
                objHttpCookie[UMS_Resource.LOGIN_ATMPT] = "1";
                Response.Cookies.Add(objHttpCookie);
            }
        }

        private int GetLoginAttempt(string UserId)//Neeraj-ID077
        {
            AddLoginAttempt(Session[UMS_Resource.SESSION_LOGINID].ToString());
            HttpCookie objHttpCookie = Request.Cookies[UMS_Resource.LOGIN_ATMPT_COOKIE];
            string[] LoginDetails = new string[2];
            int LoginAttempt = 0;
            if (objHttpCookie != null)
            {
                LoginDetails[0] = objHttpCookie[Resources.UMS_Resource.USERID];
                LoginDetails[1] = objHttpCookie[UMS_Resource.LOGIN_ATMPT];
                if (LoginDetails[0] == UserId)
                {
                    LoginAttempt = Convert.ToInt16(LoginDetails[1]);
                }
            }
            return LoginAttempt;
        }
        #endregion
    }
}