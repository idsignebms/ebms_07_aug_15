﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DynamicControls.ascx.cs"
    Inherits="UMS_Nigeria.UserControls.DynamicControls" %>
<div id="divDynamic" runat="server">
    <%
        dscontrols = _objApprovalBal.GetDetails(UMS_NigriaDAL.ReturnType.Bulk);
        if (dscontrols.Tables[0].Rows.Count > 0)
        {
            foreach (System.Data.DataRow dr in dscontrols.Tables[0].Rows)
            {
                switch (dr["ControlTypeName"].ToString())
                {
                    case "TextBox":
                                %>
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <%= dr["FieldName"]%></label><span class="span_star">*</span>
                                        <br />
                                        <input type="text" id="<%= "txt"+dr["FieldName"] %>" class="text-box" placeholder ="<%= "Enter "+dr["FieldName"] %>" onblur="return TextBoxBlurValidationlbl(this,'<%= "spantxt"+dr["FieldName"] %>','<%=dr["FieldName"] %>')" />
                                        <div class="space">
                                        </div>
                                        <span id="<%= "spantxt"+dr["FieldName"] %>" class="span_color"></span>
                                    </div>
                                </div>
                                <%
                                break;
                    case "DropDownList":
                                    %>
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <label for="name">
                                                <%= dr["FieldName"]%></label><span class="span_star">*</span><br />
                                            <select id="<%= "ddl"+dr["FieldName"] %>" class="text-box text-select" onchange="DropDownlistOnChangelbl(this,'<%= "spanddl"+dr["FieldName"] %>',' <%= dr["FieldName"] %>')">
                                                <option value="0">--select--</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                            <div class="space">
                                            </div>
                                            <span id="<%= "spanddl"+dr["FieldName"] %>" class="span_color"></span>
                                        </div>
                                    </div>
                                    <%

                                break;
                            }
                        }
                    }%>
</div>
<script type="text/C#" runat="server">
    System.Data.DataSet dscontrols = new System.Data.DataSet();
    UMS_NigeriaBAL.ApprovalBal _objApprovalBal = new UMS_NigeriaBAL.ApprovalBal();
    UMS_Nigeria.CommonMethods objCommonMethods = new UMS_Nigeria.CommonMethods();
</script>