﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;

namespace UMS_Nigeria.UserControls
{
    public partial class DynamicControls : System.Web.UI.UserControl
    {
        #region Members

        CommonMethods objCommonMethods = new CommonMethods();
        iDsignBAL objIdsignBal = new iDsignBAL();
        ApprovalBal _objApprovalBal = new ApprovalBal();


        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }
        #endregion

        #region Method
        public void BindDynamicddl()
        {
            DataSet dscontrols = new DataSet();
            dscontrols = _objApprovalBal.GetDetails(ReturnType.Bulk);

            ControlCollection Pagecontrols = divDynamic.Controls;

            for (int i = 0; i < Pagecontrols.Count; i++)
            {
                foreach (DataRow dr in dscontrols.Tables[0].Rows)
                {
                    if (Pagecontrols[i].ID == "ddl" + dr["ControlTypeName"].ToString())
                    {
                        //Control cn = Pagecontrols[i].ID;
                        objCommonMethods.BindAccountTypes(Pagecontrols[i], true);
                    }
                }
            }
        }
        #endregion
    }
}