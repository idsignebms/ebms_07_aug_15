﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCPagingV1.ascx.cs" Inherits="UMS_Nigeria.UserControls.UCPagingV1" %>

    <div id="divpaging" runat="server">
        <div align="right" runat="server" id="divLinks">
            <%--                                <h3 class="gridhead" align="left">
                                    <asp:Literal ID="litBuList" runat="server" Text="Customer with no fixed charges."></asp:Literal>
                                </h3>--%>
            <asp:LinkButton ID="lkbtnFirst" runat="server" OnClick="lkbtnFirst_Click">
                <asp:Literal ID="litFirst" runat="server" Text="<%$ Resources:Resource, FIRST%>"></asp:Literal></asp:LinkButton>
            &nbsp;|
            <asp:LinkButton ID="lkbtnPrevious" runat="server" OnClick="lkbtnPrevious_Click">
                <asp:Literal ID="litPrevious" runat="server" Text="<%$ Resources:Resource, PREVIOUS%>"></asp:Literal></asp:LinkButton>
            &nbsp;|
            <asp:Label ID="lblPage" runat="server" Text="Page"></asp:Label>
            <asp:Label ID="lblCurrentPage" runat="server" Text="1" Visible="false"></asp:Label>
            &nbsp;
            <asp:DropDownList ID="ddlGoPage" runat="server" OnSelectedIndexChanged="ddlGoPage_SelectedIndexChanged"
                AutoPostBack="True">
            </asp:DropDownList>
            &nbsp;
            <asp:Label ID="lblOff" runat="server" Text="Of"></asp:Label>
            &nbsp;
            <asp:Label ID="lblTotalPages" runat="server" Text="1"></asp:Label>
            &nbsp;|
            <asp:LinkButton ID="lkbtnNext" runat="server"  OnClick="lkbtnNext_Click">
                <asp:Literal ID="litNext" runat="server" Text="<%$ Resources:Resource, NEXT%>"></asp:Literal></asp:LinkButton>
            &nbsp;|
            <asp:LinkButton ID="lkbtnLast" runat="server" OnClick="lkbtnLast_Click">
                <asp:Literal ID="litLast" runat="server" Text="<%$ Resources:Resource, LAST%>"></asp:Literal></asp:LinkButton>
            &nbsp;|&nbsp;<asp:Literal ID="litPageSize" runat="server" Text="<%$ Resources:Resource, PAGE_SIZE%>"></asp:Literal>
            <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" 
                OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
</div>