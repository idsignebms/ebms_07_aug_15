﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iDSignHelper;
using UMS_NigeriaBE;
using System.Data;
using Resources;

namespace UMS_Nigeria.UserControls
{
    public partial class UCPagingV2 : System.Web.UI.UserControl
    {
        #region Member
        CommonMethods _objCommonMethods = new CommonMethods();
        iDsignBAL _objiDsignBal = new iDsignBAL();
        string Key = "UCPaging";
        string FunctionName = "BindGrid";
        #endregion

        #region Properties
        public int PageNum;

        public int _goPageNo
        {
            get { return string.IsNullOrEmpty(ddlGoPage.SelectedValue) ? Constants.PageSizeIncrementReport : Convert.ToInt32(ddlGoPage.SelectedValue); }
        }

        public int _pageSize
        {
            get { return string.IsNullOrEmpty(((HiddenField)Parent.FindControl("hfPageSize")).Value) ? Constants.PageSizeStartsReport : Convert.ToInt32(((HiddenField)Parent.FindControl("hfPageSize")).Value); }
            set { ((HiddenField)Parent.FindControl("hfPageSize")).Value = Convert.ToString(value); }
        }

        public string _pageNo
        {
            get { return ((HiddenField)Parent.FindControl("hfPageNo")).Value; }
            set { ((HiddenField)Parent.FindControl("hfPageNo")).Value = Convert.ToString(value); }
        }

        public string _lastPage
        {
            get { return ((HiddenField)Parent.FindControl("hfLastPage")).Value; }
            set { ((HiddenField)Parent.FindControl("hfLastPage")).Value = Convert.ToString(value); }
        }

        public string _totalRecords
        {
            get { return ((HiddenField)Parent.FindControl("hfTotalRecords")).Value; }
            set { ((HiddenField)Parent.FindControl("hfTotalRecords")).Value = Convert.ToString(value); }
        }

        //public string _pageSize
        //{
        //    get { return string.IsNullOrEmpty(((HiddenField)Parent.FindControl("hfPageSize")).Value) ? Constants.PageSizeStarts.ToString() : ((HiddenField)Parent.FindControl("hfPageSize")).Value; }
        //    set { ((HiddenField)Parent.FindControl("hfPageSize")).Value = Convert.ToString(value); }
        //}

        #endregion

        #region Methods

        public void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                //if (TotalNoOfRecs == 0)
                //    TotalNoOfRecs++;
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / _pageSize));

                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(_pageNo);
                lkbtnLast.CommandArgument = _lastPage = lblTotalPages.Text = totalpages.ToString();

                _objiDsignBal.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);

                lblCurrentPage.Text = _pageNo;

                if (totalpages == 1)
                {
                    lkbtnFirst.ForeColor
                   = lkbtnLast.ForeColor
                   = lkbtnNext.ForeColor
                   = lkbtnPrevious.ForeColor = System.Drawing.Color.Gray;
                    ddlGoPage.Enabled = false;

                }
                else
                {
                    ddlGoPage.Enabled = true;
                }
                if (string.Compare(_pageNo, _lastPage, true) == 0)
                {
                    lkbtnNext.Enabled = lkbtnLast.Enabled = false;
                }
                else
                {
                    lkbtnNext.Enabled
                    = lkbtnLast.Enabled = true;
                    lkbtnFirst.ForeColor
                   = lkbtnLast.ForeColor
                   = lkbtnNext.ForeColor
                   = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5");
                }

                BindPageCount(Convert.ToInt32(totalpages));

                BindPagingDropDown(TotalNoOfRecs);


                //UCPaging paging2 = (UCPaging)Parent.FindControl("UCPaging2");
                //paging2.ddlGoPage.SelectedIndex = ddlGoPage.SelectedIndex;

                UpdateInstance(Convert.ToInt32(totalpages), currentpage);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                _objCommonMethods.BindPagingDropDownListReport(TotalRecords, Convert.ToInt32(_pageSize), ddlPageSize);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        public void BindPageCount(int PageCount)
        {
            ddlGoPage.Items.Clear();
            for (int i = 1; i <= PageCount; i++)
            {
                ListItem list = new ListItem(i.ToString(), i.ToString());
                ddlGoPage.Items.Add(list);
            }
            ddlGoPage.Items.FindByText(_pageNo).Selected = true;
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                // _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void InvokeBaseFunction()
        {
            this.Page.GetType().InvokeMember(FunctionName, System.Reflection.BindingFlags.InvokeMethod, null, this.Page, new object[] { });
        }

        private void UpdateInstance(int PageCount, int Currentpage)
        {
            UCPagingV2 paging1 = (UCPagingV2)Parent.FindControl("UCPaging1");
            UCPagingV2 paging2 = (UCPagingV2)Parent.FindControl("UCPaging2");
            if (paging2 != null)
            {

                paging2.ddlGoPage.Enabled = ddlGoPage.Enabled;
                paging2.ddlPageSize.Enabled = ddlPageSize.Enabled;

                paging2.lblCurrentPage.Text = lblCurrentPage.Text;
                paging2.lblTotalPages.Text = lblTotalPages.Text;
                paging2.litFirst.Text = litFirst.Text;
                paging2.litLast.Text = litLast.Text;
                paging2.litNext.Text = litNext.Text;
                paging2.litPageSize.Text = litPageSize.Text;
                paging2.litPrevious.Text = litPrevious.Text;

                paging2.lkbtnLast.ForeColor = lkbtnLast.ForeColor;
                paging2.lkbtnFirst.ForeColor = lkbtnFirst.ForeColor;

                paging2.lkbtnPrevious.ForeColor = lkbtnPrevious.ForeColor;
                paging2.lkbtnNext.ForeColor = lkbtnLast.ForeColor;

                paging2.lkbtnPrevious.Enabled = lkbtnPrevious.Enabled;
                paging2.lkbtnNext.Enabled = lkbtnLast.Enabled;

                paging2.lkbtnLast.Enabled = lkbtnLast.Enabled;
                paging2.lkbtnFirst.Enabled = lkbtnFirst.Enabled;

                paging2.ddlGoPage.Items.Clear();
                for (int i = 1; i <= PageCount; i++)
                {
                    ListItem itm = new ListItem(i.ToString(), i.ToString());
                    paging2.ddlGoPage.Items.Add(itm);
                }
                paging2.ddlGoPage.SelectedIndex = Currentpage - 1;
                paging2.ddlPageSize.SelectedIndex = ddlPageSize.SelectedIndex;
            }
            if (paging1 != null)
            {
                paging1.ddlGoPage.Enabled = ddlGoPage.Enabled;
                paging1.ddlPageSize.Enabled = ddlPageSize.Enabled;

                paging1.lblCurrentPage.Text = lblCurrentPage.Text;
                paging1.lblTotalPages.Text = lblTotalPages.Text;
                paging1.litFirst.Text = litFirst.Text;
                paging1.litLast.Text = litLast.Text;
                paging1.litNext.Text = litNext.Text;
                paging1.litPageSize.Text = litPageSize.Text;
                paging1.litPrevious.Text = litPrevious.Text;

                paging1.lkbtnLast.ForeColor = lkbtnLast.ForeColor;
                paging1.lkbtnFirst.ForeColor = lkbtnFirst.ForeColor;

                paging1.lkbtnPrevious.ForeColor = lkbtnPrevious.ForeColor;
                paging1.lkbtnNext.ForeColor = lkbtnLast.ForeColor;

                paging1.lkbtnPrevious.Enabled = lkbtnPrevious.Enabled;
                paging1.lkbtnNext.Enabled = lkbtnLast.Enabled;

                paging1.lkbtnLast.Enabled = lkbtnLast.Enabled;
                paging1.lkbtnFirst.Enabled = lkbtnFirst.Enabled;

                paging1.ddlGoPage.Items.Clear();
                for (int i = 1; i <= PageCount; i++)
                {
                    ListItem itm = new ListItem(i.ToString(), i.ToString());
                    paging1.ddlGoPage.Items.Add(itm);
                }
                paging1.ddlGoPage.SelectedIndex = Currentpage - 1;
                paging1.ddlPageSize.SelectedIndex = ddlPageSize.SelectedIndex;
            }
        }
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GeneratePaging();
            }
        }

        private void GeneratePaging()
        {
            CreatePagingDT(Convert.ToInt32(_totalRecords));
            BindPagingDropDown(Convert.ToInt32(_totalRecords));
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _pageNo = Constants.pageNoValue.ToString();
                _pageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                InvokeBaseFunction();
                CreatePagingDT(Convert.ToInt32(_totalRecords));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                _pageNo = Constants.pageNoValue.ToString();
                InvokeBaseFunction();
                CreatePagingDT(Convert.ToInt32(_totalRecords));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                _pageNo = PageNum.ToString();

                InvokeBaseFunction();
                CreatePagingDT(Convert.ToInt32(_totalRecords));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                _pageNo = PageNum.ToString();

                InvokeBaseFunction();

                CreatePagingDT(Convert.ToInt32(_totalRecords));




            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                _pageNo = _lastPage;
                InvokeBaseFunction();
                CreatePagingDT(Convert.ToInt32(_totalRecords));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlGoPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                _pageNo = _goPageNo.ToString();
                InvokeBaseFunction();
                CreatePagingDT(Convert.ToInt32(_totalRecords));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Paging



        #endregion
    }
}