﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_AdditionalChargesLastTransaction.ascx.cs"
    Inherits="UMS_Nigeria.UserControls.UC_AdditionalChargesLastTransaction" %>
<asp:GridView ID="grdAdjustmentTransactions" runat="server" AutoGenerateColumns="False"
    HeaderStyle-CssClass="grid_tb_hd" Width="650px" OnRowDataBound="grdAdjustmentTransactions_RowDataBound">
    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" BorderStyle="Outset" />
    <EmptyDataTemplate>
        There is no data.
    </EmptyDataTemplate>
    <Columns>
        <asp:TemplateField HeaderText="S.No" ItemStyle-CssClass="grid_tb_td" ItemStyle-Width="5%"
            ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
                <%#Container.DisplayIndex+1 %>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField HeaderText="BatchNo" DataField="BatchNo" ItemStyle-CssClass="grid_tb_td"
            ItemStyle-Width="8%" ItemStyle-HorizontalAlign="Center" />
        <asp:BoundField HeaderText="Adjustment Type" DataField="AdjustmentName" ItemStyle-CssClass="grid_tb_td"
            ItemStyle-Width="27%" ItemStyle-HorizontalAlign="Left" />
        <asp:TemplateField HeaderText="Total Amount Adjusted" ItemStyle-HorizontalAlign="Right"
            ItemStyle-Width="18%">
            <ItemTemplate>
                <asp:Label ID="lblAmountEffected" runat="server" Text='<%#Eval("AmountEffected") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Total Amount Adjusted With Tax" ItemStyle-HorizontalAlign="Right"
            ItemStyle-Width="18%">
            <ItemTemplate>
                <asp:Label ID="lblAmountEffectedWithTax" runat="server" Text='<%#Eval("TotalAmountEffected") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField HeaderText="Units Adjusted" DataField="AdjustedUnits" ItemStyle-CssClass="grid_tb_td"
            ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField HeaderText="Reason" DataField="Remarks" ItemStyle-CssClass="grid_tb_td"
            ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" />
        <asp:TemplateField HeaderText="Adjustment Date" ItemStyle-Width="14%">
            <ItemTemplate>
                <asp:Label ID="lblCreatedDate" runat="server" Text='<%#Eval("CreatedDate") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
