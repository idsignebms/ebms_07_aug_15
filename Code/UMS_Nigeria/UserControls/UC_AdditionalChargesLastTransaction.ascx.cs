﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using UMS_NigeriaBAL;
using System.Xml;
using iDSignHelper;
using System.Data;

namespace UMS_Nigeria.UserControls
{
    public partial class UC_AdditionalChargesLastTransaction : System.Web.UI.UserControl
    {
        #region Members
        BillAdjustmentListBe objBillAdjustmentLisBeDetails = new BillAdjustmentListBe();
        BillAdjustmentsBe objBillAdjustmentsBe = new BillAdjustmentsBe();
        BillAdjustmentBal objBillAdjustmentBal = new BillAdjustmentBal();
        CommonMethods _objCommonMethods = new CommonMethods(); 
        XmlDocument xml = new XmlDocument();
        iDsignBAL objiDsignBAL = new iDsignBAL();
        #endregion

        #region Properties
        public string AccountNo { get; set; }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        #endregion

        #region Methods
        public void GetAdjustmentLastTransactions()
        {
            grdAdjustmentTransactions.DataSource = new DataTable();
            grdAdjustmentTransactions.DataBind();

            objBillAdjustmentsBe.AccountNo = AccountNo;
            xml = objBillAdjustmentBal.GetAdjustmentLastTransactions(objBillAdjustmentsBe);
            if (xml != null)
            {
                objBillAdjustmentLisBeDetails = objiDsignBAL.DeserializeFromXml<BillAdjustmentListBe>(xml);
                if (objBillAdjustmentLisBeDetails.items.Count > 0)
                {
                    grdAdjustmentTransactions.DataSource = objBillAdjustmentLisBeDetails.items;
                    grdAdjustmentTransactions.DataBind();
                }
            }

        }

        protected void grdAdjustmentTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string AdjustmentDate = _objCommonMethods.DateFormate(((Label)(e.Row.FindControl("lblCreatedDate"))).Text);
                ((Label)(e.Row.FindControl("lblCreatedDate"))).Text = AdjustmentDate;
                decimal lblAmountEffected = Convert.ToDecimal(((Label)(e.Row.FindControl("lblAmountEffected"))).Text);
                ((Label)(e.Row.FindControl("lblAmountEffected"))).Text = _objCommonMethods.GetCurrencyFormat(lblAmountEffected).ToString();
                decimal lblAmountEffectedWithTax = Convert.ToDecimal(((Label)(e.Row.FindControl("lblAmountEffectedWithTax"))).Text);
                ((Label)(e.Row.FindControl("lblAmountEffectedWithTax"))).Text = _objCommonMethods.GetCurrencyFormat(lblAmountEffectedWithTax).ToString();
            }
        }
        
        #endregion
    }
}