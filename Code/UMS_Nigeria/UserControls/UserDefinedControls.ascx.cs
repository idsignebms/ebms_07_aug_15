﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using UMS_NigeriaBE;
using System.Web.UI.HtmlControls;
using System.Drawing;
namespace UMS_Nigeria.UserControls
{
    public partial class UserDefinedControls : System.Web.UI.UserControl
    {
        #region Members

        CommonMethods objCommonMethods = new CommonMethods();
        iDsignBAL objIdsignBal = new iDsignBAL();
        MastersBAL objMastersBAL = new MastersBAL();
        UserDefinedControlsListBE_1 objUserDefinedControlsListBE_1 = new UserDefinedControlsListBE_1();
        UserDefinedControlsListBE_2 objUserDefinedControlsListBE_2 = new UserDefinedControlsListBE_2();
        MastersListBE objMastersListBE = new MastersListBE();
        string key = "UserDefinedControls";
        #endregion

        #region Events
        protected override void OnInit(EventArgs e)
        {
            //base.OnInit(e);

            BindUserDefinedControls();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //BindUserDefinedControls();
            }
        }
        #endregion

        #region Method
        public void BindUserDefinedControls()
        {
            objUserDefinedControlsListBE_1 = objIdsignBal.DeserializeFromXml<UserDefinedControlsListBE_1>(objMastersBAL.GetUserDefinedControlsList());
            objUserDefinedControlsListBE_2 = objIdsignBal.DeserializeFromXml<UserDefinedControlsListBE_2>(objMastersBAL.GetUserDefinedControlsList());

            HtmlGenericControl GrandParent = new HtmlGenericControl("div");
            GrandParent.ID = "GrandParent";

            if (objUserDefinedControlsListBE_1.items.Count > 0)
            {
                int counter = 1;
                string cssClass = "inner-box" + counter;

                Button btn = new Button();
                btn.ValidationGroup = "registration";
                string[] cssClasslist = new string[6];
                cssClasslist[0] = "inner-box";
                cssClasslist[1] = "text-inner";
                cssClasslist[2] = "clr";
                cssClasslist[4] = "text-box";
                cssClasslist[5] = "text-box text-select";


                foreach (UserDefinedControlsBE controlsItem in objUserDefinedControlsListBE_1.items)
                {
                    cssClass = cssClasslist[0] + counter;
                    HtmlGenericControl divParent = new HtmlGenericControl("div");
                    HtmlGenericControl divChild = new HtmlGenericControl("div");
                    HtmlGenericControl divClr = new HtmlGenericControl("div");
                    Label lblCaption = new Label();
                    Label lblMandatorySymbol = new Label();
                    Literal litBreak = new Literal();
                    RequiredFieldValidator rfv = new RequiredFieldValidator();
                    rfv.ForeColor = Color.Red;
                    rfv.Display = ValidatorDisplay.Dynamic;
                    rfv.ValidationGroup = "registration";
                    ((Button)this.Parent.FindControl("btnFinish")).ValidationGroup = "registration";
                    litBreak.Text = "<br />";
                    lblMandatorySymbol.CssClass = "span_star";
                    lblMandatorySymbol.Text = "*";

                    divParent.Attributes.Add("class", cssClass);
                    divChild.Attributes.Add("class", cssClasslist[1]);
                    divClr.Attributes.Add("class", cssClasslist[2]);
                    divParent.ID = "divParent" + controlsItem.UDCId;
                    divChild.ID = "divChild" + controlsItem.UDCId;
                    var list = from x in objUserDefinedControlsListBE_2.items
                               where x.UDCId == controlsItem.UDCId
                               //select x;
                               select new UserDefinedControlsBE
                               {
                                   UDCId = x.UDCId,
                                   MRefId = x.MRefId,
                                   VText = x.VText,
                               };
                    switch (controlsItem.ControlTypeId)
                    {
                        case Constants.TextBox:
                            TextBox objTextBox = new TextBox();

                            objTextBox.ID = "txt_" + controlsItem.FieldName.Replace(" ", string.Empty) + "_" + controlsItem.UDCId;
                            //objTextBox.ClientIDMode = System.Web.UI.ClientIDMode.Static;
                            if (controlsItem.IsMandatory)
                            {
                                divChild.Controls.Add(lblMandatorySymbol);
                                rfv.ID = "rfv_" + objTextBox.ID;
                                rfv.ControlToValidate = objTextBox.ID;
                                rfv.ErrorMessage = controlsItem.FieldName;
                            }
                            hfControlsID.Value = objTextBox.ID + "|";
                            objTextBox.CssClass = cssClasslist[4];
                            objTextBox.ValidationGroup = "registration";
                            lblCaption.Text = controlsItem.FieldName;
                            divChild.Controls.Add(lblCaption);

                            divChild.Controls.Add(litBreak);
                            divChild.Controls.Add(objTextBox);
                            if (controlsItem.IsMandatory)
                            {
                                divChild.Controls.Add(litBreak);
                                divChild.Controls.Add(rfv);
                            }
                            break;
                        case Constants.DropDownList:
                            DropDownList objDropDownList = new DropDownList();
                            objDropDownList.ID = "ddl_" + controlsItem.FieldName.Replace(" ", string.Empty) + "_" + controlsItem.UDCId;

                            hfControlsID.Value += objDropDownList.ID + "|";
                            objDropDownList.DataValueField = "MRefId";
                            objDropDownList.DataTextField = "VText";
                            objDropDownList.DataSource = list;
                            objDropDownList.DataBind();
                            objDropDownList.Items.Insert(0, new ListItem("--Select--", "0"));
                            lblCaption.Text = controlsItem.FieldName;
                            objDropDownList.ValidationGroup = "registration";
                            objDropDownList.CssClass = cssClasslist[5];
                            divChild.Controls.Add(lblCaption);
                            if (controlsItem.IsMandatory)
                            {
                                rfv.ID = "rfv_" + objDropDownList.ID;
                                rfv.ControlToValidate = objDropDownList.ID;
                                rfv.ErrorMessage = controlsItem.FieldName;
                                rfv.InitialValue = "--Select--";
                                divChild.Controls.Add(lblMandatorySymbol);
                            }
                            divChild.Controls.Add(litBreak);
                            divChild.Controls.Add(objDropDownList);
                            if (controlsItem.IsMandatory)
                            {
                                divChild.Controls.Add(litBreak);
                                divChild.Controls.Add(rfv);
                            }
                            break;
                        case Constants.CheckBoxList:
                            CheckBoxList objCheckBoxList = new CheckBoxList();
                            objCheckBoxList.ID = "chk_" + controlsItem.FieldName.Replace(" ", string.Empty) + "_" + controlsItem.UDCId;

                            hfControlsID.Value += objCheckBoxList.ID + "|";
                            lblCaption.Text = controlsItem.FieldName;

                            objCheckBoxList.DataValueField = "MRefId";
                            objCheckBoxList.DataTextField = "VText";

                            objCheckBoxList.DataSource = list;
                            objCheckBoxList.DataBind();

                            divChild.Controls.Add(lblCaption);
                            if (controlsItem.IsMandatory) divChild.Controls.Add(lblMandatorySymbol);
                            divChild.Controls.Add(litBreak);
                            divChild.Controls.Add(objCheckBoxList);
                            //divChild.Controls.Add(rfv);
                            break;
                        case Constants.RadioButtonList:
                            RadioButtonList objRadioButtonList = new RadioButtonList();
                            objRadioButtonList.ID = "rdo_" + controlsItem.FieldName.Replace(" ", string.Empty) + "_" + controlsItem.UDCId;

                            hfControlsID.Value += objRadioButtonList.ID + "|";
                            lblCaption.Text = controlsItem.FieldName;
                            objRadioButtonList.ValidationGroup = "registration";
                            objRadioButtonList.DataValueField = "MRefId";
                            objRadioButtonList.DataTextField = "VText";

                            objRadioButtonList.DataSource = list;
                            objRadioButtonList.DataBind();
                            objRadioButtonList.RepeatDirection = RepeatDirection.Horizontal;
                            divChild.Controls.Add(lblCaption);

                            if (controlsItem.IsMandatory)
                            {
                                rfv.ID = "rfv_" + objRadioButtonList.ID;
                                rfv.ControlToValidate = objRadioButtonList.ID;
                                rfv.ErrorMessage = controlsItem.FieldName;
                                divChild.Controls.Add(lblMandatorySymbol);
                            }
                            divChild.Controls.Add(litBreak);

                            divChild.Controls.Add(objRadioButtonList);
                            if (controlsItem.IsMandatory)
                            {
                                divChild.Controls.Add(litBreak);
                                divChild.Controls.Add(rfv);
                            }
                            break;
                    }
                    divParent.Controls.Add(divChild);
                    GrandParent.Controls.Add(divParent);
                    counter++;
                    if (counter == 4)
                    {
                        counter = 1;
                        GrandParent.Controls.Add(divClr);
                    }

                }
                if (!string.IsNullOrEmpty(hfControlsID.Value)) ((HiddenField)Parent.FindControl("hfControlsIDParent")).Value = hfControlsID.Value;
                this.Controls.Add(GrandParent);
                ((HtmlGenericControl)Parent.FindControl("divUserDefineFields")).Visible = true;
            }
            else
            {
                ((HtmlGenericControl)Parent.FindControl("divUserDefineFields")).Visible = false;
                this.Visible = false;
            }
           
        }
        #endregion
    }
}