﻿<%@ Page Title="::Add User::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="AddUser.aspx.cs" Inherits="UMS_Nigeria.UserManagement.AddUser" %>

<%@ Import Namespace="Resources" %>
<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <div class="inner-sec">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="text_total">
                <div class="text-heading">
                    <asp:Literal ID="litAddAgency" runat="server" Text="<%$ Resources:Resource, ADD_EMPLOYEE%>"></asp:Literal>
                </div>
                <div class="star_text">
                    <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="dot-line">
            </div>
            <div class="inner-box">
                <div class="inner-box1">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litName" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                            <span class="span_star">*</span></label>
                        <div class="space">
                        </div>
                        <asp:TextBox ID="txtName" TabIndex="1" CssClass="text-box" runat="server" MaxLength="300"
                            placeholder="Enter Name" onblur="return TextBoxBlurValidationlbl(this,'spanName','Name')"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbName" runat="server" TargetControlID="txtName"
                            ValidChars=" " FilterMode="ValidChars" FilterType="LowercaseLetters,UppercaseLetters,Custom">
                        </asp:FilteredTextBoxExtender>
                        <span id="spanName" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box2">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litSurName" runat="server" Text="<%$ Resources:Resource, SURNAME%>"></asp:Literal>
                            <span class="span_star">*</span></label>
                        <div class="space">
                        </div>
                        <asp:TextBox ID="txtSurName" TabIndex="2" CssClass="text-box" runat="server" MaxLength="300"
                            placeholder="Enter Surname" onblur="return TextBoxBlurValidationlbl(this,'spanSurName','SurName')"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbSurname" runat="server" TargetControlID="txtSurName"
                            ValidChars=" " FilterMode="ValidChars" FilterType="LowercaseLetters,UppercaseLetters,Custom">
                        </asp:FilteredTextBoxExtender>
                        <span id="spanSurName" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box3">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litGender" runat="server" Text="<%$ Resources:Resource, GENDER%>"></asp:Literal>
                            <span class="span_star">*</span></label>
                        <div class="space">
                        </div>
                        <asp:RadioButtonList ID="rblGender" Width="80%" TabIndex="3" onclick="RadioButtonListlbl(this,'spanGender','Gender')"
                            RepeatDirection="Horizontal" runat="server">
                        </asp:RadioButtonList>
                    </div>
                    <br />
                    <br />
                    <span id="spanGender" class="span_color"></span>
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litRoleId" runat="server" Text="<%$ Resources:Resource, ROLE_ID%>"></asp:Literal>
                            <span class="span_star">*</span></label>
                        <div class="space">
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlRoleId" TabIndex="4" CssClass="text-box select_box" onchange="RoleDropDownlistOnChangelbl(this,'spanRoleId','Role Id')"
                                    runat="server" AutoPostBack="True" Style="width: 187px;" OnSelectedIndexChanged="ddlRoleId_SelectedIndexChanged">
                                </asp:DropDownList>
                               <%-- <asp:DropDownList ID="ddlRoleId" TabIndex="4" CssClass="text-box select_box" onchange="return RoleDropDownlistOnChangelbl(this,'spanRoleId',' Role Id')"
                                    runat="server" Style="width: 187px;" >
                                </asp:DropDownList>
                                <asp:Button ID="btnDummyRoleId" style="display:none;" Text="" runat="server" OnClick="btnDummyRoleId_Click"/>--%>
                                <%--<asp:RequiredFieldValidator ID="rfvRoleId" runat="server" ErrorMessage="" InitialValue="" ControlToValidate="ddlRoleId"></asp:RequiredFieldValidator>--%>
                                <div class="space">
                                </div>
                                <span id="spanRoleId" class="span_color"></span>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="inner-box2">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litDesignation" runat="server" Text="<%$ Resources:Resource, DESIGNATION%>"></asp:Literal>
                            <span class="span_star">*</span></label>
                        <div class="space">
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlDesignation" Style="width: 187px;" TabIndex="5" CssClass="text-box select_box"
                                    onchange="DropDownlistOnChangelbl(this,'spanDisgnation','Designation')" runat="server">
                                </asp:DropDownList>
                                <div class="space">
                                </div>
                                <span id="spanDisgnation" class="span_color"></span>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="inner-box3">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litEmpId" runat="server" Text="<%$ Resources:Resource, USER_ID%>"></asp:Literal>
                            <span class="span_star">*</span></label>
                        <div class="space">
                        </div>
                        <asp:TextBox ID="txtEmpId" TabIndex="6" CssClass="text-box" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanEmpId','User Id')"
                            MaxLength="50" placeholder="Enter User Id"></asp:TextBox>
                        <span id="spanEmpId" class="span_color"></span>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litPassword" runat="server" Text="<%$ Resources:Resource, PASSWORD%>"></asp:Literal>
                            <span class="span_star">*</span></label>
                        <div class="space">
                        </div>
                        <%--onblur="return TextBoxBlurValidationlbl(this,'spanPwd','Password')"--%>
                        <asp:TextBox ID="txtPassword" TabIndex="7" CssClass="text-box" runat="server" TextMode="Password"
                            placeholder="Enter Password"></asp:TextBox>
                        <div class="space">
                        </div>
                        <asp:RegularExpressionValidator ID="revPassword" runat="server" ControlToValidate="txtPassword"
                            ValidationExpression="" ForeColor="red" ErrorMessage="The new password does not meet the length and complexity requirement.">
                        </asp:RegularExpressionValidator>
                        <span id="spanPwd" class="span_color"></span>
                        <div class="space">
                        </div>
                        <div id="divPasswordStrengthCriteria" runat="server">
                        </div>
                        <div class="space">
                        </div>
                    </div>
                </div>
                <div class="inner-box2">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litReEnterPassword" runat="server" Text="<%$ Resources:Resource, REENTER_PASSWORD%>"></asp:Literal>
                            <span class="span_star">*</span></label>
                        <div class="space">
                        </div>
                        <asp:TextBox ID="txtReEnterPassword" TabIndex="8" CssClass="text-box" runat="server"
                            onblur="return TextBoxBlurValidationlbl(this,'spanReEnterPassword','Re-Enter Password')"
                            TextMode="Password" placeholder="Re-Enter Password"></asp:TextBox>
                        <span id="spanReEnterPassword" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box3">
                    <div class="text-inner">
                        <label for="name">
                            <asp:UpdatePanel ID="upnlmail" runat="server">
                                <ContentTemplate>
                                    <asp:Literal ID="litPrimaryEmail" runat="server" Text="<%$ Resources:Resource, PRIMARY_EMAIL%>"></asp:Literal>
                                    <span id="divspanMailId" runat="server" visible="false" class="span_star">*</span>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </label>
                        <div class="space">
                        </div>
                        <asp:TextBox ID="txtPrimaryEmail" TabIndex="9" CssClass="text-box" runat="server"
                            MaxLength="500" placeholder="Enter Primary Email Id"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbPrimaryEmail" runat="server" TargetControlID="txtPrimaryEmail"
                            ValidChars="@._" FilterType="LowercaseLetters,UppercaseLetters,Numbers,Custom">
                        </asp:FilteredTextBoxExtender>
                        <span id="spanPrimaryEmail" class="span_color"></span>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litSecondaryEmail" runat="server" Text="<%$ Resources:Resource, SECONDARY_EMAIL%>"></asp:Literal>
                        </label>
                        <div class="space">
                        </div>
                        <asp:TextBox ID="txtSecondaryEmail" TabIndex="10" CssClass="text-box" runat="server"
                            MaxLength="500" placeholder="Enter Secondary Email Id"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbSecondaryEmail" runat="server" TargetControlID="txtSecondaryEmail"
                            ValidChars="@._" FilterType="LowercaseLetters,UppercaseLetters,Numbers,Custom">
                        </asp:FilteredTextBoxExtender>
                        <span id="spanSecondaryEmail" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box2">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litContactNo" runat="server" Text="<%$ Resources:Resource, CONTACT_NO%>"></asp:Literal>
                            <span class="span_star">*</span></label>
                        <div class="space">
                        </div>
                        <asp:TextBox CssClass="text-box" TabIndex="11" ID="txtCode1" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanContactNo','Code')"
                            MaxLength="3" Width="11%" placeholder="Code"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtCode1"
                            FilterType="Numbers" ValidChars=" ">
                        </asp:FilteredTextBoxExtender>
                        <asp:TextBox CssClass="text-box" ID="txtContactNo" TabIndex="12" Width="40%" runat="server"
                            onblur="return TextBoxBlurValidationlbl(this,'spanContactNo','Contact No')" MaxLength="10"
                            placeholder="Enter Contact No"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtContactNo"
                            FilterType="Numbers" ValidChars=" ">
                        </asp:FilteredTextBoxExtender>
                        <span id="spanContactNo" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box3">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litAnotherContactNo" runat="server" Text="<%$ Resources:Resource, ANOTHER_CONTACT_NO%>"></asp:Literal>
                        </label>
                        <div class="space">
                        </div>
                        <asp:TextBox CssClass="text-box" TabIndex="13" ID="txtCode2" runat="server" MaxLength="3"
                            Width="11%" placeholder="Code"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtCode2"
                            FilterType="Numbers" ValidChars=" ">
                        </asp:FilteredTextBoxExtender>
                        <asp:TextBox CssClass="text-box" TabIndex="14" ID="txtAnotherContactNo" Width="40%"
                            runat="server" MaxLength="10" placeholder="Enter Alternate Contact No"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbTxtAnotherContactNo" runat="server" TargetControlID="txtAnotherContactNo"
                            FilterType="Numbers" ValidChars=" ">
                        </asp:FilteredTextBoxExtender>
                        <span id="spanAnotherNo" class="span_color"></span>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litAddress" runat="server" Text="<%$ Resources:Resource, ADDRESS%>"></asp:Literal>
                            <span class="span_star">*</span></label>
                        <div class="space">
                        </div>
                        <asp:TextBox ID="txtAddress" TabIndex="15" CssClass="text-box" runat="server" placeholder="Enter Address"
                            TextMode="MultiLine" onblur="return TextBoxBlurValidationlbl(this,'spanAddress','Address')"></asp:TextBox>
                        <span id="spanAddress" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box2">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litDetails" runat="server" Text="<%$ Resources:Resource, Details%>"></asp:Literal>
                        </label>
                        <div class="space">
                        </div>
                        <asp:TextBox ID="txtDetails" TabIndex="16" CssClass="text-box" runat="server" TextMode="MultiLine"
                            placeholder="Enter Comments Here"></asp:TextBox>
                    </div>
                </div>
                <div class="inner-box3">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litPhoto" runat="server" Text="<%$ Resources:Resource, PHOTO%>"></asp:Literal>
                        </label>
                        <div class="space">
                        </div>
                        <asp:FileUpload ID="fupPhoto" TabIndex="17" runat="server" onchange="return filevalidate(this.value,'Photo')"
                            CssClass="fltl" />&nbsp; <span id="spanPhoto" class="span_color"></span>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner" id="divFile">
                        <label for="name">
                            <asp:Literal ID="litUpLoadDocument" runat="server" Text="<%$ Resources:Resource, DOCUMENTS%>"></asp:Literal>
                        </label>
                        <div class="space">
                        </div>
                        <asp:FileUpload ID="fupDocument" TabIndex="18" runat="server" onchange="return pdffilevalidate(this.value,'Documents')"
                            CssClass="fltl" />&nbsp;
                        <asp:LinkButton ID="lnkAddFiles" runat="server" CssClass="fltl pad_5" OnClientClick="return AddFile()"
                            Font-Bold="True">Add</asp:LinkButton><br />
                    </div>
                    <span id="spanDocuments" class="span_color"></span>
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, ISMOBILE_ACCESS%>"></asp:Literal>
                            <span class="span_star">*</span></label>
                        <div class="space">
                        </div>
                        <asp:RadioButtonList Width="80%" ID="rblIsMobileAccess" TabIndex="19" onclick="RadioButtonListlbl(this,'spanIsMobileAccess','One')"
                            RepeatDirection="Horizontal" runat="server">
                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                        </asp:RadioButtonList>
                        <br />
                    </div>
                    <br />
                    <span id="spanIsMobileAccess" class="span_color"></span>
                </div>
                <div class="clr">
                </div>
                <br />
                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>
                        <%--BU Start--%>
                        <div id="divBusinessUnits" visible="false" runat="server">
                            <div class="check_baa">
                                <div class="text-inner_a">
                                    <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNITS%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" ID="BUcbAll" Text="All" onclick="BUCheckAll();" />
                                        <asp:CheckBoxList ID="cblBusinessUnits" Width="45%" RepeatDirection="Horizontal"
                                            onclick="BUUnCheckAll();" RepeatColumns="3" runat="server">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="divNoBU" runat="server" visible="false" class="span_color">
                            There is no Business Units please select another BusinessUnit.
                        </div>
                        <%--BU END--%>
                        <%--BU,SU,SC Start--%>
                        <div id="divServiceCenters" visible="false" runat="server">
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNITS%>"></asp:Literal>
                                    </label>
                                    <div class="space">
                                    </div>
                                    <asp:DropDownList ID="ddlBusinessUnits" CssClass="text-box select_box" runat="server"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlBusinessUnits_SelectedIndexChanged">
                                        <asp:ListItem Text="Select Business Unit" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                    <br />
                                    <span class="span_color" id="spanddlBusinessUnits"></span>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, SERVICE_UNITS%>"></asp:Literal>
                                        <span class="span_star">*</span></label>
                                    <div class="space">
                                    </div>
                                    <asp:DropDownList ID="ddlServiceUnits" CssClass="text-box select_box" Enabled="false"
                                        runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlServiceUnits_SelectedIndexChanged">
                                        <asp:ListItem Text="Select Service Unit" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                    <br />
                                    <span class="span_color" id="spanddlServiceUnits"></span>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div id="lblSC" visible="false" runat="server">
                                <div class="check_baa">
                                    <div class="text-inner_a">
                                        <asp:Literal ID="lblSC1" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTERS%>"></asp:Literal>
                                        <span class="span_star">*</span>
                                    </div>
                                    <div class="check_ba">
                                        <div class="text-inner mster-input">
                                            <asp:CheckBox runat="server" Visible="false" ID="SCcbAll" AutoPostBack="true" OnCheckedChanged="cblServiceCenters_SelectedIndexChanged"
                                                Text="All" onclick="SCCheckAll();" />
                                            <div class="space">
                                            </div>
                                            <div id="divCblSC" runat="server">
                                                <asp:CheckBoxList ID="cblServiceCenters" onclick="SCUnCheckAll();" AutoPostBack="true"
                                                    OnSelectedIndexChanged="cblServiceCenters_SelectedIndexChanged" Width="179%"
                                                    RepeatDirection="Horizontal" RepeatColumns="3" runat="server">
                                                </asp:CheckBoxList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divNoSC" style="padding-left: 20px;" runat="server" visible="false" class="span_color">
                                There is no Service Centers please select another ServiceUnit.
                            </div>
                        </div>
                        <%--BU,SU,SC END--%>
                        <div class="clr">
                        </div>
                        <%--Cash Offices Start--%>
                        <div id="divCashOffices" visible="false" runat="server">
                            <div class="check_baa">
                                <div class="text-inner_a">
                                    <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, CASH_OFFICES%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox runat="server" ID="COcbAll" Text="All" onclick="COCheckAll();" />
                                        <div class="space">
                                        </div>
                                        <asp:CheckBoxList ID="cblCashOffices" onclick="COUnCheckAll();" Width="45%" RepeatDirection="Horizontal"
                                            RepeatColumns="3" runat="server">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="divNoCO" runat="server" visible="false" class="span_color">
                            There is no cash offices please select another ServiceCenter.
                        </div>
                        <%--Cash Offices End--%>
                        <%--ServiceUnits Start--%>
                        <div id="divServiceUnits" visible="false" runat="server">
                            <div class="inner-box1">
                                <div class="text-inner">
                                    <label for="name">
                                        <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNITS%>"></asp:Literal>
                                        <span class="span_star">*</span></label>
                                    <div class="space">
                                    </div>
                                    <asp:DropDownList ID="ddlBUServiceUnits" CssClass="text-box select_box" runat="server"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlBUServiceUnits_SelectedIndexChanged">
                                        <asp:ListItem Text="Select Business Unit" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div id="divNoSU" runat="server" visible="false" class="span_color">
                                There are no Service Units please select another Business Unit.
                                <div class="clear pad_10">
                                </div>
                            </div>
                            <div class="check_baa" runat="server" id="divSU" visible="false">
                                <div class="text-inner_a">
                                    <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:Resource, SERVICE_UNITS%>"></asp:Literal><span
                                        class="span_star">*</span>
                                </div>
                                <div class="check_ba">
                                    <div class="text-inner mster-input">
                                        <asp:CheckBox TabIndex="18" runat="server" Visible="false" ID="SUcbAll" Text="All"
                                            onclick="SUCheckAll();" />
                                        <div id="divCblSU" runat="server">
                                            <asp:CheckBoxList ID="cblServiceUnits" onclick="SUUnCheckAll();" Width="45%" RepeatDirection="Horizontal"
                                                RepeatColumns="3" runat="server">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--ServiceUnits End--%>
                        <%--<div class="consumer_feild" runat="server" id="divsplPermissions">
                            <div class="check_baa" style="width: 91%; margin-top: 20px;">
                                <div class="text-inner_a">
                                    <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, SPECIAL_PERMISSIONS%>"></asp:Literal>
                                </div>
                                <div class="check_ba">
                                    <div class="text_in_chk">
                                        <asp:CheckBox ID="chkCustomerModify" Text="<%$ Resources:Resource, CUSTOMER_DETAILS_MODIFICATION%>"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="check_bb">
                                    <div class="text_in_chk">
                                        <asp:CheckBox ID="chkBillAdjustment" Text="<%$ Resources:Resource, BILL_ADJUSTMENT%>"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="check_bc">
                                    <div class="text_in_chk">
                                        <asp:CheckBox ID="chkTariffAdjustment" Text="<%$ Resources:Resource, TARIFF_ADJUSTMENTS%>"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="check_ba">
                                    <div class="text_in_chk">
                                        <asp:CheckBox ID="chkPaymentAdjustment" Text="<%$ Resources:Resource, PAYMENTS_ADJUSTMENT%>"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="check_bb">
                                    <div class="text_in_chk">
                                        <asp:CheckBox ID="chkDisconnection" Text="<%$ Resources:Resource, DISCONNECTIONS%>"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="check_bc">
                                    <div class="text_in_chk">
                                        <asp:CheckBox ID="chkReconnection" Text="<%$ Resources:Resource, RECONNECTIONS%>"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="check_ba">
                                    <div class="text_in_chk">
                                        <asp:CheckBox ID="chkBillGeneration" Text="<%$ Resources:Resource, BILL_GENERATION%>"
                                            runat="server" />
                                    </div>
                                </div>
                                <div class="check_bb">
                                    <div class="text_in_chk">
                                        &nbsp;
                                    </div>
                                </div>
                            </div>
                        </div>--%>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="box_total">
                <div class="box_totalTwi_c">
                    <asp:Button ID="btnSave" TabIndex="19" OnClientClick="return Validate();" Text="<%$ Resources:Resource, SAVE%>"
                        CssClass="box_s" runat="server" OnClick="btnSave_Click" />
                </div>
                <div style="clear: both;">
                </div>
                <div class="box_totalTwo_b">
                    <div class="text-heading_2">
                        <a href="SearchUser.aspx"><span class="search_img"></span>
                            <asp:Literal ID="litSearch" runat="server" Text="User Search"></asp:Literal></a>
                    </div>
                </div>
            </div>
            <div style="clear: both;">
            </div>
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <%--Grid Starts Here--%>
                    <div class="grid_boxes">
                        <div class="grid_pagingTwo_top">
                            <div class="paging_top_title">
                                <asp:Literal ID="litCountriesList" runat="server" Text="<%$ Resources:Resource, GRID_USERS_LIST%>"></asp:Literal>
                            </div>
                            <div class="paging_top_rightTwo_content" id="divpaging" visible="false" runat="server">
                                <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvUsersList" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="grid_tb_hd"
                            AlternatingRowStyle-CssClass="color" OnRowCommand="gvUsersList_RowCommand" OnRowDataBound="gvUsersList_RowDataBound">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                <asp:Literal ID="litNoData" runat="server" Text="<%$ Resources:Resource, THERE_IS_NO_DATA%>"></asp:Literal>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                    HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        <asp:Label ID="lblActiveStatusId" Visible="false" runat="server" Text='<%#Eval("ActiveStatusId") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, EMPLOYEE_ID%>" ItemStyle-Width="50px"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserId" runat="server" Text='<%#Eval("EmployeeId") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="false" HeaderText="<%$ Resources:Resource, SURNAME%>"
                                    ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSurName" runat="server" Text='<%#Eval("SurName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, CONTACT_NO%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPrimaryContact" runat="server" Text='<%#Eval("ContactNo1") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, ANOTHER_CONTACT_NO%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" Visible="false" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSecondaryContact" runat="server" Text='<%#Eval("ContactNo2") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, PRIMARY_EMAIL%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPrimaryEmailId" runat="server" Text='<%#Eval("PrimaryEmailId") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, SECONDARY_EMAIL%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" Visible="false" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSecondaryEmaiId" runat="server" Text='<%#Eval("SecondaryEmailId") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, ROLE_ID%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRoleId" runat="server" Visible="false" Text='<%#Eval("RoleId") %>'></asp:Label>
                                        <asp:Label ID="lblRoleName" runat="server" Text='<%#Eval("RoleName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, DESIGNATION%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDesignationId" Visible="false" runat="server" Text='<%#Eval("DesignationId") %>'></asp:Label>
                                        <asp:Label ID="lblDesignationName" runat="server" Text='<%#Eval("DesignationName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="false" HeaderText="<%$ Resources:Resource, GENDER%>"
                                    ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGenderId" runat="server" Visible="false" Text='<%#Eval("GenderId") %>'></asp:Label>
                                        <asp:Label ID="lblIsMobileAccess" runat="server" Visible="false" Text='<%#Eval("IsMobileAccess") %>'></asp:Label>
                                        <asp:Label ID="lblGenderName" runat="server" Text='<%#Eval("GenderName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, ADDRESS%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress" runat="server" Text='<%#Eval("Address") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, DETAILS%>" ItemStyle-Width="5%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridDetails" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, RESET_PASSWORD%>" ItemStyle-Width="5%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPhoto" Visible="false" runat="server" Text='<%#Eval("Photo") %>'></asp:Label>
                                        <asp:Label ID="lblPhotoPath" Visible="false" runat="server" Text='<%#Eval("FilePath") %>'></asp:Label>
                                        <asp:LinkButton ID="lkbtnResetPassword" runat="server" ToolTip="Reset Password" CommandName="ResetPassword"><img src="../images/ResetPassword.png" alt="ResetPassword" /></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="13%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                       <div style="width:70px;"> <asp:LinkButton ID="lkbtnEdit" runat="server" ToolTip="Edit" Text="Edit" CommandName="EditUser"
                                            CommandArgument='<%# Eval("EmployeeId") %>'>
                                        <img src="../images/Edit.png" alt="Edit" /></asp:LinkButton>
                                        <%--<asp:LinkButton ID="lkbtnUpdate" Visible="false" runat="server" ToolTip="Update"
                            CommandName="UpdateAgency"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                        <asp:LinkButton ID="lkbtnCancel" Visible="false" runat="server" ToolTip="Cancel"
                            CommandName="CancelAgency"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>--%>
                                        <asp:LinkButton ID="lkbtnDelete" runat="server" ToolTip="Delete User" CommandName="DeleteUser"><img src="../images/delete.png" alt="Delete" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnActive" Visible="false" runat="server" ToolTip="Active User"
                                            CommandName="ActiveUser"><img src="../images/Activate.gif" alt="Active User" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnDeActive" runat="server" ToolTip="DeActive User" CommandName="DeActiveUser"><img src="../images/Deactivate.gif" alt="DeActive User" /></asp:LinkButton>
                                       </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:HiddenField ID="hfPageSize" runat="server" />
                        <asp:HiddenField ID="hfEditDocuments" runat="server" />
                        <asp:HiddenField ID="hfPageNo" runat="server" />
                        <asp:HiddenField ID="hfLastPage" runat="server" />
                        <asp:HiddenField ID="hfTotalRecords" Value="0" runat="server" />
                        <asp:HiddenField ID="hfRecognize" runat="server" />
                        <asp:HiddenField ID="hfId" runat="server" />
                        <asp:HiddenField ID="hfEditmailId" runat="server" />
                        <asp:HiddenField ID="hfmailId" runat="server" />
                        <asp:HiddenField ID="hfDocuments" runat="server" />
                    </div>
                    <div class="grid_boxes">
                        <div id="divdownpaging" visible="false" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                    <%--Grid End Here--%>
                    <div id="hidepopup">
                        <%-- Active Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                            TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="PanelActivate" runat="server" CssClass="modalPopup"
                            Style="display: none; border-color: #639B00;">
                            <div class="popheader" style="background-color: #639B00;">
                                <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                            </div>
                            <br />
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btnActiveOk" runat="server" OnClick="btnActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                    <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Active  Btn popup Closed--%>
                        <%-- DeActive  Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                            TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnDeActivate" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="PanelDeActivate" runat="server" CssClass="modalPopup"
                            Style="display: none; border-color: #639B00; z-index: 1000001;">
                            <div class="popheader" style="background-color: red;">
                                <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <br />
                                    <asp:Button ID="btnDeActiveOk" runat="server" OnClick="btnDeActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                    <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- DeActive  popup Closed--%>
                    </div>
                    <%--Edit User Start Here--%>
                    <asp:ModalPopupExtender ID="mpeEditUserpopup" runat="server" TargetControlID="hfPopup"
                        PopupControlID="pnlEditUserDetails" BackgroundCssClass="popbg" CancelControlID="btnEditCancel">
                    </asp:ModalPopupExtender>
                    <asp:Panel ID="pnlEditUserDetails" runat="server" Style="display: none;" class="editpopup_two edit">
                        <div class="out-bor">
                            <div class="inner-sec">
                                <asp:Panel ID="pnlEditMessage" runat="server">
                                    <asp:Label ID="lblEditMessage" runat="server"></asp:Label>
                                </asp:Panel>
                                <div class="text_total">
                                    <div class="text-heading">
                                        <asp:Literal ID="litEditEmployee" runat="server" Text="<%$ Resources:Resource, UPDATE_EMPLOYEE%>"></asp:Literal>
                                    </div>
                                    <div class="star_text">
                                        <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                                <div class="dot-line">
                                </div>
                                <div class="inner-box">
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditName" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                                                <span class="span_star">*</span></label>
                                            <div class="space">
                                            </div>
                                            <asp:TextBox ID="txtEditName" CssClass="text-box" runat="server" MaxLength="300"
                                                placeholder="Enter Name" onblur="return TextBoxBlurValidationlbl(this,'spanEditName','Name')"></asp:TextBox>
                                            <span id="spanEditName" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box2">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditSurname" runat="server" Text="<%$ Resources:Resource, SURNAME%>"></asp:Literal>
                                                <span class="span_star">*</span></label>
                                            <div class="space">
                                            </div>
                                            <asp:TextBox ID="txtEditSurName" CssClass="text-box" runat="server" MaxLength="300"
                                                placeholder="Enter Surname" onblur="return TextBoxBlurValidationlbl(this,'spanEditSurName','Surname')"></asp:TextBox>
                                            <span id="spanEditSurName" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box3">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditGender" runat="server" Text="<%$ Resources:Resource, GENDER%>"></asp:Literal>
                                                <span class="span_star">*</span></label>
                                            <div class="space">
                                            </div>
                                            <asp:RadioButtonList ID="rblEditGender" Width="70%" onchange="RadioButtonListlbl(this,'spanEditGender',' Select one')"
                                                RepeatDirection="Horizontal" runat="server">
                                            </asp:RadioButtonList>
                                            <div class="space">
                                                <br />
                                            </div>
                                            <span id="spanEditGender" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditRoleId" runat="server" Text="<%$ Resources:Resource, ROLE_ID%>"></asp:Literal>
                                                <span class="span_star">*</span></label>
                                            <div class="space">
                                            </div>
                                            <asp:UpdatePanel ID="upnlddlEditRoleId" runat="server">
                                                <ContentTemplate>
                                                    <asp:DropDownList ID="ddlEditRoleId" CssClass="text-box text-select" onchange=" debugger; DropDownlistOnChangelbl(this,'spanEditRoleId',' Role Id')"
                                                        runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlEditRoleId_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <div class="space">
                                                    </div>
                                                    <span id="spanEditRoleId" class="span_color"></span>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                    <div class="inner-box2">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditDesignation" runat="server" Text="<%$ Resources:Resource, DESIGNATION%>"></asp:Literal>
                                                <span class="span_star">*</span></label>
                                            <div class="space">
                                            </div>
                                            <asp:DropDownList ID="ddlEditDesignation" onchange="DropDownlistOnChangelbl(this,'spanEditDesignation',' Designation')"
                                                runat="server" CssClass="text-box text-select">
                                            </asp:DropDownList>
                                            <div class="space">
                                            </div>
                                            <span id="spanEditDesignation" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box3">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditEmployeeId" runat="server" Text="<%$ Resources:Resource, EMPLOYEE_ID%>"></asp:Literal>
                                                <span class="span_star">*</span></label>
                                            <div class="space">
                                            </div>
                                            <asp:TextBox ID="txtEditEmployeeId" Enabled="false" runat="server" MaxLength="50"
                                                placeholder="Enter Employee Id" CssClass="text-box"></asp:TextBox>
                                            <span id="spanEditEmployeeId" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditPEmail" runat="server" Text="<%$ Resources:Resource, PRIMARY_EMAIL%>"></asp:Literal>
                                                <span class="span_star" id="divEditspanMail" runat="server" visible="false">*</span>
                                            </label>
                                            <div class="space">
                                            </div>
                                            <asp:TextBox ID="txtEditPrimaryEmail" CssClass="text-box" runat="server" MaxLength="500"
                                                placeholder="Enter Primary Email Id"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtEditPrimaryEmail"
                                                ValidChars="@._" FilterType="LowercaseLetters,UppercaseLetters,Numbers,Custom">
                                            </asp:FilteredTextBoxExtender>
                                            <span id="spanEditPrimaryEmail" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box2">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditSEmail" runat="server" Text="<%$ Resources:Resource, SECONDARY_EMAIL%>"></asp:Literal>
                                            </label>
                                            <div class="space">
                                            </div>
                                            <asp:TextBox ID="txtEditSecondaryEmail" CssClass="text-box" runat="server" MaxLength="500"
                                                placeholder="Enter Secondary Email Id"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="ftbEditSEmail" runat="server" TargetControlID="txtEditSecondaryEmail"
                                                ValidChars="@._" FilterType="LowercaseLetters,UppercaseLetters,Numbers,Custom">
                                            </asp:FilteredTextBoxExtender>
                                            <span id="spanEditSecondaryEmail" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box3">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditContactNo1" runat="server" Text="<%$ Resources:Resource, CONTACT_NO%>"></asp:Literal>
                                                <span class="span_star">*</span></label>
                                            <div class="space">
                                            </div>
                                            <asp:TextBox CssClass="text-box" ID="txtEditCode1" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanEditContactNo','Code')"
                                                MaxLength="3" Width="11%" placeholder="Code"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtEditCode1"
                                                FilterType="Numbers" ValidChars=" ">
                                            </asp:FilteredTextBoxExtender>
                                            <asp:TextBox CssClass="text-box" ID="txtEditContactNo1" Width="40%" runat="server"
                                                onblur="return TextBoxBlurValidationlbl(this,'spanEditContactNo','Contact No')"
                                                MaxLength="10" placeholder="Enter Contact No"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtEditContactNo1"
                                                FilterType="Numbers" ValidChars=" ">
                                            </asp:FilteredTextBoxExtender>
                                            <span id="spanEditContactNo" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditContactNo2" runat="server" Text="<%$ Resources:Resource, ANOTHER_CONTACT_NO%>"></asp:Literal>
                                            </label>
                                            <div class="space">
                                            </div>
                                            <asp:TextBox CssClass="text-box" ID="txtEditCode2" runat="server" MaxLength="3" Width="11%"
                                                placeholder="Code"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtEditCode2"
                                                FilterType="Numbers" ValidChars=" ">
                                            </asp:FilteredTextBoxExtender>
                                            <asp:TextBox CssClass="text-box" ID="txtEditContactNo2" Width="40%" runat="server"
                                                MaxLength="10" placeholder="Enter Alternate Contact No"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtEditContactNo2"
                                                FilterType="Numbers" ValidChars=" ">
                                            </asp:FilteredTextBoxExtender>
                                            <span id="spanEditContactNo2" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box2">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditAddress" runat="server" Text="<%$ Resources:Resource, ADDRESS%>"></asp:Literal>
                                                <span class="span_star">*</span></label>
                                            <div class="space">
                                            </div>
                                            <asp:TextBox ID="txtEditAddress" runat="server" CssClass="text-box" placeholder="Enter Address"
                                                TextMode="MultiLine" onblur="return TextBoxBlurValidationlbl(this,'spanEditAddress','Address')"></asp:TextBox>
                                            <span id="spanEditAddress" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box3">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditDetails" runat="server" Text="<%$ Resources:Resource, Details%>"></asp:Literal></label>
                                            <div class="space">
                                            </div>
                                            <asp:TextBox ID="txtEditDetails" runat="server" CssClass="text-box" TextMode="MultiLine"
                                                placeholder="Enter Details"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditPhoto" runat="server" Text="<%$ Resources:Resource, PHOTO%>"></asp:Literal>
                                            </label>
                                            <div class="space">
                                            </div>
                                            <asp:FileUpload ID="fupEditPhoto" runat="server" onchange="return fileEditvalidate(this.value,'Photo')"
                                                CssClass="fltl" />&nbsp; <span id="spanfupEditPhoto" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box2">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditDocuments" runat="server" Text="<%$ Resources:Resource, DOCUMENTS%>"></asp:Literal>
                                            </label>
                                            <div class="space">
                                            </div>
                                            <div id="divEditFile">
                                                <asp:FileUpload ID="fupEditDocument" runat="server" onchange="return pdffileEditvalidate(this.value,'Documents')"
                                                    CssClass="fltl" />&nbsp;
                                                <asp:LinkButton ID="lnkEditAddFiles" runat="server" CssClass="fltl" OnClientClick="return AddEditFile()"
                                                    Font-Bold="True">Add</asp:LinkButton><br />
                                                <span id="spanEditDocuments" class="span_color"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, ISMOBILE_ACCESS%>"></asp:Literal>
                                                <span class="span_star">*</span></label>
                                            <div class="space">
                                            </div>
                                            <asp:RadioButtonList ID="rblEditIsMobileAccess" Width="70%" RepeatDirection="Horizontal"
                                                runat="server">
                                                <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                            </asp:RadioButtonList>
                                            <div class="space">
                                                <br />
                                            </div>
                                            <span id="spanEditIsMobileAccess" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="consumer_feild">
                                        <div class="consume_input editlist" id="div1" style="width: 98%; padding: 5px;">
                                            <div class="photodocumentFullWidth">
                                                <h2 id="divSubhead" runat="server" class="subhead">
                                                    <asp:Literal ID="litDownloadDocs" Visible="false" runat="server" Text="<%$ Resources:Resource, DOWNLOAD_UPLOADED_DOCUMENTS%>"></asp:Literal><br />
                                                    <br />
                                                </h2>
                                                <div id="divPhoto" runat="server" visible="false">
                                                    <asp:UpdatePanel ID="upnlPhotos" runat="server">
                                                        <ContentTemplate>
                                                            <div class="fltl">
                                                                <asp:Label runat="server" ID="lblPhotoId"></asp:Label>
                                                                <asp:Label runat="server" Visible="false" ID="lblPhotoUserId"></asp:Label>&nbsp&nbsp&nbsp&nbsp
                                                            </div>
                                                            <div class="fltr" style="width: 180px;">
                                                                <asp:LinkButton ID="lbtnPhotoDown" runat="server" OnClick="lbtnPhotoDown_Click" ForeColor="Green">Download</asp:LinkButton>&nbsp&nbsp&nbsp&nbsp
                                                                <asp:LinkButton ID="lbtnPhotoDelete" ForeColor="Red" OnClick="lbtnPhotoDelete_Click"
                                                                    runat="server">Delete</asp:LinkButton>
                                                            </div>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="lbtnPhotoDown" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </div>
                                                <asp:Repeater ID="rptrDocuments" runat="server">
                                                    <ItemTemplate>
                                                        <asp:UpdatePanel ID="upnlDocs" runat="server">
                                                            <ContentTemplate>
                                                                <div class="fltl">
                                                                    <asp:Label runat="server" Visible="false" ID="lblDocId" Text='<%#Eval("UserDocumentId") %>'></asp:Label>
                                                                    <asp:Label runat="server" ID="lblDocName" Text='<%#Eval("DocumentName") %>'></asp:Label>
                                                                </div>
                                                                <div class="fltr">
                                                                    <asp:LinkButton ID="lbtnDocName" runat="server" ForeColor="Green" OnClick="lbtnDocName_Click"
                                                                        CommandArgument='<%#Eval("DocumentName") %>'>Download</asp:LinkButton>&nbsp&nbsp&nbsp&nbsp
                                                                    <asp:LinkButton ID="lbtnDocDelete" ForeColor="Red" OnClick="lbtnDocDelete_Click"
                                                                        runat="server" CommandArgument='<%#Eval("UserDocumentId") %>'>Delete</asp:LinkButton>&nbsp&nbsp&nbsp&nbsp
                                                                </div>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="lbtnDocName" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:UpdatePanel ID="upnlEditPermissions" runat="server">
                                    <ContentTemplate>
                                        <%--BU Start--%>
                                        <div id="divEditBusinessUnits" visible="false" runat="server">
                                            <div class="check_baa">
                                                <div class="text-inner_a">
                                                    <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNITS%>"></asp:Literal>
                                                </div>
                                                <div class="check_ba">
                                                    <div class="text-inner mster-input">
                                                        <asp:CheckBox runat="server" ID="BUcbEditAll" Text="All" onclick="BUEditCheckAll();" />
                                                        <asp:CheckBoxList ID="cblEditBusinessUnits" Width="30%" RepeatDirection="Horizontal"
                                                            onclick="BUEditUnCheckAll();" RepeatColumns="3" runat="server">
                                                        </asp:CheckBoxList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                        <div id="divEditNoBU" runat="server" visible="false" class="span_color">
                                            There is no Business Units please select another BusinessUnit.
                                        </div>
                                        <%--BU END--%>
                                        <div class="clr">
                                        </div>
                                        <%--BU,SU,SC Start--%>
                                        <div id="divEditServiceCenters" visible="false" runat="server">
                                            <div class="inner-box1">
                                                <div class="text-inner">
                                                    <label for="name">
                                                        <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNITS%>"></asp:Literal>
                                                        <span class="span_star">*</span>
                                                    </label>
                                                    <div class="space">
                                                    </div>
                                                    <asp:DropDownList ID="ddlEditBusinessUnits" CssClass="text-box select_box" runat="server"
                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlEditBusinessUnits_SelectedIndexChanged">
                                                        <asp:ListItem Text="Select Business Unit" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <span class="span_color" id="spanEditddlBusinessUnits"></span>
                                                </div>
                                            </div>
                                            <div class="clr">
                                            </div>
                                            <div class="inner-box1">
                                                <div class="text-inner">
                                                    <label for="name">
                                                        <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:Resource, SERVICE_UNITS%>"></asp:Literal>
                                                        <span class="span_star">*</span></label>
                                                    <div class="space">
                                                    </div>
                                                    <asp:DropDownList ID="ddlEditServiceUnits" CssClass="text-box select_box" Enabled="false"
                                                        runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlEditServiceUnits_SelectedIndexChanged">
                                                        <asp:ListItem Text="Select Service Unit" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <span class="span_color" id="spanEditddlServiceUnits"></span>
                                                </div>
                                            </div>
                                            <div class="clr">
                                            </div>
                                            <div id="lblEditSC" runat="server" visible="false">
                                                <div class="check_baa">
                                                    <div class="text-inner_a">
                                                        <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTERS%>"></asp:Literal>
                                                        <span class="span_star">*</span>
                                                    </div>
                                                    <div class="check_ba">
                                                        <div class="text-inner mster-input">
                                                            <asp:CheckBox runat="server" Visible="false" ID="SCcbEditAll" Text="All" onclick="SCEditCheckAll();"
                                                                AutoPostBack="true" OnCheckedChanged="cblEditServiceCenters_SelectedIndexChanged" />
                                                            <div id="divEditCblSC" runat="server">
                                                                <asp:CheckBoxList ID="cblEditServiceCenters" onclick="SCEditUnCheckAll();" Width="45%"
                                                                    AutoPostBack="true" OnSelectedIndexChanged="cblEditServiceCenters_SelectedIndexChanged"
                                                                    RepeatDirection="Horizontal" RepeatColumns="3" runat="server">
                                                                </asp:CheckBoxList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divEditNoSC" runat="server" visible="false" class="span_color">
                                            There is no Service Centers please select another ServiceUnit.
                                        </div>
                                        <%--BU,SU,SC END--%>
                                        <div class="clr">
                                        </div>
                                        <%--Cash Offices Start--%>
                                        <div id="divEditCashOffices" visible="false" runat="server">
                                            <div class="check_baa">
                                                <div class="text-inner_a">
                                                    <asp:Literal ID="Literal22" runat="server" Text="<%$ Resources:Resource, CASH_OFFICES%>"></asp:Literal>
                                                    <span class="span_star">*</span>
                                                </div>
                                                <div class="check_ba">
                                                    <div class="text-inner mster-input">
                                                        <asp:CheckBox runat="server" ID="COcbEditAll" Text="All" onclick="COEditCheckAll();" />
                                                        <div class="space">
                                                        </div>
                                                        <asp:CheckBoxList ID="cblEditCashOffices" onclick="COEditUnCheckAll();" Width="45%"
                                                            RepeatDirection="Horizontal" RepeatColumns="3" runat="server">
                                                        </asp:CheckBoxList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                        <div id="divEditNoCO" runat="server" visible="false" class="span_color">
                                            There is no Cash Offices please select another ServiceCenter.
                                        </div>
                                        <%--Cash Offices End--%>
                                        <%--ServiceUnits Start--%>
                                        <div id="divEditServiceUnits" visible="false" runat="server">
                                            <div class="inner-box1">
                                                <div class="text-inner">
                                                    <label for="name">
                                                        <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNITS%>"></asp:Literal>
                                                        <span class="span_star">*</span></label>
                                                    <div class="space">
                                                    </div>
                                                    <asp:DropDownList ID="ddlEditBUServiceUnits" CssClass="text-box select_box" runat="server"
                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlEditBUServiceUnits_SelectedIndexChanged">
                                                        <asp:ListItem Text="Select Business Unit" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="clr">
                                            </div>
                                            <div class="check_baa">
                                                <div class="text-inner_a">
                                                    <asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:Resource, SERVICE_UNITS%>"></asp:Literal><span
                                                        class="span_star">*</span>
                                                </div>
                                                <div class="check_ba">
                                                    <div class="text-inner mster-input">
                                                        <asp:CheckBox runat="server" Visible="false" ID="SUcbEditAll" Text="All" onclick="SUEditCheckAll();" />
                                                        <div id="divEditCblSU" runat="server">
                                                            <asp:CheckBoxList ID="cblEditServiceUnits" onclick="SUEditUnCheckAll();" Width="45%"
                                                                RepeatDirection="Horizontal" RepeatColumns="3" runat="server">
                                                            </asp:CheckBoxList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                        <div id="divEditNoSU" runat="server" visible="false" class="span_color">
                                            There is no Service Units please select another BusinessUnit.
                                        </div>
                                        <%--ServiceUnits End--%>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div class="space">
                                </div>
                                <%--<div class="consumer_feild" runat="server" id="divEditsplPermissions">
                                    <div class="check_baa" style="width: 91%;">
                                        <div class="text-inner_a">
                                            <asp:Literal ID="litEditSpecialPermissions" runat="server" Text="<%$ Resources:Resource, SPECIAL_PERMISSIONS%>"></asp:Literal>
                                        </div>
                                        <div class="check_ba">
                                            <div class="text-inner">
                                                <asp:CheckBox ID="chkEditCustomerModify" Text="<%$ Resources:Resource, CUSTOMER_DETAILS_MODIFICATION%>"
                                                    runat="server" />
                                            </div>
                                        </div>
                                        <div class="check_bb">
                                            <div class="text-inner">
                                                <asp:CheckBox ID="chkEditBillAdjustment" Text="<%$ Resources:Resource, BILL_ADJUSTMENT%>"
                                                    runat="server" />
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                        <div class="check_ba">
                                            <div class="text-inner">
                                                <asp:CheckBox ID="chkEditTariffAdjustment" Text="<%$ Resources:Resource, TARIFF_ADJUSTMENTS%>"
                                                    runat="server" />
                                            </div>
                                        </div>
                                        <div class="check_bb">
                                            <div class="text-inner">
                                                <asp:CheckBox ID="chkEditPaymentAdjustment" Text="<%$ Resources:Resource, PAYMENTS_ADJUSTMENT%>"
                                                    runat="server" />
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                        <div class="check_ba">
                                            <div class="text-inner">
                                                <asp:CheckBox ID="chkEditDisconnection" Text="<%$ Resources:Resource, DISCONNECTIONS%>"
                                                    runat="server" />
                                            </div>
                                        </div>
                                        <div class="check_bb">
                                            <div class="text-inner">
                                                <asp:CheckBox ID="chkEditReconnection" Text="<%$ Resources:Resource, RECONNECTIONS%>"
                                                    runat="server" />
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                        <div class="check_ba">
                                            <div class="text-inner">
                                                <asp:CheckBox ID="chkEditBillGeneration" Text="<%$ Resources:Resource, BILL_GENERATION%>"
                                                    runat="server" />
                                            </div>
                                        </div>
                                        <div class="check_bb">
                                            <div class="text-inner">
                                                &nbsp;
                                            </div>
                                        </div>
                                    </div>
                                </div>--%>
                                <div class="box_total">
                                    <div class="box_totalTwi_c">
                                        <asp:Button ID="btnUpdate" OnClick="btnUpdate_Click" OnClientClick="return EditValidate();"
                                            Text="<%$ Resources:Resource, UPDATE%>" CssClass="box_s" runat="server" />
                                        <asp:Button ID="btnEditCancel" Text="<%$ Resources:Resource, CANCEL%>" CssClass="box_s"
                                            runat="server" />
                                    </div>
                                    <div style="clear: both;">
                                    </div>
                                    <div class="box_totalTwo_b">
                                        <div class="text-heading_2">
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:ModalPopupExtender ID="mpeDocpopup" runat="server" PopupControlID="pnldeletedoc"
                            TargetControlID="btndeletedoc" BackgroundCssClass="modalBackground" CancelControlID="btnCanceldoc">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btndeletedoc" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="pnldeletedoc" runat="server" CssClass="modalPopup"
                            Style="display: none; border-color: #639B00; z-index: 1000001;">
                            <div class="popheader" style="background-color: red;">
                                <asp:Label ID="lblMsg" runat="server"></asp:Label>
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <br />
                                    <asp:Button ID="btndeleteok" runat="server" OnClick="btndeleteok_Click" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                    <asp:Button ID="btnCanceldoc" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                    </asp:Panel>
                    <%--Edit User End Here--%>
                    <%--Password Popup Start Here--%>
                    <asp:ModalPopupExtender ID="mpeResetPwdpopup" runat="server" TargetControlID="hfPopup1"
                        PopupControlID="pnlChangePwd" BackgroundCssClass="popbg" CancelControlID="btnCancel">
                    </asp:ModalPopupExtender>
                    <asp:Panel ID="pnlChangePwd" runat="server" CssClass="editpopup_two" Style="display: none">
                        <h3 class="edithead" align="left">
                            <asp:Literal ID="Literal26" runat="server" Text="<%$ Resources:Resource, RESET_PASSWORD%>"></asp:Literal>
                        </h3>
                        <div class="clear">
                            &nbsp;</div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="litEditPassword" runat="server" Text="<%$ Resources:Resource, PASSWORD%>"></asp:Literal><span>*</span>
                            </div>
                            <div class="consume_input" style="width: 350px">
                                <%--onblur="return TextBoxBlurValidationlbl(this,'spanEditPassword','Password')"--%>
                                <asp:TextBox ID="txtEditPassword" runat="server" CssClass="text-box" TextMode="Password"
                                    placeholder="Enter Password"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revTxtEditPassword" runat="server" ControlToValidate="txtEditPassword"
                                    ValidationExpression="" ForeColor="red" ErrorMessage="The new password does not meet the length and complexity requierment.">
                                </asp:RegularExpressionValidator>
                                <span id="spanEditPassword" class="span_color"></span>
                                <div class="">
                                </div>
                            </div>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="litEditReEnterPassword" runat="server" Text="<%$ Resources:Resource, REENTER_PASSWORD%>"></asp:Literal><span>*</span>
                            </div>
                            <div class="consume_input" style="width: 350px">
                                <asp:TextBox ID="txtEditReEnterPassword" CssClass="text-box" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanEditReEnterPassword','Re-Enter Password')"
                                    TextMode="Password" placeholder="Re-Enter Password"></asp:TextBox>
                                <span id="spanEditReEnterPassword" class="span_color"></span>
                            </div>
                        </div>
                        <div class="clear pad_10">
                        </div>
                        <div class="consumer_feild">
                            <div style="margin-left: 156px; overflow: auto; float: left;">
                                <asp:Button ID="btnChangePwd" OnClientClick="return EditPasswordValidate();" OnClick="btnChangePwd_Click"
                                    Text="<%$ Resources:Resource, RESET%>" CssClass="calculate_but" runat="server" />
                                <asp:Button ID="btnCancel" Text="<%$ Resources:Resource, CANCEL%>" CssClass="calculate_but"
                                    runat="server" />
                            </div>
                            <br />
                            <div class="clear">
                            </div>
                            <br />
                            <div id="divPasswordResetValidation" runat="server">
                            </div>
                    </asp:Panel>
                    <%--Password Popup End Here--%>
                    <asp:HiddenField ID="hfPopup" runat="server" />
                    <asp:HiddenField ID="hfdocid" runat="server" />
                    <asp:HiddenField ID="hfPopup1" runat="server" />
                    <%-- Alert popup Start--%>
                    <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="PanelAlert"
                        TargetControlID="btnAlertDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnAlertOk">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnAlertDeActivate" runat="server" Text="Button" CssClass="popbtn" />
                    <asp:Panel ID="PanelAlert" runat="server" CssClass="modalPopup" class="poppnl" Style="display: none;">
                        <div class="popheader popheaderlblred" id="pop" runat="server">
                            <asp:Label ID="lblAlertMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnAlertOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                        TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="Panelgridalert" runat="server" DefaultButton="btngridalertok" CssClass="modalPopup"
                        Style="display: none;">
                        <div class="popheader popheaderlblred">
                            <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Alert popup Closed--%>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnUpdate" />
                    <asp:PostBackTrigger ControlID="btnSave" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/AddUsers.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <%-- <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>--%>
    <%--==============Validation script ref ends==============--%>
      <script language="javascript" type="text/javascript">
          $(document).ready(function () { assignDiv('rptrMainMasterMenu_162_4', '162') });
        </script>
</asp:Content>
