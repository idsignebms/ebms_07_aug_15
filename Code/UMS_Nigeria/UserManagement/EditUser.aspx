﻿<%@ Page Title=":: Change Profile ::" Language="C#" MasterPageFile="~/MasterPages/EBMS.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="EditUser.aspx.cs" Inherits="UMS_Nigeria.UserManagement.EditUser" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <div class="inner-sec">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="text_total">
                <div class="text-heading">
                    <asp:Literal ID="litAddEmployee" runat="server" Text="<%$ Resources:Resource, CNG_USR_PROF%>"></asp:Literal>
                </div>
                <div class="star_text">
                    <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="dot-line">
            </div>
            <div class="inner-box">
                <div class="inner-box1">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litName" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                            <span class="span_star">*</span></label>
                        <div class="space">
                        </div>
                        <asp:TextBox ID="txtName" CssClass="text-box" runat="server" MaxLength="300" placeholder="Enter Name"
                            onblur="return TextBoxBlurValidationlbl(this,'spanName','Name')"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbName" runat="server" TargetControlID="txtName"
                            ValidChars=" " FilterMode="ValidChars" FilterType="LowercaseLetters,UppercaseLetters,Custom">
                        </asp:FilteredTextBoxExtender>
                        <span id="spanName" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box2">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litSurName" runat="server" Text="<%$ Resources:Resource, SURNAME%>"></asp:Literal>
                            <span class="span_star">*</span></label>
                        <div class="space">
                        </div>
                        <asp:TextBox ID="txtSurName" CssClass="text-box" runat="server" MaxLength="300" placeholder="Enter Surname"
                            onblur="return TextBoxBlurValidationlbl(this,'spanSurName','SurName')"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbSurname" runat="server" TargetControlID="txtSurName"
                            ValidChars=" " FilterMode="ValidChars" FilterType="LowercaseLetters,UppercaseLetters,Custom">
                        </asp:FilteredTextBoxExtender>
                        <span id="spanSurName" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box3">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litGender" runat="server" Text="<%$ Resources:Resource, GENDER%>"></asp:Literal>
                            <span class="span_star">*</span></label>
                        <div class="space">
                        </div>
                        <asp:RadioButtonList ID="rblGender" Width="90%" RepeatDirection="Horizontal" runat="server">
                        </asp:RadioButtonList>
                        <div class="space">
                        </div>
                        <span id="spanGender" class="span_color"></span>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner">
                        <label for="name">
                            <%--<asp:Literal ID="litPrimaryEmail" runat="server" Text="<%$ Resources:Resource, PRIMARY_EMAIL%>"></asp:Literal>
                            <span class="span_star">*</span>--%>
                            <asp:UpdatePanel ID="upnlmail" runat="server">
                                <ContentTemplate>
                                    <asp:Literal ID="litPrimaryEmail" runat="server" Text="<%$ Resources:Resource, PRIMARY_EMAIL%>"></asp:Literal>
                                    <span id="divspanMailId" runat="server" visible="false" class="span_star">*</span>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </label>
                        <div class="space">
                        </div>
                        <asp:DropDownList ID="ddlRoleId" Visible="false" CssClass="text-box select_box" onchange="DropDownlistOnChangelbl(this,'spanRoleId',' Role Id')"
                            runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlRoleId_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:TextBox ID="txtPrimaryEmail" CssClass="text-box" runat="server" MaxLength="500"
                            placeholder="Enter Primary Email Id"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbPrimaryEmail" runat="server" TargetControlID="txtPrimaryEmail"
                            ValidChars="@._" FilterType="LowercaseLetters,UppercaseLetters,Numbers,Custom">
                        </asp:FilteredTextBoxExtender>
                        <span id="spanPrimaryEmail" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box2">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litSecondaryEmail" runat="server" Text="<%$ Resources:Resource, SECONDARY_EMAIL%>"></asp:Literal>
                        </label>
                        <div class="space">
                        </div>
                        <asp:TextBox ID="txtSecondaryEmail" CssClass="text-box" runat="server" MaxLength="500"
                            placeholder="Enter Secondary Email Id"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbSecondaryEmail" runat="server" TargetControlID="txtSecondaryEmail"
                            ValidChars="@._" FilterType="LowercaseLetters,UppercaseLetters,Numbers,Custom">
                        </asp:FilteredTextBoxExtender>
                        <span id="spanSecondaryEmail" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box3">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litContactNo" runat="server" Text="<%$ Resources:Resource, CONTACT_NO%>"></asp:Literal>
                            <span class="span_star">*</span></label>
                        <div class="space">
                        </div>
                        <asp:TextBox CssClass="text-box" ID="txtCode1" TabIndex="4" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanContactNo','Code')"
                            MaxLength="3" Width="11%" placeholder="Code"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtCode1"
                            FilterType="Numbers" ValidChars=" ">
                        </asp:FilteredTextBoxExtender>
                        <asp:TextBox CssClass="text-box" ID="txtContactNo" Width="40%" TabIndex="4" runat="server"
                            onblur="return TextBoxBlurValidationlbl(this,'spanContactNo','Contact No')" MaxLength="15"
                            placeholder="Enter Contact No"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtContactNo"
                            FilterType="Numbers" ValidChars=" ">
                        </asp:FilteredTextBoxExtender>
                        <span id="spanContactNo" class="span_color"></span>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litAnotherContactNo" runat="server" Text="<%$ Resources:Resource, ANOTHER_CONTACT_NO%>"></asp:Literal>
                        </label>
                        <div class="space">
                        </div>
                        <asp:TextBox CssClass="text-box" ID="txtCode2" TabIndex="4" runat="server" MaxLength="3"
                            Width="11%" placeholder="Code"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtCode2"
                            FilterType="Numbers" ValidChars=" ">
                        </asp:FilteredTextBoxExtender>
                        <asp:TextBox CssClass="text-box" ID="txtAnotherContactNo" Width="40%" TabIndex="5"
                            runat="server" MaxLength="15" placeholder="Enter Alternate Contact No"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="ftbTxtAnotherContactNo" runat="server" TargetControlID="txtAnotherContactNo"
                            FilterType="Numbers" ValidChars=" ">
                        </asp:FilteredTextBoxExtender>
                        <span id="spanAnotherNo" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box2">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litAddress" runat="server" Text="<%$ Resources:Resource, ADDRESS%>"></asp:Literal>
                            <span class="span_star">*</span></label>
                        <div class="space">
                        </div>
                        <asp:TextBox ID="txtAddress" CssClass="text-box" runat="server" placeholder="Enter Address"
                            TextMode="MultiLine" onblur="return TextBoxBlurValidationlbl(this,'spanAddress','Address')"></asp:TextBox>
                        <span id="spanAddress" class="span_color"></span>
                    </div>
                </div>
                <div class="inner-box3">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litDetails" runat="server" Text="<%$ Resources:Resource, Details%>"></asp:Literal>
                        </label>
                        <div class="space">
                        </div>
                        <asp:TextBox ID="txtDetails" CssClass="text-box" runat="server" TextMode="MultiLine"
                            placeholder="Enter Comments Here"></asp:TextBox>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litPhoto" runat="server" Text="Change Photo"></asp:Literal>
                        </label>
                        <div class="space">
                        </div>
                        <br />
                        <asp:FileUpload ID="fupPhoto" runat="server" onchange="return filevalidate(this.value,'Photo')"
                            CssClass="fltl" />&nbsp; <span id="spanPhoto" class="span_color"></span>
                        <div class="space">
                            <br />
                        </div>
                    </div>
                    <asp:Image runat="server" Style="border: 1px solid #000; width: 150px; height: 150px;"
                        ImageUrl="../images/ebmslogo.jpg" ID="testimage" />
                </div>
                <div class="clr">
                </div>
                <div class="inner-box1">
                    <div class="text-inner">
                        <label for="name">
                            <asp:Literal ID="litUpLoadDocument" runat="server" Text="<%$ Resources:Resource, DOCUMENTS%>"></asp:Literal>
                        </label>
                        <div class="space">
                        </div>
                        <div id="divFile">
                            <asp:FileUpload ID="fupDocument" runat="server" onchange="return pdffilevalidate(this.value,'Documents')"
                                CssClass="fltl" />&nbsp;
                            <asp:LinkButton ID="lnkAddFiles" runat="server" CssClass="fltl pad_5" OnClientClick="return AddFile()"
                                Font-Bold="True">Add</asp:LinkButton><br />
                            <span id="spanDocuments" style="color: Red; width: 252px;"></span>
                        </div>
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="out-bor">
                <h3 class="text-heading">
                    <asp:Literal ID="litDownloadDocs" Visible="false" runat="server" Text="<%$ Resources:Resource, DOWNLOAD_UPLOADED_DOCUMENTS%>"></asp:Literal>
                </h3>
                <br />                
                <div class="consume_input editlist" id="div1" style="width: auto;">
                    <div class="photodocument">
                        <asp:Repeater ID="rptrDocuments" runat="server">
                            <ItemTemplate>
                                <asp:UpdatePanel ID="upnlDocs" runat="server">
                                    <ContentTemplate>
                                        <div class="fltl" style="left:35px;position:absolute;font-size: 14px;">
                                            <asp:Label runat="server" Visible="false" ID="lblDocId" Text='<%#Eval("UserDocumentId") %>'></asp:Label>
                                            <asp:Label runat="server" ID="lblDocName" Text='<%#Eval("DocumentName") %>'></asp:Label>
                                            &nbsp;
                                        </div>
                                        <div class="fltr" style="right:220px; position:absolute;font-size: 14px;">
                                            <asp:LinkButton ID="lbtnDocName" runat="server"  ForeColor="Green" OnClick="lbtnDocName_Click"
                                                CommandArgument='<%#Eval("DocumentName") %>'>Download</asp:LinkButton><br />
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <br />
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="lbtnDocName" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                </div>
            </div>
            <div class="clr">
            </div>
            <div class="box_total">
                <div class="box_total_a">
                    <asp:Button ID="btnUpdate" OnClientClick="return Validate();" Text="<%$ Resources:Resource, UPDATE%>"
                        CssClass="box_s" runat="server" OnClick="btnUpdate_Click" />
                </div>
                <div style="clear: both;">
                </div>
                <div class="box_totalTwo_b">
                    <div class="text-heading_2">
                        <a href="../UserManagement/ChangePassword.aspx">
                            <asp:Literal ID="litSearch" runat="server" Text="Change Password"></asp:Literal></a></div>
                </div>
            </div>
            <asp:HiddenField ID="hfDocuments" runat="server" />
            <asp:HiddenField ID="hfmailId" runat="server" />
        </div>
    </div>
    <%--==============Escape button script starts==============--%>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/EditUser.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
