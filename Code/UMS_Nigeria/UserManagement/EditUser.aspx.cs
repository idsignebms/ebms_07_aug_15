﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigeriaBE;
using System.Xml;
using UMS_NigriaDAL;
using System.Data;
using System.Configuration;
using System.IO;

namespace UMS_Nigeria.UserManagement
{
    public partial class EditUser : System.Web.UI.Page
    {
        #region Members
        CommonMethods _objCommonMethods = new CommonMethods();
        iDsignBAL _objIdsignBal = new iDsignBAL();
        string Key = "EditEmployee";
        public int PageNum;
        ConsumerBal objConsumerBal = new ConsumerBal();
        UserManagementBAL objUserManagementBal = new UserManagementBAL();

        string Userid = string.Empty;
        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = _objCommonMethods.GetPagePath(this.Request.Url);
                //if (_objCommonMethods.IsAccess(path))
                //{
                _objCommonMethods.BindGender(rblGender, 0);
                GetUserDetails();
                //}
                //else
                //{ Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE); }
            }
        }

        private void GetUserDetails()
        {
            try
            {
                UserManagementBE objUserManagementBE = new UserManagementBE();
                UserManagementListBE objUserManagementListBE = new UserManagementListBE();
                UserManagementDocListBE objUserManagementDocListBE = new UserManagementDocListBE();
                XmlDocument resultedXml = new XmlDocument();

                objUserManagementBE.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                resultedXml = objUserManagementBal.Get(objUserManagementBE, ReturnType.Single);
                objUserManagementListBE = _objIdsignBal.DeserializeFromXml<UserManagementListBE>(resultedXml);
                objUserManagementDocListBE = _objIdsignBal.DeserializeFromXml<UserManagementDocListBE>(resultedXml);

                _objCommonMethods.BindRoles(ddlRoleId, 0, true);
                ddlRoleId.SelectedIndex = ddlRoleId.Items.IndexOf(ddlRoleId.Items.FindByValue(objUserManagementListBE.Items[0].RoleId.ToString()));
                ddlRoleId_SelectedIndexChanged(ddlRoleId, null);

                txtName.Text = objUserManagementListBE.Items[0].Name;
                txtSurName.Text = objUserManagementListBE.Items[0].SurName;
                rblGender.SelectedIndex = rblGender.Items.IndexOf(rblGender.Items.FindByValue(objUserManagementListBE.Items[0].GenderId.ToString()));
                Session[UMS_Resource.SESSION_USER_EMAILID] = txtPrimaryEmail.Text = objUserManagementListBE.Items[0].PrimaryEmailId;

                txtSecondaryEmail.Text = objUserManagementListBE.Items[0].SecondaryEmailId;

                //txtContactNo.Text = objUserManagementListBE.Items[0].ContactNo1;
                //txtAnotherContactNo.Text = objUserManagementListBE.Items[0].ContactNo2;
                string lblContactNo1 = objUserManagementListBE.Items[0].ContactNo1;
                string lblContactNo2 = objUserManagementListBE.Items[0].ContactNo2;

                if (lblContactNo1.Length > 0 && lblContactNo1 != "--")
                {
                    string[] contactNo = lblContactNo1.Split('-');
                    if (contactNo.Length < 2)
                    {
                        txtCode1.Text = lblContactNo1.Substring(0, 3);
                        txtContactNo.Text = lblContactNo1.Remove(0, 3);
                    }
                    else
                    {
                        txtCode1.Text = contactNo[0].ToString();
                        txtContactNo.Text = contactNo[1].ToString();
                    }
                }
                else
                    txtCode1.Text = txtContactNo.Text = string.Empty;

                if (!string.IsNullOrEmpty(lblContactNo2))
                {
                    if (lblContactNo2.Length > 0 && lblContactNo2 != "--")
                    {
                        string[] contactNo = lblContactNo2.Split('-');
                        if (contactNo.Length < 2)
                        {
                            txtCode2.Text = lblContactNo2.Substring(0, 3);
                            txtAnotherContactNo.Text = lblContactNo2.Remove(0, 3);
                        }
                        else
                        {
                            txtCode2.Text = contactNo[0].ToString();
                            txtAnotherContactNo.Text = contactNo[1].ToString();
                        }
                    }
                    else
                        txtCode2.Text = txtAnotherContactNo.Text = string.Empty;
                }
                txtAddress.Text = _objIdsignBal.ReplaceNewLines(objUserManagementListBE.Items[0].Address, false);
                txtDetails.Text = _objIdsignBal.ReplaceNewLines(objUserManagementListBE.Items[0].Details, false);
                Session[UMS_Resource.SESSION_UserName] = objUserManagementListBE.Items[0].Name;

                Label lblUserName = (Label)this.Page.Master.FindControl("lblUserName");
                lblUserName.Text = Session[UMS_Resource.SESSION_UserName].ToString();

                if (!string.IsNullOrEmpty(objUserManagementListBE.Items[0].Photo))
                {
                    string fileLocation = objUserManagementListBE.Items[0].Photo.ToString();
                    testimage.ImageUrl = Constants.User_Img_Path + fileLocation;
                }
                else
                {
                    testimage.ImageUrl = UMS_Resource.DEFAULT_USER_ICON;
                }

                if (objUserManagementDocListBE.Items.Count > 0)
                {
                    litDownloadDocs.Visible = true;
                    rptrDocuments.DataSource = objUserManagementDocListBE.Items;
                    rptrDocuments.DataBind();
                }
                else
                {
                    rptrDocuments.DataSource = new DataTable();
                    rptrDocuments.DataBind();
                }

            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                UserManagementBE objUserManagementBE = UpdateUserDetails();
                if (objUserManagementBE.IsSuccess)
                {
                    GetUserDetails();
                    Message(UMS_Resource.EMPLOYEE_UPDATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else if (objUserManagementBE.IsEmailIdExists)
                {
                    Message(UMS_Resource.EMAIL_EXISTS, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else
                {
                    Message(UMS_Resource.EMPLOYEE_UPDATE_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private UserManagementBE UpdateUserDetails()
        {
            try
            {
                UserManagementBE objUserManagementBE = new UserManagementBE();
                objUserManagementBE.EmployeeId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objUserManagementBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objUserManagementBE.Name = txtName.Text;
                objUserManagementBE.SurName = txtSurName.Text;
                objUserManagementBE.PrimaryEmailId = txtPrimaryEmail.Text;
                objUserManagementBE.SecondaryEmailId = txtSecondaryEmail.Text;
                objUserManagementBE.GenderId = Convert.ToInt32(rblGender.SelectedValue);
                objUserManagementBE.Address = _objIdsignBal.ReplaceNewLines(txtAddress.Text, true);
                objUserManagementBE.ContactNo1 = txtCode1.Text + "-" + txtContactNo.Text;
                objUserManagementBE.ContactNo2 = txtCode2.Text + "-" + txtAnotherContactNo.Text;
                objUserManagementBE.Details = _objIdsignBal.ReplaceNewLines(txtDetails.Text, true);

                string PhotoEditFileName = string.Empty;
                if (fupPhoto.FileContent.Length > 0)
                {
                    PhotoEditFileName = System.IO.Path.GetFileName(fupPhoto.FileName).ToString();//Assinging FileName
                    string PhotoExtension = Path.GetExtension(PhotoEditFileName);//Storing Extension of file into variable Extension
                    TimeZoneInfo PhotoIND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                    DateTime PhotocurrentTime = DateTime.Now;
                    DateTime Photoourtime = TimeZoneInfo.ConvertTime(PhotocurrentTime, PhotoIND_ZONE);
                    string PhotoCurrentDate = Photoourtime.ToString("dd_MM_yyyy_hh_mm_ss");
                    string PhotoModifiedFileName = Path.GetFileNameWithoutExtension(PhotoEditFileName) + "_" + PhotoCurrentDate + PhotoExtension;//Setting File Name to store it in database
                    fupPhoto.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["UserPhotos"].ToString()) + PhotoModifiedFileName);//Saving File in to the server.

                    objUserManagementBE.Photo = PhotoModifiedFileName;
                    objUserManagementBE.FilePath = ConfigurationManager.AppSettings["UserPhotos"].ToString().Replace("~", ".."); ;
                }

                string fileInput = string.Empty;
                for (int z = 0; z < Request.Files.Count; z++)//looping to save attachments
                {
                    HttpPostedFile PostedFile = Request.Files[z];//Assigning to variable to Access File
                    if (PostedFile.ContentLength != 0)//Checking the size of file is more than 0 bytes
                    {
                        string FileName = PostedFile.FileName;//Assinging FileName
                        if (FileName != PhotoEditFileName)
                        {
                            string Extension = Path.GetExtension(FileName);//Storing Extension of file into variable Extension
                            TimeZoneInfo IND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                            DateTime currentTime = DateTime.Now;
                            DateTime ourtime = TimeZoneInfo.ConvertTime(currentTime, IND_ZONE);
                            string CurrentDate = ourtime.ToString("dd_MM_yyyy_hh_mm_ss");
                            string ModifiedFileName = Path.GetFileNameWithoutExtension(FileName) + "_" + CurrentDate + Extension;//Setting File Name to store it in database
                            // File.Create(Server.MapPath(ConfigurationManager.AppSettings["UserDocuments"].ToString()) + ModifiedFileName);//Saving File in to the server.
                            PostedFile.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["UserDocuments"].ToString()) + ModifiedFileName);
                            fileInput = string.IsNullOrEmpty(fileInput) ? ModifiedFileName : fileInput + "," + ModifiedFileName;
                        }
                    }
                }
                objUserManagementBE.Path = ConfigurationManager.AppSettings["UserDocuments"].ToString().Replace("~", ".."); ;
                objUserManagementBE.DocumentName = fileInput;

                objUserManagementBE = _objIdsignBal.DeserializeFromXml<UserManagementBE>(objUserManagementBal.Insert(objUserManagementBE, Statement.Generate));
                return objUserManagementBE;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        protected void lbtnDocName_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtnDocName = (LinkButton)sender;
                string fileLocation = Server.MapPath(ConfigurationManager.AppSettings["UserDocuments"].ToString()) + lbtnDocName.CommandArgument;
                if (File.Exists(fileLocation))
                    _objCommonMethods.DownloadFile(fileLocation, string.Empty);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlRoleId_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlRoleId.SelectedIndex > 0)
                {
                    hfmailId.Value = 1.ToString();
                    divspanMailId.Visible = true;

                //    hfmailId.Value = string.Empty;
                //    DataSet dsMailId = new DataSet();
                //    dsMailId.ReadXml(Server.MapPath(ConfigurationManager.AppSettings["MailId_Mandatory_BasedOnRoleId"]));
                //    var result = (from x in dsMailId.Tables[0].AsEnumerable()
                //                  where (x.Field<string>("value")) == ddlRoleId.SelectedValue
                //                  select new
                //                  {
                //                      //value1 = x.Field<string>("value"),
                //                      flag = x.Field<string>("flag")

                //                  }).SingleOrDefault();

                //    if (Convert.ToInt32(result.flag) == 1)
                //    {
                //        hfmailId.Value = 1.ToString();
                //        divspanMailId.Visible = true;
                //    }
                //    else
                //    {
                //        divspanMailId.Visible = false;
                //    }

                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    _objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #region Methods
        private void Message(string Message, string MessageType)
        {
            try
            {
                _objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion
    }
}