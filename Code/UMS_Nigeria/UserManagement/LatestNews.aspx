<%@ Page Title="::Latest News Details::" Language="C#" MasterPageFile="~/MasterPages/EDMUMS.Master"
    AutoEventWireup="true" CodeBehind="LatestNews.aspx.cs" Inherits="UMS_Nigeria.UserManagement.LatestNews" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="edmcontainer">
        <asp:UpdatePanel ID="upNews" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div class="consumer_total">
                    <div class="pad_10">
                    </div>
                </div>
                <div class="consumer_total">
                </div>
                <div id="divLatestNewsDynamic" runat="server">
                    <div id="divgrid" class="grid newsgrid" runat="server">
                        <div id="divpaging" runat="server">
                            <div align="right" class="marg_20t" runat="server" id="divLinks">
                                <h3 class="gridhead" align="left">
                                    <asp:Literal ID="litBuList" runat="server" Text="<%$ Resources:Resource, LATEST_NEWS_DETAILS%>"></asp:Literal>
                                </h3>
                                <asp:LinkButton ID="lkbtnFirst" runat="server" CssClass="pagingfirst" OnClick="lkbtnFirst_Click">
                                    <asp:Literal ID="litFirst" runat="server" Text="<%$ Resources:Resource, FIRST%>"></asp:Literal></asp:LinkButton>
                                &nbsp;|
                                <asp:LinkButton ID="lkbtnPrevious" runat="server" CssClass="pagingfirst" OnClick="lkbtnPrevious_Click">
                                    <asp:Literal ID="litPrevious" runat="server" Text="<%$ Resources:Resource, PREVIOUS%>"></asp:Literal></asp:LinkButton>
                                &nbsp;|
                                <asp:Label ID="lblCurrentPage" runat="server" Font-Bold="true"></asp:Label>
                                &nbsp;|
                                <asp:LinkButton ID="lkbtnNext" runat="server" CssClass="pagingfirst" OnClick="lkbtnNext_Click">
                                    <asp:Literal ID="litNext" runat="server" Text="<%$ Resources:Resource, NEXT%>"></asp:Literal></asp:LinkButton>
                                &nbsp;|
                                <asp:LinkButton ID="lkbtnLast" runat="server" CssClass="pagingfirst" OnClick="lkbtnLast_Click">
                                    <asp:Literal ID="litLast" runat="server" Text="<%$ Resources:Resource, LAST%>"></asp:Literal></asp:LinkButton>
                                &nbsp;|<asp:Literal ID="litPageSize" runat="server" Text="<%$ Resources:Resource, PAGE_SIZE%>"></asp:Literal>
                                <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" CssClass="paging-size"
                                    OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="consumer_total">
                        <div class="clear">
                        </div>
                    </div>
                    <div class="consumer_total">
                        <div class="clear">
                        </div>
                    </div>
                    <div class="grid" id="divPrint" runat="server" style="overflow-x: auto;">
                        <asp:Repeater ID="rptrNews" runat="server" OnItemCommand="rptrNews_ItemCommand">
                            <ItemTemplate>
                                <div class="latest_news">
                                    <div class="fltl latest_admin">
                                        <%#Container.ItemIndex+1 %>
                                    </div>
                                    <div class="fltr delete">
                                        <asp:LinkButton ID="lkbtnDeActive" runat="server" ToolTip="Delete News" CommandArgument='<%#Eval("NoticeID") %>'
                                            CommandName="DEACTIVATENEWS" Text="Delete"><img src="../images/delete.png" alt="Delete" /></asp:LinkButton>
                                    </div>
                                    <div class="fltl clear latest_one">
                                        <%#Eval("Subject") %></div>
                                    <div class="clear pad_5">
                                    </div>
                                    <div class="latest_two">
                                        <%#Eval("Details") %></div>
                                    <div class="clear pad_5">
                                    </div>
                                    <div class="fltr latest_admin">
                                        <span class="admin_name">
                                            <%#Eval("CreatedBy") %></span>
                                        <br />
                                        <span class="admin_time">
                                            <%#Eval("CreatedDate") %>
                                        </span>
                                    </div>
                                    <div class="clear pad_5">
                                    </div>
                                </div>
                            </ItemTemplate>
                            <SeparatorTemplate>
                            </SeparatorTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <div id="divLatestNewsStatic" runat="server" visible="false">
                    <div class="grid" runat="server" style="width: 100% !important; float: left;">
                        <div id="div2" runat="server">
                            <div align="right" class="marg_20t">
                                <h3 class="gridhead" align="left">
                                    <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, LATEST_NEWS%>"></asp:Literal>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="consumer_total">
                        <div class="clear">
                        </div>
                    </div>
                    <div class="consumer_total">
                        <div class="clear">
                        </div>
                    </div>
                    <div class="grid">
                        <div class="latest_news" style="overflow: auto;">
                            <div class="fltl latest_admin">
                                1
                            </div>
                            <div class="clear">
                            </div>
                            <div class="fltl latest_one">
                                <asp:Literal ID="litStaticNewsTitle" runat="server" Text="<%$Resources:Resource ,NEWS_STATIC_TITLE %>"></asp:Literal>
                            </div>
                            <div class="fltr delete">
                            </div>
                            <div class="clear pad_5">
                            </div>
                            <div class="latest_two">
                                <asp:Literal ID="Literal2" runat="server" Text="<%$Resources:Resource ,NEWS_STATIC_DESC %>"></asp:Literal></div>
                            <div class="clear pad_5">
                            </div>
                            <div class="fltr latest_admin">
                                <span class="admin_name">Admin</span>
                                <br />
                                <span class="admin_time">14-May-14 11:37:16 AM </span>
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="hfPageSize" runat="server" />
                <asp:HiddenField ID="hfPageNo" runat="server" />
                <asp:HiddenField ID="hfLastPage" runat="server" />
                <asp:HiddenField ID="hfTotalRecords" runat="server" />
                <asp:HiddenField ID="hfNoticeId" runat="server" />
                <div id="rnkland">
                    <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                        TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnDeActivate" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="PanelDeActivate" runat="server" CssClass="modalPopup" DefaultButton="btnDeActiveOk"
                        Style="display: none; border-color: #639B00;">
                        <div class="popheader" style="background-color: red;">
                            <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                        </div>
                        <div class="" align="right">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnDeActiveOk" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" OnClick="btnDeActiveOk_Click" />
                                <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                    CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lkbtnFirst" />
                <asp:PostBackTrigger ControlID="lkbtnPrevious" />
                <asp:PostBackTrigger ControlID="lkbtnNext" />
                <asp:PostBackTrigger ControlID="lkbtnLast" />
                <asp:PostBackTrigger ControlID="ddlPageSize" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".rnkland").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script type="text/javascript">
        function pageLoad() {
            DisplayMessage('<%= pnlMessage.ClientID %>');
        };       
    </script>
    <%--==============Validation script ref ends==============--%>
    <%--==============Ajax loading script starts==============--%>
    <%--==============Validation script ref ends==============--%>
</asp:Content>
