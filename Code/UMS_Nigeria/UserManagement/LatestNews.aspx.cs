﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using Resources;
using System.Configuration;
using UMS_NigeriaBAL;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Globalization;

namespace UMS_Nigeria.UserManagement
{
    public partial class LatestNews : System.Web.UI.Page
    {
        #region Members
        CommonMethods _ObjCommonMethods = new CommonMethods();
        NotificationsBE _ObjNotificationsBE = new NotificationsBE();
        NotificationsBAL _objNotificationsBAL = new NotificationsBAL();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        XmlDocument _ObjXmlDocument = new XmlDocument();
        CommonMethods objCommonMethods = new CommonMethods();
        string Key = "AddNotice";
        public int PageNum;
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStarts : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        #endregion

        #region Events

        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            pnlMessage.CssClass = lblMessage.Text = string.Empty;
            if (!IsPostBack)
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                if (!string.IsNullOrEmpty(Request.QueryString["nw"]))
                {
                    BindNews();
                    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                    divLatestNewsDynamic.Visible = true;
                    divLatestNewsStatic.Visible = false;
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["static"]))
                {
                    divLatestNewsDynamic.Visible = false;
                    divLatestNewsStatic.Visible = true;
                }
                else
                {
                    //Response.Redirect("~/Home.aspx");
                    BindNews();
                    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                }
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindNews();
                hfPageSize.Value = ddlPageSize.SelectedItem.Text;
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnFirst_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindNews();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum -= Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindNews();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                PageNum = Convert.ToInt32(lblCurrentPage.Text.Trim());
                PageNum += Constants.pageNoValue;
                hfPageNo.Value = PageNum.ToString();
                BindNews();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnLast_Click(object sender, EventArgs e)
        {
            try
            {
                hfPageNo.Value = hfLastPage.Value;
                BindNews();
                CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            _ObjNotificationsBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            _ObjNotificationsBE.NoticeID = Convert.ToInt32(hfNoticeId.Value);
            _ObjNotificationsBE.Activestatus = 3;
            _ObjXmlDocument = _objNotificationsBAL.DeleteNewsBAL(_ObjNotificationsBE, Statement.Delete);
            _ObjNotificationsBE = _ObjiDsignBAL.DeserializeFromXml<NotificationsBE>(_ObjXmlDocument);
            if (_ObjNotificationsBE.IsSuccess == 1)
            {
                BindNews();
                Message(UMS_Resource.NOTICE_DELETED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
            }
            else
                Message(UMS_Resource.NOTICE_DELETED_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
        }

        protected void rptrNews_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            LinkButton lk = (LinkButton) e.Item.FindControl("lkbtnDeActive");
            string litBUNoticeID = ((LinkButton)e.Item.FindControl("lkbtnDeActive")).CommandArgument;
            switch (e.CommandName.ToUpper())
            {
                case "DEACTIVATENEWS":
                    mpeDeActive.Show();
                    hfNoticeId.Value = litBUNoticeID;
                    lblDeActiveMsg.Text = Resource.DELETE_NEWS_CONFIRM;
                    btnDeActiveOk.Focus();
                    break;
            }
        }
        #endregion

        #region Methods
        private void Message(string Message, string MessageType)
        {
            try
            {
                _ObjCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void BindNews()
        {
            XmlDocument ResultantXml = null;
            _ObjNotificationsBE.PageSize = PageSize;
            _ObjNotificationsBE.PageNo = Convert.ToInt32(hfPageNo.Value);
            _ObjNotificationsBE.CreatedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            if (Session[UMS_Resource.SESSION_USER_BUID] != null)
            {                
                _ObjNotificationsBE.BU_ID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
            }
            
            if (Request.QueryString["nw"] != null)
            {
                _ObjNotificationsBE.NoticeID = Convert.ToInt32(Request.QueryString["nw"]);
                ResultantXml = _objNotificationsBAL.GetNotificationBYIDtBAL(_ObjNotificationsBE);
            }
            else
            {
                ResultantXml = _objNotificationsBAL.GetNotificationsBAL(_ObjNotificationsBE);
            }
            NotificationsListBE objNotificationList = _ObjiDsignBAL.DeserializeFromXml<NotificationsListBE>(ResultantXml);
            rptrNews.DataSource = _ObjiDsignBAL.ConvertListToDataSet<NotificationsBE>(objNotificationList.Items).Tables[0];
            rptrNews.DataBind();
            if (objNotificationList.Items.Count > 0)
            {
                rptrNews.DataSource = _ObjiDsignBAL.ConvertListToDataSet<NotificationsBE>(objNotificationList.Items).Tables[0];
                rptrNews.DataBind();
                hfTotalRecords.Value = objNotificationList.Items[0].TotalRecords.ToString();
            }
            else
            {
                hfTotalRecords.Value = objNotificationList.Items.Count.ToString();
                divgrid.Visible = divpaging.Visible = false;
            }
        }

        private void CreatePagingDT(int TotalNoOfRecs)
        {
            try
            {
                double totalpages = Math.Ceiling(((double)TotalNoOfRecs / PageSize));
                DataTable dtPages = new DataTable();
                dtPages.Columns.Add("PageNo", typeof(int));
                int currentpage = Convert.ToInt32(hfPageNo.Value);
                lkbtnLast.CommandArgument = hfLastPage.Value = totalpages.ToString();
                _ObjiDsignBAL.GridViewPaging(lkbtnNext, lkbtnPrevious, lkbtnFirst, lkbtnLast, currentpage, TotalNoOfRecs);
                lblCurrentPage.Text = hfPageNo.Value;
                if (totalpages == 1) { lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = Color.Gray; }
                if (string.Compare(hfPageNo.Value, hfLastPage.Value, true) == 0) { lkbtnNext.Enabled = lkbtnLast.Enabled = false; } else { lkbtnNext.Enabled = lkbtnLast.Enabled = true; lkbtnFirst.ForeColor = lkbtnLast.ForeColor = lkbtnNext.ForeColor = lkbtnPrevious.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0078C5"); }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lkbtnDeActive_Click(object sender, EventArgs e)
        {
            LinkButton lkbtnDeActive = (LinkButton)sender;
            _ObjNotificationsBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
            _ObjNotificationsBE.BUNoticeID = lkbtnDeActive.CommandArgument;
            _ObjNotificationsBE.Activestatus = 3;
            _ObjXmlDocument = _objNotificationsBAL.DeleteNotificationBAL(_ObjNotificationsBE, Statement.Delete);
            _ObjNotificationsBE = _ObjiDsignBAL.DeserializeFromXml<NotificationsBE>(_ObjXmlDocument);
            if (_ObjNotificationsBE.IsSuccess == 1)
            {
                BindNews();
                Message(UMS_Resource.NOTICE_DELETED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
            }
            else
                Message(UMS_Resource.NOTICE_DELETED_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
        }

        private void BindPagingDropDown(int TotalRecords)
        {
            try
            {
                objCommonMethods.BindPagingDropDownList(TotalRecords, this.PageSize, ddlPageSize);
                //ddlPageSize.Items.Clear();
                //if (TotalRecords < Constants.PageSizeStarts)
                //{
                //    ListItem item = new ListItem(Constants.PageSizeStarts.ToString(), Constants.PageSizeStarts.ToString());
                //    ddlPageSize.Items.Add(item);
                //}
                //else
                //{
                //    int previousSize = Constants.PageSizeStarts;
                //    for (int i = Constants.PageSizeStarts; i <= TotalRecords; i = i + Constants.PageSizeIncrement)
                //    {
                //        ListItem item = new ListItem(i.ToString(), i.ToString());
                //        ddlPageSize.Items.Add(item);
                //        previousSize = i;
                //    }
                //    if (previousSize < TotalRecords)
                //    {
                //        ListItem item = new ListItem((previousSize + Constants.PageSizeIncrement).ToString(), (previousSize + Constants.PageSizeIncrement).ToString());
                //        ddlPageSize.Items.Add(item);
                //    }
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            try
            {
                if (Session[UMS_Resource.CULTURE] != null)
                {
                    string culture = Session[UMS_Resource.CULTURE].ToString();

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
                }
                base.InitializeCulture();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        #endregion
    }
}