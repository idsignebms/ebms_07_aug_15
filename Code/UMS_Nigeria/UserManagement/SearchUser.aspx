﻿<%@ Page Title=":: Search User ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    Theme="Green" AutoEventWireup="true" CodeBehind="SearchUser.aspx.cs" Inherits="UMS_Nigeria.UserManagement.SearchUser" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../UserControls/UCPagingV1.ascx" TagName="UCPagingV1" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <div class="out-bor">
        <asp:UpdateProgress ID="UpdateProgress" runat="server">
            <ProgressTemplate>
                <center>
                    <div id="loading-div" class="pbloading">
                        <div id="title-loading" class="pbtitle">
                            BEDC</div>
                        <center>
                            <img src="../images/loading.GIF" class="pbimg"></center>
                        <p class="pbtxt">
                            Loading Please wait.....</p>
                    </div>
                </center>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
            PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
        <div class="inner-sec">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="text_total">
                <div class="text-heading">
                    <asp:Literal ID="litAddState" runat="server" Text="<%$ Resources:Resource, SEARCH_USER%>"></asp:Literal>
                </div>
                <div class="star_text">
                    &nbsp;
                </div>
            </div>
            <div class="clear">
            </div>
            <div class="dot-line">
            </div>
            <div class="clear">
                &nbsp;</div>
            <div class="inner-box">
                <asp:Panel ID="pnlSearchBox" runat="server" DefaultButton="btnSearch">
                    <div class="inner-box1">
                        <div class="text-inner">
                            <asp:Literal ID="litName" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                            <div class="space">
                            </div>
                            <asp:TextBox CssClass="text-box" ID="txtName" TabIndex="1" runat="server" MaxLength="300"
                                placeholder="Enter Name"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbName" runat="server" TargetControlID="txtName"
                                ValidChars=" " FilterMode="ValidChars" FilterType="LowercaseLetters,UppercaseLetters,Custom">
                            </asp:FilteredTextBoxExtender>
                            <span id="spanName" class="spancolor"></span>
                        </div>
                    </div>
                    <div class="inner-box2">
                        <div class="text-inner">
                            <asp:Literal ID="litContactNo" runat="server" Text="<%$ Resources:Resource, CONTACT_NO%>"></asp:Literal>
                            <div class="space">
                            </div>
                            <asp:TextBox CssClass="text-box" ID="txtCode1" TabIndex="2" Width="11%" runat="server"
                                MaxLength="3" placeholder="Code"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtCode1"
                                FilterType="Numbers" ValidChars=" ">
                            </asp:FilteredTextBoxExtender>
                            <asp:TextBox CssClass="text-box" ID="txtContactNo" TabIndex="3" Width="45%" runat="server"
                                MaxLength="15" placeholder="Enter Contact No"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftbTxtContactNo" runat="server" TargetControlID="txtContactNo"
                                FilterType="Numbers" ValidChars=" ">
                            </asp:FilteredTextBoxExtender>
                            <span id="spanContactNo" class="spancolor"></span>
                        </div>
                    </div>
                    <div class="inner-box3">
                        <div class="text-inner">
                            <asp:Literal ID="litRoleId" runat="server" Text="<%$ Resources:Resource, ROLE_ID%>"></asp:Literal>
                            <div class="space">
                            </div>
                            <asp:DropDownList CssClass="text-box select_box" TabIndex="4" ID="ddlRoleId" runat="server">
                                <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                            </asp:DropDownList>
                            <span id="spanRoleId" class="spancolor"></span>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                    <div class="inner-box1">
                        <div class="text-inner">
                            <asp:Literal ID="litDesignation" runat="server" Text="<%$ Resources:Resource, DESIGNATION%>"></asp:Literal>
                            <div class="space">
                            </div>
                            <asp:DropDownList CssClass="text-box select_box" TabIndex="5" ID="ddlDesignation"
                                runat="server">
                                <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                            </asp:DropDownList>
                            <span id="spanDisgnation" class="spancolor"></span>
                        </div>
                    </div>
                    <div class="clear pad_10">
                    </div>
                </asp:Panel>
                <div class="box_total">
                    <div class="box_total_a">
                        <asp:Button ID="btnSearch" TabIndex="6" Text="<%$ Resources:Resource, SEARCH%>" CssClass="box_s"
                            runat="server" OnClick="btnSearch_Click" />
                    </div>
                </div>
                <div style="clear: both;">
                    <br />
                    <br />
                </div>
            </div>
            <%--<div class="grid_boxes">
                <div class="grid_paging_top">
                    <div class="paging_top_title">
                        <asp:Literal ID="litBuList" runat="server" Text="<%$ Resources:Resource, USER_LIST%>"></asp:Literal>
                    </div>
                    <div class="paging_top_right_content" id="divLinks" runat="server">
                        <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                    </div>
                </div>
            </div>
            <div class="grid_tb" id="divgrid" runat="server">
                <asp:GridView ID="gvUsersList" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="grid_tb_hd ">
                    <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                    <EmptyDataTemplate>
                        There is no users information.
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField HeaderText="<%$ Resources:Resource, GRID_SNO%>" DataField="RowNumber" />
                        <asp:BoundField HeaderText="<%$ Resources:Resource, EMPLOYEE_ID%>" DataField="EmployeeId" />
                        <asp:BoundField HeaderText="<%$ Resources:Resource, NAME%>" DataField="Name" />
                        <asp:BoundField HeaderText="<%$ Resources:Resource, SURNAME%>" DataField="SurName" />
                        <asp:BoundField HeaderText="<%$ Resources:Resource, CONTACT_NO%>" DataField="ContactNo" />
                        <asp:BoundField HeaderText="<%$ Resources:Resource, PRIMARY_EMAIL%>" DataField="PrimaryEmailId" />
                        <asp:BoundField HeaderText="<%$ Resources:Resource, ROLE_ID%>" DataField="RoleName" />
                        <asp:BoundField HeaderText="<%$ Resources:Resource, DESIGNATION%>" DataField="Designation" />
                        <asp:BoundField HeaderText="<%$ Resources:Resource, STATUS%>" DataField="Status" />
                    </Columns>
                </asp:GridView>
            </div>
            <div class="grid_boxes">
                <div id="divDownLinks" runat="server">
                    <div class="grid_paging_bottom">
                        <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                    </div>
                </div>
            </div>--%>
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <%--Grid Starts Here--%>
                    <div class="grid_boxes">
                        <div class="grid_pagingTwo_top">
                            <div class="paging_top_title">
                                <asp:Literal ID="litCountriesList" runat="server" Text="<%$ Resources:Resource, GRID_USERS_LIST%>"></asp:Literal>
                            </div>
                            <div class="paging_top_rightTwo_content" id="divpaging" visible="false" runat="server">
                                <uc1:UCPagingV1 ID="UCPaging1" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;">
                    </div>
                    <div class="grid_tb" id="divgrid" runat="server">
                        <asp:GridView ID="gvUsersList" runat="server" AutoGenerateColumns="False" HeaderStyle-CssClass="grid_tb_hd"
                            AlternatingRowStyle-CssClass="color" OnRowCommand="gvUsersList_RowCommand" OnRowDataBound="gvUsersList_RowDataBound">
                            <EmptyDataRowStyle HorizontalAlign="Center" CssClass="color" />
                            <EmptyDataTemplate>
                                <asp:Literal ID="litNoData" runat="server" Text="<%$ Resources:Resource, THERE_IS_NO_DATA%>"></asp:Literal>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_SNO%>" runat="server"
                                    HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Center">
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblRow" runat="server" Text='<%#Eval("RowNumber")%>'></asp:Label>
                                        <asp:Label ID="lblActiveStatusId" Visible="false" runat="server" Text='<%#Eval("ActiveStatusId") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, EMPLOYEE_ID%>" ItemStyle-Width="50px"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserId" runat="server" Text='<%#Eval("EmployeeId") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, NAME%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="false" HeaderText="<%$ Resources:Resource, SURNAME%>"
                                    ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSurName" runat="server" Text='<%#Eval("SurName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, CONTACT_NO%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPrimaryContact" runat="server" Text='<%#Eval("ContactNo1") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, ANOTHER_CONTACT_NO%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" Visible="false" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSecondaryContact" runat="server" Text='<%#Eval("ContactNo2") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, PRIMARY_EMAIL%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPrimaryEmailId" runat="server" Text='<%#Eval("PrimaryEmailId") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, SECONDARY_EMAIL%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" Visible="false" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSecondaryEmaiId" runat="server" Text='<%#Eval("SecondaryEmailId") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, ROLE_ID%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRoleId" runat="server" Visible="false" Text='<%#Eval("RoleId") %>'></asp:Label>
                                        <asp:Label ID="lblRoleName" runat="server" Text='<%#Eval("RoleName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, DESIGNATION%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDesignationId" Visible="false" runat="server" Text='<%#Eval("DesignationId") %>'></asp:Label>
                                        <asp:Label ID="lblDesignationName" runat="server" Text='<%#Eval("DesignationName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="false" HeaderText="<%$ Resources:Resource, GENDER%>"
                                    ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGenderId" runat="server" Visible="false" Text='<%#Eval("GenderId") %>'></asp:Label>
                                        <asp:Label ID="lblIsMobileAccess" runat="server" Visible="false" Text='<%#Eval("IsMobileAccess") %>'></asp:Label>
                                        <asp:Label ID="lblGenderName" runat="server" Text='<%#Eval("GenderName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, ADDRESS%>" ItemStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress" runat="server" Text='<%#Eval("Address") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, DETAILS%>" ItemStyle-Width="5%"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="grid_tb_td">
                                    <ItemTemplate>
                                        <asp:Label ID="lblGridDetails" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, RESET_PASSWORD%>" ItemStyle-Width="5%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPhoto" Visible="false" runat="server" Text='<%#Eval("Photo") %>'></asp:Label>
                                        <asp:Label ID="lblPhotoPath" Visible="false" runat="server" Text='<%#Eval("FilePath") %>'></asp:Label>
                                        <asp:LinkButton ID="lkbtnResetPassword" runat="server" ToolTip="Reset Password" CommandName="ResetPassword"><img src="../images/ResetPassword.png" alt="ResetPassword" /></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:Resource, GRID_ACTION%>" ItemStyle-Width="13%"
                                    ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                       <div style="width:70px;"> <asp:LinkButton ID="lkbtnEdit" runat="server" ToolTip="Edit" Text="Edit" CommandName="EditUser"
                                            CommandArgument='<%# Eval("EmployeeId") %>'>
                                        <img src="../images/Edit.png" alt="Edit" /></asp:LinkButton>
                                        <%--<asp:LinkButton ID="lkbtnUpdate" Visible="false" runat="server" ToolTip="Update"
                            CommandName="UpdateAgency"><img src="../images/update.png" alt="Update" /></asp:LinkButton>
                        <asp:LinkButton ID="lkbtnCancel" Visible="false" runat="server" ToolTip="Cancel"
                            CommandName="CancelAgency"><img src="../images/cancel.png" alt="Cancel" /></asp:LinkButton>--%>
                                        <asp:LinkButton ID="lkbtnDelete" runat="server" ToolTip="Delete User" CommandName="DeleteUser"><img src="../images/delete.png" alt="Delete" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnActive" Visible="false" runat="server" ToolTip="Active User"
                                            CommandName="ActiveUser"><img src="../images/Activate.gif" alt="Active User" /></asp:LinkButton>
                                        <asp:LinkButton ID="lkbtnDeActive" runat="server" ToolTip="DeActive User" CommandName="DeActiveUser"><img src="../images/Deactivate.gif" alt="DeActive User" /></asp:LinkButton>
                                       </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                        <asp:HiddenField ID="hfEditDocuments" runat="server" />
                        <asp:HiddenField ID="HiddenField2" runat="server" />
                        <asp:HiddenField ID="HiddenField3" runat="server" />
                        <asp:HiddenField ID="HiddenField4" Value="0" runat="server" />
                        <asp:HiddenField ID="hfRecognize" runat="server" />
                        <asp:HiddenField ID="hfId" runat="server" />
                        <asp:HiddenField ID="hfEditmailId" runat="server" />
                        <asp:HiddenField ID="hfmailId" runat="server" />
                        <asp:HiddenField ID="hfDocuments" runat="server" />
                    </div>
                    <div class="grid_boxes">
                        <div id="divdownpaging" visible="false" runat="server">
                            <div class="grid_paging_bottom">
                                <uc1:UCPagingV1 ID="UCPaging2" runat="server" />
                            </div>
                        </div>
                    </div>
                    <%--Grid End Here--%>
                    <div id="hidepopup">
                        <%-- Active Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                            TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="PanelActivate" runat="server" CssClass="modalPopup"
                            Style="display: none; border-color: #639B00;">
                            <div class="popheader" style="background-color: #639B00;">
                                <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                            </div>
                            <br />
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <asp:Button ID="btnActiveOk" runat="server" OnClick="btnActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                    <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- Active  Btn popup Closed--%>
                        <%-- DeActive  Btn popup Start--%>
                        <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                            TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btnDeActivate" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="PanelDeActivate" runat="server" CssClass="modalPopup"
                            Style="display: none; border-color: #639B00; z-index: 1000001;">
                            <div class="popheader" style="background-color: red;">
                                <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <br />
                                    <asp:Button ID="btnDeActiveOk" runat="server" OnClick="btnDeActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                    <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                        <%-- DeActive  popup Closed--%>
                    </div>
                    <%--Edit User Start Here--%>
                    <asp:ModalPopupExtender ID="mpeEditUserpopup" runat="server" TargetControlID="hfPopup"
                        PopupControlID="pnlEditUserDetails" BackgroundCssClass="popbg" CancelControlID="btnEditCancel">
                    </asp:ModalPopupExtender>
                    <asp:Panel ID="pnlEditUserDetails" runat="server" Style="display: none;" class="editpopup_two edit">
                        <div class="out-bor">
                            <div class="inner-sec">
                                <asp:Panel ID="pnlEditMessage" runat="server">
                                    <asp:Label ID="lblEditMessage" runat="server"></asp:Label>
                                </asp:Panel>
                                <div class="text_total">
                                    <div class="text-heading">
                                        <asp:Literal ID="litEditEmployee" runat="server" Text="<%$ Resources:Resource, UPDATE_EMPLOYEE%>"></asp:Literal>
                                    </div>
                                    <div class="star_text">
                                        <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                                <div class="dot-line">
                                </div>
                                <div class="inner-box">
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditName" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                                                <span class="span_star">*</span></label>
                                            <div class="space">
                                            </div>
                                            <asp:TextBox ID="txtEditName" CssClass="text-box" runat="server" MaxLength="300"
                                                placeholder="Enter Name" onblur="return TextBoxBlurValidationlbl(this,'spanEditName','Name')"></asp:TextBox>
                                            <span id="spanEditName" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box2">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditSurname" runat="server" Text="<%$ Resources:Resource, SURNAME%>"></asp:Literal>
                                                <span class="span_star">*</span></label>
                                            <div class="space">
                                            </div>
                                            <asp:TextBox ID="txtEditSurName" CssClass="text-box" runat="server" MaxLength="300"
                                                placeholder="Enter Surname" onblur="return TextBoxBlurValidationlbl(this,'spanEditSurName','Surname')"></asp:TextBox>
                                            <span id="spanEditSurName" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box3">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditGender" runat="server" Text="<%$ Resources:Resource, GENDER%>"></asp:Literal>
                                                <span class="span_star">*</span></label>
                                            <div class="space">
                                            </div>
                                            <asp:RadioButtonList ID="rblEditGender" Width="70%" onchange="RadioButtonListlbl(this,'spanEditGender',' Select one')"
                                                RepeatDirection="Horizontal" runat="server">
                                            </asp:RadioButtonList>
                                            <div class="space">
                                                <br />
                                            </div>
                                            <span id="spanEditGender" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditRoleId" runat="server" Text="<%$ Resources:Resource, ROLE_ID%>"></asp:Literal>
                                                <span class="span_star">*</span></label>
                                            <div class="space">
                                            </div>
                                            <asp:UpdatePanel ID="upnlddlEditRoleId" runat="server">
                                                <ContentTemplate>
                                                    <asp:DropDownList ID="ddlEditRoleId" CssClass="text-box text-select" onchange=" debugger; DropDownlistOnChangelbl(this,'spanEditRoleId',' Role Id')"
                                                        runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlEditRoleId_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <div class="space">
                                                    </div>
                                                    <span id="spanEditRoleId" class="span_color"></span>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                    <div class="inner-box2">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditDesignation" runat="server" Text="<%$ Resources:Resource, DESIGNATION%>"></asp:Literal>
                                                <span class="span_star">*</span></label>
                                            <div class="space">
                                            </div>
                                            <asp:DropDownList ID="ddlEditDesignation" onchange="DropDownlistOnChangelbl(this,'spanEditDesignation',' Designation')"
                                                runat="server" CssClass="text-box text-select">
                                            </asp:DropDownList>
                                            <div class="space">
                                            </div>
                                            <span id="spanEditDesignation" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box3">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditEmployeeId" runat="server" Text="<%$ Resources:Resource, EMPLOYEE_ID%>"></asp:Literal>
                                                <span class="span_star">*</span></label>
                                            <div class="space">
                                            </div>
                                            <asp:TextBox ID="txtEditEmployeeId" Enabled="false" runat="server" MaxLength="50"
                                                placeholder="Enter Employee Id" CssClass="text-box"></asp:TextBox>
                                            <span id="spanEditEmployeeId" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditPEmail" runat="server" Text="<%$ Resources:Resource, PRIMARY_EMAIL%>"></asp:Literal>
                                                <span class="span_star" id="divEditspanMail" runat="server" visible="false">*</span>
                                            </label>
                                            <div class="space">
                                            </div>
                                            <asp:TextBox ID="txtEditPrimaryEmail" CssClass="text-box" runat="server" MaxLength="500"
                                                placeholder="Enter Primary Email Id"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtEditPrimaryEmail"
                                                ValidChars="@._" FilterType="LowercaseLetters,UppercaseLetters,Numbers,Custom">
                                            </asp:FilteredTextBoxExtender>
                                            <span id="spanEditPrimaryEmail" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box2">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditSEmail" runat="server" Text="<%$ Resources:Resource, SECONDARY_EMAIL%>"></asp:Literal>
                                            </label>
                                            <div class="space">
                                            </div>
                                            <asp:TextBox ID="txtEditSecondaryEmail" CssClass="text-box" runat="server" MaxLength="500"
                                                placeholder="Enter Secondary Email Id"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="ftbEditSEmail" runat="server" TargetControlID="txtEditSecondaryEmail"
                                                ValidChars="@._" FilterType="LowercaseLetters,UppercaseLetters,Numbers,Custom">
                                            </asp:FilteredTextBoxExtender>
                                            <span id="spanEditSecondaryEmail" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box3">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditContactNo1" runat="server" Text="<%$ Resources:Resource, CONTACT_NO%>"></asp:Literal>
                                                <span class="span_star">*</span></label>
                                            <div class="space">
                                            </div>
                                            <asp:TextBox CssClass="text-box" ID="txtEditCode1" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanEditContactNo','Code')"
                                                MaxLength="3" Width="11%" placeholder="Code"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtEditCode1"
                                                FilterType="Numbers" ValidChars=" ">
                                            </asp:FilteredTextBoxExtender>
                                            <asp:TextBox CssClass="text-box" ID="txtEditContactNo1" Width="40%" runat="server"
                                                onblur="return TextBoxBlurValidationlbl(this,'spanEditContactNo','Contact No')"
                                                MaxLength="10" placeholder="Enter Contact No"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtEditContactNo1"
                                                FilterType="Numbers" ValidChars=" ">
                                            </asp:FilteredTextBoxExtender>
                                            <span id="spanEditContactNo" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditContactNo2" runat="server" Text="<%$ Resources:Resource, ANOTHER_CONTACT_NO%>"></asp:Literal>
                                            </label>
                                            <div class="space">
                                            </div>
                                            <asp:TextBox CssClass="text-box" ID="txtEditCode2" runat="server" MaxLength="3" Width="11%"
                                                placeholder="Code"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtEditCode2"
                                                FilterType="Numbers" ValidChars=" ">
                                            </asp:FilteredTextBoxExtender>
                                            <asp:TextBox CssClass="text-box" ID="txtEditContactNo2" Width="40%" runat="server"
                                                MaxLength="10" placeholder="Enter Alternate Contact No"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtEditContactNo2"
                                                FilterType="Numbers" ValidChars=" ">
                                            </asp:FilteredTextBoxExtender>
                                            <span id="spanEditContactNo2" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box2">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditAddress" runat="server" Text="<%$ Resources:Resource, ADDRESS%>"></asp:Literal>
                                                <span class="span_star">*</span></label>
                                            <div class="space">
                                            </div>
                                            <asp:TextBox ID="txtEditAddress" runat="server" CssClass="text-box" placeholder="Enter Address"
                                                TextMode="MultiLine" onblur="return TextBoxBlurValidationlbl(this,'spanEditAddress','Address')"></asp:TextBox>
                                            <span id="spanEditAddress" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box3">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditDetails" runat="server" Text="<%$ Resources:Resource, Details%>"></asp:Literal></label>
                                            <div class="space">
                                            </div>
                                            <asp:TextBox ID="txtEditDetails" runat="server" CssClass="text-box" TextMode="MultiLine"
                                                placeholder="Enter Details"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditPhoto" runat="server" Text="<%$ Resources:Resource, PHOTO%>"></asp:Literal>
                                            </label>
                                            <div class="space">
                                            </div>
                                            <asp:FileUpload ID="fupEditPhoto" runat="server" onchange="return fileEditvalidate(this.value,'Photo')"
                                                CssClass="fltl" />&nbsp; <span id="spanfupEditPhoto" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="inner-box2">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="litEditDocuments" runat="server" Text="<%$ Resources:Resource, DOCUMENTS%>"></asp:Literal>
                                            </label>
                                            <div class="space">
                                            </div>
                                            <div id="divEditFile">
                                                <asp:FileUpload ID="fupEditDocument" runat="server" onchange="return pdffileEditvalidate(this.value,'Documents')"
                                                    CssClass="fltl" />&nbsp;
                                                <asp:LinkButton ID="lnkEditAddFiles" runat="server" CssClass="fltl" OnClientClick="return AddEditFile()"
                                                    Font-Bold="True">Add</asp:LinkButton><br />
                                                <span id="spanEditDocuments" class="span_color"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inner-box1">
                                        <div class="text-inner">
                                            <label for="name">
                                                <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, ISMOBILE_ACCESS%>"></asp:Literal>
                                                <span class="span_star">*</span></label>
                                            <div class="space">
                                            </div>
                                            <asp:RadioButtonList ID="rblEditIsMobileAccess" Width="70%" RepeatDirection="Horizontal"
                                                runat="server">
                                                <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                            </asp:RadioButtonList>
                                            <div class="space">
                                                <br />
                                            </div>
                                            <span id="spanEditIsMobileAccess" class="span_color"></span>
                                        </div>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <div class="consumer_feild">
                                        <div class="consume_input editlist" id="div1" style="width: 98%; padding: 5px;">
                                            <div class="photodocumentFullWidth">
                                                <h2 id="divSubhead" runat="server" class="subhead">
                                                    <asp:Literal ID="litDownloadDocs" Visible="false" runat="server" Text="<%$ Resources:Resource, DOWNLOAD_UPLOADED_DOCUMENTS%>"></asp:Literal><br />
                                                    <br />
                                                </h2>
                                                <div id="divPhoto" runat="server" visible="false">
                                                    <asp:UpdatePanel ID="upnlPhotos" runat="server">
                                                        <ContentTemplate>
                                                            <div class="fltl">
                                                                <asp:Label runat="server" ID="lblPhotoId"></asp:Label>
                                                                <asp:Label runat="server" Visible="false" ID="lblPhotoUserId"></asp:Label>&nbsp&nbsp&nbsp&nbsp
                                                            </div>
                                                            <div class="fltr" style="width: 180px;">
                                                                <asp:LinkButton ID="lbtnPhotoDown" runat="server" OnClick="lbtnPhotoDown_Click" ForeColor="Green">Download</asp:LinkButton>&nbsp&nbsp&nbsp&nbsp
                                                                <asp:LinkButton ID="lbtnPhotoDelete" ForeColor="Red" OnClick="lbtnPhotoDelete_Click"
                                                                    runat="server">Delete</asp:LinkButton>
                                                            </div>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="lbtnPhotoDown" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </div>
                                                <asp:Repeater ID="rptrDocuments" runat="server">
                                                    <ItemTemplate>
                                                        <asp:UpdatePanel ID="upnlDocs" runat="server">
                                                            <ContentTemplate>
                                                                <div class="fltl">
                                                                    <asp:Label runat="server" Visible="false" ID="lblDocId" Text='<%#Eval("UserDocumentId") %>'></asp:Label>
                                                                    <asp:Label runat="server" ID="lblDocName" Text='<%#Eval("DocumentName") %>'></asp:Label>
                                                                </div>
                                                                <div class="fltr">
                                                                    <asp:LinkButton ID="lbtnDocName" runat="server" ForeColor="Green" OnClick="lbtnDocName_Click"
                                                                        CommandArgument='<%#Eval("DocumentName") %>'>Download</asp:LinkButton>&nbsp&nbsp&nbsp&nbsp
                                                                    <asp:LinkButton ID="lbtnDocDelete" ForeColor="Red" OnClick="lbtnDocDelete_Click"
                                                                        runat="server" CommandArgument='<%#Eval("UserDocumentId") %>'>Delete</asp:LinkButton>&nbsp&nbsp&nbsp&nbsp
                                                                </div>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="lbtnDocName" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:UpdatePanel ID="upnlEditPermissions" runat="server">
                                    <ContentTemplate>
                                        <%--BU Start--%>
                                        <div id="divEditBusinessUnits" visible="false" runat="server">
                                            <div class="check_baa">
                                                <div class="text-inner_a">
                                                    <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNITS%>"></asp:Literal>
                                                </div>
                                                <div class="check_ba">
                                                    <div class="text-inner mster-input">
                                                        <asp:CheckBox runat="server" ID="BUcbEditAll" Text="All" onclick="BUEditCheckAll();" />
                                                        <asp:CheckBoxList ID="cblEditBusinessUnits" Width="30%" RepeatDirection="Horizontal"
                                                            onclick="BUEditUnCheckAll();" RepeatColumns="3" runat="server">
                                                        </asp:CheckBoxList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                        <div id="divEditNoBU" runat="server" visible="false" class="span_color">
                                            There is no Business Units please select another BusinessUnit.
                                        </div>
                                        <%--BU END--%>
                                        <div class="clr">
                                        </div>
                                        <%--BU,SU,SC Start--%>
                                        <div id="divEditServiceCenters" visible="false" runat="server">
                                            <div class="inner-box1">
                                                <div class="text-inner">
                                                    <label for="name">
                                                        <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNITS%>"></asp:Literal>
                                                        <span class="span_star">*</span>
                                                    </label>
                                                    <div class="space">
                                                    </div>
                                                    <asp:DropDownList ID="ddlEditBusinessUnits" CssClass="text-box select_box" runat="server"
                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlEditBusinessUnits_SelectedIndexChanged">
                                                        <asp:ListItem Text="Select Business Unit" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <span class="span_color" id="spanEditddlBusinessUnits"></span>
                                                </div>
                                            </div>
                                            <div class="clr">
                                            </div>
                                            <div class="inner-box1">
                                                <div class="text-inner">
                                                    <label for="name">
                                                        <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:Resource, SERVICE_UNITS%>"></asp:Literal>
                                                        <span class="span_star">*</span></label>
                                                    <div class="space">
                                                    </div>
                                                    <asp:DropDownList ID="ddlEditServiceUnits" CssClass="text-box select_box" Enabled="false"
                                                        runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlEditServiceUnits_SelectedIndexChanged">
                                                        <asp:ListItem Text="Select Service Unit" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <span class="span_color" id="spanEditddlServiceUnits"></span>
                                                </div>
                                            </div>
                                            <div class="clr">
                                            </div>
                                            <div id="lblEditSC" runat="server" visible="false">
                                                <div class="check_baa">
                                                    <div class="text-inner_a">
                                                        <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTERS%>"></asp:Literal>
                                                        <span class="span_star">*</span>
                                                    </div>
                                                    <div class="check_ba">
                                                        <div class="text-inner mster-input">
                                                            <asp:CheckBox runat="server" Visible="false" ID="SCcbEditAll" Text="All" onclick="SCEditCheckAll();"
                                                                AutoPostBack="true" OnCheckedChanged="cblEditServiceCenters_SelectedIndexChanged" />
                                                            <div id="divEditCblSC" runat="server">
                                                                <asp:CheckBoxList ID="cblEditServiceCenters" onclick="SCEditUnCheckAll();" Width="45%"
                                                                    AutoPostBack="true" OnSelectedIndexChanged="cblEditServiceCenters_SelectedIndexChanged"
                                                                    RepeatDirection="Horizontal" RepeatColumns="3" runat="server">
                                                                </asp:CheckBoxList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divEditNoSC" runat="server" visible="false" class="span_color">
                                            There is no Service Centers please select another ServiceUnit.
                                        </div>
                                        <%--BU,SU,SC END--%>
                                        <div class="clr">
                                        </div>
                                        <%--Cash Offices Start--%>
                                        <div id="divEditCashOffices" visible="false" runat="server">
                                            <div class="check_baa">
                                                <div class="text-inner_a">
                                                    <asp:Literal ID="Literal22" runat="server" Text="<%$ Resources:Resource, CASH_OFFICES%>"></asp:Literal>
                                                    <span class="span_star">*</span>
                                                </div>
                                                <div class="check_ba">
                                                    <div class="text-inner mster-input">
                                                        <asp:CheckBox runat="server" ID="COcbEditAll" Text="All" onclick="COEditCheckAll();" />
                                                        <div class="space">
                                                        </div>
                                                        <asp:CheckBoxList ID="cblEditCashOffices" onclick="COEditUnCheckAll();" Width="45%"
                                                            RepeatDirection="Horizontal" RepeatColumns="3" runat="server">
                                                        </asp:CheckBoxList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                        <div id="divEditNoCO" runat="server" visible="false" class="span_color">
                                            There is no Cash Offices please select another ServiceCenter.
                                        </div>
                                        <%--Cash Offices End--%>
                                        <%--ServiceUnits Start--%>
                                        <div id="divEditServiceUnits" visible="false" runat="server">
                                            <div class="inner-box1">
                                                <div class="text-inner">
                                                    <label for="name">
                                                        <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNITS%>"></asp:Literal>
                                                        <span class="span_star">*</span></label>
                                                    <div class="space">
                                                    </div>
                                                    <asp:DropDownList ID="ddlEditBUServiceUnits" CssClass="text-box select_box" runat="server"
                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlEditBUServiceUnits_SelectedIndexChanged">
                                                        <asp:ListItem Text="Select Business Unit" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="clr">
                                            </div>
                                            <div class="check_baa">
                                                <div class="text-inner_a">
                                                    <asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:Resource, SERVICE_UNITS%>"></asp:Literal><span
                                                        class="span_star">*</span>
                                                </div>
                                                <div class="check_ba">
                                                    <div class="text-inner mster-input">
                                                        <asp:CheckBox runat="server" Visible="false" ID="SUcbEditAll" Text="All" onclick="SUEditCheckAll();" />
                                                        <div id="divEditCblSU" runat="server">
                                                            <asp:CheckBoxList ID="cblEditServiceUnits" onclick="SUEditUnCheckAll();" Width="45%"
                                                                RepeatDirection="Horizontal" RepeatColumns="3" runat="server">
                                                            </asp:CheckBoxList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                        <div id="divEditNoSU" runat="server" visible="false" class="span_color">
                                            There is no Service Units please select another BusinessUnit.
                                        </div>
                                        <%--ServiceUnits End--%>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div class="space">
                                </div>
                                <%--<div class="consumer_feild" runat="server" id="divEditsplPermissions">
                                    <div class="check_baa" style="width: 91%;">
                                        <div class="text-inner_a">
                                            <asp:Literal ID="litEditSpecialPermissions" runat="server" Text="<%$ Resources:Resource, SPECIAL_PERMISSIONS%>"></asp:Literal>
                                        </div>
                                        <div class="check_ba">
                                            <div class="text-inner">
                                                <asp:CheckBox ID="chkEditCustomerModify" Text="<%$ Resources:Resource, CUSTOMER_DETAILS_MODIFICATION%>"
                                                    runat="server" />
                                            </div>
                                        </div>
                                        <div class="check_bb">
                                            <div class="text-inner">
                                                <asp:CheckBox ID="chkEditBillAdjustment" Text="<%$ Resources:Resource, BILL_ADJUSTMENT%>"
                                                    runat="server" />
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                        <div class="check_ba">
                                            <div class="text-inner">
                                                <asp:CheckBox ID="chkEditTariffAdjustment" Text="<%$ Resources:Resource, TARIFF_ADJUSTMENTS%>"
                                                    runat="server" />
                                            </div>
                                        </div>
                                        <div class="check_bb">
                                            <div class="text-inner">
                                                <asp:CheckBox ID="chkEditPaymentAdjustment" Text="<%$ Resources:Resource, PAYMENTS_ADJUSTMENT%>"
                                                    runat="server" />
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                        <div class="check_ba">
                                            <div class="text-inner">
                                                <asp:CheckBox ID="chkEditDisconnection" Text="<%$ Resources:Resource, DISCONNECTIONS%>"
                                                    runat="server" />
                                            </div>
                                        </div>
                                        <div class="check_bb">
                                            <div class="text-inner">
                                                <asp:CheckBox ID="chkEditReconnection" Text="<%$ Resources:Resource, RECONNECTIONS%>"
                                                    runat="server" />
                                            </div>
                                        </div>
                                        <div class="clr">
                                        </div>
                                        <div class="check_ba">
                                            <div class="text-inner">
                                                <asp:CheckBox ID="chkEditBillGeneration" Text="<%$ Resources:Resource, BILL_GENERATION%>"
                                                    runat="server" />
                                            </div>
                                        </div>
                                        <div class="check_bb">
                                            <div class="text-inner">
                                                &nbsp;
                                            </div>
                                        </div>
                                    </div>
                                </div>--%>
                                <div class="box_total">
                                    <div class="box_totalTwi_c">
                                        <asp:Button ID="btnUpdate" OnClick="btnUpdate_Click" OnClientClick="return EditValidate();"
                                            Text="<%$ Resources:Resource, UPDATE%>" CssClass="box_s" runat="server" />
                                        <asp:Button ID="btnEditCancel" Text="<%$ Resources:Resource, CANCEL%>" CssClass="box_s"
                                            runat="server" />
                                    </div>
                                    <div style="clear: both;">
                                    </div>
                                    <div class="box_totalTwo_b">
                                        <div class="text-heading_2">
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:ModalPopupExtender ID="mpeDocpopup" runat="server" PopupControlID="pnldeletedoc"
                            TargetControlID="btndeletedoc" BackgroundCssClass="modalBackground" CancelControlID="btnCanceldoc">
                        </asp:ModalPopupExtender>
                        <asp:Button ID="btndeletedoc" runat="server" Text="Button" Style="display: none;" />
                        <asp:Panel ID="pnldeletedoc" runat="server" CssClass="modalPopup"
                            Style="display: none; border-color: #639B00; z-index: 1000001;">
                            <div class="popheader" style="background-color: red;">
                                <asp:Label ID="lblMsg" runat="server"></asp:Label>
                            </div>
                            <div class="footer" align="right">
                                <div class="fltr popfooterbtn">
                                    <br />
                                    <asp:Button ID="btndeleteok" runat="server" OnClick="btndeleteok_Click" Text="<%$ Resources:Resource, OK%>"
                                        CssClass="ok_btn" />
                                    <asp:Button ID="btnCanceldoc" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                        CssClass="cancel_btn" />
                                </div>
                                <div class="clear pad_5">
                                </div>
                            </div>
                        </asp:Panel>
                    </asp:Panel>
                    <%--Edit User End Here--%>
                    <%--Password Popup Start Here--%>
                    <asp:ModalPopupExtender ID="mpeResetPwdpopup" runat="server" TargetControlID="hfPopup1"
                        PopupControlID="pnlChangePwd" BackgroundCssClass="popbg" CancelControlID="btnCancel">
                    </asp:ModalPopupExtender>
                    <asp:Panel ID="pnlChangePwd" runat="server" CssClass="editpopup_two" Style="display: none">
                        <h3 class="edithead" align="left">
                            <asp:Literal ID="Literal26" runat="server" Text="<%$ Resources:Resource, RESET_PASSWORD%>"></asp:Literal>
                        </h3>
                        <div class="clear">
                            &nbsp;</div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="litEditPassword" runat="server" Text="<%$ Resources:Resource, PASSWORD%>"></asp:Literal><span>*</span>
                            </div>
                            <div class="consume_input" style="width: 350px">
                                <%--onblur="return TextBoxBlurValidationlbl(this,'spanEditPassword','Password')"--%>
                                <asp:TextBox ID="txtEditPassword" runat="server" CssClass="text-box" TextMode="Password"
                                    placeholder="Enter Password"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revTxtEditPassword" runat="server" ControlToValidate="txtEditPassword"
                                    ValidationExpression="" ForeColor="red" ErrorMessage="The new password does not meet the length and complexity requierment.">
                                </asp:RegularExpressionValidator>
                                <span id="spanEditPassword" class="span_color"></span>
                                <div class="">
                                </div>
                            </div>
                        </div>
                        <div class="clear pad_5">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_name">
                                <asp:Literal ID="litEditReEnterPassword" runat="server" Text="<%$ Resources:Resource, REENTER_PASSWORD%>"></asp:Literal><span>*</span>
                            </div>
                            <div class="consume_input" style="width: 350px">
                                <asp:TextBox ID="txtEditReEnterPassword" CssClass="text-box" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanEditReEnterPassword','Re-Enter Password')"
                                    TextMode="Password" placeholder="Re-Enter Password"></asp:TextBox>
                                <span id="spanEditReEnterPassword" class="span_color"></span>
                            </div>
                        </div>
                        <div class="clear pad_10">
                        </div>
                        <div class="consumer_feild">
                            <div style="margin-left: 156px; overflow: auto; float: left;">
                                <asp:Button ID="btnChangePwd" OnClientClick="return EditPasswordValidate();" OnClick="btnChangePwd_Click"
                                    Text="<%$ Resources:Resource, RESET%>" CssClass="calculate_but" runat="server" />
                                <asp:Button ID="btnCancel" Text="<%$ Resources:Resource, CANCEL%>" CssClass="calculate_but"
                                    runat="server" />
                            </div>
                            <br />
                            <div class="clear">
                            </div>
                            <br />
                            <div id="divPasswordResetValidation" runat="server">
                            </div>
                    </asp:Panel>
                    <%--Password Popup End Here--%>
                    <asp:HiddenField ID="hfPopup" runat="server" />
                    <asp:HiddenField ID="hfdocid" runat="server" />
                    <asp:HiddenField ID="hfPopup1" runat="server" />
                    <%-- Alert popup Start--%>
                    <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="PanelAlert"
                        TargetControlID="btnAlertDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnAlertOk">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btnAlertDeActivate" runat="server" Text="Button" CssClass="popbtn" />
                    <asp:Panel ID="PanelAlert" runat="server" CssClass="modalPopup" class="poppnl" Style="display: none;">
                        <div class="popheader popheaderlblred" id="pop" runat="server">
                            <asp:Label ID="lblAlertMsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <br />
                                <asp:Button ID="btnAlertOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="cancel_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                        TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
                    </asp:ModalPopupExtender>
                    <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
                    <asp:Panel ID="Panelgridalert" runat="server" DefaultButton="btngridalertok" CssClass="modalPopup"
                        Style="display: none;">
                        <div class="popheader popheaderlblred">
                            <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                        </div>
                        <div class="footer popfooterbtnrt">
                            <div class="fltr popfooterbtn">
                                <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                                    CssClass="ok_btn" />
                            </div>
                            <div class="clear pad_5">
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- Alert popup Closed--%>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnUpdate" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <asp:HiddenField ID="hfPageSize" runat="server" />
        <asp:HiddenField ID="hfPageNo" runat="server" />
        <asp:HiddenField ID="hfLastPage" runat="server" />
        <asp:HiddenField ID="hfTotalRecords" Value="0" runat="server" />
        <asp:HiddenField ID="hfName" runat="server" />
        <asp:HiddenField ID="hfContactNo" runat="server" />
        <asp:HiddenField ID="hfRoleId" runat="server" />
        <asp:HiddenField ID="hfDesignation" runat="server" />
    </div>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/AddUsers.js" type="text/javascript"></script>
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);
        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopupLoading.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }
   
    </script>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Validation script ref ends==============--%>
      <script language="javascript" type="text/javascript">
          $(document).ready(function () { assignDiv('rptrMainMasterMenu_162_4', '163') });
        </script>
</asp:Content>
