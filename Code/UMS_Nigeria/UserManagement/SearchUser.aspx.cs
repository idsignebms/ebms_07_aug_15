﻿#region Namespaces
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Resources;
using System.Threading;
using System.Globalization;
using UMS_NigeriaBE;
using iDSignHelper;
using UMS_NigeriaBAL;
using UMS_NigriaDAL;
using System.Data;
using System.Xml;
using System.IO;
using System.Configuration;
using System.Drawing;
using System.Xml.Linq;
using System.Web.UI.HtmlControls; 
#endregion

namespace UMS_Nigeria.UserManagement
{
    public partial class SearchUser : System.Web.UI.Page
    {
        #region Members
        CommonMethods objCommonMethods = new CommonMethods();
        iDsignBAL objIdsignBal = new iDsignBAL();
        ReportsBal _objRerpotsBal = new ReportsBal();
        string Key = "SearchEmployee";
        public int PageNum;
        UserManagementBAL objUserManagementBAL = new UserManagementBAL();
        XmlDocument xml = null;
        ConsumerBal objConsumerBal = new ConsumerBal();
        PasswordStrengthBAL objPasswordStrengthBAL = new PasswordStrengthBAL();
        #endregion

        #region Properties
        public int PageSize
        {
            get { return string.IsNullOrEmpty(hfPageSize.Value) ? Constants.PageSizeStarts : Convert.ToInt32(hfPageSize.Value); }
            //get { return string.IsNullOrEmpty(ddlPageSize.SelectedValue) ? Constants.PageSizeStartsReport : Convert.ToInt32(ddlPageSize.SelectedValue); }
        }
        private int Designation
        {
            get { return string.IsNullOrEmpty(ddlDesignation.SelectedValue) ? Constants.Zero : Convert.ToInt32(ddlDesignation.SelectedValue); }
        }
        private int Role
        {
            get { return string.IsNullOrEmpty(ddlRoleId.SelectedValue) ? Constants.Zero : Convert.ToInt32(ddlRoleId.SelectedValue); }
        }

        private string Name
        {
            get { return txtName.Text.Trim(); }
            set { txtName.Text = value; }
        }
        private string ContactNo
        {
            get { return txtContactNo.Text; }
            set { txtContactNo.Text = value; }
        }
        private string Code1
        {
            get { return txtCode1.Text; }
            set { txtCode1.Text = value; }
        }
        private string RoleId
        {
            get { return string.IsNullOrEmpty(ddlRoleId.SelectedValue) ? Constants.SelectedValue : ddlRoleId.SelectedValue; }
            set { ddlRoleId.SelectedValue = value.ToString(); }
        }
        private string UserName
        {
            get { return txtEditEmployeeId.Text.Trim(); }
            set { txtEditEmployeeId.Text = value; }
        }
        public string EditSC
        {
            get { return objCommonMethods.CollectSelectedItemsValues(cblEditServiceCenters, ","); }
        }
        #endregion

        #region Events
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Session[UMS_Resource.SESSION_LOGINID] == null)
                Response.Redirect(UMS_Resource.DEFAULT_PAGE);

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = pnlMessage.CssClass = string.Empty;
            if (!IsPostBack)
            {
                //string path = string.Empty;
                //path = objCommonMethods.GetPagePath(this.Request.Url);
                //if (objCommonMethods.IsAccess(path))
                //{
                BindDropDowns();
                hfPageNo.Value = Constants.pageNoValue.ToString();
                BindGrid();
                //BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                //CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                //string roleId = Session[UMS_Resource.SESSION_ROLEID].ToString();
                //}
                //else
                //{
                //    Response.Redirect(UMS_Resource.ADD_UN_AUTHORIZED_PAGE);
                //}
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                hfName.Value = txtName.Text.Trim();
                hfContactNo.Value = txtCode1.Text.Trim() + txtContactNo.Text.Trim();
                hfRoleId.Value = this.Role.ToString();
                hfDesignation.Value = this.Designation.ToString();
                BindGrid();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void cblEditServiceCenters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cblEditCashOffices.Items.Clear();
                divEditNoCO.Visible = divEditCashOffices.Visible = false;
                if (!string.IsNullOrEmpty(EditSC))
                {
                    //objCommonMethods.BindUserServiceCenters_SuIds(cblServiceCenters, SU, Resource.DDL_ALL, false);
                    objCommonMethods.BindOfficeByBU_ID(cblEditCashOffices, ddlEditBusinessUnits.SelectedValue, ddlEditServiceUnits.SelectedValue, EditSC, true, false, Resource.DDL_ALL);
                    if (cblEditCashOffices.Items.Count > 0)
                        divEditCashOffices.Visible = cblEditCashOffices.Visible = true;
                    else
                    {
                        cblEditCashOffices.Visible = false;
                        divEditNoCO.Visible = true;
                    }
                }
                mpeEditUserpopup.Show();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }
        protected void ddlEditRoleId_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //divEditsplPermissions.Visible = true;
                divEditNoBU.Visible = false;
                lblEditSC.Visible = BUcbEditAll.Checked = SCcbEditAll.Checked = COcbEditAll.Checked = SUcbEditAll.Checked = false;
                divEditCashOffices.Visible = divEditBusinessUnits.Visible = divEditServiceCenters.Visible = divEditServiceUnits.Visible = false;
                SUcbEditAll.Visible = SCcbEditAll.Visible = false;

                //if (ddlEditRoleId.SelectedIndex < 12)
                if (!string.IsNullOrEmpty(ddlEditRoleId.SelectedValue))
                //if (Convert.ToInt32(ddlEditRoleId.SelectedValue) < 12)//Commented by Karteek on 15 Apr 2015// Dont know why we are checking with >12 
                {
                    //if (ddlEditRoleId.SelectedIndex > 0)
                    if (Convert.ToInt32(ddlEditRoleId.SelectedValue) > 0)
                    {
                        //Start To get the BUS based on the login //Karteek
                        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        {
                            cblEditBusinessUnits.Items.Clear();
                            ListItem liBU = new ListItem(Session[UMS_Resource.SESSION_USER_BUNAME].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());
                            liBU.Selected = true;
                            cblEditBusinessUnits.Items.Add(liBU);
                            BUcbEditAll.Visible = false;
                        }
                        else
                            objCommonMethods.BindBusinessUnits(cblEditBusinessUnits, string.Empty, false);

                        if (cblEditBusinessUnits.Items.Count > 0)
                        {
                            divEditBusinessUnits.Visible = true;
                            divEditNoBU.Visible = false;
                        }
                        else
                        {
                            divEditBusinessUnits.Visible = false;
                            divEditNoBU.Visible = true;
                        }
                        //ENd To get the BUS based on the login

                        //Commented BY Karteek
                        //switch (Convert.ToInt32(ddlEditRoleId.SelectedValue))
                        //{
                        //    case (int)Roles.CommercialManagerRoleId:
                        //    case (int)Roles.BUManagerRoleId:
                        //    case (int)Roles.Admin:
                        //        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        //        {
                        //            cblEditBusinessUnits.Items.Clear();
                        //            ListItem liBU = new ListItem(Session[UMS_Resource.SESSION_USER_BUNAME].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());
                        //            liBU.Selected = true;
                        //            cblEditBusinessUnits.Items.Add(liBU);
                        //            BUcbEditAll.Visible = false;
                        //        }
                        //        else
                        //            objCommonMethods.BindBusinessUnits(cblEditBusinessUnits, string.Empty, false);
                        //        divEditBusinessUnits.Visible = true;
                        //        divEditNoBU.Visible = divEditCashOffices.Visible = divEditServiceCenters.Visible = divEditServiceUnits.Visible = false;
                        //        //divEditDistricts.Visible =divEditDistricts.Visible =divEditDistricts.Visible =divEditDistricts.Visible =
                        //        break;
                        //    case (int)Roles.CashierRoleId:
                        //        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        //        {
                        //            string BUID = Session[UMS_Resource.SESSION_USER_BUID].ToString();
                        //            ddlEditBusinessUnits.SelectedIndex = ddlEditBusinessUnits.Items.IndexOf(ddlEditBusinessUnits.Items.FindByValue(Session[UMS_Resource.SESSION_USER_BUID].ToString()));
                        //            ddlEditBusinessUnits.Enabled = false;
                        //            ddlEditBusinessUnits_SelectedIndexChanged(ddlBusinessUnits, new EventArgs());
                        //            ddlEditServiceUnits.Enabled = true;
                        //        }
                        //        else
                        //        {
                        //            ddlEditBusinessUnits.SelectedIndex = ddlEditServiceUnits.SelectedIndex = Constants.Zero;
                        //            ddlEditServiceUnits.Enabled = false;
                        //        }
                        //        //objCommonMethods.BindOffice(cblEditCashOffices, false);
                        //        cblEditServiceCenters.DataSource = new DataTable();
                        //        cblEditServiceCenters.DataBind();
                        //        //divEditCashOffices.Visible = 
                        //        divEditServiceCenters.Visible = true;
                        //        divEditNoBU.Visible = divEditBusinessUnits.Visible = divEditServiceUnits.Visible = false;
                        //        break;
                        //    case (int)Roles.DataEntryOperatorRoleId:
                        //        //objCommonMethods.BindDistricts(cblEditDistricts, string.Empty, false);
                        //        if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                        //        {
                        //            cblEditBusinessUnits.Items.Clear();
                        //            ListItem liBU = new ListItem(Session[UMS_Resource.SESSION_USER_BUNAME].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());
                        //            liBU.Selected = true;
                        //            cblEditBusinessUnits.Items.Add(liBU);
                        //            BUcbEditAll.Visible = false;
                        //        }
                        //        else
                        //            objCommonMethods.BindBusinessUnits(cblEditBusinessUnits, string.Empty, false);
                        //        divEditBusinessUnits.Visible = true;
                        //        divEditNoBU.Visible = divEditCashOffices.Visible = divEditServiceCenters.Visible = divEditServiceUnits.Visible = false;
                        //        break;
                        //    case (int)Roles.SUManagerRoleId:
                        //        //ddlEditBUServiceUnits.SelectedIndex = Constants.Zero;
                        //        cblEditServiceUnits.DataSource = new DataTable();
                        //        cblEditServiceUnits.DataBind();
                        //        divEditServiceUnits.Visible = true;
                        //        divEditNoBU.Visible = divEditCashOffices.Visible = divEditBusinessUnits.Visible = divEditServiceCenters.Visible = false;
                        //        ddlEditBUServiceUnits_SelectedIndexChanged(ddlEditBUServiceUnits.SelectedValue, new EventArgs());
                        //        break;
                        //    case (int)Roles.MeterReader:
                        //        divEditNoBU.Visible = false;
                        //        //divEditsplPermissions.Visible = false;
                        //        break;
                        //    default:
                        //        divEditNoBU.Visible = divEditCashOffices.Visible = divEditBusinessUnits.Visible = divEditServiceCenters.Visible = divEditServiceUnits.Visible = false;
                        //        break;
                        //}
                        //hfEditmailId.Value = string.Empty;
                        //DataSet dsMailId = new DataSet();
                        //hfmailId.Value = 1.ToString();
                        //divspanMailId.Visible = true;
                    }
                    else
                    {
                        divEditNoBU.Visible = divEditCashOffices.Visible = divEditBusinessUnits.Visible = divEditServiceCenters.Visible = divEditServiceUnits.Visible = false;
                    }
                }
                //else
                //{
                //    hfEditmailId.Value = 1.ToString();
                //    divEditspanMail.Visible = true;

                //    if (Session[UMS_Resource.SESSION_USER_BUID] != null)
                //    {
                //        cblEditBusinessUnits.Items.Clear();
                //        ListItem liBU = new ListItem(Session[UMS_Resource.SESSION_USER_BUNAME].ToString(), Session[UMS_Resource.SESSION_USER_BUID].ToString());
                //        liBU.Selected = true;
                //        cblEditBusinessUnits.Items.Add(liBU);
                //        BUcbEditAll.Visible = false;
                //    }
                //    else
                //        objCommonMethods.BindBusinessUnits(cblEditBusinessUnits, string.Empty, false);
                //    divEditBusinessUnits.Visible = true;
                //    divEditNoBU.Visible = divEditCashOffices.Visible = divEditServiceCenters.Visible = divEditServiceUnits.Visible = false;

                //}
                mpeEditUserpopup.Show();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlEditBusinessUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divEditNoSC.Visible = SCcbEditAll.Visible = false;
                if (ddlEditBusinessUnits.SelectedIndex > 0)
                {
                    objCommonMethods.BindServiceUnits(ddlEditServiceUnits, ddlEditBusinessUnits.SelectedValue, true);
                    ddlEditServiceUnits.Enabled = true;

                }
                else
                {
                    ddlEditServiceUnits.SelectedIndex = Constants.Zero;
                    ddlEditServiceUnits.Enabled = false;
                    divEditNoSC.Visible = true;
                }
                cblEditServiceCenters.DataSource = new DataTable();
                cblEditServiceCenters.DataBind();
                mpeEditUserpopup.Show();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlEditServiceUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lblEditSC.Visible = divEditNoSC.Visible = SCcbEditAll.Checked = false;
                if (ddlEditServiceUnits.SelectedIndex > 0)
                {

                    //objCommonMethods.BindServiceCenters(cblEditServiceCenters, ddlEditServiceUnits.SelectedValue, false);

                    ConsumerBe objConsumerBe = new ConsumerBe();
                    objConsumerBe.SU_ID = ddlEditServiceUnits.SelectedValue;
                    objConsumerBe.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Get(objConsumerBe, ReturnType.Single));
                    if (objConsumerListBe.items.Count > 0)
                    {
                        objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), cblEditServiceCenters, "ServiceCenterName", "ServiceCenterId");
                        lblEditSC.Visible = divEditCblSC.Visible = SCcbEditAll.Visible = true;
                    }
                    else
                    {
                        divEditCblSC.Visible = SCcbEditAll.Visible = false;
                        divEditNoSC.Visible = true;
                    }
                }
                else
                {
                    SCcbEditAll.Visible = false;
                    cblEditServiceCenters.DataSource = new DataTable();
                    cblEditServiceCenters.DataBind();
                    divEditNoSC.Visible = false;
                }
                mpeEditUserpopup.Show();
            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void ddlEditBUServiceUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divEditNoSU.Visible = SUcbEditAll.Checked = false;
                if (ddlEditBUServiceUnits.SelectedIndex > 0)
                {
                    //objCommonMethods.BindServiceUnits(cblEditServiceUnits, ddlEditBUServiceUnits.SelectedValue, true);

                    ConsumerBe objConsumerBe = new ConsumerBe();
                    objConsumerBe.BU_ID = ddlEditBUServiceUnits.SelectedValue;
                    objConsumerBe.UserId = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    ConsumerListBe objConsumerListBe = objIdsignBal.DeserializeFromXml<ConsumerListBe>(objConsumerBal.Get(objConsumerBe, ReturnType.Double));
                    if (objConsumerListBe.items.Count > 0)
                    {
                        objIdsignBal.FillCheckBoxList(objIdsignBal.ConvertListToDataSet<ConsumerBe>(objConsumerListBe.items), cblEditServiceUnits, "ServiceUnitName", "SU_ID");
                        ddlEditServiceUnits.Enabled = SUcbEditAll.Visible = divEditCblSC.Visible = true;
                    }
                    else
                    {
                        divEditNoSU.Visible = ddlEditServiceUnits.Enabled = true;
                        SUcbEditAll.Visible = divEditCblSU.Visible = false;
                    }
                }
                else
                {
                    SUcbEditAll.Visible = false;
                    cblEditServiceUnits.DataSource = new DataTable();
                    cblEditServiceUnits.DataBind();
                    divEditNoSU.Visible = true;
                }
                mpeEditUserpopup.Show();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void setPasswordValidation()
        {
            PasswordStrengthBE objPasswordStrengthBE = new PasswordStrengthBE();
            PasswordStrengthListBE objPasswordStrengthListBe = new PasswordStrengthListBE();

            XmlDocument xmlResult = new XmlDocument();

            xmlResult = objPasswordStrengthBAL.GetPasswordStrenth();
            objPasswordStrengthListBe = objIdsignBal.DeserializeFromXml<PasswordStrengthListBE>(xmlResult);
            int MinCaps = 0;
            int MinSmall = 0;
            int MinSpecial = 0;
            int MinNumeric = 0;
            int MinPassLength = 0;
            if (objPasswordStrengthListBe.Items.Count > 0)
            {
                for (int i = 0; i < objPasswordStrengthListBe.Items.Count; i++)
                {
                    if (objPasswordStrengthListBe.Items[i].StrengthTypeId == 1)
                    {
                        if (!(string.IsNullOrEmpty(objPasswordStrengthListBe.Items[i].MinLength)))
                        {
                            MinCaps = Convert.ToInt32(objPasswordStrengthListBe.Items[i].MinLength);
                        }
                    }
                    else if (objPasswordStrengthListBe.Items[i].StrengthTypeId == 2)
                    {
                        if (!(string.IsNullOrEmpty(objPasswordStrengthListBe.Items[i].MinLength)))
                        {
                            MinSmall = Convert.ToInt32(objPasswordStrengthListBe.Items[i].MinLength);
                        }
                    }
                    else if (objPasswordStrengthListBe.Items[i].StrengthTypeId == 4)
                    {
                        if (!(string.IsNullOrEmpty(objPasswordStrengthListBe.Items[i].MinLength)))
                        {
                            MinNumeric = Convert.ToInt32(objPasswordStrengthListBe.Items[i].MinLength);
                        }
                    }
                    else if (objPasswordStrengthListBe.Items[i].StrengthTypeId == 3)
                    {
                        if (!(string.IsNullOrEmpty(objPasswordStrengthListBe.Items[i].MinLength)))
                        {
                            MinSpecial = Convert.ToInt32(objPasswordStrengthListBe.Items[i].MinLength);
                        }
                    }
                    else if (objPasswordStrengthListBe.Items[i].StrengthTypeId == 5)
                    {
                        if (!(string.IsNullOrEmpty(objPasswordStrengthListBe.Items[i].MinLength)))
                        {
                            MinPassLength = Convert.ToInt32(objPasswordStrengthListBe.Items[i].MinLength);
                        }
                    }
                }
            }
            //^(?=.*?[a-z].*?[a-z])
            //(?=.*?[A-Z].*?[A-Z].*?[A-Z])
            //(?=.*?[0-9])
            //(?=.*[!@#$%^&'])
            //[^ ]{8,}$

            string regExp = "^";
            string CapsExp = string.Empty;
            string SmallExp = string.Empty;
            string NumericExp = string.Empty;
            string SpecialExp = string.Empty;
            string PassLengthExp = string.Empty;


            string validationMsg = string.Empty;
            //CAPS
            if (MinCaps > 0)
            {
                CapsExp = "(?=";
                for (int i = 0; i < MinCaps; i++)
                    CapsExp += ".*?[A-Z]";
                CapsExp += ")";
                validationMsg += "- Min " + MinCaps + " Capital Letters. <br />";
            }

            //SMALL
            if (MinSmall > 0)
            {
                SmallExp = "(?=";
                for (int i = 0; i < MinSmall; i++)
                    SmallExp += ".*?[a-z]";
                SmallExp += ")";
                validationMsg += "- Min " + MinSmall + " Small Letters. <br />";
            }

            //Numeric
            if (MinNumeric > 0)
            {
                NumericExp = "(?=";
                for (int i = 0; i < MinNumeric; i++)
                    NumericExp += ".*?[0-9]";
                NumericExp += ")";
                validationMsg += "- Min " + MinNumeric + " Digits. <br />";
            }

            //Special
            if (MinSpecial > 0)
            {
                SpecialExp = "(?=";
                for (int i = 0; i < MinSpecial; i++)
                    SpecialExp += ".*[!@#$%^&']";
                SpecialExp += ")";
                validationMsg += "- Min " + MinSpecial + " Special characters (!@#$%^&'). <br />";
            }

            //PassLength
            if (MinPassLength > 0)
            {
                PassLengthExp = "[^ ]{" + MinPassLength + ",}$";
                validationMsg += "- Min length " + MinPassLength + " characters. <br />";
            }

            regExp = regExp + CapsExp + SmallExp + NumericExp + SpecialExp + PassLengthExp;

            //Response.Write(regExp);
            revTxtEditPassword.ValidationExpression = regExp;
            divPasswordResetValidation.InnerHtml = validationMsg;
        }

        protected void gvUsersList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                hfRecognize.Value = string.Empty;
                UserManagementListBE objUMListBE = new UserManagementListBE();
                UserManagementBE objUMBE = new UserManagementBE();

                GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                Label lblUserId = (Label)row.FindControl("lblUserId");
                Label lblName = (Label)row.FindControl("lblName");
                Label lblSurName = (Label)row.FindControl("lblSurName");
                Label lblPrimaryContact = (Label)row.FindControl("lblPrimaryContact");
                Label lblSecondaryContact = (Label)row.FindControl("lblSecondaryContact");
                Label lblPrimaryEmailId = (Label)row.FindControl("lblPrimaryEmailId");
                Label lblSecondaryEmaiId = (Label)row.FindControl("lblSecondaryEmaiId");
                Label lblRoleId = (Label)row.FindControl("lblRoleId");
                //Label lblRoleName = (Label)row.FindControl("lblRoleName");//Is For Visible=False
                Label lblDesignationId = (Label)row.FindControl("lblDesignationId");
                //Label lblDesignationName = (Label)row.FindControl("lblDesignationName");//Is For Visible=False
                Label lblGenderId = (Label)row.FindControl("lblGenderId");
                //Label lblGenderName = (Label)row.FindControl("lblGenderName");
                Label lblAddress = (Label)row.FindControl("lblAddress");
                Label lblGridDetails = (Label)row.FindControl("lblGridDetails");
                Label lblPhoto = (Label)row.FindControl("lblPhoto");
                Label lblPhotoPath = (Label)row.FindControl("lblPhotoPath");
                Label lblIsMobileAccess = (Label)row.FindControl("lblIsMobileAccess");

                switch (e.CommandName.ToUpper())
                {
                    case "EDITUSER":
                        //Response.Redirect(Constants.UpdateUserPage + "?" + UMS_Resource.QSTR_EDIT_USER_ID + "=" + objIdsignBal.Encrypt(lblUserId.Text), false);
                        //lblIsMobileAccess.Text = string.Empty;
                        txtEditEmployeeId.Text = lblUserId.Text;
                        txtEditName.Text = lblName.Text;
                        txtEditSurName.Text = lblSurName.Text;
                        //txtEditContactNo1.Text = lblPrimaryContact.Text;
                        //txtEditContactNo2.Text = lblSecondaryContact.Text;
                        if (lblPrimaryContact.Text.Length > 0 && lblPrimaryContact.Text != "--")
                        {
                            string[] contactNo = lblPrimaryContact.Text.Split('-');
                            if (contactNo.Length < 2)
                            {
                                txtEditCode1.Text = lblPrimaryContact.Text.Substring(0, 3);
                                txtEditContactNo1.Text = lblPrimaryContact.Text.Remove(0, 3);
                            }
                            else
                            {
                                txtEditCode1.Text = contactNo[0].ToString();
                                txtEditContactNo1.Text = contactNo[1].ToString();
                            }
                        }
                        else
                            txtEditCode1.Text = txtEditContactNo1.Text = string.Empty;

                        if (lblSecondaryContact.Text.Length > 0 && lblSecondaryContact.Text != "--")
                        {
                            string[] contactNo = lblSecondaryContact.Text.Split('-');
                            if (contactNo.Length < 2)
                            {
                                txtEditCode2.Text = lblSecondaryContact.Text.Substring(0, 3);
                                txtEditContactNo2.Text = lblSecondaryContact.Text.Remove(0, 3);
                            }
                            else
                            {
                                txtEditCode2.Text = contactNo[0].ToString();
                                txtEditContactNo2.Text = contactNo[1].ToString();
                            }
                        }
                        else
                            txtEditCode2.Text = txtEditContactNo2.Text = string.Empty;

                        txtEditPrimaryEmail.Text = lblPrimaryEmailId.Text;
                        txtEditSecondaryEmail.Text = lblSecondaryEmaiId.Text;
                        objCommonMethods.BindRoles(ddlEditRoleId, 0, true);
                        ddlEditRoleId.SelectedIndex = ddlEditRoleId.Items.IndexOf(ddlEditRoleId.Items.FindByValue(lblRoleId.Text));
                        ddlEditRoleId_SelectedIndexChanged(ddlEditRoleId, new EventArgs());
                        objCommonMethods.BindDesignations(ddlEditDesignation, 0, true);
                        ddlEditDesignation.SelectedIndex = ddlEditDesignation.Items.IndexOf(ddlEditDesignation.Items.FindByValue(lblDesignationId.Text));
                        objCommonMethods.BindGender(rblEditGender, 0);
                        rblEditGender.SelectedIndex = rblEditGender.Items.IndexOf(rblEditGender.Items.FindByValue(lblGenderId.Text));
                        txtEditAddress.Text = objIdsignBal.ReplaceNewLines(lblAddress.Text, false);
                        txtEditDetails.Text = objIdsignBal.ReplaceNewLines(lblGridDetails.Text, false);
                        lblPhotoId.Text = lblPhoto.Text;

                        if (lblIsMobileAccess.Text == "1")
                        {
                            rblEditIsMobileAccess.Items[0].Selected = true;
                            rblEditIsMobileAccess.Items[1].Selected = false;
                        }
                        else if (lblIsMobileAccess.Text == "0")
                        {
                            rblEditIsMobileAccess.Items[1].Selected = true;
                            rblEditIsMobileAccess.Items[0].Selected = false;
                        }

                        //if (lblIsMobileAccess.Text == "True")
                        //    rblEditIsMobileAccess.SelectedIndex = 0;
                        //else
                        //    rblEditIsMobileAccess.SelectedIndex = 1;
                        //lblFilePath.Text = lblPhotoPath.Text;
                        lblPhotoUserId.Text = lblUserId.Text;
                        if (lblPhoto.Text == "--")
                            divPhoto.Visible = false;
                        else
                            divPhoto.Visible = true;

                        btnUpdate.CommandArgument = lblUserId.Text;
                        BindDocuments();
                        EditPermissions();
                        mpeEditUserpopup.Show();
                        break;
                    case "ACTIVEUSER":
                        mpeActivate.Show();
                        hfId.Value = lblUserId.Text;
                        lblActiveMsg.Text = UMS_Resource.ACTIVE_USER_POPUP_TEXT;
                        btnActiveOk.Focus();
                        //objUMBE.ActiveStatusId = Constants.Active;
                        //objUMBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objUMBE.EmployeeId = lblUserId.Text;
                        //xml = objUserManagementBAL.Insert(objUMBE, Statement.Delete);
                        //objUMBE = objIdsignBal.DeserializeFromXml<UserManagementBE>(xml);

                        //if (Convert.ToBoolean(objUMBE.IsSuccess))
                        //{
                        //    BindGridUsersList();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message(UMS_Resource.USER_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                        //}
                        //else
                        //    Message(UMS_Resource.USER_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);

                        break;
                    case "DEACTIVEUSER":
                        mpeDeActive.Show();
                        hfId.Value = lblUserId.Text;
                        lblDeActiveMsg.Text = UMS_Resource.DEACTIVE_USER_POPUP_TEXT;
                        hfRecognize.Value = UMS_Resource.DEACTIVE_USER;
                        btnDeActiveOk.Focus();
                        //objUMBE.ActiveStatusId = Constants.DeActive;
                        //objUMBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objUMBE.EmployeeId = lblUserId.Text;
                        //xml = objUserManagementBAL.Insert(objUMBE, Statement.Delete);
                        //objUMBE = objIdsignBal.DeserializeFromXml<UserManagementBE>(xml);

                        //if (Convert.ToBoolean(objUMBE.IsSuccess))
                        //{
                        //    BindGridUsersList();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message(UMS_Resource.USER_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                        //}
                        //else
                        //    Message(UMS_Resource.USER_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                        break;
                    case "DELETEUSER":
                        mpeDeActive.Show();
                        hfId.Value = lblUserId.Text;
                        lblDeActiveMsg.Text = UMS_Resource.DELETE_USER_POPUP_TEXT;
                        btnDeActiveOk.Text = UMS_Resource.YES;
                        btnDeActiveCancel.Text = UMS_Resource.NO;
                        hfRecognize.Value = UMS_Resource.DELETE_USER;
                        btnDeActiveOk.Focus();
                        //objUMBE.ActiveStatusId = Constants.Delete;
                        //objUMBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                        //objUMBE.EmployeeId = lblUserId.Text;
                        //xml = objUserManagementBAL.Insert(objUMBE, Statement.Delete);
                        //objUMBE = objIdsignBal.DeserializeFromXml<UserManagementBE>(xml);

                        //if (Convert.ToBoolean(objUMBE.IsSuccess))
                        //{
                        //    BindGridUsersList();
                        //    BindPagingDropDown(Convert.ToInt32(hfTotalRecords.Value));
                        //    CreatePagingDT(Convert.ToInt32(hfTotalRecords.Value));
                        //    Message(UMS_Resource.MESSAGETYPE_SUCCESS, UMS_Resource.USER_DELETE_SUCESS);

                        //}
                        //else
                        //    Message(UMS_Resource.USER_DELETE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                        break;
                    case "RESETPASSWORD":
                        btnChangePwd.CommandArgument = lblUserId.Text;
                        mpeResetPwdpopup.Show();
                        break;

                }

            }

            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        protected void btnActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                UserManagementBE objUMBE = new UserManagementBE();
                objUMBE.ActiveStatusId = Constants.Active;
                objUMBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objUMBE.EmployeeId = hfId.Value;
                xml = objUserManagementBAL.Insert(objUMBE, Statement.Delete);
                objUMBE = objIdsignBal.DeserializeFromXml<UserManagementBE>(xml);

                if (Convert.ToBoolean(objUMBE.IsSuccess))
                {
                    BindGrid();
                    Message(UMS_Resource.USER_ACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                }
                else
                    Message(UMS_Resource.USER_ACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnDeActiveOk_Click(object sender, EventArgs e)
        {
            try
            {
                UserManagementBE objUMBE = new UserManagementBE();
                if (hfRecognize.Value == UMS_Resource.DEACTIVE_USER)
                {
                    hfRecognize.Value = string.Empty;
                    objUMBE.ActiveStatusId = Constants.DeActive;
                    objUMBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objUMBE.EmployeeId = hfId.Value;
                    xml = objUserManagementBAL.Insert(objUMBE, Statement.Delete);
                    objUMBE = objIdsignBal.DeserializeFromXml<UserManagementBE>(xml);

                    if (Convert.ToBoolean(objUMBE.IsSuccess))
                    {
                        BindGrid();
                        Message(UMS_Resource.USER_DEACTIVATE_SUCESS, UMS_Resource.MESSAGETYPE_SUCCESS);

                    }
                    else
                        Message(UMS_Resource.USER_DEACTIVATE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
                else if (hfRecognize.Value == UMS_Resource.DELETE_USER)
                {
                    hfRecognize.Value = string.Empty;
                    objUMBE.ActiveStatusId = Constants.Delete;
                    objUMBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objUMBE.EmployeeId = hfId.Value;
                    xml = objUserManagementBAL.Insert(objUMBE, Statement.Delete);
                    objUMBE = objIdsignBal.DeserializeFromXml<UserManagementBE>(xml);

                    if (Convert.ToBoolean(objUMBE.IsSuccess))
                    {
                        BindGrid();
                        Message(UMS_Resource.MESSAGETYPE_SUCCESS, UMS_Resource.USER_DELETE_SUCESS);

                    }
                    else
                        Message(UMS_Resource.USER_DELETE_FAIL, UMS_Resource.MESSAGETYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btndeleteok_Click(object sender, EventArgs e)
        {
            try
            {
                if (hfRecognize.Value == UMS_Resource.DELETE_PHOTO)
                {
                    hfRecognize.Value = string.Empty;
                    UserManagementBE objUserBE = new UserManagementBE();
                    objUserBE.UserId = lblPhotoUserId.Text;
                    objUserBE = objIdsignBal.DeserializeFromXml<UserManagementBE>(objUserManagementBAL.Insert(objUserBE, Statement.Edit));

                    if (objUserBE.IsSuccess)
                    {
                        //Message(UMS_Resource.DOCUMENT_DELETED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_SUCCESS, UMS_Resource.PHOTO_DELETED_SUCCESS, pnlEditMessage, lblEditMessage);
                        // Message(UMS_Resource.PHOTO_DELETED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        BindGrid();
                        //lblPhotoId.Text = string.Empty;
                        divPhoto.Visible = false;
                        mpeEditUserpopup.Show();
                    }
                    else
                    {
                        objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.PHOTO_DELETED_FAIL, pnlEditMessage, lblEditMessage);
                        mpeEditUserpopup.Show();
                    }
                }
                else if (hfRecognize.Value == UMS_Resource.DELETE_DOC)
                {
                    hfRecognize.Value = string.Empty;
                    UserManagementBE objUserBE = new UserManagementBE();
                    objUserBE.UserDocumentId = Convert.ToInt32(hfdocid.Value);
                    objUserBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                    objUserBE = objIdsignBal.DeserializeFromXml<UserManagementBE>(objUserManagementBAL.Insert(objUserBE, Statement.Change));

                    if (objUserBE.IsSuccess)
                    {
                        //Message(UMS_Resource.DOCUMENT_DELETED_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                        objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_SUCCESS, UMS_Resource.DOCUMENT_DELETED_SUCCESS, pnlEditMessage, lblEditMessage);
                        BindDocuments();
                        mpeEditUserpopup.Show();
                    }
                    else
                    {
                        objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.DOCUMENT_DELETED_FAIL, pnlEditMessage, lblEditMessage);
                        mpeEditUserpopup.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void gvUsersList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblActiveStatusId = (Label)e.Row.FindControl("lblActiveStatusId");


                    if (Convert.ToInt32(lblActiveStatusId.Text) == Constants.DeActive)
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = e.Row.FindControl("lkbtnResetPassword").Visible = false;
                        e.Row.FindControl("lkbtnActive").Visible = true;
                    }
                    else
                    {
                        e.Row.FindControl("lkbtnDeActive").Visible = e.Row.FindControl("lkbtnEdit").Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void BindDocuments()
        {
            try
            {
                UserManagementBE objUserBE = new UserManagementBE();
                UserManagementListBE objUserListBE = new UserManagementListBE();
                litDownloadDocs.Visible = divSubhead.Visible = false;
                objUserBE.UserId = UserName;
                objUserListBE = objIdsignBal.DeserializeFromXml<UserManagementListBE>(objUserManagementBAL.Get(objUserBE, ReturnType.List));
                if (objUserListBE.Items.Count > 0)
                {
                    litDownloadDocs.Visible = divSubhead.Visible = true;
                    rptrDocuments.DataSource = objUserListBE.Items;
                    rptrDocuments.DataBind();
                }
                else
                {

                    rptrDocuments.DataSource = new DataTable();
                    rptrDocuments.DataBind();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lbtnDocDelete_Click(object sender, EventArgs e)
        {
            try
            {
                mpeEditUserpopup.Show();
                hfRecognize.Value = UMS_Resource.DELETE_DOC;
                mpeDocpopup.Show();
                lblMsg.Text = Resource.ARE_YOU_SURE_DELETE_THIS_DOC;
                LinkButton lbtnDocDelete = (LinkButton)sender;
                hfdocid.Value = lbtnDocDelete.CommandArgument;
                btnDeActiveOk.Focus();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lbtnDocName_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtnDocName = (LinkButton)sender;
                string fileLocation = Server.MapPath(ConfigurationManager.AppSettings["UserDocuments"].ToString()) + lbtnDocName.CommandArgument;
                if (File.Exists(fileLocation))
                    objCommonMethods.DownloadFile(fileLocation, string.Empty);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lbtnPhotoDown_Click(object sender, EventArgs e)
        {
            try
            {
                string fileLocation = Server.MapPath(ConfigurationManager.AppSettings["UserPhotos"].ToString()) + lblPhotoId.Text;
                if (File.Exists(fileLocation))
                    objCommonMethods.DownloadFile(fileLocation, string.Empty);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void lbtnPhotoDelete_Click(object sender, EventArgs e)
        {
            try
            {
                mpeEditUserpopup.Show();
                hfRecognize.Value = UMS_Resource.DELETE_PHOTO;
                mpeDocpopup.Show();
                lblMsg.Text = Resource.ARE_YOU_SURE_DELETE_THIS_PHOTO;
                btnDeActiveOk.Focus();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                UserManagementBE objUserManagementBE = UpdateUserDetails();
                if (objUserManagementBE.IsSuccess)
                {
                    BindGrid();
                    Message(UMS_Resource.EMPLOYEE_UPDATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else if (objUserManagementBE.IsEmailIdExists)
                {
                    objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.EMAIL_EXISTS, pnlEditMessage, lblEditMessage);
                    mpeEditUserpopup.Show();
                }
                else
                {
                    objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.EMPLOYEE_UPDATE_FAILED, pnlEditMessage, lblEditMessage);
                    mpeEditUserpopup.Show();
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        protected void btnChangePwd_Click(object sender, EventArgs e)
        {
            try
            {
                UserManagementBE objUMBE = new UserManagementBE();
                objUMBE.EmployeeId = btnChangePwd.CommandArgument;
                objUMBE.Password = objIdsignBal.AESEncryptString(txtEditPassword.Text);
                objUMBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objUMBE = objIdsignBal.DeserializeFromXml<UserManagementBE>(objUserManagementBAL.Insert(objUMBE, Statement.Modify));

                if (objUMBE.IsSuccess)
                {
                    Message(UMS_Resource.EMPLOYEE_PASSWORD_UPDATE_SUCCESS, UMS_Resource.MESSAGETYPE_SUCCESS);
                }
                else
                {
                    Message(UMS_Resource.EMPLOYEE_PASSWORD_UPDATE_FAILED, UMS_Resource.MESSAGETYPE_ERROR);
                    //objCommonMethods.ShowMessage(UMS_Resource.MESSAGETYPE_ERROR, UMS_Resource.EMPLOYEE_PASSWORD_UPDATE_FAILED, pnlEditMessage, lblEditMessage);                    
                }
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        #endregion

        #region Methods

        private void BindDropDowns()
        {
            try
            {

                objCommonMethods.BindRoles(ddlRoleId, 0, true);
                objCommonMethods.BindDesignations(ddlDesignation, 0, true);
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void Message(string Message, string MessageType)
        {
            try
            {
                objCommonMethods.ShowMessage(MessageType, Message, pnlMessage, lblMessage);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void BindGrid()
        {
            try
            {
                MasterSearchBE _objSearchBe = new MasterSearchBE();
                _objSearchBe.PageNo = Convert.ToInt32(hfPageNo.Value);
                _objSearchBe.PageSize = PageSize;
                _objSearchBe.Flag = Convert.ToInt32(SearchFlags.UserSearch);
                //_objSearchBe.Name = txtName.Text.Trim();
                //_objSearchBe.ContactNo = txtContactNo.Text.Trim();
                //_objSearchBe.RoleId = this.Role;
                //_objSearchBe.DesignationId = this.Designation;
                _objSearchBe.Name = hfName.Value;
                _objSearchBe.ContactNo = hfContactNo.Value;
                _objSearchBe.RoleId = hfRoleId.Value == "" ? 0 : Convert.ToInt32(hfRoleId.Value);
                _objSearchBe.DesignationId = hfDesignation.Value == "" ? 0 : Convert.ToInt32(hfDesignation.Value);

                DataSet ds = new DataSet();
                ds = _objRerpotsBal.SearchMasterBAL(_objSearchBe);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    hfTotalRecords.Value = ds.Tables[0].Rows[0]["TotalRecords"].ToString();
                    gvUsersList.DataSource = ds.Tables[0];
                    gvUsersList.DataBind();
                    divpaging.Visible = divdownpaging.Visible = true;
                }
                else
                {
                    hfTotalRecords.Value = 0.ToString();
                    gvUsersList.DataSource = new DataTable();
                    gvUsersList.DataBind();
                    divpaging.Visible = divdownpaging.Visible = false;
                }

                //UserManagementBE objUserManagementBE = new UserManagementBE();
                //UserManagementListBE objUserManagementListBE = new UserManagementListBE();
                //XmlDocument resultedXml = new XmlDocument();

                //objUserManagementBE.PageNo = Convert.ToInt32(hfPageNo.Value);
                //objUserManagementBE.PageSize = PageSize;
                //resultedXml = objUserManagementBAL.Get(objUserManagementBE, ReturnType.Bulk);

                //objUserManagementListBE = objIdsignBal.DeserializeFromXml<UserManagementListBE>(resultedXml);
                //DataTable dt = new DataTable();
                //dt = objIdsignBal.ConvertListToDataSet<UserManagementBE>(objUserManagementListBE.Items).Tables[0];

                //if (objUserManagementListBE.Items.Count > 0)
                //{
                //    divgrid.Visible = divpaging.Visible = true;
                //    hfTotalRecords.Value = objUserManagementListBE.Items[0].TotalRecords.ToString();
                //}
                //else
                //{
                //    hfTotalRecords.Value = objUserManagementListBE.Items.Count.ToString();
                //    divgrid.Visible = divpaging.Visible = false;
                //}
                //gvUsersList.DataSource = dt;
                //gvUsersList.DataBind();
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }
        }

        private void EditPermissions()
        {
            try
            {
                lblEditMessage.Text = pnlEditMessage.CssClass = string.Empty;
                //chkEditTariffAdjustment.Checked = chkEditPaymentAdjustment.Checked = chkEditBillAdjustment.Checked = false;
                //chkEditCustomerModify.Checked = chkEditDisconnection.Checked = chkEditReconnection.Checked = chkEditBillGeneration.Checked = false;
                //chkEditBudgetSegmentwiseMargin.Checked =
                UserManagementBE objUserManagementBE = new UserManagementBE();
                objUserManagementBE.UserId = this.UserName;
                objUserManagementBE = objIdsignBal.DeserializeFromXml<UserManagementBE>(objUserManagementBAL.Get(objUserManagementBE, ReturnType.Set));

                //if (objUserManagementBE.RoleId > 1)
                //{
                //specialPermissions
                //chkEditTariffAdjustment.Checked = objUserManagementBE.TariffAdjustments;
                //chkEditPaymentAdjustment.Checked = objUserManagementBE.PaymentsAdjustments;
                //chkEditBillAdjustment.Checked = objUserManagementBE.BillAdjustment;
                ////chkEditBudgetSegmentwiseMargin.Checked = objUserManagementBE.BudgetSegmentWiseMargin;
                //chkEditCustomerModify.Checked = objUserManagementBE.CustomerDetailsModification;
                //chkEditDisconnection.Checked = objUserManagementBE.Disconnection;
                //chkEditReconnection.Checked = objUserManagementBE.ReConnections;
                //chkEditBillGeneration.Checked = objUserManagementBE.BillGeneration;
                switch (objUserManagementBE.RoleId)
                {
                    case (int)Roles.BUManagerRoleId:
                        if (objUserManagementBE.BusinessUnits != null)
                        {
                            string[] BusinessUnits = objUserManagementBE.BusinessUnits.Split(',');
                            for (int i = 0; i < cblEditBusinessUnits.Items.Count; i++)
                            {
                                cblEditBusinessUnits.Items[i].Selected = false;
                                for (int j = 0; j < BusinessUnits.Length; j++)
                                {
                                    if (cblEditBusinessUnits.Items[i].Value.Trim() == BusinessUnits[j].Trim())
                                    {
                                        cblEditBusinessUnits.Items[i].Selected = true;
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    case (int)Roles.CommercialManagerRoleId:
                        if (objUserManagementBE.BusinessUnits != null)
                        {
                            string[] BusinessUnits1 = objUserManagementBE.BusinessUnits.Split(',');
                            for (int i = 0; i < cblEditBusinessUnits.Items.Count; i++)
                            {
                                cblEditBusinessUnits.Items[i].Selected = false;
                                for (int j = 0; j < BusinessUnits1.Length; j++)
                                {
                                    if (cblEditBusinessUnits.Items[i].Value == BusinessUnits1[j])
                                    {
                                        cblEditBusinessUnits.Items[i].Selected = true;
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    case (int)Roles.CashierRoleId:
                        if (objUserManagementBE.BusinessUnits != null)
                        {
                            ddlEditBusinessUnits.SelectedIndex = ddlEditBusinessUnits.Items.IndexOf(ddlEditBusinessUnits.Items.FindByValue(objUserManagementBE.BusinessUnits));
                            ddlEditBusinessUnits_SelectedIndexChanged(ddlEditBusinessUnits.SelectedValue, new EventArgs());
                            ddlEditServiceUnits.SelectedIndex = ddlEditServiceUnits.Items.IndexOf(ddlEditServiceUnits.Items.FindByValue(objUserManagementBE.ServiceUnits));
                            ddlEditServiceUnits_SelectedIndexChanged(ddlEditServiceUnits.SelectedValue, new EventArgs());

                            string[] ServiceCenters = objUserManagementBE.ServiceCenters.Split(',');
                            for (int i = 0; i < cblEditServiceCenters.Items.Count; i++)
                            {
                                cblEditServiceCenters.Items[i].Selected = false;
                                for (int j = 0; j < ServiceCenters.Length; j++)
                                {
                                    if (cblEditServiceCenters.Items[i].Value == ServiceCenters[j])
                                    {
                                        cblEditServiceCenters.Items[i].Selected = true;
                                        break;
                                    }
                                }
                            }
                            cblEditServiceCenters_SelectedIndexChanged(null, null);
                            //divEditCashOffices.Visible = true;
                            string[] CashOffices = objUserManagementBE.CashOffices.Split(',');
                            for (int i = 0; i < cblEditCashOffices.Items.Count; i++)
                            {
                                cblEditCashOffices.Items[i].Selected = false;
                                for (int j = 0; j < CashOffices.Length; j++)
                                {
                                    if (cblEditCashOffices.Items[i].Value == CashOffices[j])
                                    {
                                        cblEditCashOffices.Items[i].Selected = true;
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    case (int)Roles.SUManagerRoleId:
                        if (objUserManagementBE.BusinessUnits != null)
                        {
                            ddlEditBUServiceUnits.SelectedIndex = ddlEditBUServiceUnits.Items.IndexOf(ddlEditBUServiceUnits.Items.FindByValue(objUserManagementBE.BusinessUnits));
                            ddlEditBUServiceUnits_SelectedIndexChanged(ddlEditBUServiceUnits.SelectedValue, new EventArgs());
                            string[] ServiceUnits = objUserManagementBE.ServiceUnits.Split(',');
                            for (int i = 0; i < cblEditServiceUnits.Items.Count; i++)
                            {
                                cblEditServiceUnits.Items[i].Selected = false;
                                for (int j = 0; j < ServiceUnits.Length; j++)
                                {
                                    if (cblEditServiceUnits.Items[i].Value == ServiceUnits[j])
                                    {
                                        cblEditServiceUnits.Items[i].Selected = true;
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    case (int)Roles.DataEntryOperatorRoleId:
                        //string[] Districts = objUserManagementBE.Districts.Split(',');
                        //for (int i = 0; i < cblEditDistricts.Items.Count; i++)
                        //{
                        //    cblEditDistricts.Items[i].Selected = false;
                        //    for (int j = 0; j < Districts.Length; j++)
                        //    {
                        //        if (cblEditDistricts.Items[i].Value == Districts[j])
                        //        {
                        //            cblEditDistricts.Items[i].Selected = true;
                        //            break;
                        //        }
                        //    }
                        //}
                        if (objUserManagementBE.BusinessUnits != null)
                        {
                            string[] BusinessUnits2 = objUserManagementBE.BusinessUnits.Split(',');
                            for (int i = 0; i < cblEditBusinessUnits.Items.Count; i++)
                            {
                                cblEditBusinessUnits.Items[i].Selected = false;
                                for (int j = 0; j < BusinessUnits2.Length; j++)
                                {
                                    if (cblEditBusinessUnits.Items[i].Value == BusinessUnits2[j])
                                    {
                                        cblEditBusinessUnits.Items[i].Selected = true;
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                }
                //}
            }
            catch (Exception ex)
            {
                Message(ex.Message, UMS_Resource.MESSAGETYPE_ERROR);
                try
                {
                    objCommonMethods.ErrorMessage(ex.Message, Key, ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7));
                }
                catch (Exception logex)
                {

                }
            }

        }

        private string MultipleValues(CheckBoxList cblItems)
        {
            string Values = string.Empty;
            for (int i = 0; i < cblItems.Items.Count; i++)
            {
                if (cblItems.Items[i].Selected)
                    Values += cblItems.Items[i].Value + ",";
            }
            Values = Values.TrimEnd(',');
            return Values;
        }

        private UserManagementBE UpdateUserDetails()
        {
            try
            {
                UserManagementBE objUserManagementBE = new UserManagementBE();
                objUserManagementBE.EmployeeId = btnUpdate.CommandArgument;
                objUserManagementBE.ModifiedBy = Session[UMS_Resource.SESSION_LOGINID].ToString();
                objUserManagementBE.DesignationId = Convert.ToInt32(ddlEditDesignation.SelectedValue);
                objUserManagementBE.Name = txtEditName.Text;
                objUserManagementBE.SurName = txtEditSurName.Text;
                objUserManagementBE.PrimaryEmailId = txtEditPrimaryEmail.Text;
                objUserManagementBE.SecondaryEmailId = txtEditSecondaryEmail.Text;
                objUserManagementBE.GenderId = Convert.ToInt32(rblEditGender.SelectedValue);
                objUserManagementBE.Address = objIdsignBal.ReplaceNewLines(txtEditAddress.Text, true);
                objUserManagementBE.ContactNo1 = txtEditCode1.Text + "-" + txtEditContactNo1.Text;
                objUserManagementBE.ContactNo2 = txtEditCode2.Text + "-" + txtEditContactNo2.Text;
                objUserManagementBE.RoleId = Convert.ToInt32(ddlEditRoleId.SelectedValue);
                objUserManagementBE.Details = objIdsignBal.ReplaceNewLines(txtEditDetails.Text, true);
                if (rblEditIsMobileAccess.SelectedValue == "1")
                    objUserManagementBE.IsMobileAccess = 1;
                else
                    objUserManagementBE.IsMobileAccess = 0;
                string PhotoEditFileName = string.Empty;
                if (fupEditPhoto.FileContent.Length > 0)
                {
                    PhotoEditFileName = System.IO.Path.GetFileName(fupEditPhoto.FileName).ToString();//Assinging FileName
                    string PhotoExtension = Path.GetExtension(PhotoEditFileName);//Storing Extension of file into variable Extension
                    TimeZoneInfo PhotoIND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                    DateTime PhotocurrentTime = DateTime.Now;
                    DateTime Photoourtime = TimeZoneInfo.ConvertTime(PhotocurrentTime, PhotoIND_ZONE);
                    string PhotoCurrentDate = Photoourtime.ToString("dd_MM_yyyy_hh_mm_ss");
                    string PhotoModifiedFileName = Path.GetFileNameWithoutExtension(PhotoEditFileName) + "_" + PhotoCurrentDate + PhotoExtension;//Setting File Name to store it in database
                    fupEditPhoto.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["UserPhotos"].ToString()) + PhotoModifiedFileName);//Saving File in to the server.

                    objUserManagementBE.Photo = PhotoModifiedFileName;
                    objUserManagementBE.FilePath = ConfigurationManager.AppSettings["UserPhotos"].ToString().Replace("~", ".."); ;
                }
                //if (!string.IsNullOrEmpty(hfEditDocuments.Value))
                //{
                //    hfEditDocuments.Value = hfEditDocuments.Value.TrimEnd(',');
                //    string[] Documents = hfEditDocuments.Value.Split(',');
                //    string fileInput = string.Empty;
                //    if (Documents.Length > 0)
                //    {
                //        for (int z = 0; z < Documents.Length; z++)//looping to save attachments
                //        {
                //            //HttpPostedFile PostedFile = Request.Files[z];//Assigning to variable to Access File
                //            //if (PostedFile.ContentLength != 0)//Checking the size of file is more than 0 bytes
                //            //{
                //            string FileName = Documents[z];//Assinging FileName
                //            string Extension = Path.GetExtension(FileName);//Storing Extension of file into variable Extension
                //            TimeZoneInfo IND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                //            DateTime currentTime = DateTime.Now;
                //            DateTime ourtime = TimeZoneInfo.ConvertTime(currentTime, IND_ZONE);
                //            string CurrentDate = ourtime.ToString("dd-MM-yyyy_hh-mm-ss");
                //            string ModifiedFileName = Path.GetFileNameWithoutExtension(FileName) + "_" + CurrentDate + Extension;//Setting File Name to store it in database
                //            File.Create(Server.MapPath(ConfigurationManager.AppSettings["UserDocuments"].ToString()) + ModifiedFileName);//Saving File in to the server.

                //            fileInput = string.IsNullOrEmpty(fileInput) ? ModifiedFileName : fileInput + "," + ModifiedFileName;
                //            //}                        
                //        }
                //        objUserManagementBE.Path = ConfigurationManager.AppSettings["UserDocuments"].ToString().Replace("~", ".."); ;
                //        objUserManagementBE.DocumentName = fileInput;
                //    }
                //}

                //if (!string.IsNullOrEmpty(hfDocuments.Value))
                //{
                //hfDocuments.Value = hfDocuments.Value.TrimEnd(',');
                //string[] Documents = hfDocuments.Value.Split(',');
                string fileInput = string.Empty;
                //if (Documents.Length > 0)
                //{
                for (int z = 0; z < Request.Files.Count; z++)//looping to save attachments
                {
                    HttpPostedFile PostedFile = Request.Files[z];//Assigning to variable to Access File
                    if (PostedFile.ContentLength != 0)//Checking the size of file is more than 0 bytes
                    {
                        string FileName = PostedFile.FileName;//Assinging FileName
                        if (FileName != PhotoEditFileName)
                        {
                            string Extension = Path.GetExtension(FileName);//Storing Extension of file into variable Extension
                            TimeZoneInfo IND_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                            DateTime currentTime = DateTime.Now;
                            DateTime ourtime = TimeZoneInfo.ConvertTime(currentTime, IND_ZONE);
                            string CurrentDate = ourtime.ToString("dd_MM_yyyy_hh_mm_ss");
                            string ModifiedFileName = Path.GetFileNameWithoutExtension(FileName) + "_" + CurrentDate + Extension;//Setting File Name to store it in database
                            // File.Create(Server.MapPath(ConfigurationManager.AppSettings["UserDocuments"].ToString()) + ModifiedFileName);//Saving File in to the server.
                            PostedFile.SaveAs(Server.MapPath(ConfigurationManager.AppSettings["UserDocuments"].ToString()) + ModifiedFileName);
                            fileInput = string.IsNullOrEmpty(fileInput) ? ModifiedFileName : fileInput + "," + ModifiedFileName;
                        }
                    }
                }
                objUserManagementBE.Path = ConfigurationManager.AppSettings["UserDocuments"].ToString().Replace("~", ".."); ;
                objUserManagementBE.DocumentName = fileInput;
                //}
                //}

                switch (Convert.ToInt32(ddlEditRoleId.SelectedValue))
                {
                    case (int)Roles.CommercialManagerRoleId:
                    case (int)Roles.BUManagerRoleId:
                        objUserManagementBE.BusinessUnits = MultipleValues(cblEditBusinessUnits);
                        break;
                    case (int)Roles.CashierRoleId:
                        objUserManagementBE.BU_ID = ddlEditBusinessUnits.SelectedValue;
                        objUserManagementBE.SU_ID = ddlEditServiceUnits.SelectedValue;
                        objUserManagementBE.ServiceCenters = MultipleValues(cblEditServiceCenters);
                        objUserManagementBE.CashOffices = MultipleValues(cblEditCashOffices);
                        break;
                    case (int)Roles.DataEntryOperatorRoleId:
                        //objUserManagementBE.Districts = MultipleValues(cblEditDistricts);
                        objUserManagementBE.BusinessUnits = MultipleValues(cblEditBusinessUnits);
                        break;
                    case (int)Roles.SUManagerRoleId:
                        objUserManagementBE.BU_ID = ddlEditBUServiceUnits.SelectedValue;
                        objUserManagementBE.ServiceUnits = MultipleValues(cblEditServiceUnits);
                        break;
                }

                string buList = "";
                foreach (ListItem item in cblEditBusinessUnits.Items)
                    if (item.Selected)
                    {
                        buList = buList + item.Value + ",";
                    }
                objUserManagementBE.BusinessUnits = buList.Trim();

                //objUserManagementBE.CustomerDetailsModification = chkEditCustomerModify.Checked;
                //objUserManagementBE.BillAdjustment = chkEditBillAdjustment.Checked;
                //objUserManagementBE.TariffAdjustments = chkEditTariffAdjustment.Checked;
                //objUserManagementBE.PaymentsAdjustments = chkEditPaymentAdjustment.Checked;
                //objUserManagementBE.Disconnection = chkEditDisconnection.Checked;
                //objUserManagementBE.ReConnections = chkEditReconnection.Checked;
                //objUserManagementBE.BillGeneration = chkEditBillGeneration.Checked;
                //objUserManagementBE.BudgetSegmentWiseMargin = chkEditBudgetSegmentwiseMargin.Checked;

                objUserManagementBE = objIdsignBal.DeserializeFromXml<UserManagementBE>(objUserManagementBAL.Insert(objUserManagementBE, Statement.Update));
                return objUserManagementBE;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

        #region Culture
        protected override void InitializeCulture()
        {
            if (Session[UMS_Resource.CULTURE] != null)
            {
                string culture = Session[UMS_Resource.CULTURE].ToString();

                Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
            }
            base.InitializeCulture();
        }
        #endregion
    }
}