﻿<%@ Page Title=":: Update User ::" Language="C#" MasterPageFile="~/MasterPages/EBMSDashBoard.Master"
    AutoEventWireup="true" CodeBehind="UpdateUser.aspx.cs" Theme="Green" Inherits="UMS_Nigeria.UserManagement.UpdateUser" %>

<%@ Import Namespace="Resources" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UMSNigeriaBody" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server">
        <ProgressTemplate>
            <center>
                <div id="loading-div" class="pbloading">
                    <div id="title-loading" class="pbtitle">
                        BEDC</div>
                    <center>
                        <img src="../images/loading.GIF" class="pbimg"></center>
                    <p class="pbtxt">
                        Loading Please wait.....</p>
                </div>
            </center>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ModalPopupExtender ID="modalPopupLoading" runat="server" TargetControlID="UpdateProgress"
        PopupControlID="UpdateProgress" BackgroundCssClass="modalBackground" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
            <%--Grid Starts Here--%>
            <div class="grid_tb" id="divgrid" runat="server">
                <asp:HiddenField ID="hfPageSize" runat="server" />
                <asp:HiddenField ID="hfEditDocuments" runat="server" />
                <asp:HiddenField ID="hfPageNo" runat="server" />
                <asp:HiddenField ID="hfLastPage" runat="server" />
                <asp:HiddenField ID="hfTotalRecords" Value="0" runat="server" />
                <asp:HiddenField ID="hfRecognize" runat="server" />
                <asp:HiddenField ID="hfId" runat="server" />
                <asp:HiddenField ID="hfEditmailId" runat="server" />
                <asp:HiddenField ID="hfmailId" runat="server" />
                <asp:HiddenField ID="hfDocuments" runat="server" />
            </div>
            <%--Grid End Here--%>
            <div id="hidepopup">
                <%-- Active Btn popup Start--%>
                <asp:ModalPopupExtender ID="mpeActivate" runat="server" PopupControlID="PanelActivate"
                    TargetControlID="btnActivate" BackgroundCssClass="modalBackground" CancelControlID="btnActiveCancel">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnActivate" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="PanelActivate" runat="server" DefaultButton="btnActiveOk" CssClass="modalPopup"
                    Style="display: none; border-color: #639B00;">
                    <div class="popheader" style="background-color: #639B00;">
                        <asp:Label ID="lblActiveMsg" runat="server"></asp:Label>
                    </div>
                    <br />
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <asp:Button ID="btnActiveOk" runat="server" OnClick="btnActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                            <asp:Button ID="btnActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- Active  Btn popup Closed--%>
                <%-- DeActive  Btn popup Start--%>
                <asp:ModalPopupExtender ID="mpeDeActive" runat="server" PopupControlID="PanelDeActivate"
                    TargetControlID="btnDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnDeActiveCancel">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnDeActivate" runat="server" Text="Button" Style="display: none;" />
                <asp:Panel ID="PanelDeActivate" runat="server" DefaultButton="btnDeActiveOk" CssClass="modalPopup"
                    Style="display: none; border-color: #639B00; z-index: 1000001;">
                    <div class="popheader" style="background-color: red;">
                        <asp:Label ID="lblDeActiveMsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer" align="right">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnDeActiveOk" runat="server" OnClick="btnDeActiveOk_Click" Text="<%$ Resources:Resource, OK%>"
                                CssClass="ok_btn" />
                            <asp:Button ID="btnDeActiveCancel" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                                CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>
                <%-- DeActive  popup Closed--%>
            </div>
            <%--Edit User Start Here--%>
            <div class="out-bor">
                <div class="inner-sec">
                    <asp:Panel ID="pnlEditMessage" runat="server">
                        <asp:Label ID="lblEditMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="text_total">
                        <div class="text-heading">
                            <asp:Literal ID="litEditEmployee" runat="server" Text="<%$ Resources:Resource, UPDATE_EMPLOYEE%>"></asp:Literal>
                        </div>
                        <div class="star_text">
                            <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:Resource, MANDATORY%>"></asp:Literal>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                    <div class="dot-line">
                    </div>
                    <div class="clear pad_5 fltl">
                        <a href="AddUser.aspx" class="back_but">
                            <asp:Literal ID="litSearch" runat="server" Text="<%$ Resources:Resource, GO_TO_BACK%>"></asp:Literal></a>
                    </div>
                    <div class="clr"></div>
                    <div class="inner-box">
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litEditName" runat="server" Text="<%$ Resources:Resource, NAME%>"></asp:Literal>
                                    <span class="span_star">*</span></label>
                                <div class="space">
                                </div>
                                <asp:TextBox ID="txtEditName" CssClass="text-box" runat="server" MaxLength="300"
                                    placeholder="Enter Name" onblur="return TextBoxBlurValidationlbl(this,'spanEditName','Name')"></asp:TextBox>
                                <span id="spanEditName" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litEditSurname" runat="server" Text="<%$ Resources:Resource, SURNAME%>"></asp:Literal>
                                    <span class="span_star">*</span></label>
                                <div class="space">
                                </div>
                                <asp:TextBox ID="txtEditSurName" CssClass="text-box" runat="server" MaxLength="300"
                                    placeholder="Enter Surname" onblur="return TextBoxBlurValidationlbl(this,'spanEditSurName','Surname')"></asp:TextBox>
                                <span id="spanEditSurName" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litEditGender" runat="server" Text="<%$ Resources:Resource, GENDER%>"></asp:Literal>
                                    <span class="span_star">*</span></label>
                                <div class="space">
                                </div>
                                <asp:RadioButtonList ID="rblEditGender" Width="70%" onchange="RadioButtonListlbl(this,'spanEditGender',' Select one')"
                                    RepeatDirection="Horizontal" runat="server">
                                </asp:RadioButtonList>
                                <div class="space">
                                    <br />
                                </div>
                                <span id="spanEditGender" class="span_color"></span>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litEditRoleId" runat="server" Text="<%$ Resources:Resource, ROLE_ID%>"></asp:Literal>
                                    <span class="span_star">*</span></label>
                                <div class="space">
                                </div>
                                <asp:UpdatePanel ID="upnlddlEditRoleId" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlEditRoleId" CssClass="text-box text-select" onchange="DropDownlistOnChangelbl(this,'spanEditRoleId',' Role Id')"
                                            runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlEditRoleId_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <div class="space">
                                        </div>
                                        <span id="spanEditRoleId" class="span_color"></span>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litEditDesignation" runat="server" Text="<%$ Resources:Resource, DESIGNATION%>"></asp:Literal>
                                    <span class="span_star">*</span></label>
                                <div class="space">
                                </div>
                                <asp:DropDownList ID="ddlEditDesignation" onchange="DropDownlistOnChangelbl(this,'spanEditDesignation',' Designation')"
                                    runat="server" CssClass="text-box text-select">
                                </asp:DropDownList>
                                <div class="space">
                                </div>
                                <span id="spanEditDesignation" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litEditEmployeeId" runat="server" Text="<%$ Resources:Resource, EMPLOYEE_ID%>"></asp:Literal>
                                    <span class="span_star">*</span></label>
                                <div class="space">
                                </div>
                                <asp:TextBox ID="txtEditEmployeeId" Enabled="false" runat="server" MaxLength="50"
                                    placeholder="Enter Employee Id" CssClass="text-box"></asp:TextBox>
                                <span id="spanEditEmployeeId" class="span_color"></span>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litEditPEmail" runat="server" Text="<%$ Resources:Resource, PRIMARY_EMAIL%>"></asp:Literal>
                                    <span class="span_star" id="divEditspanMail" runat="server" visible="false">*</span>
                                </label>
                                <div class="space">
                                </div>
                                <asp:TextBox ID="txtEditPrimaryEmail" CssClass="text-box" runat="server" MaxLength="500"
                                    placeholder="Enter Primary Email Id"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtEditPrimaryEmail"
                                    ValidChars="@._" FilterType="LowercaseLetters,UppercaseLetters,Numbers,Custom">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanEditPrimaryEmail" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litEditSEmail" runat="server" Text="<%$ Resources:Resource, SECONDARY_EMAIL%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:TextBox ID="txtEditSecondaryEmail" CssClass="text-box" runat="server" MaxLength="500"
                                    placeholder="Enter Secondary Email Id"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="ftbEditSEmail" runat="server" TargetControlID="txtEditSecondaryEmail"
                                    ValidChars="@._" FilterType="LowercaseLetters,UppercaseLetters,Numbers,Custom">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanEditSecondaryEmail" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litEditContactNo1" runat="server" Text="<%$ Resources:Resource, CONTACT_NO%>"></asp:Literal>
                                    <span class="span_star">*</span></label>
                                <div class="space">
                                </div>
                                <asp:TextBox CssClass="text-box" ID="txtEditCode1" runat="server" onblur="return TextBoxBlurValidationlbl(this,'spanEditContactNo','Code')"
                                    MaxLength="3" Width="11%" placeholder="Code"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtEditCode1"
                                    FilterType="Numbers" ValidChars=" ">
                                </asp:FilteredTextBoxExtender>
                                <asp:TextBox CssClass="text-box" ID="txtEditContactNo1" Width="40%" runat="server"
                                    onblur="return TextBoxBlurValidationlbl(this,'spanEditContactNo','Contact No')"
                                    MaxLength="10" placeholder="Enter Contact No"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtEditContactNo1"
                                    FilterType="Numbers" ValidChars=" ">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanEditContactNo" class="span_color"></span>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litEditContactNo2" runat="server" Text="<%$ Resources:Resource, ANOTHER_CONTACT_NO%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:TextBox CssClass="text-box" ID="txtEditCode2" runat="server" MaxLength="3" Width="11%"
                                    placeholder="Code"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtEditCode2"
                                    FilterType="Numbers" ValidChars=" ">
                                </asp:FilteredTextBoxExtender>
                                <asp:TextBox CssClass="text-box" ID="txtEditContactNo2" Width="40%" runat="server"
                                    MaxLength="10" placeholder="Enter Alternate Contact No"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtEditContactNo2"
                                    FilterType="Numbers" ValidChars=" ">
                                </asp:FilteredTextBoxExtender>
                                <span id="spanEditContactNo2" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litEditAddress" runat="server" Text="<%$ Resources:Resource, ADDRESS%>"></asp:Literal>
                                    <span class="span_star">*</span></label>
                                <div class="space">
                                </div>
                                <asp:TextBox ID="txtEditAddress" runat="server" CssClass="text-box" placeholder="Enter Address"
                                    TextMode="MultiLine" onblur="return TextBoxBlurValidationlbl(this,'spanEditAddress','Address')"></asp:TextBox>
                                <span id="spanEditAddress" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box3">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litEditDetails" runat="server" Text="<%$ Resources:Resource, Details%>"></asp:Literal></label>
                                <div class="space">
                                </div>
                                <asp:TextBox ID="txtEditDetails" runat="server" CssClass="text-box" TextMode="MultiLine"
                                    placeholder="Enter Details"></asp:TextBox>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litEditPhoto" runat="server" Text="<%$ Resources:Resource, PHOTO%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <asp:FileUpload ID="fupEditPhoto" runat="server" onchange="return fileEditvalidate(this.value,'Photo')"
                                    CssClass="fltl" />&nbsp; <span id="spanfupEditPhoto" class="span_color"></span>
                            </div>
                        </div>
                        <div class="inner-box2">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="litEditDocuments" runat="server" Text="<%$ Resources:Resource, DOCUMENTS%>"></asp:Literal>
                                </label>
                                <div class="space">
                                </div>
                                <div id="divEditFile">
                                    <asp:FileUpload ID="fupEditDocument" runat="server" onchange="return pdffileEditvalidate(this.value,'Documents')"
                                        CssClass="fltl" />&nbsp;
                                    <asp:LinkButton ID="lnkEditAddFiles" runat="server" CssClass="fltl" OnClientClick="return AddEditFile()"
                                        Font-Bold="True">Add</asp:LinkButton><br />
                                    <span id="spanEditDocuments" class="span_color"></span>
                                </div>
                            </div>
                        </div>
                        <div class="inner-box1">
                            <div class="text-inner">
                                <label for="name">
                                    <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, ISMOBILE_ACCESS%>"></asp:Literal>
                                    <span class="span_star">*</span></label>
                                <div class="space">
                                </div>
                                <asp:RadioButtonList ID="rblEditIsMobileAccess" Width="70%" RepeatDirection="Horizontal"
                                    runat="server">
                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                </asp:RadioButtonList>
                                <div class="space">
                                    <br />
                                </div>
                                <span id="spanEditIsMobileAccess" class="span_color"></span>
                            </div>
                        </div>
                        <div class="clr">
                        </div>
                        <div class="consumer_feild">
                            <div class="consume_input editlist" id="div1" style="width: 98%; padding: 5px;">
                                <div class="photodocumentFullWidth">
                                    <h2 id="divSubhead" runat="server" class="subhead">
                                        <asp:Literal ID="litDownloadDocs" Visible="false" runat="server" Text="<%$ Resources:Resource, DOWNLOAD_UPLOADED_DOCUMENTS%>"></asp:Literal><br />
                                        <br />
                                    </h2>
                                    <div id="divPhoto" runat="server" visible="false">
                                        <asp:UpdatePanel ID="upnlPhotos" runat="server">
                                            <ContentTemplate>
                                                <div class="fltl">
                                                    <asp:Label runat="server" ID="lblPhotoId"></asp:Label>
                                                    <asp:Label runat="server" Visible="false" ID="lblPhotoUserId"></asp:Label>&nbsp&nbsp&nbsp&nbsp
                                                </div>
                                                <div class="fltr" style="width: 180px;">
                                                    <asp:LinkButton ID="lbtnPhotoDown" runat="server" OnClick="lbtnPhotoDown_Click" ForeColor="Green">Download</asp:LinkButton>&nbsp&nbsp&nbsp&nbsp
                                                    <asp:LinkButton ID="lbtnPhotoDelete" ForeColor="Red" OnClick="lbtnPhotoDelete_Click"
                                                        runat="server">Delete</asp:LinkButton>
                                                </div>
                                                <br />
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lbtnPhotoDown" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                    <asp:Repeater ID="rptrDocuments" runat="server">
                                        <ItemTemplate>
                                            <asp:UpdatePanel ID="upnlDocs" runat="server">
                                                <ContentTemplate>
                                                    <div class="fltl">
                                                        <asp:Label runat="server" Visible="false" ID="lblDocId" Text='<%#Eval("UserDocumentId") %>'></asp:Label>
                                                        <asp:Label runat="server" ID="lblDocName" Text='<%#Eval("DocumentName") %>'></asp:Label>
                                                    </div>
                                                    <div class="fltr">
                                                        <asp:LinkButton ID="lbtnDocName" runat="server" ForeColor="Green" OnClick="lbtnDocName_Click"
                                                            CommandArgument='<%#Eval("DocumentName") %>'>Download</asp:LinkButton>&nbsp&nbsp&nbsp&nbsp
                                                        <asp:LinkButton ID="lbtnDocDelete" ForeColor="Red" OnClick="lbtnDocDelete_Click"
                                                            runat="server" CommandArgument='<%#Eval("UserDocumentId") %>'>Delete</asp:LinkButton>&nbsp&nbsp&nbsp&nbsp
                                                    </div>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="lbtnDocName" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="upnlEditPermissions" runat="server">
                        <ContentTemplate>
                            <%--BU Start--%>
                            <div id="divEditBusinessUnits" visible="false" runat="server">
                                <div class="check_baa">
                                    <div class="text-inner_a">
                                        <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNITS%>"></asp:Literal>
                                    </div>
                                    <div class="check_ba">
                                        <div class="text-inner mster-input">
                                            <asp:CheckBox runat="server" ID="BUcbEditAll" Text="All" onclick="BUEditCheckAll();" />
                                            <asp:CheckBoxList ID="cblEditBusinessUnits" Width="30%" RepeatDirection="Horizontal"
                                                onclick="BUEditUnCheckAll();" RepeatColumns="3" runat="server">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div id="divEditNoBU" runat="server" visible="false" class="span_color">
                                There is no Business Units please select another BusinessUnit.
                            </div>
                            <%--BU END--%>
                            <div class="clr">
                            </div>
                            <%--BU,SU,SC Start--%>
                            <div id="divEditServiceCenters" visible="false" runat="server">
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNITS%>"></asp:Literal>
                                            <span class="span_star">*</span>
                                        </label>
                                        <div class="space">
                                        </div>
                                        <asp:DropDownList ID="ddlEditBusinessUnits" CssClass="text-box select_box" runat="server"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlEditBusinessUnits_SelectedIndexChanged">
                                            <asp:ListItem Text="Select Business Unit" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                        <br />
                                        <span class="span_color" id="spanEditddlBusinessUnits"></span>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:Resource, SERVICE_UNITS%>"></asp:Literal>
                                            <span class="span_star">*</span></label>
                                        <div class="space">
                                        </div>
                                        <asp:DropDownList ID="ddlEditServiceUnits" CssClass="text-box select_box" Enabled="false"
                                            runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlEditServiceUnits_SelectedIndexChanged">
                                            <asp:ListItem Text="Select Service Unit" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                        <br />
                                        <span class="span_color" id="spanEditddlServiceUnits"></span>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div id="lblEditSC" runat="server" visible="false">
                                    <div class="check_baa">
                                        <div class="text-inner_a">
                                            <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, SERVICE_CENTERS%>"></asp:Literal>
                                            <span class="span_star">*</span>
                                        </div>
                                        <div class="check_ba">
                                            <div class="text-inner mster-input">
                                                <asp:CheckBox runat="server" Visible="false" ID="SCcbEditAll" Text="All" onclick="SCEditCheckAll();"
                                                    AutoPostBack="true" OnCheckedChanged="cblEditServiceCenters_SelectedIndexChanged" />
                                                <div id="divEditCblSC" runat="server">
                                                    <asp:CheckBoxList ID="cblEditServiceCenters" onclick="SCEditUnCheckAll();" Width="45%"
                                                        AutoPostBack="true" OnSelectedIndexChanged="cblEditServiceCenters_SelectedIndexChanged"
                                                        RepeatDirection="Horizontal" RepeatColumns="3" runat="server">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divEditNoSC" runat="server" visible="false" class="span_color">
                                There is no Service Centers please select another ServiceUnit.
                            </div>
                            <%--BU,SU,SC END--%>
                            <div class="clr">
                            </div>
                            <%--Cash Offices Start--%>
                            <div id="divEditCashOffices" visible="false" runat="server">
                                <div class="check_baa">
                                    <div class="text-inner_a">
                                        <asp:Literal ID="Literal22" runat="server" Text="<%$ Resources:Resource, CASH_OFFICES%>"></asp:Literal>
                                        <span class="span_star">*</span>
                                    </div>
                                    <div class="check_ba">
                                        <div class="text-inner mster-input">
                                            <asp:CheckBox runat="server" ID="COcbEditAll" Text="All" onclick="COEditCheckAll();" />
                                            <div class="space">
                                            </div>
                                            <asp:CheckBoxList ID="cblEditCashOffices" onclick="COEditUnCheckAll();" Width="45%"
                                                RepeatDirection="Horizontal" RepeatColumns="3" runat="server">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div id="divEditNoCO" runat="server" visible="false" class="span_color">
                                There is no Cash Offices please select another ServiceCenter.
                            </div>
                            <%--Cash Offices End--%>
                            <%--ServiceUnits Start--%>
                            <div id="divEditServiceUnits" visible="false" runat="server">
                                <div class="inner-box1">
                                    <div class="text-inner">
                                        <label for="name">
                                            <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, BUSINESS_UNITS%>"></asp:Literal>
                                            <span class="span_star">*</span></label>
                                        <div class="space">
                                        </div>
                                        <asp:DropDownList ID="ddlEditBUServiceUnits" CssClass="text-box select_box" runat="server"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlEditBUServiceUnits_SelectedIndexChanged">
                                            <asp:ListItem Text="Select Business Unit" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="clr">
                                </div>
                                <div class="check_baa">
                                    <div class="text-inner_a">
                                        <asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:Resource, SERVICE_UNITS%>"></asp:Literal><span
                                            class="span_star">*</span>
                                    </div>
                                    <div class="check_ba">
                                        <div class="text-inner mster-input">
                                            <asp:CheckBox runat="server" Visible="false" ID="SUcbEditAll" Text="All" onclick="SUEditCheckAll();" />
                                            <div id="divEditCblSU" runat="server">
                                                <asp:CheckBoxList ID="cblEditServiceUnits" onclick="SUEditUnCheckAll();" Width="45%"
                                                    RepeatDirection="Horizontal" RepeatColumns="3" runat="server">
                                                </asp:CheckBoxList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clr">
                            </div>
                            <div id="divEditNoSU" runat="server" visible="false" class="span_color">
                                There is no Service Units please select another BusinessUnit.
                            </div>
                            <%--ServiceUnits End--%>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="space">
                    </div>
                    <div class="box_total">
                        <div class="box_totalTwi_c">
                            <asp:Button ID="btnUpdate" OnClick="btnUpdate_Click" OnClientClick="return EditValidate();"
                                Text="<%$ Resources:Resource, UPDATE%>" CssClass="box_s" runat="server" />
                        </div>
                        <div style="clear: both;">
                        </div>
                        <div class="box_totalTwo_b">
                            <div class="text-heading_2">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:ModalPopupExtender ID="mpeDocpopup" runat="server" PopupControlID="pnldeletedoc"
                TargetControlID="btndeletedoc" BackgroundCssClass="modalBackground" CancelControlID="btnCanceldoc">
            </asp:ModalPopupExtender>
            <asp:Button ID="btndeletedoc" runat="server" Text="Button" Style="display: none;" />
            <asp:Panel ID="pnldeletedoc" runat="server" DefaultButton="btnDeActiveOk" CssClass="modalPopup"
                Style="display: none; border-color: #639B00; z-index: 1000001;">
                <div class="popheader" style="background-color: red;">
                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                </div>
                <div class="footer" align="right">
                    <div class="fltr popfooterbtn">
                        <br />
                        <asp:Button ID="btndeleteok" runat="server" OnClick="btndeleteok_Click" Text="<%$ Resources:Resource, OK%>"
                            CssClass="ok_btn" />
                        <asp:Button ID="btnCanceldoc" runat="server" Text="<%$ Resources:Resource, CANCEL%>"
                            CssClass="cancel_btn" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
            <%--Edit User End Here--%>
            <asp:HiddenField ID="hfPopup" runat="server" />
            <asp:HiddenField ID="hfdocid" runat="server" />
            <asp:HiddenField ID="hfPopup1" runat="server" />
            <%-- Alert popup Start--%>
            <asp:ModalPopupExtender ID="mpeAlert" runat="server" PopupControlID="PanelAlert"
                    TargetControlID="btnAlertDeActivate" BackgroundCssClass="modalBackground" CancelControlID="btnAlertOk">
                </asp:ModalPopupExtender>
                <asp:Button ID="btnAlertDeActivate" runat="server" Text="Button" CssClass="popbtn" />
                <asp:Panel ID="PanelAlert" runat="server" CssClass="modalPopup" class="poppnl" Style="display: none;">
                    <div class="popheader popheaderlblred" id="pop" runat="server">
                        <asp:Label ID="lblAlertMsg" runat="server"></asp:Label>
                    </div>
                    <div class="footer popfooterbtnrt">
                        <div class="fltr popfooterbtn">
                            <br />
                            <asp:Button ID="btnAlertOk" runat="server" Text="<%$ Resources:Resource, OK%>" CssClass="cancel_btn" />
                        </div>
                        <div class="clear pad_5">
                        </div>
                    </div>
                </asp:Panel>

            <asp:ModalPopupExtender ID="mpegridalert" runat="server" PopupControlID="Panelgridalert"
                TargetControlID="btngridalert" BackgroundCssClass="modalBackground" CancelControlID="btngridalertok">
            </asp:ModalPopupExtender>
            <asp:Button ID="btngridalert" runat="server" Text="Button" Style="display: none;" />
            <asp:Panel ID="Panelgridalert" runat="server" DefaultButton="btngridalertok" CssClass="modalPopup"
                Style="display: none;">
                <div class="popheader popheaderlblred">
                    <asp:Label ID="lblgridalertmsg" runat="server"></asp:Label>
                </div>
                <div class="footer popfooterbtnrt">
                    <div class="fltr popfooterbtn">
                        <asp:Button ID="btngridalertok" runat="server" Text="<%$ Resources:Resource, OK%>"
                            CssClass="ok_btn" />
                    </div>
                    <div class="clear pad_5">
                    </div>
                </div>
            </asp:Panel>
            <%-- Alert popup Closed--%>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpdate" />
        </Triggers>
    </asp:UpdatePanel>
    <%--==============Escape button script starts==============--%>
    <script type="text/javascript">
        $(document).keydown(function (e) {
            // ESCAPE key pressed 
            if (e.keyCode == 27) {
                $(".hidepopup").fadeOut("slow");
                $(".modalPopup").fadeOut("slow");
                $(".modalBackground").fadeOut("slow");
                document.getElementById("overlay").style.display = "none";
            }
        });
    </script>
    <%--==============Escape button script ends==============--%>
    <%--==============Validation script ref starts==============--%>
    <script src="../JavaScript/CommonValidations.js" type="text/javascript"></script>
    <script src="../JavaScript/PageValidations/AddUsers.js" type="text/javascript"></script>
    <%--==============Validation script ref ends==============--%>

</asp:Content>
