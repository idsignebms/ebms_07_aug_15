﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UMS_NigriaDAL;

namespace UMS_Nigeria
{
    public partial class ValidateUser : System.Web.UI.Page
    {
        protected void Page_PreInit()
        {
            SecurityDongle securityDongle = new SecurityDongle();
            if (securityDongle.DongleMessage != "Success")
            {
                Response.Write(securityDongle.DongleMessage);
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}