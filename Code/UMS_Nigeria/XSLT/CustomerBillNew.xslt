﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <table bgcolor="#649b01"  style="border-bottom: 10px #000 solid;" width="100%" border="0" align="center" >
      <tr style="background-color: #649b01;">
        <td>
          <table width="100%" border="0" align="center">
            <tr>
              <td width="25%">

              </td>
              <td width="75%">
                <h1 style="color: #fff;font-size: 10px;font-weight: bold;text-align: center;text-shadow: 2px 4px 3px rgba(0,0,0,0.5);margin-top: 5px;">
                  BENIN ELECTRICITY DISTRIBUTION COMPANY
                </h1>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td>
          <table align="center"  border="0">
            <tr>
              <td bgcolor="#000"  width="20%"  style="text-align: center;" align="center">
                <h2 style="color: #fff;font-size: 8px;margin: 0;padding-bottom:20px;text-align: center;">
                  Electricity Bill
                </h2>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <table width="100%" border="0" align="center" bgcolor="#fff" cellpadding="0" cellspacing="0">
      <tr>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Name</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/Name"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Account No</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/AccountNo"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Old Account No</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/OldAccountNo"/>
        </td>
      </tr>
      <tr>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Billing Address</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/BillingAddress"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Meter No</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/MeterNo"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Connection Type</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/ConnectionType"/>
        </td>
      </tr>
      <tr>
        <td>

        </td>
        <td>

        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Business Unit</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/BusinessUnit"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Tariff Category</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/TariffCategory"/>
        </td>
      </tr>
      <tr>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Sevices Address</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/ServiceAddress"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Book Group</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/BookGroup"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Tariff Type</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/TariffType"/>
        </td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Book No</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/BookNo"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Bill Date</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/BillDate"/>
        </td>
      </tr>
      <tr>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Phone No</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/PhoneNo"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Pole No</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/PoleNo"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Bill Type</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/BillType"/>
        </td>
      </tr>
      <tr>
        <td align="left" style="color: #000;font-size: 8px;" colspan="2">
          <strong>E-mail</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Walking Sequence</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/WalkingSequence"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Bill No</strong>
        </td>
        <td align="left" style="color: #000;font-size: 7px;">
          <strong>
            <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/BillNo"/>
          </strong>
        </td>
      </tr>
      <tr>
        <td align="left" style="color: #000;font-size: 7px;" colspan="2">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/EmailId"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>
            Phase
          </strong>
        </td>
        <td  align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/Phase"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>
            Bill Remark
          </strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/BillRemarks"/>
        </td>
      </tr>
    </table>

    <table width="100%" celspacing="1">
      <tr>
        <td colspan="2" style="color: #649b01;font-style: italic;font-size: 9px;font-weight: bold;position: relative;top: -8px;" width="100%">
          Your Electricity Summary
        </td>
      </tr>
      <tr>
        <td width="85%">
          <table width="100%" >
            <tr>
              <td bgcolor="#649b01" border="0" style="font-size:7px;text-align: center;" width="18%">
                <h3 style="font-size:7px;color:#fff;">
                  Previous Balance(=N=)<br/>
                  <span style="background-color:#fff">
                    <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/NetArrears"/>
                  </span>
                </h3>
              </td>
              <td style="text-align: center;" width="2%">
                +-
              </td>
              <td bgcolor="#649b01" style="font-size:7px;border-color:#000;text-align: center;"  width="18%">
                <h3  style="font-size:7px;color:#fff;">
                  Payments(=N=)<br/>
                  <span style="background-color:#fff">
                    <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/Payments"/>
                  </span>
                </h3>
              </td>
              <td style="text-align: center;" width="2%">
                +-
              </td>
              <td bgcolor="#649b01" style="font-size:7px;border-color:#000;text-align: center;"  width="18%">
                <h3  style="font-size:7px;color:#fff;">
                  Adjustments(=N=)<br/>
                  <span style="background-color:#fff">
                    <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/Adjustments"/>
                  </span>
                </h3>
              </td>
              <td  style="text-align: center;" width="2%">
                +-
              </td>
              <td bgcolor="#649b01"  style="font-size:7px;border-color:#000;text-align: center;" width="18%">
                <h3  style="font-size:7px;color:#fff;">
                  Bill Amount(=N=)<br/>
                  <span style="background-color:#fff">
                    <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/BillAmount"/>
                  </span>
                </h3>
              </td>
              <td  style="text-align: center;" width="2%">
                =
              </td>
              <td bgcolor="#649b01" style="font-size:7px;border-color:#000;text-align: center;" width="20%">
                <h3 style="font-size:7px;color:#fff;">
                  Net Amount Payable(=N=)<br/>
                  <span style="background-color:#fff">
                    <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/NetAmountPayable"/>
                  </span>
                </h3>

              </td>
            </tr>
          </table>
        </td>
        <td width="15%" bgcolor="#649b01">
          <table width="100%"  align="center">
            <tr>
              <td  style="color: #fdfd00;border-radius: 6px;padding-left: 14px;">
                <span style="color: #fdfd00;font-size: 8px;font-weight: bold;">
                  Due Date
                </span>
                <br/>
                <span style="color: #fff;font-size: 7px;font-weight: bold;">
                  <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/DueDate"/>
                </span>
              </td>
              <tr>
              </tr>
              <td style="color: #fdfd00;border-radius: 8px;padding-left: 14px;">
                <span style="color: #fdfd00;font-size: 6px;font-weight: bold;">
                  Amount Payable(=N=)
                </span>
                <br/>
                <span style="color: #fff;font-size: 7px;font-weight: bold;">
                  <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/NetAmountPayable"/>
                </span>
                <br/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <table width="100%" align="center" cellpadding="0" cellspacing="0">
      <xsl:choose>
        <xsl:when test="/CustomerBillPDFDetails/CustomerBillDetails/LastPaidDate = '--'">
          <tr>
            <td  style="font-size: 8px; font-weight: bold; padding-bottom: 5px;" colspan="2" align="center">
              No PREVIOUS PAYMENTS
            </td>
          </tr>
        </xsl:when>
        <xsl:otherwise>
          <tr>
            <td style="font-size: 8px; font-weight: bold; padding-bottom: 5px;">
              LAST PAYMENT DATE: <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/LastPaidDate"/>
            </td>
            <td style="font-size: 8px; font-weight: bold; padding-bottom: 5px;">
              AMOUNT : <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/LastPaidAmount"/> =N=
            </td>
          </tr>
        </xsl:otherwise>
      </xsl:choose>
    </table>
    <div>
      <br/>
    </div>
    <table border="1" width="100%">
      <tr>
        <td rowspan="2" align="center" bgcolor="#649b01"  style="color: #fff; font-size: 9px;border-radius: 6px;" >
          <h3  style="color: #fff; font-size: 9px; left;padding-left:5px;text-align: center;">
            Meter No
          </h3>
        </td>
        <td colspan="2" align="center" bgcolor="#649b01" style="color: #fff; font-size: 9px;border-radius: 6px;padding:5px;">
          <h3  style="color: #fff; font-size: 9px; left;padding-left:5px;text-align: center;">
            Current Reading
          </h3>
        </td>
        <td colspan="2" align="center" bgcolor="#649b01" style="color: #fff; font-size: 9px;border-radius: 6px;">
          <h3  style="color: #fff; font-size: 9px; left;padding-left:5px;text-align: center;">
            Previous Reading
          </h3>
        </td>
        <td rowspan="2" align="center" bgcolor="#649b01" style="color: #fff; font-size: 9px;border-radius: 6px;">
          <h3  style="color: #fff; font-size: 9px; left;padding-left:5px;text-align: center;">
            Multiplier
          </h3>
        </td>
        <td rowspan="2" align="center" bgcolor="#649b01" style="color: #fff; font-size: 9px;border-radius: 6px;">
          <h3  style="color: #fff; font-size: 9px; left;padding-left:5px;text-align: center;">
            Consumption
          </h3>
        </td>
      </tr>
      <tr>
        <td style="font-size: 8px;color: #fff;font-weight: normal;" bgcolor="#649b01" align="center">
          <h3  style="color: #fff; font-size: 8px; left;padding-left:5px;text-align: center;">
            Date
          </h3>
        </td>
        <td style="font-size: 8px;color: #fff;font-weight: normal;" bgcolor="#649b01" align="center">
          <h3  style="color: #fff; font-size: 8px; left;padding-left:5px;text-align: center;">
            Reading
          </h3>
        </td>
        <td style="font-size: 8px;color: #fff;font-weight: normal;" bgcolor="#649b01" align="center">
          <h3  style="color: #fff; font-size: 8px; left;padding-left:5px;text-align: center;">
            Date
          </h3>
        </td>
        <td style="font-size: 8px;color: #fff;font-weight: normal;" bgcolor="#649b01" align="center">
          <h3  style="color: #fff; font-size: 8px; left;padding-left:5px;text-align: center;">
            Reading
          </h3>
        </td>
      </tr>
      <xsl:choose>
        <xsl:when test="count(/CustomerBillPDFDetails/CustomerLastReadingsDetails) = 0">
          <tr style="color: #000; font-size: 6px border:1px;">
            <td style="font-size: 8px;color: #000;font-weight: normal;" align="center" >
              Direct Customer
            </td>
            <td style="font-size: 8px;color: #000;font-weight: normal;" align="center">

            </td>
            <td style="font-size: 8px;color: #000;font-weight: normal;" align="center">

            </td>
            <td style="font-size: 8px;color: #000;font-weight: normal;" align="center">

            </td>
            <td style="font-size: 8px;color: #000;font-weight: normal;" align="center">

            </td>
            <td style="font-size: 8px;color: #000;font-weight: normal;" align="center">
              1
            </td>
            <td style="font-size: 8px;color: #000;font-weight: normal;" align="center">
              <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/Consumption"/>
            </td>
          </tr>
        </xsl:when>
        <xsl:otherwise>
          <xsl:for-each select="CustomerBillPDFDetails/CustomerLastReadingsDetails">
            <tr style="color: #000; font-size: 6px border:1px;">
              <td style="font-size: 8px;color: #000;font-weight: normal;" align="center" >
                <xsl:value-of select="MeterNo"/>
              </td>
              <td style="font-size: 8px;color: #000;font-weight: normal;" align="center">
                <xsl:value-of select="CurrentReadingDate"/>
              </td>
              <td style="font-size: 8px;color: #000;font-weight: normal;" align="center">
                <xsl:value-of select="CurrentReading"/>
              </td>
              <td style="font-size: 8px;color: #000;font-weight: normal;" align="center">
                <xsl:value-of select="PreviousReadingDate"/>
              </td>
              <td style="font-size: 8px;color: #000;font-weight: normal;" align="center">
                <xsl:value-of select="PreviousReading"/>
              </td>
              <td style="font-size: 8px;color: #000;font-weight: normal;" align="center">
                <xsl:value-of select="Multiplier"/>
              </td>
              <td style="font-size: 8px;color: #000;font-weight: normal;" align="center">
                <xsl:value-of select="Consumption"/>
              </td>
            </tr>
          </xsl:for-each>
        </xsl:otherwise>
      </xsl:choose>
    </table>
    <div>
      <br/>
    </div>
    <xsl:choose>
      <xsl:when test="CustomerBillPDFDetails/CustomerBillDetails/IsCAPMIMeter = 1">
        <table width="100%" border="1">
          <tr>
            <td colspan="3"  bgcolor="#649b01" style="font-size:9px;">
              <h3 style="background-color: #649b01;margin: 0;color: #fff;text-align: center;font-size:8px;">
                Paid Meter Details
              </h3>
            </td>
          </tr>
          <tr style="height:22px;">
            <td style="font-size:7px;" align="center">
              Paid Meter Bal : <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/PaidMeterAmount"/>
            </td>
            <td style="font-size:7px;"  align="center">
              Paid Meter Deduction : <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/PaidMeterDeduction"/>
            </td>
            <td style="font-size:7px;"  align="center">
              Current Paid Meter Bal : <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/PaidMeterBalance"/>
            </td>
          </tr>
        </table>
        <div>
          <br/>
        </div>
      </xsl:when>
    </xsl:choose>
    <table width="100%" border="1">
      <tr>
        <td colspan="4"  bgcolor="#649b01" style="font-size:9px;margin-bottom:10px;" align="center">
          <h3 style="color: #fff;font-size:9px;padding-bottom:20px;text-align: center;">
            Energy Charges Details
          </h3>
        </td>
      </tr>
      <tr>
        <td  style="font-size: 8px;color: #fff;font-weight: normal;" bgcolor="#649b01" align="center">
          Tariff
        </td>
        <td  style="font-size: 8px;color: #fff;font-weight: normal;" bgcolor="#649b01" align="center">
          Units
        </td>
        <td  style="font-size: 8px;color: #fff;font-weight: normal;" bgcolor="#649b01" align="center">
          Rate
        </td>
        <td  style="font-size: 8px;color: #fff;font-weight: normal;" bgcolor="#649b01" align="center">
          Amount(=N=)
        </td>
      </tr>
      <tr>
        <td style="font-size: 8px;color: #000;font-weight: normal;" align="center">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/TariffType"/>
        </td>
        <td style="font-size: 8px;color: #000;font-weight: normal;" align="center">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/Consumption"/>
        </td>
        <td style="font-size: 8px;color: #000;font-weight: normal;" align="center">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/TariffRate"/>
        </td>
        <td style="font-size: 8px;color: #000;font-weight: normal;" align="center">
          <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/EnergyCharges"/>
        </td>
      </tr>
    </table>
    <div>
      <br/>
    </div>

    <table width="100%" border="1">
      <tr>
        <td bgcolor="#649b01" width="75%">
          <h3  style="color: #fff; font-size: 8px; left;padding-left:5px;text-align: left;">
            Bill Details :  Bill Period - <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/BillPeriod"/>
          </h3>
        </td>
        <td bgcolor="#649b01" width="25%">
          <h4 style="background-color: #649b01; color: #fff; font-size: 8px;	line-height: 12px;text-align: left;padding-left:5px;">
            Amount(=N=)
          </h4>
        </td>
      </tr>
      <tr>
        <td style="font-size: 9px;	line-height: 14px; padding-bottom:4px; padding-top: 5px;" width="75%">
          <h4 style="padding-left:5px;">
            Energy Charges
          </h4>
        </td>
        <td style="font-size: 9px;line-height: 14px; padding-bottom:4px; padding-top: 5px;" width="25%">
          <h4 style="padding-left:5px;">
            <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/EnergyCharges"/>
          </h4>
        </td>
      </tr>
      <xsl:for-each select="CustomerBillPDFDetails/CustomerBillAdditionalChargeDetails">
        <tr>
          <td style="font-size: 9px;line-height: 14px; padding-bottom:4px; padding-top: 5px;" width="75%">
            <h4 style="padding-left:5px;">
              <xsl:value-of select="FixedChargeName"/>
            </h4>
          </td>
          <td style="font-size: 9px;line-height: 14px; padding-bottom:4px; padding-top: 5px;" width="25%">
            <h4 style="padding-left:5px;">
              <xsl:value-of select="FixedChargeAmount"/>
            </h4>
          </td>
        </tr>
      </xsl:for-each>
      <tr>
        <td style="font-size: 9px;line-height: 14px; padding-bottom:4px; padding-top: 5px;" width="75%">
          <h4 style="padding-left:5px;">
            Tax @<xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/TaxPercentage"/>%
          </h4>
        </td>
        <td style="font-size: 9px;line-height: 14px; padding-bottom:4px; padding-top: 5px;" width="25%">
          <h4 style="padding-left:5px;">
            <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/TaxAmount"/>
          </h4>
        </td>
      </tr>
      <tr>
        <td style="color: #649b01;font-size: 10px;line-height: 14px; padding-bottom:4px; padding-top: 5px;" width="75%">
          <h4 style="padding-left:5px;">
            Total Bill
          </h4>
        </td>
        <td style="color: #649b01;font-size: 10px;line-height: 14px; padding-bottom:4px; padding-top: 5px;" width="25%">
          <h4 style="padding-left:5px;">
            <xsl:value-of select="CustomerBillPDFDetails/CustomerBillDetails/BillAmount"/>
          </h4>
        </td>
      </tr>
    </table>
    <div>
      <br/>
    </div>
    <table width="100%" style="background-color: #fff;border-collapse: collapse;margin-top: 7px; font-size: 8px" border="1">
      <tr>
        <td style="padding: 5px;color:#fff;" colspan="4" bgcolor="#649b01" align="center">
          Consumption History
        </td>
      </tr>
      <tr>
        <td align="center">
          Billing Month
        </td>
        <td align="center">
          Consumption
        </td>
        <td align="center">
          Total Bill Payable(=N=)
        </td>
        <td align="center">
          Billing Type
        </td>
      </tr>
      <xsl:choose>
        <xsl:when test="count(/CustomerBillPDFDetails/CustomerLastConsumptionDetails) = 0">
          <tr>
            <td colspan="4" align="center">
              No Previous History
            </td>
          </tr>
        </xsl:when>
        <xsl:otherwise>
          <xsl:for-each select="CustomerBillPDFDetails/CustomerLastConsumptionDetails">
            <tr>
              <td align="center">
                <xsl:value-of select="BillMonth"/>
              </td>
              <td align="center">
                <xsl:value-of select="Consumption"/>
              </td>
              <td align="center">
                <xsl:value-of select="TotalBillPayable"/>
              </td>
              <td align="center">
                <xsl:value-of select="BillingType"/>
              </td>
            </tr>
          </xsl:for-each>
        </xsl:otherwise>
      </xsl:choose>
    </table>
    <div>
      <br/>
    </div>
    <table width="100%" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="left" style="font-size:5px;">* Make your cheque/DD payable to xxxxxxxxxxxxxxxxxx</td>
      </tr>
      <tr>
        <td align="left" style="font-size:5px;">* Cheque should be A/c payee &amp; payable at xxxxxx and not post - dated</td>
      </tr>
      <tr>
        <td align="left" style="font-size:5px;"></td>
      </tr>
      <tr>
        <td align="left" style="font-size:5px;">
          <strong>Nearest Payment Centers</strong>
        </td>
      </tr>
      <tr>
        <td align="left" style="font-size:5px;">(1) Benin City, Nigeria</td>
      </tr>
      <tr>
        <td align="left" style="font-size:5px;">(2) Benin City, Nigeria</td>
      </tr>
    </table>
    <table style="border: 1px #406500 solid;margin-top: 17px;" width="100%" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td>

        </td>
      </tr>
    </table>
    <table width="100%" align="center" cellpadding="0" cellspacing="0" class="footer">
      <tr>
        <td>
          <table width="100%" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td style="color: #fff; font-size: 5px; margin: 0; padding: 0;">
                <h5>5, AKPAKPAVA Road, Benin City Edo State.</h5>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </xsl:template>
</xsl:stylesheet>


