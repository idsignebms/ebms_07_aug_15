﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <table bgcolor="#649b01"  style="border-bottom: 10px #000 solid;" width="100%" border="0" align="center" >
      <tr style="background-color: #649b01;">
        <td>
          <table width="100%" border="0" align="center">
            <tr>
              <td width="25%">
                <img  style="margin-bottom: 3px;margin-top: 8px;" src="E:/JEEVAN/iTextSharp/ConvertHTMLToPDFusing iTextSharp/images/bedc_logo.png" alt="bedc_logo.png" width="120" height="40"  />
              </td>
              <td width="75%">
                <h1 style="color: #fff;font-size: 10px;font-weight: bold;text-align: center;text-shadow: 2px 4px 3px rgba(0,0,0,0.5);margin-top: 5px;">
                  BENIN ELECTRICITY DISTRIBUTION COMPANY
                </h1>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td>
          <table align="center"  border="0">
            <tr>
              <td bgcolor="#000"   style="border-radius: 8px;position: relative;top: -24px;">
                <h2 style="color: #fff;font-size: 8px;margin: 0;padding-bottom: 3px;text-align: center;">
                  Electricity Bill
                </h2>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <table width="100%" border="0" align="center" bgcolor="#fff">
      <tr>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Name :</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillDetails/CustomerDetails/CustomerName"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Phase</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillDetails/CustomerDetails/CustomerName"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Account No.</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillDetails/CustomerDetails/AccountNo"/>
        </td>
      </tr>
      <tr>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Billing Address:</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          billing addresssss
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Meter No.</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillDetails/CustomerDetails/CustomerName"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Connection Type</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillDetails/CustomerDetails/CustomerName"/>
        </td>
      </tr>
      <tr>
        <td>

        </td>
        <td>

        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Business Unit</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillDetails/CustomerDetails/CustomerName"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Tariff Category</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillDetails/CustomerDetails/CustomerName"/>
        </td>
      </tr>
      <tr>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Sevices Address:</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillDetails/CustomerDetails/CustomerName"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Book Group</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillDetails/CustomerDetails/CustomerName"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Tariff Type</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillDetails/CustomerDetails/CustomerName"/>
        </td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Book No.</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillDetails/CustomerDetails/CustomerName"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Bill Date</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillDetails/CustomerDetails/CustomerName"/>
        </td>
      </tr>
      <tr>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>PH No:</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillDetails/CustomerDetails/CustomerName"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Pole No.</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillDetails/CustomerDetails/CustomerName"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Bill Type</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillDetails/CustomerDetails/CustomerName"/>
        </td>
      </tr>
      <tr>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>E-mail:</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillDetails/CustomerDetails/CustomerName"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Walking Sequence</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillDetails/CustomerDetails/CustomerName"/>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Bill No.</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillDetails/CustomerDetails/CustomerName"/>
        </td>
      </tr>
      <tr>
        <td align="left" style="color: #000;font-size: 8px;">

        </td>
        <td>

        </td>
        <td align="left" style="color: #000;font-size: 8px;">

        </td>
        <td>

        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <strong>Bill Remark</strong>
        </td>
        <td align="left" style="color: #000;font-size: 8px;">
          <xsl:value-of select="CustomerBillDetails/CustomerDetails/CustomerName"/>
        </td>
      </tr>
    </table>
    <table>
      <tr>
        <td width="85%">
          <table>
            <tr>
              <td colspan="7" style="color: #649b01;font-style: italic;font-size: 9px;font-weight: bold;position: relative;top: -8px;" width="100%">
                Your Electricity Summary
              </td>
            </tr>
            <tr>
              <td bgcolor="#649b01" border="1" style="border-left-color: red; border-top-color: red; border-bottom: red thin solid; border-right-color: red" width="23%">
                <h3 style="font-size:7px;color:#fff;">
                  Arrers/Refund<br/>
                  <span style="background-color:#fff">
                    10,000.00
                  </span>
                </h3>
              </td>
              <td class="special-chars" width="2%">
                +-
              </td>
              <td bgcolor="#649b01"  width="23%">
                <h3  style="font-size:7px;color:#fff;">
                  Adjustments<br/>
                  <span style="background-color:#fff">
                    10,000.00
                  </span>
                </h3>
              </td>
              <td class="special-chars" width="2%">
                +-
              </td>
              <td bgcolor="#649b01"  style="font-size:7px;" width="23%">
                <h3  style="font-size:7px;color:#fff;">
                  Bill Amount<br/>
                  <span style="background-color:#fff">
                    10,000.00
                  </span>
                </h3>
              </td>
              <td class="special-chars" width="2%">
                =
              </td>
              <td bgcolor="#649b01" style="font-size:7px;border-color:#000; border-radius: 7px;margin: 0;padding: 0 0 46px;text-align: center;" width="25%">
                <h3 style="font-size:7px;color:#fff;">
                  Net Amount Payable<br/>
                  <span style="background-color:#fff">
                    10,000.00
                  </span>
                </h3>

              </td>
            </tr>
          </table>
        </td>
        <td width="15%">
          <table bgcolor="#649b01">
            <tr>
              <td  style="color: #fdfd00;border-radius: 6px;padding-left: 14px;">
                <span style="color: #fdfd00;font-size: 6px;font-weight: bold;">
                  Due Date
                </span>
                <br/>
                <span style="color: #fff;font-size: 6px;font-weight: bold;">
                  <xsl:value-of select="CustomerBillDetails/CustomerDetails/DueDate"/>
                </span>
              </td>
              <tr>
              </tr>
              <td style="color: #fdfd00;border-radius: 6px;padding-left: 14px;">
                <span style="color: #fdfd00;font-size: 6px;font-weight: bold;">
                  Amount Payable
                </span>
                <br/>
                <span style="color: #fff;font-size: 6px;font-weight: bold;">
                  <xsl:value-of select="CustomerBillDetails/CustomerDetails/TotalBillAmountWithTax"/>
                </span>
                <br/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <table width="100%" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td style="font-size: 8px; font-weight: bold; padding-bottom: 5px;">
          Your Last Payment of Rs. 760.00 received on dated 05-Feb-2015
        </td>
      </tr>
    </table>

    <table  style="border-collapse: collapse;margin-top: 7px;width: 100%;margin: -1px 0 0 0;" width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td rowspan="2" class="meterNo" bgcolor="#649b01"  style="color: #fff; font-size: 9px" >
          Meter No.
        </td>
        <td colspan="2" bgcolor="#649b01" style="color: #fff; font-size: 9px">
          Current Reading
        </td>
        <td colspan="2" bgcolor="#649b01" style="color: #fff; font-size: 9px">
          Previous Reading
        </td>
        <td rowspan="2" bgcolor="#649b01" style="color: #fff; font-size: 9px">
          Multiplier
        </td>
        <td rowspan="2" bgcolor="#649b01" style="color: #fff; font-size: 9px;border-radius: 0 6px 0 0;border-left: none !important;border-right: none !important;border-top: none !important;">
          Consumption
        </td>
      </tr>
      <tr>
        <td style="font-size: 8px;color: #fff;font-weight: normal;" bgcolor="#649b01">
          Date
        </td>
        <td style="font-size: 8px;color: #fff;font-weight: normal;" bgcolor="#649b01">
          Reading
        </td>
        <td style="font-size: 8px;color: #fff;font-weight: normal;" bgcolor="#649b01">
          Date
        </td>
        <td style="font-size: 8px;color: #fff;font-weight: normal;" bgcolor="#649b01">
          Reading
        </td>
      </tr>
      <tr style="color: #000; font-size: 6px border:1px;">
        <td>
          021352AD
        </td>
        <td>
          20 June 2015
        </td>
        <td>
          12452
        </td>
        <td>
          25 July 2015
        </td>
        <td>
          14452
        </td>
        <td>
          1
        </td>
        <td>
          2000
        </td>
      </tr>
    </table>

    <table width="100%" border="0" align="center">
      <tr>
        <td valign="top" width="32%">
          <table style="border-radius: 7px;border: 1px #649b01 solid;" width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#649b01"  valign="center">
                <h3 style="background-color: #649b01; border-radius: 6px 6px 0 0; margin: 0;color: #fff; text-align: center;font-size:9px">
                  Paid Meter Details
                </h3>
              </td>
            </tr>
            <tr>
              <td style="font-size: 6px;">
                Paid Meter Bal : 20,000
              </td>
            </tr>
            <tr>
              <td style="font-size: 6px;">
                Paid Meter Deduction : 15,000
              </td>
            </tr>
            <tr>
              <td style="font-size: 6px;">
                Current Paid Meter Bal : 5,000
              </td>
            </tr>
          </table>
        </td>
        <td width="1%">
        </td>
        <td valign="top" width="32%">
          <table style="border-radius: 7px;border: 1px #649b01 solid;" width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td  bgcolor="#649b01" valign="center">
                <h3 style="background-color: #649b01; border-radius: 6px 6px 0 0; margin: 0;color: #fff; text-align: center;font-size:10px">
                  Energy Charges Details
                </h3>
              </td>
            </tr>
            <tr>
              <td>
                <table width="100%" cellspacing="0" cellpadding="0">
                  <tr>
                    <td style="font-size: 6px;" align="center">
                      Tariff
                    </td>
                    <td style="font-size: 6px;" align="center">
                      Units
                    </td>
                    <td style="font-size: 6px;" align="center">
                      Rate
                    </td>
                    <td style="font-size: 6px;" align="center">
                      Amount
                    </td>
                  </tr>
                  <tr style="font-size: 6px;color:#000;">
                    <td style="font-size: 6px;" align="center">
                      R2
                    </td>
                    <td style="font-size: 6px;" align="center">
                      100
                    </td>
                    <td style="font-size: 6px;" align="center">
                      14.25
                    </td>
                    <td style="font-size: 6px;" align="center">
                      1425.00
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
        <td width="1%">
        </td>
        <td valign="top" width="34%">
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td valign="top" style="border-radius: 7px;border: 1px #649b01 solid; padding: 0;" bgcolor="#649b01" align="center">
                <h3 style="border-radius: 6px 0 0; color: #fff; font-size: 8px;  margin: 0; padding-left: 48px;">
                  Bill Details
                </h3>
              </td>
              <td valign="top" style="border-radius: 7px;border: 1px #649b01 solid; padding: 0;" bgcolor="#649b01"  align="center">
                <h4 style="background-color: #649b01; border-radius: 0 6px 0 0; color: #fff; font-size: 8px; margin: -1px 0 0;">
                  Amount
                </h4>
              </td>
            </tr>
            <tr>
              <td colspan="2" style="font-size:6px;">
                <table width="100%" cellspacing="0" cellpadding="0">
                  <tr>
                    <td style="font-size: 8px; left: 10px;  line-height: 24px; padding-bottom: 33px; padding-top: 12px; position: relative; width: 213px;">
                      Bill Period
                    </td>
                    <td rowspan="4" style="border-left: 1px #649b01 solid">
                      <xsl:value-of select="CustomerBillDetails/CustomerDetails/LastBillDate"/>
                    </td>
                    <td>
                      <xsl:value-of select="CustomerBillDetails/CustomerDetails/BillDate"/>
                    </td>
                  </tr>
                  <tr>
                    <td style="font-size: 8px; left: 10px;  line-height: 24px; padding-bottom: 33px; padding-top: 12px; position: relative; width: 213px;">
                      Energy Charges
                    </td>
                    <td>
                      <xsl:value-of select="CustomerBillDetails/CustomerDetails/NetEnergyCharges"/>
                    </td>
                  </tr>
                  <tr>
                    <td style="font-size: 8px; left: 10px;  line-height: 24px; padding-bottom: 33px; padding-top: 12px; position: relative; width: 213px;">
                      Additional Charges
                    </td>
                    <td>

                    </td>
                  </tr>
                  <tr>
                    <td style="font-size: 8px; left: 10px;  line-height: 24px; padding-bottom: 33px; padding-top: 12px; position: relative; width: 213px;">
                      Tax @<xsl:value-of select="CustomerBillDetails/CustomerDetails/VatPercent"/>%
                    </td>
                    <td>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3">
                      <table style="border-top: 1px #649b01 solid; left: 0px; position: relative;" width="100%"
                          cellspacing="0" cellpadding="0">
                        <tr>
                          <td style="border-right: 1px solid #649b01;color: #649b01; font-size: 11px; font-weight: bold; left: 10px; line-height: 63px; position: relative; width: 203px;">
                            Total Bill
                          </td>
                          <td>
                            <xsl:value-of select="CustomerBillDetails/CustomerDetails/TotalBillAmountWithTax"/>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>

    </table>
    <table width="100%" style="background-color: #fff;border-collapse: collapse;margin-top: 7px; font-size: 8px">
      <tr>
        <td style="padding: 5px;color:#fff;" colspan="5" bgcolor="#649b01" valign="center" align="center">
          Consumption History
        </td>
      </tr>
      <tr>
        <td>
          Billing Month
        </td>
        <td>
          Consumption
        </td>
        <td>
          ADC
        </td>
       
        <td>
          Total Bill Payable
        </td>
        <td>
          Billing Type
        </td>
      </tr>
      <xsl:for-each select="CustomerBillDetails/PastBillDetails">
        <tr>
          <td>
            <xsl:value-of select="BillingMonth"/>
          </td>
          <td>
            <xsl:value-of select="Consumption"/>
          </td>
          <td>
            <xsl:value-of select="ADC"/>
          </td>
          <td>
            <xsl:value-of select="TotalBillAmount"/>
          </td>
          <td>
            <xsl:value-of select="BillType"/>
          </td>
        </tr>
      </xsl:for-each>
    </table>
  </xsl:template>
</xsl:stylesheet>


