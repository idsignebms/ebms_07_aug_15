﻿
//=====To implement google map do following steps=====

//=====script in header=====
//<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
//=====Css in header=====
// .map_canvas
//        {
//            width: 200px;
//            height: 200px;
//        }
//=====Call js in header=====
//<script src="../scripts/GoogleMap.js" type="text/javascript"></script>
//=====Call follwing Script in body:=====
// <script>
//        google.maps.event.addDomListener(window, 'load', initialize(divID, lat, lan);
//  </script>

function initialize(map_canv, lat, lng) {


    var map_canvas = document.getElementById(map_canv);
    //            alert(map_canvas);
    var map_options = {

        center: new google.maps.LatLng(lat, lng),

        zoom: 40,

        mapTypeId: google.maps.MapTypeId.ROADMAP

    }

    var map = new google.maps.Map(map_canvas, map_options)
}