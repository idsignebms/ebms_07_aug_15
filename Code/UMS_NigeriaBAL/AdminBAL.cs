﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;
using iDSignHelper;
using UMS_NigeriaBE;
using UMS_NigriaDAL;

namespace UMS_NigeriaBAL
{
    public class AdminBAL
    {
        #region Members

        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        AdminDAL _ObjAdminDAL = new AdminDAL();
        #endregion

        #region Method
        public XmlDocument Admin(AdminBE _ObjAdminBE, Statement action)
        {
            try
            {
                return _ObjAdminDAL.Admin(_ObjiDsignBAL.DoSerialize<AdminBE>(_ObjAdminBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument Admin(AdminBE _ObjAdminBE, ReturnType value)
        {
            try
            {
                return _ObjAdminDAL.Admin(_ObjiDsignBAL.DoSerialize<AdminBE>(_ObjAdminBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetRolesList(AdminBE objAdminBE)
        {
            XmlDocument xml = null;
            try
            {
                xml = _ObjAdminDAL.GetRolesList(_ObjiDsignBAL.DoSerialize<AdminBE>(objAdminBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument GetAccessLevelsList()
        {
            XmlDocument xml = null;
            try
            {
                xml = _ObjAdminDAL.GetAccessLevelsList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument GetAccessLevel(AdminBE objAdminBE)
        {
            XmlDocument xml = null;
            try
            {
                xml = _ObjAdminDAL.GetAccessLevel(_ObjiDsignBAL.DoSerialize<AdminBE>(objAdminBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument GetRolePermissions(AdminBE _objAdminBe)
        {
            XmlDocument xml = null;
            try
            {
                xml = _ObjAdminDAL.GetRolePermissions(_ObjiDsignBAL.DoSerialize<AdminBE>(_objAdminBe));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion
    }
}
