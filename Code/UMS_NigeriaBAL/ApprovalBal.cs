﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using UMS_NigriaDAL;
using System.Xml;
using UMS_NigeriaBE;
using System.Data;

namespace UMS_NigeriaBAL
{
    public class ApprovalBal
    {
        #region Members

        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        ApprovalDal _objApprovalDal = new ApprovalDal();

        #endregion

        public XmlDocument IsApproval(ApprovalBe _objApprovalBe)
        {
            return _objApprovalDal.IsApproval(_ObjiDsignBAL.DoSerialize<ApprovalBe>(_objApprovalBe));
        }

        public XmlDocument IsAuthorizeApproval(ApprovalBe _objApprovalBe)
        {
            return _objApprovalDal.IsAuthorizeApproval(_ObjiDsignBAL.DoSerialize<ApprovalBe>(_objApprovalBe));
        }

        public DataSet GetApprovalStatusList()
        {
            return _objApprovalDal.GetApprovalStatusList();
        }

        public DataSet GetApprovalSettings()
        {
            return _objApprovalDal.GetApprovalSettings();
        }

        public XmlDocument Get(ApprovalBe _objApprovalBe, ReturnType value)
        {
            return _objApprovalDal.Get(_ObjiDsignBAL.DoSerialize<ApprovalBe>(_objApprovalBe), value);
        }

        public XmlDocument Get(ReturnType value)
        {
            return _objApprovalDal.Get(value);
        }
        public DataSet GetDetails(ReturnType value)
        {
            return _objApprovalDal.GetDetails(value);
        }

        public XmlDocument Insert(ApprovalBe _objApprovalBe, Statement value)
        {
            return _objApprovalDal.Insert(_ObjiDsignBAL.DoSerialize<ApprovalBe>(_objApprovalBe), value);
        }

        public XmlDocument Insert(ApprovalMessagesBe _objApprovalMessagesBe, Statement value)
        {
            return _objApprovalDal.Insert(_ObjiDsignBAL.DoSerialize<ApprovalMessagesBe>(_objApprovalMessagesBe), value);
        }

        public DataSet GetDataSet(ApprovalMessagesBe _objApprovalMessagesBe, ReturnType value)
        {
            return _objApprovalDal.GetDataSet(_ObjiDsignBAL.DoSerialize<ApprovalMessagesBe>(_objApprovalMessagesBe), value);
        }

        public DataSet GetFunctionAccessSettings(ReturnType action)
        {
            try
            {
                return _objApprovalDal.GetFunctionAccessSettings(action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
