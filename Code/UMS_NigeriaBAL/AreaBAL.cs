﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Xml;
using System.Data;

namespace UMS_NigeriaBAL
{
    public class AreaBAL
    {
        #region Members
        AreaBe ObjAreaBE = new AreaBe();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        AreaDAL  ObjAreaDAL = new AreaDAL();
        #endregion

        #region Methods
        public XmlDocument IsAreaExists(AreaBe objAreaBe)
        {
            try
            {
                return ObjAreaDAL.IsAreaExists(_ObjiDsignBAL.DoSerialize<AreaBe>(objAreaBe));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public XmlDocument Area(AreaBe objAreaBe,Statement Value)
        {
            try
            {
                return ObjAreaDAL.Area(_ObjiDsignBAL.DoSerialize<AreaBe>(objAreaBe),Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public XmlDocument Area(AreaBe objAreaBe,ReturnType Value)
        {
            try
            {
                return ObjAreaDAL.Area(_ObjiDsignBAL.DoSerialize<AreaBe>(objAreaBe),Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
