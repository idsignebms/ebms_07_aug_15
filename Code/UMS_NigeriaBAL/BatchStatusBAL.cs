﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;
using iDSignHelper;
using UMS_NigeriaBE;
using UMS_NigriaDAL;

namespace UMS_NigeriaBAL
{
    public class BatchStatusBAL
    {
        #region Members
        BatchStatusDAL _ObjBatchStatusDAL = new BatchStatusDAL();
        iDsignBAL _objIDsignBal=new iDsignBAL();
        #endregion

        #region Method
        public DataSet GetBatchPaymentStatus(BatchStatusBE objBatchStatusBE)
        {
            try
            {
                return _ObjBatchStatusDAL.GetBatchPaymentStatus(_objIDsignBal.DoSerialize<BatchStatusBE>(objBatchStatusBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetReopenBatchPayments(BatchStatusBE _objBatchStatusBE)
        {
            try
            {
                return _ObjBatchStatusDAL.GetReopenBatchPayments(_objIDsignBal.DoSerialize<BatchStatusBE>(_objBatchStatusBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetReopenBatchAdjustments(BatchStatusBE _objBatchStatusBE)
        {
            try
            {
                return _ObjBatchStatusDAL.GetReopenBatchAdjustments(_objIDsignBal.DoSerialize<BatchStatusBE>(_objBatchStatusBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument CloseBatchPayment(BatchStatusBE _objBatchStatusBe)
        {
            return _ObjBatchStatusDAL.CloseBatchPayment(_objIDsignBal.DoSerialize<BatchStatusBE>(_objBatchStatusBe));
        }

        public XmlDocument ReopenBatchPayment(BatchStatusBE _objBatchStatusBe)
        {
            return _ObjBatchStatusDAL.ReopenBatchPayment(_objIDsignBal.DoSerialize<BatchStatusBE>(_objBatchStatusBe));
        }

        public XmlDocument ReopenBatchAdjustments(BatchStatusBE _objBatchStatusBe)
        {
            return _ObjBatchStatusDAL.ReopenBatchAdjustments(_objIDsignBal.DoSerialize<BatchStatusBE>(_objBatchStatusBe));
        }

        public XmlDocument DeleteBatchPayment(BatchStatusBE _objBatchStatusBe)
        {
            return _ObjBatchStatusDAL.DeleteBatchPayment(_objIDsignBal.DoSerialize<BatchStatusBE>(_objBatchStatusBe));
        }

        public DataSet GetBatchAdjustmentStatus(BatchStatusBE _objBatchStatusBe) 
        {
            try
            {
                return _ObjBatchStatusDAL.GetBatchAdjustmentStatus(_objIDsignBal.DoSerialize<BatchStatusBE>(_objBatchStatusBe));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument CloseBatchAdjustment(BatchStatusBE _objBatchStatusBe)
        {
            return _ObjBatchStatusDAL.CloseBatchAdjustment(_objIDsignBal.DoSerialize<BatchStatusBE>(_objBatchStatusBe));
        }

        public XmlDocument DeleteBatchAdjustment(BatchStatusBE _objBatchStatusBe)
        {
            return _ObjBatchStatusDAL.DeleteBatchAdjustment(_objIDsignBal.DoSerialize<BatchStatusBE>(_objBatchStatusBe));
        }

        #endregion
    }
}
