﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using UMS_NigriaDAL;
using System.Xml;
using UMS_NigeriaBE;
using System.Data;

namespace UMS_NigeriaBAL
{
    public class BillAdjustmentBal
    {
        #region Members

        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        BillAdjustmentDal _objBillAdjustmentDal = new BillAdjustmentDal();

        #endregion

        public XmlDocument InsertAdjustmentDetails(BillAdjustmentsBe _objBillAdjustmentsBe, DataTable dt, Statement action)
        {
            try
            {
                return _objBillAdjustmentDal.InsertAdjustmentDetails(_ObjiDsignBAL.DoSerialize<BillAdjustmentsBe>(_objBillAdjustmentsBe), _ObjiDsignBAL.DoSerialize(dt), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument InsertAdjustedReadDetails(BillAdjustmentsBe _objBillAdjustmentsBe, Statement action)
        {
            try
            {
                return _objBillAdjustmentDal.InsertAdjustedReadDetails(_ObjiDsignBAL.DoSerialize<BillAdjustmentsBe>(_objBillAdjustmentsBe), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public XmlDocument GetAdjustmentDetails(BillAdjustmentsBe _objBillAdjustmentsBe, ReturnType value)
        {
            try
            {
                return _objBillAdjustmentDal.GetAdjustmentDetails(_ObjiDsignBAL.DoSerialize<BillAdjustmentsBe>(_objBillAdjustmentsBe), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument MeterConsumptionAdjustmentBAL(XmlDocument XmlDoc)
        {
            return _objBillAdjustmentDal.MeterConsumptionAdjustmentDAL(XmlDoc);
        }

        public XmlDocument CancelAdjustmentBAL(XmlDocument XmlDoc)
        {
            return _objBillAdjustmentDal.CancelAdjustmentDAL(XmlDoc);
        }

        public XmlDocument InsertAdjustmentBatchBAL(BillAdjustmentsBe _objBillAdjustmentsBe)
        {
            return _objBillAdjustmentDal.InsertAdjustmentBatchDAL(_ObjiDsignBAL.DoSerialize<BillAdjustmentsBe>(_objBillAdjustmentsBe));
        }

        public XmlDocument GetAdjustmentBatchByNoBAL(BillAdjustmentsBe _objBillAdjustmentsBe)
        {
            return _objBillAdjustmentDal.GetAdjustmentBatchByNoDAL(_ObjiDsignBAL.DoSerialize<BillAdjustmentsBe>(_objBillAdjustmentsBe));
        }

        public XmlDocument GetReasonCodeListBAL()
        {
            return _objBillAdjustmentDal.GetReasonCodeListDAL();
        }

        public XmlDocument UpdateCustomerOutstandingBAL(BillAdjustmentsBe _objBillAdjustmentsBe)
        {
            return _objBillAdjustmentDal.UpdateCustomerOutstandingDAL(_ObjiDsignBAL.DoSerialize<BillAdjustmentsBe>(_objBillAdjustmentsBe));
        }

        public XmlDocument BillAdjustmentBAL(BillAdjustmentsBe _objBillAdjustmentsBe)
        {
            return _objBillAdjustmentDal.BillAdjustmentDAL(_ObjiDsignBAL.DoSerialize<BillAdjustmentsBe>(_objBillAdjustmentsBe));
        }

        public XmlDocument GetAdjustmentBatchDetailsBAL(BillAdjustmentsBe _objBillAdjustmentsBe)
        {
            return _objBillAdjustmentDal.GetAdjustmentBatchDetailsDAL(_ObjiDsignBAL.DoSerialize<BillAdjustmentsBe>(_objBillAdjustmentsBe));
        }

        public XmlDocument DeleteBatchAdjustmentsBAL(BillAdjustmentsBe _objBillAdjustmentsBe)
        {
            return _objBillAdjustmentDal.DeleteBatchAdjustmentsDAL(_ObjiDsignBAL.DoSerialize<BillAdjustmentsBe>(_objBillAdjustmentsBe));
        }

        public XmlDocument AdjustmentForNoBill(BillAdjustmentsBe _objBillAdjustmentsBe, AdjustmentNoBill action)
        {
            return _objBillAdjustmentDal.AdjustmentForNoBill(_ObjiDsignBAL.DoSerialize<BillAdjustmentsBe>(_objBillAdjustmentsBe), action);
        }

        public XmlDocument GetAdjustmentLastTransactions(BillAdjustmentsBe _objBillAdjustmentsBe)
        {
            return _objBillAdjustmentDal.GetAdjustmentLastTransactions(_ObjiDsignBAL.DoSerialize<BillAdjustmentsBe>(_objBillAdjustmentsBe));
        }
    }
}
