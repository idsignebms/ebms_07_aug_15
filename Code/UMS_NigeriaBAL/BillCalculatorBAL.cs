﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UMS_NigriaDAL;
using UMS_NigeriaBE;
using iDSignHelper;
namespace UMS_NigeriaBAL
{
  public class BillCalculatorBAL
  {
      #region Members
      BillCalculatorBE _ObjBillCalculatorBE = new BillCalculatorBE();
      iDsignBAL _ObjiDsignBAL = new iDsignBAL();
      BillCalculatorDAL _ObjBillCalculatorDAL = new BillCalculatorDAL();
      #endregion
      public XmlDocument GetFixedAndEneryCharges(BillCalculatorBE _ObjBillCalculatorBE,ReturnType value)
      {
          try
          {
              return _ObjBillCalculatorDAL.GetFixedAndEneryCharges(_ObjiDsignBAL.DoSerialize<BillCalculatorBE>(_ObjBillCalculatorBE), value);
          }
          catch (Exception ex)
          {
              throw ex;
          }
      }
      public XmlDocument GetCustomerType(ReturnType value)
      {
          try
          {
              return _ObjBillCalculatorDAL.GetCustomerType(value);
          }
          catch (Exception ex)
          {
              throw ex;
          }
      }
      public XmlDocument GetCustomerSubTypes(BillCalculatorBE _ObjBillCalculatorBE, ReturnType value)
      {
          try
          {
              return _ObjBillCalculatorDAL.GetCustomerSubTypes(_ObjiDsignBAL.DoSerialize<BillCalculatorBE>(_ObjBillCalculatorBE), value);
          }
          catch (Exception ex)
          {
              throw ex;
          }
      }
      public XmlDocument CalculateBillWithConsumption(BillCalculatorBE _ObjBillCalculatorBE, ReturnType value)
      {
          try
          {
              return _ObjBillCalculatorDAL.CalculateBillWithConsumption(_ObjiDsignBAL.DoSerialize<BillCalculatorBE>(_ObjBillCalculatorBE), value);
          }
          catch (Exception ex)
          {
              throw ex;
          }
      }
    }
}
