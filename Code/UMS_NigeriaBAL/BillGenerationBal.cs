﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using UMS_NigeriaBE;
using System.Data;

namespace UMS_NigeriaBAL
{
    public class BillGenerationBal
    {
        BillGenerationDal _objBillGenerationDal = new BillGenerationDal();
        iDsignBAL _objiDsignBal = new iDsignBAL();

        public XmlDocument GetDetails(ReturnType value)
        {
            try
            {
                return _objBillGenerationDal.GetDetails(value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetDetails(BillGenerationBe billGenerationBe, ReturnType value)
        {
            try
            {
                return _objBillGenerationDal.GetDetails(_objiDsignBal.DoSerialize<BillGenerationBe>(billGenerationBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetBillCycleDetails(BillGenerationBe billGenerationBe, ReturnType value)
        {
            try
            {
                return _objBillGenerationDal.GetBillCycleDetails(_objiDsignBal.DoSerialize<BillGenerationBe>(billGenerationBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }


        public XmlDocument InsertDetails(BillGenerationBe billGenerationBe, Statement action)
        {
            try
            {
                return _objBillGenerationDal.InsertDetails(_objiDsignBal.DoSerialize<BillGenerationBe>(billGenerationBe), action);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument UpdateBillDetails(BillGenerationBe billGenerationBe, Statement action)
        {
            try
            {
                return _objBillGenerationDal.UpdateBillDetails(_objiDsignBal.DoSerialize<BillGenerationBe>(billGenerationBe), action);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetCustomerBills(BillGenerationBe billGenerationBe)
        {
            try
            {
                return _objBillGenerationDal.GetCustomerBills(_objiDsignBal.DoSerialize<BillGenerationBe>(billGenerationBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetCustomerReadingDetails(BillGenerationBe billGenerationBe, ReturnType value)
        {
            try
            {
                return _objBillGenerationDal.GetCustomerReadingDetails(_objiDsignBal.DoSerialize<BillGenerationBe>(billGenerationBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetMeterReadingAdjstDetailsBAL(BillGenerationBe billGenerationBe)
        { // Try and Cath isn't require here
            return _objBillGenerationDal.GetMeterReadingAdjstDetailsDAL(_objiDsignBal.DoSerialize<BillGenerationBe>(billGenerationBe));
        }
        public XmlDocument UpdatedMeterReadingAdjustmentsBAL(BillGenerationBe billGenerationBe)
        { // Try and Cath isn't require here
            return _objBillGenerationDal.UpdatedMeterReadingAdjustmentsDAL(_objiDsignBal.DoSerialize<BillGenerationBe>(billGenerationBe));
        }

        public XmlDocument CheckBillValidations(BillGenerationBe billGenerationBe, ReturnType value)
        {
            try
            {
                return _objBillGenerationDal.CheckBillValidations(_objiDsignBal.DoSerialize<BillGenerationBe>(billGenerationBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GenerateCustomersBills(BillGenerationBe billGenerationBe)
        {
            try
            {
                return _objBillGenerationDal.GenerateCustomersBills(_objiDsignBal.DoSerialize<BillGenerationBe>(billGenerationBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GenerateCustomersBills_GetDetails(BillGenerationBe billGenerationBe)
        {
            try
            {
                return _objBillGenerationDal.GenerateCustomersBills_GetDetails(_objiDsignBal.DoSerialize<BillGenerationBe>(billGenerationBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public DataSet GenerateCustomersBills_GetDetails_Satya(BillGenerationBe billGenerationBe)
        {
            try
            {
                return _objBillGenerationDal.GenerateCustomersBills_GetDetails_Satya(_objiDsignBal.DoSerialize<BillGenerationBe>(billGenerationBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument UpdateBillGenFile(BillGenerationBe billGenerationBe)
        {
            try
            {
                return _objBillGenerationDal.UpdateBillGenFile(_objiDsignBal.DoSerialize<BillGenerationBe>(billGenerationBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet DownloadBillingFile(BillGenerationBe billGenerationBe)
        {
            try
            {
                return _objBillGenerationDal.DownloadBillingFile(_objiDsignBal.DoSerialize<BillGenerationBe>(billGenerationBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet DownloadBillingFileForCustomer(BillGenerationBe billGenerationBe)
        {
            try
            {
                return _objBillGenerationDal.DownloadBillingFileForCustomer(_objiDsignBal.DoSerialize<BillGenerationBe>(billGenerationBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public DataSet GenerateBillForCustomer(BillGenerationBe billGenerationBe)
        {
            try
            {
                return _objBillGenerationDal.GenerateBillForCustomer(_objiDsignBal.DoSerialize<BillGenerationBe>(billGenerationBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument UpdateBillGenByCustFile(BillGenerationBe billGenerationBe)
        {
            try
            {
                return _objBillGenerationDal.UpdateBillGenByCustFile(_objiDsignBal.DoSerialize<BillGenerationBe>(billGenerationBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetCustomerBillDetailsForPDF(CustomerBillPDFBE customerBillPDFBE)
        {
            try
            {
                return _objBillGenerationDal.GetCustomerBillDetailsForPDF(_objiDsignBal.DoSerialize<CustomerBillPDFBE>(customerBillPDFBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument InsertCustomerBillDetailsForPDF(CustomerBillPDFDetailsBE _objCustomerBillPDFDetailsBE)
        {
            try
            {
                return _objBillGenerationDal.InsertCustomerBillDetailsForPDF(_objiDsignBal.DoSerialize<CustomerBillPDFDetailsBE>(_objCustomerBillPDFDetailsBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
    }
}
