﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Xml;
using System.Data;

namespace UMS_NigeriaBAL
{
    public class BillReadingsBAL
    {
        #region Members
        BillingBE _ObjBillBE = new BillingBE();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        BillReadingDAL _ObjBillDAL = new BillReadingDAL();
        #endregion
        #region Methods
        public XmlDocument GetPreviousCustomerReadings(XmlDocument XmlDoc)
        {
            try
            {
                return _ObjBillDAL.GetPreviousCustomerReadings(XmlDoc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetPreviousCustomerReadingsBookwise(XmlDocument XmlDoc)
        {
            try
            {
                return _ObjBillDAL.GetPreviousCustomerReadingsBookwise(XmlDoc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public XmlDocument InsertCustomerBillReadingBAL(XmlDocument xmlCustomerDetails)
        {
            try
            {
                return _ObjBillDAL.InsertCustomerBillReading(xmlCustomerDetails, Statement.Insert);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public XmlDocument InsertDirectCustomerBillReadingBAL(XmlDocument xmlCustomerDetails)
        {
            try
            {
                return _ObjBillDAL.InsertCustomerBillReading(xmlCustomerDetails, Statement.Edit);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument InsertMeterReadingsLogs(XmlDocument xmlCustomerDetails)
        {
            try
            {
                return _ObjBillDAL.InsertCustomerBillReading(xmlCustomerDetails, Statement.Generate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument UpdateCustomerBillReadingBAL(XmlDocument xmlCustomerDetails)
        {
            try
            {
                return _ObjBillDAL.InsertCustomerBillReading(xmlCustomerDetails, Statement.Update);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public XmlDocument GetMeterDecimalValues(XmlDocument xmlCustomerDetails)
        {
            try
            {
                return _ObjBillDAL.InsertCustomerBillReading(xmlCustomerDetails, Statement.Change);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument Get(BillingBE _ObjBillingBE, ReturnType value)
        {
            try
            {
                return _ObjBillDAL.Get(_ObjiDsignBAL.DoSerialize<BillingBE>(_ObjBillingBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public XmlDocument UpdateBatchTotalBAL(XmlDocument xmlBatchDetails)
        {
            try
            {
                return _ObjBillDAL.UpdateBatchTotalDAL(xmlBatchDetails, Statement.Update);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument Batch(XmlDocument xmlBatchDetails)
        {
            try
            {
                return _ObjBillDAL.Batch(xmlBatchDetails, Statement.Check);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument PresentReadingAdjustment(BillingBE _ObjBillingBE, Statement action)
        {
            try
            {
                return _ObjBillDAL.PresentReadingAdjustment(_ObjiDsignBAL.DoSerialize<BillingBE>(_ObjBillingBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Methods For BatchEntry

        public XmlDocument GetCashierDetailsBAL()
        {
            try
            {
                return _ObjBillDAL.GetCashierDAL();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetCashierDetailsBAL(BillingBE _objBilling)
        {
            try
            {
                return _ObjBillDAL.GetCashierDAL(_ObjiDsignBAL.DoSerialize<BillingBE>(_objBilling));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument InsertBatchEntry(BillingBE _objBilling, Statement value)
        {
            try
            {
                return _ObjBillDAL.BatchEntry(_ObjiDsignBAL.DoSerialize<BillingBE>(_objBilling), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public XmlDocument GetCashierOfficesDAL()
        {
            try
            {
                return _ObjBillDAL.GetCashierOfficesDAL();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument InsertBulkReadings(BillingBE _objBilling, DataTable dt, Statement action)
        {
            try
            {
                return _ObjBillDAL.InsertBulkReadings(_ObjiDsignBAL.DoSerialize<BillingBE>(_objBilling), _ObjiDsignBAL.DoSerialize(dt), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InsertMeterReadingsDT(string CreatedBy, int MeterReadingFrom, DataTable dtReadings)//Karteek
        {
            try
            {
                return _ObjBillDAL.InsertMeterReadingsDT(CreatedBy, MeterReadingFrom, dtReadings);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public bool InsertBookwiseMeterReadingsDT(string CreatedBy, int MeterReadingFrom, DataTable dtReadings, bool isFinalApproval, int FunctionId, int ActiveStatusId, string BU_ID)//Karteek
        {
            try
            {
                return _ObjBillDAL.InsertBookwiseMeterReadingsDT(CreatedBy, MeterReadingFrom, dtReadings, isFinalApproval, FunctionId, ActiveStatusId, BU_ID);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public int InsertAverageUploadDT(DataTable dtReadings)//Karteek
        {
            try
            {
                return _ObjBillDAL.InsertAverageUploadDT(dtReadings);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public bool InsertBatchPaymentUploadDT(int BatchNo, string BatchName, string BatchDate, string CreatedBy, string Description, string BU_ID, int TotalCustomers, string FilePath, DataTable dtPayments, out bool isExists)//Karteek
        {
            try
            {
                return _ObjBillDAL.InsertBatchPaymentUploadDT(BatchNo, BatchName, BatchDate, CreatedBy, Description, BU_ID, TotalCustomers, FilePath, dtPayments, out isExists);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public bool InsertBatchMeterReadingUploadDT(int BatchNo, string BatchName, string BatchDate, string CreatedBy, string Description, int TotalCustomers, string FilePath, DataTable dtMeterReadings, string BU_ID, out bool isExists)//Bhimaraju
        {
            try
            {
                return _ObjBillDAL.InsertBatchMeterReadingUploadDT(BatchNo, BatchName, BatchDate, CreatedBy, Description, TotalCustomers, FilePath, dtMeterReadings,BU_ID, out isExists);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public bool InsertBatchAverageConsumptionUploadDT(int BatchNo, string BatchName, string BatchDate, string CreatedBy, string Description, int TotalCustomers, string FilePath, DataTable dtMeterReadings, string BU_ID, out bool isExists)//Bhimaraju
        {
            try
            {
                return _ObjBillDAL.InsertBatchAverageConsumptionUploadDT(BatchNo, BatchName, BatchDate, CreatedBy, Description, TotalCustomers, FilePath, dtMeterReadings, BU_ID, out isExists);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public bool InsertPaymentUploadTransactionDT(int BatchNo, int UploadBatchId, string CreatedBy, int TotalCustomers, string FilePath, DataTable dtPayments)//Karteek
        {
            try
            {
                return _ObjBillDAL.InsertPaymentUploadTransactionDT(BatchNo, UploadBatchId, CreatedBy, TotalCustomers, FilePath, dtPayments);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public bool InsertReadingUploadTransactionDT(int BatchNo, int UploadBatchId, string CreatedBy, int TotalCustomers, string FilePath, DataTable dtPayments)//Faiz-ID103
        {
            try
            {
                return _ObjBillDAL.InsertReadingUploadTransactionDT(BatchNo, UploadBatchId, CreatedBy, TotalCustomers, FilePath, dtPayments);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public bool InsertAverageUploadTransactionDT(int BatchNo, int UploadBatchId, string CreatedBy, int TotalCustomers, string FilePath, DataTable dtPayments)//Faiz-ID103
        {
            try
            {
                return _ObjBillDAL.InsertAverageUploadTransactionDT(BatchNo, UploadBatchId, CreatedBy, TotalCustomers, FilePath, dtPayments);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }


        #endregion
        #region DcumentPayments
        public XmlDocument Insert(System.Data.DataTable dt, Statement action)
        {
            try
            {
                return _ObjBillDAL.Insert(_ObjiDsignBAL.DoSerialize(dt), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public XmlDocument GetFulInfoXL(System.Data.DataTable dt)
        {
            try
            {
                return _ObjBillDAL.GetFullInfoXL(_ObjiDsignBAL.DoSerialize(dt));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public XmlDocument GetDirectReadingsInfoXL(System.Data.DataTable dt)
        {
            try
            {
                return _ObjBillDAL.GetDirectReadingsInfoXL(_ObjiDsignBAL.DoSerialize(dt));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetValidateReadingsUpload(DataTable dtReadings)
        {
            try
            {
                return _ObjBillDAL.GetValidateReadingsUpload(dtReadings);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetValidateAvgConsumptionUpload(DataTable dtReadings)
        {
            try
            {
                return _ObjBillDAL.GetValidateAvgConsumptionUpload(dtReadings);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #endregion
    }
}
