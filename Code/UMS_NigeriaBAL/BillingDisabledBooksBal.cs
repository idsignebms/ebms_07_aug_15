﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using UMS_NigeriaBE;
using System.Data;
namespace UMS_NigeriaBAL
{
    public class BillingDisabledBooksBal
    {
        #region Members

        BillingDisabledBooksDal objBillingDisabledBooksDal = new BillingDisabledBooksDal();
        iDsignBAL objiDsignBal = new iDsignBAL();
        BillingDisabledBooksBe objBillingDisabledBooksBe = new BillingDisabledBooksBe();
        #endregion

        public XmlDocument Insert(BillingDisabledBooksBe objBillingDisabledBooksBe, Statement value)
        {
            try
            {
                return objBillingDisabledBooksDal.Insert(objiDsignBal.DoSerialize<BillingDisabledBooksBe>(objBillingDisabledBooksBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetDisabledBooks(BillingDisabledBooksBe objBillingDisabledBooksBe)
        {
            try
            {
                return objBillingDisabledBooksDal.GetDisabledBooks(objiDsignBal.DoSerialize<BillingDisabledBooksBe>(objBillingDisabledBooksBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
    }
}
