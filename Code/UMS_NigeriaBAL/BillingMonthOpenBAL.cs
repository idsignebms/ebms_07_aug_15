﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Xml;
using System.Data;
namespace UMS_NigeriaBAL
{
    public class BillingMonthOpenBAL
    {
        #region Members
        iDsignBAL _objiDsignBAL = new iDsignBAL();
        BillingMonthOpenDAL _objBillingMonthOpenDAL = new BillingMonthOpenDAL();
        #endregion

        #region Methods For Billing Month Open Status

        public XmlDocument InsertBillingMonth(BillingMonthOpenBE _ObjBillingMonthOpenBE, Statement action)
        {
            try
            {
                return _objBillingMonthOpenDAL.InsertBillingMonth(_objiDsignBAL.DoSerialize<BillingMonthOpenBE>(_ObjBillingMonthOpenBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetBillingMonth(BillingMonthOpenBE _ObjBillingMonthOpenBE, ReturnType value)
        {
            try
            {
                return _objBillingMonthOpenDAL.GetBillingMonth(_objiDsignBAL.DoSerialize<BillingMonthOpenBE>(_ObjBillingMonthOpenBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
