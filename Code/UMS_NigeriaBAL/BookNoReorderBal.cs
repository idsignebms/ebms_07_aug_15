﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UMS_NigriaDAL;
using iDSignHelper;
using UMS_NigeriaBE;
using System.Xml;

namespace UMS_NigeriaBAL
{
    public class BookNoReorderBal
    {
        #region Members
        BookNoReorderDal _objBookNoReorderDal = new BookNoReorderDal();
        iDsignBAL _objiDsignBal = new iDsignBAL();
        BookNoReorderBe _objBookNoReorderBe = new BookNoReorderBe();
        #endregion

        #region Methods
        public XmlDocument Get(BookNoReorderBe _objBookNoReorderBe, ReturnType value)
        {
            try
            {
                return _objBookNoReorderDal.Get(_objiDsignBal.DoSerialize<BookNoReorderBe>(_objBookNoReorderBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument Insert(BookNoReorderBe _objBookNoReorderBe, Statement action)
        {
            try
            {
                return _objBookNoReorderDal.Insert(_objiDsignBal.DoSerialize<BookNoReorderBe>(_objBookNoReorderBe), action);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion
    }
}
