﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using UMS_NigriaDAL;
using System.Xml;
using UMS_NigeriaBE;

namespace UMS_NigeriaBAL
{
    public class CashierBAL
    {
        #region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CashierDAL _objCashierDAL = new CashierDAL();
        #endregion

        #region Methods
        public XmlDocument InsertCashier(CashierBE _objCashierBE, Statement action)
        {
            try
            {
                return _objCashierDAL.InsertCashier(_ObjiDsignBAL.DoSerialize<CashierBE>(_objCashierBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetCashier(CashierBE _objCashierBE, ReturnType value)
        {
            try
            {
                return _objCashierDAL.GetCashier(_ObjiDsignBAL.DoSerialize<CashierBE>(_objCashierBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
