﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using UMS_NigriaDAL;
using UMS_NigeriaBE;
using System.Xml;
using System.Data;

namespace UMS_NigeriaBAL
{
    public class ChangeBookNoBal
    {
        #region Members

        iDsignBAL _objiDsignBAL = new iDsignBAL();
        ChangeBookNoDal _objChangeBookNoDal = new ChangeBookNoDal();
        #endregion

        #region Methods

        public XmlDocument Get(ChangeBookNoBe _objChangeBookNoBe, ReturnType value)
        {
            try
            {
                return _objChangeBookNoDal.Get(_objiDsignBAL.DoSerialize<ChangeBookNoBe>(_objChangeBookNoBe), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public XmlDocument GetCustomerDetailsForNameChangeByAccno_ByCheck(RptCustomerLedgerBe objRptCustomerLedgerBe)
        {
            try
            {
                return _objChangeBookNoDal.GetCustomerDetailsForNameChangeByAccno_ByCheck(_objiDsignBAL.DoSerialize<RptCustomerLedgerBe>(objRptCustomerLedgerBe));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public XmlDocument Insert(ChangeBookNoBe _objChangeBookNoBe, Statement ation)
        {
            try
            {
                return _objChangeBookNoDal.Insert(_objiDsignBAL.DoSerialize<ChangeBookNoBe>(_objChangeBookNoBe), ation);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public XmlDocument InsertApprovalDetails(ChangeBookNoBe _objChangeBookNoBe, Statement ation)
        {
            try
            {
                return _objChangeBookNoDal.InsertApprovalDetails(_objiDsignBAL.DoSerialize<ChangeBookNoBe>(_objChangeBookNoBe), ation);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument InsertDetails(ChangeContactBE _objChangeContactBE, Statement ation)
        {
            try
            {
                return _objChangeBookNoDal.InsertDetails(_objiDsignBAL.DoSerialize<ChangeContactBE>(_objChangeContactBE), ation);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetDetails(ChangeContactBE _objChangeContactBE, ReturnType value)
        {
            try
            {
                return _objChangeBookNoDal.GetDetails(_objiDsignBAL.DoSerialize<ChangeContactBE>(_objChangeContactBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetNameChangesApproveList(ChangeBookNoBe objChangeBookNoBe)
        {
            try
            {
                return _objChangeBookNoDal.GetNameChangesApproveList(_objiDsignBAL.DoSerialize<ChangeBookNoBe>(objChangeBookNoBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetStatusChangesApproveList(ChangeBookNoBe objChangeBookNoBe)
        {
            try
            {
                return _objChangeBookNoDal.GetStatusChangesApproveList(_objiDsignBAL.DoSerialize<ChangeBookNoBe>(objChangeBookNoBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetMeterNoChangesApproveList(ChangeBookNoBe objChangeBookNoBe)
        {
            try
            {
                return _objChangeBookNoDal.GetMeterNoChangesApproveList(_objiDsignBAL.DoSerialize<ChangeBookNoBe>(objChangeBookNoBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetDirectCustomerAverageUploadBE(DirectCustomerAverageUploadBE objDirectCustomerAverageUploadBE)
        {
            try
            {
                return _objChangeBookNoDal.GetDirectCustomerAverageUploadBE(_objiDsignBAL.DoSerialize<DirectCustomerAverageUploadBE>(objDirectCustomerAverageUploadBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument UpdateDirectCustomerAverageUpload(DirectCustomerAverageUploadBE objDirectCustomerAverageUploadBE)
        {
            try
            {
                return _objChangeBookNoDal.UpdateDirectCustomerAverageUpload(_objiDsignBAL.DoSerialize<DirectCustomerAverageUploadBE>(objDirectCustomerAverageUploadBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetCustDetForContactInfoByAccno_ByCheck(RptCustomerLedgerBe objRptCustomerLedgerBe)
        {
            try
            {
                return _objChangeBookNoDal.GetCustDetForContactInfoByAccno_ByCheck(_objiDsignBAL.DoSerialize<RptCustomerLedgerBe>(objRptCustomerLedgerBe));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
