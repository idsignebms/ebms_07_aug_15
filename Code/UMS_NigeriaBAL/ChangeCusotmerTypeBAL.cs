﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using UMS_NigriaDAL;
using System.Xml;
using UMS_NigeriaBE;
using System.Data;

namespace UMS_NigeriaBAL
{


    public class ChangeCusotmerTypeBAL
    {
        #region Members

        iDsignBAL _objiDsignBAL = new iDsignBAL();
        ChangeCusotmerTypeDAL _objChangeCusotmerTypeDAL = new ChangeCusotmerTypeDAL();
        #endregion

        #region Methods
        public XmlDocument Get(ChangeCustomerTypeBE _objChangeCusotmerTypeBE, ReturnType value)
        {
            try
            {
                return _objChangeCusotmerTypeDAL.Get(_objiDsignBAL.DoSerialize<ChangeCustomerTypeBE>(_objChangeCusotmerTypeBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument CustomerType(ChangeCustomerTypeBE _objChangeCustomerTypeBE, Statement statement)
        {
            try
            {
                return _objChangeCusotmerTypeDAL.CustomerType(_objiDsignBAL.DoSerialize<ChangeCustomerTypeBE>(_objChangeCustomerTypeBE), statement);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument Insert(ChangeCustomerTypeBE _objChangeCustomerTypeBE, Statement ation)
        {
            try
            {
                return _objChangeCusotmerTypeDAL.Insert(_objiDsignBAL.DoSerialize<ChangeCustomerTypeBE>(_objChangeCustomerTypeBE), ation);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetTypeChangesApproveList(ChangeCustomerTypeBE _objChangeCustomerTypeBE)
        {
            try
            {
                return _objChangeCusotmerTypeDAL.GetTypeChangesApproveList(_objiDsignBAL.DoSerialize<ChangeCustomerTypeBE>(_objChangeCustomerTypeBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetBindApproveList(ChangeCustomerTypeBE _objChangeCustomerTypeBE, ReturnType value)
        {
            try
            {
                return _objChangeCusotmerTypeDAL.GetBindApproveList(_objiDsignBAL.DoSerialize<ChangeCustomerTypeBE>(_objChangeCustomerTypeBE), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument InsertApproveList(ChangeCustomerTypeBE _objChangeCustomerTypeBE, Statement action)
        {
            try
            {
                return _objChangeCusotmerTypeDAL.InsertApproveList(_objiDsignBAL.DoSerialize<ChangeCustomerTypeBE>(_objChangeCustomerTypeBE), action);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument GetCustDetForTypeChange_ByCheck(RptCustomerLedgerBe objRptCustomerLedgerBe)
        {
            try
            {
                return _objChangeCusotmerTypeDAL.GetCustDetForTypeChange_ByCheck(_objiDsignBAL.DoSerialize<RptCustomerLedgerBe>(objRptCustomerLedgerBe));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
