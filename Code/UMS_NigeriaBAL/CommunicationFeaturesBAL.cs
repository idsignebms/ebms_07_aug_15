﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using UMS_NigeriaBE;
using System.Data;

namespace UMS_NigeriaBAL
{
    public class CommunicationFeaturesBAL
    {
        #region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CommunicationFeaturesDAL _objCommunicationFeaturesDAL = new CommunicationFeaturesDAL();
        #endregion

        #region Methods
        public XmlDocument IsEmailFeatureEnabled(CommunicationFeaturesBE _objCommunicationFeaturesBE)
        {
            try
            {
                return _objCommunicationFeaturesDAL.IsEmailFeatureEnabled(_ObjiDsignBAL.DoSerialize<CommunicationFeaturesBE>(_objCommunicationFeaturesBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument IsSMSFeatureEnabled(CommunicationFeaturesBE _objCommunicationFeaturesBE)
        {
            try
            {
                return _objCommunicationFeaturesDAL.IsSMSFeatureEnabled(_ObjiDsignBAL.DoSerialize<CommunicationFeaturesBE>(_objCommunicationFeaturesBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetDetails(CommunicationFeaturesBE _objCommunicationFeaturesBE, ReturnType value)
        {
            try
            {
                return _objCommunicationFeaturesDAL.GetDetails(_ObjiDsignBAL.DoSerialize<CommunicationFeaturesBE>(_objCommunicationFeaturesBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetCommunicatinFeatures()
        {
            return _objCommunicationFeaturesDAL.GetCommunicatinFeatures();
        }

        public XmlDocument UpdateCommunicationFeatures(CommunicationFeaturesBE objCommunicationFeaturesBE)
        {
            return _objCommunicationFeaturesDAL.UpdateCommunicationFeatures(_ObjiDsignBAL.DoSerialize<CommunicationFeaturesBE>(objCommunicationFeaturesBE));
        }
        #endregion
    }
}
