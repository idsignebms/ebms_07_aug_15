﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using UMS_NigeriaBE;
using System.Data;

namespace UMS_NigeriaBAL
{
    public class ConsumerBal
    {
        #region Members

        ConsumerDal objConsumerDal = new ConsumerDal();
        iDsignBAL objiDsignBal = new iDsignBAL();
        ConsumerBe _objconsumerbe = new ConsumerBe();
        #endregion

        #region Methods
        //Getting Login Details
        public XmlDocument Insert(ConsumerBe objConsumerBe, Statement value)
        {
            try
            {
                return objConsumerDal.Insert(objiDsignBal.DoSerialize<ConsumerBe>(objConsumerBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument InsertDetails(ConsumerBe objConsumerBe, Statement value)
        {
            try
            {
                return objConsumerDal.InsertDetails(objiDsignBal.DoSerialize<ConsumerBe>(objConsumerBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument Get(ConsumerBe objConsumerBe, ReturnType value)
        {
            try
            {
                return objConsumerDal.Get(objiDsignBal.DoSerialize<ConsumerBe>(objConsumerBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetDetails(ConsumerBe objConsumerBe, ReturnType value)
        {
            try
            {
                return objConsumerDal.GetDetails(objiDsignBal.DoSerialize<ConsumerBe>(objConsumerBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetSearchCustomers(XmlDocument XmlDoc)
        {
            try
            {
                return objConsumerDal.GetSearchCustomers(XmlDoc);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetSearchCustomers(string searchString, int startIndex, int endIndex)
        {
            try
            {
                return objConsumerDal.GetSearchCustomers(searchString, startIndex, endIndex);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetServiceDetails(ConsumerBe objConsumerBe, ReturnType value)
        {
            try
            {
                return objConsumerDal.GetServiceDetails(objiDsignBal.DoSerialize<ConsumerBe>(objConsumerBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument Get(ReturnType value)
        {
            try
            {
                return objConsumerDal.Get(value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument Retrieve(ReturnType value)
        {
            try
            {
                return objConsumerDal.Retrieve(value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument Retrieve(ConsumerBe objConsumerBe, ReturnType value)
        {
            try
            {
                return objConsumerDal.Retrieve(objiDsignBal.DoSerialize<ConsumerBe>(objConsumerBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetCustomerRouteManagementBAL(ConsumerBe objConsumerBe)
        {
            try
            {
                return objConsumerDal.GetCustomerRouteManagementDAL(objiDsignBal.DoSerialize<ConsumerBe>(objConsumerBe), ReturnType.Get);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument UpdateRouteSequenceBAL(string RouteId, string Seq)
        {
            _objconsumerbe.Route = RouteId;
            _objconsumerbe.RouteSeq = Seq;
            return objConsumerDal.GetCustomerRouteManagementDAL(objiDsignBal.DoSerialize<ConsumerBe>(_objconsumerbe), ReturnType.Single);
        }

        public XmlDocument GetCustomerPaymentHistoryBAL(ConsumerBe objConsumerBe)
        {
            try
            {
                return objConsumerDal.GetCustomerPaymentHistoryDAL(objiDsignBal.DoSerialize<ConsumerBe>(objConsumerBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetCustomerBillDeails(ConsumerBe objConsumerBe, ReturnType value)
        {
            try
            {
                return objConsumerDal.GetCustomerBillDeails(objiDsignBal.DoSerialize<ConsumerBe>(objConsumerBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetBookNoListBAL()
        {
            try
            {
                return objConsumerDal.GetBookNoListDAL();
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetCustomersBy_CB_BAL(XmlDocument XmlDoc)
        {
            try
            {
                return objConsumerDal.GetCustomersBy_CB_DAL(XmlDoc);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument UpdateCustomerLongitudeAndLatitudeBAL(XmlDocument XmlDoc)
        {
            try
            {
                return objConsumerDal.UpdateCustomerLongitudeAndLatitudeDAL(XmlDoc);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetCustomerDeails(ConsumerBe objConsumerBe, ReturnType value)
        {
            try
            {
                return objConsumerDal.GetCustomersDetails(objiDsignBal.DoSerialize<ConsumerBe>(objConsumerBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetMarketerByAccountNo(ConsumerBe objConsumerBe, ReturnType value)
        {
            try
            {
                return objConsumerDal.GetMarketerByAccountNo(objiDsignBal.DoSerialize<ConsumerBe>(objConsumerBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet BookNoSearchBAL(ConsumerBe objConsumerBe)
        {
            try
            {
                return objConsumerDal.BookNoSearchDAL(objiDsignBal.DoSerialize<ConsumerBe>(objConsumerBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #region Customer Import Methods
        public XmlDocument InsertBatchCustBulkUploadBAL(BillingBE objBillingBE)
        {
            try
            {
                return objConsumerDal.InsertBatchCustBulkUploadDAL(objiDsignBal.DoSerialize<BillingBE>(objBillingBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument IsExistsBatchCustBulkUploadBAL(BillingBE objBillingBE)
        {
            try
            {
                return objConsumerDal.IsExistsBatchCustBulkUploadDAL(objiDsignBal.DoSerialize<BillingBE>(objBillingBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument InsertBatchVsAccontNoBAL(DataTable dt)
        {
            try
            {
                return objConsumerDal.InsertBatchVsAccontNoDAL(objiDsignBal.DoSerialize(dt));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument CustomerBulkUploadBAL(DataSet ds)
        {
            try
            {
                return objConsumerDal.CustomerBulkUploadDAL(objiDsignBal.DoSerialize(ds));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public DataSet GetCustDetailBlkUpldBAL(CustomerBulkBe objCustomerBulkBe)
        {
            try
            {
                return objConsumerDal.GetCustDetailBlkUpldDAL(objiDsignBal.DoSerialize<CustomerBulkBe>(objCustomerBulkBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion

        #region Reports
        public DataSet GetAccountswithoutCycleBAL(ConsumerBe objConsumerBe)
        {
            return objConsumerDal.GetAccountswithoutCycleDAL(objiDsignBal.DoSerialize<ConsumerBe>(objConsumerBe));
        }
        public DataSet GetAccountswithoutMeterBAL(ConsumerBe objConsumerBe)
        {
            return objConsumerDal.GetAccountswithoutMeterDAL(objiDsignBal.DoSerialize<ConsumerBe>(objConsumerBe));
        }
        public DataSet GetTariffBUReportBAL(ConsumerBe objConsumerBe)
        {
            return objConsumerDal.GetTariffBUReportDAL(objiDsignBal.DoSerialize<ConsumerBe>(objConsumerBe));
        }
        public DataSet GetAccountsWithNoNameOrAddress(ConsumerBe objConsumerBe)
        {
            return objConsumerDal.GetAccountsWithNoNameOrAddress(objiDsignBal.DoSerialize<ConsumerBe>(objConsumerBe));
        }

        #endregion

        #region Customer Management With new table structure 

        public XmlDocument CustomerRegistrationBAL(CustomerRegistrationBE CustomerRegistrationBE)//Neeraj-ID077
        {
            try
            {
                return objConsumerDal.CustomerRegistrationDAL(objiDsignBal.DoSerialize<CustomerRegistrationBE>(CustomerRegistrationBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetCustomerAddress(CustomerRegistrationBE CustomerRegistrationBE)//Neeraj-ID077
        {
            try
            {
                return objConsumerDal.GetCustomerAddress(objiDsignBal.DoSerialize<CustomerRegistrationBE>(CustomerRegistrationBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument ChangeCustomersAddress(CustomerRegistrationBE CustomerRegistrationBE)//Neeraj-ID077
        {
            try
            {
                return objConsumerDal.ChangeCustomersAddress(objiDsignBal.DoSerialize<CustomerRegistrationBE>(CustomerRegistrationBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument ChangeCustomersBookNo(CustomerRegistrationBE CustomerRegistrationBE)//Neeraj-ID077
        {
            try
            {
                return objConsumerDal.ChangeCustomersBookNo(objiDsignBal.DoSerialize<CustomerRegistrationBE>(CustomerRegistrationBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument IsOldAccountNoExists(CustomerRegistrationBE CustomerRegistrationBE)//Neeraj-ID077
        {
            try
            {
                return objConsumerDal.IsOldAccountNoExists(objiDsignBal.DoSerialize<CustomerRegistrationBE>(CustomerRegistrationBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument IsEmailExists(CustomerRegistrationBE CustomerRegistrationBE)//Fiaz - ID103
        {
            try
            {
                return objConsumerDal.IsEmailExists(objiDsignBal.DoSerialize<CustomerRegistrationBE>(CustomerRegistrationBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument AssignMeter(CustomerRegistrationBE CustomerRegistrationBE)//NEERAJ - ID077
        {
            try
            {
                return objConsumerDal.AssignMeter(objiDsignBal.DoSerialize<CustomerRegistrationBE>(CustomerRegistrationBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument GetCustomerForAssignMeter(CustomerRegistrationBE CustomerRegistrationBE)//NEERAJ - ID077
        {
            try
            {
                return objConsumerDal.GetCustomerForAssignMeter(objiDsignBal.DoSerialize<CustomerRegistrationBE>(CustomerRegistrationBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument GetCustomerForAdjustmentNoBill(CustomerRegistrationBE CustomerRegistrationBE)//NEERAJ - ID077
        {
            try
            {
                return objConsumerDal.GetCustomerForAdjustmentNoBill(objiDsignBal.DoSerialize<CustomerRegistrationBE>(CustomerRegistrationBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument ReAssignMeter(CustomerRegistrationBE CustomerRegistrationBE)//NEERAJ - ID077
        {
            try
            {
                return objConsumerDal.ReAssignMeter(objiDsignBal.DoSerialize<CustomerRegistrationBE>(CustomerRegistrationBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument GetCustomerForBillGen(CustomerRegistrationBE CustomerRegistrationBE)//NEERAJ - ID077
        {
            try
            {
                return objConsumerDal.GetCustomerForBillGen(objiDsignBal.DoSerialize<CustomerRegistrationBE>(CustomerRegistrationBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetCustomerDetails(CustomerRegistrationBE CustomerRegistrationBE)//NEERAJ - ID077
        {
            try
            {
                return objConsumerDal.GetCustomerDetails(objiDsignBal.DoSerialize<CustomerRegistrationBE>(CustomerRegistrationBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument GetCustomerDetailsByApprovalID(ChangeCustomerTypeBE objChangeCustomerTypeBE)//NEERAJ - ID077
        {
            try
            {
                return objConsumerDal.GetCustomerDetailsByApprovalID(objiDsignBal.DoSerialize<ChangeCustomerTypeBE>(objChangeCustomerTypeBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument GetIdentityDetails(CustomerRegistrationBE CustomerRegistrationBE)//NEERAJ - ID077
        {
            try
            {
                return objConsumerDal.GetIdentityDetails(objiDsignBal.DoSerialize<CustomerRegistrationBE>(CustomerRegistrationBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetCustomerDocuments(CustomerRegistrationBE CustomerRegistrationBE)//NEERAJ - ID077
        {
            try
            {
                return objConsumerDal.GetCustomerDocuments(objiDsignBal.DoSerialize<CustomerRegistrationBE>(CustomerRegistrationBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetCustomerUDFValues(CustomerRegistrationBE CustomerRegistrationBE)//NEERAJ - ID077
        {
            try
            {
                return objConsumerDal.GetCustomerUDFValues(objiDsignBal.DoSerialize<CustomerRegistrationBE>(CustomerRegistrationBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }


        #endregion

        public XmlDocument GetCustomerAddress_ByCheck(RptCustomerLedgerBe ObjRptCustomerLedgerBe)//Neeraj-ID077
        {
            try
            {
                return objConsumerDal.GetCustomerAddress_ByCheck(objiDsignBal.DoSerialize<RptCustomerLedgerBe>(ObjRptCustomerLedgerBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument GetCustomerDetailsForMeter_ByCheck(RptCustomerLedgerBe ObjRptCustomerLedgerBe)//Neeraj-ID077
        {
            try
            {
                return objConsumerDal.GetCustomerDetailsForMeter_ByCheck(objiDsignBal.DoSerialize<RptCustomerLedgerBe>(ObjRptCustomerLedgerBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument GetCustomerBook_ByCheck(RptCustomerLedgerBe ObjRptCustomerLedgerBe)//Neeraj-ID077
        {
            try
            {
                return objConsumerDal.GetCustomerBook_ByCheck(objiDsignBal.DoSerialize<RptCustomerLedgerBe>(ObjRptCustomerLedgerBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument CustomerRegistrationByApproval(CustomerRegistrationBE ObjCustomerRegistrationBE)//Neeraj-ID077
        {
            try
            {
                return objConsumerDal.CustomerRegistrationByApproval(objiDsignBal.DoSerialize<CustomerRegistrationBE>(ObjCustomerRegistrationBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion
    }
}
