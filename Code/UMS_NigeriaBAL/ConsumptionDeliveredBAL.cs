﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;

namespace UMS_NigeriaBAL
{
    public class ConsumptionDeliveredBAL
    {
        #region Members
        ConsumptionDeliveredDAL objConsumptionDeliveredDAL = new ConsumptionDeliveredDAL();
        iDsignBAL objiDsignBal = new iDsignBAL();
        #endregion
        public XmlDocument ConsumptionDelivered(ConsumptionDeliveredBE _objConsumptionDeliveredBE, Statement statement)
        {
            return objConsumptionDeliveredDAL.ConsumptionDelivered(objiDsignBal.DoSerialize<ConsumptionDeliveredBE>(_objConsumptionDeliveredBE), statement);
        }

        public XmlDocument ConsumptionDelivered(ConsumptionDeliveredBE _objConsumptionDeliveredBE, ReturnType value)
        {
            return objConsumptionDeliveredDAL.ConsumptionDelivered(objiDsignBal.DoSerialize<ConsumptionDeliveredBE>(_objConsumptionDeliveredBE), value);
        }
    }
}
