﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UMS_NigriaDAL;
using iDSignHelper;
using UMS_NigeriaBE;
using System.Xml;

namespace UMS_NigeriaBAL
{
    public class CustomerBulkBal
    {
        #region Members
        CustomerBulkDal _objCustomerBulkDal = new CustomerBulkDal();
        iDsignBAL _objiDsignBal = new iDsignBAL();
        CustomerBulkBe _objCustomerBulkBe = new CustomerBulkBe();
        #endregion

        #region Methods

        public XmlDocument Get(CustomerBulkBe _objCustomerBulkBe, ReturnType value)
        {
            try
            {
                return _objCustomerBulkDal.Get(_objiDsignBal.DoSerialize<CustomerBulkBe>(_objCustomerBulkBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        
        #endregion
    }
}
