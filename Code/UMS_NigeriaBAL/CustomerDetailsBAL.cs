﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UMS_NigriaDAL;
using UMS_NigeriaBE;
using iDSignHelper;
using System.Data;

namespace UMS_NigeriaBAL
{
    public class CustomerDetailsBAL
    {
        #region Members
        CustomerDetailsBe objCustomerDetailsBe = new CustomerDetailsBe();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        CustomerDetailsDAL objCustomerDetailsDAL = new CustomerDetailsDAL();
        #endregion

        public XmlDocument GetBillDetails(CustomerDetailsBe objCustomerDetailsBe, ReturnType value)
        {
            try
            {
                return objCustomerDetailsDAL.Get(_ObjiDsignBAL.DoSerialize<CustomerDetailsBe>(objCustomerDetailsBe), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument Readings(CustomerDetailsBe objCustomerDetailsBe, Statement value)
        {
            try
            {
                return objCustomerDetailsDAL.Readings(_ObjiDsignBAL.DoSerialize<CustomerDetailsBe>(objCustomerDetailsBe), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument CountCustomersBAL(CustomerDetailsBe objCustomerDetailsBe, Statement value)
        {
            try
            {
                return objCustomerDetailsDAL.CountCustomersDAL(_ObjiDsignBAL.DoSerialize<CustomerDetailsBe>(objCustomerDetailsBe), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument InsertBulkCustomers(DataSet ds, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objCustomerDetailsDAL.InsertBulkCustomers(_ObjiDsignBAL.DoSerialize(ds), action); break;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument SendBillAdjustmentForApprovalBAL(CustomerDetailsBe OjbCustomerDetailsBe)
        {
            try
            {
                return objCustomerDetailsDAL.SendBillAdjustmentForApprovalDAL(_ObjiDsignBAL.DoSerialize<CustomerDetailsBe>(OjbCustomerDetailsBe));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument SendMeterAdjustmentForApprovalBAL(CustomerDetailsBe OjbCustomerDetailsBe)
        {
            try
            {
                return objCustomerDetailsDAL.SendMeterAdjustmentForApprovalDAL(_ObjiDsignBAL.DoSerialize<CustomerDetailsBe>(OjbCustomerDetailsBe));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetBillAdjustmentDetailsBAL(CustomerDetailsBe objCustomerDetailsBe)
        {
            return objCustomerDetailsDAL.GetBillAdjustmentDetailsDAL(_ObjiDsignBAL.DoSerialize<CustomerDetailsBe>(objCustomerDetailsBe));
        }

        public XmlDocument GetCustomersByAccountNo_ServBAL(CustomerDetailsBe objCustomerDetailsBe)//NEERAJ--ID077
        {
            try
            {
                return objCustomerDetailsDAL.GetCustomersByAccountNo_ServDAL(_ObjiDsignBAL.DoSerialize<CustomerDetailsBe>(objCustomerDetailsBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetCustomersDetailsForSPTBillingBAL(XmlDocument XmlDoc)//NEERAJ--ID077
        {
            return objCustomerDetailsDAL.GetCustomersDetailsForSPTBillingDAL(XmlDoc);
        }

        public XmlDocument InsertNewCustomerLocatorBAL(NewCustomerLocatorBE objNewCustomerLocatorBE)//Neeraj-ID077
        {
            XmlDocument xml = null;

            return xml = objCustomerDetailsDAL.InsertNewCustomerLocatorDAL(_ObjiDsignBAL.DoSerialize<NewCustomerLocatorBE>(objNewCustomerLocatorBE));

        }

        public XmlDocument GetCustLocatorForApprovalBAL(NewCustomerLocatorBE objNewCustomerLocatorBE)//Neeraj-ID077
        {
            XmlDocument xml = null;

            return xml = objCustomerDetailsDAL.GetCustLocatorForApprovalDAL(_ObjiDsignBAL.DoSerialize<NewCustomerLocatorBE>(objNewCustomerLocatorBE));

        }

        public XmlDocument GetCustLocatorActionedBAL(NewCustomerLocatorBE objNewCustomerLocatorBE)//Neeraj-ID077
        {
            XmlDocument xml = null;

            return xml = objCustomerDetailsDAL.GetCustLocatorActionedDAL(_ObjiDsignBAL.DoSerialize<NewCustomerLocatorBE>(objNewCustomerLocatorBE));

        }

        public XmlDocument ApproveCustLocatorBAL(NewCustomerLocatorBE objNewCustomerLocatorBE)//Neeraj-ID077
        {
            XmlDocument xml = null;

            return xml = objCustomerDetailsDAL.ApproveCustLocatorDAL(_ObjiDsignBAL.DoSerialize<NewCustomerLocatorBE>(objNewCustomerLocatorBE));

        }

        public XmlDocument UpdateCustomerOutstandingBAL(CustomerDetailsBe OjbCustomerDetailsBe)//Neeraj-ID077
        {
            XmlDocument xml = null;

            return xml = objCustomerDetailsDAL.UpdateCustomerOutstandingDAL(_ObjiDsignBAL.DoSerialize<CustomerDetailsBe>(OjbCustomerDetailsBe));

        }
    }
}


