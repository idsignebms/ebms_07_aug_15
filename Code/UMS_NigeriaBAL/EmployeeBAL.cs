﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UMS_NigriaDAL;
using UMS_NigeriaBE;
using iDSignHelper;

namespace UMS_NigeriaBAL
{
    public class EmployeeBAL
    {
        #region Members

        EmployeeDAL objEmployeeDAL = new EmployeeDAL();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        #endregion

        #region Call Business Logic

        public XmlDocument GetEmployeeForListBindDAL()
        {
            return objEmployeeDAL.GetEmployeeForListBindDAL();
        }

        public XmlDocument GetEmployee(EmployeeBE _objEmployeeBE, ReturnType value)
        {
            try
            {
                return objEmployeeDAL.GetEmployee(_ObjiDsignBAL.DoSerialize<EmployeeBE>(_objEmployeeBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public XmlDocument InsertEmployee(EmployeeBE _objEmployeeBE, Statement action)
        {
            try
            {
                return objEmployeeDAL.InsertEmployee(_ObjiDsignBAL.DoSerialize<EmployeeBE>(_objEmployeeBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
