﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using iDSignHelper;

namespace UMS_NigeriaBAL
{
    public class GeneralReportsBal
    {

        #region Members
        iDsignBAL _objiDsignBal = new iDsignBAL();
        GeneralReportsDal _objGeneralReportsDal = new GeneralReportsDal();
        #endregion

        #region Methods
        public DataSet GetLedgerReport(LedgerBe _objLedgerBe, ReturnType value)
        {
            try
            {
                return _objGeneralReportsDal.GetLedgerReport(_objiDsignBal.DoSerialize<LedgerBe>(_objLedgerBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetDebitReport(RptDebitBe _objRptDebitBe, ReturnType value)
        {
            try
            {
                return _objGeneralReportsDal.GetDebitReport(_objiDsignBal.DoSerialize<RptDebitBe>(_objRptDebitBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetCreditReport(RptCreditBe _objRptCreditBe, ReturnType value)
        {
            try
            {
                return _objGeneralReportsDal.GetCreditReport(_objiDsignBal.DoSerialize<RptCreditBe>(_objRptCreditBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public DataSet GetPaymentsReport(RptPaymentsBe _objRptPaymentsBe, ReturnType value)
        {
            try
            {
                return _objGeneralReportsDal.GetPaymentsReport(_objiDsignBal.DoSerialize<RptPaymentsBe>(_objRptPaymentsBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public DataSet GetDirectCustReport(RptDirectCustomersBe _objRptDirectCustomersBe, ReturnType value)
        {
            try
            {
                return _objGeneralReportsDal.GetDirectCustReport(_objiDsignBal.DoSerialize<RptDirectCustomersBe>(_objRptDirectCustomersBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public DataSet GetReadCustReport(RptReadCustomersBe _objRptReadCustomersBe, ReturnType value)
        {
            try
            {
                return _objGeneralReportsDal.GetReadCustReport(_objiDsignBal.DoSerialize<RptReadCustomersBe>(_objRptReadCustomersBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetNoNameOrAddressReport(RptNoNameOrAddressBe _objRptNoNameOrAddressBe, ReturnType value)
        {
            try
            {
                return _objGeneralReportsDal.GetNoNameOrAddressReport(_objiDsignBal.DoSerialize<RptNoNameOrAddressBe>(_objRptNoNameOrAddressBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public DataSet GetBillsReport(RptCustomerBillsBe _objRptCustomerBillsBe, ReturnType value)
        {
            try
            {
                return _objGeneralReportsDal.GetBillsReport(_objiDsignBal.DoSerialize<RptCustomerBillsBe>(_objRptCustomerBillsBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public DataSet GetUsageConsumedReport(RptUsageConsumedBe _objRptUsageConsumedBe, ReturnType value)
        {
            try
            {
                return _objGeneralReportsDal.GetUsageConsumedReport(_objiDsignBal.DoSerialize<RptUsageConsumedBe>(_objRptUsageConsumedBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public DataSet GetNoUsageConsumedReport(RptNoUsageConsumedBe _objRptNoUsageConsumedBe, ReturnType value)
        {
            try
            {
                return _objGeneralReportsDal.GetNoUsageConsumedReport(_objiDsignBal.DoSerialize<RptNoUsageConsumedBe>(_objRptNoUsageConsumedBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion
    }
}
