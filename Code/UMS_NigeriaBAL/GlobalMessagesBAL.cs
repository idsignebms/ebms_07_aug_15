﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using iDSignHelper;
using UMS_NigriaDAL;
using System.Data;
using UMS_NigeriaBE;

namespace UMS_NigeriaBAL
{
   public class GlobalMessagesBAL
   {

       #region Parameters
       private GlobalMessagesDAL _objGlobalMessagesDal = new GlobalMessagesDAL();
       GlobalMessages objGlobalMessege = new GlobalMessages();
       iDsignBAL _ObjiDsignBAL = new iDsignBAL();

       #endregion
       #region Methods
       // GLOBAL MISSING MESSAGE
       public XmlDocument AddGllobalMessageBAL(XmlDocument xml)
       {
           return _objGlobalMessagesDal.GlobalMessageActionDAL(xml, Statement.Insert);
        }

       public XmlDocument InsertGlobalMessages(XmlDocument Xml, Statement action)
       {
           try
           {
               return _objGlobalMessagesDal.InsertGlobalMessages(Xml, action);
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
       public XmlDocument GetGlobalMessages(GlobalMessages objGlobalMessages, ReturnType value)
       {
           try
           {
               return _objGlobalMessagesDal.GetGlobalMessages(_ObjiDsignBAL.DoSerialize<GlobalMessages>(objGlobalMessages), value);
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       public XmlDocument GetStatesBAL()
       {
           return _objGlobalMessagesDal.GetStatesDAL();
       }
       public XmlDocument GetBillingTypesBAL()
       {

           return _objGlobalMessagesDal.GetBillingTypesDAL();

       }
       public XmlDocument DeleteGlobalMessageBAL(XmlDocument xml) 
       {
           return _objGlobalMessagesDal.GlobalMessageActionDAL(xml, Statement.Delete);
       }
       public XmlDocument GetBillingMessagesBAL(XmlDocument xml)
       {
           return _objGlobalMessagesDal.GetBillingMessagesDAL(xml);
       }

       // DISTRICT MESSAGES
       public XmlDocument GetDistrictlevelMessagesBAL(XmlDocument xml)
       {
           return _objGlobalMessagesDal.GetDistrictlevelMessagesDAL(xml);
       }
       public XmlDocument DistrictlevelMessageActionBAL(XmlDocument xml)
       {
           return _objGlobalMessagesDal.DistrictlevelMessageActionDAL(xml, Statement.Insert);
       }
       public XmlDocument DeleteDisitrictlevelMessageBAL(XmlDocument xml)
       {
           return _objGlobalMessagesDal.DistrictlevelMessageActionDAL(xml, Statement.Delete);
       }
       public XmlDocument GetBillingMessagesBasedOnDistrictsBAL(XmlDocument xml)
       {
           return _objGlobalMessagesDal.GetBillingMessagesBasedOnDistrictsDAL(xml);
       }
       #endregion
   }
}
