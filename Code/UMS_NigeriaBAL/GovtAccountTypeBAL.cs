﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using iDSignHelper;
using UMS_NigeriaBE;
using UMS_NigriaDAL;

namespace UMS_NigeriaBAL
{
    public class GovtAccountTypeBAL
    {
        #region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        GovtAccountTypeDAL _ObjGovtAccountTypeDAL = new GovtAccountTypeDAL();
        #endregion

        #region Methods

        public XmlDocument GovtAccoutnType(GovtAccountTypeBE _objGovtAccountTypeBE, Statement action)
        {
            try
            {
                return _ObjGovtAccountTypeDAL.GovtAccoutnType(_ObjiDsignBAL.DoSerialize<GovtAccountTypeBE>(_objGovtAccountTypeBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetGovtAccountTypeList(GovtAccountTypeBE _objGovtAccountTypeBe)
        {
            try
            {
                return _ObjGovtAccountTypeDAL.GetGovtAccountTypeList(_ObjiDsignBAL.DoSerialize<GovtAccountTypeBE>(_objGovtAccountTypeBe));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public XmlDocument GetGovtAccountTypeList()
        {
            try
            {
                return _ObjGovtAccountTypeDAL.GetGovtAccountTypeList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
