﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using UMS_NigriaDAL;
using UMS_NigeriaBE;
using System.Xml;

namespace UMS_NigeriaBAL
{
    public class IsTamperBal
    {
        #region Members
        iDsignBAL _objiDsignBAL = new iDsignBAL();
        IsTamperDal _objIsTamperDal = new IsTamperDal();
        #endregion

        #region Method      

        public XmlDocument InsertTamper(IsTamperBe _objIsTamperBe, Statement action)
        {
            try
            {
                return _objIsTamperDal.InsertTamper(_objiDsignBAL.DoSerialize<IsTamperBe>(_objIsTamperBe), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
