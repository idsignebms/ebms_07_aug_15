﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml;
using iDSignHelper;
using UMS_NigeriaBE;
using UMS_NigriaDAL;

namespace UMS_NigeriaBAL
{
    public class LGABAL
    {
        #region Members

        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        LGADAL _ObjLGADAL = new LGADAL();
        #endregion

        #region Method
        public XmlDocument LGA(LGABE _ObjLGABE, Statement action)
        {
            try
            {
                return _ObjLGADAL.LGA(_ObjiDsignBAL.DoSerialize<LGABE>(_ObjLGABE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument LGA(LGABE _objLGABe, ReturnType value)
        {
            try
            {
                return _ObjLGADAL.LGA(_ObjiDsignBAL.DoSerialize<LGABE>(_objLGABe), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
