﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Xml;

namespace UMS_NigeriaBAL
{
    public class LoginPageBAL
    {
        #region Members
        LoginPageDAL _ObjLoginPageDAL = new LoginPageDAL();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        #endregion

        #region Methods
        //Getting Login Details
        public XmlDocument Login(LoginPageBE _ObjLoginPageBE, ReturnType value)
        {
            try
            {
                return _ObjLoginPageDAL.GetLoginDetails(_ObjiDsignBAL.DoSerialize<LoginPageBE>(_ObjLoginPageBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
