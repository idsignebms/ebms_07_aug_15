﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using UMS_NigriaDAL;
using System.Xml;
using UMS_NigeriaBE;

namespace UMS_NigeriaBAL
{
    public class MarketersBal
    {
        #region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        MarketersDal _objMarketersDal = new MarketersDal();
        #endregion

        #region Methods
        public XmlDocument InsertMarketer(MarketersBe _objMarketersBe, Statement action)
        {
            try
            {
                return _objMarketersDal.InsertMarketer(_ObjiDsignBAL.DoSerialize<MarketersBe>(_objMarketersBe), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetMarketer(MarketersBe _objMarketersBe, ReturnType value)
        {
            try
            {
                return _objMarketersDal.GetMarketer(_ObjiDsignBAL.DoSerialize<MarketersBe>(_objMarketersBe), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
