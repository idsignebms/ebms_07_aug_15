﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Xml;
using System.Data;

namespace UMS_NigeriaBAL
{
    public class MastersBAL
    {
        #region Members

        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        MastersDAL _ObjMastersDAL = new MastersDAL();
        #endregion

        #region Methods For Countries,States,Districts
        //Insert Countries
        public XmlDocument Countries(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.Countries(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Insert States
        public XmlDocument States(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.States(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        ////Insert Districts
        //public XmlDocument Districts(MastersBE _ObjMastersBE, Statement action)
        //{
        //    try
        //    {
        //        return _ObjMastersDAL.Districts(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        //Get Countries List For DDL
        public XmlDocument GetCountries(ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.GetCountries(value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get States List For DDL
        public XmlDocument GetStates(ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.GetStates(value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument Get(MastersBE _ObjMastersBE, ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.Get(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Methods For BusinessUnits
        public XmlDocument GetBusinessUnits(MastersBE _ObjMastersBE, ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.GetBusinessUnits(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetBusinessUnits(ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.GetBusinessUnits(value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument BusinessUnits(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.BusinessUnits(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Methods For ServiceUnits

        public XmlDocument ServiceUnit(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.ServiceUnit(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument ServiceUnit(MastersBE _ObjMastersBE, ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.ServiceUnit(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods For ServiceCenters

        public XmlDocument ServiceCenter(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.ServiceCenter(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument ServiceCenter(MastersBE _ObjMastersBE, ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.ServiceCenter(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods For SubStations

        public XmlDocument SubStation(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.SubStation(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument SubStation(MastersBE _ObjMastersBE, ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.SubStation(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods For Feeders

        public XmlDocument Feeder(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.Feeder(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument Feeder(MastersBE _ObjMastersBE, ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.Feeder(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetFeeders(ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.GetFeeders(value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods For TransFormers

        public XmlDocument TransFormer(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.TransFormer(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument TransFormer(MastersBE _ObjMastersBE, ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.TransFormer(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods For Poles

        //public XmlDocument Pole(MastersBE _ObjMastersBE, Statement action)
        //{
        //    try
        //    {
        //        return _ObjMastersDAL.Pole(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public XmlDocument Pole(MastersBE _ObjMastersBE, ReturnType value)
        //{
        //    try
        //    {
        //        return _ObjMastersDAL.Pole(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), value);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        #endregion

        #region Methods For Cycle

        public XmlDocument Cycle(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.Cycle(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument Cycle(MastersBE _ObjMastersBE, ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.Cycle(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods For BookNumber

        public XmlDocument BookNumber(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.BookNumber(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument BookNumber(MastersBE _ObjMastersBE, ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.BookNumber(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods For Agency

        public XmlDocument Agency(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.Agency(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument Agency(MastersBE _ObjMastersBE, ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.Agency(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods For MeterTypes

        public XmlDocument MeterType(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.MeterType(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument MeterType(MastersBE _ObjMastersBE, ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.MeterType(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods For CustomerTypes

        public XmlDocument CustomerType(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.CustomerType(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument CustomerType(MastersBE _ObjMastersBE, ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.CustomerType(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods For CustomerIdentityTypes

        public XmlDocument IdentityType(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.CustomerIdentityType(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument IdentityType(MastersBE _ObjMastersBE, ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.CustomerIdentityType(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods For Designation

        public XmlDocument Designation(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.Designation(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument Designation(MastersBE _ObjMastersBE, ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.Designation(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods For Title

        public XmlDocument Title(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.Title(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument Title(MastersBE _ObjMastersBE, ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.Title(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods For Account Types

        public XmlDocument AccountType(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.AccountType(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument AccountType(MastersBE _ObjMastersBE, ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.AccountType(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Methods For RouteMangement

        public XmlDocument RouteMangement(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.RouteManagement(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument RouteMangement(MastersBE _ObjMastersBE, ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.RouteManagement(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region PaymentEntry

        public XmlDocument BatchEntry(BatchEntryBe _ObjBatchEntryBe, Statement action)
        {
            try
            {
                return _ObjMastersDAL.BatchEntry(_ObjiDsignBAL.DoSerialize<BatchEntryBe>(_ObjBatchEntryBe), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument PaymentEntry(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.PaymentEntry(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument PaymentEntryTemporaryBAL(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.PaymentEntryTemporaryDAL(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument PaymentEntry(ReturnType value)
        {
            try
            {
                return _ObjMastersDAL.PaymentEntry(value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument PaymentUploadValidationBAL(PaymentsBe _objPaymentsBe, DataTable PaymentUploadDt)
        {
            try
            {
                return _ObjMastersDAL.PaymentUploadValidationDAL(_ObjiDsignBAL.DoSerialize<PaymentsBe>(_objPaymentsBe), _ObjiDsignBAL.DoSerialize(PaymentUploadDt));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods For CashOffices

        public XmlDocument CashOffices(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.CashOffices(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetCashOfficesList(MastersBE _ObjMastersBE)
        {
            try
            {
                return _ObjMastersDAL.GetCashOfficesList(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region PaymentList
        public XmlDocument PaymentList(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.PaymentList(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Batch
        public XmlDocument DeleteBatch(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.DeleteBatch(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DcumentPayments
        public XmlDocument Insert(PaymentsBe _objPaymentsBe, DataTable dt, Statement action)
        {
            try
            {
                return _ObjMastersDAL.Insert(_ObjiDsignBAL.DoSerialize<PaymentsBe>(_objPaymentsBe), _ObjiDsignBAL.DoSerialize(dt), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Customers

        public XmlDocument IsCustomerInBillProcessBAL(MastersBE _ObjMastersBE, Statement action)
        {
            try
            {
                return _ObjMastersDAL.IsCustomerInBillProcessDAL(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetCustomersInBillingProcess()
        {
            return _ObjMastersDAL.GetCustomersInBillingProcess();
        }

        #endregion

        #region Approval

        public XmlDocument GetApprovalsListBAL(MastersBE _ObjMastersBE)//Neeraj
        {
            return _ObjMastersDAL.GetApprovalsListDAL(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE));
        }

        #endregion

        #region EBill
        public XmlDocument GetEBillDetailsBAL(MastersBE _ObjMasterBE)
        {
            return _ObjMastersDAL.GetEBillDetailsDAL(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMasterBE));
        }
        public XmlDocument CheckEBillExecutionTimeBAL(MastersBE _ObjMasterBE)
        {
            return _ObjMastersDAL.CheckEBillExecutionTimeDAL(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMasterBE));
        }
        public XmlDocument InsertEBillPaymentBAL(DataTable dtEbillPaymentDetails)
        {
            XmlDocument xml = _ObjiDsignBAL.DoSerialize(dtEbillPaymentDetails);
            return _ObjMastersDAL.InsertEBillPaymentDAL(xml);
        }
        public XmlDocument InsertEBillMailLogsBAL(DataTable EmailStatus)
        {
            return _ObjMastersDAL.InsertEBillMailLogsDAL(_ObjiDsignBAL.DoSerialize(EmailStatus));
        }
        public XmlDocument GetEBillMailToSendBAL()
        {
            return _ObjMastersDAL.GetEBillMailToSendDAL();
        }
        public XmlDocument UpdateEBillMailsToSendBAL(DataTable dtEBillID)
        {
            return _ObjMastersDAL.UpdateEBillMailsToSendDAL(_ObjiDsignBAL.DoSerialize(dtEBillID));
        }
        public XmlDocument GetBuHeadOfficeMailsBAL()
        {
            return _ObjMastersDAL.GetBuHeadOfficeMailsDAL();
        }
        #endregion

        #region Key Authentication
        public XmlDocument AuthenticateKey(MastersBE _ObjMasterBE)
        {
            return _ObjMastersDAL.AuthenticateKey(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMasterBE));
        }
        #endregion

        #region Estimation
        public XmlDocument GetEstimatedCustomersBAL(EstimationBE ObjEstimationBE) // NEERAJ KANOJIY--ID077
        {
            return _ObjMastersDAL.GetEstimatedCustomersDAL(_ObjiDsignBAL.DoSerialize<EstimationBE>(ObjEstimationBE));
        }

        public XmlDocument InsertEstimationBAL(EstimationBE ObjEstimationBE, DataTable objDataTable) // NEERAJ KANOJIY--ID077
        {
            return _ObjMastersDAL.InsertEstimationDAL(_ObjiDsignBAL.DoSerialize<EstimationBE>(ObjEstimationBE), _ObjiDsignBAL.DoSerialize(objDataTable));
        }

        public XmlDocument GetEstimatedCustomersByCycleBAL(EstimationBE ObjEstimationBE) // NEERAJ KANOJIY--ID077
        {
            return _ObjMastersDAL.GetEstimatedCustomersByCycleDAL(_ObjiDsignBAL.DoSerialize<EstimationBE>(ObjEstimationBE));
        }

        public XmlDocument UpdateEstimationBAL(DataTable objDataTable) // NEERAJ KANOJIY--ID077
        {
            return _ObjMastersDAL.UpdateEstimationDAL(_ObjiDsignBAL.DoSerialize(objDataTable));
        }

        public XmlDocument DeleteEstimationSettingBAL(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            xml = _ObjMastersDAL.DeleteEstimationSettingDAL(XmlDoc);
            return xml;
        }

        public XmlDocument GetEstimatedDetails(EstimationBE ObjEstimationBE, ReturnType value)
        {
            XmlDocument xml = null;
            xml = _ObjMastersDAL.GetEstimatedDetails(_ObjiDsignBAL.DoSerialize<EstimationBE>(ObjEstimationBE), value);
            return xml;
        }
        #endregion

        #region Spot Billing
        public XmlDocument GetSpotBillingInputsDAL(XmlDocument XmlDoc)
        {
            return _ObjMastersDAL.GetSpotBillingInputsDAL(XmlDoc);
        }

        public XmlDocument InsertHHIputFileDetailsBAL(XmlDocument XmlDoc)
        {
            return _ObjMastersDAL.InsertHHIputFileDetailsDAL(XmlDoc);
        }

        public XmlDocument IsHHInputFileExistsBAL(XmlDocument XmlDoc)
        {
            return _ObjMastersDAL.IsHHInputFileExistsDAL(XmlDoc);
        }

        public XmlDocument GetHHInputFilesListBAL(XmlDocument XmlDoc)
        {
            return _ObjMastersDAL.GetHHInputFilesListDAL(XmlDoc);
        }

        public XmlDocument DeleteHHInputFileBAL(XmlDocument XmlDoc)
        {
            return _ObjMastersDAL.DeleteHHInputFileDAL(XmlDoc);
        }
        #endregion

        #region Autocomplete

        public XmlDocument AutoCompleteListBAL(XmlDocument XmlDoc) //Neeraj-ID077
        {
            return _ObjMastersDAL.AutoCompleteListDAL(XmlDoc);
        }
        #endregion

        //#region Methods For PolesManagement

        ////As per new table structure
        //public XmlDocument AddPole(MastersBE _ObjMastersBE)//Insert PoleMaster
        //{
        //    try
        //    {
        //        return _ObjMastersDAL.AddPole(_ObjiDsignBAL.DoSerialize<MastersBE>(_ObjMastersBE));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //#endregion

        public XmlDocument GetMeterNoList(MastersBE objMastersBE)//Neeraj-ID077
        {
            try
            {
                return _ObjMastersDAL.GetMeterNoList(_ObjiDsignBAL.DoSerialize<MastersBE>(objMastersBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetAvailableMeterList(MastersBE objMastersBE)//Neeraj-ID077
        {
            try
            {
                return _ObjMastersDAL.GetAvailableMeterList(_ObjiDsignBAL.DoSerialize<MastersBE>(objMastersBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetClusterCategoriesListToBind()//Neeraj-ID077
        {
            try
            {
                return _ObjMastersDAL.GetClusterCategoriesListToBind();
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetUserDefinedControlsList()//Neeraj-ID077
        {
            try
            {
                return _ObjMastersDAL.GetUserDefinedControlsList();
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument BindCycleByBU(MastersBE objMastersBE)//Neeraj-ID077
        {
            try
            {
                return _ObjMastersDAL.BindCycleByBU(_ObjiDsignBAL.DoSerialize<MastersBE>(objMastersBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument IsMeterExist(MastersBE objMastersBE)//Neeraj-ID077
        {
            try
            {
                return _ObjMastersDAL.IsMeterExist(_ObjiDsignBAL.DoSerialize<MastersBE>(objMastersBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument ChangeReadCustToDirect(MastersBE objMastersBE)//Neeraj-ID077
        {
            try
            {
                return _ObjMastersDAL.ChangeReadCustToDirect(_ObjiDsignBAL.DoSerialize<MastersBE>(objMastersBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument GetDisconnectedMeterDetails(MastersBE objMastersBE)//Neeraj-ID077
        {
            try
            {
                return _ObjMastersDAL.GetDisconnectedMeterDetails(_ObjiDsignBAL.DoSerialize<MastersBE>(objMastersBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetValidatePaymentsUpload(string BU_ID, DataTable dtPayments)//Karteek
        {
            try
            {
                return _ObjMastersDAL.GetValidatePaymentsUpload(BU_ID, dtPayments);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public bool InsertPaymentsDT(string CreatedBy, string DocumentPath, string DocumentName, int BatchNo, DataTable dtPayments, out string Status)//Karteek
        {
            try
            {
                return _ObjMastersDAL.InsertPaymentsDT(CreatedBy, DocumentPath, DocumentName, BatchNo, dtPayments, out Status);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public int InsertPaymentsDT_Temp(string CreatedBy, string DocumentPath, string DocumentName, int BatchNo, DataTable dtPayments)//Karteek
        {
            try
            {
                return _ObjMastersDAL.InsertPaymentsDT_Temp(CreatedBy, DocumentPath, DocumentName, BatchNo, dtPayments);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
    }
}
