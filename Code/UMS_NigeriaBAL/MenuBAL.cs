﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using iDSignHelper;
using UMS_NigeriaBE;
using UMS_NigriaDAL;

namespace UMS_NigeriaBAL
{
    public class MenuBAL
    {
        #region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        MenuDAL _ObjMenuDAL = new MenuDAL();
        #endregion

        #region Methods

        public XmlDocument Menu(MenuBE _objMenuBE, Statement action)
        {
            try
            {
                return _ObjMenuDAL.Menu(_ObjiDsignBAL.DoSerialize<MenuBE>(_objMenuBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetGovtAccountTypeList(MenuBE _objMenuBE)
        {
            try
            {
                return _ObjMenuDAL.Menu(_ObjiDsignBAL.DoSerialize<MenuBE>(_objMenuBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public XmlDocument GetMenuList()
        {
            try
            {
                return _ObjMenuDAL.GetMenuList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument Get(ReturnType value)
        {
            try
            {
                return _ObjMenuDAL.Get(value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
