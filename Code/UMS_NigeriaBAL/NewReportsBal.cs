﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UMS_NigriaDAL;
using UMS_NigeriaBE;
using iDSignHelper;
using System.Xml;
using System.Data;

namespace UMS_NigeriaBAL
{
    public class NewReportsBal
    {
        #region Members
        NewReportsDal _objNewReportsDal = new NewReportsDal();
        iDsignBAL _objiDsignBal = new iDsignBAL();
        NewReportsBe _objNewReportsBe = new NewReportsBe();
        #endregion

        #region Methods

        public XmlDocument Get(NewReportsBe _objNewReportsBe, ReturnType value)
        {
            try
            {
                return _objNewReportsDal.Get(_objiDsignBal.DoSerialize<NewReportsBe>(_objNewReportsBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetData(PDFReportsBe _objPDFReportsBe, ReportType value)
        {
            try
            {
                return _objNewReportsDal.GetData(_objiDsignBal.DoSerialize<PDFReportsBe>(_objPDFReportsBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }


        #endregion
    }
}
