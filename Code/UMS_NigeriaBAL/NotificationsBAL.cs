﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using UMS_NigeriaBE;

namespace UMS_NigeriaBAL
{
   public class NotificationsBAL
    {
        #region Members
        NotificationsBE _ObjNotificationsBE = new NotificationsBE();
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        NotificationsDAL _ObjNotificationsDAL = new NotificationsDAL();
        #endregion

        #region Method
        public XmlDocument InsertNotificationBAL(NotificationsBE ObjNotification, Statement value)
        {
            try
            {
                return _ObjNotificationsDAL.InsertNotificationDAL(_ObjiDsignBAL.DoSerialize<NotificationsBE>(ObjNotification), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public XmlDocument GetNotificationListBAL(NotificationsBE ObjNotificationsBE)
        {
            try
            {
                return _ObjNotificationsDAL.GetNotificationListDAL(_ObjiDsignBAL.DoSerialize<NotificationsBE>(ObjNotificationsBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public XmlDocument DeleteNotificationBAL(NotificationsBE ObjNotification, Statement value)
        {
            try
            {
                return _ObjNotificationsDAL.DeleteNotificationDAL(_ObjiDsignBAL.DoSerialize<NotificationsBE>(ObjNotification), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public XmlDocument GetNotificationsBAL(NotificationsBE ObjNotificationsBE)
        {
            try
            {
                return _ObjNotificationsDAL.GetNotificationsDAL(_ObjiDsignBAL.DoSerialize<NotificationsBE>(ObjNotificationsBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public XmlDocument GetNotificationBYIDtBAL(NotificationsBE ObjNotificationsBE)
        {
            try
            {
                return _ObjNotificationsDAL.GetNotificationBYIDsDAL(_ObjiDsignBAL.DoSerialize<NotificationsBE>(ObjNotificationsBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public XmlDocument DeleteNewsBAL(NotificationsBE ObjNotification, Statement value)
        {
            try
            {
                return _ObjNotificationsDAL.DeleteNewsDAL(_ObjiDsignBAL.DoSerialize<NotificationsBE>(ObjNotification), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion    
    }
}
