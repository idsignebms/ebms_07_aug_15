﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using UMS_NigriaDAL;
using System.Xml;
using UMS_NigeriaBE;

namespace UMS_NigeriaBAL
{
    public class PaidMeterBal
    {
        #region Members

        iDsignBAL _objiDsignBAL = new iDsignBAL();
        PaidMeterDal _objPaidMeterDal = new PaidMeterDal();
        #endregion

        #region Methods
        public XmlDocument Get(PaidMeterBe _objPaidMeterBe, ReturnType value)
        {
            try
            {
                return _objPaidMeterDal.Get(_objiDsignBAL.DoSerialize<PaidMeterBe>(_objPaidMeterBe), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument Insert(PaidMeterBe _objPaidMeterBe, Statement ation)
        {
            try
            {
                return _objPaidMeterDal.Insert(_objiDsignBAL.DoSerialize<PaidMeterBe>(_objPaidMeterBe), ation);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
