﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;
using iDSignHelper;
using UMS_NigeriaBE;
using UMS_NigriaDAL;

namespace UMS_NigeriaBAL
{
    public class PasswordStrengthBAL
    {
        #region Members

        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        private PasswordStrengthDAL _ObjPasswordStrengthDAL = new PasswordStrengthDAL();

        #endregion

        public XmlDocument GetPasswordStrenth()
        {
            try
            {
                return _ObjPasswordStrengthDAL.GetPasswordStrenth();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument UpdatePasswordStrength(DataSet dsPasswordStrength)
        {
            return _ObjPasswordStrengthDAL.UpdatePasswordStrength(_ObjiDsignBAL.DoSerialize(dsPasswordStrength));
        }
    }
}
