﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using System.Xml;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Data;

namespace UMS_NigeriaBAL
{
    public class PaymentsBal
    {
        PaymentsDal _objPaymentsDal = new PaymentsDal();
        iDsignBAL _objiDsignBal = new iDsignBAL();


        public XmlDocument Insert(PaymentsBe _objPaymentsBe, Statement action)
        {
            try
            {
                return _objPaymentsDal.Insert(_objiDsignBal.DoSerialize<PaymentsBe>(_objPaymentsBe), action);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetDetails(PaymentsBe _objPaymentsBe, ReturnType value)
        {
            try
            {
                return _objPaymentsDal.GetDetails(_objiDsignBal.DoSerialize<PaymentsBe>(_objPaymentsBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument PaymentUploadBatches(PaymentsBatchBE _objPaymentsBatchBE, ReturnType value)
        {
            try
            {
                return _objPaymentsDal.PaymentUploadBatches(_objiDsignBal.DoSerialize<PaymentsBatchBE>(_objPaymentsBatchBE), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument PaymentUploadBatches(PaymentsBatchBE _objPaymentsBatchBE, Statement action)
        {
            try
            {
                return _objPaymentsDal.PaymentUploadBatches(_objiDsignBal.DoSerialize<PaymentsBatchBE>(_objPaymentsBatchBE), action);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet PaymentBatchProcesFailures(PaymentsBatchBE _objPaymentsBatchBE, ReturnType value)
        {
            try
            {
                return _objPaymentsDal.PaymentBatchProcesFailures(_objiDsignBal.DoSerialize<PaymentsBatchBE>(_objPaymentsBatchBE), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet PaymentBatchProcesSuccess(PaymentsBatchBE _objPaymentsBatchBE, ReturnType value)
        {
            try
            {
                return _objPaymentsDal.PaymentBatchProcesSuccess(_objiDsignBal.DoSerialize<PaymentsBatchBE>(_objPaymentsBatchBE), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
    }
}
