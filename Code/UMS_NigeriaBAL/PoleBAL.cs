﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using iDSignHelper;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Data;

namespace UMS_NigeriaBAL
{
    public class PoleBAL
    {
        #region Members

        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        PoleDAL _ObjPoleDAL = new PoleDAL();
        #endregion

        #region Methods For PolesManagement

        //As per new table structure
        public XmlDocument AddPole(PoleBE _ObjPoleBE)//Insert PoleMaster
        {
            try
            {
                return _ObjPoleDAL.AddPole(_ObjiDsignBAL.DoSerialize<PoleBE>(_ObjPoleBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument AddPoleDescription(PoleBE _ObjPoleBE)//Insert PoleDescription
        {
            try
            {
                return _ObjPoleDAL.AddPoleDescription(_ObjiDsignBAL.DoSerialize<PoleBE>(_ObjPoleBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument UpdatePole(PoleBE _ObjPoleBE)//Update PoleMaster
        {
            try
            {
                return _ObjPoleDAL.UpdatePole(_ObjiDsignBAL.DoSerialize<PoleBE>(_ObjPoleBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument UpdatePoleMaster(PoleBE _ObjPoleBE)//Update PoleMaster
        {
            try
            {
                return _ObjPoleDAL.UpdatePoleMaster(_ObjiDsignBAL.DoSerialize<PoleBE>(_ObjPoleBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument DeletePole(PoleBE _ObjPoleBE)//Delete PoleMaster
        {
            try
            {
                return _ObjPoleDAL.DeletePole(_ObjiDsignBAL.DoSerialize<PoleBE>(_ObjPoleBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument DeletePoleMaster(PoleBE _ObjPoleBE)//Delete PoleMaster
        {
            try
            {
                return _ObjPoleDAL.DeletePoleMaster(_ObjiDsignBAL.DoSerialize<PoleBE>(_ObjPoleBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetPoles(PoleBE _ObjPoleBE,ReturnType value)
        {
            try
            {
                XmlDocument xml = _ObjiDsignBAL.DoSerialize<PoleBE>(_ObjPoleBE);
                return _ObjPoleDAL.GetPoles(xml, value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetMasterPoles(PoleBE _ObjPoleBE, ReturnType value)
        {
            try
            {
                XmlDocument xml = _ObjiDsignBAL.DoSerialize<PoleBE>(_ObjPoleBE);
                return _ObjPoleDAL.GetMasterPoles(xml, value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetParentPoles(PoleBE _objPoleBe, ReturnType value)
        {
            try
            {
                return _ObjPoleDAL.GetParentPoles(_ObjiDsignBAL.DoSerialize<PoleBE>(_objPoleBe), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetPoleByOrderId(PoleBE _objPoleBe)
        {
            try
            {
                return _ObjPoleDAL.GetPoleByOrderId(_ObjiDsignBAL.DoSerialize<PoleBE>(_objPoleBe));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet PoleNumberSearch()
        {
            try
            {
                return _ObjPoleDAL.PoleNumberSearch();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument IsPoleNoExists(PoleBE _objPoleBe)
        {
            try
            {
                return _ObjPoleDAL.IsPoleNoExists(_ObjiDsignBAL.DoSerialize<PoleBE>(_objPoleBe));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument IsCustomerExistsExists()
        {
            try
            {
                return _ObjPoleDAL.IsCustomerExistsExists();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetPoleOrderId()
        {
            try
            {
                return _ObjPoleDAL.GetPoleOrderId();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 
        #endregion
    }
}
