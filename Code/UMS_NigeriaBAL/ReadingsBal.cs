﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using System.Xml;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Data;

namespace UMS_NigeriaBAL
{
    public class ReadingsBal
    {
        ReadingsDal _objReadingsDal = new ReadingsDal();
        iDsignBAL _objiDsignBal = new iDsignBAL();


        public XmlDocument ReadingUploadBatches(ReadingsBatchBE _objReadingsBatchBE, ReturnType value)
        {
            try
            {
                return _objReadingsDal.ReadingUploadBatches(_objiDsignBal.DoSerialize<ReadingsBatchBE>(_objReadingsBatchBE), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument ReadingUploadBatches(ReadingsBatchBE _objReadingsBatchBE, Statement action)
        {
            try
            {
                return _objReadingsDal.ReadingUploadBatches(_objiDsignBal.DoSerialize<ReadingsBatchBE>(_objReadingsBatchBE), action);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet ReadingBatchProcesFailures(ReadingsBatchBE _objReadingsBatchBE, ReturnType value)
        {
            try
            {
                return _objReadingsDal.ReadingBatchProcesFailures(_objiDsignBal.DoSerialize<ReadingsBatchBE>(_objReadingsBatchBE), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet ReadingBatchProcessSuccess(ReadingsBatchBE _objReadingsBatchBE, ReturnType value)
        {
            try
            {
                return _objReadingsDal.ReadingBatchProcessSuccess(_objiDsignBal.DoSerialize<ReadingsBatchBE>(_objReadingsBatchBE), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }


        public XmlDocument AvgerageConsumptionUploadBatches(ReadingsBatchBE _objReadingsBatchBE, ReturnType value)
        {
            try
            {
                return _objReadingsDal.AvgerageConsumptionUploadBatches(_objiDsignBal.DoSerialize<ReadingsBatchBE>(_objReadingsBatchBE), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument AvgerageConsumptionUploadBatches(ReadingsBatchBE _objReadingsBatchBE, Statement action)
        {
            try
            {
                return _objReadingsDal.AvgerageConsumptionUploadBatches(_objiDsignBal.DoSerialize<ReadingsBatchBE>(_objReadingsBatchBE), action);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet AvgerageConsumptionBatchProcesFailures(ReadingsBatchBE _objReadingsBatchBE, ReturnType value)
        {
            try
            {
                return _objReadingsDal.AvgerageConsumptionBatchProcesFailures(_objiDsignBal.DoSerialize<ReadingsBatchBE>(_objReadingsBatchBE), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet AvgerageConsumptionBatchSuccessTransactions(ReadingsBatchBE _objReadingsBatchBE, ReturnType value)
        {
            try
            {
                return _objReadingsDal.AvgerageConsumptionBatchSuccessTransactions(_objiDsignBal.DoSerialize<ReadingsBatchBE>(_objReadingsBatchBE), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
    }
}
