﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using iDSignHelper;
using UMS_NigeriaBE;
using UMS_NigriaDAL;

namespace UMS_NigeriaBAL
{
    public class RegionsBAL
    {
        #region Members

        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        RegionsDAL _ObjRegionsDAL = new RegionsDAL();
        #endregion

        #region Method
        public XmlDocument Region(RegionsBE _objRegionsBeBE, Statement action)
        {
            try
            {
                return _ObjRegionsDAL.Region(_ObjiDsignBAL.DoSerialize<RegionsBE>(_objRegionsBeBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument Get(RegionsBE _objRegionsBe, ReturnType value)
        {
            try
            {
                return _ObjRegionsDAL.Regions(_ObjiDsignBAL.DoSerialize<RegionsBE>(_objRegionsBe), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion
    }
}
