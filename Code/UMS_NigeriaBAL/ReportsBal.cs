﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using UMS_NigeriaBE;
using System.Data;
namespace UMS_NigeriaBAL
{
    public class ReportsBal
    {
        #region Members
        ReportsDal _objReportsDal = new ReportsDal();
        iDsignBAL _objiDsignBal = new iDsignBAL();
        ReportsBe _objReportsBe = new ReportsBe();
        #endregion

        #region Methods

        public XmlDocument Get(ReturnType value)
        {
            try
            {
                return _objReportsDal.Get(value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument Get(ReportsBe _objReportsBe, ReturnType value)
        {
            try
            {
                return _objReportsDal.Get(_objiDsignBal.DoSerialize<ReportsBe>(_objReportsBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetBooksForBookwiseMeterReading(ReportsBe _objReportsBe)
        {
            try
            {
                return _objReportsDal.GetBooksForBookwiseMeterReading(_objiDsignBal.DoSerialize<ReportsBe>(_objReportsBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetBooksForBookwiseCustomerExport(ReportsBe _objReportsBe)
        {
            try
            {
                return _objReportsDal.GetBooksForBookwiseCustomerExport(_objiDsignBal.DoSerialize<ReportsBe>(_objReportsBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetNotDisabledBooks(ReportsBe _objReportsBe)
        {
            try
            {
                return _objReportsDal.GetNotDisabledBooks(_objiDsignBal.DoSerialize<ReportsBe>(_objReportsBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        
        public XmlDocument Get(RptAuditTrayBe _objReportsBe, ReturnType value)
        {
            try
            {
                return _objReportsDal.Get(_objiDsignBal.DoSerialize<RptAuditTrayBe>(_objReportsBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        
        public XmlDocument Get(RptMeterReadingBE _objReportsBe, ReturnType value)
        {
            try
            {
                return _objReportsDal.Get(_objiDsignBal.DoSerialize<RptMeterReadingBE>(_objReportsBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument Get(RptPaymentsEditListBE _objReportsBe, ReturnType value)
        {
            try
            {
                return _objReportsDal.Get(_objiDsignBal.DoSerialize<RptPaymentsEditListBE>(_objReportsBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetDetails(RptPaymentsEditListBE _objReportsBe, ReturnType value)
        {
            try
            {
                return _objReportsDal.GetDetails(_objiDsignBal.DoSerialize<RptPaymentsEditListBE>(_objReportsBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        
        public XmlDocument GetList(RptCheckMetersBe _objRptCheckMetersBe, ReturnType value)
        {
            try
            {
                return _objReportsDal.GetList(_objiDsignBal.DoSerialize<RptCheckMetersBe>(_objRptCheckMetersBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetList(RptAuditTrayBe _objRptAuditTrayBe, ReturnType value)
        {
            try
            {
                return _objReportsDal.GetList(_objiDsignBal.DoSerialize<RptAuditTrayBe>(_objRptAuditTrayBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public DataSet GetLogsList(RptAuditTrayBe _objRptAuditTrayBe, ReturnType value)
        {
            try
            {
                return _objReportsDal.GetLogsList(_objiDsignBal.DoSerialize<RptAuditTrayBe>(_objRptAuditTrayBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetList(RptAdjustmentEditListBE _objRptAdjustmentEditListBE, ReturnType value)
        {
            try
            {
                return _objReportsDal.GetList(_objiDsignBal.DoSerialize<RptAdjustmentEditListBE>(_objRptAdjustmentEditListBE), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetAddressList(RptAuditAddressBe _objRptAuditAddressBe, ReturnType value)
        {
            try
            {
                return _objReportsDal.GetAddressList(_objiDsignBal.DoSerialize<RptAuditAddressBe>(_objRptAuditAddressBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetLedger(RptCustomerLedgerBe _objLedgerBe, ReturnType value)
        {
            try
            {
                return _objReportsDal.GetLedger(_objiDsignBal.DoSerialize<RptCustomerLedgerBe>(_objLedgerBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetPreBillingDetails(RptPreBillingBe _objprebillingBe, ReturnType value)
        {
            try
            {
                return _objReportsDal.GetPreBillingDetails(_objiDsignBal.DoSerialize<RptPreBillingBe>(_objprebillingBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetPreBillingReports(RptPreBillingBe _objprebillingBe, ReturnType value)
        {
            try
            {
                return _objReportsDal.GetPreBillingReports(_objiDsignBal.DoSerialize<RptPreBillingBe>(_objprebillingBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetBatchWisePaymentsList(RptPaymentsEditListBE _objReportsBe)
        {
            try
            {
                return _objReportsDal.GetBatchWisePaymentsList(_objiDsignBal.DoSerialize<RptPaymentsEditListBE>(_objReportsBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetCustomersExportByBookWise(RptCheckMetersBe objReportBe)
        {
            try
            {
                return _objReportsDal.GetCustomersExportByBookWise(_objiDsignBal.DoSerialize<RptCheckMetersBe>(objReportBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetReport(ReportsBe objReportBe, ReturnType value)
        {
            try
            {
                return _objReportsDal.GetReport(_objiDsignBal.DoSerialize<ReportsBe>(objReportBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetAgedCustsList(RptAgedCustsBe _objRptAgedCustsBe)
        {
            try
            {
                return _objReportsDal.GetAgedCustsList(_objiDsignBal.DoSerialize<RptAgedCustsBe>(_objRptAgedCustsBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetNewCustomersReport(RptCheckMetersBe objReportBe)
        {
            try
            {
                return _objReportsDal.GetNewCustomersReport(_objiDsignBal.DoSerialize<RptCheckMetersBe>(objReportBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetNoUsageCustomersReport(RptCheckMetersBe objReportBe)
        {
            try
            {
                return _objReportsDal.GetNoUsageCustomersReport(_objiDsignBal.DoSerialize<RptCheckMetersBe>(objReportBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetCustWithNoFxdChargesReportBAL(RptCheckMetersBe objReportBe)
        {
            try
            {
                return _objReportsDal.GetCustWithNoFxdChargesReportDAL(_objiDsignBal.DoSerialize<RptCheckMetersBe>(objReportBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetUnBilledCustomersReport(RptCheckMetersBe objReportBe)
        {
            try
            {
                return _objReportsDal.GetUnBilledCustomersReport(_objiDsignBal.DoSerialize<RptCheckMetersBe>(objReportBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet SearchMasterBAL(MasterSearchBE objMasterSearchBE)
        {
            try
            {
                return _objReportsDal.SearchMasterDAL(_objiDsignBal.DoSerialize<MasterSearchBE>(objMasterSearchBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet RptCustStatisticsByTariff(PDFReportsBe _objPDFReportsBe, ReturnType value)
        {
            try
            {
                return _objReportsDal.RptCustStatisticsByTariff(_objiDsignBal.DoSerialize<PDFReportsBe>(_objPDFReportsBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetLastCloseMonth()
        {
            try
            {
                return _objReportsDal.GetLastCloseMonth();
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetApprovalRegistrationsAuditRay(RptAuditTrayBe objRptAuditTrayBe)
        {
            try
            {
                return _objReportsDal.GetApprovalRegistrationsAuditRay(_objiDsignBal.DoSerialize<RptAuditTrayBe>(objRptAuditTrayBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument IsCustomerExistsInBU_Billing_INDV(RptCustomerLedgerBe objRptCustomerLedgerBe)
        {
            try
            {
                return _objReportsDal.IsCustomerExistsInBU_Billing_INDV(_objiDsignBal.DoSerialize<RptCustomerLedgerBe>(objRptCustomerLedgerBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetCustomerDetails_ByCheck(RptCustomerLedgerBe _objLedgerBe)
        {
            try
            {
                return _objReportsDal.GetCustomerDetails_ByCheck(_objiDsignBal.DoSerialize<RptCustomerLedgerBe>(_objLedgerBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetCusotmerReading(RptCustomerLedgerBe _objLedgerBe)
        {
            try
            {
                return _objReportsDal.GetCusotmerReading(_objiDsignBal.DoSerialize<RptCustomerLedgerBe>(_objLedgerBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion
    }
}
