﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using iDSignHelper;
using UMS_NigriaDAL;
using UMS_NigeriaBE;

namespace UMS_NigeriaBAL
{
    public class RptBatchNoWisePaymentsBal
    {
        #region Members

        iDsignBAL _objiDsignBAL = new iDsignBAL();
        RptBatchNoWisePaymentsDal _objRptBatchNoDal = new RptBatchNoWisePaymentsDal();
        #endregion

        #region Methods
        public XmlDocument Get(RptBatchNoWisePaymentsBe _objRptBatchNoBe, ReturnType value)
        {
            try
            {
                return _objRptBatchNoDal.Get(_objiDsignBAL.DoSerialize<RptBatchNoWisePaymentsBe>(_objRptBatchNoBe), value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument Insert(RptBatchNoWisePaymentsBe _objRptBatchNoBe, Statement ation)
        {
            try
            {
                return _objRptBatchNoDal.Insert(_objiDsignBAL.DoSerialize<RptBatchNoWisePaymentsBe>(_objRptBatchNoBe), ation);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
