﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using UMS_NigeriaBE;
using UMS_NigriaDAL;
using System.Xml;

namespace UMS_NigeriaBAL
{
    public class SideMenusBAL
    {
        iDsignBAL iDsignBAL = new iDsignBAL();
        SideMenusDAL _ObjSideMenusDAL = new SideMenusDAL();
        SideMenusBE _ObjSideMenusBE = new SideMenusBE();

        public XmlDocument GetMenus(int RoleId)
        {
            try
            {
                _ObjSideMenusBE.Role_Id = RoleId;

                return _ObjSideMenusDAL.GetMenus(iDsignBAL.DoSerialize<SideMenusBE>(_ObjSideMenusBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument GetNewMenusBAL(SideMenusBE ObjSideMenuBe)
        {
            try
            {
               

                return _ObjSideMenusDAL.GetNewMenusDAL(iDsignBAL.DoSerialize<SideMenusBE>(ObjSideMenuBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument GetNewMenusNewBAL(SideMenusBE ObjSideMenuBe)
        {
            try
            {


                return _ObjSideMenusDAL.GetNewMenusNewDAL(iDsignBAL.DoSerialize<SideMenusBE>(ObjSideMenuBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument GetConfigurationSettingsMenu(SideMenusBE ObjSideMenuBe)
        {
            try
            {
                return _ObjSideMenusDAL.GetConfigurationSettingsMenu(iDsignBAL.DoSerialize<SideMenusBE>(ObjSideMenuBe));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
    }
}
