﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using UMS_NigeriaBE;
using System.Data;

namespace UMS_NigeriaBAL
{
    public class SpotBillingOutputBal
    {
        #region Members

        SpotBillingOutputDal _objSpotBillingOutputDal = new SpotBillingOutputDal();
        iDsignBAL _objiDsignBal = new iDsignBAL();

        #endregion

        #region Methods

        public XmlDocument GetDetails(SpotBillingOutputBe billingOutputBe, ReturnType value)
        {
            try
            {
                return _objSpotBillingOutputDal.GetDetails(_objiDsignBal.DoSerialize<SpotBillingOutputBe>(billingOutputBe), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }


        public XmlDocument Insert(SpotBillingOutputBe billingOutputBe, Statement action)
        {
            try
            {
                return _objSpotBillingOutputDal.Insert(_objiDsignBal.DoSerialize<SpotBillingOutputBe>(billingOutputBe), action);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion

    }
}
