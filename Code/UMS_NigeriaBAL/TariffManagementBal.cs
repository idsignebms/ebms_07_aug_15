﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using UMS_NigriaDAL;
using System.Xml;
using UMS_NigeriaBE;
using System.Data;

namespace UMS_NigeriaBAL
{
    public class TariffManagementBal
    {
        #region Members
        TariffManagementDal objTariffManagementDal = new TariffManagementDal();
        iDsignBAL objiDsignBal = new iDsignBAL();

        #endregion

        #region Methods
        public XmlDocument Get(TariffManagementBE objTariffManagementBE, ReturnType value)
        {
            try
            {
                return objTariffManagementDal.Get(objiDsignBal.DoSerialize<TariffManagementBE>(objTariffManagementBE), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetTariffApprovalList(TariffManagementBE objTariffManagementBE)
        {
            try
            {
                return objTariffManagementDal.GetTariffApprovalList(objiDsignBal.DoSerialize<TariffManagementBE>(objTariffManagementBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public DataSet GetTariffChangesApproveList(TariffManagementBE objTariffManagementBE)
        {
            try
            {
                return objTariffManagementDal.GetTariffChangesApproveList(objiDsignBal.DoSerialize<TariffManagementBE>(objTariffManagementBE));
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        
        public XmlDocument Get(ReturnType value)
        {
            try
            {
                return objTariffManagementDal.Get(value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument Insert(TariffManagementBE objTariffManagementBE, Statement action)
        {
            try
            {
                return objTariffManagementDal.Insert(objiDsignBal.DoSerialize<TariffManagementBE>(objTariffManagementBE), action);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetTariff(TariffManagementBE objTariffManagementBE, ReturnType value)
        {
            try
            {
                return objTariffManagementDal.GetTariff(objiDsignBal.DoSerialize<TariffManagementBE>(objTariffManagementBE), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument InsertTariff(TariffManagementBE objTariffManagementBE, Statement action)
        {
            try
            {
                return objTariffManagementDal.InsertTariff(objiDsignBal.DoSerialize<TariffManagementBE>(objTariffManagementBE), action);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion
    }
}
