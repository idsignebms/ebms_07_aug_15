﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using iDSignHelper;
using UMS_NigeriaBE;
using UMS_NigriaDAL;

namespace UMS_NigeriaBAL
{
    public class UserDefinedControlsBAL
    {
        #region Members
        iDsignBAL _ObjiDsignBAL = new iDsignBAL();
        UserDefinedControlsDAL _ObjUserDefinedControlsDAL = new UserDefinedControlsDAL();
        #endregion

        public XmlDocument GetUserDefinedControlItems(UserDefinedControlsBE objUserDefinedControlsBE)
        {
            try
            {
                return _ObjUserDefinedControlsDAL.GetUserDefinedControlItems(_ObjiDsignBAL.DoSerialize<UserDefinedControlsBE>(objUserDefinedControlsBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument IsCustomerAssociated(UserDefinedControlsBE objUserDefinedControlsBE)
        {
            try
            {
                return _ObjUserDefinedControlsDAL.IsCustomerAssociated(_ObjiDsignBAL.DoSerialize<UserDefinedControlsBE>(objUserDefinedControlsBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetUserDefinedControlsList()
        {
            try
            {
                return _ObjUserDefinedControlsDAL.GetUserDefinedControlsList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetUserDefinedControlsDetails(UserDefinedControlsBE objUserDefinedControlsBE)
        {
            try
            {
                return _ObjUserDefinedControlsDAL.GetUserDefinedControlsDetails(_ObjiDsignBAL.DoSerialize<UserDefinedControlsBE>(objUserDefinedControlsBE));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument UserDefinedControlsItems(UserDefinedControlsBE _ObjUserDefinedControlsBE, Statement action)
        {
            try
            {
                return _ObjUserDefinedControlsDAL.UserDefinedControlsItems(_ObjiDsignBAL.DoSerialize<UserDefinedControlsBE>(_ObjUserDefinedControlsBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument UserDefinedControlsDetails(UserDefinedControlsBE _ObjUserDefinedControlsBE, Statement action)
        {
            try
            {
                return _ObjUserDefinedControlsDAL.UserDefinedControlsDetails(_ObjiDsignBAL.DoSerialize<UserDefinedControlsBE>(_ObjUserDefinedControlsBE), action);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
