﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UMS_NigriaDAL;
using iDSignHelper;
using System.Xml;
using UMS_NigeriaBE;

namespace UMS_NigeriaBAL
{
    public class UserManagementBAL
    {
        #region Members
        UserManagementDAL objUserManagementDAL = new UserManagementDAL();
        iDsignBAL objiDsignBal = new iDsignBAL();

        #endregion

        #region Methods

        public XmlDocument Get(ReturnType value)
        {
            try
            {
                return objUserManagementDAL.Get(value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument Get(UserManagementBE objUserManagementBE, ReturnType value)
        {
            try
            {
                return objUserManagementDAL.Get(objiDsignBal.DoSerialize<UserManagementBE>(objUserManagementBE), value);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument Insert(UserManagementBE objUserManagementBE, Statement action)
        {
            try
            {
                return objUserManagementDAL.Insert(objiDsignBal.DoSerialize<UserManagementBE>(objUserManagementBE),action);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument DisableUserBAL(UserManagementBE objUserManagementBE)//Neeraj-ID077
        {
            return objUserManagementDAL.DisableUserDAL(objiDsignBal.DoSerialize<UserManagementBE>(objUserManagementBE));
        }
        #endregion
    }
}
