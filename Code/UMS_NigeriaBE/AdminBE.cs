﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;

namespace UMS_NigeriaBE
{
    public class AdminBE
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string Permissions { get; set; }
        public bool IsAssigned { get; set; }
        public int MenuId { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public int Menu_Order { get; set; }
        public int ReferenceMenuId { get; set; }
        public string Icon1 { get; set; }
        public string Icon2 { get; set; }
        public string Page_Order { get; set; }
        public bool IsActive { get; set; }
        public bool ActiveHeader { get; set; }
        public string Menu_Image { get; set; }
        public bool IsAllChecked { get; set; }

        public string ExistingRoleName { get; set; }

        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }

        public int RowsEffected { get; set; }

        public bool IsUsersAssociated { get; set; }

        public int AccessLevelID { get; set; }
        public string AccessLevelName { get; set; }
    }
}
