﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRoot("AdminBEInfoByXml")]
    public class AdminListBE
    {
        [XmlElement("AdminListBE", IsNullable = true)]
        public List<AdminBE> Items { get; set; }

    }

    [XmlRoot("AdminBEInfoByXml")]
    public class AdminListBE_1
    {
        [XmlElement("AdminListBE_1", IsNullable = true)]
        public List<AdminBE> Items { get; set; }

    }
}
