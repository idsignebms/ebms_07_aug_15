﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class ApprovalBe
    {
        public int FunctionId { get; set; }

        public string Function { get; set; }

        public int AccessLevels { get; set; }

        public string CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        public string ModifiedDate { get; set; }

        public bool IsActive { get; set; }

        public int ApprovalRoleId { get; set; }

        public int RoleId { get; set; }

        public int Level { get; set; }

        public bool IsFinalApproval { get; set; }

        public string UserIds { get; set; }

        public bool IsExist { get; set; }

        public string UserId { get; set; }

        public int UserDetailsId { get; set; }

        public bool IsSuccess { get; set; }

        public string Levels { get; set; }

        public string Roles { get; set; }

        public string UserDetailIds { get; set; }

        public string BU_ID { get; set; }
    }

    #region Approval Messages
    public class ApprovalMessagesBe
    {
        public string Message { get; set; }

        public string CreatedBy { get; set; }

        public int TariffChangeRequestId { get; set; }

        public bool IsSuccess { get; set; }
    }
    #endregion

}
