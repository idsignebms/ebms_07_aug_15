﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("ApprovalsInfoByXml")]
    public class ApprovalListBe
    {
        [XmlElement("ApprovalsList", IsNullable = true)]
        public List<ApprovalBe> items { get; set; }
    }
}
