﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class AreaBe
    {
        public int AreaCode { get; set; }
        public string AreaDetails { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }

        public string ZipCode { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifedBy { get; set; }
        public string ModifiedDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsExists { get; set; }
        public bool IsRecordsAssociated { get; set; }

        public int RowNumber { get; set; }
        public int RowsEffected { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalRecords { get; set; }

    }
}
