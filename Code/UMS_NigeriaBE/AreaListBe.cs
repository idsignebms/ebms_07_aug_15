﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("AreaInfoByXml", IsNullable = true)]
    public class AreaListBE
    {
        [XmlElement("AreaBE")]
        public List<AreaBe> items { get; set; }
    }
}
