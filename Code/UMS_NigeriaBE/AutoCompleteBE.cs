﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class AutoCompleteBE
    {
        public int FilterType { get; set; }
        public string ListValue { get; set; }
        public string SearchValue { get; set; }
        public string BUID { get; set; }
    }
}
