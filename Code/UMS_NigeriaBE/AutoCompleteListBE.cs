﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("AutoCompleteListBE")]
    public class AutoCompleteListBE
    {
        [XmlElement("AutoCompleteBE", IsNullable = true)]
        public List<AutoCompleteBE> items { get; set; }
    }
}
