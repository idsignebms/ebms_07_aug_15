﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class BatchStatusBE
    {
        public string  RowNumber { get; set; }
        public int BatchId { get; set; }
        public int BatchNo { get; set; }
        public decimal BatchTotal { get; set; }
        public string BatchDate { get; set; }
        public decimal PaidAmount { get; set; }
        public string BatchStatus { get; set; }

        public int RowsEffected { get; set; }

        public string BU_ID { get; set; }

        public int PageNo { get; set; }
        public int PageSize { get; set; }
    }
}
