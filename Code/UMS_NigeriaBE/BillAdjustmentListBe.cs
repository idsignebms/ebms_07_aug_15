﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("BillAdjustmentsInfoByXml")]
    public class BillAdjustmentListBe
    {
        [XmlElement("BillAdjustments")]
        public List<BillAdjustmentsBe> items { get; set; }

    }
    [XmlRootAttribute("BillAdjustmentsInfoByXml")]
    public class BillAdjustmentLisBeDetails
    {
        [XmlElement("BillAdjustmentLisBeDetails")]
        public List<BillAdjustmentsBe> items { get; set; }

    }
}
