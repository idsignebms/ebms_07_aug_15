﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class BillAdjustmentsBe
    {
        #region String Properties

        public string AccountNo { get; set; }

        public string CustomerId { get; set; }
        public int AdjustmentLogId { get; set; }

        public string Remarks { get; set; }

        public string PreviousReading { get; set; }

        public string CurrentReadingBeforeAdjustment { get; set; }

        public string CurrentReadingAfterAdjustment { get; set; }

        public string Reason { get; set; }

        public string CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedDate { get; set; }

        public string ModifiedBy { get; set; }

        public string ReasonCode { get; set; }

        public string DESCRIPTION { get; set; }

        public string BatchDate { get; set; }

        public string AdjustmentName { get; set; }

        public string BU_ID { get; set; }


        #endregion

        #region Int Properties

        public int BillAdjustmentType { get; set; }

        public int AdjustedUnits { get; set; }

        public int ApprovalStatusId { get; set; }

        public int CustomerBillId { get; set; }

        public int ChargeID { get; set; }

        public int BillAdjustmentId { get; set; }

        public int Consumption { get; set; }

        public int RowCount { get; set; }

        public int BatchID { get; set; }

        public int BatchNo { get; set; }

        public int RCID { get; set; }

        public int TotalCustomers { get; set; }

        public int PendingUnit { get; set; }

        public int CreditAmount { get; set; }

        public int DebitAmount { get; set; }

        public int FunctionId { get; set; }

        public int BatchStatusId { get; set; }
        #endregion

        #region Bool Properties
        public bool IsSuccess { get; set; }
        public bool IsAdjusted { get; set; }
        public bool IsExists { get; set; }
        public bool IsFinalApproval { get; set; }
        public bool IsApprovalInProcess { get; set; }
        public bool IsAllReadyApproved { get; set; }
        public bool IsInProcess { get; set; }
        public bool IsNotNew { get; set; }       
        
        #endregion

        #region Decimal Properties

        public decimal AmountEffected { get; set; }

        public decimal TaxEffected { get; set; }

        public decimal TotalAmountEffected { get; set; }

        public decimal EnergryCharges { get; set; }

        public decimal BatchTotal { get; set; }

        public decimal PendingAmount { get; set; }

        public decimal AdditionalCharges { get; set; }

        #endregion

    }
}
