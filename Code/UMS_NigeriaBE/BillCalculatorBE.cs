﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class BillCalculatorBE
    {
        public string Date { get; set; }
        public int CustomerTypeId { get; set; }
        public int SubType { get; set; }
        public float Consumption { get; set; }
        public float Vat { get; set; }
        public double BillAmount { get; set; }
        public double FixedChargeAmount { get; set; }
        public int ClassId { get; set; }
        public string CustomerTypeName { get; set; }
        public string CustomerSubTypeNames { get; set; }
        public string TaxAmonut { get; set; }
        public string EnergyChargeAmount { get; set; }
        public string AdditionalChargeAmount { get; set; }
        public string AdditionalTaxAmount { get; set; }
        public string EnergyTaxAmount { get; set; }
        public string TotalEnergyCharges { get; set; }
        public string ChargeName { get; set; }
        public string Amount { get; set; }
        public string Tax { get; set; }
        public string ChargeAmount { get; set; }
        public string TaxAmount { get; set; }
        public string TariffType { get; set; }
        public string AccountNo { get; set; }
        public bool IsAccountNoExists { get; set; }
    }
}
