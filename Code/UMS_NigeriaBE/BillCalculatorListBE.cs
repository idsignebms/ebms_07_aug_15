﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("BillCalculatorInfoByXml")]
    public class BillCalculatorListBE
    {
        [XmlElement("BillCalculatorBE")]
        public List<BillCalculatorBE> items { get; set; }
    }

    [XmlRootAttribute("BillCalculatorInfoByXml")]
    public class BillCalculatorListCharges
    {
        [XmlElement("BillCalculatorAdditionalCharges")]
        public List<BillCalculatorBE> items { get; set; }
    }
}
