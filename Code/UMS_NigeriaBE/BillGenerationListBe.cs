﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("BillGenerationInfoByXml", IsNullable = true)]
    public class BillGenerationListBe
    {
        [XmlElement("BillGenerationList")]
        public List<BillGenerationBe> Items { get; set; }
    }
}
