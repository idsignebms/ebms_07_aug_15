﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class BillingBE
    {
        #region Members
        //Meter Reading 
        public int CustomerReadingId { get; set; }
        public string CustomerUniqueNo { get; set; }
        public string ReadDate { get; set; }
        public string ReadBy { get; set; }
        public string PreviousReading { get; set; }
        public string PresentReading { get; set; }
        public string AverageReading { get; set; }
        public string Usage { get; set; }
        public string TotalReadingEnergies { get; set; }
        public string TotalReadings { get; set; }
        public string CustomerType { get; set; }        
        public string Multiplier { get; set; }
        public string ReadType { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ReadMethod { get; set; }
        public string AccNum { get; set; }
        public string AccountNo { get; set; }
        public string Name { get; set; }
        public string MeterNo { get; set; }
        public string PrvExist { get; set; }
        public int IsSuccess { get; set; }
        public int CustomerTypeId { get; set; }
        public int PageNo { get; set; }
        public int TotalRecords { get; set; }
        public int PageSize { get; set; }
        public string RouteNum { get; set; }
        public int MeterDials { get; set; }
        public int Decimals { get; set; }
        public decimal Amount { get; set; }
        public int IsBilled { get; set; }
        public DateTime ReadingDate { get; set; }
        public string LastBillReadType { get; set; }
        public string CurrentReading { get; set; }


        //Billing Details
        public string BatchDate { get; set; }
        public string UserId { get; set; }
        public int BatchNo { get; set; }
        public int BatchId { get; set; }
        public decimal BatchTotal { get; set; }
        public int BatchStausId { get; set; }
        public string CashierId { get; set; }
        public string CashierName { get; set; }
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }
        public string BatchName { get; set; }
        public string Notes { get; set; }
        public string DocFileName { get; set; }
        public string RecFileName { get; set; }
        public int BatchStatusId { get; set; }
        public string Description { get; set; }
        public string PaymentMode { get; set; }

        public bool IsPaymentModeExists { get; set; }
        public string PaymentTypes { get; set; }
        public decimal BatchPendingAmount { get; set; }
        public string BUID { get; set; }
        public string BU_ID { get; set; }
        public string SU_ID { get; set; }
        public string SC_ID { get; set; }

        //Document Uploads
        public string DocumentPath { get; set; }
        public string Document { get; set; }
        public string Remarks { get; set; }
        public string LatestDate { get; set; }

        public bool IsExists { get; set; }
        public bool IsPrvExists { get; set; }
        public bool IsApprovalProces { get; set; }
        public bool IsActive { get; set; }
        public bool IsDirectCustomer { get; set; }


        public bool IsLocked { get; set; }
        public int ApproveStatusId { get; set; }

        public int Flag { get; set; }

        public int ReadCodeId { get; set; }

        public string EstimatedBillDate { get; set; }

        public bool IsActiveMonth { get; set; }

        public bool IsActiveBillMonth { get; set; }

        public bool IsDuplicate { get; set; }

        public bool IsLatestBill { get; set; }
        public bool IsTamper { get; set; }

        public string OldAccountNo { get; set; }
        public int ActiveStatusId { get; set; }

        public int RowCount { get; set; }
        public int MeterReadingFrom { get; set; }
        public string InititalReading { get; set; }
        public bool Rollover { get; set; }

        public string BookNo { get; set; }
        public int MarketerId { get; set; }

        public bool IsMeterChangedDateExists { get; set; }
        public bool IsMeterAssignedDateExists { get; set; }
        public bool IsReadToDirectApproval { get; set; }
        public bool IsMeterChangeApproval { get; set; }
        public bool IsPerformAction { get; set; }
        public bool IsFinalApproval { get; set; }
        public bool IsInitialBillingkWh { get; set; }        
        public int FunctionId { get; set; }
        public int RowNumber { get; set; }

        public bool IsMaxReading { get; set; }
        public bool IsMinReading { get; set; }
        public bool IsCustomerHaveReadings { get; set; }
        
        public string GlobalAccountNumber { get; set; }
        public string GlobalAccNoAndAccNo { get; set; }
        public string Tariff { get; set; }
        public decimal OutstandingAmount { get; set; }
        public string ReadingMonth { get; set; }
        public string ReadCode { get; set; }
        public string BilledDate { get; set; }

        public bool IsApprovalInProcess { get; set; }
        #endregion
    }
}
