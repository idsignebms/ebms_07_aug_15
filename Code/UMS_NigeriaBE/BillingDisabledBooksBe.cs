﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class BillingDisabledBooksBe
    {
        #region Integers
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int RowNumber { get; set; }
        public int YearId { get; set; }
        public int MonthId { get; set; }
        public int TotalRecords { get; set; }
        public int ApproveStatusId { get; set; }
        public int DisabledBookId { get; set; }
        public int DisableTypeId { get; set; }
        #endregion

        #region String
        public string BookNo { get; set; }
        public string Details { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string MonthYear { get; set; }
        public string DisableType { get; set; }
        public string BU_ID { get; set; }
        #endregion

        #region booleans
        public bool IsSuccess { get; set; }
        public bool IsPartialBill { get; set; }
        public bool BookNoExistsForThisMonth { get; set; }
        #endregion

        public string DisableDate { get; set; }
    }
}
