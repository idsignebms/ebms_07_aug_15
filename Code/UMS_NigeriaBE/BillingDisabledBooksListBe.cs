﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("BillingDisabledBooksBeInfoByXml", IsNullable = true)]
    public class BillingDisabledBooksListBe
    {
        [XmlElement("BillingDisabledBooksBe")]
        public List<BillingDisabledBooksBe> items { get; set; }
    }
}
