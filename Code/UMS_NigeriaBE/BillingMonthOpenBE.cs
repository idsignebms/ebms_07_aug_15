﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class BillingMonthOpenBE
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public bool IsSuccess { get; set; }
        public string StatusType { get; set; }
        public int RowNumber { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public string Name { get; set; }
        public int OpenStatusId { get; set; }
        public bool IsBillingMonthExists { get; set; }
        public string ModifiedBy { get; set; }
        public string CreatedBy { get; set; }
        public int BillMonthId { get; set; }
        public int TotalRecords { get; set; }
        public int TotalBills { get; set; }
        public int BillingQueueScheduleId { get; set; }
        public string Details { get; set; }
        public string Cycle { get; set; }
        public string Feeder { get; set; }
        public decimal TotalAmount { get; set; }
        public string LastBillGeneratedDate { get; set; }
        public string Reason { get; set; }
        public bool IsExists { get; set; }
        public bool IsActiveMonth { get; set; }
        public int flag { get; set; }
        public bool IsLesserMonthYear { get; set; }
        public bool IsSomeBGNotBilled { get; set; }
        
    }
}
