﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("BillingMonthOpenBEInfoByXml", IsNullable = true)]
    public class BillingMonthOpenListBE
    {
        [XmlElement("BillingMonthOpenBE")]
        public List<BillingMonthOpenBE> Items { get; set; }
    }
}
