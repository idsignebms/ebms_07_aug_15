﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class BookNoReorderBe
    {
        #region stringProperties

        public string BU_ID { get; set; }
        public string BusinessUnitName { get; set; }
        public string SU_ID { get; set; }
        public string ServiceUnitName { get; set; }
        public string SC_ID { get; set; }
        public string ServiceCenterName { get; set; }
        public string BookNo { get; set; }
        public string BookCode { get; set; }
        public string CycleId { get; set; }
        public string Orders { get; set; }
        public string AccountNo { get; set; }
        public string Name { get; set; }
        public string ServiceAddress { get; set; }
        public string MobileNo { get; set; }        
        #endregion

        #region IntProperties
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int RowNumber { get; set; }
        public int TotalRecords { get; set; }
        public int SortOrder { get; set; }
        public int EffectedRows { get; set; }
        #endregion
    }
}