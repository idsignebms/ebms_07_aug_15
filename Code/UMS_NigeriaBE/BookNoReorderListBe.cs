﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("BookNoReorderBeInfoByXml", IsNullable = true)]
    public class BookNoReorderListBe
    {
        [XmlElement("BookNoReorderBe")]
        public List<BookNoReorderBe> Items { get; set; }
    }

    [XmlRootAttribute("ChangeBookNoBeInfoByXml", IsNullable = true)]
    public class ChangeBookNoListBe
    {
        [XmlElement("ChangeBookNoBe")]
        public List<ChangeBookNoBe> Items { get; set; }
    }
}
