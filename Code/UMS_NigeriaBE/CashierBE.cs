﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class CashierBE
    {
        public int RowNumber { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string ContactNo1 { get; set; }
        public string ContactNo2 { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string Notes { get; set; }
        public int CashOfficeId { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string EmailId { get; set; }
        public bool IsSuccess { get; set; }
        public bool IsEmailIdExists { get; set; }
        public int CashierId { get; set; }
        public string Cashier { get; set; }
        public int ActiveStatusId { get; set; }

        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalRecords { get; set; }
        public int RowsEffected { get; set; }

        public int Flag { get; set; }
        public string AccountNo { get; set; }
        public string CashOffice { get; set; }

        public string BU_ID { get; set; }
    }
}
