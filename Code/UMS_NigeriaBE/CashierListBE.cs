﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("CashierBeInfoByXml", IsNullable = true)]
    public class CashierListBE
    {
        [XmlElement("CashierBE")]
        public List<CashierBE> Items { get; set; }
    }
}
