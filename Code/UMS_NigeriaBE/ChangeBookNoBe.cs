﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class ChangeBookNoBe
    {
        public string UserId { get; set; }
        public string AccountNo { get; set; }
        public string GlobalAccountNumber { get; set; }
        public string GlobalAccNoAndAccNo { get; set; }
        public string StateCode { get; set; }
        public string BU_ID { get; set; }
        public string SU_ID { get; set; }
        public string ServiceCenterId { get; set; }
        public string CycleId { get; set; }
        public string BookNo { get; set; }
        public string PostalLandMark { get; set; }
        public string PostalStreet { get; set; }
        public string PostalCity { get; set; }
        public string PostalHouseNo { get; set; }
        public string PostalZipCode { get; set; }
        public string ServiceLandMark { get; set; }
        public string ServiceStreet { get; set; }
        public string ServiceCity { get; set; }
        public string ServiceHouseNo { get; set; }
        public string ServiceZipCode { get; set; }
        public string ModifiedBy { get; set; }
        public string Details { get; set; }
        public string Name { get; set; }
        public string SurName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }        
        public string KnownAs { get; set; }
        public string MeterNo { get; set; }
        public string Tariff { get; set; }
        public string OldAccountNo { get; set; }
        public string OldMeterReading { get; set; }
        public string InitialReading { get; set; }
        public string OldMeterNo { get; set; }
        public string PreviousReading { get; set; }
        public string PreviousReadingDate { get; set; }
        public string AverageUsage { get; set; }
        public string AverageDailyUsage { get; set; }
        public string LastReadType { get; set; }
        public string OldMeterReadingDate { get; set; }
        public string NewMeterReadingDate { get; set; }
        public string NewMeterInitialReading { get; set; }
        

        public bool IsAccountNoNotExists { get; set; }
        public bool IsBookNoChanged { get; set; }
        public bool IsSuccess { get; set; }
        public bool IsMeterNumberNotExists { get; set; }
        public bool IsMeterNoExists { get; set; }
        public bool IsExist { get; set; }
        public bool IsApprovalInProcess { get; set; }
        public bool IsMeterNoAlreadyRaised { get; set; }
        public bool IsOldMeterReadingIsWrong { get; set; }
        public bool IsDirectCustomer { get; set; }
        public bool IsFinalApproval { get; set; }
        public bool IsReadingsExist { get; set; }
        public bool IsReadToDirectApproval { get; set; }
        public bool IsHavingCapmiAmt { get; set; }
        public bool IsNotCreditMeter { get; set; }
        
        public int InitialBillingkWh { get; set; }        
        public int ActiveStatusId { get; set; }
        public int ApprovalStatusId { get; set; }
        public int MeterTypeId { get; set; }
        public int MeterDials { get; set; }
        public string ActiveStatus { get; set; }
        public int Flag { get; set; }
        public int FunctionId { get; set; }
        public int RowNumber { get; set; }
        public int ActiveStatusChangeLogId { get; set; }
        public int MeterNumberChangeLogId { get; set; }
        public int AddressChangeLogId { get; set; }
        public int ReadToDirectChangeLogId { get; set; }
        
        //Change Name
        public int NameChangeLogId { get; set; }
        public string OldTitle { get; set; }
        public string OldFirstName { get; set; }
        public string OldMiddleName { get; set; }
        public string OldLastName { get; set; }
        public string OldKnownAs { get; set; }
        public string Status { get; set; }
        public string NextRole { get; set; }
        public string CurrentRole { get; set; }
        public int PresentApprovalRole { get; set; }
        public int NextApprovalRole { get; set; }

        public string ChangeDate { get; set; }

        public decimal OutStandingAmount { get; set; }

        public DateTime LastTransactionDate { get; set; }
        public string LastTransactionDate1 { get; set; }
    }

    public class ChangeContactBE
    {
        public string GlobalAccNoAndAccountNo { get; set; }
        public string GlobalAccountNumber { get; set; }
        public string AccountNo { get; set; }
        public string OldAccountNo { get; set; }
        public string Name { get; set; }
        public string ServiceAddress { get; set; }
        public string ModifiedBy { get; set; }
        public string Details { get; set; }
        public string OldEmailId { get; set; }
        public string NewEmailId { get; set; }
        public string OldHomeContactNumber { get; set; }
        public string NewHomeContactNumber { get; set; }
        public string OldBusinessContactNumber { get; set; }
        public string NewBusinessContactNumber { get; set; }
        public string OldOtherContactNumber { get; set; }
        public string NewOtherContactNumber { get; set; }
        public string BU_ID { get; set; }

        public bool IsFinalApproval { get; set; }
        public bool IsApprovalInProcess { get; set; }
        public bool IsAccountNoNotExists { get; set; }
        public bool IsSuccess { get; set; }
        
        public int ApprovalStatusId { get; set; }
        public int FunctionId { get; set; }
        public int RowNumber { get; set; }
        public int ContactChangeLogId { get; set; }
        public int PresentApprovalRole { get; set; }
        public int NextApprovalRole { get; set; }

        public decimal OutStandingAmount { get; set; }
    }
}
