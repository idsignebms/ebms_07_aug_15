﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class ChangeCustomerTypeBE
    {
        public string AccountNo { get; set; }
        public string OldAccountNo { get; set; }
        public string GlobalAccountNumber { get; set; }
        public string GlobalAccNoAndAccNo { get; set; }
        public string FirstName { get; set; }
        public string Tariff { get; set; }
        public string MeterNo { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string KnownAs { get; set; }
        public string Reason { get; set; }
        public string ModifiedBy { get; set; }
        public string BU_ID { get; set; }
        public string UserId { get; set; }

        public int CustomerTypeChangeLogId { get; set; }
        public int AssignMeterChangeId { get; set; }
        public int BookNoChangeLogId { get; set; }
        public int MeterReadingLogId { get; set; }
        public int PaymentLogId { get; set; }
        public int AverageChangeLogId { get; set; }
        public int AdjustmentLogId { get; set; }        
        
        public int CustomerTypeId { get; set; }
        public string CustomerType { get; set; }
        public bool IsAccountNoNotExists { get; set; }

        public int ApprovalStatusId { get; set; }
        public bool IsApprovalInProcess { get; set; }
        public bool IsSuccess { get; set; }

        public int FunctionId { get; set; }
        public bool IsFinalApproval { get; set; }

        public bool IsRollOver { get; set; }
        public int MeterReadingFromId { get; set; }

        public decimal OutStandingAmount { get; set; }
        public decimal Consumption { get; set; }

        public int ARID { get; set; }

        public int PresentMeterReadingChangeLogId { get; set; }
    }
}
