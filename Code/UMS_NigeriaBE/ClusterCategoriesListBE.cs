﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("ClusterCategoriesBEInfoByXml", IsNullable = true)]
    public class ClusterCategoriesListBE
    {
        [XmlElement("ClusterCategoriesBE")]
        public List<ClusterCategoriesBE> items { get; set; }
    }
}
