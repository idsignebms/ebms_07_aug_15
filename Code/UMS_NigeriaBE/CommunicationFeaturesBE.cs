﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class CommunicationFeaturesBE
    {
        public int CommunicationFeaturesID { get; set; }
        public string FeatureName { get; set; }
        public bool IsEmailFeature { get; set; }
        public bool IsSMSFeature { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public bool ActiveStatusId { get; set; }

        public bool IsEmailEnabledBillGeneration { get; set; }
        public bool IsSMSEnabledBillGeneration { get; set; }

        public bool IsEmailEnabledPayments { get; set; }
        public bool IsSMSEnabledPayments { get; set; }

        public bool IsEmailEnabledAdjustments { get; set; }
        public bool IsSMSEnabledAdjustments { get; set; }

        public string MobileNo { get; set; }
        public string GlobalAccountNumber { get; set; }
        public string EmailId { get; set; }
        public bool IsSuccess { get; set; }
        public string Name { get; set; }
        public string PaidAmount { get; set; }
        public string PaidDate { get; set; }
        public string TransactionId { get; set; }
        public string AdjustedAmount { get; set; }
        public string OutStandingAmount { get; set; }
        public string MeterNumber { get; set; }        
    }
}
