﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class Constants
    {
        #region RoleIds

        public const int BUManagerRoleId = 2;
        public const int CommercialManagerRoleId = 3;
        public const int CashierRoleId = 4;
        public const int DataEntryOperatorRoleId = 5;
        public const int SUManagerRoleId = 6;
        public const int AdminRoleId = 1;

        #endregion

        #region Constant values

        public static decimal Vat = 5;
        public const int PageSizeIncrement = 10;
        public const int PageSizeStarts = 15;
        public const int SearchCustPageSize = 100;
        public const int SearchCustPageSizeIncrement = 100;
        public const int SessionPageMethodsSeconds = 60;
        public const int pageNoValue = 1;
        public const int Zero = 0;
        public const string SelectedValue = "--Select--";
        public const int Active = 1;
        public const int DeActive = 2;
        public const int Delete = 3;
        public const int ConsumptionTypeId = 2;
        public const int MeterReadingPageSizeIncrement = 500;
        public const int MeterReadingPageStart = 15;
        public const int BatchFinishedStatusId = 2;
        public const int BILLING_YEAR_LENGTH = 3;
        public const int Meter_Reading_HugePercentage = 10;
        public const string Hypen = "--";
        public const string MMC = "Meter Maintenance Charge";
        public const int txtMaxLength = 1;
        public const string ACTIVE = "Active";
        public const string DEACTIVE = "DeActive";
        public const string DELETE = "Delete";

        public const string SAVE = "Save";
        public const string SAVENEXT = "SaveNext";
        public const string Mobile = "Mobile";
        public const string Web = "Web";
        public const string Spot = "Spot";
        public const string ServiceCenter = "ServiceCenter";
        public const string ServiceUnit = "ServiceUnit";
        public const string BookNumbers = "BookNumbers";
        public const string BusinessUnits = "BusinessUnits";
        public const string Cycle = "CycleCode";
        public const string Route = "RouteCode";

        public const int LastValidYear = 5;
        public const int StartValidYear = 1900;
        public const int MaxLoginAttempts = 3;
        public const int ReportMonths = 12;
        public const int HihEstimationReportMonths = 6;

        public const int ReadCode = 1; //For Direct Direct Customers

        //For Approval Purpose
        public const int NEW = 0;
        public const int PROCESS = 1;
        public const int APPROVED = 2;
        public const int REJECTED = 3;

        public const int PageSizeStartsReport = 100;
        public const int PageSizeIncrementReport = 100;
        public const int CommandTimeOut = 3600;
        #endregion

        #region CurrencyFormate
        public const int INR_Format = 1;
        public const int MILLION_Format = 2;
        #endregion

        #region PageNames

        public const string NewCountryPageName = "NewCountry";
        public const string MeterReadingPageName = "MeterReading";
        public const string EditCustomerPageName = "EditCustomer";
        public const string SearchCustomerPageName = "SearchCustomer";
        public const string AdvanceSearchCustomerPageName = "AdvanceSearchCustomer";
        public const string DirectCustomersPageName = "DirectCustomers";
        public const string BillAdjustmentsPageName = "BillAdjustments";
        public const string CustomerBillPayment = "CustomerBillPayment";
        public const string TariffChangeConfirm = "TariffChangeConfirm";
        public const string TariffChange = "TariffChange";
        public const string CustomerLedger = "CustomerLedger";
        public const string ConfigurationSettings = "Configuration Settings";

        #endregion

        #region PageUrl

        public const string LoginPage = "~/LoginPage.aspx";
        public const string MeterReadingUpload = "~/Billing/MeterReadingUpload.aspx";
        public const string BillGenerationstep2 = "~/Billing/BillGenerationstep2.aspx";
        public const string CustomerChangeTariffConfirm = "~/TariffManagement/CustomerChangeTariffConfirm.aspx";
        public const string CustomerChangeTariff = "~/TariffManagement/CustomerChangeTariff.aspx";
        public const string AddNewFixedChargePage = "~/TariffManagement/AddNewFixedCharge.aspx";
        public const string RptCustomerLedgerPage = "~/Reports/CustomerLedger.aspx";
        public const string BookNoChangeLogsPage = "~/Reports/BookNoChangeLogs.aspx";
        public const string TariffChangeLogsPage = "~/Reports/TariffChangeLogs.aspx";
        public const string CustomerChangeLogsPage = "~/Reports/CustomerChangeLogs.aspx";
        public const string MeterChangeLogsPage = "~/Reports/MeterChangeLogs.aspx";
        public const string CustomerAddressChangeLogsPage = "~/Reports/CustomerAddressChangeLogs.aspx";
        public const string CustomerNameChangeLogsPage = "~/Reports/CustomerNameChangeLogs.aspx";
        public const string CustomerTypeChangeLogsPage = "~/Reports/CustomerTypeChangeLog.aspx";
        public const string ReadToDirectChangeLogsPage = "~/Reports/ReadToDirectChangeLog.aspx";
        public const string AssignedMeterCustomersLogsPage = "~/Reports/AssignedMeterCustomersLog.aspx";
        public const string PresentMeterReadingChangeLogsPage = "~/Reports/PresentMeterReadingAdjustmentLog.aspx";
        public const string AuditTrayReportPage = "~/Reports/AuditTrayReport.aspx";
        public const string DirecrCustomersConsumptionPreiviewPage = "~/Billing/DirectCustomersConsumptionPreview.aspx";
        public const string DirectCustomersReadingsUploadPage = "~/Billing/DirectCustomersConsumptionUpload.aspx";
        public const string User_Img_Path = "~/Uploads/UserPhotos/";
        public const string AddUserPage = "~/UserManagement/AddUser.aspx";
        public const string UpdateUserPage = "~/UserManagement/UpdateUser.aspx";
        public const string Region = "~/Masters/AddRegions.aspx";
        public const string CustomerRegistrationApprovalLog = "~/Reports/CustomerRegistrationApprovalLog.aspx";
        #endregion

        #region Cookies

        public const string COOKIE_USERNAME = "UserName";
        public const string COOKIE_PASSWORD = "Password";

        #endregion

        #region MainMenuId
        public const int SettingsMenu = 7;
        #endregion

        #region Billing Month Open Status
        public const int Opened = 1;
        public const int Closed = 2;
        public const int Cancelled = 3;
        #endregion

        #region SpotBilling
        public const char SpotBillingInputSeprator = '|';
        public const string SpotBillingInputReplacement = "-";
        #endregion

        #region Bill Adjustment

        //Based on AdjustmentTypes.xml, Neeraj-ID077

        public const string Reading_Adjustment = "1";
        public const string Additional_Charges_Adjustment = "2";
        public const string Consumption_Adjustment = "3";
        public const string Bill_Adjustment = "4";


        public const string QS_BatchID = "bid";
        public const string QS_AdjustmentTypeid = "atid";
        public const string QS_Initiat = "?";
        public const string QS_Seprator = "&";
        public const string QS_EqualTo = "=";
        public const string QS_AdjustmentText = "adtxt";
        public const string QS_BatchDate = "dt";
        #endregion

        #region Customers Registration Steps
        public const int TotalRegistrationSetps = 12;
        #endregion

        #region Pole
        public const bool IsParentPole = true;
        #endregion

        #region User Defined Controls
        public const int TextBox = 1;
        public const int DropDownList = 2;
        public const int RadioButtonList = 3;
        public const int CheckBoxList = 4;

        #endregion

        #region Report Names & Codes

        public const string CustomersWithoutBookGroupReport_Code = "RP-A001";
        public const string CustomersWithoutBookGroupReport_Name = "Customers Without Book Group";
        public const string TariffBUReport_Code = "RP-A011";
        public const string TariffBUReport_Name = "Tariff BU Report";
        public const string CustomerWithNoMeterReport_Code = "RP-A005";
        public const string CustomerWithNoMeterReport_Name = "Direct Customers Report";
        public const string AccountsonContinuousEstimationReport_Code = "RP-A004";
        public const string AccountsonContinuousEstimationReport_Name = "Accounts on Continuous Estimation";
        public const string AccountStatusReport_Code = "RP-A005";
        public const string AccountStatusReport_Name = "Account Status";
        public const string AccountsWithDebitBalanceReport_Code = "RP-A002";
        public const string AccountsWithDebitBalanceReport_Name = "Accounts With Debit Balance";
        public const string PaymentsReport_Code = "RP-A004";
        public const string PaymentsReport_Name = "Payments Report";
        public const string AccountsWithCreditBalanceReport_Code = "RP-A003";
        public const string AccountsWithCreditBalanceReport_Name = "Accounts With Credit Balance";
        //public const string AccountsWithNoNameAddressCustomersReport_Code = "RP-A007";
        //public const string AccountsWithNoNameAddressCustomersReport_Name = "Accounts With No Name Or Address Customers";
        public const string InCompleteCustomerDetailsReport_Code = "RP-A007";
        public const string InCompleteCustomerDetailsReport_Name = "InComplete Customer Details Report";
        public const string CheckMetersCustomersReport_Code = "RP-A010";
        public const string CheckMetersCustomersReport_Name = "Check Meters Customers Report";
        public const string BatchNumberWisePaymentsListReport_Code = "RP-A011";
        public const string BatchNumberWisePaymentsListReport_Name = "Batch Number Wise Payments List";
        public const string CustomerPreBillingReport_Code = "RP-A012";
        public const string CustomerPreBillingReport_Name = "Customer Pre Billing Report";
        public const string AuditTrayReport_Code = "RP-A013";
        public const string AuditTrayReport_Name = "Audit Tray Report";
        public const string MeterChangeLogReport_Code = "RP-A014";
        public const string MeterChangeLogReport_Name = "Meter Change Log";
        public const string AddressChangeLogReport_Code = "RP-A015";
        public const string AddressChangeLogReport_Name = "Address Change Log";
        public const string NameChangeLogReport_Code = "RP-A016";
        public const string NameChangeLogReport_Name = "Name Change Log";
        public const string BookNoChangeLogReport_Code = "RP-A017";
        public const string BookNoChangeLogReport_Name = "Book No Change Log";
        public const string TariffChangeLogReport_Code = "RP-A018";
        public const string TariffChangeLogReport_Name = "Tariff Change Log";
        public const string CustomerBillsReport_Code = "RP-A008";
        public const string CustomerBillsReport_Name = "Customer Bills";
        public const string BatchWiseAdjustmentsListReport_Code = "RP-A020";
        public const string BatchWiseAdjustmentsListReport_Name = "Batch Wise Adjustments List";
        public const string NewCustomersReport_Code = "RP-A021";
        public const string NewCustomersReport_Name = "New Customers Report";
        public const string AgedCustomersAccountReceivableReport_Code = "RP-A022";
        public const string AgedCustomersAccountReceivableReport_Name = "Aged Customers Account Receivable Report";
        public const string NoUsageCustomersReport_Code = "RP-A010";
        public const string NoUsageCustomersReport_Name = "No Usage Customers Report";
        public const string CustomerWithNoFixedChargesReport_Code = "RP-A024";
        public const string CustomerWithNoFixedChargesReport_Name = "Customers With No Fixed Charges";
        public const string UnbilledCustomersReport_Code = "RP-A025";
        public const string UnbilledCustomersReport_Name = "Unbilled Customers Report";
        public const string StatusChangeLogReport_Code = "RP-A026";
        public const string StatusChangeLogReport_Name = "Status Change Log";
        public const string PreBilling_NonReadCustomersReport_Code = "RP-A027";
        public const string PreBilling_NonReadCustomersReport_Name = "Non Read Customers Report";
        public const string PreBilling_ReadCustomersReport_Code = "RP-A028";
        public const string PreBilling_ReadCustomersReport_Name = "Read Customers Report";
        public const string PreBilling_NoBilledCustomersReport_Code = "RP-A029";
        public const string PreBilling_NoBilledCustomersReport_Name = "No Billed Customers Report";
        public const string PreBilling_ZeroUsageCustomersReport_Code = "RP-A030";
        public const string PreBilling_ZeroUsageCustomersReport_Name = "Zero Usage Customers Report";
        public const string PreBilling_High_LowEstimatedCustomersReport_Code = "RP-A031";
        public const string PreBilling_High_LowEstimatedCustomersReport_Name = "High Or Low Estimated Customers Report";
        public const string CustomerTypeChangeLog_Code = "RP-A032";
        public const string CustomerTypeChangeLog_Name = "Customer Type Change Log";
        public const string PreBilling_PartialBillCustomersReport_Code = "RP-A033";
        public const string PreBilling_PartialBillCustomersReport_Name = "Partial Bill Customers Report";
        public const string PreBilling_AvgNotUploadedCustomersReport_Code = "RP-A034";
        public const string PreBilling_AvgNotUploadedCustomersReport_Name = "Average Not Uploaded Customers Report";
        public const string PreBilling_AvgUploadedCustomersReport_Code = "RP-A035";
        public const string PreBilling_AvgUploadedCustomersReport_Name = "Direct Customers Average Uploaded Report";
        public const string ReadToDirectLogReport_Code = "RP-A036";
        public const string ReadToDirectLogReport_Name = "Read To Direct Change Customers Log";
        public const string AssignMeterLogReport_Code = "RP-A037";
        public const string AssignMeterLogReport_Name = "Assigned Meter Customers Log";
        public const string BookWiseCustomerExportReport_Code = "RP-A038";
        public const string BookWiseCustomerExportReport_Name = "Book Wise Customers Export";
        public const string CustomerLedgerReport_Code = "RP-A001";
        public const string CustomerLedgerReport_Name = "Customer Ledger Report";
        public const string CustomerWithMeterReport_Code = "RP-A006";
        public const string CustomerWithMeterReport_Name = "Read Customers Report";
        public const string UsageConsumedReport_Code = "RP-A009";
        public const string UsageConsumedReport_Name = "Consumption Report";
        public const string CustomerPresentReadingAdjustLogReport_Code = "RP-A039";
        public const string CustomerPresentReadingAdjustLogReport_Name = "Customer Present Meter Reading Adjusted Log";
        #endregion
    }

    #region Enum
    public enum ReadCode
    {
        Direct = 1,
        Read = 2,
        Estimate = 3,
        Minimum = 4,
        Average = 5
    }
    public enum MeterReadingFrom
    {
        AccountWise = 1,
        BookWise = 2,
        //RouteWise = 3,
        BulkUpload = 4,
        //MobileUpload = 5
    }

    public enum EnumBillProcessType
    {
        CycleWsie = 1,
        FeederWise = 2,
    }

    public enum NotificationFeature
    {
        Bill,
        Batch
    }

    public enum EnumBillGenarationStatus
    {
        BillingGenarationCompleted = 1,
        FileCreated = 2,
        EmailSent = 3,
        BillingQueeClosed = 4,
        BillGenarationPending = 5
    }

    #region FilterTypes
    public enum EnumFilterTypes
    {
        BusinessUnits = 1,
        Cycles = 2
    }
    #endregion
    public enum EnumMeterTypes
    {
        MD = 1,
        NonMD = 2
    }

    public enum EnumApprovalStatus
    {
        New = 0,
        Process = 1,
        Approved = 2,
        Rejected = 3,
        Hold = 4
    }
    public enum BillingRules//Neeraj-ID077
    {
        AsPerSettingsRule = 1,
        AsPerAverageReadingRule = 2
    }

    public enum EnumApprovalFunctions
    {
        BillAdjustment = 1,
        PaymentEntry = 2,
        ChangeCustomerTariff = 3,
        ChangeCustomerBookNo = 4,
        BookNoDisabled = 5,
        ChangeCustomerAddress = 6,
        ChangeCustomerMeterNo = 7,
        ChangeCustomerName = 8,
        ChangeCustomerStatus = 9,
        CustomerTypeChange = 10,
        AssignMeter=11,
        ChangeReadToDirect=12,
        MeterReadings=13,
        DirectCustomerAverageUpload=14,
        CustomerRegistration=15,
        PresentMeterReadingAdjustment = 16
    }

    public enum CommunicationFeatures
    {
        BillGeneration = 1,
        Payments = 2,
        Adjustments = 3
    }

    public enum PaymentType
    {
        BatchPayment=1,
        BulkUpload=2,
        CustomerPayment=3
    }
    public enum PaymentFrom
    {
        AdvancePayment = 1,
        CustomerWiseBillPayment = 2,
        BatchWisePayment = 3
    }
    public enum ActiveStatus
    {
        Active = 1,
        InActive = 2,
        Hold = 3,
        Closed = 4	
    }
    public enum CustomerStatus
    {
        Active = 1,
        InActive = 2,
        Hold = 3,
        Closed = 4
    }


    #endregion
}
