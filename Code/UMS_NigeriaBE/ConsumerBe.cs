﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class ConsumerBe
    {
        public int IdentityTypeId { get; set; }

        public string IdentityType { get; set; }

        public string BU_ID { get; set; }

        public string BusinessUnitName { get; set; }

        public string SU_ID { get; set; }

        public int RowNumber { get; set; }

        public string ServiceUnitName { get; set; }

        public string ServiceCenterId { get; set; }

        public string ServiceCenterName { get; set; }

        public string BookID { get; set; }

        public string BookName { get; set; }

        public string SubStationId { get; set; }

        public string SubStationName { get; set; }

        public string FeederId { get; set; }

        public string FeederName { get; set; }

        public string TransFormerId { get; set; }

        public string TransFormerName { get; set; }

        public string PoleId { get; set; }

        public string PoleName { get; set; }

        public string ClassID { get; set; }

        public string ClassName { get; set; }

        public int CustomerId { get; set; }

        public string CustomerTypeName { get; set; }

        public string DistrictCode { get; set; }

        public string StateCode { get; set; }

        public int PhaseId { get; set; }

        public string Phase { get; set; }

        public int ConnectionReasonId { get; set; }

        public string Reason { get; set; }

        public string BookNo { get; set; }

        public int ReadCodeId { get; set; }
        public int MarketerId { get; set; }

        public string ReadCode { get; set; }

        public int AccountTypeId { get; set; }

        public string AccountType { get; set; }

        public int MeterTypeId { get; set; }

        public string MeterType { get; set; }

        public int MeterStatusId { get; set; }

        public string MeterStatus { get; set; }

        public int CustomerTypeId { get; set; }

        public string CustomerType { get; set; }

        public int AgencyId { get; set; }

        public string AgencyDetails { get; set; }

        public string Title { get; set; }

        public string Name { get; set; }

        public string SurName { get; set; }

        public string KnownAs { get; set; }

        public string EmailId { get; set; }

        public string DocumentNo { get; set; }

        public string EmployeeCode { get; set; }

        public string HomeContactNo { get; set; }

        public string BusinessContactNo { get; set; }

        public string OtherContactNo { get; set; }

        public string PostalLandMark { get; set; }

        public string PostalStreet { get; set; }

        public string PostalCity { get; set; }

        public string PostalHouseNo { get; set; }

        public string PostalDetails { get; set; }

        public string PostalZipCode { get; set; }

        public string IdentityNo { get; set; }

        public string ServiceLandMark { get; set; }

        public string ServiceStreet { get; set; }

        public string ServiceCity { get; set; }

        public string ServiceHouseNo { get; set; }

        public string ServiceDetails { get; set; }

        public string ServiceZipCode { get; set; }

        public string ServiceEmailId { get; set; }

        public string MobileNo { get; set; }

        public int CustomerSequenceNo { get; set; }

        public int RouteNo { get; set; }

        public int RouteSequenceNo { get; set; }

        public string NeighborAccountNo { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public int TariffId { get; set; }

        public decimal MultiplicationFactor { get; set; }

        public string ApplicationDate { get; set; }

        public string OrganizationCode { get; set; }

        public string OldAccountNo { get; set; }

        public bool IsEmbassyCustomer { get; set; }

        public string EmbassyCode { get; set; }

        public bool IsVIPCustomer { get; set; }

        public string MeterNo { get; set; }

        public string MeterSerialNo { get; set; }

        public string ConnectionDate { get; set; }

        public int MeterDials { get; set; }

        public string InitialReading { get; set; }

        public string CurrentReading { get; set; }

        public string AverageReading { get; set; }
        public string MinimumReading { get; set; }

        public string SetupDate { get; set; }

        public string CertifiedBy { get; set; }

        public int ProcessedById { get; set; }

        public string InstalledBy { get; set; }
        public int TotalRecords
        {
            get;
            set;
        }
        public string ContactPersonName { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        public bool IsSuccess { get; set; }

        public bool IsUpdated { get; set; }


        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public string ProcessedBy { get; set; }

        public string AgencyName { get; set; }

        public string CustomerDocumentPath { get; set; }

        public string CustomerDocumentNames { get; set; }

        public string UserId { get; set; }

        public string CycleName { get; set; }

        public string CycleId { get; set; }

        public bool IsDocumentNoExists { get; set; }

        public bool IsCustomerSequenceNo { get; set; }

        public string CustomerUniqueNo { get; set; }

        public string AccountNo { get; set; }
        public string GlobalAccountNumber { get; set; }


        public string DistrictName { get; set; }

        public string BillNo { get; set; }

        public string StateName { get; set; }

        public string Tariff { get; set; }

        public int Step { get; set; }

        public bool IsMeterNoExists { get; set; }
        public bool IsMeterNumberNotExists { get; set; }

        public bool IsMeterSerialNoExists { get; set; }

        public bool IsNeighborAccountNotExists { get; set; }

        public bool IsNeighborAccountNoExists { get; set; }

        public bool IsOldAccountNoExists { get; set; }

        public int CycleNo { get; set; }

        public string ContactName { get; set; }

        public string ContactNo { get; set; }

        public string NoOfAccounts { get; set; }

        public int RouteId { set; get; }

        public string Route { set; get; }
        public string RouteSeq { set; get; }
        public int CustomerDocumentId { get; set; }
        public string SetviceAddress { get; set; }
        public string AccNum { get; set; }


        public int EnergyCharges { get; set; }

        public int Count { get; set; }

        public int Year { get; set; }

        public int Month { get; set; }

        public int RuleID { get; set; }

        public string RuleName { get; set; }

        public int EstCustomerRuleId { get; set; }

        public decimal Amount { get; set; }

        public string ServiceAddress { get; set; }
        public string BillStatus { get; set; }
        public string MonthYear { get; set; }
        public int IsBillGenerated { get; set; }
        public string CustomerAddress { get; set; }
        public int RowsEffected { get; set; }
        public bool IsOldAccountNotValid { get; set; }
        public bool IsBookNoChanged { get; set; }
        public int BookChanged { get; set; }
        public string BookNoWithDetails { get; set; }
        public string ActiveStatus { get; set; }
        public int StatusId { get; set; }

        public string Comments { get; set; }
        public string IdentityId { get; set; }
        public string CreatedDate { get; set; }
        public decimal TotalBillAmountWithArrears { get; set; }
        public decimal Usage { get; set; }
        public decimal NetEnergyCharges { get; set; }
        public string BookCode { get; set; }
        public int ContinusEstimations { get; set; }
        public string MonthYear1 { get; set; }
        public string MonthYear2 { get; set; }
        public string MonthYear3 { get; set; }
        public decimal Usage1 { get; set; }
        public decimal Usage2 { get; set; }
        public decimal Usage3 { get; set; }
        public decimal Amount1 { get; set; }
        public decimal Amount2 { get; set; }
        public decimal Amount3 { get; set; }
        public string ClassName1 { get; set; }
        public string ClassName2 { get; set; }
        public string ClassName3 { get; set; }
        public decimal MeterAmt { get; set; }
        public int CustomerSortOrder { get; set; }

        public bool IsCAPMIMeter { get; set; }
        
    }
}
