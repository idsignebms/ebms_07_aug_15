﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("ConsumerBeInfoByXml", IsNullable = true)]
    public class ConsumerListBe
    {
        [XmlElement("Consumer")]
        public List<ConsumerBe> items { get; set; }
    }
}
