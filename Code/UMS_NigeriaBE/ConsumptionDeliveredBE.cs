﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class ConsumptionDeliveredBE
    {
        public int ConsumptionDeliveredId { get; set; }
        public string BU_ID { get; set; }
        public string BusinessUnitName { get; set; }
        public string SU_ID { get; set; }
        public string ServiceUnitName { get; set; }
        public decimal Capacity { get; set; }
        public decimal SUPPMConsumption { get; set; }
        public decimal SUCreditConsumption { get; set; }
        public decimal TotalConsumption { get; set; }
        public int ConsumptionMonth { get; set; }
        public int ConsumptionYear { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string ConsumptionMontYear { get; set; }

        public int RowsEffected { get; set; }

        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalRecords { get; set; }
        public int RowNumber { get; set; }

        public bool ConsumptionRecordExists { get; set; }
        public bool ForceUpdate { get; set; }
    }
}
