﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("ConsumptionDeliveredBEInfoByXml", IsNullable = true)]
    public class ConsumptionDeliveredListBE
    {
        [XmlElement("ConsumptionDeliveredBE")]
        public List<ConsumptionDeliveredBE> Items { get; set; }
    }
}
