﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class CustomerBillPDFBE
    {
        public string GlobalAccountNo { get; set; }
        public int BillMonth { get; set; }
        public int BillYear { get; set; }
    }

    public class CustomerBillPDFDetailsBE
    {
        public string GlobalAccountNo { get; set; }
        public string BillNo { get; set; }
        public string CreatedBy { get; set; }
        public string FilePath { get; set; }
        public string EmailId { get; set; }
        public int BillMonth { get; set; }
        public int BillYear { get; set; }
        public int CustomerBillId { get; set; }
    }

    public class CustomerBillDetailsBE
    {
        public string AccountNo { get; set; }
        public string GlobalAccountNo { get; set; }
        public string OldAccountNo { get; set; }        
        public string Name { get; set; }
        public string Phase { get; set; }
        public string BillingAddress { get; set; }
        public string ServiceAddress { get; set; }
        public string PhoneNo { get; set; }
        public string EmailId { get; set; }
        public string MeterNo { get; set; }
        public string BusinessUnit { get; set; }
        public string ServiceUnit { get; set; }
        public string ServiceCenter { get; set; }
        public string BookGroup { get; set; }
        public string BookNo { get; set; }
        public string PoleNo { get; set; }
        public string WalkingSequence { get; set; }
        public string ConnectionType { get; set; }
        public string TariffCategory { get; set; }
        public string TariffType { get; set; }
        public string BillDate { get; set; }
        public string BillType { get; set; }
        public string BillNo { get; set; }
        public string BillRemarks { get; set; }
        public string NetArrears { get; set; }
        public string Payments { get; set; }
        public string Adjustments { get; set; }
        public string BillAmount { get; set; }
        public string NetAmountPayable { get; set; }
        public string DueDate { get; set; }
        public string LastPaidAmount { get; set; }
        public string LastPaidDate { get; set; }
        public string PaidMeterAmount { get; set; }
        public string PaidMeterDeduction { get; set; }
        public string PaidMeterBalance { get; set; }
        public string Consumption { get; set; }
        public string TariffRate { get; set; }
        public string EnergyCharges { get; set; }
        public string BillPeriod { get; set; }
        public string FixedCharges { get; set; }
        public decimal TaxPercentage { get; set; }
        public string TaxAmount { get; set; }
        public string TotalBillAmount { get; set; }
        public bool IsCAPMIMeter { get; set; }
        public string MonthName { get; set; }
        public string BulkEmails { get; set; }
        public int Year { get; set; }
        public int CustomerBillId { get; set; }
    }

    public class CustomerLastConsumptionDetailsBE
    {
        public string BillNo { get; set; }
        public string BillMonth { get; set; }
        public string Consumption { get; set; }
        public string ADC { get; set; }
        public string CurrentDemand { get; set; }
        public string TotalBillPayable { get; set; }
        public string BillingType { get; set; }
    }

    public class CustomerLastReadingsDetailsBE
    {
        public string MeterNo { get; set; }
        public string CurrentReadingDate { get; set; }
        public string CurrentReading { get; set; }
        public string PreviousReadingDate { get; set; }
        public string PreviousReading { get; set; }
        public string Multiplier { get; set; }
        public string Consumption { get; set; }
    }

    public class CustomerAdditionalChargesBE
    {
        public string FixedChargeId { get; set; }
        public string FixedChargeName { get; set; }
        public string FixedChargeAmount { get; set; }
    }
}
