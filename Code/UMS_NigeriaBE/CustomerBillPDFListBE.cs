﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("CustomerBillPDFDetails", IsNullable = true)]
    public class CustomerBillPDFListBE
    {
        [XmlElement("CustomerBillPDF")]
        public List<CustomerBillPDFBE> Items { get; set; }
    }

    [XmlRootAttribute("CustomerBillPDFDetails", IsNullable = true)]
    public class CustomerBillDetailsListBE
    {
        [XmlElement("CustomerBillDetails")]
        public List<CustomerBillDetailsBE> Items { get; set; }
    }

    [XmlRootAttribute("CustomerBillPDFDetails", IsNullable = true)]
    public class CustomerLastConsumptionDetailsListBE
    {
        [XmlElement("CustomerLastConsumptionDetails")]
        public List<CustomerLastConsumptionDetailsBE> Items { get; set; }
    }

    [XmlRootAttribute("CustomerBillPDFDetails", IsNullable = true)]
    public class CustomerLastReadingsDetailsListBE
    {
        [XmlElement("CustomerLastReadingsDetails")]
        public List<CustomerLastReadingsDetailsBE> Items { get; set; }
    }

    [XmlRootAttribute("CustomerBillPDFDetails", IsNullable = true)]
    public class CustomerBillAdditionalChargesListBE
    {
        [XmlElement("CustomerBillAdditionalChargeDetails")]
        public List<CustomerAdditionalChargesBE> Items { get; set; }
    }
}
