﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class CustomerDetailsBe
    {

        #region string Properties

        public string AccountNo { get; set; }

        public string ReciptNO { get; set; }

        public string ReceivedDate { get; set; }

        public string PaymentMode { get; set; }

        public string Name { get; set; }

        public string DocumentNo { get; set; }

        public string ClassName { get; set; }

        public string CustomerType { get; set; }        

        public string BillNo { get; set; }

        public string BusinessUnitName { get; set; }

        public string ServiceUnitName { get; set; }

        public string ServiceCenterName { get; set; }

        public string FeederId { get; set; }

        public string TransformerId { get; set; }

        public string PoleId { get; set; }

        public string InjectionSubStationId { get; set; }

        public string Remarks { get; set; }

        public string ChargeName { get; set; }

        public string ServiceAddress { get; set; }

        public string RouteSequenceNo { get; set; }

        public string OldAccountNo { get; set; }

        public string CustomerAddress { get; set; }

        public string ContactNo { get; set; }

        public string BookCycleId { get; set; }

        public string BookGroup { get; set; }

        public string BookName { get; set; }

        public string Tariff { get; set; }

        public string FromKW { get; set; }

        public string ToKW { get; set; }

        public string MeterNo { get; set; }

        public string SearchValue { get; set; }

        public string MeterMultiplier { get; set; }

        public string ReadTypeIndication { get; set; }

        public string AccNoAndGlobalAccNo { get; set; }
        
        #endregion

        #region int Properties

        public int SNO { get; set; }

        public int ClassID { get; set; }

        public int TotalBillPendings { get; set; }

        public int Decimals { get; set; }

        public int CustomerReadingId { get; set; }

        public int CustomerTypeId { get; set; }

        public int ReadCodeId { get; set; }

        public int ChargeID { get; set; }

        public int Count { get; set; }

        public int BillYear { get; set; }

        public int BillMonth { get; set; }

        public int CustomerBillID { get; set; }

        public int BATID { get; set; }

        public int BAID { get; set; }

        public string CustomerAdditioalCharges { get; set; }

        public string CustomerID { get; set; }

        public string ApprovalStatusId { get; set; }

        public int PageSize { get; set; }

        public int PageNo { get; set; }

        public int TotalRecords { get; set; }

        public int FLAG { get; set; }

        public int BalanceUsage { get; set; }

        public int RowsEffected { get; set; }

        public int ActiveStatusId { get; set; }

        #endregion

        #region bool Properties

        public bool IsSuccess { get; set; }

        public bool IsActiveMonth { get; set; }

        public bool IsAdjested { get; set; }

        public int MeterDials { get; set; }

        #endregion

        #region decimal Properties

        public decimal AmountPaid { get; set; }

        public decimal TotalDueAmount { get; set; }

        public decimal TotalBillAmountWithTax { get; set; }

        public decimal TotalAmount { get; set; }

        public string LastPaymentDate { get; set; }

        public decimal LastPaidAmount { get; set; }

        public string LastBillGeneratedDate { get; set; }

        public string BillingMonthName { get; set; }

        public string ReadType { get; set; }

        public decimal PreviousReading { get; set; }

        public decimal PresentReading { get; set; }

        public decimal Consumption { get; set; }

        public decimal FixedCharges { get; set; }

        public decimal AdditionalCharges { get; set; }

        public decimal NetEnergyCharges { get; set; }

        public decimal NetFixedCharges { get; set; }

        public decimal NetArrears { get; set; }

        public decimal EnergyClassCharge { get; set; }

        public decimal Amount { get; set; }

        public decimal TotalBillAmount { get; set; }

        public decimal TotalBillAmountWithArrears { get; set; }

        public decimal Vat { get; set; }

        public decimal TotaPayments { get; set; }

        public decimal DueBill { get; set; }

        public decimal TaxPercent { get; set; }

        public decimal GrandTotal { get; set; }

        public decimal OutStandingAmount { get; set; }


        #endregion
    }
}
