﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    //[XmlRootAttribute("CustomerDetailsInfoByXml", IsNullable = true)]
    //public class CustomerDetailsListBe
    //{
    //    [XmlElement("Customerdetails")]
    //    public List<CustomerDetailsBe> items { get; set; }

    //}
     
    [XmlRootAttribute("CustomerDetailsInfoByXml", IsNullable = true)]
    public class CustomerDetailsBillsListBe
    {
        [XmlElement("CustomerBillDetails")]
        public List<CustomerDetailsBe> items { get; set; }

    }

    [XmlRootAttribute("CustomerDetailsInfoByXml", IsNullable = true)]
    public class CustomerDetailsListBe
    {
        [XmlElement("Customerdetails")]
        public List<CustomerDetailsBe> items { get; set; }
    }
}
