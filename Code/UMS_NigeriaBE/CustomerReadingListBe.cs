﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{

    [XmlRootAttribute("CustomerReadingListBeByXml")]
    public class CustomerReadingListBe
    {
        [XmlElement("BillingBE")]
        public List<BillingBE> items { get; set; }

    }
    [XmlRootAttribute("CustomerReadingListBeByXml")]
    public class CustomerReadingListBeDetails
    {
        [XmlElement("BillingListBE")]
        public List<BillingBE> items { get; set; }

    }
}
