﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class CustomerRegistrationBE
    {
        #region String

        public string ApplicationDate { get; set; }
        public string ConnectionDate { get; set; }
        public string SetupDate { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string GolbalAccountNumber { get; set; }       
        public string GlobalAccNoAndAccNo { get; set; }        
        public string TitleTanent { get; set; }
        public string TitleLandlord { get; set; }
        public string AreaService { get; set; }
        public string FirstNameTanent { get; set; }
        public string FirstNameLandlord { get; set; }
        public string MiddleNameTanent { get; set; }
        public string MiddleNameLandlord { get; set; }
        public string LastNameTanent { get; set; }
        public string LastNameLandlord { get; set; }
        public string EmployeeName { get; set; }
        public string InstalledBy { get; set; }
        public string AgencyId { get; set; }
        public string KnownAs { get; set; }
        public string HomeContactNumberLandlord { get; set; }
        public string BusinessContactNumber { get; set; }
        public string OldAccountNo { get; set; }
        public string OrganizationCode { get; set; }
        public string ConnectionReasonId { get; set; }
        public string PZipCode { get; set; }
        public string SZipCode { get; set; }
        public string Landmark { get; set; }
        public string Details { get; set; }
        public string BookCode { get; set; }
        public string PoleID { get; set; }
        public string AccountNo { get; set; }
        public string DocumentNo { get; set; }
        public string CreatedBy { get; set; }
        public string ModifedBy { get; set; }
        public string Status { get; set; }
        public string MeterNumber { get; set; }
        public string EmbassyCode { get; set; }
        public string HouseNoPostal { get; set; }
        public string HouseNoService { get; set; }
        public string StreetPostal { get; set; }
        public string StreetService { get; set; }
        public string CityPostaL { get; set; }
        public string CityService { get; set; }
        public string BusinessUnit { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string CertifiedBy { get; set; }
        public string Seal1 { get; set; }
        public string Seal2 { get; set; }
        public string ApplicationProcessedBy { get; set; }
        public string EmailIdTanent { get; set; }
        public string EmailIdLandlord { get; set; }
        public string UploadDocumentID { get; set; }
        public string EmailID { get; set; }
        public string ContactName { get; set; }
        public string PhoneNumberTanent { get; set; }
        public string PhoneNumberLandlord { get; set; }
        public string AlternatePhoneNumberTanent { get; set; }
        public string AlternatePhoneNumberLandlord { get; set; }
        public string BusinessPhoneNumberLandlord { get; set; }
        public string OtherPhoneNumberLandlord { get; set; }
        public string StatusText { get; set; }
        public string BookNo { get; set; }
        public string ZipCodePostal { get; set; }
        public string ZipCodeService { get; set; }
        public string AreaPostal { get; set; }
        public string IdentityTypeIdList { get; set; }
        public string IdentityNumberList { get; set; }
        public string DocumentName { get; set; }
        public string Path { get; set; }
        public string UDFTypeIdList { get; set; }
        public string UDFValueList { get; set; }
        public string BU_ID { get; set; }
        public string SU_ID { get; set; }
        public string SC_ID { get; set; }
        public string CycleId { get; set; }
        public string StateCode { get; set; }
        public string Reason { get; set; }
        public string Tariff { get; set; }
        public string FullServiceAddress { get; set; }
        public string RouteName { get; set; }
        public string Name { get; set; }
        public string GlobalAccountNumber { get; set; }
        public string ServiceUnitName { get; set; }
        public string ServiceCenterName { get; set; }
        public string CycleName { get; set; }
        public string CustomerType { get; set; }
        public string TitleLandLord { get; set; }
        public string AccountType { get; set; }
        public string ClassName { get; set; }
        public string ClusterCategoryName { get; set; }
        public string Phase { get; set; }
        public string ReadType { get; set; }
        public string EmployeeNameProcessBy { get; set; }
        public string AgencyName { get; set; }
        public string MeterAssignedDate { get; set; }
        public string ExistingBook { get; set; }
        public string LastReadingDate { get; set; }
        public string LastMeterReading { get; set; }
        #endregion

        #region int
        public int ID { get; set; }
        public int ServiceAddressID { get; set; }
        public int PostalAddressID { get; set; }
        public int InitialBillingKWh { get; set; }
        public int InitialReading { get; set; }
        public int PresentReading { get; set; }
        public int ActiveStatusId { get; set; }
        public int EmployeeCode { get; set; }
        public int IdentityTypeId { get; set; }
        public int IdentityId { get; set; }
        public int IdentityNumber { get; set; }
        public int AccountTypeId { get; set; }
        public int CustomerTypeId { get; set; }
        public int TariffClassID { get; set; }
        public int RouteNumber { get; set; }
        public int RouteSequenceNumber { get; set; }
        public int PhaseId { get; set; }
        public int ReadCodeID { get; set; }
        public int SortOrder { get; set; }
        public int AddressID { get; set; }
        public int TenentId { get; set; }
        public int Bedcinfoid { get; set; }
        public int Bedconfid { get; set; }
        public int StepsCount { get; set; }
        public int ClusterCategoryId { get; set; }
        public int MGActTypeID { get; set; }
        public int RowsEffected { get; set; }
        public int ApprovalStatusId { get; set; }
        public int FunctionId { get; set; }
        public int MeterDials { get; set; }

        public int AverageReading { get; set; }//Faiz-ID103
        #endregion

        #region decimal
        public decimal MeterAmount { get; set; }
        public decimal OutStandingAmount { get; set; }
        public decimal AverageConsumption { get; set; }
        public decimal CapmiOutstandingAmount { get; set; }

        #endregion

        #region bool
        public bool IsSameAsService { get; set; }
        public bool IsSameAsTenent { get; set; }
        public bool IsBEDCEmployee { get; set; }
        public bool IsVIPCustomer { get; set; }
        public bool IsEmbassyCustomer { get; set; }
        public bool IsBookNoChanged { get; set; }
        public bool IsServiceAddress { get; set; }
        public bool IsCAPMI { get; set; }
        public bool IsSuccessful { get; set; }
        public bool IsCommunicationPostal { get; set; }
        public bool IsCommunicationService { get; set; }
        public bool IsCommunication { get; set; }
        public bool IsApprovalInProcess { get; set; }
        public bool IsMeterChangeApprovalInProcess { get; set; }        
        public bool IsSuccess { get; set; }
        public bool IsFinalApproval { get; set; }
        public bool IsMeterAlreadyAssigned { get; set; }
        public bool IsMeterReqByAnotherCust { get; set; }
        public bool IsNotCreditMeter { get; set; }

        #endregion

        
    }
}
