﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{

    [XmlRootAttribute("CustomerRegistrationInfoByXml", IsNullable = true)]
    public class CustomerRegistrationList_1BE
    {
        [XmlElement("CustomerRegistrationList_1BE")]
        public List<CustomerRegistrationBE> items { get; set; }
    }

    [XmlRootAttribute("CustomerRegistrationInfoByXml", IsNullable = true)]
    public class CustomerRegistrationList_2BE
    {
        [XmlElement("CustomerRegistrationList_2BE")]
        public List<CustomerRegistrationBE> items { get; set; }
    }
}
