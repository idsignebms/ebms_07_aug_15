﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class DirectCustomerAverageUploadBE
    {
        public string BUID { get; set; }
        public string AccountNo { get; set; }
        public string GlobalAccountNumber { get; set; }
        public string AccNoAndGlobalAccNo { get; set; }
        public string OldAccountNo { get; set; }
        public string Name { get; set; }
        public string ServiceAddress { get; set; }
        public string Tariff { get; set; }


        public bool IsSuccess { get; set; }
        public bool IsHoldOrClosed { get; set; }
        public bool IsAccountNoNotExists { get; set; }
        public bool IsReadCustomer { get; set; }
        public bool IsCustExistsInOtherBU { get; set; }


        public decimal AverageReading { get; set; }
        public string ModifiedBy { get; set; }

        public int ApprovalStatusId { get; set; }
        public int FunctionId { get; set; }
        public bool IsFinalApproval { get; set; }
        public string Details { get; set; }
        public bool IsApprovalInProcess { get; set; }

        public string ClusterType { get; set; }
        public string CustomerStatus { get; set; }

        public decimal OutStandingAmount { get; set; }
        public string BU_ID { get; set; }
    }
}
