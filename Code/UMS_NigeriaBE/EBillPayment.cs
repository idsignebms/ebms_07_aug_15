﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class EBillPayment
    {
        #region String Properties

        public string Trnid                     { get; set; }
        public string Product                   { get; set; }
        public string AcNo                      { get; set; }
        public string TrnDate                   { get; set; }
        public string TrnTime                   { get; set; }
        public string SourceBank                { get; set; }
        public string Status                    { get; set; }
        public string Ind                       { get; set; }
        public string BU_TxtFile                { get; set; }
        public string BU_XlsFile                { get; set; }
        public string ConsolidatedFile          { get; set; }
        public string ConsolidatedDuplicateFile { get; set; }
        public string CreatedDate               { get; set; }
        public string TO                        { get; set; }
        public string CC                        { get; set; }
        public string BCC                       { get; set; }

        #endregion

        #region int Properties

        public int AVR_DISTRICT_SEQ             { get; set; }
        public int EBillMailLogsId              { get; set; }
        public int BUEBMID                      { get; set; }
        public int RowsEffected                 { get; set; }
        #endregion

        #region decimal Properties

        public decimal TrnAmount                { get; set; }
        public decimal TransactionFee           { get; set; }

        #endregion

        #region bool Properties
        public bool IsMailSent                  { get; set; }
        #endregion
    }
}
