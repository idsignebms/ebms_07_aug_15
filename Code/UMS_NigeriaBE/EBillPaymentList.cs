﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRoot("EBillPaymentListXml", IsNullable = true)]
    public class EBillPaymentList
    {
        [XmlElement("EBillPayment")]
        public List<EBillPayment> items { get; set; }
    }
}
