﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class EmployeeBE
    {
        public int ActiveStatusId { get; set; }
        public int BEDCEmpId { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalRecords { get; set; }
        public int RowNumber { get; set; }
        

        public string EmployeeName { get; set; }
        public int DesignationId { get; set; }
        public string Designation { get; set; }
        public string Location { get; set; }
        public string EmployeCode { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public string AnotherContactNo { get; set; }
        public string ModifiedBy { get; set; }
        public string CreatedBy { get; set; }

        public bool IsSuccess { get; set; }
    }
}
