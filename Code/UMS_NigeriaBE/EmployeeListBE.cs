﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("EmployeeListBEInfoByXml", IsNullable = true)]
    public class EmployeeListBE
    {
        [XmlElement("EmployeeBE")]
        public List<EmployeeBE> Items { get; set; }
    }
}
