﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class ErrorConstants
    {
        public const string btnSave_Click = "btnSave_Click";
        public const string Page_PreInit = "Page_PreInit";
        public const string Page_Load = "Page_Load";
        public const string Insert = "Insert";
        public const string btnActiveOk_Click = "btnActiveOk_Click";
        public const string Update = "Update";
        public const string RowDataBound = "RowDataBound";
        public const string RowCommand = "RowCommand";
        public const string Message = "Message";
        public const string BindGrid = "BindGrid";
        public const string InitializeCulture = "InitializeCulture";
    }
}
