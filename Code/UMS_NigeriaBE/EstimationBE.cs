﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class EstimationBE
    {
        public int EnergyToCalculate { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public string Cycle { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public int EstimatedCustomers { get; set; }
        public int AvgUsage { get; set; }
        public int AvgReadingPerCust { get; set; }
        public int BillingRule { get; set; }
        public bool IsExists { get; set; }
        public int MonthlyUsage { get; set; }
        public int Total { get; set; }
        public bool IsSuccess { get; set; }

        public int TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int RowNumber { get; set; }
        public int EstimationSettingId { get; set; }
        public int RowsAffected { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string CycleId { get; set; }
        public string CycleName { get; set; }
        public int TotalMetersUsage { get; set; }
        public string ServiceCenterId { get; set; }
        public int TotalReadCustomers { get; set; }
        public int TotalNonReadCustomers { get; set; }
        public decimal TotalMinimumUsage { get; set; }

        public string CategoryName { get; set; }
        public int ClusterCategoryId { get; set; }

        public string MonthName { get; set; }

        public int TotalReadCustomersHavingReadings { get; set; }
        public int TotalNonReadCustomersCount { get; set; }
        public int TotalUplodedCustomersCount { get; set; }
        public int TotalNonUploadedCustomersCount { get; set; }

        public int TotalReadCustomersCount { get; set; }
        public int TotalDirectCustomers { get; set; }

        public int TotalInActiveCustomersCount { get; set; }

        public Int64 TotalUsageForReadCustomersHavingReadings { get; set; }
        public Int64 TotalNonReadCustomersUsage { get; set; }
        public Int64 TotalUsageForUploadedCustomers { get; set; }
        public Int64 TotalNonUploadedCustomersUsage { get; set; }

        public Int64 TotalUsageForReadCustomers { get; set; }
        public Int64 TotalDirectCustomersUsage { get; set; }

        public decimal CapacityInKVA { get; set; }
        public decimal SUPPMConsumption { get; set; }
        public decimal SUCreditConsumption { get; set; }        
    }
}
