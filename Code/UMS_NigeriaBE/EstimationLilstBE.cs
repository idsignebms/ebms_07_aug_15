﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("EstimationInfoByXml", IsNullable = true)]
    public class EstimationLilstBE
    {
         [XmlElement("EstimationDetails")]
        public List<EstimationBE> items { get; set; }
    }

    [XmlRootAttribute("EstimationInfoByXml", IsNullable = true)]
    public class EstimationDetailsLilstBE
    {
        [XmlElement("EstimatedUsageDetails")]
        public List<EstimationBE> items { get; set; }
    }
}
