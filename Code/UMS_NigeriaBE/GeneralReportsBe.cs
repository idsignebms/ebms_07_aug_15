﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class GeneralReportsBe
    {
    }

    public class LedgerBe
    {
        public string GlobalAccountNo { get; set; }
        public string BU_ID { get; set; }
        public int ReportMonths { get; set; }        
    }

    public class RptDebitBe
    {
        public string BU_ID { get; set; }
        public string SU_ID { get; set; }
        public string SC_ID { get; set; }
        public string CycleId { get; set; }
        public string BookNo { get; set; }
        public string MinAmt { get; set; }
        public string MaxAmt { get; set; }

        public int PageSize { get; set; }
        public int PageNo { get; set; }
    }

    public class RptCreditBe
    {
        public string BU_ID { get; set; }
        public string SU_ID { get; set; }
        public string SC_ID { get; set; }
        public string CycleId { get; set; }
        public string BookNo { get; set; }
        public string MinAmt { get; set; }
        public string MaxAmt { get; set; }

        public int PageSize { get; set; }
        public int PageNo { get; set; }
    }

    public class RptPaymentsBe
    {
        public string BU_ID { get; set; }
        public string CycleId { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string ReceivedDeviceId { get; set; }

        public int PageSize { get; set; }
        public int PageNo { get; set; }        
    }

    public class RptDirectCustomersBe
    {
        public string BU_ID { get; set; }
        public string SU_ID { get; set; }
        public string SC_ID { get; set; }
        public string Tariff { get; set; }

        public int PageSize { get; set; }
        public int PageNo { get; set; }   
    }

    public class RptReadCustomersBe
    {
        public string BU_ID { get; set; }
        public string SU_ID { get; set; }
        public string SC_ID { get; set; }
        public string Tariff { get; set; }

        public int PageSize { get; set; }
        public int PageNo { get; set; }
    }

    public class RptNoNameOrAddressBe
    {
        public string BU_ID { get; set; }
        public string SU_ID { get; set; }
        public string SC_ID { get; set; }
        public string CycleId { get; set; }
        public string BookNo { get; set; }

        public int PageSize { get; set; }
        public int PageNo { get; set; }
    }

    public class RptCustomerBillsBe
    {
        public string BU_ID { get; set; }
        public string SU_ID { get; set; }
        public string SC_ID { get; set; }
        public string CycleId { get; set; }
        public string BookNo { get; set; }
        public string TariffIds { get; set; }
        public string Months { get; set; }
        public string Years { get; set; }
        public int BillProcessTypeId { get; set; }
        public int PageSize { get; set; }
        public int PageNo { get; set; }
        
    }

    public class RptUsageConsumedBe
    {
        public string BU_ID { get; set; }
        public string SU_ID { get; set; }
        public string SC_ID { get; set; }
        public string CycleId { get; set; }
        public string BookNo { get; set; }

        public int PageSize { get; set; }
        public int PageNo { get; set; }
        public int MonthId { get; set; }
        public int YearId { get; set; }
    }

    public class RptNoUsageConsumedBe
    {
        public string BU_ID { get; set; }
        public string SU_ID { get; set; }
        public string SC_ID { get; set; }
        public string CycleId { get; set; }
        public string BookNo { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        

        public int PageSize { get; set; }
        public int PageNo { get; set; }
        public int YearId { get; set; }
        public int MonthId { get; set; }

    }
}
