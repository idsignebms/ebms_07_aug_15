﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class GlobalMessages
    {
        #region Members
        private int _globalmessageId;
        private string _message;
        private string _stateId;
        private int _billingtypeId;
        private int _pageNo;
        private int _pageSize;
        private int _activeStatus;
        private string _createdBy;
        private DateTime _createdDate;
        private string _modifiedBy;
        private DateTime _modifiedDate;
        private int _DistrictId;
        private string _DistrictCode;
        private int _isGlobalMSG;
        private int _totalRecords;
        #endregion

        #region Parameters
        public int GlobalMessageId
        {
            get { return _globalmessageId; }
            set { _globalmessageId = value; }
        }
        public int PageNo
        {
            get { return _pageNo; }
            set { _pageNo = value; }
        }
        public int TotalRecords
        {
            get { return _totalRecords; }
            set { _totalRecords = value; }
        }
        public int PageSize
        {
            get { return _pageSize; }
            set { _pageSize = value; }
        }
        public int IsGlobalMSG
        {
            get { return _isGlobalMSG; }
            set { _isGlobalMSG = value; }
        }
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }
        public string StateId
        {
            get { return _stateId; }
            set { _stateId = value; }
        }
        public string DistrictCode
        {
            get { return _DistrictCode; }
            set { _DistrictCode = value; }
        }
        public int BillingTypeId
        {
            get { return _billingtypeId; }
            set { _billingtypeId = value; }
        }
        public int ActiveStatus
        {
            get { return _activeStatus; }
            set { _activeStatus = value; }
        }
        public string CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set { _createdDate = value; }
        }
        public string ModifiedBy
        {
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _modifiedDate; }
            set { _modifiedDate = value; }
        }
        public int DistrictId
        {
            get { return _DistrictId; }
            set { _DistrictId = value; }
        }
        public string BU_ID { get; set; }
        public string BusinessUnitName { get; set; }        
        public string DistrictMessageId { get; set; }
        public bool IsSuccess { get; set; }
        public string BillingType { get; set; }
        public int RowNumber { get; set; }
        public int ActiveStatusId { get; set; }
        #endregion
       
    }
}
