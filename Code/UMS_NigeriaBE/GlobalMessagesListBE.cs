﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("GlobalMessageBEInfoByXml", IsNullable = true)]
   public class GlobalMessagesListBE
    {
       
       
           [XmlElement("GlobalMessages")]
           public List<GlobalMessages> Items { get; set; }

           [XmlElement("BillingBE")]
           public List<BillingBE> CustomerReading{ get; set; }
       
    }
}
