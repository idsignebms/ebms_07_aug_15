﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class GovtAccountTypeBE
    {
        public int GActTypeID { get; set; }
        public string AccountType { get; set; }
        public int ActiveStatusId { get; set; }
        public string Details { get; set; }
        public int RefActTypeID { get; set; }

        public string RefActTypeIDString { get; set; }
        public int Levels { get; set; }

        public string AccountCode { get; set; }

        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }

        public int RowsEffected { get; set; }

        public bool IsShiftCustomer { get; set; }
    }
}
