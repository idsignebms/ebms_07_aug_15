﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{

        [XmlRoot("GovtAccountTypeBEInfoByXml", IsNullable = true)]
        public class GovtAccountTypeListBE
        {
            [XmlElement("GovtAccountTypeBE")]
            public List<GovtAccountTypeBE> Items { get; set; }
        }
    
}
