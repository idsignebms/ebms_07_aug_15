﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class IsTamperBe
    {
        public string GlobalAccountNumber { get; set; }
        public string MeterNo { get; set; }
        public string ReadingDate { get; set; }
        public string Reason { get; set; }
        public string CreatedBy { get; set; }

        public bool IsSuccess { get; set; }
        public bool IsExists { get; set; }
        

        public int MeterReaderId { get; set; }

    }
}
