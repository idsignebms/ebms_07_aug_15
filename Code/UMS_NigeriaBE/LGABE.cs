﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class LGABE
    {
        public int Count { get; set; }

        public int RowNumber { get; set; }

        public int LGAId { get; set; }
        public string LGAName { get; set; }
        public string BU_ID { get; set; }
        public string BusinessUnitName { get; set; }

        public int RowsEffected { get; set; }
        public int ActiveStatusId { get; set; }

        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }

        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalRecords { get; set; }

        public int AssciatedStateCount { get; set; }
    }
}
