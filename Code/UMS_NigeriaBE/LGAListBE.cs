﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("LGABeInfoByXml", IsNullable = true)]
    public class LGAListBE
    {
        [XmlElement("LGABE")]
        public List<LGABE> items { get; set; }
    }
}
