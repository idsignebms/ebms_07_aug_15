﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class LoginBE
    {
        public string LoginId { get; set; }

        public string Password { get; set; }

        public int RoleId { get; set; }

        public int Status { get; set; }

        public string CountryCode { get; set; }

        public string CountryName { get; set; }

        public bool IsFirstLogin { get; set; }

        public int Count { get; set; }

        public string UserName { get; set; }

        public string LastLoginTime { get; set; }

        public string Currency { get; set; }

        public bool IsBaseCurrency { get; set; }

        public int PageNo { get; set; }

        public int PageSize { get; set; }

        public int TotalRecords { get; set; }

        public string OldCountryCode { get; set; }

        public bool IsExists { get; set; }

        public bool IsAccessAccountReviewer { get; set; }

        public bool IsEffect { get; set; }

        public bool IsNew { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        public string HelpDetails { get; set; }

        public int EffectedRows { get; set; }

        public int SortOrder { get; set; }

        public int RowNumber { get; set; }

        public string Orders { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public string Duration { get; set; }

        public string Comments { get; set; }

        public int LogId { get; set; }

        public string RefreshTime { get; set; }

        public bool IsEnableApproveDT { get; set; }
    }
}
