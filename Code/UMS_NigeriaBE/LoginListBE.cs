﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
     [XmlRootAttribute("LoginInfoByXml", IsNullable = true)]
    public class LoginListBE
    {
         [XmlElement("LoginList")]
         public List<LoginBE> Items { get; set; }
    } 
   
}
