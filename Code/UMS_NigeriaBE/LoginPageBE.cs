﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class LoginPageBE
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsSuccess { get; set; }
        public int RoleId { get; set; }
        public string UserId { get; set; }
        public string Designation { get; set; }
        public string PrimaryEmailId { get; set; }
        public bool IsDisabled { get; set; }
        public bool IsExists { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsLogin { get; set; }
    }
}
