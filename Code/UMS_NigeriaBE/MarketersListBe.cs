﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("MarketersBeInfoByXml", IsNullable = true)]
    public class MarketersListBe
    {
        [XmlElement("MarketersBe")]
        public List<MarketersBe> Items { get; set; }
    }
}
