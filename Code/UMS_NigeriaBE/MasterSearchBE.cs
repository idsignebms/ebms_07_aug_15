﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class MasterSearchBE
    {
        #region Int

        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int RowNumber { get; set; }
        public int TotalRecords { get; set; }
        public int Flag { get; set; }
        public int RoleId { get; set; }
        public int DesignationId { get; set; }
        
        #endregion

        #region string

        public string BookNo { get; set; }
        public string BookCode { get; set; }
        public string BusinessUnit { get; set; }
        public string BU_ID { get; set; }
        public string BusinessUnitCode { get; set; }
        public string StateName { get; set; }
        public string CycleNo { get; set; }
        public string CycleName { get; set; }
        public string CycleId { get; set; }
        public string CycleCode { get; set; }
        public string MeterNo { get; set; }
        public string MeterSerialNo { get; set; }
        public string MeterType { get; set; }
        public string MeterSize { get; set; }
        public string MeterBrand { get; set; }
        public string MeterRating { get; set; }
        public string NextCalibrationDate { get; set; }
        public string ServiceHoursBeforeCalibration { get; set; }
        public string MeterMultiplier { get; set; }
        public string MeterDials { get; set; }        
        public string Brand { get; set; }
        public string RouteCode { get; set; }
        public string RouteName { get; set; }
        public string ServiceCenter { get; set; }
        public string ServiceCenterCode { get; set; }
        public string ServiceUnit { get; set; }
        public string ServiceUnitCode { get; set; }
        public string ContactName { get; set; }
        public string ContactNo { get; set; }
        public string Name { get; set; }
        public string EmployeeId { get; set; }
        public string SurName { get; set; }
        public string PrimaryEmailId { get; set; }
        public string Designation { get; set; }
        public string Status { get; set; }

        #endregion

        #region double
        #endregion
    }
}
