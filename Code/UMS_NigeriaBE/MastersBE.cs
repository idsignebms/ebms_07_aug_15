﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class MastersBE
    {
        public int Count { set; get; }

        //Countries Data
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public bool IsCountryNameExists { get; set; }
        public bool IsCountryCodeExists { get; set; }

        //States Data
        public string StateName { get; set; }
        public string StateCode { get; set; }
        public string DisplayCode { get; set; }
        public bool IsDisplayCodeExists { get; set; }
        public bool IsStateCodeExists { get; set; }
        public bool IsStateNameExists { get; set; }
        //Districts Data
        public string DistrictName { get; set; }
        public string DistrictCode { get; set; }
        public bool IsDistrictNameExists { get; set; }
        public bool IsDistrictCodeExists { get; set; }
        public string DistrictState { get; set; }

        public string Notes { get; set; }

        public bool IsSuccess { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public int TotalRecords { get; set; }
        public int ApprovalStatusId { get; set; }
        public int FunctionId { get; set; }
        public int PaymentFromId { get; set; }
        public bool IsApprovalInProcess { get; set; }
        public bool IsFinalApproval { get; set; }
        
        // For Paging
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int RowNumber { get; set; }
        public bool IsActive { get; set; }

        public int startIndex { get; set; }
        public int endIndex { get; set; }

        

        //Business Units
        public string BusinessUnitName { get; set; }
        public string BU_ID { get; set; }
        public int ActiveStatusId { get; set; }

        //ServiceUnits
        public string SU_ID { get; set; }
        public string ServiceUnitName { get; set; }

        //ServiceCenters
        public string ServiceCenterId { get; set; }
        public string ServiceCenterName { get; set; }

        //SubStation
        public string SubStationId { get; set; }
        public string SubStationName { get; set; }
        //Feeder
        public string FeederId { get; set; }
        public string FeederName { get; set; }
        //TransFormer
        public string TransFormerId { get; set; }
        public string TransFormerName { get; set; }
        //Poles
        public string PoleId { get; set; }
        public string PoleName { get; set; }
        //Cycle
        public string CycleName { get; set; }
        public int CycleNo { get; set; }
        public string Details { get; set; }
        public string ContactName { get; set; }
        public string ContactNo { get; set; }
        public bool IsCycleExists { get; set; }
        public bool IsCycleNoExists { get; set; }
        public string CycleId { get; set; }
        //Agency
        public string AgencyName { get; set; }
        public string ContactNo2 { get; set; }
        public bool IsAgencyNameExists { get; set; }
        public int AgencyId { get; set; }
        //BookNumber
        public string BookNo { get; set; }
        public int NoOfAccounts { get; set; }
        public bool IsBookNumberExists { get; set; }
        public string BookName { get; set; }//Faiz - ID103(18-02-2015)

        //MeterTypes
        public string MeterType { get; set; }
        public string MeterTypeId { get; set; }
        public bool IsMeterTypeExists { get; set; }
        public int ActiveStatus { get; set; }
        public bool IsMaster { get; set; }//Faiz - ID103(29-01-2015)

        public string UserId { get; set; }
        //MessageTypes
        public int MessageId { get; set; }
        public string MessageType { get; set; }
        //RouteMangement
        public string RouteName { get; set; }
        public bool IsRouteNameExists { get; set; }
        public decimal AvgMaxLimit { get; set; }
        public int RouteId { get; set; }
        //Payment Entry
        public string AccountNo { get; set; }
        public decimal PaidAmount { get; set; }
        public string ReceiptNo { get; set; } // change to string 27-09-14(ID-065)
        //public int ReceiptNo { get; set; }
        public int PaymentModeId { get; set; }
        public string PaymentMode { get; set; }
        public string BillNo { get; set; }
        public bool IsDefault { get; set; }
        public string Name { get; set; }
        public string ClassName { get; set; }
        public decimal BatchPendingAmount { get; set; }
        // public string BatchNo { get; set; }

        public string Document { get; set; }
        public string DocumentPath { get; set; }
        public int BatchNo { get; set; }
        public int BatchId { get; set; }
        public bool IsCustomerExists { get; set; }
        public bool IsPaymentEntry { get; set; }
        public string PaymentRecievedDate { get; set; }
        public decimal BillTotal { get; set; }
        public decimal DueAmount { get; set; }
        public bool IsMorePaidAmount { get; set; }
        public string BillDate { get; set; }
        //CashOffices
        public int CashierId { get; set; }
        public int CashOfficeId { get; set; }
        public string CashOffice { get; set; }
        public bool IsCashOfficeExists { get; set; }
        //MeterInformation
        public string MeterNo { get; set; }
        public int MeterId { get; set; }
        public string MeterSize { get; set; }
        public string MeterMultiplier { get; set; }
        public string MeterBrand { get; set; }
        public int MeterStatus { get; set; }
        public string MeterSerialNo { get; set; }
        public string MeterRating { get; set; }
        public string NextCalibrationDate { get; set; }
        public string ServiceHoursBeforeCalibration { get; set; }
        public int MeterDials { get; set; }
        public int Decimals { get; set; }
        public bool IsMeterNoExists { get; set; }
        public bool IsMeterSerialNoExists { get; set; }
        public int EffectedRows { get; set; }
        public int CustomerPaymentID { get; set; }
        public int CustomerBPID { get; set; }
        public string PageName { get; set; }
        public bool IsPageAccess { get; set; }
        public bool Status { get; set; }
        public int Flag { get; set; }
        public string BillMonth { get; set; }
        public string BillYear { get; set; }
        public int BillOpenStatusID { get; set; }
        public string FeatureName { get; set; }
        public string PENDING_KEY { get; set; }
        public string Feature { get; set; }
        public DateTime TrnDate { get; set; }
        public decimal TrnAmount { get; set; }
        public string AVR_DISTRICT_DESC { get; set; }
        public decimal BatchTotal { get; set; }
        public string TrnTime { get; set; }
        public string TrnId { get; set; }
        public string Product { get; set; }
        public string SourceBank { get; set; }
        public string DistName { get; set; }
        public string DistCode { get; set; }
        public string Acno { get; set; }
        public string PayStatus { get; set; }
        public bool IsExists { get; set; }
        public string Key { get; set; }
        public bool IsCAPMIMeter { get; set; }
        public string IsCAPMIMeterString { get; set; }
        public decimal CAPMIAmount { get; set; }

        public string BUCode { get; set; }
        public bool IsBUCodeExists { get; set; }
        public string SUCode { get; set; }
        public bool IsSUCodeExists { get; set; }
        public string SCCode { get; set; }
        public bool IsSCCodeExists { get; set; }
        public string CycleCode { get; set; }
        public bool IsCycleCodeExists { get; set; }
        public string BookCode { get; set; }
        public bool IsBookCodeExists { get; set; }
        public string RouteCode { get; set; }
        public bool IsRouteCodeExists { get; set; }
        public string Remarks { get; set; }
        public int TotalBillPending { get; set; }
        public int MarketerId { get; set; }
        public string Marketer { get; set; }
        public string IsBillInProcess { get; set; }
        public string PaymentReceiptNo { get; set; }

        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string ZIP { get; set; }
        public string Currency { get; set; }
        public string CurrencySymbol { get; set; }

        public int LGAId { get; set; }
        public string LGAName { get; set; }

        public int RegionId { get; set; }
        public string RegionName { get; set; }

        //Faiz - ID103 29-Jan-2015
        //AccountType
        public int AccountTypeId { get; set; }
        public string AccountType { get; set; }
        public string AccountCode { get; set; }
        public bool IsAccountTypeExists { get; set; }
        public bool IsAccountCodeExists { get; set; }

        //Faiz - ID103 29-Jan-2015
        //CustomerType
        public int CustomerTypeId { get; set; }
        public string CustomerType { get; set; }
        public bool IsCustomerTypeExists { get; set; }
        public int BillGenCount { get; set; }
        public int PaymentCount { get; set; }
        public int BillAdjustmentCount { get; set; }
        public int TotalCustomers { get; set; }
        public int PaymentType { get; set; }
        ////MPoleMasterDetails -- Faiz-ID103
        //public string Description { get; set; }
        //public int PoleMasterOrderID { get; set; }
        //public int PoleMasterCodeLength { get; set; }
        //public int RowsEffected { get; set; }

        public bool IsSuccessful { get; set; }
        public string StatusText { get; set; }

        public string OldSeviceCenterID { get; set; }
        public string BNO { get; set; }

        public bool IsMeterChangeExist { get; set; }
        public bool IsReadingsExist { get; set; }
        public bool IsHavingCapmiAmt { get; set; }
        
        public string OldAccountNo { get; set; }

        public string AccNoAndGlobalAccNo { get; set; }


        //Faiz - ID103 30-Jun-2015
        //IdentityType
        public int IdentityId { get; set; }
        public string Type { get; set; }
        public bool IsTypeExists { get; set; }


        //Faiz - ID103 30-Jun-2015
        //Designation
        public int DesignationId { get; set; }
        public string DesignationName { get; set; }
        public bool IsDesignationExists { get; set; }

        //Faiz - ID103 30-Jun-2015
        //Title
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public bool IsTitleExists { get; set; }
    }

    public class BatchEntryBe
    {
        public int BatchNo { get; set; }
        public int BatchId { get; set; }
        public int PaymentModeId { get; set; }
        public string BU_ID { get; set; }
        public string BatchDate { get; set; }
    }
}
