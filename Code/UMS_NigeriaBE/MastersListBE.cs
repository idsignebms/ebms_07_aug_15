﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("MastersBEInfoByXml", IsNullable = true)]
    public class MastersListBE
    {
        [XmlElement("MastersBE")]
        public List<MastersBE> Items { get; set; }
    }
    [XmlRootAttribute("MastersBEInfoByXml", IsNullable = true)]
    public class MastersListBE_MoreInfo
    {
        [XmlElement("MastersBE_MoreInfo")]
        public List<MastersBE> Items { get; set; }
    }
}
