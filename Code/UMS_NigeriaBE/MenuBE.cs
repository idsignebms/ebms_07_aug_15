﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class MenuBE
    {
        public int MenuId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int ReferenceMenuId { get; set; }
        public int Page_Order { get; set; }
        public int Menu_Order { get; set; }

        public string ModifiedBy { get; set; }

        public int RowsEffected { get; set; }
        public bool IsNameExists { get; set; }
    }

    public class EditListReportsBE
    {
        public string ReportCode { get; set; }
        public string ReportName { get; set; }
        public string PagePath { get; set; }
    }
}
