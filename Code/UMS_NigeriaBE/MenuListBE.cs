﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRoot("MenuBEInfoByXml", IsNullable = true)]
    public class MenuListBE
    {
        [XmlElement("MenuBE")]
        public List<MenuBE> Items { get; set; }
    }

    [XmlRootAttribute("EditListReportsBEInfoByXml", IsNullable = true)]
    public class EditListReportsBE_MoreInfo
    {
        [XmlElement("EditListReportsBEList")]
        public List<EditListReportsBE> Items { get; set; }
    }
}
