﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class NewCustomerLocatorBE
    {
        #region string Properties

        public string Name                 { get; set; }

        public string Primary_ContactNo    { get; set; }

        public string Secondary_ContactNo  { get; set; }

        public string CycleID              { get; set; }

        public string MarketerName         { get; set; }

        public string Note                 { get; set; }

        public string Longitude            { get; set; }

        public string Latitude             { get; set; }

        public string CreatedDate          { get; set; }

        public string CreatedBy            { get; set; }

        public string ModifiedDate         { get; set; }

        public string ModifiedBy           { get; set; }

        public string CycleName            { get; set; }

        public string ApprovalStatus       { get; set; }

        public string BUID                 { get; set; }

        #endregion

        #region int Properties

        public int NewCustomerLocatorId    { get; set; }

        public int ApprovalStatusId        { get; set; }

        public int PageNo                  { get; set; }

        public int PageSize                { get; set; }

        public int RowNumber               { get; set; }

        public int RowEffected             { get; set; }

        public int TotalRecords            { get; set; }
        
        #endregion

        #region bool Properties
        #endregion

        #region decimal Properties
        #endregion
    }
}
