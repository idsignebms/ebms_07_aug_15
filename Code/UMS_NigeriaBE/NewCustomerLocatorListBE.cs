﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("NewCustomerLocatorListBEXML")]
    public class NewCustomerLocatorListBE
    {
        [XmlElement("NewCustomerLocatorBE")]
        public List<NewCustomerLocatorBE> Items { get; set; }
    }
}
