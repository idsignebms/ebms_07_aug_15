﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class NewReportsBe
    {
        public string BU_ID { get; set; }   //Input
        public string Tariff { get; set; }
        public string KWHSold { get; set; }
        public string KVASold { get; set; }
        public string WeightedAverage { get; set; }
        public string NoOfStubs { get; set; }

        public decimal FixedCharges{get;set;}
        public decimal TotalAmountBilled{get;set;}
        public decimal TotalAmountCollected{get;set;}

        public int Year { get; set; }   //Input
        public int Month { get; set; }  //Input
        public int SNo { get; set; }
        public int NoBilled { get; set; }
        public int TotalPopulation { get; set; }
    }

    public class PDFReportsBe
    {
        public string BU_ID { get; set; }  
        public int Year { get; set; }  
        public int Month { get; set; } 
    }
}
