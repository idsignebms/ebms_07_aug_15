﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class NotificationsBE
    {
        public int NoticeID { get; set; }
        public string BU_ID { get; set; }//if null then all else id will be seprated with comma
        public string Subject { get; set; }
        public string Details { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int IsSuccess { get; set; }
        public string FilePath { get; set; }
        public string Srno { get; set; }
        public string BusinessUnitName { get; set; }
        public string BUNoticeID { get; set; }
        public bool IsAllBuss { get; set; }
        public int Activestatus { get; set; }
        public string Title { get; set; }
        // For Paging
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int RowNumber { get; set; }
        public bool IsActive { get; set; }
        public int TotalRecords { get; set; }
    }
}
