﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("NotificationsBEXML")]
    public class NotificationsListBE
    {
        [XmlElement("NotificationsBE")]
        public List<NotificationsBE> Items { get; set; }
    }
}
