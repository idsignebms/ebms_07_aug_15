﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class PaidMeterBe
    {
        public string AccountNo { get; set; }
        public string CreatedBy { get; set; }
        public string Details { get; set; }
        public string Name { get; set; }
        public string MeterNo { get; set; }
        public string Tariff { get; set; }
        public string OldAccountNo { get; set; }
        public string AccNoAndGlobalAccNo { get; set; }        
        public decimal MeterCost { get; set; }
        public decimal OutStandingAmount { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal BilladjustmentAmount { get; set; }

        public int ApprovalStatusId { get; set; }
        public int Flag { get; set; }
        public bool IsNotPaidCustomer { get; set; }
        public bool IsSuccess { get; set; }        
    }
}
