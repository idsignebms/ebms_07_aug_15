﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class PasswordStrengthBE
    {
        public int StrengthTypeId { get; set; }
        public string StrengthName { get; set; }
        public string MinLength { get; set; }
        public string MaxLength { get; set; }
        public bool IsActive { get; set; }

        public int RowCount { get; set; }
    }
}
