﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRoot("PasswordStrengthBEInfoByXml", IsNullable = true)]
    public class PasswordStrengthListBE
    {
        [XmlElement("PasswordStrengthBE")]
        public List<PasswordStrengthBE> Items { get; set; }
    }
}
