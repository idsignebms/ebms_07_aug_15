﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class PaymentsBatchBE
    {
        public int PUploadBatchId { get; set; }
        public string BatchNo { get; set; }
        public DateTime BatchDate { get; set; }
        public string StrBatchDate { get; set; }
        public int TotalCustomers { get; set; }
        public string CreatedBy { get; set; }
        public string LastModifyBy { get; set; }
        public string Notes { get; set; }
        public bool IsClosedBatch { get; set; }

        public int PUploadFileId { get; set; }
        public string FilePath { get; set; }
        public int TotalSucessTransactions { get; set; }
        public int TotalFailureTransactions { get; set; }
        public bool IsTrasactionsCompleted { get; set; }

        public int PaymentTransactionId { get; set; }
        public string SNO { get; set; }
        public string AccountNo { get; set; }
        public string AmountPaid { get; set; }
        public string ReceiptNo { get; set; }
        public string ReceivedDate { get; set; }
        public string PaymentMode { get; set; }

        public int PaymentSucessTransactionID { get; set; }
        public string Comments { get; set; }

        public int PaymentFailureransactionID { get; set; }

        public int RowNumber { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalRecords { get; set; }

        public int RowsEffected { get; set; }


        public DateTime CreatedDate { get; set; }
        public string StrCreatedDate { get; set; }

        public string BU_ID { get; set; }
    }
}
