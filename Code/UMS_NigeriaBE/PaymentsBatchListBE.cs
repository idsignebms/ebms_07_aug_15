﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("PaymentsBatchBEInfoByXml", IsNullable = true)]
    public class PaymentsBatchListBE
    {
        [XmlElement("PaymentsBatchBE")]
        public List<PaymentsBatchBE> Items { get; set; }
    }
}
