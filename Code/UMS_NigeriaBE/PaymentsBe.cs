﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class PaymentsBe
    {
        public string AccountNo { get; set; }
        public string GlobalAccountNumber { get; set; }
        public string AccNoAndGlobalAccNo { get; set; }
        public decimal PaidAmount { get; set; }
        public int EffectedRows { get; set; }
        public int CustomerPaymentID { get; set; }
        public string CreatedBy { get; set; }
        public string BU_Id { get; set; }
        public int RowNumber { get; set; }
        public string ReceiptNO { get; set; }
        public DateTime ReceivedDate { get; set; }
        public bool IsDuplicate { get; set; }
        public bool IsExists { get; set; }
        public string PaymentMode { get; set; }
        public int PaymentModeId { get; set; }
        public string IsBillInProcess { get; set; }
        public int TotalPendingBills { get; set; }
        public int SNO { get; set; }
        public string DocumentPath { get; set; }
        public string DocumentName { get; set; }
        public int BatchNo { get; set; }
        public int Flag { get; set; }
        public string OldAccountNo { get; set; }
        public string PaymentDate { get; set; }
        public string Name { get; set; }
        public string MeterNumber { get; set; }
        public string ClassName { get; set; }
        public string ServiceAddress { get; set; }
        public string PaymentReceivedDate { get; set; }
        public string LastPaidDate { get; set; }
        public string LastBillGeneratedDate { get; set; }
        public decimal OutStandingAmount { get; set; }
        public decimal LastPaidAmount { get; set; }
        public decimal LastBillAmount { get; set; }
        public string StatusText { get; set; }
        public bool IsSuccess { get; set; }
    }
}
