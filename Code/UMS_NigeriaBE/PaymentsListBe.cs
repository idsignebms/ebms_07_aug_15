﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("PaymentsInfoByXml", IsNullable = true)]
    public class PaymentsListBe
    {
        [XmlElement("PaymentsList")]
        public List<PaymentsBe> Items { get; set; }
    }
}
