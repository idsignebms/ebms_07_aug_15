﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class PoleBE
    {

        public int RowNumber { get; set; }

        //MPoleMasterDetails -- Faiz-ID103
        public int PoleMasterId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int PoleMasterOrderID { get; set; }
        public int PoleMasterCodeLength { get; set; }

        //PoleDescription
        public int PoleId { get; set; }
        public string Code { get; set; }
        public string Comments { get; set; }

        public int ActiveStatusId { get; set; }

        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }

        public int RowsEffected { get; set; }

        //PoleOrder
        public int PoleOrder { get; set; }

        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalRecords { get; set; }


        public string Master { get; set; }
        public string Parent { get; set; }

        public bool IsParentPole { get; set; }
        public bool IsExists { get; set; }
        public string PoleInputCode { get; set; }

        public bool IsPoleInformationExisis { get; set; }
        public bool IsOrderIdExists { get; set; }

        public int MaxPoleOrderId { get; set; }

        public int IsParentActive { get; set; }

        public bool IsHavingChilds { get; set; }
        public bool IsHavingCustomers { get; set; }

    }
}
