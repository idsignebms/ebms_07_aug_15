﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRoot("PoleBEInfoByXml", IsNullable = true)]
    public class PoleListBE
    {
        [XmlElement("PoleBE")]
        public List<PoleBE> items { get; set; }
    }
}
