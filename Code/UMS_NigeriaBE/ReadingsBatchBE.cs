﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class ReadingsBatchBE
    {
        public int RUploadBatchId { get; set; }
        public string BatchNo { get; set; }
        public DateTime BatchDate { get; set; }
        public string StrBatchDate { get; set; }
        public int TotalCustomers { get; set; }
        public string CreatedBy { get; set; }
        public string LastModifyBy { get; set; }
        public string Notes { get; set; }
        public bool IsClosedBatch { get; set; }

        public int RUploadFileId { get; set; }
        public string FilePath { get; set; }
        public int TotalSucessTransactions { get; set; }
        public int TotalFailureTransactions { get; set; }
        public bool IsTrasactionsCompleted { get; set; }

        public int ReadingTransactionId { get; set; }
        public string SNO { get; set; }
        public string AccountNo { get; set; }
        public string CurrentReading { get; set; }
        public string ReadingDate { get; set; }

        public int ReadingSucessTransactionID { get; set; }
        public string Comments { get; set; }

        public string ReadingFailureransactionID { get; set; }

        public int RowNumber { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalRecords { get; set; }

        public int RowsEffected { get; set; }

        //For Avg Consumption Bulk Upload
        public string AverageReading { get; set; }
        public int AvgUploadBatchId { get; set; }
        public int AvgUploadFileId { get; set; }

        public DateTime CreatedDate { get; set; }
        public string StrCreatedDate { get; set; }

        public string BU_ID { get; set; }
        public int AvgSucessTransactionID { get; set; }
    }
}
