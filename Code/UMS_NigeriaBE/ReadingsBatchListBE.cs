﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("ReadingsBatchBEInfoByXml", IsNullable = true)]
    public class ReadingsBatchListBE
    {
        [XmlElement("ReadingsBatchBE")]
        public List<ReadingsBatchBE> Items { get; set; }
    }
}
