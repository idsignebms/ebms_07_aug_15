﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace UMS_NigeriaBE
{
    public class RegionsBE
    {
        public int Count { get; set; }

        public int RowNumber { get; set; }

        public int RegionId { get; set; }
        public string RegionName { get; set; }

        public string StateCode { get; set; }
        public string StateName { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }

        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }

        public int RowsEffected { get; set; }
        public int ActiveStatusId { get; set; }

        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalRecords { get; set; }
    }
}
