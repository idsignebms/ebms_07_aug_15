﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{

    [XmlRoot("RegionsBEInfoByXml", IsNullable = true)]
    public class RegionsListBE
    {
        [XmlElement("RegionsBE")]
        public List<RegionsBE> Items { get; set; }
    }

}
