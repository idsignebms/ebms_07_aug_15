﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class ReportsBe
    {
        #region stringProperties
        public string BU_ID { get; set; }
        public string BusinessUnitName { get; set; }
        public string ServiceUnitName { get; set; }
        public string ServiceCenterName { get; set; }
        public string SU_ID { get; set; }
        public string BookNo { get; set; }
        public string ServiceCenterId { get; set; }
        public string SubStationId { get; set; }
        public string FeederId { get; set; }
        public string TransFormerId { get; set; }
        public string PoleId { get; set; }
        public string CycleId { get; set; }
        public string CycleName { get; set; }
        public string MonthName { get; set; }
        public string YearName { get; set; }
        public string Name { get; set; }
        public string ServiceAddress { get; set; }
        public string City { get; set; }
        public string AccountNo { get; set; }
        public string OldAccountNo { get; set; }
        public string CustomerUniqueNo { get; set; }
        public string LastPaidDate { get; set; }
        public string TotalPaidAmount { get; set; }
        public string BillDate { get; set; }
        public string BillAmount { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string RecievedDate { get; set; }
        public string ReceivedDevice { get; set; }
        public string ReceivedDeviceId { get; set; }
        public string BookNoWithDetails { get; set; }
        public string BookCode { get; set; }
        public string MinAmt { get; set; }
        public string MaxAmt { get; set; }
        public string BookNumber { get; set; }


        public string GlobalAccountNumber { get; set; }//Faiz - ID103
        #endregion

        #region IntProperties
        public int Month { get; set; }
        public int Year { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int RowNumber { get; set; }
        public int TotalRecords { get; set; }
        public int BookSortOrder { get; set; }
        public int CustomerSortOrder { get; set; }
        #endregion

        #region DecimalProperties
        public decimal TotalDueAmount { get; set; }
        public decimal LastPaidAmount { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal DueAmount { get; set; }
        public decimal TotalReceivedAmount { get; set; }
        public decimal TotalDue { get; set; }
        public decimal OverPayAmount { get; set; }
        public decimal TotalBillsAmount { get; set; }
        public decimal TotalCreditBalance { get; set; }
        #endregion

        #region BoolProperties
        public bool AllSelected { get; set; }
        #endregion
    }

    public class RptCheckMetersBe
    {
        #region stringProperties
        public string BU_ID { get; set; }
        public string SU_ID { get; set; }
        public string BookNo { get; set; }
        public string ServiceCenterId { get; set; }
        public string CycleId { get; set; }
        public string Name { get; set; }
        public string AccountNo { get; set; }
        public string InitialReading { get; set; }
        public string CheckMeterCurrentReading { get; set; }
        public string MeterCurrentReading { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        #endregion

        #region IntProperties
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int RowNumber { get; set; }
        public int TotalRecords { get; set; }
        public int FromMonth { get; set; }
        public int ToMonth { get; set; }
        public int FromYear { get; set; }
        public int ToYear { get; set; }
        #endregion
    }

    public class RptCustomerLedgerBe
    {
        #region stringProperties
        public string BusinessUnitName { get; set; }
        public string ServiceUnitName { get; set; }
        public string BookNo { get; set; }
        public string ServiceCenterName { get; set; }
        public string CycleName { get; set; }
        public string Name { get; set; }
        public string AccountNo { get; set; }
        public string OldAccountNo { get; set; }
        public string MobileNo { get; set; }
        public string Particulars { get; set; }
        public string ClassName { get; set; }
        public string ServiceAddress { get; set; }
        public string TransactionDate { get; set; }
        public string ReferenceNo { get; set; }
        public string Remarks { get; set; }
        public string MeterNo { get; set; }
        public string BUID { get; set; }
        public bool IsSuccess { get; set; }
        public bool IsCustExistsInOtherBU { get; set; }
        public bool CustIsNotActive { get; set; }//Faiz-ID103
        public bool IsDisabledBook { get; set; }
        #endregion

        #region Decimal Properties

        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Balance { get; set; }
        public decimal Due { get; set; }

        #endregion

        #region IntProperties
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int RowNumber { get; set; }
        public int TotalRecords { get; set; }
        public int ReportMonths { get; set; }
        public int Flag { get; set; }
        public int ActiveStatusId { get; set; }
        public int CustomerTypeId { get; set; }

        #endregion

        #region bool
        public bool IsPrepaidCustomer { get; set; }
        public bool IsNotCreditMeter { get; set; }
        #endregion
    }

    public class RptPreBillingBe
    {
        #region Properties

        public string BookNo { get; set; }
        public string CycleName { get; set; }
        public string AccountNo { get; set; }
        public int ReadCodeId { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public string NameOfMonth { get; set; }
        public string AverageReading { get; set; }
        public string HighLowReadDetails { get; set; }
        public int ReportMonths { get; set; }
        public decimal EstimationValue { get; set; }

        public string BU_ID { get; set; }
        public string ClusterType { get; set; }

        public string ReadBy { get; set; }
        

        public string SU_ID { get; set; }// Faiz-ID103
        public string ServiceCenterId { get; set; }//Faiz-ID103
        public DateTime PresentReadDate { get; set; }//Faiz-ID103

        public string GlobalAccountNumber   {get;set;}
        public string OldAccountNo          {get;set;}
        public string MeterNo           {get;set;}
        public string ClassName             {get;set;}
        public string Name                  {get;set;}
        public string ServiceAddress        {get;set;}
        public string PreviousReading       {get;set;}
        public DateTime PreviousReadDate      {get;set;}
        public string PresentReading        {get;set;}
        public decimal Usage { get; set; }
        public string TotalReadings         {get;set;}
        public string LatestTransactionDate {get;set;}
        public string TransactionLog { get; set; }
        public string BusinessUnitName { get; set; }
        public string ServiceUnitName { get; set; }
        public string ServiceCenterName { get; set; }
        public int CustomerSortOrder { get; set; }
        public string CreatedBy { get; set; }
        public string Comments { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ReadDate { get; set; }
        

        #endregion

    }

    public class RptAuditTrayBe
    {
        #region StringProperties
        public string AccountNo { get; set; }
        public string OldBookNo { get; set; }
        public string NewBookNo { get; set; }
        public string Remarks { get; set; }
        public string ApprovalStatus { get; set; }
        public string Name { get; set; }
        public string TransactionDate { get; set; }
        public string OldTariff { get; set; }
        public string NewTariff { get; set; }
        public string BU_ID { get; set; }
        public string SU_ID { get; set; }
        public string SC_ID { get; set; }
        public string CycleId { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string OldName { get; set; }
        public string NewName { get; set; }
        public string OldSurName { get; set; }
        public string NewSurName { get; set; }
        public string OldKnownAs { get; set; }
        public string NewKnownAs { get; set; }
        public string OldPostalAddress { get; set; }
        public string NewPostalAddress { get; set; }
        public string Readings { get; set; }
        public string OldStatus { get; set; }
        public string NewStatus { get; set; }
        public string OldMeterNo { get; set; }
        public string NewMeterNo { get; set; }
        public string OldMeterType { get; set; }
        public string NewMeterType { get; set; }
        public string OldAddress { get; set; }
        public string NewAddress { get; set; }
        public string OldServiceAddress { get; set; }
        public string NewServiceAddress { get; set; }
        public string ClassId { get; set; }
        public string ClassName { get; set; }
        public string CreatedBy { get; set; }
        public string OldTitle { get; set; }
        public string NewTitle { get; set; }
        public string OldFirstName { get; set; }
        public string NewFirstName { get; set; }
        public string OldMiddleName { get; set; }
        public string NewMiddleName { get; set; }
        public string OldLastName { get; set; }
        public string NewLastName { get; set; }
        public string ReportedId { get; set; }
        public string ReportedBy { get; set; }
        public string BusinessUnitName { get; set; }
        public string ServiceUnitName { get; set; }
        public string ServiceCenterName { get; set; }
        public string CycleName { get; set; }
        public string BookCode { get; set; }
        public string CustomerSortOrder { get; set; }
        public string BookSortOrder { get; set; }
        public string OldAccountNo { get; set; }
        public string OldCustomerType { get; set; }
        public string NewCustomerType { get; set; }
        public string AssignedMeterDate { get; set; }
        public string OldCluster { get; set; }
        public string NewCluster { get; set; }
        public string LastTransactionDate { get; set; }
        public string StatusChangedDate { get; set; }

        public string PreviousReading { get; set; }
        public string PresentReading { get; set; }
        public string NewPresentReading { get; set; }
        public string PreviousReadDate { get; set; }
        public string PresentReadDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        
        #endregion

        #region IntProperties
        public int TotalChanges { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TariffId { get; set; }
        public int OldDials { get; set; }
        public int NewDials { get; set; }
        public decimal InitialReading { get; set; }
        public int InitialBillingkWh { get; set; }
        #endregion
    }

    public class RptAuditAddressBe
    {
        public string GlobalAccountNumber { get; set; }
        public string BU_ID { get; set; }
        public string SU_ID { get; set; }
        public string SC_ID { get; set; }
        public string CycleId { get; set; }
        public string NewBookNo { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string ClassId { get; set; }
        public string Remarks { get; set; }
        public string ApprovalStatus { get; set; }
        public string Name { get; set; }
        public string TransactionDate { get; set; }
        public string BusinessUnitName { get; set; }
        public string ServiceUnitName { get; set; }
        public string ServiceCenterName { get; set; }
        public string CustomerSortOrder { get; set; }
        public string BookSortOrder { get; set; }
        public string OldAccountNo { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string CycleName { get; set; }
        public string BookNumber { get; set; }
        public string ClassName { get; set; }
        

        public string OldPostal_HouseNo { get; set; }
        public string OldPostal_StreetName { get; set; }
        public string OldPostal_City { get; set; }
        public string OldPostal_Landmark { get; set; }
        public string OldPostal_AreaCode { get; set; }
        public string OldPostal_ZipCode { get; set; }
        public string NewPostal_HouseNo { get; set; }
        public string NewPostal_StreetName { get; set; }
        public string NewPostal_City { get; set; }
        public string NewPostal_Landmark { get; set; }
        public string NewPostal_AreaCode { get; set; }
        public string NewPostal_ZipCode { get; set; }
        public string OldService_HouseNo { get; set; }
        public string OldService_StreetName { get; set; }
        public string OldService_City { get; set; }
        public string OldService_Landmark { get; set; }
        public string OldService_AreaCode { get; set; }
        public string OldService_ZipCode { get; set; }
        public string NewService_HouseNo { get; set; }
        public string NewService_StreetName { get; set; }
        public string NewService_City { get; set; }
        public string NewService_Landmark { get; set; }
        public string NewService_AreaCode { get; set; }
        public string NewService_ZipCode { get; set; }
        public string OldPostalAddress { get; set; }
        public string NewPostalAddress { get; set; }
        public string OldServiceAddress { get; set; }
        public string NewServiceAddress { get; set; }
        public string LastTransactionDate { get; set; }

        public int TotalChanges { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int RowNumber { get; set; }
    }

    public class RptAgedCustsBe
    {
        #region StringProperties
        public string BU_ID { get; set; }
        public string SU_ID { get; set; }
        public string SC_ID { get; set; }
        public string Date { get; set; }

        public string AccountNo { get; set; }
        public string OldAccountNo { get; set; }
        public string Name { get; set; }

        public string BillGeneratedDate30 { get; set; }
        public string BillGeneratedDate60 { get; set; }
        public string BillGeneratedDate90 { get; set; }
        public string BillGeneratedDate120 { get; set; }

        #endregion

        #region Decimal
        public decimal OutStandingAmount { get; set; }

        //30Days        
        public decimal TotalBillAmountWithTax30 { get; set; }
        public decimal TotalBillAmountWithArrears30 { get; set; }
        //60Days
        public decimal TotalBillAmountWithTax60 { get; set; }
        public decimal TotalBillAmountWithArrears60 { get; set; }
        //90Days
        public decimal TotalBillAmountWithTax90 { get; set; }
        public decimal TotalBillAmountWithArrears90 { get; set; }
        //120Days
        public decimal TotalBillAmountWithTax120 { get; set; }
        public decimal TotalBillAmountWithArrears120 { get; set; }
        #endregion

        #region IntProperties
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        #endregion
    }

    public class RptCustomerStatisticsByTariffBE
    {
        public string BU_ID { get; set; }
        public string Tariff { get; set; }
        public string RevenueBilled { get; set; }
        public int Active { get; set; }
        public int InActive { get; set; }
        public int Hold { get; set; }
        public int TotalPopulation { get; set; }

        public int Month { get; set; }
        public int Year { get; set; }
    }

    public class RptMeterReadingBE
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string MeterReadingFrom { get; set; }
        public string MeterNumber { get; set; }
        public int MeterReadingFromId { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public string BU_ID { get; set; }
        public string SU_ID { get; set; }
        public string SC_ID { get; set; }
        public string CycleId { get; set; }
        public string BookNo { get; set; }

        public int RowNumber { get; set; }
        public int CustomerReadingId { get; set; }
        public string PreviousReading { get; set; }
        public string PresentReading { get; set; }
        public int Usage { get; set; }
        public string AverageReading { get; set; }
        public string ReadDate { get; set; }
        public string TransactionDate { get; set; }
        public int TotalRecords { get; set; }
        public string GlobalAccountNumber { get; set; }
        public string OldAccountNo { get; set; }
        public string BookGroup { get; set; }
        public string CustomerName { get; set; }
        public string ServiceAdress { get; set; }
    }

    public class RptPaymentsEditListBE
    {
        public string UserName { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string TransactionFrom { get; set; }
        public int TransactionFromId { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public string BU_ID { get; set; }
        public string SU_ID { get; set; }
        public string SC_ID { get; set; }
        public string CycleId { get; set; }
        public string BookNo { get; set; }

        public int RowNumber { get; set; }
        public int BatchNo { get; set; }
        public int BatchID { get; set; }
        public string TransactionDate { get; set; }
        public int TotalRecords { get; set; }
        public string GlobalAccountNumber { get; set; }
        public string OldAccountNo { get; set; }
        public string BookGroup { get; set; }
        public string CustomerName { get; set; }
        public string ServiceAdress { get; set; }
        public string PaidDate { get; set; }
        public string Amount { get; set; }
        public string PaidAmount { get; set; }
        public string MeterNumber { get; set; }
        public string ReceiptNo { get; set; }
    }

    public class RptAdjustmentEditListBE
    {
        public string UserName { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string AdjustmentName { get; set; }
        public int AdjustmentId { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public string BU_ID { get; set; }
        public string SU_ID { get; set; }
        public string SC_ID { get; set; }
        public string CycleId { get; set; }
        public string BookNo { get; set; }

        public int RowNumber { get; set; }
        public string TransactionDate { get; set; }
        public int TotalRecords { get; set; }
        public string GlobalAccountNumber { get; set; }
        public string OldAccountNo { get; set; }
        public string BookGroup { get; set; }
        public string CustomerName { get; set; }
        public string ServiceAdress { get; set; }
        public string AdjustmentDate { get; set; }
        public string Amount { get; set; }
        public string BatchNo { get; set; }        
    }
}
