﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("ReportsBeInfoByXml", IsNullable = true)]
    public class ReportsListBe
    {
        [XmlElement("Reports")]
        public List<ReportsBe> items { get; set; }
    }

    [XmlRootAttribute("RptCheckMetersInfoByXml", IsNullable = true)]
    public class RptCheckMetersListBe
    {
        [XmlElement("RptCheckMeters")]
        public List<RptCheckMetersBe> items { get; set; }
    }

    [XmlRootAttribute("RptCustomerLedgerInfoByXml", IsNullable = true)]
    public class RptCustomerLedgerListBe
    {
        [XmlElement("CustomerLedger")]
        public List<RptCustomerLedgerBe> items { get; set; }
    }

    [XmlRootAttribute("RptCustomerLedgerInfoByXml", IsNullable = true)]
    public class RptCustomerLedgerDetailsListBe
    {
        [XmlElement("CustomerDetails")]
        public List<RptCustomerLedgerBe> items { get; set; }
    }

    [XmlRootAttribute("RptPreBillingInfoByXml", IsNullable = true)]
    public class RptPreBillingListBe
    {
        [XmlElement("PreBillingList")]
        public List<RptPreBillingBe> items { get; set; }
    }

    [XmlRootAttribute("RptAuditTrayInfoByXml", IsNullable = true)]
    public class RptAuditTrayListBe
    {
        [XmlElement("AuditTrayList")]
        public List<RptAuditTrayBe> items { get; set; }
    }

    [XmlRootAttribute("RptAuditTrayAddressInfoByXml", IsNullable = true)]
    public class RptAuditAddressListBe
    {
        [XmlElement("AuditTrayAddressList")]
        public List<RptAuditAddressBe> items { get; set; }
    }
    
    [XmlRootAttribute("RptAgedCustsInfoByXml", IsNullable = true)]
    public class RptAgedCustsListBe
    {
        [XmlElement("RptAgedCustsList")]
        public List<RptAgedCustsBe> items { get; set; }
    }

    [XmlRootAttribute("RptMeterReadingsInfoByXml", IsNullable = true)]
    public class RptMeterReadingsListBe
    {
        [XmlElement("RptMeterReadingsList")]
        public List<RptMeterReadingBE> items { get; set; }
    }

    [XmlRootAttribute("RptPaymentsEditListInfoByXml", IsNullable = true)]
    public class RptPaymentsEditListListBe
    {
        [XmlElement("RptPaymentsEditList")]
        public List<RptPaymentsEditListBE> items { get; set; }
    }

    [XmlRootAttribute("RptAdjustmentEditListInfoByXml", IsNullable = true)]
    public class RptAdjustmentEditListListBe
    {
        [XmlElement("AdjustmentEditList")]
        public List<RptAdjustmentEditListBE> items { get; set; }
    }
}
