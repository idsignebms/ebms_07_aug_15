﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class RptBatchNoWisePaymentsBe
    {
        public string BatchNo { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string BatchName { get; set; }
        public string BatchDate { get; set; }
        public string AccountNo { get; set; }
        public string ReceiptNo { get; set; }
        public string BillNo { get; set; }
        public string PaymentMode { get; set; }
        public string AdjustmentType { get; set; }
        public string AdjustedDate { get; set; }
        public string Reason { get; set; }
        public string Name { get; set; }
        
        
        public decimal BatchTotal { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal TotalBatchAmount { get; set; }
        public decimal AdjustedAmt { get; set; }

        #region IntProperties
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int RowNumber { get; set; }
        public int TotalRecords { get; set; }
        #endregion    
    }
}
