﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("RptBatchNoWisePaymentsBeInfoByXml", IsNullable = true)]
    public class RptBatchNoWisePaymentsListBe
    {
        [XmlElement("RptBatchNoWisePaymentsBe")]
        public List<RptBatchNoWisePaymentsBe> items { get; set; }
    }
}
