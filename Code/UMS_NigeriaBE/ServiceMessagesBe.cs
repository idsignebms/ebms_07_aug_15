﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class ServiceMessagesBe
    {
        public bool IsSuccess { get; set; }

        public string Message { get; set; }

        public bool ConfirmMessageStatus { get; set; }
    }
}
