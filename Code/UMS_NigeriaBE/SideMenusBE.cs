﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class SideMenusBE
    {
        public int Main_Menu_Id { get; set; }

        public string Main_Menu_Text { get; set; }

        public int Main_Menu_Order { get; set; }

        public string CssClassName { get; set; }

        public bool HasSubMenu { get; set; }

        public int Page_Id { get; set; }

        public int SubMenu_Id { get; set; }

        public string SubMenu_Text { get; set; }

        public int Role_Id { get; set; }

        public string Path { get; set; }

        public string Icon1 { get; set; }

        public string PageName { get; set; }

        public string Menu_Image { get; set; }
    }
}
