﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("SideMenuBeInfoByXml", IsNullable = true)]
    public class SideMenusListBE
    {
        [XmlElement("SideMenu")]
        public List<SideMenusBE> items { get; set; }
    }

    [XmlRootAttribute("SideMenuBeInfoByXml", IsNullable = true)]
    public class MainMenuListBE
    {
        [XmlElement("MainMenu")]
        public List<SideMenusBE> items { get; set; }
    }
}
