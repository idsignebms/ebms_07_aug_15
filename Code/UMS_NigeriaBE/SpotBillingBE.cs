﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class SpotBillingBE//Neeraj-ID077
    {
        #region String Properties

        public string Cons_AccNo { get; set; }
        public string Cons_SeqNo { get; set; }
        public string Meter_No { get; set; }
        public string Service_No { get; set; }
        public string BU_Name { get; set; }
        public string Cons_Name { get; set; }
        public string Cons_Address { get; set; }
        public string Prev_ReadDate { get; set; }
        public string Cons_Cycle_No { get; set; }
        public string Cons_Cycle_Name { get; set; }
        public string CustomerCare_No { get; set; }
        public string Prev_Bill_Date { get; set; }
        public string Message1 { get; set; }
        public string Message2 { get; set; }
        public string Message3 { get; set; }
        public string Message4 { get; set; }
        public string Last_Payment_Date { get; set; }
        public string Marketer_Name { get; set; }
        public string Due_Date { get; set; }
        public string InitialReading { get; set; }
        public string OldAccNo { get; set; }
        public string EstimationType { get; set; }
        public string BookCycleId { get; set; }
        public string CreatedBy { get; set; }
        public string FileName { get; set; }
        public string Notes { get; set; }
        public string CycleName { get; set; }
        public string MonthName { get; set; }
        public string Highestconsumption { get; set; }
        public string Cons_TariffCode { get; set; }
        public string Prev_Reading { get; set; }
        public string MeterChangeStatus { get; set; }
        public string PreviousBilltype { get; set; }
        #endregion

        #region Int Properties

        public int Adj_Units { get; set; }
        public int Estimated_units { get; set; }
        public int Tax { get; set; }
        public int DueDateFlag { get; set; }
        public int DueDateDays { get; set; }
        public int Multiplication_Factors { get; set; }
        public int ReadCodeId { get; set; }

        public int Prv_Mtr_Consumption { get; set; }
        public int Dails { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int RowCount { get; set; }
        public int CurrentBillSetting { get; set; }
        public int Phase { get; set; }

        #endregion

        #region Decimal Properties

        public decimal Adj_Ammount { get; set; }
        public decimal Net_Arrers { get; set; }
        public decimal Late_Payment_Charges { get; set; }
        public decimal AvgUnits { get; set; }
        public decimal Fixed_Charges { get; set; }
        public decimal Rate { get; set; }
        public decimal PreviousBilledUnits { get; set; }

        #endregion

        #region Boolean Properties

        public bool IsExists { get; set; }

        #endregion
    }
}
