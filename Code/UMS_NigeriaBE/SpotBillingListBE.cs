﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("SpotBillingInputInfoByXML", IsNullable=true)]
    public class SpotBillingListBE
    {
        [XmlElement("SpotBillingInputDetails")]
        public List<SpotBillingBE> items { get; set; }
    }
}
