﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class SpotBillingOutputBe
    {
        public string Cons_AccNo { get; set; }

        public string BillNo { get; set; }

        public string Meter_No { get; set; }

        public string Prev_Reading { get; set; }

        public string CurrentReading { get; set; }

        public string BilledDate { get; set; }

        public int EnregyUnits { get; set; }

        public decimal EnergyCharges { get; set; }

        public decimal FixedChages { get; set; }

        public int Tax { get; set; }

        public decimal Tax_Amount { get; set; }

        public decimal BillAmount { get; set; }

        public decimal NetArres { get; set; }

        public decimal AdjustmentAmount { get; set; }

        public decimal TotalDueNow { get; set; }

        public DateTime DueDate { get; set; }

        public decimal RateCaluclated { get; set; }

        public string BillType { get; set; }

        public int MeterSatus { get; set; }

        public string Check_Meter_Reading { get; set; }

        public string Check_Meter_No { get; set; }

        public int BalanceUnits { get; set; }

        public string CycleId { get; set; }

        public bool IsValidCycle { get; set; }

        public bool IsValid { get; set; }

        public bool IsExists { get; set; }

        public string CreatedBy { get; set; }

        public int Year { get; set; }

        public int Month { get; set; }
    }
}
