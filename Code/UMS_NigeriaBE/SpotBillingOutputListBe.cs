﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("SpotBillOutPutBEInfoByXml", IsNullable = true)]
    public class SpotBillingOutputListBe
    {
        [XmlElement("SpotBillOutputDetails")]
        public List<SpotBillingOutputBe> Items { get; set; }
    }

}
