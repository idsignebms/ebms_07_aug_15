﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class TariffManagementBE
    {

        public decimal Amount { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int ClassID { get; set; }
        public int ChargeId { get; set; }
        public string ChargeName { get; set; }
        public bool IsSuccess { get; set; }
        public string CreatedBy { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public string ClassName { get; set; }
        public int FromKW { get; set; }
        public int ToKW { get; set; }
        public int AdditionalChargeID { get; set; }
        public int IsActive { get; set; }
        public string FromKWs { get; set; }
        public string ToKWs { get; set; }
        public string Charges { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int RowNumber { get; set; }
        public int TotalRecords { get; set; }
        public bool ActiveStatusId { get; set; }
        public string ModifiedBy { get; set; }
        public int EnergyClassChargeID { get; set; }
        public string TariffMonth { get; set; }
        public string TariffYear { get; set; }
        public string Details { get; set; }
        public string CategoryName { get; set; }
        public bool IsClassNameExists { get; set; }
        public int RefClassID { get; set; }
        public string MeterNo { get; set; }
        public string AccountNo { get; set; }
        public string OldAccountNo { get; set; }
        public string Tariff { get; set; }
        public string Name { get; set; }
        public string ServiceAddress { get; set; }
        public string LastBillDate { get; set; }
        public string LastPaidDate { get; set; }
        public string DueAmount { get; set; }
        public int OldClassID { get; set; }
        public int NewClassID { get; set; }
        public string PreviousTariffName { get; set; }
        public string ChangeRequestedTariffName { get; set; }
        public string BU_ID { get; set; }
        public string BookNo { get; set; }
        public int Flag { get; set; }
        public int ApprovalStatusId { get; set; }
        public int TariffChangeRequestId { get; set; }
        public bool ApprovalIsInProcess { get; set; }
        public bool IsChargeNameExists { get; set; }
        public bool IsCustExistsInOtherBU { get; set; }
        public int FunctionId { get; set; }
        public string UserId { get; set; }
        public bool IsFinalApproval { get; set; }
        public string GlobalAccNoAndAccNo { get; set; }
        public int Count { get; set; }

        //ClassStatus
        public string ClassStatus { get; set; }

        public bool IsLastActiveRecord { get; set; }
        public bool IsAmtIncr { get; set; }
        public string ClusterType { get; set; }
        public int ClusterTypeId { set; get; }
        public int OldClusterTypeId { set; get; }
        public int CustomerActiveStatusId { set; get; }

        public bool IsCustomersAssociated { get; set; }
        public bool IsPastDate { get; set; }
    }
}
