﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("TariffManagementBEInfoByXml", IsNullable = true)]
    public class TariffManagementListBE
    {
        [XmlElement("TariffManagement")]
        public List<TariffManagementBE> items { get; set; }
    }
}
