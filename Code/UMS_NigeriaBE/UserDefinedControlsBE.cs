﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class UserDefinedControlsBE
    {
        public int UDCId { get; set; }
        public string FieldName { get; set; }
        public int ControlTypeId { get; set; }
        public bool IsMandatory { get; set; }
        public bool IsActive{ get; set; }
        public string ControlTypeName { get; set; }
        public bool IsHavingRef { get; set; }
        public int MRefId { get; set; }
        public string VText { get; set; }
        public int Ud_Id { get; set; }
        public string AccountNo { get; set; }
        public string Value { get; set; }

        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalRecords { get; set; }
        public int RowNumber { get; set; }
        public int RowsEffected { get; set; }

        public bool IsFieldNameExists { get; set; }

        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }

        public int AssciatedCustomerCount { get; set; }

        public string ItemsString { get; set; }

        public bool IsControlRefExists { get; set; }

        public bool IsValue { get; set; }
    }
}
