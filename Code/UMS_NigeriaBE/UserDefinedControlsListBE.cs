﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
    [XmlRootAttribute("UserDefinedControlsInfoByXml")]
    public class UserDefinedControlsListBE_1
    {
        [XmlElement("UserDefinedControlsListBE_1")]
        public List<UserDefinedControlsBE> items { get; set; }

    }
    [XmlRootAttribute("UserDefinedControlsInfoByXml")]
    public class UserDefinedControlsListBE_2
    {
        [XmlElement("UserDefinedControlsListBE_2")]
        public List<UserDefinedControlsBE> items { get; set; }

    }
}
