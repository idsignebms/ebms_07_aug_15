﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UMS_NigeriaBE
{
    public class UserManagementBE
    {
        public int TotalRecords { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int RowNumber { get; set; }
        public int ActiveStatusId { get; set; }
        public string EmployeeId { get; set; }
        public string Name { get; set; }
        public string SurName { get; set; }
        public int DesignationId { get; set; }
        public string DesignationName { get; set; }
        public string Password { get; set; }
        public string PrimaryEmailId { get; set; }
        public string SecondaryEmailId { get; set; }
        public string ContactNo1 { get; set; }
        public string ContactNo2 { get; set; }
        public int GenderId { get; set; }
        public string GenderName { get; set; }
        public string Address { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string Details { get; set; }
        public string Photo { get; set; }
        public string ScannedDocumet { get; set; }
        public bool IsUserIdExists { get; set; }
        public bool IsEmailIdExists { get; set; }
        public bool IsSuccess { get; set; }
        public string Path { get; set; }
        public string DocumentName { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string FilePath { get; set; }
        public int UserDetailsId { get; set; }
        public bool BudgetSegmentWiseMargin { get; set; }
        public bool CustomerDetailsModification { get; set; }
        public bool BillAdjustment { get; set; }
        public bool TariffAdjustments { get; set; }
        public bool PaymentsAdjustments { get; set; }
        public bool Disconnection { get; set; }
        public bool ReConnections { get; set; }
        public bool BillGeneration { get; set; }
        public string BusinessUnits { get; set; }
        public string ServiceUnits { get; set; }
        public string ServiceCenters { get; set; }
        public string Districts { get; set; }
        public string CashOffices { get; set; }
        public string BU_ID { get; set; }
        public string SU_ID { get; set; }
        public string UserId { get; set; }
        public bool NoPermissions { get; set; }
        public int UserDocumentId { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public bool IsPassWordWrong { get; set; }
        public int IsMobileAccess { get; set; }
    }
}
