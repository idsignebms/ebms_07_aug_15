﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UMS_NigeriaBE
{
     [XmlRootAttribute("UserManagementBEInfoByXml", IsNullable = true)]
    public class UserManagementListBE
    {
         [XmlElement("UserManagementBE")]
         public List<UserManagementBE> Items { get; set; }
    }

     [XmlRootAttribute("UserManagementBEInfoByXml", IsNullable = true)]
     public class UserManagementDocListBE
     {
         [XmlElement("UserDocuments")]
         public List<UserManagementBE> Items { get; set; }
     }
}
