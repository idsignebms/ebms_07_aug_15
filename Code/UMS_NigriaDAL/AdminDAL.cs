﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml;
using iDSignHelper;

namespace UMS_NigriaDAL
{
    public class AdminDAL
    {
        #region Members
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL objiDsignDAL = new iDsignDAL();
        CommonDAL objCommonDal = new CommonDAL();
        #endregion


        #region StoredProcedures

        //GET
        private string PROC_USP_GETPAGEPERMISIONS = "USP_GetPagePermisions";//Faiz - ID103
        private string PROC_USP_GETROLESLIST = "USP_GetRolesList";//Faiz - ID103
        private string PROC_USP_GETGetAccessLevelsList = "USP_GetAccessLevelsList";//Faiz - ID103
        private string PROC_USP_GETGetAccessLevel = "USP_GetAccessLevel";//Faiz - ID103
        private string PROC_USP_GETROLEPERMISSIONS = "USP_GetRolePermissions";

        //Insert
        private string PROC_USP_INSERTROLE = "USP_InsertRole";

        //Update
        private string PROC_USP_UPDATEROLE = "USP_UpdateRole";

        //Delete
        private string PROC_USP_DELETEROLE = "USP_DeleteRole";

        #endregion


        #region Methods

        public XmlDocument Admin(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTROLE, con);
                        break;//Insert Countries 
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DELETEROLE, con);
                        break;//Delete  
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATEROLE, con); 
                        break;//Update 
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument Admin(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETPAGEPERMISIONS, con); break;//Get Admin  //--Faiz - ID103
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument GetRolesList(XmlDocument XmlDoc)
        {
            return objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETROLESLIST, con);
        }

        public XmlDocument GetAccessLevelsList()
        {
            return objiDsignDAL.ExecuteXmlReader(PROC_USP_GETGetAccessLevelsList, con);
        }

        public XmlDocument GetAccessLevel(XmlDocument XmlDoc)
        {
            return objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETGetAccessLevel, con);
        }

        public XmlDocument GetRolePermissions(XmlDocument XmlDoc)
        {
            return objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETROLEPERMISSIONS, con);
        }
        #endregion
    }
}
