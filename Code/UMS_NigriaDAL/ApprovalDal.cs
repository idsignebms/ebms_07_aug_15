﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data.SqlClient;
using System.Configuration;
using iDSignHelper;
using System.Data;

namespace UMS_NigriaDAL
{
    public class ApprovalDal
    {
        #region Members

        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL objiDsignDAL = new iDsignDAL();
        CommonDAL objCommonDal = new CommonDAL();

        #endregion

        #region ProcedureNames

        private static string PROC_USP_IsApproval = "USP_IsApproval";
        private static string PROC_USP_IsApprovalAuthenticated = "USP_IsApprovalAuthenticated";
        private static string PROC_USP_GetApprovalStatusList = "USP_GetApprovalStatusList";
        private static string PROC_USP_GetApprovalFunctions = "USP_GetApprovalFunctions";
        private static string PROC_USP_GetApprovalFunctionsWithCount = "USP_GetApprovalFunctionsWithCount";
        private static string PROC_USP_GetUsersByRoleId = "USP_GetUsersByRoleId";
        private static string PROC_USP_GetUsersByBU = "USP_GetUsersByBU";
        private static string PROC_USP_GetLevels = "USP_GetLevels";
        private static string PROC_USP_InsertApprovalLevels = "USP_InsertApprovalLevels";
        private static string PROC_USP_IsRequestExistForApprovalLevels = "USP_IsRequestExistForApprovalLevels";
        private static string PROC_USP_GetApprovalSettings = "USP_GetApprovalSettings";
        private static string PROC_USP_UpdateActiveStatusForApproval = "USP_UpdateActiveStatusForApproval";
        private static string PROC_USP_InsertMessageForTariffChangeRequest = "USP_InsertMessageForTariffChangeRequest";
        private static string PROC_USP_GetTariffRequestMessagesById = "USP_GetTariffRequestMessagesById";
        private static string PROC_USP_GETUSERDEFINEDCONTROLSLIST = "USP_GetUserDefinedControlsList";
        private static string PROC_USP_IsFinalApproval = "USP_IsFinalApproval";//Karteek      
        private static string PROC_USP_GETFUNCTIONALACCESSSETTINGS = "USP_GetFunctionalAccessSettings";//Bhimaraju.V[ID065]      
        private static string PROC_USP_ISREQUESTEXISTFORFUNCTIONACCESSPERMISSION = "USP_IsRequestExistForFunctionAccessPermission";//Bhimaraju.V[ID065]      
        private static string PROC_USP_UPDATEACTIVESTATUSFORFUNCTIONALPERMISSION = "USP_UpdateActiveStatusForFunctionalPermission";//Bhimaraju.V[ID065]      

        #endregion

        #region Methods

        public XmlDocument IsApproval(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsApproval, con);//Is feaures in approval process
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument IsAuthorizeApproval(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsApprovalAuthenticated, con);//Is feaures in approval process
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public DataSet GetApprovalStatusList()
        {
            return objCommonDal.ExecuteReader_Get(PROC_USP_GetApprovalStatusList, con);
        }

        public DataSet GetApprovalSettings()
        {
            return objCommonDal.ExecuteReader_Get(PROC_USP_GetApprovalSettings, con);
        }

        public XmlDocument Get(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.List:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetUsersByRoleId, con);
                        break;
                    case ReturnType.Single:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsRequestExistForApprovalLevels, con);
                        break;
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetTariffRequestMessagesById, con);
                        break;
                    case ReturnType.Multiple:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetUsersByBU, con);
                        break;
                    case ReturnType.Retrieve:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetLevels, con);
                        break;
                    case ReturnType.Set:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetApprovalFunctionsWithCount, con); break;
                    case ReturnType.Fetch:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_ISREQUESTEXISTFORFUNCTIONACCESSPERMISSION, con); break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public DataSet GetDataSet(XmlDocument XmlDoc, ReturnType value)
        {
            DataSet ds = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GetTariffRequestMessagesById, con); break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public XmlDocument Get(ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GetApprovalFunctions, con); break;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        public DataSet GetDetails(ReturnType value)
        {
            DataSet ds = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Bulk:
                        ds = objCommonDal.ExecuteReader_Get(PROC_USP_GETUSERDEFINEDCONTROLSLIST, con); break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public XmlDocument Insert(XmlDocument XmlDoc, Statement value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertApprovalLevels, con);
                        break;
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateActiveStatusForApproval, con);
                        break;
                    case Statement.Service:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertMessageForTariffChangeRequest, con);
                        break;
                    case Statement.Check:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsFinalApproval, con);
                        break;
                    case Statement.Change:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATEACTIVESTATUSFORFUNCTIONALPERMISSION, con);
                        break;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public DataSet GetFunctionAccessSettings(ReturnType action)
        {
            DataSet ds = null;
            try
            {
                switch (action)
                {
                    case ReturnType.Get:
                        ds = objCommonDal.ExecuteReader_Get(PROC_USP_GETFUNCTIONALACCESSSETTINGS, con); break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        #endregion


    }
}
