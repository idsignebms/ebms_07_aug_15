﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using UMS_NigeriaBE;
using System.Xml;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace UMS_NigriaDAL
{
    public class AreaDAL
    {
        #region construction
        public AreaDAL()
        {
            InitializeProcedures();
        }
        #endregion

        #region Members
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL objiDsignDAL = new iDsignDAL();
        CommonDAL objCommonDal = new CommonDAL();
        public static string[] Procedures = new string[1];
        #endregion

        #region Procedures
        public static void InitializeProcedures()//Neeraj
        {
            //Here maintain list for procedures:
            Procedures[0] = "MASTERS.USP_IsAreaExists";
        }
        private string PROC_USP_GetArea = "USP_GetArea";
        private string PROC_USP_InsertArea = "USP_InsertArea";
        private string PROC_USP_UpdateArea = "USP_UpdateArea";
        private string PROC_USP_DeleteArea = "USP_DeleteArea";
        #endregion

        #region Bussiness Logic
        public XmlDocument IsAreaExists(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, Procedures[0], con);
            }
            catch (Exception ex)
            {
                //write log
            }

            return xml;
        }

        public XmlDocument Area(XmlDocument XmlDoc,Statement Value)
        {
            XmlDocument xml = null;
            try
            {
                switch (Value)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc,PROC_USP_InsertArea , con); break;
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateArea, con); break;
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DeleteArea, con); break;
                }
            }
            catch (Exception ex)
            {
                //write log
            }

            return xml;
        }

        public XmlDocument Area(XmlDocument XmlDoc,ReturnType Value)
        {
            XmlDocument xml = null;
            try
            {
                switch (Value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc,PROC_USP_GetArea , con); break;
                }
            }
            catch (Exception ex)
            {
                //write log
            }

            return xml;
        }

        #endregion
    }
}
