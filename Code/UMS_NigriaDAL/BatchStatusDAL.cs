﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml;
using iDSignHelper;
using UMS_NigeriaBE;

namespace UMS_NigriaDAL
{
    public class BatchStatusDAL
    {
        #region Members
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        CommonDAL objCommonDal = new CommonDAL();
        iDsignDAL _ObjiDsignDAL = new iDsignDAL(); 
        #endregion

        #region StoredProcedures
        
        //GET
        private string PROC_USP_GetBatchPaymentStatus = "USP_GetBatchPaymentStatus";//Faiz - ID103
        private string PROC_USP_GetReopenBatchPayemts = "USP_GetReopenBatchPayemts";//Faiz - ID103
        private string PROC_USP_GetReopenBatchAdjustments = "USP_GetReopenBatchAdjustments";//Faiz - ID103

        private string PROC_USP_CloseBatchPayment = "USP_CloseBatchPayment";//Faiz - ID103
        private string PROC_USP_ReopenBatchPayment = "USP_ReopenBatchPayment";//Faiz - ID103
        private string PROC_USP_ReopenBatchAdjustment = "USP_ReopenBatchAdjustment";//Faiz - ID103

        private string PROC_USP_DeleteBatchPayment = "USP_DeleteBatchPayment";//Faiz - ID103

        private string PROC_USP_GetBatchAdjustmentStatus = "USP_GetBatchAdjustmentStatus";//Faiz - ID103

        private string PROC_USP_CloseBatchAdjustment = "USP_CloseBatchAdjustment";//Faiz - ID103

        private string PROC_USP_DeleteBatchAdjustment = "USP_DeleteBatchAdjustment";//Faiz - ID103

        #endregion

        public DataSet GetBatchPaymentStatus(XmlDocument xml)
        {
            return objCommonDal.ExecuteReader_Get(xml,PROC_USP_GetBatchPaymentStatus, con);
        }

        public DataSet GetReopenBatchPayments(XmlDocument xml)
        {
            return objCommonDal.ExecuteReader_Get(xml, PROC_USP_GetReopenBatchPayemts, con);
        }

        public DataSet GetReopenBatchAdjustments(XmlDocument xml)
        {
            return objCommonDal.ExecuteReader_Get(xml, PROC_USP_GetReopenBatchAdjustments, con);
        }


        public XmlDocument CloseBatchPayment(XmlDocument xml)
        {
            return _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_CloseBatchPayment, con);
        }
        public XmlDocument ReopenBatchPayment(XmlDocument xml)
        {
            return _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_ReopenBatchPayment, con);
        }

        public XmlDocument ReopenBatchAdjustments(XmlDocument xml)
        {
            return _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_ReopenBatchAdjustment, con);
        }

        public XmlDocument DeleteBatchPayment(XmlDocument xml)
        {
            return _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_DeleteBatchPayment, con);
        }

        public DataSet GetBatchAdjustmentStatus(XmlDocument xml) 
        {
            return objCommonDal.ExecuteReader_Get(xml,PROC_USP_GetBatchAdjustmentStatus, con);
        }

        public XmlDocument CloseBatchAdjustment(XmlDocument xml)
        {
            return _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_CloseBatchAdjustment, con);
        }

        public XmlDocument DeleteBatchAdjustment(XmlDocument xml)
        {
            return _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_DeleteBatchAdjustment, con);
        }
    }
}
