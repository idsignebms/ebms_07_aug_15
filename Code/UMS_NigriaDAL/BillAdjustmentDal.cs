﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;

namespace UMS_NigriaDAL
{
    public class BillAdjustmentDal
    {
        iDsignDAL _objiDsignDAL = new iDsignDAL();
        SqlConnection SqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        

        private static string PROC_USP_INSERT_BILLADJUSTMENT = "USP_INSERT_BILLADJUSTMENT";
        private static string PROC_USP_GETBILLADJUSTMENTSTATUS = "USP_GETBILLADJUSTMENTSTATUS";
        private static string PROC_USP_INSERT_BILLREADINGADJUSTMENT = "USP_INSERT_BILLREADINGADJUSTMENT";
        private static string PROC_USP_GETADJUSTEDBILLDETAILS = "USP_GETADJUSTEDBILLDETAILS";
        private static string PROC_USP_MeterConsumptionAdjustment = "USP_MeterConsumptionAdjustment";
        private static string PROC_USP_CancelAdjustment = "USP_CancelAdjustment";

        private static string PROC_USP_InsertAdjustmentBatch = "USP_InsertAdjustmentBatch";
        private static string PROC_USP_GetAdjustmentBatchByNo = "USP_GetAdjustmentBatchByNo";
        private static string PROC_USP_GetReasonList = "USP_GetReasonList";
        private static string PROC_USP_UpdateCustomerOutstanding = "USP_UpdateCustomerOutstanding";
        private static string PROC_USP_BillAdjustment = "USP_BillAdjustment";
        private static string PROC_USP_GetAdjustmentBatchDetails = "USP_GetAdjustmentBatchDetails";

        private static string PROC_USP_Delete_Batch_Adjustments = "USP_Delete_Batch_Adjustments";
        private static string PROC_USP_AdjustmentForNoBill = "USP_AdjustmentForNoBill";
        private static string PROC_IsBillExists = "IsBillExists";
        private static string PROC_USP_UpdateAdjustmentForNoBill = "USP_UpdateAdjustmentForNoBill";
        private static string PROC_USP_DeleteAdjustmentForNoBill = "USP_DeleteAdjustmentForNoBill";
        private static string PROC_USP_GetAdjustmentForNoBill = "USP_GetAdjustmentForNoBill";
        private static string PROC_USP_IsNoBillAdjustmentExists = "USP_IsNoBillAdjustmentExists";
        private string PROC_USP_GetAdjustmentLastTransactions = "USP_GetAdjustmentLastTransactions";


        public XmlDocument InsertAdjustmentDetails(XmlDocument XmlDoc, XmlDocument MultiXmlDoc, Statement action)
        {
            try
            {
                XmlDocument xml = null;
                switch (action)
                {
                    case Statement.Insert:
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, MultiXmlDoc, PROC_USP_INSERT_BILLADJUSTMENT, SqlCon);
                        break;
                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public XmlDocument InsertAdjustedReadDetails(XmlDocument XmlDoc, Statement action)
        {
            try
            {
                XmlDocument xml = null;
                switch (action)
                {
                    case Statement.Insert:
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERT_BILLREADINGADJUSTMENT, SqlCon);
                        break;
                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public XmlDocument GetAdjustmentDetails(XmlDocument XmlDoc, ReturnType value)
        {
            try
            {
                XmlDocument xml = null;
                switch (value)
                {
                    case ReturnType.Check:
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETBILLADJUSTMENTSTATUS, SqlCon);
                        break;
                    case ReturnType.Fetch:
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETADJUSTEDBILLDETAILS, SqlCon);
                        break;
                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public XmlDocument MeterConsumptionAdjustmentDAL(XmlDocument XmlDoc)//Neeraj-ID0077
        {
            XmlDocument xml = null;
            try
            {
                xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_MeterConsumptionAdjustment, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }
        public XmlDocument CancelAdjustmentDAL(XmlDocument XmlDoc)//Neeraj-ID0077
        {
            XmlDocument xml = null;
            try
            {
                xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CancelAdjustment, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument InsertAdjustmentBatchDAL(XmlDocument XmlDoc)//Neeraj-ID0077
        {
            XmlDocument xml = null;
            try
            {
                xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertAdjustmentBatch, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetAdjustmentBatchByNoDAL(XmlDocument XmlDoc)//Neeraj-ID0077
        {
            XmlDocument xml = null;
            try
            {
                xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetAdjustmentBatchByNo, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetReasonCodeListDAL()//Neeraj-ID0077
        {
            XmlDocument xml = null;
            try
            {
                xml = _objiDsignDAL.ExecuteXmlReader(PROC_USP_GetReasonList, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument UpdateCustomerOutstandingDAL(XmlDocument XmlDoc)//Neeraj-ID0077 
        {
            XmlDocument xml = null;
            try
            {
                xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateCustomerOutstanding, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument BillAdjustmentDAL(XmlDocument XmlDoc)//Neeraj-ID0077 
        {
            XmlDocument xml = null;
            try
            {
                xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_BillAdjustment, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetAdjustmentBatchDetailsDAL(XmlDocument XmlDoc)//Neeraj-ID0077 
        {
            XmlDocument xml = null;
            try
            {
                xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetAdjustmentBatchDetails, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument DeleteBatchAdjustmentsDAL(XmlDocument XmlDoc)//Neeraj-ID0077 
        {
            XmlDocument xml = null;
            try
            {
                xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_Delete_Batch_Adjustments, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument AdjustmentForNoBill(XmlDocument XmlDoc, AdjustmentNoBill action)//Neeraj-ID0077 
        {
            try
            {
                XmlDocument xml = null;
                switch (action)
                {
                    case AdjustmentNoBill.AddAdjustment:
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_AdjustmentForNoBill, SqlCon);
                        break;
                    case AdjustmentNoBill.UpdateAdjustment:
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateAdjustmentForNoBill, SqlCon);
                        break;
                    case AdjustmentNoBill.DeleteAdjustment:
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DeleteAdjustmentForNoBill, SqlCon);
                        break;
                    case AdjustmentNoBill.IsBillExists:
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_IsBillExists, SqlCon);
                        break;
                    case AdjustmentNoBill.GetAdjsutmentDetails:
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetAdjustmentForNoBill, SqlCon);
                        break;
                    case AdjustmentNoBill.IsAdjustmentDone:
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsNoBillAdjustmentExists, SqlCon);
                        break;
                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetAdjustmentLastTransactions(XmlDocument XmlDoc)//Neeraj-ID0077 
        {
            XmlDocument xml = null;
            try
            {
                xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetAdjustmentLastTransactions, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }
    }
}
