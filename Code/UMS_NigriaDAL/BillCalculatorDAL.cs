﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using iDSignHelper;
namespace UMS_NigriaDAL
{
  public class BillCalculatorDAL
  {
      #region Member

      public static string PROC_USP_CalculateBillAmount = "USP_CalculateBillAmount";
      public static string PROC_USP_GetCustomerTypes = "USP_GetCustomerTypes";
      public static string PROC_USP_GetCustomerSubTypes = "USP_GetCustomerSubTypes";
      public static string PROC_USP_CalculateBillWithConsumption="USP_CalculateBillWithConsumption";
      public static string PROC_USP_GetBillingChargeDetails = "USP_GetBillingChargeDetails";
      private static string PROC_USP_CheckCustomerAccountExists = "USP_CheckCustomerAccountExists";
      
      SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
      iDsignDAL _ObjiDsignDAL = new iDsignDAL(); 
      #endregion

      public XmlDocument GetCustomerType(ReturnType value)
      {
          XmlDocument xml = null;
          try
          {
              switch (value)
              {
                  case ReturnType.Multiple:
                      xml = _ObjiDsignDAL.ExecuteXmlReader(PROC_USP_GetCustomerTypes, con); break;
              }
          }
          catch (Exception ex)
          {
              throw ex;
          }
          return xml;
      }
      public XmlDocument GetCustomerSubTypes(XmlDocument xml, ReturnType value)  //Get List of SubType ClassNames
      {
          XmlDocument xmldoc = null;
          try
          {
              switch (value)
              {
                  case ReturnType.Multiple:
                      xmldoc = _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_GetCustomerSubTypes, con); break;
              }
          }
          catch (Exception ex)
          {
              throw ex;
          }
          return xmldoc;
      }
      public XmlDocument GetFixedAndEneryCharges(XmlDocument xml, ReturnType value)
      {
          XmlDocument xmldoc = null;
          try
          {
              switch (value)
              {
                  case ReturnType.Get:
                      xmldoc = _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_CalculateBillAmount, con);break;
                  case ReturnType.Single:
                      xmldoc = _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_CheckCustomerAccountExists, con); break;
              }
          }
          catch (Exception ex)
          {
              throw ex;
          }
          return xmldoc;
      }
      public XmlDocument CalculateBillWithConsumption(XmlDocument xml, ReturnType value)
      {
          XmlDocument xmldoc = null;
          try
          {
              switch (value)
              {
                  case ReturnType.Get:
                      xmldoc = _ObjiDsignDAL.ExecuteXmlReader(xml,PROC_USP_GetBillingChargeDetails, con); break;
              }
          }
          catch (Exception ex)
          {
              throw ex;
          }
          return xmldoc;
      }
      

  }
}
