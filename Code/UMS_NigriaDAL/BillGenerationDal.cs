﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Data;
using UMS_NigeriaBE;

namespace UMS_NigriaDAL
{
    public class BillGenerationDal
    {
        iDsignDAL objiDsignDAL = new iDsignDAL();
        SqlConnection SqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        CommonDAL objCommonDal = new CommonDAL();

        #region ProcedureNames

        private static string PROC_USP_GETBILLPROCESSTYPES = "USP_GETBILLPROCESSTYPES";
        private static string PROC_USP_GETBILLOPENINGYEARS = "USP_GETBILLOPENINGYEARS";
        private static string PROC_USP_GETBILLOPENINGMONTHS = "USP_GETBILLOPENINGMONTHS";
        private static string PROC_USP_GETBILLOPENINGMONTHS_YEARS = "USP_GETBILLOPENINGMONTHS_YEARS";

        //Insert First Step Details
        private static string PROC_USP_INSERTBILLINGQUEUEDETAILS = "USP_INSERTBILLINGQUEUEDETAILS";
        private static string PROC_USP_ISCUSTOMERSEXISTS_BYFEEDERCYCLES = "USP_ISCUSTOMERSEXISTS_BYFEEDERCYCLES";
        private static string PROC_USP_GETCUSTOMERDETAILS_BYFEEDERCYCLES = "USP_GETCUSTOMERDETAILS_BYFEEDERCYCLES";
        private static string PROC_USP_GetTariffDetails_ByCycles = "USP_GetTariffDetails_ByCycles";//Karteek
        private static string PROC_USP_ISBILLGENERATEDFORMONTH = "USP_ISBILLGENERATEDFORMONTH";
        private static string PROC_USP_GetDisableBookCustomersCount_ByCycles_New = "USP_GetDisableBookCustomersCount_ByCycles_New";

        private static string PROC_USP_GETDISABLEBOOKCUSTOMERS_BYCYCLES = "USP_GETDISABLEBOOKCUSTOMERS_BYCYCLES";
        private static string PROC_USP_GETDISABLEBOOKS_BYCYCLES = "USP_GETDISABLEBOOKS_BYCYCLES";
        private static string PROC_USP_ISEXISTSTARIFFENERGIES_CYCLE = "USP_ISEXISTSTARIFFENERGIES_CYCLE";

        private static string PROC_USP_GETBILLSCHEDULEDETAILS_BYSERVICESTARTTIME = "USP_GETBILLSCHEDULEDETAILS_BYSERVICESTARTTIME";
        private static string PROC_USP_INSERTBILLGENERATIONDETAILS = "USP_INSERTBILLGENERATIONDETAILS";

        private static string PROC_USP_INSERTBILLGENERATION_BYACCOUNTNO = "USP_INSERTBILLGENERATION_BYACCOUNTNO";
        private static string PROC_USP_GETPREVIOUSREADING_BYACCOUNTNO = "USP_GETPREVIOUSREADING_BYACCOUNTNO";

        private static string PROC_USP_GETCUSTOMERREADINGDETAILS = "USP_GETCUSTOMERREADINGDETAILS";
        private static string PROC_USP_GETCUSTOMERBILLSDETAILS = "USP_GETCUSTOMERBILLSDETAILS";

        private static string PROC_USP_GetEnergyCharges_ByConsumption = "USP_GetEnergyCharges_ByConsumption";//Neeraj

        private static string PROC_USP_UpdateBillDetails_MeterReading = "USP_UpdateBillDetails_MeterReading";//Neeraj

        private static string PROC_USP_GETREADINGDETAILS_BYACCOUNTNO = "USP_GETREADINGDETAILS_BYACCOUNTNO";
        private static string PROC_USP_INSERTCUSTOMERBILLGENERATION = "USP_INSERTCUSTOMERBILLGENERATION";

        private static string PROC_USP_UPDATEBILLGENERATIONSTATUS = "USP_UPDATEBILLGENERATIONSTATUS";
        private static string PROC_USP_UPDATEBILLINGQUEUEDETAILS = "USP_UPDATEBILLINGQUEUEDETAILS";
        private static string PROC_USP_UPDATEBILLFILEDETAILS = "USP_UPDATEBILLFILEDETAILS";
        private static string PROC_USP_UPDATECUSTOMERBILLFILE = "USP_UPDATECUSTOMERBILLFILE";
        private static string PROC_USP_GetBillOpenCloseYears = "USP_GetBillOpenCloseYears";
        private static string PROC_USP_GetOpenMonthAndYear = "USP_GetOpenMonthAndYear";

        //Validation related Procedures
        private static string PROC_USP_ISPAYMENTSBACTHESCLOSE_MONTH = "USP_ISPAYMENTSBACTHESCLOSE_MONTH";
        private static string PROC_USP_ISADJUSTMENTBACTHESCLOSE_MONTH = "USP_ISADJUSTMENTBACTHESCLOSE_MONTH";
        private static string PROC_USP_ISCYCLEREADINGSEXISTS_MONTH = "USP_ISCYCLEREADINGSEXISTS_MONTH";
        private static string PROC_USP_ISCYCLEESTIMATIONEXISTS_MONTH = "USP_ISCYCLEESTIMATIONEXISTS_MONTH";
        private string PROC_USP_InsertBillGenerationDetails_DEMO = "USP_InsertBillGenerationDetails_DEMO"; //Neeraj
        private string PROC_USP_InsertBillGenerationDetails_New_satya = "USP_InsertBillGenerationDetails_New"; //Satya
        private string PROC_USP_UpdateBillFileDetails_DEMO = "USP_UpdateBillFileDetails_DEMO";
        private string PROC_USP_UpdateBillGenerationStatus_DEMO = "USP_UpdateBillGenerationStatus_DEMO";


        private string PROC_UPS_GetBillTextFiles = "UPS_GetBillTextFiles";
        private string PROC_UPS_GetBillTextFilesForCustomers = "UPS_GetBillTextFilesForCustomers";
        private string PROC_USP_GenerateBillForCustomer = "USP_GenerateBillForCustomer";
        private string PROC_USP_UpdateFiilePathCustomerDetails = "USP_UpdateFiilePathCustomerDetails";



        private static string PROC_USP_GetBillYears = "USP_GetBillYears";
        private static string PROC_USP_GetBillMonths = "USP_GetBillMonths";
        private static string PROC_USP_GetBillAllMonths_Year = "USP_GetBillAllMonths_Year";
        private static string PROC_USP_GetDisableBooksOnlyNoPwr = "USP_GetDisableBooksOnlyNoPwr";

        //Bill Generation PDF
        private static string PROC_USP_GetCustomerBillDetails_ToGeneratePDF = "USP_GetCustomerBillDetails_ToGeneratePDF";//Karteek
        private static string PROC_USP_InsertCustomerPDFBillDetails = "USP_InsertCustomerPDFBillDetails";//Bhimaraju Vanka
        
        #endregion

        #region Methods

        public XmlDocument GetDetails(ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GETBILLPROCESSTYPES, SqlCon);
                        break;
                    case ReturnType.Multiple:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GETBILLOPENINGYEARS, SqlCon);
                        break;
                    case ReturnType.Check:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GETBILLSCHEDULEDETAILS_BYSERVICESTARTTIME, SqlCon);
                        break;
                    case ReturnType.Bulk:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GetBillOpenCloseYears, SqlCon);
                        break;
                    case ReturnType.Single:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GetOpenMonthAndYear, SqlCon);
                        break;
                    case ReturnType.Fetch:
                        xml = objiDsignDAL.ExecuteXmlReader(PROC_USP_GetBillYears, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetDetails(XmlDocument Xml, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_GETBILLOPENINGMONTHS, SqlCon);
                        break;
                    case ReturnType.Check:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_ISCUSTOMERSEXISTS_BYFEEDERCYCLES, SqlCon);
                        break;
                    case ReturnType.Retrieve:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_GETCUSTOMERDETAILS_BYFEEDERCYCLES, SqlCon);
                        break;
                    case ReturnType.Fetch:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_ISBILLGENERATEDFORMONTH, SqlCon);
                        break;
                    case ReturnType.Single:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_GETPREVIOUSREADING_BYACCOUNTNO, SqlCon);
                        break;
                    case ReturnType.Bulk:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_GETBILLOPENINGMONTHS_YEARS, SqlCon);
                        break;
                    case ReturnType.List:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_GetTariffDetails_ByCycles, SqlCon);
                        break;
                    case ReturnType.Group:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_GetBillMonths, SqlCon);
                        break;
                    case ReturnType.User:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_GetBillAllMonths_Year, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetBillCycleDetails(XmlDocument Xml, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.List:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_GETDISABLEBOOKCUSTOMERS_BYCYCLES, SqlCon);
                        break;
                    case ReturnType.Group:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_GETDISABLEBOOKS_BYCYCLES, SqlCon);
                        break;
                    case ReturnType.Check:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_ISEXISTSTARIFFENERGIES_CYCLE, SqlCon);
                        break;
                    case ReturnType.Single:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_GetDisableBookCustomersCount_ByCycles_New, SqlCon);
                        break;
                    case ReturnType.Fetch:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_GetDisableBooksOnlyNoPwr, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument InsertDetails(XmlDocument Xml, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_INSERTBILLINGQUEUEDETAILS, SqlCon, Constants.CommandTimeOut);
                        break;
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_INSERTBILLGENERATIONDETAILS, SqlCon, Constants.CommandTimeOut);
                        break;
                    case Statement.Modify:// For readings and Bill Generation From Service(Mobiles) Side
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_INSERTBILLGENERATION_BYACCOUNTNO, SqlCon);
                        break;
                    case Statement.Generate:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_INSERTCUSTOMERBILLGENERATION, SqlCon);
                        break;

                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument UpdateBillDetails(XmlDocument Xml, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_UPDATEBILLGENERATIONSTATUS, SqlCon);
                        break;
                    case Statement.Change:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_UPDATEBILLINGQUEUEDETAILS, SqlCon);
                        break;
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_UPDATEBILLFILEDETAILS, SqlCon);
                        break;
                    case Statement.Modify:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_UPDATECUSTOMERBILLFILE, SqlCon);
                        break;

                    case Statement.UpdateBillSattus:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_UpdateBillGenerationStatus_DEMO, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public DataSet GetCustomerBills(XmlDocument XmlDoc)
        {
            try
            {
                return objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GETCUSTOMERBILLSDETAILS, SqlCon, 300);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument GetCustomerReadingDetails(XmlDocument Xml, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_GETCUSTOMERREADINGDETAILS, SqlCon);
                        break;
                    case ReturnType.Fetch:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_GETCUSTOMERBILLSDETAILS, SqlCon);
                        break;
                    case ReturnType.Multiple:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_GETREADINGDETAILS_BYACCOUNTNO, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetMeterReadingAdjstDetailsDAL(XmlDocument Xml)//Method will fetch meter reading adjustment details. --Neeraj
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_GetEnergyCharges_ByConsumption, SqlCon);
                return xml;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument UpdatedMeterReadingAdjustmentsDAL(XmlDocument Xml)//Method will update meter reading adjustment details. --Neeraj
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_UpdateBillDetails_MeterReading, SqlCon);
                return xml;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public XmlDocument CheckBillValidations(XmlDocument Xml, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_ISCYCLEREADINGSEXISTS_MONTH, SqlCon);
                        break;
                    case ReturnType.Check:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_ISCYCLEESTIMATIONEXISTS_MONTH, SqlCon);
                        break;
                    case ReturnType.Fetch:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_ISPAYMENTSBACTHESCLOSE_MONTH, SqlCon);
                        break;
                    case ReturnType.Single:
                        xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_ISADJUSTMENTBACTHESCLOSE_MONTH, SqlCon);
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GenerateCustomersBills(XmlDocument Xml)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_InsertBillGenerationDetails_DEMO, SqlCon, Constants.CommandTimeOut);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }
        public DataSet GenerateCustomersBills_GetDetails(XmlDocument xml)
        {
            try
            {
                return objCommonDal.ExecuteReader_Get(xml, PROC_USP_InsertBillGenerationDetails_DEMO, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public DataSet GenerateCustomersBills_GetDetails_Satya(XmlDocument xml)
        {
            try
            {
                return objCommonDal.ExecuteReader_Get(xml, PROC_USP_InsertBillGenerationDetails_New_satya, SqlCon, Constants.CommandTimeOut);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public XmlDocument UpdateBillGenFile(XmlDocument Xml)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_UpdateBillFileDetails_DEMO, SqlCon, Constants.CommandTimeOut);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }


        public DataSet DownloadBillingFile(XmlDocument XmlDoc)
        {
            DataSet ds = null;
            try
            {
                ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_UPS_GetBillTextFiles, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }
        public DataSet DownloadBillingFileForCustomer(XmlDocument XmlDoc)
        {
            DataSet ds = null;
            try
            {
                ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_UPS_GetBillTextFilesForCustomers, SqlCon);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }
        public DataSet GenerateBillForCustomer(XmlDocument xmlDoc)
        {
            DataSet ds = new DataSet();
            try
            {
                ds = objCommonDal.ExecuteReader_Get(xmlDoc, PROC_USP_GenerateBillForCustomer, SqlCon, Constants.CommandTimeOut);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return ds;
        }

        public XmlDocument UpdateBillGenByCustFile(XmlDocument Xml)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_UpdateFiilePathCustomerDetails, SqlCon, Constants.CommandTimeOut);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument GetCustomerBillDetailsForPDF(XmlDocument Xml)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_GetCustomerBillDetails_ToGeneratePDF, SqlCon, Constants.CommandTimeOut);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        public XmlDocument InsertCustomerBillDetailsForPDF(XmlDocument Xml)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(Xml, PROC_USP_InsertCustomerPDFBillDetails, SqlCon, Constants.CommandTimeOut);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }

        #endregion

    }
}
