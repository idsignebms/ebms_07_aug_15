﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using iDSignHelper;
using System.Configuration;
using System.Xml;
using System.Data;
using UMS_NigeriaBE;

namespace UMS_NigriaDAL
{
    public class BillReadingDAL
    {
        #region Member

        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL _ObjiDsignDAL = new iDsignDAL();
        CommonDAL objCommonDal = new CommonDAL();
        #endregion

        #region Procedures

        //private static string PROC_USP_GETBILLPREVIOUSREADING = "USP_GetBillPreviousReading";
        //private static string PROC_USP_GETBILLPREVIOUSREADING = "USP_GetBillPreviousReading_NEW";
        private static string PROC_USP_GETBILLPREVIOUSREADING = "USP_GetAccountwiseMeterReadings";//Karteek
        //private static string PROC_USP_GETBILLPREVIOUSREADING_BOOKWISE = "USP_GetBillPreviousReading_Bookwise";
        private static string PROC_USP_GETBILLPREVIOUSREADING_BOOKWISE = "USP_GetBookwiseCUstomerReadings";//Karteek

        //private static string PROC_USP_INSERTBILLREADING = "USP_InsertCustomerBillReading";
        private static string PROC_USP_INSERTBILLREADING = "USP_InsertCustomerBillReading_NEW";
        //private static string PROC_USP_UPDATEBILLREADING = "USP_UpdateBillPreviousReading";
        private static string PROC_USP_UPDATEBILLREADING = "USP_UpdateBillxPreviousReading_New";

        private static string PROC_USP_InsertCustomerMeterReadingLogs = "USP_InsertCustomerMeterReadingLogs";//Karteek
        private static string PROC_USP_InsertCustomerMeterReadingAdjustment = "USP_InsertCustomerMeterReadingAdjustment";//BhimarajuVanka

        private static string PROC_USP_GETMETERDECIMALVALUES = "USP_GetMeterDecimalValues";
        private static string PROC_USP_GETCASHIERS = "USP_GetCashiers";
        private static string PROC_USP_GETOFFICES = "USP_GetCashOffices";
        private static string PROC_USP_GetCashOffices_ByBU_ID = "USP_GetCashOffices_ByBU_ID";
        private static string PROC_USP_INSERTBATCHDETAILS = "USP_InsertBatchDetails";
        private static string PROC_USP_ISACCOUNTNOEXSIT = "USP_ISACCOUNTNOEXSIT";
        private static string PROC_USP_GetBatchDetailsByBatchNo = "USP_GetBatchDetailsByBatchNo";
        private static string PROC_USP_UpdateBatchAmountByBatchNo = "USP_UpdateBatchAmountByBatchNo";
        private static string PROC_USP_CHECKPAYMENTTYPEEXISTSORNOT = "USP_CHECKPAYMENTTYPEEXISTSORNOT";
        private static string PROC_USP_UpdateBatchStatus = "USP_UpdateBatchStatus";
        private static string PROC_USP_UPLOADMETERREADINGS = "USP_InsertMeterReadingFromExcel";
        //private static string PROC_USP_GETFULLUPLOADMETERREADINGS = "USP_GetAllInfoXLMeterReading";
        private static string PROC_USP_GETFULLUPLOADMETERREADINGS = "USP_GetAllInfoXLMeterReading_New";
        private static string PROC_USP_UpdateBatchTotal = "USP_UpdateBatchTotal";

        private static string PROC_USP_IsBatchNoExists = "USP_IsBatchNoExists";
        private static string PROC_USP_ISACTIVEBILLMONTH = "USP_ISACTIVEBILLMONTH";
        private static string PROC_USP_GetAllAccNoInfoXLMeterReadings = "USP_GetAllAccNoInfoXLMeterReadings";
        private static string PROC_USP_InsertDirectCustomerBillReading = "USP_InsertDirectCustomerBillReading";
        private string PROC_USP_GetCashiersByBU = "USP_GetCashiersByBU";

        private string PROC_USP_InsertMeterReadings_Bulk = "USP_InsertMeterReadings_Bulk";//Karteek
        private string PROC_USP_GetAllInfoXLMeterReading_WithDT = "USP_GetAllInfoXLMeterReading_WithDT";//Karteek
        private string PROC_USP_InsertBulkMeterReadings_WithDT = "USP_InsertBulkMeterReadings_WithDT";//Karteek

        private string PROC_USP_GetConsumptionAvgUploadData_WithDT = "USP_GetConsumptionAvgUploadData_WithDT";//Karteek
        private string PROC_USP_InsertBulkAgerageUpload_WithDT = "USP_InsertBulkAgerageUpload_WithDT";//Karteek
        private string PROC_USP_InsertPaymentBatchUploadDetails = "USP_InsertPaymentBatchUploadDetails";//Karteek
        private string PROC_USP_InsertPaymentUploadTransactionDetails = "USP_InsertPaymentUploadTransactionDetails";//Karteek
        private string PROC_USP_InsertMeterReadingBatchUploadDetails = "USP_InsertMeterReadingBatchUploadDetails";//Bhimaraju
        private string PROC_USP_InsertAverageConsumptionBatchUploadDetails = "USP_InsertAverageConsumptionBatchUploadDetails";//Faiz-ID103
        private string PROC_USP_InsertReadingUploadTransactionDetails = "USP_InsertReadingUploadTransactionDetails";//Faiz-ID103
        private string PROC_USP_InsertAverageUploadTransactionDetails = "USP_InsertAverageUploadTransactionDetails";//Faiz-ID103

        private string PROC_USP_InsertBookwiseMeterReadings_WithDT = "USP_InsertBookwiseMeterReadings_WithDT";//Karteek

        #endregion

        #region Methods

        public XmlDocument GetPreviousCustomerReadings(XmlDocument xmlCustomerDetails)
        {
            try
            {
                return _ObjiDsignDAL.ExecuteXmlReader(xmlCustomerDetails, PROC_USP_GETBILLPREVIOUSREADING, con, Constants.CommandTimeOut);
                // return objCommonDal.ExecuteReader_Get(xmlCustomerDetails, PROC_USP_GETBILLPREVIOUSREADING, con, 300);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetPreviousCustomerReadingsBookwise(XmlDocument xmlCustomerDetails)
        {
            try
            {
                return _ObjiDsignDAL.ExecuteXmlReader(xmlCustomerDetails, PROC_USP_GETBILLPREVIOUSREADING_BOOKWISE, con, Constants.CommandTimeOut);
                // return objCommonDal.ExecuteReader_Get(xmlCustomerDetails, PROC_USP_GETBILLPREVIOUSREADING, con, 300);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument InsertBulkReadings(XmlDocument XmlDoc, XmlDocument multiXmlDoc, Statement action)
        {
            XmlDocument xml = new XmlDocument();
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objCommonDal.ExecuteXmlReader(XmlDoc, multiXmlDoc, PROC_USP_InsertMeterReadings_Bulk, con, Constants.CommandTimeOut);
                        break;
                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlCustomerDetails"></param>
        /// <param name="Act"></param>
        /// <returns></returns>
        public XmlDocument InsertCustomerBillReading(XmlDocument xmlCustomerDetails, Statement Act)
        {
            XmlDocument ResultXml = new XmlDocument();
            switch (Act)
            {
                case Statement.Insert:
                    ResultXml = _ObjiDsignDAL.ExecuteXmlReader(xmlCustomerDetails, PROC_USP_INSERTBILLREADING, con, Constants.CommandTimeOut); break;
                case Statement.Update:
                    ResultXml = _ObjiDsignDAL.ExecuteXmlReader(xmlCustomerDetails, PROC_USP_UPDATEBILLREADING, con, Constants.CommandTimeOut);
                    break;
                case Statement.Change:
                    ResultXml = _ObjiDsignDAL.ExecuteXmlReader(xmlCustomerDetails, PROC_USP_GETMETERDECIMALVALUES, con);
                    break;
                case Statement.Edit:
                    ResultXml = _ObjiDsignDAL.ExecuteXmlReader(xmlCustomerDetails, PROC_USP_InsertDirectCustomerBillReading, con, Constants.CommandTimeOut);
                    break;
                case Statement.Generate:
                    ResultXml = _ObjiDsignDAL.ExecuteXmlReader(xmlCustomerDetails, PROC_USP_InsertCustomerMeterReadingLogs, con, Constants.CommandTimeOut);
                    break;
            }
            return ResultXml;
        }
        public XmlDocument UpdateBatchTotalDAL(XmlDocument xmlBatchDetails, Statement Act)
        {
            XmlDocument ResultXml = new XmlDocument();
            switch (Act)
            {
                case Statement.Update:
                    ResultXml = _ObjiDsignDAL.ExecuteXmlReader(xmlBatchDetails, PROC_USP_UpdateBatchTotal, con);
                    break;
            }
            return ResultXml;
        }

        public bool InsertMeterReadingsDT(string CreatedBy, int MeterReadinsFrom, DataTable dtReadings)
        {
            try
            {
                bool isSuccess = false;

                SqlParameter[] parameters =
                {
                  new SqlParameter("@MeterReadingFrom", SqlDbType.Int){ Value = MeterReadinsFrom },
                  new SqlParameter("@CreateBy", SqlDbType.VarChar,50){ Value = CreatedBy },
                  new SqlParameter("@TempReadings", SqlDbType.Structured){ Value = dtReadings }
                };

                SqlDataReader dr = objCommonDal.ExecuteReader_GetDataReader(PROC_USP_InsertBulkMeterReadings_WithDT, parameters, con, Constants.CommandTimeOut);

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        isSuccess = Convert.ToBoolean(dr["IsSuccess"].ToString());
                    }
                }
                return isSuccess;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con.State == ConnectionState.Open) { con.Close(); }
            }
        }
        public bool InsertBookwiseMeterReadingsDT(string CreatedBy, int MeterReadinsFrom, DataTable dtReadings, bool isFinalApproval, int FunctionId, int ActiveStatusId, string BU_ID)
        {
            try
            {
                bool isSuccess = false;

                SqlParameter[] parameters =
                {
                  new SqlParameter("@MeterReadingFrom", SqlDbType.Int){ Value = MeterReadinsFrom },
                  new SqlParameter("@FunctionId", SqlDbType.Int){ Value = FunctionId },
                  new SqlParameter("@CreateBy", SqlDbType.VarChar,50){ Value = CreatedBy },
                  new SqlParameter("@BU_ID", SqlDbType.VarChar,50){ Value = BU_ID },
                  new SqlParameter("@IsFinalApproval", SqlDbType.Bit){ Value = isFinalApproval },
                  new SqlParameter("@ActiveStatusId", SqlDbType.Int){ Value = ActiveStatusId },
                  new SqlParameter("@TempReadings", SqlDbType.Structured){ Value = dtReadings }
                };

                SqlDataReader dr = objCommonDal.ExecuteReader_GetDataReader(PROC_USP_InsertBookwiseMeterReadings_WithDT, parameters, con, Constants.CommandTimeOut);

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        isSuccess = Convert.ToBoolean(dr["IsSuccess"].ToString());
                    }
                }
                return isSuccess;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con.State == ConnectionState.Open) { con.Close(); }
            }
        }
        public bool InsertBatchPaymentUploadDT(int BatchNo, string BatchName, string BatchDate, string CreatedBy, string Description, string BU_ID, int TotalCustomers, string FilePath, DataTable dtPayments, out bool isExists)
        {
            try
            {
                bool isSuccess = false;
                isExists = false;

                SqlParameter[] parameters =
                {
                  new SqlParameter("@BatchNo", SqlDbType.Int){ Value = BatchNo },
                  new SqlParameter("@BatchName", SqlDbType.VarChar,50){ Value = BatchName },
                  new SqlParameter("@BatchDate", SqlDbType.VarChar,50){ Value = BatchDate },
                  new SqlParameter("@CreatedBy", SqlDbType.VarChar,50){ Value = CreatedBy },
                  new SqlParameter("@Description", SqlDbType.VarChar){ Value = Description },
                  new SqlParameter("@BU_ID", SqlDbType.VarChar,50){ Value = BU_ID },
                  new SqlParameter("@TotalCustomers", SqlDbType.Int){ Value = TotalCustomers },
                  new SqlParameter("@FilePath", SqlDbType.VarChar){ Value = FilePath },
                  new SqlParameter("@TempPayments", SqlDbType.Structured){ Value = dtPayments }
                };

                SqlDataReader dr = objCommonDal.ExecuteReader_GetDataReader(PROC_USP_InsertPaymentBatchUploadDetails, parameters, con, Constants.CommandTimeOut);

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        isSuccess = Convert.ToBoolean(dr["IsSuccess"].ToString());
                        isExists = Convert.ToBoolean(dr["IsExists"].ToString());
                    }
                }
                return isSuccess;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con.State == ConnectionState.Open) { con.Close(); }
            }
        }
        public bool InsertBatchMeterReadingUploadDT(int BatchNo, string BatchName, string BatchDate, string CreatedBy, string Description, int TotalCustomers, string FilePath, DataTable dtMeterReadings, string BU_ID, out bool isExists)
        {
            try
            {
                bool isSuccess = false;
                isExists = false;

                SqlParameter[] parameters =
                {
                  new SqlParameter("@BatchNo", SqlDbType.Int){ Value = BatchNo },
                  new SqlParameter("@BatchName", SqlDbType.VarChar,50){ Value = BatchName },
                  new SqlParameter("@BatchDate", SqlDbType.VarChar,50){ Value = BatchDate },
                  new SqlParameter("@CreatedBy", SqlDbType.VarChar,50){ Value = CreatedBy },
                  new SqlParameter("@Description", SqlDbType.VarChar){ Value = Description },
                  new SqlParameter("@TotalCustomers", SqlDbType.Int){ Value = TotalCustomers },
                  new SqlParameter("@FilePath", SqlDbType.VarChar){ Value = FilePath },
                  new SqlParameter("@BU_ID", SqlDbType.VarChar){ Value = BU_ID },
                  new SqlParameter("@TempReadings", SqlDbType.Structured){ Value = dtMeterReadings }
                };

                SqlDataReader dr = objCommonDal.ExecuteReader_GetDataReader(PROC_USP_InsertMeterReadingBatchUploadDetails, parameters, con, Constants.CommandTimeOut);

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        isSuccess = Convert.ToBoolean(dr["IsSuccess"].ToString());
                        isExists = Convert.ToBoolean(dr["IsExists"].ToString());
                    }
                }
                return isSuccess;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con.State == ConnectionState.Open) { con.Close(); }
            }
        }

        public bool InsertBatchAverageConsumptionUploadDT(int BatchNo, string BatchName, string BatchDate, string CreatedBy, string Description, int TotalCustomers, string FilePath, DataTable dtMeterReadings, string BU_ID, out bool isExists)
        {
            try
            {
                bool isSuccess = false;
                isExists = false;

                SqlParameter[] parameters =
                {
                  new SqlParameter("@BatchNo", SqlDbType.Int){ Value = BatchNo },
                  new SqlParameter("@BatchName", SqlDbType.VarChar,50){ Value = BatchName },
                  new SqlParameter("@BatchDate", SqlDbType.VarChar,50){ Value = BatchDate },
                  new SqlParameter("@CreatedBy", SqlDbType.VarChar,50){ Value = CreatedBy },
                  new SqlParameter("@BU_ID", SqlDbType.VarChar,50){ Value = BU_ID },
                  new SqlParameter("@Description", SqlDbType.VarChar){ Value = Description },
                  new SqlParameter("@TotalCustomers", SqlDbType.Int){ Value = TotalCustomers },
                  new SqlParameter("@FilePath", SqlDbType.VarChar){ Value = FilePath },
                  new SqlParameter("@TempReadings", SqlDbType.Structured){ Value = dtMeterReadings }
                };

                SqlDataReader dr = objCommonDal.ExecuteReader_GetDataReader(PROC_USP_InsertAverageConsumptionBatchUploadDetails, parameters, con, Constants.CommandTimeOut);

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        isSuccess = Convert.ToBoolean(dr["IsSuccess"].ToString());
                        isExists = Convert.ToBoolean(dr["IsExists"].ToString());
                    }
                }
                return isSuccess;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con.State == ConnectionState.Open) { con.Close(); }
            }
        }

        public bool InsertPaymentUploadTransactionDT(int BatchNo, int UploadBatchId, string CreatedBy, int TotalCustomers, string FilePath, DataTable dtPayments)
        {
            try
            {
                bool isSuccess = false;

                SqlParameter[] parameters =
                {
                  new SqlParameter("@BatchNo", SqlDbType.Int){ Value = BatchNo },
                  new SqlParameter("@UploadBatchId", SqlDbType.Int){ Value = UploadBatchId },
                  new SqlParameter("@CreatedBy", SqlDbType.VarChar,50){ Value = CreatedBy },
                  new SqlParameter("@TotalCustomers", SqlDbType.Int){ Value = TotalCustomers },
                  new SqlParameter("@FilePath", SqlDbType.VarChar){ Value = FilePath },
                  new SqlParameter("@TempPayments", SqlDbType.Structured){ Value = dtPayments }
                };

                SqlDataReader dr = objCommonDal.ExecuteReader_GetDataReader(PROC_USP_InsertPaymentUploadTransactionDetails, parameters, con, Constants.CommandTimeOut);

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        isSuccess = Convert.ToBoolean(dr["IsSuccess"].ToString());
                    }
                }
                return isSuccess;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con.State == ConnectionState.Open) { con.Close(); }
            }
        }

        public bool InsertReadingUploadTransactionDT(int BatchNo, int UploadBatchId, string CreatedBy, int TotalCustomers, string FilePath, DataTable dtPayments)
        {
            try
            {
                bool isSuccess = false;

                SqlParameter[] parameters =
                {
                  new SqlParameter("@BatchNo", SqlDbType.Int){ Value = BatchNo },
                  new SqlParameter("@UploadBatchId", SqlDbType.Int){ Value = UploadBatchId },
                  new SqlParameter("@CreatedBy", SqlDbType.VarChar,50){ Value = CreatedBy },
                  new SqlParameter("@TotalCustomers", SqlDbType.Int){ Value = TotalCustomers },
                  new SqlParameter("@FilePath", SqlDbType.VarChar){ Value = FilePath },
                  new SqlParameter("@TempReadings", SqlDbType.Structured){ Value = dtPayments }
                };

                SqlDataReader dr = objCommonDal.ExecuteReader_GetDataReader(PROC_USP_InsertReadingUploadTransactionDetails, parameters, con, Constants.CommandTimeOut);

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        isSuccess = Convert.ToBoolean(dr["IsSuccess"].ToString());
                    }
                }
                return isSuccess;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con.State == ConnectionState.Open) { con.Close(); }
            }
        }

        public bool InsertAverageUploadTransactionDT(int BatchNo, int UploadBatchId, string CreatedBy, int TotalCustomers, string FilePath, DataTable dtReadings)
        {
            try
            {
                bool isSuccess = false;

                SqlParameter[] parameters =
                {
                  new SqlParameter("@BatchNo", SqlDbType.Int){ Value = BatchNo },
                  new SqlParameter("@UploadBatchId", SqlDbType.Int){ Value = UploadBatchId },
                  new SqlParameter("@CreatedBy", SqlDbType.VarChar,50){ Value = CreatedBy },
                  new SqlParameter("@TotalCustomers", SqlDbType.Int){ Value = TotalCustomers },
                  new SqlParameter("@FilePath", SqlDbType.VarChar){ Value = FilePath },
                  new SqlParameter("@TempReadings", SqlDbType.Structured){ Value = dtReadings }
                };

                SqlDataReader dr = objCommonDal.ExecuteReader_GetDataReader(PROC_USP_InsertAverageUploadTransactionDetails, parameters, con, Constants.CommandTimeOut);

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        isSuccess = Convert.ToBoolean(dr["IsSuccess"].ToString());
                    }
                }
                return isSuccess;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con.State == ConnectionState.Open) { con.Close(); }
            }
        }

        #region BatchEntry

        public XmlDocument GetCashierDAL()
        {
            return _ObjiDsignDAL.ExecuteXmlReader(PROC_USP_GETCASHIERS, con);  //Get Cashier
        }

        public XmlDocument GetCashierDAL(XmlDocument xml)
        {
            return _ObjiDsignDAL.ExecuteXmlReader(xml, PROC_USP_GetCashiersByBU, con);  //Get Cashier
        }

        public XmlDocument GetCashierOfficesDAL()
        {
            return _ObjiDsignDAL.ExecuteXmlReader(PROC_USP_GETOFFICES, con);  //Get offices
        }

        public XmlDocument BatchEntry(XmlDocument XmlDoc, Statement value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case Statement.Insert:
                        xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTBATCHDETAILS, con);
                        break;//Get BatchEntry 
                    case Statement.Check:
                        xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_ISACCOUNTNOEXSIT, con);
                        break;
                    case Statement.Update:
                        xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateBatchAmountByBatchNo, con);
                        break;
                    case Statement.Edit:
                        xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateBatchStatus, con);
                        break;
                    case Statement.Service:
                        xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTBATCHDETAILS, con);
                        break;//Get BatchEntry 
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument PresentReadingAdjustment(XmlDocument XmlDoc, Statement value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case Statement.Insert:
                        xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertCustomerMeterReadingAdjustment, con);
                        break;//Insert Customer Present Adjustment 
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument Get(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetBatchDetailsByBatchNo, con);
                        break;
                    case ReturnType.Group:
                        xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CHECKPAYMENTTYPEEXISTSORNOT, con);
                        break;
                    case ReturnType.Check:
                        xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_ISACTIVEBILLMONTH, con);
                        break;
                    case ReturnType.List:
                        xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETOFFICES, con);
                        break;
                    case ReturnType.Multiple:
                        xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCashOffices_ByBU_ID, con);
                        break;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument Batch(XmlDocument XmlDoc, Statement value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case Statement.Check:
                        xml = _ObjiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsBatchNoExists, con);
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        #endregion

        #region DocumentPayments

        public XmlDocument Insert(XmlDocument multiXmlDoc, Statement action)
        {
            XmlDocument xml = new XmlDocument();

            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = _ObjiDsignDAL.ExecuteXmlReader(PROC_USP_UPLOADMETERREADINGS, multiXmlDoc, con);
                        break;
                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetFullInfoXL(XmlDocument multiXmlDoc)
        {
            return objCommonDal.ExecuteXmlReader(multiXmlDoc, PROC_USP_GETFULLUPLOADMETERREADINGS, con, Constants.CommandTimeOut);
        }

        public XmlDocument GetDirectReadingsInfoXL(XmlDocument multiXmlDoc)
        {
            return objCommonDal.ExecuteXmlReader(multiXmlDoc, PROC_USP_GetAllAccNoInfoXLMeterReadings, con, Constants.CommandTimeOut);
        }

        public DataSet GetValidateReadingsUpload(DataTable dtReadings)
        {
            SqlParameter[] parameters =
            {
              new SqlParameter("@TempCustomer", SqlDbType.Structured){ Value = dtReadings }
            };
            return objCommonDal.ExecuteReader_GetDataSet(PROC_USP_GetAllInfoXLMeterReading_WithDT, parameters, con, Constants.CommandTimeOut);
        }

        public DataSet GetValidateAvgConsumptionUpload(DataTable dtReadings)
        {
            SqlParameter[] parameters =
            {
              new SqlParameter("@TblConcumption", SqlDbType.Structured){ Value = dtReadings }
            };
            return objCommonDal.ExecuteReader_GetDataSet(PROC_USP_GetConsumptionAvgUploadData_WithDT, parameters, con, Constants.CommandTimeOut);
        }

        public int InsertAverageUploadDT(DataTable dtReadings)
        {
            try
            {
                int count = 0;

                SqlParameter[] parameters =
                {
                  new SqlParameter("@TblAvgUpload", SqlDbType.Structured){ Value = dtReadings }
                };

                SqlDataReader dr = objCommonDal.ExecuteReader_GetDataReader(PROC_USP_InsertBulkAgerageUpload_WithDT, parameters, con, Constants.CommandTimeOut);

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        count = Convert.ToInt32(dr["IsSuccess"].ToString());
                    }
                }
                return count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (con.State == ConnectionState.Open) { con.Close(); }
            }
        }

        #endregion

        #endregion
    }
}
