﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Data;

namespace UMS_NigriaDAL
{
    public class BillingDisabledBooksDal
    {
        #region Members
        iDsignDAL objiDsignDAL = new iDsignDAL();
        CommonDAL objCommonDal = new CommonDAL();
        SqlConnection SqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        #endregion

        #region SP_Reports
        private string PROC_USP_InsertBillingDisabledBookNos = "USP_InsertBillingDisabledBookNos";
        private string PROC_USP_GetBillingDisabledBookNo = "USP_GetBillingDisabledBookNo";
        private string PROC_USP_UpdateDisabledBookNoStatus = "USP_UpdateDisabledBookNoStatus";
        private string PROC_USP_GetDisableTypes = "USP_GetDisableTypes";
        #endregion

        #region Methods
        public XmlDocument Insert(XmlDocument XmlDoc, Statement value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertBillingDisabledBookNos, SqlCon); break;
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateDisabledBookNoStatus, SqlCon); break;
                    case Statement.Generate:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetDisableTypes, SqlCon); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;
        }
        public DataSet GetDisabledBooks(XmlDocument XmlDoc)
        {
            DataSet ds = new DataSet();
            try
            {
                ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GetBillingDisabledBookNo, SqlCon, 300);
                return ds;
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        #endregion
    }
}
