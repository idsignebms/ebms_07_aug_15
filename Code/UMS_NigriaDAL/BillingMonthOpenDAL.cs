﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using UMS_NigeriaBE;
using System.Xml;
using System.Data.SqlClient;
using System.Configuration;

namespace UMS_NigriaDAL
{
    public class BillingMonthOpenDAL
    {
        #region Members
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL objiDsignDAL = new iDsignDAL();
        #endregion

        #region StoredProcedures

        private static string PROC_USP_GetMonthOpenList = "USP_GetMonthOpenList";
        private static string PROC_USP_InsertBillingMonthOpen = "USP_InsertBillingMonthOpen";
        private static string PROC_USP_ChangeGlobalStatusLock = "USP_ChangeGlobalStatusLock";
        private static string PROC_USP_GetBilledMonthLockStatus = "USP_GetBilledMonthLockStatus";
        private static string PROC_USP_ChangeBilledMonthLockStatus = "USP_ChangeBilledMonthLockStatus";
        //private static string PROC_USP_Insert_PDFReportData = "USP_Insert_PDFReportData";
        private static string PROC_USP_Update_PDFReportData = "USP_Update_PDFReportData";


        private static string PROC_USP_ISPREVIOUSBILLMONTHOPEN = "USP_ISPREVIOUSBILLMONTHOPEN";///Suresh
        private static string PROC_USP_ISBILLMONTHOPENED = "USP_ISBILLMONTHOPENED";
        private static string PROC_USP_IsGreaterBillMonthOpen = "USP_IsGreaterBillMonthOpen";

        #endregion

        #region Methods

        #region Methods For Billing Pages

        public XmlDocument InsertBillingMonth(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_InsertBillingMonthOpen, con); break;//Insert
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_ChangeGlobalStatusLock, con, Constants.CommandTimeOut); break;
                    case Statement.Change:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_ChangeBilledMonthLockStatus, con); break;
                    case Statement.Modify:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_Update_PDFReportData, con); break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }


        public XmlDocument GetBillingMonth(XmlDocument xmlDoc, ReturnType value)
        {
            XmlDocument xml = new XmlDocument();

            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(xmlDoc, PROC_USP_GetMonthOpenList, con); break; //Get List For Grid
                    case ReturnType.Fetch:
                        xml = objiDsignDAL.ExecuteXmlReader(xmlDoc, PROC_USP_GetBilledMonthLockStatus, con); break; //Get List For Grid
                    case ReturnType.Check:
                        xml = objiDsignDAL.ExecuteXmlReader(xmlDoc, PROC_USP_ISPREVIOUSBILLMONTHOPEN, con);
                        break;
                    case ReturnType.Single:
                        xml = objiDsignDAL.ExecuteXmlReader(xmlDoc, PROC_USP_ISBILLMONTHOPENED, con);
                        break;
                    case ReturnType.Double:
                        xml = objiDsignDAL.ExecuteXmlReader(xmlDoc, PROC_USP_IsGreaterBillMonthOpen, con);
                        break;
                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion
    }
}
