﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;

namespace UMS_NigriaDAL
{
    public class BookNoReorderDal
    {
        #region Members
        iDsignDAL objiDsignDAL = new iDsignDAL();
        SqlConnection SqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        #endregion

        #region Stored Procedures
        private string PROC_USP_GetBookNoReOrderList = "USP_GetBookNoReOrderList";
        private string PROC_USP_UpdateBookNumbersOrder = "USP_UpdateBookNumbersOrder";
        private string PROC_USP_UpdateCustomersOrder = "USP_UpdateCustomersOrder";
        private string PROC_USP_GetCustomersReOrderList = "USP_GetCustomersReOrderList";
        #endregion

        #region Methods
        public XmlDocument Get(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetBookNoReOrderList, SqlCon); break;
                    case ReturnType.Fetch:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomersReOrderList, SqlCon); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;

        }

        public XmlDocument Insert(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateBookNumbersOrder, SqlCon); break;
                    case Statement.Change:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateCustomersOrder, SqlCon); break;
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return xml;

        }
        #endregion
    }
}
