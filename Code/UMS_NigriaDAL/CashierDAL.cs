﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using iDSignHelper;
using System.Configuration;
using System.Xml;

namespace UMS_NigriaDAL
{
    public class CashierDAL
    {
        #region Members
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL objiDsignDAL = new iDsignDAL();
        CommonDAL objCommonDal = new CommonDAL();
        #endregion

        #region StoredProcedures
        public static string PROC_USP_INSERTCASHIERS = "USP_InsertCashiers";
        public static string PROC_USP_UPDATECASHIERS = "USP_UpdateCashiers";
        public static string PROC_USP_DELETECASHIERS = "USP_DeleteCashiers";
        public static string PROC_USP_GETCASHIERSFORDDL = "USP_GetCashiersForDdl";

        public static string PROC_USP_GETCASHIERSLIST = "USP_GetCashiersList";
        public static string PROC_USP_GetCustomerCashiers_ByBookNo = "USP_GetCustomerCashiers_ByBookNo";

        #endregion

        #region Methods
        public XmlDocument InsertCashier(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_INSERTCASHIERS, con); break;//Insert Cashier 
                    case Statement.Update:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UPDATECASHIERS, con); break;//Update Cashier
                    case Statement.Delete:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_DELETECASHIERS, con); break;//Update Cashier
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument GetCashier(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETCASHIERSFORDDL, con); break;//Get Cashier for DDL
                    case ReturnType.List:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETCASHIERSLIST, con); break;// for Grid
                    case ReturnType.Single:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerCashiers_ByBookNo, con); break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        #endregion
    }
}
