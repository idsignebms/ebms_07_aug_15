﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using iDSignHelper;
using System.Configuration;
using System.Xml;
using System.Data;

namespace UMS_NigriaDAL
{
    public class ChangeBookNoDal
    {
        #region Members
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL _objiDsignDAL = new iDsignDAL();
        CommonDAL objCommonDal = new CommonDAL();
        #endregion

        #region Sps

        private static string PROC_USP_GETCUSTDETFORBOOKNOCHANGEBYACCNO = "USP_GetCustDetForBookNoChangeByAccno";
        private static string PROC_USP_CHANGECUSTOMERBOOKNO = "USP_ChangeCustomerBookNo";
        private static string PROC_USP_CHANGECUSTOMERNAME = "USP_ChangeCustomerName";
        private static string PROC_USP_CHANGECUSTOMERADDRESS = "USP_ChangeCustomerAddress";
        private static string PROC_USP_CHANGECUSTOMERACTIVESTATUS = "USP_ChangeCustomerActiveStatus";
        private static string PROC_USP_CHANGECUSTOMERMETERINFORMATION = "USP_ChangeCustomerMeterInformation";
        private static string PROC_USP_GETMACTIVESTATUSDETAILS = "USP_GetMActiveStatusDetails";
        private static string PROC_USP_GETACTIVESTATUSDETAILSFORCUSTOMER = "USP_GetActiveStatusDetailsForCustomer";
        private static string PROC_USP_GETCUSTOMERNAMECHANGELOGSTOAPPROVE = "USP_GetCustomerNameChangeLogsToApprove";
        private static string PROC_USP_CUSTOMERNAMECHANGEAPPROVAL = "USP_CustomerNameChangeApproval";
        private static string PROC_USP_GETCUSTOMERSTATUSCHANGELOGSTOAPPROVE = "USP_GetCustomerStatusChangeLogsToApprove";
        private static string PROC_USP_CUSTOMERSTATUSCHANGEAPPROVAL = "USP_CustomerStatusChangeApproval";
        private static string PROC_USP_GETMETERNOCHANGELOGSTOAPPROVE = "USP_GetMeterNoChangeLogsToApprove";
        private static string PROC_USP_CUSTOMERMETERCHANGEAPPROVAL = "USP_CustomerMeterChangeApproval";
        private static string PROC_USP_CUSTOMERADDRESSCHANGEAPPROVAL = "USP_CustomerAddressChangeApproval";
        private static string PROC_USP_CUSTOMERREADTODIRECTCHANGEAPPROVAL = "USP_CustomerReadToDirectChangeApproval";
        

        private string PROC_USP_GetDirectCustomerAverageUpload = "USP_GetDirectCustomerAverageUpload";
        private string PROC_USP_UpdateDirectCustomerAverageUpload = "USP_UpdateDirectCustomerAverageUpload";

        //Change Customer Cantact Info
        private static string PROC_USP_CHANGECUSTOMERCONTACTINFO = "USP_ChangeCustomerContactInfo";
        private static string PROC_USP_GETCUSTDETFORCONTACTINFOBYACCNO = "USP_GetCustDetForContactInfoByAccno";
        private string USP_GetCustDetForContactInfoByAccno_ByCheck = "USP_GetCustDetForContactInfoByAccno_ByCheck";

        private string USP_GetCustDetForNameChangeByAccno_ByCheck = "USP_GetCustDetForNameChangeByAccno_ByCheck";
        
        #endregion

        #region Methods

        public XmlDocument Get(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get: //Get Details Of Customer For BookNo Change
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETCUSTDETFORBOOKNOCHANGEBYACCNO, con); break;
                    case ReturnType.List: //Get Details Of Customer For BookNo Change
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETMACTIVESTATUSDETAILS, con); break;
                    case ReturnType.Bulk: //Get Details Of Customer For BookNo Change
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETACTIVESTATUSDETAILSFORCUSTOMER, con); break;

                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public XmlDocument GetCustomerDetailsForNameChangeByAccno_ByCheck(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, USP_GetCustDetForNameChangeByAccno_ByCheck, con);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument Insert(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    //case Statement.Insert: //Insert Details Of Customer BookNo Change
                    //xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CHANGECUSTOMERBOOKNO, con); break;
                    case Statement.Update: //Update Details Of Customer Name Change
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CHANGECUSTOMERNAME, con); break;
                    case Statement.Change: //Change Details Of Customer Address Change
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CHANGECUSTOMERADDRESS, con); break;
                    case Statement.Check: //Change Details Of Customer ActiveStatus Change
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CHANGECUSTOMERACTIVESTATUS, con); break;
                    case Statement.Edit: //Change Details Of Customer MeterNo Change
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CHANGECUSTOMERMETERINFORMATION, con); break;
                    case Statement.Modify:
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CUSTOMERNAMECHANGEAPPROVAL, con); break;
                    case Statement.Insert:
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CUSTOMERSTATUSCHANGEAPPROVAL, con); break;
                    case Statement.Service:
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CUSTOMERMETERCHANGEAPPROVAL, con); break;
                    case Statement.Generate :
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CUSTOMERADDRESSCHANGEAPPROVAL, con); break;
                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument InsertApprovalDetails(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert: //Update Details Of Customer Read To Direct Change
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CUSTOMERREADTODIRECTCHANGEAPPROVAL, con); break;
                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument InsertDetails(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert:
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CHANGECUSTOMERCONTACTINFO, con); break;
                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument GetDetails(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get: //Get Details Of Customer For Cantact Info Change
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETCUSTDETFORCONTACTINFOBYACCNO, con); break;
                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetNameChangesApproveList(XmlDocument XmlDoc)
        {
            DataSet ds = null;
            ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GETCUSTOMERNAMECHANGELOGSTOAPPROVE, con);
            return ds;
        }

        public DataSet GetStatusChangesApproveList(XmlDocument XmlDoc)
        {
            DataSet ds = null;
            ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GETCUSTOMERSTATUSCHANGELOGSTOAPPROVE, con);
            return ds;
        }

        public DataSet GetMeterNoChangesApproveList(XmlDocument XmlDoc)
        {
            DataSet ds = null;
            ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GETMETERNOCHANGELOGSTOAPPROVE, con);
            return ds;
        }

        public XmlDocument GetDirectCustomerAverageUploadBE(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetDirectCustomerAverageUpload, con); 
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument UpdateDirectCustomerAverageUpload(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateDirectCustomerAverageUpload, con); 
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument GetCustDetForContactInfoByAccno_ByCheck(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, USP_GetCustDetForContactInfoByAccno_ByCheck, con);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion
    }
}
