﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iDSignHelper;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Configuration;

namespace UMS_NigriaDAL
{
    public class ChangeCusotmerTypeDAL
    {
        #region Members
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL _objiDsignDAL = new iDsignDAL();
        CommonDAL objCommonDal = new CommonDAL();
        #endregion

        #region Stored Procedures
        private string PROC_USP_GETCUSTOMERFORCUSTTYPECHANGE = "USP_GetCustomerForCustTypeChange";
        private string PROC_USP_CHANGECUSTOMERTYPE = "USP_ChangeCustomerType";
        private string PROC_USP_GETCUSTOMERTYPECHANGELOGSTOAPPROVE = "USP_GetCustomerTypeChangeLogsToApprove";
        private string PROC_USP_CUSTOMERTYPECHANGEAPPROVAL = "USP_CustomerTypeChangeApproval";
        private string PROC_USP_GETCUSTOMERREADTODIRECTCHANGELOGSTOAPPROVE = "USP_GetCustomerReadToDirectChangeLogsToApprove";
        private string PROC_USP_GETCUSTOMERADDRESSCHANGELOGSTOAPPROVE = "USP_GetCustomerAddressChangeLogsToApprove";
        private string PROC_USP_GETCUSTOMERASSIGNMETERLOGSTOAPPROVE = "USP_GetCustomerAssignMeterLogsToApprove";
        private string PROC_USP_CUSTOMERASSIGNMETERCHANGEAPPROVAL = "USP_CustomerAssignMeterChangeApproval";
        private string PROC_USP_GETCUSTOMERBOOKNOCHANGELOGSTOAPPROVE = "USP_GetCustomerBookNoChangeLogsToApprove";
        private string PROC_USP_CUSTOMERBOOKNOCHANGEAPPROVAL = "USP_CustomerBookNoChangeApproval";
        private string PROC_USP_GETMETERREADINGLOGSTOAPPROVE = "USP_GetMeterReadingLogsToApprove";
        private string PROC_USP_GETCUSTOMERPAYMENTLOGSTOAPPROVE = "USP_GetCustomerPaymentLogsToApprove";
        private string PROC_USP_GETDIRECTCUSTOMERSAVERAGEUPLOADLOGSTOAPPROVE = "USP_GetDirectCustomersAverageUploadLogsToApprove";
        private string PROC_USP_CUSTOMERDIRECTAVERAGECHANGEAPPROVAL = "USP_CustomerDirectAverageChangeApproval";
        private string PROC_USP_CUSTOMERPAYMENTSAPPROVAL = "USP_CustomerPaymentsApproval";
        private string PROC_USP_GETCUSTOMERADJUSTMENTLOGSTOAPPROVE = "USP_GetCustomerAdjustmentLogsToApprove";
        private string PROC_USP_BILLADJUSTMENTCHANGEAPPROVAL = "USP_BillAdjustmentChangeApproval";
        private string PROC_USP_CUSTOMERREADINGSCHANGEAPPROVAL = "USP_CustomerReadingsChangeApproval";
        private string PROC_USP_CUSTOMERPRESENTREADINGADJUSTMENTAPPROVAL = "USP_CustomerPresentReadingAdjustmentApproval";//Bhimaraju
        private string PROC_USP_GETCUSTOMERPRESENTREADINGCHANGELOGSTOAPPROVE = "USP_GetCustomerPresentReadingChangeLogsToApprove";//Bhimaraju

        private string PROC_USP_ISPROCESSEDMETERREADINGAPPROVAL = "USP_IsProcessedMeterReadingApproval";//Karteek
        private string PROC_USP_GetCustomerForCustTypeChange_ByCheck = "USP_GetCustomerForCustTypeChange_ByCheck";
        private string PROC_USP_USP_ApprovalRegistrationInsert = "CUSTOMERS.USP_ApprovalRegistrationInsert";
        private string PROC_USP_GetApprovalRegistrations = "USP_GetApprovalRegistrations";
        #endregion

        public XmlDocument Get(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get: //Get Details Of Customer For BookNo Change
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GETCUSTOMERFORCUSTTYPECHANGE, con); break;
                    case ReturnType.Check:
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_ISPROCESSEDMETERREADINGAPPROVAL, con); break;
                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument CustomerType(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Update: //Update Details Of Customer Type Change
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CHANGECUSTOMERTYPE, con); break;
                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument Insert(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Modify:
                        xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CUSTOMERTYPECHANGEAPPROVAL, con); break;
                }
                return xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetTypeChangesApproveList(XmlDocument XmlDoc)
        {
            DataSet ds = null;
            ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GETCUSTOMERTYPECHANGELOGSTOAPPROVE, con);
            return ds;
        }

        public DataSet GetBindApproveList(XmlDocument XmlDoc, ReturnType value)
        {
            DataSet ds = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get: //Get Approval Details Of Customer For Address Change
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GETCUSTOMERADDRESSCHANGELOGSTOAPPROVE, con); break;
                    case ReturnType.Bulk: //Get Approval Details Of Customer For ReadToDirect Change
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GETCUSTOMERREADTODIRECTCHANGELOGSTOAPPROVE, con); break;
                    case ReturnType.Check: //Get Approval Details Of Customer For AssignMeter Change
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GETCUSTOMERASSIGNMETERLOGSTOAPPROVE, con); break;
                    case ReturnType.Data: //Get Approval Details Of Customer For Book Change
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GETCUSTOMERBOOKNOCHANGELOGSTOAPPROVE, con); break;
                    case ReturnType.Double: //Get Approval Details Of Customer For MeterReading
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GETMETERREADINGLOGSTOAPPROVE, con); break;
                    case ReturnType.Fetch: //Get Approval Details Of Customer For Payments
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GETCUSTOMERPAYMENTLOGSTOAPPROVE, con); break;
                    case ReturnType.Group: //Get Approval Details Of Customer For Direct Customer Average Upload
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GETDIRECTCUSTOMERSAVERAGEUPLOADLOGSTOAPPROVE, con); break;
                    case ReturnType.List: //Get Approval Details Of Customer For Bill Adjustments
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GETCUSTOMERADJUSTMENTLOGSTOAPPROVE, con); break;
                    case ReturnType.User:
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GetApprovalRegistrations, con); break;
                    case ReturnType.Multiple: //Get Approval Details Of Customer For Meter Reading Change
                        ds = objCommonDal.ExecuteReader_Get(XmlDoc, PROC_USP_GETCUSTOMERPRESENTREADINGCHANGELOGSTOAPPROVE, con); break;
                }
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public XmlDocument InsertApproveList(XmlDocument XmlDoc, Statement action)
        {
            XmlDocument Xml = null;
            try
            {
                switch (action)
                {
                    case Statement.Insert: //Get Approval Details Of Customer For Address Change
                        Xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CUSTOMERASSIGNMETERCHANGEAPPROVAL, con); break;
                    case Statement.Change: //Get Approval Details Of Customer For Book Change
                        Xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CUSTOMERBOOKNOCHANGEAPPROVAL, con); break;
                    case Statement.Check: //Get Approval Details Of Customer For Meter Reading
                        Xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CUSTOMERREADINGSCHANGEAPPROVAL, con); break;
                    case Statement.Delete: //Get Approval Details Of Customer For Payments
                        Xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CUSTOMERPAYMENTSAPPROVAL, con); break;
                    case Statement.Edit: //Get Approval Details Of Customer For Direct Customer Average Upload
                        Xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CUSTOMERDIRECTAVERAGECHANGEAPPROVAL, con); break;
                    case Statement.Generate: //Get Approval Details Of Customer For Bill Adjustment
                        Xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_BILLADJUSTMENTCHANGEAPPROVAL, con); break;
                    case Statement.Update: //Get Approval Details Of Customer For Bill Adjustment
                        Xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_USP_ApprovalRegistrationInsert, con); break;
                    case Statement.Modify: //Get Approval Details Of Customer For Bill Adjustment
                        Xml = _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_CUSTOMERPRESENTREADINGADJUSTMENTAPPROVAL, con); break;
                }
                return Xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public XmlDocument GetCustDetForTypeChange_ByCheck(XmlDocument XmlDoc)
        {
            return _objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerForCustTypeChange_ByCheck, con);
        }
    }
}
