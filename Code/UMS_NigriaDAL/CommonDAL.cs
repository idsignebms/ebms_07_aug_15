﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace UMS_NigriaDAL
{
    public class CommonDAL
    {
        public DataSet ExecuteReader_Get(XmlDocument xmlDoc, string procedureName, SqlConnection sqlCon)
        {
            DataSet ds = new DataSet();
            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlDataAdapter sqlDap = new SqlDataAdapter(procedureName, sqlCon);
            sqlDap.SelectCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter param1 = new SqlParameter("@XmlDoc", SqlDbType.Xml);
            param1.Value = xmlDoc.InnerXml;
            sqlDap.SelectCommand.Parameters.Add(param1);
            sqlDap.Fill(ds);
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return ds;
        }

        public DataSet ExecuteReader_Get(string procedureName, SqlConnection sqlCon)
        {
            DataSet ds = new DataSet();
            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlDataAdapter sqlDap = new SqlDataAdapter(procedureName, sqlCon);
            sqlDap.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlDap.Fill(ds);
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return ds;
        }

        public SqlDataReader ExecuteDataReader_Get(XmlDocument xmlDoc, string procedureName, SqlConnection sqlCon)
        {
            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlCommand cmd = new SqlCommand(procedureName, sqlCon);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter param1 = new SqlParameter("@XmlDoc", SqlDbType.Xml);
            param1.Value = xmlDoc.InnerXml;
            cmd.Parameters.Add(param1);
            SqlDataReader dr = cmd.ExecuteReader();
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return dr;
        }

        public SqlDataReader ExecuteDataReader_Get(string procedureName, SqlConnection sqlCon)
        {
            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlCommand cmd = new SqlCommand(procedureName, sqlCon);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return dr;
        }

        public DataSet ExecuteReader_Get(XmlDocument xmlDoc, string procedureName, SqlConnection sqlCon, int CommandTimeout)
        {
            DataSet ds = new DataSet();
            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlDataAdapter sqlDap = new SqlDataAdapter(procedureName, sqlCon);
            sqlDap.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlDap.SelectCommand.CommandTimeout = CommandTimeout;
            SqlParameter param1 = new SqlParameter("@XmlDoc", SqlDbType.Xml);
            param1.Value = xmlDoc.InnerXml;
            sqlDap.SelectCommand.Parameters.Add(param1);
            sqlDap.Fill(ds);
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return ds;
        }

        public DataSet ExecuteReader_Get(string procedureName, SqlConnection sqlCon, int CommandTimeout)
        {
            DataSet ds = new DataSet();
            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlDataAdapter sqlDap = new SqlDataAdapter(procedureName, sqlCon);
            sqlDap.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlDap.SelectCommand.CommandTimeout = CommandTimeout;
            sqlDap.Fill(ds);
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return ds;
        }

        public SqlDataReader ExecuteDataReader_Get(XmlDocument xmlDoc, string procedureName, SqlConnection sqlCon, int CommandTimeout)
        {
            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlCommand cmd = new SqlCommand(procedureName, sqlCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = CommandTimeout;
            SqlParameter param1 = new SqlParameter("@XmlDoc", SqlDbType.Xml);
            param1.Value = xmlDoc.InnerXml;
            cmd.Parameters.Add(param1);
            SqlDataReader dr = cmd.ExecuteReader();
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return dr;
        }

        public SqlDataReader ExecuteDataReader_Get(string procedureName, SqlConnection sqlCon, int CommandTimeout)
        {
            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlCommand cmd = new SqlCommand(procedureName, sqlCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = CommandTimeout;
            SqlDataReader dr = cmd.ExecuteReader();
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return dr;
        }

        public DataSet ExecuteReader_GetDataSet(string procedureName, SqlParameter[] parameters, SqlConnection sqlCon, int CommandTimeout)
        {
            DataSet ds = new DataSet();
            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlDataAdapter sqlDap = new SqlDataAdapter(procedureName, sqlCon);
            sqlDap.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlDap.SelectCommand.CommandTimeout = CommandTimeout;
            sqlDap.SelectCommand.Parameters.AddRange(parameters);
            sqlDap.Fill(ds);
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return ds;
        }

        public SqlDataReader ExecuteReader_GetDataReader(string procedureName, SqlParameter[] parameters, SqlConnection sqlCon, int CommandTimeout)
        {
            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlCommand cmd = new SqlCommand(procedureName, sqlCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = CommandTimeout;
            cmd.Parameters.AddRange(parameters);
            SqlDataReader dr = cmd.ExecuteReader();
            //if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return dr;
        }

        public XmlDocument ExecuteXmlReader(XmlDocument xmlDoc, XmlDocument MultiXmlDoc, string procedureName, SqlConnection sqlCon, int CommandTimeOut)
        {
            XmlDocument xml = new XmlDocument();

            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlCommand sqlCmd = new SqlCommand(procedureName, sqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;

            sqlCmd.CommandTimeout = CommandTimeOut;

            SqlParameter param1 = new SqlParameter("@XmlDoc", SqlDbType.Xml);
            param1.Value = xmlDoc.InnerXml;
            sqlCmd.Parameters.Add(param1);

            SqlParameter param2 = new SqlParameter("@MultiXmlDoc", SqlDbType.Xml);
            param2.Value = MultiXmlDoc.InnerXml;
            sqlCmd.Parameters.Add(param2);

            using (XmlReader xmlReader = sqlCmd.ExecuteXmlReader())
            {
                while (xmlReader.Read())
                {
                    xml.Load(xmlReader);
                }
            }
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return xml;
        }

        public XmlDocument ExecuteXmlReader(XmlDocument MultiXmlDoc, string procedureName, SqlConnection sqlCon, int CommandTimeout)
        {
            XmlDocument xml = new XmlDocument();

            if (sqlCon.State == ConnectionState.Closed) { sqlCon.Open(); }
            SqlCommand cmd = new SqlCommand(procedureName, sqlCon);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = CommandTimeout;
            SqlParameter param1 = new SqlParameter("@MultiXmlDoc", SqlDbType.Xml);
            param1.Value = MultiXmlDoc.InnerXml;
            cmd.Parameters.Add(param1);
            using (XmlReader xmlReader = cmd.ExecuteXmlReader())
            {
                while (xmlReader.Read())
                {
                    xml.Load(xmlReader);
                }
            }
            if (sqlCon.State == ConnectionState.Open) { sqlCon.Close(); }
            return xml;
        }
    }

    public enum ReturnType
    {
        Get,
        Single,
        Double,
        Multiple,
        Retrieve,
        Bulk,
        Set,
        Group,
        Fetch,
        List,
        Check,
        User,
        Data
    }

    public enum Statement
    {
        Insert,
        Update,
        Check,
        Delete,
        Modify,
        Edit,
        Change,
        Generate,
        Service,
        UpdateBillSattus

    }

    public enum SearchFlags
    {
        BussinessUnitSearch = 1,
        ServiceUnitSearch = 2,
        ServiceCenterSearch = 3,
        CycleSearch = 4,
        BookNoSearch = 5,
        RouteSearch = 6,
        MeterSearch = 7,
        UserSearch = 8,
        ChangeMeterBusinessUnit = 9
    }

    public enum Roles
    {
        Admin = 1,
        BUManagerRoleId = 2,
        CommercialManagerRoleId = 3,
        CashierRoleId = 4,
        DataEntryOperatorRoleId = 5,
        SUManagerRoleId = 6,
        MeterReader = 7,
        HeadOffice = 8,
        Accountant = 9,
        CustomerService = 10,
        Supervisior = 11,
        SuperAdmin = 12
    }

    public enum ReportType
    {
        RptBillingStatus,
        RptBillingStatistics,
        RptBillingStatusForSCByBUCustomerType,
        RptCustomerStatisticsByTariffClass,
        RptCustomerStatisticsByTariffClasswithDescription,
        RptGetCustomersBillingStatisticsInfo
    }
    public enum AdjustmentNoBill
    {
        AddAdjustment = 1,
        UpdateAdjustment = 2,
        DeleteAdjustment = 3,
        IsBillExists = 4,
        GetAdjsutmentDetails = 5,
        IsAdjustmentDone = 6
    }
}
