﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using iDSignHelper;
using System.Xml;
using System.Data;

namespace UMS_NigriaDAL
{
    public class CommunicationFeaturesDAL
    {
        #region Members
        SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["Sqlcon"]);
        iDsignDAL objiDsignDAL = new iDsignDAL();
        CommonDAL objCommonDal = new CommonDAL();
        #endregion

        #region StoredProcedures
        public static string PROC_USP_IsEmailFeatureEnabled = "USP_IsEmailFeatureEnabled";
        public static string PROC_USP_IsSMSFeatureEnabled = "USP_IsSMSFeatureEnabled";
        public static string PROC_USP_GetCustomerMobileNo = "USP_GetCustomerMobileNo";
        public static string PROC_USP_GetCustomerEmailId = "USP_GetCustomerEmailId";
        public static string PROC_USP_GetCustomerPaymnetDetForMail = "USP_GetCustomerPaymnetDetForMail";
        public static string PROC_USP_UpdateCommunicationFeatures = "USP_UpdateCommunicationFeatures";
        public static string PROC_USP_GetCommunicatinFeatures = "USP_GetCommunicationFeatures";
        public static string PROC_USP_GetCustomerAdjustmentDetForMail = "USP_GetCustomerAdjustmentDetForMail";
        #endregion

        #region Methods

        public XmlDocument IsEmailFeatureEnabled(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsEmailFeatureEnabled, con);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument IsSMSFeatureEnabled(XmlDocument XmlDoc)
        {
            XmlDocument xml = null;
            try
            {
                xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_IsSMSFeatureEnabled, con); 
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }

        public XmlDocument UpdateCommunicationFeatures(XmlDocument XmlDoc)
        {
            return objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_UpdateCommunicationFeatures, con);
        }

        public DataSet GetCommunicatinFeatures()
        {
            return objCommonDal.ExecuteReader_Get(PROC_USP_GetCommunicatinFeatures, con);
        }

        public XmlDocument GetDetails(XmlDocument XmlDoc, ReturnType value)
        {
            XmlDocument xml = null;
            try
            {
                switch (value)
                {
                    case ReturnType.Get:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerMobileNo, con); break;
                    case ReturnType.Fetch:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerEmailId, con); break;
                    case ReturnType.Bulk:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerPaymnetDetForMail, con); break;
                    case ReturnType.Data:
                        xml = objiDsignDAL.ExecuteXmlReader(XmlDoc, PROC_USP_GetCustomerAdjustmentDetForMail, con); break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xml;
        }
        #endregion
    }
}
